﻿/// <reference path="../../../Common/ResizeSensor.d.ts" />

module Api.ViewModels.Placement {
    import alert = kendo.alert;
    declare var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL: string;
    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;
    export class StudentSummaryVm {

        revenueRedirectButton: JQuery;
        awardsRedirectButton: JQuery;
        ledgerRedirectButton: JQuery;
        paymentPlanRedirectButton: JQuery;
        statusHistoryRedirectButton: JQuery;
        examsRedirectButton: JQuery;
        notesRedirectButton: JQuery;
        progressReportRedirectButton: JQuery;
        moreContactsRedirectButton: JQuery;
        progressReport: Api.ProgressReport;
        currentStudentEnrollment: JQuery;
        statusToolTipContainer: JQuery;
        hoursToolTipContainer: JQuery;
        mainPane: JQuery;
        attendanceSummary: kendo.data.ObservableObject;
        contactSummary: kendo.data.ObservableObject;
        studentSummary: Api.StudentSummary;
        programSummary: kendo.data.ObservableObject;
        constructor() {
            this.revenueRedirectButton = $("#revenueRedirect");
            this.awardsRedirectButton = $("#awardsRedirect");
            this.ledgerRedirectButton = $("#ledgerRedirect");
            this.paymentPlanRedirectButton = $("#paymentPlanRedirect");
            this.statusHistoryRedirectButton = $("#statusHistoryButton");
            this.examsRedirectButton = $("#examsRedirect");
            this.notesRedirectButton = $("#NotesRedirect");
            this.progressReportRedirectButton = $("#progressReportRedirect");
            this.studentSummary = new Api.StudentSummary();
            this.currentStudentEnrollment = $("#currentStudentEnrollment");
            this.statusToolTipContainer = $("#statusToolTipContainer");
            this.hoursToolTipContainer = $("#hoursToolTipContainer");
            this.moreContactsRedirectButton = $("#moreContactsRedirect");
            this.mainPane = $(".main-container");
            this.progressReport = new ProgressReport();
            //create observable for panel control
            this.attendanceSummary = kendo.observable({
                TotalHours: 0 as number,
                ScheduledHours: 0 as number,
                AbsentHours: 0 as number,
                MakeupHours: 0 as number,
                ScheduledHoursWeek: 0 as number,
                ScheduledHoursLastDate: 0 as number,
                LastDateAttended: "" as string,
                AttendancePercentage: 0 as number,
            })
            //bind observable to attendance summary
            kendo.bind($("#attendanceSummary"), this.attendanceSummary);

            //create observable for panel control
            this.programSummary = kendo.observable({
                Status: "" as string,
                StartDate: "" as string,
                DropDate: "" as string,
                DropReason: "" as string,
                IsDropped: false as boolean,
                EnrollmentDate: "" as string,
                ReenrollDate: "" as string,
                GraduationDate: "" as string,
                RevisedGraduationDate: "" as string,
                BadgeId: "" as string,
                StudentGroup: [] as Array<any>,
                OverallGPA: "" as string,
                LoaStartDate: "" as string,
                LoaEndDate: "" as string
        
                
            })
            //bind observable to program summary
            kendo.bind($("#programSummary"), this.programSummary);


            //create observable for panel control
            this.contactSummary = kendo.observable({
                Address: "" as string,
                Phone: "" as string,
                Email: "" as string,
            })
            //bind observable to contact summary
            kendo.bind($("#ContactSummary"), this.contactSummary);


        }

        public initialize() {

            var that = this;

            let progressReport = new ProgressReport();

            $("#ContentMain2_PhoneHidden").kendoMaskedTextBox({
                mask: "(999)-000-0000"
            });
            //$("#PhoneHidden").data("kendoMaskedTextBox");

            that.revenueRedirectButton.on("click",
                e => {
                    window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                        "SA/StudentRevenueLedger.aspx?resid=177&cmpid=" +
                        XMASTER_GET_CURRENT_CAMPUS_ID +
                        "&desc=Student%20Revenue%20Ledger&mod=SA");
                });

            that.awardsRedirectButton.on("click",
                e => {
                    window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                        "FA/StudentAwards.aspx?resid=175&cmpid=" +
                        XMASTER_GET_CURRENT_CAMPUS_ID +
                        "&desc=Awards&mod=SA");
                });

            that.ledgerRedirectButton.on("click",
                e => {
                    window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                        "SA/StudentLedger.aspx?resid=116&cmpid=" +
                        XMASTER_GET_CURRENT_CAMPUS_ID +
                        "&desc=Student%20Ledger&mod=SA");
                });

            that.paymentPlanRedirectButton.on("click",
                e => {
                    window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                        "FA/StudentPaymentPlan.aspx?resid=303&cmpid=" +
                        XMASTER_GET_CURRENT_CAMPUS_ID +
                        "&desc=Payment%20Plans&mod=SA");
                });

            that.statusHistoryRedirectButton.on("click",
                e => {
                    var stuEnrollId = $("#currentStudentEnrollment").val();
                    window.open(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                        "AR/StuStatusChangeHist.aspx?resid=630&mod=AR&cmpid=" +
                        XMASTER_GET_CURRENT_CAMPUS_ID +
                        "&StuEnrollId=" + stuEnrollId, "_blank","toolbar=no,status=no,resizable=yes,scrollbars=yes,width=900px,height=470px");
                });

            that.examsRedirectButton.on("click",
                e => {
                    window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                        "AR/PostExamResults.aspx?resid=542&cmpid=" +
                        XMASTER_GET_CURRENT_CAMPUS_ID +
                        "&desc=Post%20Exam%20Results&mod=AR");
                });

            that.notesRedirectButton.on("click",
                e => {
                    window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                        "SY/StudentNotes.aspx?resid=479&cmpid=" +
                        XMASTER_GET_CURRENT_CAMPUS_ID +
                        "&desc=Notes&mod=AR");
                });

            that.progressReportRedirectButton.on("click",
                e => {
                    var stuEnrollId = $("#currentStudentEnrollment").val();
                    var reportParams = {};
                    reportParams["studentEnrollmentId"] = stuEnrollId;
                    that.progressReport.generateReportByStudentEnrollmentId(stuEnrollId, function (response: Object) {

                    });
                });

            that.moreContactsRedirectButton.on("click", e => {
                window.location.assign(XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL +
                    "AR/StudentContacts.aspx?resid=155&cmpid=" +
                    XMASTER_GET_CURRENT_CAMPUS_ID +
                    "&desc=Students%20Contacts&mod=AR");
            })

            kendo.ui.progress($("#attendanceSummary"), true);

            this.studentSummary.getProgramSummary(this.currentStudentEnrollment.val(), (response) => {
                if (response) {
                    if (response.isDropped) {
                        this.programSummary.set("Status", response.status + " on " + response.dropDateString);

                        let tooltip = $('<span class="k-icon k-i-warning title-iv-warning" id="statusToolTip" title="Drop Reason: ' + response.dropReason + '."></span>') as JQuery;
                        this.statusToolTipContainer.append(tooltip);
                        tooltip.kendoTooltip({
                            width: 150,
                            position: "top"
                        });
                    }
                    else {
                        let tooltip = $("#statusToolTip") as JQuery;
                        if (tooltip.length) {
                            tooltip.data("kendoTooltip").destroy()
                            tooltip.remove();
                        }
                        this.programSummary.set("Status", response.status);

                    }
                    this.programSummary.set("StartDate", response.startDateString);
                    this.programSummary.set("DropDate", response.dropDateString);
                    this.programSummary.set("DropReason", response.dropReason);
                    this.programSummary.set("IsDropped", response.isDropped);
                    this.programSummary.set("EnrollmentDate", response.enrollmentDateString);
                    this.programSummary.set("ReenrollDate", response.reEnrollmentDateString);
                    this.programSummary.set("GraduationDate", response.graduationDateString);
                    this.programSummary.set("RevisedGraduationDate", response.revisedGraduationDateString);
                    this.programSummary.set("BadgeId", response.badgeId);  
                    this.programSummary.set("OverallGPA", Number(response.overallGPA).toFixed(2)); 
                   
                    this.programSummary.set("LoaStartDate", response.loaStartDateString);
                    this.programSummary.set("LoaEndDate", response.loaEndDateString);

                    if (response.loaStartDateString.length < 3) {
                        $('#loaDates').hide()
                       
                    }

                    if (response.studentGroups) {
                        if (response.studentGroups.length) {
                            this.programSummary.set("StudentGroup", response.studentGroups);

                            let listbox1 = $("#StudentGroup").kendoListBox({
                                dataSource: this.programSummary.get("StudentGroup")
                            });

                        }
                    }

                    this.studentSummary.getAttendanceSummary(this.currentStudentEnrollment.val(),
                        (response) => {
                            if (response) {

                                if (response.transferHours > 0) {
                                    this.attendanceSummary.set("TotalHours", (response.actualHours + response.transferHours).toFixed(2));

                                    let tooltip = $('<span class="k-icon k-i-warning title-iv-warning" id="hoursToolTip" title="' + response.actualHours + ' Hours in this school'
                                        + '<br>'
                                        + response.transferHours + ' Transfer Hours"></span>') as JQuery;
                                    this.hoursToolTipContainer.append(tooltip);
                                    tooltip.kendoTooltip({
                                        width: 170,
                                        position: "top"
                                    });
                                }
                                else {
                                    let tooltip = $("#hoursToolTip") as JQuery;
                                    if (tooltip.length) {
                                        tooltip.data("kendoTooltip").destroy()
                                        tooltip.remove();
                                    }
                                    this.attendanceSummary.set("TotalHours", response.actualHours);

                                }

                                this.attendanceSummary.set("ScheduledHours", response.scheduledHours);
                                this.attendanceSummary.set("AbsentHours", response.absentHours);
                                this.attendanceSummary.set("MakeupHours", response.makeupHours);
                                this.attendanceSummary.set("ScheduledHoursWeek", response.scheduledHoursPerWeek);
                                this.attendanceSummary.set("ScheduledHoursLastDate", response.scheduledHoursLastDate);
                                this.attendanceSummary.set("LastDateAttended", response.lastDateAttendedString);
                                this.attendanceSummary.set("AttendancePercentage", response.attendancePercentageFormatted);

                            }
                            kendo.ui.progress($("#attendanceSummary"), false);

                        }, false);
                  
                }

            }, false)
     

            this.studentSummary.getContactSummary(this.currentStudentEnrollment.val(),
                (response) => {
                    if (response) {
                        this.contactSummary.set("Address", response.fullAddress);
                        this.contactSummary.set("PhoneHidden", response.phone);
                        this.contactSummary.set("EmailHidden", response.email);
                        this.contactSummary.set("Phone", $("#ContentMain2_PhoneHidden").val());
                        this.contactSummary.set("Email", $("#ContentMain2_EmailHidden").val());
                        $("#ContentMain2_emailLink").prop("href", "mailto:" + $("#ContentMain2_EmailHidden").val());
                    }
                }, false);
        }
    }
}