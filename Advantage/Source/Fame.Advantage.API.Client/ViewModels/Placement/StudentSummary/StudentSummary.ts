﻿module Api.ViewModels.Placement {
    //import studentSummaryVm = ViewModels.Placement.StudentSummaryVm;

    export class StudentSummary {
        studentSummaryVm: StudentSummaryVm;

        constructor() {
            this.studentSummaryVm = new StudentSummaryVm();
        }

        public initialize() {
            this.studentSummaryVm.initialize();
        }
    }
}