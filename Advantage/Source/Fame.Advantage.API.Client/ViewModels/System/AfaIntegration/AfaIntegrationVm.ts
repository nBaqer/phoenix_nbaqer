﻿/// <reference path="../../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" />
module Api.ViewModels.System {
    import KendoGrid = Api.Components.Grid;
    import AfaIntegrationObj = Api.AfaIntegration;
    import ISettings = API.Models.ISettings;
    import IAfaStudentExist = API.Models.IAfaStudentExist;
    import ILeadBase = API.Models.ILeadBase;
    import IAdvStagingDemographicsV1 = API.Models.IAdvStagingDemographicsV1;
    import ICatalog = API.Models.ICatalog;
    import ILead = API.Models.ILead;
    import IAfaLeadSyncException = API.Models.IAfaLeadSyncException;
    import IAdvStagingProgramV1 = API.Models.IAdvStagingProgramV1;
    import IAdvStagingEnrollmentV1 = API.Models.IAdvStagingEnrollmentV1;
    import IAdvStagingPaymentPeriodV1 = API.Models.IAdvStagingPaymentPeriodV1;
    import IAdvStagingAwardV1 = API.Models.IAdvStagingAwardV1;
    import IAdvStagingDisbursementV1 = API.Models.IAdvStagingDisbursementV1;
    import IAdvStagingAttendanceV1 = API.Models.IAdvStagingAttendanceV1;

    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;

    export class AfaIntegrationVm {
        afaIntObj: AfaIntegrationObj;
        //gvDemographics: KendoGrid;
        constructor() {
            this.afaIntObj = new AfaIntegrationObj();
            this.initialize(); // initializing the grid and writing the click events of the buttons
        }

        public initialize() {
            var that = this;
            // click event of the button
            $("#btnSyncDemographics").on("click",
                () => {
                    // make a call to the sync all method
                    this.syncAllDemographics();
                });
            $("#btnSyncProgramVersion").on("click",
                () => {
                    // make a call to the sync all method
                    this.syncAllProgramVersions();
                });
            $("#btnSyncEnrollments").on("click",
                () => {
                    // make a call to the sync all method
                    this.syncAllEnrollments();
                });
            $("#btnSyncPaymentPeriod").on("click",
                () => {
                    // make a call to the sync all method
                    this.syncAllPaymentPeriods();
                });
            $("#btnSyncPaymentPeriodAttendance").on("click",
                () => {
                    // make a call to the sync all method
                    this.syncAllAttendance();
                });
            $("#btnSyncAwards").on("click",
                () => {
                    // make a call to the sync all method
                    this.syncAllAwards();
                });
            $("#btnSyncDisbursements").on("click",
                () => {
                    // make a call to the sync all method
                    this.syncAllDisbursements();
                });
            //$("#btnSyncRefunds").on("click",
            //    () => {
            //        // make a call to the sync all method
            //        this.syncAllRefunds();
            //    });

        }


        // insert into exception table
        private syncAllDemographics() {
            var that = this;
            // get catalog
            let catalog = [] as Array<ICatalog>;
            that.afaIntObj.getCatalog((catalogResponse: any) => {
                if (catalogResponse.length > 0) {
                    catalog = <Array<ICatalog>>catalogResponse;
                    var sessionKey: string;
                    that.afaIntObj.getAfaSessionKey((sessionKeyResponse: any) => {
                        if (sessionKeyResponse != null) {
                            sessionKey = sessionKeyResponse.sessionKey;
                            if (sessionKey != null) {
                                // get all demographics
                                that.afaIntObj.getAllDemographics(sessionKey, XMASTER_GET_CURRENT_CAMPUS_ID,
                                    (response: any) => {
                                    });
                            }
                        }
                    });

                } else {
                    catalog = null;
                }
            });

        }
        groupByArray(xs, key) {
            return xs.reduce(function (rv, x) {
                let v = key instanceof Function ? key(x) : x[key];
                let el = rv.find((r) => r && r.key === v);
                if (el) { el.values.push(x); }
                else {
                    rv.push({ key: v, values: [x] });
                }
                return rv;
            }, []);
        }

        groupBy = function (xs, key) {
            return xs.reduce(function (rv, x) {
                (rv[x[key]] = rv[x[key]] || []).push(x);
                return rv;
            }, {});
        };
        private chunks(array, size: number) {
            var results = [];
            while (array.length) {

                results.push(array.splice(0, size));
            }
            return results;
        };
        private chunksByKey(array, size: number, key: string) {
            var results = [];
            var groupedResults = this.groupBy(array, key);
            var chunk = [];

            Object.keys(groupedResults).forEach(function (key, index) {
                let val = groupedResults[key];
                if ((val.length + chunk.length) > size) {
                    results.push(chunk.slice())
                    chunk = [];
                }
                chunk.push(val)
                chunk = chunk.reduce((acc, val) => acc.concat(val), []);;
            });
            //$.each(groupedResults, (i, val) => {

            //})
            if (chunk.length)
                results.push(chunk)
            return results;
        };
        private syncAllProgramVersions() {
            var that = this;
            let prgVersions = [] as Array<IAdvStagingProgramV1>;
            that.afaIntObj.getCmsIdForCampusId(XMASTER_GET_CURRENT_CAMPUS_ID,
                (getCmsId: any) => {
                    if (getCmsId.length > 0) {
                        let cmsId = getCmsId;
                        if (cmsId != null) {
                            that.afaIntObj.getAllProgramVersionsForCampus(XMASTER_GET_CURRENT_CAMPUS_ID,
                                (ProgVersionResponse: any) => {
                                    if (ProgVersionResponse.length > 0) {
                                        prgVersions = <Array<IAdvStagingProgramV1>>ProgVersionResponse;
                                        let prgVersionsToPost = [] as Array<IAdvStagingProgramV1>;
                                        //for each program version call to post
                                        for (var i = 0; i < ProgVersionResponse.length; i++) {
                                            let prgVersionToPost = {} as IAdvStagingProgramV1;
                                            prgVersionToPost.locationCMSID = cmsId;
                                            prgVersionToPost.programName = ProgVersionResponse[i].programName;
                                            prgVersionToPost.dateCreated = ProgVersionResponse[i].dateCreated;
                                            prgVersionToPost.programVersionID = ProgVersionResponse[i].programVersionID;
                                            prgVersionToPost.dateUpdated = ProgVersionResponse[i].dateUpdated;
                                            prgVersionToPost.userCreated = ProgVersionResponse[i].userCreated;
                                            prgVersionToPost.userUpdated = ProgVersionResponse[i].userUpdated;
                                            prgVersionsToPost.push(prgVersionToPost);
                                        }
                                        // final call
                                        this.afaIntObj.postProgramVersionToAfa(prgVersionsToPost, (pvResponse: any) => {
                                            if (pvResponse != null) {

                                            }
                                        });
                                    }
                                });



                        }
                    }
                });


        }

        private syncAllEnrollments() {
            var that = this;
            let enrollments = [] as Array<IAdvStagingEnrollmentV1>;
            that.afaIntObj.getAllEnrollmentsForAfaSync(XMASTER_GET_CURRENT_CAMPUS_ID,
                (EnrollmentResponse: any) => {
                    if (EnrollmentResponse.length > 0) {
                        enrollments = <Array<IAdvStagingEnrollmentV1>>EnrollmentResponse;
                        let EnrollmentsToPost = [] as Array<IAdvStagingEnrollmentV1>;
                        for (var i = 0; i < EnrollmentResponse.length; i++) {
                            let enrollmentToPost = {} as IAdvStagingEnrollmentV1;
                            enrollmentToPost.locationCMSID = EnrollmentResponse[i].locationCMSID;
                            enrollmentToPost.sisEnrollmentID = EnrollmentResponse[i].sisEnrollmentID;
                            enrollmentToPost.programName = EnrollmentResponse[i].programName;
                            enrollmentToPost.programVersionID = EnrollmentResponse[i].programVersionID;
                            enrollmentToPost.idStudent = EnrollmentResponse[i].idStudent;
                            enrollmentToPost.startDate = EnrollmentResponse[i].startDate;
                            enrollmentToPost.gradDate = EnrollmentResponse[i].gradDate;
                            enrollmentToPost.dateCreated = EnrollmentResponse[i].dateCreated;
                            enrollmentToPost.dateUpdated = EnrollmentResponse[i].dateUpdated;
                            enrollmentToPost.userCreated = EnrollmentResponse[i].userCreated;
                            enrollmentToPost.userUpdated = EnrollmentResponse[i].userUpdated;
                            enrollmentToPost.LeaveOfAbsenceDate = EnrollmentResponse[i].leaveOfAbsenceDate;
                            enrollmentToPost.DroppedDate = EnrollmentResponse[i].droppedDate;
                            enrollmentToPost.LastDateAttendance = EnrollmentResponse[i].lastDateAttendance;
                            enrollmentToPost.StatusEffectiveDate = EnrollmentResponse[i].statusEffectiveDate;
                            enrollmentToPost.AdmissionsCriteria = EnrollmentResponse[i].admissionsCriteria;
                            enrollmentToPost.EnrollmentStatus = EnrollmentResponse[i].enrollmentStatus;
                            enrollmentToPost.SSN = EnrollmentResponse[i].ssn;
                            enrollmentToPost.ReturnFromLOADate = EnrollmentResponse[i].returnFromLOADate;
                            EnrollmentsToPost.push(enrollmentToPost);
                        }
                        // final call
                        this.afaIntObj.postEnrollmentsToAfa(EnrollmentsToPost,
                            (pvResponse: any) => {
                                if (pvResponse != null) {
                                    console.log("Sync enrollments complete.");
                                }
                            });
                    }
                });
        }

        private syncAllPaymentPeriods() {
            var that = this;
            that.afaIntObj.getAllPaymentPeriods(XMASTER_GET_CURRENT_CAMPUS_ID,
                (response: any) => {
                    if (response.length > 0) {
                        let paymentPeriods = [] as Array<IAdvStagingPaymentPeriodV1>;
                        paymentPeriods = <Array<IAdvStagingPaymentPeriodV1>>response
                        let ppChunks = that.chunksByKey(paymentPeriods, 250, "sisEnrollmentID");
                        for (var i = 0; i < ppChunks.length; i++) {
                            let pp = ppChunks[i];
                            that.afaIntObj.postPaymentPeriods(pp, () => { });
                        }
                        console.log("Sync payment periods complete.");
                    }
                });


        }

        private syncAllAwards() {
            var that = this;
            that.afaIntObj.getAllAwards(XMASTER_GET_CURRENT_CAMPUS_ID,
                (response: any) => {
                    if (response.length > 0) {
                        let awards = [] as Array<IAdvStagingAwardV1>;
                        awards = <Array<IAdvStagingAwardV1>>response
                        console.log(awards.length);
                        let awardChunks = that.chunksByKey(awards, 250, "sisEnrollmentID");
                        for (var i = 0; i < awardChunks.length; i++) {
                            let a = awardChunks[i];
                            that.afaIntObj.syncAwards(a, (result) => { console.log(result) })
                        }
                        console.log("Sync awards complete.");

                    }
                });


        }
        private syncAllDisbursements() {
            var that = this;
            //that.afaIntObj.syncDisbursements((result) => { console.log(result) })
            that.afaIntObj.getAllDisbursements(XMASTER_GET_CURRENT_CAMPUS_ID,
                (response: any) => {
                    if (response.length > 0) {
                        let disbursements = [] as Array<IAdvStagingDisbursementV1>;
                        disbursements = <Array<IAdvStagingDisbursementV1>>response
                        let disChunks = that.chunksByKey(disbursements, 250, "awardID");
                        for (var i = 0; i < disChunks.length; i++) {
                            let dis = disChunks[i];
                            that.afaIntObj.syncDisbursements(dis, (result) => { console.log(result) })
                        }
                        console.log("Sync disbursements complete.");

                    }
                });
        }

        private syncAllAttendance() {
            var that = this;

            //that.afaIntObj.calculateAllAttendanceUpdatesForCampus(XMASTER_GET_CURRENT_CAMPUS_ID,
            //    (response: any) => {
            //        if (response.length > 0) {
            //            let attendanceUpdates = [] as Array<IAdvStagingAttendanceV1>;
            //            attendanceUpdates = <Array<IAdvStagingAttendanceV1>>response
            //            that.afaIntObj.postAttendanceUpdatesToAfa(attendanceUpdates, () => { })

            that.afaIntObj.getAllEnrollmentIdsForAfa(XMASTER_GET_CURRENT_CAMPUS_ID,
                (response: any) => {
                    if (response.length > 0) {
                        let enrollmentIds = [] as Array<string>;
                        enrollmentIds = <Array<string>>response
                        let enrollmentIdChunks = that.chunks(enrollmentIds, 25);
                        for (var i = 0; i < enrollmentIdChunks.length; i++) {
                            let enrollmentIdChunk = enrollmentIdChunks[i];
                            that.afaIntObj.calculateAttendanceUpdatesForEnrollments(enrollmentIdChunk,
                                (attResponse: any) => {
                                    if (attResponse.length > 0) {
                                        let attendanceUpdates = [] as Array<IAdvStagingAttendanceV1>;
                                        attendanceUpdates = <Array<IAdvStagingAttendanceV1>>attResponse;
                                        that.afaIntObj.postAttendanceUpdatesToAfa(attendanceUpdates, () => { });
                                    }

                                });
                        }

                        //for (var i = 0; i < enrollmentIds.length; i++) {
                        //    let enrollmentId = enrollmentIds[i];
                        //    that.afaIntObj.calculateAttendanceUpdatesForEnrollment(enrollmentId,
                        //        (attResponse: any) => {
                        //            if (attResponse.length > 0) {
                        //                let attendanceUpdate = null as IAdvStagingAttendanceV1;
                        //                attendanceUpdate = <IAdvStagingAttendanceV1>attResponse
                        //                that.afaIntObj.postAttendanceUpdatesToAfa([attendanceUpdate], () => { })
                        //            }

                        //        })
                        //}

                        console.log("Sync attendance complete.");
                    }

                });


        }

        private syncAllRefunds() {
            var that = this;
            that.afaIntObj.postAllRefundsToAfa(XMASTER_GET_CURRENT_CAMPUS_ID,
                (response: any) => {
                    if (response.length > 0)
                        console.log("Sync refunds complete.");
                });


        }
    }
}