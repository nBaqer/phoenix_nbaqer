﻿module Api.ViewModels.System {
    import CampusDropDownList = Api.Components.CampusDropDownList;

    export class ManageUser {

        manageUserVm: ManageUserVm;
        constructor(hostName?: string) {
            this.manageUserVm = new ManageUserVm(hostName);
        }

        public initialize() {
            this.manageUserVm.initialize();
        }
    }
}