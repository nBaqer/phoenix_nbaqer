﻿module Api.System.ManageUsers.Constants {
    export var ADVANTAGE_USER = "Advantage";
    export var VENDOR = "Vendor";
    export var PASSWORD_SET_URL = "SetPassword.aspx";
    export var PASSWORD_RESET_URL = "PasswordResetDefault.aspx";
    export var X_PLEASE_FILL_REQUIRED_FIELDS = "Please enter the required values.";
    export var UPDATE_SUCCESS = "Record updated successfully";
    export var UPDATE_UNSUCCESSFULL = "Failed updating Record";
    export var SAVE_SUCCESS = "Record saved successfully";
    export var SAVE_UNSUCCESSFULL = "Failed saving Record";
    export var USERNAME_EXISTS = "Username already exists in the database";
    export var EMAIL_EXISTS = "Email already exists in the database";
    export var EMAIL_EXISTS_TENANT = "Email already exists in the Advantage";

}