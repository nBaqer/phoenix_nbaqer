﻿/// <reference path="../../../../Fame.Advantage.Client.AD/Common/DropDownOutputModel.ts" />
/// <reference path="../../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" />
module Api.ViewModels.System {
    import DropDownList = Api.Components.DropDownList;
    import KendoGrid = Api.Components.Grid;
    import X_PLEASE_FILL_REQUIRED_FIELDS = Api.System.ManageUsers.Constants.X_PLEASE_FILL_REQUIRED_FIELDS;
    import SAVE_SUCCESSFUL = Api.System.ManageUsers.Constants.SAVE_SUCCESS;
    import ADVANTAGE_USER = Api.System.ManageUsers.Constants.ADVANTAGE_USER;
    import VENDOR = Api.System.ManageUsers.Constants.VENDOR;
    import PASSWORD_RESET_URL = Api.System.ManageUsers.Constants.PASSWORD_RESET_URL;
    import PASSWORD_SET_URL = Api.System.ManageUsers.Constants.PASSWORD_SET_URL;
    import UPDATE_UNSUCCESSFUL = Api.System.ManageUsers.Constants.UPDATE_UNSUCCESSFULL;
    import USERNAME_EXISTS = Api.System.ManageUsers.Constants.USERNAME_EXISTS;
    import EMAIL_EXISTS = Api.System.ManageUsers.Constants.EMAIL_EXISTS;
    import EMAIL_EXISTS_TENANT = Api.System.ManageUsers.Constants.EMAIL_EXISTS_TENANT;
    import ISettings = API.Models.ISettings;
    import IFilterSearch = API.Models.IFilterSearch;
    import IFilterParams = API.Models.IFilterParameters;
    import ManageUserObj = Api.ManageUser;
    declare var CURRENT_USER_EMAIL: string;

    export class ManageUserVm {
        userProfileValidator: kendo.ui.Validator;
        userRolesValidator: kendo.ui.Validator;
        userRoles: { [id: string]: UserRole; };
        userGrid: KendoGrid;
        campusDropDownList: DropDownList;
        rolesDropDownList: DropDownList;
        extraRolesList: Array<DropDownList>;
        userSearchFilter: JQuery;
        userTypeFilter: DropDownList;
        userTypeDropDownList: DropDownList;
        moduleDropDownList: DropDownList;
        statusDropDownList: DropDownList;
        manageUserPanel: kendo.ui.PanelBar;
        userProfilePanel: kendo.data.ObservableObject;
        canExpand: boolean;
        userProfilePanelOpen: boolean;
        roleCounter: number;
        selectedUserType: string;
        selectedUserTypeText: string;
        selectedUserEmail: string;
        hostName?: string;
        campusGroup: CampusGroup;
        roles: Roles;
        userApi: User;
        roleParams: IFilterParams;
        manageUser: ManageUserObj;
        emptyGuid: string = "00000000-0000-0000-0000-000000000000";
        constructor(hostName?: string) {
            this.userSearchFilter = $("#txtUserSearch");
            this.userRoles = { "1": new UserRole() };
            this.canExpand = false;
            this.userProfilePanelOpen = false;
            this.roleCounter = 1;
            this.selectedUserType = "";
            this.campusGroup = new CampusGroup();
            this.roles = new Roles();
            this.userApi = new User();
            this.roleParams = {} as IFilterParams;
            this.manageUser = new ManageUserObj();
            this.hostName = hostName;
            this.extraRolesList = [];
        }

        public initialize() {
            var that = this;

            let userType = new UserType();
            let resource = new Resource();
            let status = new Status();

            let gridSettings: ISettings = {} as ISettings;
            let gridParams: IFilterParams = {} as IFilterParams;
            let gridSearch: IFilterSearch = {} as IFilterSearch;
            let userTypeGridFilterSettings: ISettings = {} as ISettings;
            userTypeGridFilterSettings.settings = { optionLabel: "All" };

            gridSettings.settings = {
                pageable: {
                    info: true,
                    refresh: false,
                    pageSizes: 20,
                    previousNext: true,
                    numeric: false
                },
                columns: [{
                    template: dataItem => "<a class='fullNameLink' href='#'>" + kendo.htmlEncode(dataItem.value.fullName) + "</a>",
                    field: "value.fullName",
                    title: "Full Name",
                    width: 180
                }, {
                    field: "value.email",
                    title: "Email",
                    width: 150
                }, {
                    field: "value.displayName",
                    title: "Display Name",
                    width: 125
                }, {
                    field: "value.userType",
                    title: "Type",
                    width: 100
                },
                {
                    template: dataItem => {
                        let status = dataItem.value.status;

                        if (status === "Active") {

                            return "<span>" + kendo.htmlEncode(status) + "</span>";
                        }

                        return "<span style='color:red;'>" + kendo.htmlEncode(status) + "</span>";
                    },
                    field: "value.status",
                    title: "Status",
                    width: 75
                }]

            };

            //Initialzie user grid
            that.userGrid = new KendoGrid("userGrid", that.userApi, undefined, undefined, gridSettings);

            //initialize dropdowns
            that.moduleDropDownList = new DropDownList("ddlModule", resource, { params: { resourceTypeId: 1 } } as IFilterParams, undefined, () => { });
            that.statusDropDownList = new DropDownList("ddlUserStatus", status, undefined, { settings: { dataValueField: "value" } } as ISettings, () => { });
            that.userTypeFilter = new DropDownList("ddlUserTypeFilter", userType, undefined, userTypeGridFilterSettings, () => { });
            that.campusDropDownList = new DropDownList("ddCampus1", that.campusGroup, undefined, undefined, () => { });
            that.rolesDropDownList = new DropDownList("ddRoles1", that.roles, undefined, undefined, () => { });

            //create observable for panel control
            that.userProfilePanel = kendo.observable({
                isAdvantageUser: false,
                isVendor: false,
                isVisible: false,
                userStatusRadio: "All",
                canViewUsers: false,
                canResetPassword: false,
                canCancelStepTwo: false,
                canPrint: false,
                canSave: true,
                hasStarted: false,
                user: { userId: that.emptyGuid, userType: "0", fullName: "", displayName: "", email: "", userStatus: that.emptyGuid, module: "0", campusRoles: that.userRoles },
                viewUsers: function () {
                    gridSearch.filter = that.userSearchFilter.val();
                    //reset params
                    gridParams.params = {};

                    if (that.userTypeFilter.hasValue()) {
                        gridParams.params["userTypeId"] = that.userTypeFilter.getValue();
                    }
                    let userStatus = this.get("userStatusRadio");
                    if (userStatus !== "All") {
                        if (userStatus === "Active") {
                            gridParams.params["status"] = true;
                        }
                        else if (userStatus === "Inactive") {
                            gridParams.params["status"] = false;
                        }
                    }
                    that.userGrid.reload(gridSearch, gridParams);
                    this.set("canViewUsers", true);
                    this.set("canPrint", true);

                },
                show: function () {
                    this.set("isVisible", true);
                },
                hide: function () {
                    this.set("isVisible", false);
                },
                showAdvantageUserFields: function () {

                    this.set("isAdvantageUser", true);
                },
                hideAdvantageUserFields: function () {

                    this.set("isAdvantageUser", false);
                },
                showVendorFields: function () {

                    this.set("isVendor", true);
                },
                hideVendorFields: function () {

                    this.set("isVendor", false);
                },
                resetPassword: function () {
                    if (that.userProfilePanel.get("user.userId") !== that.emptyGuid && that.userProfilePanel.get("user.email") !== "" && that.userProfilePanel.get("user.accountActive")) {
                        let email = { to: [that.userProfilePanel.get("user.email")], link: that.hostName.replace("default.aspx", PASSWORD_SET_URL), userTypeCode: that.userProfilePanel.get("user.userTypeCode") }
                        //send email here
                        that.userApi.sendUserResetPasswordEmail(email,
                            (response: Object) => {
                                if (response["hasPassed"]) {
                                    MasterPage.SHOW_INFO_WINDOW(
                                        "An email with instructions on how to reset the user's password has been sent to the user's registered email address.");

                                } else {
                                    MasterPage.SHOW_ERROR_WINDOW(response["resultStatus"]);
                                }

                            });



                    }

                    else if (!that.userProfilePanel.get("user.accountActive")) {
                        MasterPage.SHOW_INFO_WINDOW(
                            "The account must be marked as active before attempting to reset the password.");
                    }
                },
                save: function () {

                    that.resetValidator("#liManageUserUserProfile");
                    that.resetValidator("#liManageUserUserRoles");
                    if (that.userProfileValidator.validate() && that.userRolesValidator.validate()) {


                        let userModel = that.userProfilePanel.get("user").toJSON();
                        let userTypeText = that.userTypeDropDownList.getKendoDropDown().text();
                        if (userTypeText === VENDOR) {
                            //reset module and display name
                            userModel.module = "0";
                            userModel.displayName = "";
                        }

                        let user = {};
                        let cr = [];
                        let foo = userModel["campusRoles"];
                        for (var k in foo) {
                            if (foo.hasOwnProperty(k)) {
                                cr.push(foo[k]);
                            }
                        }
                        user["userRoles"] = cr;
                        user["fullName"] = userModel.fullName;
                        user["displayName"] = userModel.displayName;
                        user["email"] = userModel.email;
                        user["userId"] = userModel.userId;
                        user["userTypeId"] = userModel.userType;
                        user["userStatusId"] = userModel.userStatus;
                        user["defaultModuleId"] = userModel.module;
                        //POST TO SERVER HERE
                        //Edit
                        if (user["userId"] !== that.emptyGuid && that.userProfilePanel.get("canResetPassword")) {
                            if (that.selectedUserEmail === CURRENT_USER_EMAIL) {
                                $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(
                                    "You are changing the email associated with your account. You will need to login again if you continue."))
                                    .then((confirmed) => {
                                        if (confirmed) {
                                            that.updateUser(user);
                                            window.location.replace("../Logout.aspx");
                                        }
                                    });
                            } else {
                                that.updateUser(user);
                            }

                        }

                        //Create
                        else {
                            that.manageUser.saveUser(user,
                                (response: Object) => {
                                    if (response["hasPassed"]) {
                                        if (response["sendEmail"]) {
                                            MasterPage.SHOW_INFO_WINDOW_PROMISE(
                                                "The user has been created. If active, an email with instructions on how to set up the user's password will be sent to the user's registered email address shortly.")
                                                .done(() => {
                                                    if (response["accountActive"]) {
                                                        let email = {
                                                            to: [user["email"]],
                                                            link: that.hostName.replace("default.aspx",
                                                                PASSWORD_SET_URL),
                                                            userTypeCode: response["userTypeCode"]
                                                        };
                                                        //send email here
                                                        that.userApi.sendUserEmail(email,
                                                            (response: Object) => {
                                                                if (!response["hasPassed"]) {
                                                                    MasterPage.SHOW_ERROR_WINDOW(response["resultStatus"]);
                                                                }

                                                            });

                                                    }

                                                });
                                        } else {
                                            MasterPage.SHOW_INFO_WINDOW(
                                                "The user has been created.");
                                        }
                                        that.userProfilePanel.get("cancelStepThree()");
                                    }
                                    else {
                                        MasterPage.SHOW_ERROR_WINDOW(
                                            "An error occured when trying to create the user." +
                                            "\n" +
                                            response["resultStatus"]);
                                    }


                                });
                        }
                    } else {
                        MasterPage.SHOW_WARNING_WINDOW(X_PLEASE_FILL_REQUIRED_FIELDS);
                    }
                },
                cancelStepTwo: function () {

                    //reset global user roles pointer
                    that.userRoles = { "1": new UserRole() }

                    //reset observable properties
                    this.set("isAdvantageUser", false);
                    this.set("isVendor", false);
                    this.set("isVisible", false);
                    this.set("canResetPassword", false);
                    this.set("canCancelStepTwo", false);
                    this.set("canSave", true);
                    this.set("canViewUsers", false);
                    this.set("canPrint", false);
                    this.set("userStatusRadio", "All");
                    this.set("hasStarted", false);
                    this.set("user",
                        {
                            userId: that.emptyGuid,
                            userType: "0",
                            fullName: "",
                            displayName: "",
                            email: "",
                            userStatus: that.emptyGuid,
                            module: "0",
                            campusRoles: that.userRoles
                        });

                    //collapse panel
                    that.collapseUserProfilePanel();

                    that.resetUserProfilePanel();

                    //reset validators
                    that.resetValidator("#liManageUserUserProfile");
                    //expand first panel
                    that.expandSelectTaskPanel();

                },
                cancelStepThree: function () {
                    //reset global user roles pointer
                    that.userRoles = { "1": new UserRole() }

                    //reset observable properties
                    this.set("isAdvantageUser", false);
                    this.set("isVendor", false);
                    this.set("isVisible", false);
                    this.set("canResetPassword", false);
                    this.set("canSave", true);
                    this.set("canCancelStepTwo", false);

                    //reset select task panel
                    this.set("canViewUsers", false);
                    this.set("canPrint", false);
                    that.userSearchFilter.val("");
                    that.userTypeFilter.reset();
                    this.set("userStatusRadio", "All");

                    this.set("hasStarted", false);
                    this.set("user",
                        {
                            userId: that.emptyGuid,
                            userType: "0",
                            fullName: "",
                            displayName: "",
                            email: "",
                            userStatus: that.emptyGuid,
                            module: "0",
                            campusRoles: that.userRoles
                        });


                    //collapse panel
                    that.collapseUserRolePanel();

                    that.resetUserRolePanel();

                    //collapse panel
                    that.collapseUserProfilePanel();

                    that.resetUserProfilePanel();


                    //reset validators
                    that.resetValidator("#liManageUserUserProfile");
                    that.resetValidator("#liManageUserUserRoles");

                    //expand first panel
                    that.expandSelectTaskPanel();
                },
                showCancelConfirmation: function () {
                    $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(
                        "Are you sure that you want to cancel your changes?"))
                        .then((confirmed) => {
                            if (confirmed) {
                                that.userProfilePanel.get("cancelStepThree()");
                            }
                        });

                }
            });

            //use kendo panel bar for collapse/expand control
            $("#manage-user-progress").kendoPanelBar({
                expand: function (e) {
                    if (e.item.id == 'liManageUserSelectTask' && that.userProfilePanel.get("hasStarted")) {
                        e.preventDefault();
                    }
                    if (e.item.id == 'liManageUserUserProfile' && !that.canExpand) {
                        e.preventDefault();
                    }
                    if (e.item.id == 'liManageUserUserRoles') {
                        if (that.userProfilePanelOpen) {
                            that.userProfileValidator.destroy();
                            that.userProfileValidator = MasterPage.ADVANTAGE_VALIDATOR("liManageUserUserProfile");
                            that.resetValidator("#liManageUserUserProfile");
                            if (that.userProfileValidator.validate()) {
                                that.userProfilePanel.set("canCancelStepTwo", false);
                                return;
                            } else {
                                MasterPage.SHOW_WARNING_WINDOW(X_PLEASE_FILL_REQUIRED_FIELDS);
                            }
                        }
                        e.preventDefault();

                    }

                    that.canExpand = false;
                },
                expandMode: "multiple",
                collapse: function (e) {
                    //if is adding or editing user, prevent panel collapse and return
                    if (that.userProfilePanel.get("hasStarted")) {
                        e.preventDefault();
                        return;
                    }

                    if (e.item.id == 'liManageUserUserProfile') {
                        that.userProfilePanelOpen = false;
                    }
                }
            });

            that.manageUserPanel = $("#manage-user-progress").data("kendoPanelBar");


            //expand first panel by default
            that.expandSelectTaskPanel();

            //bind observable to main panel
            kendo.bind($("#manage-user-progress"), this.userProfilePanel);


            //initalize dropdown dependent on observable panel
            that.userTypeDropDownList = new DropDownList("ddlUserType", userType, undefined, undefined, ((value) => {
                that.selectedUserType = this.userTypeDropDownList.getValue();
                that.selectedUserTypeText = this.userTypeDropDownList.getKendoDropDown().text();

                that.setUserProfileListeners(false);

                if (that.selectedUserTypeText === ADVANTAGE_USER) {
                    this.userProfilePanel.get("showAdvantageUserFields()");
                    this.userProfilePanel.get("hideVendorFields()");
                    this.userProfilePanel.get("show()");
                }
                else if (that.selectedUserTypeText === VENDOR) {
                    this.userProfilePanel.get("hideAdvantageUserFields()");
                    this.userProfilePanel.get("showVendorFields()");
                    that.userProfilePanel.set("user.displayName", "");
                    that.userProfilePanel.set("user.module", "0");
                    this.userProfilePanel.get("show()");


                } else {
                    this.userProfilePanel.get("hideAdvantageUserFields()");
                    this.userProfilePanel.get("hideVendorFields()");
                    this.userProfilePanel.get("hide()");

                }
                that.roleParams.params = { userTypeId: that.selectedUserType };
                that.rolesDropDownList.reload(that.roleParams);

                $.each(that.extraRolesList,
                    (i, val) => {
                        val.reload(that.roleParams);
                    });

                that.resetValidator("#liManageUserUserProfile");

                that.setUserProfileListeners(true);


            }) as any);

            //initialize show user type help box
            that.showUserTypeHelp();


            //Initialize Validators
            that.userProfileValidator = MasterPage.ADVANTAGE_VALIDATOR("liManageUserUserProfile");
            that.userRolesValidator = MasterPage.ADVANTAGE_VALIDATOR("liManageUserUserRoles");

            //event for adding users
            $("#addUserBtn").on("click",
                () => {
                    if (that.userProfilePanel.get("canViewUsers")) {

                        that.userProfilePanel.set("canViewUsers", false);
                        that.userProfilePanel.set("canPrint", false);
                        that.userProfilePanel.set("canResetPassword", false);

                    }


                    that.userProfilePanel.set("hasStarted", true);
                    that.canExpand = true;
                    that.expandUserProfilePanel();
                });

            //event for adding roles
            $("#addRoleBtn").on("click",
                () => {
                    that.userRoles = that.userProfilePanel.get("user.campusRoles");
                    that.userRoles[that.roleCounter + 1] =
                        new UserRole({ campusGroupId: "", roleId: "" });
                    that.userProfilePanel.set("user.campusRoles", that.userRoles);
                    that.appendNewRoleRow("#userRolesSection");
                    kendo.unbind($("#manage-user-progress"));
                    kendo.bind($("#manage-user-progress"), that.userProfilePanel);
                    that.userRolesValidator.destroy();
                    that.userRolesValidator = MasterPage.ADVANTAGE_VALIDATOR("liManageUserUserRoles");
                });

            //events for deleting roles
            $(document).on("click", ".deleteRoleBtn",
                (e) => {
                    let parent = $(e.target).closest(".formRow");
                    that.userRoles = that.userProfilePanel.get("user.campusRoles");
                    delete that.userRoles[parent.attr("data-id")];
                    that.userProfilePanel.set("user.campusRoles", that.userRoles);
                    parent.remove();
                    kendo.unbind($("#manage-user-progress"));
                    kendo.bind($("#manage-user-progress"), that.userProfilePanel);

                });

            //event for starting edit user process
            $(document).on("click",
                ".fullNameLink",
                (e) => {
                    let row = $(e.target).closest("tr");
                    let rowItem = that.userGrid.getRowDataItem(row);
                    that.manageUser.fetchUserDetails(rowItem["text"], (user: Object) => {
                        that.resetUserRolePanel();
                        that.selectedUserEmail = user["email"];
                        that.userProfilePanel.set("user", {
                            userId: user["userId"], userType: user["userTypeId"], userTypeCode: user["userTypeCode"], fullName: user["fullName"],
                            displayName: user["displayName"], email: user["email"], userStatus: user["userStatusId"],
                            module: user["defaultModuleId"], accountActive: user["accountActive"]
                        });
                        let userCampRoles = user["userRoles"];
                        if (!(userCampRoles.length > 0)) {
                            that.userRoles = { "1": new UserRole() };

                        } else {
                            that.roleParams.params = { userTypeId: user["userTypeId"] };
                            that.userRoles = { "1": new UserRole({ campusGroupId: userCampRoles[0].campusGroupId, roleId: userCampRoles[0].roleId }) }
                            let i = 1;

                            for (i = 1; i < userCampRoles.length; i++) {
                                var val = userCampRoles[i];
                                that.userRoles[that.roleCounter + 1] =
                                    new UserRole({ campusGroupId: val["campusGroupId"], roleId: val["roleId"] });
                                that.appendNewRoleRow("#userRolesSection", val["campusGroupId"], val["roleId"]);

                            }
                        }

                        that.userProfilePanel.set("hasStarted", false);
                        that.collapseSelectTaskPanel();
                        that.userProfilePanel.set("hasStarted", true);
                        that.userProfilePanel.set("canResetPassword", true);
                        that.userProfilePanel.set("user.campusRoles", that.userRoles);
                        $("#ddlUserType").trigger("change");

                        kendo.unbind($("#manage-user-progress"));
                        kendo.bind($("#manage-user-progress"), that.userProfilePanel);

                        that.canExpand = true;
                        that.expandUserProfilePanel();
                        that.expandUserRolePanel();

                    });

                });

            //event for printing grid
            $('#printUsersBtn').click(() => {
                this.userGrid.printGrid("Advantage User Listing", true);
            });

            $('#liManageUserSelectTask').keypress(function (e) {
                var key = e.which;
                if (key == 13)  // the enter key code
                {
                    if ($("#txtUserSearch").is(":focus")) {
                        that.userProfilePanel.get("viewUsers()");
                    }
                }
            });
        }

        private updateUser(user: object) {
            let that = this;
            that.manageUser.updateUser(user, function (response: Object) {
                if (response["hasPassed"]) {
                    MasterPage.SHOW_INFO_WINDOW_PROMISE(
                        "The user has been updated.")
                        .done(() => {
                            that.userProfilePanel.get("cancelStepThree()");
                        });
                }
                else {
                    MasterPage.SHOW_ERROR_WINDOW(
                        "An error occured when trying to update the user." +
                        "\n" +
                        response["resultStatus"]);
                }

            });
        }
        private showUserTypeHelp() {
            let helpText = "";
            let userType = $("#userTypeHelp");
            helpText = "Advantage Users will have access to the Advantage system itself.\nVendors will have access to communicate with the Advantage API.";
            MasterPage.showDialogBox(helpText, userType);
        }

        private fetchFromObject(obj, prop) {

            if (typeof obj === 'undefined') {
                return false;
            }

            var index = prop.indexOf('.');
            if (index > -1) {
                return this.fetchFromObject(obj[prop.substring(0, index)], prop.substr(index + 1));
            }

            return obj[prop];
        }
        private expandSelectTaskPanel() {
            this.manageUserPanel.expand($("#liManageUserSelectTask"), true);
        }
        private collapseSelectTaskPanel() {
            this.manageUserPanel.collapse($("#liManageUserSelectTask"), true);
        }
        private collapseUserProfilePanel() {
            this.manageUserPanel.collapse($("#liManageUserUserProfile"), true);

        }
        private collapseUserRolePanel() {
            this.manageUserPanel.collapse($("#liManageUserUserRoles"), true);

        }
        private resetUserRolePanel() {
            //reset user role panel
            this.roleCounter = 1;
            this.campusDropDownList.reset();;
            this.rolesDropDownList.reset();;
            this.extraRolesList = [];
            $(".deleteRoleBtn").trigger("click");
        }
        private resetUserProfilePanel() {
            this.userTypeDropDownList.reset();;
            this.moduleDropDownList.reset();;
            this.statusDropDownList.reset();;
            //empty all text fields
            $("#liManageUserUserProfile").find("input:text").val("");
        }
        private expandUserProfilePanel() {
            this.userProfilePanelOpen = true;
            this.manageUserPanel.expand($("#liManageUserUserProfile"), true);
        }
        private expandUserRolePanel() {
            this.userProfilePanelOpen = true;
            this.manageUserPanel.expand($("#liManageUserUserRoles"), true);
        }
        private appendNewRoleRow(selector, campusGroup: string = "", campusRole: string = "") {
            let cnt = ++this.roleCounter;
            $(selector).append('<div class="formRow" data-id="' + cnt + '">' +
                '<div style="width: 30%; display: inline; float: left" class= "inLineBlock ddlDynamicCampus">' +
                'Campus Group' + '<div style = "padding: 5px;">' +
                '<input id="ddCampus' + cnt + '" class= "fieldlabel1 inputCustom" data-bind="value:user.campusRoles[' + cnt + '].campusGroupId" required/>' +
                '</div>' +
                '</div>' +
                '<div style = "width: 30%; display: inline; float: left; margin-left:5px;" class="ddlDynamicRole">' +
                'Role<div style ="padding: 5px;">' +
                '<input id="ddRoles' + cnt + '" class= "fieldlabel1 inputCustom"  data-bind="value:user.campusRoles[' + cnt + '].roleId" required/>' +
                '</div>' +
                '</div>' +
                '<span class="btn deleteRoleBtn" style="width:5%;"><img style="margin-top:25px;margin-left:-75px;" src = "../images/icon/Trash.gif" alt ="Delete Role" ></span>' +
                '<small>To delete a Role, select X. </small>' +
                '<div class= "clearfix"></div>' +
                '</div>');

            new DropDownList("ddCampus" + cnt, this.campusGroup, undefined, undefined, () => { });
            this.extraRolesList.push(new DropDownList("ddRoles" + cnt, this.roles, this.roleParams, undefined, () => { }));
        }
        resetValidator(selector) {
            $(selector).find("input").removeClass("valueRequired");
            $(selector).find(".k-input").removeClass("valueRequired");
            $(selector).find(".advantageTooltipClass").remove();
        }
        setUserProfileListeners(attach: boolean = true) {
            let fieldsToCheck = $("#liManageUserUserProfile").find(":input").filter(":enabled").not("#ddlUserType").not("button");
            let openUserRolesPanel = true;
            let dropDowns = fieldsToCheck.filter('[data-role="dropdownlist"]');
            let regularTextInput = fieldsToCheck.not(dropDowns);
            var that = this;
            if (attach) {
                dropDowns.on('change',
                    function () {
                        that.checkIfUserRolesPanelShouldOpen();
                    });
                regularTextInput.on('blur',
                    function () {
                        that.checkIfUserRolesPanelShouldOpen();
                    });
            } else {
                dropDowns.off('change');
                regularTextInput.off('blur');
            }
        }
        checkIfUserRolesPanelShouldOpen() {
            let fieldsToCheck = $("#liManageUserUserProfile").find(":input").filter(":enabled").not("#ddlUserType").not("button");
            let openUserRolesPanel = true;
            let dropDowns = fieldsToCheck.filter('[data-role="dropdownlist"]');
            let regularTextInput = fieldsToCheck.not(dropDowns);
            let j = 0;
            for (j = 0; j < dropDowns.length; j++) {

                if ($(dropDowns[j]).data("kendoDropDownList").value() === "") {
                    openUserRolesPanel = false;
                    break;
                }
            }

            if (!openUserRolesPanel) {
                return;
            }

            for (j = 0; j < regularTextInput.length; j++) {

                if ($(regularTextInput[j]).val() === "") {
                    openUserRolesPanel = false;
                    break;
                }
            }

            if (openUserRolesPanel) {
                this.expandUserRolePanel();
            }
        }

    }
}