﻿/// <reference path="../../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../../api/reports/dataexport9010.ts" />

declare var $find: any;
declare var Page_ClientValidate: any;
declare var Page_IsValid: any;
declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;

module Api.ViewModels {

    export class DataExport9010 {

        apiService: Api.Data9010;
        $radlistBoxCampuses: any;
        $radlistBoxPrograms: any;
        $radListBoxGroups: any;
        $studentSearch: any;
        $fiscalYearStart: any;
        $fiscalYearEnd: any;
        $activitiesConductedAmount: any;
        $generateExportBtn: any;
        $campusesDropDown: any;
        $hiddenStuEnrollmentId: any;

        constructor() {
            this.apiService = new Api.Data9010();
        }

        init() {
            var that = this;
            this.$radlistBoxCampuses = $("[id*='RadListBoxCampuses']").first();
            this.$radlistBoxPrograms = $("[id*='RadListBoxPrograms']").first();
            this.$radListBoxGroups = $("[id*='RadListBoxGroups']").first();
            this.$studentSearch = $("input[id*='DDLStudentSearch_ClientState']");
            this.$fiscalYearStart = $("input[id*='fiscalYearStart']");
            this.$fiscalYearEnd = $("input[id*='fiscalYearEnd']");
            this.$activitiesConductedAmount = $("input[id*='activitiesConductedAmount']");
            this.$generateExportBtn = $("[id*='generateExportBtn']");
            this.$campusesDropDown = $("[id*='ddlCampuses']");
            this.$hiddenStuEnrollmentId = $("input[id*='hiddenStuEnrollmentId']");

            this.$generateExportBtn.on('click', function (e) {
                that.generateExtract();
            });

            this.$generateExportBtn.parent().removeClass('rfdSkinnedButton');
            this.$generateExportBtn.removeClass('rfdDecorated');
            this.$generateExportBtn.kendoButton();
        }

        validate_RadListBoxPrograms(sender, args) {
            args.IsValid = this.getSelectedPrograms().length !== 0;
            return;
        }

        validate_RadListBoxGroups(sender, args) {
            args.IsValid = this.getSelectedGroups().length !== 0;
            return;
        }

        validate_RadListBoxCampuses(sender, args) {
            args.IsValid = this.getSelectedCampuses().length !== 0;
            return;
        }

        formValid() {
            return true;
        }

        generateExtract() {
            var that = this;
            if (Page_ClientValidate() && this.formValid()) {

                var campuses = this.getSelectedCampuses();
                var programs = this.getSelectedPrograms();
                var groups = this.getSelectedGroups();

                //if any campuses checked get values
                if (campuses)
                    campuses = campuses.map(function (e) { return e.value });

                //if any programs checked get values
                if (programs)
                    programs = programs.map(function (e) { return e.value });

                //if any groups checked get values
                if (groups)
                    groups = groups.map(function (e) { return e.value });

                $find($("[id*='RadListBoxPrograms']").first().attr('id'))._allChecked



                var apiData = {
                    campusIds: campuses ? campuses : [],
                    fiscalYearStart: this.$fiscalYearStart.val(),
                    fiscalYearEnd: this.$fiscalYearEnd.val(),
                    activitiesConductedAmount: this.$activitiesConductedAmount.val(),
                    programIds: this.allProgramsChecked() ? ["All"] : programs,
                    groupIds: this.allGroupsChecked() ? ["All"] : groups,
                    stuEnrollmentId: this.$hiddenStuEnrollmentId.val(),
                };

                this.apiService.generateDataExtract(apiData, function (response: Object) {

                });
            }
        }

        getSelectedPrograms() {
            return $find(this.$radlistBoxPrograms.attr('id'))._itemData.filter(function (e) { return e.checked === true });
        }

        getSelectedGroups() {
            return $find(this.$radListBoxGroups.attr('id'))._itemData.filter(function (e) { return e.checked === true });
        }

        getSelectedCampuses() {
            return $find(this.$radlistBoxCampuses.attr('id'))._itemData.filter(function (e) { return e.checked === true });
        }

        allGroupsChecked() {
            return $find(this.$radListBoxGroups.attr('id'))._allChecked;
        }

        allProgramsChecked() {
            return $find(this.$radlistBoxPrograms.attr('id'))._allChecked;
        }

        OnClientItemCheckedHandler(sender, eventArgs) {
            var item = eventArgs.get_item();
            item.set_selected(item.get_checked());
        }
    }
}
