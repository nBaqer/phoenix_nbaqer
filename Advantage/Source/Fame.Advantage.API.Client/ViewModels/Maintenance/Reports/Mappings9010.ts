﻿module Api.ViewModels.Maintenance {
	export class Mappings9010 {
        mappings9010Vm: Mappings9010Vm;

        constructor() {
            this.mappings9010Vm = new Mappings9010Vm();
        }

        public initialize() {
            this.mappings9010Vm.initialize();
        }
	}
}