﻿/// <reference path="../../../../Fame.Advantage.Client.AD/Common/DropDownOutputModel.ts" />
/// <reference path="../../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../../../Fame.Advantage.API.Client/Components/Common/DropDownList.ts" />
/// <reference path="../../../../Fame.Advantage.API.Client/Components/Common/Grid.ts" />
/// <reference path="../../../../Fame.Advantage.API.Client/API/SystemCatalog/Mappings/ProgramStateBoardCourseMapping.ts" />

module Api.ViewModels.Maintenance {

    import DropDownList = Api.Components.DropDownList;
    import KendoGrid = Api.Components.Grid;
    import ISettings = API.Models.ISettings;
    import IFilterParameters = API.Models.IFilterParameters;

    export class Mappings9010Vm {
        btnSave: JQuery;
        btnPrint: JQuery;
        campusDropdown: DropDownList;
        awardMapping9010Grid: KendoGrid;
        awardMappingsDS: FundSource9010MappingApiCalls
        emptyGuid: string = "00000000-0000-0000-0000-000000000000";
        awardTypeMappings: Array<any>;

        constructor() {
            this.btnSave = $("#btnSave");
            this.btnPrint = $("#btnPrint");
            this.awardTypeMappings = [];
        }

        public initialize() {
            var that = this;

            //button styling
            this.btnSave.parent().removeClass('rfdSkinnedButton');
            this.btnSave.removeClass('rfdDecorated');
            this.btnSave.kendoButton();

            var campus = new Campus();
            this.awardMappingsDS = new FundSource9010MappingApiCalls();

            let gridSettings: ISettings = {} as ISettings;
            gridSettings.settings = {
                pageable: false,
                autoBind: false,
                serverPaging: false,
                sortable: true,
                dataBound: function (e) {
                    that.initGridDropDowns();
                },
                filterable: false,
                height: 400,
                columns: [
                    {
                        field: "isActive",
                        title: "Status",
                        width: "100px"
                    },
                    {
                        field: "entityType",
                        title: "Entity Type",
                        width: "125px"
                    },
                    {
                        field: "entityCode",
                        title: "Code",
                        width: "125px"
                    },
                    {
                        field: "entityDescription",
                        title: "Fund Source"
                    },
                    {
                        title: "90/10 Mapping"
                    }],
                rowTemplate: '<tr data-uid="#= uid #" id="#= entityId #"><td><strong>#: isActive ? "Active" : "Inactive" #</strong></td><td><strong>#: entityType  #</strong></td><td><strong>#: entityCode  #</strong></td><td><strong>#: entityDescription #</strong></td><td><div class="formRow">' +
                '<div style="width: 30%; display: inline; float: left" class="inLineBlock">' +
                '<input class="fieldlabel1 inputCustom ddlAwardTypes9010" value="#= mappingType9010Id #" required/>' +
                '</div>' +
                '</td></tr>',
                altRowTemplate: '<tr data-uid="#= uid #" id="#= entityId #" class="k-alt"><td><strong>#: isActive ? "Active" : "Inactive" #</strong></td><td><strong>#: entityType  #</strong></td><td><strong>#: entityCode  #</strong></td><td><strong>#: entityDescription #</strong></td><td><div class="formRow">' +
                '<div style="width: 30%; display: inline; float: left" class="inLineBlock">' +
                '<input class="fieldlabel1 inputCustom ddlAwardTypes9010" value="#= mappingType9010Id #" required/>' +
                '</div>' +
                '</td></tr>',
                noRecords: {
                    template: "Select a campus to populate the 90/10 mappings."
                }
            };

            this.btnSave.on("click", function (e) {
                that.btnOnSave_Click(e, this);
            });

            this.btnPrint.on("click", function (e) {
                that.btnPrint_Click(e, this);
            });

            this.campusDropdown = new DropDownList("ddCampus", campus, undefined, { settings: { dataValueField: "value" } } as ISettings, ((value) => {
                this.initializeGrid();
            }));

            //Initialize mapping grid
            this.awardMapping9010Grid = new KendoGrid("awardMapping9010Grid", this.awardMappingsDS, undefined, undefined, gridSettings);
            this.awardMapping9010Grid.fetch();
        }


        public initializeGrid() {
            var that = this;

            if (!this.campusDropdown.hasValue()) {
                return;
            }

            this.awardMapping9010Grid.reload(undefined, { params: { campusId: this.campusDropdown.getValue() } } as IFilterParameters);

            this.awardMapping9010Grid.fetch().done((e) => {
                that.awardTypeMappings = that.awardMapping9010Grid.getData().map((e: any) => { return e.toJSON(); });
                this.initGridDropDowns();
            });
        }

        initGridDropDowns() {
            var that = this;
            let mappingTypes9010 = new AwardTypes9010();

            //init each dropdown list for each row
            $("#awardMapping9010Grid").find("input.ddlAwardTypes9010:text").each((i, val) => {
                var $this = $(val);
                let dd = new DropDownList($this, mappingTypes9010, undefined, {
                    settings: {
                        dataValueField: "id", dataTextField: "description", autoBind: true,
                    }
                } as ISettings,
                    () => {
                        let entityId = $this.closest("tr").attr("id");

                        for (var i = 0; i < that.awardTypeMappings.length; i++) {
                            var mapping = that.awardTypeMappings.filter((item) => { return item.entityId === entityId })[0];
                            mapping.mappingType9010Description = dd.getText();
                            mapping.mappingType9010Id = dd.getValue();
                        }
                    });


                dd.fetch().done(() => {
                    let entityId = $this.closest("tr").attr("id");
                    var mapping = that.awardTypeMappings.filter((item) => { return item.entityId === entityId })[0];
                    if (mapping.isTitleIV) {
                        dd.setReadOnly(true);
                        dd.setValue(dd.getData().map((e: any) => { return e.toJSON(); }).filter((item) => { return item.description === "Title IV"; })[0].id);
                    }
                });
            });
        }

        //persist backing grid model to server
        public btnOnSave_Click(e, item) {

            if (!$('#ddCampus').val()) {
                MasterPage.SHOW_INFO_WINDOW("Please select a campus.");
                return;
            }

            this.awardMappingsDS.UpdateAwardMappings9010(this.awardTypeMappings, XMASTER_GET_CURRENT_CAMPUS_ID, (response: any) => {
                if (response.resultStatus === 2)
                    MasterPage.SHOW_ERROR_WINDOW(response.resultStatusMessage);
                else
                    MasterPage.SHOW_INFO_WINDOW("The 90/10 mappings have been successfully saved.");
            });
        }

        //download campus mappings file
        public btnPrint_Click(e, item) {

            if (!$('#ddCampus').val()) {
                MasterPage.SHOW_INFO_WINDOW("Please select a campus.");
                return;
            }

            this.awardMappingsDS.printCampus9010Mappings($('#ddCampus').val(), function (response: Object) {

            });
        }
    }
}