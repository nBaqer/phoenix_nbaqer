﻿/// <reference path="../../../../Fame.Advantage.Client.AD/Common/DropDownOutputModel.ts" />
/// <reference path="../../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../../../Fame.Advantage.API.Client/Components/Common/DropDownList.ts" />
/// <reference path="../../../../Fame.Advantage.API.Client/Components/Common/Grid.ts" />
/// <reference path="../../../../Fame.Advantage.API.Client/API/SystemCatalog/Mappings/ProgramStateBoardCourseMapping.ts" />
/// <reference path="StateBoardAgencyMaster.ts" />


module Api.ViewModels.Maintenance {
    import States = Api.SystemCatalog.Models.IStates
    import DropDownList = Api.Components.DropDownList;
    import KendoGrid = Api.Components.Grid;
    import ISettings = API.Models.ISettings;
    import IFilterParameters = API.Models.IFilterParameters;
    import Program = Api.Program;
    import StateBoardCourse = Api.StateBoardCourse;
    import ProgramStateBoardCourseMapping = Api.SystemCatalog.Mappings.ProgramStateBoardCourseMapping;

    export class IndianaStateBoardSettingsVm {
        X_PLEASE_FILL_REQUIRED_FIELDS = "Please enter the required values.";
        indianaStateboardSection: JQuery;
        ownerName: JQuery;
        schoolName: JQuery;
        address1: JQuery;
        address2: JQuery;
        city: JQuery;
        zipCode: JQuery;
        ownerLicenseNumber: JQuery;
        officerName1: JQuery;
        officerName2: JQuery;
        officerName3: JQuery;
        laAddress1: JQuery;
        laAddress2: JQuery;
        laCity: JQuery;
        laZipCode: JQuery;
        stateBoardAgenciesDropdown: DropDownList;
        statesDropdown: DropDownList;
        countriesDropdown: DropDownList;
        laStatesDropdown: DropDownList;
        laCountriesDropdown: DropDownList;
        settingDetailsPanel: kendo.ui.PanelBar;
        programCourseMappingGrid: KendoGrid;
        gridParams: IFilterParameters;
        programStateBoardCourseMappings: Array<ProgramStateBoardCourseMapping>;
        stateBoardSettingId: string;
        sbsValidator: kendo.ui.Validator;
        emptyGuid: string = "00000000-0000-0000-0000-000000000000";

        countries: Country;
        agencies: StateBoardAgency;
        masterPage: StateBoardAgencyMaster;

        constructor(masterPage: StateBoardAgencyMaster) {
            this.masterPage = masterPage;
            this.indianaStateboardSection = $('#indianaStateboardSection');
            this.ownerName = this.indianaStateboardSection.find("#txtOwnerName");
            this.schoolName = this.indianaStateboardSection.find("#txtSchoolName");
            this.address1 = this.indianaStateboardSection.find("#txtSchoolAddress1");
            this.address2 = this.indianaStateboardSection.find("#txtSchoolAddress2");
            this.zipCode = this.indianaStateboardSection.find("#txtZipCode");
            this.city = this.indianaStateboardSection.find("#txtCity");
            this.ownerLicenseNumber = this.indianaStateboardSection.find("#txtOwnerLicenseNumber");
            this.officerName1 = this.indianaStateboardSection.find("#txtOfficerName1");
            this.officerName2 = this.indianaStateboardSection.find("#txtOfficerName2");
            this.officerName3 = this.indianaStateboardSection.find("#txtOfficerName3");
            this.laAddress1 = this.indianaStateboardSection.find("#licensingAgencyAddress1");
            this.laAddress2 = this.indianaStateboardSection.find("#licensingAgencyAddress2");
            this.laCity = this.indianaStateboardSection.find("#licensingAgencyCity");
            this.laZipCode = this.indianaStateboardSection.find("#licensingAgencyZipCode");
            this.programStateBoardCourseMappings = [];
            this.stateBoardSettingId = this.emptyGuid;
        }

        public initialize() {
            var that = this;
            this.countries = new Country();
            this.agencies = new StateBoardAgency();
            this.indianaStateboardSection.show();

            this.masterPage.btnNew.attr('disabled', null);
            this.masterPage.btnSave.attr('disabled', null);
            this.masterPage.btnDelete.attr('disabled', null);

            let gridSettings: ISettings = {} as ISettings;
            this.gridParams = { params: { stateBoardId: "" } } as IFilterParameters;

            that.sbsValidator = MasterPage.ADVANTAGE_VALIDATOR("indianaStateboardSection");
            let programs = new Program();

            gridSettings.settings = {
                pageable: false,
                autoBind: false,
                columns: [{
                    field: "text",
                    title: "Program"
                },
                {
                    title: "State Course Code"
                }],
                rowTemplate: '<tr data-uid="#= uid #" id="#= value #"><td><strong>#: text #</strong></td><td><div class="formRow">' +
                '<div style="width: 30%; display: inline; float: left" class="inLineBlock">' +
                '<input class="fieldlabel1 inputCustom ddlStateBoardCourse" required/>' +
                '</div>' +
                '</td></tr>',
                altRowTemplate: '<tr data-uid="#= uid #" id="#= value #" class="k-alt"><td><strong>#: text #</strong></td><td><div class="formRow">' +
                '<div style="width: 30%; display: inline; float: left" class="inLineBlock">' +
                '<input class="fieldlabel1 inputCustom ddlStateBoardCourse" required/>' +
                '</div>' +
                '</td></tr>',
                noRecords: {
                    template: "Select a campus to begin the program/state board course mapping."
                }


            };

            that.statesDropdown = new DropDownList("ddState", that.masterPage.stateBoardStates, undefined, { settings: { dataValueField: "value" } } as ISettings, () => { }, "GetStatesByCountryId");

            that.countriesDropdown = new DropDownList("ddCountry", that.countries, undefined, { settings: { dataValueField: "value" } } as ISettings, ((value) => {
                let selectedCountry = this.countriesDropdown.getValue();
                let statesFilterParam: IFilterParameters = {
                    params: {
                        countryId: selectedCountry
                    }
                } as IFilterParameters;
                that.statesDropdown = new DropDownList("ddState", that.masterPage.stateBoardStates, statesFilterParam, { settings: { dataValueField: "value" } } as ISettings, () => { }, "GetStatesByCountryId");
            }) as any);

            let statesFilterParam: IFilterParameters = {
                params: {
                    stateId: that.masterPage.stateBoardStatesAndAgenciesDropdown.getValue()
                }
            } as IFilterParameters;

            //-----------licensing agency dropdowns------------------- 
            that.stateBoardAgenciesDropdown = new DropDownList("ddlLicensingAgency", that.agencies, statesFilterParam, { settings: { dataValueField: "value" } } as ISettings, (value) => {

                let selectedAgencyId = this.stateBoardAgenciesDropdown.getSelected();
                if (selectedAgencyId === undefined || selectedAgencyId === "") {
                    this.laAddress1.val("");
                    this.laAddress2.val("");
                    this.laCountriesDropdown.setValue("");
                    this.laCity.val("");
                    this.laStatesDropdown.setValue("");
                    this.laZipCode.val("");
                }
                else {
                    that.agencies.fetchAgencyDetails(selectedAgencyId, (licensingAgencyInformation: Object) => {

                        this.laAddress1.val(licensingAgencyInformation["address1"]);
                        this.laAddress2.val(licensingAgencyInformation["address2"]);
                        this.laCountriesDropdown.setValue(licensingAgencyInformation["countryId"]);
                        this.laCity.val(licensingAgencyInformation["city"]);
                        this.laStatesDropdown.setValue(licensingAgencyInformation["stateId"]);
                        this.laZipCode.val(licensingAgencyInformation["zipCode"]);
                    });
                }


            });
            that.laCountriesDropdown = new DropDownList("licensingAgencyCountry", that.countries, undefined, { settings: { dataValueField: "value" } } as ISettings, () => {

                let selectedCountry = that.laCountriesDropdown.getValue();
                let statesFilterParam: IFilterParameters = {
                    params: {
                        countryId: selectedCountry
                    }
                } as IFilterParameters;
                that.laStatesDropdown.reload(statesFilterParam);

            });
            that.laStatesDropdown = new DropDownList("licensingAgencyState", that.masterPage.stateBoardStates, undefined, { settings: { dataValueField: "value" } } as ISettings, () => { });
            that.laZipCode.focus(() => {
                that.sbsValidator.validateInput(that.laZipCode);
            });
            // -------------------------------------------------------- 

            $("#panelBarDetailsStateBoard").kendoPanelBar({
                expand: function (e) {

                },
                expandMode: "multiple"
            });

            that.settingDetailsPanel = $("#panelBarDetailsStateBoard").data("kendoPanelBar");

            that.masterPage.btnDelete.on("click",
                function (e) {
                    MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure you want to delete this setting?").then((confirmed) => {

                        if (confirmed) {
                            var stateId = that.masterPage.stateBoardStatesAndAgenciesDropdown.getValue();
                            var campusId = that.masterPage.campusDropdown.getValue();
                            var reportId = that.masterPage.stateBoardReportsDropdown.getValue();
                            that.masterPage.reports.deleteStatateBoardReportSetting(campusId,
                                reportId,
                                stateId,
                                () => {
                                    that.clearFields();
                                    that.masterPage.resetPage();
                                    that.indianaStateboardSection.hide();

                                });
                        }

                    });

                });


            that.masterPage.btnSave.on("click",
                function (e) {

                    if (that.sbsValidator.validate()) {

                        let stateId = that.masterPage.stateBoardStatesAndAgenciesDropdown.getValue();
                        let campusId = that.masterPage.campusDropdown.getValue();
                        let reportId = that.masterPage.stateBoardReportsDropdown.getValue();

                        //-------Campus Details--------
                        let ownerName = that.ownerName.val();
                        let schoolName = that.schoolName.val();
                        let schoolAddress1 = that.address1.val();
                        let schoolAddress2 = that.address2.val();
                        let city = that.city.val();
                        let country = that.countriesDropdown.getValue();
                        let state = that.statesDropdown.getValue();
                        let zipCode = that.zipCode.val();
                        let ownerLicenseNumber = that.ownerLicenseNumber.val();
                        let officerName1 = that.officerName1.val();
                        let officerName2 = that.officerName2.val();
                        let officerName3 = that.officerName3.val();
                        //-----------------------------

                        //-------State Board Licensing Agency Details--------
                        let licensingAgency = that.stateBoardAgenciesDropdown.getValue();
                        let laAddress1 = that.laAddress1.val();
                        let laAddress2 = that.laAddress2.val();
                        let laCountry = that.laCountriesDropdown.getValue();
                        let laCity = that.laCity.val();
                        let laState = that.laStatesDropdown.getValue();
                        let laZipCode = that.laZipCode.val();
                        //---------------------------------------------------

                        let stateBoardReportSetting = {}
                        stateBoardReportSetting["schoolStateBoardReportId"] = that.stateBoardSettingId;
                        //-------Campus Details--------
                        stateBoardReportSetting["CampusId"] = campusId;
                        stateBoardReportSetting["ReportId"] = reportId;
                        stateBoardReportSetting["StateId"] = stateId;
                        stateBoardReportSetting["OwnerName"] = ownerName;
                        stateBoardReportSetting["SchoolName"] = schoolName;
                        stateBoardReportSetting["SchoolAddress1"] = schoolAddress1;
                        stateBoardReportSetting["SchoolAddress2"] = schoolAddress2;
                        stateBoardReportSetting["CampusCity"] = city;
                        stateBoardReportSetting["CampusCountryId"] = country;
                        stateBoardReportSetting["CampusStateId"] = state;
                        stateBoardReportSetting["CampusZipCode"] = zipCode;
                        stateBoardReportSetting["OwnerLicenseNumber"] = ownerLicenseNumber;
                        stateBoardReportSetting["OfficerName1"] = officerName1;
                        stateBoardReportSetting["OfficerName2"] = officerName2;
                        stateBoardReportSetting["OfficerName3"] = officerName3;
                        //-----------------------------

                        //-------State Board Licensing Agency Details--------
                        stateBoardReportSetting["LicensingAgencyId"] = licensingAgency;
                        stateBoardReportSetting["LicensingAddress1"] = laAddress1;
                        stateBoardReportSetting["LicensingAddress2"] = laAddress2;
                        stateBoardReportSetting["LicensingCountryId"] = laCountry;
                        stateBoardReportSetting["LicensingCity"] = laCity;
                        stateBoardReportSetting["LicensingStateId"] = laState;
                        stateBoardReportSetting["LicensingZipCode"] = laZipCode;
                        //---------------------------------------------------

                        //-------Campus Program Mapping to Licensing Agency Course Code--------
                        stateBoardReportSetting["ProgramStateBoardCourseMappings"] = that.programStateBoardCourseMappings;
                        //---------------------------------------------------------------------

                        that.masterPage.reports.saveOrUpdateStateBoardSetting(stateBoardReportSetting,
                            (response: Object) => {
                                if (response["resultStatus"] === 0) {
                                    MasterPage.SHOW_INFO_WINDOW(
                                        "The state board settings have been saved.");
                                    that.stateBoardSettingId = response["result"]["schoolStateBoardReportId"];

                                } else {
                                    MasterPage.SHOW_ERROR_WINDOW(
                                        response["resultStatusMessage"]);
                                }
                            });
                    }
                    else {
                        MasterPage.SHOW_WARNING_WINDOW(that.X_PLEASE_FILL_REQUIRED_FIELDS);
                    }
                });

            let selectedStateBoardState = that.masterPage.stateBoardStatesAndAgenciesDropdown.getValue();
            let stateBoardStatesFilterParam: IFilterParameters = {
                params: {
                    stateId: selectedStateBoardState
                }
            } as IFilterParameters;

            //Initialize program/course mapping grid
            that.programCourseMappingGrid = new KendoGrid("programCourseMappingGrid", programs, undefined, undefined, gridSettings);
            that.programCourseMappingGrid.fetch();
            
        }


        public clearFields() {
            this.stateBoardSettingId = this.emptyGuid;
            this.masterPage.stateBoardStatesAndAgenciesDropdown.reset();
            this.masterPage.campusDropdown.reset();
            this.ownerName.val("");
            this.schoolName.val("");
            this.address1.val("");
            this.address2.val("");
            this.city.val("");
            this.countriesDropdown.reset();
            this.ownerLicenseNumber.val("");
            this.officerName1.val("");
            this.officerName2.val("");
            this.officerName3.val("");
            this.stateBoardAgenciesDropdown.reset();
            this.laCountriesDropdown.reset();
            this.laStatesDropdown.reset();
            this.laAddress1.val("");
            this.laAddress2.val("");
            this.laCity.val("");
            this.laZipCode.val("");
            this.programCourseMappingGrid.clear();
            this.programCourseMappingGrid.fetch();
            this.programStateBoardCourseMappings = [];
            MasterPage.advantageValitorReset("indianaStateboardSection");
            
        }

        public loadStateBoardSetting() {
            var states = new States();
            var countries = new Country();
            var campus = new Campus();
            var agencies = new StateBoardAgency();

            var that = this;

            let selectedCampus = that.masterPage.campusDropdown.getValue();
            let selectedStateBoardReport = that.masterPage.stateBoardReportsDropdown.getValue();
            let selectedStateBoardState = that.masterPage.stateBoardStatesAndAgenciesDropdown.getValue();

            if (selectedCampus !== undefined &&
                selectedStateBoardReport !== undefined &&
                selectedStateBoardState !== undefined) {
                this.masterPage.reports.fetchStateBoardReport(selectedCampus, selectedStateBoardReport, selectedStateBoardState, (campusInformation: Object) => {
                    this.stateBoardSettingId = campusInformation["schoolStateBoardReportId"];
                    //owner set
                    this.ownerName.val(campusInformation["ownerName"]);
                    this.ownerLicenseNumber.val(campusInformation["ownerLicenseNumber"]);
                    this.officerName1.val(campusInformation["officerName1"]);
                    this.officerName2.val(campusInformation["officerName2"]);
                    this.officerName3.val(campusInformation["officerName3"]);

                    // school set
                    this.schoolName.val(campusInformation["schoolName"]);
                    this.address1.val(campusInformation["schoolAddress1"]);
                    this.address2.val(campusInformation["schoolAddress2"]);
                    this.city.val(campusInformation["campusCity"]);
                    this.countriesDropdown.setValue(campusInformation["campusCountryId"]);
                    this.zipCode.val(campusInformation["campusZipCode"]);

                    let selectedCountry = this.countriesDropdown.getValue();
                    let statesFilterParam: IFilterParameters = {
                        params: {
                            countryId: selectedCountry
                        }
                    } as IFilterParameters;

                    this.statesDropdown = new DropDownList("ddState", states, statesFilterParam, { settings: { dataValueField: "value" } } as ISettings, (statesValues) => { }, "GetStatesByCountryId", campusInformation["campusStateId"]);

                    //licensing set

                    if (campusInformation["licensingAgencyId"] === undefined || campusInformation["licensingAgencyId"] === "") {
                        this.laAddress1.val("");
                        this.laAddress2.val("");
                        this.laCountriesDropdown.setValue("");
                        this.laCity.val("");
                        this.laStatesDropdown.setValue("");
                        this.laZipCode.val("");
                    } else {
                        this.stateBoardAgenciesDropdown.setValue(campusInformation["licensingAgencyId"]);
                        this.laAddress1.val(campusInformation["licensingAddress1"]);
                        this.laAddress2.val(campusInformation["licensingAddress2"]);
                        this.laCountriesDropdown.setValue(campusInformation["licensingCountryId"]);
                        this.laCity.val(campusInformation["licensingCity"]);
                        this.laStatesDropdown.setValue(campusInformation["licensingStateId"]);
                        this.laZipCode.val(campusInformation["licensingZipCode"]);
                    }


                    //mappings set
                    var mappings = campusInformation["programStateBoardCourseMappings"];
                    this.programStateBoardCourseMappings = [];
                    $.each(mappings,
                        (idx, val) => {
                            this.programStateBoardCourseMappings.push(
                                new ProgramStateBoardCourseMapping({
                                    programId: val.programId,
                                    stateBoardCourseId: val.stateBoardCourseId
                                }));
                        });

                });
            }

        }

        public initializeGrid() {
            var that = this;
            this.loadStateBoardSetting();

            let stateBoardCourseCodes = new StateBoardCourse();
            if (that.masterPage.campusDropdown.hasValue()) {
                this.programCourseMappingGrid.reload(undefined,
                    { params: { campusId: that.masterPage.campusDropdown.getValue() } } as IFilterParameters);
                this.programStateBoardCourseMappings = [];
                this.programCourseMappingGrid.fetch().done(() => {
                    $("#programCourseMappingGrid").find("input.ddlStateBoardCourse:text").each((i, val) => {
                        var $this = $(val);
                        let dd = new DropDownList($this, stateBoardCourseCodes,
                            { params: { stateId: that.masterPage.stateBoardStatesAndAgenciesDropdown.getValue() } } as IFilterParameters, {
                                settings: {
                                    dataValueField: "value", autoBind: false,
                                }
                            } as ISettings,
                            () => {
                                if (dd.hasValue()) {
                                    let value = dd.getValue();
                                    let programId = $this.closest("tr").attr("id");

                                    let result =
                                        this.programStateBoardCourseMappings.filter((e) => {
                                            return e.programId === programId;
                                        });
                                    if (result.length === 1) {

                                        let index = this.programStateBoardCourseMappings.indexOf(result[0]);
                                        this.programStateBoardCourseMappings.splice(index,
                                            1,
                                            new ProgramStateBoardCourseMapping({
                                                programId: programId,
                                                stateBoardCourseId: value
                                            }));
                                    } else {
                                        this.programStateBoardCourseMappings.push(
                                            new ProgramStateBoardCourseMapping({
                                                programId: programId,
                                                stateBoardCourseId: value
                                            }));
                                    }
                                }
                            });

                        dd.fetch().done(() => {

                            let programId = $this.closest("tr").attr("id");
                            let result =
                                this.programStateBoardCourseMappings.filter((e) => {
                                    return e.programId === programId;
                                });
                            dd.setValue(result[0].stateBoardCourseId);
                        });
                    });

                });
            }
        }

        //reverse any event listeners or hidden sections for this specific agency or stateboard
        public cleanUp() {
            this.indianaStateboardSection.hide();
            this.masterPage.btnSave.unbind('click');
            this.masterPage.btnDelete.unbind('click');
            
        }
    }
}