﻿module Api.ViewModels.Maintenance {
	export class FileStorage {
        fileStorageVm: FileStorageVm;

        constructor() {
            this.fileStorageVm = new FileStorageVm();
        }

        public initialize() {
            this.fileStorageVm.initialize();
        }
	}
}