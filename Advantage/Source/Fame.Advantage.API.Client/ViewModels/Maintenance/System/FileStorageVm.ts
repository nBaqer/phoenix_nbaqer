﻿/// <reference path="../../../../Fame.Advantage.Client.AD/Common/DropDownOutputModel.ts" />
/// <reference path="../../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../../../Fame.Advantage.API.Client/Components/Common/DropDownList.ts" />
/// <reference path="../../../../Fame.Advantage.API.Client/Components/Common/Grid.ts" />
/// <reference path="../../../../Fame.Advantage.API.Client/API/SystemCatalog/Mappings/ProgramStateBoardCourseMapping.ts" />

module Api.ViewModels.Maintenance {

    import DropDownList = Api.Components.DropDownList;
    import KendoGrid = Api.Components.Grid;
    import ISettings = API.Models.ISettings;
    import IFilterParameters = API.Models.IFilterParameters;
    import FileStorageConfigs = API.Enums.FileStorageConfigs;
    import CampusFileConfig = API.Models.ICampusFileConfig;
    import CustomFileConfig = API.Models.ICustomFileFeatureConfig;
    export class FileStorageVm {
        mainValidator: kendo.ui.Validator;
        hdnModUser: string;
        btnSave: JQuery;
        btnDelete: JQuery;
        campusGroupDropdown: DropDownList;
        fileStorageTypeDropdown: DropDownList;
        pathMain: JQuery;
        pathMainLabel: JQuery;
        cloudKeyMain: JQuery;
        userNameMain: JQuery;
        passwordMain: JQuery;
        featuresSwitch: JQuery;
        campusConfigurationId: string;
        campusFileConfig: CampusFileConfig;
        fileFeaturesGrid: KendoGrid;
        initialGridWidth: number;
        fileFeaturesDS: FileConfigFeatures;
        campusFileConfigDS: CampusFileConfiguration;
        emptyGuid: string = "00000000-0000-0000-0000-000000000000";
        awardTypeMappings: Array<any>;
        fileStorageConfigs: Array<any>;
        fileStorageTypeFeatures: Array<DropDownList>
        perFeatureConfiguration: boolean;
        constructor() {
            this.hdnModUser = $("#hdnModUser").val();
            this.btnSave = $("#btnSave");
            this.btnDelete = $("#btnDelete");
            this.pathMain = $("#inputPathMain");
            this.pathMainLabel = $("#pathMainLabel");
            this.cloudKeyMain = $("#inputCloudKeyMain");
            this.userNameMain = $("#inputUserNameMain");
            this.passwordMain = $("#inputPasswordMain");
            this.featuresSwitch = $("#features-switch");
            this.awardTypeMappings = [];
            this.fileStorageTypeFeatures = [];
            this.perFeatureConfiguration = false;
            this.fileStorageConfigs = [{ text: FileStorageConfigs[FileStorageConfigs.Cloud], value: FileStorageConfigs.Cloud },
            { text: FileStorageConfigs[FileStorageConfigs.File], value: FileStorageConfigs.File },
            { text: FileStorageConfigs[FileStorageConfigs.Network], value: FileStorageConfigs.Network }]
        }

        public initialize() {
            var that = this;
            that.campusConfigurationId = that.emptyGuid;
            let campusGroups = new CampusGroup();
            this.fileFeaturesDS = new FileConfigFeatures();
            this.campusFileConfigDS = new CampusFileConfiguration();
            that.mainValidator = MasterPage.ADVANTAGE_VALIDATOR("mainConfigSection");
            let gridSettings: ISettings = {} as ISettings;
            gridSettings.settings = {
                pageable: false,
                serverPaging: false,
                sortable: false,
                dataBound: function (e) {
                    that.initGridDropDowns();
                    that.toggleColumns(false, ["cloudKey", "username", "password"]);
                },
                filterable: false,
                height: 250,
                columns: [
                    {
                        field: "fileStorageType",
                        title: "Type",
                        template: '<input type="text" class="fieldlabel1 inputCustom inputFileStorageType" disabled required/>'
                    },
                    {
                        field: "text",
                        title: "Feature",
                    },
                    {
                        title: "Path",
                        field: "filePath",
                        template: '<div >' +
                        '<input type="text" class="fieldlabel1 inputCustom inputPath k-textbox" disabled required/>' +
                        '</div>'
                    }, {
                        title: "Cloud Key",
                        field: "cloudKey",
                        hidden: true,
                        template: '<div>' +
                        '<input type="text" class="fieldlabel1 inputCustom inputCloudKey k-textbox" disabled required/>' +
                        '</div>'
                    }, {
                        title: "Username",
                        field: "username",
                        hidden: true,
                        template: '<div>' +
                        '<input type="text" class="fieldlabel1 inputCustom inputUserName k-textbox" disabled required/>' +
                        '</div>'
                    }, {
                        title: "Password",
                        field: "password",
                        hidden: true,
                        template: '<div>' +
                        '<input type="text" class="fieldlabel1 inputCustom inputPassword k-textbox" disabled required/>' +
                        '</div>'
                    }],

                noRecords: {
                    template: "Select a campus group."
                }
            };


            this.btnSave.on("click", function (e) {
                if (that.mainValidator.validate()) {
                    let campusConfigurationId = that.campusConfigurationId;
                    let appliesToAllFeatures = !that.perFeatureConfiguration as boolean;
                    let campusGroupId = that.campusGroupDropdown.getValue();
                    let passwordMain = that.passwordMain.val();
                    let usernameMain = that.userNameMain.val();
                    let cloudKeyMain = that.cloudKeyMain.val();
                    let pathMain = that.pathMain.val();
                    let fileStorageTypeMain: FileStorageConfigs = FileStorageConfigs[that.fileStorageTypeDropdown.getText()];
                    let modUser = that.hdnModUser;
                    let customFileFeatures = new Array<CustomFileConfig>();

                    if (!appliesToAllFeatures) {
                        //var customFeatures = that.fileStorageTypeFeatures.filter((ddl) => { return FileStorageConfigs[ddl.getText()] !== fileStorageTypeMain })

                        that.fileStorageTypeFeatures.forEach((ddl) => {
                            let $tr = ddl.getKendoDropDown().element.closest('tr');
                            let fileStorageType = FileStorageConfigs[ddl.getText()] as FileStorageConfigs;
                            let path = $tr.find('input.inputPath').val();
                            let cloudKey = $tr.find('input.inputCloudKey').val();
                            let username = $tr.find('input.inputUserName').val();
                            let password = $tr.find('input.inputPassword').val();
                            if (fileStorageType !== fileStorageTypeMain || path !== pathMain
                                || cloudKey != cloudKeyMain || username != usernameMain || password != passwordMain) {
                                let featureId = parseInt(that.fileFeaturesGrid.getKendoGridObject().dataItem($tr).get("value"));
                                let customConfig = {
                                    campusFileConfigurationId: campusConfigurationId, cloudKey: cloudKey,
                                    featureId: featureId, fileStorageType: fileStorageType, modUser: modUser, path: path, userName: username,
                                    password: password
                                } as CustomFileConfig

                                customFileFeatures.push(customConfig);

                            }



                        })
                    }
                    let campusFileConfig = {
                        id: campusConfigurationId,
                        campusGroupId: campusGroupId,
                        appliesToAllFeatures: appliesToAllFeatures,
                        cloudKey: cloudKeyMain,
                        fileStorageType: fileStorageTypeMain,
                        path: pathMain,
                        userName: usernameMain,
                        password: passwordMain,
                        customFeatureConfigurations: customFileFeatures,
                        modUser: modUser

                    } as CampusFileConfig
                    that.campusFileConfigDS.updateCampusFileConfig(campusFileConfig, (res: any) => {

                        if (res.result) {
                            that.campusFileConfig = <CampusFileConfig>res.result;
                            that.campusConfigurationId = that.campusFileConfig.id;
                            MasterPage.SHOW_INFO_WINDOW(
                                "The file storage configuration setting has been saved.");
                        }
                        else {
                            MasterPage.SHOW_ERROR_WINDOW("An error occured when trying to save the file storage setting.")
                        }
                    });
                }

            });

            this.btnDelete.on("click", function (e) {
                $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(
                    "Are you sure that you want to delete the file storage configuration?"))
                    .then((confirmed) => {
                        if (confirmed) {
                            let campusFileConfig = that.campusConfigurationId;
                            that.campusFileConfigDS.deleteCampusFileConfig(campusFileConfig, (res) => {
                                if (res) {
                                    MasterPage.SHOW_INFO_WINDOW(
                                        "The file storage configuration setting has been deleted.");
                                    that.campusGroupDropdown.setValue("", false);
                                    that.resetPage()
                                        ;
                                }
                                else {
                                    MasterPage.SHOW_ERROR_WINDOW("An error occured when trying to delete the file storage setting.")
                                }
                            });
                        }
                    });

            });

            this.campusGroupDropdown = new DropDownList("ddlCampusGroup", campusGroups, undefined, { settings: { dataValueField: "value" } } as ISettings, ((value) => {
                if (this.campusGroupDropdown.hasValue()) {

                    if (this.campusConfigurationId !== that.emptyGuid)
                        that.resetPage();


                    that.campusFileConfigDS.getCampusFileConfigByCampusGroupId(this.campusGroupDropdown.getValue(),
                        (campusFileConfig: any) => {
                            if (campusFileConfig) {
                                that.campusFileConfig = <CampusFileConfig>campusFileConfig;
                                that.campusConfigurationId = that.campusFileConfig.id;
                                that.restoreValues(that.campusFileConfig);
                                that.btnDelete.prop("disabled", false);
                            }

                        });
                }
            }));

            //Initialize features grid
            this.fileFeaturesGrid = new KendoGrid("fileFeaturesGrid", this.fileFeaturesDS, undefined, undefined, gridSettings);
            this.fileFeaturesGrid.fetch();
            that.fileFeaturesGrid.setWidth(that.fileFeaturesGrid.getWidth());

            this.fileStorageTypeDropdown = new DropDownList($("#ddlFileStorageTypes"), that.fileStorageConfigs, undefined, { settings: { dataValueField: "value" } } as ISettings, ((value) => {
                that.toggleSections();

            }));


            that.pathMain.on("blur", () => {

                if (!(that.perFeatureConfiguration)) {
                    $.each($(".inputPath"), (i, val) => {
                        $(val).val(that.pathMain.val())

                    })
                }

            })

            that.cloudKeyMain.on("blur", () => {

                if (!(that.perFeatureConfiguration)) {
                    $.each($(".inputCloudKey"), (i, val) => {
                        $(val).val(that.cloudKeyMain.val())

                    })
                }

            })
            that.userNameMain.on("blur", () => {

                if (!(that.perFeatureConfiguration)) {
                    $.each($(".inputUserName"), (i, val) => {
                        $(val).val(that.userNameMain.val())

                    })
                }

            })

            that.passwordMain.on("blur", () => {

                if (!(that.perFeatureConfiguration)) {
                    $.each($(".inputPassword"), (i, val) => {
                        $(val).val(that.passwordMain.val())

                    })
                }

            })
            that.featuresSwitch.on("click", () => {
                that.perFeatureConfiguration = !(that.featuresSwitch.is(":checked"))
                if (that.perFeatureConfiguration) {
                    that.enableGrid(true);
                }
                else {
                    that.enableGrid(false);
                }
            })
        }


        private resetPage() {
            var that = this;
            that.fileStorageTypeDropdown.setValue("", false);
            that.pathMain.val("");
            that.cloudKeyMain.val("");
            that.userNameMain.val("");
            that.passwordMain.val("");
            that.campusConfigurationId = that.emptyGuid;
            that.btnDelete.prop("disabled", true);
            that.togglePerFeatureSwitch(true);


            this.fileStorageTypeFeatures.forEach((ddl) => {
                let $tr = ddl.getKendoDropDown().element.closest('tr');
                let $path = $tr.find('input.inputPath');
                let $cloudKey = $tr.find('input.inputCloudKey');
                let $username = $tr.find('input.inputUserName');
                let $password = $tr.find('input.inputPassword');

                ddl.setValue("");
                $path.val("");
                $cloudKey.val("");
                $username.val("");
                $password.val("");

            })

            that.toggleColumns(false, ["cloudKey", "username", "password"])
            that.toggleMainContainers(false);
            that.enableGrid(false);
        }

        private toggleSections() {
            let that = this;
            that.toggleMainContainers(false);
            that.toggleColumns(true);

            if (this.fileStorageTypeDropdown.getValue() === FileStorageConfigs.Cloud.toString()) {
                MasterPage.Common.show($("#inputCloudKeyContainer"));
                MasterPage.Common.show($("#inputPathContainer"));

                that.toggleColumns(false, ["username", "password"])
            }
            else if (this.fileStorageTypeDropdown.getValue() === FileStorageConfigs.Network.toString()) {
                MasterPage.Common.show($("#inputUserCredentialsContainer"));
                MasterPage.Common.show($("#inputPathContainer"));

                that.toggleColumns(false, ["cloudKey"])
            }
            else {
                if (this.fileStorageTypeDropdown.getValue() === FileStorageConfigs.File.toString()) {
                    MasterPage.Common.show($("#inputPathContainer"));

                }
                that.toggleColumns(false, ["cloudKey", "username", "password"])

            }


            if (!(that.perFeatureConfiguration)) {
                that.fileStorageTypeFeatures.forEach((val) => {

                    val.setValue(that.fileStorageTypeDropdown.getValue())

                })
            }
            //else {
            //    that.enableGrid(false);
            //    that.enableGrid(true);
            //}
        }
        public toggleMainContainers(show: boolean) {
            if (show) {
                MasterPage.Common.show(("#inputPathContainer"));
                MasterPage.Common.show($("#inputCloudKeyContainer"));
                MasterPage.Common.show($("#inputUserCredentialsContainer"));

            }
            else {
                MasterPage.Common.hide($("#inputPathContainer"));
                MasterPage.Common.hide($("#inputCloudKeyContainer"));
                MasterPage.Common.hide($("#inputUserCredentialsContainer"));
            }
        }

        public toggleColumns(show: boolean, columns?: Array<number | string>) {

            if (show) {

                if (columns && columns.length > 0) {
                    columns.forEach((val) => {
                        this.fileFeaturesGrid.showColumn(val);
                    })
                }
                else {
                    this.fileFeaturesGrid.getColumns().forEach((val) => {

                        this.fileFeaturesGrid.showColumn(val.field);
                    })
                }
            }
            else {
                if (columns && columns.length > 0) {
                    columns.forEach((val) => {
                        this.fileFeaturesGrid.hideColumn(val);
                    })
                }
                else {
                    this.fileFeaturesGrid.getColumns().forEach((val) => {

                        this.fileFeaturesGrid.hideColumn(val.field);
                    })
                }
            }
            //this.fileFeaturesGrid.resizeColumns();
        }

        public togglePerFeatureSwitch(appliesToAll: boolean) {
            this.featuresSwitch.prop('checked', appliesToAll);
            this.perFeatureConfiguration = !(this.featuresSwitch.is(":checked"))
        }

        public restoreValues(campusConfig: CampusFileConfig) {
            this.campusGroupDropdown.setValue(campusConfig.campusGroupId, false);
            this.fileStorageTypeDropdown.setValue(campusConfig.fileStorageType.toString());
            this.togglePerFeatureSwitch(campusConfig.appliesToAllFeatures);
            this.cloudKeyMain.val(campusConfig.cloudKey);
            this.pathMain.val(campusConfig.path);
            this.userNameMain.val(campusConfig.userName);
            this.passwordMain.val(campusConfig.password);

            this.fileStorageTypeFeatures.forEach((ddl) => {
                let $tr = ddl.getKendoDropDown().element.closest('tr');
                let $path = $tr.find('input.inputPath');
                let $cloudKey = $tr.find('input.inputCloudKey');
                let $username = $tr.find('input.inputUserName');
                let $password = $tr.find('input.inputPassword');

                let featureId = parseInt(this.fileFeaturesGrid.getKendoGridObject().dataItem($tr).get("value"));

                let customFeature = campusConfig.customFeatureConfigurations.filter((cus) => { return cus.featureId === featureId })[0];

                if (customFeature) {
                    ddl.setValue(customFeature.fileStorageType.toString());
                    $path.val(customFeature.path);
                    $cloudKey.val(customFeature.cloudKey);
                    $username.val(customFeature.userName);
                    $password.val(customFeature.password);

                }
                else {
                    ddl.setValue(this.fileStorageTypeDropdown.getValue());
                    $path.val(this.pathMain.val());
                    $cloudKey.val(this.cloudKeyMain.val());
                    $username.val(this.userNameMain.val());
                    $password.val(this.passwordMain.val());

                }



            })


        }

        //function to disable or enable inputs 
        public enableGrid(enable: boolean) {
            let that = this;
            let rows = $("#fileFeaturesGrid")
                .data("kendoGrid")
                .tbody
                .find("tr");

            rows.each((i, val) => {

                let $ele = $(val)
                let featureStorageTypeDD = that.fileStorageTypeFeatures[i];
                let featurePath = $ele.find("input.inputPath");
                if (enable) {
                    featureStorageTypeDD.enable(enable);
                    featurePath.prop("disabled", false);
                    if (featureStorageTypeDD.getValue() === FileStorageConfigs.Cloud.toString()) {
                        let cloudKey = $ele.find("input.inputCloudKey")
                        cloudKey.prop("disabled", false);
                    }
                    else if (featureStorageTypeDD.getValue() === FileStorageConfigs.Network.toString()) {
                        let userName = $ele.find("input.inputUserName")
                        let password = $ele.find("input.inputPassword")
                        userName.prop("disabled", false);
                        password.prop("disabled", false);

                    }

                }
                else {
                    featureStorageTypeDD.enable(enable);
                    featurePath.prop("disabled", true);
                    let cloudKey = $ele.find("input.inputCloudKey")
                    cloudKey.prop("disabled", true);
                    let userName = $ele.find("input.inputUserName")
                    let password = $ele.find("input.inputPassword")
                    userName.prop("disabled", true);
                    password.prop("disabled", true);

                }

            })


        }


        initGridDropDowns() {
            var that = this;
            that.fileStorageTypeFeatures = [];
            //init each dropdown list for each row
            $("#fileFeaturesGrid").find(".inputFileStorageType").each((i, val) => {
                var $this = $(val);
                let dd = new DropDownList($this, this.fileStorageConfigs, undefined, undefined,
                    () => {

                        //logic to disable inputs per row of feature grid and toggle columns
                        if (that.perFeatureConfiguration) {
                            let $tr = dd.getKendoDropDown().element.closest('tr');
                            let $cloudKey = $tr.find('input.inputCloudKey');
                            let $username = $tr.find('input.inputUserName');
                            let $password = $tr.find('input.inputPassword');
                            let $path = $tr.find('input.inputPath');

                            //file storage type and file path should always be enabled for all file storage types when per feature configuration is true
                            dd.enable(true);
                            $path.prop("disabled", false);

                            //disable rest by default
                            $cloudKey.prop("disabled", true);
                            $username.prop("disabled", true);
                            $password.prop("disabled", true);


                            if (dd.getValue() === FileStorageConfigs.Cloud.toString()) {
                                if (that.fileFeaturesGrid.getColumnByName("cloudKey").hidden) {
                                    that.toggleColumns(true, ["cloudKey"])
                                }
                                $cloudKey.prop("disabled", false);
                            }
                            else if (dd.getValue() === FileStorageConfigs.Network.toString()) {
                                $username.prop("disabled", false);
                                $password.prop("disabled", false);
                                if (that.fileFeaturesGrid.getColumnByName("username").hidden) {
                                    that.toggleColumns(true, ["username"])
                                }
                                if (that.fileFeaturesGrid.getColumnByName("password").hidden) {
                                    that.toggleColumns(true, ["password"])
                                }
                            }
                            let cloudFields = that.fileStorageTypeFeatures.filter((fstf) => { return fstf.getValue() === FileStorageConfigs.Cloud.toString() });
                            let networkFields = that.fileStorageTypeFeatures.filter((fstf) => { return fstf.getValue() === FileStorageConfigs.Network.toString() });

                            if (!(cloudFields.length > 0)) {

                                that.toggleColumns(false, ["cloudKey"])

                            }
                            if (!(networkFields.length > 0)) {
                                that.toggleColumns(false, ["username", "password"])
                            }
                        }
                    });

                that.fileStorageTypeFeatures.push(dd);

            });
        }

    }
}