﻿module Api.ViewModels.Maintenance {

    import Reports = Api.Reports.Models.Reports
    import States = Api.SystemCatalog.Models.IStates
    import DropDownList = Api.Components.DropDownList;
    import KendoGrid = Api.Components.Grid;
    import ISettings = API.Models.ISettings;
    import IFilterParameters = API.Models.IFilterParameters;
    import Program = Api.Program;
    import StateBoardCourse = Api.StateBoardCourse;
    import ProgramStateBoardCourseMapping = Api.SystemCatalog.Mappings.ProgramStateBoardCourseMapping;

    declare var XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL: string;

    export class StateBoardAgencyMaster {

        //validators
        dropdownsValidator: kendo.ui.Validator;

        //dropdowns
        stateBoardStatesAndAgenciesDropdown: DropDownList;
        stateBoardReportsDropdown: DropDownList;
        campusDropdown: DropDownList;

        //data sources
        campus: Campus;
        reports: Api.Reports.Models.Reports;
        stateBoardStates: Api.States;

        //services
        accreditingAgencyService: Api.AccreditingAgency;

        //button
        btnNew: JQuery;
        btnSave: JQuery;
        btnDelete: JQuery;

        //jquery objects
        reportTypeDDRow: JQuery;

        // all stateboard states
        indianaStateBoardVM: IndianaStateBoardSettingsVm;
        emptyGuid: string = "00000000-0000-0000-0000-000000000000";
        stateBoardsAndAccreditingAgenciesData: Array<any>;
        currentSelectedStateboardOrAgency: any;

        constructor() {
            this.btnNew = $("#btnNew");
            this.btnSave = $("#btnSave");
            this.btnDelete = $("#ContentMain2_btnDelete");
            this.reportTypeDDRow = $('#reportTypeDDRow');
            this.dropdownsValidator = MasterPage.ADVANTAGE_VALIDATOR('sharedDropDowns');
        }

        public initialize() {
            var that = this;
            this.campus = new Campus();
            this.reports = new Api.Reports.Models.Reports();
            this.stateBoardStates = new States();
            this.accreditingAgencyService = new AccreditingAgency();

            //disable buttons
            this.btnNew.attr('disabled', 'disabled');
            this.btnSave.attr('disabled', 'disabled');
            this.btnDelete.attr('disabled', 'disabled');

            this.stateBoardReportsDropdown = new DropDownList("ddStateBoardReports", this.reports, undefined, { settings: { dataValueField: "value" } } as ISettings, (e) => {

                this.dropdownsValidator.validateInput(e.target);
                that.stateBoardOrAgencyHandler();
                if (this.currentSelectedStateboardOrAgency instanceof IndianaStateBoardSettingsVm) {
                    (this.currentSelectedStateboardOrAgency as IndianaStateBoardSettingsVm).initializeGrid();
                }


            });

            this.campusDropdown = new DropDownList("ddCampus", this.campus, undefined, { settings: { dataValueField: "value" } } as ISettings, ((e) => {
                this.dropdownsValidator.validateInput(e.target);
                that.stateBoardOrAgencyHandler();
                if (this.currentSelectedStateboardOrAgency instanceof IndianaStateBoardSettingsVm) {
                    (this.currentSelectedStateboardOrAgency as IndianaStateBoardSettingsVm).initializeGrid();
                }
                else if (this.currentSelectedStateboardOrAgency instanceof NaccasVm) {
                    (this.currentSelectedStateboardOrAgency as NaccasVm).CampusDropdownChange();
                }
            }));




            //merge stateboards and accrediting agencies requests and build dropdown
            this.stateBoardStates.getAllStateBoardStatesRequest((stateBoardStatesResponse: any) => {

                this.accreditingAgencyService.getAccreditingAgencies((accreditingAgenciesResponse: any) => {

                    var stateBoardsAndAccreditingAgencies = new Array<any>();

                    if (accreditingAgenciesResponse.resultStatus === 2)
                        MasterPage.SHOW_ERROR_WINDOW(accreditingAgenciesResponse.resultStatusMessage);

                    var a = stateBoardStatesResponse.map((e) => {
                        return { text: e.text, isAccreditingAgency: false, value: e.value };
                    });

                    var b = accreditingAgenciesResponse.map((e) => {
                        return { text: e.description, isAccreditingAgency: true, accreditingAgencyId: e.accreditingAgencyId, value: e.code };
                    });

                    stateBoardsAndAccreditingAgencies = stateBoardsAndAccreditingAgencies.concat(a).concat(b);
                    this.stateBoardsAndAccreditingAgenciesData = stateBoardsAndAccreditingAgencies;
                    var localDataSource = new kendo.data.DataSource({ data: stateBoardsAndAccreditingAgencies });

                    //setup three main top dropdowns
                    this.stateBoardStatesAndAgenciesDropdown = new DropDownList("ddStateBoardStates", this.stateBoardStates, undefined, { settings: { dataValueField: "value" } } as ISettings, (e) => {

                        this.campusDropdown.reset();
                        that.dropdownsValidator.validateInput(e.target);

                        let selectedStateBoardState = that.stateBoardStatesAndAgenciesDropdown.getValue();
                        let stateBoardStatesFilterParam: IFilterParameters = {
                            params: {
                                stateId: selectedStateBoardState
                            }
                        } as IFilterParameters;

                        that.stateBoardReportsDropdown.reload(stateBoardStatesFilterParam);
                        that.stateBoardOrAgencyHandler();
                        if (this.currentSelectedStateboardOrAgency instanceof IndianaStateBoardSettingsVm) {
                            (this.currentSelectedStateboardOrAgency as IndianaStateBoardSettingsVm).initializeGrid();
                        }
                    });

                    this.stateBoardStatesAndAgenciesDropdown.setDataSource(localDataSource);
                });
            });

            this.btnNew.on("click",
                function (e) {
                    var that = this;
                    MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure you want to cancel your changes?").then(
                        (confirmed) => {
                            if (confirmed) {

                                let redirectPage = XMASTER_PAGE_USER_OPTIONS_RESOLVE_URL + "sy/StateBoardSettings.aspx?resid=863&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&State%20Board/Accrediting%20Agency%20Settings&mod=";
                                window.location.assign(redirectPage);
                            }
                        });
                });
        }

        //adapter that instantiates correct vm depending on drop down values
        public stateBoardOrAgencyHandler() {
            var that = this;

            //unbind if there was a currently selected stateboard or agency
            if (this.currentSelectedStateboardOrAgency && this.currentSelectedStateboardOrAgency.cleanUp) {
                this.currentSelectedStateboardOrAgency.cleanUp();
            }


            //selected stateboard state or agency
            var sbOrA = this.stateBoardsAndAccreditingAgenciesData.filter((item) => {
                return item.value === that.stateBoardStatesAndAgenciesDropdown.getValue();
            })[0];

            //selected report type
            var rType = this.stateBoardReportsDropdown.getData().toJSON().filter((item) => {
                return item.value === that.stateBoardReportsDropdown.getValue();
            })[0];

            if (sbOrA !== undefined && sbOrA.value !== "") {

                if (!sbOrA.isAccreditingAgency && sbOrA.text === "Indiana" && rType && rType.text === "State Board Report") { //indiana stateboard report
                    let sb = new IndianaStateBoardSettingsVm(this);
                    sb.initialize();
                    this.currentSelectedStateboardOrAgency = sb;
                }
                else if (sbOrA.isAccreditingAgency && sbOrA.value === "NACCAS") { //NACCAS
                    let sb = new NaccasVm(this);
                    sb.initialize();
                    this.currentSelectedStateboardOrAgency = sb;
                }
                else if (sbOrA.isAccreditingAgency && sbOrA.value === "COE") { //COE
                    MasterPage.SHOW_ERROR_WINDOW("Not yet implemented");
                }
                else {
                    this.campusDropdown.reset();
                }
                //this.resetPage();
            }
            //do nothing if no cases satisfied
        }

        public resetPage() {
            this.stateBoardStatesAndAgenciesDropdown.reset();
            this.stateBoardReportsDropdown.reset();
            this.campusDropdown.reset();
            this.reportTypeDDRow.show();
            this.currentSelectedStateboardOrAgency = null;
        }
    }
}