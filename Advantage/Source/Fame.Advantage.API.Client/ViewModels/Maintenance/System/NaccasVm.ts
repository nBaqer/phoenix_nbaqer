﻿/// <reference path="../../../../Fame.Advantage.Client.AD/Common/DropDownOutputModel.ts" />
/// <reference path="../../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../../../Fame.Advantage.API.Client/Components/Common/DropDownList.ts" />
/// <reference path="../../../../Fame.Advantage.API.Client/Components/Common/MultiSelectDropDownList.ts" />
/// <reference path="../../../../Fame.Advantage.API.Client/Components/Common/Grid.ts" />
/// <reference path="../../../../Fame.Advantage.API.Client/API/SystemCatalog/Mappings/ProgramStateBoardCourseMapping.ts" />
/// <reference path="StateBoardAgencyMaster.ts" />
/// <reference path="../../../API/SystemCatalog/DropReason.ts" />
module Api.ViewModels.Maintenance {
    import Reports = Api.Reports.Models.Reports
    import States = Api.SystemCatalog.Models.IStates
    import DropDownList = Api.Components.DropDownList;
    import KendoGrid = Api.Components.Grid;
    import ISettings = API.Models.ISettings;
    import IFilterParameters = API.Models.IFilterParameters;
    import Program = Api.Program;
    import ProgramVersion = Api.ProgramVersions
    import CommonSourses = Api.CommonSources;
    import AcreditingAgency = Api.AccreditingAgency;
    import DropReason = Api.DropReason
    import StateBoardCourse = Api.StateBoardCourse;
    import ProgramStateBoardCourseMapping = Api.SystemCatalog.Mappings.ProgramStateBoardCourseMapping;
    import MultiSelectDropDownList = Api.Components.MultiSelectDropDownList;
    import Data = kendo.data;


    export class NaccasVm {
        masterPage: StateBoardAgencyMaster;
        X_PLEASE_FILL_REQUIRED_FIELDS = "Please enter the required values.";
        naccasSection: JQuery;
        naccasPanelDetailSection: JQuery;
        oweSchoolMoneyInput: JQuery;
        allowedToGraduateInput: JQuery;

        oweSchoolMoneyDD: DropDownList;
        allowedToGraduateDD: DropDownList;

        campusSectionValidator: kendo.ui.Validator;
        dropReasonMappingValidator: kendo.ui.Validator;
        naccasSectionValidator: kendo.ui.Validator;
        naccasDetailsPanel: kendo.ui.PanelBar;
        approvedProgramVersionGrid: KendoGrid;
        dropReasonMappingGrid: KendoGrid;
        emptyGuid: string = "00000000-0000-0000-0000-000000000000";
        programVersion: ProgramVersions;
        commonSource: CommonSources;
        accreditingAgency: AcreditingAgency;
        dropReason: DropReason;

        sbsValidator: kendo.ui.Validator;

        constructor(masterPage: StateBoardAgencyMaster) {
            this.masterPage = masterPage;
            this.naccasSection = $('#naccasSection');
            this.naccasPanelDetailSection = $("#panelBarDetailsNaccas");
        }



        public initialize() {
            var that = this;




            this.programVersion = new ProgramVersions();
            this.commonSource = new CommonSources();
            this.accreditingAgency = new AccreditingAgency();
            this.dropReason = new DropReason();
            //view changes
            this.masterPage.reportTypeDDRow.hide();
            this.naccasSection.show();

            this.masterPage.btnNew.attr('disabled', null);
            this.masterPage.btnSave.attr('disabled', null);
            this.masterPage.btnDelete.attr('disabled', null);

            that.sbsValidator = MasterPage.ADVANTAGE_VALIDATOR("naccasSection");

            let gridSettings: ISettings = {} as ISettings;
            that.naccasSection.hide();


            gridSettings.settings = {
                pageable: false,
                autoBind: false,
                columns: [{
                    field: "programVersionDescription",
                    title: "Program Version "
                },
                {
                    title: "Select All",
                    selectable: true,
                }, {
                    hidden: true,
                    field: "programVersionId",
                    template: function (item) {
                        return '<input data-pvId="' + item.programVersionId + '"/>';
                    }
                }
                ],
                noRecords: {
                    template: "Select a campus and a accrediting agency"
                }


            };

            let gridSettingsDropReasonMapping: ISettings = {} as ISettings;

            gridSettingsDropReasonMapping.settings = {
                pageable: false,
                autoBind: false,
                editable: "inline",
                columns: [{
                    field: "text",
                    title: "NACCAS Drop Reasons"
                },
                {
                    title: "Drop Reasons",
                    template: function (dataItem) {
                        return '<input name="advantageDropReason" class="fieldlabel1 inputCustom ddlAdvantageDropReason" data-naccasDropReason="' + dataItem.value + '"required/>'
                    }
                }
                ],

            };

            //validator setup
            this.naccasSectionValidator = MasterPage.ADVANTAGE_VALIDATOR("naccasSection");
            this.campusSectionValidator = MasterPage.ADVANTAGE_VALIDATOR("campusDetailsContent");




            // -------------------------------------------------------- 
            this.naccasPanelDetailSection.kendoPanelBar({
                expand: function (e) {

                },
                expandMode: "multiple"
            });

            that.naccasDetailsPanel = this.naccasPanelDetailSection.data("kendoPanelBar");
            that.naccasDetailsPanel.collapse($("li", that.naccasDetailsPanel.element), false);




            // initialize the campus Section
            that.oweSchoolMoneyDD = new DropDownList("ddOweSchoolMoney", this.commonSource, undefined, {
                settings: {
                    dataTextField: "text",
                    dataValueField: "value"
                }
            }, (undefined) => {
                var $this = $("#ddOweSchoolMoney")
                var ddElement = $this.data('kendoDropDownList');
                if (ddElement.value() === undefined || ddElement.value() === '') {
                    $this.addClass('valueRequired');
                } else {
                    $this.prev().children('span.k-input').removeClass('valueRequired')
                    $this.removeClass('valueRequired')
                }
            },
                "getYesNo");


            that.allowedToGraduateDD = new DropDownList("ddAllowedToGraduate", this.commonSource, undefined, {
                settings: {
                    dataTextField: "text",
                    dataValueField: "value"
                }
            },
                (undefined) => {
                    var $this = $("#ddAllowedToGraduate")
                    var ddElement = $this.data('kendoDropDownList');
                    if (ddElement.value() === undefined || ddElement.value() === '') {
                        $this.addClass('valueRequired');
                    } else {
                        $this.prev().children('span.k-input').removeClass('valueRequired')
                        $this.removeClass('valueRequired')
                    }
                },
                "getYesNo");


            this.campusSectionValidator = $("#campusDetailsContent").kendoValidator({
                rules: {
                    hasCampusDetailValid: function (input) {
                        var valid = true;
                        if (input.hasClass('campusDetailInput')) {

                            var ms = input.each((i, val) => {
                                var $this = $(val);

                                if ($this.is('input')) {
                                    let ddElement = $this.data("kendoDropDownList");
                                    if (ddElement.value() === undefined || ddElement.value() === '') {
                                        $this.addClass('valueRequired')
                                        valid = false;
                                    } else {
                                        $this.removeClass('valueRequired')
                                    }
                                }

                            });
                        }
                        return valid;
                    }
                },
                messages: {
                    hasCampusDetailValid: "Please select Yes or No"
                }
            }).data("kendoValidator");


            var campusId = that.masterPage.campusDropdown.hasValue()
                ? that.masterPage.campusDropdown.getValue()
                : that.emptyGuid;

            let campusFilterParam: IFilterParameters = {
                params: {
                    campusId: campusId
                }
            } as IFilterParameters;


            //Initialize approve course grid
            that.approvedProgramVersionGrid = new KendoGrid("approvedNaccasProgramsGrid", this.programVersion, undefined, campusFilterParam, gridSettings, () => { }, "getProgramVersionForApprovalDS");
            that.approvedProgramVersionGrid.fetch().done(() => {
                that.approvedProgramVersionGrid.setPageSizeToAll();
                $('#approvedNaccasProgramsGrid').css({ "cssText": "height: auto !important" });
                $('#approvedNaccasProgramsGrid').find('.k-grid-content').css({ "cssText": "height: auto !important" });
            });

            $($('*[data-field="programVersionDescription"] ~ th').children()[1]).removeClass('k-no-text')
                .css({ "cssText": "color: #999 !important; font-weight: 400 !important;font-size: 10pt !important; line-height: 18pt !important" }).text('Select All');

            //Initialize drop reason mapping grid
            that.dropReasonMappingGrid = new KendoGrid("dropReasonNaccasMappingGrid", this.dropReason, undefined, undefined, gridSettingsDropReasonMapping, () => { }, "GetNaccasDropReasons");
            that.dropReasonMappingGrid.fetch();


            

            this.masterPage.btnSave.on('click',
                function () {
                    if (that.campusSectionValidator.validateInput($('.campusDetailInput')) && that.dropReasonMappingValidator.validateInput($('.ddlAdvantageDropReason'))) {


                        var model = that.GetNaccasFormModel();
                        that.accreditingAgency.saveOrUpdateNaccasSetting(model,
                            (response: Object) => {
                                if (response["resultStatus"] === 0) {
                                    MasterPage.SHOW_INFO_WINDOW(
                                        "The naccas settings have been saved.");

                                } else {
                                    MasterPage.SHOW_ERROR_WINDOW(
                                        response["resultStatusMessage"]);
                                }
                            });
                    }
                    else {
                        //that.sbsValidator.v
                        MasterPage.SHOW_WARNING_WINDOW(that.X_PLEASE_FILL_REQUIRED_FIELDS);
                    }
                });

            this.masterPage.btnDelete.on('click', function () {

                MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE("Are you sure you want to delete this setting?").then(
                    (confirmed) => {
                        if (confirmed) {
                            var model = that.GetNaccasFormModel();
                            that.accreditingAgency.deleteNaccasSetting(model,

                                (response: Object) => {
                                    that.masterPage.campusDropdown.reset();
                                    that.ClearForm();
                                    that.masterPage.resetPage();
                                });
                        }

                    })


            })

        }

        //reverse any event listeners or hidden sections for this specific agency or stateboard
        public cleanUp() {
            this.naccasSection.hide();
            this.masterPage.reportTypeDDRow.show();
            this.masterPage.btnSave.unbind('click');
            this.masterPage.btnDelete.unbind('click');
        }

        public getCheckedApprovedNaccasProgramVersion() {
            var selectedProgramVersion = []
            $("#approvedNaccasProgramsGrid input[type=checkbox]:checked").each(function () {
                if ($(this).parent().next().children('input').attr('data-pvid') !== undefined) {

                    selectedProgramVersion.push($(this).parent().next().children('input').attr('data-pvid'));
                }
            });
            return selectedProgramVersion;
        }

        public getDropReasonMappings() {
            var mappings = []
            $("#dropReasonNaccasMappingGrid").find("input.ddlAdvantageDropReason").each((i, val) => {
                var $this = $(val);
                let ddElement = $this.data("kendoMultiSelect");
                let map = {
                    naccasReasonId: $this.attr('data-naccasDropReason'),
                    advantageReasons: ddElement.value()
                }

                mappings.push(map);
            }
            );
            return mappings;
        }

        public naccasDropdownReasonGridInitializer() {
            var that = this;

            that.dropReasonMappingGrid.reload(undefined, undefined);
            that.dropReasonMappingGrid.fetch().done(() => {

                if (that.masterPage.campusDropdown.hasValue()) {
                    var campusId = that.masterPage.campusDropdown.hasValue()
                        ? that.masterPage.campusDropdown.getValue()
                        : that.emptyGuid;
                    let campusFilterParam: IFilterParameters = {
                        params: {
                            campusId: campusId
                        }
                    } as IFilterParameters;
                    $("#dropReasonNaccasMappingGrid").find("input.ddlAdvantageDropReason").each((i, val) => {
                        var $this = $(val);
                        let dd = new MultiSelectDropDownList($this,
                            this.dropReason,
                            campusFilterParam,
                            undefined,
                            (selected) => {
                                var val = $this.data('kendoMultiSelect').value();
                                if (val.length === 0) {
                                    $this.prev().addClass('valueRequired');
                                } else {
                                    $this.prev().removeClass('valueRequired')
                                }
                            }
                        )


                        let ddElement = $this.data("kendoMultiSelect");
                        ddElement.ul.width(500);
                        
                        dd.fetch().done(() => {
                            dd.setPageSizeToAll();
                        })
                    });



                    that.accreditingAgency.fetchNaccasSettings(campusId,
                        (setting: Object) => {
                            that.allowedToGraduateDD.setValue(setting["allowGraduateWithoutFullCompletion"]);
                            that.oweSchoolMoneyDD.setValue(setting["allowGraduateAndOweMoney"]);

                            var aPV = setting["approvedProgramVersions"];
                            var mappings = setting["dropReasonMappings"];
                            for (var i = 0; i < aPV.length; i++) {
                                var checkbox = $("#approvedNaccasProgramsGrid")
                                    .find('input[data-pvid="' + aPV[i] + '"]')
                                    .parent().prev()
                                    .children("input");

                                checkbox.prop("checked", true);
                            }

                            for (var i = 0; i < mappings.length; i++) {
                                var map = mappings[i];
                                var multiselect = $("#dropReasonNaccasMappingGrid")
                                    .find('input.ddlAdvantageDropReason[data-naccasDropReason="' +
                                    map["naccasReasonId"] +
                                    '"]')
                                let ddElement = multiselect.data("kendoMultiSelect");
                                ddElement.value(map["advantageReasons"]);

                            }

                        });

                } else {

                    that.InitializeAdvantageDropReasonMultiSelectsNoCampus();

                }

                $('#dropReasonNaccasMappingGrid').css({ "cssText": "height: auto !important" });
                $('#dropReasonNaccasMappingGrid').find('.k-grid-content').css({ "cssText": "height: auto !important" });


                that.dropReasonMappingValidator = $("#dropReasonNaccasMappingGrid").kendoValidator({
                    rules: {
                        hasItems: function (input) {
                            var valid = true;
                            if (input.hasClass('ddlAdvantageDropReason')) {

                                var ms = input.each((i, val) => {
                                    var $this = $(val);

                                    if ($this.is('input')) {
                                        let ddElement = $this.data("kendoMultiSelect");
                                        if (ddElement.value().length === 0) {
                                            $this.prev().addClass('valueRequired')
                                            valid = false;
                                        } else {
                                            $this.prev().removeClass('valueRequired')
                                        }
                                    }

                                });
                            }
                            return valid;
                        }
                    },
                    messages: {
                        hasItems: "Please select a drop reason to map NACCAS drop reason"
                    }
                }).data("kendoValidator");
            })
        }

        public InitializeAdvantageDropReasonMultiSelectsNoCampus() {

            $("#dropReasonNaccasMappingGrid").find("input.ddlAdvantageDropReason").each((i, val) => {
                var $this = $(val);
                let dd = new MultiSelectDropDownList($this,
                    this.commonSource,
                    undefined,
                    undefined,
                    undefined,
                    "getNoCampusSelectedOption");


                let ddElement = $this.data("kendoMultiSelect");
                ddElement.ul.width(500);
                ddElement.value('');
            });
        }

        public GetNaccasFormModel() {
            var allowGraduateAndOweMoney = this.oweSchoolMoneyDD.getValue()
            var allowGraduateWithoutFullCompletion = this.allowedToGraduateDD.getValue()
            var approvedProgramVersions = this.getCheckedApprovedNaccasProgramVersion()
            var dropReasonMappings = this.getDropReasonMappings()
            var campusId = this.masterPage.campusDropdown.getValue()
            const model = {
                campusId: campusId,
                allowGraduateAndOweMoney: allowGraduateAndOweMoney,
                allowGraduateWithoutFullCompletion: allowGraduateWithoutFullCompletion,
                approvedProgramVersions: approvedProgramVersions,
                dropReasonMappings: dropReasonMappings

            }
            return model;
        }

        public ClearForm() {
            this.allowedToGraduateDD.setValue('');
            this.oweSchoolMoneyDD.setValue('');
            $("#approvedNaccasProgramsGrid").find('input[type="checkbox"]').prop("checked", false);

            let campusFilterParam: IFilterParameters = {
                params: {
                    campusId: this.emptyGuid
                }
            } as IFilterParameters;



            this.approvedProgramVersionGrid.reload(undefined, campusFilterParam);
            this.approvedProgramVersionGrid.fetch();

            $("#dropReasonNaccasMappingGrid").find('input.ddlAdvantageDropReason').each((i, val) => {
                var ms = $(val).data("kendoMultiSelect");
                ms.value('');
            });

            this.InitializeAdvantageDropReasonMultiSelectsNoCampus();
            this.naccasSection.hide();
        }

        public CampusDropdownChange() {
            this.naccasDropdownReasonGridInitializer();

            if (this.masterPage.campusDropdown.hasValue()) {

                this.naccasDetailsPanel.expand($("li", this.naccasDetailsPanel.element), false);
            } else {
                this.naccasDetailsPanel.collapse($("li", this.naccasDetailsPanel.element), false);
            }

            if (this.masterPage.campusDropdown.hasValue()) {

                this.naccasSection.show();
            } else {

                this.naccasSection.hide();
            }
        }
    }
}