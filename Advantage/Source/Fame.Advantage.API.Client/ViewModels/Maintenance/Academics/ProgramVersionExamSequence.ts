﻿/// <reference path="../../../../Fame.Advantage.API.Client/Components/Common/Grid.ts" />
module Api.ViewModels.Maintenance {
    import KendoGrid = Api.Components.Grid;
    import DropDownList = Api.Components.DropDownList;
    import ISettings = API.Models.ISettings;
    import IFilterParameters = API.Models.IFilterParameters;
    import ProgramVersion = Api.ProgramVersions;

    export class ProgramVersionExamSequence {
        examSequenceGrid: kendo.ui.Grid;

        programVersion: ProgramVersions;

        constructor() {

        }

        public initialize() {

            var that = this;


            this.programVersion = new ProgramVersions();

            let gridSettings: ISettings = {} as ISettings;

            gridSettings.settings = {
                pageable: false,
                autoBind: false,
                columns:[
                    {
                        field: "sequenceNumber",
                        title: "Order"
                    },
                    {
                        field: "name",
                        title:"Exam"
                    }],
                noRecords: {
                    template :"No exams found"
                }
            }

            var programVersionId = "94F01F22-E6D8-468F-815E-913030DED725";

            let examsGridFilterParam: IFilterParameters = {
                params: {
                    programVersionId: programVersionId
                }
            } as IFilterParameters;


            //that.examSequenceGrid = new KendoGrid("Exams",
            //    this.programVersion,
            //    undefined,
            //    examsGridFilterParam,
            //    gridSettings,
            //    () => {},
            //    "getProgramVersionExams");
            

            that.examSequenceGrid = $("#programVersionExamSequenceTable").kendoGrid({ height: 700 }).data("kendoGrid")

            //var dataSource = examSequenceGrid.data().kendoGrid.dataSource.view()
            //dataSource.schema = {
            //    model: {
            //        id: "position",
            //        fields: {
            //            id: { type: "number" },
            //            gradeBookDetailId: { type: "string" },
            //            position: { type: "number" }
            //        }
            //    }
            //}
            //examSequenceGrid.table.kendoDraggable({
            //    filter: "tbody > tr",
            //    group: "gridGroup",
            //    hint: function (e) {
            //        return $('<div class="k-grid k-widget"><table><tbody><tr>' + e.html() + '</tr></tbody></table></div>');
            //    }
            //});

            //examSequenceGrid.table/*.find("tbody > tr")*/.kendoDropTarget({
            //    group: "gridGroup",
            //    drop: function (e) {
            //        var target = dataSource.get($(e.draggable.currentTarget).data("id")),
            //            dest = $(e.target);

            //        if (dest.is("th")) {
            //            return;
            //        }
            //        dest = dataSource.get(dest.parent().data("id"));

            //        //not on same item
            //        if (target.get("position") !== dest.get("position")) {
            //            //reorder the items
            //            var tmp = target.get("position");
            //            target.set("position", dest.get("position"));
            //            dest.set("position", tmp);

            //            dataSource.sort({ field: "position", dir: "asc" });
            //        }
            //    }
            //});

            that.examSequenceGrid.table.kendoSortable({
                filter: ">tbody >tr",
                hint: $.noop,
                cursor: "move",
                placeholder: function (element) {
                    return element.clone().addClass("k-state-hover").css("opacity", 0.65);
                },
                container: "#programVersionExamSequenceTable tbody",
                change: function (e) {
                    //var skip = examSequenceGrid.dataSource.skip();
                    //var oldIndex = e.oldIndex + skip;
                    //var newIndex = e.newIndex + skip;
                    //var data = examSequenceGrid.dataSource.data();
                    var dataItem = that.examSequenceGrid.dataSource.getByUid(e.item.attr('data-uid'));

                    that.examSequenceGrid.dataSource.remove(dataItem);
                    that.examSequenceGrid.dataSource.insert(e.newIndex, dataItem);
                    //console.log(e.newIndex + 1)
                }
            });

        }

        public GetGridDataSource() {
            return this.examSequenceGrid.dataSource;
        }
    }
}