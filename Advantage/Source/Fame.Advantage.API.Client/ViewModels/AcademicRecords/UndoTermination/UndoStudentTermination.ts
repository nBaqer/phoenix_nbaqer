﻿/// <reference path="../../../components/academicrecords/studentenrollment.ts" />
/// <reference path="../../../components/academicrecords/studentsearchautocomplete.ts" />
/// <reference path="../../../../fame.advantage.client.ad/textmessages.ts" />
/// <reference path="../../../components/common/constants.ts" />


module API.ViewModels.AcademicRecords.UndoTermination {
    import UndoStudentSearch = Api.Components.StudentSearchAutoComplete;
    import StudentEnrollment = Api.Components.StudentEnrollment;
    import ISystemStatus = API.Models.ISystemStatusModel;
    import r2t4ResultsVM = ViewModels.AcademicRecords.StudentTermination;
    let r2T4Termination = new Api.R2T4StudentTermination();
    import Common = Components.Common;


    /**
     * This is the default class for initializing the Undo Termination Page.
     * Receive in the constructor any required parameters like the campusId.
     * Declare the View Model variable and call the initialization methods as needed
     */
    export class UndoStudentTermination {
        undoStudentSearchComponent: UndoStudentSearch;
        campusId: string;
        studentId: string;
        studentName: string;
        studentSsn: string;

        constructor(campusId: string) {
            this.campusId = campusId;
            if (campusId != null) {
                this.initializeUndoStudentSearch();
                this.clearSearchOnBlur();
                this.clearSearchResult();
                this.preventPasteOnInput();
                this.handleEnterKeyOnInput();
                this.clearSearchResultOnClearText();
                this.cancelUndoTermination();
                this.initiateUndoTermination();
            }
        }


        private initializeUndoStudentSearch() {
            /**
             * Components are resuable user controls that have built in functionality and encapsulate to a degree certain business logic.
             * It is common to search a student by first name, last name, phone number, SSN or enrollment, because the same logic can be reused
             * across a lot of pages, we need to build the functionality so other pages can reuse the same code. In this case the component
             * Student Search Auto Complete will accept the minium parameteres needed to get out of the box funcionality and get you going with just the Id
             * of the component(form id, input id, or button id), the current campus and one event, the OnChange to be called when a student is selected.
             */
            let systemStatus = {} as Models.ISystemStatusModel;

            this.undoStudentSearchComponent = new UndoStudentSearch("txtUndoStudentSearch",
                this.campusId,
                (response) => {
                    let enrollmentDetails: ISystemStatus = { StudentId: response.value, IsActiveEnrollments: false, IsDroppedEnrollments: true };
                    this.getEnrollments(response.value, enrollmentDetails);
                    this.studentId = response.value.studentId;
                    this.studentName = response.value.displayName;
                    this.studentSsn = response.value.ssn;
                    Common.undoStudentName = response.value.displayName;
                    (document.getElementById("dvUndoEnrollment") as HTMLDivElement).setAttribute("style", "display:block");
                });
        }


        //Method to get the enrollments.
        private getEnrollments(studentInfo, systemStatus) {
            try {
                let studentEnrollmentId: string;
                let programName: string;
                let enrollmentSaveMessage: string;
                let studentName: string;
                document.getElementById('UndoStudentEnrollment').innerHTML = "";
                let studentEnrollmentDetails = new StudentEnrollment("UndoStudentEnrollment",
                    studentInfo.studentId, systemStatus,
                    ((value) => {
                        $("#spnUndo").removeClass("disabled");
                        $("#spnCancel").removeClass("disabled");
                        $("#btnCancelUndoTermination").prop('disabled', false);
                        $("#btnUndoTermination").prop('disabled', false);
                        Common.undoEnrollmentName = value.programVersionDescription;
                        $("#dvUndoEnrollment").show();

                    }) as any,
                    (() => { return this.isTerminationDataValid(); }) as any);
            }
            catch (e) {
                return;
            }
        }

        public isTerminationDataValid(): boolean {
            let isValidForm: boolean = true;
            $("#divTerminationDetail").find("input:text").each((index, element) => {
                let tooltip = $(element).prop("name") + "tt";
                let tool = $(`#${tooltip}`);
                if (tool.length > 0) {
                    isValidForm = false;
                }
            });
            return isValidForm;
        }

        //Method to clear the search text box on blur event
        private clearSearchOnBlur() {
            $("span.k-icon.k-clear-value.k-i-close").click(() => {
                $("#dvUndoEnrollment").hide();
                $("#spnUndo").addClass("disabled");
                $("#spnCancel").addClass("disabled");
                $("#btnCancelUndoTermination").prop('disabled', true);
                $("#btnUndoTermination").prop('disabled', true);
            });
        }

        //Method to clear the search text box on key up event
        private clearSearchResult() {
            $("#txtUndoStudentSearch").keyup(() => {
                $("#spnUndo").addClass("disabled");
                $("#spnCancel").addClass("disabled");
                $("#btnCancelUndoTermination").prop('disabled', true);
                $("#btnUndoTermination").prop('disabled', true);
            });
        }

        //Method to clear the search text box on key press event
        private clearSearchResultOnClearText() {
            $("#txtUndoStudentSearch").on("input", () => {
                $("#dvUndoEnrollment").hide();
                $("#spnUndo").addClass("disabled");
                $("#spnCancel").addClass("disabled");
                $("#btnCancelUndoTermination").prop('disabled', true);
                $("#btnUndoTermination").prop('disabled', true);
            });
        }

        // Method to stop copy paste on stdent search textbox
        private preventPasteOnInput() {
            $("#txtUndoStudentSearch").bind({
                paste: () => {
                    return false;
                }
            });
        }

        // Method to handle enter event on undo stdent search textbox
        public handleEnterKeyOnInput() {
            $(document).keypress((event) => {
                if (event.key !== "F5") {
                    if (event.currentTarget["activeElement"].type !== "button") {
                        if (event.keyCode === 13) {
                            event.preventDefault();
                            return false;
                        }
                    }
                } else {
                    window.location.assign(document.location.href);
                    event.preventDefault();
                    return false;
                }
                return true;
            });
        }

        //Method to cancel undo termination
        private cancelUndoTermination() {
            $("#btnCancelUndoTermination").click(() => {
                try {
                    $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(Common.cancelUndoTerminationMessage))
                        .then(confirmed => {
                            if (confirmed) {
                                if (document.location.href !== null) {
                                    window.location.assign(document.location.href);
                                }
                            }
                        });
                }
                catch (e) {
                    return;
                }
            });

        }

        //Method to start undo termination.
        private initiateUndoTermination() {
            $("#btnUndoTermination").click(() => {
                try {
                    $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(AD.X_MESSAGE_UNDO_TERMINATION.replace("$StundentName", Common.undoStudentName).replace("$EnrollmentName", Common.undoEnrollmentName)))
                        .then(confirmed => {
                            if (confirmed) {
                                if (document.location.href !== null) {
                                    let enrollmentId = $("input[name='studentEnrollment']:checked").val();
                                    r2T4Termination.UndoTerminationById({ enrollmentId },
                                        (result) => {
                                            window.location.assign(document.location.href);
                                        });
                                }
                            }
                        });
                }
                catch (e) {
                    return;
                }
            });

        }
    }
}