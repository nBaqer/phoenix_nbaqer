﻿/// <reference path="../../../../fame.advantage.client.ad/textmessages.ts" />
/// <reference path="../../../../fame.advantage.client.masterpage/AdvantageMessageBoxs.ts" />
/// <reference path="../../../../Fame.Advantage.Client.AD/Common/DropDownOutputModel.ts" />
/// <reference path="../R2T4Results/R2T4ResultsVM.ts" />
/// <reference path="../../../../fame.advantage.client.masterpage/commonfunctions.ts" />

module API.ViewModels {
    import X_PLEASE_FILL_REQUIRED_FIELDS = AD.X_PLEASE_FILL_REQUIRED_FIELDS;
    import StudentTerminationApi = Api.R2T4StudentTermination;
    import ApproveTerminationVm =
    ViewModels.AcademicRecords.StudentTermination.ApproveTermination.ApproveTerminationVm;
    import Utility = API.Common.Utility;
    import Common = Components.Common;
    import R2T4ResultsVm = ViewModels.AcademicRecords.R2T4Results.R2T4ResultsVm;
    import iR2T4Results = API.AcademicRecords.Models.IR2T4Results;
    let r2T4Termination = new Api.R2T4StudentTermination();
    import ProgramVersions = Api.ProgramVersions;
    import Enrollments = Api.Enrollments;
    import IStudentEnrollment = Models.IStudentEnrollment;

    /**
     * This is the default class for initializing the Student Termination Page.
     * Receive in the constructor any required parameters like the campusId.
     * Declare the View Model variable and call the initialization methods as needed
     */
    export class StudentTermination {
        /**
         * Declaring the master viewModel. The viewModel will have all the necessary UI business logic and it will connect UI elements to their API request and components.
         * The master viewmodel can initialize children viewModels based on workflows needs. For example, the initialize method will set up the first screen only.
         * The second step(tab) can be another view model called StudentTermionationStep2Vm for example, and be called when is needed or be declared as an object to be used
         * inside the StudentTerminationVm itself.
         */
        public studentValidator: kendo.ui.Validator;
        viewModel: StudentTerminationVm;
        public r2T4ResultsVm: R2T4ResultsVm;
        approveTerminationVm: ApproveTerminationVm;
        r2T4Panel: any;
        r2T4PanelResult: any;
        terminationId: string;
        emptyGuid: any = "00000000-0000-0000-0000-000000000000";

        public utility: Utility;
        terminationModel: any;

        public r2T4InputModel = {} as Models.IR2T4Input;
        public r2T4Termination = new Api.R2T4StudentTermination();

        constructor(campusId: string) {
            this.viewModel = new StudentTerminationVm(campusId);
            this.r2T4ResultsVm = new R2T4ResultsVm(this.viewModel);
            if (campusId != null) {
                this.viewModel.initialize();
            }
            this.studentValidator = MasterPage.ADVANTAGE_VALIDATOR("divTerminationDetail", false);

            this.terminationModel = {} as Models.IStudentTermination;
            this.r2T4Panel = $(".panelbar").kendoPanelBar();
            this.r2T4PanelResult = $(".panelbarR2T4Result").kendoPanelBar();

            //event to navigate to approve termination page after clicking approve termination tab button.
            (document.getElementById("liApproveTermination") as HTMLInputElement).onclick = () => {
                try {

                    this.r2T4ResultsVm.studentName = this.viewModel.studentName;
                    this.r2T4ResultsVm.studentSsn = this.viewModel.studentSsn;
                    this.r2T4ResultsVm.enrollmentName = this.viewModel.enrollmentName;
                    this.r2T4ResultsVm.expandR2T4InputSections();
                    this.r2T4ResultsVm.expandR2T4ResultSections();
                    if ($("#chkClaculationPeriod").prop("checked") && ($("#txtStudentSearch").val() === "" || !(Common.r2T4CompletionStatus.isR2T4ResultsCompleted
                        || Common.r2T4CompletionStatus.isR2T4OverrideResultsCompleted)))
                        return;
                    if (!$("#chkClaculationPeriod").prop("checked") && !Common.isNonR2T4ApproveTabEnabled)
                        return;
                    else if ((!$("#chkClaculationPeriod").prop("checked") && Common.isNonR2T4ApproveTabEnabled) || ($("#chkClaculationPeriod").prop("checked") && (Common.r2T4CompletionStatus.isR2T4ResultsCompleted || Common.r2T4CompletionStatus.isR2T4OverrideResultsCompleted))) {
                        if ($("#chkClaculationPeriod").prop("checked")) {
                            if (Common.r2T4CompletionStatus.isR2T4ResultsCompleted ||
                                Common.r2T4CompletionStatus.isR2T4OverrideResultsCompleted) {
                                this.r2T4ResultsVm.isResultsExists(
                                    (result) => {
                                        if (!!result) {
                                            if ($("#liR2T4Result").hasClass("is-active")) {
                                                if (this.r2T4ResultsVm.validate()) {
                                                    if ($("#ChkOverride").is(':checked')) {
                                                        if ($("#txtTicket").val() === '') {
                                                            MasterPage.SHOW_INFO_WINDOW(
                                                                AD.X_MESSAGE_R2T4Result_TICKET_NO_REQUIRED);
                                                            return;
                                                        }
                                                    }
                                                    if ($("#ChkOverride").is(':checked')) {
                                                        Utility.checkMessagesForOverride(false,
                                                            (response) => {
                                                                if (response) {
                                                                    this.r2T4ResultsVm.navigateToApproveTermination();
                                                                } else {
                                                                    this.r2T4ResultsVm.initializeApproveTermination();
                                                                }
                                                            });
                                                    } else {
                                                        this.r2T4ResultsVm.navigateToApproveTermination(true);
                                                        this.r2T4ResultsVm.initializeApproveTermination();
                                                    }
                                                }
                                            } else if ($("#liR2T4Input").hasClass("is-active")) {
                                                if (this.isR2T4InputDataValid()) {
                                                    let validation = this.validateInputFields();
                                                    if (validation.length === 0) {
                                                        if (this.r2T4ResultsVm.isInputModified) {
                                                            this.isRecalculateResult(true);
                                                        } else {
                                                            this.r2T4ResultsVm.initializeApproveTermination();
                                                        }
                                                    } else {
                                                        return;
                                                    }
                                                }
                                            } else if ($("#liTerminationDetails").hasClass("is-active")) {
                                                this.saveOrUpdateTerminationDetails(true);
                                            }
                                        }
                                    });
                            }
                        } else {
                            if (Common.isNonR2T4ApproveTabEnabled)
                                this.saveOrUpdateTerminationDetails(true);
                        }
                    }
                } catch (e) {
                    this.getTabName();
                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL
                        .replace("$CurrentTabName", Common.activeTabName)
                        .replace("$CurrentTabName", Common.activeTabName));
                    return;
                }
            };

            (document.getElementById("liR2T4Input") as HTMLInputElement).onclick = () => {
                try {
                    if ($("#liR2T4Input").hasClass("is-invalid") ||
                        Common.isR2T4InputTabDisabled ||
                        !$("input[id='chkClaculationPeriod']").prop('checked')) {
                        return;
                    }

                    //Expanding all the panel bars.
                    this.r2T4ResultsVm.expandR2T4InputSections();

                    if ($("#liTerminationDetails").hasClass("is-active")) {
                        //assign the required attribute
                        this.viewModel.setComponentAttribute("txtStudentSearch", "required", "true");
                        this.saveOrUpdateTerminationDetails();
                    } else if ($("#liR2T4Result").hasClass("is-active")) {
                        if (this.r2T4ResultsVm.validate()) {
                            if ($("#ChkOverride").is(':checked')) {
                                if ($("#txtTicket").val() === '') {
                                    MasterPage.SHOW_INFO_WINDOW(AD.X_MESSAGE_R2T4Result_TICKET_NO_REQUIRED);
                                    return;
                                }
                            }
                            if ($("#ChkOverride").is(':checked')) {
                                Utility.checkMessagesForOverride(false,
                                    (response) => {
                                        if (response) {
                                            this.r2T4ResultsVm.saveResultsOnBackClick();
                                            this.viewModel.bindR2T4InputData($("#hdnTerminationId").val());
                                        } else {
                                            this.r2T4ResultsVm.navigateToR2T4Input();
                                            this.viewModel.bindR2T4InputData($("#hdnTerminationId").val());
                                        }
                                    });
                            } else {
                                this.r2T4ResultsVm.saveResultsOnBackClick();
                                this.viewModel.bindR2T4InputData($("#hdnTerminationId").val());
                            }
                        }
                    } else if ($("#liApproveTermination").hasClass("is-active")) {
                        Utility.changeElementStyle("divR2T4Input", "is-active", true);
                        Utility.changeElementStyle("liR2T4Input", "is-active", true);
                        Utility.changeElementStyle("divR2T4Result", "is-active", false);
                        Utility.changeElementStyle("liR2T4Result", "is-active", false);
                        Utility.changeElementStyle("liApproveTermination", "is-active", false);
                        $("#liR2T4Input").addClass("is-active").removeClass("is-visited");
                        $("#liR2T4Input,#liR2T4Result,#liApproveTermination").removeClass("is-invalid");
                        $("#divApproveTermination,#liApproveTermination").removeClass("is-active");
                        Common.isR2T4InputTabDisabled = false;
                        this.viewModel.bindR2T4InputData($("#hdnTerminationId").val());
                    }
                    this.showTuitionChargedCheckbox(); 
                    this.r2T4ResultsVm.isInputModified = false;
                } catch (e) {
                    this.getTabName();
                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL
                        .replace("$CurrentTabName", Common.activeTabName)
                        .replace("$CurrentTabName", Common.activeTabName));
                    return;
                }
            };

            (document.getElementById("liTerminationDetails") as HTMLInputElement).onclick = () => {
                try {
                    this.navigateToTerminationDetails();
                } catch (e) {
                    this.getTabName();
                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL
                        .replace("$CurrentTabName", Common.activeTabName)
                        .replace("$CurrentTabName", Common.activeTabName));
                    return;
                }
            };

            (document.getElementById("btnNext") as HTMLInputElement).onclick = () => {
                this.enrollmentValidation();
                this.r2T4ResultsVm.expandR2T4InputSections();
                try {
                    let programVersions = new ProgramVersions();
                    let inputParameters = { enrollmentId: $("#studentEnrollment input:radio:checked").val(), lastAttendedDate: $("#dtLastDateAttended").val() };
                    let terminationData = this.generateR2T4TerminationModel(this.terminationModel);
                    this.studentValidator = MasterPage.ADVANTAGE_VALIDATOR("divTerminationDetail");
                    if (this.studentValidator.validate()) {
                        if (this.viewModel.dateValidation && this.viewModel.isValidDod) {
                            programVersions.getStudentAwardsByEnrollmentId(inputParameters,
                                (studentAwards: any) => {
                                    if (!!studentAwards &&
                                        studentAwards.length > 0 &&
                                        studentAwards[0].resultStatus === AD.X_MESSAGE_NO_AWARDS) {
                                        $("#chkClaculationPeriod").prop('checked', false);
                                        (document.getElementById("dvCalculationPeriod") as HTMLDivElement).setAttribute(
                                            "style",
                                            "display:none");
                                        let messageText = AD.X_MESSAGE_NO_AWARDS_FOR_STUDENT.replace("$studentName", $("#txtStudentSearch").val()).replace("$enrollmentName", Common.enrollmentName);
                                        if ($("#hdnTerminationId").val() === this.emptyGuid) {
                                            MasterPage.showDialogPopup(messageText);
                                        }
                                        terminationData.calculationPeriodType = Common.periodTypeId =
                                            $("input[id='chkClaculationPeriod']").prop('checked')
                                                ? $("input[name='periodName']:checked")[0].id
                                                : null;
                                        terminationData.isPerformingR2T4Calculator =
                                            $("input[id='chkClaculationPeriod']").prop('checked');

                                        if (!$("#chkClaculationPeriod").prop("checked")) {
                                            if (terminationData.studentEnrollmentId === null ||
                                                terminationData.studentEnrollmentId === undefined) {
                                                MasterPage.SHOW_WARNING_WINDOW(X_PLEASE_FILL_REQUIRED_FIELDS);
                                            } else {
                                                Common.isNonR2T4ApproveTabEnabled = true;
                                                this.saveOrUpdateTerminationDetails(true);
                                            }
                                        } else {
                                            this.saveOrUpdateTerminationDetails();
                                            this.showTuitionChargedCheckbox();
                                        }
                                    } else {
                                        if (!$("#chkClaculationPeriod").prop("checked")) {
                                            Common.isNonR2T4ApproveTabEnabled = true;
                                        }
                                        this.saveOrUpdateTerminationDetails(!$("#chkClaculationPeriod").prop("checked"));
                                        this.showTuitionChargedCheckbox();
                                    }
                                });
                        }
                    }
                    else {
                        MasterPage.SHOW_WARNING_WINDOW(X_PLEASE_FILL_REQUIRED_FIELDS);
                    }
                    this.r2T4ResultsVm.isInputModified = false;
                } catch (e) {
                    this.getTabName();
                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL
                        .replace("$CurrentTabName", Common.activeTabName)
                        .replace("$CurrentTabName", Common.activeTabName));
                    return;
                }
            };

            (document.getElementById("btnSaveTermination") as HTMLInputElement).onclick = () => {
                try {
                    if ($("#dvEnrollment").css("display") !== "none" && !($('#spnSave').hasClass('disabled'))) {
                        if (this.viewModel.dateValidation && this.viewModel.isValidDod) {
                            let inputParameters = { enrollmentId: $("#studentEnrollment input:radio:checked").val(), lastAttendedDate: $("#dtLastDateAttended").val() };
                            let terminationData = this.generateR2T4TerminationModel(this.terminationModel);
                            terminationData.isR2T4ApproveTabEnabled = Common.isNonR2T4ApproveTabEnabled;
                            let programVersions = new ProgramVersions();
                            if (terminationData.terminationId !== this.emptyGuid) {
                                this.terminationId = terminationData.terminationId;
                                terminationData.methodType = Number(Common.MethodType.Save);
                                programVersions.getStudentAwardsByEnrollmentId(inputParameters,
                                    (studentAwards: any) => {
                                        if (!!studentAwards &&
                                            studentAwards.length > 0 &&
                                            studentAwards[0].resultStatus === AD.X_MESSAGE_NO_AWARDS) {
                                            $("#chkClaculationPeriod").prop('checked', false);
                                            (document.getElementById("dvCalculationPeriod") as HTMLDivElement)
                                                .setAttribute("style", "display:none");
                                            let messageText = AD.X_MESSAGE_NO_AWARDS_FOR_STUDENT.replace("$studentName", $("#txtStudentSearch").val()).replace("$enrollmentName", Common.enrollmentName);
                                            MasterPage.showDialogPopup(messageText);
                                            terminationData.calculationPeriodType = Common.periodTypeId =
                                                $("input[id='chkClaculationPeriod']").prop('checked')
                                                ? $("input[name='periodName']:checked")[0].id
                                                : null;
                                            terminationData.isPerformingR2T4Calculator =
                                                $("input[id='chkClaculationPeriod']").prop('checked');
                                        }
                                        this.r2T4Termination.updateTermination(terminationData,
                                            (response: any) => {
                                                if (response["resultStatus"] ===
                                                    AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                                    $("#hdnTerminationId").val(response["terminationId"]);
                                                    MasterPage.SHOW_INFO_WINDOW(response.resultStatus);
                                                } else {
                                                    this.getTabName();
                                                    MasterPage.SHOW_ERROR_WINDOW(AD
                                                        .X_MESAGE_SAVE_INPUT_NEXT_FAIL
                                                        .replace("$CurrentTabName", Common.activeTabName)
                                                        .replace("$CurrentTabName", Common.activeTabName));
                                                    return;
                                                }
                                            });
                                    });
                            } else {
                                terminationData.methodType = Number(Common.MethodType.Save);
                                programVersions.getStudentAwardsByEnrollmentId(inputParameters,
                                    (studentAwards: any) => {
                                        if (!!studentAwards &&
                                            studentAwards.length > 0 &&
                                            studentAwards[0].resultStatus === AD.X_MESSAGE_NO_AWARDS) {
                                            $("#chkClaculationPeriod").prop('checked', false);
                                            (document.getElementById("dvCalculationPeriod") as HTMLDivElement)
                                                .setAttribute("style", "display:none");
                                            let messageText = AD.X_MESSAGE_NO_AWARDS_FOR_STUDENT.replace("$studentName", $("#txtStudentSearch").val()).replace("$enrollmentName", Common.enrollmentName);
                                            MasterPage.showDialogPopup(messageText);
                                            terminationData.calculationPeriodType = Common.periodTypeId =
                                                $("input[id='chkClaculationPeriod']").prop('checked')
                                                ? $("input[name='periodName']:checked")[0].id
                                                : null;
                                            terminationData.isPerformingR2T4Calculator =
                                                $("input[id='chkClaculationPeriod']").prop('checked');
                                        }
                                        this.r2T4Termination.saveTermination(terminationData,
                                            (response: any) => {
                                                if (response["resultStatus"] ===
                                                    AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                                    this.terminationId = response["terminationId"];
                                                    $("#hdnTerminationId").val(this.terminationId);
                                                    MasterPage.SHOW_INFO_WINDOW(response.resultStatus);
                                                } else {
                                                    this.getTabName();
                                                    MasterPage.SHOW_ERROR_WINDOW(AD
                                                        .X_MESAGE_SAVE_INPUT_NEXT_FAIL
                                                        .replace("$CurrentTabName", Common.activeTabName)
                                                        .replace("$CurrentTabName", Common.activeTabName));
                                                    return;
                                                }
                                            });
                                    });
                            }
                        }
                    }
                } catch (e) {
                    this.getTabName();
                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL
                        .replace("$CurrentTabName", Common.activeTabName)
                        .replace("$CurrentTabName", Common.activeTabName));
                    return;
                }
            };

            //Cancel operation for the termination detail, R2T4Input tab, R2T4Result and approve termination.
            $("#btnCancelTermination, #btnCancelTerminationR2T4Input, #btnCancelTerminationR2T4Result, #btnApprovalTerminationCancel")
                .click(function() {
                    try {
                        $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(Common.cancelConfirmationMessage))
                            .then(confirmed => {
                                if (confirmed) {
                                    this.terminationId = $("#hdnTerminationId").val();
                                    if (document.location.href !== null) {
                                        if (this.terminationId === undefined ||
                                            this.terminationId === "" ||
                                            this.terminationId === this.emptyGuid ||
                                            this.terminationId === Common.nullGuid) {
                                            window.location.assign(document.location.href);
                                        } else {
                                            let termination = { "terminationId": this.terminationId };
                                            new StudentTerminationApi().deleteTerminationById(termination,
                                                (result) => {
                                                    let resultMessage = result["message"];
                                                    if (resultMessage === AD.X_MESAGE_TERMINATION_DELETE_SUCCESS) {
                                                        window.location.assign(document.location.href);
                                                    } else {
                                                        MasterPage.SHOW_ERROR_WINDOW(AD
                                                            .X_MESAGE_TERMINATION_DELETE_FAIL_ERROR
                                                            .replace("$StundentName", Common.studentName)
                                                            .replace("$EnrollmentName", Common.enrollmentName));
                                                    }
                                                });
                                        }
                                    }
                                }
                            });
                    } catch (e) {
                        MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_TERMINATION_DELETE_FAIL_ERROR
                            .replace("$StundentName", Common.studentName)
                            .replace("$EnrollmentName", Common.enrollmentName));
                    }
                });

            //Back from Input to termination details
            (document.getElementById("btnR2T4InputBack") as HTMLInputElement).onclick = () => {
                try {
                    this.navigateToTerminationDetails();
                } catch (e) {
                    this.getTabName();
                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL
                        .replace("$CurrentTabName", Common.activeTabName)
                        .replace("$CurrentTabName", Common.activeTabName));
                    return;
                }
            };

            //Back to R2T4Input tab on back button click
            (document.getElementById("btnR2T4InputSave") as HTMLInputElement).onclick = () => {
                try {
                    if (this.isR2T4InputDataValid()) {
                        this.r2T4ResultsVm.isResultsExists((result) => {
                            if (!!result) {
                                if (this.r2T4ResultsVm.isInputModified) {
                                    this.checkIsInputModified(true);
                                } else {
                                    this.saveOrUpdateR2T4InputDetails(false,
                                        (response) => {
                                            if (response["resultStatus"] !==
                                                AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                                this.getTabName();
                                                MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL
                                                    .replace("$CurrentTabName", Common.activeTabName)
                                                    .replace("$CurrentTabName", Common.activeTabName));
                                                return;
                                            } else {
                                                MasterPage.SHOW_INFO_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS);
                                            }
                                        });
                                }
                            } else {
                                this.saveOrUpdateR2T4InputDetails(false,
                                    (response) => {
                                        if (response["resultStatus"] !==
                                            AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                            this.getTabName();
                                            MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL
                                                .replace("$CurrentTabName", Common.activeTabName)
                                                .replace("$CurrentTabName", Common.activeTabName));
                                            return;
                                        } else {
                                            MasterPage.SHOW_INFO_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS);
                                        }
                                    });
                            }
                        });
                    }
                } catch (e) {
                    this.getTabName();
                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL
                        .replace("$CurrentTabName", Common.activeTabName)
                        .replace("$CurrentTabName", Common.activeTabName));
                    return;
                }
            };

            (document.getElementById("divR2T4Input") as HTMLInputElement).onchange = () => {
                this.r2T4ResultsVm.isInputModified = true;
            };

            (document.getElementById("btnR2T4InputNext") as HTMLInputElement).onclick = () => {
                try {
                    this.r2T4ResultsVm.expandR2T4InputSections();
                    this.navigateToR2T4Results(true);
                } catch (e) {
                    this.getTabName();
                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL
                        .replace("$CurrentTabName", Common.activeTabName)
                        .replace("$CurrentTabName", Common.activeTabName));
                    return;
                }
            };

            (document.getElementById("liR2T4Result") as HTMLInputElement).onclick = () => {
                this.r2T4ResultsVm.expandR2T4InputSections();
                this.r2T4ResultsVm.expandR2T4ResultSections();
                if ($("#liR2T4Result").hasClass("is-invalid") ||
                    Common.isR2T4ResultTabDisabled ||
                    $("#txtStudentSearch").val() === "") {
                    return;
                }

                //Set the student name, ssn and enrollment name  for R2T4 results variables
                this.r2T4ResultsVm.studentSsn = this.viewModel.studentSsn;
                this.r2T4ResultsVm.studentName = this.viewModel.studentName;
                this.r2T4ResultsVm.enrollmentName = this.viewModel.enrollmentName;

                if ($("#liTerminationDetails").hasClass("is-active")) {
                    this.r2T4ResultsVm.isResultsExists((response) => {
                        if (!!response) {
                            this.saveOrUpdateTerminationDetails(false, true);
                        }
                    });
                } else if ($("#liR2T4Input").hasClass("is-active")) {
                    this.navigateToR2T4Results();
                } else if ($("#liApproveTermination").hasClass("is-active")) {
                    $("#divR2T4Result, #liR2T4Result").removeClass("is-visited is-invalid").addClass("is-active");
                    $(
                            "#divTerminationDetail, #liTerminationDetails, #divR2T4Input,  #liR2T4Input, #divApproveTermination")
                        .removeClass("is-active is-invalid").addClass("is-visited");
                    $("#liApproveTermination").removeClass("is-active");
                    $("#dateDetermination").text($("#dtDateOfDetermination").val());
                    this.r2T4Termination.fetchR2T4Inputdetails(this.terminationId,
                        (response: any) => {
                            if (!!response) {
                                let r2T4InputDetail: Models.IR2T4Input = this.generateR2T4InputModel(response);
                                this.viewModel.initializeNewR2T4Result(r2T4InputDetail, false, false, false);
                            }
                        });
                }
            };

            /**
            *  event to capture the Back button click event in R2T4 results tab.
            */
            (document.getElementById("btnR2T4ResultBack") as HTMLInputElement).onclick = () => {
                if (this.r2T4ResultsVm.validate()) {
                    if ($("#ChkOverride").is(':checked')) {
                        if ($("#txtTicket").val() === '') {
                            MasterPage.SHOW_INFO_WINDOW(AD.X_MESSAGE_R2T4Result_TICKET_NO_REQUIRED);
                            return;
                        }
                    }
                    if ($("#ChkOverride").is(':checked')) {
                        Utility.checkMessagesForOverride(false,
                            (response) => {
                                if (response) {
                                    this.r2T4ResultsVm.saveResultsOnBackClick();
                                    this.viewModel.bindR2T4InputData($("#hdnTerminationId").val());
                                } else {
                                    this.r2T4ResultsVm.navigateToR2T4Input();
                                    this.viewModel.bindR2T4InputData($("#hdnTerminationId").val());
                                }
                            });
                    } else {
                        this.r2T4ResultsVm.saveResultsOnBackClick();
                        this.viewModel.bindR2T4InputData($("#hdnTerminationId").val());
                    }
                }
            };
        }

        //this function checks if any active enrollment is selected or not
        //return true if any selected else false
        private enrollmentValidation() {
            var selectedIndex = -1;
            $("input[name='studentEnrollment']").each((index, element) => {
                if ($(element).prop("checked")) {
                    selectedIndex = index;
                }
            });
            if (selectedIndex !== -1) {
                $("#spnEnrollment").hide();
            } else {
                $("#spnEnrollment").show();
            } 
        }

        public validateInputFields() {
            let validationMessages: string[] = this.validateR2T4Input();
            if (validationMessages.length > 0) {
                if (validationMessages.length > 1) {
                    let messageSlice = validationMessages.slice(0, validationMessages.length - 1).toString()
                        .replace(",", ", ");
                    let valMessage =
                        `${AD.X_MESAGE_R2T4Input_Prefix} ${messageSlice} and ${validationMessages[
                            validationMessages.length - 1]}`;
                    MasterPage.SHOW_WARNING_WINDOW(valMessage);
                } else {
                    MasterPage.SHOW_WARNING_WINDOW(
                        `${AD.X_MESAGE_R2T4Input_Prefix} ${validationMessages[0]}`);
                }
            }

            return validationMessages;
        }

        private navigateToR2T4Results(isR2T4InputCompleted: boolean = false) {
            this.r2T4ResultsVm.expandR2T4ResultSections();
            if (this.isR2T4InputDataValid()) {
                let validation = this.validateInputFields();
                if (validation.length === 0)
                    this.saveOrUpdateR2T4InputOnNext(isR2T4InputCompleted);
                else
                    return;
            }
        }

        private validateR2T4Input(): string[] {
            let validationMessages: string[] = [];
            let aidValueEntered = false;
            let institutionalChargesEntered = false;
            let percentageValueNotEntered = true;
            $("#dvTitleIVAid").find("input:text").each(function() {
                if ($(this).val() !== "" && parseFloat($(this).val()) > 0) {
                    aidValueEntered = true;
                }
            });
            
            if (this.viewModel.unitTypeDescription == null || this.viewModel.unitTypeDescription == undefined) {
                let enrollmentId = $("#studentEnrollment input:radio:checked").val();
                this.getEnrollmentDetails(this.viewModel.studentId, enrollmentId, (response: any) => {
                    if (!!response && response.unitTypeDescription.toLowerCase() === "none") {
                        if (this.viewModel.isNonTermProgram == null || this.viewModel.isNonTermProgram == undefined) {
                            let programVersions = new ProgramVersions();
                            programVersions.getProgramVersionDetailByEnrollmentId(enrollmentId,
                                (programVersion: any) => {
                                    if (!!programVersion) {
                                        percentageValueNotEntered = programVersion.academicCalendarId === Number(Common.AcademicCalendarProgram.NonTerm);
                                    }
                                });
                        } else {
                            percentageValueNotEntered = this.viewModel.isNonTermProgram;
                        }
                    } else {
                        if (this.viewModel.programUnitTypeId === AD.ProgramUnitTypes.ClockHour) {
                            if ($("#dtWithdrawDate").val() === "" ||
                                $("#txtScheduledHours").val() === "" ||
                                $("#txtTotalHours").val() === "") {
                                percentageValueNotEntered = false;
                            }
                        } else {
                            if ($("#chkAttendance").is(":checked")) {
                                if ($("#dtStartDate").val() === "" ||
                                    $("#dtEndDate").val() === "") {
                                    percentageValueNotEntered = false;
                                }
                            } else {
                                if ($("#dtWithdrawDate").val() === "" ||
                                    $("#txtScheduledHours").val() === "" ||
                                    $("#txtTotalHours").val() === "" ||
                                    $("#dtStartDate").val() === "" ||
                                    $("#dtEndDate").val() === "") {
                                    percentageValueNotEntered = false;
                                }
                            }
                        }
                    }
                });
            } else if (this.viewModel.unitTypeDescription.toLowerCase() === "none") {
                if ($("#dtStartDate").val() !== "" && $("#dtEndDate").val() !== "") {
                    percentageValueNotEntered = true;
                } else {
                    percentageValueNotEntered = false;
                }
            }

            $("#dvInstitutionalChargesPosted").find("input:text").each(function () {
                if (parseFloat($(this).val()) >= 0) {
                    institutionalChargesEntered = true;
                }
            });
            if (!aidValueEntered) {
                validationMessages.push(AD.X_MESAGE_R2T4Input_AID_FILL);
            }
            
            if (!percentageValueNotEntered) {
                validationMessages.push(AD.X_MESAGE_R2T4Input_Percentage_FILL);
            }
            if (!institutionalChargesEntered) {
                validationMessages.push(AD.X_MESAGE_R2T4Input_InstituitionFess_FILL);
            }
            return validationMessages;
        }

        private getEnrollmentDetails(studentId: string, enrollmentId: string, callBackMethod: (response: Response) => any): any {
            let enrollments = new Enrollments();
            if (studentId !== undefined && enrollmentId !== undefined) {
                let studentEnrollmentDetail: IStudentEnrollment = { studentId: studentId, enrollmentId: enrollmentId };
                return enrollments.getEnrollmentDetail(studentEnrollmentDetail, callBackMethod);
            }
        }

        public saveOrUpdateR2T4InputOnNext(isR2T4InputCompleted: boolean = false) {
            let r2T4InputDetail: Models.IR2T4Input = this.generateR2T4InputModel(this.r2T4InputModel);
            r2T4InputDetail.isR2T4InputCompleted =
                (Common.r2T4CompletionStatus.isR2T4InputCompleted || isR2T4InputCompleted);
            //Set the student name, ssn and enrollment name  for R2T4 results variables
            this.r2T4ResultsVm.studentSsn = this.viewModel.studentSsn;
            this.r2T4ResultsVm.studentName = this.viewModel.studentName;
            this.r2T4ResultsVm.enrollmentName = this.viewModel.enrollmentName;

            this.r2T4ResultsVm.isResultsExists(
                (result) => {
                    if (this.r2T4ResultsVm.isInputModified && result) {
                        this.isRecalculateResult();
                    }
                   else if (this.r2T4ResultsVm.isInputModified && !result) {
                        if (r2T4InputDetail.r2T4InputId !== undefined &&
                            r2T4InputDetail.r2T4InputId !== "" &&
                            r2T4InputDetail.r2T4InputId !== this.emptyGuid) {
                            this.r2T4Termination.updateR2T4InputDetail(r2T4InputDetail,
                                (response: any) => {
                                    $("#hdnR2T4InputId").val(response["r2T4InputId"]);
                                    this.prepareNewR2T4Results(r2T4InputDetail, false, false);
                                });
                        } else {
                            this.r2T4Termination.saveR2T4InputDetail(r2T4InputDetail,
                                (response: any) => {
                                    Common.isR2T4InputExists = true;
                                    $("#hdnR2T4InputId").val(response["r2T4InputId"]);
                                    this.prepareNewR2T4Results(r2T4InputDetail, false, false);
                                });
                        }
                    } else if (!this.r2T4ResultsVm.isInputModified && !result && isR2T4InputCompleted) {
                        if (!!r2T4InputDetail &&
                            !!r2T4InputDetail.r2T4InputId &&
                            r2T4InputDetail.r2T4InputId !== this.emptyGuid) {
                            this.r2T4Termination.updateR2T4InputDetail(r2T4InputDetail,
                                (response: any) => {
                                    $("#hdnR2T4InputId").val(response["r2T4InputId"]);
                                    this.prepareNewR2T4Results(r2T4InputDetail, false, false);
                                });
                        }
                    } else if ((!this.r2T4ResultsVm.isInputModified && result) ||
                        (!this.r2T4ResultsVm.isInputModified && !result)) {
                        this.prepareNewR2T4Results(r2T4InputDetail, false, false);
                    }
                });
        }

        //this method will check if any input has data validation message
        //and return true if there is no validation message else false
        private isR2T4InputDataValid(): boolean {
            let isValidForm: boolean = true;
            $("#dvPercentageOfPeriod").find("input:text").each((index, element) => {
                let tooltip = $(element).prop("name") + "tt";
                let tool = $(`#${tooltip}`);
                if (tool.length > 0) {
                    isValidForm = false;
                }
            });
            return isValidForm;
        }

        //call this method once the save or update of termination details is completed.
        private onSaveOrUpdateCompletion(): void {
            if ((document.getElementById("chkClaculationPeriod") as HTMLInputElement).checked) {
                this.prepareR2T4Input($("#hdnTerminationId").val());
                Common.isR2T4InputTabDisabled = false;
            } else {
                this.approveTerminationVm = new ApproveTerminationVm(this.viewModel, true);
                this.approveTerminationVm.prepareApproveTermination(true);
            }
        }

        public saveOrUpdateR2T4InputDetails(showMsg: boolean, callBack: (response: Response) => any) {
            let r2T4Input = {} as Models.IR2T4Input;
            let r2T4Termination = new Api.R2T4StudentTermination();
            let r2T4InputDetail: Models.IR2T4Input = this.generateR2T4InputModel(r2T4Input);
            this.r2T4ResultsVm.isResultsExists((result) => {
                if (result)
                    r2T4InputDetail.isR2T4InputCompleted = Common.r2T4CompletionStatus.isR2T4InputCompleted || !!result;
                else
                    r2T4InputDetail.isR2T4InputCompleted = Common.r2T4CompletionStatus.isR2T4InputCompleted;
                if (r2T4InputDetail.r2T4InputId !== undefined && r2T4InputDetail.r2T4InputId !== "" && r2T4InputDetail.r2T4InputId !== this.emptyGuid) {
                    r2T4Termination.updateR2T4InputDetail(r2T4InputDetail,
                        (response: any) => {
                            $("#hdnR2T4InputId").val(response["r2T4InputId"]);
                            if (showMsg) {
                                MasterPage.SHOW_INFO_WINDOW(response.resultStatus);
                            }
                            return callBack(response);
                        });
                } else {
                    r2T4Termination.saveR2T4InputDetail(r2T4InputDetail,
                        (response: any) => {
                            $("#hdnR2T4InputId").val(response["r2T4InputId"]);
                            Common.isR2T4InputExists = true;
                            if (showMsg) {
                                MasterPage.SHOW_INFO_WINDOW(response.resultStatus);
                            }
                            return callBack(response);
                        });
                }
            });
        }

        private navigatebackAfterInputModify() {
            $("#divTerminationDetail, #liTerminationDetails").addClass("is-active")
                .removeClass("is-visited");
            $("#divR2T4Input, #divR2T4Result, #divApproveTermination").removeClass("is-active is-visited is-invalid");
            $("#liR2T4Input, #liR2T4Result").removeClass("is-active is-invalid").addClass("is-visited");
            Common.isR2T4ResultTabDisabled = false;
            Common.isR2T4ApproveTabDisabled = false;
            this.r2T4ResultsVm.isInputModified = false;
        }

        private navigateToTerminationDetails(): void {
            if ($("#studentEnrollment input:radio:checked").val()) {
                $("#btnCancelTermination").prop("disabled", false);
            }

            this.viewModel.isLdaChanged = false;
            if ($("#liR2T4Input").hasClass("is-active")) {
                if (this.isR2T4InputDataValid()) {
                    this.r2T4ResultsVm.isResultsExists((result) => {
                        if (!!result) {
                            if (this.r2T4ResultsVm.isInputModified) {
                               this.checkIsInputModified();
                                 } else {
                                this.saveOrUpdateR2T4InputDetails(false,
                                    (response) => {
                                        if (response["resultStatus"] !==
                                            AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                            this.getTabName();
                                            MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL
                                                .replace("$CurrentTabName", Common.activeTabName)
                                                .replace("$CurrentTabName", Common.activeTabName));
                                            return;
                                        }
                                    });
                                $("#divR2T4Input, #divR2T4Result, #divApproveTermination")
                                    .removeClass("is-active is-visited is-invalid");
                                $("#liTerminationDetails, #divTerminationDetail").addClass("is-active")
                                    .removeClass("is-visited");
                                $("#liR2T4Input").removeClass("is-active is-invalid").addClass("is-visited");
                            }
                        } else {
                            this.saveOrUpdateR2T4InputDetails(false,
                                (response) => {
                                    if (response["resultStatus"] !== AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                        this.getTabName();
                                        MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL
                                            .replace("$CurrentTabName", Common.activeTabName)
                                            .replace("$CurrentTabName", Common.activeTabName));
                                        return;
                                    }
                                });
                            $("#divR2T4Input, #divR2T4Result, #divApproveTermination")
                                .removeClass("is-active is-visited is-invalid");
                            $("#liTerminationDetails, #divTerminationDetail").addClass("is-active")
                                .removeClass("is-visited");
                            $("#liR2T4Input").removeClass("is-active is-visited is-invalid");
                        }
                    });
                    Common.isR2T4InputTabDisabled = false;
                }
            } else if ($("#liR2T4Result").hasClass("is-active")) {
                if (this.r2T4ResultsVm.validate()) {
                    if ($("#ChkOverride").is(':checked')) {
                        if ($("#txtTicket").val() === '') {
                            MasterPage.SHOW_INFO_WINDOW(AD.X_MESSAGE_R2T4Result_TICKET_NO_REQUIRED);
                            return;
                        }
                    }
                    if (this.r2T4ResultsVm.validate()) {
                        if ($("#ChkOverride").is(":checked")) {
                            if ($("#txtTicket").val() === '') {
                                MasterPage.SHOW_INFO_WINDOW(AD.X_MESSAGE_R2T4Result_TICKET_NO_REQUIRED);
                                return;
                            }
                        }
                        if ($("#ChkOverride").is(':checked')) {
                            Utility.checkMessagesForOverride(false,
                                (response) => {
                                    if (response) {
                                        this.saveR2T4ResultDetails();
                                        $("#divR2T4Input, #divR2T4Result, #divApproveTermination")
                                            .removeClass("is-active is-visited is-invalid");
                                        $("#liTerminationDetails, #divTerminationDetail").addClass("is-active")
                                            .removeClass("is-visited");
                                        if (Common.r2T4CompletionStatus.isR2T4ResultsCompleted ||
                                            Common.isR2T4ResultsCompleted) {
                                            $("#liR2T4Result").removeClass("is-active is-invalid")
                                                .addClass("is-visited");
                                        } else {
                                            $("#liR2T4Result").removeClass("is-visited is-invalid is-active");
                                        }
                                    } else {
                                        $("#divR2T4Input, #divR2T4Result, #divApproveTermination")
                                            .removeClass("is-active is-visited is-invalid");
                                        $("#liTerminationDetails, #divTerminationDetail").addClass("is-active")
                                            .removeClass("is-visited");
                                        if (Common.r2T4CompletionStatus.isR2T4ResultsCompleted ||
                                            Common.isR2T4ResultsCompleted) {
                                            $("#liR2T4Result").removeClass("is-active is-invalid")
                                                .addClass("is-visited");
                                        } else {
                                            $("#liR2T4Result").removeClass("is-visited is-invalid is-active");
                                        }
                                    }

                                });
                        } else {
                            this.saveR2T4ResultDetails();
                            $("#divR2T4Input, #divR2T4Result, #divApproveTermination")
                                .removeClass("is-active is-visited is-invalid");
                            $("#liTerminationDetails, #divTerminationDetail").addClass("is-active")
                                .removeClass("is-visited");
                            if (Common.r2T4CompletionStatus.isR2T4ResultsCompleted || Common.isR2T4ResultsCompleted) {
                                $("#liR2T4Result").removeClass("is-active is-invalid").addClass("is-visited");
                            } else {
                                $("#liR2T4Result").removeClass("is-visited is-invalid is-active");
                            }
                        }

                    }

                    $("#liR2T4Input").removeClass("is-active is-invalid").addClass("is-visited");
                }
            } else if ($("#liApproveTermination").hasClass("is-active")) {
                if ($("#chkClaculationPeriod").prop("checked")) {
                    $("#liR2T4Input, #liR2T4Result").removeClass("is-active is-invalid")
                        .addClass("is-visited");
                } else {
                    $("#liR2T4Input, #liR2T4Result").removeClass("is-active is-visited").addClass("is-invalid");
                }
                $("#divR2T4Input, #divR2T4Result, #divApproveTermination")
                    .removeClass("is-active is-visited is-invalid");
                $("#liTerminationDetails, #divTerminationDetail").addClass("is-active").removeClass("is-visited");
                $("#liApproveTermination").removeClass("is-active");
            }
        }

        //this function is to set tabs if discard changes in input tab
        private discardInputModifyChanges(): void {
            $("#divTerminationDetail, #liTerminationDetails").addClass("is-active")
                .removeClass("is-visited");
            $("#divR2T4Input, #divR2T4Result, #divApproveTermination").removeClass("is-active is-visited is-invalid");
            $("#liR2T4Input").removeClass("is-active is-invalid").addClass("is-visited");
            Common.isR2T4ApproveTabDisabled = true;
            this.r2T4ResultsVm.isInputModified = false;
        }

        private saveR2T4ResultDetails(): void {
            let r2T4ResultModel = {} as iR2T4Results;
            let r2T4ResultDetail: iR2T4Results = this.r2T4ResultsVm.generateR2T4ResultModel(r2T4ResultModel);
            Utility.isSupportUser(
                (response) => {
                    if (!!response) {
                        if ($("#ChkOverride").is(':checked')) {
                            r2T4ResultDetail.isR2T4OverrideResultsCompleted =
                                Common.r2T4CompletionStatus.isR2T4OverrideResultsCompleted;
                            this.r2T4ResultsVm.saveOrUpdateOverrideResults(r2T4ResultDetail, false, () => {});
                        } else {
                            r2T4ResultDetail.isR2T4ResultsCompleted =
                                Common.r2T4CompletionStatus.isR2T4ResultsCompleted;
                            this.r2T4ResultsVm.saveOrUpdateR2T4Results(r2T4ResultDetail, false,false, () => {});
                        }
                    } else {
                        r2T4ResultDetail.isR2T4ResultsCompleted = Common.r2T4CompletionStatus.isR2T4ResultsCompleted;
                        this.r2T4ResultsVm.saveOrUpdateR2T4Results(r2T4ResultDetail, false,false, () => {});
                    }
                    Common.isR2T4ResultTabDisabled = false;
                });
        }

        private prepareR2T4Input(terminationId: string): void {
            this.r2T4Panel.data("kendoPanelBar").expand($(".k-state-active"), false);
            this.changeElementStyle("divTerminationDetail", "is-active", false);
            this.changeElementStyle("liTerminationDetails", "is-active", false);
            this.changeElementStyle("divR2T4Input", "is-active", true);
            this.changeElementStyle("liR2T4Input", "is-active", true);
            $("#liTerminationDetails").removeClass("is-active").addClass("is-visited");
            $("#liR2T4Input").addClass("is-active");
            $("#liR2T4Input, #liR2T4Result").removeClass("is-invalid");
            $("#liR2T4Input").addClass("is-active").removeClass("is-invalid");
            $("#divApproveTermination, #liApproveTermination").removeClass("is-active").removeClass("is-visited");
            $("#chkTuitionCharged").prop("disabled", false).prop("checked", false);
            (document.getElementById("txtcreditBalanceRefunded") as HTMLInputElement).disabled = false;
            $("#txtcreditBalanceRefunded").removeClass("txtDisable");
            this.viewModel.bindR2T4InputData(terminationId);
        }

        private prepareR2T4Results(r2T4InputDetail, updateResults): void {
            this.r2T4PanelResult.data("kendoPanelBar").expand($(".k-state-active"), false);
            this.changeElementStyle("divTerminationDetail", "is-active", false);
            this.changeElementStyle("liTerminationDetails", "is-active", false);
            this.changeElementStyle("divR2T4Input", "is-active", false);
            this.changeElementStyle("liR2T4Input", "is-active", false);
            this.changeElementStyle("TabApproveTermination", "is-active", false);
            this.changeElementStyle("divR2T4Input", "is-active", false);
            this.changeElementStyle("liR2T4Input", "is-active", false);
            $("#liTerminationDetails, #liR2T4Input").removeClass("is-active").addClass("is-visited");
            $("#liR2T4Result, #divR2T4Result").removeClass("is-visited").addClass("is-active");
            $("#divApproveTermination, #liApproveTermination").removeClass("is-active").removeClass("is-visited");
            this.viewModel.initializeR2T4Result(r2T4InputDetail, this.r2T4ResultsVm.isInputModified, updateResults);
        }

        private prepareNewR2T4Results(r2T4InputDetail, isSaveR2T4Results, resultCompleted): void {
            this.r2T4PanelResult.data("kendoPanelBar").expand($(".k-state-active"), false);
            this.changeElementStyle("divTerminationDetail", "is-active", false);
            this.changeElementStyle("TabApproveTermination", "is-active", false);
            this.changeElementStyle("divR2T4Input", "is-active", false);
            this.changeElementStyle("liR2T4Input", "is-active", false);
            $("#liTerminationDetails, #liR2T4Input").removeClass("is-active").addClass("is-visited");
            $("#liR2T4Result, #divR2T4Result").removeClass("is-visited").addClass("is-active");
            $("#divApproveTermination, #liApproveTermination").removeClass("is-active").removeClass("is-visited");
            this.viewModel.initializeNewR2T4Result(r2T4InputDetail,this.r2T4ResultsVm.isInputModified,isSaveR2T4Results,resultCompleted);
        }

        private changeElementStyle(elementId: string, className: string, isRemove: boolean): void {
            var divTerminationDetail = document.getElementById(elementId) as HTMLElement;
            if (isRemove === false) {
                divTerminationDetail.classList.remove(className);
            } else {
                divTerminationDetail.classList.add(className);
            }
        }

        private generateR2T4TerminationModel(terminationModel: any): any {
            let isNewTermination: boolean =
                $("#hdnTerminationId").val() === undefined || $("#hdnTerminationId").val() === "";
            terminationModel.studentEnrollmentId = $("input[name='studentEnrollment']:checked").val();
            terminationModel.statusCodeId = $("#ddlStatus").val() === "" ? null : $("#ddlStatus").val();
            terminationModel.dateWithdrawalDetermined = $("#dtDateOfDetermination").val() === ""
                ? null
                : new Date($("#dtDateOfDetermination").val()).toDateString();
            terminationModel.lastDateAttended = $("#dtLastDateAttended").val() === ""
                ? null
                : new Date($("#dtLastDateAttended").val()).toDateString();
            terminationModel.calculationPeriodType = Common.periodTypeId = $("input[id='chkClaculationPeriod']").prop('checked')
                ? $("input[name='periodName']:checked")[0].id
                : null;
            terminationModel.dropReasonId = $("#ddlDropReason").val() === "" ? null : $("#ddlDropReason").val();
            terminationModel.isPerformingR2T4Calculator = $("input[id='chkClaculationPeriod']").prop('checked');
            terminationModel.terminationId = isNewTermination ? this.emptyGuid : $("#hdnTerminationId").val();
            terminationModel.createdBy = terminationModel.updatedBy = $("#hdnUserId").val();
            return terminationModel;
        }

        //generates a R2T4 Input model with data from UI component.
        private generateR2T4InputModel(r2T4InputModel: Models.IR2T4Input): any {
            let isNewTermination = $("#hdnTerminationId").val() === undefined || $("#hdnTerminationId").val() === "";
            let isNewR2T4InputId = $("#hdnR2T4InputId").val() === undefined || $("#hdnR2T4InputId").val() === "";

            r2T4InputModel.r2T4InputId = isNewR2T4InputId ? this.emptyGuid : $("#hdnR2T4InputId").val();
            r2T4InputModel.terminationId = isNewTermination ? this.emptyGuid : $("#hdnTerminationId").val();
            r2T4InputModel.programUnitTypeId = this.viewModel.programUnitTypeId;

            r2T4InputModel.pellGrantDisbursed = $("#txtPellGrantDisbursed").val();
            r2T4InputModel.pellGrantCouldDisbursed = $("#txtPellGrantCouldDisbursed").val();

            r2T4InputModel.fseogDisbursed = $("#txtFseogDisbursed").val();
            r2T4InputModel.fseogCouldDisbursed = $("#txtFseogCouldDisbursed").val();

            r2T4InputModel.teachGrantDisbursed = $("#txtTeachGrantDisbursed").val();
            r2T4InputModel.teachGrantCouldDisbursed = $("#txtTeachGrantCouldDisbursed").val();

            r2T4InputModel.iraqAfgGrantDisbursed = $("#txtIraqAfgGrantDisbursed").val();
            r2T4InputModel.iraqAfgGrantCouldDisbursed = $("#txtIraqAfgGrantCouldDisbursed").val();

            r2T4InputModel.unsubLoanNetAmountDisbursed = $("#txtUnsubLoanNetAmountDisbursed").val();
            r2T4InputModel.unsubLoanNetAmountCouldDisbursed = $("#txtUnsubLoanNetAmountCouldDisbursed").val();

            r2T4InputModel.subLoanNetAmountDisbursed = $("#txtSubLoanNetAmountDisbursed").val();
            r2T4InputModel.subLoanNetAmountCouldDisbursed = $("#txtSubLoanNetAmountCouldDisbursed").val();

            r2T4InputModel.perkinsLoanDisbursed = $("#txtPerkinsLoanDisbursed").val();
            r2T4InputModel.perkinsLoanCouldDisbursed = $("#txtPerkinsLoanCouldDisbursed").val();

            r2T4InputModel.directGraduatePlusLoanDisbursed = $("#txtDirectGraduatePlusLoanDisbursed").val();
            r2T4InputModel.directGraduatePlusLoanCouldDisbursed = $("#txtDirectGraduatePlusLoanCouldDisbursed").val();

            r2T4InputModel.directParentPlusLoanDisbursed = $("#txtDirectParentPlusLoanDisbursed").val();
            r2T4InputModel.directParentPlusLoanCouldDisbursed = $("#txtDirectParentPlusLoanCouldDisbursed").val();

            r2T4InputModel.withdrawalDate = $("#dtWithdrawDate").val() === "" ? null : $("#dtWithdrawDate").val();

            if (this.viewModel.programUnitTypeId !== AD.ProgramUnitTypes.ClockHour) {
                r2T4InputModel.startDate = $("#dtStartDate").val() === "" ? null : $("#dtStartDate").val();
                r2T4InputModel.scheduledEndDate = $("#dtEndDate").val() === "" ? null : $("#dtEndDate").val();
            } else {
                r2T4InputModel.startDate = null;
                r2T4InputModel.scheduledEndDate = null;
            }

            if (r2T4InputModel.createdBy === undefined ||
                r2T4InputModel.createdBy === null ||
                r2T4InputModel.createdBy === "") {
                r2T4InputModel.createdBy = $("#hdnUserId").val();
                r2T4InputModel.updatedBy = $("#hdnUserId").val();
            } else {
                r2T4InputModel.updatedBy = $("#hdnUserId").val();
            }

            //Not required to take attendance
            r2T4InputModel.isAttendanceNotRequired = $("#chkAttendance").prop("checked");
            r2T4InputModel.completedTime = $("#txtScheduledHours").val();
            r2T4InputModel.totalTime = $("#txtTotalHours").val();
            //Institutional charges posted
            let calculationPeriod: any = $("input[name='periodName']:checked")[0];
            let isPaymentPeriod = $("label[for=" + calculationPeriod.id + "]").text().split(" ").join("") === Common.CalculationPeriodType[Common.CalculationPeriodType.PaymentPeriod];
            r2T4InputModel.isTuitionChargedByPaymentPeriod = isPaymentPeriod ? !($("#chkTuitionCharged").prop("checked")) : null;
            r2T4InputModel.creditBalanceRefunded = $("#txtcreditBalanceRefunded").val();
            r2T4InputModel.tuitionFee = $("#txtTuitionFee").val();
            r2T4InputModel.roomFee = $("#txtRoomFee").val();
            r2T4InputModel.boardFee = $("#txtBoardFee").val();
            r2T4InputModel.otherFee = $("#txtOtherFee").val();
            r2T4InputModel.paymentType = $("label[for=" + $("input[name='periodName']:checked")[0].id + "]").text().split(" ").join("") === Common.CalculationPeriodType[Common.CalculationPeriodType.PaymentPeriod] ? 1 : 2;

            return r2T4InputModel;
        }

        private saveOrUpdateTerminationDetails(isNavigationToLastTab: boolean = false,
            isNavigationToResultsTab: boolean = false): void {
           this.studentValidator = MasterPage.ADVANTAGE_VALIDATOR("divTerminationDetail");
            if (this.studentValidator.validate()) {
                if (this.viewModel.dateValidation && this.viewModel.isValidDod) {
                let terminationData = this.generateR2T4TerminationModel(this.terminationModel);
                if (terminationData.studentEnrollmentId === null ||
                    terminationData.studentEnrollmentId === undefined) {
                    MasterPage.SHOW_WARNING_WINDOW(X_PLEASE_FILL_REQUIRED_FIELDS);
                } else {
                    terminationData.isR2T4ApproveTabEnabled = Common.isNonR2T4ApproveTabEnabled;
                    if (terminationData.terminationId !== this.emptyGuid) {
                        this.terminationId = terminationData.terminationId;
                        terminationData.methodType = Number(Common.MethodType.Others);

                        this.r2T4Termination.updateTermination(terminationData,
                            (response) => {
                                if (response["resultStatus"] === AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                    this.terminationId = response["terminationId"];
                                    $("#hdnTerminationId").val(this.terminationId);
                                    if (isNavigationToLastTab) {
                                        if ($("#chkClaculationPeriod").prop("checked")) {
                                            this.r2T4ResultsVm.initializeApproveTermination();
                                            Common.isR2T4InputTabDisabled = Common.isR2T4ResultTabDisabled =
                                                Common.isR2T4ApproveTabDisabled = false;
                                        } else {
                                            this.approveTerminationVm =
                                                new ApproveTerminationVm(this.viewModel, true);
                                            this.approveTerminationVm.prepareApproveTermination(
                                                !(document.getElementById("chkClaculationPeriod") as
                                                    HTMLInputElement).checked);
                                            $("#liR2T4Result, #liR2T4Input").removeClass("is-invalid is-visited")
                                                .addClass("is-invalid");
                                        }
                                    } else {
                                        if (!isNavigationToResultsTab) {
                                            this.onSaveOrUpdateCompletion();
                                            $("#liR2T4Input").addClass("is-active").removeClass("is-visited");
                                            $("#liTerminationDetails, #liR2T4Result").removeClass("is-active");
                                        } else {
                                            this.r2T4Termination.fetchR2T4Inputdetails(this.terminationId,
                                                (r2T4InputDetail) => {
                                                    if (!!r2T4InputDetail) {
                                                        this.prepareR2T4Results(r2T4InputDetail, false);
                                                    }
                                                });
                                        }
                                    }
                                } else {
                                    this.getTabName();
                                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL
                                        .replace("$CurrentTabName", Common.activeTabName)
                                        .replace("$CurrentTabName", Common.activeTabName));
                                    return;
                                }
                            });
                    } else {
                        terminationData.methodType = Number(Common.MethodType.Others);
                        this.r2T4Termination.saveTermination(terminationData,
                            (response) => {
                                if (response["resultStatus"] === AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                    this.terminationId = response["terminationId"];
                                    $("#hdnTerminationId").val(this.terminationId);
                                    if (isNavigationToLastTab) {
                                        if ($("#chkClaculationPeriod").prop("checked")) {
                                            this.r2T4ResultsVm.initializeApproveTermination();
                                            Common.isR2T4InputTabDisabled = Common.isR2T4ResultTabDisabled =
                                                Common.isR2T4ApproveTabDisabled = false;
                                        } else {
                                            this.approveTerminationVm =
                                                new ApproveTerminationVm(this.viewModel, true);
                                            this.approveTerminationVm.prepareApproveTermination(
                                                !(document.getElementById("chkClaculationPeriod") as
                                                    HTMLInputElement).checked);
                                            $("#liR2T4Result, #liR2T4Input").removeClass("is-invalid is-visited")
                                                .addClass("is-invalid");
                                        }
                                    } else {
                                        if (!isNavigationToResultsTab) {
                                            this.onSaveOrUpdateCompletion();
                                            $("#liR2T4Input").addClass("is-active").removeClass("is-visited");
                                            $("#liTerminationDetails, #liR2T4Result").removeClass("is-active");
                                            Common.isR2T4InputTabDisabled = false;
                                        } else {
                                            this.r2T4Termination.fetchR2T4Inputdetails(this.terminationId,
                                                (r2T4InputDetail) => {
                                                    if (!!r2T4InputDetail) {
                                                        this.prepareR2T4Results(r2T4InputDetail, false);
                                                    }
                                                });
                                        }
                                    }
                                } else {
                                    this.getTabName();
                                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL
                                        .replace("$CurrentTabName", Common.activeTabName)
                                        .replace("$CurrentTabName", Common.activeTabName));
                                    return;
                                }
                            });
                    }
                    Common.isR2T4InputTabDisabled = false;
                    $("#liR2T4Input").addClass("is-active").removeClass("is-visited");
                    $("#liTerminationDetails, #liR2T4Result").removeClass("is-active");
                }
            }
        } else {
                    MasterPage.SHOW_WARNING_WINDOW(X_PLEASE_FILL_REQUIRED_FIELDS);
                }
                this.studentValidator.destroy();
                this.studentValidator = MasterPage.ADVANTAGE_VALIDATOR("divTerminationDetail", false);
         }

        //This function is to delete R2T4Results data, if user modify any/all R2T4Input data and navigate back to Termination Details tab.
        private deleteR2T4ResultData(callback : (rersponse: Response)=> any) {
            this.terminationId = $("#hdnTerminationId").val();
            if (document.location.href !== null) {
                let termination = { "terminationId": this.terminationId };
                    new StudentTerminationApi().deleteR2T4ResultByTerminationId(termination,
                        (result) => {
                            let resultMessage = result["message"];
                            Common.isR2T4ResultsExists = Common.isR2T4OverrideResultsExists = false;
                            if (resultMessage === AD.X_MESAGE_TERMINATION_DELETE_SUCCESS) {
                                $("#divR2T4Result, #divApproveTermination").removeClass("is-active")
                                    .removeClass("is-visited").addClass("is-invalid");
                                callback(result);
                            } else {
                                MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_RESULT_DELETE_FAIL);
                            }
                        });
                }
        }

      private confirmNotUpdateInput() {
            this.r2T4ResultsVm.initializeApproveTermination();
            $("#divApproveTermination, #liApproveTermination").addClass("is-active")
                .removeClass("is-visited");
            $("#divTerminationDetail,#divR2T4Input, #divR2T4Result")
                .removeClass("is-active is-invalid").addClass("is-visited");
        }

        private saveInputAfterModify(isSaveButton: boolean = false) {
            //if click on Yes on confirmation message then update the modified R2T4 input values and recalculate the R2T4Results and display on R2T4 Results tab.
            this.deleteR2T4ResultData((responseCallback)=>
            {
                let resultMessage = responseCallback["message"];
                if (resultMessage === AD.X_MESAGE_TERMINATION_DELETE_SUCCESS) {
                    Common.r2T4CompletionStatus.isR2T4InputCompleted = false;
                    Common.r2T4CompletionStatus.isR2T4ResultsCompleted = false;
                    this.saveOrUpdateR2T4InputDetails(false, (response) => { 
                        if (response["resultStatus"] !== AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                            this.getTabName();
                            MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL
                                .replace("$CurrentTabName", Common.activeTabName)
                                .replace("$CurrentTabName", Common.activeTabName));
                            return;
                        } else {
                            if (isSaveButton) {
                                $("#divR2T4Input, #liR2T4Input").addClass("is-active");
                            } else {
                                this.clearInput();
                            }
                        }
                    });
                }
            });
            Common.isR2T4ResultTabDisabled = true;
            Common.isR2T4ApproveTabDisabled = true;
            $("#divR2T4Result,#liR2T4Result,#liApproveTermination, #divApproveTermination,#divR2T4Input, #liR2T4Input")
                .removeClass("is-active is-visited is-invalid");
        }

        private clearInput() {
            $("#divR2T4Input").find("input:text").each(
                (index, element) => {
                    if ($(element).val() > 0) {
                        $(element).val("");
                    }
                });
            $("#divR2T4Input").find("input:checkbox").each(
                (index, element) => {
                    if ($(element).prop("checked") === true) {
                        $(element).prop("checked", false);
                    }
                });
            $("#divTerminationDetail, #liTerminationDetails").addClass("is-active").removeClass("is-visited");
        }
        
      //Method To get the current active tab name.  
        public getTabName() {
            if ($("#liTerminationDetails").hasClass("is-active")) {
                Common.activeTabName = "TerminationDetails";
            } else if ($("#liR2T4Input").hasClass("is-active")) {
                Common.activeTabName = "R2T4Input";
            } else if ($("#liR2T4Result").hasClass("is-active")) {
                Common.activeTabName = "R2T4Result";
            } else if ($("#liApproveTermination").hasClass("is-active")) {
                Common.activeTabName = "ApproveTermination";
            }
        }

        //this method is to validate and display the confirmation messages if Input is modified
        private checkIsInputModified(isSaveButton: boolean = false): void {
            this.r2T4ResultsVm.isOverrideResultsExists((resultOverride) => {
                if (!!resultOverride && resultOverride !== this.emptyGuid) {
                    this.r2T4Termination.getSavedR2T4Results($("#hdnTerminationId").val(),
                        (response: any) => {
                            if (response.isInputIncluded) {
                                if (Common.r2T4CompletionStatus.isR2T4ResultsCompleted ||
                                    Common.r2T4CompletionStatus.isR2T4OverrideResultsCompleted) {
                                    let textMessage = Common.r2T4InputModifyOverride;
                                    this.showPopupToConfirmUpdateInputOrNot(textMessage, isSaveButton);
                                } else {
                                    let textMessage = Common.r2T4InputModifyOverrideResults;
                                    this.showPopupToConfirmUpdateInputOrNot(textMessage, isSaveButton);
                                }
                            } else {
                                if (Common.r2T4CompletionStatus.isR2T4ResultsCompleted ||
                                    Common.r2T4CompletionStatus.isR2T4OverrideResultsCompleted) {
                                    let textMessage = Common.r2T4InputChangeBackOrTab;
                                    this.showPopupToConfirmUpdateInputOrNot(textMessage, isSaveButton);
                                } else {
                                    let textMessage = Common.r2T4InputChangeBack;
                                    this.showPopupToConfirmUpdateInputOrNot(textMessage, isSaveButton);
                                }
                            }
                        });
                } else {
                    if (isSaveButton) {
                        if (Common.r2T4CompletionStatus.isR2T4ResultsCompleted ||
                            Common.r2T4CompletionStatus.isR2T4OverrideResultsCompleted) {
                            let textMessage = Common.r2T4InputChangeBackOrTab;
                            this.showPopupToConfirmUpdateInputOrNot(textMessage, isSaveButton);
                        } else {
                            let textMessage = Common.r2T4InputChangeBack;
                            this.showPopupToConfirmUpdateInputOrNot(textMessage, isSaveButton);
                        }
                    }
                    else if (!isSaveButton) {
                        if (Common.r2T4CompletionStatus.isR2T4ResultsCompleted || Common.r2T4CompletionStatus.isR2T4OverrideResultsCompleted) {
                            let textMessage = Common.r2T4InputChangeBackOrTab;
                            this.showPopupToConfirmUpdateInputOrNot(textMessage, isSaveButton);
                           } else {
                            $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(Common.r2T4InputChangeBack))
                                .then(confirmed => {
                                    if (confirmed) {
                                       this.saveInputAfterModify();
                                    }
                                    else {
                                        this.discardInputModifyChanges();
                                    }
                                    $("#divR2T4Input, #divR2T4Result, #divApproveTermination")
                                        .removeClass("is-active is-visited is-invalid");
                                    $("#liTerminationDetails, #divTerminationDetail").addClass("is-active")
                                        .removeClass("is-visited");
                                });
                        } 
                    }
                 }
            });
        }

        //This function is to show the popup and do the actions according to the confirmation
        private showPopupToConfirmUpdateInputOrNot(textMessage: string, isSaveButton: boolean = false) {
            if (isSaveButton) {
                $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(textMessage))
                    .then(confirmed => {
                        if (confirmed) {
                            this.saveInputAfterModify(true);
                        } else {
                            this.viewModel.bindR2T4InputData($("#hdnTerminationId").val());
                            Common.isR2T4ResultTabDisabled = false;
                            Common.isR2T4ApproveTabDisabled = false;
                            this.r2T4ResultsVm.isInputModified = false;
                        }
                    });
            } else if (!isSaveButton) {
                if (textMessage === Common.r2T4InputModifyOverride ||textMessage === Common.r2T4InputChangeBackOrTab ) {
                    $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(textMessage))
                        .then(confirmed => {
                            if (confirmed) {
                                this.saveInputAfterModify();
                            } else {
                                this.navigatebackAfterInputModify();
                            }
                        });
                } else if (textMessage === Common.r2T4InputModifyOverrideResults ||textMessage === Common.r2T4InputChangeBack) {
                    $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(textMessage))
                        .then(confirmed => {
                            if (confirmed) {
                               this.saveInputAfterModify();
                            } else {
                                this.discardInputModifyChanges();
                            }
                        });
                }
             }
        }

        //this function is to validate whether to recalculate or not Result data when modify input and go forward.
        private isRecalculateResult(isApproveTerminationTab : boolean = false) {
                        this.r2T4ResultsVm.isOverrideResultsExists((resultOverride) => {
                            if (!!resultOverride && resultOverride !== this.emptyGuid) {
                                this.r2T4Termination.getSavedR2T4Results($("#hdnTerminationId").val(),
                                    (response: any) => {
                                        if (response.isInputIncluded) {
                                            if (!isApproveTerminationTab) {
                                    if (Common.r2T4CompletionStatus.isR2T4ResultsCompleted || Common.r2T4CompletionStatus.isR2T4OverrideResultsCompleted) { 
                                    let message = Common.r2T4InputModifyApproveTermination;
                                    this.showPopupToConfirmRecalculateResultOrNot(message, isApproveTerminationTab);
                                }
					            else{
                                    let message = Common.r2T4InputModifyOnlyResults;
                                    this.showPopupToConfirmRecalculateResultOrNot(message, isApproveTerminationTab);
                                }
                            }
                            else if (isApproveTerminationTab) {
                                let message = Common.r2T4InputModifyApproveTermination;
                                this.showPopupToConfirmRecalculateResultOrNot(message, isApproveTerminationTab);
                            }
                        }
			            else{
                             if(!isApproveTerminationTab){
                            if (Common.r2T4CompletionStatus.isR2T4ResultsCompleted ||
                                Common.r2T4CompletionStatus.isR2T4OverrideResultsCompleted) {
                                let message = Common.r2T4InputModifyApproveTerminationTab;
                                this.showPopupToConfirmRecalculateResultOrNot(message, isApproveTerminationTab);
                            }
                            else {
                                let message = Common.r2T4InputChangeNext;
                                this.showPopupToConfirmRecalculateResultOrNot(message, isApproveTerminationTab);
                            }
                        }
			                else if (isApproveTerminationTab) {
                            let message = Common.r2T4InputModifyApproveTerminationTab;
                            this.showPopupToConfirmRecalculateResultOrNot(message, isApproveTerminationTab);
                        }
                    }
                });
                }
	            else{
                if (!isApproveTerminationTab) {
                    if (Common.r2T4CompletionStatus.isR2T4ResultsCompleted ||
                        Common.r2T4CompletionStatus.isR2T4OverrideResultsCompleted) {
                        let message = Common.r2T4InputChangeNextAfterApproveTerm;
                        this.showPopupToConfirmRecalculateResultOrNot(message, isApproveTerminationTab);
                    }
                    else {
                        this.messageIfReachedTillResultOnly(Common.r2T4InputChangeNext);
                    }
                }
                else if (isApproveTerminationTab) {
                    let message = Common.r2T4InputModifyApproveTerminationTab;
                    this.showPopupToConfirmRecalculateResultOrNot(message, isApproveTerminationTab);
                }
            }
         });
        }

        //This function is to show the popup and do the actions according to the confirmation.
        private showPopupToConfirmRecalculateResultOrNot(textMessage: string, isApproveTerminationTab){
        let r2T4InputDetail: Models.IR2T4Input = this.generateR2T4InputModel(this.r2T4InputModel);
        if (textMessage === Common.r2T4InputModifyApproveTermination || textMessage === Common.r2T4InputModifyApproveTerminationTab || textMessage === Common.r2T4InputChangeNextAfterApproveTerm) {
        $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(textMessage))
            .then(confirmed => {
                //if click on yes on confirmation message then update the modified R2T4 input values and recalculate the R2T4Results and display on R2T4 Results tab.
                if (confirmed) {
                    this.saveOrUpdateR2T4InputDetails(false,
                        (response) => {
                            if (response["resultStatus"] ===
                                AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                this.prepareNewR2T4Results(r2T4InputDetail, true, true);
                            } else {
                                this.getTabName();
                                MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL.replace("$CurrentTabName", Common.activeTabName)
                                    .replace("$CurrentTabName", Common.activeTabName));
                                return;
                            }
                        });
                    } else {
                    if (!isApproveTerminationTab) {
                        if (!!r2T4InputDetail &&
                            !!r2T4InputDetail.r2T4InputId &&
                            r2T4InputDetail.r2T4InputId !== this.emptyGuid) {
                            this.r2T4ResultsVm.isInputModified = false;
                            this.prepareNewR2T4Results(r2T4InputDetail, false, true);
                        }
                    }
                    else if (isApproveTerminationTab) {
                        this.confirmNotUpdateInput();
                    }
                }
            });
            }
            else if (textMessage === Common.r2T4InputModifyOnlyResults || textMessage === Common.r2T4InputChangeNext) {
            this.messageIfReachedTillResultOnly(textMessage);
            }
        }

        private messageIfReachedTillResultOnly(textMessage : string){
            let r2T4InputDetail: Models.IR2T4Input = this.generateR2T4InputModel(this.r2T4InputModel);
            $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(textMessage))
                .then(confirmed => {
                //if click on yes on confirmation message then update the modified R2T4 input values and recalculate the R2T4Results and display on R2T4 Results tab.
                if (confirmed) {
                this.saveOrUpdateR2T4InputDetails(false,
                    (response) => {
                        if (response["resultStatus"] ===
                            AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                            this.prepareNewR2T4Results(r2T4InputDetail, true, false);
                        } else {
                            this.getTabName();
                            MasterPage.SHOW_ERROR_WINDOW(AD
                                .X_MESAGE_SAVE_INPUT_NEXT_FAIL
                                .replace("$CurrentTabName", Common.activeTabName)
                                .replace("$CurrentTabName", Common.activeTabName));
                            return;
                        }
                    });

            } else {
                if (!!r2T4InputDetail && !!r2T4InputDetail.r2T4InputId &&
                    r2T4InputDetail.r2T4InputId !== this.emptyGuid) {
                    this.r2T4ResultsVm.isInputModified = false;
                    this.prepareNewR2T4Results(r2T4InputDetail, false, false);
                }
            }
        });
        }

        // This function is used show/hide the 'Tuition charged by payment period' checkbox based on the selection of calculation period type.
        private showTuitionChargedCheckbox(): void {
            let calculationPeriod: any = $("input[name='periodName']:checked")[0];
            if (!!calculationPeriod) {
                let displayTuitionCharged = $("label[for=" + calculationPeriod.id + "]").text().split(" ").join("") === Common.CalculationPeriodType[Common.CalculationPeriodType.PaymentPeriod] ? "block" : "none";
                $("#dvTuitionCharged").css("display", displayTuitionCharged);
                let info = MasterPage.Common.detectNavigator().toLowerCase();
                let infoarray = info.split(" ");
                if (infoarray[0] !== "firefox")
                    $("#tblInstitutionalChargesPosted").removeClass("table-up-spacing"); 
            }
        }
    }
}