﻿/// <reference path="../r2t4results/R2T4ResultsVM.ts" />
/// <reference path="../../../../fame.advantage.client.ad/textmessages.ts" />
/// <reference path="../../../api/academicrecords/programversions.ts" />

module API.ViewModels {
    import r2t4ResultsVM = ViewModels.AcademicRecords.R2T4Results.R2T4ResultsVm;
    import StudentDropReason = Api.Components.SystemCatalog.StudentDropReason;
    import StudentSearchAutoComplete = Api.Components.StudentSearchAutoComplete;
    import StudentEnrollment = Api.Components.StudentEnrollment;
    import CalculationPeriodType = Api.Components.SystemCatalog.R2T4CalculationPeriodType;
    import Enrollments = Api.Enrollments;
    import IStudentEnrollment = Models.IStudentEnrollment;
    import DropStatusCode = Api.Components.DropStatusCodesDropDownList;
    import StudentAcademicCalendar = Api.Components.SystemCatalog.StudentAcademicCalendar;
    import constants = Components.Common;
    import helpDialogBox = MasterPage;
    import IFetchByEnrollId = Models.IFetchByEnrollId;
    import UserRoleStatusCheck = Api.Components.SystemCatalog.UserRoleStatusCheck;
    import ProgramVersions = Api.ProgramVersions;
    let r2T4Termination = new Api.R2T4StudentTermination();

    /**
        * Master View Model for the Student Termination Page.
        * The viewModel will have all the necessary UI business logic and it will connect UI elements to their API request and components.
        * The master viewModel can initialize children viewModels based on workflows needs. For example, the initialize method will set up the first screen only.
        * The second step(tab) can be another view model called StudentTermionationStep2Vm for example, and be called when is needed or be declared as an object to be used
        * inside the StudentTerminationVm itself.
       */
    export class StudentTerminationVm {
        terminationDetailsTab: TerminationDetailsTab;
        campusId: string;
        studentId: string;
        enrollmentId: string;
        enrollmentStartDate: Date;
        lastDateAttended: any;
        ldaFromAttendance: any = undefined;
        dateOfDetermination: any;
        dropReasonId: string;
        statusId: string;
        enrollmentDetails: any;
        studentName: string;
        studentSsn: string;
        calculationPeriodTypeId: string;
        enrollmentName: string;
        studentSearchComponent: StudentSearchAutoComplete;
        studentDropReasonComponent: StudentDropReason;
        studentStatusLoad: Api.Components.StudentTerminationStatusCodeDropdownList;
        studentEnrollment: StudentEnrollment;
        calculationPeriodType: CalculationPeriodType;
        ldaDatePicker: any;
        studentEnrollmentDetails: any;
        studentEnrollmentById: any;
        unitTypeDescription: string;
        public isClockHr: boolean;
        programUnitTypeId: number = AD.ProgramUnitTypes.CreditHour;
        public dateValidation: boolean = true;
        public isValidDod: boolean = true;
        emptyGuid: string = "00000000-0000-0000-0000-000000000000";
        public isPaymentPeriodChecked: boolean;
        private previousEnrollmentId: string;
        private previousStudentName: string;
        private previousProgramName: string;
        private onTextChange: boolean = false;
        isLdaChanged: boolean = false;
        private isDodPopupRequired = undefined;
        private isPopupOpen: boolean = false;
        public resultsVm = new r2t4ResultsVM(this);
        ldaEnabled: boolean = false;
        private previousStudentEnrollmentId: string;
        private campusProgramVersionDetails: any;
        public studentValidator: kendo.ui.Validator;
        isNonTermProgram: boolean = false;

        constructor(campusId: string) {
            this.campusId = campusId;
        }

        public initialize() {
            this.terminationDetailsTab = new TerminationDetailsTab();
            this.initializeStep1();
        }

        private initializeStudentSearch() {
            /**
             * Components are resuable user controls that have built in functionality and encapsulate to a degree certain business logic.
             * It is common to search a student by first name, last name, phone number, SSN or enrollment, because the same logic can be reused
             * across a lot of pages, we need to build the functionality so other pages can reuse the same code. In this case the component
             * Student Search Auto Complete will accept the minium parameteres needed to get out of the box funcionality and get you going with just the Id
             * of the component(form id, input id, or button id), the current campus and one event, the OnChange to be called when a student is selected.
             */
            this.studentSearchComponent = new StudentSearchAutoComplete("txtStudentSearch",
                this.campusId,
                (response) => {
                    $("#liTerminationDetails").addClass("is-active").removeClass("is-visited is-invalid");
                    $("#liR2T4Input, #liR2T4Result, #liApproveTermination").removeClass("is-active is-visited is-invalid");
                    this.initializeStep2(response.value);
                    constants.isR2T4InputTabDisabled = constants.isR2T4ResultTabDisabled = constants.isR2T4ApproveTabDisabled = true;
                    this.studentId = response.value.studentId;
                    this.studentName = response.value.displayName;
                    this.studentSsn = response.value.ssn;
                    constants.studentName = response.value.displayName;
                    (document.getElementById("dvEnrollment") as HTMLDivElement).setAttribute("style", "display:block");
                    $("#spnSave").addClass("disabled");
                    $("#spnCancel").addClass("disabled");
                    (document.getElementById("dvCalculationPeriod") as HTMLDivElement).setAttribute("style", "display:none");
                });
        }
        private initializeBindStudentStatus() {
            let dropStatusLoad = new DropStatusCode("ddlStatus", this.campusId,
                () => {
                    this.statusId = $("#ddlStatus").data("kendoDropDownList").value();
                });
            this.statusId = dropStatusLoad.selectedStatusId;
        }

        private initializeBindDropReason() {
            this.studentDropReasonComponent = new StudentDropReason("ddlDropReason",
                this.campusId,
                ((value) => {
                    if (value !== undefined) {
                        this.dropReasonId = $("#ddlDropReason").data("kendoDropDownList").value();
                    }
                }) as any);
        }

        private initializeCalculationPeriod() {
            $("#chkClaculationPeriod").click(() => {
                this.initializeCalculationPeriodType(() => { });
                this.checkPeriodUsed();
            });
        }

        //get the perform R2T4 checkbox value and displaying period used for calculation radiobutton.
        private checkPeriodUsed() {
            if ((document.getElementById("chkClaculationPeriod") as HTMLInputElement).checked) {
                (document.getElementById("dvCalculationPeriod") as HTMLDivElement).setAttribute("style", "display:block");
                (document.getElementById("periodName") as HTMLDivElement).setAttribute("style", "display:block");
                if ($("#hdnTerminationId").val() !== "" && $("#hdnTerminationId").val() !== this.emptyGuid) {
                    $("#liR2T4Input, #liR2T4Result").removeClass("is-active is-visited is-invalid");
                    this.isR2T4InputExists((response: any) => {
                        constants.isR2T4InputTabDisabled = constants.isR2T4ResultTabDisabled = !!response ? response : !response;
                        $("#liApproveTermination").removeClass("is-active is-invalid");
                    });
                }
            } else {
                (document.getElementById("dvCalculationPeriod") as HTMLDivElement).setAttribute("style", "display:none");
                if (this.isPopupOpen === false && $("#hdnTerminationId").val() !== "" && $("#hdnTerminationId").val() !== this.emptyGuid) {
                    this.isR2T4InputExists(
                        (result: any) => {
                            if (!!result) {
                                this.resultsVm.isResultsExists(
                                    (response) => {
                                        if (!!response) {
                                            if (constants.r2T4CompletionStatus.isR2T4ResultsCompleted || constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted) {
                                                this.deleteR2T4InputData(false,
                                                    AD.X_MESSAGE_R2T4ApproveTerminationCALCULATIONPERIOD_CHANGE);
                                            } else {
                                                this.deleteR2T4InputData(false,
                                                    AD.X_MESSAGE_R2T4ResultCALCULATIONPERIOD_CHANGE);
                                            }
                                        } else {
                                            this.deleteR2T4InputData(false,
                                                AD.X_MESSAGE_R2T4InputCALCULATIONPERIOD_CHANGE);
                                        }
                                    });
                            }
                        });
                }
            }
        }

        //get the calculation period type and binding to radiobutton
        private initializeCalculationPeriodType(onLoadComplete: any) {
            this.isPopupOpen = false;
            this.calculationPeriodType = new CalculationPeriodType("periodName", true, onLoadComplete, this.onCalculationPeriodTypeChange);
        }

        private onCalculationPeriodTypeChange(): any {
            let confirmationMessage = "";
            let r2T4ResultExist = false;

            if (constants.r2T4CompletionStatus.isR2T4ResultsCompleted ||
                constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted) {
                confirmationMessage = AD.X_MESSAGE_AppTermination_CALCULATION_PERIOD_CHANGE;
                r2T4ResultExist = true;
            } else if (constants.r2T4CompletionStatus.isR2T4InputCompleted) {
                confirmationMessage = AD.X_MESSAGE_R2T4Result_CALCULATION_PERIOD_CHANGE;
                r2T4ResultExist = true;
            } else if (constants.isR2T4InputExists && !constants.r2T4CompletionStatus.isR2T4InputCompleted) {
                confirmationMessage = AD.X_MESSAGE_R2T4Input_CALCULATION_PERIOD_CHANGE;
            }

            if (confirmationMessage.length > 0) {
                if (!this.isPopupOpen) {
                    this.isPopupOpen = true;
                    $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(confirmationMessage))
                        .then(confirmed => {
                            if (confirmed) {
                                this.isPopupOpen = false;
                                let terminationId = $("#hdnTerminationId").val();
                                if (constants.isR2T4InputExists) {
                                    r2T4Termination.fetchR2T4Inputdetails(terminationId,
                                        (r2T4InputDetail: any) => {
                                            if (!!r2T4InputDetail) {
                                                let calculationPeriod: any = $("input[name='periodName']:checked")[0];
                                                constants.periodTypeId = calculationPeriod.id;
                                                let isPaymentPeriod = $("label[for=" + calculationPeriod.id + "]").text().split(" ").join("") === constants.CalculationPeriodType[constants.CalculationPeriodType.PaymentPeriod];
                                                r2T4InputDetail.isTuitionChargedByPaymentPeriod = isPaymentPeriod ? r2T4InputDetail.isTuitionChargedByPaymentPeriod : null;
                                                r2T4InputDetail.creditBalanceRefunded = isPaymentPeriod ? r2T4InputDetail.creditBalanceRefunded : null;
                                                r2T4InputDetail.isR2T4InputCompleted = false;
                                                let terminationModel = {} as any;

                                                let isNewTermination: boolean = $("#hdnTerminationId").val() === undefined || $("#hdnTerminationId").val() === "";
                                                terminationModel.studentEnrollmentId = $("input[name='studentEnrollment']:checked").val();
                                                terminationModel.statusCodeId = $("#ddlStatus").val() === "" ? null : $("#ddlStatus").val();
                                                terminationModel.dateWithdrawalDetermined = $("#dtDateOfDetermination").val() === "" ? null : new Date($("#dtDateOfDetermination").val()).toDateString();
                                                terminationModel.lastDateAttended = $("#dtLastDateAttended").val() === "" ? null : new Date($("#dtLastDateAttended").val()).toDateString();
                                                terminationModel.calculationPeriodType = constants.periodTypeId = $("input[id='chkClaculationPeriod']").prop("checked") ? $("input[name='periodName']:checked")[0].id : null;
                                                terminationModel.dropReasonId = $("#ddlDropReason").val() === "" ? null : $("#ddlDropReason").val();
                                                terminationModel.isPerformingR2T4Calculator = $("input[id='chkClaculationPeriod']").prop("checked");
                                                terminationModel.terminationId = isNewTermination ? this.emptyGuid : $("#hdnTerminationId").val();
                                                terminationModel.createdBy = terminationModel.updatedBy = $("#hdnUserId").val();

                                                r2T4Termination.updateTermination(terminationModel,
                                                    () => {
                                                        if (constants.isR2T4InputExists) {
                                                            r2T4Termination.updateR2T4InputDetail(r2T4InputDetail,
                                                                () => {
                                                                    if (r2T4ResultExist) {
                                                                        let termination = { terminationId } as any;
                                                                        new Api.R2T4StudentTermination()
                                                                            .deleteR2T4ResultByTerminationId(
                                                                            termination,
                                                                            () => {
                                                                                $(
                                                                                    "#liR2T4Input, #liR2T4Result, #liApproveTermination")
                                                                                    .removeClass(
                                                                                    "is-active is-visited is-invalid");
                                                                                constants.isR2T4ResultTabDisabled =
                                                                                    constants
                                                                                        .isR2T4ApproveTabDisabled =
                                                                                    true;
                                                                                constants.r2T4CompletionStatus
                                                                                    .isR2T4ResultsCompleted =
                                                                                    constants.r2T4CompletionStatus
                                                                                        .isR2T4OverrideResultsCompleted =
                                                                                    constants.r2T4CompletionStatus
                                                                                        .isR2T4InputCompleted =
                                                                                    constants.isR2T4ResultsExists =
                                                                                    constants
                                                                                        .isR2T4OverrideResultsExists =
                                                                                    false;
                                                                            });
                                                                    }
                                                                });
                                                        }
                                                    });
                                            }
                                        });
                                }
                            } else {
                                this.isPopupOpen = false;
                                let periodRadioList: any = document.getElementsByName("periodName");
                                periodRadioList.forEach(item => {
                                    if (item.id === constants.periodTypeId) {
                                        $(`#${item.id}`).prop("checked", "true");
                                    } else {
                                        $(`#${item.id}`).removeAttr("checked");
                                    }
                                });
                            }
                        });
                }
            }
        }

        private enableDatePicker(componentId: string, flag: boolean): void {
            componentId = "#" + componentId;
            let datepicker = $(componentId).data("kendoDatePicker");
            datepicker.enable(flag);
        }

        //This function will handle the open event for date picker
        //and set the current date as default when there is no date defined
        private createDatePicker(componentId: string) {
            componentId = `#${componentId}`;
            return $(componentId).kendoDatePicker({
                open: (event) => {
                    let element = event.sender as any;
                    if (element.dateView.calendar !== undefined && element.dateView.calendar !== null) {
                        element.dateView.calendar.value($(componentId).val());
                        element.dateView.calendar.navigate($(componentId).val() !== "" ? new Date($(componentId).val()) : new Date(), "month");
                    }
                }
            });
        }

        private initializeDateOfDetermination(): void {
            this.createDatePicker("dtDateOfDetermination");
            $("#dtDateOfDetermination").change(() => {

                if (!this.dateOfDeterminationValidator("#dtDateOfDetermination", $("#dtDateOfDetermination"), this.studentEnrollmentDetails))
                    return;
                if (MasterPage.isValidDate($("#dtDateOfDetermination").val())) {
                    $("#dtDateOfDetermination").val(MasterPage.formatDate($("#dtDateOfDetermination").val()));
                }
                if (this.enableAndDisableLastDateAttended(this.studentEnrollmentDetails))
                    if (this.validateDate($("#dtDateOfDetermination").val()))
                        this.validateDate($("#dtLastDateAttended").val());

                var isLdaEnabled = $("#dtLastDateAttended").prop("disabled");
                if ($('#LDA').text() !== AD.X_DATE_TYPE) {
                    if (isLdaEnabled === false) {
                        var enrollmentDate = new Date(this.studentEnrollmentDetails.enrollmentDate);
                        var enrollStartDate = new Date(this.studentEnrollmentDetails.startDate);
                        var dateOfDetermination = new Date($("#dtDateOfDetermination").val());
                        if (dateOfDetermination < enrollStartDate && dateOfDetermination > enrollmentDate) {
                            (document.getElementById("spLDA") as HTMLSpanElement).textContent = "";
                            this.setComponentAttribute("dtLastDateAttended", "required", "false");
                        }
                    }
                }

                if ($("#dtDateOfDetermination").val() === "") {
                    if (isLdaEnabled === false) {
                        $("#dtLastDateAttended").val("");
                    }
                    MasterPage.destroyTooltip($("#dtDateOfDetermination"));
                }
                if ($("#chkClaculationPeriod").prop("checked") || !this.ldaEnabled) {
                    if (this.isPopupOpen === false &&
                        $("#hdnTerminationId").val() !== "" &&
                        $("#hdnTerminationId").val() !== this.emptyGuid &&
                        $("#dtDateOfDetermination").val() !== this.dateOfDetermination) {
                        if (this.isDodPopupRequired) {
                            if (constants.isR2T4ResultsExists || constants.isR2T4OverrideResultsExists) {
                                if (constants.r2T4CompletionStatus.isR2T4ResultsCompleted || constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted) {
                                    this.saveTerminationOnDodOrLdaChange(false,
                                        AD.X_MESSAGE_R2T4ApproveTermination_DOD_BIGGER_THAN_ENROLLMENTDATE);
                                } else {
                                    this.saveTerminationOnDodOrLdaChange(false,
                                        AD.X_MESSAGE_R2T4Result_DOD_BIGGER_THAN_ENROLLMENTDATE);
                                }
                            }
                        } else if (this.isDodPopupRequired === false || $("#dtDateOfDetermination").val() === "") {
                            if (constants.isR2T4InputExists) {
                                let dodChangePopupMessage = "";
                                if (constants.isR2T4ResultsExists || constants.isR2T4OverrideResultsExists) {
                                    if (constants.r2T4CompletionStatus.isR2T4ResultsCompleted ||
                                        constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted) {
                                        dodChangePopupMessage = $("#dtDateOfDetermination").val() !== "" ? AD.X_MESSAGE_R2T4ApproveTerminationDOD_CHANGE : AD.X_MESSAGE_R2T4ApproveTermination_BLANK_DOD;
                                        this.deleteR2T4InputData(true, dodChangePopupMessage);
                                    } else {
                                        dodChangePopupMessage = $("#dtDateOfDetermination").val() !== "" ? AD.X_MESSAGE_R2T4ResultDOD_CHANGE : AD.X_MESSAGE_R2T4Result_BLANK_DOD;
                                        this.deleteR2T4InputData(true, dodChangePopupMessage);
                                    }
                                } else {
                                    dodChangePopupMessage = $("#dtDateOfDetermination").val() !== "" ? AD.X_MESSAGE_R2T4InputDOD_CHANGE : AD.X_MESSAGE_R2T4Input_BLANK_DOD;
                                    this.deleteR2T4InputData(true, dodChangePopupMessage);
                                }
                            }
                        }
                    }
                }

                if (this.studentEnrollmentDetails !== undefined && this.studentEnrollmentDetails !== null && $("#dtDateOfDetermination").val() !== "") {
                    if (this.studentEnrollmentDetails.calculationPeriodTypeId != null) {
                        $("#chkClaculationPeriod").prop("checked", true);
                        $("#chkClaculationPeriod").prop("disabled", "disabled");

                        $("#dvCalculationPeriod").prop("style", "display:block");
                        $("#dvCalculationPeriod").prop("disabled", "disabled");

                        StudentEnrollment.enableOrDisableR2T4Checkbox(this.enrollmentDetails, $("#dtDateOfDetermination").val());
                        this.assignCalculationTypeUsed(this.studentEnrollmentDetails.calculationPeriodTypeId);
                    }
                }

            });
        }


        // this method used to assign the calculation period type for given calculationPeriodTypeId
        // and enable or disable the radiobuttons based on the flag disableRadioButtons
        private assignCalculationTypeUsed(calculationPeriodTypeId: string): void {
            let periodRadioList: any = document.getElementsByName("periodName");
            let assignPeriodUsed = () => {
                for (let i = 0; i < periodRadioList.length; i++) {
                    let radioButtonId = periodRadioList.item(i).id.toString();
                    if (radioButtonId === calculationPeriodTypeId) {
                        $(`#${radioButtonId}`).prop("checked", "true");
                    } else {
                        $(`#${radioButtonId}`).removeAttr("checked");
                    }

                    $(`#${radioButtonId}`).prop("disabled", "disabled");
                }
            };
            if (periodRadioList.length > 0) {
                assignPeriodUsed();
            } else {
                this.initializeCalculationPeriodType(() => { assignPeriodUsed(); });
            }
        }

        //*Initializing the withdraw date calender*//
        private initializeWithdrawDate(): void {
            this.createDatePicker("dtWithdrawDate");
        }

        //*Initializing the start date calender*//
        private initializeStartDate(): void {
            this.createDatePicker("dtStartDate");
        }

        //*Initializing the end date calender*//
        private initializeEndDate(): void {
            this.createDatePicker("dtEndDate");
        }

        //*Initializing the R2T4Result date calender*//
        private initializeR2T4ResultDate(): void {
            this.createDatePicker("cal2CreditWithdrawalDateEnabled");

            this.createDatePicker("calEndDate");

            this.createDatePicker("calStartDate");

            this.createDatePicker("dtPostWithdrwal");

            this.createDatePicker("dtDeadline");

            this.createDatePicker("dtResponseReceived");

            this.createDatePicker("dtGrantTransferred");

            this.createDatePicker("dtLoanTransferred");

            this.resultsVm.validatePwdDate();
        }

        /**
       * Intilizing the controls for R2T4Result tab /
       */
        public initializeNewR2T4Result(r2T4InputDetail: any, isInputModified: boolean, isSaveR2T4Results: boolean, resultCompleted: boolean) {
            $("#lblStudentNameR2T4Result").text(this.studentName);
            $("#dateDetermination").text($("#dtDateOfDetermination").val());
            $("#lblSSNR2T4Result").text(this.studentSsn);
            $("#lblStudentNameR2T4ResultPWD").text(this.studentName);
            $("#dateDeterminationPWD").text($("#dtDateOfDetermination").val());
            $("#lblSSNR2T4ResultPWD").text(this.studentSsn);

            this.resultsVm.studentName = this.studentName;
            this.resultsVm.studentSsn = this.studentSsn;
            this.resultsVm.enrollmentName = this.enrollmentName;

            this.calculationPeriodTypeId = $("input[id='chkClaculationPeriod']").prop('checked') ? $("input[name='periodName']:checked")[0].id : null;
            this.isPaymentPeriod(this.calculationPeriodTypeId,
                () => { });

            this.resultsVm.isResultsExists(
                (result) => {
                    if (isInputModified || !result) {
                        new Api.R2T4StudentTermination().fetchR2T4Resultdetails(r2T4InputDetail,
                            (response) => {
                                //calling the specific R2T4ResultsVM for setting the data in results tab.
                                this.resultsVm.setR2T4ResultDetails(response, r2T4InputDetail);
                                if (isSaveR2T4Results) {
                                    this.resultsVm.updateR2T4ResultDetails(resultCompleted, isInputModified);
                                }
                            });
                    } else if (!isInputModified || result) {
                        this.resultsVm.getSavedR2T4Details(r2T4InputDetail);
                    }
                    if (!result) {
                        var dt = new Date();
                        let strDate = this.getFormattedDate(dt);
                        $("#dateCompleted").text(strDate);
                    }
                });
            this.resultsVm.isInputModified = false;
            this.resultsVm.disablePwdControls();
            $("#divR2T4Result :input").prop("readonly", true);
            //Checking the the user is support user or not.
            this.isSupportUser(
                (response) => {
                    if (response) {
                        $("#dvOverride").show();
                    } else {
                        $("#dvOverride").hide();
                    }
                });
        }

        /**
        * Intilizing the controls for R2T4Result tab /
        */
        public initializeR2T4Result(r2T4InputDetail: any, isInputModified: boolean, updateResults: boolean) {
            $("#lblStudentNameR2T4Result").text(this.studentName);
            $("#dateDetermination").text($("#dtDateOfDetermination").val());
            $("#lblSSNR2T4Result").text(this.studentSsn);
            $("#lblStudentNameR2T4ResultPWD").text(this.studentName);
            $("#dateDeterminationPWD").text($("#dtDateOfDetermination").val());
            $("#lblSSNR2T4ResultPWD").text(this.studentSsn);

            this.resultsVm.studentName = this.studentName;
            this.resultsVm.studentSsn = this.studentSsn;
            this.resultsVm.enrollmentName = this.enrollmentName;

            this.calculationPeriodTypeId = $("input[id='chkClaculationPeriod']").prop('checked') ? $("input[name='periodName']:checked")[0].id : null;
            this.isPaymentPeriod(this.calculationPeriodTypeId,
                () => { });

            this.resultsVm.isResultsExists(
                (result) => {
                    if ((isInputModified || !result) && updateResults) {
                        new Api.R2T4StudentTermination().fetchR2T4Resultdetails(r2T4InputDetail,
                            (response) => {
                                //calling the specific R2T4ResultsVM for setting the data in results tab.
                                this.resultsVm.setR2T4ResultDetails(response, r2T4InputDetail);

                            });
                    } else if (!isInputModified || result) {
                        this.resultsVm.getSavedR2T4Details(r2T4InputDetail);
                    }
                    if (!result) {
                        var dt = new Date();
                        let strDate = this.getFormattedDate(dt);
                        $("#dateCompleted").text(strDate);
                    }
                });
            this.resultsVm.isInputModified = false;
            this.resultsVm.disablePwdControls();
            $("#divR2T4Result :input").prop("readonly", true);
            //Checking the the user is support user or not.
            this.isSupportUser(
                (response) => {
                    if (response) {
                        $("#dvOverride").show();
                    } else {
                        $("#dvOverride").hide();
                    }
                });
        }

        /**
         * Method to validate the date
         */
        private validateDate(date: any): boolean {
            this.dateValidation = MasterPage.isValidDate(date) !== false && MasterPage.isdate1(date) !== false;
            return this.dateValidation;
        }

        private getEnrollmentDetails(studentId: string, enrollmentId: string, callBackMethod: (response: Response) => any): any {
            let enrollments = new Enrollments();
            if (studentId !== undefined && enrollmentId !== undefined) {
                let studentEnrollmentDetail: IStudentEnrollment = { studentId: studentId, enrollmentId: enrollmentId };
                return enrollments.getEnrollmentDetail(studentEnrollmentDetail, callBackMethod);
            }
        }

        private initializeLastDateAttended(): void {
            this.ldaDatePicker = this.createDatePicker("dtLastDateAttended");
            $("#dtLastDateAttended").change(() => {
                if (this.lastDtAttendedValidator("#dtLastDateAttended", this.ldaDatePicker) && $("#chkClaculationPeriod").prop("checked"))
                    if (this.validateDate($("#dtLastDateAttended").val()) && this.validateDate($("#dtDateOfDetermination").val()))
                        if (this.isPopupOpen === false &&
                            $("#hdnTerminationId").val() !== "" &&
                            $("#hdnTerminationId").val() !== this.emptyGuid &&
                            $("#dtLastDateAttended").val() !== this.lastDateAttended) {
                            if (constants.isR2T4InputExists) {
                                if (constants.isR2T4ResultsExists || constants.isR2T4OverrideResultsExists) {
                                    if (constants.r2T4CompletionStatus.isR2T4ResultsCompleted == undefined ||
                                        constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted == undefined) {
                                        r2T4Termination.getR2T4CompletedStatus($("#hdnTerminationId").val(),
                                            (completionStatus: any) => {
                                                if (!!completionStatus) {
                                                    constants.r2T4CompletionStatus = completionStatus;
                                                    if (constants.r2T4CompletionStatus.isR2T4ResultsCompleted ||
                                                        constants.r2T4CompletionStatus
                                                            .isR2T4OverrideResultsCompleted) {
                                                        this.saveTerminationOnLdaChange(true,
                                                            AD.X_MESSAGE_R2T4ApproveTermination_LDA_CHANGE);
                                                    } else {
                                                        this.saveTerminationOnLdaChange(true,
                                                            AD.X_MESSAGE_R2T4Result_LDA_CHANGE);
                                                    }
                                                }
                                            });
                                    } else {
                                        if (constants.r2T4CompletionStatus.isR2T4ResultsCompleted ||
                                            constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted) {
                                            this.saveTerminationOnLdaChange(true,
                                                AD.X_MESSAGE_R2T4ApproveTermination_LDA_CHANGE);
                                        } else {
                                            this.saveTerminationOnLdaChange(true,
                                                AD.X_MESSAGE_R2T4Result_LDA_CHANGE);
                                        }
                                    }
                                } else {
                                    this.saveTerminationOnLdaChange(true,
                                        AD.X_MESSAGE_R2T4Input_LDA_CHANGE);
                                }
                            }
                        }
            });
            this.setComponentAttribute("dtLastDateAttended", "required", "false");
            this.enableDatePicker("dtLastDateAttended", false);
        }

        private initializePopUpEndDate(): void {
            $("#dtPeriodEndDate").change(() => {
                this.studentValidator = MasterPage.ADVANTAGE_VALIDATOR("window");
                if (this.studentValidator.validate() && this.validateDate($("#dtPeriodEndDate").val())) {
                    return true;
                } else {
                    return false;
                }
            });
        }

        private isRequiredLastDateAttended(response): boolean {
            if (response === undefined) return undefined;

            this.enrollmentStartDate = undefined;
            if (response.startDate !== undefined) {
                let startDate = new Date(response.startDate);
                this.enrollmentStartDate = new Date(startDate.toDateString());
            }
            this.unitTypeDescription = response.unitTypeDescription !== undefined ? response.unitTypeDescription : "";
            let dofDetermination = (document.getElementById("dtDateOfDetermination") as HTMLTextAreaElement).value;
            let dateOfDetermination = dofDetermination !== undefined ? new Date(dofDetermination) : undefined;

            if (dateOfDetermination !== undefined
                && this.enrollmentStartDate !== undefined
                && dateOfDetermination >= this.enrollmentStartDate
                && response.systemStatusId !== undefined
                && Number(response.systemStatusId) !== Number(AD.SystemStatus.FutureStart)
            ) {
                this.isDodPopupRequired = true;
                constants.attendanceUnitType = this.unitTypeDescription.toLowerCase();
                if (this.unitTypeDescription.toLowerCase() === "none")
                    return false;
                else
                    return true;
            } else if (dateOfDetermination !== undefined &&
                this.enrollmentStartDate !== undefined &&
                dateOfDetermination < this.enrollmentStartDate &&
                response.systemStatusId !== undefined &&
                Number(response.systemStatusId) !== Number(AD.SystemStatus.FutureStart)) {
                this.isDodPopupRequired = false;
                return true;
            }

            this.isDodPopupRequired = undefined;
            return undefined;
        }

        public setComponentAttribute(componentId: string, attributeName: string, attributeValue: string): void {
            let element = document.getElementById(componentId);
            if (attributeName.toLowerCase() === "required") {
                if (attributeValue.toLowerCase() === "false") {
                    element.removeAttribute(attributeName.toLowerCase());
                } else {
                    element.setAttribute(attributeName, "");
                }
            } else {
                if (element !== undefined && element !== null) {
                    element.setAttribute(attributeName, attributeValue);
                }
            }
        }

        private initializeStep1() {
            this.initializeBindStudentStatus();
            this.initializeBindDropReason();
            this.initializeCalculationPeriod();
            this.initializeStudentSearch();
            this.initializeDateOfDetermination();
            this.initializeLastDateAttended();
            this.studentSearchValidation();
            this.clearSearchResult();
            this.clearSearchOnBlur();
            this.handleEnterKeyOnInput();
            this.clearSearchResultOnClearText();
            this.initializeWithdrawDate();
            this.initializeStartDate();
            this.initializeEndDate();
            this.initializeAttendance();
            this.checkValueTotal();
            this.checkValueCompleted();
            this.checkWithDrawDateOnChange();
            this.checkStartDateOnChange();
            this.checkEndDateOnChange();
            this.restrictOnlyZeros();
            this.initializeOverideCheck();
            this.initializeR2T4ResultDate();
            this.disableCancelTermination();
            this.setTitleIvCreditBalance();
            this.initializePopUpEndDate();
        }

        //Disable cancel termination button on first time tab load.
        public disableCancelTermination() {
            if ($("#txtStudentSearch").val().length === 0 || $("#hdnTerminationId").val() === "") {
                $("#btnCancelTermination").prop("disabled", true);
            }
        }

        public handleEnterKeyOnInput() {
            $(document).keypress((event) => {
                if (event.key !== "F5") {
                    if (event.currentTarget["activeElement"].type !== "button") {
                        if (event.keyCode === 13) {
                            event.preventDefault();
                            return false;
                        }
                    }
                } else {
                    window.location.assign(document.location.href);
                    event.preventDefault();
                    return false;
                }
                return true;
            });

            //Remove the required field validation on change of value seletion
            $("#ddlDropReason").on("blur", () => {
                let controlId = $("#ddlDropReason").parent().find(".k-dropdown-wrap>span");
                if ($("#ddlDropReason").val() !== "")
                    $(controlId).removeClass("valueRequired");
            });

            //Remove the required field validation on change of value seletion
            $("#ddlStatus").on("blur", () => {
                let controlId = $("#ddlStatus").parent().find(".k-dropdown-wrap>span");
                if ($("#ddlStatus").val() !== "")
                    $(controlId).removeClass("valueRequired");
            });

            $("#txtStudentSearch").bind({
                paste: () => {
                    return false;
                }
            });

            //restricting copy paste for numeric fields.
            $(".money").bind("cut copy paste", (e) => {
                e.preventDefault();
            });

            //restricting copy paste for numeric fields.
            $("#txtScheduledHours").bind("cut copy paste", (e) => {
                e.preventDefault();
            });

            //restricting copy paste for numeric fields.
            $("#txtTotalHours").bind("cut copy paste", (e) => {
                e.preventDefault();
            });
        }

        private initializeBindStudentStatusByEnrollment(value: any) {
            let dropStatusLoad = new Api.Components.StudentTerminationStatusCodeDropdownList("ddlStatus", this.campusId, value.enrollmentId,
                () => {
                    this.statusId = $("#ddlStatus").data("kendoDropDownList").value();
                    var ddl = $("#ddlStatus").data("kendoDropDownList");
                    var selectedItem = ddl.dataSource.data()[ddl.select()];
                    if (selectedItem !== undefined) {
                        
                    }

                });
            this.statusId = dropStatusLoad.selectedStatusId;
            return dropStatusLoad;
        }

        //This function validates the date of determination with enrollment date
        //Date of determination should be always greater than enrollment date
        public dateOfDeterminationValidator(componentId: string, input: any, studentEnrollment: any): boolean {
            MasterPage.destroyTooltip(input);
            if (studentEnrollment !== undefined && input.val() !== undefined && input.val() !== "") {
                if (MasterPage.cutomDateFormatValidation(input)) {
                    let enrollmentDate = kendo.parseDate(studentEnrollment.enrollmentDate);
                    let dateOfDetermination = kendo.parseDate($("#dtDateOfDetermination").val());
                    let dateLastAttended = kendo.parseDate($("#dtLastDateAttended").val());

                    if (enrollmentDate !== undefined && enrollmentDate !== null) {
                        if (dateOfDetermination <= enrollmentDate) {
                            MasterPage.createTooltip(
                                AD.X_MESSAGE_DOD_GREATER_THAN_ENROLLMENTDATE +
                                "' " +
                                this.getFormattedDate(enrollmentDate) +
                                " '",
                                input);
                            this.dateValidation = false;
                            return this.dateValidation;
                        }
                        if (dateOfDetermination < dateLastAttended) {
                            if ($('#LDA').text() === AD.X_DATE_TYPE) {
                                MasterPage.createTooltip(AD.X_MESSAGE_DOD_LESS_THAN_WD +
                                    "' " +
                                    this.getFormattedDate(dateLastAttended) +
                                    " '",
                                    input);
                            } else {
                                MasterPage.createTooltip(
                                    AD.X_MESSAGE_DOD_LESS_THAN_LDA +
                                    "' " +
                                    this.getFormattedDate(dateLastAttended) +
                                    " '",
                                    input);
                            }
                            this.dateValidation = false;
                            return this.dateValidation;
                        } else if (dateOfDetermination < dateLastAttended) {
                            if ($('#LDA').text() === AD.X_DATE_TYPE) {
                                MasterPage.createTooltip(AD.X_MESSAGE_DOD_LESS_THAN_WD +
                                    "' " +
                                    this.getFormattedDate(dateLastAttended) +
                                    " '",
                                    input);
                            } else {
                                MasterPage.createTooltip(
                                    AD.X_MESSAGE_DOD_LESS_THAN_LDA +
                                    "' " +
                                    this.getFormattedDate(dateLastAttended) +
                                    " '",
                                    input);
                            }
                        }

                    }

                }
            }
            this.dateValidation = true;
            return this.dateValidation;
        }

        //This function validates the last date attended with enrollment start date and date of determination
        //Last attended date should be greater than or equal to enrollment start date and
        //should be less than or equal to date of determination
        private lastDtAttendedValidator(componentId: string, input: any): boolean {
            MasterPage.destroyTooltip(input);
            if (MasterPage.cutomDateFormatValidation(input)) {
                let dateLastAttended = kendo.parseDate($("#dtLastDateAttended").val());
                let dateOfDetermination = kendo.parseDate($("#dtDateOfDetermination").val());
                if (dateLastAttended !== undefined && dateLastAttended !== null) {
                    if (dateOfDetermination < dateLastAttended) {
                        if ($('#LDA').text() === AD.X_DATE_TYPE) {
                            MasterPage.createTooltip(AD.X_MESSAGE_WITHDRAWL_LESS_THAN_DOD + "' " + this.getFormattedDate(dateOfDetermination) + " '", input);
                        } else {
                            MasterPage.createTooltip(AD.X_MESSAGE_LDA_LESS_THAN_DOD + "' " + this.getFormattedDate(dateOfDetermination) + " '", input);
                        }
                        this.dateValidation = false;
                        return this.dateValidation;
                    } else if (this.enrollmentStartDate === null ||
                        this.enrollmentStartDate === undefined ||
                        this.enrollmentStartDate > dateLastAttended) {
                        if ($('#LDA').text() === AD.X_DATE_TYPE) {
                            MasterPage.createTooltip(AD.X_MESSAGE_WITHDRAWL_GREATER_THAN_ENROLLMENTDATE + "' " + this.getFormattedDate(this.enrollmentStartDate) + " '", input);
                        } else {
                            MasterPage.createTooltip(AD.X_MESSAGE_LDA_GREATER_THAN_ENROLLMENTDATE + "' " + this.getFormattedDate(this.enrollmentStartDate) + " '", input);
                        }
                        this.dateValidation = false;
                        return this.dateValidation;
                    }
                }
            } else {
                this.dateValidation = false;
                return this.dateValidation;
            }
            this.dateValidation = true;
            return this.dateValidation;
        }

        //this function will reset and reload the termination detail view and load the updated enrollment list for the same student
        public reloadTerminationDetailView(isSingleEnrollment: boolean = false): void {
            var studentInfo: any = {};
            if (!isSingleEnrollment) {
                studentInfo = { studentId: this.studentId, displayName: this.studentName };
                $("#txtStudentSearch").val(studentInfo.displayName);
            } else {
                $("#txtStudentSearch").val("");
                this.clearEnrollmentsOnBlur(false);
            }
            this.previousEnrollmentId = undefined;
            $("#hdnTerminationId").val("");
            this.clearSearchResultOnEnrollment();
            this.initializeStep2(studentInfo);
        }

        public initializeStep2(studentInfo) {
            try {
                let studentEnrollmentId: string;
                let programName: string;
                let enrollmentSaveMessage: string;
                let studentName: string;
                $("#spnEnrollment").hide();

                let systemStatus = {} as Models.ISystemStatusModel;
                systemStatus.IsActiveEnrollments = true;
                systemStatus.IsDroppedEnrollments = false;

                $("#ddlStatus").attr("disabled", "disabled");
                $("#ddlDropReason").attr("disabled", "disabled");

                let studentEnrollmentDetails = new StudentEnrollment("studentEnrollment",
                    studentInfo.studentId, systemStatus,
                    ((value) => {

                        this.setComponentAttribute("ddlDropReason", "required", "true");

                        $("#ddlStatus").removeAttr("disabled");
                        $("#ddlDropReason").removeAttr("disabled");

                        this.initializeBindStudentStatusByEnrollment(value).onDataBound(() => {

                            $("#hdnR2T4InputId").val("");
                            $("#spnEnrollment").hide();
                            if (this.previousEnrollmentId !== undefined) {
                                studentEnrollmentId = this.previousStudentEnrollmentId = this.previousEnrollmentId;
                                programName = this.previousProgramName;
                                studentName = this.previousStudentName;
                                enrollmentSaveMessage = `Termination record for ${studentName} and ${programName} is saved`;
                            }
                            this.isNonTermProgram = false;
                            constants.statusCodeId = value.statusCodeId;
                            this.previousEnrollmentId = value.enrollmentId;
                            $("#hdnEnrollmentId").val(value.enrollmentId);
                            this.previousProgramName = value.programVersionDescription;
                            this.previousStudentName = studentInfo.displayName;

                            if (value.unitTypeDescription.toLowerCase() === "none") {
                                (document.getElementById("LDA") as HTMLSpanElement).textContent = "Withdrawal date";
                            }
                            else {
                                (document.getElementById("LDA") as HTMLSpanElement).textContent = "Last date attended";
                            }

                            if ($("#hdnTerminationId").val() !== "" &&
                                $("#hdnTerminationId").val() !== this.emptyGuid &&
                                this.dateValidation) {
                                let terminationModel = {} as Models.IStudentTermination;
                                let terminationData = this.generateR2T4TerminationModel(terminationModel);
                                terminationData.studentEnrollmentId = studentEnrollmentId;
                                this.saveR2T4TerminationDetails(terminationData,
                                    $("#hdnTerminationId").val(),
                                    (response) => {
                                        if (response["resultStatus"] === AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                            MasterPage.SHOW_INFO_WINDOW(enrollmentSaveMessage);
                                            this.bindDataOnEnrollmentSelection(value);
                                        } else {
                                            this.setPreviouslySelectedId();
                                            MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_FAIL.replace("$StundentName", constants.studentName).replace("$EnrollmentName", constants.enrollmentName));
                                            return;
                                        }
                                    });
                            }
                            else {
                                $("#ContentMain2_btnSave").parent().removeClass("rfdInputDisabled");
                                this.bindDataOnEnrollmentSelection(value);
                            }


                        });


                      
                    }) as any,
                    (() => { return this.isTerminationDataValid(); }) as any);
                this.setComponentAttribute("ddlStatus", "required", "true");
                this.setComponentAttribute("ddlDropReason", "required", "true");
                this.setComponentAttribute("dtDateOfDetermination", "required", "true");
            }
            catch (e) {
                MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_FAIL.replace("$StundentName", constants.studentName).replace("$EnrollmentName", constants.enrollmentName));
                return;
            }
        }

        //--This function will get called when an enrollment is selected.
        //--It fetches the data related to the selected enrollment from the table and binds the data to the respective elements. --//
        public bindDataOnEnrollmentSelection(enrollmentDetail: any) {
            $("#spnSave").removeClass("disabled");
            $("#spnCancel").removeClass("disabled");
            $("#btnCancelTermination").prop("disabled", false).removeClass("rfdInputDisabled");
            this.enrollmentId = enrollmentDetail.enrollmentId;
            this.setupLastDateAttendedField(this.studentId, this.enrollmentId);
            this.enrollmentDetails = enrollmentDetail;
            this.enrollmentName = enrollmentDetail.programVersionDescription;
            constants.enrollmentName = enrollmentDetail.programVersionDescription;
            this.clearSearchResultOnEnrollment(false);
            $("#dtDateOfDetermination").val("");
            $("#dtLastDateAttended").val("");
            this.setComponentAttribute("dtLastDateAttended", "required", "false");
            this.enableDatePicker("dtLastDateAttended", false);
            (document.getElementById("spLDA") as HTMLSpanElement).textContent = "";
            this.initializeStep3(this.enrollmentId);
            $("#ChkOverride").prop('checked', false);
            $("#txtTicket").val('');
        }

        //--This function will get called when an enrollment is selected. It fetches all the saved data from the table--//
        public initializeStep3(enrollId) {
            this.initializeCalculationPeriodType(() => { });
            this.fetchStudentTermination(enrollId, false);
        }

        private fetchStudentTermination(enrollId: string, isOnDodOrLdaChange: boolean): void {
            let studentTerminationData = {} as IFetchByEnrollId;
            studentTerminationData.enrollmentId = enrollId;
            if (enrollId !== undefined || enrollId !== "")
                r2T4Termination.fetchTerminationByEnrollmentId(studentTerminationData,
                    (response: any) => {
                        $("#liTerminationDetails").addClass("is-active").removeClass("is-visited is-invalid");
                        if (!isOnDodOrLdaChange)
                            $("#liR2T4Input, #liR2T4Result, #liApproveTermination").removeClass("is-active is-visited is-invalid");
                        if (response === undefined) {
                            $("#hdnTerminationId").val(this.emptyGuid);
                            this.getLastDateAttended(enrollId);
                        } else if (response !== undefined && response.terminationId === this.emptyGuid) {
                            $("#hdnTerminationId").val(this.emptyGuid);
                            constants.isR2T4InputTabDisabled = true;
                            constants.isR2T4ResultTabDisabled = true;
                            constants.isR2T4ApproveTabDisabled = true;
                            constants.isR2T4InputExists = false;
                            constants.r2T4CompletionStatus = {} as Models.IR2T4CompletionStatus;
                            this.getLastDateAttended(enrollId);
                        } else {
                            this.ldaFromAttendance = undefined;
                            this.studentEnrollmentById = response;
                            this.setTerminationDetails(this.studentEnrollmentById);
                            if (!isOnDodOrLdaChange) {
                                $("#liTerminationDetails").addClass("is-active").removeClass("is-visited is-invalid");
                                r2T4Termination.getR2T4CompletedStatus($("#hdnTerminationId").val(),
                                    (completionStatus: any) => {
                                        if (!!completionStatus) {
                                            constants.r2T4CompletionStatus = completionStatus;
                                            if (this.studentEnrollmentById.terminationId !== this.emptyGuid) {
                                                this.isR2T4InputExists(
                                                    (result: any) => {
                                                        if (!!result) {
                                                            constants.isR2T4InputExists = true;
                                                            constants.r2T4CompletionStatus.isR2T4InputCompleted
                                                                ? $("#liR2T4Input").removeClass("is-invalid is-active").addClass("is-visited")
                                                                : $("#liR2T4Input").removeClass("is-invalid is-active is-visited");
                                                            constants.isR2T4InputTabDisabled = false;
                                                            this.resultsVm.isResultsExists(
                                                                (value: any) => {
                                                                    if (!!value) {
                                                                        constants.isR2T4ResultsExists = constants.isR2T4OverrideResultsExists = true;
                                                                        (constants.r2T4CompletionStatus.isR2T4ResultsCompleted || constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted)
                                                                            ? $("#liR2T4Result").removeClass("is-invalid is-active").addClass("is-visited")
                                                                            : $("#liR2T4Result").removeClass("is-invalid is-active is-visited");
                                                                        constants.isR2T4ResultTabDisabled = false;
                                                                    } else {
                                                                        constants.isR2T4ResultsExists = constants.isR2T4OverrideResultsExists = false;
                                                                        if (this.studentEnrollmentById.isPerformingR2T4Calculator) {
                                                                            $("#liR2T4Result").removeClass("is-active is-visited is-invalid");
                                                                        } else {
                                                                            $("#liR2T4Result").removeClass("is-active is-visited").addClass("is-invalid");
                                                                        }
                                                                        constants.isR2T4ResultTabDisabled = true;
                                                                    }
                                                                });
                                                        } else {
                                                            constants.isR2T4InputExists = false;
                                                            if (this.studentEnrollmentById.isPerformingR2T4Calculator) {
                                                                $("#liR2T4Input").removeClass("is-active is-visited is-invalid");
                                                            } else {
                                                                $("#liR2T4Input, #liR2T4Result").removeClass("is-active is-visited").addClass("is-invalid");
                                                            }
                                                            constants.isR2T4InputTabDisabled = true;
                                                        }
                                                    });
                                                if ($("#chkClaculationPeriod").prop("checked")) {
                                                    this.resultsVm.isResultsExists(
                                                        (value: any) => {
                                                            if (!!value) {
                                                                r2T4Termination.getSavedR2T4Results($("#hdnTerminationId").val(),
                                                                    (result: any) => {
                                                                        if (result.isInputIncluded) {
                                                                            if (result.isR2T4OverrideResultsCompleted) {
                                                                                constants.r2T4CompletionStatus
                                                                                    .isR2T4OverrideResultsCompleted = true;
                                                                                constants.isR2T4ApproveTabDisabled = false;
                                                                            } else {
                                                                                constants.r2T4CompletionStatus
                                                                                    .isR2T4OverrideResultsCompleted = false;
                                                                                constants.isR2T4ApproveTabDisabled = true;
                                                                            }
                                                                        } else {
                                                                            if (result.isR2T4ResultsCompleted) {
                                                                                constants.r2T4CompletionStatus
                                                                                    .isR2T4ResultsCompleted = true;
                                                                                constants.isR2T4ApproveTabDisabled = false;
                                                                            } else {
                                                                                constants.r2T4CompletionStatus
                                                                                    .isR2T4ResultsCompleted = false;
                                                                                constants.isR2T4ApproveTabDisabled = true;
                                                                            }
                                                                        }
                                                                    });
                                                            }
                                                        });
                                                } else {
                                                    constants.isR2T4ApproveTabDisabled = !this.studentEnrollmentById
                                                        .isR2T4ApproveTabEnabled;
                                                }
                                            }
                                        }

                                        let programVersions = new ProgramVersions();
                                        let inputParameters = {
                                            enrollmentId: enrollId,
                                            lastAttendedDate: $("#dtLastDateAttended").val()
                                        };
                                        programVersions.getStudentAwardsByEnrollmentId(inputParameters,
                                            (studentAwards: any) => {
                                                if (!!studentAwards && studentAwards.length > 0) {
                                                    if (studentAwards[0].resultStatus === AD.X_MESSAGE_NO_AWARDS) {
                                                        $("#chkClaculationPeriod").prop('checked', false);
                                                        (document.getElementById("dvCalculationPeriod") as HTMLDivElement).setAttribute("style", "display:none");
                                                        let messageText = AD.X_MESSAGE_NO_AWARDS_FOR_STUDENT.replace("$studentName", $("#txtStudentSearch").val()).replace("$enrollmentName", constants.enrollmentName);
                                                        MasterPage.showDialogPopup(messageText);
                                                    }
                                                }
                                            });
                                    });
                            }
                        }
                    });
        }

        private getLastDateAttended(enrollId) {
            if ($('#LDA').text() !== AD.X_DATE_TYPE) {
                r2T4Termination.getLastDateAttended(enrollId,
                    (value: any) => {
                        if (!!value) {
                            var lastDate = StudentTerminationVm.formatDate(new Date(value));
                            if (lastDate === "1/1/1") {
                                $("#dtLastDateAttended").val("");
                                this.enableDatePicker("dtLastDateAttended", true);
                            } else {
                                $("#dtLastDateAttended").val(MasterPage.formatDate(lastDate));
                                this.enableDatePicker("dtLastDateAttended", false);
                                let dateLastAttended = kendo.parseDate($("#dtLastDateAttended").val());
                                let dateOfDetermination = kendo.parseDate($("#dtDateOfDetermination").val());
                                if (!!dateOfDetermination && dateOfDetermination < dateLastAttended) {
                                    MasterPage.createTooltip(AD.X_MESSAGE_DOD_LESS_THAN_LDA + "' " + this.ldaFromAttendance + " '", $("#dtDateOfDetermination"));
                                }
                            }
                        }
                    });
            } else {
                $("#dtLastDateAttended").val("");
                this.enableDatePicker("dtLastDateAttended", true);
            }
        }
        //this method will check if any input data has data validation message
        //and return true if there is no validation message else false
        public isTerminationDataValid(): boolean {
            let isValidForm: boolean = true;
            $("#divTerminationDetail").find("input:text").each((index, element) => {
                let tooltip = $(element).prop("name") + "tt";
                let tool = $(`#${tooltip}`);
                if (tool.length > 0) {
                    isValidForm = false;
                }
            });
            return isValidForm;
        }
        //this method convert given number into currency format and return a string.
        formatNumberAsCurrency(value: number): string {
            return value.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }

        //this method convert given number into currency format and return a string.
        formatNumberAsCurrencyPercenatgeTab(value: number): string {
            return value.toFixed(3).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }

        //this method convert given number into percentage format and return a string.
        formatNumberAsPercenatgeResults(value: number): string {
            return value.toFixed(1).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
        }
        //--This function is used to bind the R2T4 Input data to the controls in R2T4Input tab.//
        private setR2T4InputDetails(response: any): any {
            if (response !== undefined) {
                $("#hdnR2T4InputId").val(response.r2T4InputId);
                if (response.pellGrantDisbursed !== undefined && response.pellGrantDisbursed !== null && response.pellGrantDisbursed !== NaN)
                    $("#txtPellGrantDisbursed").val(this.formatNumberAsCurrency(parseFloat(response.pellGrantDisbursed)));

                if (response.pellGrantCouldDisbursed !== undefined && response.pellGrantCouldDisbursed !== null && response.pellGrantCouldDisbursed !== NaN)
                    $("#txtPellGrantCouldDisbursed").val(this.formatNumberAsCurrency(parseFloat(response.pellGrantCouldDisbursed)));

                if (response.fseogDisbursed !== undefined && response.fseogDisbursed !== null && response.fseogDisbursed !== NaN)
                    $("#txtFseogDisbursed").val(this.formatNumberAsCurrency(parseFloat(response.fseogDisbursed)));

                if (response.fseogCouldDisbursed !== undefined && response.fseogCouldDisbursed !== null && response.fseogCouldDisbursed !== NaN)
                    $("#txtFseogCouldDisbursed").val(this.formatNumberAsCurrency(parseFloat(response.fseogCouldDisbursed)));

                if (response.teachGrantDisbursed !== undefined && response.teachGrantDisbursed !== null && response.teachGrantDisbursed !== NaN)
                    $("#txtTeachGrantDisbursed").val(this.formatNumberAsCurrency(parseFloat(response.teachGrantDisbursed)));

                if (response.teachGrantCouldDisbursed !== undefined && response.teachGrantCouldDisbursed !== null && response.teachGrantCouldDisbursed !== NaN)
                    $("#txtTeachGrantCouldDisbursed").val(this.formatNumberAsCurrency(parseFloat(response.teachGrantCouldDisbursed)));

                if (response.iraqAfgGrantDisbursed !== undefined && response.iraqAfgGrantDisbursed !== null && response.iraqAfgGrantDisbursed !== NaN)
                    $("#txtIraqAfgGrantDisbursed").val(this.formatNumberAsCurrency(parseFloat(response.iraqAfgGrantDisbursed)));

                if (response.iraqAfgGrantCouldDisbursed !== undefined && response.iraqAfgGrantCouldDisbursed !== null && response.iraqAfgGrantCouldDisbursed !== NaN)
                    $("#txtIraqAfgGrantCouldDisbursed").val(this.formatNumberAsCurrency(parseFloat(response.iraqAfgGrantCouldDisbursed)));

                if (response.unsubLoanNetAmountDisbursed !== undefined && response.unsubLoanNetAmountDisbursed !== null && response.unsubLoanNetAmountDisbursed !== NaN)
                    $("#txtUnsubLoanNetAmountDisbursed").val(this.formatNumberAsCurrency(parseFloat(response.unsubLoanNetAmountDisbursed)));

                if (response.unsubLoanNetAmountCouldDisbursed !== undefined && response.unsubLoanNetAmountCouldDisbursed !== null && response.unsubLoanNetAmountCouldDisbursed !== NaN)
                    $("#txtUnsubLoanNetAmountCouldDisbursed").val(this.formatNumberAsCurrency(parseFloat(response.unsubLoanNetAmountCouldDisbursed)));

                if (response.subLoanNetAmountDisbursed !== undefined && response.subLoanNetAmountDisbursed !== null && response.subLoanNetAmountDisbursed !== NaN)
                    $("#txtSubLoanNetAmountDisbursed").val(this.formatNumberAsCurrency(parseFloat(response.subLoanNetAmountDisbursed)));

                if (response.subLoanNetAmountCouldDisbursed !== undefined && response.subLoanNetAmountCouldDisbursed !== null && response.subLoanNetAmountCouldDisbursed !== NaN)
                    $("#txtSubLoanNetAmountCouldDisbursed").val(this.formatNumberAsCurrency(parseFloat(response.subLoanNetAmountCouldDisbursed)));

                if (response.perkinsLoanDisbursed !== undefined && response.perkinsLoanDisbursed !== null && response.perkinsLoanDisbursed !== NaN)
                    $("#txtPerkinsLoanDisbursed").val(this.formatNumberAsCurrency(parseFloat(response.perkinsLoanDisbursed)));

                if (response.perkinsLoanCouldDisbursed !== undefined && response.perkinsLoanCouldDisbursed !== null && response.perkinsLoanCouldDisbursed !== NaN)
                    $("#txtPerkinsLoanCouldDisbursed").val(this.formatNumberAsCurrency(parseFloat(response.perkinsLoanCouldDisbursed)));

                if (response.directGraduatePlusLoanDisbursed !== undefined && response.directGraduatePlusLoanDisbursed !== null && response.directGraduatePlusLoanDisbursed !== NaN)
                    $("#txtDirectGraduatePlusLoanDisbursed").val(this.formatNumberAsCurrency(parseFloat(response.directGraduatePlusLoanDisbursed)));

                if (response.directGraduatePlusLoanCouldDisbursed !== undefined && response.directGraduatePlusLoanCouldDisbursed !== null && response.directGraduatePlusLoanCouldDisbursed !== NaN)
                    $("#txtDirectGraduatePlusLoanCouldDisbursed").val(this.formatNumberAsCurrency(parseFloat(response.directGraduatePlusLoanCouldDisbursed)));

                if (response.directParentPlusLoanDisbursed !== undefined && response.directParentPlusLoanDisbursed !== null && response.directParentPlusLoanDisbursed !== NaN)
                    $("#txtDirectParentPlusLoanDisbursed").val(this.formatNumberAsCurrency(parseFloat(response.directParentPlusLoanDisbursed)));

                if (response.directParentPlusLoanCouldDisbursed !== undefined && response.directParentPlusLoanCouldDisbursed !== null && response.directParentPlusLoanCouldDisbursed !== NaN)
                    $("#txtDirectParentPlusLoanCouldDisbursed").val(this.formatNumberAsCurrency(parseFloat(response.directParentPlusLoanCouldDisbursed)));

                if (response.startDate !== undefined && response.startDate !== null && response.startDate !== NaN) {
                    let date = StudentTerminationVm.formatDate(response.startDate);
                    $("#dtStartDate").val(MasterPage.formatDate(date));
                }

                if (response.scheduledEndDate !== undefined && response.scheduledEndDate !== null && response.scheduledEndDate !== NaN) {
                    let date = StudentTerminationVm.formatDate(response.scheduledEndDate);
                    $("#dtEndDate").val(MasterPage.formatDate(date));
                }

                if (response.completedTime !== undefined && response.completedTime !== null && response.completedTime !== NaN)
                    $("#txtScheduledHours").val(response.completedTime);

                if (response.totalTime !== undefined && response.totalTime !== null && response.totalTime !== NaN)
                    $("#txtTotalHours").val(response.totalTime);

                if (response.withdrawalDate !== undefined &&
                    response.withdrawalDate !== null &&
                    response.withdrawalDate !== NaN) {
                    let date = StudentTerminationVm.formatDate(response.withdrawalDate);
                    $("#dtWithdrawDate").val(MasterPage.formatDate(date));
                } else {
                    $("#dtWithdrawDate").val("");
                }

                let isLdaRequired = $("#dtLastDateAttended").prop("required");
                if (response.isAttendanceNotRequired !== undefined && response.isAttendanceNotRequired !== null && response.isAttendanceNotRequired !== NaN) {
                    if (response.isAttendanceNotRequired === true && this.isLdaChanged === false && isLdaRequired) {
                        if (this.isClockHr) {
                            $("#txtTotalHours, #txtScheduledHours, #dtWithdrawDate").removeClass("txtDisable");
                            $("#txtTotalHours, #txtScheduledHours").prop("disabled", false);
                            this.enableDatePicker("dtWithdrawDate", true);
                            $("#chkAttendance").prop("checked", true);
                            $("#chkAttendance").prop("disabled", false);
                            $("#spWithdrawDt, #spHoursCompleted, #spHoursPeriod").prop("textContent", "*");
                        }
                        else {
                           // $("#txtTotalHours, #txtScheduledHours, #dtWithdrawDate").addClass("txtDisable");
                            $("#txtTotalHours, #txtScheduledHours").prop("disabled", true);
                            this.enableDatePicker("dtWithdrawDate", false);
                            $("#chkAttendance").prop("checked", true);
                            $("#chkAttendance").prop("disabled", false);
                            $("#spWithdrawDt, #spHoursCompleted, #spHoursPeriod").prop("textContent", "");
                        }
                    }
                    else {
                        if (this.isLdaChanged && isLdaRequired) {
                            $("#dtWithdrawDate").val("");
                            $("#dtWithdrawDate").change();
                            $("#dtWithdrawDate").removeClass("txtDisable");
                            this.isLdaChanged = false;
                        }
                        else if (!this.isLdaChanged && !isLdaRequired && response.isAttendanceNotRequired === true) {
                            $("#dtWithdrawDate").val($("#dtLastDateAttended").val());
                            if (this.isClockHr) {
                                $("#txtTotalHours, #txtScheduledHours, #dtWithdrawDate").attr("enabled", "enabled");
                                $("#txtTotalHours, #txtScheduledHours, #dtWithdrawDate").removeClass("txtDisable");
                                this.enableDatePicker("dtWithdrawDate", true);
                            }
                            else {
                                $("#txtTotalHours, #txtScheduledHours, #dtWithdrawDate").attr("disabled", "disabled");
                              //  $("#txtTotalHours, #txtScheduledHours, #dtWithdrawDate").addClass("txtDisable");
                                this.enableDatePicker("dtWithdrawDate", false);
                            }
                            this.setComponentAttribute("txtScheduledHours", "required", "false");
                            this.setComponentAttribute("txtTotalHours", "required", "false");
                            this.setComponentAttribute("dtWithdrawDate", "required", "false");
                        }
                        else if (this.isLdaChanged && $("#dtWithdrawDate").val() === "") {
                            if (this.isClockHr) {
                                $("#txtTotalHours, #txtScheduledHours, #dtWithdrawDate").attr("enabled", "enabled");
                                $("#txtTotalHours, #txtScheduledHours, #dtWithdrawDate").removeClass("txtDisable");
                                this.enableDatePicker("dtWithdrawDate", true);
                            }
                            else {
                                $("#txtTotalHours, #txtScheduledHours, #dtWithdrawDate").attr("disabled", "disabled");
                             //   $("#txtTotalHours, #txtScheduledHours, #dtWithdrawDate").addClass("txtDisable");
                                this.enableDatePicker("dtWithdrawDate", false);
                            }
                            this.setComponentAttribute("txtScheduledHours", "required", "false");
                            this.setComponentAttribute("txtTotalHours", "required", "false");
                            this.setComponentAttribute("dtWithdrawDate", "required", "false");
                        }
                        else {
                                $("#txtTotalHours, #txtScheduledHours, #dtWithdrawDate").removeClass("txtDisable");
                                $("#txtTotalHours, #txtScheduledHours").prop("disabled", false);
                                this.enableDatePicker("dtWithdrawDate", true);
                                $("#chkAttendance").prop("checked", false);
                                $("#spWithdrawDt, #spHoursCompleted, #spHoursPeriod").prop("textContent", "*");
                        }
                    }
                }
                if (response.isAttendanceNotRequired === false) {
                    if ($("#dtWithdrawDate").val() === "" && $("#txtScheduledHours").val() === "" && $("#txtTotalHours").val() === "") {
                        $("#chkAttendance").prop("disabled", false).prop("checked", false);
                        $("#txtTotalHours, #txtScheduledHours, #dtWithdrawDate").removeAttr("disabled");
                        $("#txtTotalHours, #txtScheduledHours, #dtWithdrawDate").removeClass("txtDisable");
                        this.enableDatePicker("dtWithdrawDate", true);
                    } else {
                        $("#chkAttendance").prop("disabled", true);
                    }
                }
                if ($("#dtWithdrawDate").val() === "" &&
                    $("#txtScheduledHours").val() === "" &&
                    $("#txtTotalHours").val() === "") {
                    $("#chkAttendance").prop("disabled", false);
                }

                if (response.isTuitionChargedByPaymentPeriod === false) {
                    $("#chkTuitionCharged").prop("disabled", false).prop("checked", false);
                    if (response.creditBalanceRefunded !== undefined && response.creditBalanceRefunded !== null && response.creditBalanceRefunded !== NaN) {
                        $("#txtcreditBalanceRefunded").val(this.formatNumberAsCurrency(parseFloat(response.creditBalanceRefunded)));
                        (document.getElementById("txtcreditBalanceRefunded") as HTMLInputElement).disabled = false;
                        $("#txtcreditBalanceRefunded").removeClass("txtDisable");
                    }
                }
                else if (response.isTuitionChargedByPaymentPeriod === true) {
                    $("#chkTuitionCharged").prop("disabled", false).prop("checked", true);
                    (document.getElementById("txtcreditBalanceRefunded") as HTMLInputElement).disabled = true;
                    $("#txtcreditBalanceRefunded").val("");
                    $("#txtcreditBalanceRefunded").addClass("txtDisable");
                }

                if (response.tuitionFee !== undefined && response.tuitionFee !== null && response.tuitionFee !== NaN)
                    $("#txtTuitionFee").val(this.formatNumberAsCurrency(parseFloat(response.tuitionFee)));

                if (response.roomFee !== undefined && response.roomFee !== null && response.roomFee !== NaN)
                    $("#txtRoomFee").val(this.formatNumberAsCurrency(parseFloat(response.roomFee)));

                if (response.boardFee !== undefined && response.boardFee !== null && response.boardFee !== NaN)
                    $("#txtBoardFee").val(this.formatNumberAsCurrency(parseFloat(response.boardFee)));

                if (response.otherFee !== undefined && response.otherFee !== null && response.otherFee !== NaN)
                    $("#txtOtherFee").val(this.formatNumberAsCurrency(parseFloat(response.otherFee)));
            }

            if (!this.isClockHr) {
                let programVersions = new ProgramVersions();
                programVersions.getProgramVersionDetailByEnrollmentId(this.enrollmentId,
                    (programVersion: any) => {
                        if (!!programVersion) {
                            programVersions.getCampusProgramVersionDetail(this.enrollmentId,
                                (campusprogramVersion: any) => {
                                    if (!!campusprogramVersion && campusprogramVersion.resultStatus === true) {
                                        this.campusProgramVersionDetails = campusprogramVersion;
                                        if (programVersion.academicCalendarId ===
                                            Number(constants.AcademicCalendarProgram.NonTerm)) {
                                            this.isNonTermProgram = true;
                                            $("#spHoursCompleted, #spHoursPeriod").prop("textContent", "");
                                            if (campusprogramVersion.isSelfPaced != null &&
                                                campusprogramVersion.isSelfPaced === true) {
                                                this.enableOrDisablePercentageofPeriodCompleted(true, campusprogramVersion.isSelfPaced);
                                                return;
                                            }
                                            if (campusprogramVersion.isSelfPaced != null &&
                                                campusprogramVersion.isSelfPaced === false) {
                                                this.toggleNotRequiredToTakeAttendanceCheckbox(true, true);
                                            }
                                        } else if (programVersion.academicCalendarId ===
                                            Number(constants.AcademicCalendarProgram.NonStandardTerm)) {
                                            if (campusprogramVersion.termSubEqualInLen != null &&
                                                campusprogramVersion.termSubEqualInLen === true) {
                                                this.toggleNotRequiredToTakeAttendanceCheckbox(false);
                                            } else {
                                                this.toggleNotRequiredToTakeAttendanceCheckbox(true);
                                            }
                                        } else if (programVersion.academicCalendarId != null &&
                                            (programVersion.academicCalendarId ===
                                                Number(constants.AcademicCalendarProgram.Quarter) ||
                                                programVersion.academicCalendarId ===
                                                Number(constants.AcademicCalendarProgram.Semester) ||
                                                programVersion.academicCalendarId ===
                                                Number(constants.AcademicCalendarProgram.Trimester))) {
                                            this.toggleNotRequiredToTakeAttendanceCheckbox(false);
                                        }
                                    }
                                });
                        }
                    });
            }
            this.enableOrDisablePercentageofPeriodCompleted(false, response.isSelfPaceProgram);
        }

        private setTerminationDetails(response: any): any {
            if (response !== undefined) {
                $("#hdnTerminationId").val(response.terminationId);
                let statusId = response.statusCodeId;
                let dropreasonId = response.dropReasonId;
                let status = $("#ddlStatus").data("kendoDropDownList");
                status.value(statusId);
                let dropreason = $("#ddlDropReason").data("kendoDropDownList");
                dropreason.value(dropreasonId);
                if (response.dateWithdrawalDetermined !== null) {
                    let withdrawaldate = StudentTerminationVm.formatDate(response.dateWithdrawalDetermined);
                    $("#dtDateOfDetermination").val(MasterPage.formatDate(withdrawaldate));
                    StudentEnrollment.enableOrDisableR2T4Checkbox(this.enrollmentDetails, withdrawaldate);
                    if (this.studentEnrollmentDetails !== undefined && this.studentEnrollmentDetails !== null)
                        this.dateOfDeterminationValidator("#dtDateOfDetermination",
                            $("#dtDateOfDetermination"),
                            this.studentEnrollmentDetails);
                } else {
                    $("#dtDateOfDetermination").val("");
                }
                this.dateOfDetermination = $("#dtDateOfDetermination").val();

                this.enableAndDisableLastDateAttended(this.studentEnrollmentDetails);
                if (response.lastDateAttended !== null) {
                    let lastDate = StudentTerminationVm.formatDate(response.lastDateAttended);
                    $("#dtLastDateAttended").val(MasterPage.formatDate(lastDate));
                    if ($('#LDA').text() === AD.X_DATE_TYPE) {
                        this.enableDatePicker("dtLastDateAttended", true);
                    } else {
                        this.enableDatePicker("dtLastDateAttended", false);
                    }
                } else {
                    $("#dtLastDateAttended").val("");
                }
                this.lastDateAttended = $("#dtLastDateAttended").val();
                if (response.isPerformingR2T4Calculator) {
                    $("input[id='chkClaculationPeriod']").prop("checked", response.isPerformingR2T4Calculator);
                    (document.getElementById("dvCalculationPeriod") as HTMLDivElement).setAttribute("style", "display:block");
                    (document.getElementById("periodName") as HTMLDivElement).setAttribute("style", "display:block");
                    constants.periodTypeId = response.calculationPeriodType;
                    this.assignCalculationTypeUsed(response.calculationPeriodType);
                } else {
                    (document.getElementById("periodName") as HTMLDivElement).setAttribute("style", "display:none");
                }
                constants.isNonR2T4ApproveTabEnabled = response.isR2T4ApproveTabEnabled;
            }
        }

        // Method to convert date into MM/dd/yyyy format
        public static formatDate(inputDate: any): string {
            if (!!inputDate) {
                let date = new Date(inputDate);
                // Months use 0 index.
                return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
            }
            return "";
        }

        //----------End- Fetch------------------------//

        private setupLastDateAttendedField(studentId: string, enrollmentId: string) {
            if (studentId !== "" && enrollmentId !== "") {
                this.getEnrollmentDetails(studentId,
                    enrollmentId,
                    (response) => {
                        this.studentEnrollmentDetails = response;
                        this.enableAndDisableLastDateAttended(this.studentEnrollmentDetails);
                        return;
                    });
            }
            this.setComponentAttribute("dtLastDateAttended", "required", "false");
            this.enableDatePicker("dtLastDateAttended", false);
            (document.getElementById("spLDA") as HTMLSpanElement).textContent = "";
        }

        private enableAndDisableLastDateAttended(response: any): boolean {
            if (response !== undefined) {
                let isRequired = this.isRequiredLastDateAttended(response);
                MasterPage.destroyTooltip(this.ldaDatePicker);

                if (isRequired !== undefined) {
                    if (isRequired) {
                        this.setComponentAttribute("dtLastDateAttended", "required", "true");
                        (document.getElementById("spLDA") as HTMLSpanElement).textContent = "*";
                    } else {
                        if ($('#LDA').text() === AD.X_DATE_TYPE) {
                            (document.getElementById("spLDA") as HTMLSpanElement).textContent = "*";
                            this.setComponentAttribute("dtLastDateAttended", "required", "true");
                        } else {
                            (document.getElementById("spLDA") as HTMLSpanElement).textContent = "";
                            this.setComponentAttribute("dtLastDateAttended", "required", "false");
                        }
                    }
                    this.ldaEnabled = ($('#LDA').text() !== AD.X_DATE_TYPE || this.ldaFromAttendance === undefined || this.ldaFromAttendance === "");
                }

                return this.dateOfDeterminationValidator("#dtLastDateAttended",
                    $("#dtDateOfDetermination"),
                    this.studentEnrollmentDetails);
            } else {
                this.ldaEnabled = false;
                this.setComponentAttribute("dtLastDateAttended", "required", "false");
                (document.getElementById("spLDA") as HTMLSpanElement).textContent = "";
                return true;
            }

        }

        public initializeR2T4Input(terminationId) {
            (document.getElementById("lblStundentName") as HTMLLabelElement).textContent = this.studentName;
            (document.getElementById("lblEnrollmentName") as HTMLLabelElement).textContent = this.enrollmentName;
            (document.getElementById("lblStundentSSN") as HTMLLabelElement).textContent = this.studentSsn;
            //always set to true and uncheck for specific cases elsewhere
            $("#chkAttendance").prop("checked", true);

            this.resultsVm.isInputModified = false;
            if (terminationId !== undefined || terminationId !== "") {
                if ($("#hdnTerminationId").val() === "")
                    $("#hdnTerminationId").val(terminationId);
                this.isR2T4InputExists((result: any) => {
                    if (!!result) {
                        let studentTerminationData = {} as IFetchByEnrollId;
                        studentTerminationData.enrollmentId = $("#studentEnrollment input:radio:checked").val();
                        r2T4Termination.fetchTerminationByEnrollmentId(studentTerminationData,
                            (studentTerminationResponse: any) => {

                                r2T4Termination.fetchR2T4Inputdetails(terminationId,
                                    (response) => {
                                        if (response === undefined) {
                                            $("#hdnR2T4InputId").val(this.emptyGuid);
                                        } else {
                                            this.setR2T4InputDetails(response);
                                            if (this.isLdaChanged) {
                                                $("#dtWithdrawDate").val($("#dtLastDateAttended").val());
                                            }
                                            if (response["withdrawalDate"] === null ||
                                                response["withdrawalDate"] === "") {
                                                $("#dtWithdrawDate").val('');
                                            }
                                        }
                                    });

                                if (!!studentTerminationResponse) {
                                    if (StudentTerminationVm.formatDate($("#dtLastDateAttended").val()) !== StudentTerminationVm.formatDate(studentTerminationResponse.lastDateAttended)) {
                                        this.populatePercentageOfPeriodSection(0, null);
                                    }
                                }
                            });

                    } else {
                        this.populatePercentageOfPeriodSection(0, null);
                    }
                    this.enableOrDisablePercentageofPeriodCompleted(false);
                });
            }
            this.programUnitTypeId = this.isClockHr ? AD.ProgramUnitTypes.ClockHour : AD.ProgramUnitTypes.CreditHour;
            helpDialogBox.showDialogBox(this.isClockHr ? constants.clockHourText : constants.creditHourText, $("#percentOfPaymentHelp"));
            this.showTitle4AidHelp();
            this.showInstitutionalChargeHelp();
        }

        private showTitle4AidHelp() {
            let helpText = "";
            let payment = $("#title4AidHelp");
            helpText = constants.titleIvAidMessage;
            helpDialogBox.showDialogBox(helpText, payment);
        }

        //show message on click of Institutional Charge Help icon--
        private showInstitutionalChargeHelp() {
            let helpText = "";
            let paymentPeriod = $("#institutionalChargeHelp");
            helpText = constants.institutionalChargeMessage;
            helpDialogBox.showDialogBox(helpText, paymentPeriod);
        }

        public isClockHour(enrollmentId: string, callBackMethod: (response: Response) => any): any {
            let isClockHour: boolean = false;
            let academicCalendarType = new StudentAcademicCalendar();
            if (enrollmentId !== undefined) {
                isClockHour = academicCalendarType.isClockHour(enrollmentId, callBackMethod);
            }
            return isClockHour;
        }

        public isSupportUser(callBackMethod: (response: Response) => any): any {
            //let isSupportUser: boolean = false;
            let userRoleStatus = new UserRoleStatusCheck();
            let isSupportUser = userRoleStatus.isSupportUser(callBackMethod);
            return isSupportUser;
        }

        //checking the selected calculation period type is Payment period or not and
        //setting the selected calculation period type in R2T4Results tab.
        public isPaymentPeriod(periodTypeId: string, callBackMethod: (response: Response) => any): any {
            let academicCalendarType = new StudentAcademicCalendar();
            if (periodTypeId !== undefined) {
                academicCalendarType.isPaymentPeriod(periodTypeId, (isPaymentPeriod) => {
                    if (isPaymentPeriod) {
                        $("#rdbPayPeriod").prop("checked", "true");
                        $("#rdbPeriodEnroll").attr("disabled", "true");
                    } else {
                        $("#rdbPeriodEnroll").prop("checked", "true");
                        $("#rdbPayPeriod").attr("disabled", "true");
                    }
                });
            }
        }

        public studentSearchValidation() {
            $("#txtStudentSearch").on("propertychange change keyup paste input", () => {
                $("#txtStudentSearch").removeClass("valueRequired");
            });
        }

        public clearSearchResult() {
            $("#txtStudentSearch").keyup(() => {
                try {
                    let studentEnrollmentId: string;
                    if ($("#hdnTerminationId").val() !== this.emptyGuid &&
                        $("#hdnTerminationId").val() !== "" &&
                        this.previousEnrollmentId !== undefined &&
                        $("#txtStudentSearch").val() === "" &&
                        this.dateValidation &&
                        !this.onTextChange) {
                        studentEnrollmentId = this.previousEnrollmentId;
                        let terminationModel = {} as Models.IStudentTermination;
                        let terminationData = this.generateR2T4TerminationModel(terminationModel);
                        terminationData.studentEnrollmentId = studentEnrollmentId;
                        this.saveR2T4TerminationDetails(terminationData,
                            $("#hdnTerminationId").val(),
                            (response) => {
                                if (response["resultStatus"] === AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                    $("#hdnTerminationId").val("");
                                    this.clearEnrollmentsOnBlur(true);
                                } else {
                                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_FAIL.replace("$StundentName", constants.studentName).replace("$EnrollmentName", constants.enrollmentName));
                                    return;
                                }
                            });
                    } else if ((document.getElementById("txtStudentSearch") as HTMLInputElement).value === "") {
                        $("#hdnTerminationId").val("");
                        this.clearEnrollmentsOnBlur(false);
                        this.onTextChange = false;
                    }
                    if ($("#txtStudentSearch").val().length === 0 || $("#hdnTerminationId").val() === "") {
                        this.setTerminationDetailsActive(true);
                    }
                } catch (e) {
                    console.error(e);
                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_FAIL.replace("$StundentName", constants.studentName).replace("$EnrollmentName", constants.enrollmentName));
                    return;
                }
            });
        }

        public clearSearchResultOnClearText() {
            $("#txtStudentSearch").on("input", () => {
                try {
                    let studentEnrollmentId: string;
                    if ($("#hdnTerminationId").val() !== this.emptyGuid &&
                        $("#hdnTerminationId").val() !== "" &&
                        this.previousEnrollmentId !== undefined &&
                        this.dateValidation &&
                        $("#txtStudentSearch").val().length === 1) {
                        this.onTextChange = true;
                        studentEnrollmentId = this.previousEnrollmentId;
                        let terminationModel = {} as Models.IStudentTermination;
                        let terminationData = this.generateR2T4TerminationModel(terminationModel);
                        terminationData.studentEnrollmentId = studentEnrollmentId;
                        this.saveR2T4TerminationDetails(terminationData,
                            $("#hdnTerminationId").val(),
                            (response) => {
                                if (response["resultStatus"] === AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                    $("#hdnTerminationId").val("");
                                    this.clearEnrollmentsOnBlur(true);
                                } else {
                                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_FAIL.replace("$StundentName", constants.studentName).replace("$EnrollmentName", constants.enrollmentName));
                                    return;
                                }
                            });
                    } else {
                        if ($("#txtStudentSearch").val().length === 0 &&
                            $("#hdnTerminationId").val() !== this.emptyGuid)
                            this.clearEnrollmentsOnBlur(true);
                        else {
                            if ($("#hdnTerminationId").val() !== this.emptyGuid && $("#hdnTerminationId").val() !== "" && this.previousStudentName !== undefined && this.previousProgramName !== undefined) {
                                this.previousEnrollmentId = undefined;
                                let enrollmentSaveMessage = `Termination record for ${this.previousStudentName}  and  ${this.previousProgramName} is saved`;
                                MasterPage.SHOW_INFO_WINDOW(enrollmentSaveMessage);
                            }
                            this.clearEnrollmentsOnBlur(false);
                        }
                    }
                } catch (e) {
                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_FAIL.replace("$StundentName", constants.studentName).replace("$EnrollmentName", constants.enrollmentName));
                    return;
                }
            });
        }

        private clearSearchOnBlur() {
            $("span.k-icon.k-clear-value.k-i-close").click(() => {
                try {
                    let studentEnrollmentId: string;
                    if ($("#hdnTerminationId").val() !== this.emptyGuid &&
                        $("#hdnTerminationId").val() !== "" &&
                        this.previousEnrollmentId !== undefined &&
                        this.dateValidation) {
                        studentEnrollmentId = this.previousEnrollmentId;
                        let terminationModel = {} as Models.IStudentTermination;
                        let terminationData = this.generateR2T4TerminationModel(terminationModel);
                        terminationData.studentEnrollmentId = studentEnrollmentId;
                        this.saveR2T4TerminationDetails(terminationData,
                            $("#hdnTerminationId").val(),
                            (response) => {
                                if (response["resultStatus"] === AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                    $("#hdnTerminationId").val("");
                                    this.clearEnrollmentsOnBlur(true);
                                } else {
                                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_FAIL.replace("$StundentName", constants.studentName).replace("$EnrollmentName", constants.enrollmentName));
                                    return;
                                }
                            });
                    } else {
                        $("#hdnTerminationId").val("");
                        this.clearEnrollmentsOnBlur(false);
                    }
                    this.setTerminationDetailsActive(true);
                } catch (e) {
                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_FAIL.replace("$StundentName", constants.studentName).replace("$EnrollmentName", constants.enrollmentName));
                    return;
                }
            });
        }

        public clearSearchResultOnEnrollment(isOnEnrollmentClear: boolean = false) {
            if (isOnEnrollmentClear && this.previousStudentName !== undefined && this.previousProgramName !== undefined) {
                this.previousEnrollmentId = undefined;
                let enrollmentSaveMessage = `Termination record for ${this.previousStudentName}  and  ${this.previousProgramName} is saved`;
                MasterPage.SHOW_INFO_WINDOW(enrollmentSaveMessage);
                $("#liR2T4Input").removeClass("is-active").removeClass("is-visited");
                $("#liR2T4Result").removeClass("is-active").removeClass("is-visited");
                $("#liApproveTermination").removeClass("is-active");
            }
            let status: any = $("#ddlStatus").data("kendoDropDownList");
            if (status !== undefined && status !== null) {
                status.select(0);
            }

            let dropreason: any = $("#ddlDropReason").data("kendoDropDownList");
            if (dropreason !== undefined && dropreason !== null) {
                dropreason.select(0);
            }

            $("#dtDateOfDetermination").val("");
            $("#dtLastDateAttended").val("");
            $(".k-dropdown .k-dropdown-wrap>span").removeClass("valueRequired");
            $("#dtDateOfDetermination").removeClass("valueRequired");
            $("#dtLastDateAttended").removeClass("valueRequired");

            (document.getElementById("spLDA") as HTMLSpanElement).textContent = "";
            MasterPage.destroyTooltip(this.ldaDatePicker);
            MasterPage.destroyTooltip($("#dtDateOfDetermination"));
            this.enableDatePicker("dtLastDateAttended", false);
            this.studentEnrollmentDetails = undefined;

            (document.getElementById("chkClaculationPeriod") as HTMLInputElement).checked = false;
            (document.getElementById("chkClaculationPeriod") as HTMLInputElement).disabled = true;
            $("#dvCalculationPeriod").attr("style", "display:none");
        }

        //--This function is called when the student name is changed or cleared --//
        private clearEnrollmentsOnBlur(isOnEnrollmentClear: boolean) {
            (document.getElementById("dvEnrollment") as HTMLDivElement).setAttribute("style", "display: none");
            (document.getElementById("dvCalculationPeriod") as HTMLDivElement).setAttribute("style",
                "display:none");
            $("#studentEnrollment").empty();
            $("#spnSave").addClass("disabled");
            $("#spnCancel").addClass("disabled");
            if (!isOnEnrollmentClear)
                $("#hdnTerminationId").val(this.emptyGuid);
            this.disableCancelTermination();
            this.clearSearchResultOnEnrollment(isOnEnrollmentClear);
        }

        //*Student's Percentage of Period Completed' section validation under R2T4 Inputs screen on Student Termination screen-Clock/Credit Hour Program*//
        public bindR2T4InputData(terminationId: string) {
            let elementAttribute = $("#dtLastDateAttended").prop("required");
            this.isClockHour(this.enrollmentId,
                (response) => {
                    this.isClockHr = response ? true : false;
                    this.clearR2T4InputData();
                    if (response) {
                        $("#rdbClock").prop("checked", true);
                        $("#rdbCredit").prop("disabled", true);
                        $("#startDate").attr("style", "display:none");
                        $("#endDate").attr("style", "display:none");
                        $("#dvAttendanceRequired").attr("style", "display:none");
                        $("#startDate").val("");
                        $("#endDate").val("");

                        this.enableDatePicker("dtWithdrawDate", true);
                        $("#txtTotalHours").prop("disabled", false);
                        $("#txtScheduledHours").prop("disabled", false);
                        $("#txtTotalHours").removeClass("txtDisable");
                        $("#txtScheduledHours").removeClass("txtDisable");

                        this.setComponentAttribute("dtWithdrawDate", "required", "true");
                        this.setComponentAttribute("txtScheduledHours", "required", "true");
                        this.setComponentAttribute("txtTotalHours", "required", "true");
                        this.setComponentAttribute("dtStartDate", "required", "false");
                        this.setComponentAttribute("dtEndDate", "required", "false");

                        $("#lblHoursPeriod").prop("textContent", "Total hours in period");
                        $("#lblHoursCompleted").prop("textContent", "Hours scheduled to complete");
                        $("#spHoursCompleted, #spHoursPeriod, #spWithdrawDt").prop("textContent", "*");
                        $("#dtWithdrawDate").val($("#dtLastDateAttended").val());
                        if (elementAttribute) {
                            this.enableDatePicker("dtWithdrawDate", true);
                        } else {
                            $("#txtTotalHours, #txtScheduledHours, #dtWithdrawDate").removeClass("txtDisable");
                            $("#txtTotalHours, #txtScheduledHours").prop("disabled", false);
                            this.enableDatePicker("dtWithdrawDate", true);
                        }
                    }
                    else {
                        $("#rdbCredit").prop("checked", true);
                        $("#rdbClock").prop("disabled", true);
                        $("#startDate").attr("style", "display:block");
                        $("#endDate").attr("style", "display:block");
                        $("#dvAttendanceRequired").attr("style", "display:block");
                        $("#lblHoursCompleted").prop("textContent", "Completed days");
                        $("#lblHoursPeriod").prop("textContent", "Total days");
                        if (elementAttribute) {
                            (document.getElementById("chkAttendance") as HTMLInputElement).checked = false;
                            (document.getElementById("chkAttendance") as HTMLInputElement).disabled = false;
                            this.setComponentAttribute("dtWithdrawDate", "required", "true");
                            (document.getElementById("spStartDt") as HTMLSpanElement).textContent = "*";
                            (document.getElementById("spEndDt") as HTMLSpanElement).textContent = "*";
                            $("#lblHoursPeriod").prop("textContent", "Total days");
                            $("#lblHoursCompleted").prop("textContent", "Completed days");
                            $("#spHoursCompleted, #spHoursPeriod, #spWithdrawDt").prop("textContent", "*");
                            this.setupPercentageOfPeriodSection();
                            $("#dtWithdrawDate").val($("#dtLastDateAttended").val());
                            this.enableDatePicker("dtWithdrawDate", true);
                            this.setComponentAttribute("dtWithdrawDate", "required", "true");
                            if ($("#dtWithdrawDate").val() !== "") {
                                $("#chkAttendance").prop("checked", false);
                                $("#chkAttendance").prop("disabled", true);
                            }
                        }
                        else {
                            this.setComponentAttribute("dtStartDate", "required", "true");
                            this.setComponentAttribute("dtEndDate", "required", "true");

                            (document.getElementById("spStartDt") as HTMLSpanElement).textContent = "*";
                            (document.getElementById("spEndDt") as HTMLSpanElement).textContent = "*";

                            $("#spHoursCompleted, #spHoursPeriod, #spWithdrawDt").prop("textContent", "");

                            (document.getElementById("chkAttendance") as HTMLInputElement).checked = true;
                            (document.getElementById("chkAttendance") as HTMLInputElement).disabled = false;

                           // $("#txtTotalHours").addClass("txtDisable");
                           // $("#txtScheduledHours").addClass("txtDisable");

                            this.setupPercentageOfPeriodSection();
                            $("#dtWithdrawDate").val($("#dtLastDateAttended").val());
                        }
                    }
                    this.initializeR2T4Input(terminationId);
                });
        }

        //*Initializing the Attendance checkbox*//
        private initializeAttendance() {
            $("#chkAttendance").click(() => {
                if ((document.getElementById("chkAttendance") as HTMLInputElement).checked) {
                    (document.getElementById("txtTotalHours") as HTMLInputElement).disabled = true;
                    (document.getElementById("txtScheduledHours") as HTMLInputElement).disabled = true;
                   // $("#txtTotalHours").addClass("txtDisable");
                   // $("#txtScheduledHours").addClass("txtDisable");
                    $("#txtTotalHours").val("");
                    $("#txtScheduledHours").val("");
                   // $("#dtWithdrawDate").addClass("txtDisable");

                    $("#lblHoursPeriod").prop("textContent", "Total days");
                    $("#lblHoursCompleted").prop("textContent", "Completed days");
                    $("#spHoursCompleted, #spHoursPeriod, #spWithdrawDt").prop("textContent", "");

                    this.enableDatePicker("dtWithdrawDate", false);
                    this.setupPercentageOfPeriodSection();
                } else {
                    (document.getElementById("txtTotalHours") as HTMLInputElement).disabled = false;
                    (document.getElementById("txtScheduledHours") as HTMLInputElement).disabled = false;
                    $("#txtTotalHours").removeClass("txtDisable");
                    $("#txtScheduledHours").removeClass("txtDisable");
                    $("#dtWithdrawDate").removeClass("txtDisable");
                    (document.getElementById("spStartDt") as HTMLSpanElement).textContent = "*";
                    (document.getElementById("spEndDt") as HTMLSpanElement).textContent = "*";
                    (document.getElementById("spWithdrawDt") as HTMLSpanElement).textContent = "*";

                    $("#lblHoursPeriod").prop("textContent", "Total days");
                    $("#lblHoursCompleted").prop("textContent", "Completed days");
                    $("#spHoursCompleted, #spHoursPeriod, #spWithdrawDt").prop("textContent", "*");

                    this.enableDatePicker("dtWithdrawDate", true);
                    this.setupPercentageOfPeriodSection();
                }
            });
        }

        //this function will setup the R2T4 result UI override mode and enabled all the input for modification.
        private onOverrideCheckBoxChecked() {
            $("#divR2T4Result").find("input:text").each((index, element) => {
                $(element).prop("readonly", false);
            });

            this.resultsVm.enablePwdControls();

            this.isClockHour(this.enrollmentId,
                (response) => {
                    this.isClockHr = response ? true : false;
                    if (this.isClockHr) {
                        $("#divCalender").show();
                        $("#divDate").hide();
                    } else {
                        $("#dvcalStartDateResult").show();
                        $("#dvStartDateResult").hide();
                        $("#dvCalEndDateResult").show();
                        $("#dvEndDateResult").hide();
                        $("#divCalender").show();
                        $("#divCalender").prop("enabled", true);
                        $("#divDate").hide();
                        $("#chkNAttend").prop("disabled", false);
                        $("#tblNoAttend").show();
                    }
                });

            var dtwithDraw = $("#txt2CreditWithdrawalDate").val();
            $("#cal2CreditWithdrawalDateEnabled").val(dtwithDraw);

            var dtStart = $("#txtStartDate").val();
            $("#calStartDate").val(dtStart);

            var dtEnd = $("#txtEndDate").val();
            $("#calEndDate").val(dtEnd);
        }
        //Checking override conditions
        public initializeOverideCheck() {
            $("#ChkOverride").click(() => {
                if ((document.getElementById("ChkOverride") as HTMLInputElement).checked) {
                    this.onOverrideCheckBoxChecked();
                } else {
                    $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(AD.X_MESSAGE_R2T4_RESULT_OVERRIDE_DISCARD))
                        .then(confirmed => {
                            if (confirmed) {
                                $("#txtTicket").val("");

                                let terminationId = $("#hdnTerminationId").val();
                                r2T4Termination.deleteR2T4ResultByTerminationId({ "terminationId": terminationId }, () => {
                                    $("#divR2T4Result").find("input:text").each((index, element) => {
                                        $(element).val("");
                                        $(element).prop("readonly", true);
                                    });
                                    this.resultsVm.disablePwdControls();
                                    this.isClockHour(this.enrollmentId,
                                        (response) => {
                                            this.isClockHr = response ? true : false;
                                            if (response) {
                                                $("#divCalender").show();
                                                $("#divDate").hide();
                                            } else {
                                                $("#dvcalStartDateResult").hide();
                                                $("#dvStartDateResult").show();
                                                $("#dvCalEndDateResult").hide();
                                                $("#dvEndDateResult").show();
                                                $("#divCalender").hide();
                                                $("#divDate").show();
                                                $("#chkNAttend").prop("disabled", true);
                                                $("#tblNoAttend").show();
                                            }
                                        }
                                    );
                                    r2T4Termination.fetchR2T4Inputdetails(terminationId,
                                        (r2T4InputDetail) => {
                                            if (!!r2T4InputDetail) {
                                                var r2T4InputResult = this.getR2T4InputResult(r2T4InputDetail);
                                                this.initializeNewR2T4Result(r2T4InputResult, false, false, false);
                                            }
                                        });
                                    constants.isR2T4ResultsExists = constants.isR2T4OverrideResultsExists = false;
                                });
                            } else {
                                $("#ChkOverride").prop("checked", true);
                                this.onOverrideCheckBoxChecked();
                            }
                        }
                        );
                }
            });
        }

        //This function will format the values for multiple fields and return the r2T4InputDetail back.
        public getR2T4InputResult(r2T4InputDetail: any): any {
            if (!!r2T4InputDetail) {
                r2T4InputDetail.pellGrantDisbursed = (!!r2T4InputDetail.pellGrantDisbursed) ?
                    this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.pellGrantDisbursed)) : "";
                r2T4InputDetail.pellGrantCouldDisbursed = (!!r2T4InputDetail.pellGrantCouldDisbursed)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.pellGrantCouldDisbursed)) : "";
                r2T4InputDetail.fseogDisbursed = (!!r2T4InputDetail.fseogDisbursed)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.fseogDisbursed)) : "";
                r2T4InputDetail.fseogCouldDisbursed = (!!r2T4InputDetail.fseogCouldDisbursed)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.fseogCouldDisbursed)) : "";
                r2T4InputDetail.teachGrantDisbursed = (!!r2T4InputDetail.teachGrantDisbursed)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.teachGrantDisbursed)) : "";
                r2T4InputDetail.teachGrantCouldDisbursed = (!!r2T4InputDetail.teachGrantCouldDisbursed)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.teachGrantCouldDisbursed)) : "";
                r2T4InputDetail.iraqAfgGrantDisbursed = (!!r2T4InputDetail.iraqAfgGrantDisbursed)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.iraqAfgGrantDisbursed)) : "";
                r2T4InputDetail.iraqAfgGrantCouldDisbursed = (!!r2T4InputDetail.iraqAfgGrantCouldDisbursed)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.iraqAfgGrantCouldDisbursed)) : "";
                r2T4InputDetail.unsubLoanNetAmountDisbursed = (!!r2T4InputDetail.unsubLoanNetAmountDisbursed)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.unsubLoanNetAmountDisbursed)) : "";
                r2T4InputDetail.unsubLoanNetAmountCouldDisbursed = (!!r2T4InputDetail.unsubLoanNetAmountCouldDisbursed)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.unsubLoanNetAmountCouldDisbursed)) : "";
                r2T4InputDetail.subLoanNetAmountDisbursed = (!!r2T4InputDetail.subLoanNetAmountDisbursed)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.subLoanNetAmountDisbursed)) : "";
                r2T4InputDetail.subLoanNetAmountCouldDisbursed = (!!r2T4InputDetail.subLoanNetAmountCouldDisbursed)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.subLoanNetAmountCouldDisbursed)) : "";
                r2T4InputDetail.perkinsLoanDisbursed = (!!r2T4InputDetail.perkinsLoanDisbursed)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.perkinsLoanDisbursed)) : "";
                r2T4InputDetail.perkinsLoanCouldDisbursed = (!!r2T4InputDetail.perkinsLoanCouldDisbursed)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.perkinsLoanCouldDisbursed)) : "";
                r2T4InputDetail.directGraduatePlusLoanDisbursed = (!!r2T4InputDetail.directGraduatePlusLoanDisbursed)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.directGraduatePlusLoanDisbursed)) : "";
                r2T4InputDetail.directGraduatePlusLoanCouldDisbursed = (!!r2T4InputDetail.directGraduatePlusLoanCouldDisbursed)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.directGraduatePlusLoanCouldDisbursed)) : "";
                r2T4InputDetail.directParentPlusLoanDisbursed = (!!r2T4InputDetail.directParentPlusLoanDisbursed)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.directParentPlusLoanDisbursed)) : "";
                r2T4InputDetail.directParentPlusLoanCouldDisbursed = (!!r2T4InputDetail.directParentPlusLoanCouldDisbursed)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.directParentPlusLoanCouldDisbursed)) : "";
                r2T4InputDetail.tuitionFee = (!!r2T4InputDetail.tuitionFee)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.tuitionFee)) : "";
                r2T4InputDetail.roomFee = (!!r2T4InputDetail.roomFee)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.roomFee)) : "";
                r2T4InputDetail.boardFee = (!!r2T4InputDetail.boardFee)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.boardFee)) : "";
                r2T4InputDetail.otherFee = (!!r2T4InputDetail.otherFee)
                    ? this.formatNumberAsCurrency(parseFloat(r2T4InputDetail.otherFee)) : "";
            }

            return r2T4InputDetail;
        }

        //*Checking the empty conditions for total hours*//
        private checkValueTotal() {
            $("#txtTotalHours").keyup(() => {
                if ($("#dtWithdrawDate").val() === "" && $("#txtScheduledHours").val() === "" && $("#txtTotalHours").val() === "") {
                    (document.getElementById("chkAttendance") as HTMLInputElement).disabled = false;
                } else {
                    (document.getElementById("chkAttendance") as HTMLInputElement).disabled = true;
                }
            });
        }

        //*Checking the empty conditions for scheduled hours*//
        private checkValueCompleted() {
            $("#txtScheduledHours").keyup(() => {
                if ($("#dtWithdrawDate").val() === "" && $("#txtScheduledHours").val() === "" && $("#txtTotalHours").val() === "") {
                    (document.getElementById("chkAttendance") as HTMLInputElement).disabled = false;
                } else {
                    (document.getElementById("chkAttendance") as HTMLInputElement).disabled = true;
                }
            });
        }

        //*Checking the empty conditions for Attendance chekbox*//
        private checkWithDrawDateOnChange() {
            this.createDatePicker("dtWithdrawDate");
            $("#dtWithdrawDate").change(() => {
                if (this.withdrawalDateValidator("#dtWithdrawDate", $("#dtWithdrawDate"))) {
                    if ($("#dtWithdrawDate").val() === "" && $("#txtScheduledHours").val() === "" && $("#txtTotalHours").val() === "") {
                        $("#chkAttendance").prop("disabled", false);
                    } else {
                        $("#chkAttendance").prop("disabled", true);
                    }
                }
            });
        }

        //*Restricting from entering only zeros*//
        public restrictOnlyZeros(): void {
            $("#txtScheduledHours").change(() => {
                MasterPage.destroyTooltip($("#txtScheduledHours"));
                if (parseInt($("#txtScheduledHours").val()) === 0) {
                    MasterPage.createTooltip(this.isClockHr ? AD.X_MESAGE_R2T4Input_ZERO_VAL_SCHEDULED : AD.X_MESAGE_R2T4Input_ZERO_VAL_COMPLETED_DAYS, $("#txtScheduledHours"));
                }
                else if ($("#txtTotalHours").val() !== undefined &&
                    $("#txtTotalHours").val() !== null &&
                    $("#txtTotalHours").val() !== "" &&
                    parseInt($("#txtScheduledHours").val()) > parseInt($("#txtTotalHours").val())) {
                    MasterPage.createTooltip(this.isClockHr ? AD.X_MESAGE_R2T4Input_TOTALHRS_LESSTHAN_COMPLETEDHRS : AD.X_MESAGE_R2T4Input_TOTALDAYS_LESSTHAN_COMPLETEDDAYS, $("#txtScheduledHours"));
                } else {
                    MasterPage.destroyTooltip($("#txtScheduledHours"));
                    if (parseInt($("#txtTotalHours").val()) > 0)
                        MasterPage.destroyTooltip($("#txtTotalHours"));
                }
            });

            $("#txtTotalHours").change(() => {
                MasterPage.destroyTooltip($("#txtTotalHours"));
                if (parseInt($("#txtTotalHours").val()) === 0) {
                    MasterPage.createTooltip(this.isClockHr ? AD.X_MESAGE_R2T4Input_ZERO_VAL_TOTAL : AD.X_MESAGE_R2T4Input_ZERO_VAL_TOTAL_DAYS, $("#txtTotalHours"));
                }
                else if (parseInt($("#txtTotalHours").val()) < parseInt($("#txtScheduledHours").val())) {
                    MasterPage.createTooltip(this.isClockHr ? AD.X_MESAGE_R2T4Input_TOTALHRS_LESSTHAN_COMPLETEDHRS : AD.X_MESAGE_R2T4Input_TOTALDAYS_LESSTHAN_COMPLETEDDAYS, $("#txtTotalHours"));
                } else {
                    MasterPage.destroyTooltip($("#txtTotalHours"));
                    if (parseInt($("#txtScheduledHours").val()) > 0)
                        MasterPage.destroyTooltip($("#txtScheduledHours"));
                }
            });
        }

        //*Validating start date with end date*//
        private checkStartDateOnChange() {
            this.createDatePicker("dtStartDate");
            $("#dtStartDate").change(() => {
                if (this.startDateValidator("#dtStartDate", $("#dtStartDate"))) {
                    this.scheduleEndDateValidator("#dtEndDate", $("#dtEndDate"));
                    this.withdrawalDateValidator("#dtWithdrawDate", $("#dtWithdrawDate"));
                }
            });
        }

        //*Checking end date*//s
        private checkEndDateOnChange() {
            this.createDatePicker("dtEndDate");
            $("#dtEndDate").change(() => {
                if (this.scheduleEndDateValidator("#dtEndDate", $("#dtEndDate"))) {
                    this.startDateValidator("#dtStartDate", $("#dtStartDate"));
                    this.withdrawalDateValidator("#dtWithdrawDate", $("#dtWithdrawDate"));
                }
            });
        }

        //*Validating End date with start date*//
        private scheduleEndDateValidator(componentId: string, input: any): boolean {
            MasterPage.destroyTooltip(input);
            if (MasterPage.cutomDateFormatValidation(input)) {
                if ($("#dtEndDate").val() !== "") {
                    let startDate = kendo.parseDate($("#dtStartDate").val());
                    let endDate = kendo.parseDate($("#dtEndDate").val());
                    let startDt = "' " + this.getFormattedDate(startDate) + " '";
                    if (endDate < this.enrollmentStartDate) {
                        MasterPage.createTooltip(AD.X_MESSAGE_ENDDATE_LESS_THAN_STARTDATE.replace("[date]", startDt) + "' " + this.getFormattedDate(this.enrollmentStartDate) + " '", input);
                        this.dateValidation = false;
                        return this.dateValidation;
                    }
                    if (endDate <= startDate) {
                        MasterPage.createTooltip(AD.X_MESSAGE_ENDDATE_LESS_THAN_STARTDATE.replace("[date]", startDt) + "' " + this.getFormattedDate(this.enrollmentStartDate) + " '", input);
                        this.dateValidation = false;
                        return this.dateValidation;
                    }
                }
            }
            this.dateValidation = true;
            return this.dateValidation;
        }

        //*Validating Withdrawal date field with LDA and Scheduled end date*//
        private withdrawalDateValidator(componentId: string, input: any): boolean {
            MasterPage.destroyTooltip(input);
            if (MasterPage.cutomDateFormatValidation(input)) {
                if ($("#dtWithdrawDate").val() !== "") {
                    let withdrawalDate = kendo.parseDate($("#dtWithdrawDate").val());
                    if (withdrawalDate !== undefined && withdrawalDate !== null) {
                        if (this.enrollmentStartDate !== undefined && this.enrollmentStartDate !== null
                            && withdrawalDate < this.enrollmentStartDate) {
                            MasterPage.createTooltip(AD.X_MESSAGE_WD_GREATER_THAN_ENROLLMENTDATE + "' " + this.getFormattedDate(this.enrollmentStartDate) + " '", input);
                            this.dateValidation = false;
                            return this.dateValidation;
                        }

                        if ($("#dtDateOfDetermination").val() !== "") {
                            if (withdrawalDate > kendo.parseDate($("#dtDateOfDetermination").val())) {
                                MasterPage.createTooltip(AD.X_MESSAGE_WD_LESS_THAN_OR_EQUAL_TO_DOD + "' " + this.getFormattedDate(kendo.parseDate($("#dtDateOfDetermination").val())) + " '", input);
                                this.dateValidation = false;
                                return this.dateValidation;
                            }
                        }

                        if ($("#dtStartDate").val() !== "") {
                            let startDate = kendo.parseDate($("#dtStartDate").val());
                            if (startDate !== undefined && startDate !== null && withdrawalDate < startDate && this.isClockHr === false) {
                                MasterPage.createTooltip(AD.X_MESSAGE_WD_GREATER_THAN_STARTDATE + "' " + this.getFormattedDate(startDate) + " '", input);
                                this.dateValidation = false;
                                return this.dateValidation;
                            }
                        }

                        if ($("#dtEndDate").val() !== "") {
                            let scheduledEndDate = kendo.parseDate($("#dtEndDate").val());
                            if (scheduledEndDate !== undefined && scheduledEndDate !== null && withdrawalDate >= scheduledEndDate && this.isClockHr === false) {
                                MasterPage.createTooltip(AD.X_MESSAGE_WD_GREATER_THAN_SCHEDULEDENDDATE + "' " + this.getFormattedDate(scheduledEndDate) + " '", input);
                                this.dateValidation = false;
                                return this.dateValidation;
                            }
                        }
                    }
                }
                this.dateValidation = true;
                return this.dateValidation;
            }
            return this.dateValidation;
        }

        //*Validating End date with start date*//
        private startDateValidator(componentId: string, input: any): boolean {
            MasterPage.destroyTooltip(input);
            if (MasterPage.cutomDateFormatValidation(input)) {
                let startDate = kendo.parseDate($("#dtStartDate").val());
                let endDate = kendo.parseDate($("#dtEndDate").val());
                if ($("#dtStartDate").val() !== "" && startDate != undefined) {
                    if (startDate < this.enrollmentStartDate) {
                        MasterPage.createTooltip(AD.X_MESSAGE_STARTDATE_GREATER_THAN_ENROLLMENTSTARTDATE + "' " + this.getFormattedDate(this.enrollmentStartDate) + " '", input);
                        this.dateValidation = false;
                        return this.dateValidation;
                    }
                }
                if ($("#dtStartDate").val() !== "" && $("#dtEndDate").val() !== "") {
                    if (startDate >= endDate) {
                        MasterPage.createTooltip(AD.X_MESSAGE_STARTDATE_LESS_THAN_ENDDATE + "' " + this.getFormattedDate(endDate) + " '", input);
                        this.dateValidation = false;
                        return this.dateValidation;
                    }
                }
            }
            this.dateValidation = true;
            return this.dateValidation;
        }

        //Ability to specify information required to calculate percentage of period calculated
        //under 'Student's Percentage of Period Completed' section on 'R2T4 Input' tab-Credit Hour Program
        public setupPercentageOfPeriodSection() {
            if ($("#chkAttendance").prop("checked")) {
                if (this.unitTypeDescription !== undefined
                    && this.unitTypeDescription.toLowerCase() === "none"
                    && !this.isClockHr
                    && $("#chkClaculationPeriod").prop("checked")) {
                    this.enableDatePicker("dtStartDate", true);
                    this.enableDatePicker("dtEndDate", true);

                    this.setComponentAttribute("dtStartDate", "required", "true");
                    this.setComponentAttribute("dtEndDate", "required", "true");

                    $("#txtTotalHours, #txtScheduledHours").prop("disabled", true);
                  //  $("#txtTotalHours, #txtScheduledHours").addClass("txtDisable");
                    this.enableDatePicker("dtWithdrawDate", false);

                    this.setComponentAttribute("txtScheduledHours", "required", "false");
                    this.setComponentAttribute("txtTotalHours", "required", "false");
                    this.setComponentAttribute("dtWithdrawDate", "required", "false");
                }
            } else {
                if (this.unitTypeDescription !== undefined
                    && this.unitTypeDescription.toLowerCase() !== "none"
                    && !this.isClockHr
                    && $("#chkClaculationPeriod").prop("checked")) {
                    $("#txtTotalHours, #txtScheduledHours").prop("disabled", false);
                    $("#txtTotalHours, #txtScheduledHours").removeClass("txtDisable");
                    this.setComponentAttribute("txtScheduledHours", "required", "true");
                    this.setComponentAttribute("txtTotalHours", "required", "true");

                    this.enableDatePicker("dtWithdrawDate", true);
                    this.enableDatePicker("dtStartDate", true);
                    this.enableDatePicker("dtEndDate", true);

                    this.setComponentAttribute("dtWithdrawDate", "required", "true");
                    this.setComponentAttribute("dtStartDate", "required", "true");
                    this.setComponentAttribute("dtEndDate", "required", "true");
                }
            }
        }

        public calculatePercentageOfPeriodCalculated() {
            if ($("#chkAttendance").prop("checked")) {
                if (this.unitTypeDescription !== undefined
                    && this.unitTypeDescription.toLowerCase() === "none"
                    && !this.isClockHr
                    && $("#chkClaculationPeriod").prop("checked")) {
                    this.enableDatePicker("dtStartDate", true);
                    this.enableDatePicker("dtEndDate", true);

                    this.setComponentAttribute("dtStartDate", "required", "true");
                    this.setComponentAttribute("dtEndDate", "required", "true");

                    $("#txtTotalHours, #txtScheduledHours").prop("disabled", true);
                   // $("#txtTotalHours, #txtScheduledHours").addClass("txtDisable");
                    this.enableDatePicker("dtWithdrawDate", false);

                    this.setComponentAttribute("txtScheduledHours", "required", "false");
                    this.setComponentAttribute("txtTotalHours", "required", "false");
                    this.setComponentAttribute("dtWithdrawDate", "required", "false");
                }
            } else {
                if (this.unitTypeDescription !== undefined
                    && this.unitTypeDescription.toLowerCase() !== "none"
                    && !this.isClockHr
                    && $("#chkClaculationPeriod").prop("checked")) {
                    $("#txtTotalHours, #txtScheduledHours").prop("disabled", false);
                    $("#txtTotalHours, #txtScheduledHours").removeClass("txtDisable");
                    this.setComponentAttribute("txtScheduledHours", "required", "true");
                    this.setComponentAttribute("txtTotalHours", "required", "true");

                    this.enableDatePicker("dtWithdrawDate", true);
                    this.enableDatePicker("dtStartDate", true);
                    this.enableDatePicker("dtEndDate", true);

                    this.setComponentAttribute("dtWithdrawDate", "required", "true");
                    this.setComponentAttribute("dtStartDate", "required", "true");
                    this.setComponentAttribute("dtEndDate", "required", "true");
                }
            }
        }

        //-- This method saves the R2T4 Student Termination details into database when the enrollment or the student name is changed. --//
        private saveR2T4TerminationDetails(terminationData: any, terminationId: string, callBack: (response: Response) => any): any {
            try {
                if (this.dateValidation) {
                    let r2T4Termination = new Api.R2T4StudentTermination();
                    terminationData.isR2T4ApproveTabEnabled = constants.isNonR2T4ApproveTabEnabled;
                    if (terminationId !== this.emptyGuid) {
                        terminationData.methodType = Number(constants.MethodType.Save);
                        r2T4Termination.updateTermination(terminationData,
                            (response: any) => {
                                $("#hdnTerminationId").val(response["terminationId"]);
                                return callBack(response);
                            });
                    } else {
                        terminationData.methodType = Number(constants.MethodType.Save);
                        r2T4Termination.saveTermination(terminationData,
                            (response: any) => {
                                $("#hdnTerminationId").val(response["terminationId"]);
                                return callBack(response);
                            });
                    }
                }
            } catch (e) {
                MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_FAIL.replace("$StundentName", constants.studentName).replace("$EnrollmentName", constants.enrollmentName));
                return;
            }
        }

        //-- This method gets the details from the input elements in Termination Details tab and assigns data to the model. --//
        private generateR2T4TerminationModel(terminationModel: any): any {
            let isNewTermination: boolean = $("#hdnTerminationId").val() === undefined || $("#hdnTerminationId").val() === "";
            terminationModel.studentEnrollmentId = $("input[name='studentEnrollment']:checked").val();
            terminationModel.statusCodeId = $("#ddlStatus").val() === "" ? null : $("#ddlStatus").val();
            terminationModel.dateWithdrawalDetermined = $("#dtDateOfDetermination").val() === "" ? null : new Date($("#dtDateOfDetermination").val()).toDateString();
            terminationModel.lastDateAttended = $("#dtLastDateAttended").val() === "" ? null : new Date($("#dtLastDateAttended").val()).toDateString();
            terminationModel.calculationPeriodType = constants.periodTypeId = $("input[id='chkClaculationPeriod']").prop("checked") ? $("input[name='periodName']:checked")[0].id : null;
            terminationModel.dropReasonId = $("#ddlDropReason").val() === "" ? null : $("#ddlDropReason").val();
            terminationModel.isPerformingR2T4Calculator = $("input[id='chkClaculationPeriod']").prop("checked");
            terminationModel.terminationId = isNewTermination ? this.emptyGuid : $("#hdnTerminationId").val();
            terminationModel.createdBy = terminationModel.updatedBy = $("#hdnUserId").val();
            return terminationModel;
        }

        //-- This method deletes the R2T4 details and saves the termination details --//
        private deleteR2T4InputData(onDodChange: boolean = false, textMessage: string): void {
            try {
                if (this.dateValidation) {
                    this.isPopupOpen = true;
                    $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(textMessage))
                        .then(confirmed => {
                            let terminationId = $("#hdnTerminationId").val();
                            this.isPopupOpen = false;
                            if (confirmed) {
                                if (onDodChange) {
                                    $("#chkClaculationPeriod").prop("checked", false);
                                    $("#chkClaculationPeriod").prop("disabled", "disabled");
                                    $("#periodName").css("display", "none");
                                    $("#dtLastDateAttended").val("");
                                    this.setComponentAttribute("dtLastDateAttended", "required", "false");
                                    (document.getElementById("spLDA") as HTMLSpanElement).textContent = "";
                                    this.enableDatePicker("dtLastDateAttended", false);
                                    if (constants.r2T4CompletionStatus.isR2T4ResultsCompleted ||
                                        constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted) {
                                        constants.isR2T4ApproveTabDisabled = false;
                                        constants.isNonR2T4ApproveTabEnabled = !$("#chkClaculationPeriod").prop("checked");
                                    } else {
                                        constants.isR2T4ApproveTabDisabled = true;
                                        constants.isNonR2T4ApproveTabEnabled = false;
                                    }
                                } else {
                                    $("#periodName").css("display", "none");
                                    if (constants.r2T4CompletionStatus.isR2T4ResultsCompleted ||
                                        constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted) {
                                        constants.isNonR2T4ApproveTabEnabled = true;
                                        constants.isR2T4ApproveTabDisabled = false;
                                    } else {
                                        constants.isNonR2T4ApproveTabEnabled = false;
                                        constants.isR2T4ApproveTabDisabled = true;
                                    }
                                }
                                $("#liTerminationDetails").removeClass("is-visited is-invalid").addClass("is-active");
                                $("#liR2T4Input, #liR2T4Result").removeClass("is-visited is-active")
                                    .addClass("is-invalid");
                                $("#liApproveTermination").removeClass("is-active is-invalid");
                                let terminationModel = {} as Models.IStudentTermination;

                                let terminationData = this.generateR2T4TerminationModel(terminationModel);

                                this.saveR2T4TerminationDetails(terminationData,
                                    terminationId,
                                    (response) => {
                                        if (response["resultStatus"] === AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                            this.lastDateAttended = terminationData.lastDateAttended;
                                            this.dateOfDetermination = terminationData.dateWithdrawalDetermined;
                                        } else {
                                            MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_FAIL.replace("$StundentName", constants.studentName).replace("$EnrollmentName", constants.enrollmentName));
                                            return;
                                        }
                                    });

                                let termination = { "terminationId": terminationId };
                                r2T4Termination.deleteR2T4DetailsByTerminationId(termination,
                                    (response) => {
                                        $("#hdnR2T4InputId").val(this.emptyGuid);
                                        constants.isR2T4InputExists = false;
                                        if (!!response) {
                                            r2T4Termination.deleteR2T4ResultByTerminationId(termination,
                                                () => {
                                                    $("#hdnR2T4ResultId").val(this.emptyGuid);
                                                    constants.isR2T4ResultsExists =
                                                        constants.isR2T4OverrideResultsExists = false;
                                                    constants.r2T4CompletionStatus.isR2T4ResultsCompleted =
                                                        constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted =
                                                        constants.r2T4CompletionStatus.isR2T4InputCompleted = false;
                                                });
                                        }
                                    });
                            } else {
                                let studentTerminationData = {} as IFetchByEnrollId;
                                studentTerminationData.enrollmentId = this.enrollmentId;
                                r2T4Termination.fetchTerminationByEnrollmentId(studentTerminationData,
                                    (terminationData: any) => {
                                        if (!!terminationData) {
                                            $("#hdnTerminationId").val(this.emptyGuid);
                                            this.setTerminationDetails(terminationData);
                                            this.resultsVm.isResultsExists((response: any) => {
                                                constants.isR2T4ResultTabDisabled = !!response ? !response : response;
                                                if (constants.isR2T4ResultsCompleted ||
                                                    constants.r2T4CompletionStatus.isR2T4ResultsCompleted) {
                                                    constants.isR2T4ApproveTabDisabled = false;
                                                }
                                            });
                                        } else {
                                            this.setTerminationDetailsActive(false);
                                            constants.isR2T4ApproveTabDisabled = constants.isR2T4ResultTabDisabled =
                                                true;
                                        }
                                    });
                            }
                        });
                    constants.isR2T4ResultTabDisabled = true;
                }
            } catch (e) {
                MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_FAIL.replace("$StundentName", constants.studentName).replace("$EnrollmentName", constants.enrollmentName));
                return;
            }
        }

        //-- This method saves the termination details and deletes Input,R2T4Results,R2T4OverrideResults data from DB on modification of Last Date Attended --//
        private saveTerminationOnLdaChange(onLdaChange: boolean = false, textMessage: string): void {
            try {
                if (this.dateValidation) {
                    this.isLdaChanged = false;
                    this.isPopupOpen = true;
                    $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(textMessage))
                        .then(confirmed => {
                            let terminationId = $("#hdnTerminationId").val();
                            this.isPopupOpen = false;
                            if (confirmed) {
                                if (onLdaChange) {
                                    this.isLdaChanged = true;
                                    let terminationModel = {} as Models.IStudentTermination;
                                    let terminationData = this.generateR2T4TerminationModel(terminationModel);
                                    this.saveR2T4TerminationDetails(terminationData, terminationId, (response) => {
                                        if (response["resultStatus"] === AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                            this.fetchStudentTermination(this.enrollmentId, true);
                                            let studentTerminationId = { "terminationId": terminationId };
                                            r2T4Termination.deleteR2T4DetailsByTerminationId(studentTerminationId,
                                                () => {
                                                    constants.isR2T4ResultTabDisabled = true;
                                                    constants.isR2T4InputTabDisabled = true;
                                                    constants.isR2T4ApproveTabDisabled =
                                                        $("#chkClaculationPeriod").prop("checked");
                                                    constants.r2T4CompletionStatus.isR2T4ResultsCompleted =
                                                        constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted =
                                                        constants.r2T4CompletionStatus.isR2T4InputCompleted = false;
                                                    $("#liR2T4Input").removeClass("is-visited is-active is-invalid");
                                                    $("#liR2T4Result").removeClass("is-visited is-active is-invalid");
                                                    $("#liApproveTermination").removeClass("is-visited is-active is-invalid");
                                                    $("#hdnR2T4ResultId").val(this.emptyGuid);
                                                    $("#hdnR2T4InputId").val(this.emptyGuid);
                                                    constants.isR2T4ResultsExists =
                                                        constants.isR2T4OverrideResultsExists = false;
                                                });
                                        } else {
                                            MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_FAIL.replace("$StundentName", constants.studentName).replace("$EnrollmentName", constants.enrollmentName));
                                            return;
                                        }
                                        this.setTerminationDetailsActive(false);
                                        $("#liR2T4Input").removeClass("is-visited is-active is-invalid");
                                    });
                                }
                            } else {
                                this.fetchStudentTermination(this.enrollmentId, true);
                                r2T4Termination.getR2T4CompletedStatus($("#hdnTerminationId").val(),
                                    (completionStatus: any) => {
                                        if (!!completionStatus) {
                                            constants.r2T4CompletionStatus = completionStatus;
                                            constants.isR2T4ResultTabDisabled =
                                                !constants.r2T4CompletionStatus.isR2T4InputCompleted;
                                            if (constants.r2T4CompletionStatus.isR2T4ResultsCompleted ||
                                                constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted)
                                                constants.isR2T4ApproveTabDisabled = false;
                                            else
                                                constants.isR2T4ApproveTabDisabled = true;
                                        }
                                    });
                                this.setTerminationDetailsActive(false, true);
                            }
                        });
                }
            } catch (e) {
                MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_FAIL.replace("$StundentName", constants.studentName).replace("$EnrollmentName", constants.enrollmentName));
                return;
            }
        }

        //-- This method saves the termination details on change of Date of Determination and/or Last Date Attended --//
        private saveTerminationOnDodOrLdaChange(onLdaChange: boolean = false, textMessage: string): void {
            try {
                if (this.dateValidation) {
                    this.isLdaChanged = false;
                    this.isPopupOpen = true;
                    $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(textMessage))
                        .then(confirmed => {
                            let terminationId = $("#hdnTerminationId").val();
                            this.isPopupOpen = false;
                            if (confirmed) {
                                if (onLdaChange) {
                                    this.isLdaChanged = true;
                                    this.saveOrUpdateR2T4InputDetails((responseCallback) => {
                                        if (!!responseCallback) {
                                            let studentTerminationId = { "terminationId": terminationId };
                                            r2T4Termination.deleteR2T4ResultByTerminationId(studentTerminationId,
                                                () => {
                                                    constants.isR2T4ResultTabDisabled = true;
                                                    constants.isR2T4ApproveTabDisabled =
                                                        $("#chkClaculationPeriod").prop("checked");
                                                    constants.r2T4CompletionStatus.isR2T4ResultsCompleted =
                                                        constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted =
                                                        constants.r2T4CompletionStatus.isR2T4InputCompleted = false;
                                                    $("#liR2T4Result").removeClass("is-visited is-active is-invalid");
                                                    $("#hdnR2T4ResultId").val(this.emptyGuid);
                                                    constants.isR2T4ResultsExists =
                                                        constants.isR2T4OverrideResultsExists = false;
                                                });
                                        }
                                        this.setTerminationDetailsActive(false);
                                        $("#liR2T4Input").removeClass("is-visited is-active is-invalid");
                                    });
                                } else {
                                    let terminationModel = {} as Models.IStudentTermination;
                                    let terminationData = this.generateR2T4TerminationModel(terminationModel);
                                    this.saveR2T4TerminationDetails(terminationData,
                                        terminationId,
                                        (response) => {
                                            if (response["resultStatus"] === AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                                this.fetchStudentTermination(this.enrollmentId, true);
                                                constants.isR2T4ResultTabDisabled = false;
                                                r2T4Termination.getR2T4CompletedStatus($("#hdnTerminationId").val(),
                                                    (completionStatus: any) => {
                                                        if (!!completionStatus) {
                                                            constants.r2T4CompletionStatus = completionStatus;
                                                            if (constants.r2T4CompletionStatus.isR2T4ResultsCompleted ||
                                                                constants.r2T4CompletionStatus
                                                                    .isR2T4OverrideResultsCompleted)
                                                                constants.isR2T4ApproveTabDisabled = false;
                                                            else
                                                                constants.isR2T4ApproveTabDisabled = true;
                                                        }
                                                    });
                                            } else {
                                                MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_FAIL.replace("$StundentName", constants.studentName).replace("$EnrollmentName", constants.enrollmentName));
                                                return;
                                            }
                                        });
                                    this.setTerminationDetailsActive(false, true);
                                }
                            } else {
                                this.fetchStudentTermination(this.enrollmentId, true);
                                r2T4Termination.getR2T4CompletedStatus($("#hdnTerminationId").val(),
                                    (completionStatus: any) => {
                                        if (!!completionStatus) {
                                            constants.r2T4CompletionStatus = completionStatus;
                                            constants.isR2T4ResultTabDisabled =
                                                !constants.r2T4CompletionStatus.isR2T4InputCompleted;
                                            if (constants.r2T4CompletionStatus.isR2T4ResultsCompleted ||
                                                constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted)
                                                constants.isR2T4ApproveTabDisabled = false;
                                            else
                                                constants.isR2T4ApproveTabDisabled = true;
                                        }
                                    });
                                this.setTerminationDetailsActive(false, true);
                            }
                        });
                }
            } catch (e) {
                MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_FAIL.replace("$StundentName", constants.studentName).replace("$EnrollmentName", constants.enrollmentName));
                return;
            }
        }

        //-- This method resets the field values in Percentage of Period Section in R2T4 Input tab--//
        private clearPercentageOfPeriodSection(response: Models.IR2T4Input, callback: (response) => any) {
            this.isClockHour(this.enrollmentId,
                (isClockHr) => {
                    if (isClockHr) {
                        response.isAttendanceNotRequired = isClockHr ? response.isAttendanceNotRequired : !response.isAttendanceNotRequired;
                    } else {
                        response.isAttendanceNotRequired = !$("#dtLastDateAttended").prop("required");
                    }
                    response.startDate = null;
                    response.scheduledEndDate = null;
                    response.withdrawalDate = null;
                    response.totalTime = null;
                    response.completedTime = null;
                    callback(response);
                });
        }

        //-- This method saves the R2T4 Input Details --//
        private saveOrUpdateR2T4InputDetails(callback: (response: Response) => any) {
            try {
                let r2T4Termination = new Api.R2T4StudentTermination();
                let terminationId = $("#hdnTerminationId").val();
                if (terminationId !== "" || terminationId !== this.emptyGuid)
                    r2T4Termination.fetchR2T4Inputdetails(terminationId,
                        (response: any) => {
                            if ($("#hdnR2T4InputId").val() !== this.emptyGuid) {
                                if (response !== undefined && response.r2T4InputId !== this.emptyGuid) {
                                    this.clearPercentageOfPeriodSection(response,
                                        (r2T4InputDetail) => {
                                            r2T4InputDetail.isR2T4InputCompleted = false;
                                            let calculationPeriod: any = $("input[name='periodName']:checked")[0];
                                            let isPaymentPeriod = $("label[for=" + calculationPeriod.id + "]").text().split(" ").join("") === constants.CalculationPeriodType[constants.CalculationPeriodType.PaymentPeriod];
                                            r2T4InputDetail.isTuitionChargedByPaymentPeriod = isPaymentPeriod ? !r2T4InputDetail.isTuitionChargedByPaymentPeriod : null;
                                            r2T4Termination.updateR2T4InputDetail(r2T4InputDetail,
                                                (result: any) => {
                                                    $("#hdnR2T4InputId").val(result["r2T4InputId"]);
                                                    let terminationModel = {} as Models.IStudentTermination;
                                                    let terminationData =
                                                        this.generateR2T4TerminationModel(terminationModel);
                                                    this.saveR2T4TerminationDetails(terminationData,
                                                        terminationId,
                                                        (r2T4TerminationDetailsResponse) => {
                                                            if (r2T4TerminationDetailsResponse["resultStatus"] ===
                                                                AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                                                this.lastDateAttended =
                                                                    terminationData.lastDateAttended;
                                                                this.dateOfDetermination =
                                                                    terminationData.dateWithdrawalDetermined;
                                                                callback(terminationData);
                                                            } else {
                                                                MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_FAIL.replace("$StundentName", constants.studentName).replace("$EnrollmentName", constants.enrollmentName));
                                                                return;
                                                            }
                                                        });
                                                });
                                        });
                                }
                            }
                        });
            } catch (e) {
                MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_FAIL.replace("$StundentName", constants.studentName).replace("$EnrollmentName", constants.enrollmentName));
                return;
            }
        }

        //-- This method is used make the termination details active and disabling other tabs --//
        private setTerminationDetailsActive(isOnStudentClear: boolean, isOnLdaOrDodChange: boolean = false) {
            $("#liTerminationDetails").removeClass("is-visited is-invalid").addClass("is-active");
            if (isOnStudentClear) {
                $("#liR2T4Input").removeClass("is-active is-invalid is-visited");
            } else {
                if ($("#liR2T4Input").hasClass("is-visited")) {
                    $("#liR2T4Input").removeClass("is-active is-invalid").addClass("is-visited");
                } else {
                    $("#liR2T4Input").removeClass("is-visited is-active is-invalid");
                }
            }
            if (isOnLdaOrDodChange)
                if ($("#liR2T4Result").hasClass("is-visited")) {
                    $("#liR2T4Result").removeClass("is-active is-invalid").addClass("is-visited");
                } else {
                    $("#liR2T4Result").removeClass("is-visited is-active is-invalid");
                }
            else
                $("#liR2T4Result").removeClass("is-visited is-active is-invalid");
            $("#liApproveTermination").removeClass("is-visited is-active is-invalid");
        }

        //clears the controls of R2T4Input tab.
        private clearR2T4InputData(): void {
            $("#divR2T4Input").find("input:text").val("");
            $("#dtStartDate").val("");
            $("#dtEndDate").val("");
            $("#dtWithdrawDate").val("");
        }

        /*
        *  isR2T4InputExists method checks whether the R2T4 Input data is available in the database for the given termination Id
        */
        public isR2T4InputExists(onResponseCallback: (response: Response) => any) {
            let terminationId = $("#hdnTerminationId").val();
            r2T4Termination.isR2T4InputExists(terminationId,
                (response: any) => {
                    return onResponseCallback(response);
                });
        }

        //this method is to format date in to MM/DD/yyyy format
        private getFormattedDate(date) {
            let year = date.getFullYear();
            let month = (1 + date.getMonth()).toString();
            month = month.length > 1 ? month : '0' + month;
            let day = date.getDate().toString();
            day = day.length > 1 ? day : '0' + day;
            return month + '/' + day + '/' + year;
        }

        // Set the previously selected enrollment in case of exception.
        private setPreviouslySelectedId(): void {
            if (this.previousStudentEnrollmentId !== "" && this.previousStudentEnrollmentId !== undefined)
                $("#studentEnrollment").find("input[value='" + this.previousStudentEnrollmentId + "']").prop("checked", "checked");
        }

        //this function is to enable/disable textbox based on the checkbox selection.
        private setTitleIvCreditBalance() {
            $("#chkTuitionCharged").click(() => {
                if ((document.getElementById("chkTuitionCharged") as HTMLInputElement).checked) {
                    (document.getElementById("txtcreditBalanceRefunded") as HTMLInputElement).disabled = true;
                    $("#txtcreditBalanceRefunded").val("");
                    $("#txtcreditBalanceRefunded").addClass("txtDisable");
                } else {
                    (document.getElementById("txtcreditBalanceRefunded") as HTMLInputElement).disabled = false;
                    $("#txtcreditBalanceRefunded").removeClass("txtDisable");
                }
            });
        }

        private getFailedCourseDetails(callBack: (response: Response) => void, endDate?: VarDate): any {
            let withdrawalPeriodDetails: any = { enrollmentId: this.enrollmentId, endDate: endDate };
            let programVersions = new ProgramVersions();
            programVersions.getDaysRequiredToCompleteFailedCourse(
                withdrawalPeriodDetails,
                (failedCourseDetails: any) => {
                    return callBack(failedCourseDetails);
                });
        }

        // This function is to auto-poulate the Percentage Of Period on next button click.
        private populatePercentageOfPeriodSection(daysRequiredToCompleteFailedCourse: number, endDate?: VarDate): void {
            let programVersions = new ProgramVersions();
            this.enrollmentId = $("#studentEnrollment input:radio:checked").val();
            if (endDate === undefined) { endDate = null; }
            programVersions.getProgramVersionDetailByEnrollmentId(this.enrollmentId,
                (programVersion: any) => {
                    if (!!programVersion && $("input[id='chkClaculationPeriod']").prop("checked")) {
                        let calculationPeriod: any = $("input[name='periodName']:checked")[0];
                        let isPaymentPeriod = $("label[for=" + calculationPeriod.id + "]").text().split(" ").join("") === constants.CalculationPeriodType[constants.CalculationPeriodType.PaymentPeriod];
                        if (!this.isClockHr) {
                            if (!!programVersion.academicCalendarId && programVersion.academicCalendarId === Number(constants.AcademicCalendarProgram.NonTerm) ||
                                programVersion.academicCalendarId === Number(constants.AcademicCalendarProgram.NonStandardTerm)) {
                                programVersions.getCampusProgramVersionDetail(this.enrollmentId,
                                    (campusprogramVersion: any) => {
                                        if (!!campusprogramVersion && campusprogramVersion.resultStatus === true) {
                                            this.campusProgramVersionDetails = campusprogramVersion;
                                            if (programVersion.academicCalendarId === Number(constants.AcademicCalendarProgram.NonTerm)) {
                                                if (campusprogramVersion.isSelfPaced != null && campusprogramVersion.isSelfPaced === true) {
                                                    this.enableOrDisablePercentageofPeriodCompleted(true, campusprogramVersion.isSelfPaced);
                                                    return;
                                                }
                                                if (campusprogramVersion.isSelfPaced != null && campusprogramVersion.isSelfPaced === false) {
                                                    this.isNonTermProgram = true;
                                                    $("#spHoursCompleted, #spHoursPeriod").prop("textContent", "");
                                                    this.getFailedCourseAndNonTermProgramDetails(programVersions,
                                                        isPaymentPeriod,
                                                        this.enrollmentId,
                                                        daysRequiredToCompleteFailedCourse,
                                                        endDate);
                                                }
                                            } else if (programVersion.academicCalendarId === Number(constants.AcademicCalendarProgram.NonStandardTerm)) {
                                                this.getNonStandardTermProgramDetails(programVersions, campusprogramVersion, isPaymentPeriod, this.enrollmentId, endDate);
                                            }
                                        }
                                    });
                            } else if (programVersion.academicCalendarId != null &&
                                (programVersion.academicCalendarId ===
                                    Number(constants.AcademicCalendarProgram.Quarter) ||
                                    programVersion.academicCalendarId ===
                                    Number(constants.AcademicCalendarProgram.Semester) ||
                                    programVersion.academicCalendarId ===
                                    Number(constants.AcademicCalendarProgram.Trimester))) {
                                this.getStandardTermProgramDetails(programVersions, this.enrollmentId);
                            }
                        } else {
                            this.clockHourProgramDetails(programVersions, isPaymentPeriod, this.enrollmentId);
                        }

                        this.enableOrDisablePercentageofPeriodCompleted(false);
                    }
                });
        }

        // This function gets the details of the failed course of the student and also calculates the non term program details.
        private getFailedCourseAndNonTermProgramDetails(programVersions: ProgramVersions, isPaymentPeriod: boolean, selectedEnrollmentId: any, daysRequiredToCompleteFailedCourse: number, endDate?: VarDate): void {
            if (endDate == null || endDate.toString() === "") {
                this.getFailedCourseDetails((failedCourseResponse: any) => {
                    if (!!failedCourseResponse) {
                        if (failedCourseResponse.daysRequiredToCompleteFailedCourse === 0 &&
                            (failedCourseResponse.resultStatus.search(AD.X_MESSAGE_END_DATE) !== -1 ||
                                failedCourseResponse.resultStatus.search(AD.X_MESSAGE_WITHDRAWAL_END_DATE) !== -1)) {
                            this.showPopup(this.enrollmentId,
                                failedCourseResponse.resultStatus,
                                Number(constants.TermType.NonTermForFailedCoursesEndDate));
                        } else if (failedCourseResponse.daysRequiredToCompleteFailedCourse === 0 &&
                            (failedCourseResponse.resultStatus !== AD.X_MESSAGE_NO_FAILED_COURSES &&
                                failedCourseResponse.resultStatus !== AD.X_MESSAGE_COURSES_NOT_FOUND)) {
                            this.showPopup(this.enrollmentId,
                                failedCourseResponse.resultStatus,
                                Number(constants.TermType.NonTermForFailedCoursesNoOfDays),
                                endDate);
                        } else {
                            this.getNonTermProgramDetails(programVersions,
                                isPaymentPeriod,
                                selectedEnrollmentId,
                                daysRequiredToCompleteFailedCourse,
                                endDate);
                        }
                    }
                },
                    endDate);
            } else {
                programVersions.getCreditHoursWithdrawalPaymentPeriod(
                    selectedEnrollmentId,
                    (currentPeriodDetailsWithCreditsEarned: any) => {
                        if (!!currentPeriodDetailsWithCreditsEarned) {
                            $("#dtStartDate")
                                .val(MasterPage.formatDate(
                                    StudentTerminationVm.formatDate(
                                        currentPeriodDetailsWithCreditsEarned
                                            .startDateOfWithdrawalPeriod)));
                        }
                        if (new Date(endDate) < new Date($("#dtStartDate").val()) ||
                            new Date(endDate) <
                            new Date($("#dtLastDateAttended").val())
                        ) {
                            this.showPopup(selectedEnrollmentId,
                                AD.X_MESSAGE_ENDDATE_LESSTHAN_STARTDATE
                                    .replace("$startDate", $("#dtStartDate").val())
                                    .replace("$withdrawal",
                                    $("#dtLastDateAttended").val()),
                                Number(constants.TermType.NonTermNotSelfPaced));
                        } else {
                            this.getFailedCourseDetails((failedCourseResponse: any) => {
                                if (!!failedCourseResponse) {
                                    if (failedCourseResponse.daysRequiredToCompleteFailedCourse === 0 &&
                                        (failedCourseResponse.resultStatus.search(AD.X_MESSAGE_END_DATE) !== -1 ||
                                            failedCourseResponse.resultStatus.search(AD.X_MESSAGE_WITHDRAWAL_END_DATE) !== -1)) {
                                        this.showPopup(this.enrollmentId,
                                            failedCourseResponse.resultStatus,
                                            Number(constants.TermType.NonTermForFailedCoursesEndDate));
                                    } else if (failedCourseResponse.daysRequiredToCompleteFailedCourse === 0 &&
                                        (failedCourseResponse.resultStatus !== AD.X_MESSAGE_NO_FAILED_COURSES &&
                                            failedCourseResponse.resultStatus !== AD.X_MESSAGE_COURSES_NOT_FOUND)) {
                                        this.showPopup(this.enrollmentId,
                                            failedCourseResponse.resultStatus,
                                            Number(constants.TermType.NonTermForFailedCoursesNoOfDays),
                                            endDate);
                                    } else {
                                        this.getNonTermProgramDetails(programVersions,
                                            isPaymentPeriod,
                                            selectedEnrollmentId,
                                            daysRequiredToCompleteFailedCourse,
                                            endDate);
                                    }
                                }
                            },
                                endDate);
                        }
                    });
            }
        }

        // This function fetches the details of Percentage of period completed section for Standard-Term programs on next button click.
        private getStandardTermProgramDetails(programVersions: ProgramVersions, selectedEnrollmentId: any): void {
            programVersions.getCompletedAndTotalDaysForStandardTermPrograms(
                selectedEnrollmentId,
                (creditHourPaymentPeriodDaysDetail: any) => {
                    if (!!creditHourPaymentPeriodDaysDetail) {
                        $("#txtScheduledHours")
                            .val(creditHourPaymentPeriodDaysDetail.completedDays);
                        $("#txtTotalHours").val(creditHourPaymentPeriodDaysDetail.totalDays);
                    }
                    programVersions.getWithdrawalPeriodTermDetails(
                        selectedEnrollmentId,
                        (withdrawalPeriodTermDetails: any) => {
                            if (!!withdrawalPeriodTermDetails) {
                                $("#dtStartDate")
                                    .val(MasterPage.formatDate(StudentTerminationVm.formatDate(withdrawalPeriodTermDetails.termStartDate)));
                                $("#dtEndDate")
                                    .val(MasterPage.formatDate(StudentTerminationVm.formatDate(withdrawalPeriodTermDetails.termEndDate)));
                            }
                        });

                    this.toggleNotRequiredToTakeAttendanceCheckbox(false,false,false);

                });
        }

        // This function fetches the details of Percentage of period completed section for Non-Term program on next button click.
        private getNonTermProgramDetails(programVersions: ProgramVersions,
            isPaymentPeriod: boolean,
            selectedEnrollmentId: any,
            daysRequiredToCompleteFailedCourse: number,
            endDate?: VarDate): void {
            let withdrawalPeriodDetailsForNonTermProgram: any = {
                enrollmentId: selectedEnrollmentId,
                endDate: endDate,
                numberOfDaysRequired: daysRequiredToCompleteFailedCourse
            };
            this.enableOrDisablePercentageofPeriodCompleted(false);
            if (this.studentEnrollmentDetails.unitTypeDescription.toLowerCase() !== "none") {
                if (isPaymentPeriod) {
                    if (endDate != null && endDate.toString() !== "") {
                        programVersions.getCreditHoursWithdrawalPaymentPeriod(
                            selectedEnrollmentId,
                            (currentPeriodDetailsWithCreditsEarned: any) => {
                                if (!!currentPeriodDetailsWithCreditsEarned) {
                                    $("#dtStartDate")
                                        .val(MasterPage.formatDate(
                                            StudentTerminationVm.formatDate(
                                                currentPeriodDetailsWithCreditsEarned
                                                    .startDateOfWithdrawalPeriod)));
                                }

                                if (new Date(endDate) < new Date($("#dtStartDate").val()) ||
                                    new Date(endDate) <
                                    new Date($("#dtLastDateAttended").val())
                                ) {
                                    this.showPopup(selectedEnrollmentId,
                                        AD.X_MESSAGE_ENDDATE_LESSTHAN_STARTDATE
                                            .replace("$startDate", $("#dtStartDate").val())
                                            .replace("$withdrawal",
                                            $("#dtLastDateAttended").val()),
                                        Number(constants.TermType.NonTermNotSelfPaced));
                                } else {
                                    $("#dtEndDate")
                                        .val(MasterPage.formatDate(StudentTerminationVm.formatDate(endDate)));
                                    programVersions.getTotalDaysForNonTermNotSelfPacedProgram(
                                        withdrawalPeriodDetailsForNonTermProgram,
                                        (creditHourTotalDays: any) => {
                                            if (!!creditHourTotalDays) {
                                                if (creditHourTotalDays.totalDays === 0 &&
                                                    (creditHourTotalDays.resultStatus == null ||
                                                        creditHourTotalDays.resultStatus == undefined)) {
                                                    MasterPage.SHOW_ERROR_WINDOW(
                                                        AD.X_MESSAGE_NON_TERM_NOT_SELFPACED_TOTAL_DAYS_ISSUE);
                                                    return;
                                                }
                                                if (creditHourTotalDays.resultStatus.search(AD.X_MESSAGE_END_DATE) !== -1 || creditHourTotalDays.resultStatus.search(AD.X_MESSAGE_WITHDRAWAL_END_DATE) !== -1 || creditHourTotalDays.resultStatus.search(AD.X_FAILED_CALCULATING_FRAGMENT) !== -1) {
                                                    this.showPopup(selectedEnrollmentId,
                                                        creditHourTotalDays.resultStatus,
                                                        Number(constants.TermType.NonTermNotSelfPaced));
                                                } else {
                                                    this.closeKendoWindow();
                                                    programVersions.getCreditHourCompletedDays(
                                                        selectedEnrollmentId,
                                                        (creditHourCompletedDays: any) => {
                                                            $("#txtScheduledHours").val(creditHourCompletedDays);
                                                        });
                                                    $("#txtTotalHours").val(creditHourTotalDays.totalDays);
                                                }
                                            }
                                        });
                                }
                            });
                    } else {
                        programVersions.getTotalDaysForNonTermNotSelfPacedProgram(
                            withdrawalPeriodDetailsForNonTermProgram,
                            (creditHourTotalDays: any) => {
                                if (!!creditHourTotalDays) {
                                    if (creditHourTotalDays.totalDays === 0 &&
                                        (creditHourTotalDays.resultStatus == null ||
                                            creditHourTotalDays.resultStatus == undefined)) {
                                        MasterPage.SHOW_ERROR_WINDOW(AD
                                            .X_MESSAGE_NON_TERM_NOT_SELFPACED_TOTAL_DAYS_ISSUE);
                                        return;
                                    }
                                    if (creditHourTotalDays.resultStatus.search(AD.X_MESSAGE_END_DATE) !== -1 ||
                                        creditHourTotalDays.resultStatus.search(AD.X_MESSAGE_WITHDRAWAL_END_DATE) !==
                                        -1) {
                                        this.showPopup(selectedEnrollmentId,
                                            creditHourTotalDays.resultStatus,
                                            Number(constants.TermType.NonTermNotSelfPaced));
                                    } else {
                                        programVersions.getCreditHourCompletedDays(
                                            selectedEnrollmentId,
                                            (creditHourCompletedDays: any) => {
                                                $("#txtScheduledHours").val(creditHourCompletedDays);

                                                programVersions.getCreditHoursWithdrawalPaymentPeriod(
                                                    selectedEnrollmentId,
                                                    (currentPeriodDetailsWithCreditsEarned: any) => {
                                                        if (!!currentPeriodDetailsWithCreditsEarned) {
                                                            $("#dtStartDate")
                                                                .val(MasterPage.formatDate(
                                                                    StudentTerminationVm.formatDate(
                                                                        currentPeriodDetailsWithCreditsEarned
                                                                            .startDateOfWithdrawalPeriod)));
                                                        }

                                                        programVersions.getEndDateOfCreditHourPaymentPeriod(
                                                            selectedEnrollmentId,
                                                            (creditHourWithdrawalEndDateDetails: any) => {
                                                                if (!!creditHourWithdrawalEndDateDetails) {
                                                                    $("#dtEndDate")
                                                                        .val(MasterPage.formatDate(
                                                                            StudentTerminationVm.formatDate(
                                                                                creditHourWithdrawalEndDateDetails
                                                                                    .endDateOfWithdrawalPeriod)));
                                                                }
                                                                this.closeKendoWindow();
                                                                if (creditHourTotalDays.totalDays === 0) {
                                                                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_NON_TERM_NOT_SELFPACED_TOTAL_DAYS_ISSUE);
                                                                    return;
                                                                } else if (creditHourCompletedDays === 0) {
                                                                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_NON_TERM_NOT_SELFPACED_COMPLETED_DAYS_ISSUE);
                                                                    return;
                                                                }
                                                            });
                                                    });
                                            });

                                        $("#txtTotalHours").val(creditHourTotalDays.totalDays);
                                    }
                                }
                            });
                    }
                } else {
                    if (endDate != null && endDate.toString() !== "") {
                        programVersions.getNonStandardWithdrawalPeriodOfEnrollment(
                            selectedEnrollmentId,
                            (currentPeriodDetailsWithCreditsEarned: any) => {
                                if (!!currentPeriodDetailsWithCreditsEarned) {
                                    $("#dtStartDate").val(MasterPage.formatDate(
                                        StudentTerminationVm.formatDate(
                                            currentPeriodDetailsWithCreditsEarned
                                                .startDateOfWithdrawalPeriod)));
                                }

                                if (new Date(endDate) < new Date($("#dtStartDate").val()) ||
                                    new Date(endDate) < new Date($("#dtLastDateAttended").val())) {
                                    this.showPopup(selectedEnrollmentId,
                                        AD.X_MESSAGE_ENDDATE_LESSTHAN_STARTDATE
                                            .replace("$startDate", $("#dtStartDate").val())
                                            .replace("$withdrawal", $("#dtLastDateAttended").val()),
                                        Number(constants.TermType.NonTermNotSelfPaced));
                                } else {
                                    $("#dtEndDate").val(endDate.toString());
                                    programVersions
                                        .getNonTermCompletedAndTotalDaysForPeriodOfEnrollment(
                                        withdrawalPeriodDetailsForNonTermProgram,
                                        (creditHourPaymentPeriodDaysDetail: any) => {
                                            if (!!creditHourPaymentPeriodDaysDetail) {
                                                if (creditHourPaymentPeriodDaysDetail.resultStatus.search(
                                                    AD.X_MESSAGE_END_DATE) !==
                                                    -1 ||
                                                    creditHourPaymentPeriodDaysDetail.resultStatus.search(
                                                        AD.X_MESSAGE_WITHDRAWAL_END_DATE) !==
                                                    -1) {
                                                    this.showPopup(selectedEnrollmentId,
                                                        creditHourPaymentPeriodDaysDetail.resultStatus,
                                                        Number(constants.TermType.NonTermNotSelfPaced));
                                                } else {
                                                    this.closeKendoWindow();
                                                    $("#txtScheduledHours").val(creditHourPaymentPeriodDaysDetail
                                                        .completedDays);
                                                    $("#txtTotalHours")
                                                        .val(creditHourPaymentPeriodDaysDetail.totalDays);
                                                }
                                            }
                                        });
                                }
                            });
                    } else {
                        programVersions
                            .getNonTermCompletedAndTotalDaysForPeriodOfEnrollment(
                            withdrawalPeriodDetailsForNonTermProgram,
                            (creditHourPaymentPeriodDaysDetail: any) => {
                                if (!!creditHourPaymentPeriodDaysDetail) {
                                    if (creditHourPaymentPeriodDaysDetail.resultStatus.search(AD.X_MESSAGE_END_DATE) !== -1 || creditHourPaymentPeriodDaysDetail.resultStatus.search(AD.X_MESSAGE_WITHDRAWAL_END_DATE) !== -1) {
                                        this.showPopup(selectedEnrollmentId,
                                            creditHourPaymentPeriodDaysDetail.resultStatus,
                                            Number(constants.TermType.NonTermNotSelfPaced));
                                    } else {
                                        programVersions.getNonStandardWithdrawalPeriodOfEnrollment(
                                            selectedEnrollmentId,
                                            (currentPeriodDetailsWithCreditsEarned: any) => {
                                                if (!!currentPeriodDetailsWithCreditsEarned) {
                                                    $("#dtStartDate").val(MasterPage.formatDate(
                                                        StudentTerminationVm.formatDate(
                                                            currentPeriodDetailsWithCreditsEarned
                                                                .startDateOfWithdrawalPeriod)));
                                                }

                                                programVersions.getEndDateOfCreditHourPeriodOfEnrollment(
                                                    selectedEnrollmentId,
                                                    (creditHourWithdrawalEndDateDetails: any) => {
                                                        if (!!creditHourWithdrawalEndDateDetails) {
                                                            $("#dtEndDate")
                                                                .val(MasterPage.formatDate(
                                                                    StudentTerminationVm.formatDate(
                                                                        creditHourWithdrawalEndDateDetails
                                                                            .endDateOfWithdrawalPeriod)));
                                                        }
                                                        this.closeKendoWindow();
                                                        if (creditHourPaymentPeriodDaysDetail.completedDays === 0 ||
                                                            creditHourPaymentPeriodDaysDetail.totalDays === 0) {
                                                            MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_NONTERM_NOT_SELFPACED_COMPLETED_AND_TOTAL_DAYS_ISSUE);
                                                            return;
                                                        }
                                                    });
                                            });
                                        $("#txtScheduledHours")
                                            .val(creditHourPaymentPeriodDaysDetail.completedDays);
                                        $("#txtTotalHours").val(creditHourPaymentPeriodDaysDetail.totalDays);
                                    }
                                }
                            });
                    }
                }
            }
            this.toggleNotRequiredToTakeAttendanceCheckbox(true);
        }

        // This function fetches the details of Percentage of period completed section for Non-Standard Term program on next button click.
        private getNonStandardTermProgramDetails(programVersions: ProgramVersions, campusprogramVersion: any, isPaymentPeriod: boolean, selectedEnrollmentId: any, endDate?: VarDate): void {
            let withdrawalPeriodDetails: any = { enrollmentId: selectedEnrollmentId, endDate: endDate };
            this.enableOrDisablePercentageofPeriodCompleted(false);
            if (campusprogramVersion.termSubEqualInLen != null && campusprogramVersion.termSubEqualInLen === true) {
                programVersions
                    .getDetailsForNonStandardTermSubstantiallyEqualLength(
                        selectedEnrollmentId,
                        (creditHourPaymentPeriodDaysDetail: any) => {
                            if (!!creditHourPaymentPeriodDaysDetail) {
                                $("#txtScheduledHours")
                                    .val(creditHourPaymentPeriodDaysDetail
                                        .completedDays);
                                $("#txtTotalHours").val(creditHourPaymentPeriodDaysDetail
                                    .totalDays);
                              
                            }
                            programVersions.getWithdrawalPeriodTermDetails(
                                selectedEnrollmentId,
                                (withdrawalPeriodTermDetails: any) => {
                                    if (!!withdrawalPeriodTermDetails) {
                                        $("#dtStartDate")
                                            .val(MasterPage.formatDate(StudentTerminationVm.formatDate(withdrawalPeriodTermDetails.termStartDate)));
                                        $("#dtEndDate")
                                            .val(MasterPage.formatDate(StudentTerminationVm.formatDate(withdrawalPeriodTermDetails.termEndDate)));
                                    }
                                });

                            this.toggleNotRequiredToTakeAttendanceCheckbox(false, false, false);

                        });
            } else {
                if (isPaymentPeriod) {
                    programVersions
                        .getDetailsForNonStandardTermNotSubstantiallyEqualLength(
                        withdrawalPeriodDetails,
                        (creditHourPaymentPeriodDaysDetail: any) => {
                            if (!!creditHourPaymentPeriodDaysDetail) {
                                if (creditHourPaymentPeriodDaysDetail.resultStatus.search(AD.X_MESSAGE_END_DATE) !== -1 || creditHourPaymentPeriodDaysDetail.resultStatus.search(AD.X_MESSAGE_WITHDRAWAL_END_DATE) !== -1) {
                                    this.showPopup(selectedEnrollmentId,
                                        creditHourPaymentPeriodDaysDetail.resultStatus,
                                        Number(constants.TermType.NonStandard));
                                } else {
                                    if (this.studentEnrollmentDetails.unitTypeDescription.toLowerCase() !== "none") {
                                        programVersions
                                            .getPaymentPeriodForNotSubstantiallyEqualInLength(
                                            selectedEnrollmentId,
                                            (creditHourPaymentPeriodDetails: any) => {
                                                if (!!creditHourPaymentPeriodDetails) {
                                                    if (creditHourPaymentPeriodDetails.paymentPeriodTermType === 1) {
                                                        $("#dtStartDate")
                                                            .val(MasterPage.formatDate(
                                                                StudentTerminationVm.formatDate(
                                                                    creditHourPaymentPeriodDetails
                                                                        .withdrawalPaymentPeriodStartDate)));
                                                    } else if (creditHourPaymentPeriodDetails
                                                        .paymentPeriodTermType === 2) {
                                                        $("#dtStartDate")
                                                            .val(MasterPage.formatDate(
                                                                StudentTerminationVm.formatDate(
                                                                    creditHourPaymentPeriodDetails.termStartDate)));
                                                        $("#dtEndDate").val(MasterPage.formatDate(
                                                            StudentTerminationVm.formatDate(
                                                                creditHourPaymentPeriodDetails.termEndDate)));
                                                        endDate = $("#dtEndDate").val();
                                                    }
                                                }

                                                        if (endDate == null || endDate.toString() === "") {
                                                            programVersions.getEndDateOfCreditHourPaymentPeriod(
                                                                selectedEnrollmentId,
                                                                (creditHourWithdrawalEndDateDetails: any) => {
                                                                    if (!!creditHourWithdrawalEndDateDetails) {
                                                                        $("#dtEndDate")
                                                                            .val(MasterPage.formatDate(
                                                                                StudentTerminationVm.formatDate(
                                                                                    creditHourWithdrawalEndDateDetails
                                                                                    .endDateOfWithdrawalPeriod)));
                                                                    }
                                                                    this.closeKendoWindow();
                                                                    if (creditHourPaymentPeriodDaysDetail.completedDays === 0 ||
                                                                        creditHourPaymentPeriodDaysDetail.totalDays === 0) {
                                                                        MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_NONSTANDARD_SUB_NOT_EQUAL_COMPLETED_AND_TOTAL_DAYS_ISSUE);
                                                                        return;
                                                                    }
                                                                });
                                                        } else {
                                                            if (new Date(endDate) < new Date($("#dtStartDate").val()) || new Date(endDate) < new Date($("#dtLastDateAttended").val())) {
                                                                this.showPopup(selectedEnrollmentId,
                                                                    AD.X_MESSAGE_ENDDATE_LESSTHAN_STARTDATE.replace("$startDate", $("#dtStartDate").val()).replace("$withdrawal", $("#dtLastDateAttended").val()),
                                                                    Number(constants.TermType.NonTermNotSelfPaced));
                                                            } else {
                                                                this.closeKendoWindow();
                                                            }
                                                            if ((creditHourPaymentPeriodDaysDetail.completedDays === 0 ||
                                                                creditHourPaymentPeriodDaysDetail.totalDays === 0) && new Date(endDate) >= new Date($("#dtStartDate").val())) {
                                                                MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_NONSTANDARD_SUB_NOT_EQUAL_COMPLETED_AND_TOTAL_DAYS_ISSUE);
                                                                return;
                                                            }
                                                            $("#dtEndDate").val(endDate.toString());
                                                        }
                                                    });

                                            $("#txtScheduledHours")
                                                .val(creditHourPaymentPeriodDaysDetail
                                                    .completedDays);
                                            $("#txtTotalHours").val(creditHourPaymentPeriodDaysDetail
                                                .totalDays);

                                        } else {
                                          
                                        }
                                   

                                        this.toggleNotRequiredToTakeAttendanceCheckbox(true);
                                    }
                                }
                            });
                } else {
                    programVersions.getNonStandardProgramDaysDetail(
                        withdrawalPeriodDetails,
                        (nonStandardProgramDaysDetail: any) => {
                            if (!!nonStandardProgramDaysDetail) {
                                if (nonStandardProgramDaysDetail.resultStatus.search(AD.X_MESSAGE_END_DATE) !== -1 || nonStandardProgramDaysDetail.resultStatus.search(AD.X_MESSAGE_WITHDRAWAL_END_DATE) !== -1) {
                                    this.showPopup(selectedEnrollmentId,
                                        nonStandardProgramDaysDetail.resultStatus,
                                        Number(constants.TermType.NonStandard));
                                } else {
                                    if (this.studentEnrollmentDetails.unitTypeDescription.toLowerCase() !== "none") {
                                        programVersions.getNonStandardWithdrawalPeriodOfEnrollment(
                                            selectedEnrollmentId,
                                            (currentPeriodDetailsWithCreditsEarned: any) => {
                                                if (!!currentPeriodDetailsWithCreditsEarned) {
                                                    $("#dtStartDate").val(MasterPage.formatDate(
                                                        StudentTerminationVm.formatDate(
                                                            currentPeriodDetailsWithCreditsEarned
                                                                .startDateOfWithdrawalPeriod)));
                                                }

                                                if (endDate == null || endDate.toString() === "") {
                                                    programVersions.getEndDateOfCreditHourPeriodOfEnrollment(
                                                        selectedEnrollmentId,
                                                        (creditHourWithdrawalEndDateDetails: any) => {
                                                            if (!!creditHourWithdrawalEndDateDetails) {
                                                                $("#dtEndDate")
                                                                    .val(MasterPage.formatDate(
                                                                        StudentTerminationVm.formatDate(
                                                                            creditHourWithdrawalEndDateDetails
                                                                                .endDateOfWithdrawalPeriod)));
                                                            }
                                                            this.closeKendoWindow();
                                                            if (nonStandardProgramDaysDetail.completedDays === 0 ||
                                                                nonStandardProgramDaysDetail.totalDays === 0) {
                                                                MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_NONSTANDARD_SUB_NOT_EQUAL_COMPLETED_AND_TOTAL_DAYS_ISSUE);
                                                                return;
                                                            }
                                                        });
                                                } else {
                                                    if (new Date(endDate) < new Date($("#dtStartDate").val()) || new Date(endDate) < new Date($("#dtLastDateAttended").val())) {
                                                        this.showPopup(selectedEnrollmentId,
                                                            AD.X_MESSAGE_ENDDATE_LESSTHAN_STARTDATE.replace("$startDate", $("#dtStartDate").val()).replace("$withdrawal", $("#dtLastDateAttended").val()),
                                                            Number(constants.TermType.NonTermNotSelfPaced));
                                                    } else {
                                                        this.closeKendoWindow();
                                                    }
                                                    if (nonStandardProgramDaysDetail.completedDays === 0 ||
                                                        nonStandardProgramDaysDetail.totalDays === 0 && new Date(endDate) >= new Date($("#dtStartDate").val())) {
                                                        MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_NONSTANDARD_SUB_NOT_EQUAL_COMPLETED_AND_TOTAL_DAYS_ISSUE);
                                                        return;
                                                    }
                                                    $("#dtEndDate").val(endDate.toString());
                                                }
                                            });
                                    } else {
                                        this.closeKendoWindow();
                                        if (nonStandardProgramDaysDetail.completedDays === 0 ||
                                            nonStandardProgramDaysDetail.totalDays === 0) {
                                            MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_NONSTANDARD_SUB_NOT_EQUAL_COMPLETED_AND_TOTAL_DAYS_ISSUE);
                                            return;
                                        }
                                    }
                                    $("#txtTotalHours").val(nonStandardProgramDaysDetail.totalDays);
                                    $("#txtScheduledHours").val(nonStandardProgramDaysDetail.completedDays);
                                    this.toggleNotRequiredToTakeAttendanceCheckbox(true);
                                }
                            }
                        });
                }
            }
        }

        // This function fetches the details of Percentage of period completed section for clock hour program on next button click.
        private clockHourProgramDetails(programVersions: ProgramVersions, isPaymentPeriod: boolean, selectedEnrollmentId: any): void {
            if (isPaymentPeriod) {
                programVersions.getHoursScheduledToComplete(
                    this.enrollmentId,
                    (scheduledAndTotalHours: any) => {
                        if (!!scheduledAndTotalHours) {
                            $("#txtTotalHours").val(scheduledAndTotalHours.totalHoursInPeriod);
                            $("#txtScheduledHours")
                                .val(scheduledAndTotalHours
                                    .hoursScheduledToComplete);
                            if (scheduledAndTotalHours.totalHoursInPeriod === 0 ||
                                scheduledAndTotalHours.hoursScheduledToComplete === 0) {
                                MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_CLOCK_HOUR_COMPLETED_AND_TOTAL_HOURS_ISSUE);
                                return;
                            }
                        }
                    });
            } else {
                programVersions.getScheduledAndTotalHoursForPeriodOfEnrollment(
                    this.enrollmentId,
                    (scheduledAndTotalHours: any) => {
                        if (!!scheduledAndTotalHours) {
                            $("#txtTotalHours").val(scheduledAndTotalHours.totalHoursInPeriod);
                            $("#txtScheduledHours")
                                .val(scheduledAndTotalHours
                                    .hoursScheduledToComplete);
                            if (scheduledAndTotalHours.totalHoursInPeriod === 0 ||
                                scheduledAndTotalHours.hoursScheduledToComplete === 0) {
                                MasterPage.SHOW_ERROR_WINDOW(AD.X_MESSAGE_CLOCK_HOUR_COMPLETED_AND_TOTAL_HOURS_ISSUE);
                                return;
                            }
                        }
                    });
            }
        }

        // This method is to enable or disable the Percentage of Period Completed Section.
        private enableOrDisablePercentageofPeriodCompleted(isEnabled: boolean, isSelfPace?: boolean): void {
            $("#dtWithdrawDate").val($("#dtLastDateAttended").val());
            $("#dvTitleIVAid, #dvInstitutionalCharges").find(":input").prop("disabled", isEnabled);
            $("#dvPercentageOfPeriod").find(":input").prop("disabled", !isEnabled);
            if (isEnabled) {
                $("#dtStartDate, #dtEndDate, #dtWithdrawDate, #txtScheduledHours, #txtTotalHours")
                    .removeClass("txtDisable");
            } else {
               // $("#dtStartDate, #dtEndDate, #dtWithdrawDate, #txtScheduledHours, #txtTotalHours")
                 //   .addClass("txtDisable");
            }
            this.enableDatePicker("dtStartDate", isEnabled);
            this.enableDatePicker("dtEndDate", isEnabled);
            this.enableDatePicker("dtWithdrawDate", isEnabled);
            if (isSelfPace != null && isSelfPace) {
                $("#chkAttendance").prop("checked", true);
                $("#txtTotalHours, #txtScheduledHours, #dtWithdrawDate").attr("disabled", "disabled");
                this.enableDatePicker("dtWithdrawDate", false);
            }
            if ((document.getElementById("chkTuitionCharged") as HTMLInputElement).checked) {
                (document.getElementById("txtcreditBalanceRefunded") as HTMLInputElement).disabled = true;
                $("#txtcreditBalanceRefunded").val("");
                $("#txtcreditBalanceRefunded").addClass("txtDisable");
            }
        }

        // This method is to show pop up window for entering the end date or number of days for payment period calculation.
        private showPopup(enrollmentId: any, message: string, termType: number, withdrawalEndDate?: VarDate) {
            $("#messageContent").text(message);
            this.createDatePicker("dtPeriodEndDate");
            $("#txtNoOfDays").val("");
            $("#dtPeriodEndDate").val("");
            $("#spNoOfDays").css("display", "none");
            $("#spEnddate").css("display", "none");
            if (termType !== Number(constants.TermType.NonTermForFailedCoursesNoOfDays)) {
                $("#tdEndDateHeight").css("display", "block");
                $("#trNoOfDays").css("display", "none");
                $("#trPeriodEndDate").css("display", "block");
                $("#trNoOfDaysHeight").css("display", "none");
            } else {
                $("#tdEndDateHeight").css("display", "none");
                $("#trPeriodEndDate").css("display", "none");
                $("#trNoOfDays").css("display", "block");
                $("#trNoOfDaysHeight").css("display", "block");
            }

            var myWindow = $("#window");
            let win = myWindow.kendoWindow({
                width: "400px",
                title: "",
                modal: true,
                visible: false,
                scrollable: false,
                resizable: false,
            });
            $("#window").data("kendoWindow").center().open();
            $(".k-window-actions").css("display", "none");
            let studEnrollmentId = enrollmentId;
            $("#btnEndDate").click(() => {
                let endDate = (withdrawalEndDate == null || withdrawalEndDate.toString() === "") ? $("#dtPeriodEndDate").val() : withdrawalEndDate;
                $("#dtEndDate").val(MasterPage.formatDate($("#dtPeriodEndDate").val()));
                let noOfDays = $("#txtNoOfDays").val();
                this.studentValidator = MasterPage.ADVANTAGE_VALIDATOR("window");
                if (!this.studentValidator.validate() && !this.validateDate(endDate)) {
                    return false;
                }
                let programVersions = new ProgramVersions();
                let calculationPeriod: any = $("input[name='periodName']:checked")[0];
                let isPaymentPeriod = $("label[for=" + calculationPeriod.id + "]").text().split(" ").join("") === constants.CalculationPeriodType[constants.CalculationPeriodType.PaymentPeriod];

                if (termType === Number(constants.TermType.NonTermNotSelfPaced)) {
                    if (endDate === '') {
                        $("#spEnddate").css("display", "block");
                        return false;
                    }
                    this.getNonTermProgramDetails(programVersions, isPaymentPeriod, studEnrollmentId, 0, endDate);
                }
                else if (termType === Number(constants.TermType.NonStandard)) {
                    if (endDate === '') {
                        $("#spEnddate").css("display", "block");
                        return false;
                    }
                    this.getNonStandardTermProgramDetails(programVersions, this.campusProgramVersionDetails, isPaymentPeriod, studEnrollmentId, endDate);
                }
                else if (termType === Number(constants.TermType.NonTermForFailedCoursesNoOfDays)) {
                    if (noOfDays === '') {
                        $("#spNoOfDays").css("display", "block");
                        return false;
                    }
                    this.getNonTermProgramDetails(programVersions, isPaymentPeriod, studEnrollmentId, noOfDays, endDate);
                }
                else if (termType === Number(constants.TermType.NonTermForFailedCoursesEndDate)) {
                    if (endDate === '') {
                        $("#spEnddate").css("display", "block");
                        return false;
                    }
                    this.getFailedCourseAndNonTermProgramDetails(programVersions, isPaymentPeriod, studEnrollmentId, 0, endDate);
                }
                return true;
            });
        }

        // This method is to check/uncheck the Not Required To Take Attendance Checkbox.
        private toggleNotRequiredToTakeAttendanceCheckbox(enableCheckbox: boolean, isNonTermNotSelfPaced: boolean = false, check?:boolean): void {
            if (this.studentEnrollmentDetails != undefined && this.studentEnrollmentDetails != null) {
                if (this.studentEnrollmentDetails.unitTypeDescription.toLowerCase() === "none") {
                    if (isNonTermNotSelfPaced) {
                        if ($("#chkAttendance").prop("checked") === true) {
                            //$("#dtWithdrawDate, #txtScheduledHours, #txtTotalHours").addClass("txtDisable");
                            $("#txtScheduledHours, #txtTotalHours").prop("disabled", true);
                            this.enableDatePicker("dtStartDate", false);
                        } else {
                            $("#dtWithdrawDate, #txtScheduledHours, #txtTotalHours").removeClass("txtDisable");
                            $("#txtScheduledHours, #txtTotalHours").prop("disabled", false);
                            this.enableDatePicker("dtStartDate", true);
                        }
                    } 

                    if (check != null) {
                        $("#chkAttendance").prop("checked", check);
                    }
                    if (enableCheckbox) {
                        $("#chkAttendance").removeAttr("disabled");
                        $("#dtStartDate, #dtEndDate").removeClass("txtDisable");
                        $("#dtStartDate, #dtEndDate").val("");
                        this.enableDatePicker("dtStartDate", true);
                        this.enableDatePicker("dtEndDate", true);
                    } else {
                        $("#chkAttendance").prop("disabled", "disabled");
                    }
                } else {
                    $("#chkAttendance").prop("disabled", "disabled");
                    $("#chkAttendance").prop("checked", false);
                }
            }
        }

        // This method is to close the kendo window if its open.
        private closeKendoWindow(): void {
            if ($("#window").data("kendoWindow") != undefined) {
                $("#window").data("kendoWindow").close();
            }
        }
    }
}