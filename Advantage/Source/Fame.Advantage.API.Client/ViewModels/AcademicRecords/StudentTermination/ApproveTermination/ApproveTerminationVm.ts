﻿/// <reference path="../../../../common/utility.ts" />
module API.ViewModels.AcademicRecords.StudentTermination.ApproveTermination {
    import common = Components.Common;
    import Utility = Common.Utility;
    //import IStudentEnrollment = Models.IStudentEnrollment;
    import StudentEnrollment = Api.Components.StudentEnrollment;

    export class ApproveTerminationVm {
        approveTerminationConfirm: string = "";
        appTermConfirmForR2T4MultEnrollment: string = "";
        appTermConfirmForNonR2T4MultEnrollment: string = "";
        appTermConfirmForR2T4SingleEnrollment: string = "";
        appTermConfirmForNonR2T4SingleEnrollment: string = "";
        studentName: string = "";
        studentSsn: string = "";
        enrollmentName: string = "";
        approvalTerminationPanel: any;
        isNonR2T4View: boolean = false;
        isClockHrs: boolean = false;
        terminationId: string = "";
        r2T4Termination = new Api.R2T4StudentTermination();
        r2T4InputDetail: any;
        indexForAdditionalSec: number = 0;
        r2T4PanelResult: any = $(".panelbarR2T4Result").kendoPanelBar();
        r2T4InputModel = {} as Models.IR2T4Input;
        studentTerminationVm: StudentTerminationVm;
        studentEnrollment: StudentEnrollment;
        emptyGuid: any = "00000000-0000-0000-0000-000000000000";
        terminationModel = {} as Models.IStudentTermination;
        pwdValue: string = "";

        //constructor of approval termination view
        constructor(studentTerminationVm: StudentTerminationVm, isNonR2T4: boolean) {
            this.enrollmentName = studentTerminationVm.enrollmentName;
            this.studentName = studentTerminationVm.studentName;
            this.studentSsn = studentTerminationVm.studentSsn;
            this.isNonR2T4View = isNonR2T4;
            this.studentTerminationVm = studentTerminationVm;

            this.approveTerminationConfirm = AD.X_MESSAGE_APPROVE_TERMINATION_CONFIRM.replace("$StundentName", this.studentName).replace("$EnrollmentName", this.enrollmentName);
            this.appTermConfirmForR2T4MultEnrollment = AD.X_MESSAGE_APPROVE_TERMINATION_CONFIRM_R2T4_MULTIPLE.replace("$StundentName", this.studentName).replace("$EnrollmentName", this.enrollmentName);
            this.appTermConfirmForNonR2T4MultEnrollment = AD.X_MESSAGE_APPROVE_TERMINATION_CONFIRM_NONR2T4_MULTIPLE.replace("$StundentName", this.studentName).replace("$EnrollmentName", this.enrollmentName);
            this.appTermConfirmForR2T4SingleEnrollment = AD.X_MESSAGE_APPROVE_TERMINATION_CONFIRM_R2T4_SINGLE.replace("$StundentName", this.studentName).replace("$EnrollmentName", this.enrollmentName);
            this.appTermConfirmForNonR2T4SingleEnrollment = AD.X_MESSAGE_APPROVE_TERMINATION_CONFIRM_NONR2T4_SINGLE.replace("$StundentName", this.studentName).replace("$EnrollmentName", this.enrollmentName);

            if (isNonR2T4) {
                $("#dvR2T4AdditionalInfo").hide();
            } else {
                $("#dvR2T4AdditionalInfo").show();
            }

            this.terminationId = $("#hdnTerminationId").val();
            this.approvalTerminationPanel = $("#panelApproveTermination").kendoPanelBar();
            this.approveTerminationBackClick();
            this.approveTerminationClick();
            this.r2T4ResultPreviewClick();
            this.terminationDetailPreviewClick();
        }

        //this function will get the termination detail for given termination id
        getTerminationDetail(terminationId: string): void {
            this.r2T4Termination.getTerminationDetails(terminationId, response => {
                if (!!response) {
                    $("#lblStundentNameAppTerm").text(this.studentName);
                    $("#lblStundentSSNAppTerm").text(this.studentSsn);
                    $("#lblEnrollmentNameAppTerm").text(this.enrollmentName);
                    $("#lblEnrollmentAppTerm").text(this.enrollmentName);

                    $("#lblStatusAppTerm").text(response["status"]);
                    $("#lblDropreasonAppTerm").text(response["dropReason"]);
                    $("#lblDODAppTerm").text(response["dateOfDetermination"]);

                    if (common.attendanceUnitType === "none") {
                        $("#lblLDAAppTerm").text('');
                    } else {
                        $("#lblLDAAppTerm").text(response["lastDateAttended"]);
                    }
                    if (response["lastDateAttended"] !== "") {
                        $("#lblWithdrawalDateAppTerm").text(response["lastDateAttended"]);
                        $("#dvTerminationWithdrawalDate").show();
                    }
                    else {
                        $("#dvTerminationWithdrawalDate").hide();
                    }

                    //R2T4 Calculation Information  
                    this.getR2T4CalculationDetails(response["r2T4CalculationSummaryDetail"]);

                    //Additional Information Section  
                    this.getAdditionalInformationForR2T4(response["additionalInformationDetail"]);
                }
                this.showHideAdditionalInformationSection();
            });
        }

        //this function get the R2T4 calculation details based on the R2T4 result saved.
        getR2T4CalculationDetails(r2T4CalculationSummaryDetail: any) {
            if (!!r2T4CalculationSummaryDetail) {

                $("#lblTotalChargesAppTerm").text(r2T4CalculationSummaryDetail.totalCharges);
                $("#lblTotalTitleIVAidAppTerm").text(r2T4CalculationSummaryDetail.totalTitleIvAid);
                $("#lblTotalTitleIVAidDisburseAppTerm").text(r2T4CalculationSummaryDetail.totalTitleIvAidDisbursed);
                $("#lblPercentageEarnedAppTerm").text(r2T4CalculationSummaryDetail.percentageOfTitleIvAidEarned);

                $("#dvAmountReturnedBySchool, #dvAmountReturnedByStudent, #dvPWDAmount").hide();

                if (!!r2T4CalculationSummaryDetail.postWithdrawalDisbursement && r2T4CalculationSummaryDetail.postWithdrawalDisbursement !== "$0.00") {
                    $("#lblPWDAmountAppTerm").text(r2T4CalculationSummaryDetail.postWithdrawalDisbursement);
                    this.pwdValue = r2T4CalculationSummaryDetail.postWithdrawalDisbursement;
                    $("#dvPWDAmount").show();
                }

                if ((($("#txtResultJ").val() === "" || $("#txtResultJ").val() === "0.00") && r2T4CalculationSummaryDetail.postWithdrawalDisbursement.split('$')[1] <= 0) && !!r2T4CalculationSummaryDetail.totalTitleIvAidToReturn) {
                    $("#dvLoanAndGrantReturnByStudent, #dvLoanAndGrantReturnBySchool").empty();
                    $("#dvAmountReturnedBySchool, #dvAmountReturnedByStudent").show();
                    $("#lblTotalAidReturnAppTerm").text(r2T4CalculationSummaryDetail.totalTitleIvAidToReturn);

                    $("#lblTotalReturnBySchoolAppTerm").text(r2T4CalculationSummaryDetail.totalAmountToBeReturnedBySchool);

                    var amountToBeReturnedBySchool = r2T4CalculationSummaryDetail.amountToBeReturnedBySchool;
                    if (!!amountToBeReturnedBySchool) {
                        var index = 0;
                        amountToBeReturnedBySchool.forEach(item => {
                            index++;
                            let fieldName = (index) + ". " + item["text"];
                            let dvToAdd = `<div class='approvalTermForm marginBtm0'><div class='inLineBlock terminationDetailLabel width180px r2T4CalcResultPointLabel'>
                                    <span>${fieldName}:</span></div><div class= 'inLineBlock r2T4CalcResultsLabel width100px'><span>${item["value"]}</span></div></div>`;
                            $("#dvLoanAndGrantReturnBySchool").append(dvToAdd);
                        });
                    }

                    $("#lblTotalReturnByStudentAppTerm").text(r2T4CalculationSummaryDetail.totalAmountToBeReturnedByStudent);
                    var amountToBeReturnedByStudent = r2T4CalculationSummaryDetail.amountToBeReturnedByStudent;
                    if (!!amountToBeReturnedByStudent) {
                        var index1 = 0;
                        amountToBeReturnedByStudent.forEach(item => {
                            index1++;
                            let fieldName = (index1) + ". " + item["text"];
                            let dvToAdd = `<div class='approvalTermForm marginBtm0'><div class='inLineBlock terminationDetailLabel width180px r2T4CalcResultPointLabel'>
                                    <span>${fieldName}:</span></div><div class= 'inLineBlock r2T4CalcResultsLabel width100px'><span>${item["value"]}</span></div></div>`;
                            $("#dvLoanAndGrantReturnByStudent").append(dvToAdd);
                        });
                    }
                }
            }
        }

        //this function populate the additional information for student's R2T4
        getAdditionalInformationForR2T4(additionalInformationDetail: any) {
            if (!!additionalInformationDetail) {

                this.isClockHrs = additionalInformationDetail.isClockHour;

                //in case 50% attendance and CreditHrs
                if (!additionalInformationDetail.isClockHour &&
                    additionalInformationDetail.isNotRequiredToTakeAattendance) {
                    $("#dv50CreditHrsAdditionalInfo").show();
                    $("#spIndex_1").text(this.getNextAvailableIndex());
                    $("#spUserName").text(additionalInformationDetail.r2T4InputUserName);
                } else {
                    $("#dv50CreditHrsAdditionalInfo").hide();
                }

                //in case 100% attendance
                if (additionalInformationDetail.isAttendance100Percent) {
                    if (!additionalInformationDetail.isClockHour) {
                        $("#dv100PerClockHrsAdditionalInfo").hide();
                        $("#dv100PerCreditHrsAdditionalInfo").show();
                        $("#spIndex_2").text(this.getNextAvailableIndex());
                    } else {
                        $("#dv100PerClockHrsAdditionalInfo").show();
                        $("#dv100PerCreditHrsAdditionalInfo").hide();
                        $("#spIndex_3").text(this.getNextAvailableIndex());
                    }
                } else {
                    $("#dv100PerClockHrsAdditionalInfo").hide();
                    $("#dv100PerCreditHrsAdditionalInfo").hide();
                }

                //overridden fields with actual and overridden values
                this.getR2T4OverriddenFieldDetails(additionalInformationDetail);

                //Non refundable grants
                this.getNonRefundableGrants(additionalInformationDetail);

                let isPaymentPeriod = $("label[for=" + additionalInformationDetail.calculationPeriodTypeId + "]")
                    .text().split(" ").join("") ===
                    common.CalculationPeriodType[common.CalculationPeriodType
                        .PaymentPeriod];

                //Tuition charged by payment period' check box is not selected on 'R2T4 Input' tab
                if (additionalInformationDetail.isTuitionByPaymentPeriod &&
                    isPaymentPeriod &&
                    (this.pwdValue === "$0.00" || this.pwdValue === "")) {
                    $("#spIndex_6").text(this.getNextAvailableIndex());
                    $("#lblTotalInstituitionalChargesAppTerm").text($("#lblTotalChargesAppTerm").text());
                    $("#lblCreditBalanceRefundedToStudentAppTerm")
                        .text(additionalInformationDetail.creditBalanceRefunded);
                    $("#lblTotalTitleIVAidDisbursedAppTerm").text($("#lblTotalTitleIVAidDisburseAppTerm").text());
                    $("#dvTuitionNotChargedByPaymentPeriodAdditionalInfo").show();
                } else {
                    $("#dvTuitionNotChargedByPaymentPeriodAdditionalInfo").hide();
                }
            } else {
                $("#dvTuitionNotChargedByPaymentPeriodAdditionalInfo").hide();
            }
        }

        //this function gets overridden fields with actual and overridden values 
        getR2T4OverriddenFieldDetails(additionalInformationDetail: any) {
            if (!!additionalInformationDetail) {

                var r2T4ResultFieldsWithOverriddenValues = additionalInformationDetail.r2T4ResultFieldsWithOverriddenValues;
                if (!!r2T4ResultFieldsWithOverriddenValues) {
                    let tableRows = "";
                    $("#tbodyOverriddenR2T4Info").empty();
                    r2T4ResultFieldsWithOverriddenValues.forEach(item => {
                        let tr = `<tr><td class='left-padding4px'>${item["fieldName"]}</td><td class='align-center'>from </td><td class='align-right padding5px'>${item["resultValue"]}</td ><td class='align-center'> to </td><td class='align-right padding5px'>${item["overriddenResultValue"]}</td></tr>`;
                        tableRows = tableRows + tr;
                    });
                    if (tableRows.length > 0) {
                        $("#spIndex_4").text(this.getNextAvailableIndex());
                        $("#aTicketNumber").prop("href", `https://support.fameinc.com/hc/en-us/requests/${additionalInformationDetail.ticketNumber}`);
                        $("#spUserNameOverridden").text(additionalInformationDetail.overriddenUserName);
                        $("#aTicketNumber").text(additionalInformationDetail.ticketNumber);
                        $("#tbodyOverriddenR2T4Info").append(tableRows);
                        $("#dvOverriddenR2T4AdditionalInfo").show();
                    } else {
                        $("#dvOverriddenR2T4AdditionalInfo").hide();
                    }
                }
            }
        }

        //this function gets non refundable grants for student
        getNonRefundableGrants(additionalInformationDetail: any) {
            if (!!additionalInformationDetail) {
                var titleIvGrantLessThan50Dollar = additionalInformationDetail.titleIvGrantLessThan50Dollar;
                if (!!titleIvGrantLessThan50Dollar) {
                    let tableRows = "";
                    $("#tbodyLoanToRefund").empty();
                    titleIvGrantLessThan50Dollar.forEach(item => {
                        const tr = `<tr><td class='align-left padding5px width30'>${item["text"]}</td><td class='align-right padding5px' >${item["value"]}</td></tr>`;
                        tableRows = tableRows + tr;
                    });
                    if (tableRows.length > 0) {
                        $("#spIndex_5").text(this.getNextAvailableIndex());
                        $("#tbodyLoanToRefund").append(tableRows);
                        $("#dvLessThan50PerR2T4AdditionalInfo").show();
                    } else {
                        $("#dvLessThan50PerR2T4AdditionalInfo").hide();
                    }
                }
            }
        }

        // This function gets the count of active enrollments.
        getActiveEnrollmentsCount(enrollmentId: string): number {
            var activeEnrollmentCount = 0;
            $("input[name='studentEnrollment']").each((index, element) => {
                if (!$(element).prop("disabled") && $(element).val() !== enrollmentId) {
                    activeEnrollmentCount += 1;
                }
            });

            return activeEnrollmentCount;
        }

        //This function to get all values from the controls
        private generateR2T4TerminationModel(terminationModel: any): any {
            let isNewTermination: boolean = $("#hdnTerminationId").val() === undefined || $("#hdnTerminationId").val() === "";
            terminationModel.enrollmentId = $("input[name='studentEnrollment']:checked").val();
            terminationModel.statusCodeId = $("#ddlStatus").val() === "" ? null : $("#ddlStatus").val();
            terminationModel.dateDetermined = $("#dtDateOfDetermination").val() === "" ? null : new Date($("#dtDateOfDetermination").val()).toDateString();
            terminationModel.lastDateAttended = $("#dtLastDateAttended").val() === "" ? null : new Date($("#dtLastDateAttended").val()).toDateString();
            terminationModel.calculationPeriodType = $("input[id='chkClaculationPeriod']").prop('checked') ? $("input[name='periodName']:checked")[0].id : null;
            terminationModel.dropReasonId = $("#ddlDropReason").val() === "" ? null : $("#ddlDropReason").val();
            terminationModel.isPerformingR2T4Calculator = $("input[id='chkClaculationPeriod']").prop('checked');
            terminationModel.terminationId = isNewTermination ? this.emptyGuid : $("#hdnTerminationId").val();
            terminationModel.createdBy = terminationModel.updatedBy = $("#hdnUserId").val();
            return terminationModel;
        }

        //This function will handle the Approve Termination button click.
        approveTerminationClick(): void {
            try {
                $("#btnApproveTermination").unbind().click(() => {
                    Utility.getTabName();
                    $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(this.approveTerminationConfirm))
                        .then(confirmed => {
                            if (confirmed) {
                                if (!this.isNonR2T4View) {
                                    this.downloadR2T4ResultsReport(this.studentName,
                                        this.enrollmentName,
                                        this.isClockHrs,
                                        false,
                                        () => {
                                            this.downloadStudentSummaryReport(this.studentName,
                                                this.enrollmentName,
                                                false,
                                                () => {
                                                    this.approveTermination();
                                                });
                                        });
                                }
                                else {
                                    this.downloadStudentSummaryReport(this.studentName,
                                        this.enrollmentName, false,
                                        () => {
                                            this.approveTermination();
                                        });
                                }
                            }
                        });
                });
            }
            catch (e) {
                MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_APPROVE_TERMINATION.replace("$StundentName", common.studentName).replace("$EnrollmentName", common.enrollmentName));
            }
        }

        approveTermination() {
            let terminationDetails = this.generateR2T4TerminationModel(this.terminationModel);
            this.r2T4Termination.updateEnrollmentStatus(terminationDetails,
                (response) => {
                    if (!!response) {
                        if (response["resultStatus"] ===
                            AD.X_MESAGE_APPROVE_STUDENT_TERMINATION_SUCCESS) {
                            var studentStatusChanges = this.generateStudentStatusChangeModel();
                            this.r2T4Termination.saveStudentStatusChange(studentStatusChanges, (result) => {
                                if (!!result) {
                                    if (response["resultStatus"] !== AD.X_MESAGE_SAVE_STUDENT_STATUS_CHANGES_UNSUCCESS) {
                                        var activeEnrollments = this.getActiveEnrollmentsCount(this.studentTerminationVm.enrollmentId);
                                        if (activeEnrollments > 0) {
                                            $.when(MasterPage
                                                .SHOW_CONFIRMATION_WINDOW_PROMISE(this.isNonR2T4View ? this.appTermConfirmForNonR2T4MultEnrollment
                                                    : this.appTermConfirmForR2T4MultEnrollment))
                                                .then((confirmedYes) => {
                                                    this.onUserConfirmation(confirmedYes);
                                                });
                                        } else {
                                            $.when(MasterPage
                                                .SHOW_INFO_WINDOW(this.isNonR2T4View ? this.appTermConfirmForNonR2T4SingleEnrollment
                                                    : this.appTermConfirmForR2T4SingleEnrollment))
                                                .then(() => {
                                                    this.studentTerminationVm.reloadTerminationDetailView(true);
                                                    this.enableTerminationDetailsTab();
                                                });
                                        }

                                    } else {
                                        MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_APPROVE_TERMINATION.replace("$StundentName", common.studentName).replace("$EnrollmentName", common.enrollmentName));
                                        return;
                                    }
                                }
                            });
                        } else {
                            MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_APPROVE_TERMINATION.replace("$StundentName", common.studentName).replace("$EnrollmentName", common.enrollmentName));
                            return;
                        }
                    }
                });
        }

        //this function will handle the preview button click under R2T4 Calculation Details
        r2T4ResultPreviewClick() {
            $("#btnR2T4ResultPreview").unbind().click(() => {
                this.downloadR2T4ResultsReport(this.studentName, this.enrollmentName, this.isClockHrs, true, () => { });
            });
        }

        //this function will handle the preview button click under Termination Details
        terminationDetailPreviewClick() {
            $("#btnterminationDetailsPreview").unbind().click(() => {
                this.downloadStudentSummaryReport(this.studentName, this.enrollmentName, true, () => { });
            });
        }

        // This function binds the enrollment details generates the model for Student Status Changes.
        generateStudentStatusChangeModel(): any {
            let studentStatusChanges =
                {
                    studentEnrollmentId: this.studentTerminationVm.enrollmentDetails.enrollmentId,
                    campusId: this.studentTerminationVm.campusId,
                    newStatusId: $("#ddlStatus").val(),
                    dropReasonId: $("#ddlDropReason").val(),
                    isReversal: false,
                    dateOfChange: $("#dtDateOfDetermination").val(),
                    originalStatusId: common.statusCodeId
                } as Models.IStudentStatusChanges;

            return studentStatusChanges;
        }

        // This function will take user to the default screen basaed confirmation.
        onUserConfirmation(confirmedYes): void {
            if (confirmedYes) {
                if (this.isNonR2T4View) {
                    this.studentTerminationVm.reloadTerminationDetailView();
                    this.enableTerminationDetailsTab();
                }
                else {
                    this.studentTerminationVm.reloadTerminationDetailView();
                    this.enableTerminationDetailsTab();
                }
            } else {
                if (document.location.href !== null) {
                    window.location.assign(document.location.href);
                }
            }
        }

        // This function will call the report service to generate student termination summary report and download it
        downloadStudentSummaryReport(studentName: string, enrollmentName: string, isPreviewReport: boolean, onResponseCallback: () => any) {
            var reportRequestData = {} as API.AcademicRecords.Models.IReportRequest;
            reportRequestData.parameters = {};
            reportRequestData.parameters["StudentTerminationId"] = this.terminationId;
            reportRequestData.parameters["CampusId"] = this.studentTerminationVm.campusId;
            reportRequestData.reportLocation = Components.Common.studentTermantionSummaryReport;
            reportRequestData.parameters["reportCategory"] = "Termination Details";
            reportRequestData.parameters["reportName"] = "Termination Details";
            reportRequestData.parameters["reportExtension"] = ".pdf";
            reportRequestData.parameters["isPreviewReport"] = isPreviewReport;
            this.r2T4Termination.generateReport(reportRequestData, `${studentName}_${this.terminationId}_SummaryReport`, () => { onResponseCallback(); });
        }

        // This function will call the report service to generate R2T4 result report and download it
        downloadR2T4ResultsReport(studentName: string, enrollmentName: string, isClockHrs: boolean, isPreviewReport: boolean, onResponseCallback: () => any) {
            var reportRequestData = {} as API.AcademicRecords.Models.IReportRequest;
            reportRequestData.parameters = {};
            reportRequestData.parameters["StudentTerminationId"] = this.terminationId;
            reportRequestData.parameters["CampusId"] = this.studentTerminationVm.campusId;
            reportRequestData.reportLocation = isClockHrs ? Components.Common.studentTermantionR2T4ClockHrsReport : Components.Common.studentTermantionR2T4CreditHrsReport;
            reportRequestData.parameters["reportCategory"] = "R2T4";
            reportRequestData.parameters["reportName"] = "R2T4";
            reportRequestData.parameters["reportExtension"] = ".pdf";
            reportRequestData.parameters["isPreviewReport"] = isPreviewReport;
            this.r2T4Termination.generateReport(reportRequestData, `${studentName}_${this.terminationId}_R2T4ResultReport`, () => { onResponseCallback(); });
        }

        //This function will handle the Approve Termination Back button click
        approveTerminationBackClick() {
            $("#btnApproveTerminationBack").unbind().click(() => {
                this.expandR2T4ResultSections();
                if (this.isNonR2T4View) {
                    $("#divTerminationDetail, #liTerminationDetails").addClass("is-active").removeClass("is-visited");
                    $("#divR2T4Input, #divR2T4Result, #divApproveTermination").removeClass("is-active").removeClass("is-visited").removeClass("is-invalid");
                    $("#liR2T4Input, #liR2T4Result").removeClass("is-active is-visited").addClass("is-invalid");
                } else {
                    $("#divR2T4Result, #liR2T4Result").addClass("is-active").removeClass("is-visited");
                    $("#divTerminationDetail, #liTerminationDetails, #divR2T4Input,  #liR2T4Input, #divApproveTermination")
                        .removeClass("is-active").addClass("is-visited");
                    $("#dateDetermination").text($("#dtDateOfDetermination").val());
                    this.studentTerminationVm.initializeNewR2T4Result(this.r2T4InputDetail, false, false, false);
                }
                $("#liApproveTermination").removeClass("is-active is-invalid");
            });
        }

        //this method will initialize the student information on the header section
        initializStudentInformation(isNonR2T4: boolean): void {
            this.isNonR2T4View = isNonR2T4;
            $("#dv50CreditHrsAdditionalInfo, #dv100PerCreditHrsAdditionalInfo, #dv100PerClockHrsAdditionalInfo, #dvOverriddenR2T4AdditionalInfo, #dvLessThan50PerR2T4AdditionalInfo, #dvAdditionalInfoApproval").hide();
            $("#liApproveTermination").removeClass("is-visited").addClass("is-active");
            $("#lblStundentNameAppTerm").text(this.studentName);
            $("#lblStundentSSNAppTerm").text(this.studentSsn);
            $("#lblEnrollmentNameAppTerm").text(this.enrollmentName);

            if (isNonR2T4) {
                $("#dvR2T4CalculationApproval").hide();
            } else {
                $("#dvR2T4CalculationApproval").show();
            }

            this.expandAllPanels();

            this.getTerminationDetail(this.terminationId);
        }

        //this will expand all the panels.
        expandAllPanels(): void {
            this.approvalTerminationPanel.data("kendoPanelBar").expand($(".k-state-active"), false);
            $("#dvTerminationDetailApproval").addClass("k-state-active");
            $("#dvR2T4CalculationApproval").addClass("k-state-active");
            $("#dvAdditionalInfoApproval").addClass("k-state-active");
        }

        //This function does the UI setup for approve termination screen.
        prepareApproveTermination(isNonR2T4: boolean = true): void {
            if (isNonR2T4) {
                $("#divTerminationDetail, #liTerminationDetails").removeClass("is-active").addClass("is-visited");
                $("#divR2T4Result, #liR2T4Result, #divR2T4Input, #liR2T4Input").removeClass("is-active").removeClass("is-visited").addClass("is-invalid");
                common.isR2T4ApproveTabDisabled = false;
            } else {
                $("#divTerminationDetail, #liTerminationDetails, #divR2T4Result, #liR2T4Result, #divR2T4Input, #liR2T4Input").removeClass("is-active").removeClass("is-invalid").addClass("is-visited");
            }
            $("#divApproveTermination, #liApproveTermination").addClass("is-active").removeClass("is-visited");
            this.initializStudentInformation(isNonR2T4);
            this.expandAllPanels();
        }

        //this function returns next available index for the additional section.
        getNextAvailableIndex() {
            this.indexForAdditionalSec = this.indexForAdditionalSec + 1;
            return this.indexForAdditionalSec;
        }

        //Expanding all the panel bars for R2T4Result tab.
        expandR2T4ResultSections(): void {
            this.r2T4PanelResult.data("kendoPanelBar").expand($(".k-state-active"), true);
            $("#dvAidInfo").addClass("k-state-active");
            $("#dvPercentagebar").addClass("k-state-active");
            $("#dvAmountEarned").addClass("k-state-active");
            $("#dvDisbursed").addClass("k-state-active");
            $("#dvAidDue").addClass("k-state-active");
            $("#dvReturnFund").addClass("k-state-active");
            $("#dvAmountUnearned").addClass("k-state-active");
            $("#dvRepayment").addClass("k-state-active");
            $("#dvGrantFund").addClass("k-state-active");
            $("#dvReturnGrant").addClass("k-state-active");
            $("#dvPostWithDrawl").addClass("k-state-active");
        };

        //this function will reset the termination detail screen and will disable all the tabs as default
        enableTerminationDetailsTab(): void {
            $("#liTerminationDetails, #divTerminationDetail").removeClass("is-visited").addClass("is-active");
            $("#liApproveTermination, #divApproveTermination").removeClass("is-active");
            $("#liR2T4Input, #liR2T4Result").removeClass("is-visited is-active is-invalid");
            common.isR2T4InputTabDisabled = common.isR2T4ResultTabDisabled = common.isR2T4ApproveTabDisabled = true;
        }

        //this function will show and hide the additional information section based on the the any information is shown.
        showHideAdditionalInformationSection(): void {
            var isNoneInformationDisplayed = ($("#dvLessThan50PerR2T4AdditionalInfo").css("display") === "none"
                && $("#dvOverriddenR2T4AdditionalInfo").css("display") === "none"
                && $("#dv100PerClockHrsAdditionalInfo").css("display") === "none"
                && $("#dv100PerCreditHrsAdditionalInfo").css("display") === "none"
                && $("#dv50CreditHrsAdditionalInfo").css("display") === "none")
                && $("#dvTuitionNotChargedByPaymentPeriodAdditionalInfo").css("display") === "none";

            if (isNoneInformationDisplayed)
                $("#dvAdditionalInfoApproval").hide();
            else
                $("#dvAdditionalInfoApproval").show();
        }
    }
}