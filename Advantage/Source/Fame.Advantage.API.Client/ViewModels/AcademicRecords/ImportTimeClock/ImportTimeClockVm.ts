﻿/// <reference path="../../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../../../Fame.Advantage.API.Client/Components/Common/Grid.ts" />

module Api.ViewModels.AcademicRecords {

    import KendoGrid = Api.Components.Grid;
    import ISettings = API.Models.ISettings;
    import IFilterParameters = API.Models.IFilterParameters;
    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;

    export class ImportTimeClockVm {
        timeClockLogGrid: KendoGrid;
        timeClockImportLogDS: TimeClockImportLog

        constructor() {
        }

        public initialize() {
            var that = this;
            this.timeClockImportLogDS = new TimeClockImportLog();

            let gridSettings: ISettings = {} as ISettings;
            gridSettings.settings = {
                pageable: false,
                autoBind: false,
                serverPaging: false,
                sortable: false,
                dataBound: function (e) {
                    $('#timeclockImportLogGrid').find('tr').on('click', function (e) {
                        var txt = $(this).find('.logResults').first().text();
                        $('textarea').first().text(txt);
                    });
                },
                filterable: false,
                height: 350,
                width: 600,
                columns: [
                    {
                        field: "fileName",
                        title: "File Name",
                        width: "100px"
                    },
                    {
                        field: "message",
                        title: "Message",
                        width: "100px"
                    },
                    {
                        field: "status",
                        title: "Status",
                        width: "100px"
                    },
                    {
                        field: "modDate",
                        title: "Import Date",
                        width: "100px"
                    },
                    {
                        field: "modUser",
                        title: "Import User",
                        width: "100px"
                    }],
                rowTemplate: '<tr data-uid="#= uid #"><td>#: fileName  #</td><td class="logResults" style="display: block; height:30px;">#: message  #</td><td>#: status == 2 ? "Processing" : status == 1 ? "Processed" : "Processed - Error" #</td><td>#:  new Date(modDate).toLocaleDateString() + " " + new Date(modDate).toLocaleTimeString() #</td><td>#: (message.toLocaleLowerCase().indexOf("automated") > -1 && modUser.toLocaleLowerCase().indexOf("support") > -1) ? "Auto Import" : modUser #</td></tr>',
                altRowTemplate: '<tr class="k-alt" data-uid="#= uid #"><td>#: fileName  #</td><td class="logResults" style="display: block; height:30px;">#: message  #</td><td>#: status == 2 ? "Processing" : status == 1 ? "Processed" : "Processed - Error" #</td><td>#: new Date(modDate).toLocaleDateString() + " " + new Date(modDate).toLocaleTimeString() #</td><td>#: (message.toLocaleLowerCase().indexOf("automated") > -1 && modUser.toLocaleLowerCase().indexOf("support") > -1) ? "Auto Import" : modUser #</td></tr>',
                noRecords: {
                    template: "No log records."
                }
            };

            this.timeClockLogGrid = new KendoGrid("timeclockImportLogGrid", this.timeClockImportLogDS, undefined, { params: { campusId: XMASTER_GET_CURRENT_CAMPUS_ID } } as IFilterParameters, gridSettings);
            this.timeClockLogGrid.reload();
            this.timeClockLogGrid.fetch(() => {
                this.disableUploadIfProcessing();
            });

            setInterval((e) => {
                this.timeClockLogGrid.reload();
                this.timeClockLogGrid.fetch(() => {
                    this.disableUploadIfProcessing();
                });
            }, 5000);
        }

        disableUploadIfProcessing() {
            var dta = this.timeClockLogGrid.getData();
            var $uploadBtn = $('#ContentMain2_Submit1');
            if (dta && dta.length > 0 && dta[0].message.toLowerCase().indexOf("processing") > -1) {
                $uploadBtn.attr('disabled', 'disabled');
                $uploadBtn.val("Processing...");
            }
            else {
                $uploadBtn.attr('disabled', null);
                $uploadBtn.val("Upload");
            }
        }
    }
}