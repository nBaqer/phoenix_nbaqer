﻿module Api.ViewModels.AcademicRecords {
	export class ImportTimeClock {
        importTimeClockVm: ImportTimeClockVm;

        constructor() {
            this.importTimeClockVm = new ImportTimeClockVm();
        }

        public initialize() {
            this.importTimeClockVm.initialize();
        }
	}
}