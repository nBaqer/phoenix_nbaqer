﻿/// <reference path="../../../../AdvWeb/Kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../../../Fame.Advantage.Client.AD/Common/DropDownOutputModel.ts" />
/// <reference path="../../../../Fame.Advantage.API.Client/Components/Common/DropDownList.ts" />
/// <reference path="../../../../Fame.Advantage.API.Client/Components/Common/Grid.ts" />
module Api.ViewModels.AcademicRecords.TitleIVResults {
    import IStudentEnrollment = API.Models.IStudentEnrollment;
    import ISettings = API.Models.ISettings;
    import DropDownList = Api.Components.DropDownList;
    import KendoGrid = Api.Components.Grid;
    import IFilterParameters = API.Models.IFilterParameters;
    import IDataProvider = API.Models.IDataProvider;

    export class TitleIVResultsVm {
        enrollmentsdd: DropDownList;
        enrollments: Enrollments;
        titleIvResultsGrid: KendoGrid;
        selectedEnrollment: string;
        selectedEnrollmentText: string;
        studentId: string;
        titleIvFilterParams: IFilterParameters;
        constructor(studentId: string) {
            this.studentId = studentId;
        }


        public initialize() {
            var that = this;
            let enrollment = new Enrollments();


            let titleIvResultsFilterSettings: ISettings =
                { settings: { dataTextField: "programVersionDescription", dataValueField: "enrollmentId", autoBind: false } } as ISettings;
            let enrollmentFilterParams: IFilterParameters = {
                params: {
                    StudentId: that.studentId,
                    IsActiveEnrollments: true
                }
            } as IFilterParameters;




            that.enrollmentsdd = new DropDownList("enrollmentsdd", enrollment, enrollmentFilterParams, titleIvResultsFilterSettings, (value) => {
                that.selectedEnrollment = this.enrollmentsdd.getValue();
                that.selectedEnrollmentText = this.enrollmentsdd.getKendoDropDown().text();

                let titleIv = new TitleIV();

                let titleIvGridSettings: ISettings = {} as ISettings;

                titleIvGridSettings.settings = {
                    columns: [
                        {
                            field: "increment",
                            title: "Increment",
                            width: 40
                        },
                        {
                            field: "triggeredAt",
                            title: "Triggered At",
                            width: 80
                        },
                        {
                            field: "checkPointDateString",
                            title: "Check Point",
                            width: 40
                        },
                        {
                            field: "sapCheckDateString",
                            title: "Recalc Date",
                            width: 40
                        },
                        {
                            field: "sapStatus",
                            title: "SAP Status",
                            width: 40
                        },
                        {
                            field: "reason",
                            title: "Reason",
                            width: 80
                        },
                        {
                            field: "changeStatus",
                            title: "Change Status",

                            template: dataItem => {
                                let status = dataItem.canMoveToProbation;

                                if (status === true) {

                                    return '<input type="button" class="k-button info placeOnProbation"  name="info"  value="Place on Probation" />';
                                }

                                return " ";
                            },

                            filterable: false,
                            sortable: false,
                            width: 50
                        },
                        {
                            field: "canMoveToProbation",
                            filterable: false,
                            sortable: false,
                            hidden: true


                        },
                        {  // this needs to be the last column 
                            field: "fasapCheckResultsId",
                            filterable: false,
                            sortable: false,
                            hidden: true

                        }
                    ],
                    noRecords: {
                        template: "No Title IV SAP records for this enrollment."
                    }
                };

                that.titleIvFilterParams = {
                    params: { StudentId: that.studentId, EnrollmentId: that.selectedEnrollment }
                } as IFilterParameters;

                that.titleIvResultsGrid = new KendoGrid("titleIvResultsGrid", titleIv, undefined, that.titleIvFilterParams, titleIvGridSettings);


            });

            that.enrollmentsdd.fetch(() => {
                let selEnrollment: string = MasterPage.GET_QUERY_STRING_PARAMETER_BY_NAME("selectedEnrollment");
                if (selEnrollment) {
                    that.enrollmentsdd.setValue(selEnrollment);
                }
            });

            $(document).off("click").on("click", ".placeOnProbation",
                (e) => {

                    $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE("Student SAP status for this increment will be changed from Title IV Ineligible to Title IV Probation. Do you want to continue? "))
                        .then(confirmed => {

                            if (confirmed) {
                                var row = $(e.target).closest('tr');
                                var fASAPCheckResultsId = row.find('td:last').text().toString();
                                that.updateTitleIVToPrbation(fASAPCheckResultsId);
                            }

                        });

                });
        }


        updateTitleIVToPrbation(fASAPCheckResultsId) {

            var titleIVApi = new Api.TitleIV();
            var that = this;
            titleIVApi.PlaceStudentOnTitleIVPrbation({ FASAPCheckResultsId: fASAPCheckResultsId }, (response: any) => {
                if (response === true) {

                    location.href = MasterPage.updateQueryStringParameter(location.href, "selectedEnrollment", that.enrollmentsdd.getValue());
                    //that.titleIvResultsGrid.reload();
                } else {
                    MasterPage.SHOW_ERROR_WINDOW("Error occurred when changing Title IV Ineligible to Title IV Probation.");
                }


            });


        }

    }




}