﻿module Api.ViewModels.AcademicRecords.TitleIVResults {

    export class TitleIVResults {
        viewModel: TitleIVResultsVm;
        
        constructor(studentId: string) {
            try {
                this.viewModel = new TitleIVResultsVm(studentId);
                
            } catch (e) {
                $("body").css("cursor", "default");
                MasterPage.SHOW_ERROR_WINDOW(e.message + "/n" + e.stack);
            }
        }

        public initialize() {
            this.viewModel.initialize();
        }

        
    }
}