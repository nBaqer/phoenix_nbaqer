﻿/// <reference path="../../../API/AcademicRecords/Attendance/ScheduledHours.ts" />
/// <reference path="../../../API/SystemCatalog/Campus.ts" />
/// <reference path="../../../API/AcademicRecords/ProgramVersion.ts" />
/// <reference path="../../../API/AcademicRecords/StudentGroups.ts" />
/// <reference path="../../../Components/Common/DropDownList.ts" />
/// <reference path="../../../Components/Common/Grid.ts" />
/// <reference path="../../../API/Tools/GradDateCalculator.ts" />
module Api.ViewModels.AcademicRecords.Attendance {
    import DropDownList = Api.Components.DropDownList;
    import ISettings = API.Models.ISettings;
    import IFilterSearch = API.Models.IFilterSearch;
    import IFilterParams = API.Models.IFilterParameters;
    import AdjustScheduledHours = API.Models.IAdjustScheduledHours;
    import ProgramVersion = Api.ProgramVersion;
    import StudentGroups = Api.StudentGroups;
    import ScheduledHours = Api.Attendance.ScheduledHours;
    import Campus = Api.Campus;
    import KendoGrid = Api.Components.Grid;
    import AdjustableSchedules = API.Enums.AdjustableSchedules;
    import AdjustmentMethods = API.Enums.AdjustmentMethods;
    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;
    export class AdjustScheduledHoursVm {
        calculationData: any;
        campus: Campus;
        scheduledHours: ScheduledHours;
        studentGroups: StudentGroups;
        studentsGrid: KendoGrid;
        gridParams: IFilterParams;
        gridSearch: IFilterSearch;
        programVersion: ProgramVersion;
        dateOfAdjustmentPicker: kendo.ui.DatePicker;
        dateOfAdjustment: Date;
        adjustmentAmountInput: JQuery;
        adjustmentMethod: AdjustmentMethods;
        scheduleToAdjust: AdjustableSchedules;
        hoursContainer: JQuery;
        studentGroupsContainer: JQuery;
        programVersionContainer: JQuery;
        listBox: kendo.ui.ListBox;
        campusDropDownList: DropDownList;
        programVersionDropDownList: DropDownList;
        studentGroupsDropDownList: DropDownList;
        scheduleToAdjustDropDown: kendo.ui.DropDownList;
        adjustmentMethodDropDown: kendo.ui.DropDownList;
        campusId: string;
        programVersionId: string;
        studentGroupId: string;
        emptyGuid: string = "00000000-0000-0000-0000-000000000000";
        mainPane: JQuery;
        splitter: kendo.ui.Splitter;


        constructor() {
            this.campus = new Campus();
            this.programVersion = new ProgramVersion();
            this.scheduledHours = new ScheduledHours();
            this.studentGroups = new StudentGroups();
            this.hoursContainer = $("#hoursAmountContainer");
            this.studentGroupsContainer = $("#studentGroupsContainer");
            this.programVersionContainer = $("#programVersionContainer");
            this.adjustmentAmountInput = $("#txtHoursAdjustment");
            this.mainPane = $(".main-container");
        }

        public initialize() {

            var that = this;
            let gridSettings: ISettings = {} as ISettings;
            this.gridParams = {} as IFilterParams;
            this.gridSearch = {} as IFilterSearch;

            gridSettings.settings = {
                autoBind: false,
                columns: [{
                    field: "studentName",
                    title: "Student Name",
                    width: 125
                },
                {
                    field: "scheduledHours",
                    title: "Scheduled Hours",
                    width: 50
                },
                {
                    field: "totalPriorAdjustments",
                    title: "Adjustments",
                    width: 50
                },
                {
                    field: "actualHours",
                    title: "Actual Hours",
                    template: function (dataItem: any) {
                        var val = "Not Posted";
                        if (dataItem.actualHours !== 9999) {
                            val = dataItem.actualHours;
                        }
                        return val;
                    },
                    width: 50
                    //template: "<td class role='gridcell'>#: 555 # </td>"
                },
                {
                    field: "studentGroups",
                    title: "Student Groups",
                    width: 100
                },
                {
                    field: "programVersion",
                    title: "Program Version",
                    width: 100
                }],
                groupable: false,
                group: { field: "leadId" },
                height: "50%",
            };

            // create DatePicker from input HTML element
            $("#dpDateOfAdjustment").kendoDatePicker({
                change: function () {
                    that.dateOfAdjustment = this.value();
                },
                open: function () {
                    var calendar = this.dateView.calendar;
                }
            });


            $("#searchBtn").on('click',
                () => {

                    if (!this.campusId) {
                        MasterPage.SHOW_INFO_WINDOW("Please select a campus.");
                        return;
                    }

                    if (!this.dateOfAdjustment) {
                        MasterPage.SHOW_INFO_WINDOW("Date of Adjustment is required.");
                        return;
                    }

                    this.gridParams.params = {};
                    this.gridParams.params["campusId"] = that.campusId;
                    this.gridParams.params["scheduledDay"] = kendo.toString(that.dateOfAdjustment, 'd');

                    if (that.programVersionContainer.is(":visible")) {
                        this.gridParams.params["programVersionId"] = that.programVersionId;
                    }

                    if (that.studentGroupsContainer.is(":visible")) {
                        this.gridParams.params["studentGroupId"] = that.studentGroupId;
                    }

                    this.studentsGrid.reload(undefined, this.gridParams);
                    this.studentsGrid.fetch();
                });


            $("#btnSave").on("click", (e) => {

                let adjustScheduledHours = {} as AdjustScheduledHours;

                adjustScheduledHours.campusId = that.campusId;
                adjustScheduledHours.adjustmentDate = kendo.toString(that.dateOfAdjustment, 'd');
                adjustScheduledHours.adjustmentMethod = parseInt(that.adjustmentMethodDropDown.value());

                if (that.programVersionContainer.is(":visible")) {
                    adjustScheduledHours.programVersionId = that.programVersionId;
                }

                if (that.studentGroupsContainer.is(":visible")) {
                    adjustScheduledHours.studentGroupId = that.studentGroupId;
                }

                if (that.hoursContainer.is(":visible")) {
                    adjustScheduledHours.amountToAdjust = that.adjustmentAmountInput.val();
                }

                that.scheduledHours.adjustScheduledHours(adjustScheduledHours, function (response: any) {
                    if (response.resultStatus !== 0) {
                        MasterPage.SHOW_ERROR_WINDOW(response.resultStatusMessage ? response.resultStatusMessage : "Error adjusting hours.");
                    }
                    else {
                        MasterPage.SHOW_INFO_WINDOW("Successfully updated scheduled hours.");
                    }

                    $("#searchBtn").click();
                });

                e.preventDefault();
            });

            $("#btnNew").on("click", () => {

                that.campusDropDownList.getKendoDropDown().select((x) => {
                    return x.value == XMASTER_GET_CURRENT_CAMPUS_ID;
                });

                that.dateOfAdjustmentPicker.value("");
                that.adjustmentAmountInput.val("0.00");
                $("#ddlSelectAdjustmentMethod").data("kendoDropDownList").select(0);
                $("#ddlSelectAdjustmentMethod").data("kendoDropDownList").trigger("change");
                $("#ddlDefineScheduleAdjust").data("kendoDropDownList").select(0);
                $("#ddlDefineScheduleAdjust").data("kendoDropDownList").trigger("change");
                that.studentGroupsDropDownList.reset();
                that.programVersionDropDownList.reset();
                that.studentsGrid.clear();
                this.studentsGrid.fetch();
            });


            $("#ddlSelectAdjustmentMethod").kendoDropDownList({
                change: function () {
                    that.adjustmentMethod = this.value();

                    if (that.adjustmentMethod == AdjustmentMethods.SetCustom) {
                        that.hoursContainer.show();
                    }
                    else {
                        that.hoursContainer.hide();
                    }
                }
            });

            $("#ddlDefineScheduleAdjust").kendoDropDownList({
                change: function () {
                    that.scheduleToAdjust = this.value();

                    if (that.scheduleToAdjust == AdjustableSchedules.StudentGroup) {
                        that.studentGroupsContainer.show();
                        that.programVersionContainer.hide();
                    }
                    else if (that.scheduleToAdjust == AdjustableSchedules.ProgramVersion) {
                        that.studentGroupsContainer.hide();
                        that.programVersionContainer.show();
                    }
                    else {
                        that.studentGroupsContainer.hide();
                        that.programVersionContainer.hide();
                    }
                }
            });


            that.campusDropDownList = new DropDownList("ddlCampus", that.campus, undefined, {
                settings: {
                    dataValueField: "value"
                }
            } as ISettings, () => {

                that.campusId = that.campusDropDownList.getValue();

                if (that.campusDropDownList.hasValue()) {

                    that.programVersionDropDownList.getKendoDropDown().enable(true);
                    that.programVersionDropDownList.reload({ params: { campusId: that.campusId } } as IFilterParams);

                    that.studentGroupsDropDownList.getKendoDropDown().enable(true);
                    that.studentGroupsDropDownList.reload({ params: { campusId: that.campusId } } as IFilterParams);

                } else {
                    that.programVersionDropDownList.getKendoDropDown().enable(false);
                    that.programVersionDropDownList.getKendoDropDown().select(0);
                    that.programVersionDropDownList.getKendoDropDown().trigger("change");
                    that.studentGroupsDropDownList.getKendoDropDown().enable(false);
                    that.studentGroupsDropDownList.getKendoDropDown().select(0);
                }

            });

            that.campusDropDownList.getKendoDropDown().bind("dataBound", () => {

                if (that.campusDropDownList.getKendoDropDown().list.length > 0) {

                    that.campusDropDownList.getKendoDropDown().select((x) => {
                        return x.value == XMASTER_GET_CURRENT_CAMPUS_ID;
                    });

                    that.campusDropDownList.getKendoDropDown().trigger("change");
                    that.campusId = that.campusDropDownList.getValue();
                    that.programVersionDropDownList.reload({ params: { campusId: that.campusId } } as IFilterParams);
                    that.studentGroupsDropDownList.reload({ params: { campusId: that.campusId } } as IFilterParams);
                }

            })

            that.programVersionDropDownList = new DropDownList("ddlProgramVersion", that.programVersion, undefined, { settings: { dataValueField: "value", autoBind: false } } as ISettings,
                () => {
                    that.programVersionId = that.programVersionDropDownList.getValue();
                });

            that.studentGroupsDropDownList = new DropDownList("ddlStudentGroups", that.studentGroups, undefined, { settings: { dataValueField: "value", autoBind: false } } as ISettings,
                () => {
                    that.studentGroupId = that.studentGroupsDropDownList.getValue();
                });

            //Initialize student grid
            that.studentsGrid = new KendoGrid("studentsGrid", that.scheduledHours, undefined,
                {
                    params: {
                        campusId: that.campusId,
                        scheduledDay: kendo.toString(that.dateOfAdjustment, 'd')
                    }
                }, gridSettings);

            $("#horizontal").kendoSplitter({
                panes: [
                    { collapsible: true, resizable: false, min: "20%", size: "20%" },
                    { collapsible: false, resizable: false, min: "80%", size: "80%" }
                ]
            });

            that.splitter = $("#horizontal").data("kendoSplitter");
            that.splitter.bind("collapse", () => { that.recalculateGridHeight() });
            that.splitter.bind("resize", () => { that.recalculateGridHeight() });

            new ResizeSensor(that.mainPane, function () {
                that.splitter.resize();
            });

            that.dateOfAdjustmentPicker = $("#dpDateOfAdjustment").data("kendoDatePicker");
            that.adjustmentMethodDropDown = $("#ddlSelectAdjustmentMethod").data("kendoDropDownList");
        }

        recalculateGridHeight() {
            let fg_cont = $(".flexGrid-container");
            let fg_filter = $(".flexGrid-filter");
            let fg_grouping_header = $(".flexGrid .k-grouping-header");
            let fg_grid_header = $(".flexGrid .k-grid-header");
            let fg_grid_content = $(".flexGrid .k-grid-content");
            let fg_grid_footer = $(".flexGrid .k-grid-pager");
            let fg_grid = $(".flexGrid .k-grid");

            fg_grid.css("height", "");
            if (!fg_cont || !fg_grid_content) {
                return;
            }
            let adjustment_height = 0;
            let grid_content_height = fg_cont.innerHeight();

            if (fg_filter) {
                adjustment_height += fg_filter.innerHeight();
            }
            if (fg_grouping_header) {
                adjustment_height += fg_grouping_header.innerHeight();
            }
            if (fg_grid_header) {
                adjustment_height += fg_grid_header.innerHeight();
            }
            if (fg_grid_footer) {
                adjustment_height += fg_grid_footer.innerHeight();
            }

            grid_content_height -= adjustment_height + 26;
            fg_grid_content.innerHeight(grid_content_height);
        }
    }
}