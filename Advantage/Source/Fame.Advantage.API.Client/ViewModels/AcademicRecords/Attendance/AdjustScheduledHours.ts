﻿module Api.ViewModels.AcademicRecords.Attendance {

    export class AdjustScheduledHours {

        adjustScheduledHoursVm: AdjustScheduledHoursVm;
        constructor() {
            this.adjustScheduledHoursVm = new AdjustScheduledHoursVm();
        }

        public initialize() {
            this.adjustScheduledHoursVm.initialize();
        }
    }
}