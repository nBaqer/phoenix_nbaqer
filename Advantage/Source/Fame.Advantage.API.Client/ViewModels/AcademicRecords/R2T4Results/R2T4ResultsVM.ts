﻿/// <reference path="../../../../Fame.Advantage.Client.MasterPage/AdvantageValidation.ts"/>
/// <reference path="../../../../fame.advantage.client.masterpage/dialogbox.ts"/>
/// <reference path="../../../../AdvWeb/Kendo/typescript/kendo.all.d.ts"/>
/// <reference path="../../../../Fame.Advantage.Client.AD/Common/Enumerations.ts"/>    
/// <reference path="../../../../Fame.Advantage.API.Client/ViewModels/AcademicRecords/StudentTermination/ApproveTermination/ApproveTerminationVm.ts" />

module API.ViewModels.AcademicRecords.R2T4Results {
    import constants = Components.Common;
    import Utility = Common.Utility;
    let r2T4Termination = new Api.R2T4StudentTermination();
    import iR2T4Results = API.AcademicRecords.Models.IR2T4Results;
    import iResultMap = API.AcademicRecords.Models.IResultMap;
    import iR2T4ResultStatus = API.AcademicRecords.Models.IR2T4ResultStatus;
    import ApproveTerminationVm = AcademicRecords.StudentTermination.ApproveTermination.ApproveTerminationVm;

    /**
        * Master View Model for the R2T4Results tab in student termination Page.
        * The viewModel will have all the necessary UI business logic and it will connect UI elements to their API request and components.
        * The master viewModel can initialize children viewModels based on workflows needs. For example, the initialize method will set up the first screen only.
        * The second step(tab) can be another view model called StudentTermionationStep2Vm for example, and be called when is needed or be declared as an object to be used
        * inside the StudentTerminationVm itself.
       */
    export class R2T4ResultsVm {
        public isInputModified: boolean = false;
        public utility: Utility;
        studentName: string;
        approveTerminationVm: ApproveTerminationVm;
        studentSsn: string;
        calculationPeriodTypeId: string;
        enrollmentName: string;
        emptyGuid: string = "00000000-0000-0000-0000-000000000000";
        resultMap = {} as iResultMap;
        r2T4ResultStatus = {} as iR2T4ResultStatus;
        public dateValidation: boolean = true;
        r2T4Panel: any = $(".panelbar").kendoPanelBar();
        r2T4PanelResult: any = $(".panelbarR2T4Result").kendoPanelBar();
        studentTerminationVm: StudentTerminationVm;

        constructor(studentTerminationVm: StudentTerminationVm) {
            this.studentTerminationVm = studentTerminationVm;
            /*
             *  event to capture the Save button click event in R2T4 results tab.
             */
            (document.getElementById("btnR2T4ResultSave") as HTMLInputElement).onclick = () => {
                if (this.validate()) {
                    if ($("#ChkOverride").is(':checked')) {
                        if ($("#txtTicket").val() === '') {
                            MasterPage.SHOW_INFO_WINDOW(AD.X_MESSAGE_R2T4Result_TICKET_NO_REQUIRED);
                            return;
                        }
                    }
                }
                if (this.validate()) {
                    let isSupportUser: boolean;
                    let r2T4ResultModel = {} as iR2T4Results;
                    let r2T4ResultDetail: iR2T4Results = this.generateR2T4ResultModel(r2T4ResultModel);
                    Utility.isSupportUser(
                        (response) => {
                            if (response) {
                                isSupportUser = true;
                                if ($("#ChkOverride").is(':checked')) {
                                    Utility.checkMessagesForOverride(true,(messageResponse) => {
                                        if (messageResponse) {
                                            r2T4ResultDetail.isR2T4ResultsCompleted = constants.r2T4CompletionStatus.isR2T4ResultsCompleted = false;
                                            r2T4ResultDetail.isR2T4OverrideResultsCompleted = constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted = false;
                                            this.saveOrUpdateOverrideResults(r2T4ResultDetail, true, () => { }, true);
                                            constants.isR2T4ApproveTabDisabled = true;
                                        } else {
                                            constants.isR2T4ApproveTabDisabled = false;
                                            r2T4Termination.fetchR2T4Inputdetails($("#hdnTerminationId").val(),
                                                (r2T4InputDetail) => {
                                                    if (!!r2T4InputDetail) {
                                                        var r2T4InputResult = this.studentTerminationVm.getR2T4InputResult(r2T4InputDetail);
                                                        this.studentTerminationVm.initializeNewR2T4Result(r2T4InputResult, false, false, false);
                                                    }
                                                });
                                        }
                                    });
                                } else {
                                    this.saveOrUpdateR2T4Results(r2T4ResultDetail, true,false, () => { });
                                }
                            } else {
                                isSupportUser = false;
                                this.saveOrUpdateR2T4Results(r2T4ResultDetail, true,false, () => { });
                            }
                        });
                }
            };

            //event to navigate to approve termination page and after clicking next button.
            (document.getElementById("btnR2T4ResultNext") as HTMLInputElement).onclick = () => {
                this.expandR2T4ResultSections();
                if (this.validate()) {
                    if ($("#ChkOverride").prop("checked") && $("#txtTicket").val() === "") {
                        MasterPage.SHOW_INFO_WINDOW(AD.X_MESSAGE_R2T4Result_TICKET_NO_REQUIRED);
                        constants.isR2T4ResultsCompleted = false;
                        return;
                    }
                    if ($("#ChkOverride").is(':checked')) {
                        Utility.checkMessagesForOverride(false,
                            (response) => {
                                constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted = true;
                                Utility.isResultModified = false;
                                if (response) {
                                    this.navigateToApproveTermination(true); 
                                } else {
                                    this.initializeApproveTermination();
                                }
                               
                            });
                    } else {
                        constants.r2T4CompletionStatus.isR2T4ResultsCompleted = true;
                        Utility.isResultModified = false;
                        this.navigateToApproveTermination(true);
                        this.initializeApproveTermination();
                    }
                }
            };

            //event to check results modified or not.
            (document.getElementById("divR2T4Result") as HTMLInputElement).onchange = () => {
                if ($("#ChkOverride").is(':checked')) {
                    Utility.isResultModified = true;
                }
            };
        }

        public saveResultsOnBackClick() {
            if ($("#ChkOverride").is(':checked')) {
                if ($("#txtTicket").val() === '') {
                    MasterPage.SHOW_INFO_WINDOW(AD.X_MESSAGE_R2T4Result_TICKET_NO_REQUIRED);
                    return;
                }
            }
            if (this.validate()) {
                this.expandR2T4InputSections();
                let isSupportUser: boolean;
                let r2T4ResultModel = {} as iR2T4Results;
                let r2T4ResultDetail: iR2T4Results = this.generateR2T4ResultModel(r2T4ResultModel);
                Utility.isSupportUser(
                    (response) => {
                        if (response) {
                            isSupportUser = true;
                            if ($("#ChkOverride").is(':checked')) {
                                r2T4ResultDetail.isR2T4ResultsCompleted = (constants.r2T4CompletionStatus.isR2T4ResultsCompleted || constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted);
                                constants.r2T4CompletionStatus.isR2T4ResultsCompleted = r2T4ResultDetail.isR2T4ResultsCompleted;
                                this.saveOrUpdateOverrideResults(r2T4ResultDetail, false, () => { });
                            } else {
                                r2T4ResultDetail.isR2T4ResultsCompleted = (constants.r2T4CompletionStatus.isR2T4ResultsCompleted || constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted);
                                constants.r2T4CompletionStatus.isR2T4ResultsCompleted = r2T4ResultDetail.isR2T4ResultsCompleted;
                                this.saveOrUpdateR2T4Results(r2T4ResultDetail, false, false, () => { });
                            }
                        } else {
                            isSupportUser = false;
                            r2T4ResultDetail.isR2T4ResultsCompleted = (constants.r2T4CompletionStatus.isR2T4ResultsCompleted || constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted);
                            constants.r2T4CompletionStatus.isR2T4ResultsCompleted = r2T4ResultDetail.isR2T4ResultsCompleted; 
                            this.saveOrUpdateR2T4Results(r2T4ResultDetail, false, false, () => { });
                        }

                    });
                constants.isR2T4ResultTabDisabled = false;
                $("#liR2T4Input, #liR2T4Result").removeClass("is-invalid");
                r2T4Termination.getR2T4CompletedStatus($("#hdnTerminationId").val(),
                    (completionStatus: any) => {
                        if (!!completionStatus) {
                            constants.r2T4CompletionStatus = completionStatus;
                            if (constants.r2T4CompletionStatus.isR2T4ResultsCompleted || constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted || constants.isR2T4ResultsCompleted) {
                                $("#liR2T4Result").removeClass("is-active is-invalid").addClass("is-visited");
                            } else {
                                $("#liR2T4Result").removeClass("is-active is-invalid is-visited");
                            }
                        }
                    });
                this.isInputModified = false;
                this.navigateToR2T4Input();
            }
        }

        //method to navigate to approve termination.
        public navigateToApproveTermination(isR2T4ResultsCompleted: boolean = false) {
            if ($("#ChkOverride").prop("checked") && $("#txtTicket").val() === "") {
                MasterPage.SHOW_INFO_WINDOW(AD.X_MESSAGE_R2T4Result_TICKET_NO_REQUIRED);
                constants.isR2T4ResultsCompleted = false;
                return;
            }
            //validates the result data.
            if (this.validate()) {
                let isSupportUser: boolean;
                let r2T4ResultModel = {} as iR2T4Results;
                let r2T4ResultDetail: iR2T4Results = this.generateR2T4ResultModel(r2T4ResultModel);
                Utility.isSupportUser(
                    (response) => {
                        if (response) {
                            isSupportUser = true;
                            if ($("#ChkOverride").is(':checked')) {
                                r2T4ResultDetail.isR2T4OverrideResultsCompleted = constants.isR2T4ResultsCompleted = (constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted || isR2T4ResultsCompleted);
                                constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted = r2T4ResultDetail.isR2T4OverrideResultsCompleted;
                                r2T4ResultDetail.isR2T4ResultsCompleted = true;
                                this.saveOrUpdateOverrideResults(r2T4ResultDetail, false, () => {
                                    constants.r2T4CompletionStatus.isR2T4ResultsCompleted = true;
                                    Utility.isResultModified = false;
                                    this.initializeApproveTermination();
                                });
                            } else {
                                r2T4ResultDetail.isR2T4ResultsCompleted = (constants.r2T4CompletionStatus.isR2T4ResultsCompleted || isR2T4ResultsCompleted);
                                constants.r2T4CompletionStatus.isR2T4ResultsCompleted = r2T4ResultDetail.isR2T4ResultsCompleted;
                                this.saveOrUpdateR2T4Results(r2T4ResultDetail, false, false, () => {
                                    constants.r2T4CompletionStatus.isR2T4ResultsCompleted = true;
                                    Utility.isResultModified = false;
                                    this.initializeApproveTermination();
                                }, true);
                            }
                        } else {
                            isSupportUser = false;
                            r2T4ResultDetail.isR2T4ResultsCompleted = (constants.r2T4CompletionStatus.isR2T4ResultsCompleted || isR2T4ResultsCompleted);
                            constants.r2T4CompletionStatus.isR2T4ResultsCompleted = r2T4ResultDetail.isR2T4ResultsCompleted;
                            this.saveOrUpdateR2T4Results(r2T4ResultDetail, false, false, () => {
                                constants.r2T4CompletionStatus.isR2T4ResultsCompleted = true;
                                Utility.isResultModified = false;
                                this.initializeApproveTermination();
                            }, true);
                        }
                        constants.isR2T4ApproveTabDisabled = false;
                        constants.isR2T4ResultTabDisabled = false;
                    });
            } else {
                constants.isR2T4ResultsCompleted = false;
            }
        }

        /*
        *  saveOrUpdateOverrideResults method saves or updates the Overridden results.
        */
        public saveOrUpdateOverrideResults(r2T4ResultDetail, showMsg: boolean, callbackOnResponse: () => any, isR2T4ResultsIncomplete: boolean = false) {
            Utility.getTabName();
            try {
                this.isSaveOrUpdate(r2T4ResultDetail,
                    AD.R2T4ResultType.R2T4Override,
                    (result) => {
                        if (result !== undefined &&
                            result !== null &&
                            result.toString() !== this.emptyGuid &&
                            result.toString() !== "") {
                            r2T4ResultDetail.ticketNumber = $("#txtTicket").val();
                            r2T4ResultDetail.isOverride = $("#ChkOverride").is(':checked');
                            r2T4ResultDetail.r2T4ResultsId = result;

                            r2T4Termination.updateOverrideR2T4Results(r2T4ResultDetail,
                                (response: any) => {
                                    $("#hdnR2T4ResultId").val(response["r2T4ResultsId"]);
                                    if (response["resultStatus"] === AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                        if (showMsg) {
                                            MasterPage.SHOW_INFO_WINDOW(response.resultStatus);
                                        }
                                    } else {
                                        MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL.replace("$CurrentTabName", constants.activeTabName).replace("$CurrentTabName", constants.activeTabName));
                                        return;
                                    }
                                    callbackOnResponse();
                                });
                        } else {
                            r2T4ResultDetail.ticketNumber = $("#txtTicket").val();
                            r2T4ResultDetail.isOverride = $("#ChkOverride").is(':checked');
                            r2T4Termination.saveOverrideR2T4Results(r2T4ResultDetail,
                                (response: any) => {
                                    if (response["resultStatus"] === AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                        $("#hdnR2T4ResultId").val(response["r2T4ResultsId"]);
                                        constants.isR2T4OverrideResultsExists = true;
                                        if (showMsg) {
                                            MasterPage.SHOW_INFO_WINDOW(response.resultStatus);
                                        }
                                    }
                                    else {
                                        MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL.replace("$CurrentTabName", constants.activeTabName).replace("$CurrentTabName", constants.activeTabName));
                                        return;
                                    }
                                    if (isR2T4ResultsIncomplete) {
                                        this.r2T4ResultStatus.terminationId = $("#hdnTerminationId").val();
                                        this.r2T4ResultStatus.status = false;
                                        r2T4Termination.updateR2T4ResultStatus(this.r2T4ResultStatus, () => {});
                                    }
                                    callbackOnResponse();
                                });
                        }
                    });
            }
            catch (e) {
                MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL.replace("$CurrentTabName", constants.activeTabName).replace("$CurrentTabName", constants.activeTabName));
            }
        }

        //this function initialize the approve termination screen and setup the user interface.
        initializeApproveTermination(): void {
            Utility.changeElementStyle("divTerminationDetail", "is-active", false);
            Utility.changeElementStyle("liTerminationDetails", "is-active", false);
            Utility.changeElementStyle("divR2T4Input", "is-active", false);
            Utility.changeElementStyle("liR2T4Input", "is-active", false);
            Utility.changeElementStyle("divR2T4Result", "is-active", false);
            Utility.changeElementStyle("liR2T4Result", "is-active", false);
            $("#liTerminationDetails, #liR2T4Input, #liR2T4Result ").removeClass("is-active").addClass("is-visited");
            $("#liApproveTermination, #divApproveTermination").addClass("is-active");
            (document.getElementById("divApproveTermination") as HTMLInputElement).style.visibility = 'visible';

            this.approveTerminationVm = new ApproveTerminationVm(this.studentTerminationVm, false);
            this.approveTerminationVm.prepareApproveTermination(false);
        }
        /*
        *  saveOrUpdateR2T4Results methods saves or updates the R2T4 Results.
        */
        public saveOrUpdateR2T4Results(r2T4ResultDetail, showMsg: boolean, isInputModified: boolean, callbackOnResponse: () => any, isOnResultsNextClick: boolean = false) {
            Utility.getTabName();
            try {
                this.isSaveOrUpdate(r2T4ResultDetail,
                    AD.R2T4ResultType.R2T4Result,
                    (result) => {
                        if (result !== undefined &&
                            result !== null &&
                            result.toString() !== this.emptyGuid &&
                            result.toString() !== "") {
                            r2T4ResultDetail.r2T4ResultsId = result;
                            if (isInputModified) {
                                r2T4Termination.updateR2T4Results(r2T4ResultDetail,
                                    (response: any) => {
                                        $("#hdnR2T4ResultId").val(response["r2T4ResultsId"]);

                                        if (response["resultStatus"] ===
                                            AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                            if (showMsg) {
                                                MasterPage.SHOW_INFO_WINDOW(response.resultStatus);
                                            }
                                        } else {
                                            MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL
                                                .replace("$CurrentTabName", constants.activeTabName)
                                                .replace("$CurrentTabName", constants.activeTabName));
                                            return;
                                        }
                                        callbackOnResponse();
                                    });
                            }
							else {
                                if (showMsg) {
                                    MasterPage.SHOW_INFO_WINDOW(AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS);
                                }
                                else {
                                    if (isOnResultsNextClick) {
                                        this.r2T4ResultStatus.terminationId = $("#hdnTerminationId").val();
                                        this.r2T4ResultStatus.status = true;
                                        r2T4Termination.updateR2T4ResultStatus(this.r2T4ResultStatus,
                                            () => {
                                                if (constants.r2T4CompletionStatus.isR2T4ResultsCompleted ||
                                                    constants.r2T4CompletionStatus
                                                    .isR2T4OverrideResultsCompleted) {
                                                    $("#liR2T4Result").removeClass("is-active is-invalid")
                                                        .addClass("is-visited");
                                                } else {
                                                    $("#liR2T4Result")
                                                        .removeClass("is-active is-invalid is-visited");
                                                }
                                            });
                                    } else {
                                        r2T4Termination.getR2T4CompletedStatus($("#hdnTerminationId").val(),
                                            (completionStatus: any) => {
                                                if (!!completionStatus) {
                                                    constants.r2T4CompletionStatus = completionStatus;
                                                    if (constants.r2T4CompletionStatus.isR2T4ResultsCompleted || constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted) {
                                                        $("#liR2T4Result").removeClass("is-active is-invalid").addClass("is-visited");
                                                    } else {
                                                        $("#liR2T4Result").removeClass("is-active is-invalid is-visited");
                                                    }
                                                }
                                            });
                                    }
                                }
                            }
                        } else {
                            r2T4Termination.getR2T4CompletedStatus($("#hdnTerminationId").val(),
                                (completionStatus: any) => {
                                    if (!!completionStatus) {
                                        constants.r2T4CompletionStatus = completionStatus;
                                        if (constants.r2T4CompletionStatus.isR2T4ResultsCompleted || constants.r2T4CompletionStatus.isR2T4OverrideResultsCompleted) {
                                            $("#liR2T4Result").removeClass("is-active is-invalid").addClass("is-visited");
                                        } else {
                                            $("#liR2T4Result").removeClass("is-active is-invalid is-visited");
                                        }
                                        if (isOnResultsNextClick)
                                            constants.r2T4CompletionStatus.isR2T4ResultsCompleted = true;
                                        r2T4ResultDetail.isR2T4ResultsCompleted = constants.r2T4CompletionStatus.isR2T4ResultsCompleted;
                                        r2T4Termination.saveR2T4Results(r2T4ResultDetail,
                                            (response: any) => {
                                                $("#hdnR2T4ResultId").val(response["r2T4ResultsId"]);
                                                constants.isR2T4ResultsExists = true;
                                                if (response["resultStatus"] === AD.X_MESSAGE_SAVE_STUDENT_TERMINATION_SUCCESS) {
                                                    if (showMsg) {
                                                        $("#liR2T4Result").addClass("is-active");
                                                        MasterPage.SHOW_INFO_WINDOW(response.resultStatus);
                                                    }
                                                } else {
                                                    MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL
                                                        .replace("$CurrentTabName", constants.activeTabName)
                                                        .replace("$CurrentTabName", constants.activeTabName));
                                                    return;
                                                }
                                                callbackOnResponse();
                                            });
                                    }
                                });
                            
                        }
                    });
            }
            catch (e) {
                MasterPage.SHOW_ERROR_WINDOW(AD.X_MESAGE_SAVE_INPUT_NEXT_FAIL.replace("$CurrentTabName", constants.activeTabName).replace("$CurrentTabName", constants.activeTabName));
            }
        }

        /*
        *  includeDuplicateOverriddenFields method includes the colon seperated values of duplicated result fields to R2T4 Result Detail for saving overridden results.
        */
        public includeDuplicateOverriddenFields(r2T4ResultDetail: any) {
            let duplicateFields = Utility.getDuplicateFields();
            let overriddenValue: string = "";
            $.each(duplicateFields, function (index, field) {
                if ($("#" + field).val() !== null) {
                    overriddenValue = overriddenValue + field + Utility.equalsString + $("#" + field).val() + Utility.colonSeparator;
                }
            });
            r2T4ResultDetail.overriddenData = overriddenValue;
        }

        /*
        *  includePWDOverriddenFields method includes the colon seperated values of PWD results fields to R2T4 Result Detail for saving overridden results.
       */
        public includePwdOverriddenFields(r2T4ResultDetail: any) {
            let pwdFields = Utility.getPostWithdrawalFields();
            let overriddenPwdValue: string = "";
            $.each(pwdFields, function (index, field) {
                if ($("#" + field).val() !== null) {
                    let fieldType = field.substring(0, 3);
                    if (fieldType !== "chk" && fieldType !== "dt") {
                        overriddenPwdValue = overriddenPwdValue +
                            field +
                            Utility.equalsString +
                            $("#" + field).val() +
                            Utility.colonSeparator;
                    } else {
                        if (fieldType === "chk") {
                            if ($("#" + field).is(":checked"))
                                overriddenPwdValue = overriddenPwdValue +
                                    field +
                                    Utility.equalsString +
                                    "true" +
                                    Utility.colonSeparator;
                            else
                                overriddenPwdValue = overriddenPwdValue +
                                    field +
                                    Utility.equalsString +
                                    "false" +
                                    Utility.colonSeparator;
                        }
                        else if (fieldType === "dt") {
                            var dateValue = $("#" + field).val() === "" ? null : $("#" + field).val();
                            overriddenPwdValue = overriddenPwdValue +
                                field +
                                Utility.equalsString +
                                dateValue +
                                Utility.colonSeparator;
                        }
                    }
                }
            });
            r2T4ResultDetail.postWithdrawalData = overriddenPwdValue;
        }

        /*
        *  setPwdOverriddenFields method sets the values to the controls in Post-withdrawal data for the overridden field Results.
        */
        public setPwdOverriddenFields(postWithdrawalData: any) {
            var data = postWithdrawalData.split(":");
            let pwdFields = Utility.getPostWithdrawalFields();
            $.each(data, function (index,value) {
                let dataPair = value.split("=");
                let fieldType = dataPair[0].substring(0, 3);
                if (fieldType !== "chk" && fieldType !== "dt") {
                    $("#" + dataPair[0]).val(dataPair[1]);
                } else {
                    if (fieldType === "chk") {
                        if (dataPair[1] === "true" && dataPair[1] !== "" && dataPair[1] !== null && dataPair[1] !== undefined)
                            $("#" + dataPair[0]).prop('checked', true);
                    }
                    else if (fieldType === "dt") {
                        let dateVlaue = Utility.formatDate(dataPair[1]);
                        $("#" + dataPair[0]).val(MasterPage.formatDate(dateVlaue));
                    }
                }
            });
        }

        /*
         *  getSavedR2T4Details method fetches the saved R2T4 Results from database for the termination id.
         */
        public getSavedR2T4Details(r2T4InputDetail: any) {
            let terminationId = $("#hdnTerminationId").val();
            Utility.isClockHour($("#hdnEnrollmentId").val(),
                (response) => {
                    if (response) {
                        $("#rdbClock").prop("checked", true);
                        $("#rdbCredit").prop("checked", false);
                        $("#rdbCredit").prop("disabled", true);
                    }
                    else {
                        $("#rdbCredit").prop("checked", true);
                        $("#rdbClock").prop("checked", false);
                        $("#rdbClock").prop("disabled", true);
                    }
                });

            r2T4Termination.getSavedR2T4Results(terminationId,
                (response: any) => {
                    if (response.isInputIncluded) {
                        this.setOverriddenResults(response, r2T4InputDetail);
                        } else {
                        this.setSavedR2T4Results(response, r2T4InputDetail);
                    }
                });
        }

        /*
        *  isResultsExists method checks whether the R2T4 results are avilable in the database for the given termination Id
        */
        public isResultsExists(onResponseCallback: (response: Response) => any) {
            let terminationId = $("#hdnTerminationId").val();
            r2T4Termination.isResultsExists(terminationId,
                (response: any) => {
                    return onResponseCallback(response);
                });
        }

        /*
       *  isOverrideResultsExists method checks whether the R2T4 override results are avilable in the database for the given termination Id
       */
        public isOverrideResultsExists(onResponseCallback: (response: Response) => any) {
            let terminationId = $("#hdnTerminationId").val();
            let resultType = AD.R2T4ResultType.R2T4Override;
            this.generateResultMap(this.resultMap, terminationId, resultType);
            r2T4Termination.isTerminationIdExists(this.resultMap,
                (response: any) => {
                    onResponseCallback(response);
                }
            );
        }

        /*
          *  setSavedR2T4Results method sets the values to the controls in R2t4Result tab for the saved Results.
          */
        public setOverriddenResults(response: any, r2T4InputDetail: any): any {
            this.clearR2T4Results();
            if (response !== undefined) {
                //---1. Student's Title IV Aid Information----//
                if (response.pellGrantDisbursed !== undefined &&
                    response.pellGrantDisbursed !==
                    null &&
                    response.pellGrantDisbursed !== "" &&
                    response.pellGrantDisbursed !== NaN)
                    $("#txtPellA").val(Utility.formatNumberAsCurrency(parseFloat(response.pellGrantDisbursed)));
                if (response.pellGrantCouldDisbursed !== undefined &&
                    response.pellGrantCouldDisbursed !==
                    null &&
                    response.pellGrantCouldDisbursed !== "" &&
                    response.pellGrantCouldDisbursed !== NaN)
                    $("#txtPellB").val(Utility.formatNumberAsCurrency(parseFloat(response.pellGrantCouldDisbursed)));
                if (response.fseogDisbursed !== undefined &&
                    response.fseogDisbursed !==
                    null &&
                    response.fseogDisbursed !== "" &&
                    response.fseogDisbursed !== NaN)
                    $("#txtFSEOGA").val(Utility.formatNumberAsCurrency(parseFloat(response.fseogDisbursed)));
                if (response.fseogCouldDisbursed !== undefined &&
                    response.fseogCouldDisbursed !==
                    null &&
                    response.fseogCouldDisbursed !== "" &&
                    response.fseogCouldDisbursed !== NaN)
                    $("#txtFSEOGB").val(Utility.formatNumberAsCurrency(parseFloat(response.fseogCouldDisbursed)));
                if (response.teachGrantDisbursed !== undefined &&
                    response.teachGrantDisbursed !==
                    null &&
                    response.teachGrantDisbursed !== "" &&
                    response.teachGrantDisbursed !== NaN)
                    $("#txtTeachA").val(Utility.formatNumberAsCurrency(parseFloat(response.teachGrantDisbursed)));
                if (response.teachGrantCouldDisbursed !== undefined &&
                    response.teachGrantCouldDisbursed !==
                    null &&
                    response.teachGrantCouldDisbursed !== "" &&
                    response.teachGrantCouldDisbursed !== NaN)
                    $("#txtTeachB").val(Utility.formatNumberAsCurrency(parseFloat(response.teachGrantCouldDisbursed)));
                if (response.iraqAfgGrantDisbursed !== undefined &&
                    response.iraqAfgGrantDisbursed !==
                    null &&
                    response.iraqAfgGrantDisbursed !== "" &&
                    response.iraqAfgGrantDisbursed !== NaN)
                    $("#txtIraqAfganA").val(Utility.formatNumberAsCurrency(parseFloat(response.iraqAfgGrantDisbursed)));
                if (response.iraqAfgGrantCouldDisbursed !== undefined &&
                    response.iraqAfgGrantCouldDisbursed !==
                    null &&
                    response.iraqAfgGrantCouldDisbursed !== "" &&
                    response.iraqAfgGrantCouldDisbursed !== NaN)
                    $("#txtIraqAfganB").val(Utility.formatNumberAsCurrency(parseFloat(response.iraqAfgGrantCouldDisbursed)));
                if (response.subTotalAmountDisbursedA !== undefined &&
                    response.subTotalAmountDisbursedA !==
                    null &&
                    response.subTotalAmountDisbursedA !== "" &&
                    response.subTotalAmountDisbursedA !== NaN)
                    $("#txtSubTotalA").val(Utility.formatNumberAsCurrency(parseFloat(response.subTotalAmountDisbursedA)));
                if (response.subTotalAmountCouldDisbursedC !== undefined &&
                    response.subTotalAmountCouldDisbursedC !==
                    null &&
                    response.subTotalAmountCouldDisbursedC !== "" &&
                    response.subTotalAmountCouldDisbursedC !== NaN)
                    $("#txtSubTotalC").val(Utility.formatNumberAsCurrency(parseFloat(response.subTotalAmountCouldDisbursedC)));

                if (response.boxEResult !== undefined &&
                    response.boxEResult !==
                    null &&
                    response.boxEResult !== "" &&
                    response.boxEResult !== NaN)
                    $("#txtResultE").val(Utility.formatNumberAsCurrency(parseFloat(response.boxEResult)));
                if (response.boxFResult !== undefined &&
                    response.boxFResult !==
                    null &&
                    response.boxFResult !== "" &&
                    response.boxFResult !== NaN)
                    $("#txtstep1F").val(Utility.formatNumberAsCurrency(parseFloat(response.boxFResult)));

                if (response.unsubLoanNetAmountDisbursed !== undefined &&
                    response.unsubLoanNetAmountDisbursed !==
                    null &&
                    response.unsubLoanNetAmountDisbursed !== "" &&
                    response.unsubLoanNetAmountDisbursed !== NaN)
                    $("#txtUnSubFFEL").val(Utility.formatNumberAsCurrency(parseFloat(response.unsubLoanNetAmountDisbursed)));
                if (response.unsubLoanNetAmountCouldDisbursed !== undefined &&
                    response.unsubLoanNetAmountCouldDisbursed !==
                    null &&
                    response.unsubLoanNetAmountCouldDisbursed !== "" &&
                    response.unsubLoanNetAmountCouldDisbursed !== NaN)
                    $("#txtUnSubFFElM").val(Utility.formatNumberAsCurrency(parseFloat(response.unsubLoanNetAmountCouldDisbursed)));
                if (response.subLoanNetAmountDisbursed !== undefined &&
                    response.subLoanNetAmountDisbursed !==
                    null &&
                    response.subLoanNetAmountDisbursed !== "" &&
                    response.subLoanNetAmountDisbursed !== NaN)
                    $("#txtSubFFEL").val(Utility.formatNumberAsCurrency(parseFloat(response.subLoanNetAmountDisbursed)));
                if (response.subLoanNetAmountCouldDisbursed !== undefined &&
                    response.subLoanNetAmountCouldDisbursed !==
                    null &&
                    response.subLoanNetAmountCouldDisbursed !== "" &&
                    response.subLoanNetAmountCouldDisbursed !== NaN)
                    $("#txtSubFFElM").val(Utility.formatNumberAsCurrency(parseFloat(response.subLoanNetAmountCouldDisbursed)));
                if (response.perkinsLoanDisbursed !== undefined &&
                    response.perkinsLoanDisbursed !==
                    null &&
                    response.perkinsLoanDisbursed !== "" &&
                    response.perkinsLoanDisbursed !== NaN)
                    $("#txtPerkins").val(Utility.formatNumberAsCurrency(parseFloat(response.perkinsLoanDisbursed)));
                if (response.perkinsLoanCouldDisbursed !== undefined &&
                    response.perkinsLoanCouldDisbursed !==
                    null &&
                    response.perkinsLoanCouldDisbursed !== "" &&
                    response.perkinsLoanCouldDisbursed !== NaN)
                    $("#txtPerkinsM").val(Utility.formatNumberAsCurrency(parseFloat(response.perkinsLoanCouldDisbursed)));
                if (response.directGraduatePlusLoanDisbursed !== undefined &&
                    response.directGraduatePlusLoanDisbursed !==
                    null &&
                    response.directGraduatePlusLoanDisbursed !== "" &&
                    response.directGraduatePlusLoanDisbursed !== NaN)
                    $("#txtFFElStudent").val(Utility.formatNumberAsCurrency(parseFloat(response.directGraduatePlusLoanDisbursed)));
                if (response.directGraduatePlusLoanCouldDisbursed !== undefined &&
                    response.directGraduatePlusLoanCouldDisbursed !==
                    null &&
                    response.directGraduatePlusLoanCouldDisbursed !== "" &&
                    response.directGraduatePlusLoanCouldDisbursed !== NaN)
                    $("#txtFFELStudentM").val(Utility.formatNumberAsCurrency(parseFloat(response.directGraduatePlusLoanCouldDisbursed)));
                if (response.directParentPlusLoanDisbursed !== undefined &&
                    response.directParentPlusLoanDisbursed !==
                    null &&
                    response.directParentPlusLoanDisbursed !== "" &&
                    response.directParentPlusLoanDisbursed !== NaN)
                    $("#txtFFELParent").val(Utility.formatNumberAsCurrency(parseFloat(response.directParentPlusLoanDisbursed)));
                if (response.directParentPlusLoanCouldDisbursed !== undefined &&
                    response.directParentPlusLoanCouldDisbursed !==
                    null &&
                    response.directParentPlusLoanCouldDisbursed !== "" &&
                    response.directParentPlusLoanCouldDisbursed !== NaN)
                    $("#txtFFELParentM").val(Utility.formatNumberAsCurrency(parseFloat(response.directParentPlusLoanCouldDisbursed)));

                if (response.subTotalNetAmountDisbursedB !== undefined &&
                    response.subTotalNetAmountDisbursedB !==
                    null &&
                    response.subTotalNetAmountDisbursedB !== "" &&
                    response.subTotalNetAmountDisbursedB !== NaN)
                    $("#txtSubTotalB").val(Utility.formatNumberAsCurrency(parseFloat(response.subTotalNetAmountDisbursedB)));

                if (response.subTotalNetAmountDisbursedD !== undefined &&
                    response.subTotalNetAmountDisbursedD !==
                    null &&
                    response.subTotalNetAmountDisbursedD !== "" &&
                    response.subTotalNetAmountDisbursedD !== NaN)
                    $("#txtSubtotalD").val(Utility.formatNumberAsCurrency(parseFloat(response.subTotalNetAmountDisbursedD)));

                if (response.boxGResult !== undefined &&
                    response.boxGResult !==
                    null &&
                    response.boxGResult !== "" &&
                    response.boxGResult !== NaN)
                    $("#txtStep1G").val(Utility.formatNumberAsCurrency(parseFloat(response.boxGResult)));

                //---2.Percentage of Title IV Aid Earned-----//overriden results
                $("#dvStartDateResult").hide();
                $("#dvEndDateResult").hide();
                $("#divDate").hide();
                Utility.isClockHour($("#hdnEnrollmentId").val(),
                    (result) => {
                        if (result) {
                            $("#divClockDesc").show();
                            $("#divCreditDesc").hide();
                            $("#tblNoAttend").hide();
                            $("#divStep2StartDate").hide();
                            $("#divStep2EndDate").hide();
                            $("#divCalender").show();
                            if (response.withdrawalDate !== undefined &&
                                response.withdrawalDate !== "") {
                                let dtWithDrawlDate = Utility.formatDate(response.withdrawalDate);
                                if (dtWithDrawlDate !== null) {
                                    dtWithDrawlDate = MasterPage.formatDate(dtWithDrawlDate);
                                }
                                Utility.isSupportUser(
                                    (user) => {
                                        if (user) {
                                            $("#cal2CreditWithdrawalDateEnabled")
                                                .val(dtWithDrawlDate);
                                        } else {
                                            $("#txt2CreditWithdrawalDate").val(dtWithDrawlDate);
                                            $("#divDate").show();
                                            $("#divCalender").hide();
                                        }
                                    });
                                
                            }
                            $("#txtCompletedDays").val(response.completedTime);
                            $("#txtTotalDays").val(response.totalTime);
                            if (response.percentageOfActualAttendance !== undefined &&
                                response.percentageOfActualAttendance !== null &&
                                response.percentageOfActualAttendance !== "" &&
                                response.percentageOfActualAttendance !== NaN)
                                $("#txtCreditActual")
                                    .val(Utility.formatNumberAsCurrencyPercenatgeTab(
                                        parseFloat(response.percentageOfActualAttendance)));

                            (document.getElementById("resultTabCompletedDays") as HTMLInputElement).innerText =
                                "Hours scheduled to complete";
                            (document.getElementById("resultTabTotalDays") as HTMLInputElement).innerText =
                                "Total hours in period";
                            document.getElementById('resultTabTotalDays').style.fontWeight = 'bold';
                            document.getElementById('resultTabCompletedDays').style.fontWeight = 'bold';
                        } else {
                            $("#dvcalStartDateResult").show();
                            $("#dvCalEndDateResult").show();
                            $("#divCalender").show();
                            $("#tblNoAttend").show();
                            $("#divClockDesc").hide();
                            $("#divCreditDesc").show();
                            $("#divStep2StartDate").show();
                            $("#divStep2EndDate").show();
                            $("#divDate").hide();

                            if (response.startDate !== undefined &&
                                response.startDate !== "") {
                                let dtStartDate = Utility.formatDate(response.startDate);
                                if (dtStartDate !== null)
                                    dtStartDate = MasterPage.formatDate(dtStartDate);
                                Utility.isSupportUser(
                                    (user) => {
                                        if (user) {
                                            $("#calStartDate").val(dtStartDate);
                                        } else {
                                            $("#txtStartDate").val(dtStartDate);
                                            $("#dvStartDateResult").show();
                                            $("#dvcalStartDateResult").hide();
                                        }
                                    });
                            }
                            if (response.scheduledEndDate !== undefined &&
                                response.scheduledEndDate !== "") {
                                let dtEndDate = Utility.formatDate(response.scheduledEndDate);
                                if (dtEndDate !== null)
                                    dtEndDate = MasterPage.formatDate(dtEndDate);
                                Utility.isSupportUser(
                                    (user) => {
                                        if (user) {
                                            $("#calEndDate").val(dtEndDate);
                                        } else {
                                            $("#txtEndDate").val(dtEndDate);
                                            $("#dvEndDateResult").show();
                                            $("#dvCalEndDateResult").hide();
                                        }
                                    });
                            }
                            if (response.withdrawalDate !== undefined &&
                                response.withdrawalDate !== "") {
                                let dtWithDrawlDate = Utility.formatDate(response.withdrawalDate);
                                if (dtWithDrawlDate !== null) {
                                    dtWithDrawlDate = MasterPage.formatDate(dtWithDrawlDate);
                                }
                                Utility.isSupportUser(
                                    (user) => {
                                        if (user) {
                                            $("#cal2CreditWithdrawalDateEnabled").val(dtWithDrawlDate);
                                            $("#divDate").hide();
                                            $("#txt2CreditWithdrawalDate").hide();
                                        } else {
                                            $("#txt2CreditWithdrawalDate").val(dtWithDrawlDate);
                                            $("#divDate").show();
                                            $("#divCalender").hide();
                                        }
                                    });
                            }
                            if (response.isAttendanceNotRequired === true) {
                                $("#chkNAttend").prop('checked', true);
                            } else {
                                $("#chkNAttend").prop('checked', false);
                                $("#txtCompletedDays").val(response.completedTime);
                                $("#txtTotalDays").val(response.totalTime);
                                if (response.percentageOfActualAttendance !== undefined &&
                                    response.percentageOfActualAttendance !== null &&
                                    response.percentageOfActualAttendance !== "" &&
                                    response.percentageOfActualAttendance !== NaN)
                                    $("#txtCreditActual")
                                        .val(Utility.formatNumberAsCurrencyPercenatgeTab(
                                            parseFloat(response.percentageOfActualAttendance)));
                            }

                            (document.getElementById("resultTabCompletedDays") as HTMLInputElement).innerText =
                                "Completed days";
                            (document.getElementById("resultTabTotalDays") as HTMLInputElement).innerText =
                                "Total days";
                            document.getElementById('resultTabTotalDays').style.fontWeight = 'bold';
                            document.getElementById('resultTabCompletedDays').style.fontWeight = 'bold';
                        }
                    });
                if (response.boxHResult !== undefined && response.boxHResult !==
                    null && response.boxHResult !== "" && response.boxHResult !== NaN)
                    $("#txtCreditAttendencePerc").val(Utility.formatNumberAsPercenatgeResults(parseFloat(response.boxHResult)));
                //---3. Amount of Title IV Aid Earned by the Student----//
                if (response.boxHResult !== undefined && response.boxHResult !==
                    null && response.boxHResult !== "" && response.boxHResult !== NaN)
                    $("#txtBoxH").val(Utility.formatNumberAsPercenatgeResults(parseFloat(response.boxHResult)));

                if (response.boxGResult !== undefined && response.boxGResult !==
                    null && response.boxGResult !== "" && response.boxGResult !== NaN)
                    $("#txtBoxG").val(Utility.formatNumberAsCurrency(parseFloat(response.boxGResult)));

                if (response.boxIResult !== undefined && response.boxIResult !==
                    null && response.boxIResult !== "" && response.boxIResult !== NaN)
                    $("#txtBoxI").val(Utility.formatNumberAsCurrency(parseFloat(response.boxIResult)));

                //---4. Total Title IV Aid to be Disbursed or Returned----//
                if (response.boxJResult !== undefined &&
                    response.boxJResult !==
                    null &&
                    response.boxJResult !== "" &&
                    response.boxJResult !== NaN)
                    $("#txtResultJ, #txtPWD, #txtPWDOffered")
                        .val(Utility.formatNumberAsCurrency(parseFloat(response.boxJResult)));

                if (response.boxKResult !== undefined &&
                    response.boxKResult !==
                    null &&
                    response.boxKResult !== "" &&
                    response.boxKResult !== NaN)
                    $("#txtResultK")
                        .val(Utility.formatNumberAsCurrency(parseFloat(response.boxKResult)));

                //----5. Amount of Unearned Title IV Aid Due from the School-----//
                if (response.tuitionFee !== undefined &&
                    response.tuitionFee !==
                    null &&
                    response.tuitionFee !== "" &&
                    response.tuitionFee !== NaN)
                    $("#txt5Tuition").val(Utility.formatNumberAsCurrency(parseFloat(response.tuitionFee)));
                if (response.roomFee !== undefined &&
                    response.roomFee !==
                    null &&
                    response.roomFee !== "" &&
                    response.roomFee !== NaN)
                    $("#txt5Room").val(Utility.formatNumberAsCurrency(parseFloat(response.roomFee)));
                if (response.boardFee !== undefined &&
                    response.boardFee !==
                    null &&
                    response.boardFee !== "" &&
                    response.boardFee !== NaN)
                    $("#txt5Board").val(Utility.formatNumberAsCurrency(parseFloat(response.boardFee)));
                if (response.otherFee !== undefined &&
                    response.otherFee !==
                    null &&
                    response.otherFee !== "" &&
                    response.otherFee !== NaN)
                    $("#txt5Other").val(Utility.formatNumberAsCurrency(parseFloat(response.otherFee)));

                if (response.boxLResult !== undefined &&
                    response.boxLResult !==
                    null &&
                    response.boxLResult !== "" &&
                    response.boxLResult !== NaN)
                    $("#txt5L").val(Utility.formatNumberAsCurrency(parseFloat(response.boxLResult)));

                if (response.boxMResult !== undefined &&
                    response.boxMResult !==
                    null &&
                    response.boxMResult !== "" &&
                    response.boxMResult !== NaN)
                    $("#txt5BoxMResult")
                        .val(Utility.formatNumberAsPercenatgeResults(parseFloat(response.boxMResult)));

                if (response.boxNResult !== undefined &&
                    response.boxNResult !==
                    null &&
                    response.boxNResult !== "" &&
                    response.boxNResult !== NaN)
                    $("#txt5BoxNResult")
                        .val(Utility.formatNumberAsCurrency(parseFloat(response.boxNResult)));

                if (response.boxOResult !== undefined &&
                    response.boxOResult !==
                    null &&
                    response.boxOResult !== "" &&
                    response.boxOResult !== NaN)
                    $("#txt5BoxOresult")
                        .val(Utility.formatNumberAsCurrency(parseFloat(response.boxOResult)));

                //----6. Return of Funds by the School---//
                if (response.unsubDirectLoanSchoolReturn !== undefined &&
                    response.unsubDirectLoanSchoolReturn !== "" &&
                    response.unsubDirectLoanSchoolReturn !== NaN &&
                    response.unsubDirectLoanSchoolReturn !==
                    null) {
                    $("#txtUnSubLoan6")
                        .val(Utility.formatNumberAsCurrency(parseFloat(response.unsubDirectLoanSchoolReturn)));
                }
                if (response.subDirectLoanSchoolReturn !== undefined &&
                    response.subDirectLoanSchoolReturn !== "" &&
                    response.subDirectLoanSchoolReturn !== NaN &&
                    response.subDirectLoanSchoolReturn !==
                    null) {
                    $("#txtSubLoan6")
                        .val(Utility.formatNumberAsCurrency(parseFloat(response.subDirectLoanSchoolReturn)));
                }

                if (response.perkinsLoanSchoolReturn !== undefined &&
                    response.perkinsLoanSchoolReturn !== "" &&
                    response.perkinsLoanSchoolReturn !== NaN &&
                    response.perkinsLoanSchoolReturn !==
                    null) {
                    $("#txtPerkinsLoan6")
                        .val(Utility.formatNumberAsCurrency(parseFloat(response.perkinsLoanSchoolReturn)));
                }
                if (response.directGraduatePlusLoanSchoolReturn !== undefined &&
                    response.directGraduatePlusLoanSchoolReturn !== "" &&
                    response.directGraduatePlusLoanSchoolReturn !== NaN &&
                    response.directGraduatePlusLoanSchoolReturn !==
                    null) {
                    $("#txtFFELStudent6")
                        .val(Utility.formatNumberAsCurrency(
                            parseFloat(response.directGraduatePlusLoanSchoolReturn)));
                }
                if (response.directParentPlusLoanSchoolReturn !== undefined &&
                    response.directParentPlusLoanSchoolReturn !== "" &&
                    response.directParentPlusLoanSchoolReturn !== NaN &&
                    response.directParentPlusLoanSchoolReturn !==
                    null) {
                    $("#txtFFELParent6")
                        .val(Utility.formatNumberAsCurrency(
                            parseFloat(response.directParentPlusLoanSchoolReturn)));
                }

                if (response.pellGrantSchoolReturn !== undefined &&
                    response.pellGrantSchoolReturn !== "" &&
                    response.pellGrantSchoolReturn !== NaN &&
                    response.pellGrantSchoolReturn !==
                    null) {
                    $("#txtPellGrant6")
                        .val(Utility.formatNumberAsCurrency(parseFloat(response.pellGrantSchoolReturn)));
                }
                if (response.fseogSchoolReturn !== undefined &&
                    response.fseogSchoolReturn !== "" &&
                    response.fseogSchoolReturn !== NaN &&
                    response.fseogSchoolReturn !==
                    null) {
                    $("#txtFSEOGGrant6")
                        .val(Utility.formatNumberAsCurrency(parseFloat(response.fseogSchoolReturn)));
                }
                if (response.teachGrantSchoolReturn !== undefined &&
                    response.teachGrantSchoolReturn !== "" &&
                    response.teachGrantSchoolReturn !== NaN &&
                    response.teachGrantSchoolReturn !==
                    null) {
                    $("#txtTeachGrant6")
                        .val(Utility.formatNumberAsCurrency(parseFloat(response.teachGrantSchoolReturn)));
                }
                if (response.iraqAfgGrantSchoolReturn !== undefined &&
                    response.iraqAfgGrantSchoolReturn !== "" &&
                    response.iraqAfgGrantSchoolReturn !== NaN &&
                    response.iraqAfgGrantSchoolReturn !==
                    null) {
                    $("#txtIASGrant6")
                        .val(Utility.formatNumberAsCurrency(parseFloat(response.iraqAfgGrantSchoolReturn)));
                }

                if (response.boxPResult !== undefined &&
                    response.boxPResult !==
                    null &&
                    response.boxPResult !== "" &&
                    response.boxPResult !== NaN)
                    $("#txtStep6P").val(Utility.formatNumberAsCurrency(parseFloat(response.boxPResult)));

                //----- 7. Initial Amount of Unearned Title IV Aid Due from the Student----//
                if (response.boxQResult !== undefined &&
                    response.boxQResult !==
                    null &&
                    response.boxQResult !== "" &&
                    response.boxQResult !== NaN)
                    $("#txtStep7Result").val(Utility.formatNumberAsCurrency(parseFloat(response.boxQResult)));

                //----- 8. Repayment of the Student's loans----//
                if (response.boxRResult !== undefined &&
                    response.boxRResult !==
                    null &&
                    response.boxRResult !== "" &&
                    response.boxRResult !== NaN)
                    $("#txt8BoxR").val(Utility.formatNumberAsCurrency(parseFloat(response.boxRResult)));

                //----- 9. Grant Funds to be Returned----//

                if (response.boxSResult !== undefined &&
                    response.boxSResult !==
                    null &&
                    response.boxSResult !== "" &&
                    response.boxSResult !== NaN)
                    $("#txt9BoxSResult")
                        .val(Utility.formatNumberAsCurrency(parseFloat(response.boxSResult)));

                if (response.boxTResult !== undefined &&
                    response.boxTResult !==
                    null &&
                    response.boxTResult !== "" &&
                    response.boxTResult !== NaN)
                    $("#txt9BoxTResult")
                        .val(Utility.formatNumberAsCurrency(parseFloat(response.boxTResult)));

                if (response.boxUResult !== undefined &&
                    response.boxUResult !==
                    null &&
                    response.boxUResult !== "" &&
                    response.boxUResult !== NaN)
                    $("#txtBox9Result")
                        .val(Utility.formatNumberAsCurrency(parseFloat(response.boxUResult)));

                //----- 10. Return of Grant Funds by the Student----//
                if (response.pellGrantAmountToReturn !== undefined &&
                    response.pellGrantAmountToReturn !==
                    null &&
                    response.pellGrantAmountToReturn !== "" &&
                    response.pellGrantAmountToReturn !== NaN &&
                    response.pellGrantAmountToReturn > 0) {
                    $("#txt10Pell").val(Utility.formatNumberAsCurrency(parseFloat(response.pellGrantAmountToReturn)));
                }
                if (response.fseogAmountToReturn !== undefined &&
                    response.fseogAmountToReturn !==
                    null &&
                    response.fseogAmountToReturn !== "" &&
                    response.fseogAmountToReturn !== NaN &&
                    response.fseogAmountToReturn > 0) {
                    $("#txt10FSEOG").val(Utility.formatNumberAsCurrency(parseFloat(response.fseogAmountToReturn)));
                }
                if (response.teachGrantAmountToReturn !== undefined &&
                    response.teachGrantAmountToReturn !==
                    null &&
                    response.teachGrantAmountToReturn !== "" &&
                    response.teachGrantAmountToReturn !== NaN &&
                    response.teachGrantAmountToReturn > 0) {
                    $("#txt10TG").val(Utility.formatNumberAsCurrency(parseFloat(response.teachGrantAmountToReturn)));
                }
                if (response.iraqAfgGrantAmountToReturn !== undefined &&
                    response.iraqAfgGrantAmountToReturn !==
                    null &&
                    response.iraqAfgGrantAmountToReturn !== "" &&
                    response.iraqAfgGrantAmountToReturn !== NaN &&
                    response.iraqAfgGrantAmountToReturn > 0) {
                    $("#txt10IFSG").val(Utility.formatNumberAsCurrency(parseFloat(response.iraqAfgGrantAmountToReturn)));
                }

                if (response.overriddenData !== undefined &&
                    response.overriddenData !==
                    null &&
                    response.overriddenData !== "" &&
                    response.overriddenData !== NaN) {
                    this.setDuplicatedOverriddenFields(response.overriddenData);
                }
                if (response.postWithdrawalData !== undefined &&
                    response.postWithdrawalData !==
                    null &&
                    response.postWithdrawalData !== "" &&
                    response.postWithdrawalData !== NaN) {
                    this.setPwdOverriddenFields(response.postWithdrawalData);
                }
                Utility.isSupportUser(
                    (user) => {
                        if (user) {
                            if (response.ticketNumber !== null && response.ticketNumber !== undefined && response.ticketNumber !== "" && response.ticketNumber !== NaN) {
                                $("#ChkOverride").prop("checked", true);
                                $("#txtTicket").val(response.ticketNumber);
                                $("#divR2T4Result :input").prop("readonly", false);
                                this.enablePwdControls();
                            }
                        }
                    });

                let dateFormCompleted = Utility.formatDate(response.updatedDate);
                $("#dateCompleted").text(dateFormCompleted);
            }
        }

        /*
        *  setDuplicatedOverriddenFields method sets the values to the controls in R2t4Result tab for the overridden duplicated field Results.
        */
        public setDuplicatedOverriddenFields(overriddenData: any) {
            var data = overriddenData.split(":");
            let duplicateFields = Utility.getDuplicateFields();
            $.each(data, function (index, value) {
                let dataPair = value.split("=");
                $("#" + dataPair[0]).val(dataPair[1]);
            });
        }

        /*
        *  setSavedR2T4Results method sets the values to the controls in R2t4Result tab for the saved Results.
        */
        public setSavedR2T4Results(response: any, r2T4InputDetail: any): any {
            this.clearR2T4Results();
            if (response !== undefined) {
                //---1. Student's Title IV Aid Information----//
                if (response.pellGrantDisbursed !== undefined &&
                    response.pellGrantDisbursed !==
                    null &&
                    response.pellGrantDisbursed !== "" &&
                    response.pellGrantDisbursed !== NaN)
                    $("#txtPellA").val(Utility.formatNumberAsCurrency(parseFloat(response.pellGrantDisbursed)));
                if (response.pellGrantCouldDisbursed !== undefined &&
                    response.pellGrantCouldDisbursed !==
                    null &&
                    response.pellGrantCouldDisbursed !== "" &&
                    response.pellGrantCouldDisbursed !== NaN)
                    $("#txtPellB").val(Utility.formatNumberAsCurrency(parseFloat(response.pellGrantCouldDisbursed)));
                if (response.fseogDisbursed !== undefined &&
                    response.fseogDisbursed !==
                    null &&
                    response.fseogDisbursed !== "" &&
                    response.fseogDisbursed !== NaN)
                    $("#txtFSEOGA").val(Utility.formatNumberAsCurrency(parseFloat(response.fseogDisbursed)));
                if (response.fseogCouldDisbursed !== undefined &&
                    response.fseogCouldDisbursed !==
                    null &&
                    response.fseogCouldDisbursed !== "" &&
                    response.fseogCouldDisbursed !== NaN)
                    $("#txtFSEOGB").val(Utility.formatNumberAsCurrency(parseFloat(response.fseogCouldDisbursed)));
                if (response.teachGrantDisbursed !== undefined &&
                    response.teachGrantDisbursed !==
                    null &&
                    response.teachGrantDisbursed !== "" &&
                    response.teachGrantDisbursed !== NaN)
                    $("#txtTeachA").val(Utility.formatNumberAsCurrency(parseFloat(response.teachGrantDisbursed)));
                if (response.teachGrantCouldDisbursed !== undefined &&
                    response.teachGrantCouldDisbursed !==
                    null &&
                    response.teachGrantCouldDisbursed !== "" &&
                    response.teachGrantCouldDisbursed !== NaN)
                    $("#txtTeachB").val(Utility.formatNumberAsCurrency(parseFloat(response.teachGrantCouldDisbursed)));
                if (response.iraqAfgGrantDisbursed !== undefined &&
                    response.iraqAfgGrantDisbursed !==
                    null &&
                    response.iraqAfgGrantDisbursed !== "" &&
                    response.iraqAfgGrantDisbursed !== NaN)
                    $("#txtIraqAfganA").val(Utility.formatNumberAsCurrency(parseFloat(response.iraqAfgGrantDisbursed)));
                if (response.iraqAfgGrantCouldDisbursed !== undefined &&
                    response.iraqAfgGrantCouldDisbursed !==
                    null &&
                    response.iraqAfgGrantCouldDisbursed !== "" &&
                    response.iraqAfgGrantCouldDisbursed !== NaN)
                    $("#txtIraqAfganB").val(Utility.formatNumberAsCurrency(parseFloat(response.iraqAfgGrantCouldDisbursed)));


                if (response.subTotalAmountDisbursedA !== undefined &&
                    response.subTotalAmountDisbursedA !==
                    null &&
                    response.subTotalAmountDisbursedA !== "" &&
                    response.subTotalAmountDisbursedA !== NaN)
                    $("#txtSubTotalA").val(Utility.formatNumberAsCurrency(parseFloat(response.subTotalAmountDisbursedA)));
                if (response.subTotalAmountCouldDisbursedC !== undefined &&
                    response.subTotalAmountCouldDisbursedC !==
                    null &&
                    response.subTotalAmountCouldDisbursedC !== "" &&
                    response.subTotalAmountCouldDisbursedC !== NaN)
                    $("#txtSubTotalC").val(Utility.formatNumberAsCurrency(parseFloat(response.subTotalAmountCouldDisbursedC)));
                if (response.subTotalAmountDisbursedA !== undefined &&
                    response.subTotalAmountDisbursedA !==
                    null &&
                    response.subTotalAmountDisbursedA !== "" &&
                    response.subTotalAmountDisbursedA !== NaN)
                    $("#txtEA").val(Utility.formatNumberAsCurrency(parseFloat(response.subTotalAmountDisbursedA)));
                if (response.subTotalNetAmountDisbursedB !== undefined &&
                    response.subTotalNetAmountDisbursedB !==
                    null &&
                    response.subTotalNetAmountDisbursedB !== "" &&
                    response.subTotalNetAmountDisbursedB !== NaN)
                    $("#txtEB").val(Utility.formatNumberAsCurrency(parseFloat(response.subTotalNetAmountDisbursedB)));
                if (response.boxEResult !== undefined &&
                    response.boxEResult !==
                    null &&
                    response.boxEResult !== "" &&
                    response.boxEResult !== NaN)
                    $("#txtResultE").val(Utility.formatNumberAsCurrency(parseFloat(response.boxEResult)));
                if (response.subTotalAmountDisbursedA !== undefined &&
                    response.subTotalAmountDisbursedA !==
                    null &&
                    response.subTotalAmountDisbursedA !== "" &&
                    response.subTotalAmountDisbursedA !== NaN)
                    $("#txtFA").val(Utility.formatNumberAsCurrency(parseFloat(response.subTotalAmountDisbursedA)));
                if (response.subTotalAmountCouldDisbursedC !== undefined &&
                    response.subTotalAmountCouldDisbursedC !==
                    null &&
                    response.subTotalAmountCouldDisbursedC !== "" &&
                    response.subTotalAmountCouldDisbursedC !== NaN)
                    $("#txtFC").val(Utility.formatNumberAsCurrency(parseFloat(response.subTotalAmountCouldDisbursedC)));
                if (response.boxFResult !== undefined &&
                    response.boxFResult !==
                    null &&
                    response.boxFResult !== "" &&
                    response.boxFResult !== NaN)
                    $("#txtstep1F").val(Utility.formatNumberAsCurrency(parseFloat(response.boxFResult)));

                if (response.unsubLoanNetAmountDisbursed !== undefined &&
                    response.unsubLoanNetAmountDisbursed !==
                    null &&
                    response.unsubLoanNetAmountDisbursed !== "" &&
                    response.unsubLoanNetAmountDisbursed !== NaN)
                    $("#txtUnSubFFEL").val(Utility.formatNumberAsCurrency(parseFloat(response.unsubLoanNetAmountDisbursed)));
                if (response.unsubLoanNetAmountCouldDisbursed !== undefined &&
                    response.unsubLoanNetAmountCouldDisbursed !==
                    null &&
                    response.unsubLoanNetAmountCouldDisbursed !== "" &&
                    response.unsubLoanNetAmountCouldDisbursed !== NaN)
                    $("#txtUnSubFFElM").val(Utility.formatNumberAsCurrency(parseFloat(response.unsubLoanNetAmountCouldDisbursed)));
                if (response.subLoanNetAmountDisbursed !== undefined &&
                    response.subLoanNetAmountDisbursed !==
                    null &&
                    response.subLoanNetAmountDisbursed !== "" &&
                    response.subLoanNetAmountDisbursed !== NaN)
                    $("#txtSubFFEL").val(Utility.formatNumberAsCurrency(parseFloat(response.subLoanNetAmountDisbursed)));
                if (response.subLoanNetAmountCouldDisbursed !== undefined &&
                    response.subLoanNetAmountCouldDisbursed !==
                    null &&
                    response.subLoanNetAmountCouldDisbursed !== "" &&
                    response.subLoanNetAmountCouldDisbursed !== NaN)
                    $("#txtSubFFElM").val(Utility.formatNumberAsCurrency(parseFloat(response.subLoanNetAmountCouldDisbursed)));
                if (response.perkinsLoanDisbursed !== undefined &&
                    response.perkinsLoanDisbursed !==
                    null &&
                    response.perkinsLoanDisbursed !== "" &&
                    response.perkinsLoanDisbursed !== NaN)
                    $("#txtPerkins").val(Utility.formatNumberAsCurrency(parseFloat(response.perkinsLoanDisbursed)));
                if (response.perkinsLoanCouldDisbursed !== undefined &&
                    response.perkinsLoanCouldDisbursed !==
                    null &&
                    response.perkinsLoanCouldDisbursed !== "" &&
                    response.perkinsLoanCouldDisbursed !== NaN)
                    $("#txtPerkinsM").val(Utility.formatNumberAsCurrency(parseFloat(response.perkinsLoanCouldDisbursed)));
                if (response.directGraduatePlusLoanDisbursed !== undefined &&
                    response.directGraduatePlusLoanDisbursed !==
                    null &&
                    response.directGraduatePlusLoanDisbursed !== "" &&
                    response.directGraduatePlusLoanDisbursed !== NaN)
                    $("#txtFFElStudent").val(Utility.formatNumberAsCurrency(parseFloat(response.directGraduatePlusLoanDisbursed)));
                if (response.directGraduatePlusLoanCouldDisbursed !== undefined &&
                    response.directGraduatePlusLoanCouldDisbursed !==
                    null &&
                    response.directGraduatePlusLoanCouldDisbursed !== "" &&
                    response.directGraduatePlusLoanCouldDisbursed !== NaN)
                    $("#txtFFELStudentM").val(Utility.formatNumberAsCurrency(parseFloat(response.directGraduatePlusLoanCouldDisbursed)));
                if (response.directParentPlusLoanDisbursed !== undefined &&
                    response.directParentPlusLoanDisbursed !==
                    null &&
                    response.directParentPlusLoanDisbursed !== "" &&
                    response.directParentPlusLoanDisbursed !== NaN)
                    $("#txtFFELParent").val(Utility.formatNumberAsCurrency(parseFloat(response.directParentPlusLoanDisbursed)));
                if (response.directParentPlusLoanCouldDisbursed !== undefined &&
                    response.directParentPlusLoanCouldDisbursed !==
                    null &&
                    response.directParentPlusLoanCouldDisbursed !== "" &&
                    response.directParentPlusLoanCouldDisbursed !== NaN)
                    $("#txtFFELParentM").val(Utility.formatNumberAsCurrency(parseFloat(response.directParentPlusLoanCouldDisbursed)));

                if (response.subTotalNetAmountDisbursedB !== undefined &&
                    response.subTotalNetAmountDisbursedB !==
                    null &&
                    response.subTotalNetAmountDisbursedB !== "" &&
                    response.subTotalNetAmountDisbursedB !== NaN)
                    $("#txtSubTotalB").val(Utility.formatNumberAsCurrency(parseFloat(response.subTotalNetAmountDisbursedB)));

                if (response.subTotalNetAmountDisbursedD !== undefined &&
                    response.subTotalNetAmountDisbursedD !==
                    null &&
                    response.subTotalNetAmountDisbursedD !== "" &&
                    response.subTotalNetAmountDisbursedD !== NaN)
                    $("#txtSubtotalD").val(Utility.formatNumberAsCurrency(parseFloat(response.subTotalNetAmountDisbursedD)));

                if (response.subTotalAmountDisbursedA !== undefined &&
                    response.subTotalAmountDisbursedA !==
                    null &&
                    response.subTotalAmountDisbursedA !== "" &&
                    response.subTotalAmountDisbursedA !== NaN)
                    $("#txtStep1FA").val(Utility.formatNumberAsCurrency(parseFloat(response.subTotalAmountDisbursedA)));

                if (response.subTotalNetAmountDisbursedB !== undefined &&
                    response.subTotalNetAmountDisbursedB !==
                    null &&
                    response.subTotalNetAmountDisbursedB !== "" &&
                    response.subTotalNetAmountDisbursedB !== NaN)
                    $("#txtStep1FB").val(Utility.formatNumberAsCurrency(parseFloat(response.subTotalNetAmountDisbursedB)));

                if (response.subTotalAmountCouldDisbursedC !== undefined &&
                    response.subTotalAmountCouldDisbursedC !==
                    null &&
                    response.subTotalAmountCouldDisbursedC !== "" &&
                    response.subTotalAmountCouldDisbursedC !== NaN)
                    $("#txtStep1FC").val(Utility.formatNumberAsCurrency(parseFloat(response.subTotalAmountCouldDisbursedC)));
                if (response.subTotalNetAmountDisbursedD !== undefined &&
                    response.subTotalNetAmountDisbursedD !==
                    null &&
                    response.subTotalNetAmountDisbursedD !== "" &&
                    response.subTotalNetAmountDisbursedD !== NaN)
                    $("#txtStep1FD").val(Utility.formatNumberAsCurrency(parseFloat(response.subTotalNetAmountDisbursedD)));

                if (response.boxGResult !== undefined &&
                    response.boxGResult !==
                    null &&
                    response.boxGResult !== "" &&
                    response.boxGResult !== NaN)
                    $("#txtStep1G").val(Utility.formatNumberAsCurrency(parseFloat(response.boxGResult)));
                //..binding Institutional charges.
                if (response.tuitionFee !== undefined &&
                    response.tuitionFee !==
                    null &&
                    response.tuitionFee !== "" &&
                    response.tuitionFee !== NaN)
                    $("#txt5Tuition").val(Utility.formatNumberAsCurrency(parseFloat(response.tuitionFee)));
                if (response.roomFee !== undefined &&
                    response.roomFee !==
                    null &&
                    response.roomFee !== "" &&
                    response.roomFee !== NaN)
                    $("#txt5Room").val(Utility.formatNumberAsCurrency(parseFloat(response.roomFee)));
                if (response.boardFee !== undefined &&
                    response.boardFee !==
                    null &&
                    response.boardFee !== "" &&
                    response.boardFee !== NaN)
                    $("#txt5Board").val(Utility.formatNumberAsCurrency(parseFloat(response.boardFee)));
                if (response.otherFee !== undefined &&
                    response.otherFee !==
                    null &&
                    response.otherFee !== "" &&
                    response.otherFee !== NaN)
                    $("#txt5Other").val(Utility.formatNumberAsCurrency(parseFloat(response.otherFee)));

                //---2.Percentage of Title IV Aid Earned-----// saved details
                $("#dvcalStartDateResult").hide();
                $("#dvCalEndDateResult").hide();
                $("#divCalender").hide();
                Utility.isClockHour($("#hdnEnrollmentId").val(),
                    (result) => {
                        if (result) {
                            $("#divClockDesc").show();
                            $("#divCreditDesc").hide();
                            $("#tblNoAttend").hide();
                            $("#divStep2StartDate").hide();
                            $("#divStep2EndDate").hide();
                            if (response.withdrawalDate !== undefined &&
                                response.withdrawalDate !== null &&
                                response.withdrawalDate !== "") {
                                let dtWithDrawlDate = Utility.formatDate(response.withdrawalDate);
                                $("#txt2CreditWithdrawalDate").val(MasterPage.formatDate(dtWithDrawlDate));
                                $("#divDate").show();
                            }

                            $("#txtCompletedDays").val(response.completedTime);
                            $("#txtTotalDays").val(response.totalTime);
                            if (response.percentageOfActualAttendance !== undefined &&
                                response.percentageOfActualAttendance !== null &&
                                response.percentageOfActualAttendance !== "" &&
                                response.percentageOfActualAttendance !== NaN)
                                $("#txtCreditActual")
                                    .val(Utility.formatNumberAsCurrencyPercenatgeTab(
                                        parseFloat(response.percentageOfActualAttendance)));

                            (document.getElementById("resultTabCompletedDays") as HTMLInputElement).innerText =
                                "Hours scheduled to complete";
                            (document.getElementById("resultTabTotalDays") as HTMLInputElement).innerText =
                                "Total hours in period";
                            document.getElementById('resultTabTotalDays').style.fontWeight = 'bold';
                            document.getElementById('resultTabCompletedDays').style.fontWeight = 'bold';
                        } else {
                            $("#dvStartDateResult").show();
                            $("#dvEndDateResult").show();
                            $("#divDate").show();
                            $("#tblNoAttend").show();
                            $("#txt2CreditWithdrawalDate").show();
                            $("#divClockDesc").hide();
                            $("#divCreditDesc").show();
                            $("#divStep2StartDate").show();
                            $("#divStep2EndDate").show();
                            if (response.startDate !== undefined &&
                                response.startDate !== null &&
                                response.startDate !== "") {
                                let dtStartDate = Utility.formatDate(response.startDate);
                                $("#txtStartDate").val(MasterPage.formatDate(dtStartDate));
                            }
                            if (response.scheduledEndDate !== undefined &&
                                response.scheduledEndDate !== null &&
                                response.scheduledEndDate !== "") {
                                let dtEndDate = Utility.formatDate(response.scheduledEndDate);
                                $("#txtEndDate").val(MasterPage.formatDate(dtEndDate));
                            }
                            if (response.withdrawalDate !== undefined &&
                                response.withdrawalDate !== null &&
                                response.withdrawalDate !== "") {
                                let dtWithDrawlDate = Utility.formatDate(response.withdrawalDate);
                                $("#txt2CreditWithdrawalDate").val(MasterPage.formatDate(dtWithDrawlDate));

                            }
                            if (response.isAttendanceNotRequired === true) {
                                $("#chkNAttend").prop('checked', true);
                            } else {
                                $("#chkNAttend").prop('checked', false);
                                $("#txtCompletedDays").val(response.completedTime);
                                $("#txtTotalDays").val(response.totalTime);
                                if (response.percentageOfActualAttendance !== undefined &&
                                    response.percentageOfActualAttendance !== null &&
                                    response.percentageOfActualAttendance !== "" &&
                                    response.percentageOfActualAttendance !== NaN)
                                    $("#txtCreditActual")
                                        .val(Utility.formatNumberAsCurrencyPercenatgeTab(
                                            parseFloat(response.percentageOfActualAttendance)));
                            }
                            (document.getElementById("resultTabCompletedDays") as HTMLInputElement).innerText =
                                "Completed days";
                            (document.getElementById("resultTabTotalDays") as HTMLInputElement).innerText =
                                "Total days";
                            document.getElementById('resultTabTotalDays').style.fontWeight = 'bold';
                            document.getElementById('resultTabCompletedDays').style.fontWeight = 'bold';
                        }
                    });

                if (response.boxHResult !== undefined && response.boxHResult !==
                    null && response.boxHResult !== "" && response.boxHResult !== NaN)
                    $("#txtCreditAttendencePerc").val(Utility.formatNumberAsPercenatgeResults(parseFloat(response.boxHResult)));
                //---3. Amount of Title IV Aid Earned by the Student----//
                if (response.boxHResult !== undefined && response.boxHResult !==
                    null && response.boxHResult !== "" && response.boxHResult !== NaN)
                    $("#txtBoxH").val(Utility.formatNumberAsPercenatgeResults(parseFloat(response.boxHResult)));

                if (response.boxGResult !== undefined && response.boxGResult !==
                    null && response.boxGResult !== "" && response.boxGResult !== NaN)
                    $("#txtBoxG").val(Utility.formatNumberAsCurrency(parseFloat(response.boxGResult)));

                if (response.boxIResult !== undefined && response.boxIResult !==
                    null && response.boxIResult !== "" && response.boxIResult !== NaN)
                    $("#txtBoxI").val(Utility.formatNumberAsCurrency(parseFloat(response.boxIResult)));

                //---4. Total Title IV Aid to be Disbursed or Returned----//
                if (response.boxIResult !== response.boxEResult) {
                    if (response.boxJResult !== undefined &&
                        response.boxJResult !==
                        null &&
                        response.boxJResult !== "" &&
                        response.boxJResult !== NaN) {
                        if (response.boxIResult !== undefined &&
                            response.boxIResult !==
                            null &&
                            response.boxIResult !== "" &&
                            response.boxIResult !== NaN)
                            $("#txt4JI").val(Utility.formatNumberAsCurrency(parseFloat(response.boxIResult)));
                        if (response.boxEResult !== undefined &&
                            response.boxEResult !==
                            null &&
                            response.boxEResult !== "" &&
                            response.boxEResult !== NaN)
                            $("#txt4JE").val(Utility.formatNumberAsCurrency(parseFloat(response.boxEResult)));

                        if (response.boxJResult !== undefined &&
                            response.boxJResult !==
                            null &&
                            response.boxJResult !== "" &&
                            response.boxJResult !== NaN)
                            $("#txtResultJ, #txtPWD, #txtPWDOffered")
                                .val(Utility.formatNumberAsCurrency(parseFloat(response.boxJResult)));
                    }

                    if (response.boxKResult !== undefined &&
                        response.boxKResult !==
                        null &&
                        response.boxKResult !== "" &&
                        response.boxKResult !== NaN) {

                        if (response.boxEResult !== undefined &&
                            response.boxEResult !==
                            null &&
                            response.boxEResult !== "" &&
                            response.boxEResult !== NaN)
                            $("#txt4KE")
                                .val(Utility.formatNumberAsCurrency(parseFloat(response.boxEResult)));

                        if (response.boxIResult !== undefined &&
                            response.boxIResult !==
                            null &&
                            response.boxIResult !== "" &&
                            response.boxIResult !== NaN)
                            $("#txt4KI")
                                .val(Utility.formatNumberAsCurrency(parseFloat(response.boxIResult)));

                        if (response.boxKResult !== undefined &&
                            response.boxKResult !==
                            null &&
                            response.boxKResult !== "" &&
                            response.boxKResult !== NaN)
                            $("#txtResultK")
                                .val(Utility.formatNumberAsCurrency(parseFloat(response.boxKResult)));
                    }

                    //----5. Amount of Unearned Title IV Aid Due from the School-----//
                    if (response.boxLResult !== undefined &&
                        response.boxLResult >= 0 &&
                        response.boxLResult !==
                        null &&
                        response.boxLResult !== "" &&
                        response.boxLResult !== NaN) {

                        if (response.boxLResult !== undefined &&
                            response.boxLResult !==
                            null &&
                            response.boxLResult !== "" &&
                            response.boxLResult !== NaN)
                            $("#txt5L").val(Utility.formatNumberAsCurrency(parseFloat(response.boxLResult)));

                        if (response.boxHResult !== undefined &&
                            response.boxHResult !==
                            null &&
                            response.boxHResult !== "" &&
                            response.boxHResult !== NaN)
                            $("#txt5BoxH")
                                .val(Utility.formatNumberAsPercenatgeResults(parseFloat(response.boxHResult)));

                        if (response.boxMResult !== undefined &&
                            response.boxMResult !==
                            null &&
                            response.boxMResult !== "" &&
                            response.boxMResult !== NaN)
                            $("#txt5BoxMResult")
                                .val(Utility.formatNumberAsPercenatgeResults(parseFloat(response.boxMResult)));

                        if (response.boxLResult !== undefined &&
                            response.boxLResult !==
                            null &&
                            response.boxLResult !== "" &&
                            response.boxLResult !== NaN)
                            $("#txt5BoxL").val(Utility.formatNumberAsCurrency(parseFloat(response.boxLResult)));

                        if (response.boxMResult !== undefined &&
                            response.boxMResult !==
                            null &&
                            response.boxMResult !== "" &&
                            response.boxMResult !== NaN)
                            $("#txt5BoxM")
                                .val(Utility.formatNumberAsPercenatgeResults(parseFloat(response.boxMResult)));

                        if (response.boxNResult !== undefined &&
                            response.boxNResult !==
                            null &&
                            response.boxNResult !== "" &&
                            response.boxNResult !== NaN)
                            $("#txt5BoxNResult")
                                .val(Utility.formatNumberAsCurrency(parseFloat(response.boxNResult)));

                        if (response.boxOResult !== undefined &&
                            response.boxOResult !==
                            null &&
                            response.boxOResult !== "" &&
                            response.boxOResult !== NaN)
                            $("#txt5BoxOresult")
                                .val(Utility.formatNumberAsCurrency(parseFloat(response.boxOResult)));
                    }
                    //----6. Return of Funds by the School---//
                    if (response.unsubDirectLoanSchoolReturn !== undefined &&
                        response.unsubDirectLoanSchoolReturn !== "" &&
                        response.unsubDirectLoanSchoolReturn !== NaN &&
                        response.unsubDirectLoanSchoolReturn !==
                        null) {
                        $("#txtUnSubLoan6")
                            .val(Utility.formatNumberAsCurrency(parseFloat(response.unsubDirectLoanSchoolReturn)));
                    }
                    if (response.subDirectLoanSchoolReturn !== undefined &&
                        response.subDirectLoanSchoolReturn !== "" &&
                        response.subDirectLoanSchoolReturn !== NaN &&
                        response.subDirectLoanSchoolReturn !==
                        null) {
                        $("#txtSubLoan6")
                            .val(Utility.formatNumberAsCurrency(parseFloat(response.subDirectLoanSchoolReturn)));
                    }

                    if (response.perkinsLoanSchoolReturn !== undefined &&
                        response.perkinsLoanSchoolReturn !== "" &&
                        response.perkinsLoanSchoolReturn !== NaN &&
                        response.perkinsLoanSchoolReturn !==
                        null) {
                        $("#txtPerkinsLoan6")
                            .val(Utility.formatNumberAsCurrency(parseFloat(response.perkinsLoanSchoolReturn)));
                    }
                    if (response.directGraduatePlusLoanSchoolReturn !== undefined &&
                        response.directGraduatePlusLoanSchoolReturn !== "" &&
                        response.directGraduatePlusLoanSchoolReturn !== NaN &&
                        response.directGraduatePlusLoanSchoolReturn !==
                        null) {
                        $("#txtFFELStudent6")
                            .val(Utility.formatNumberAsCurrency(
                                parseFloat(response.directGraduatePlusLoanSchoolReturn)));
                    }
                    if (response.directParentPlusLoanSchoolReturn !== undefined &&
                        response.directParentPlusLoanSchoolReturn !== "" &&
                        response.directParentPlusLoanSchoolReturn !== NaN &&
                        response.directParentPlusLoanSchoolReturn !==
                        null) {
                        $("#txtFFELParent6")
                            .val(Utility.formatNumberAsCurrency(
                                parseFloat(response.directParentPlusLoanSchoolReturn)));
                    }

                    if (response.pellGrantSchoolReturn !== undefined &&
                        response.pellGrantSchoolReturn !== "" &&
                        response.pellGrantSchoolReturn !== NaN &&
                        response.pellGrantSchoolReturn !==
                        null) {
                        $("#txtPellGrant6")
                            .val(Utility.formatNumberAsCurrency(parseFloat(response.pellGrantSchoolReturn)));
                    }
                    if (response.fseogSchoolReturn !== undefined &&
                        response.fseogSchoolReturn !== "" &&
                        response.fseogSchoolReturn !== NaN &&
                        response.fseogSchoolReturn !==
                        null) {
                        $("#txtFSEOGGrant6")
                            .val(Utility.formatNumberAsCurrency(parseFloat(response.fseogSchoolReturn)));
                    }
                    if (response.teachGrantSchoolReturn !== undefined &&
                        response.teachGrantSchoolReturn !== "" &&
                        response.teachGrantSchoolReturn !== NaN &&
                        response.teachGrantSchoolReturn !==
                        null) {
                        $("#txtTeachGrant6")
                            .val(Utility.formatNumberAsCurrency(parseFloat(response.teachGrantSchoolReturn)));
                    }
                    if (response.iraqAfgGrantSchoolReturn !== undefined &&
                        response.iraqAfgGrantSchoolReturn !== "" &&
                        response.iraqAfgGrantSchoolReturn !== NaN &&
                        response.iraqAfgGrantSchoolReturn !==
                        null) {
                        $("#txtIASGrant6")
                            .val(Utility.formatNumberAsCurrency(parseFloat(response.iraqAfgGrantSchoolReturn)));
                    }

                    if (response.boxPResult !== undefined &&
                        response.boxPResult !==
                        null &&
                        response.boxPResult !== "" &&
                        response.boxPResult !== NaN)
                        $("#txtStep6P").val(Utility.formatNumberAsCurrency(parseFloat(response.boxPResult)));

                    //----- 7. Initial Amount of Unearned Title IV Aid Due from the Student----//
                    if (response.boxKResult !== undefined &&
                        response.boxKResult !==
                        null &&
                        response.boxKResult !== "" &&
                        response.boxKResult !== NaN)
                        $("#txt7K").val(Utility.formatNumberAsCurrency(parseFloat(response.boxKResult)));

                    if (response.boxOResult !== undefined &&
                        response.boxOResult !==
                        null &&
                        response.boxOResult !== "" &&
                        response.boxOResult !== NaN)
                        $("#txt7O").val(Utility.formatNumberAsCurrency(parseFloat(response.boxOResult)));

                    if (response.boxQResult !== undefined &&
                        response.boxQResult !==
                        null &&
                        response.boxQResult !== "" &&
                        response.boxQResult !== NaN)
                        $("#txtStep7Result").val(Utility.formatNumberAsCurrency(parseFloat(response.boxQResult)));

                    //----- 8. Repayment of the Student's loans----//
                    if (response.boxRResult !== undefined &&
                        response.boxRResult !==
                        null &&
                        response.boxRResult !== "" &&
                        response.boxRResult !== NaN) {
                        if (response.subTotalNetAmountDisbursedB !== undefined &&
                            response.subTotalNetAmountDisbursedB !==
                            null &&
                            response.subTotalNetAmountDisbursedB !== "" &&
                            response.subTotalNetAmountDisbursedB !== NaN)
                            $("#txt8boxB")
                                .val(Utility.formatNumberAsCurrency(
                                    parseFloat(response.subTotalNetAmountDisbursedB)));

                        if (response.boxPResult !== undefined &&
                            response.boxPResult !==
                            null &&
                            response.boxPResult !== "" &&
                            response.boxPResult !== NaN)
                            $("#txt8P").val(Utility.formatNumberAsCurrency(parseFloat(response.boxPResult)));

                        if (response.boxRResult !== undefined &&
                            response.boxRResult !==
                            null &&
                            response.boxRResult !== "" &&
                            response.boxRResult !== NaN)
                            $("#txt8BoxR").val(Utility.formatNumberAsCurrency(parseFloat(response.boxRResult)));
                    }
                    //----- 9. Grant Funds to be Returned----//
                    if (!(response.boxQResult <= response.boxRResult)) {
                        if (response.boxQResult !== undefined &&
                            response.boxQResult !==
                            null &&
                            response.boxQResult !== "" &&
                            response.boxQResult !== NaN)
                            $("#txt9BoxQ").val(Utility.formatNumberAsCurrency(parseFloat(response.boxQResult)));

                        if (response.boxRResult !== undefined &&
                            response.boxRResult !==
                            null &&
                            response.boxRResult !== "" &&
                            response.boxRResult !== NaN)
                            $("#txt9BoxR").val(Utility.formatNumberAsCurrency(parseFloat(response.boxRResult)));

                        if (response.boxSResult !== undefined &&
                            response.boxSResult !==
                            null &&
                            response.boxSResult !== "" &&
                            response.boxSResult !== NaN)
                            $("#txt9BoxSResult")
                                .val(Utility.formatNumberAsCurrency(parseFloat(response.boxSResult)));

                        if (response.boxFResult !== undefined &&
                            response.boxFResult !==
                            null &&
                            response.boxFResult !== "" &&
                            response.boxFResult !== NaN)
                            $("#txt9BoxF").val(Utility.formatNumberAsCurrency(parseFloat(response.boxFResult)));

                        if (response.boxTResult !== undefined &&
                            response.boxTResult !==
                            null &&
                            response.boxTResult !== "" &&
                            response.boxTResult !== NaN)
                            $("#txt9BoxTResult")
                                .val(Utility.formatNumberAsCurrency(parseFloat(response.boxTResult)));

                        if (response.boxSResult !== undefined &&
                            response.boxSResult !==
                            null &&
                            response.boxSResult !== "" &&
                            response.boxSResult !== NaN)
                            $("#txt9Boxs").val(Utility.formatNumberAsCurrency(parseFloat(response.boxSResult)));

                        if (response.boxTResult !== undefined &&
                            response.boxTResult !==
                            null &&
                            response.boxTResult !== "" &&
                            response.boxTResult !== NaN)
                            $("#txt9BoxT").val(Utility.formatNumberAsCurrency(parseFloat(response.boxTResult)));

                        if (response.boxUResult !== undefined &&
                            response.boxUResult !==
                            null &&
                            response.boxUResult !== "" &&
                            response.boxUResult !== NaN)
                            $("#txtBox9Result")
                                .val(Utility.formatNumberAsCurrency(parseFloat(response.boxUResult)));

                        //----- 10. Return of Grant Funds by the Student----//
                        if (response.pellGrantAmountToReturn !== undefined &&
                            response.pellGrantAmountToReturn !==
                            null &&
                            response.pellGrantAmountToReturn !== "" &&
                            response.pellGrantAmountToReturn !== NaN &&
                            response.pellGrantAmountToReturn > 0) {
                            $("#txt10Pell").val(Utility.formatNumberAsCurrency(parseFloat(response.pellGrantAmountToReturn)));
                        }
                        if (response.fseogAmountToReturn !== undefined &&
                            response.fseogAmountToReturn !==
                            null &&
                            response.fseogAmountToReturn !== "" &&
                            response.fseogAmountToReturn !== NaN &&
                            response.fseogAmountToReturn > 0) {
                            $("#txt10FSEOG").val(Utility.formatNumberAsCurrency(parseFloat(response.fseogAmountToReturn)));
                        }
                        if (response.teachGrantAmountToReturn !== undefined &&
                            response.teachGrantAmountToReturn !==
                            null &&
                            response.teachGrantAmountToReturn !== "" &&
                            response.teachGrantAmountToReturn !== NaN &&
                            response.teachGrantAmountToReturn > 0) {
                            $("#txt10TG").val(Utility.formatNumberAsCurrency(parseFloat(response.teachGrantAmountToReturn)));
                        }
                        if (response.iraqAfgGrantAmountToReturn !== undefined &&
                            response.iraqAfgGrantAmountToReturn !==
                            null &&
                            response.iraqAfgGrantAmountToReturn !== "" &&
                            response.iraqAfgGrantAmountToReturn !== NaN &&
                            response.iraqAfgGrantAmountToReturn > 0) {
                            $("#txt10IFSG").val(Utility.formatNumberAsCurrency(parseFloat(response.iraqAfgGrantAmountToReturn)));
                        }
                        //---11. Post-Withdrawal data----//
                       
                    }
                }
                if (response.overriddenData !== undefined &&
                    response.overriddenData !==
                    null &&
                    response.overriddenData !== "" &&
                    response.overriddenData !== NaN) {
                    this.setDuplicatedOverriddenFields(response.overriddenData);
                }
                if (response.postWithdrawalData !== undefined &&
                    response.postWithdrawalData !==
                    null &&
                    response.postWithdrawalData !== "" &&
                    response.postWithdrawalData !== NaN) {
                    this.setPwdOverriddenFields(response.postWithdrawalData);
                }

                let dateFormCompleted = Utility.formatDate(response.updatedDate);
                $("#dateCompleted").text(dateFormCompleted);
            }

            let dateFormCompleted = Utility.formatDate(response.updatedDate);
            $("#dateCompleted").text(dateFormCompleted);
        }

        /*
        *  setR2T4ResultDetails  will be used to bind the Results data to controls when new calculation of results takes place.
       */
        public setR2T4ResultDetails(response: any, r2T4InputDetail: any): any {
            this.clearR2T4Results();
            if (response !== undefined) {
                //---1. Student's Title IV Aid Information----//
                $("#txtPellA").val(r2T4InputDetail.pellGrantDisbursed);
                $("#txtPellB").val(r2T4InputDetail.pellGrantCouldDisbursed);
                $("#txtFSEOGA").val(r2T4InputDetail.fseogDisbursed);
                $("#txtFSEOGB").val(r2T4InputDetail.fseogCouldDisbursed);
                $("#txtTeachA").val(r2T4InputDetail.teachGrantDisbursed);
                $("#txtTeachB").val(r2T4InputDetail.teachGrantCouldDisbursed);
                $("#txtIraqAfganA").val(r2T4InputDetail.iraqAfgGrantDisbursed);
                $("#txtIraqAfganB").val(r2T4InputDetail.iraqAfgGrantCouldDisbursed);
                if (response.results.stepResults.BoxA !== undefined &&
                    response.results.stepResults.BoxA !==
                    null &&
                    response.results.stepResults.BoxA !== "" &&
                    response.results.stepResults.BoxA !== NaN)
                    $("#txtSubTotalA").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxA)));
                if (response.results.stepResults.BoxC !== undefined &&
                    response.results.stepResults.BoxC !==
                    null &&
                    response.results.stepResults.BoxC !== "" &&
                    response.results.stepResults.BoxC !== NaN)
                    $("#txtSubTotalC").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxC)));
                if (response.results.stepResults.BoxA !== undefined &&
                    response.results.stepResults.BoxA !==
                    null &&
                    response.results.stepResults.BoxA !== "" &&
                    response.results.stepResults.BoxA !== NaN)
                    $("#txtEA").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxA)));
                if (response.results.stepResults.BoxB !== undefined &&
                    response.results.stepResults.BoxB !==
                    null &&
                    response.results.stepResults.BoxB !== "" &&
                    response.results.stepResults.BoxB !== NaN)
                    $("#txtEB").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxB)));
                if (response.results.stepResults.BoxE !== undefined &&
                    response.results.stepResults.BoxE !==
                    null &&
                    response.results.stepResults.BoxE !== "" &&
                    response.results.stepResults.BoxE !== NaN)
                    $("#txtResultE").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxE)));
                if (response.results.stepResults.BoxA !== undefined &&
                    response.results.stepResults.BoxA !==
                    null &&
                    response.results.stepResults.BoxA !== "" &&
                    response.results.stepResults.BoxA !== NaN)
                    $("#txtFA").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxA)));
                if (response.results.stepResults.BoxC !== undefined &&
                    response.results.stepResults.BoxC !==
                    null &&
                    response.results.stepResults.BoxC !== "" &&
                    response.results.stepResults.BoxC !== NaN)
                    $("#txtFC").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxC)));
                if (response.results.stepResults.BoxF !== undefined &&
                    response.results.stepResults.BoxF !==
                    null &&
                    response.results.stepResults.BoxF !== "" &&
                    response.results.stepResults.BoxF !== NaN)
                    $("#txtstep1F").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxF)));

                $("#txtUnSubFFEL").val(r2T4InputDetail.unsubLoanNetAmountDisbursed);
                $("#txtUnSubFFElM").val(r2T4InputDetail.unsubLoanNetAmountCouldDisbursed);
                $("#txtSubFFEL").val(r2T4InputDetail.subLoanNetAmountDisbursed);
                $("#txtSubFFElM").val(r2T4InputDetail.subLoanNetAmountCouldDisbursed);
                $("#txtPerkins").val(r2T4InputDetail.perkinsLoanDisbursed);
                $("#txtPerkinsM").val(r2T4InputDetail.perkinsLoanCouldDisbursed);
                $("#txtFFElStudent").val(r2T4InputDetail.directGraduatePlusLoanDisbursed);
                $("#txtFFELStudentM").val(r2T4InputDetail.directGraduatePlusLoanCouldDisbursed);
                $("#txtFFELParent").val(r2T4InputDetail.directParentPlusLoanDisbursed);
                $("#txtFFELParentM").val(r2T4InputDetail.directParentPlusLoanCouldDisbursed);

                if (response.results.stepResults.BoxB !== undefined &&
                    response.results.stepResults.BoxB !==
                    null &&
                    response.results.stepResults.BoxB !== "" &&
                    response.results.stepResults.BoxB !== NaN)
                    $("#txtSubTotalB").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxB)));

                if (response.results.stepResults.BoxD !== undefined &&
                    response.results.stepResults.BoxD !==
                    null &&
                    response.results.stepResults.BoxD !== "" &&
                    response.results.stepResults.BoxD !== NaN)
                    $("#txtSubtotalD").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxD)));

                if (response.results.stepResults.BoxA !== undefined &&
                    response.results.stepResults.BoxA !==
                    null &&
                    response.results.stepResults.BoxA !== "" &&
                    response.results.stepResults.BoxA !== NaN)
                    $("#txtStep1FA").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxA)));

                if (response.results.stepResults.BoxB !== undefined &&
                    response.results.stepResults.BoxB !==
                    null &&
                    response.results.stepResults.BoxB !== "" &&
                    response.results.stepResults.BoxB !== NaN)
                    $("#txtStep1FB").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxB)));

                if (response.results.stepResults.BoxC !== undefined &&
                    response.results.stepResults.BoxC !==
                    null &&
                    response.results.stepResults.BoxC !== "" &&
                    response.results.stepResults.BoxC !== NaN)
                    $("#txtStep1FC").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxC)));
                if (response.results.stepResults.BoxD !== undefined &&
                    response.results.stepResults.BoxD !==
                    null &&
                    response.results.stepResults.BoxD !== "" &&
                    response.results.stepResults.BoxD !== NaN)
                    $("#txtStep1FD").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxD)));

                if (response.results.stepResults.BoxG !== undefined &&
                    response.results.stepResults.BoxG !==
                    null &&
                    response.results.stepResults.BoxG !== "" &&
                    response.results.stepResults.BoxG !== NaN)
                    $("#txtStep1G").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxG)));

                //---2.Percentage of Title IV Aid Earned----- calculated // 
                $("#dvcalStartDateResult").hide();
                $("#dvCalEndDateResult").hide();
                $("#divCalender").hide();

                if (response.programType === constants.ProgramType.ClockHour) {
                    $("#divStep2StartDate").hide();
                    $("#divClockDesc").show();
                    $("#divCreditDesc").hide();
                    $("#divStep2EndDate").hide();
                    $("#tblNoAttend").hide();
                    if (r2T4InputDetail.withdrawalDate !== undefined &&
                        r2T4InputDetail.withdrawalDate !== null &&
                        r2T4InputDetail.withdrawalDate !== "") {
                        let dtWithDrawlDate = Utility.formatDate(r2T4InputDetail.withdrawalDate);
                        $("#txt2CreditWithdrawalDate").val(dtWithDrawlDate);
                        $("#divDate").show();
                    }

                    $("#txtCompletedDays").val(r2T4InputDetail.completedTime);
                    $("#txtTotalDays").val(r2T4InputDetail.totalTime);
                    if (response.results.stepResults.ActualAttendence !== undefined &&
                        response.results.stepResults.ActualAttendence !== null && response.results.stepResults.ActualAttendence !== ""
                        && response.results.stepResults.ActualAttendence !== NaN)
                        $("#txtCreditActual").val(Utility.formatNumberAsCurrencyPercenatgeTab(parseFloat(response.results.stepResults.ActualAttendence)));

                    (document.getElementById("resultTabCompletedDays") as HTMLInputElement).innerText = "Hours scheduled to complete";
                    (document.getElementById("resultTabTotalDays") as HTMLInputElement).innerText = "Total hours in period";
                    document.getElementById('resultTabTotalDays').style.fontWeight = 'bold';
                    document.getElementById('resultTabCompletedDays').style.fontWeight = 'bold';
                }
                else if (response.programType === constants.ProgramType.CreditHour) {
                    $("#tblNoAttend").show();
                    $("#divClockDesc").hide();
                    $("#divCreditDesc").show();
                    $("#divStep2StartDate").show();
                    $("#divStep2EndDate").show();
                    $("#divDate").show();
                    $("#txt2CreditWithdrawalDate").show();
                    if (r2T4InputDetail.startDate !== undefined &&
                        r2T4InputDetail.startDate !== null &&
                        r2T4InputDetail.startDate !== "") {
                        let dtStartDate = Utility.formatDate(r2T4InputDetail.startDate);
                        $("#txtStartDate").val(MasterPage.formatDate(dtStartDate));
                        $("#dvStartDateResult").show();
                    }
                    if (r2T4InputDetail.scheduledEndDate !== undefined &&
                        r2T4InputDetail.scheduledEndDate !== null &&
                        r2T4InputDetail.scheduledEndDate !== "") {
                        let dtEndDate = Utility.formatDate(r2T4InputDetail.scheduledEndDate);
                        $("#txtEndDate").val(MasterPage.formatDate(dtEndDate));
                        $("#dvEndDateResult").show();
                    }
                    if (r2T4InputDetail.withdrawalDate !== undefined &&
                        r2T4InputDetail.withdrawalDate !== null &&
                        r2T4InputDetail.withdrawalDate !== "") {
                        let dtWithDrawlDate = Utility.formatDate(r2T4InputDetail.withdrawalDate);
                        $("#txt2CreditWithdrawalDate").val(MasterPage.formatDate(dtWithDrawlDate));
                        $("#divDate").show();
                    }
                    if (r2T4InputDetail.isAttendanceNotRequired === true) {
                        $("#chkNAttend").prop('checked', true);
                    } else {
                        $("#chkNAttend").prop('checked', false);
                        $("#txtCompletedDays").val(r2T4InputDetail.completedTime);
                        $("#txtTotalDays").val(r2T4InputDetail.totalTime);
                        if (response.results.stepResults.ActualAttendence !== undefined &&
                            response.results.stepResults.ActualAttendence !== null && response.results.stepResults.ActualAttendence !== ""
                            && response.results.stepResults.ActualAttendence !== NaN)
                            $("#txtCreditActual").val(Utility.formatNumberAsCurrencyPercenatgeTab(parseFloat(response.results.stepResults.ActualAttendence)));
                    }

                    (document.getElementById("resultTabCompletedDays") as HTMLInputElement).innerText = "Completed days";
                    (document.getElementById("resultTabTotalDays") as HTMLInputElement).innerText = "Total days";
                    document.getElementById('resultTabTotalDays').style.fontWeight = 'bold';
                    document.getElementById('resultTabCompletedDays').style.fontWeight = 'bold';
                }
                if (response.results.stepResults.BoxH !== undefined && response.results.stepResults.BoxH !==
                    null && response.results.stepResults.BoxH !== "" && response.results.stepResults.BoxH !== NaN)
                    $("#txtCreditAttendencePerc").val(Utility.formatNumberAsPercenatgeResults(parseFloat(response.results.stepResults.BoxH)));
                //---3. Amount of Title IV Aid Earned by the Student----//
                if (response.results.stepResults.BoxH !== undefined && response.results.stepResults.BoxH !==
                    null && response.results.stepResults.BoxH !== "" && response.results.stepResults.BoxH !== NaN)
                    $("#txtBoxH").val(Utility.formatNumberAsPercenatgeResults(parseFloat(response.results.stepResults.BoxH)));

                if (response.results.stepResults.BoxG !== undefined && response.results.stepResults.BoxG !==
                    null && response.results.stepResults.BoxG !== "" && response.results.stepResults.BoxG !== NaN)
                    $("#txtBoxG").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxG)));

                if (response.results.stepResults.BoxI !== undefined && response.results.stepResults.BoxI !==
                    null && response.results.stepResults.BoxI !== "" && response.results.stepResults.BoxI !== NaN)
                    $("#txtBoxI").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxI)));

                //---4. Total Title IV Aid to be Disbursed or Returned----//
                if (response.isStep4Required) {
                    if (response.results.stepResults.BoxJ !== undefined &&
                        response.results.stepResults.BoxJ !==
                        null &&
                        response.results.stepResults.BoxJ !== "" &&
                        response.results.stepResults.BoxJ !== NaN) {
                        if (response.results.stepResults.BoxI !== undefined &&
                            response.results.stepResults.BoxI !==
                            null &&
                            response.results.stepResults.BoxI !== "" &&
                            response.results.stepResults.BoxI !== NaN)
                            $("#txt4JI").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxI)));
                        if (response.results.stepResults.BoxE !== undefined &&
                            response.results.stepResults.BoxE !==
                            null &&
                            response.results.stepResults.BoxE !== "" &&
                            response.results.stepResults.BoxE !== NaN)
                            $("#txt4JE").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxE)));

                        if (response.results.stepResults.BoxJ !== undefined &&
                            response.results.stepResults.BoxJ !==
                            null &&
                            response.results.stepResults.BoxJ !== "" &&
                            response.results.stepResults.BoxJ !== NaN)
                            $("#txtResultJ, #txtPWD, #txtPWDOffered")
                                .val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxJ)));
                    }

                    if (response.results.stepResults.BoxK !== undefined &&
                        response.results.stepResults.BoxK !==
                        null &&
                        response.results.stepResults.BoxK !== "" &&
                        response.results.stepResults.BoxK !== NaN) {

                        if (response.results.stepResults.BoxE !== undefined &&
                            response.results.stepResults.BoxE !==
                            null &&
                            response.results.stepResults.BoxE !== "" &&
                            response.results.stepResults.BoxE !== NaN)
                            $("#txt4KE")
                                .val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxE)));

                        if (response.results.stepResults.BoxI !== undefined &&
                            response.results.stepResults.BoxI !==
                            null &&
                            response.results.stepResults.BoxI !== "" &&
                            response.results.stepResults.BoxI !== NaN)
                            $("#txt4KI")
                                .val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxI)));

                        if (response.results.stepResults.BoxK !== undefined &&
                            response.results.stepResults.BoxK !==
                            null &&
                            response.results.stepResults.BoxK !== "" &&
                            response.results.stepResults.BoxK !== NaN)
                            $("#txtResultK")
                                .val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxK)));
                    }
                }
                //----5. Amount of Unearned Title IV Aid Due from the School-----//
                $("#txt5Tuition").val(r2T4InputDetail.tuitionFee);
                $("#txt5Room").val(r2T4InputDetail.roomFee);
                $("#txt5Board").val(r2T4InputDetail.boardFee);
                $("#txt5Other").val(r2T4InputDetail.otherFee);

                if (response.results.stepResults.BoxL !== undefined &&
                    response.results.stepResults.BoxL >= 0 &&
                    response.results.stepResults.BoxL !==
                    null &&
                    response.results.stepResults.BoxL !== "" &&
                    response.results.stepResults.BoxL !== NaN) {

                    if (response.results.stepResults.TotalinstitutionalCharges !== undefined &&
                        response.results.stepResults.TotalinstitutionalCharges !==
                        null &&
                        response.results.stepResults.TotalinstitutionalCharges !== "" &&
                        response.results.stepResults.TotalinstitutionalCharges !== NaN) {
                        $("#txt5L").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.TotalinstitutionalCharges)));
                    }

                    if (response.results.stepResults.BoxH !== undefined &&
                        response.results.stepResults.BoxH !==
                        null &&
                        response.results.stepResults.BoxH !== "" &&
                        response.results.stepResults.BoxH !== NaN)
                        $("#txt5BoxH").val(Utility.formatNumberAsPercenatgeResults(parseFloat(response.results.stepResults.BoxH)));

                    if (response.results.stepResults.BoxM !== undefined &&
                        response.results.stepResults.BoxM !==
                        null &&
                        response.results.stepResults.BoxM !== "" &&
                        response.results.stepResults.BoxM !== NaN)
                        $("#txt5BoxMResult")
                            .val(Utility.formatNumberAsPercenatgeResults(parseFloat(response.results.stepResults.BoxM)));

                    if (response.results.stepResults.BoxL !== undefined &&
                        response.results.stepResults.BoxL !==
                        null &&
                        response.results.stepResults.BoxL !== "" &&
                        response.results.stepResults.BoxL !== NaN)
                        $("#txt5BoxL").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxL)));

                    if (response.results.stepResults.BoxM !== undefined &&
                        response.results.stepResults.BoxM !==
                        null &&
                        response.results.stepResults.BoxM !== "" &&
                        response.results.stepResults.BoxM !== NaN)
                        $("#txt5BoxM").val(Utility.formatNumberAsPercenatgeResults(parseFloat(response.results.stepResults.BoxM)));

                    if (response.results.stepResults.BoxN !== undefined &&
                        response.results.stepResults.BoxN !==
                        null &&
                        response.results.stepResults.BoxN !== "" &&
                        response.results.stepResults.BoxN !== NaN)
                        $("#txt5BoxNResult")
                            .val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxN)));

                    if (response.results.stepResults.BoxO !== undefined &&
                        response.results.stepResults.BoxO !==
                        null &&
                        response.results.stepResults.BoxO !== "" &&
                        response.results.stepResults.BoxO !== NaN)
                        $("#txt5BoxOresult")
                            .val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxO)));
                }
                //----6. Return of Funds by the School---//
                if (response.results.stepResults.UnsubsidizedFFELDirectStaffordLoan !== undefined && response.results.stepResults.unsubsidizedFfelOrDirectStaffordLoan !== "" && response.results.stepResults.unsubsidizedFfelOrDirectStaffordLoan !== NaN) {
                    $("#txtUnSubLoan6").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.UnsubsidizedFFELDirectStaffordLoan)));
                }
                if (response.results.stepResults.SubsidizedFFELDirectStaffordLoan !== undefined && response.results.stepResults.SubsidizedFFELDirectStaffordLoan !== "" && response.results.stepResults.SubsidizedFFELDirectStaffordLoan !== NaN) {
                    $("#txtSubLoan6").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.SubsidizedFFELDirectStaffordLoan)));
                }

                if (response.results.stepResults.PerkinsLoan !== undefined && response.results.stepResults.PerkinsLoan !== "" && response.results.stepResults.PerkinsLoan !== NaN) {
                    $("#txtPerkinsLoan6").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.PerkinsLoan)));
                }
                if (response.results.stepResults.FFELDirectPLUSStudent !== undefined && response.results.stepResults.FFELDirectPLUSStudent !== "" && response.results.stepResults.FFELDirectPLUSStudent !== NaN) {
                    $("#txtFFELStudent6").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.FFELDirectPLUSStudent)));
                }
                if (response.results.stepResults.FFELDirectPLUSParent !== undefined && response.results.stepResults.FFELDirectPLUSParent !== "" && response.results.stepResults.FFELDirectPLUSParent !== NaN) {
                    $("#txtFFELParent6").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.FFELDirectPLUSParent)));
                }

                if (response.results.stepResults.Pell !== undefined && response.results.stepResults.Pell !== "" && response.results.stepResults.Pell !== NaN) {
                    $("#txtPellGrant6").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.Pell)));
                }
                if (response.results.stepResults.FSEOG !== undefined && response.results.stepResults.FSEOG !== "" && response.results.stepResults.FSEOG !== NaN) {
                    $("#txtFSEOGGrant6").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.FSEOG)));
                }
                if (response.results.stepResults.TEACHGrant !== undefined && response.results.stepResults.TEACHGrant !== "" && response.results.stepResults.TEACHGrant !== NaN) {
                    $("#txtTeachGrant6").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.TEACHGrant)));
                }
                if (response.results.stepResults.IraqAfghanistanServiceGrant !== undefined && response.results.stepResults.IraqAfghanistanServiceGrant !== "" && response.results.stepResults.IraqAfghanistanServiceGrant !== NaN) {
                    $("#txtIASGrant6").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.IraqAfghanistanServiceGrant)));
                }

                if (response.results.stepResults.BoxP !== undefined && response.results.stepResults.BoxP !==

                    null && response.results.stepResults.BoxP !== "" && response.results.stepResults.BoxP !== NaN)
                    $("#txtStep6P").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxP)));

                //----- 7. Initial Amount of Unearned Title IV Aid Due from the Student----//
                if (response.results.stepResults.BoxK !== undefined && response.results.stepResults.BoxK !==

                    null && response.results.stepResults.BoxK !== "" && response.results.stepResults.BoxK !== NaN)
                    $("#txt7K").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxK)));

                if (response.results.stepResults.BoxO !== undefined && response.results.stepResults.BoxO !==

                    null && response.results.stepResults.BoxO !== "" && response.results.stepResults.BoxO !== NaN)
                    $("#txt7O").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxO)));

                if (response.results.stepResults.BoxQ !== undefined && response.results.stepResults.BoxQ !==

                    null && response.results.stepResults.BoxQ !== "" && response.results.stepResults.BoxQ !== NaN)
                    $("#txtStep7Result").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxQ)));

                //----- 8. Repayment of the Student's loans----//
                if (response.results.stepResults.BoxR !== undefined &&
                    response.results.stepResults.BoxR !==
                    null &&
                    response.results.stepResults.BoxR !== "" &&
                    response.results.stepResults.BoxR !== NaN) {
                    if (response.results.stepResults.BoxB !== undefined &&
                        response.results.stepResults.BoxB !==
                        null &&
                        response.results.stepResults.BoxB !== "" &&
                        response.results.stepResults.BoxB !== NaN)
                        $("#txt8boxB").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxB)));

                    if (response.results.stepResults.BoxP !== undefined &&
                        response.results.stepResults.BoxP !==
                        null &&
                        response.results.stepResults.BoxP !== "" &&
                        response.results.stepResults.BoxP !== NaN)
                        $("#txt8P").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxP)));

                    if (response.results.stepResults.BoxR !== undefined &&
                        response.results.stepResults.BoxR !==
                        null &&
                        response.results.stepResults.BoxR !== "" &&
                        response.results.stepResults.BoxR !== NaN)
                        $("#txt8BoxR").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxR)));
                }
                //----- 9. Grant Funds to be Returned----//
                if (response.isStep9Required) {
                    if (response.results.stepResults.BoxQ !== undefined &&
                        response.results.stepResults.BoxQ !==
                        null &&
                        response.results.stepResults.BoxQ !== "" &&
                        response.results.stepResults.BoxQ !== NaN)
                        $("#txt9BoxQ").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxQ)));

                    if (response.results.stepResults.BoxR !== undefined &&
                        response.results.stepResults.BoxR !==
                        null &&
                        response.results.stepResults.BoxR !== "" &&
                        response.results.stepResults.BoxR !== NaN)
                        $("#txt9BoxR").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxR)));

                    if (response.results.stepResults.BoxS !== undefined &&
                        response.results.stepResults.BoxS !==
                        null &&
                        response.results.stepResults.BoxS !== "" &&
                        response.results.stepResults.BoxS !== NaN)
                        $("#txt9BoxSResult")
                            .val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxS)));

                    if (response.results.stepResults.BoxF !== undefined &&
                        response.results.stepResults.BoxF !==
                        null &&
                        response.results.stepResults.BoxF !== "" &&
                        response.results.stepResults.BoxF !== NaN)
                        $("#txt9BoxF").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxF)));

                    if (response.results.stepResults.BoxT !== undefined &&
                        response.results.stepResults.BoxT !==
                        null &&
                        response.results.stepResults.BoxT !== "" &&
                        response.results.stepResults.BoxT !== NaN)
                        $("#txt9BoxTResult")
                            .val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxT)));

                    if (response.results.stepResults.BoxS !== undefined &&
                        response.results.stepResults.BoxS !==
                        null &&
                        response.results.stepResults.BoxS !== "" &&
                        response.results.stepResults.BoxS !== NaN)
                        $("#txt9Boxs").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxS)));

                    if (response.results.stepResults.BoxT !== undefined &&
                        response.results.stepResults.BoxT !==
                        null &&
                        response.results.stepResults.BoxT !== "" &&
                        response.results.stepResults.BoxT !== NaN)
                        $("#txt9BoxT").val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxT)));

                    if (response.results.stepResults.BoxU !== undefined &&
                        response.results.stepResults.BoxU !==
                        null &&
                        response.results.stepResults.BoxU !== "" &&
                        response.results.stepResults.BoxU !== NaN)
                        $("#txtBox9Result")
                            .val(Utility.formatNumberAsCurrency(parseFloat(response.results.stepResults.BoxU)));
                }
                //----- 10. Return of Grant Funds by the Student----//
                if (response.isStep10Required) {
                    var grants = response.grants;
                    grants.forEach((g) => {
                        switch (g.grantName) {
                            case constants.pell:
                                if (g.grantName === constants.pell && g.amountToBeReturnedByStudent > 0) {
                                    $("#txt10Pell").val(Utility.formatNumberAsCurrency(parseFloat(g.amountToBeReturnedByStudent)));
                                }
                                break;
                            case constants.fSeog:
                                if (g.grantName === constants.fSeog && g.amountToBeReturnedByStudent > 0) {
                                    $("#txt10FSEOG").val(Utility.formatNumberAsCurrency(parseFloat(g.amountToBeReturnedByStudent)));
                                }
                                break;
                            case constants.tEachGrant:
                                if (g.grantName === constants.tEachGrant && g.amountToBeReturnedByStudent > 0) {
                                    $("#txt10TG").val(Utility.formatNumberAsCurrency(parseFloat(g.amountToBeReturnedByStudent)));
                                }
                                break;
                            case constants.iraqAfghanistanServiceGrant:
                                if (g.grantName === constants.iraqAfghanistanServiceGrant &&
                                    g.amountToBeReturnedByStudent > 0) {
                                    $("#txt10IFSG").val(Utility.formatNumberAsCurrency(parseFloat(g.amountToBeReturnedByStudent)));
                                }
                                break;
                        }
                    });
                }
            }
        }

        /*
        *  generateR2T4ResultModel method generates the R2T4 Result object to pass to API to save/update.
        */
        public generateR2T4ResultModel(r2T4ResultModel: iR2T4Results): any {
            let isNewR2T4ResultId = $("#hdnR2T4ResultId").val() === undefined || $("#hdnR2T4ResultId").val() === "";
            r2T4ResultModel.r2T4ResultsId = isNewR2T4ResultId ? this.emptyGuid : $("#hdnR2T4ResultId").val();
            r2T4ResultModel.terminationId = $("#hdnTerminationId").val();
            r2T4ResultModel.subTotalAmountDisbursedA = $("#txtSubTotalA").val();
            r2T4ResultModel.subTotalNetAmountDisbursedB = $("#txtSubTotalB").val();
            r2T4ResultModel.subTotalAmountCouldDisbursedC = $("#txtSubTotalC").val();
            r2T4ResultModel.subTotalNetAmountDisbursedD = $("#txtSubtotalD").val();
            r2T4ResultModel.boxEResult = $("#txtResultE").val();
            r2T4ResultModel.boxFResult = $("#txtstep1F").val();
            r2T4ResultModel.boxGResult = $("#txtStep1G").val();
            r2T4ResultModel.boxHResult = $("#txtCreditAttendencePerc").val();
            r2T4ResultModel.boxIResult = $("#txtBoxI").val();
            r2T4ResultModel.boxJResult = $("#txtResultJ").val();
            r2T4ResultModel.boxKResult = $("#txtResultK").val();
            r2T4ResultModel.boxLResult = $("#txt5L").val();
            r2T4ResultModel.boxMResult = $("#txt5BoxMResult").val();
            r2T4ResultModel.boxNResult = $("#txt5BoxNResult").val();
            r2T4ResultModel.boxOResult = $("#txt5BoxOresult").val();
            r2T4ResultModel.boxPResult = $("#txtStep6P").val();
            r2T4ResultModel.boxQResult = $("#txtStep7Result").val();
            r2T4ResultModel.boxRResult = $("#txt8BoxR").val();
            r2T4ResultModel.boxSResult = $("#txt9BoxSResult").val();
            r2T4ResultModel.boxTResult = $("#txt9BoxTResult").val();
            r2T4ResultModel.boxUResult = $("#txtBox9Result").val();
            r2T4ResultModel.percentageOfActualAttendance = $("#txtCreditActual").val();
            r2T4ResultModel.unsubDirectLoanSchoolReturn = $("#txtUnSubLoan6").val();
            r2T4ResultModel.subDirectLoanSchoolReturn = $("#txtSubLoan6").val();
            r2T4ResultModel.perkinsLoanSchoolReturn = $("#txtPerkinsLoan6").val();
            r2T4ResultModel.directGraduatePlusLoanSchoolReturn = $("#txtFFELStudent6").val();
            r2T4ResultModel.directParentPlusLoanSchoolReturn = $("#txtFFELParent6").val();
            r2T4ResultModel.pellGrantSchoolReturn = $("#txtPellGrant6").val();
            r2T4ResultModel.fseogSchoolReturn = $("#txtFSEOGGrant6").val();
            r2T4ResultModel.teachGrantSchoolReturn = $("#txtTeachGrant6").val();
            r2T4ResultModel.iraqAfgGrantSchoolReturn = $("#txtIASGrant6").val();
            r2T4ResultModel.pellGrantAmountToReturn = $("#txt10Pell").val();
            r2T4ResultModel.fseogAmountToReturn = $("#txt10FSEOG").val();
            r2T4ResultModel.teachGrantAmountToReturn = $("#txt10TG").val();
            r2T4ResultModel.iraqAfgGrantAmountToReturn = $("#txt10IFSG").val();
            r2T4ResultModel.postWithdrawalData = $("#txtPWD").val();

            this.includeDuplicateOverriddenFields(r2T4ResultModel);
            this.includePwdOverriddenFields(r2T4ResultModel);

            //if ($("#ChkOverride").is(":checked")) { 
                r2T4ResultModel = this.includeR2T4InputInResults(r2T4ResultModel); 
            //}

            return r2T4ResultModel;
        }

        // This method is used to bind the Overridden R2T4 input fileds in results tab by support user.
        public includeR2T4InputInResults(r2T4ResultModel: iR2T4Results): any {

            r2T4ResultModel.pellGrantDisbursed = $("#txtPellA").val();
            r2T4ResultModel.pellGrantCouldDisbursed = $("#txtPellB").val();

            r2T4ResultModel.fseogDisbursed = $("#txtFSEOGA").val();
            r2T4ResultModel.fseogCouldDisbursed = $("#txtFSEOGB").val();

            r2T4ResultModel.teachGrantDisbursed = $("#txtTeachA").val();
            r2T4ResultModel.teachGrantCouldDisbursed = $("#txtTeachB").val();

            r2T4ResultModel.iraqAfgGrantDisbursed = $("#txtIraqAfganA").val();
            r2T4ResultModel.iraqAfgGrantCouldDisbursed = $("#txtIraqAfganB").val();

            r2T4ResultModel.unsubLoanNetAmountDisbursed = $("#txtUnSubFFEL").val();
            r2T4ResultModel.unsubLoanNetAmountCouldDisbursed = $("#txtUnSubFFElM").val();

            r2T4ResultModel.subLoanNetAmountDisbursed = $("#txtSubFFEL").val();
            r2T4ResultModel.subLoanNetAmountCouldDisbursed = $("#txtSubFFElM").val();

            r2T4ResultModel.perkinsLoanDisbursed = $("#txtPerkins").val();
            r2T4ResultModel.perkinsLoanCouldDisbursed = $("#txtPerkinsM").val();

            r2T4ResultModel.directGraduatePlusLoanDisbursed = $("#txtFFElStudent").val();
            r2T4ResultModel.directGraduatePlusLoanCouldDisbursed = $("#txtFFELStudentM").val();

            r2T4ResultModel.directParentPlusLoanDisbursed = $("#txtFFELParent").val();
            r2T4ResultModel.directParentPlusLoanCouldDisbursed = $("#txtFFELParentM").val();
            if ($("#ChkOverride").is(':checked')) {
                r2T4ResultModel.withdrawalDate = $("#cal2CreditWithdrawalDateEnabled").val() === ""
                    ? null
                    : $("#cal2CreditWithdrawalDateEnabled").val();
            }
            else
                r2T4ResultModel.withdrawalDate = $("#txt2CreditWithdrawalDate").val() === ""
                    ? null
                    : $("#txt2CreditWithdrawalDate").val();
            Utility.isClockHour($("#hdnEnrollmentId").val(),
                (response) => {
                    if (response) {
                        r2T4ResultModel.startDate = null;
                        r2T4ResultModel.scheduledEndDate = null;
                    } else {
                        if ($("#ChkOverride").is(':checked')) {
                            r2T4ResultModel.startDate =
                                $("#calStartDate").val() === "" ? null : $("#calStartDate").val();
                            r2T4ResultModel.scheduledEndDate =
                                $("#calEndDate").val() === "" ? null : $("#calEndDate").val();
                        } else {
                            r2T4ResultModel.startDate =
                                $("#txtStartDate").val() === "" ? null : $("#txtStartDate").val();
                            r2T4ResultModel.scheduledEndDate =
                                $("#txtEndDate").val() === "" ? null : $("#txtEndDate").val();
                        }
                    }
                });
            //Not required to take attendance
            r2T4ResultModel.isAttendanceNotRequired = $("#chkNAttend").prop("checked");
            r2T4ResultModel.completedTime = $("#txtCompletedDays").val();
            r2T4ResultModel.totalTime = $("#txtTotalDays").val();

            //Institutional charges posted
            r2T4ResultModel.isTuitionChargedByPaymentPeriod = $("#chkTuitionCharged").prop("checked");
            r2T4ResultModel.tuitionFee = $("#txt5Tuition").val();
            r2T4ResultModel.roomFee = $("#txt5Room").val();
            r2T4ResultModel.boardFee = $("#txt5Board").val();
            r2T4ResultModel.otherFee = $("#txt5Other").val();
            return r2T4ResultModel;

        }

        //clears the controls of R2T4Result tab.
        private clearR2T4Results() {
            $("#Studentbar").find("input:text").each(function () {
                $(this).val("");
            });
            $("#Studentbar").find("class:k-datepicker").each(function () {
                $(this).val("");
            });
            $("#Studentbar").find("input:checkbox").each(function () {
                $(this).prop('checked', false);
            });
            $("#ChkOverride").prop('checked', false);
            $("#txtTicket").val("");
            $("#txt2CreditWithdrawalDate").val("");
            $("#txtCompletedDays").val("");
            $("#txtTotalDays").val("");
            $("#txtCreditActual").val("");
        }

        //this method will check if any input has data validation message 
        //and return true if there is no validation message else false
        public validate(): boolean {
            let isValidForm: boolean = true;
            $("#divR2T4Result").find("input:text").each((index, element) => {
                let tooltip = $(element).prop("name") + "tt";
                let tool = $(`#${tooltip}`);
                if (tool.length > 0) {
                    isValidForm = false;
                }
            });
            return isValidForm;
        }

        //this method generates a object to pass to API for check the results data existance based on terminationId and Type of results table
        private generateResultMap(resultMap: iResultMap, terminationId: string, resultType: number) {
            resultMap.terminationId = terminationId;
            resultMap.tabId = resultType;
            return resultMap;
        }

        //this method provides the logic to identify whether to save or update the results.
        private isSaveOrUpdate(r2T4ResultDetail, resultType, onResponseCallback: (response: Response) => any) {
            this.generateResultMap(this.resultMap, r2T4ResultDetail.terminationId, resultType);
            r2T4Termination.isTerminationIdExists(this.resultMap,
                (response: any) => {
                    onResponseCallback(response);
                }
            );
        }

        //this method provides implementation to navigate to R2T4Input tab fro R2T4Results tab.
        public navigateToR2T4Input() {
            Utility.changeElementStyle("divR2T4Input", "is-active", true);
            Utility.changeElementStyle("liR2T4Input", "is-active", true);
            Utility.changeElementStyle("divR2T4Result", "is-active", false);
            Utility.changeElementStyle("liR2T4Result", "is-active", false);
            Utility.changeElementStyle("liApproveTermination", "is-active", false);
            $("#liR2T4Input").addClass("is-active").removeClass("is-visited");
            $("#liR2T4Input,#liR2T4Result").removeClass("is-invalid");
            $("#liApproveTermination").removeClass("is-active");
            //$("#liR2T4Result").addClass("is-visited");
        }

        //this method disables the date and checkbox controls in PWD form.
        public disablePwdControls() {
            $('#dtPostWithdrwal').data('kendoDatePicker').enable(false);
            $('#dtDeadline').data('kendoDatePicker').enable(false);
            $('#dtResponseReceived').data('kendoDatePicker').enable(false);
            $('#dtGrantTransferred').data('kendoDatePicker').enable(false);
            $('#dtLoanTransferred').data('kendoDatePicker').enable(false);
            $("#chkResponseReceived").prop("disabled", true);
            $("#chkResponseNotReceived").prop("disabled", true);
            $("#chkNotAccept").prop("disabled", true);
        }

        //this method enables the date and checkbox controls in PWD form.
        public enablePwdControls() {
            $('#dtPostWithdrwal').data('kendoDatePicker').enable(true);
            $('#dtDeadline').data('kendoDatePicker').enable(true);
            $('#dtResponseReceived').data('kendoDatePicker').enable(true);
            $('#dtGrantTransferred').data('kendoDatePicker').enable(true);
            $('#dtLoanTransferred').data('kendoDatePicker').enable(true);
            $("#chkResponseReceived").prop("disabled", false);
            $("#chkResponseNotReceived").prop("disabled", false);
            $("#chkNotAccept").prop("disabled", false);
        }

        //this method handle the change event of date controls
        public validatePwdDate() {
            $("#dtPostWithdrwal").change(() => {
                this.dateValidator("#dtPostWithdrwal", $("#dtPostWithdrwal"));
            });

            $("#dtDeadline").change(() => {
                this.dateValidator("#dtDeadline", $("#dtDeadline"));
            });

            $("#dtResponseReceived").change(() => {
                this.dateValidator("#dtResponseReceived", $("#dtResponseReceived"));
            });

            $("#dtGrantTransferred").change(() => {
                this.dateValidator("#dtGrantTransferred", $("#dtGrantTransferred"));
            });

            $("#dtLoanTransferred").change(() => {
                this.dateValidator("#dtLoanTransferred", $("#dtLoanTransferred"));
            });

            $("#calStartDate").change(() => {
                this.dateValidator("#calStartDate", $("#calStartDate"));
            });

            $("#calEndDate").change(() => {
                this.dateValidator("#calStartDate", $("#calEndDate"));
            });

            $("#cal2CreditWithdrawalDateEnabled").change(() => {
                this.dateValidator("#cal2CreditWithdrawalDateEnabled", $("#cal2CreditWithdrawalDateEnabled"));
            });
        }

        //this method validates the date format.
        private dateValidator(componentId: string, input: any): boolean {
            MasterPage.destroyTooltip(input);
            if (MasterPage.cutomDateFormatValidation(input)) {
            }
            this.dateValidation = true;
            return this.dateValidation;
        }

        //Expanding all the panel bars for R2T4Input tab.
        public expandR2T4InputSections(): void {
            this.r2T4Panel.data("kendoPanelBar").expand($(".k-state-active"), true);
            $("#dvTitleIVAid").addClass("k-state-active");
            $("#dvPercentageOfPeriod").addClass("k-state-active");
            $("#dvInstitutionalCharges").addClass("k-state-active");
        }

        //Expanding all the panel bars for R2T4Result tab.
        public expandR2T4ResultSections(): void {
            this.r2T4PanelResult.data("kendoPanelBar").expand($(".k-state-active"), true);
            $("#dvAidInfo").addClass("k-state-active");
            $("#dvPercentagebar").addClass("k-state-active");
            $("#dvAmountEarned").addClass("k-state-active");
            $("#dvDisbursed").addClass("k-state-active");
            $("#dvAidDue").addClass("k-state-active");
            $("#dvReturnFund").addClass("k-state-active");
            $("#dvAmountUnearned").addClass("k-state-active");
            $("#dvRepayment").addClass("k-state-active");
            $("#dvGrantFund").addClass("k-state-active");
            $("#dvReturnGrant").addClass("k-state-active");
            $("#dvPostWithDrawl").addClass("k-state-active");
        }

        //this function is to update R2T4Results data when input is modified.
        public updateR2T4ResultDetails(isR2T4ResultsCompleted: boolean = false, isInputModified: boolean = false): void {
            let r2T4ResultModel = {} as iR2T4Results;
            let r2T4ResultDetail: iR2T4Results = this.generateR2T4ResultModel(r2T4ResultModel);
            Utility.isSupportUser(
                (response) => {
                    if (!!response) {
                        if ($("#ChkOverride").is(':checked')) {
                            this.saveOrUpdateOverrideResults(r2T4ResultDetail, false, () => { });
                        } else {
                            r2T4ResultDetail.isR2T4ResultsCompleted = (constants.r2T4CompletionStatus.isR2T4ResultsCompleted || isR2T4ResultsCompleted);
                            constants.r2T4CompletionStatus.isR2T4ResultsCompleted = r2T4ResultDetail.isR2T4ResultsCompleted;
                            this.saveOrUpdateR2T4Results(r2T4ResultDetail, false,true, () => { });
                        }
                    } else {
                        r2T4ResultDetail.isR2T4ResultsCompleted = (constants.r2T4CompletionStatus.isR2T4ResultsCompleted || isR2T4ResultsCompleted);
                        constants.r2T4CompletionStatus.isR2T4ResultsCompleted = r2T4ResultDetail.isR2T4ResultsCompleted;
                        this.saveOrUpdateR2T4Results(r2T4ResultDetail, false,true, () => { });
                    }
                    constants.isR2T4ResultTabDisabled = false;
                });
        }
       
    }
}