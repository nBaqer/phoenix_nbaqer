﻿/// <reference path="../../../api/request.ts" />
module API.ViewModels.AcademicRecords.R2T4Results {
    import RequestType = Api.RequestType;
    import RequestParameterType = Api.RequestParameterType;
    import Request = Api.Request;

    /**
     * This is the default class for initializing the Student Termination Page.
     * Receive in the constructor any required parameters like the campusId.
     * Declare the View Model variable and call the initialization methods as needed
     */
    export class R2T4Results {
        //This fetchR2T4Resultdetails is used fetch the R2T4 Result details for the given R2T4 input detail.
        public fetchR2T4Resultdetails(r2T4InputDetail: API.Models.IR2T4Input, onResponseCallback: (response: Response) => any) {
            const request = new Request();
            request.send(RequestType.Post, "v1/AcademicRecords/R2T4Result/GetR2T4Result", r2T4InputDetail, RequestParameterType.Body, onResponseCallback);
        }

        //This API is used for saving the R2T4 Result detail.
        public saveR2T4Results(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Post, "v1/AcademicRecords/R2T4Result/Post", data, RequestParameterType.Body, onResponseCallback);
        }
        public saveOverrideR2T4Results(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Post, "v1/AcademicRecords/R2T4Result/PostOverrideR2T4Result", data, RequestParameterType.Body, onResponseCallback);
        }

        public updateOverrideR2T4Results(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Put, "v1/AcademicRecords/R2T4Result/PutOverrideResults", data, RequestParameterType.Body, onResponseCallback);
        }

        public updateR2T4Results(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Put, "v1/AcademicRecords/R2T4Result/Put", data, RequestParameterType.Body, onResponseCallback);
        }

        public isTerminationIdExists(data: any, onResponseCallback: (response: Response) => any) {
            let request = new Request();
            request.send(RequestType.Put, "v1/AcademicRecords/R2T4Result/SaveOverrideR2T4Result", data, RequestParameterType.Body, onResponseCallback);
        }
    }
}