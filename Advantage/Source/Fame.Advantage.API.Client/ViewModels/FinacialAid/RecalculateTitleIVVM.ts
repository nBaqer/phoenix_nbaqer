﻿/// <reference path="../../API/AcademicRecords/TitleIVMulti.ts" />
/// <reference path="../../API/FinancialAid/ProgramVersion.ts" />
/// <reference path="../../Components/Common/SearchAutoComplete.ts" />
/// <reference path="../../Common/ResizeSensor.d.ts" />

module API.ViewModels.FinancialAid {
    import Constants = Components.Common;
    import DropDownList = Api.Components.DropDownList;
    import AutoComplete = Api.Components.SearchAutoComplete;
    import KendoGrid = Api.Components.Grid;
    import ISettings = API.Models.ISettings;
    import IFilterSearch = API.Models.IFilterSearch;
    import IFilterParams = API.Models.IFilterParameters;
    import TitleIVMulti = Api.TitleIVMulti;
    import TitleIV = Api.TitleIV;
    import ProgramVersion = Api.FinancialAid.ProgramVersion;
    import Student = Api.Student;
    import Campus = Api.Campus;

    export class RecalculateTitleIVVM {
        userProfileValidator: kendo.ui.Validator;
        userRolesValidator: kendo.ui.Validator;
        titleIVResultsGrid: KendoGrid;
        gridParams: IFilterParams;
        gridSearch: IFilterSearch;
        campusDropDownList: DropDownList;
        programVersionDropDownList: DropDownList;
        recalcSpecificBtn: JQuery;
        mainPane: JQuery;
        enrollments: Array<string>;
        campusId: string;
        programVersionId: string;
        studentName: string;
        studentAutoComplete: AutoComplete;
        splitter: kendo.ui.Splitter;
        recalculatePanelBar: kendo.ui.PanelBar;
        userProfilePanel: kendo.data.ObservableObject;
        canExpand: boolean;
        titleIVResults: TitleIVMulti;
        titleIV: TitleIV;
        campus: Campus;
        masterCampusId: string;
        programVersion: ProgramVersion;
        student: Student;
        emptyGuid: string = "00000000-0000-0000-0000-000000000000";
        studentStatuses: Array<Constants.SystemStatus>;
        initResize: boolean;
        constructor() {

            this.campus = new Campus();
            this.programVersion = new ProgramVersion();
            this.student = new Student();
            this.titleIVResults = new TitleIVMulti();
            this.titleIV = new TitleIV();
            this.studentStatuses = [Constants.SystemStatus.CurrentlyAttending, Constants.SystemStatus.AcademicProbation, Constants.SystemStatus.Externship];
            this.masterCampusId = $("#MasterCampusDropDown").val();
            this.enrollments = [];
            this.campusId = "";
            this.programVersionId = "";
            this.recalcSpecificBtn = $("#recalculateSpecificBtn");
            this.mainPane = $(".main-container");
        }

        public initialize() {
            var that = this;
            let gridSettings: ISettings = {} as ISettings;
            this.gridParams = {} as IFilterParams;
            this.gridSearch = {} as IFilterSearch;
            this.initResize = true;
            gridSettings.settings = {
                autoBind: false,
                columns: [{
                    field: "studentName",
                    title: "Student Name",
                    width: 180
                },
                {
                    field: "increment",
                    title: "Increment",
                    width: 50
                },
                {
                    field: "checkPointDateString",
                    title: "Check Point Date",
                    width: 100
                },
                {
                    field: "sapCheckDateString",
                    title: "Recalc Date",
                    width: 100
                },
                {
                    field: "sapStatus",
                    title: "Status",
                    width: 180
                },
                {
                    field: "reason",
                    title: "Reason",
                    width: 180
                },
                {
                    field: "active",
                    title: "Status",
                    attributes: {
                        "style": "text-align:center;"
                    },
                    template: dataItem => {
                        let status = dataItem.active;

                        if (!status) {
                            console.log("Inactive")
                            return '<span class="k-icon k-i-warning title-iv-warning" title="This increment no longer applies due to a recent change in attendance or GPA."></span>';
                        }

                        return "";
                    },
                    width: 50
                }],
                groupable: true,
                group: { field: "studentName" },
                noRecords: {
                    template: "No data is currently available."
                }

            };
            //$("#vertical").kendoSplitter({
            //    orientation: "vertical",
            //    panes: [
            //        { collapsible: true, size: "10%" },
            //        { collapsible: false, size: "80%" },
            //        { collapsible: false, resizable: false, size: "10%" }
            //    ]
            //});
            $("#horizontal").kendoSplitter({
                panes: [
                    { collapsible: true, resizable: false, min: "20%", size: "20%" },
                    { collapsible: false, resizable: false, min: "80%", size: "80%" }
                ]
            });
            that.splitter = $("#horizontal").data("kendoSplitter");
            that.splitter.bind("collapse", () => { that.recalculateGridHeight() });
            that.splitter.bind("resize", () => { that.recalculateGridHeight() });

            new ResizeSensor(that.mainPane, function () {
                that.splitter.resize();
            });

            //setTimeout(function(){that.splitter.resize()}, 10000);

            //splitter.expand("#left-pane");
            $("#recalcPanelBar").kendoPanelBar({
                expandMode: "single",
                animation: {
                    // fade-out closing items over 1000 milliseconds
                    collapse: {
                        duration: 1000,
                        effects: "fadeOut"
                    },
                    // fade-in and expand opening items over 500 milliseconds
                    expand: {
                        duration: 500,
                        effects: "expandVertical fadeIn"
                    }
                }

            });
            that.recalculatePanelBar = $("#recalcPanelBar").data("kendoPanelBar");

            that.recalculatePanelBar.enable($("#liRecalc"), false);
            //Initialzie user grid
            that.titleIVResultsGrid = new KendoGrid("titleIVResultsGrid", that.titleIVResults, undefined, undefined, gridSettings);

            //initialize dropdowns
            that.campusDropDownList = new DropDownList("ddlCampus", that.campus, undefined, {
                settings: {
                    dataValueField: "value"
                },
                dataBound: () => {
                    that.campusDropDownList.setValue(that.masterCampusId);
                }
            } as ISettings, () => {

                if (that.campusDropDownList.hasValue()) {
                    that.programVersionDropDownList.getKendoDropDown().enable(true);
                    that.programVersionDropDownList.reload(
                        { params: { campusId: that.campusDropDownList.getValue() } } as IFilterParams);
                } else {
                    that.programVersionDropDownList.getKendoDropDown().enable(false);
                }
                that.closeRecalculationPanel();
                that.campusId = that.campusDropDownList.getValue();

            });
            that.programVersionDropDownList = new DropDownList("ddlProgramVersion", that.programVersion, undefined, { settings: { dataValueField: "value", autoBind: false, enable: false } } as ISettings,
                () => {

                    if (that.programVersionDropDownList.hasValue()) {
                        this.gridParams.params = {};
                        this.gridParams.params["campusId"] = that.campusDropDownList.getValue();
                        this.gridParams.params["programVersionId"] = that.programVersionDropDownList.getValue();
                        that.titleIVResultsGrid.reload(undefined, this.gridParams);
                        that.titleIVResultsGrid.fetch();

                        that.expandRecalculationPanel();
                        that.studentAutoComplete.reload({ params: { campusId: that.campusDropDownList.getValue(), programVersionId: that.programVersionDropDownList.getValue(), studentStatuses: that.studentStatuses , showAll: true} } as IFilterParams);
                    } else {
                        that.closeRecalculationPanel();
                    }

                    that.programVersionId = that.programVersionDropDownList.getValue();
                });
            that.studentAutoComplete = new AutoComplete("acStudents", that.student, undefined, undefined,

                (response) => {
                    this.studentName = response.value.displayName;
                    if (response.value.enrollments) {
                        this.enrollments = response.value.enrollments;
                        this.recalcSpecificBtn.prop("disabled", false);
                    } else {
                        this.enrollments = [];
                    }

                },

                (e) => {
                    if (e.currentTarget.value === "") {
                        this.enrollments = [];
                        this.recalcSpecificBtn.prop("disabled", true);
                    }
                },




            );

            $("#resetBtn").on('click',
                () => {
                    if (that.campusDropDownList.hasValue()) {
                        that.campusDropDownList.reset();
                    }
                    if (that.programVersionDropDownList.hasValue()) {
                        that.programVersionDropDownList.reset();
                    }

                    that.programVersionDropDownList.getKendoDropDown().enable(false);
                    that.closeRecalculationPanel();
                    that.titleIVResultsGrid.clear();
                    this.titleIVResultsGrid.fetch();
                });

            $("#recalculateAllBtn").on('click',
                () => {
                    that.showExecuteAllConfirmation();
                });

            $("#recalculateSpecificBtn").on('click',
                () => {
                    that.showExecuteSpecificConfirmation();
                });
            $("#searchBtn").on('click',
                () => {
                    var val = $("#gridStudentTitleIVFilter").val();
                    if (this.programVersionId !== "") {
                        this.gridSearch.filter = val;
                        //reset params
                        this.gridParams.params = {};
                        this.gridParams.params["campusId"] = this.campusId;
                        this.gridParams.params["programVersionId"] = this.programVersionId;
                        this.titleIVResultsGrid.reload(this.gridSearch, this.gridParams);
                        this.titleIVResultsGrid.fetch();
                    }

                });

            $("#center-pane").keypress(e => {
                var key = e.which;
                if (key == 13)  // the enter key code
                {
                    e.preventDefault();

                    if ($("#gridStudentTitleIVFilter").is(":focus")) {
                        var val = $("#gridStudentTitleIVFilter").val();
                        if (this.programVersionId !== "") {
                            this.gridSearch.filter = val;
                            //reset params
                            this.gridParams.params = {};
                            this.gridParams.params["campusId"] = this.campusId;
                            this.gridParams.params["programVersionId"] = this.programVersionId;
                            this.titleIVResultsGrid.reload(this.gridSearch, this.gridParams);
                            that.titleIVResultsGrid.fetch();
                        }
                    }
                }
            });

            that.titleIVResultsGrid.bind("dataBound",
                () => {

                    $(".title-iv-warning").kendoTooltip({
                        width: 150,
                        position: "top"
                    });
                });

        }

        private showExecuteAllConfirmation() {
            $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(
                "Are you sure that you want to recalculate ALL Title IV SAP increments for ALL students in the selected program version?"))
                .then((confirmed) => {
                    if (confirmed) {
                        this.executeTitleIVSapCheck(true);
                    }
                });

        }
        private showExecuteSpecificConfirmation() {
            $.when(MasterPage.SHOW_CONFIRMATION_WINDOW_PROMISE(
                "Are you sure that you want to recalculate ALL Title IV SAP increments for student " + this.studentName + " ?"))
                .then((confirmed) => {
                    if (confirmed) {
                        this.executeTitleIVSapCheck(false);
                    }
                });

        }

        private executeTitleIVSapCheck(all: boolean) {

            if (this.campusId !== "" && this.programVersionId !== "") {
                let titleIVExecution = {};
                titleIVExecution["campusId"] = this.campusId;
                titleIVExecution["programVersionId"] = this.programVersionId;
                titleIVExecution["runAllIncrements"] = true;
                if (!all) {
                    titleIVExecution["allowAnyStatus"] = true;
                    titleIVExecution["studentEnrollments"] = this.enrollments;
                    this.titleIV.executeTitleIVSapCheck(titleIVExecution, (response: any) => {
                        if (response.length === 0) {
                            MasterPage.SHOW_INFO_WINDOW(
                                "Title IV SAP recalculation has been completed.");
                            this.gridParams.params = {};
                            this.gridParams.params["enrollments"] = this.enrollments;
                            this.titleIVResultsGrid.reload(undefined, this.gridParams);
                            this.titleIVResultsGrid.fetch();
                        }
                        //else { // this is for error checking can be added back later
                        //    if (!response[0].FailedMessage) {
                        //        MasterPage.SHOW_ERROR_WINDOW("One or more increments failed to execute.");
                        //    } else {
                        //        MasterPage.SHOW_ERROR_WINDOW(response[0].FailedMessage);
                        //    }
                        //}
                    }, true);

                } else {
                    this.titleIV.executeTitleIVSapCheck(titleIVExecution, () => { }, false);
                    MasterPage.SHOW_INFO_WINDOW("The Title IV SAP is being recalculated for all students with an in school status. This process will complete within a few minutes.");

                }

            } else {
                MasterPage.SHOW_ERROR_WINDOW("An unexpected error occured, please refresh the page and try again.");
            }
        }
        private expandRecalculationPanel() {
            this.recalculatePanelBar.enable($("#liRecalc"), true);
            this.recalculatePanelBar.expand($("#liRecalc"), true);
            //empty all text fields
            this.studentAutoComplete.reset();
            this.studentName = "";
            this.recalcSpecificBtn.prop("disabled", true);

        }
        private closeRecalculationPanel() {
            this.recalculatePanelBar.collapse($("#liRecalc"), true);
            this.recalculatePanelBar.enable($("#liRecalc"), false);
            //empty all text fields
            this.studentAutoComplete.reset();
            this.studentName = "";
            this.recalcSpecificBtn.prop("disabled", true);

        }

        recalculateGridHeight() {
            let fg_cont = $(".flexGrid-container");
            let fg_filter = $(".flexGrid-filter");
            let fg_grouping_header = $(".flexGrid .k-grouping-header");
            let fg_grid_header = $(".flexGrid .k-grid-header");
            let fg_grid_content = $(".flexGrid .k-grid-content");
            let fg_grid_footer = $(".flexGrid .k-grid-pager");
            let fg_grid = $(".flexGrid .k-grid");

            fg_grid.css("height", "");
            if (!fg_cont || !fg_grid_content) {
                return;
            }
            let adjustment_height = 0;
            let grid_content_height = fg_cont.innerHeight();

            //if (this.initResize) {
            //    //fg_grid_footer.innerHeight(fg_grid_footer.innerHeight() + 25);
            //    this.initResize = false;
            //}
            if (fg_filter) {
                adjustment_height += fg_filter.innerHeight();
            }
            if (fg_grouping_header) {
                adjustment_height += fg_grouping_header.innerHeight();
            }
            if (fg_grid_header) {
                adjustment_height += fg_grid_header.innerHeight();
            }
            if (fg_grid_footer) {
                adjustment_height += fg_grid_footer.innerHeight();
            }

            grid_content_height -= adjustment_height + 26;
            //if (this.initResize) {
            //    grid_content_height -= 48;
            //    this.initResize = false;
            //}
            fg_grid_content.innerHeight(grid_content_height);

        }
    }
}