﻿module API.ViewModels.FinancialAid {

    export class RecalculateTitleIV {

        recalculateTitleIVVm: RecalculateTitleIVVM;
        constructor() {
            this.recalculateTitleIVVm = new RecalculateTitleIVVM();
        }

        public initialize() {
            this.recalculateTitleIVVm.initialize();
        }
    }
}