﻿/// <reference path="../../../Fame.Advantage.Client.MasterPage/AdvantageMessageBoxs.ts" />
///// <reference path="../../Fame.Advantage.API.Client/Common/ResizeSensor.d.ts" />
///// <reference path="../../AdvWeb/Scripts/Fame.Advantage.API.Client.d.ts" />
///// <reference path="../../Fame.Advantage.Client/Dashboard1/Grid.d.ts" />
/// <reference path="../../API/Common/Models/ISettings.ts" />
module Api.ViewModels.Widgets {
    import ISettings = API.Models.ISettings;
    declare var storageCache;
    declare var XMASTER_GET_BASE_URL: string;

    export class StudentsThatMetProgramLengthWidget {
        campusId: string;

        /**
         * Students Ready for Graduation Widget.
         */
        studentsReadyForGraduationGrid: Api.Components.Grid;
        studentsReadyForGraduationWidget: kendo.data.ObservableObject;
        apiStudentsReadyForGraduation: Api.AcademicRecords.StudentsThatHaveMetProgramLength;
        studentsReadyForGraduationGridSettings: ISettings;
        emptyGuid: string = "00000000-0000-0000-0000-000000000000";
        hardLoad = false;

    

        constructor(campusId: string) {
            this.campusId = campusId;
        }

        public refreshGraduationGrid() {
            let that = this;
            let ddlInSchoolStatuses = $('#ddlInSchoolStatuses').data("kendoDropDownList");
            let schoolStatusValue = ddlInSchoolStatuses.value();

            let ddlHaveMet = $('#ddlHaveMet').data("kendoDropDownList");
            let haveMetValue = ddlHaveMet.value();


            that.studentsReadyForGraduationGrid.setParameters({
                params: { "campusId": that.campusId, "filterByStatus": schoolStatusValue, "haveMetOption": haveMetValue,  pageSize: 20, page: 1 }
            });
            that.studentsReadyForGraduationGrid.refresh();
        }
        public initialize(widgetSettings) {
            let that = this;
            this.apiStudentsReadyForGraduation = new Api.AcademicRecords.StudentsThatHaveMetProgramLength();
            this.studentsReadyForGraduationGridSettings = {} as ISettings;

            that.setStudentsReadyForGraduationSetting(that);

            var $widget = $("#studentsReadyForGraduationWidget");

            that.studentsReadyForGraduationGrid = new Api.Components.Grid("studentsReadyForGraduationGrid",
                that.apiStudentsReadyForGraduation,
                undefined,
                { params: { "campusId": that.campusId, "filterByStatus": that.getGraduationStatusFilter(that), "haveMetOption": that.getHavemetOption(that),pageSize: 20, page: 1, hardLoad: that.hardLoad } },
                that.studentsReadyForGraduationGridSettings,
                () => { /* On Change*/
                    /*Do nothing*/
                });

            $('#gradRefreshBtn').on('click', function () {

                that.hardLoad = true;
                that.studentsReadyForGraduationGrid.setParameters({ params: { "campusId": that.campusId, "filterByStatus": that.getGraduationStatusFilter(that), "haveMetOption": that.getHavemetOption(that), pageSize: 20, page: 1, hardLoad: that.hardLoad } });
                that.studentsReadyForGraduationGrid.refresh();

                that.hardLoad = false;
            });


            $('#ddlHaveMet').kendoDropDownList({
                dataSource: that.apiStudentsReadyForGraduation.getHaveMetOptions(() => {
                }),
                
                dataTextField: "text",
                dataValueField: "value",
                change: (data) => {
                    that.refreshGraduationGrid();
                },
                dataBound: function (dd) {
                    let value = dd.sender.dataSource.data().filter(function(option: any) {
                        return option.selected === true;
                    })[0].value;
                    if (value > 0) {
                    this.select(value-1);

                    }
                }
            });
            

            $('#ddlGraduationStatuses').kendoDropDownList({
                dataSource: that.apiStudentsReadyForGraduation.getGraduationStatuses(() => {
                    return { "campusId": that.campusId }
                }),
                dataTextField: "text",
                dataValueField: "value",
                change: (data) => {
                    let ddlGraduationStatuses = $('#ddlGraduationStatuses').data("kendoDropDownList");
                    let value = ddlGraduationStatuses.value();
                    let dropdowns = $('.graduationStatusDropdownTemplate');
                    for (let index = 0; index <= dropdowns.length - 1; index++) {
                        let ddlStatusChange =
                            $('[data-gradStatusStudentEnrollmentId="' +
                                $(dropdowns[index]).attr("data-gradStatusStudentEnrollmentId") +
                                '"]').data("kendoDropDownList");
                        if (ddlStatusChange !== undefined) {
                            ddlStatusChange.value(value);
                        }
                    }
                }
            });

            $('#ddlInSchoolStatuses').kendoDropDownList({
                dataSource: that.apiStudentsReadyForGraduation.getStatusesToFilter(() => {
                    return { "campusId": that.campusId }
                }),
                dataTextField: "text",
                dataValueField: "value",
                change: (e) => {
                    that.refreshGraduationGrid();
                }
            });

            $("#studentsReadyForGraduationGrid").data("kendoGrid").bind("dataBound", (e) => {
                var grid = e.sender;
                var items = e.sender.items();
                if (e.sender.dataSource.total() <= e.sender.dataSource.pageSize() || e.sender.dataSource.pageSize() === null) {
                    e.sender.pager.element.hide();
                } else {
                    e.sender.pager.element.show();
                }

                items.each(function (e) {
                    var dataItem = grid.dataItem(this);
                    var ddt = $(this).find('.graduationStatusDropdownTemplate');
                    $(ddt).attr("data-StatusChangeEnrollment", dataItem.studentEnrollmentId);

                    $(ddt).kendoDropDownList({
                        value: dataItem.value,
                        dataSource: that.apiStudentsReadyForGraduation.getGraduationStatuses(() => {
                            return { "campusId": that.campusId }
                        }),
                        dataTextField: "text",
                        dataValueField: "value"
                    });
                });
            });

            that.studentsReadyForGraduationGrid.bind("dataBound", () => {
                let noticeCount = that.studentsReadyForGraduationGrid.count();
                var template = kendo.template('<span class="k-label" id="studentsReadyForGraduationCountLabel">Pending:#=noticeCount#</span>');
                var data = { noticeCount: noticeCount }; //A value in JavaScript/JSON
                var result = template(data); //Pass the data to the compiled template
                let noticeCountLabel = $("#studentsReadyForGraduationCountLabel");
                if (noticeCountLabel.length) {
                    noticeCountLabel.remove();
                }
                $("#studentsReadyForGraduationHeader").append(result); //display the result

                if (noticeCount > 0) {
                    that.studentsReadyForGraduationWidget.set("canGrad", true);
                }
                else {
                    that.studentsReadyForGraduationWidget.set("canGrad", false);
                }
                $(".k-grid-content").height(140);


            });


            that.studentsReadyForGraduationWidget = kendo.observable({
                canGrad: false,
                changeStatusSelectedStudents: function () {
                    let selectedItems = that.studentsReadyForGraduationGrid.getSelectedKeyNames();
                    if (selectedItems === undefined || selectedItems === null || selectedItems.length === 0) {
                        MasterPage.SHOW_WARNING_WINDOW("Select at least one student to change status");
                        return;
                    }
                    let statusToChange: Array<API.Models.IListItem> = new Array<API.Models.IListItem>();
                    $(selectedItems).each((index, value) => {
                        let statusChangeDropDown = $('[data-StatusChangeEnrollment="' + value + '"]').data("kendoDropDownList");
                        if (statusChangeDropDown !== undefined && statusChangeDropDown !== null) {
                            if (statusChangeDropDown.value() !== that.emptyGuid) {
                                let item: API.Models.IListItem = {} as API.Models.IListItem;
                                item.value = value as any;
                                item.text = statusChangeDropDown.value();
                                statusToChange.push(item);
                                //statusToChange.push(JSON.parse('{ "' + value + '": "' + statusChangeDropDown.value() + '"}'));
                            }
                        }
                    });

                    if (statusToChange.length === 0) {
                        MasterPage.SHOW_WARNING_WINDOW("Select at least one student and status to change");
                        return;
                    }

                    that.apiStudentsReadyForGraduation.changeStatus(
                        { "campusId": that.campusId, "enrollmentsToUpdate": statusToChange }, (response:any) => {
                            if (response.result) {
                                MasterPage.SHOW_INFO_WINDOW_PROMISE("The statuses have been updated for the selected students!").done(() => {
                                    that.hardLoadAfterChange(that)
                                    that.studentsReadyForGraduationGrid.getKendoGridObject().clearSelection();
                                });
                            } else {
                                MasterPage.SHOW_ERROR_WINDOW("One or more error(s) occurred when changing statuses for the selected student(s): <br>" + response.resultStatusMessage.replace("\n","<br>"));
                            }
                            // 
                        });
                }
            });
            kendo.bind($widget, that.studentsReadyForGraduationWidget);
            $widget.show(); // must be replaced to inject template html onto DOM instead of showing and hiding widget
        }

        setStudentsReadyForGraduationSetting(that) {
            that.studentsReadyForGraduationGridSettings.settings = {
                height: "100%",
                resizable: true,

                columns: [
                    {
                        field: "name",
                        title: "Name",
                        template: that.getStudentSummaryLink,
                        width: 180
                    },
                    {
                        field: "status",
                        title: "Status",
                        width: 60
                    },

                    {
                        field: "missingGrades",
                        title: "Missing Grades",
                        width: 50
                    },

                    {
                        field: "ledgerBalance",
                        title: "Ledger Balance",
                        width: 50
                    },
                    { field: "graduationStatus", title: "New Status", width: "100px", template: that.graduationStatusDropDownEditor },
                    {
                        selectable: true,
                        headerTemplate: kendo.template('# var checkboxGuid = kendo.guid(); #' + '<input class="k-checkbox" data-role="checkbox" aria-label="Select all rows" aria-checked="false" type="checkbox" id="#= checkboxGuid #">' + '<label for="#= checkboxGuid #" class="k-checkbox-label k-no-text">##&\\#8203;##</label>')
                        , width: "30px"
                    }
                ],
                persistSelection: true,

                dataSource: {
                    schema: {
                        model: { id: "studentEnrollmentId" }
                    }
                }
            };
        }
        getStudentSummaryLink(dataItem) {
            return '<a href="javascript:void(0);" onclick="javascript:location.href = \'' + XMASTER_GET_BASE_URL + "PL/StudentMasterSummary.aspx?resid=868&mod=AR&cmpid=" + dataItem.campusId + "&desc=View Student&entityType=0&entity=" + dataItem.studentId + '\';">' + dataItem.name + '</a>';
        }
        graduationStatusDropDownEditor(dataItem) {
            return '<input required class="graduationStatusDropdownTemplate" style="width:125px" data-gradStatusStudentEnrollmentId="' + dataItem.studentEnrollmentId + '"/>';
        }
        getGraduationStatusFilter(that) {
            let ddlInSchoolStatuses = $('#ddlInSchoolStatuses').data("kendoDropDownList");
            if (ddlInSchoolStatuses !== undefined) {
                if (ddlInSchoolStatuses.value() !== undefined && ddlInSchoolStatuses.value() !== null) {
                    return ddlInSchoolStatuses.value();
                }
            }

            return that.emptyGuid;
        }

        getHavemetOption(that) {
            let ddlHaveMetOptions = $('#ddlHaveMet').data("kendoDropDownList");
            if (ddlHaveMetOptions !== undefined) {
                if (ddlHaveMetOptions.value() !== undefined && ddlHaveMetOptions.value() !== null) {
                    return ddlHaveMetOptions.value();
                }
            }

            return 0;
        }

        hardLoadAfterChange(that) {
            that.hardLoad = true;
            that.studentsReadyForGraduationGrid.setParameters({ params: { "campusId": that.campusId, "filterByStatus": that.getGraduationStatusFilter(that),"haveMetOption": that.getHaveMetOptions(that), pageSize: 20, page: 1, hardLoad: that.hardLoad } });
            that.studentsReadyForGraduationGrid.refresh();
            that.hardLoad = false;
        }

    }
}