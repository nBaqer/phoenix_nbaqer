﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AfaSession.cs" company="FAME Inc">
//   2018
// </copyright>
// <summary>
//   The AFA session.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.AFA.Api.Library.AFA
{
    /// <summary>
    /// The AFA session.
    /// </summary>
    public class AfaSession
    {
        /// <summary>
        /// Gets or sets the client key. This is the ClientKey which is used only for the 1st call to get the sessionKey
        /// </summary>
        public string ClientKey { get; set; }

        /// <summary>
        /// Gets or sets the user name. This is also used only for first call to get the sessionKey
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password. This is also used only for first call to get the sessionKey
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the session key.
        /// </summary>
        public string SessionKey { get; set; }

        /// <summary>
        /// Gets or sets the id user.
        /// </summary>
        public long IdUser { get; set; }
    }
}
