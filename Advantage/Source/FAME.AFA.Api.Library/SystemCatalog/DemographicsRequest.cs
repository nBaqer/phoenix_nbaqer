﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DemograpicsRequest.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the DemographicsRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Diagnostics;
using Fame.Integration.Adv.Afa.Messages.WebApi.Entities;

namespace FAME.AFA.Api.Library.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    using Newtonsoft.Json;

    /// <summary>
    /// The demographics request.
    /// </summary>
    public class DemographicsRequest : Request
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="DemographicsRequest"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public DemographicsRequest(string clientKey, string userName, string password, string apiUrl)
            : base(clientKey, userName, password, apiUrl)
        {
        }


        /// <summary>
        /// The get demographics for the first time (ideally should be only time call).
        /// </summary>
        /// <returns>
        /// The JSON list of the leads demographics in AFA.
        /// </returns>
        public List<AdvStagingDemographicsV1> GetDemographics(string cmsId)
        {
         
            // CONTENT-TYPE header
            var uri = new Uri(this.GetUrl() + "/integration/sis/demographic/v1/get/" + cmsId.ToString());

            List<AdvStagingDemographicsV1> leads = new List<AdvStagingDemographicsV1>();
            HttpResponseMessage response;

            // Do a get call to AFA Service............
            var task = Client.GetAsync(uri).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
                Debug.WriteLine($"Function: GetAfaDemographics - Code: {response.StatusCode} - Reason: {response.ReasonPhrase} - StatusSuccessCode:{response.IsSuccessStatusCode}");
                if (response.IsSuccessStatusCode)
                {
                    var leadjson = response.Content.ReadAsStringAsync().Result;
                    leads = JsonConvert.DeserializeObject<List<AdvStagingDemographicsV1>>(leadjson);
                }

                Debug.WriteLine($"info: Initial Lead Demographics - success:{leads.Count} leads were found. ");
            });

            task.Wait();

            return leads;
        }

    }
}
