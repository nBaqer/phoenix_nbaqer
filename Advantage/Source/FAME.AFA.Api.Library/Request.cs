﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using FAME.AFA.Api.Library.AFA;
using Newtonsoft.Json;
namespace FAME.AFA.Api.Library
{
    public class Request : RequestBase
    {
        private AfaSession Payload;
        private bool _isAuthenticated;
        public bool IsAuthenticated
        {
            get => _isAuthenticated;
            set => _isAuthenticated = value;
        }
        public Request(string clientKey, string userName, string passWord, string url)
            : base(url)
        {
            this.Payload = new AfaSession
            {
                ClientKey = clientKey,
                UserName = userName,
                Password = passWord
            };
            this.Authenticate();
        }
        public Request(AfaSession payload, string url)
        : base(url)
        {
            this.Payload = payload;
            this.Authenticate();
        }
        public void Authenticate()
        {
            // Create the headers
            base.Client.DefaultRequestHeaders.Add("ClientKey", this.Payload.ClientKey);

            var uri = new Uri(base.GetUrl() + "/users/userlogin");
            HttpResponseMessage response;
            AfaSession payload = new AfaSession();

            try
            {
                var task = base.Client.PostAsJsonAsync(uri, this.Payload).ContinueWith(taskwithmsg =>
                {
                    payload = JsonConvert.DeserializeObject<AfaSession>(
                        taskwithmsg.Result.Content.ReadAsStringAsync().Result.ToString());
                    if ((int)taskwithmsg.Result.StatusCode == 200)
                    {
                        payload.ClientKey = this.Payload.ClientKey;
                        payload.UserName = this.Payload.UserName; // "famedev";
                        payload.Password = this.Payload.Password; // "pass";
                        response = taskwithmsg.Result;
                        Debug.WriteLine($"Code: {response.StatusCode} - Reason: {response.ReasonPhrase}");
                    }
                });
                task.Wait();
            }
            // Post to Service............

            catch (HttpRequestException e)
            {
                this.IsAuthenticated = false;
            }
            if (payload?.SessionKey != null)
            {
                this.IsAuthenticated = true;
                this.SetSessionKey(payload.SessionKey);
                this.Payload = payload;
            }

        }

    }
}
