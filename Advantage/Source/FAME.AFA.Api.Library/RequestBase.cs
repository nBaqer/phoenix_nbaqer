﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Request.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   The request.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.AFA.Api.Library
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;

    /// <summary>
    /// The request.
    /// </summary>
    public class RequestBase
    {
        /// <summary>
        /// The api url.
        /// </summary>
        private string apiUrl;

        public string ApiUrl => apiUrl;

        /// <summary>
        /// The sessionKey.
        /// </summary>
        private string sessionKey;

        /// <summary>
        /// Initializes a new instance of the <see cref="RequestBase"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        /// <param name="sessionKey">
        /// The sessionKey.
        /// </param>
        public RequestBase(string apiUrl, string sessionKey = null)
        {
            this.apiUrl = apiUrl;
            this.Client = new HttpClient();
            if (sessionKey != null)
            {
                this.sessionKey = sessionKey;
                this.Client.DefaultRequestHeaders.Add("SessionKey", this.sessionKey);
            }
            this.Client.DefaultRequestHeaders.Add("Accept", "application/json");
        }

        public void SetSessionKey(string sessKey)
        {
            this.sessionKey = sessKey;
            this.Client.DefaultRequestHeaders.Remove("SessionKey");
            this.Client.DefaultRequestHeaders.Add("SessionKey", this.sessionKey);

        }

        public string GetUrl()
        {
            return this.apiUrl;
        }
        /// <summary>
        /// Gets the client.
        /// </summary>
        public HttpClient Client { get; }
    }

}
