﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ProgramVersionRequest.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the ProgramVersionRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Fame.Integration.Adv.Afa.Messages.WebApi.Entities;

namespace FAME.AFA.Api.Library.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    using Newtonsoft.Json;

    /// <summary>
    /// The program version.
    /// </summary>
    public class ProgramVersionRequest : Request
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="ProgramVersionRequest"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public ProgramVersionRequest(string clientKey, string userName, string password, string apiUrl)
            : base(clientKey, userName, password, apiUrl)
        {
        }

        /// <summary>
        /// The send new program version to AFA Api.
        /// </summary>
        /// <param name="programVersions">
        /// The list of program versions to post for each campus that has AFA Integration enabled.
        /// </param>

        /// <returns>
        /// The <see cref="bool"/>.
        /// True if all records were posted successfully, false if at least one record failed to post.
        /// </returns>
        public bool SendNewProgramVersion(IEnumerable<AdvStagingProgramV1> programVersions)
        {
            if (!this.IsAuthenticated) return false;
            var route = this.GetUrl() + "/integration/sis//program/v1/post";
            var result = true;
            foreach (var pv in programVersions)
            {
                var response = Task.Run(() => this.Client.PostAsJsonAsync(route, pv)).Result;
                if (!response.IsSuccessStatusCode)
                {
                    result = false;
                }
            }

            return result;

        }


        /// <summary>
        /// The send updated program version to AFA Api.
        /// </summary>
        /// <param name="programVersions">
        /// The list of program versions to put for each campus that has AFA Integration enabled.
        /// </param>

        /// <returns>
        /// The <see cref="bool"/>.
        /// True if all records were updated successfully, false if at least one record failed to updated.
        /// </returns>
        public bool SendUpdatedProgramVersion(IEnumerable<AdvStagingProgramV1> programVersions)
        {
            if (!this.IsAuthenticated) return false;
            var route = this.GetUrl() + "/integration/sis//program/v1/post";
            var result = true;
            foreach (var pv in programVersions)
            {
                var response = Task.Run(() => this.Client.PostAsJsonAsync(route, pv)).Result;
                if (!response.IsSuccessStatusCode)
                {
                    result = false;
                }
            }

            return result;

        }

    }
}
