﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="PaymentPeriodRequest.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the PaymentPeriodRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace FAME.AFA.Api.Library.AcademicRecords
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using System.Diagnostics;
    using Fame.Integration.Adv.Afa.Messages.WebApi.Entities;
    using Newtonsoft.Json;

    /// <summary>
    /// The payment period request.
    /// </summary>
    public class PaymentPeriodRequest : Request
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="PaymentPeriodRequest"/> class.
        /// </summary>
        /// <param name="clientKey">
        /// The client key.
        /// </param>
        /// <param name="userName">
        /// The username.
        /// </param>
        /// <param name="password">
        /// The password.
        /// </param>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        public PaymentPeriodRequest(string clientKey, string userName, string password, string apiUrl)
            : base(clientKey, userName, password, apiUrl)
        {
        }

        /// <summary>
        /// The get payment periods.
        /// </summary>
        /// <returns>
        /// The JSON list of the payment periods in AFA.
        /// </returns>
        public List<AdvStagingPaymentPeriodV1> GetPaymentPeriods(string cmsId)
        {
            if (!this.IsAuthenticated) return new List<AdvStagingPaymentPeriodV1>();

            // CONTENT-TYPE header
            var uri = new Uri(this.ApiUrl + "/integration/sis/PaymentPeriod/v1/get/" + cmsId.ToString());

            List <AdvStagingPaymentPeriodV1> paymentPeriods = new List<AdvStagingPaymentPeriodV1>();
            HttpResponseMessage response;

            // Do a get call to AFA Service............
            var task = Client.GetAsync(uri).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
                if (response.IsSuccessStatusCode)
                {
                    var paymentPeriodsJson = response.Content.ReadAsStringAsync().Result;
                    paymentPeriods = JsonConvert.DeserializeObject<List<AdvStagingPaymentPeriodV1>>(paymentPeriodsJson);
                }

            });

            task.Wait();

            return paymentPeriods;
        }
        /// <summary>
        /// The post payment period attendance update.
        /// </summary>
        /// <returns>
        /// Boolean indicating if call was successful or not.
        /// </returns>
        public bool PostPaymentPeriodAttendanceUpdate(AdvStagingAttendanceV1 paymentPeriodUpdate)
        {
            if (!this.IsAuthenticated) return false;

            //route
            var route = new Uri(this.ApiUrl + "/integration/sis/attendanceupdate/v1/post");

            var response =  Task.Run(() => this.Client.PostAsJsonAsync(route, paymentPeriodUpdate)).Result;
            if (response.IsSuccessStatusCode)
            {
                var responseContent =  Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                if (!string.IsNullOrEmpty(responseContent))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
