﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Fame.Integration.Adv.Afa.Messages.WebApi.Entities;

namespace FAME.AFA.Api.Library.AcademicRecords
{
    public class EnrollmentRequest : Request
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="EnrollmentRequest"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public EnrollmentRequest(string clientKey, string userName, string password, string apiUrl)
            : base(clientKey, userName, password, apiUrl)
        {
        }


        /// <summary>
        /// The send new enrollment to AFA Api.
        /// </summary>
        /// <param name="enrollments">
        /// The list of enrollments to post for each campus that has AFA Integration enabled.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// True if all records were posted successfully, false if at least one record failed to post.
        /// </returns>
        public bool SendEnrollmentToAFA(IEnumerable<AdvStagingEnrollmentV3> enrollments)
        {
            if (!this.IsAuthenticated) return false;
            var route = this.GetUrl() + "/integration/sis//enrollment/v3/post";
            var result = true;

            var response = Task.Run(() => this.Client.PostAsJsonAsync(route, enrollments)).Result;
            if (!response.IsSuccessStatusCode)
            {
                result = false;
            }

            return result;
        }
    }
}
