﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StudentAwardRequest.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the StudentAwardRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Fame.Integration.Adv.Afa.Messages.WebApi.Entities;

namespace FAME.AFA.Api.Library.FinancialAid
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    using Newtonsoft.Json;

    /// <summary>
    /// The student award request.
    /// </summary>
    public class StudentAwardRequest : Request
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="StudentAwardRequest"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public StudentAwardRequest(string clientKey, string userName, string password, string apiUrl)
            : base(clientKey, userName, password, apiUrl)
        {
        }


        /// <summary>
        /// The get title IV awards.
        /// </summary>
        /// <returns>
        /// The JSON list of the title IV awards in AFA.
        /// </returns>
        public List<AdvStagingAwardV1> GetTitleIVAwards(string cmsId)
        {
            // CONTENT-TYPE header
            var uri = new Uri(this.ApiUrl + "/integration/sis/award/v1/get/" + cmsId.Trim());

            List<AdvStagingAwardV1> awards = new List<AdvStagingAwardV1>();
            HttpResponseMessage response;

            // Do a get call to AFA Service............
            var task = Client.GetAsync(uri).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
                if (response.IsSuccessStatusCode)
                {
                    var awardsJson = response.Content.ReadAsStringAsync().Result;
                    awards = JsonConvert.DeserializeObject<List<AdvStagingAwardV1>>(awardsJson);
                }

            });

            task.Wait();

            return awards;
        }
        /// <summary>
        /// The get title IV award disbursements.
        /// </summary>
        /// <returns>
        /// The JSON list of the title IV award disbursements in AFA.
        /// </returns>
        public List<AdvStagingDisbursementV1> GetTitleIVAwardDisbursements(string cmsId)
        {
            // CONTENT-TYPE header
            var uri = new Uri(this.ApiUrl + "/integration/sis/disbursement/v1/get/"+ cmsId.ToString());

            List<AdvStagingDisbursementV1> disbursements = new List<AdvStagingDisbursementV1>();
            HttpResponseMessage response;

            // Do a get call to AFA Service............
            var task = Client.GetAsync(uri).ContinueWith(taskwithmsg =>
            {
                response = taskwithmsg.Result;
                if (response.IsSuccessStatusCode)
                {
                    var disbursementsJson = response.Content.ReadAsStringAsync().Result;
                    disbursements = JsonConvert.DeserializeObject<List<AdvStagingDisbursementV1>>(disbursementsJson);
                }

            });

            task.Wait();

            return disbursements;
        }
    }
}
