﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DisbursementRequest.cs" company="FAME Inc.">
//   FAME Inc. 2018
// </copyright>
// <summary>
//   Defines the DisbursementRequest type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using Fame.Integration.Adv.Afa.Messages.WebApi.Entities;

namespace FAME.AFA.Api.Library.FinancialAid
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;

    using Newtonsoft.Json;

    /// <summary>
    /// The award disbursement request.
    /// </summary>
    public class DisbursementRequest : Request
    {

        /// <summary>
        /// Initializes a new instance of the <see cref="DisbursementRequest"/> class.
        /// </summary>
        /// <param name="apiUrl">
        /// The api url.
        /// </param>
        /// <param name="token">
        /// The token.
        /// </param>
        public DisbursementRequest(string clientKey, string userName, string password, string apiUrl)
            : base(clientKey, userName, password, apiUrl)
        {
        }

        /// <summary>
        /// The send disbursement adjustments to AFA Api.
        /// </summary>
        /// <param name="disbursements">
        /// The list of disbursements with adjustments to send to AFA.
        /// </param>

        /// <returns>
        /// The <see cref="bool"/>.
        /// True if all records were updated successfully, false if at least one record failed to updated.
        /// </returns>
        public async Task<bool> PostDisbursementAdjustment(IEnumerable<AdvStagingRefundV1> disbursements)
        {
            if (!this.IsAuthenticated) return false;
            var route = this.ApiUrl + "/integration/sis/refund/v1/post";
            var result = true;
            foreach (var dis in disbursements)
            {
                var response = await this.Client.PostAsJsonAsync(route, dis);
                if (!response.IsSuccessStatusCode)
                {
                    result = false;
                }
            }

            return result;

        }

    }
}
