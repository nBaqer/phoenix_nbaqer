﻿
Namespace Info
    <Serializable()>
    Public Class ParameterDetailItemInfo
        Public Enum ItemValueType
            [String] = 0
            [Integer] = 1
            [Boolean] = 3
            [Long] = 4
            [Decimal] = 5
            [Date] = 6
            [Guid] = 7
            [Xml] = 8
        End Enum
        Private _ItemId As Long
        Private _ItemName As String
        Private _ControllerClass As String
        Private _DetailId As Long
        Private _SetId As Long
        Private _SetName As String
        Private _SectionType As ParameterSectionInfo.SecType
        Private _PageId As Long
        Private _PageName As String
        Private _CaptionOverride As String
        Private _ItemSeq As Integer
        Private _ReturnValueName As String
        Private _ValueProp As ItemValueType

        Public Property ItemId() As Long
            Get
                Return _ItemId
            End Get
            Set(ByVal Value As Long)
                _ItemId = Value
            End Set
        End Property
        Public Property ItemName() As String
            Get
                Return _ItemName
            End Get
            Set(ByVal Value As String)
                _ItemName = Value
            End Set
        End Property
        Public Property ControllerClass() As String
            Get
                Return _ControllerClass
            End Get
            Set(ByVal Value As String)
                _ControllerClass = Value
            End Set
        End Property
        Public Property DetailId() As Long
            Get
                Return _DetailId
            End Get
            Set(ByVal Value As Long)
                _DetailId = Value
            End Set
        End Property
        Public Property SetId() As Long
            Get
                Return _SetId
            End Get
            Set(ByVal Value As Long)
                _SetId = Value
            End Set
        End Property
        Public Property SetName() As String
            Get
                Return _SetName
            End Get
            Set(ByVal Value As String)
                _SetName = Value
            End Set
        End Property
        Public Property SectionType() As ParameterSectionInfo.SecType
            Get
                Return _SectionType
            End Get
            Set(ByVal Value As ParameterSectionInfo.SecType)
                _SectionType = Value
            End Set
        End Property
        Public Property PageId() As Long
            Get
                Return _PageId
            End Get
            Set(ByVal Value As Long)
                _PageId = Value
            End Set
        End Property
        Public Property PageName() As String
            Get
                Return _PageName
            End Get
            Set(ByVal Value As String)
                _PageName = Value
            End Set
        End Property
        Public Property CaptionOverride() As String
            Get
                Return _CaptionOverride
            End Get
            Set(ByVal Value As String)
                _CaptionOverride = Value
            End Set
        End Property
        Public Property ItemSeq() As Integer
            Get
                Return _ItemSeq
            End Get
            Set(ByVal Value As Integer)
                _ItemSeq = Value
            End Set
        End Property
        Public Property ValueProp() As ItemValueType
            Get
                Return _ValueProp
            End Get
            Set(ByVal Value As ItemValueType)
                _ValueProp = Value
            End Set
        End Property
        Public Property ReturnValueName() As String
            Get
                Return _ReturnValueName
            End Get
            Set(ByVal Value As String)
                _ReturnValueName = Value
            End Set
        End Property

        Public Property ParameterItemPropertyCollection As List(Of ParameterItemPropertyInfo)

        Public Property ParameterDetailPropertyCollection As List(Of ParameterDetailPropertyInfo)
    End Class
End Namespace
