﻿Imports System
Imports System.Xml.Serialization
Namespace Info
    <Serializable()> _
    Public Class SortItemInfo

        Public _FieldName As String
        Public _SetId As Integer
        Public _SortSeq As Integer
        Public _DisplayName As String
        Public _IsDecending As Boolean

        Public Sub New()

        End Sub

        Public Sub New(ByVal FieldName As String, ByVal SetId As Integer, _
                       ByVal SortSeq As Integer, ByVal DisplayName As String, _
                       ByVal IsDecending As Boolean)

            _FieldName = FieldName
            _SetId = SetId
            _SortSeq = SortSeq
            _DisplayName = DisplayName
            _IsDecending = IsDecending
        End Sub
        <XmlElement(ElementName:="FieldName")> _
        Public Property FieldName() As String
            Get
                Return _FieldName
            End Get
            Set(ByVal Value As String)
                _FieldName = Value
            End Set
        End Property
        <XmlElement(ElementName:="SetId")> _
        Public Property SetId() As Integer
            Get
                Return _SetId
            End Get
            Set(ByVal Value As Integer)
                _SetId = Value
            End Set
        End Property
        <XmlElement(ElementName:="SortSeq")> _
        Public Property SortSeq() As Integer
            Get
                Return _SortSeq
            End Get
            Set(ByVal Value As Integer)
                _SortSeq = Value
            End Set
        End Property
        <XmlElement(ElementName:="DisplayName")> _
        Public Property DisplayName() As String
            Get
                Return _DisplayName
            End Get
            Set(ByVal Value As String)
                _DisplayName = Value
            End Set
        End Property
        <XmlElement(ElementName:="IsDecending")> _
        Public Property IsDecending() As Boolean
            Get
                Return _IsDecending
            End Get
            Set(ByVal Value As Boolean)
                _IsDecending = Value
            End Set
        End Property

    End Class
End Namespace
