﻿
Imports FAME.Parameters.Info

Namespace Interfaces
    Public Interface IParamItemControl
        Property ItemDetail() As ParameterDetailItemInfo
        Property Caption() As String
        Property SqlConn() As String

    End Interface
End Namespace
