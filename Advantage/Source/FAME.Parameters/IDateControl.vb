﻿

Namespace Interfaces
    Public Interface IDateControl
        Inherits IFilterControl
        Enum ModeType
            [Single] = 0
            Range = 1
            [Default] = 2
        End Enum
        Enum DefaultDateType
            None = 0
            OnCalendar1 = 1
            OnCalendar2 = 2
            OnBoth = 3
            AwardYear = 4
        End Enum
        Property Mode() As ModeType
        Property ShowDefaultDate() As DefaultDateType
    End Interface
End Namespace
