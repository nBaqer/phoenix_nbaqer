﻿
Imports FAME.Parameters.Info

Namespace Interfaces
    Public Interface ISortControl
        Inherits IParamItemControl

        Property SortOptions() As List(Of SortItemInfo)
        Property SavedSettings() As List(Of SortItemInfo)
        Function GetControlSettings() As List(Of SortItemInfo)
        Sub LoadSavedReportSettings()
    End Interface
End Namespace