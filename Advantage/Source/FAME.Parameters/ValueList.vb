﻿Namespace Collections
    <Serializable()>
    Public MustInherit Class ValueList
        Inherits System.Collections.CollectionBase
        Public Enum ValueType
            [String] = 0
            [Integer] = 1
            [Boolean] = 3
            [Long] = 4
            [Decimal] = 5
            [Date] = 6
            [Guid] = 7
            [Xml] = 8
        End Enum
        Private _ValueName As String
        Private _DataType As ValueType
        Public Property ValueName() As String
            Get
                Return _ValueName
            End Get
            Set(ByVal Value As String)
                _ValueName = Value
            End Set
        End Property
        Public Property DataType() As ValueType
            Get
                Return _DataType
            End Get
            Set(ByVal Value As ValueType)
                _DataType = Value
            End Set
        End Property
    End Class
End Namespace
