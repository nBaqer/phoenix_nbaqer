﻿

Namespace Interfaces
    Public Interface ISearchControl
        Inherits IFilterControl
        Enum ModeType
            [UnFiltered] = 0
            [Filtered] = 1
        End Enum
        Property DAClass() As String
        Property DAMethod() As String
        Property BindingTextField() As String
        Property BindingValueField() As String
        Property AssemblyFilePathDA() As String
        Property AssemblyDA() As System.Reflection.Assembly
        Property Caption2() As String
        Property Caption3() As String
        Property Mode() As ModeType
    End Interface
End Namespace
