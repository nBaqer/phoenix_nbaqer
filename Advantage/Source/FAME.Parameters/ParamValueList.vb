﻿Namespace Collections
    <Serializable()>
    Public Class ParamValueList(Of t)
        Inherits ValueList
        Public Sub Add(ByVal value As t)
            List.Add(value)
        End Sub
        Public Sub AddWithConvert(ByVal value As Object)
            List.Add(Convert(value))
        End Sub
        Public Sub Remove(ByVal index As Integer)
            If index > Count - 1 Or index < 0 Then
                Throw New Exception("index error: item not found")
            Else
                List.RemoveAt(index)
            End If
        End Sub
        Private Function Convert(ByVal value As Object) As t
            Return CType(value, t)
        End Function
        'Public Function Contains(ByVal value As String) As Boolean
        '    Return List.Contains(value)
        'End Function
        Default Public ReadOnly Property Item(ByVal index As Integer) As t
            Get
                Return CType(List.Item(index), t)
            End Get
        End Property
    End Class
End Namespace
