﻿Imports System
Imports System.Xml.Serialization
Namespace Info
    <Serializable()> _
    Public Class ParamItemUserSettingsInfo
        Private _ItemName As String
        Private _ItemId As Long
        Private _DetailId As Long
        Private _FriendlyName As String
        Private _ControlSettingsCollection As List(Of ControlSettingInfo)
        <XmlElement(ElementName:="ItemName")> _
        Public Property ItemName() As String
            Get
                Return _ItemName
            End Get
            Set(ByVal value As String)
                _ItemName = value
            End Set
        End Property
        <XmlElement(ElementName:="ItemId")> _
        Public Property ItemId() As Long
            Get
                Return _ItemId
            End Get
            Set(ByVal value As Long)
                _ItemId = value
            End Set
        End Property
        <XmlElement(ElementName:="DetailId")> _
        Public Property DetailId() As Long
            Get
                Return _DetailId
            End Get
            Set(ByVal value As Long)
                _DetailId = value
            End Set
        End Property
        <XmlElement(ElementName:="FriendlyName")> _
        Public Property FriendlyName() As String
            Get
                Return _FriendlyName
            End Get
            Set(ByVal value As String)
                _FriendlyName = value
            End Set
        End Property
        <XmlArray(ElementName:="ControlSettingsCollection")> _
        Public Property ControlSettingsCollection() As List(Of ControlSettingInfo)
            Get
                Return _ControlSettingsCollection
            End Get
            Set(ByVal value As List(Of ControlSettingInfo))
                _ControlSettingsCollection = value
            End Set
        End Property
    End Class
End Namespace
