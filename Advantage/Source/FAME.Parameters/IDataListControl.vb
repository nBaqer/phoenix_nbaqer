﻿
Namespace Interfaces
    Public Interface IDataListControl
        Inherits IFilterControl
        Property DAClass() As String
        Property DAMethod() As String
        Property BindingTextField() As String
        Property BindingValueField() As String
        Property AssemblyFilePathDA() As String
        Property AssemblyDA() As System.Reflection.Assembly
    End Interface
End Namespace
