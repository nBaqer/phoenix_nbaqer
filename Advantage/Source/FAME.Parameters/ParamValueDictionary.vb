﻿Namespace Collections

    <Serializable()>
    Public Class ParamValueDictionary
        Inherits System.Collections.DictionaryBase

        Private _Name As String
        Private _CtrlName As String
        Public Property Name() As String
            Get
                Return _Name
            End Get
            Set(ByVal Value As String)
                _Name = Value
            End Set
        End Property
        Public Property CtrlName() As String
            Get
                Return _CtrlName
            End Get
            Set(ByVal Value As String)
                _CtrlName = Value
            End Set
        End Property
        ''' <summary>
        ''' Add a Dictionary entry.
        ''' </summary>
        ''' <param name="modifier"></param>
        ''' <param name="col"></param>
        ''' <remarks></remarks>
        Public Sub Add(ByVal modifier As ModDictionary.Modifier, ByVal col As ModDictionary)
            Dictionary.Add(modifier, col)
        End Sub

        ' 
        ''' <summary>
        ''' Return an object with the given key.
        ''' </summary>
        ''' <param name="key"></param>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Default Public Property Item(ByVal key As ModDictionary.Modifier) As ModDictionary
            Get
                Return DirectCast(Dictionary.Item(key), ModDictionary)
            End Get
            Set(ByVal Value As ModDictionary)
                Dictionary.Item(key) = Value
            End Set
        End Property

        ' 
        ''' <summary>
        ''' Return a collection containing the Dictionary's keys.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Keys() As ICollection
            Get
                Return Dictionary.Keys
            End Get
        End Property

        ' 
        ''' <summary>
        ''' Return a collection containing the Dictionary's values.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Values() As ICollection
            Get
                Return Dictionary.Values
            End Get
        End Property

        ''' <summary>
        ''' Return True if the Dictionary contains this ValueList
        ''' </summary>
        ''' <param name="key"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Contains(ByVal key As ModDictionary.Modifier) As Boolean
            Return Dictionary.Contains(key)
        End Function

        ' Remove this entry.
        Public Sub Remove(ByVal key As ModDictionary.Modifier)
            Dictionary.Remove(key)
        End Sub
    End Class
End Namespace
