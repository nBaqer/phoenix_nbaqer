﻿Namespace Info
    <Serializable()>
    Public Class ParameterSetInfo
        Private _SetId As Long
        Private _SetName As String
        Private _SetDisplayName As String
        Private _SetDescription As String
        Private _SetType As String
        Private _ParameterSectionCollection As List(Of ParameterSectionInfo)
        Public Property SetId() As Long
            Get
                Return _SetId
            End Get
            Set(ByVal Value As Long)
                _SetId = Value
            End Set
        End Property
        Public Property SetName() As String
            Get
                Return _SetName
            End Get
            Set(ByVal Value As String)
                _SetName = Value
            End Set
        End Property
        Public Property SetDisplayName() As String
            Get
                Return _SetDisplayName
            End Get
            Set(ByVal Value As String)
                _SetDisplayName = Value
            End Set
        End Property
        Public Property SetDescription() As String
            Get
                Return _SetDescription
            End Get
            Set(ByVal Value As String)
                _SetDescription = Value
            End Set
        End Property
        Public Property SetType() As String
            Get
                Return _SetType
            End Get
            Set(ByVal Value As String)
                _SetType = Value
            End Set
        End Property
        Public Property ParameterSectionCollection() As List(Of ParameterSectionInfo)
            Get
                Return _ParameterSectionCollection
            End Get
            Set(ByVal Value As List(Of ParameterSectionInfo))
                _ParameterSectionCollection = Value
            End Set
        End Property
    End Class
End Namespace
