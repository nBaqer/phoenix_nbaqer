﻿
Namespace Collections
    <Serializable()>
    Public Class ModDictionary
        Inherits System.Collections.DictionaryBase
        Public Enum Modifier
            IsEqualTo = 0
            IsNotEqualTo = 1
            GreaterThanOrEqualTo = 2
            LessThanOrEqualTo = 3
            GreaterThan = 4
            LessThan = 5
            InList = 6
            IsNull = 7
            IsNotNull = 8
            IsEmpty = 9
            IsNotEmpty = 10
            Contains = 11
            StartsWith = 12
            EndsWith = 13
        End Enum
        Private _Name As String
        ''' <summary>
        ''' The name of the ParameterSet
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Name() As String
            Get
                Return _Name
            End Get
            Set(ByVal Value As String)
                _Name = Value
            End Set
        End Property

        ''' <summary>
        ''' Add a Dictionary entry.
        ''' </summary>
        ''' <param name="name"></param>
        ''' <param name="col"></param>
        ''' <remarks></remarks>
        Public Sub Add(ByVal name As String, ByVal col As ValueList)
            Dictionary.Add(name, col)
        End Sub

        ' 
        ''' <summary>
        ''' Return an object with the given key.
        ''' </summary>
        ''' <param name="key"></param>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Default Public Property Item(ByVal key As String) As ValueList
            Get
                Return DirectCast(Dictionary.Item(key), ValueList)
            End Get
            Set(ByVal Value As ValueList)
                Dictionary.Item(key) = Value
            End Set
        End Property

        ' 
        ''' <summary>
        ''' Return a collection containing the Dictionary's keys.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Keys() As ICollection
            Get
                Return Dictionary.Keys
            End Get
        End Property

        ' 
        ''' <summary>
        ''' Return a collection containing the Dictionary's values.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Values() As ICollection
            Get
                Return Dictionary.Values
            End Get
        End Property

        ''' <summary>
        ''' Return True if the Dictionary contains this ParameterValueDictionary
        ''' </summary>
        ''' <param name="key"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Contains(ByVal key As String) As Boolean
            Return Dictionary.Contains(key)
        End Function

        ' Remove this entry.
        Public Sub Remove(ByVal key As String)
            Dictionary.Remove(key)
        End Sub
    End Class
End Namespace

