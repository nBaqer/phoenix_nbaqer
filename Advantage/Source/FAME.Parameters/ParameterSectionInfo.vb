﻿Namespace Info
    <Serializable()>
    Public Class ParameterSectionInfo
        Public Enum SecType
            [Advanced] = 0
            [Quick] = 1
        End Enum
        Private _SectionId As Long
        Private _SectionName As String
        Private _SectionCaption As String
        Private _SetId As Long
        Private _SectionSeq As Integer
        Private _SectionDescription As String
        Private _SectionType As SecType
        Private _ParameterDetailItemCollection As List(Of ParameterDetailItemInfo)
        Public Property SectionId() As Long
            Get
                Return _SectionId
            End Get
            Set(ByVal Value As Long)
                _SectionId = Value
            End Set
        End Property
        Public Property SectionName() As String
            Get
                Return _SectionName
            End Get
            Set(ByVal Value As String)
                _SectionName = Value
            End Set
        End Property
        Public Property SectionCaption() As String
            Get
                Return _SectionCaption
            End Get
            Set(ByVal Value As String)
                _SectionCaption = Value
            End Set
        End Property
        Public Property SetId() As Long
            Get
                Return _SetId
            End Get
            Set(ByVal Value As Long)
                _SetId = Value
            End Set
        End Property
        Public Property SectionSeq() As Integer
            Get
                Return _SectionSeq
            End Get
            Set(ByVal Value As Integer)
                _SectionSeq = Value
            End Set
        End Property
        Public Property SectionDescription() As String
            Get
                Return _SectionDescription
            End Get
            Set(ByVal Value As String)
                _SectionDescription = Value
            End Set
        End Property
        Public Property SectionType() As SecType
            Get
                Return _SectionType
            End Get
            Set(ByVal Value As SecType)
                _SectionType = Value
            End Set
        End Property
        Public Property ParameterDetailItemCollection() As List(Of ParameterDetailItemInfo)
            Get
                Return _ParameterDetailItemCollection
            End Get
            Set(ByVal Value As List(Of ParameterDetailItemInfo))
                _ParameterDetailItemCollection = Value
            End Set
        End Property
    End Class
End Namespace
