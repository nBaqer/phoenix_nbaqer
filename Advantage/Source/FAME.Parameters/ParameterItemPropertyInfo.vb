﻿Namespace Info
    <Serializable()>
    Public Class ParameterItemPropertyInfo
        Private _ItemPropertyId As Long
        Private _ItemId As Long
        Private _ChildControl As String
        Private _PropName As String
        Private _PropCallType As String
        Private _Value As String
        Private _ValueType As String
        Public Property ItemPropertyId() As Long
            Get
                Return _ItemPropertyId
            End Get
            Set(ByVal Value As Long)
                _ItemPropertyId = Value
            End Set
        End Property
        Public Property ItemId() As Long
            Get
                Return _ItemId
            End Get
            Set(ByVal Value As Long)
                _ItemId = Value
            End Set
        End Property
        Public Property ChildControl() As String
            Get
                Return _ChildControl
            End Get
            Set(ByVal Value As String)
                _ChildControl = Value
            End Set
        End Property
        Public Property PropName() As String
            Get
                Return _PropName
            End Get
            Set(ByVal Value As String)
                _PropName = Value
            End Set
        End Property
        Public Property PropCallType() As String
            Get
                Return _PropCallType
            End Get
            Set(ByVal Value As String)
                _PropCallType = Value
            End Set
        End Property
        Public Property Value() As String
            Get
                Return _Value
            End Get
            Set(ByVal Value As String)
                _Value = Value
            End Set
        End Property
        Public Property ValueType() As String
            Get
                Return _ValueType
            End Get
            Set(ByVal Value As String)
                _ValueType = Value
            End Set
        End Property
    End Class
End Namespace
