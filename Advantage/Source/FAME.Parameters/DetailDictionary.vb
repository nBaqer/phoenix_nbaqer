﻿Namespace Collections
    <Serializable()>
    Public Class DetailDictionary
        Inherits System.Collections.DictionaryBase
        Private _Name As String
        ''' <summary>
        ''' The name of the ParameterSet
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Property Name() As String
            Get
                Return _Name
            End Get
            Set(ByVal Value As String)
                _Name = Value
            End Set
        End Property

        ''' <summary>
        ''' Add a Dictionary entry.
        ''' </summary>
        ''' <param name="itemname"></param>
        ''' <param name="col"></param>
        ''' <remarks></remarks>
        Public Sub Add(ByVal itemname As String, ByVal col As ParamValueDictionary)
            Dictionary.Add(itemname, col)
        End Sub

        ' 
        ''' <summary>
        ''' Return an object with the given key.
        ''' </summary>
        ''' <param name="key"></param>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Default Public Property Item(ByVal key As String) As ParamValueDictionary
            Get
                Return DirectCast(Dictionary.Item(key), ParamValueDictionary)
            End Get
            Set(ByVal Value As ParamValueDictionary)
                Dictionary.Item(key) = Value
            End Set
        End Property

        ' 
        ''' <summary>
        ''' Return a collection containing the Dictionary's keys.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Keys() As ICollection
            Get
                Return Dictionary.Keys
            End Get
        End Property

        ' 
        ''' <summary>
        ''' Return a collection containing the Dictionary's values.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public ReadOnly Property Values() As ICollection
            Get
                Return Dictionary.Values
            End Get
        End Property

        ''' <summary>
        ''' Return True if the Dictionary contains this ParameterValueDictionary
        ''' </summary>
        ''' <param name="key"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Function Contains(ByVal key As String) As Boolean
            Return Dictionary.Contains(key)
        End Function

        ' Remove this entry.
        Public Sub Remove(ByVal key As String)
            Dictionary.Remove(key)
        End Sub
    End Class
End Namespace
