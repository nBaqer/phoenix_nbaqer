﻿
Imports FAME.Parameters.Info
Namespace Interfaces
    Public Interface IParamSetControl
        Property ParameterSetLookup() As String
        Property SetId() As Integer
        Property SqlConn() As String
        'ReadOnly Property ParameterSet() As ParameterSetInfo
        Property ParamSet() As ParameterSetInfo
        Property DisplayName() As String
    End Interface
End Namespace
