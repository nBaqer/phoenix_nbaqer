﻿Imports System
Imports System.Xml.Serialization
Namespace Info
    <Serializable()> _
Public Class ControlValueInfo
        Private _DisplayText As String
        Private _KeyData As String
        <XmlElement(ElementName:="DisplayText")> _
        Public Property DisplayText() As String
            Get
                Return _DisplayText
            End Get
            Set(ByVal value As String)
                _DisplayText = value
            End Set
        End Property
        <XmlElement(ElementName:="KeyData")> _
        Public Property KeyData() As String
            Get
                Return _KeyData
            End Get
            Set(ByVal value As String)
                _KeyData = value
            End Set
        End Property
    End Class
End Namespace
