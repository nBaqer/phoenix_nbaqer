﻿
Imports FAME.Parameters.Info
Imports System.Web.UI
Namespace Interfaces
    Public Interface ICustomControl
        Inherits IParamItemControl
        Property SavedSettings() As ParamItemUserSettingsInfo
        Function GetControlSettings() As ParamItemUserSettingsInfo
        Function GetDisplayData() As Control
        Sub LoadSavedReportSettings()
    End Interface
End Namespace
