﻿Imports FAME.Parameters.Info
Imports FAME.Parameters.Collections

Namespace Interfaces
    Public Interface IFilterControl
        Inherits IParamItemControl
        Property SavedSettings() As ParamItemUserSettingsInfo
        Property SendData() As Boolean
        Function GetControlSettings() As ParamItemUserSettingsInfo
        Function GetReturnValues() As DetailDictionary
        Sub LoadSavedReportSettings()
        Function SetFilterMode() As Boolean
        Sub ClearControls()
    End Interface
End Namespace
