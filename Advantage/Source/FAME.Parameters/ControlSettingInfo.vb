﻿Imports System
Imports System.Xml.Serialization
Namespace Info
    <Serializable()> _
    Public Class ControlSettingInfo
        Private _ControlName As String
        Private _ControlValueCollection As List(Of ControlValueInfo)
        <XmlElement(ElementName:="ControlName")> _
        Public Property ControlName() As String
            Get
                Return _ControlName
            End Get
            Set(ByVal value As String)
                _ControlName = value
            End Set
        End Property
        <XmlArray(ElementName:="ControlValueCollection")> _
        Public Property ControlValueCollection() As List(Of ControlValueInfo)
            Get
                Return _ControlValueCollection
            End Get
            Set(ByVal value As List(Of ControlValueInfo))
                _ControlValueCollection = value
            End Set
        End Property

    End Class
End Namespace
