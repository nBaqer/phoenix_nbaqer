﻿using FAME.Advantage.Domain.MultiTenant;
using FluentAssertions;
using NUnit.Framework;

namespace FAME.Advantage.MultiTenantHost.Tests.Persistence
{
    public class ApiAuthenticationKeyPersistenceTester : DomainPersistenceTester<int>
    {
        [Test]
        public void should_save_and_load_ApiAuthenticationKey()
        {
            var dataConnection = new DataConnection(string.Empty, string.Empty, string.Empty, string.Empty);

            var environment = new TenantEnvironment("test", "3.4", "test");
            SaveSupportingEntity(environment);

            var tenant = new Tenant(string.Empty, string.Empty, dataConnection, environment);
            SaveSupportingEntity(tenant);

            var apiAuthenticationKey = new ApiAuthenticationKey(tenant);

            var persistedApiAuthenticationKey = VerifyPersistence(apiAuthenticationKey);

            persistedApiAuthenticationKey.ID.Should().BeGreaterThan(0);
            persistedApiAuthenticationKey.Key.Should().Be(apiAuthenticationKey.Key);
            persistedApiAuthenticationKey.Tenant.ID.Should().Be(tenant.ID);
        }
    }
}
