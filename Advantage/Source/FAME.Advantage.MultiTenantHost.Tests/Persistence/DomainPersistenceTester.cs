﻿using System;
using System.Collections.Generic;
using System.Linq;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.Infrastructure.Extensions;
using FAME.Advantage.Domain.Persistence.Infrastructure.NHibernate;
using FAME.Advantage.Domain.Persistence.Infrastructure.UoW;
using NUnit.Framework;

namespace FAME.Advantage.MultiTenantHost.Tests.Persistence
{
    public abstract class DomainPersistenceTester : DomainPersistenceTester<Guid> {}

    public abstract class DomainPersistenceTester<TId>
    {
        private IList<DomainEntityWithTypedID<TId>> _insertedEntities;

        protected IUnitOfWork UnitOfWork { get; private set; }
        protected IRepositoryWithTypedID<TId> Repository { get; private set; }
        protected bool ReverseTearDown { get; set; }

        [SetUp]
        public void SetUp()
        {
            UnitOfWork = new UnitOfWork(SessionFactory.Instance);
            _insertedEntities = new List<DomainEntityWithTypedID<TId>>();
            Repository = new NHibernateRepository<TId>(UnitOfWork);
            ReverseTearDown = true;

            UnitOfWork.Start();
        }

        [TearDown]
        public void TearDown()
        {
            var entities = ReverseTearDown
                    ? _insertedEntities.Reverse()
                    : _insertedEntities;

            entities.Each(e => {
                // ReSharper disable AccessToDisposedClosure
                Repository.Delete(e);
                UnitOfWork.Flush();
                UnitOfWork.Clear();
                // ReSharper restore AccessToDisposedClosure
            });

            UnitOfWork.Dispose();
        }

        protected void SaveSupportingEntity<T>(T entity) where T : DomainEntityWithTypedID<TId>
        {
            Repository.Save(entity);
            UnitOfWork.Flush();

            _insertedEntities.Add(entity);
        }

        protected T VerifyPersistence<T>(T first) where T : DomainEntityWithTypedID<TId>
        {
            var second = VerifyPersistence(first, Repository);
            _insertedEntities.Add(second);

            return second;
        }

        protected T VerifyPersistence<T, TEntityId>(T first, IRepositoryWithTypedID<TEntityId> repository) where T : DomainEntityWithTypedID<TEntityId>
        {
            repository.Save(first);
            UnitOfWork.Flush();
            UnitOfWork.Clear();

            return repository.Load<T>(first.ID);
        }

        protected void SetDeleteOrder(IList<DomainEntityWithTypedID<TId>> entites)
        {
            ReverseTearDown = false;
            _insertedEntities = entites;
        }

        protected void RunInUnitOfWork(Action<IRepositoryWithTypedID<TId>> action)
        {
            var unitOfWork = new UnitOfWork(SessionFactory.Instance);
            var repository = new NHibernateRepository<TId>(unitOfWork);

            using (var scope = unitOfWork.CreateScope()) {
                action.Invoke(repository);
                scope.Complete();
            }
        }
    }
}
