﻿using System;
using System.Collections.Specialized;
using System.Reflection;
using System.Web.Security;
using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.MultiTenant;
using FAME.Advantage.Domain.Persistence.Infrastructure.NHibernate;
using FluentAssertions;
using NUnit.Framework;

namespace FAME.Advantage.MultiTenantHost.Tests.Persistence
{
    public class TenantUserPersistenceTester : DomainPersistenceTester<int>
    {
        private string _userName;
        private string _password;
        private MembershipUser _user;
        private object _userId;
        private TenantUser _newEntity;
        private IRepositoryWithTypedID<Guid> _repository;
        private SqlMembershipProvider _provider;

        [SetUp]
        public void SubSetUp()
        {
            // We are using FluentAssertions to check for null
            // ReSharper disable PossibleNullReferenceException
            _userName = GetRandomUserName();
            _password = CreateRandomPassword();
            _provider = GetMembershipProvider();
            _userId = Guid.NewGuid();

            MembershipCreateStatus status;
            _user = _provider.CreateUser(_userName, _password, _userName, "question", "answer", true, _userId, out status);

            status.Should().Be(MembershipCreateStatus.Success);
            _user.Should().NotBeNull();
            _user.ProviderUserKey.Should().Be(_userId);
            _repository = GetRepository<Guid>();
            // ReSharper restore PossibleNullReferenceException
        }

        private SqlMembershipProvider GetMembershipProvider()
        {
            var collection = new NameValueCollection {
                { "connectionStringName", "MultiTenant" },
                { "enablePasswordRetrieval", "false" },
                { "enablePasswordReset", "true" },
                { "requiresQuestionAndAnswer", "false" },
                { "applicationName", "Advantage" },
                { "requiresUniqueEmail", "false" },
                { "passwordFormat", "Hashed" }
            };

            var provider = new SqlMembershipProvider();
            provider.Initialize("AspNetSqlMembershipProviderNoSecurityAnswer", collection);

            return provider;
        }

        [Test]
        public void should_save_and_load_tenant_user()
        {
            var environment = GetEnvironment();
            var tenant = GetTenant(environment);

            var entity = _repository.Get<TenantUser>((Guid)_userId);
            entity.Should().NotBeNull();
            entity.AddTenant(tenant, isSupport: true, isDefault: true);

            _newEntity = VerifyPersistence(entity, _repository);
            _newEntity.Tenants.ShouldAllBeEquivalentTo(entity.Tenants);
        }

        [TearDown]
        public void SubTearDown()
        {
            if (_newEntity != null) {
                _newEntity.ClearTenants(Repository);
                _repository.SaveAndFlush(_newEntity);
            }

            if (_user != null) {
                _provider.DeleteUser(_userName, deleteAllRelatedData: true);
            }
        }

        private IRepositoryWithTypedID<T> GetRepository<T>()
        {
            return new NHibernateRepository<T>(UnitOfWork);
        }

        private Tenant GetTenant(TenantEnvironment environment)
        {
            var dataConnection = new DataConnection("testServer", "testDatabase", "testUserName", "testPassword");
            var tenant = new Tenant("test", "me", dataConnection, environment);
            SaveSupportingEntity(tenant);
            return tenant;
        }

        private TenantEnvironment GetEnvironment()
        {
            var environment = new TenantEnvironment("test", "3.4", "test");
            SaveSupportingEntity(environment);

            return environment;
        }

        private string CreateRandomPassword()
        {
            return String.Format("Password*${0}", GetDateString());
        }

        private string GetRandomUserName()
        {
            return String.Format("unit-test-{0}@fameinc.com", GetDateString());
        }

        private string GetDateString()
        {
            return DateTime.Now.ToString("MMddyyyyhhmmss");
        }
    }
}
