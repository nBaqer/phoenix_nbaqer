﻿using FAME.Advantage.Domain.MultiTenant;
using FluentAssertions;
using NUnit.Framework;

namespace FAME.Advantage.MultiTenantHost.Tests.Persistence
{
    public class TenantPersistenceTester : DomainPersistenceTester<int>
    {
        [Test]
        public void should_save_and_load_tenant()
        {
            var environment = new TenantEnvironment("test", "3.4", "test");
            SaveSupportingEntity(environment);
            
            var entity = new Tenant(
                "test", 
                "support", 
                new DataConnection(@"DEV2\SQL2008R2", "MTI", "AdvMTIUsr", "this is a test"),
                environment
            );

            var newEntity = VerifyPersistence(entity);

            newEntity.Name.Should().Be(entity.Name);
            newEntity.ModifiedBy.Should().Be(entity.ModifiedBy);
            newEntity.Connection.Should().Be(entity.Connection);
        }
    }
}
