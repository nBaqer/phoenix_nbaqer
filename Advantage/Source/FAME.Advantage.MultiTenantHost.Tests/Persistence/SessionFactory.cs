﻿using FAME.Advantage.Domain.MultiTenant.Persistence;
using NHibernate;

namespace FAME.Advantage.MultiTenantHost.Tests.Persistence
{
    public static class SessionFactory
    {
        private static readonly ISessionFactory INSTANCE =
            new MultiTenantSessionFactoryConfig().CreateSessionFactory();

        static SessionFactory() { }

        public static ISessionFactory Instance
        {
            get { return INSTANCE; }
        }
    }
}
