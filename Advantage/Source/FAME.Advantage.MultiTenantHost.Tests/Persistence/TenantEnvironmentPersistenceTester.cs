﻿using FAME.Advantage.Domain.MultiTenant;
using FluentAssertions;
using NUnit.Framework;

namespace FAME.Advantage.MultiTenantHost.Tests.Persistence
{
    public class TenantEnvironmentPersistenceTester : DomainPersistenceTester<int>
    {
        [Test]
        public void should_save_and_load_tenant_environment()
        {
            var entity = new TenantEnvironment("test", "3.4", "test");

            var newEntity = VerifyPersistence(entity);

            newEntity.Name.Should().Be(entity.Name);
            newEntity.VersionNumber.Should().Be(entity.VersionNumber);
            newEntity.ApplicationURL.Should().Be(entity.ApplicationURL);
        }
    }
}
