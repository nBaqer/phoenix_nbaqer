﻿using FAME.Advantage.Domain.Infrastructure.Entities;
using FAME.Advantage.Domain.MultiTenant;
using FAME.Advantage.MultiTenantHost.Lib.Controllers;
using FAME.Advantage.MultiTenantHost.Lib.Infrastructure.Automapper;
using FAME.Advantage.MultiTenantHost.Lib.Models.ApiAuthenticationKey;

using NUnit.Framework;
using Rhino.Mocks;
using StructureMap;
using System.Linq;

namespace FAME.Advantage.MultiTenantHost.Tests.Controllers
{
    public class ApiAuthenticationKeyControllerTester
    {
        Container container;
        public ApiAuthenticationKeyControllerTester()
        {
            container = new Container(x => x.Scan(scan =>
            {
                scan.AssemblyContainingType<IMapperDefinition>();
                scan.AddAllTypesOf<IMapperDefinition>();
            }));
        }

        [Test]
        public void Post_should_make_call_to_save_correctly()
        {
            var input = new InsertApiAuthenticationKeyModel {TenantId = 27};
            var mockRepository = MockRepository.GenerateStrictMock<IRepositoryWithTypedID<int>>();
            mockRepository.Stub(x => x.Load<Tenant>(input.TenantId))
                .Return(new Tenant(string.Empty, string.Empty, null, null) { ID = input.TenantId });
            mockRepository.Expect(x => x.Save(Arg<ApiAuthenticationKey>.Matches(k => k.Tenant.ID == input.TenantId)));
            container.Configure(x => x.For<IRepositoryWithTypedID<int>>().Use(mockRepository));

            var controller = container.GetInstance<ApiAuthenticationKeyController>();

            controller.Post(input);

            mockRepository.VerifyAllExpectations();
        }

        [Test]  
        public void Delete_should_make_call_to_delete_correctly()
        {
            int? input = 27;
            var mockRepository = MockRepository.GenerateStrictMock<IRepositoryWithTypedID<int>>();
            mockRepository.Stub(x => x.Query<ApiAuthenticationKey>())
                .Return(new[] { new ApiAuthenticationKey(new Tenant(string.Empty, string.Empty, null, null)) { ID = input.Value } }.AsQueryable());
            mockRepository.Expect(x => x.Delete(Arg<ApiAuthenticationKey>.Matches(k => k.ID == input.Value)));
            container.Configure(x => x.For<IRepositoryWithTypedID<int>>().Use(mockRepository));

            var controller = container.GetInstance<ApiAuthenticationKeyController>();

            controller.Delete(input);

            mockRepository.VerifyAllExpectations();
        }
    }
}
