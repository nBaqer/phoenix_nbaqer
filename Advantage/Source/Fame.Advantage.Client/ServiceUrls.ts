﻿// This has all references to Root Directory.....
module dash {

    export var XGET_VOYANT_ADDRESS_URL = "proxy/api/Wapi/Voyant/Voyant/GetVoyantDash";
    export var XGET_WAPI_OPERATION_SETTINGS_URL = "proxy/api/SystemStuff/Maintenance/WapiOperationSettings";
    export var XPOST_WAPI_ON_DEMAND_FLAGS = "proxy/api/SystemStuff/Maintenance/WapiOperationSettings/PostOnDemandFlags";
    export var XGET_WAPI_ON_DEMAND_FLAGS = "proxy/api/SystemStuff/Maintenance/WapiOperationSettings/GetOnDemandFlags";
    export var XGET_SINGLE_WAPI_ON_DEMAND_FLAG = "proxy/api/SystemStuff/Maintenance/WapiOperationSettings/GetSingleOnDemandFlag";



    export var XGET_WAPI_WINDOWS_SERVICE_STATUS_URL = "proxy/api/SystemStuff/Maintenance/WapiWindowsService";


    // Other external names used
    export var X_NAME_VOYANT_CURRENT_OPERATION_MODE = "VOYANT_CURRENT";
}

// This has all references to Root Directory.....
module Dash1 {

    export var XGET_VOYANT_ADDRESS_URL = "proxy/api/Wapi/Voyant/Voyant/GetVoyantDash";
    export var XGET_WAPI_OPERATION_SETTINGS_URL = "proxy/api/SystemStuff/Maintenance/WapiOperationSettings";
    export var XPOST_WAPI_ON_DEMAND_FLAGS = "proxy/api/SystemStuff/Maintenance/WapiOperationSettings/PostOnDemandFlags";
    export var XGET_WAPI_ON_DEMAND_FLAGS = "proxy/api/SystemStuff/Maintenance/WapiOperationSettings/GetOnDemandFlags";
    export var XGET_SINGLE_WAPI_ON_DEMAND_FLAG = "proxy/api/SystemStuff/Maintenance/WapiOperationSettings/GetSingleOnDemandFlag";
    export var XGET_WAPI_VOYANT_WIDGET_SETTINGS_URL = "proxy/api/Widgets/VoyantWidget/Get";


    export var XGET_WAPI_WINDOWS_SERVICE_STATUS_URL = "proxy/api/SystemStuff/Maintenance/WapiWindowsService";


    // Other external names used
    export var X_NAME_VOYANT_CURRENT_OPERATION_MODE = "VOYANT_CURRENT";
}