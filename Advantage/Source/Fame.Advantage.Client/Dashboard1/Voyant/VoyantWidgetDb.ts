﻿module Voyant {
    export class VoyantWidgetDb {

        //DataSourceError(e) {
        //    alert(e.responseText);
        //}

        // Get the WAPI Operation Settings 
        public GetDashUrl(): any {
            return $.ajax({
                url: dash.XGET_VOYANT_ADDRESS_URL,
                type: 'GET',
                dataType: 'text'
            });
        }

        // Return if OK a activities of type operation mode actives
        public GetOperationSettings(operationMode: string, contex: any) {
            return $.ajax({
                url: dash.XGET_WAPI_OPERATION_SETTINGS_URL + "?Id=0&OnlyActives=0&OperationMode=" + operationMode,
                context: contex,
                type: 'GET',
                dataType: "json"
            });
        }

        // Get Widget operations parameters....
        public GetWidgetSettings(userid: string, campusid: string, servicename: string) {
            return $.ajax({
                url: Dash1.XGET_WAPI_VOYANT_WIDGET_SETTINGS_URL,
                data: {'UserGuid': userid, 'CampusGuid': campusid, 'WindowServiceName': servicename  },
                type: 'GET',
                dataType: "json"
            });


        }

        // Get the Windows Service status (uninstalled, running stop)
        public GetWindowsServiceStatus(contex: any) {
            return $.ajax(
                {
                    url: dash.XGET_WAPI_WINDOWS_SERVICE_STATUS_URL,
                    context: contex,
                    type: "GET",
                    dataType: "text",
                    data: { ServiceName: 'WapiWindowsService' }
                });
        }

        // Post on - demand flag
        public PostOnDemandFlag(operationId: number, value: number, contex: any) {
            return $.ajax({
                url: dash.XPOST_WAPI_ON_DEMAND_FLAGS + "?Id=" + operationId + "&Value=" + value,
                context: contex,
                type: 'POST',
                dataType: 'text'
            });
        }

        public GetOnDemandFlag(operationId: number, contex: any) {
            return $.ajax({
                url: dash.XGET_SINGLE_WAPI_ON_DEMAND_FLAG + "?id=" + operationId,
                context: contex,
                type: 'GET',
                dataType: 'text'
            });
        }

    }
}