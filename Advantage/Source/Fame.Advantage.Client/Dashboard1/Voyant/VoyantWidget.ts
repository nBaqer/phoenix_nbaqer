﻿/// <reference path="../../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../../advweb/kendo/typescript/kendo.all.d.ts" />
module Voyant {

    export class VoyantWidget {

        public viewModel: VoyantWidgetVm;

        constructor() {

            // Create the view model for Voyant widget....
            this.viewModel = new VoyantWidgetVm();

            // Events associated with VOYANT
            document.getElementById('VoyantFrame').onload = function () {
            };

            // Expand general function for VOYANT widget
            $(".panel-wrap").on("click", "span.k-i-maximize", function (e) {
                var w:kendo.ui.Window = ($("#winVoyant").data("kendoWindow")) as any;
                w.open();
                w.maximize();
            });

            //$(".panel-wrap").on("click", "span.k-i-refresh", function (e) {
            //    if (window.confirm("An on demand operation will be sent to Voyant to update the prediction information. Click OK to continue.")) {
            //        var vm1 = new VoyantWidgetVm();
            //        vm1.GetOperationVoyantParameters(vm1.ExecuteOnDemandOperation);
            //        var w: kendo.ui.Window = $("#WindowsWapiDashOnDemand").data("kendoWindow") as any;
            //        w.open();
            //    }
            //    e.preventDefault();

            //});

            //Voyant expanded windows
            var win = $("#winVoyant").kendoWindow(
                {
                    title: 'VOYANT PREDICTIONS',
                    actions: ['Close'],
                    modal: false,
                    iframe: true,
                    open: function () {
                        this.wrapper.css({ zIndex: 0 });
                    },
                    dragend: function () {
                        var offset = this.wrapper.offset();

                        if (offset.top < 160) {
                            this.wrapper.css({ top: 160 });
                        }

                        if (offset.left < 40) {
                            this.wrapper.css({ left: 40 });
                        }

                        var windowW = (window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth);
                        var popupW = this.wrapper.width();
                        var displa = offset.left + popupW;
                        if (displa > windowW) {
                            this.wrapper.css({ left: (windowW - (popupW + 10)) });
                        }

                        var windowH = (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight);
                        var popupH = this.wrapper.height();
                        var displaH = offset.top + popupH;
                        if (displaH > windowH) {
                            this.wrapper.css({ top: (windowH - (popupH + 40)) });
                        }
                    },
                    resize: function () {
                        if (this.options.isMaximized) {
                            this.wrapper.css({ top: 160 });
                            var windowH = (window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight);
                            this.wrapper.css({ height: (windowH - (210)) });
                            this.wrapper.css({ left: 40 });
                            var windowW = (window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth);
                            this.wrapper.css({ width: (windowW - (55)) });
                        }

                    },
                    refresh: function () {
                        win.find(".k-loading-mask").remove();
                    }
                });

            $("#WindowsWapiDashOnDemand").kendoWindow(
                {
                    actions: [],
                    title: "On Demand Operation In Progress...",
                    width: "400px",
                    height: "120px",
                    visible: false,
                    modal: true,
                    resizable: false,
                });


            // Configure Add New Configuration Button
            var vm = this.viewModel;
            $("#btnDashCancelOnDemand").kendoButton({
                enable: true,
                click: function () { vm.StopOnDemandOperation(); }
            });

            // Configure the widget...
            this.viewModel.GetWidgetConfigurationValues();

            $("#openWinTt").kendoTooltip({
                autoHide: true,
                position: "top",
                showAfter:1000,
                content:"Open Detail Screen"
            });

            // Eliminate iframe permission after some time 
            setTimeout(eliminateScriptSandBox, 10000);

            function eliminateScriptSandBox(){
                document.getElementById('VoyantFrame').setAttribute("sandbox", "");
                   
                };
        }
    }
}