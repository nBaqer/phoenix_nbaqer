﻿/// <reference path="../../../fame.advantage.client.masterpage/advantagemessageboxs.ts" />

module Voyant {

    const messageVoyantNoActive = "Voyant Service is not active";
    const messageFlagNotSet = "On demand Operation Fail to set the Flag:";
    const messageOperationCanceled = "The On Demand Operation was canceled";

    declare var XMASTER_GET_USER_ID: string;
    declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;
    declare function __doPostBack();

    export class VoyantWidgetVm extends kendo.Observable {
        public db: VoyantWidgetDb;
        private IntervalTimer: number;
        private OperationId: number;

        // Constructor
        constructor() {
            super();
            this.db = new VoyantWidgetDb();
        }

        // Get All data need for the widget to configure
        public GetWidgetConfigurationValues() {
            // Set filter values...
            var campusid: string = XMASTER_GET_CURRENT_CAMPUS_ID;
            var userid: string = XMASTER_GET_USER_ID;
            var servicename: string = 'WapiWindowsService';

            this.db.GetWidgetSettings(userid, campusid, servicename)
                .done(msg => {
                    if (msg.toString() === '') {
                        alert("Voyant Widget, Error in communication");
                        return;
                    }

                    // Configure widget...

                    $("#VoyantFrame").attr("src", msg.UrlWidget);
 

                    var win = $('#DashBoardVoyant');
                    
                    
                    var winmax:any = $("#winVoyant").data("kendoWindow");
                    winmax.refresh({ url: msg.UrlWidGetMaximized });

                    //win.parent().css("visibility", "visible");
                    if (msg.ShowCustomCommand == true) {
                        win.find(".k-i-refresh").css("visibility", "visible");
                    }

                    if (msg.ShowMinimizeCommand == true) {
                        win.find(".k-i-arrowhead-n").css("visibility", "visible");
                    }

                    if (msg.ShowMaximizeCommand == true) {
                        win.find(".k-i-maximize").css("visibility", "visible");
                    }

                   var frame = <HTMLIFrameElement>document.getElementById('VoyantFrame');
                    frame.src = frame.src;


                })
                .fail(e => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(e);
                    //alert(e.responseText);
                });
        }

        // Get status of service and operation and configure the voyant windows dashboard
        // and the on demand button. If the call back is passed executed it (on demand operation)
        public GetOperationVoyantParameters(callbackOnDemand: any) {
            var opActive: boolean;
            var opIntervalPoll: number;
            this.db.GetOperationSettings(dash.X_NAME_VOYANT_CURRENT_OPERATION_MODE, this)
                .done(msg => {
                    if (msg.toString() == '') {
                        //win.parent().css("visibility", "hidden");
                        return;
                    }
                    if (msg.length > 0) {
                        this.OperationId = msg[0].ID;
                        opActive = msg[0].IsActiveOperation;
                        opIntervalPoll = msg[0].PollSecOnDemandOperation;
                    }

                    this.db.GetWindowsServiceStatus(this)
                        .done(ms => {
                            ms = ms.replace('"', '');
                            ms = ms.replace('"', '');
                            ms = ms.trim();
                            if (ms.toLowerCase() === 'uninstalled') {
                                // win.parent().css("visibility", "hidden");
                                return;
                            }
                            var serviceStatus = ms.toLowerCase();

                            //Analysis icon
                            //if (serviceStatus != 'running' || opActive == false) {
                            //    // win.parent().find(".k-i-refresh").parent().css("visibility", "hidden");
                            //}
                            //else {
                            //    // win.parent().find(".k-i-refresh").parent().css("visibility", "visible");
                            //}

                            // Call the callback function On Demand if exists
                            if (callbackOnDemand != null) {
                                if (serviceStatus === 'running' && opActive) {
                                    callbackOnDemand(this);
                                }
                                else {
                                   // alert("Voyant Service is not active");
                                    var w = $("#WindowsWapiDashOnDemand").data("kendoWindow") as any;
                                    w.close();
                                    MasterPage.SHOW_WARNING_WINDOW(messageVoyantNoActive);
                                }
                            }

                        })
                        .fail(e => {
                            MasterPage.SHOW_DATA_SOURCE_ERROR(e);
                           // alert(e.responseText);
                        });

                })
                .fail(e => {
                    MasterPage.SHOW_DATA_SOURCE_ERROR(e);
                   //alert(e.responseText);
                });
        }

        // Execute on demand operation VOYANT_CURRENT
        public ExecuteOnDemandOperation(contex: any) {
            if (contex.OperationId == 0) {
                return;
            }

            if (contex.OperationActive == false) {
                return;
            }

            // Send to server the On-demand Flag to 1
            var db = new VoyantWidgetDb();
            db.PostOnDemandFlag(contex.OperationId, 1, contex)
                .always(function (msg) {
                    if (msg.status !== 200) {
                        var windowd = $("#WindowsWapiDashOnDemand").data("kendoWindow") as any;
                        windowd.setOptions({
                            visible: false
                        });
                        windowd.close();
                        MasterPage.SHOW_ERROR_WINDOW(messageFlagNotSet + msg.statusText);
                        //alert();
                        return;
                    }

                    var windowx = $("#WindowsWapiDashOnDemand").data("kendoWindow") as any;
                    windowx.setOptions({
                        visible: true
                    });
                    
                    windowx.open();
                    windowx.center();
                    // Begin process to wait for operation complexion
                    var maxTim = contex.OperationIntervalPollDemand;
                    var elapsedTime = 0;
                    contex.IntervalTimer = setInterval(function () {
                        if (elapsedTime > maxTim) {
                            clearInterval(contex.IntervalTimer);
                            var win = $("#WindowsWapiDashOnDemand").data("kendoWindow") as any;
                            win.setOptions({
                                visible: false
                            });
                            //win.visible = false;
                            win.close();
                            contex.DashDataServices.PostOnDemandFlag(contex.OperationId, 0, contex);
                            MasterPage.SHOW_WARNING_WINDOW(messageOperationCanceled);
                            //alert("The On Demand Operation was canceled");
                            return;
                        }

                        // make the call....
                        db.GetOnDemandFlag(contex.OperationId, contex)
                            .done(function (flag) {
                                if (flag.OnDemandOperation === 0) {
                                    clearInterval(contex.IntervalTimer);
                                    var winx = $("#WindowsWapiDashOnDemand").data("kendoWindow") as any;
                                    winx.setOptions({
                                        visible: false
                                    });
                                   // winx.visible = false;
                                    winx.close();
                                    if (flag.Error === true) {
                                        
                                        alert(flag.Result);
                                    } else {
                                        if (flag.Result == null) {
                                            alert("Operation was successfully , but something was wrong. Please review configuration");
                                        } else {
                                            alert("Voyant Predictions update completed successfully");
                                        }
                                    }
                                    ;
                                    __doPostBack();
                                    return;
                                }

                                elapsedTime += 2;
                            })
                            .fail(function (msg1) {
                                clearInterval(contex.IntervalTimer);
                                var winx = $("#WindowsWapiDashOnDemand").data("kendoWindow") as any;
                                winx.setOptions({
                                    visible: false
                                });
                               // winx.visible = false;
                                winx.close();
                                MasterPage.SHOW_DATA_SOURCE_ERROR(msg1);
                                //alert(db.DataSourceError(msg1));
                            });
                    }, 2000);
                });
        }

        // Try to stop on demand operation
        public StopOnDemandOperation() {
            clearInterval(this.IntervalTimer);
            var wind = $("#WindowsWapiDashOnDemand").data("kendoWindow") as any;
            wind.setOptions({
                visible: false
            });
            //window.visible = false;
            wind.close();
            //var db = new WapiConfigDb();
            this.db.PostOnDemandFlag(this.OperationId, 0, this);
            MasterPage.SHOW_WARNING_WINDOW(messageOperationCanceled);
            //alert("The On Demand Operation was canceled");
        }
    }
}