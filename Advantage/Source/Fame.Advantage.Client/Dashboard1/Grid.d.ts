﻿/// <reference path="../../Fame.Advantage.API.Client/API/Common/Models/IFilterSearch.ts" />
/// <reference path="../../Fame.Advantage.API.Client/API/Common/Models/IFilterParameters.ts" />
/// <reference path="../../Fame.Advantage.API.Client/API/Common/Models/ISettings.ts" />
/// <reference path="../../Fame.Advantage.API.Client/API/Common/Models/IDataProvider.ts" />
/// <reference path="../../Fame.Advantage.API.Client/Components/Common/Grid.ts" />

import IFilterSearch = API.Models.IFilterSearch;
import IFilterParams = API.Models.IFilterParameters;
import ISettings = API.Models.ISettings;
import IDataProvider = API.Models.IDataProvider;
import Grid = Api.Components.Grid;
//declare class Grid {
//    constructor(componentId: string, dataProvider: IDataProvider, searchFilter?: IFilterSearch, paramsFilter?: IFilterParams, gridSettings?: ISettings, onChange?: (response) => any);
//    detach(callback: Function): void;
//}
