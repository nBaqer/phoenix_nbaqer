﻿///// <reference path="../../Fame.Advantage.API.Client/Common/ResizeSensor.d.ts" />
/// <reference path="../../Fame.Advantage.API.Client/API/FinancialAid/PrintTitleIVNotice.ts" />
/// <reference path="../../Fame.Advantage.API.Client/API/AcademicRecords/StudentsThatHaveMetProgramLength.ts" />
/// <reference path="../../Fame.Advantage.API.Client/API/Common/Models/IFilterSearch.ts" />
/// <reference path="../../Fame.Advantage.API.Client/API/Common/Models/IFilterParameters.ts" />
/// <reference path="../../Fame.Advantage.API.Client/API/Common/Models/ISettings.ts" />
/// <reference path="../../Fame.Advantage.API.Client/API/SystemCatalog/Models/IListItem.ts" />
/// <reference path="../../Fame.Advantage.API.Client/API/Common/Models/IDataProvider.ts" />
/// <reference path="../../Fame.Advantage.API.Client/Components/Common/Grid.ts" />
/// <reference path="../../Fame.Advantage.API.Client/API/Reports/TitleIvNoticeReport.ts" />
/// <reference path="../../Fame.Advantage.API.Client/API/SystemCatalog/Widget.ts" />
/// <reference path="../../Fame.Advantage.API.Client/ViewModels/Widgets/StudentsThatMetProgramLengthWidget.ts" />
///// <reference path="../../AdvWeb/Scripts/Fame.Advantage.API.Client.d.ts" />
///// <reference path="../../Fame.Advantage.Client/Dashboard1/Grid.d.ts" />

module Dash1 {
    import TitleIvNoticeReport = Api.TitleIvNoticeReport;
    import Widget = Api.Widget;
    declare var XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID: string;

    export class DashBoardVm1 extends kendo.Observable {

        public DashDataServices: DashBoardDb1;
        private IntervalTimer: number;
        private OperationId: number;
        mainPane: JQuery;
        sideBar: JQuery;
        mainContent: JQuery;


        /**
         * Title IV Widget
         */
        titleIVNoticesGrid: Api.Components.Grid;
        titleIVSapNoticesWidget: kendo.data.ObservableObject;
        printTitleIVNotice: Api.FinancialAid.PrintTitleIVNotice;
        titleIvNoticeReport: Api.TitleIvNoticeReport;
        titleIvGridSettings: ISettings;

        studentsThatMetProgramLengthWidget: Api.ViewModels.Widgets.StudentsThatMetProgramLengthWidget;

        widgetService: Api.Widget;
        emptyGuid: string = "00000000-0000-0000-0000-000000000000";
        // Constructor
        constructor() {
            super();
            this.DashDataServices = new DashBoardDb1();
            this.mainPane = $(".main-container");
            this.sideBar = $("#sidebar");
            this.mainContent = $("#main-content");
            this.titleIvGridSettings = {} as ISettings;
            this.printTitleIVNotice = new Api.FinancialAid.PrintTitleIVNotice();
            this.titleIvNoticeReport = new TitleIvNoticeReport();
           
            this.widgetService = new Widget();
        }
        public initialize() {
            let that = this;
            let campusId = XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID;

            that.studentsThatMetProgramLengthWidget =
                new Api.ViewModels.Widgets.StudentsThatMetProgramLengthWidget(campusId);

            that.setIGridSettings(that);

            $("#sidebar").kendoSortable({
                filter: ">div",
                cursor: "move",
                connectWith: "#main-content",
                placeholder: this.placeholder,
                hint: this.hint,

            });

            $("#main-content").kendoSortable({
                filter: ">div",
                cursor: "move",
                connectWith: "#sidebar",
                placeholder: this.placeholder,
                hint: this.hint
            });

            //expand
            $(".panel-wrap").on("click", "span.k-i-sort-desc-sm", function (e) {
                var contentElement = $(e.target).closest(".widget").find(">div");
                $(e.target)
                    .removeClass("k-i-sort-desc-sm")
                    .addClass("k-i-sort-asc-sm");

                kendo.fx(contentElement).expand("vertical").stop().play();
            });

            //collapse
            $(".panel-wrap").on("click", "span.k-i-sort-asc-sm", function (e) {
                var contentElement = $(e.target).closest(".widget").find(">div");
                $(e.target)
                    .removeClass("k-i-sort-asc-sm")
                    .addClass("k-i-sort-desc-sm");

                kendo.fx(contentElement).expand("vertical").stop().reverse();
            })

            //load widgets
            this.loadWidgets(that);
        }

        placeholder(element) {
            return element.clone().addClass("placeholder");
        }

        hint(element) {
            return element.clone().addClass("hint")
                .height(element.height())
                .width(element.width());
        }

        public loadingElement(): JQuery {
            return $("<div class='k-loading-mask' style='position: static;  z-index: 50000; '><span class='k-loading-text'>Loading...</span><div class='k-loading-image'/><div class='k-loading-color'/></div>");
        }


        //given campusId
        loadWidgets(that) {
            var campusId = XMASTER_PAGE_USER_OPTIONS_CURRENT_CAMPUSID;
            var resId = $('#hdnPageResourceId').val();

            if (!campusId || !resId)
                return; // parameters not set on page, do not show widgets

            //retrieve list of settings for current user/res from db
            that.widgetService.getDashboardWidgets(campusId, resId, (e: any) => {

                //for each widget in settings
                for (var i = 0; i < e.length; i++) {
                    var widgetSettings = e[i];

                    var initFunctionForWidget = arrayOfWidgetFunctions[widgetSettings.code];
                    if (initFunctionForWidget)
                        initFunctionForWidget(widgetSettings);
                }
            });

            var arrayOfWidgetFunctions = {
                "T4SAPNotice": function (widgetSettings) {

                    var $titleIVWidget = $("#titleIVSapNoticesWidget");

                    that.titleIVNoticesGrid = new Api.Components.Grid("titleIVNoticesGrid",
                        that.printTitleIVNotice,
                        undefined,
                        { params: { "campusId": campusId } },
                        that.titleIvGridSettings,
                        () => {

                        });

                    that.titleIVNoticesGrid.bind("dataBound", () => {
                        let noticeCount = that.titleIVNoticesGrid.count();
                        var template = kendo.template('<span class="k-label" id="noticeCountLabel">Pending:#=noticeCount#</span>');
                        var data = { noticeCount: noticeCount }; //A value in JavaScript/JSON
                        var result = template(data); //Pass the data to the compiled template
                        let noticeCountLabel = $("#noticeCountLabel");
                        if (noticeCountLabel.length) {
                            noticeCountLabel.remove();
                        }
                        $("#sapNoticeHeader").append(result); //display the result

                        if (noticeCount > 0) {
                            that.titleIVSapNoticesWidget.set("canPrint", true);
                        }
                        else {
                            that.titleIVSapNoticesWidget.set("canPrint", false);
                        }
                        $(".k-grid-content").height(140);
                    });


                    that.titleIVSapNoticesWidget = kendo.observable({
                        canPrint: false,
                        print: function () {
                            let noticesToPrint = that.titleIVNoticesGrid.getSelectedKeyNames();

                            that.titleIvNoticeReport.generateReportEnrollmentIdList(noticesToPrint,
                                () => {
                                    window.location.reload();
                                });

                        }

                    });

                    kendo.bind($titleIVWidget, that.titleIVSapNoticesWidget);
                    $titleIVWidget.show(); // must be replaced to inject template html onto DOM instead of showing and hiding widget
                },
                "StudentsReadyForGraduation": function (widgetSettings) {
                    that.studentsThatMetProgramLengthWidget.initialize(widgetSettings);
                }
            }
        }

        setTitleIvGridSettings(that) {
            that.titleIvGridSettings.settings = {
                height: "100%",
                resizable: true,

                columns: [
                    {
                        field: "studentName",
                        title: "Name",
                        width: 180
                    },
                    {
                        field: "sapStatus",
                        title: "Result",
                        width: 100
                    },
                    { selectable: true, width: "50px" },
                ],
                persistSelection: true,
                pageable: true,
                dataSource: {
                    schema: {
                        model: { id: "studentEnrollmentId" }
                    }
                }
            };
        }

        setIGridSettings(that) {
            that.setTitleIvGridSettings(that);
        }

    }
}