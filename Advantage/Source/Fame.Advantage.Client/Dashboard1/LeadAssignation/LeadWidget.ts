﻿/// <reference path="../../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../../advweb/kendo/typescript/kendo.all.d.ts" />
module LeadWidget {

    export class LeadMyWidget {

        public viewModel: LeadMyWidgetVm;

        constructor() {

            this.viewModel = new LeadMyWidgetVm();
            this.viewModel.CreateChartUnAssignedLeads();
            this.viewModel.CreateChartMyLead();
            ($("#MyLeadWidget").data("kendoChart") as any).redraw();
        }
    }
}