﻿
module LeadWidget {

declare var XMASTER_GET_BASE_URL: string;
declare var XMASTER_GET_CURRENT_CAMPUS_ID: string;

    export class LeadMyWidgetVm extends kendo.Observable {
        MyData: any;
        LeadData: any;
        db: LeadMyWidgetDb;


        constructor() {
            super();
            this.db = new LeadMyWidgetDb();
            this.MyData = this.db.getMyLeads();
            this.LeadData = this.db.GetLeadData();

        }

        CreateChartUnAssignedLeads() {
            var fthis = this;
            $("#UnAssignedWidget").kendoChart({
                theme: "blueOpal",
                chartArea: {
                    width: 300,
                    height: 200,
                    // border: { color: "cyan", width: 1 },
                    margin: 20
                },
                //title: {
                //    text: "No Assigned Leads",
                //    font: "12px"
                //},

                legend: {
                    visible: false
                    //position: "top"
                },
                seriesDefaults: {
                    labels: {
                        // template: "#= category # - #= kendo.format('{0:P}', percentage)#",
                        template: "#= category #",
                        //position: "outsideEnd",
                        visible: true,
                        background: "transparent",
                        font: "9px sans-serif"
                    }
                },
                dataSource: {
                    data: fthis.LeadData
                },
                series: [{
                    type: "donut",
                    field: "value",
                    visibleInLegend: false,
                    categoryField: "category",
                    explodeField: "explode"

                }],
                tooltip: {
                    visible: true,
                    template: "#= value#"
                },
                seriesClick: function (e) {
                    // Close the other chart exploding
                    var chart = $('#MyLeadWidget').data('kendoChart') as any;
                    $(chart.dataSource.options.data).each(function (i, item: any) {
                        item.explode = false;
                    });
                    fthis.CreateChartMyLead();

                    var selectedItem: string;
                    $(e.sender.dataSource.options.data).each(function (i, item: any) {
                        if (item.category != e.category) {
                            item.explode = false;
                        }
                        else {
                            item.explode = true;
                            selectedItem = item.category;
                        }

                    });
                    fthis.CreateChartUnAssignedLeads();
                    fthis.GoLoggerPageButton(selectedItem);

                }
            } as any);
        }

        CreateChartMyLead() {
            var fthis = this;

            $("#MyLeadWidget").kendoChart({
                theme: "blueOpal",
                chartArea: {
                    width: 300,
                    height: 200,
                    //border: { color: "cyan", width: 1 },
                    margin: 20

                },

                title: {
                //    text: "My Assigned Leads",
                //    font: "12px"
                },
                legend: {
                    visible: false
                },
                seriesDefaults: {
                    labels: {
                        template: "#= category #",
                        //position: "outsideEnd",
                        visible: true,
                        background: "transparent",
                        font: "9px sans-serif"

                    }
                },

                dataSource: {
                    data: fthis.MyData
                },
                series: [{
                    type: "donut",
                    field: "value",
                    visibleInLegend: false,
                    categoryField: "category",
                    explodeField: "explode"
                }],
                tooltip: {
                    visible: true,
                    template: "#= value#"
                },
                seriesClick: function (e) {
                    var chart = $('#UnAssignedWidget').data('kendoChart') as any ;
                    $(chart.dataSource.options.data).each(function (i, item: any) {
                        item.explode = false;
                    });
                    fthis.CreateChartUnAssignedLeads();

                    $(e.sender.dataSource.options.data).each(function (i, item: any) {
                        if (item.category != e.category) {
                            item.explode = false;
                        }
                        else {
                            item.explode = true;
                        }
                    });
                    fthis.CreateChartMyLead();
                    var selectedItem: string;
                    $(e.sender.dataSource.options.data).each(function (i, item: any) {
                        if (item.category != e.category) {
                            item.explode = false;
                        }
                        else {
                            item.explode = true;
                            selectedItem = item.category;
                        }

                    });
                    fthis.CreateChartUnAssignedLeads();
                    fthis.GoLoggerPageButton(selectedItem); // Go to MyLeadManagement  page
                }
            } as any);


        }

        // Go to Admission -> Common Task -> My Leads Management
        GoLoggerPageButton(category: string) {
            var qs;
            //var loc = location.href;
            var param = "&Category=" + category; 
            //var campusId = $.fn.QueryString[&cmpid];
            if (XMASTER_GET_CURRENT_CAMPUS_ID != null && XMASTER_GET_CURRENT_CAMPUS_ID != "") {
                qs = XMASTER_GET_BASE_URL + "/AD/MyLeadManagement.aspx?resid=820&mod=AD&cmpid=" + XMASTER_GET_CURRENT_CAMPUS_ID + "&desc=My Leads Management" + param;

            }
            else {
                qs = XMASTER_GET_BASE_URL + "/AD/MyLeadManagement.aspx?resid=820&mod=AD&desc=My Leads Management" + param;

            }

            location.href = qs;
        }

     }
 }