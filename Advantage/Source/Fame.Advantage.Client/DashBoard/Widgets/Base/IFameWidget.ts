﻿module dash.widget {
    
    /// <summary>
    /// IWidget describe what expose a Basic template widget
    /// </summary>
    export interface IFameWidget {

    //#region Properties
        WidgetId:number;
        WidgetCode: string;
        WidgetClassification: dash.DashLayer;
        VisibleRefresh: boolean;
        VisibleMaximize: boolean;
        VisibleCustom: boolean;
        Title: string;
        Visible: boolean;
        //Column: number; //column 1 and 2 are valid values
    //#endregion

    //#region Methods to override
        CustomHtml(e: any): Node;
        ShouldBeVisible(dashtype: dash.DashLayer);
        ShowWidget(insertionPoint: HTMLElement);
        Destroy();
    //#endregion
    
    //#region Events callbacks    
       // CloseWidget(e:any);
        RefreshWidget(e:any);
        MaximizeWidget(e:any);
        CustomWidget(e:any);
    //#endregion

    }//end IWidget
}

