﻿/// <reference path="../../../../advweb/kendo/typescript/jquery.d.ts" />

module dash.widget {

    export class FameWidget implements IFameWidget {


        //#region Properties
        InnerCode: HTMLElement;
        WidgetId: number;
        WidgetCode: string;
        WidgetClassification: dash.DashLayer;
        
        // Property VisibleRefresh........................................
        get VisibleRefresh(): boolean {
            var valsel: string = "fameWidget" + this.WidgetId.toString();
            var visible = $("#" + valsel + " .refresh").css("display");
            return (visible == "inline");
        }

        set VisibleRefresh(isVis: boolean) {
            var valsel: string = "fameWidget" + this.WidgetId.toString();
            if (isVis) {
                $("#" + valsel + " .refresh").css("display", "inline");
            }
            else {
                $("#" + valsel + " .refresh").css("display", "none");
            }
        }
        // ..............................................................
        // Property Visible Maximize.....................................
        get VisibleMaximize(): boolean {
            var valsel: string = "fameWidget" + this.WidgetId.toString();
            var visible = $("#" + valsel + " .openwin").css("display");
            return (visible == "inline");
        }

        set VisibleMaximize(isVis: boolean) {
            var valsel: string = "fameWidget" + this.WidgetId.toString();
            if (isVis) {
                $("#" + valsel + " .openwin").css("display", "inline");
            }
            else {
                $("#" + valsel + " .openwin").css("display", "none");
            }
        }
        // ..............................................................
        // Property Visible Custom.......................................
        get VisibleCustom(): boolean {
            var valsel: string = "fameWidget" + this.WidgetId.toString();
            var visible = $("#" + valsel + " .custom").css("display");
            return (visible == "inline");
        }

        set VisibleCustom(isVis: boolean) {
            var valsel: string = "fameWidget" + this.WidgetId.toString();
            if (isVis) {
                $("#" + valsel + " .custom").css("display", "inline");
            }
            else {
                $("#" + valsel + " .custom").css("display", "none");
            }
        }
        // ..............................................................
  
        // Property Visible Custom.......................................
        get Visible(): boolean {
            var valsel: string = "fameWidget" + this.WidgetId.toString();
            var visible = $("#" + valsel).css("display");
            return (visible == "inline");
        }

        set Visible(isVis: boolean) {
            var valsel: string = "fameWidget" + this.WidgetId.toString();
            if (isVis) {
                $("#" + valsel).css("display", "");
            }
            else {
                $("#" + valsel).css("display", "none");
            }
        }
        // ..............................................................
     
        Title: string;
       // Visible: boolean;
        //#endregion

        widgetHtml: HTMLElement;

        constructor(widgetid: number, widgetcode: string) {
           // var that = this;
            this.WidgetId = widgetid;
            this.WidgetCode = widgetcode;
        }

        //#region Methods
       // Override it in derived classes
         CustomHtml(e: any): Node {

            var html: HTMLElement = document.createElement("span");
            html.innerHTML = "Hello FAME Widget. Override CustomHtml to place your code";
            return html;
        }

       
        ShowWidget(insertionPoint: HTMLElement) {
            var that = this;
            var html = this.GetBaseHtml();
            // Insert the CustomHtml
            var insertion: Node = html.getElementsByClassName("famePlaceHolder")[0];
            insertion.appendChild(this.CustomHtml(null));
            insertionPoint.appendChild(html);
            var valsel: string = "fameWidget" + this.WidgetId.toString();
            
            // Add default events.................................
            $("#" + valsel).on("click", "span.k-i-custom", function (e) { that.CustomWidget(e); });
            $("#" + valsel).on("click", "span.k-i-refresh", function (e) { that.RefreshWidget(e); });
            $("#" + valsel).on("click", "span.k-i-maximize", function (e) { that.MaximizeWidget(e); });
            $("#" + valsel).on("click", "span.k-i-arrowhead-s", function (e) { that.ExpandWidget(e); });
            $("#" + valsel).on("click", "span.k-i-arrowhead-n", function (e) { that.CollapseWidget(e); });
        }

        Destroy() {
            var valsel: string = "fameWidget" + this.WidgetId.toString();
            $("#" + valsel).remove();

        }

        //#endregion

        // If the dashboard value passed is the same of the widget is visible.
        // Or in the case that is ALL.
        ShouldBeVisible(dashtype: dash.DashLayer) {
            if (dashtype == dash.DashLayer.All) {
                this.Visible = true;
            }
            else {
                this.Visible = (this.WidgetClassification == dashtype);
           }
        }

        //#region Events callbacks    
       
        RefreshWidget(e: any) {

            alert("Refresh Event: override in descendant");
        }

        MaximizeWidget(e: any) {

            alert("Maximize Event: override in descendant");
        }

        CustomWidget(e: any) {

            alert("Custom Event: override in descendant");
        }

        ExpandWidget(e: any) {
            var contentElement = $(e.target).closest(".widget").find(">div");
            $(e.target)
                .removeClass("k-i-arrowhead-s")
                .addClass("k-i-arrowhead-n");

            kendo.fx(contentElement).expand("vertical").stop().play();
        }

        CollapseWidget(e: any) {
            var contentElement = $(e.target).closest(".widget").find(">div");
            $(e.target)
                .removeClass("k-i-arrowhead-n")
                .addClass("k-i-arrowhead-s");

            kendo.fx(contentElement).expand("vertical").stop().reverse();
        }
        //#endregion

        //#region Utilities
        GetBaseHtml(): HTMLElement {
            var html: HTMLElement = document.createElement("div");
            var idval: string = "fameWidget" + this.WidgetId.toString();
            html.setAttribute("id", idval);
            html.setAttribute("class", "widget");
            html.setAttribute("name", this.WidgetCode);
            var h3: HTMLElement = document.createElement("h3");
            var title = document.createTextNode(this.Title);
            h3.appendChild(title);
            h3.setAttribute("class", "handler");
            //var span: HTMLElement = document.createElement("span");
            //span.setAttribute("class", "collapse k-icon k-i-close");
            //span.setAttribute("style", "visibility: visible");
            //h3.appendChild(span);
            var span: HTMLElement = document.createElement("span");
            span.setAttribute("class", "collapse k-icon k-i-arrowhead-n");
            span.setAttribute("style", "display: inline");
            h3.appendChild(span);
            span = document.createElement("span");
            span.setAttribute("class", "openwin k-icon k-i-maximize");
            span.setAttribute("style", "display: inline");
            h3.appendChild(span);
            span = document.createElement("span");
            span.setAttribute("class", "refresh k-icon k-i-refresh");
            span.setAttribute("style", "display: inline");
            h3.appendChild(span);
            span = document.createElement("span");
            span.setAttribute("class", "custom k-icon k-i-custom");
            span.setAttribute("style", "display: inline");
            h3.appendChild(span);
            html.appendChild(h3);
            var div = document.createElement("div");
            div.setAttribute("class", "famePlaceHolder");

            html.appendChild(div);
            return html;
        }


        //#endregion


    }
} 