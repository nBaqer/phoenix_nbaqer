﻿
module dash.widget {

    export class NewLeadWidget extends FameWidget {

        public viewModel: NewLeadWidgetVm;

        constructor(insertionPoint: string) {
            super(5, "NewLeadWidget");
            this.Title = "Imported Lead Management";
            this.viewModel = new NewLeadWidgetVm();
            var insert = document.getElementById(insertionPoint);
            this.ShowWidget(insert);
            this.viewModel.CreateChartUnAssignedLeads();
            this.viewModel.CreateChartMyLead();
            $("#MyLeadWidget").data("kendoChart").redraw();
            $("#UnAssignedWidget").data("kendoChart").redraw();

         

        }

        // Override HTML member...
        CustomHtml(e) {
            var html: HTMLElement = document.createElement("table");
           
            html.setAttribute("class", "newLeadWidgetTable");
            // First Graph
            var tr2: HTMLElement = document.createElement("tr");
            var td2: HTMLElement = document.createElement("td");
            td2.setAttribute("class", "leadColumnWidget");
            //var span2 = document.createElement("span");
            //span2.setAttribute("class", "leadSpanWidget");
            //span2.innerText = "No Assigned Leads";
            //td2.appendChild(span2);

            var graph2: HTMLElement = document.createElement("div");
            graph2.setAttribute("id", "UnAssignedWidget");
            //graph2.setAttribute("style", "width:100%");
            td2.appendChild(graph2);
            tr2.appendChild(td2);

            //second graph
            //var tr: HTMLElement = document.createElement("tr");
            var td: HTMLElement = document.createElement("td");
            td.setAttribute("class", "leadColumnWidget");
            //var span = document.createElement("span");
            //span.setAttribute("class", "leadSpanWidget");
            //span.innerText = "No Assigned Leads";
            //td.appendChild(span);

            var graph1: HTMLElement = document.createElement("div");
            graph1.setAttribute("id", "MyLeadWidget");
            //graph1.setAttribute("style", "width:100%");

            td.appendChild(graph1);
            tr2.appendChild(td);
            html.appendChild(tr2);
            return html;

        }


    }         
   


}