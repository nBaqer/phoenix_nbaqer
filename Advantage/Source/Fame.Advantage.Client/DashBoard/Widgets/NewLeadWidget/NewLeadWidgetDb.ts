﻿module dash.widget {
    export class NewLeadWidgetDb {

        DataSourceError(e) {
            alert(e.responseText);
        }

        // Fake data-sources

        GetMyLeads() {

            return [{
                "category": "New Lead",
                "value": 8,
                "color": '#FDBFBF',


            }, {
                    "category": "Document Pending",
                    "value": 4,
                    //"color": '#7FD4FF'
                }, {
                    "category": "Call pending",
                    "value": 1,
                   // "color": '#7FD4FF'
                }, {
                    "category": "Ready to Enroll",
                    "value": 6,
                   // "color": '#7FD4FF'

                }];
        } //End GetMyLeads

        GetLeadData() {
            return [{
                category: "Today",
                value: 5,
                color: '#FDBFBF'


            }, {
                    category: "Yesterday",
                    value: 2,
                //    color: '#FFFFD4'
                }, {
                    category: "10 days ago",
                    value: 1,
                //    color: '#7FD4FF'

                }, {
                    category: "Old",
                    value: 3,
                 //   color: '#007FFF'
                }];
        } // End GetLeadData

    }
}