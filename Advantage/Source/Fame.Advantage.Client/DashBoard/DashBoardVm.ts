﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />


module dash {

    export class DashBoardVm extends kendo.Observable {

        public Widgets: dash.widget.IFameWidget[];
        public Dashdb: DashBoardDb;
        public dashConfigWindows: kendo.data.DataSource;
        public dashConfigGridData: kendo.data.DataSource;
        public dashConfigDropDownData: any; //kendo.data.DataSource;


        // Constructor
        constructor() {
            super();
            this.Dashdb = new DashBoardDb();
            this.dashConfigWindows = this.Dashdb.getGridDataSource(null);
            this.dashConfigGridData = this.dashConfigWindows.options.data[0].widgets;
            this.dashConfigDropDownData = this.dashConfigWindows.options.data[0].roles;
            this.Widgets = [];
            // Create the widget and store in the Widget array

        }

        // Execute the change of dashboard module....
        ClickSelector(e) {
            //var id = e.sender.element.prop("id");
            var id = e.id;
            var layer = this.ConvertEnum(id);
            for (var i = 0; i < this.Widgets.length; i++) {
                this.Widgets[i].ShouldBeVisible(layer);
            }
        }

        // Execute the click of button dashboard configuration windows
        OpenDashConfigWindows(e) {

            var wd: kendo.ui.Window = $("#dash-window-config").data("kendoWindow") as any;
            var btnConfig: JQuery = $("#dash-config");
            //wd.visible = true;
            var btTop: number = btnConfig.offset().top;
            var btLeft: number = btnConfig.offset().left;
            var btWidth: number = btnConfig.width() * 2;

            wd.setOptions({
                position: {
                    top: btTop,
                    left: ((btLeft + btWidth) - wd.wrapper.width())
                }
            });
            // wd.center();
            wd.open();





            //this.Dashdb.DashConfigDatasource(null).read();
            var grid:any = $("#dashWidgetGrid").data("kendoGrid");
            grid.refresh();


        }

        ConvertEnum(text: string): dash.DashLayer {
            switch (text.toLowerCase()) {
                case "role0": { return dash.DashLayer.All; }
                case "role1": { return dash.DashLayer.Admission; }
                case "role2": { return dash.DashLayer.Academic; }
                case "role3": { return dash.DashLayer.StuAccount; }
                case "role4": { return dash.DashLayer.Finaid; }
                case "role5": { return dash.DashLayer.Faculty; }
                case "role6": { return dash.DashLayer.Placement; }
                case "role7": { return dash.DashLayer.Hr; }
                default: throw "Enumeration not recognize";
            }
        }

        GetToolBarConfig() {
            // First make invisible all buttons... except all
            for (var i: number = 0; i < 8; i++) {
                var btn: HTMLElement = document.getElementById("role" + i);
                btn.style.display= "none";
            }

            // select the button that should be visible based in the roles
            for (var y: number = 0; y < this.dashConfigDropDownData.length; y++) {
                var rol = "role" + this.dashConfigDropDownData[y].rol;
                var btn1: HTMLElement = document.getElementById(rol);
                //btn1.style.setAttribute("display", "inherit");

            }

            // Select the active tab in dashboard toolbar.
            var selectedbutton = this.dashConfigWindows.options.data[0].dashboardModule;
            var ee: any = new Object();
            ee.id = "role" + selectedbutton;
            this.ClickSelector(ee);
            $("#" + ee.id).addClass("k-state-active");

        }

        GetWidgetsToDash() {
            // get info from stored widgets
            var widgetlist: any = this.dashConfigGridData;
            this.Widgets = [];
            for (var i = 0; i < widgetlist.length; i++) {
                this.ShowHideWidget(widgetlist[i]);
            }
        }



        ShowHideWidget(widget: any) {
            // test if widget exists
            var wg:any = $("#fameWidget" + widget.wId).data("dash.widget.IFameWidget");
            // If exists destroy it!
            if (wg != null) { wg.Destroy(); }

            if (widget.wSelected == true) {
                // If it is selected by the user create it.
                wg = this.CreateWidget(widget);
                wg.WidgetClassification = widget.wClassif;
                this.Widgets.push(wg);
                
            }

        }


        CreateWidget(widget: any): dash.widget.IFameWidget {
            var newWig: dash.widget.IFameWidget = null;
            switch (widget.wId) {
                case "5": {
                    newWig = new dash.widget.NewLeadWidget((widget.column == 1) ? "left-content" : "right-content");
                    break;
                }
                default: {
                    newWig = new dash.widget.FameWidget(widget.wId, widget.wCode);
                    newWig.Title = widget.wCode;
                    newWig.ShowWidget(document.getElementById((widget.column == 1) ? "left-content" : "right-content"));
                }
            }

            return newWig;
        }




        //GetWidgetInfo() {
        //    // insert dynamically a widget
        //    var widget = new dash.widget.FameWidget(1, "TEST1");
        //    widget.VisibleCustom = false;
        //    widget.VisibleMaximize = false;
        //    widget.VisibleRefresh = false;
        //    widget.WidgetClassification = dash.DashLayer.Admission;
        //    widget.Title = "FAME Widget";
        //    var element = document.getElementById("left-content");
        //    widget.ShowWidget(element);
        //    this.Widgets.push(widget);
        //    var widget1 = new dash.widget.FameWidget(2, "TEST2");
        //    widget1.Title = "Second Example";
        //    var element1 = document.getElementById("right-content");
        //    widget1.ShowWidget(element1);
        //    widget1.WidgetClassification = dash.DashLayer.Academic;
        //    this.Widgets.push(widget1);
        //    var widget2 = new dash.widget.FameWidget(3, "TEST3");
        //    widget2.Title = "Third Example";
        //    var element2 = document.getElementById("left-content");
        //    widget2.ShowWidget(element2);
        //    widget2.WidgetClassification = dash.DashLayer.Faculty;
        //    this.Widgets.push(widget2);
        //    var widget3 = new dash.widget.FameWidget(4, "TEST4");
        //    widget3.Title = "Other Example";
        //    var element3 = document.getElementById("left-content");
        //    widget3.ShowWidget(element3);
        //    widget3.VisibleRefresh = false;
        //    this.Widgets.push(widget3);
        //    widget3.WidgetClassification = dash.DashLayer.Academic;

        //    var lead = new dash.widget.NewLeadWidget("right-content");
        //    lead.VisibleCustom = false;
        //    lead.VisibleMaximize = false;
        //    lead.VisibleRefresh = false;
        //    lead.WidgetClassification = dash.DashLayer.Admission;
        //    this.Widgets.push(lead);


        //    $(window).on("resize", function () {
        //        kendo.resize($(".dashboardWrap"));
        //    });
        //}



    } // End Class
} // End module