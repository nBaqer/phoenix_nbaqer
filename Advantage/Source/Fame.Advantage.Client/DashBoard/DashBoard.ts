﻿/// <reference path="../../advweb/kendo/typescript/kendo.all.d.ts" />
/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
module dash {

    export enum DashLayer {
        All = 0,
        Admission = 1,
        Academic = 2,
        StuAccount = 3,
        Finaid = 4,
        Faculty = 5,
        Placement = 6,
        Hr = 7,
    }

    export class DashBoard {
        public ViewModel: DashBoardVm;
        public dashconfigwindow: kendo.ui.Window;

       
        // Constructor 
        constructor() {
            this.ViewModel = new DashBoardVm();
            var viewModel = this.ViewModel;

            //#region Auxiliary function to pass the context to called function
            var clickSelectorFunction = (function (e) {
                viewModel.ClickSelector(e);
            }).bind(viewModel);

            var clickConfigFunction = (function (e) {
                viewModel.OpenDashConfigWindows(e);
            }).bind(viewModel);
            //#endregion

            //#region Toolbar dashboard

            // Create the kendo tool-bar with the module selection and the configuration windows button
            $("#dash-toolbar").kendoToolBar({
                resizable: false,
                items: [
                    { template: "<label style='margin-right:10px; font-weight:bold'>My Dashboard</label>" },
                    { type: "separator" },
                    {
                        type: "buttonGroup",
                        buttons: [
                            { text: "All", id: "role0", togglable: true, group: "myGroup", toggle: clickSelectorFunction },
                            { text: "Admissions", id: "role1", togglable: true, group: "myGroup", toggle: clickSelectorFunction },
                            { text: "Academics", id: "role2", togglable: true, group: "myGroup", toggle: clickSelectorFunction },
                            { text: "Student Accounts", id: "role3", togglable: true, group: "myGroup", toggle: clickSelectorFunction },
                            { text: "Financial Aid", id: "role4", togglable: true, group: "myGroup", toggle: clickSelectorFunction },
                            { text: "Faculty", id: "role5", togglable: true, group: "myGroup", toggle: clickSelectorFunction },
                            { text: "Placement", id: "role6", togglable: true, group: "myGroup", toggle: clickSelectorFunction },
                            { text: "Human Resources", id: "role7", togglable: true, group: "myGroup", toggle: clickSelectorFunction }
                        ]
                    },
                    { type: "button",  id: "dash-config", text:"'", spriteCssClass: "k-icon k-i-hbars", attributes: { style: "float: right;" } }
                ]
            });

            // Create a tooltip around the dashboard config button
            var tooltip = document.createElement("div");
            tooltip.setAttribute("id", "dash-config-tt");
            tooltip.setAttribute("style", "float: right");
            var configbutton = document.getElementById("dash-config");
            tooltip.innerHTML = configbutton.outerHTML;
            configbutton.parentNode.replaceChild(tooltip, configbutton);
           //configbutton.replaceNode(tooltip);
            configbutton = document.getElementById("dash-config");

            // Put the tooltip information            
            $("#dash-config-tt").kendoTooltip({ content: "Dashboard Personalization", position: "top" });

            //Select the All button as initial
            var toolbar = $("#dash-toolbar").data("kendoToolBar") as any;
            toolbar.toggle("#all", true); //select button with id: "all"

            // Add click event to open configuration window
            configbutton.addEventListener("click", clickConfigFunction);

             //#endregion

            //#region Dashboard column structure creation

            // Create the column left of the dashboard 
            $("#left-content").kendoSortable({
                filter: ">div",
                cursor: "move",
                connectWith: " #right-content",
                placeholder: this.placeholder,
                hint: this.hint,
                handler: ".handler"
            });

            // Create column right of dashboard     
            $("#right-content").kendoSortable({
                filter: ">div",
                cursor: "move",
                connectWith: "#left-content",
                placeholder: this.placeholder,
                hint: this.hint,
                handler: ".handler"
            });

            // Load the Widgets.....
            //viewModel.GetWidgetInfo();

            //#endregion 

            //#region Define the windows pop up for configuration
            $("#dashWidgetGrid").kendoGrid({
                dataSource:  viewModel.dashConfigGridData,
                
                
                rowTemplate: kendo.template($("#dashWidgetGrid-template").html()),
                altRowTemplate: kendo.template($("#dashWidgetGrid-alttemplate").html()),
                height: 500
            });
            // Define the configuration windows....
            $("#dash-window-config").kendoWindow({
                maxWidth: 640,
                height: "650px",
                actions: ["Close"],
                title: "Personalize your dashboard",
                animation: {
                    open: {
                        effects: "slideIn:left fadeIn",
                        duration: 900
                    },
                    close: {
                        effects: "slideIn:left fadeIn",
                        //reverse: true,
                        duration:900
                    }
                }
            });

            $("#dash-config-tab").kendoTabStrip({
                animation: {
                    open: {
                        effects: "fadeIn"
                    }
                },
               
            });

            // Configure OK Button
            $("#dash-config-ok").kendoButton({
                enable: true,
                click: e => {  }
            });
            // Configure Clear Button
            $("#dash-config-clear").kendoButton({
                enable: true,
                click: e => {  }
            });
            // Configure Cancel Button
            $("#dash-config-cancel").kendoButton({
                enable: true,
                click: e => { }
            });

            // Create option combo box
            $("#dash-config-optioncb").kendoDropDownList({
                dataSource: viewModel.dashConfigDropDownData,
                dataTextField: "desc",
                dataValueField: "rol",
                value: viewModel.dashConfigWindows.options.data[0].dashboardModule
            });

            //#endregion

            // Config the widgets
            viewModel.GetWidgetsToDash();
            
            // Config the toolbar
            viewModel.GetToolBarConfig();

            $(window).on("resize", function () {
                kendo.resize($(".dashboardWrap"), true);
            });
        }

        //#region Dashboard functions
  
        public placeholder(element) {
            return element.clone().addClass("placeholder");
        }

        public hint(element) {
            return element.clone().addClass("hint")
                .height(element.height())
                .width(element.width());
        }
        //#endregion
    }
}