﻿/// <reference path="../../advweb/kendo/typescript/jquery.d.ts" />
/// <reference path="../serviceurls.ts" />


module dash {
    export class DashBoardDb {

      
        DataSourceError(e) {
            alert(e.responseText);
        }

        getGridDataSource(verb: string): kendo.data.DataSource {
            var ethis = this;
            var dat = ethis.GetDashConfigData();
            var dsource = new kendo.data.DataSource(
                {
                    data: dat,
                });

            return dsource;
        }


        GetDashConfigData() {
            return [{dashboardModule:1, widgets: [
                  {wId:'5', wSelected:true, wCode:'NewLeadWidget', column:1, wClassif:'1', wDescription:'Lorem ipsum dolor sit amet, consectetur adipiscing elit',wImage:'images/Widgets/FameWidget.png' }
                , { wId: '2', wSelected: true, wCode: 'Sample2', column: 1, wClassif:'1', wDescription:'Lorem ipsum dolor sit amet, consectetur adipiscing elit',wImage:'images/Widgets/FameWidget.png' }
                , { wId: '3', wSelected: true, wCode: 'Sample3', column: 1, wClassif:'2', wDescription:'Lorem ipsum dolor sit amet, consectetur adipiscing elit',wImage:'images/Widgets/FameWidget.png' }
                , { wId: '4', wSelected: false, wCode: 'Sample4', column: 1, wClassif:'2', wDescription:'Lorem ipsum dolor sit amet, consectetur adipiscing elit',wImage:'images/Widgets/FameWidget.png' } 
                , { wId: '1', wSelected: true, wCode: 'Sample5', column: 1, wClassif:'1', wDescription:'Lorem ipsum dolor sit amet, consectetur adipiscing elit',wImage:'images/Widgets/FameWidget.png' } 
                , { wId: '6', wSelected: false, wCode: 'Sample6', column: 2, wClassif:'2', wDescription:'Lorem ipsum dolor sit amet, consectetur adipiscing elit',wImage:'images/Widgets/FameWidget.png' } 
                , { wId: '7', wSelected: false, wCode: 'Sample7', column: 2, wClassif:'2', wDescription:'Lorem ipsum dolor sit amet, consectetur adipiscing elit',wImage:'images/Widgets/FameWidget.png' } 
                , { wId: '8', wSelected: true, wCode: 'Sample8', column: 2, wClassif: '2', wDescription: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit', wImage: 'images/Widgets/FameWidget.png' }],
                roles: [{ rol: 1, desc: 'Admission' }, { rol: 2, desc: 'Academics' }, {rol: 0, desc:'All'} ]  }];

        }

    }

   
}