'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------
Imports System
Imports System.Collections.Generic


Namespace Advantage.Data.ORM	
	
	Public Partial Class SyUser

		Private _userName As String 
		Public Overridable Property UserName As String 
			Get
		        Return Me._userName
			End Get
			Set(ByVal value As String)
		        Me._userName = value
			End Set
		End Property

		Private _userId As Guid 
		Public Overridable Property UserId As Guid 
			Get
		        Return Me._userId
			End Get
			Set(ByVal value As Guid)
		        Me._userId = value
			End Set
		End Property

		Private _showDefaultCampus As Boolean 
		Public Overridable Property ShowDefaultCampus As Boolean 
			Get
		        Return Me._showDefaultCampus
			End Get
			Set(ByVal value As Boolean)
		        Me._showDefaultCampus = value
			End Set
		End Property

		Private _salt As String 
		Public Overridable Property Salt As String 
			Get
		        Return Me._salt
			End Get
			Set(ByVal value As String)
		        Me._salt = value
			End Set
		End Property

		Private _password As String 
		Public Overridable Property Password As String 
			Get
		        Return Me._password
			End Get
			Set(ByVal value As String)
		        Me._password = value
			End Set
		End Property

		Private _modUser As String 
		Public Overridable Property ModUser As String 
			Get
		        Return Me._modUser
			End Get
			Set(ByVal value As String)
		        Me._modUser = value
			End Set
		End Property

		Private _moduleId As Short 
		Public Overridable Property ModuleId As Short 
			Get
		        Return Me._moduleId
			End Get
			Set(ByVal value As Short)
		        Me._moduleId = value
			End Set
		End Property

		Private _modDate As Date? 
		Public Overridable Property ModDate As Date? 
			Get
		        Return Me._modDate
			End Get
			Set(ByVal value As Date?)
		        Me._modDate = value
			End Set
		End Property

		Private _isLoggedIn As Boolean? 
		Public Overridable Property IsLoggedIn As Boolean? 
			Get
		        Return Me._isLoggedIn
			End Get
			Set(ByVal value As Boolean?)
		        Me._isLoggedIn = value
			End Set
		End Property

		Private _isDefaultAdminRep As Boolean? 
		Public Overridable Property IsDefaultAdminRep As Boolean? 
			Get
		        Return Me._isDefaultAdminRep
			End Get
			Set(ByVal value As Boolean?)
		        Me._isDefaultAdminRep = value
			End Set
		End Property

		Private _isAdvantageSuperUser As Boolean 
		Public Overridable Property IsAdvantageSuperUser As Boolean 
			Get
		        Return Me._isAdvantageSuperUser
			End Get
			Set(ByVal value As Boolean)
		        Me._isAdvantageSuperUser = value
			End Set
		End Property

		Private _fullName As String 
		Public Overridable Property FullName As String 
			Get
		        Return Me._fullName
			End Get
			Set(ByVal value As String)
		        Me._fullName = value
			End Set
		End Property

		Private _email As String 
		Public Overridable Property Email As String 
			Get
		        Return Me._email
			End Get
			Set(ByVal value As String)
		        Me._email = value
			End Set
		End Property

		Private _confirmPassword As String 
		Public Overridable Property ConfirmPassword As String 
			Get
		        Return Me._confirmPassword
			End Get
			Set(ByVal value As String)
		        Me._confirmPassword = value
			End Set
		End Property

        Private _campusId As Guid?
        Public Overridable Property CampusId As Guid?
            Get
                Return Me._campusId
            End Get
            Set(ByVal value As Guid?)
                Me._campusId = value
            End Set
        End Property

		Private _accountActive As Boolean? 
		Public Overridable Property AccountActive As Boolean? 
			Get
		        Return Me._accountActive
			End Get
			Set(ByVal value As Boolean?)
		        Me._accountActive = value
			End Set
		End Property

		Private _user As SyUser 
		Public Overridable Property User As SyUser 
			Get
		        Return Me._user
			End Get
			Set(ByVal value As SyUser)
		        Me._user = value
			End Set
		End Property

		Private _syResource As SyResource 
		Public Overridable Property SyResource As SyResource 
			Get
		        Return Me._syResource
			End Get
			Set(ByVal value As SyResource)
		        Me._syResource = value
			End Set
		End Property

		Private _syUsersRolesCampGrps As IList(Of SyUsersRolesCampGrp)  = new List(Of SyUsersRolesCampGrp)
		Public Overridable ReadOnly Property SyUsersRolesCampGrps As IList(Of SyUsersRolesCampGrp) 
			Get
		        Return Me._syUsersRolesCampGrps
			End Get
		End Property

		Private _syUsers As IList(Of SyUser)  = new List(Of SyUser)
		Public Overridable ReadOnly Property SyUsers As IList(Of SyUser) 
			Get
		        Return Me._syUsers
			End Get
		End Property

	End Class
End Namespace
