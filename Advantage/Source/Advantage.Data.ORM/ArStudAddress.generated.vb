'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated by a tool.
'
'     Changes to this file may cause incorrect behavior and will be lost if
'     the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------
Imports System


Namespace Advantage.Data.ORM	
	
	Public Partial Class ArStudAddress

		Private _stdAddressId As Guid 
		Public Overridable Property StdAddressId As Guid 
			Get
		        Return Me._stdAddressId
			End Get
			Set(ByVal value As Guid)
		        Me._stdAddressId = value
			End Set
		End Property

		Private _studentId As Guid? 
		Public Overridable Property StudentId As Guid? 
			Get
		        Return Me._studentId
			End Get
			Set(ByVal value As Guid?)
		        Me._studentId = value
			End Set
		End Property

		Private _address1 As String 
		Public Overridable Property Address1 As String 
			Get
		        Return Me._address1
			End Get
			Set(ByVal value As String)
		        Me._address1 = value
			End Set
		End Property

		Private _address2 As String 
		Public Overridable Property Address2 As String 
			Get
		        Return Me._address2
			End Get
			Set(ByVal value As String)
		        Me._address2 = value
			End Set
		End Property

		Private _city As String 
		Public Overridable Property City As String 
			Get
		        Return Me._city
			End Get
			Set(ByVal value As String)
		        Me._city = value
			End Set
		End Property

		Private _stateId As Guid? 
		Public Overridable Property StateId As Guid? 
			Get
		        Return Me._stateId
			End Get
			Set(ByVal value As Guid?)
		        Me._stateId = value
			End Set
		End Property

		Private _zip As String 
		Public Overridable Property Zip As String 
			Get
		        Return Me._zip
			End Get
			Set(ByVal value As String)
		        Me._zip = value
			End Set
		End Property

		Private _countryId As Guid? 
		Public Overridable Property CountryId As Guid? 
			Get
		        Return Me._countryId
			End Get
			Set(ByVal value As Guid?)
		        Me._countryId = value
			End Set
		End Property

		Private _addressTypeId As Guid? 
		Public Overridable Property AddressTypeId As Guid? 
			Get
		        Return Me._addressTypeId
			End Get
			Set(ByVal value As Guid?)
		        Me._addressTypeId = value
			End Set
		End Property

		Private _modUser As String 
		Public Overridable Property ModUser As String 
			Get
		        Return Me._modUser
			End Get
			Set(ByVal value As String)
		        Me._modUser = value
			End Set
		End Property

		Private _modDate As Date? 
		Public Overridable Property ModDate As Date? 
			Get
		        Return Me._modDate
			End Get
			Set(ByVal value As Date?)
		        Me._modDate = value
			End Set
		End Property

		Private _statusId As Guid? 
		Public Overridable Property StatusId As Guid? 
			Get
		        Return Me._statusId
			End Get
			Set(ByVal value As Guid?)
		        Me._statusId = value
			End Set
		End Property

		Private _default1 As Boolean 
		Public Overridable Property Default1 As Boolean 
			Get
		        Return Me._default1
			End Get
			Set(ByVal value As Boolean)
		        Me._default1 = value
			End Set
		End Property

		Private _foreignZip As Boolean 
		Public Overridable Property ForeignZip As Boolean 
			Get
		        Return Me._foreignZip
			End Get
			Set(ByVal value As Boolean)
		        Me._foreignZip = value
			End Set
		End Property

		Private _otherState As String 
		Public Overridable Property OtherState As String 
			Get
		        Return Me._otherState
			End Get
			Set(ByVal value As String)
		        Me._otherState = value
			End Set
		End Property

		Private _arStudent As ArStudent 
		Public Overridable Property ArStudent As ArStudent 
			Get
		        Return Me._arStudent
			End Get
			Set(ByVal value As ArStudent)
		        Me._arStudent = value
			End Set
		End Property

	End Class
End Namespace
