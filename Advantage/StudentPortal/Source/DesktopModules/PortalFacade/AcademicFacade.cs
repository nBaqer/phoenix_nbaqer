﻿using System;
using System.Collections.Generic;
using System.Linq;
using PortalData.Academic;
using PortalDb.AcademicServices;

namespace PortalFacade
{
    public static class AcademicFacade
    {
        #region Get Catalogs (enrollments, Terms, Courses)
        /// <summary>
        /// Get a list of enrollments (programs)
        /// </summary>
        /// <param name="studentNumber"></param>
        /// <returns></returns>
        public static IList<EnrollmentOutput> GetEnrollments(string studentNumber)
        {
            var db = new EnrollmentServiceDb();
            var list = db.GetEnrollments(studentNumber);
            return list;
        }

        /// <summary>
        /// Get a list of terms for a particular enrollment (Program).
        /// </summary>
        /// <param name="stuEnrollId">Enrollment Guid</param>
        /// <returns></returns>
        public static IList<TermOutput> GetTerms(Guid stuEnrollId)
        {
            var db = new EnrollmentServiceDb();
            var list = db.GetTerms(stuEnrollId);
            return list;
        }

        /// <summary>
        /// Get the course list filtered by enrollment 
        /// or enrollment and term 
        /// or enrollment, term and instructor.
        /// </summary>
        /// <param name="stuEnrollGuid"></param>
        /// <param name="termGuid"></param>
        /// <param name="instructorGuid"></param>
        /// <returns></returns>
        public static IList<CourseOutput> GetCourses(Guid stuEnrollGuid, Guid? termGuid, Guid? instructorGuid)
        {
            var db = new EnrollmentServiceDb();
            var list = db.GetCourses(stuEnrollGuid, termGuid, instructorGuid);
            return list;
        }

        #endregion

        #region Test Coditions (students has class)
        /// <summary>
        /// See if the student has assignated a course for any of the possible enrollments.
        /// </summary>
        /// <param name="studentNumber"></param>
        /// <returns></returns>
        public static bool IsStudentAssignedCourses(string studentNumber)
        {
            var db = new EnrollmentServiceDb();
            var list = db.GetEnrollments(studentNumber);
            if (list.Count == 0)
            {
                return false;
            }

            return list.Aggregate(false, (current, output) => current | db.HasCourses(output.EnrollmentId.GetValueOrDefault()));
        }
        #endregion

        #region Enrollments Related operation
        public static List<EnrollmentOutput> GetNameAndNumber(string stuEnrollid)
        {
            var db = new EnrollmentServiceDb();
            var list = db.GetNameAndNumber(stuEnrollid);
            return list;
        }

        public static List<ConfigOutput> GetConfigSettings(string stuEnrollId)
        {
            var db = new EnrollmentServiceDb();
            var list = db.GetConfigSettings(stuEnrollId);
            return list;
        }

        #endregion

        #region Grade Book Realated Operation

        /// <summary>
        /// Get Grade Books Results.
        /// </summary>
        /// <param name="stuEnrollmentsId"></param>
        /// <param name="termId"></param>
        /// <param name="clsSectionId"></param>
        /// <returns></returns>
        public static IList<GradeBookResultOutput> GetGradeBookResults(string stuEnrollmentsId, string termId,string clsSectionId)
        {
            var db = new GradeBookServiceDb();
            var list = db.GetGradeBookResults(stuEnrollmentsId, termId, clsSectionId);
            return list;
        }

        /// <summary>
        /// Get grade book Details
        /// </summary>
        /// <param name="stuEnrollmentsId"></param>
        /// <param name="termId"></param>
        /// <param name="clsSectionId"></param>
        /// <returns></returns>
        public static IList<GradeBookComponentsOutput> GetGradeBookComponents(string stuEnrollmentsId, string termId,string clsSectionId)
        {
            var db = new GradeBookServiceDb();
            var list = db.GetGradeBookComponents(stuEnrollmentsId, termId, clsSectionId);
            return list;
        }


        #endregion
    }
}
