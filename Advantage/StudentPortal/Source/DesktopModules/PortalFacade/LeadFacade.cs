﻿using PortalData.Lead;

namespace PortalFacade
{
    public static class LeadFacade
    {
        /// <summary>
        /// Insert a lead.
        /// </summary>
        /// <param name="info">Lead Info onject</param>
        /// <returns></returns>
        public static string InsertLeadInPortal(LeadInfo info)
        {
            // Adecuate the input parameters
            info.StateId = (info.StateId == "00000000-0000-0000-0000-000000000000") ? null : info.StateId;
            info.DrivLicStateID = (info.DrivLicStateID == "00000000-0000-0000-0000-000000000000") ? null : info.DrivLicStateID;
            
            var db = new PortalDb.LeadServices.LeadServiceDb();
            var resultString = db.InsertLeadInPortal(info);
            return resultString;
        }
    }
}
