﻿using System;
using System.Data;
using System.Data.SqlClient;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;

namespace Christoc.Modules.StudentInfoFrame
{
    public partial class View : StudentInfoFrameModuleBase, IActionable
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            imgStudent.ImageUrl = DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo().Profile.PhotoURLFile;
            string userName = DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo().Username;

            if (!IsPostBack)
            {
                string conString = GetSQLConnection();
                var clCon = new SqlConnection(conString);
                var cmd = new SqlCommand(" SELECT TOP 1 (FirstName+' '+ LastName) AS StudentName,StudentNumber,HomeEmail, Phone=CASE WHEN (LEN(Phone)=10) THEN SUBSTRING(Phone,1,3) + '.' + SUBSTRING(Phone,4,3) + '.' + Substring(Phone,7,4) ELSE Phone END " +
                                                " FROM arstudent Stu " +
                                               "  LEFT JOIN dbo.arStudentPhone Phone ON Stu.StudentId=Phone.StudentId" +
                                                " WHERE Stu.StudentNumber=@UserName AND default1=1 ", clCon);

                var adp = new SqlDataAdapter {SelectCommand = cmd};
                cmd.Parameters.AddWithValue("@UserName", userName);
                var ds = new DataSet();
                clCon.Open();

                try
                {
                    adp.Fill(ds, "Student");
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        lblStuName.Text = ds.Tables[0].Rows[0].Field<string>(0);
                        lblStuID.Text = @"Student ID: " + ds.Tables[0].Rows[0].Field<string>(1);
                        lblEmail.Text = @"Email: " + ds.Tables[0].Rows[0].Field<string>(2);
                        lblPhone.Text = @"Phone: " + ds.Tables[0].Rows[0].Field<string>(3);

                    }

                }
                catch (Exception exc)
                {
                    Exceptions.ProcessModuleLoadException(this, exc);
                    Response.Write("<script language='javascript'>alert('Database Connection cannot be established');</script>");
                }
                finally
                {
                    adp.Dispose();
                    cmd.Dispose();
                    clCon.Close();
                }
            }
        }

        public ModuleActionCollection ModuleActions
        {
            get
            {
                var actions = new ModuleActionCollection
                    {
                        {
                            GetNextActionID(), Localization.GetString("EditModule", LocalResourceFile), "", "", "",
                            EditUrl(), false, SecurityAccessLevel.Edit, true, false
                        }
                    };
                return actions;
            }
        }
        private string GetSQLConnection()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["ClientConnectionString"].ConnectionString;
        }
    }
}