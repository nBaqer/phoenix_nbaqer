﻿using Christoc.Modules.MyAttendance.Data.OutputData;
using Christoc.Modules.MySchedule.Data.OutputData;
using FAME.AdvantageV1.BusinessFacade;
using FAME.AdvantageV1.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Christoc.Modules.MyAttendance.Data
{
    public class AttendanceData
    {
        //Linq data context object
        readonly AttendanceLinqClassesDataContext dbContext = new AttendanceLinqClassesDataContext();

        #region Public Methods
        
        /// <summary>
        /// get the enrollments for the student using the student number
        /// </summary>
        /// <param name="studentNumber"></param>
        /// <returns></returns>
        public List<EnrollmentOutput> GetEnrollments(string studentNumber)
        {
            var data = (from e in dbContext.sp_getStuEnrollmentsPortal(studentNumber)
                        select new EnrollmentOutput
                        {
                            EnrollmentId = e.StuEnrollId,
                            Enrollment = e.Enrollment
                        }).OrderBy(n => n.Enrollment).ToList();

            return data;
        }

        /// <summary>
        /// get the available terms for the student schedule for the dropdown list
        /// </summary>
        /// <param name="stuEnrollId"></param>
        /// <returns></returns>
        public List<TermOutput> GetTerms(Guid stuEnrollId)
        {
            var data = (from res in dbContext.arResults
                        join enr in dbContext.arStuEnrollments on res.StuEnrollId equals enr.StuEnrollId
                        join cs in dbContext.arClassSections on res.TestId equals cs.ClsSectionId
                        join cst in dbContext.arClassSectionTerms on cs.ClsSectionId equals cst.ClsSectionId
                        join t in dbContext.arTerms on cst.TermId equals t.TermId
                        join r in dbContext.arReqs on cs.ReqId equals r.ReqId
                        join cmpgrpcmps in dbContext.syCmpGrpCmps on cs.CampusId equals cmpgrpcmps.CampusId
                        where enr.StuEnrollId == stuEnrollId
                        select new TermOutput
                        {
                            TermId = t.TermId,
                            Description = t.TermDescrip

                        }).Distinct().OrderBy(n => n.Description).ToList();



            return data;
        }

        /// <summary>
        /// get the available courses for the student for the dropdown list
        /// </summary>
        /// <param name="stuEnrollId"></param>
        /// <param name="termId"></param>
        /// <returns></returns>
        public List<CourseOutput> GetCourses(Guid stuEnrollId, Guid termId)
        {
            var data = (from res in dbContext.arResults
                join enr in dbContext.arStuEnrollments on res.StuEnrollId equals enr.StuEnrollId
                join cs in dbContext.arClassSections on res.TestId equals cs.ClsSectionId
                join cst in dbContext.arClassSectionTerms on cs.ClsSectionId equals cst.ClsSectionId
                join t in dbContext.arTerms on cst.TermId equals t.TermId
                join r in dbContext.arReqs on cs.ReqId equals r.ReqId
                join cmpgrpcmps in dbContext.syCmpGrpCmps on cs.CampusId equals cmpgrpcmps.CampusId
                where enr.StuEnrollId == stuEnrollId
                && cst.TermId == termId
                select new CourseOutput
                {
                    ClassSectionId = cs.ClsSectionId,
                    Description = r.Descrip
                }).Distinct().OrderBy(n => n.Description).ToList();


            return data;

        }


        /// <summary>
        /// Build the data for the report
        /// </summary>
        /// <param name="stuEnrollId"></param>
        /// <param name="termId"></param>
        /// <param name="sectionId"></param>
        /// <returns></returns>
        public DataSet BuildReportDataSet(string stuEnrollId, string termId, string sectionId)
        {
            //build report params object that will be used to get the report data
            var rptParamInfo = new ReportParamInfo
            {
                ResId = 492,
                RptTitle = "Student Attendance",
                CampusId = GetCampusIdFromEnrollment(stuEnrollId),
                OrderBy = "",
                OrderByString = "",
                FilterListString = "",
                FilterOther = "arstuenrollments.StuEnRollId = '" + stuEnrollId + "' ",
                FilterOtherString = "",
                ShowFilters = false,
                ShowRptNotes = false,
                ShowRptDateIssue = false,
                ShowRptDescription = false,
                ShowRptInstructions = false,
                ShowSortBy = false,
                TermId = termId
            };

            //get report data
            var ds = new CRStudentAttendanceReport().GetReportDataSet(rptParamInfo);
            
            //make a copy of the table with student attendance
            var dtcopy = ds.Tables[0].Copy();

            // See if no record was returned...
            if (dtcopy.Rows.Count < 1)
            {
                return null;
            }


            //create a new dataset to output filtered data
            var dsOutput = new DataSet();

            var x = (from DataRow row in dtcopy.Rows
                where (row.Field<Guid>("ClsSectionId") == new Guid(sectionId))
                select row) ;

            if (x.Any())
            {
                dsOutput.Tables.Add(
                    dtcopy.AsEnumerable()
                        .Where(row => row.Field<Guid>("ClsSectionId") == new Guid(sectionId))
                        .CopyToDataTable());
            }
            else
            {
                return null;
            }

            //filter the copied table by class section id, and add to output dataset

            dsOutput.Tables[0].TableName = ds.Tables[0].TableName;

            //build a new params table to add to dataset
            DataTable dt = BuildReportParamsTable();

            //add params info to data row
            //add then to dataset
            DataRow dr = dt.NewRow();
            dr["SchoolName"] = "";
            dr["ShowFilters"] = true;
            dr["ShowSortBy"] = false;
            dr["ShowRptDescription"] = false;
            dr["ShowRptInstructions"] = false;
            dr["ShowRptNotes"] = false;
            dr["StudentIdentifier"] = "SNN";
            dr["SchoolLogo"] = DBNull.Value;

            dt.Rows.Add(dr);
            dt.TableName = "ReportParams";
            dsOutput.Tables.Add(dt);

            //return dataset
            return dsOutput;
        }
        #endregion

        /// <summary>
        /// get the campus for this enrollment id
        /// </summary>
        /// <param name="enrollId"></param>
        /// <returns></returns>
        private string GetCampusIdFromEnrollment( string enrollId )
        {
            Guid? cmpId = null;
            var arStuEnrollment = dbContext.arStuEnrollments.FirstOrDefault(n => n.StuEnrollId == new Guid(enrollId));
            if (arStuEnrollment != null)
                cmpId =arStuEnrollment.CampusId;

            return cmpId.ToString();
        }

        
        /// <summary>
        /// build the params table
        /// </summary>
        /// <returns></returns>
        private DataTable BuildReportParamsTable()
        {
            var dt = new DataTable();

            dt.Columns.Add("SchoolName", Type.GetType("System.String"));
            dt.Columns.Add("ShowFilters", Type.GetType("System.Boolean"));
            dt.Columns.Add("Filter", Type.GetType("System.String"));
            dt.Columns.Add("FilterOthers", Type.GetType("System.String"));
            dt.Columns.Add("ShowSortBy", Type.GetType("System.Boolean"));
            dt.Columns.Add("SortBy", Type.GetType("System.String"));
            dt.Columns.Add("ShowRptDescription", Type.GetType("System.Boolean"));
            dt.Columns.Add("ShowRptInstructions", Type.GetType("System.Boolean"));
            dt.Columns.Add("ShowRptNotes", Type.GetType("System.Boolean"));
            dt.Columns.Add("StudentIdentifier", Type.GetType("System.String"));
            dt.Columns.Add("SchoolLogo", Type.GetType("System.Byte[]"));

            return dt;
        }
    }
}