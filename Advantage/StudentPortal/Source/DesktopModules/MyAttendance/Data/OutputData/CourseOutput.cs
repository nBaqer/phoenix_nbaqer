﻿using System;


namespace Christoc.Modules.MySchedule.Data.OutputData
{
    public class CourseOutput
    {
        public Guid ReqId { get; set; }
        public Guid? ClassSectionId { get; set; }
        public string Description { get; set; }
        public Guid? GradeSystemDetailId { get; set; }
    }
}