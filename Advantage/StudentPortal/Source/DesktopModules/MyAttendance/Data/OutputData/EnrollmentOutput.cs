﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.MyAttendance.Data.OutputData
{
    public class EnrollmentOutput
    {
        public Guid? EnrollmentId { get; set; }
        public string Enrollment { get; set; }
        public string Status { get; set; }
        public string Shift { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpectedGradDate { get; set; }
    }
}