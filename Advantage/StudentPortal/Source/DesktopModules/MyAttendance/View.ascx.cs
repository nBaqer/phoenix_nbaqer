﻿using System.Web;
using Christoc.Modules.MyAttendance.Data;
using Christoc.Modules.MyAttendance.Data.OutputData;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.UI.WebControls;
using FAME.Advantage.Common;
using PortalData;

namespace Christoc.Modules.MyAttendance
{
    public partial class View : MyAttendanceModuleBase, IActionable
    {
        #region Class private variables

        private readonly string studentNumber;
        private AttendanceData atData;

        #endregion

        #region Constructor

        public View()
        {
            studentNumber = DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo().Username;
        }

        #endregion

        #region Class events
        /// <summary>
        /// page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            lblError.Text = string.Empty;
            atData = new AttendanceData();
            if (HttpContext.Current.Session["PortalAdvAppSettings"] == null)
                HttpContext.Current.Session["PortalAdvAppSettings"] = AdvAppSettings.GetAppSettings();


            if (!IsPostBack)
            {
                BuildEnrollments();
                if (ddlEnrollment.Items.Count > 0)
                {
                    BuildTerms();
                    if (ddlTerm.Items.Count == 0)
                    {
                        lblError.Text = Constant.X_WARNING_USER_HAS_NO_ATTENDANCE;
                        btnView.Enabled = false;
                    }
                    else
                    {
                        BuildCourses();
                        if (ddlCourses.Items.Count == 0)
                        {
                            lblError.Text = Constant.X_WARNING_USER_HAS_NO_ATTENDANCE;
                            btnView.Enabled = false;
                        }
                        else
                        {
                            lblError.Text = string.Empty;
                            btnView.Enabled = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// event fired when user changes enrollment.
        /// Update the terms and courses based on the enrollment chosen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlEnrollment_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            BuildTerms();
            BuildCourses();
            lblError.Text = string.Empty;
        }


        /// <summary>
        /// event fired when user chooses a term
        /// update the course list based on the term chosen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlTerm_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            BuildCourses();
            lblError.Text = string.Empty;
        }


        /// <summary>
        /// event fired when the user clicks to view the report
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnView_OnClick(object sender, EventArgs e)
        {
            //try
            //{
                lblError.Text = string.Empty;
                BuildReport();
            //}
            //catch (Exception ex)
            //{
            //    if (ex.Message.Contains("Sorry"))
            //    {
            //        lblError.Text = ex.Message;// Exceptions.ProcessModuleLoadException("Sorry, No data matching your selection", this, ex);
            //    }
            //    else
            //    {
            //        throw;
            //    }
            //}
        }

        #endregion

        #region Class Methods

        /// <summary>
        /// Build the enrollments dropdown list
        /// </summary>
        private void BuildEnrollments()
        {
            try
            {
                ddlEnrollment.DataTextField = "Enrollment";
                ddlEnrollment.DataValueField = "EnrollmentId";
                var enrollment = atData.GetEnrollments(studentNumber);
                if (enrollment.Count == 0)
                {
                    lblError.Text = Constant.X_WARNING_USER_HAS_NO_RECORDS;
                    btnView.Enabled = false;
                }
                else
                {
                    btnView.Enabled = true;
                    lblError.Text = string.Empty;
                }
                ddlEnrollment.DataSource = enrollment;
                ddlEnrollment.DataBind();
            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException("An error occured while attempting to retrieve the filter options", this, ex);
            }
        }

        /// <summary>
        /// build the terms dropdown list
        /// </summary>
        private void BuildTerms()
        {
            var lstTerms = atData.GetTerms(new Guid(ddlEnrollment.SelectedValue));
            ddlTerm.DataTextField = "Description";
            ddlTerm.DataValueField = "TermId";
            ddlTerm.DataSource = lstTerms;
            ddlTerm.DataBind();

            string currentTerm = GetCurrentTerm(lstTerms);
            if (!string.IsNullOrEmpty(currentTerm))
                ddlTerm.SelectedValue = currentTerm;
            else
                ddlTerm.SelectedIndex = ddlTerm.Items.Count - 1;
        }

        /// <summary>
        /// build the courses dropdown list
        /// </summary>
        private void BuildCourses()
        {
            var lstCourses = atData.GetCourses(new Guid(ddlEnrollment.SelectedValue), new Guid(ddlTerm.SelectedValue));
            ddlCourses.DataTextField = "Description";
            ddlCourses.DataValueField = "ClassSectionId";
            ddlCourses.DataSource = lstCourses;
            ddlCourses.DataBind();
            ddlCourses.Items.Insert(0, new ListItem("Choose Course", "0"));

        }

        /// <summary>
        /// build and export the report to crystal reports
        /// </summary>
        private void BuildReport()
        {
            if (ddlCourses.SelectedIndex == 0)
            {
                lblError.Text = @"Please choose a course";
            }
            else
            {
                lblError.Text = "";
                DataSet ds = atData.BuildReportDataSet(ddlEnrollment.SelectedValue, ddlTerm.SelectedValue, ddlCourses.SelectedValue);
                if (ds == null)
                {
                    lblError.Text = @"Sorry, No data matching your selection";
                    return;
                }

                if (ds.Tables[0].Rows.Count > 0)
                {
                    string reportPath = Server.MapPath("DesktopModules/MyAttendance/arStudentAttendance.rpt");
                    var rep = new CrystalDecisions.CrystalReports.Engine.ReportDocument();

                    rep.Load(reportPath);
                    rep.Database.Tables[0].SetDataSource(ds);

                    Response.Buffer = false;
                    Response.ClearContent();
                    Response.ClearHeaders();

                    rep.ExportToHttpResponse(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, Response, true, "MyAttendance");

                    rep.Dispose();
                }
            }
        }

        /// <summary>
        /// get the current term from the list passed in
        /// </summary>
        /// <param name="lstTerms"></param>
        /// <returns></returns>
        private string GetCurrentTerm(IEnumerable<TermOutput> lstTerms)
        {
            DateTime today = DateTime.Now;
            string termId = string.Empty;
            foreach (var item in lstTerms)
            {
                if (item.StartDate != null && item.EndDate != null)
                {
                    if (DateTime.Compare(today, item.StartDate.Value) > 0 && DateTime.Compare(today, item.EndDate.Value) <= 0)
                    {
                        termId = item.TermId.ToString();
                        break;
                    }
                }
            }

            return termId;
        }

        /// <summary>
        /// DNN required method
        /// </summary>
        public ModuleActionCollection ModuleActions
        {
            get
            {
                var actions = new ModuleActionCollection
                    {
                        {
                            GetNextActionID(), Localization.GetString("EditModule", LocalResourceFile), "", "", "",
                            EditUrl(), false, SecurityAccessLevel.Edit, true, false
                        }
                    };
                return actions;
            }
        }

        #endregion

    }
}

