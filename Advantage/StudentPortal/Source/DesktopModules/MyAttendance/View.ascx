﻿<%@ Control Language="C#" AutoEventWireup="True" CodeBehind="View.ascx.cs" Inherits="Christoc.Modules.MyAttendance.View"  %>
<%@ Register TagPrefix="cr" Namespace="CrystalDecisions.Web" Assembly="CrystalDecisions.Web, Version=13.0.3500.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<div align="center" >
    
    <table style="width: 90%">
        <tr>
            <td ><span style='font-size:12px;'>To see your attendance, please choose a program, term, and course.  When you click the view button,your attendance will be downloaded according to the selection you have made.<br/>  
                Please understand that your attendance information could take a few minutes to download.</span>
            </td>
        </tr>
    </table>
    <br/>
    <table style="padding: 3px; margin:3px" >
        
        <tr>
            <td>
                <asp:Label ID="lblEnrollment" runat="server" Text="Program:" ></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlEnrollment" runat="server" Width="400px" AutoPostBack="True" OnSelectedIndexChanged="ddlEnrollment_OnSelectedIndexChanged" ></asp:DropDownList>    
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblTerm" runat="server" Text="Term:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlTerm" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlTerm_OnSelectedIndexChanged" Width="400px"></asp:DropDownList>
            </td>
        </tr>
        <tr>
             <td>
                <asp:Label ID="lblCourses" runat="server" Text="Course:" ></asp:Label>
            </td>
            <td>
                <asp:DropDownList ID="ddlCourses" runat="server" Width="400px" AutoPostBack="True"></asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td  style="text-align: left">
                <asp:Button ID="btnView" runat="server" Text="View/Download" OnClick="btnView_OnClick" OnClientClick="return confirm('Please wait while we download your attendance.  This could take a few minutes.')"  />
            </td>
        </tr>
        <tr>
            <td style="text-align: center" colspan="2">
                <br/>
                <br/>
                <asp:Label ID='lblError' runat="server" ForeColor="Red" Font-Bold="True" ></asp:Label>
            </td>
        </tr>
    </table>
 
</div>

 







