﻿using System;

namespace PortalData.Demographic
{
    /// <summary>
    /// Hold the state for combobox
    /// </summary>
    public class StateOutput
    {
        public Guid StateGuid { get; set; }
        public string StateDescription { get; set; }
    }
}
