﻿using System;

namespace PortalData.Academic
{
    public class TermOutput
    {
        public Guid TermId { get; set; }
        public string Description { get; set; }
    }
}