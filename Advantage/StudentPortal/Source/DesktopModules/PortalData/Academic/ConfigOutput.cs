﻿namespace PortalData.Academic
{
    public class ConfigOutput
    {
        public string GradeCourseRepetitionsMethod { get; set; }
        public bool IncludeHoursForFailingGrade { get; set; }
        public string GradesFormat { get; set; }
        public string TranscriptType { get; set; }
        public string AddCreditsByService { get; set; }
    }
}
