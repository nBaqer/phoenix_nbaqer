﻿using System;

namespace PortalData.Academic
{
    /// <summary>
    /// Hold grade book results
    /// </summary>
    public class GradeBookResultOutput
    {
        public string Course { get; set; }
        public string Term { get; set; }
        public DateTime? ClassStartDate { get; set; }
        public DateTime? ClassEndDate { get; set; }
        public string InstructorName { get; set; }
        public string Grade { get; set; }
        public decimal? Credits { get; set; }
        public decimal? Hours { get; set; }
        public string TermId { get; set; }
        public string ClsSectionId { get; set; }
        public string StuEnrollId { get; set; } 
    }
}
