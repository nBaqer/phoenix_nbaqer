﻿namespace PortalData.Academic
{
    /// <summary>
    /// Grade book details components.
    /// </summary>
    public class GradeBookComponentsOutput
    {
        public string GradeBookComponent { get; set; }
        public string ComponentType { get; set; }
        public decimal? Score { get; set; }
        public string Weight { get; set; } 
    }
}
