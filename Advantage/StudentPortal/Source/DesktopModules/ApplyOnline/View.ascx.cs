﻿using System;
using System.Net;
using System.Net.Mail;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using DotNetNuke.Services.Exceptions;
using PortalData.Lead;
using Telerik.Web.UI;

using DotNetNuke.Security;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;

namespace Christoc.Modules.ApplyOnline
{
    public partial class View : ApplyOnlineModuleBase, IActionable
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BindDropDowns();
            lblSubmitNote.Text = @" By clicking submit, I agree that a school representative can contact me regarding educational services by phone or email.";
 
            //lblSubmitNote.Text = @" By clicking submit, I agree that " + ConfigurationManager.AppSettings["schoolName"] +
            //                     @" may email me or contact me regarding educational services by telephone and/or email by using the telephone number and email address" +
            //                     @" provided above. And I also understand this consent is not required to attend " + ConfigurationManager.AppSettings["schoolName"] + @".";

            txtPhone.MaxLength = chkInternational.Checked == false ? 10 : 16;
            txtPhoneAlt.MaxLength = chkInternationalAlt.Checked == false ? 10 : 16;
            ddlState.Enabled = chkIntlAddress.Checked == false;
            lblAsterik120.Visible = chkIntlAddress.Checked;
            lblOtherState.Visible = chkIntlAddress.Checked;
            txtOtherState.Visible = chkIntlAddress.Checked;
            if (IsPostBack) return;
            ddlCountries.SelectedText = "USA";
            ddlCountries.Enabled = false;
        }

        public ModuleActionCollection ModuleActions
        {
            get
            {
                var actions = new ModuleActionCollection
                {
                    {
                        GetNextActionID(), Localization.GetString("EditModule", LocalResourceFile), "", "", "",
                        EditUrl(), false, SecurityAccessLevel.Edit, true, false
                    }
                };
                return actions;
            }
        }

        protected void ddlCampus_SelectedIndexChanged(object sender, DropDownListEventArgs e)
        {
            ddlProgram.Items.Clear();
            ddlProgram.DataBind();

            ddlProgram.Enabled = ddlProgram.Items.Count > 0;
        }

        protected void ddlProgram_SelectedIndexChanged(object sender, DropDownListEventArgs e)
        {
            lblddlProgramErrMsg.Visible = false;
            ddlSource.DataBind();
            ddlPrevEdLvl.DataBind();
            ddlSource.Enabled = true;
            ddlPrevEdLvl.Enabled = true;
        }

        protected void btnSave1_Click(object sender, EventArgs e)
        {
            if (ddlCampus == null) return;
            lblddlCampusErrMsg.Visible = ddlCampus.SelectedText == "";
            lblddlProgramErrMsg.Visible = (lblddlCampusErrMsg.Visible == false && ddlProgram.SelectedText == "");

            if (ddlCampus.SelectedText == "" || ddlProgram.SelectedText == "") return;
            RadMultiPage1.SelectedIndex = 1;
            RadTabStrip1.SelectedIndex = RadTabStrip1.SelectedTab.Index + 1;
            RadTabStrip1.SelectedTab.Enabled = true;
        }

        protected void btnSave2_Click(object sender, EventArgs e)
        {
            lbltxtDOBErrMsg.Text = "";

            lbltxtFNameErrMsg.Visible = txtFName.Text == string.Empty;
            lbltxtLNameErrMsg.Visible = txtLName.Text == string.Empty;
            lbltxtEmailErrMsg.Visible = (IsValidEmailAddress(txtEmail.Text) == false);
            if (txtEmailAlt.Text != string.Empty)
            {
                var emailAlt = IsValidEmailAddress(txtEmailAlt.Text);
                if (emailAlt == false)
                {
                    txtEmailAlt.Text = "";
                }
            }

            if (txtDOB.SelectedDate.HasValue)
                lbltxtDOBErrMsg.Visible = (IsValidDateOfBirth(txtDOB.SelectedDate.Value, ddlCampus.SelectedValue) == false);
            else
            {
                lbltxtDOBErrMsg.Text = @"Date of Birth is required.";
                lbltxtDOBErrMsg.Visible = true;
            }
            lblddlGenderErrMsg.Visible = ddlGender.SelectedText == string.Empty;
            lbltxtPhoneErrMsg.Visible = txtPhone.Text == string.Empty;
            lbltxtAdd1ErrMsg.Visible = txtAdd1.Text == string.Empty;
            lbltxtCityErrMsg.Visible = txtCity.Text == string.Empty;
            lblddlStateErrMsg.Visible = ddlState.SelectedText == string.Empty;
            if (txtOtherState.Visible)
            {
                lbltxtOtherStateErrMsg.Visible = txtOtherState.Text == string.Empty;
            }
            lbltxtZipErrMsg.Visible = txtZip.Text == string.Empty;

            if (txtFName.Text == string.Empty || txtLName.Text == string.Empty || lbltxtEmailErrMsg.Visible ||
                lbltxtDOBErrMsg.Visible || lblddlGenderErrMsg.Visible || txtPhone.Text == string.Empty ||
                txtAdd1.Text == string.Empty || txtCity.Text == string.Empty || ddlState.SelectedText == string.Empty ||
                txtZip.Text == string.Empty) return;
            RadMultiPage1.SelectedIndex = 2;
            RadTabStrip1.SelectedIndex = RadTabStrip1.SelectedTab.Index + 1;
            RadTabStrip1.SelectedTab.Enabled = true;
        }

        protected void btnSave3_Click(object sender, EventArgs e)
        {
            RadMultiPage1.SelectedIndex = 3;
            RadTabStrip1.SelectedIndex = RadTabStrip1.SelectedTab.Index + 1;
            RadTabStrip1.SelectedTab.Enabled = true;
        }

        protected void btnSave4_Click(object sender, EventArgs e)
        {
            btnSave3.Enabled = false;
            lblDateErrMsg.Text = string.Empty;
            lblLeadInsertStatus.Text = string.Empty;

            if (rdtPicker.SelectedDate.ToString() != string.Empty)
            {
                var selectedDateTime = Convert.ToDateTime(rdtPicker.SelectedDate);
                var currentDateTime = Convert.ToDateTime(DateTime.Now);
                var validTime = DateTime.Compare(selectedDateTime, currentDateTime);

                if (validTime < 0)
                {
                    lblDateErrMsg.Text = @"Please select your preferred appointment date & time later than current day.";
                    lblDateErrMsg.Visible = true;
                    btnSave3.Enabled = true;
                }
                else
                {
                    InsertLead();
                }
            }

            else
            {
                InsertLead();
            }
        }

        /// <summary>
        /// Insert a new lead.
        /// </summary>
        protected void InsertLead()
        {
            DateTime? dob = (txtDOB.SelectedDate == null) ? (DateTime?)null : txtDOB.SelectedDate.Value;
            var info = new LeadInfo
            {
                CampusId = ddlCampus.SelectedValue,
                ProgId = ddlProgram.SelectedValue,
                SourceTypeId = ddlSource.SelectedValue,
                PrevEduLvlId = ddlPrevEdLvl.SelectedValue,
                FirstName = txtFName.Text,
                MiddleName = txtMName.Text,
                LastName = txtLName.Text,
                BirthDate = dob,
                GenderId = ddlGender.SelectedValue,
                PhoneNumber = txtPhone.Text,
                ForeignPhone = chkInternational.Checked,
                PhoneNumber2 = txtPhoneAlt.Text,
                ForeignPhone2 = chkInternationalAlt.Checked,
                Email = txtEmail.Text,
                AlternativeEmail = txtEmailAlt.Text,
                Address1 = txtAdd1.Text,
                Address2 = txtAdd2.Text,
                City = txtCity.Text,
                StateId = ddlState.SelectedValue,
                OtherState = txtOtherState.Text,
                Zip = txtZip.Text,
                ForeignZip = chkIntlAddress.Checked,
                CountyId = ddlCounties.SelectedValue,
                CountryId = ddlCountries.SelectedValue,
                RaceId = ddlEthCode.SelectedValue,
                NationalityId = ddlNationality.SelectedValue,
                Citizen = ddlCitizen.SelectedValue,
                MaritalStatus = ddlMaritalStatus.SelectedValue,
                FamilyIncome = ddlFamilyIncome.SelectedValue,
                Children = txtDependents.Text,
                DrivLicStateID = ddlDrLicState.SelectedValue,
                DrivLicNumber = txtDrLicNumber.Text,
                Comments = txtComments.Text
            };
            lblLeadInsertStatus.Text = PortalFacade.LeadFacade.InsertLeadInPortal(info);

            if (lblLeadInsertStatus.Text.Contains("You will receive a confirmation email shortly"))
            {
                btnSave1.Enabled = false;
                btnSave2.Enabled = false;
                btnSave3.Enabled = false;
                btnSave4.Enabled = false;
                lblLeadInsertStatus.Visible = true;
                SendEmail(ddlCampus.SelectedValue);
            }
            else
            {
                lblLeadInsertStatus.Visible = true;
                btnSave3.Enabled = true;
            }
        }

        public bool IsValidEmailAddress(string email)
        {
            if (string.IsNullOrEmpty(email))
                return false;
            var regex = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
            return regex.IsMatch(email) && !email.EndsWith(".");
        }

        public bool IsValidDateOfBirth(DateTime dob, string campusId)
        {
            var constring = ConfigurationManager.ConnectionStrings["ClientConnectionString"].ConnectionString;

            using (var clConDob = new SqlConnection(constring))
            {
                var cmdDob =
                    new SqlCommand(" SELECT TOP 1 AppValue.Value FROM dbo.syConfigAppSettings AppSetting " +
                                   " INNER JOIN syConfigAppSetValues AppValue" +
                                   " ON AppSetting.SettingId=AppValue.SettingId" +
                                   " WHERE AppSetting.KeyName='StudentAgeLimit' " +
                                   " AND (CampusId=@CampusId OR CampusId IS NULL) ", clConDob);
                var adpDob = new SqlDataAdapter { SelectCommand = cmdDob };
                cmdDob.Parameters.AddWithValue("@CampusId", campusId);
                var dsDob = new DataSet();

                try
                {
                    clConDob.Open();
                    adpDob.Fill(dsDob, "StudentAgeLimit");

                    var ageLimit = Convert.ToInt32(dsDob.Tables[0].Rows[0].Field<string>(0));

                    var age = DateTime.Now.Year - dob.Year - (DateTime.Now.DayOfYear < dob.DayOfYear ? 1 : 0);

                    if (age >= ageLimit)
                        return true;
                    else
                    {
                        lbltxtDOBErrMsg.Text = @" You did not meet the age requirement. The minimum age required is " + ageLimit + @" or greater.";
                        return false;
                    }
                }
                catch (Exception)
                {
                    Response.Write("<script language='javascript'>alert('SQL Error');</script>");
                }
                finally
                {
                    clConDob.Close();
                }
            }
            return false;
        }

        protected void SendEmail(string campusId)
        {
            var portalContactEmail = string.Empty;
            var emailSubject = string.Empty;
            var emailBody = string.Empty;

            var constring = ConfigurationManager.ConnectionStrings["ClientConnectionString"].ConnectionString;
            using (var clCon = new SqlConnection(constring))
            {
                var cmd = new SqlCommand(" SELECT PortalContactEmail,EmailSubject,EmailBody FROM syCampuses WHERE CampusId=@CampusId ", clCon);
                var adp = new SqlDataAdapter { SelectCommand = cmd };
                cmd.Parameters.AddWithValue("@CampusId", campusId);
                var ds = new DataSet();

                try
                {
                    clCon.Open();
                    adp.Fill(ds, "PortalEmail");

                    portalContactEmail = ds.Tables[0].Rows[0].Field<string>(0);
                    emailSubject = ds.Tables[0].Rows[0].Field<string>(1);
                    emailBody = ds.Tables[0].Rows[0].Field<string>(2);

                    adp.Dispose();
                    cmd.Dispose();
                }
                catch (Exception)
                {
                    Response.Write("<script language='javascript'>alert('SQL Error');</script>");
                }
                finally
                {
                    clCon.Close();
                }

                /*  Send Email to School */

                using (var mailMessage = new MailMessage(portalContactEmail, portalContactEmail))
                {
                    mailMessage.Subject = "New Lead Information Received from " + txtFName.Text + " " + txtLName.Text + " at " +
                                          DateTime.Now;

                    mailMessage.Body = (" \r\n " + "New Lead information has been saved to Advantage for : " +
                                        " \r\n \r\n" + " Full Name : " + txtFName.Text + " " + txtLName.Text);

                    if (!String.IsNullOrEmpty(txtAdd1.Text))
                    {
                        mailMessage.Body = mailMessage.Body + " \r\n " + "Address 1 : " + txtAdd1.Text;
                    }
                    if (!String.IsNullOrEmpty(txtAdd2.Text))
                    {
                        mailMessage.Body = mailMessage.Body + " \r\n " + "Address 2 : " + txtAdd2.Text;
                    }
                    if (!String.IsNullOrEmpty(txtCity.Text))
                    {
                        mailMessage.Body = mailMessage.Body + " \r\n " + "City : " + txtCity.Text;
                    }
                    if (!String.IsNullOrEmpty(ddlState.SelectedText))
                    {
                        mailMessage.Body = mailMessage.Body + " \r\n " + "State : " + ddlState.SelectedText;
                    }
                    if (!String.IsNullOrEmpty(txtOtherState.Text))
                    {
                        mailMessage.Body = mailMessage.Body + " \r\n " + "Other State : " + txtOtherState.Text;
                    }
                    if (!String.IsNullOrEmpty(txtZip.Text))
                    {
                        mailMessage.Body = mailMessage.Body + " \r\n " + "Zip : " + txtZip.Text;
                    }
                    if (!String.IsNullOrEmpty(ddlCountries.SelectedText))
                    {
                        mailMessage.Body = mailMessage.Body + " \r\n " + "Country : " + ddlCountries.SelectedText;
                    }
                    if (chkIntlAddress.Checked)
                    {
                        mailMessage.Body = mailMessage.Body + " \r\n " + "Is Address International? : Yes";
                    }
                    if (chkIntlAddress.Checked == false)
                    {
                        mailMessage.Body = mailMessage.Body + " \r\n " + "Is Address International? : No";
                    }

                    if (!String.IsNullOrEmpty(txtPhone.Text))
                    {
                        mailMessage.Body = mailMessage.Body + " \r\n  \r\n" + " Primary Phone Number: " + txtPhone.Text;
                    }

                    if (chkInternational.Checked)
                    {
                        mailMessage.Body = mailMessage.Body + " \r\n " + "Is Primary Phone International? : Yes";
                    }
                    if (chkInternational.Checked == false)
                    {
                        mailMessage.Body = mailMessage.Body + " \r\n " + "Is Primary Phone International? : No";
                    }
                    if (!String.IsNullOrEmpty(txtPhoneAlt.Text))
                    {
                        mailMessage.Body = mailMessage.Body + " \r\n  \r\n" + " Alternate Phone Number: " + txtPhoneAlt.Text;
                    }
                    if (chkInternationalAlt.Checked)
                    {
                        mailMessage.Body = mailMessage.Body + " \r\n " + "Is Alternate Phone International? : Yes";
                    }
                    if (chkInternationalAlt.Checked == false)
                    {
                        mailMessage.Body = mailMessage.Body + " \r\n " + "Is Alternate Phone International? : No";
                    }

                    mailMessage.Body = mailMessage.Body + " \r\n \r\n" + "Email : " + txtEmail.Text;
                    mailMessage.Body = mailMessage.Body + " \r\n \r\n" + "Program : " + ddlProgram.SelectedText;
                    mailMessage.Body = mailMessage.Body + " \r\n " + "Campus : " + ddlCampus.SelectedText;

                    if (!String.IsNullOrEmpty(rdtPicker.SelectedDate.ToString()))
                    {
                        mailMessage.Body = mailMessage.Body + " \r\n \r\n " + "Appointment scheduled on  : " +
                                           rdtPicker.SelectedDate;
                    }

                    mailMessage.IsBodyHtml = false;
                    var smtp = new SmtpClient
                    {
                        Host = ConfigurationManager.AppSettings["SMTPHost"],
                        EnableSsl = true
                    };
                    var networkCred = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"], ConfigurationManager.AppSettings["SMTPPassword"]);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = networkCred;
                    smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["port"]);
                    smtp.Send(mailMessage);
                }

                /*  Send Email to Lead */

                using (var mailMessage = new MailMessage(portalContactEmail, txtEmail.Text))
                {

                    mailMessage.Subject = (!String.IsNullOrEmpty(emailSubject)) ? emailSubject : ConfigurationManager.AppSettings["schoolName"] + " - Your information has been received";
                    mailMessage.Body = (!String.IsNullOrEmpty(emailBody))
                         ? "Dear " + txtFName.Text + " " + txtLName.Text + ", \r\n \r\n " + emailBody
                         : "Dear " + txtFName.Text + " " + txtLName.Text + ", \r\n \r\n Your information has been received. Our school associate will contact you.";

                    mailMessage.IsBodyHtml = false;
                    var smtpLead = new SmtpClient
                    {
                        Host = ConfigurationManager.AppSettings["SMTPHost"],
                        EnableSsl = true
                    };
                    var networkCredLead = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"], ConfigurationManager.AppSettings["SMTPPassword"]);
                    smtpLead.UseDefaultCredentials = true;
                    smtpLead.Credentials = networkCredLead;
                    smtpLead.Port = Convert.ToInt32(ConfigurationManager.AppSettings["port"]);
                    smtpLead.Send(mailMessage);
                }
            }
        }

        protected void chkIntlPhone_CheckedChanged(object sender, EventArgs e)
        {
            txtPhone.Text = string.Empty;
            txtPhone.MaxLength = chkInternational.Checked == false ? 10 : 16;
        }

        protected void chkInternationalAlt_CheckedChanged(object sender, EventArgs e)
        {
            txtPhoneAlt.Text = string.Empty;
            txtPhoneAlt.MaxLength = chkInternationalAlt.Checked == false ? 10 : 16;
        }

        protected void chkIntlAddress_CheckedChanged(object sender, EventArgs e)
        {
            ddlCountries.DefaultMessage = "--Select--";
            ddlState.DefaultMessage = "--Select--";
            ddlCountries.SelectedIndex = -1;
            ddlState.SelectedIndex = -1;
            txtOtherState.Text = string.Empty;
            ddlState.Enabled = chkIntlAddress.Checked == false;
            lblAsterik120.Visible = chkIntlAddress.Checked;
            lblOtherState.Visible = chkIntlAddress.Checked;
            txtOtherState.Visible = chkIntlAddress.Checked;
            ddlCountries.Enabled = chkIntlAddress.Checked;
            ddlCounties.Enabled = !chkIntlAddress.Checked;
            ddlState.SelectedIndex = ddlState.Enabled ? -1 : ddlState.FindItemByText("Other State").Index;
            ddlCountries.SelectedIndex = ddlCountries.Enabled ? -1 : ddlCountries.FindItemByText("usa".ToUpper()).Index;
        }
        #region Private

        /// <summary>
        /// Get data for dropdown controls and bind the data
        /// </summary>
        private void BindDropDowns()
        {
            try
            {
                // Country
                ddlCountries.DataTextField = "CountryDescription";
                ddlCountries.DataValueField = "CountryGuid";
                var countries = PortalFacade.DemographicFacade.GetActiveCountries();
                ddlCountries.DataSource = countries;
                ddlCountries.DataBind();

                // State
                ddlState.DataTextField = "StateDescription";
                ddlState.DataValueField = "StateGuid";
                var states = PortalFacade.DemographicFacade.GetActiveStatesAndOtherState();
                ddlState.DataSource = states;
                ddlState.DataBind();

                // License State
                ddlDrLicState.DataTextField = "StateDescription";
                ddlDrLicState.DataValueField = "StateGuid";
                ddlDrLicState.DataSource = states;
                ddlDrLicState.DataBind();

                //County
                ddlCounties.DataTextField = "CountyDescription";
                ddlCounties.DataValueField = "CountyGuid";
                var counties = PortalFacade.DemographicFacade.GetActiveCounties();
                ddlCounties.DataSource = counties;
                ddlCounties.DataBind();
                
            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException("An error occured while attempting to retrieve the filter options", this, ex);
            }
        }

        #endregion

    }
}

