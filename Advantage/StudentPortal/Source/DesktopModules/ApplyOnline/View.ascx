﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Christoc.Modules.ApplyOnline.View" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>

<script type="text/javascript" id="telerikClientEvents1">

    function txtPhone_OnKeyPress(sender, args) {
        var regexp = new RegExp("[0-9 \b]{1}");
        if (!args.get_keyCharacter().match(regexp))
            args.set_cancel(true);
    }

    function txtZip_OnKeyPress(sender, args) {
        var regexp = new RegExp("[a-zA-Z0-9 \b]");
        if (!args.get_keyCharacter().match(regexp))
            args.set_cancel(true);
    }

</script>

<div>
    <telerik:RadAjaxManager ID="RadAjaxManager1" runat="server">
        <ajaxsettings>
            <telerik:AjaxSetting AjaxControlID="RadMultiPage1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadTabStrip1" />
                    <telerik:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="RadTabStrip1">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="RadTabStrip1" />
                    <telerik:AjaxUpdatedControl ControlID="RadMultiPage1" />
                </UpdatedControls>
            </telerik:AjaxSetting>
        </ajaxsettings>
    </telerik:RadAjaxManager>
</div>
<div>
    <h2>Apply Online</h2>
    <p>
        <span style="font-size: 14px;" >Thank you for considering our school for your educational needs. Please complete the below given form for applying online so that one of our school associate will contact you via phone or email.</span>
        <br/><br/>
    </p>
</div>
<div class="tabStripWrapper">
    <telerik:RadTabStrip ID="RadTabStrip1" SelectedIndex="0" runat="server" MultiPageID="RadMultiPage1" Align="Justify" Skin="Metro" CausesValidation="False">
        <tabs>
        <telerik:RadTab runat="server" PageViewID="RadPageView1" Text="Program & Area of Interest" TabIndex="0" Font-Bold="True" Font-Size="Larger">
        </telerik:RadTab>
        <telerik:RadTab runat="server" PageViewID="RadPageView2" Text="Contact Information" TabIndex="1" Font-Bold="True" Font-Size="Larger" Enabled="False">
        </telerik:RadTab>
            <telerik:RadTab runat="server" PageViewID="RadPageView3" Text="Additional Details" TabIndex="2" Font-Bold="True" Font-Size="Larger" Enabled="False">
        </telerik:RadTab>
        <telerik:RadTab runat="server" PageViewID="RadPageView4" Text="Make an Appointment" TabIndex="3" Font-Bold="True" Font-Size="Larger" Enabled="False">
        </telerik:RadTab>
    </tabs>
    </telerik:RadTabStrip>
    <telerik:RadMultiPage ID="RadMultiPage1" Runat="server" SelectedIndex="0">
        <telerik:RadPageView ID="RadPageView1" runat="server" CssClass="mainLayout">
            <table>
                <tr>
                    <td>
                        <br />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblReqFlds1" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="Red">* Indicates Required Fields </asp:Label></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding-left: 5em">
                        <asp:Label ID="lblAsterik1" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red">* </asp:Label>
                        <asp:Label ID="lblCampus" runat="server" Font-Bold="True" ForeColor="Black" Font-Names="Arial" Font-Size="10pt"> What is your preferred campus location? </asp:Label></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding-left: 5em">&nbsp; &nbsp;
                        <telerik:RadDropDownList ID="ddlCampus" runat="server" AutoPostBack="True" DataSourceID="dsCampus" DataTextField="CampusDescrip" DataValueField="CampusId" DefaultMessage="--Select--" Enabled="True" OnSelectedIndexChanged="ddlCampus_SelectedIndexChanged" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Skin="Metro" Width="200px"></telerik:RadDropDownList>
                        &nbsp;&nbsp;
                        <asp:Label ID="lblddlCampusErrMsg" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="Red" Visible="False"> Campus selection is required. </asp:Label>
                        <asp:SqlDataSource ID="dsCampus" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>" SelectCommand="usp_AdvPortalCampuses" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding-left: 5em">
                        <asp:Label ID="lblAsterik2" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red">* </asp:Label>
                        <asp:Label ID="lblProgram" runat="server" Font-Bold="True" Font-Names="Arial" ForeColor="Black" Font-Size="10pt"> Which program are you interested in? </asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding-left: 5em">&nbsp; &nbsp;
                        <telerik:RadDropDownList ID="ddlProgram" runat="server" AutoPostBack="True" DataSourceID="dsProgram" DataTextField="ProgDescrip" DataValueField="ProgId" DefaultMessage="--Select--" DropDownHeight="200px" Enabled="False" ExpandDirection="Down" OnSelectedIndexChanged="ddlProgram_SelectedIndexChanged" Width="400px" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Skin="Metro"></telerik:RadDropDownList>
                        &nbsp;&nbsp;
                        <asp:Label ID="lblddlProgramErrMsg" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="Red" Visible="False"> Program selection is required. </asp:Label>
                        <asp:SqlDataSource ID="dsProgram" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>" SelectCommand=" SELECT DISTINCT Program.ProgId,Program.ProgDescrip FROM syCmpGrpCmps CmpGrp
                                            INNER JOIN arPrgGrp PrgGrp ON CmpGrp.CampGrpId = PrgGrp.CampGrpId
                                            INNER JOIN arPrgVersions PrgVer ON PrgGrp.PrgGrpId=PrgVer.PrgGrpId
                                            INNER JOIN arPrograms Program ON PrgVer.ProgId=Program.ProgId
                                            INNER JOIN syStatuses Statuses ON Program.StatusId=Statuses.StatusId
                                            WHERE Statuses.Status='Active' AND CmpGrp.CampusId=@CampusId 
                                            UNION
                                            SELECT '00000000-0000-0000-0000-000000000000' AS ProgId, 'Unknown' AS ProgDescrip
                                            ORDER BY ProgDescrip">
                            <SelectParameters>
                                <asp:ControlParameter ControlID="ddlCampus" Name="CampusId" PropertyName="SelectedValue" />
                            </SelectParameters>
                        </asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding-left: 5em">
                        <asp:Label ID="lblSource" runat="server" Font-Bold="True" ForeColor="Black" Font-Names="Arial" Font-Size="10pt">&nbsp; How did you hear about us? </asp:Label></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding-left: 5em">&nbsp;
                        <telerik:RadDropDownList ID="ddlSource" runat="server" AutoPostBack="True" DataSourceID="dsSource" 
                            DataTextField="SourceTypeDescrip" DataValueField="SourceTypeId" DefaultMessage="--Select--" 
                            DropDownHeight="200px" DropDownWidth="250px" ExpandDirection="Down" Enabled="False" Width="250px"
                             Font-Names="Arial" Font-Size="10pt" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" 
                            Skin="Metro">
                        </telerik:RadDropDownList>
                        <asp:SqlDataSource ID="dsSource" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>
                            "
                            SelectCommand=" SELECT DISTINCT SourceTypeId,SourceTypeDescrip FROM adSourceType SrcType
                                                INNER JOIN syStatuses Statuses ON SrcType.StatusId=Statuses.StatusId
                                                WHERE Statuses.Status='Active' ORDER BY SourceTypeDescrip"></asp:SqlDataSource>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding-left: 5em">&nbsp;
                        <asp:Label ID="lblPrevEdLvl" runat="server" Font-Bold="True" ForeColor="Black" Font-Names="Arial" Font-Size="10pt"> What is your current education level? </asp:Label>
                    </td>
                    </tr>
                <tr>
                    <td>&nbsp;</td>
                    </tr>
                <tr>
                    <td style="padding-left: 5em">&nbsp;
                            <telerik:RadDropDownList ID="ddlPrevEdLvl" runat="server" AutoPostBack="True" DataSourceID="dsEducation"
                                DataTextField="EdLvlDescrip" DataValueField="EdLvlID" DefaultMessage="--Select--" DropDownHeight="100px"
                                DropDownWidth="175px" ExpandDirection="Down" Enabled="False" Width="175px" Font-Names="Arial"
                                Font-Size="10pt" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Skin="Metro">
                            </telerik:RadDropDownList>
                        <asp:SqlDataSource ID="dsEducation" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>" SelectCommand=" SELECT DISTINCT PR.EdLvlID,PR.EdLvlDescrip FROM adEdLvls PR, syStatuses ST 
                                            WHERE PR.StatusId = ST.StatusId AND ST.Status = 'Active'
                                            ORDER BY PR.EdLvlDescrip"></asp:SqlDataSource>
                    </td>
                    </tr>
                <tr>
                    <td>&nbsp;</td>
                    </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr style="text-align: left">
                    <td style="padding-left: 60em">
                        <asp:Button ID="btnSave1" runat="server" CssClass="btnSave" OnClick="btnSave1_Click" Text="Save &amp; Continue" Font-Size="Small" Skin="Metro" />
                    </td>
                </tr>

            </table>
        </telerik:RadPageView>
        <telerik:RadPageView ID="RadPageView2" runat="server" CssClass="mainLayout">
            <table>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lblReqFlds2" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="Red">* Indicates Required Fields </asp:Label></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <div style="padding-left: 5em;">
                <table>
                    <tr>
                        <td style="width: 2000px">
                            <asp:Label ID="lblAsterik3" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red">* </asp:Label>
                            <asp:Label ID="lblFName" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> First Name: </asp:Label></td>
                        <td style="width: 500px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td style="width: 2000px">
                            <telerik:RadTextBox ID="txtFName" Runat="server" MaxLength="50" Width="250px" Wrap="False" Font-Names="Arial" Font-Size="10pt" Font-Bold="True" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></telerik:RadTextBox>
                            <asp:Label ID="lbltxtFNameErrMsg" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="Red" Visible="False"> First Name is required. </asp:Label></td>
                        <td style="width: 500px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 2000px">
                            <asp:Label ID="lblMName" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Middle Name: </asp:Label>
                        </td>
                        <td style="width: 500px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td style="width: 2000px">
                            <telerik:RadTextBox ID="txtMName" Runat="server" MaxLength="50" Width="200px" Wrap="False" Font-Names="Arial" Font-Size="10pt" Font-Bold="True" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></telerik:RadTextBox>
                        </td>
                        <td style="width: 500px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 2000px">
                            <asp:Label ID="lblAsterik4" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red">* </asp:Label>
                            <asp:Label ID="lblLName" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Last Name: </asp:Label></td>
                        <td style="width: 500px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td style="width: 2000px">
                            <telerik:RadTextBox ID="txtLName" Runat="server" MaxLength="50" Width="250px" Wrap="False" Font-Names="Arial" Font-Size="10pt" Font-Bold="True" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></telerik:RadTextBox>
                            <asp:Label ID="lbltxtLNameErrMsg" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="Red" Visible="False"> Last Name is required. </asp:Label></td>
                        <td style="width: 500px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 2000px">
                            <asp:Label ID="lblAsterik5" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red">* </asp:Label>
                            <asp:Label ID="lblDOB" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Date of Birth: </asp:Label>
                            <asp:Label ID="lblDOBEx" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="8pt" ForeColor="Blue"> (Ex: MM/DD/YYYY) </asp:Label>
                        </td>
                        <td style="width: 500px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td style="width: 2000px">
                            <telerik:RadDatePicker ID="txtDOB" Runat="server" Culture="en-US" MaxLength="10" MinDate="1900-01-01" Skin="Metro" Width="200px" Font-Names="Arial" Font-Size="10pt" Font-Bold="True" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></telerik:RadDatePicker>
                            <asp:Label ID="lbltxtDOBErrMsg" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="Red" Visible="False"> Date of Birth is required. </asp:Label></td>
                        <td style="width: 500px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 2000px">
                            <asp:Label ID="lblAsterik6" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red">* </asp:Label>
                            <asp:Label ID="lblGender" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Gender: </asp:Label></td>
                        <td style="width: 500px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td style="width: 2000px">
                            <telerik:RadDropDownList ID="ddlGender" runat="server" DataSourceID="dsGender" DataTextField="GenderDescrip" DataValueField="GenderId" DefaultMessage="--Select--" DropDownHeight="75px" ExpandDirection="Down" Width="200px" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Skin="Metro"></telerik:RadDropDownList>
                            <asp:SqlDataSource ID="dsGender" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>
                                "
                                SelectCommand=" SELECT DISTINCT GenderId,GenderDescrip
                                                    FROM dbo.adGenders Gender
                                                    INNER JOIN dbo.syStatuses Statuses
                                                    ON Statuses.StatusId = Gender.StatusId
                                                    WHERE Statuses.Status='Active'
                                                    ORDER BY GenderDescrip"></asp:SqlDataSource>
                            <asp:Label ID="lblddlGenderErrMsg" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="Red" Visible="False"> Gender is required. </asp:Label></td>
                        <td style="width: 500px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 2000px">
                            <asp:Label ID="lblAsterik7" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red">* </asp:Label>
                            <asp:Label ID="lblPhone" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Primary Phone Number: </asp:Label>
                            <asp:Label ID="lblPhoneEx" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="8pt" ForeColor="Blue"> (Ex: 9999999999) </asp:Label>
                        </td>
                        <td style="width: 500px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td style="width: 2000px">
                            <telerik:RadTextBox ID="txtPhone" Runat="server" Skin="Metro" Width="200px" Wrap="False" Font-Names="Arial" Font-Size="10pt" Font-Bold="True" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                <clientevents onkeypress="txtPhone_OnKeyPress" />
                            </telerik:RadTextBox>
                            &nbsp;&nbsp;
                            <asp:CheckBox ID="chkInternational" runat="server" Font-Names="Arial" Font-Size="10pt" ForeColor="Blue" BackColor="White" BorderStyle="None" Text="Check if International (Non-USA) Phone" ToolTip="Check if international only" AutoPostBack="True" OnCheckedChanged="chkIntlPhone_CheckedChanged" />
                            &nbsp;&nbsp;
                            <asp:Label ID="lbltxtPhoneErrMsg" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="Red" Visible="False"> Primary Phone number is required. </asp:Label>
                        </td>
                        <td style="width: 500px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 2000px">
                            <asp:Label ID="lblPhoneAlt" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Alternate Phone Number: </asp:Label>
                            <asp:Label ID="lblAltPhoneEx" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="8pt" ForeColor="Blue"> (Ex: 9999999999) </asp:Label></td>
                        <td style="width: 500px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td style="width: 2000px">
                            <telerik:RadTextBox ID="txtPhoneAlt" Runat="server" Skin="Metro" Width="200px" Wrap="False" Font-Names="Arial" Font-Size="10pt" Font-Bold="True" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                <clientevents onkeypress="txtPhone_OnKeyPress" />
                            </telerik:RadTextBox>
                            &nbsp;&nbsp;
                            <asp:CheckBox ID="chkInternationalAlt" runat="server" Font-Names="Arial" Font-Size="10pt" ForeColor="Blue" BackColor="White" BorderStyle="None" Text="Check if International (Non-USA) Phone" ToolTip="Check if international only" AutoPostBack="True" OnCheckedChanged="chkInternationalAlt_CheckedChanged" />
                        </td>
                        <td style="width: 500px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 2000px">
                            <asp:Label ID="lblAsterik8" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red">* </asp:Label>
                            <asp:Label ID="lblEmail" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Primary Email Address: </asp:Label></td>
                        <td style="width: 500px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td style="width: 2000px">
                            <telerik:RadTextBox ID="txtEmail" Runat="server" MaxLength="50" Skin="Metro" Width="250px" Wrap="False" Font-Names="Arial" Font-Size="10pt" Font-Bold="True" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                            </telerik:RadTextBox>
                            <asp:Label ID="lbltxtEmailErrMsg" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="Red" Visible="False"> Valid Email Address is required. </asp:Label></td>
                        <td style="width: 500px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 2000px">
                            <asp:Label ID="lblEmailAlt" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Alternate Email Address: </asp:Label></td>
                        <td style="width: 500px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td style="width: 2000px">
                            <telerik:RadTextBox ID="txtEmailAlt" Runat="server" MaxLength="50" Skin="Metro" Width="250px" Wrap="False" Font-Names="Arial" Font-Size="10pt" Font-Bold="True" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                            </telerik:RadTextBox>
                        </td>
                        <td style="width: 500px"></td>
                        <td style="width: 150px"></td>
                        <td style="width: 150px"></td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                        </td>
                    </tr>
                </table>
            </div>
            <div style="padding-left: 5em;">
                <table>
                    <tr>
                        <td style="width: 800px">
                            <asp:Label ID="lblAsterik9" runat="server" Font-Size="12pt" Font-Bold="False" ForeColor="Red" Font-Names="Arial">* </asp:Label>
                            <asp:Label ID="lblAdd1" runat="server" Font-Size="10pt" Font-Bold="True" Font-Names="Arial" ForeColor="Black"> Address 1 : </asp:Label>
                            &nbsp;&nbsp;
                            <asp:CheckBox ID="chkIntlAddress" runat="server" Font-Names="Arial" Font-Size="10pt" ForeColor="Blue" BackColor="White" BorderStyle="None" Text="Check if International (Non-USA) Address" ToolTip="Check if international only" AutoPostBack="True" OnCheckedChanged="chkIntlAddress_CheckedChanged" />
                        </td>
                        <td style="width: 500px">
                            <asp:Label ID="lblAdd2" runat="server" Font-Size="10pt" Font-Bold="True" Font-Names="Arial" ForeColor="Black"> Address 2 : </asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 800px">
                            <telerik:RadTextBox ID="txtAdd1" Runat="server" MaxLength="50" Skin="Metro" Width="325px" Wrap="False" Font-Names="Arial" Font-Size="10pt" Font-Bold="True" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></telerik:RadTextBox>
                            <asp:Label ID="lbltxtAdd1ErrMsg" runat="server" Font-Size="8pt" ForeColor="Red" Font-Names="Arial" Visible="False"> Address 1 is required. </asp:Label></td>
                        <td style="width: 500px">
                            <telerik:RadTextBox ID="txtAdd2" Runat="server" Skin="Metro" Width="325px" Wrap="False" Font-Names="Arial" Font-Size="10pt" Font-Bold="True" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></telerik:RadTextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <p>
                <br />
            </p>
            <div style="padding-left: 5em;">
                <table>
                    <tr>
                        <td style="width: 500px">
                            <asp:Label ID="lblAsterik10" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red">* </asp:Label>
                            <asp:Label ID="lblCity" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> City : </asp:Label></td>
                        <td style="width: 500px">
                            <asp:Label ID="lblAsterik11" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red">* </asp:Label>
                            <asp:Label ID="lblState" runat="server" Font-Bold="True" Font-Size="10pt" ForeColor="Black"> State : </asp:Label></td>
                        <td style="width: 500px">
                            <asp:Label ID="lblAsterik120" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red" Visible="False">* </asp:Label>
                            <asp:Label ID="lblOtherState" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" Visible="False"> Specify State : </asp:Label>
                    </tr>
                    <tr>
                        <td style="width: 500px">
                            <telerik:RadTextBox ID="txtCity" Runat="server" MaxLength="50" Skin="Metro" Width="200px" Wrap="False" Font-Names="Arial" Font-Size="10pt" Font-Bold="True" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></telerik:RadTextBox>
                            <asp:Label ID="lbltxtCityErrMsg" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="Red" Visible="False"> City is required. </asp:Label></td>
                        <td style="width: 500px">
                            <telerik:RadDropDownList ID="ddlState" runat="server"  
                                DefaultMessage="--Select--" DropDownHeight="200px" DropDownWidth="200px" ExpandDirection="Down"
                                 Width="200px" Font-Names="Arial" 
                                Font-Size="10pt" Font-Bold="True" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" >
                            </telerik:RadDropDownList>
                            <asp:Label ID="lblddlStateErrMsg" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="Red" Visible="False"> State is required. </asp:Label></td>
                        <td style="width: 500px">
                            <telerik:RadTextBox ID="txtOtherState" Runat="server" Width="175px" MaxLength="50" Wrap="False" Font-Names="Arial" Font-Size="10pt" Font-Bold="True" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Visible="False"></telerik:RadTextBox>
                            <asp:Label ID="lbltxtOtherStateErrMsg" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="Red" Visible="False"> State is required. </asp:Label></td>
                    </tr>
                    <tr>
                        <td>&nbsp; </td>
                    </tr>

                    <tr>
                        <td style="width: 500px">
                            <asp:Label ID="lblAsterik12" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red">* </asp:Label>
                            <asp:Label ID="lblZip" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Zip : </asp:Label></td>
                        <td style="width: 500px">
                            <asp:Label ID="lblCounty" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> County : </asp:Label></td>
                        <td style="width: 500px">
                            <asp:Label ID="lblCountry" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Country : </asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 500px">
                            <telerik:RadTextBox ID="txtZip" Runat="server" Skin="Metro" Width="200px" MaxLength="10" Wrap="False" Font-Names="Arial" Font-Size="10pt" Font-Bold="True" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                <clientevents onkeypress="txtZip_OnKeyPress" />
                            </telerik:RadTextBox>
                            <asp:Label ID="lbltxtZipErrMsg" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="Red" Visible="False"> Zip is required. </asp:Label></td>
                        <td>
                            <telerik:RadDropDownList ID="ddlCounties" runat="server"  
                                DefaultMessage="--Select--" DropDownHeight="100px" DropDownWidth="200px" 
                                ExpandDirection="Down" Width="200px" Font-Names="Arial" Font-Size="10pt" Font-Bold="True"
                                 ForeColor="Black" BorderColor="Black"
                                 BorderStyle="Solid" BorderWidth="1px" >
                            </telerik:RadDropDownList>
                        </td>
                        <td>
                            <telerik:RadDropDownList ID="ddlCountries" runat="server" DefaultMessage="--Select--" DropDownHeight="100px"
                                 DropDownWidth="200px" ExpandDirection="Down" Width="200px" Font-Names="Arial" 
                                Font-Size="10pt" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                             </telerik:RadDropDownList>
                       </td>
                    </tr>
                    <tr>
                        <td>&nbsp; </td>
                    </tr>
                    <tr>
                        <td>&nbsp; </td>
                    </tr>
                </table>
            </div>
            <div style="padding-left: 60em;">
                <asp:Button ID="btnSave2" runat="server" CssClass="btnSave" Enabled="True" OnClick="btnSave2_Click" Text="Save &amp; Continue" Font-Size="Small" Skin="Metro"/>
            </div>
            <p>
                <br />
            </p>
        </telerik:RadPageView>
        <telerik:RadPageView ID="RadPageView3" runat="server" CssClass="mainLayout">
            <p>
                <br />
            </p>
            <div style="padding-left: 5em;">
                <table>
                    <tr>
                        <td>
                            <asp:Label ID="lblSkip" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Red"> This section is completely optional.You can skip this section by clicking on Save &amp; Continue button below. </asp:Label></td>
                    </tr>
                </table>
            </div>
            <p>
                <br />
            </p>
            <div style="padding-left: 5em;">
                <table>
                    <tr>
                        <td style="width: 500px">
                            <asp:Label ID="lblRace" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Race : </asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 500px">
                            <telerik:RadDropDownList ID="ddlEthCode" runat="server" DataSourceID="dsRace" DataTextField="EthCodeDescrip" DataValueField="EthCodeId" DefaultMessage="--Select--" DropDownHeight="100px" DropDownWidth="300px" ExpandDirection="Down" Width="300px" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Skin="Metro"></telerik:RadDropDownList>
                            <asp:SqlDataSource ID="dsRace" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>
                                "
                                SelectCommand=" SELECT DISTINCT EthCodeId,EthCodeDescrip 
                                                    FROM dbo.adEthCodes EthCode
                                                    INNER JOIN dbo.syStatuses Statuses
                                                    ON Ethcode.StatusId=Statuses.StatusId
                                                    WHERE Statuses.Status='Active'
                                                    ORDER BY EthCodeDescrip"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 500px">
                        &nbsp;
                    </tr>
                    <tr>
                        <td style="width: 500px">
                            <asp:Label ID="lblNationality" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Nationality : </asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 500px">
                            <telerik:RadDropDownList ID="ddlNationality" runat="server" DataSourceID="dsNationality" DataTextField="NationalityDescrip" DataValueField="NationalityId" DefaultMessage="--Select--" DropDownHeight="100px" DropDownWidth="200px" ExpandDirection="Down" Width="200px" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Skin="Metro"></telerik:RadDropDownList>
                            <asp:SqlDataSource ID="dsNationality" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>
                                "
                                SelectCommand=" SELECT DISTINCT NationalityId,NationalityDescrip FROM dbo.adNationalities Nationality
                                                INNER JOIN dbo.syStatuses Statuses
                                                ON Nationality.StatusId=Statuses.StatusId
                                                WHERE Statuses.Status='Active'
                                                ORDER BY NationalityDescrip"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 500px">
                        &nbsp;
                    </tr>
                    <tr>
                        <td style="width: 500px">
                            <asp:Label ID="lblCitizen" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Citizenship Status : </asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 500px">
                            <telerik:RadDropDownList ID="ddlCitizen" runat="server" DataSourceID="dsCitizen" DataTextField="CitizenshipDescrip" DataValueField="CitizenshipId" DefaultMessage="--Select--" DropDownHeight="100px" DropDownWidth="200px" ExpandDirection="Down" Width="200px" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Skin="Metro"></telerik:RadDropDownList>
                            <asp:SqlDataSource ID="dsCitizen" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>
                                "
                                SelectCommand=" SELECT DISTINCT CitizenshipId,CitizenshipDescrip FROM adCitizenships Citizen
                                                INNER JOIN dbo.syStatuses Statuses
                                                ON Citizen.StatusId = Statuses.StatusId
                                                WHERE Status='Active'
                                                ORDER BY CitizenshipDescrip
                                                "></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 500px">
                        &nbsp;
                    </tr>
                    <tr>
                        <td style="width: 500px">
                            <asp:Label ID="lblMaritalStatus" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Marital Status : </asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 500px">
                            <telerik:RadDropDownList ID="ddlMaritalStatus" runat="server" DataSourceID="dsMaritalStatus" DataTextField="MaritalStatDescrip" DataValueField="MaritalStatId" DefaultMessage="--Select--" DropDownHeight="100px" DropDownWidth="200px" ExpandDirection="Down" Width="200px" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Skin="Metro"></telerik:RadDropDownList>
                            <asp:SqlDataSource ID="dsMaritalStatus" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>
                                "
                                SelectCommand=" SELECT DISTINCT MaritalStatId,MaritalStatDescrip FROM adMaritalStatus MaritalStatus
                                                INNER JOIN dbo.syStatuses Statuses
                                                ON MaritalStatus.StatusId = Statuses.StatusId
                                                WHERE Statuses.Status='Active'
                                                ORDER BY MaritalStatDescrip"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 500px">
                        &nbsp;
                    </tr>
                    <tr>
                        <td style="width: 500px">
                            <asp:Label ID="lblFamilyIncome" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Family Income : </asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 500px">
                            <telerik:RadDropDownList ID="ddlFamilyIncome" runat="server" DataSourceID="dsFamilyIncome" DataTextField="FamilyIncomeDescrip" DataValueField="FamilyIncomeID" DefaultMessage="--Select--" DropDownHeight="100px" DropDownWidth="200px" ExpandDirection="Down" Width="200px" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Skin="Metro"></telerik:RadDropDownList>
                            <asp:SqlDataSource ID="dsFamilyIncome" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>
                                "
                                SelectCommand=" SELECT DISTINCT FamilyIncomeID,FamilyIncomeDescrip FROM syFamilyIncome Income
                                                INNER JOIN dbo.syStatuses Statuses
                                                ON Income.StatusId = Statuses.StatusId
                                                WHERE Statuses.Status='Active'
                                                ORDER BY FamilyIncomeDescrip"></asp:SqlDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 500px">
                        &nbsp;
                    </tr>
                    <tr>
                        <td style="width: 500px">
                            <asp:Label ID="lblDependents" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Number of Dependents : </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 500px">
                            <telerik:RadNumericTextBox ID="txtDependents" Runat="server" Font-Names="Arial" Font-Size="10pt" MaxLength="2" MaxValue="99" MinValue="0" Skin="Metro" Type="Number" Width="50px" Font-Bold="True" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                <numberformat decimaldigits="0" groupseparator="" />
                            </telerik:RadNumericTextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 500px">
                        &nbsp;
                    </tr>
                    <tr>
                        <td style="width: 500px">
                            <asp:Label ID="lblDrLicState" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Driver&#39;s License State : </asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 500px">
                            <telerik:RadDropDownList ID="ddlDrLicState" runat="server" 
                                DefaultMessage="--Select--" DropDownHeight="200px" DropDownWidth="200px" ExpandDirection="Down" 
                                Width="200px" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" BorderColor="Black" 
                                BorderStyle="Solid" BorderWidth="1px" >
                            </telerik:RadDropDownList>
<%--                            <asp:SqlDataSource ID="dsDrLicState" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>
                             "
                                SelectCommand=" SELECT DISTINCT StateId,StateDescrip FROM syStates States
                                                INNER JOIN syStatuses Statuses ON States.StatusId = Statuses.StatusId
                                                WHERE Statuses.Status='Active' 
											    UNION 
											    SELECT '00000000-0000-0000-0000-000000000000' AS StateId, 'Other State' AS StateDescrip 
                                                ORDER BY StateDescrip "></asp:SqlDataSource>--%>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 500px">
                        &nbsp;
                    </tr>
                    <tr>
                        <td style="width: 500px">
                            <asp:Label ID="lblDrLicNumber" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Driver&#39;s License Number : </asp:Label></td>
                    </tr>
                    <tr>
                        <td style="width: 500px">
                            <telerik:RadTextBox ID="txtDrLicNumber" Runat="server" Skin="Metro" Width="300px" Wrap="False" Font-Names="Arial" Font-Size="10pt" Font-Bold="True" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></telerik:RadTextBox>
                        </td>
                    </tr>
                </table>
            </div>

            <table>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding-left: 60em">
                        <asp:Button ID="btnSave3" runat="server" CssClass="btnSave" Enabled="True" OnClick="btnSave3_Click" Text="Save &amp; Continue" Font-Size="Small" /></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </telerik:RadPageView>
        <telerik:RadPageView ID="RadPageView4" runat="server" runat="server" CssClass="mainLayout">
            <p>
                <br />
            </p>
            <table>
                <tr>
                    <td style="padding-left: 5em">
                        <asp:Label ID="lblDate" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Preferred date and time: </asp:Label></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding-left: 5em">
                        <telerik:RadDateTimePicker ID="rdtPicker" Runat="server" Culture="en-US" EnableScreenBoundaryDetection="False" Width="200px" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Skin="Metro"></telerik:RadDateTimePicker>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding-left: 5em">
                        <asp:Label ID="lblComments" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Black"> Comments/Questions : (Max 200 Characters) </asp:Label></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding-left: 5em">
                        <telerik:RadTextBox ID="txtComments" Runat="server" Columns="0" Font-Names="Arial" Font-Size="12pt" Height="100px" MaxLength="200" Rows="5" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Skin="Metro" TextMode="MultiLine" Width="500px"></telerik:RadTextBox>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding-left: 5em">
                        <asp:Label ID="lblDateErrMsg" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="Red" Visible="False"></asp:Label></td>
                </tr>
                <tr>
                    <td style="padding-left: 5em">
                        <asp:Label ID="lblLeadInsertStatus" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="#000099" Visible="False"></asp:Label></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding-left: 60em">
                        <asp:Button ID="btnSave4" runat="server" CssClass="btnSave" Enabled="True" OnClick="btnSave4_Click" Text=" Submit " Font-Size="Small" /></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding-left: 5em">
                        <asp:Label ID="lblSubmitNote" runat="server" BorderStyle="None" Font-Bold="True" Font-Names="Arial" Font-Size="Smaller" ForeColor="#0C090A" Width="850px"></asp:Label></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </telerik:RadPageView>
    </telerik:RadMultiPage>
</div>

