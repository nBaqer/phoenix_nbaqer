﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Christoc.Modules.MyProgress.View" %>    
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<br/><br/>
 <div style="text-align: left; font-size:14px;">
        The information shown below reflects your current academic progress. The information provides a snapshot of your progress, grades,
        registered courses, attendance and overall average for the selected program. Please click on the Get Progress Report button to view your academic progress.
    </div>
<br/><br/>
<div align="center">
    <table>
        <tr>
            <td><asp:Label ID="lblEnrollment" runat="server" Text="Program:&nbsp;" ></asp:Label></td>
        
            <td><asp:DropDownList ID="ddlEnrollment" runat="server" Width="400px" ></asp:DropDownList> </td>
        
            <td align="right"><asp:Button ID="btnSearch" runat="server" Text="Get Progress Report" OnClick="btnSearch_OnClick" ></asp:Button> </td>
        </tr>
    </table>
	   <telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
     <AjaxSettings>
         <telerik:AjaxSetting AjaxControlID="btnSearch">
             <UpdatedControls>
                 <telerik:AjaxUpdatedControl ControlID="btnSearch" LoadingPanelID="RadAjaxLoadingPanel1"/>
             </UpdatedControls>
        </telerik:AjaxSetting>
     </AjaxSettings>
 </telerik:RadAjaxManager>  
 <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Height="150px"
    Width="150px" IsSticky="true" MinDisplayTime="500">
    <img alt="Loading Degree Audit..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Default.Ajax.loading.gif") %>'
        style="border: 0px;" />
</telerik:RadAjaxLoadingPanel>  
</div>
    <br/>
    <br/>
    <div id="TranscriptWarningLabel" runat="server" style="width: 100%; text-align: center; color: red; font-weight: bold "></div>

