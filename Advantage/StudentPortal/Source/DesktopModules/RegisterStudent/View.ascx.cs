﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Security;
using DotNetNuke.Security.Membership;
using DotNetNuke.Security.Roles;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using DotNetNuke.Entities.Users;

namespace Christoc.Modules.RegisterStudent
{
    public partial class View : RegisterStudentModuleBase, IActionable
    {
        string firstName = string.Empty;
        string lastName = string.Empty;
        string displayName = string.Empty;
        string studentId = string.Empty;
        string password = string.Empty;
        string conPassword = string.Empty;
        string email = string.Empty;
        public bool IsPwdOk = false;

        protected void Page_Load(object sender, EventArgs e)
        {
            
            
            //txtLName.Attributes.Add("autocomplete", "off");
            //txtStuID.Attributes.Add("autocomplete", "off");
            //txtPWord.Attributes.Add("autocomplete", "off");
            //txtPWordConfirm.Attributes.Add("autocomplete", "off");

            txtLName.Focus();
        }

        public ModuleActionCollection ModuleActions
        {
            get
            {
                var actions = new ModuleActionCollection
                    {
                        {
                            GetNextActionID(), Localization.GetString("EditModule", LocalResourceFile), "", "", "",
                            EditUrl(), false, SecurityAccessLevel.Edit, true, false
                        }
                    };
                return actions;
            }
        }

        protected void btnRegister_Click(object sender, EventArgs e)
        {
            lblStatus.Text = string.Empty;
            lblStatus.Visible = false;

            lbltxtLNameErrMsg.Visible = txtLName.Text == string.Empty;
            lbltxtStuIDErrMsg.Visible = txtStuID.Text == string.Empty;
            lbltxtPWordErrMsg.Visible = txtPWord.Text == string.Empty;
            lbltxtPWordConfirmErrMsg.Visible = txtPWordConfirm.Text == string.Empty;

            if (txtLName.Text == string.Empty || txtStuID.Text == string.Empty || txtPWord.Text == string.Empty || txtPWordConfirm.Text == string.Empty) return;
            studentId = Server.HtmlEncode(txtStuID.Text.Trim());
            lastName = Server.HtmlEncode(txtLName.Text.Trim());
            password = Server.HtmlEncode(txtPWord.Text.Trim());
            conPassword = Server.HtmlEncode(txtPWordConfirm.Text.Trim());

            ValidatePassword();

            if (IsPwdOk)
            {
                IsRegisteredStudent();

                if (!String.IsNullOrEmpty(studentId) && !String.IsNullOrEmpty(lastName) && String.IsNullOrEmpty(lblStatus.Text))
                {
                    RegisterStudent();
                }
                else
                {
                    lblStatus.Visible = true;
                }
            }
            else
            {
                lblStatus.Visible = true;
            }
        }

        private void IsRegisteredStudent()
        {
            var conString = ConfigurationManager.ConnectionStrings["SiteSqlServer"].ConnectionString;
            var dnnCon = new SqlConnection(conString);
            var cmd = new SqlCommand(" SELECT * FROM Users WHERE LastName=LTRIM(RTRIM(@LName)) AND Username=LTRIM(RTRIM(@UserName))", dnnCon);
            cmd.Parameters.AddWithValue("@LName", lastName);
            cmd.Parameters.AddWithValue("@UserName", studentId);

            dnnCon.Open();
            var reader = cmd.ExecuteReader();

            if (!reader.HasRows)
            {
                Get_StudentInfo();
            }
            else
            {
                studentId = "";
                lastName = "";
                password = "";
                txtLName.Text = "";
                txtStuID.Text = "";
                txtPWord.Text = "";
                txtPWordConfirm.Text = "";
                lblStatus.Text = "";
                txtLName.Focus();

                lblStatus.ForeColor = System.Drawing.Color.Red;
                lblStatus.Text = @" You are registered already. Please log into website to access your student information. ";
            }
            dnnCon.Close();
            reader.Dispose();
            cmd.Dispose();
        }

        private void Get_StudentInfo()
        {
            var conString = ConfigurationManager.ConnectionStrings["ClientConnectionString"].ConnectionString;
            var clCon = new SqlConnection(conString);
            var cmd = new SqlCommand(" SELECT TOP 1 Stu.FirstName,Stu.LastName,Stu.StudentNumber,Stu.HomeEmail  FROM arstudent Stu " +
                                     " INNER JOIN arStuEnrollments Enr ON Stu.StudentId=Enr.StudentId " +
                                     " WHERE Stu.StudentNumber=LTRIM(RTRIM(@stunum)) AND Stu.LastName=LTRIM(RTRIM(@LName))", clCon);
            var adp = new SqlDataAdapter { SelectCommand = cmd };
            cmd.Parameters.AddWithValue("@stunum", studentId);
            cmd.Parameters.AddWithValue("@LName", lastName);
            var ds = new DataSet();

            try
            {
                clCon.Open();
                adp.Fill(ds, "RegStudent");

                if (ds.Tables[0].Rows.Count > 0)
                {
                    firstName = ds.Tables[0].Rows[0].Field<string>(0);
                    lastName = ds.Tables[0].Rows[0].Field<string>(1);
                    displayName = (firstName + ' ' + lastName);
                    studentId = ds.Tables[0].Rows[0].Field<string>(2);
                    email = ds.Tables[0].Rows[0].Field<string>(3);
                }
                else
                {
                    studentId = "";
                    lastName = "";
                    password = "";
                    txtLName.Text = "";
                    txtStuID.Text = "";
                    txtPWord.Text = "";
                    txtPWordConfirm.Text = "";
                    lblStatus.Text = "";
                    txtLName.Focus();
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                    lblStatus.Text = @" You student information is not found.  Please either provide correct student information or contact school associate.";
                }

                clCon.Close();
                adp.Dispose();
                cmd.Dispose();
            }
            catch (Exception exc)
            {
                Exceptions.ProcessModuleLoadException(this, exc);
                Response.Write("<script language='javascript'>alert('Database Connection cannot be established');</script>");
            }
            finally
            {
                clCon.Close();
            }
        }

        private void ValidatePassword()
        {
            if (password != conPassword)
            {
                lblStatus.ForeColor = System.Drawing.Color.Red;
                lblStatus.Text = @" Password and Confirm Password are not the same. Please re-enter.";
            }
            else if (password.Length < 7)
            {
                lblStatus.ForeColor = System.Drawing.Color.Red;
                lblStatus.Text = @"Password must be at least 7 characters long. Please try again.";
            }
            else
            {
                IsPwdOk = true;
            }
        }

        private void RegisterStudent()
        {
            var objStudent = new UserInfo
            {
                PortalID = PortalId,
                IsSuperUser = false,
                FirstName = firstName,
                LastName = lastName,
                DisplayName = displayName,
                Username = studentId,
                Email = email
            };

// ReSharper disable once CSharpWarnings::CS0618
            var objMembership = new UserMembership
            {
                Approved = true,
                CreatedDate = DateTime.Now,
                Password = password
            };

            objStudent.Membership = objMembership;

            var result = UserController.CreateUser(ref objStudent);

            if (result == UserCreateStatus.Success)
            {
                if (! objStudent.IsSuperUser)
                {

                    var objRoles = new RoleController();

                    // auto-assign user to portal roles
                    var arrRoles = objRoles.GetPortalRoles(objStudent.PortalID);
                    for (int i = 0; i < arrRoles.Count - 1; i++)
                    {
                        var objRole = (RoleInfo) arrRoles[i];
                        if (objRole.AutoAssignment)
                        {
                            objRoles.AddUserRole(objStudent.PortalID, objStudent.UserID, objRole.RoleID, Null.NullDate, Null.NullDate);
                        }
                    }

                    //objRoles.AddUserRole(objStudent.PortalID, objStudent.UserID, 5, Null.NullDate, Null.NullDate);
                }

            }
            
            if (result == UserCreateStatus.Success)
            {
                
                lblStatus.ForeColor = System.Drawing.Color.DarkBlue;
                lblStatus.Text = @" You are registered successfully. Please log into website to access your student information.";
                txtLName.Text = "";
                txtStuID.Text = "";
                txtPWord.Text = "";
                txtPWordConfirm.Text = "";
                txtLName.Focus();

                lblStatus.Visible = true;
            }
            else
            {
                lblStatus.ForeColor = System.Drawing.Color.Red;
                lblStatus.Text = @" You registration is not successful. Please contact school associate.";
                lblStatus.Visible = true;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            var url = Request.QueryString.Get("returnurl");
            Response.Redirect(url);
        }
    }
}