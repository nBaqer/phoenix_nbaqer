﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Christoc.Modules.MyTranscript.View" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>
<br/><br/>
 <div style="text-align: left; font-size:14px;">
     This transcript is official only when signed by the school official and embossed with the School’s raised seal.  
     Federal Law prohibits the release of this document to a person or institution without the written consent of the student. 
     Please click on the Get Unofficial Transcript to view your completed academic progress
    </div>
<br/><br/>
<div align="center">
    <table>
        <tr>
            <td><asp:Label ID="lblEnrollment" runat="server" Text="Program:&nbsp;" ></asp:Label></td>
        
            <td><asp:DropDownList ID="ddlEnrollment" runat="server" Width="400px" ></asp:DropDownList> </td>
        
            <td align="right"><asp:Button ID="btnSearch" runat="server" Text="Get Unofficial Transcript" OnClick="btnSearch_OnClick" ></asp:Button> </td>
        </tr>
    </table>
	   <telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
     <AjaxSettings>
         <telerik:AjaxSetting AjaxControlID="btnSearch">
             <UpdatedControls>
                 <telerik:AjaxUpdatedControl ControlID="btnSearch" LoadingPanelID="RadAjaxLoadingPanel1"/>
             </UpdatedControls>
        </telerik:AjaxSetting>
     </AjaxSettings>
 </telerik:RadAjaxManager>  
 <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Height="150px"
    Width="150px" IsSticky="true" MinDisplayTime="500">
    <img alt="Loading Degree Audit..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Default.Ajax.loading.gif") %>'
        style="border: 0px;" />
</telerik:RadAjaxLoadingPanel>  
</div>
    <br/>
    <br/>
    <div id="TranscriptWarningLabel" runat="server" style="width: 100%; text-align: center; color: red; font-weight: bold "></div>
