﻿using System;

namespace Christoc.Modules.MySchedule.Data.OutputData
{
    public class EnrollmentOutput
    {

        public Guid? EnrollmentId { get; set; }
        public string Enrollment { get; set; }
        public string Status { get; set; }
        public string Shift { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpectedGradDate { get; set; }

    }
}