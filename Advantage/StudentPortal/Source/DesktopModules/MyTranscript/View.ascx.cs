﻿/*
' Copyright (c) 2014  Christoc.com
'  All rights reserved.
' 
' THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
' TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
' THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
' CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
' DEALINGS IN THE SOFTWARE.
' 
*/

using System;
using System.Configuration;
using System.Web.UI;
using Christoc.Modules.MyTranscript.ReportGenerator;
using PortalFacade;
using DotNetNuke.Common;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using PortalData;

namespace Christoc.Modules.MyTranscript
{
   
    public partial class View : MyTranscriptModuleBase, IActionable
    {
        //private TranscriptData tranData;       //  PortalFacade.AcademicFacade.GetEnrollments(StudentNumber)
        private string studentNumber;

        protected void Page_Load(object sender, EventArgs e)
        {
            studentNumber = DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo().Username;
            //tranData = new TranscriptData();
            if (!IsPostBack)
            {
                var hasCourses = PortalFacade.AcademicFacade.IsStudentAssignedCourses(studentNumber);
                if (hasCourses == false)
                {
                    TranscriptWarningLabel.InnerText = Constant.X_WARNING_USER_HAS_NO_RECORDS;
                    btnSearch.Enabled = false;
                    return;
                }

                TranscriptWarningLabel.InnerText = string.Empty;
                btnSearch.Enabled = true;
                BindDropDowns();
            }
            // Get Term and if have term, get Courses
            // if they are empty put message about no grade information.

        }

        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            GetAndExportReport();
        }

        /// <summary>
        /// Get data for dropdown controls and bind the data
        /// </summary>
        private void BindDropDowns()
        {
            try
            {
                ddlEnrollment.DataTextField = "Enrollment";
                ddlEnrollment.DataValueField = "EnrollmentId";
                ddlEnrollment.DataSource = PortalFacade.AcademicFacade.GetEnrollments(studentNumber); 
                ddlEnrollment.DataBind();
                if (ddlEnrollment.Items.Count == 0)
                {
                    TranscriptWarningLabel.InnerText = Constant.X_WARNING_USER_HAS_NO_RECORDS;
                    btnSearch.Enabled = false;
                }
                else
                {
                    TranscriptWarningLabel.InnerText = string.Empty;
                    btnSearch.Enabled = true;
                }

            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException("An error occured while attempting to retrieve the filter options", this, ex);
            }

        }


        public ModuleActionCollection ModuleActions
        {
            get
            {
                var actions = new ModuleActionCollection
                    {
                        {
                            GetNextActionID(), Localization.GetString("EditModule", LocalResourceFile), "", "", "",
                            EditUrl(), false, SecurityAccessLevel.Edit, true, false
                        }
                    };
                return actions;
            }
        }

        private void GetAndExportReport()
        {

            Byte[] getReportAsBytes;

            string strStuEnrollId = ddlEnrollment.SelectedValue;
            

            string strReportPath = ConfigurationManager.AppSettings["Reports.ReportsFolder"];

            try
            {
                getReportAsBytes = (new MyTranscriptReportGenerator()).RenderReport("pdf", strReportPath, strStuEnrollId);
            }
            catch (Exception messageException)
            {
                throw messageException.InnerException;
            }


            ExportReport("pdf", getReportAsBytes);

        }

        private void ExportReport(string exportFormat, Byte[] getReportAsBytes)
        {
            string strExtension;
            string strMimeType;
            switch (exportFormat.ToLower())
            {
                case "pdf":
                    strExtension = "pdf";
                    strMimeType = "application/pdf";
                    break;

                    
                case "excel":
                    strExtension = "xls";
                    strMimeType = "application/vnd.excel";
                    break; 

                    

                case "csv":
                    strExtension = "csv";
                    strMimeType = "text/csv";
                    break;
                default:
                    throw new Exception("Unrecognized type. Type must be PDF, Excel or Image, HTML.");
            }
            Session["SSRS_FileExtension"] = strExtension;
            Session["SSRS_MimeType"] = strMimeType;
            Session["SSRS_ReportOutput"] = getReportAsBytes;



            //This code builds the URL and renders the report
            //In the module definition for user control  - DisplayReport.ascx create a key - MyTranscriptReport
            //Detailed documentation available at http://www.adefwebserver.com/DotNetNukeHELP/NavigateURL/ website
            //string myUrl = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "MyTranscriptReport", "mid=" + ModuleId);
            string myUrl = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "MyTranscriptReport", "mid=" + ModuleId);
            string script11 = "window.open('";
            string script12 = myUrl;
            string script13 = "','SSRSReport5','resizable=yes,left=200px,top=200px,modal=no');";
            string jsScript = script11 + script12 + script13;
            ScriptManager.RegisterStartupScript(Page, typeof(Page), "myscript", jsScript, true);

            //This code works - balaji 6.19.2014
            // Response.Redirect(Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "MyTranscriptReport", "mid=" + ModuleId.ToString()));

            //This code also works balaji 6.19.2014
            //string myUrl = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "MyTranscriptReport", "mid=" + ModuleId.ToString());
            //string script1 = string.Format("window.open('http://localhost/advantageportal/My-Info/My-Schedule/ctl/MyTranscriptReport/mid/2494','SSRSReport1','resizable=yes,left=200px,top=200px,modal=no');", DateTime.Now.ToLongTimeString());
            //ScriptManager.RegisterStartupScript(Page, typeof(Page), "myscript", script1, true);



        }

       
    }
}//201109BARROD201