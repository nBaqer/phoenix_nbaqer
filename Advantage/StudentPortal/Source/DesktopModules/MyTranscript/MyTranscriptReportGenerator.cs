using System;
using System.Configuration;
using FAME.Advantage.Reporting.ReportExecution2005;
using FAME.Advantage.Reporting.ReportService2005;

namespace Christoc.Modules.MyTranscript.ReportGenerator
{
    public class MyTranscriptReportGenerator
    {
        #region "Report Variables"
        private readonly ReportingService2005 rptService = new ReportingService2005();
        private readonly ReportExecutionService rptExecutionService = new ReportExecutionService();
        private string strReportName;
        private readonly string strHistoryId = null;
        private const bool BOOL_FOR_RENDERING = false;
        private readonly FAME.Advantage.Reporting.ReportService2005.DataSourceCredentials[] credentials = null;
        private readonly FAME.Advantage.Reporting.ReportService2005.ParameterValue[] rptParameterValues = null;
        private FAME.Advantage.Reporting.ReportService2005.ReportParameter[] rptParameters;
        private FAME.Advantage.Reporting.ReportExecution2005.ParameterValue[] rptExecutionServiceParameterValues;
        private ExecutionInfo rptExecutionInfoObj = new ExecutionInfo();


        //Variables needed to render report
        private readonly string deviceInfo = null;
        private Byte[] generateReportAsBytes;
        private string encoding = String.Empty;
        private static string _strExtension = string.Empty;
        private static string _strMimeType = string.Empty;
        private FAME.Advantage.Reporting.ReportExecution2005.Warning[] warnings;
        private string[] streamIDs;
        private readonly string _strConnectionString = ConfigurationManager.ConnectionStrings["ClientConnectionString"].ConnectionString;
        private string strServerName, strDbName, strUserName, strPassword;
        #endregion


        #region "Initialize Web Services"

        private void InitializeWebServices()
        {

            strServerName = GetServerName(_strConnectionString);
            strDbName = GetDatabaseName(_strConnectionString);
            strUserName = GetUserName(_strConnectionString);
            strPassword = GetPassword(_strConnectionString);

            //Set the credentials and url for the report services
            //The report service object is needed to get the parameter details associated with a specific report
            rptService.Credentials = System.Net.CredentialCache.DefaultCredentials;
            rptService.Url = ConfigurationManager.AppSettings["ReportServices"];

            //Set the credentials and url for the report execution services
            rptExecutionService.Credentials = System.Net.CredentialCache.DefaultCredentials;
            rptExecutionService.Url = ConfigurationManager.AppSettings["ReportExecutionServices"];
        }
        #endregion

        #region "Initialize Export Formats"
        private static void ConfigureExportFormat(ref string strType)
        {
            switch (strType.ToUpper())
            {
                case "PDF":
                    _strExtension = "pdf";
                    _strMimeType = "application/pdf";
                    break;
                case "EXCEL":
                case "XLS":
                    _strExtension = "xls";
                    _strMimeType = "application/vnd.excel";
                    break;
                case "WORD":
                    _strExtension = "doc";
                    _strMimeType = "application/vnd.ms-word";
                    break;
                case "CSV":
                    _strExtension = "csv";
                    _strMimeType = "text/csv";
                    break;
                default:
                    throw new Exception("Unrecognized type: " + strType + ". Type must be PDF, Excel or Image, HTML.");
            }
        }
        #endregion

        #region "Utility Functions"
        public string GetDatabaseName(string connString)
        {
            dynamic lcConnString = connString.ToLower();

            // if this is a Jet database, find the index of the "data source" setting
            int startIndex = lcConnString.IndexOf("initial catalog=");
            if (startIndex > -1)
                startIndex += 16;
            // if the "database", "data source" or "initial catalog" values are not 
            // found, return an empty string
            if (startIndex == -1)
                return "";
            // find where the database name/path ends
            int endIndex = lcConnString.IndexOf(";", startIndex);
            if (endIndex == -1)
                endIndex = lcConnString.Length;
            // return the substring with the database name/path
            return connString.Substring(startIndex, endIndex - startIndex);
        }
        public string GetServerName(string connString)
        {
            dynamic lcConnString = connString.ToLower();

            // if this is a Jet database, find the index of the "data source" setting
            int startIndex = lcConnString.IndexOf("data source=");
            if (startIndex > -1)
                startIndex += 12;

            // if the "database", "data source" or "initial catalog" values are not 
            // found, return an empty string
            if (startIndex == -1)
                return "";

            // find where the database name/path ends
            int endIndex = lcConnString.IndexOf(";", startIndex);
            if (endIndex == -1)
                endIndex = lcConnString.Length;

            // return the substring with the database name/path
            return connString.Substring(startIndex, endIndex - startIndex);
        }
        public string GetUserName(string connString)
        {
            dynamic lcConnString = connString.ToLower();

            // if this is a Jet database, find the index of the "data source" setting
            int startIndex = lcConnString.IndexOf("user id=");
            if (startIndex > -1)
                startIndex += 8;

            // if the "uid" values are not 
            // found, return an empty string
            if (startIndex == -1)
                return "";

            // find where the database name/path ends
            int endIndex = lcConnString.IndexOf(";", startIndex);
            if (endIndex == -1)
                endIndex = lcConnString.Length;

            // return the substring with the database name/path
            return connString.Substring(startIndex, endIndex - startIndex);
        }
        public string GetPassword(string connString)
        {
            dynamic lcConnString = connString.ToLower();

            // if this is a Jet database, find the index of the "data source" setting
            int startIndex = lcConnString.IndexOf("password=");
            if (startIndex > -1)
                startIndex += 9;

            // if the "uid" values are not 
            // found, return an empty string
            if (startIndex == -1)
                return "";

            // find where the database name/path ends
            int endIndex = lcConnString.IndexOf(";", startIndex);
            if (endIndex == -1)
                endIndex = lcConnString.Length;

            // return the substring with the database name/path
            return connString.Substring(startIndex, endIndex - startIndex);
        }
        #endregion


        #region "Build Report"
        public Byte[] RenderReport(string format, string strReportPath, string strStuEnrollId)
        {
            //Initialize the report services and report execution services
            InitializeWebServices();

            //Set the path of report
            // strReportName = strReportPath + "ProgressReport/PR_Main_DonotShowWorkUnits_Portal";
            strReportName = strReportPath + "TranscriptSingleStudent/TR_Sub0_Main";
            //"/Advantage Reports/" + getReportParametersObj.Environment + "/Security/ManageSecurity" 'getReportParametersObj.ReportPath   '"/Advantage Reports/StudentAccounts/IPEDS/IPEDSMissingDataReport"

            //Use the Report Services: GetReportParameters method to get a list of parameters for a specified report
            //This is the only location where the reportingservices2005 is used
            rptParameters = rptService.GetReportParameters(strReportName, strHistoryId, BOOL_FOR_RENDERING, rptParameterValues, credentials);

            //Need to load the specific report before passing the parameter values
            rptExecutionInfoObj = rptExecutionService.LoadReport(strReportName, strHistoryId);

            // Prepare report parameter. 
            rptExecutionServiceParameterValues = BuildReports(rptParameters, strStuEnrollId);

            rptExecutionService.Timeout = System.Threading.Timeout.Infinite;

            //Set Report Parameters
            rptExecutionService.SetExecutionParameters(rptExecutionServiceParameterValues, "en-us");

            //Call ConfigureExportFormat to get the file extensions
            ConfigureExportFormat(ref format);

            //Render the report
            generateReportAsBytes = rptExecutionService.Render(format, deviceInfo, ref _strExtension, ref encoding, ref _strMimeType, ref warnings, ref streamIDs);

            return generateReportAsBytes;
        }
        private FAME.Advantage.Reporting.ReportExecution2005.ParameterValue[] BuildReports(FAME.Advantage.Reporting.ReportService2005.ReportParameter[] rptParameters1, string strStuEnrollId)
        {

            var rptExecutionParamValues = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue[rptParameters1.Length];
            int iParam = 0;

            if (rptParameters1.Length > 0)
            {
                // Server Name       0
                iParam = 0;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "servername",
                    Name = "servername",
                    Value = string.IsNullOrEmpty(strServerName) ? null : strServerName
                };

                // Database Name      1
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "databasename",
                    Name = "databasename",
                    Value = string.IsNullOrEmpty(strDbName) ? null : strDbName
                };

                // User id           2
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "uid",
                    Name = "uid",
                    Value = string.IsNullOrEmpty(strUserName) ? null : strUserName
                };

                // password        3
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "password",
                    Name = "password",
                    Value = string.IsNullOrEmpty(strPassword) ? null : strPassword
                };

                // StuEnrollId List     4
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Student Enrollment Id List",
                    Name = "StuEnrollIdList",
                    Value = string.IsNullOrEmpty(strStuEnrollId) ? null : strStuEnrollId
                };


                // CampusId       5
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Campus Id",
                    Name = "CampusId",
                    Value = null
                };

                // Order By       6
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Order By",
                    Name = "OrderBy",
                    Value = "LastName Asc, FirstName Asc, MiddleName Asc"
                };

                // Status List      7
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Status List",
                    Name = "StatusList",
                    Value = null
                };

                 // Logo Position (Logo Options)      8
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Official Logo Position",
                    Name = "OfficialLogoPosition",
                    Value = "Left"
                };

                // Show School Name                  9
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show School Name",
                    Name = "ShowSchoolName",
                    Value = "true"
                };

                // Show Campus Address               10
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Campus Address",
                    Name = "ShowCampusAddress",
                    Value = "true"
                };

                // Show School Phone       11
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Phone",
                    Name = "ShowSchoolPhone",
                    Value = "true"
                };

                // Show School Fax        12
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Fax",
                    Name = "ShowSchoolFax",
                    Value = "true"
                };

                // Show School WebSite     13
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show WebSite",
                    Name = "ShowSchoolWebSite",
                    Value = "true"
                };


                // Is Official Transcript      14
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Is Official Transcript?",
                    Name = "IsOfficialTranscript",
                    Value = "false"
                };

                // Show Current Date       15
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Current Date",
                    Name = "ShowCurrentDate",
                    Value = "True"
                };

                //// Show Flod Mark
                //iParam += 1;
                //rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                //{
                //    Label = "Show Flod Mark",
                //    Name = "ShowFlodMark",
                //    Value = "false"
                //};

                //  ShowSSN            16
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show SSN",
                    Name = "ShowSSN",
                    Value = "true"
                };

                // Show Student Phone       17
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Student Phone",
                    Name = "ShowStudentPhone",
                    Value = "true"
                };

                // Show Student DOB       18
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Student DOB",
                    Name = "ShowStudentDOB",
                    Value = "true"
                };

                // Show Student Email       19
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Student Email",
                    Name = "ShowStudentEmail",
                    Value = "true"
                };

                // Show Multiple Programas on Single Transcript       20
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Multiple Programas on Single Transcript",
                    Name = "ShowMultipleEnrollments",
                    Value = "false"
                };

                // Show Last Day of Atendance         21
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Last Day Attended",
                    Name = "ShowLDA",
                    Value = "true"
                };

                // Show Completed Hours       22
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Completed Hours",
                    Name = "ShowCompletedHours",
                    Value = "true"
                };

                // Courses Layout            23  Values: "ShowTerms/Modules", "ShowCoursesOnly" or "ShowCourseCategories" layout
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Courses Layout",
                    Name = "CoursesLayout",
                    Value = "ShowTerms/Modules"
                };

                // Show Term Description     24
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Term Description",
                    Name = "ShowTermDescription",
                    Value = "true"
                };

                // Show course components     25
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show course components",
                    Name = "ShowCourseComponents",
                    Value = "false"
                };

                // Show Date Issued Column      26
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Date Issued Column",
                    Name = "ShowDateIssuedColumn",
                    Value = "true"
                };

                // Show Credits Column     27
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Credits Column",
                    Name = "ShowCreditsColumn",
                    Value = "true"
                };

                // Show Hours Column       28
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Hours Column",
                    Name = "ShowHoursColumn",
                    Value = "true"
                };

                // Show Course Summary      28
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Course Summary",
                    Name = "ShowCourseSummary",
                    Value = "true"
                };

                // Show Grade Points in Summary     30
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Grade Points in Summary",
                    Name = "ShowGradePoints",
                    Value = "true"
                };

                // Show Grade Scale      31
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Grade Scale",
                    Name = "ShowGradeScale",
                    Value = "true"
                };


                // Show Student Signature Line     32
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Student Signature Line",
                    Name = "ShowStudentSignatureLine",
                    Value = "true"
                };

                // Show School Official Signature Line       33
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show School Official Signature Line",
                    Name = "ShowSchoolOfficialSignatureLine",
                    Value = "false"
                };

                // Show Disclaimer        34
                iParam += 1;
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Disclaimer",
                    Name = "ShowDisclaimer",
                    Value = "true"
                };

                //DisclaimerText          35
                iParam += 1;               
                rptExecutionParamValues[iParam] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Disclaimer Text",
                    Name = "DisclaimerText",
                    Value = "This Transcript is official only when signed by the school official and embossed with the school�s raised seal.  Federal law prohibits the release of this document to a person or institution without the written consent of the student."
                };

            }
            return rptExecutionParamValues;
        }
        #endregion
    }
}