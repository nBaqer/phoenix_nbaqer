using System;
using System.Configuration;
using FAME.Advantage.Reporting.ReportExecution2005;
using FAME.Advantage.Reporting.ReportService2005;

namespace Christoc.Modules.MySchedule.ReportGenerator
{
    public class MyProgressReportGenerator
    {
        #region "Report Variables"
        private readonly ReportingService2005 rptService = new ReportingService2005();
        private readonly ReportExecutionService rptExecutionService = new ReportExecutionService();
        private string strReportName;
        private readonly string strHistoryId = null;
        private const bool BOOL_FOR_RENDERING = false;
        private readonly FAME.Advantage.Reporting.ReportService2005.DataSourceCredentials[] credentials = null;
        private readonly FAME.Advantage.Reporting.ReportService2005.ParameterValue[] rptParameterValues = null;
        private FAME.Advantage.Reporting.ReportService2005.ReportParameter[] rptParameters;
        private FAME.Advantage.Reporting.ReportExecution2005.ParameterValue[] rptExecutionServiceParameterValues;
        private ExecutionInfo rptExecutionInfoObj = new ExecutionInfo();


        //Variables needed to render report
        private readonly string deviceInfo = null;
        private Byte[] generateReportAsBytes;
        private string encoding = String.Empty;
        private static string _strExtension = string.Empty;
        private static string _strMimeType = string.Empty;
        private FAME.Advantage.Reporting.ReportExecution2005.Warning[] warnings;
        private string[] streamIDs;
        private readonly string _strConnectionString = ConfigurationManager.ConnectionStrings["ClientConnectionString"].ConnectionString;
        private string strServerName, strDbName, strUserName, strPassword;
        #endregion


        #region "Initialize Web Services"

        private void InitializeWebServices()
        {

            strServerName = GetServerName(_strConnectionString);
            strDbName = GetDatabaseName(_strConnectionString);
            strUserName = GetUserName(_strConnectionString);
            strPassword = GetPassword(_strConnectionString);

            //Set the credentials and url for the report services
            //The report service object is needed to get the parameter details associated with a specific report
            rptService.Credentials = System.Net.CredentialCache.DefaultCredentials;
            rptService.Url = ConfigurationManager.AppSettings["ReportServices"];

            //Set the credentials and url for the report execution services
            rptExecutionService.Credentials = System.Net.CredentialCache.DefaultCredentials;
            rptExecutionService.Url = ConfigurationManager.AppSettings["ReportExecutionServices"];
        }
        #endregion

        #region "Initialize Export Formats"
        private static void ConfigureExportFormat(ref string strType)
        {
            switch (strType.ToUpper())
            {
                case "PDF":
                    _strExtension = "pdf";
                    _strMimeType = "application/pdf";
                    break;
                case "EXCEL":
                case "XLS":
                    _strExtension = "xls";
                    _strMimeType = "application/vnd.excel";
                    break;
                case "WORD":
                    _strExtension = "doc";
                    _strMimeType = "application/vnd.ms-word";
                    break;
                case "CSV":
                    _strExtension = "csv";
                    _strMimeType = "text/csv";
                    break;
                default:
                    throw new Exception("Unrecognized type: " + strType + ". Type must be PDF, Excel or Image, HTML.");
            }
        }
        #endregion

        #region "Utility Functions"
        public string GetDatabaseName(string connString)
        {
            dynamic lcConnString = connString.ToLower();

            // if this is a Jet database, find the index of the "data source" setting
            int startIndex = lcConnString.IndexOf("initial catalog=");
            if (startIndex > -1)
                startIndex += 16;
            // if the "database", "data source" or "initial catalog" values are not 
            // found, return an empty string
            if (startIndex == -1)
                return "";
            // find where the database name/path ends
            int endIndex = lcConnString.IndexOf(";", startIndex);
            if (endIndex == -1)
                endIndex = lcConnString.Length;
            // return the substring with the database name/path
            return connString.Substring(startIndex, endIndex - startIndex);
        }
        public string GetServerName(string connString)
        {
            dynamic lcConnString = connString.ToLower();

            // if this is a Jet database, find the index of the "data source" setting
            int startIndex = lcConnString.IndexOf("data source=");
            if (startIndex > -1)
                startIndex += 12;

            // if the "database", "data source" or "initial catalog" values are not 
            // found, return an empty string
            if (startIndex == -1)
                return "";

            // find where the database name/path ends
            int endIndex = lcConnString.IndexOf(";", startIndex);
            if (endIndex == -1)
                endIndex = lcConnString.Length;

            // return the substring with the database name/path
            return connString.Substring(startIndex, endIndex - startIndex);
        }
        public string GetUserName(string connString)
        {
            dynamic lcConnString = connString.ToLower();

            // if this is a Jet database, find the index of the "data source" setting
            int startIndex = lcConnString.IndexOf("user id=");
            if (startIndex > -1)
                startIndex += 8;

            // if the "uid" values are not 
            // found, return an empty string
            if (startIndex == -1)
                return "";

            // find where the database name/path ends
            int endIndex = lcConnString.IndexOf(";", startIndex);
            if (endIndex == -1)
                endIndex = lcConnString.Length;

            // return the substring with the database name/path
            return connString.Substring(startIndex, endIndex - startIndex);
        }
        public string GetPassword(string connString)
        {
            dynamic lcConnString = connString.ToLower();

            // if this is a Jet database, find the index of the "data source" setting
            int startIndex = lcConnString.IndexOf("password=");
            if (startIndex > -1)
                startIndex += 9;

            // if the "uid" values are not 
            // found, return an empty string
            if (startIndex == -1)
                return "";

            // find where the database name/path ends
            int endIndex = lcConnString.IndexOf(";", startIndex);
            if (endIndex == -1)
                endIndex = lcConnString.Length;

            // return the substring with the database name/path
            return connString.Substring(startIndex, endIndex - startIndex);
        }
        #endregion


        #region "Build Report"
        public Byte[] RenderReport(string format, string strReportPath, string strStuEnrollId)
        {
            //Initialize the report services and report execution services
            InitializeWebServices();

            //Set the path of report
            // strReportName = strReportPath + "ProgressReport/PR_Main_DonotShowWorkUnits_Portal";
            strReportName = strReportPath + "TranscriptSingleStudent/TR_Sub0_Main";
            //"/Advantage Reports/" + getReportParametersObj.Environment + "/Security/ManageSecurity" 'getReportParametersObj.ReportPath   '"/Advantage Reports/StudentAccounts/IPEDS/IPEDSMissingDataReport"

            //Use the Report Services: GetReportParameters method to get a list of parameters for a specified report
            //This is the only location where the reportingservices2005 is used
            rptParameters = rptService.GetReportParameters(strReportName, strHistoryId, BOOL_FOR_RENDERING, rptParameterValues, credentials);

            //Need to load the specific report before passing the parameter values
            rptExecutionInfoObj = rptExecutionService.LoadReport(strReportName, strHistoryId);

            // Prepare report parameter. 
            rptExecutionServiceParameterValues = BuildReports(rptParameters, strStuEnrollId);

            rptExecutionService.Timeout = System.Threading.Timeout.Infinite;

            //Set Report Parameters
            rptExecutionService.SetExecutionParameters(rptExecutionServiceParameterValues, "en-us");

            //Call ConfigureExportFormat to get the file extensions
            ConfigureExportFormat(ref format);

            //Render the report
            generateReportAsBytes = rptExecutionService.Render(format, deviceInfo, ref _strExtension, ref encoding, ref _strMimeType, ref warnings, ref streamIDs);

            return generateReportAsBytes;
        }
        private FAME.Advantage.Reporting.ReportExecution2005.ParameterValue[] BuildReports(FAME.Advantage.Reporting.ReportService2005.ReportParameter[] rptParameters1, string strStuEnrollId)
        {

            var rptExecutionParamValues = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue[rptParameters1.Length];

            if (rptParameters1.Length > 0)
            {

                rptExecutionParamValues[0] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "servername",
                    Name = "servername",
                    Value = string.IsNullOrEmpty(strServerName) ? null : strServerName
                };

                rptExecutionParamValues[1] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "databasename",
                    Name = "databasename",
                    Value = string.IsNullOrEmpty(strDbName) ? null : strDbName
                };

                rptExecutionParamValues[2] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "uid",
                    Name = "uid",
                    Value = string.IsNullOrEmpty(strUserName) ? null : strUserName
                };

                rptExecutionParamValues[3] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "password",
                    Name = "password",
                    Value = string.IsNullOrEmpty(strPassword) ? null : strPassword
                };

                rptExecutionParamValues[4] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Student Enrollment Id List",
                    Name = "StuEnrollIdList",
                    Value = string.IsNullOrEmpty(strStuEnrollId) ? null : strStuEnrollId
                };

                rptExecutionParamValues[5] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Status List",
                    Name = "StatusList",
                    Value = null
                };

                rptExecutionParamValues[6] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Campus Id",
                    Name = "CampusId",
                    Value = null
                };

                rptExecutionParamValues[7] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Is Official Transcript?",
                    Name = "IsOfficialTranscript",
                    Value = "false"
                };

                rptExecutionParamValues[8] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Current Date",
                    Name = "ShowCurrentDate",
                    Value = "True"
                };

                rptExecutionParamValues[9] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Official Logo Position",
                    Name = "OfficialLogoPosition",
                    Value = "Left"
                };

                rptExecutionParamValues[10] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Student Phone",
                    Name = "ShowStudentPhone",
                    Value = "true"
                };

                rptExecutionParamValues[11] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Student DOB",
                    Name = "ShowStudentDOB",
                    Value = "true"
                };

                rptExecutionParamValues[12] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Student Email",
                    Name = "ShowStudentEmail",
                    Value = "true"
                };

                rptExecutionParamValues[13] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Multiple Enrollments",
                    Name = "ShowMultipleEnrollments",
                    Value = "false"
                };

                rptExecutionParamValues[14] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Order By",
                    Name = "OrderBy",
                    Value = "LastName Asc,FirstName Asc,MiddleName Asc"
                };

                rptExecutionParamValues[15] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Last Day Attended",
                    Name = "ShowLDA",
                    Value = "true"
                };

                rptExecutionParamValues[16] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Completed Hours",
                    Name = "ShowCompletedHours",
                    Value = "true"
                };

                rptExecutionParamValues[17] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Terms",
                    Name = "ShowTerms",
                    Value = "true"
                };

                rptExecutionParamValues[18] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Work Units under each Subject",
                    Name = "ShowWorkUnitsUnderEachSubject",
                    Value = "false"
                };

                rptExecutionParamValues[19] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Course Summary",
                    Name = "ShowCourseSummary",
                    Value = "true"
                };

                rptExecutionParamValues[20] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Grade Scale",
                    Name = "ShowGradeScale",
                    Value = "true"
                };

                rptExecutionParamValues[21] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Show Signature Line",
                    Name = "ShowSignatureLine",
                    Value = "true"
                };

                rptExecutionParamValues[22] = new FAME.Advantage.Reporting.ReportExecution2005.ParameterValue
                {
                    Label = "Disclaimer Text",
                    Name = "DisclaimerText",
                    Value = "This Transcript is official only when signed by the school official and embossed with the school�s raised seal.  Federal law prohibits the release of this document to a person or institution without the written consent of the student."
                };

            }
            return rptExecutionParamValues;
        }
        #endregion
    }
}