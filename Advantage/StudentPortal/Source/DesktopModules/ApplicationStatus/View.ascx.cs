﻿using Christoc.Modules.ApplicationStatus.Data;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System;

namespace Christoc.Modules.ApplicationStatus
{
    public partial class View : ApplicationStatusModuleBase, IActionable
    {
        #region Class Members

        private readonly ApplicationStatusData _appStatusData = new ApplicationStatusData();

        #endregion


        #region Class Events

        /// <summary>
        /// page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e){}


        /// <summary>
        /// Event fired when user clicks to check application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSumbit_Click(object sender, EventArgs e)
        {
            try
            {
                if (LeadExists())
                    CheckApplicationStatus();
                else
                    lblNoRecords.Text = "No records are found for the information provided. Please contact our school associate for any questions.";

            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException("An error occured while checking the status of your application.", this, ex);
            }

        }

        #endregion


        #region Class Methods

        /// <summary>
        /// check for existing lead
        /// </summary>
        /// <returns></returns>
        private bool LeadExists()
        {
            return _appStatusData.LeadExists(txtFName.Text, txtLName.Text, txtEmail.Text);
        }

        /// <summary>
        /// Check the application status and set message and grid accordingly
        /// </summary>
        private void CheckApplicationStatus()
        {
            ApplicationStatusData.AppStatus eAppStatus = _appStatusData.RequiredUnapprovedDocs(txtFName.Text, txtLName.Text, txtEmail.Text);
            if (eAppStatus == ApplicationStatusData.AppStatus.AllApproved)
            
                lblNoRecords.Text = "Thank you for submitting all the required documents. Please contact our school associate for any questions.";
            
            else if (eAppStatus == ApplicationStatusData.AppStatus.NoneApproved)
            
                lblNoRecords.Text = "Please submit the required documents as soon as possible. Contact our school associate for any questions.";
            
            else if(eAppStatus == ApplicationStatusData.AppStatus.SomeApproved )
                lblNoRecords.Text ="Thank you for submitting some of the required documents. Please submit the remaining required documents as soon as possible or contact our school associate for any questions.";
            else 
                lblNoRecords.Text = "Please submit the required documents as soon as possible. Contact our school associate for any questions.";
        }

        /// <summary>
        /// DOTNETNUKE required method
        /// </summary>
        public ModuleActionCollection ModuleActions
        {
            get
            {
                var actions = new ModuleActionCollection
                    {
                        {
                            GetNextActionID(), Localization.GetString("EditModule", LocalResourceFile), "", "", "",
                            EditUrl(), false, SecurityAccessLevel.Edit, true, false
                        }
                    };
                return actions;
            }
        }

        #endregion
    }
}