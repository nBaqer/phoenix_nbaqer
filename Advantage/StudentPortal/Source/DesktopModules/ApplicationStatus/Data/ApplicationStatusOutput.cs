﻿

namespace Christoc.Modules.ApplicationStatus.Data
{
    public class ApplicationStatusOutput
    {

        public string DocumentDescription { get; set; }
        public string StatusDescription { get; set; }

    }
}