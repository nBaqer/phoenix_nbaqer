﻿using System.Collections.Generic;
using System.Data;
using System.Linq;


namespace Christoc.Modules.ApplicationStatus.Data
{
    public class ApplicationStatusData
    {

        /// <summary>
        /// Determine if lead exists or not
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public bool LeadExists(string firstName, string lastName, string emailAddress)
        {
            using (var dbContext = new ApplicationStatusLinqClassesDataContext())
            {
                return dbContext.adLeads.Count(
                    n => n.FirstName.ToUpper().Trim() == firstName.ToUpper().Trim() &&    
                    n.LastName.ToUpper().Trim() == lastName.ToUpper().Trim() &&
                    n.HomeEmail.ToUpper().Trim() == emailAddress.ToUpper().Trim()) > 0;

            }
        }
       
        /// <summary>
        /// Get any unapproved documents
        /// </summary>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public AppStatus RequiredUnapprovedDocs(string firstName, string lastName, string emailAddress)
        {
            using (var dbContext = new ApplicationStatusLinqClassesDataContext())
            {
                var data = (from m in dbContext.usp_GetLeadDocsStatus(firstName.Trim(),lastName.Trim(),emailAddress.Trim())
                    select new ApplicationStatusOutput()
                    {
                        DocumentDescription = m.DocumentDescrip,
                        StatusDescription = m.DocStatusDescrip

                    }).ToList();


                int appCount = data.Count(n => n.StatusDescription.ToUpper() == "APPROVED");

                if (appCount == data.Count())
                    return AppStatus.AllApproved;
                else if (appCount == 0)
                    return AppStatus.NoneApproved;
                else
                    return AppStatus.SomeApproved;

            }

        }


        public enum AppStatus 
        {
            AllApproved,
            NoneApproved,
            SomeApproved
        }

    }

}