﻿using System;
using System.Collections.Generic;
using PortalData.Academic;
using PortalFacade;
using Telerik.Web.UI;

using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;


namespace Christoc.Modules.MyGradeBook
{

    public partial class View : MyGradeBookModuleBase, IActionable
    {
        #region Page Events (Load)

        protected void Page_Load(object sender, EventArgs e)
        {
            lblHiddenUserName.Text = DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo().Username;
            try
            {
                if (!IsPostBack)
                {
                    ddlEnrollment.Items.Clear();
                    ddlTerm.Items.Clear();
                    ddlClsSection.Items.Clear();

                    ddlEnrollment.DataBind();
                    ddlTerm.DataBind();
                    ddlClsSection.DataBind();
                    if (ddlEnrollment.Items.Count == 0)
                    {
                        GradeBookWarningLabel.InnerText = PortalData.Constant.X_WARNING_USER_HAS_NO_RECORDS;
                    }
                }
            }
            catch (Exception exc) //Module failed to load
            {
                Exceptions.ProcessModuleLoadException(this, exc);
            }
        }

        #endregion

        #region DNN infrastructure
        public ModuleActionCollection ModuleActions
        {
            get
            {
                var actions = new ModuleActionCollection
                    {
                        {
                            GetNextActionID(), Localization.GetString("EditModule", LocalResourceFile), "", "", "",
                            EditUrl(), false, SecurityAccessLevel.Edit, true, false
                        }
                    };
                return actions;
            }
        }
        #endregion

        #region Filters Events

        protected void ddlTerm_visible(object sender, EventArgs e)
        {
            ddlTerm.DataBind();

            if (ddlTerm.Items.Count <= 0)
            {
                lblTerm.Visible = false;
                ddlTerm.Visible = false;

                lblClsSection.Visible = false;
                ddlClsSection.Visible = false;

                lblHiddenTxtMsg.Text = @" You are not registered for any term in '" + ddlEnrollment.SelectedItem + @"' Program.";
                lblHiddenTxtMsg.Visible = true;
            }
            else
            {
                lblTerm.Visible = true;
                ddlTerm.Visible = true;

                lblClsSection.Visible = true;
                ddlClsSection.Visible = true;

                lblHiddenTxtMsg.Visible = false;
                rgStudentGradeBook.Rebind();
            }
        }

        protected void ddlClsSection_visible(object sender, EventArgs e)
        {
            ddlClsSection.DataBind();

            if (ddlClsSection.Items.Count <= 0)
            {
                lblClsSection.Visible = false;
                ddlClsSection.Visible = false;

                lblHiddenTxtMsg.Text = @" You are not registered for any class section.";
                lblHiddenTxtMsg.Visible = true;
            }
            else
            {
                lblClsSection.Visible = true;
                ddlClsSection.Visible = true;
                rgStudentGradeBook.Rebind();
            }
        }

        protected void ddlClsSection_IndexChanged(object sender, DropDownListEventArgs e)
        {
            if (ddlClsSection.Items.Count > 0)
            {
                rgStudentGradeBook.Rebind();
            }

        }

        #endregion

        //protected void rgStudentGradeBook_PreRender(object sender, EventArgs e)
        //{
        //    HideColumnRecursive(rgStudentGradeBook.MasterTableView);
        //}

        //public void HideColumnRecursive(GridTableView tableView)
        //{
        //    GridItem[] nestedViewItems = tableView.GetItems(GridItemType.NestedView);
        //    foreach (var gridItem in nestedViewItems)
        //    {
        //        var nestedViewItem = (GridNestedViewItem) gridItem;
        //        foreach (GridTableView nestedView in nestedViewItem.NestedTableViews)
        //        {
        //            GridColumn termId = nestedView.GetColumnSafe("TermId");
        //            GridColumn clSectionId = nestedView.GetColumnSafe("ClsSectionId");

        //            if (termId != null)
        //            {
        //                termId.Visible = false;
        //                clSectionId.Visible = false;
        //            }

        //        }
        //    }
        //}

        protected void rgStudentGradeBook_SelectedIndexChanged(object sender, EventArgs e)
        {
           // var item = (GridDataItem)rgStudentGradeBook.SelectedItems[0];
        }

 
        #region Master - Detail GradeBook Table events

        protected void rgStudentGradeBook_NeedDataSource(object sender, GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable)
            {
                if (ddlEnrollment.Items.Count == 0)
                {   // Student does not have enrollments.
                    return;
                }

                rgStudentGradeBook.DataSource = AcademicFacade.GetGradeBookResults(
                                                                            ddlEnrollment.SelectedValue,
                                                                            ddlTerm.SelectedValue,
                                                                            ddlClsSection.SelectedValue
                                                                        );
            }
        }

        protected void rgStudentGradeBook_DetailTableDataBind(object source, GridDetailTableDataBindEventArgs e)
        {
             var dataItem = e.DetailTableView.ParentItem;
            
            // Check if the Cls section Id is valid....
            // Transfer courses has not class data section because they was taken in other school
            // Then you have nothing to show in details.
            Guid noused;
            if (Guid.TryParse(dataItem["ClsSectionId"].Text, out noused) == false)
            {
                e.DetailTableView.DataSource = new List<GradeBookComponentsOutput>();
                return;
            }
 
            e.DetailTableView.DataSource = AcademicFacade.GetGradeBookComponents(
                                                            dataItem.GetDataKeyValue("StuEnrollId").ToString(),
                                                            dataItem.GetDataKeyValue("TermId").ToString(),
                                                            dataItem.GetDataKeyValue("ClsSectionId").ToString()
                                                            );
        }

        protected void rgStudentGradeBook_PreRender(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                if (rgStudentGradeBook.MasterTableView.Items != null && rgStudentGradeBook.MasterTableView.Items.Count > 0)
                { rgStudentGradeBook.MasterTableView.Items[0].Expanded = true;}
            }
        }


        #endregion
  
    }
}