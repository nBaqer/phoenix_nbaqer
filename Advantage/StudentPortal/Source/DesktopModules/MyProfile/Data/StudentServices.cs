﻿using System.Configuration;
using System.Data.SqlClient;
using Christoc.Modules.MyProfile.Data.OutputData;

namespace Christoc.Modules.MyProfile.Data
{
    public class StudentServices
    {
        /// <summary>
        /// Get the demographic student data
        /// </summary>
        /// <param name="username">Student user name</param>
        /// <returns>
        /// A StudentInfo object. If the student has not data, a empty Student Info is returned.
        /// </returns>
        public StudentInfo GetStudentInfo(string username)
        {
            string conString = ConfigurationManager.ConnectionStrings["ClientConnectionString"].ConnectionString;
            var clCon = new SqlConnection(conString);
            using (clCon)
            {
                var cmd = new SqlCommand(
                    " SELECT (FirstName+' '+ LastName) AS StudentName, ISNULL(CONVERT (VARCHAR(10),DOB,101),'') AS DOB, " +
                    " ISNULL(HomeEmail,'') AS HomeEmail, ISNULL(WorkEmail,'') AS WorkEmail," +
                    " Phone1= (SELECT TOP 1 ISNULL(Phone,'') FROM arStudentPhone WHERE StudentId=Stu.StudentId AND default1=1), " +
                    " Phone1Intl=(SELECT ISNULL((SELECT  ForeignPhone FROM arStudentPhone WHERE Phone=(SELECT TOP 1 ISNULL(Phone,'') FROM arStudentPhone WHERE StudentId=Stu.StudentId AND default1=1)),0)), " +
                    " Phone1Type= (SELECT PhoneTypeDescrip FROM dbo.syPhoneType WHERE PhoneTypeId=(SELECT PhoneTypeId FROM dbo.arStudentPhone WHERE StudentId=Stu.StudentId AND Phone IN (SELECT TOP 1 ISNULL(Phone,'') FROM arStudentPhone WHERE StudentId=Stu.StudentId AND default1=1))), " +
                    " Phone2=(SELECT TOP 1 ISNULL(Phone,'') FROM arStudentPhone WHERE StudentId=Stu.StudentId AND default1=0), " +
                    " Phone2Intl=(SELECT ISNULL((SELECT  ForeignPhone FROM arStudentPhone WHERE Phone=(SELECT TOP 1 ISNULL(Phone,'') FROM arStudentPhone WHERE StudentId=Stu.StudentId AND default1=0)),0)), " +
                    " Phone2Type= (SELECT PhoneTypeDescrip FROM dbo.syPhoneType WHERE PhoneTypeId=(SELECT PhoneTypeId FROM dbo.arStudentPhone WHERE StudentId=Stu.StudentId AND Phone IN (SELECT TOP 1 ISNULL(Phone,'') FROM arStudentPhone WHERE StudentId=Stu.StudentId AND default1=0))), " +
                    " IsAddressIntl=(SELECT ISNULL((SELECT TOP 1 ISNULL(ForeignZip ,'') FROM dbo.arStudAddresses WHERE studentid=Stu.StudentId ORDER BY default1 DESC),0)), " +
                    " Address1=(SELECT TOP 1 ISNULL(Address1 ,'') FROM dbo.arStudAddresses WHERE studentid=Stu.StudentId ORDER BY default1 DESC), " +
                    " Address2=(SELECT TOP 1 ISNULL(Address2 ,'') FROM dbo.arStudAddresses WHERE studentid=Stu.StudentId ORDER BY default1 DESC), " +
                    " City=(SELECT TOP 1 ISNULL(City ,'') FROM dbo.arStudAddresses WHERE studentid=Stu.StudentId ORDER BY default1 DESC), " +
                    " [State]=(SELECT TOP 1 ISNULL(SStates.StateDescrip ,'') FROM dbo.arStudAddresses Addresses LEFT JOIN dbo.syStates SStates ON Addresses.StateId=SStates.StateId WHERE Addresses.studentid=Stu.StudentId ORDER BY default1 DESC), " +
                    " [OtherState]=(SELECT TOP 1 ISNULL(OtherState ,'') FROM dbo.arStudAddresses WHERE Studentid=Stu.StudentId ORDER BY default1 DESC)," +
                    " Zip=(SELECT TOP 1 ISNULL(Zip ,'') FROM dbo.arStudAddresses WHERE studentid=Stu.StudentId ORDER BY default1 DESC), " +
                    " [Country]=(SELECT TOP 1 ISNULL(Countries.CountryDescrip ,'') FROM dbo.arStudAddresses Addresses LEFT JOIN dbo.adCountries Countries ON Addresses.CountryId=Countries.CountryId WHERE Addresses.studentid=Stu.StudentId ORDER BY default1 DESC) " +
                    " FROM arstudent Stu " +
                    " WHERE StudentNumber=@UserName ", clCon);

                cmd.Parameters.AddWithValue("@UserName", username);
                clCon.Open();

                var reader = cmd.ExecuteReader();

                // Prepare the result
                var result = new StudentInfo();

                // if none records return, the result return with count 0.
                if (reader.HasRows)
                {
                    // Should only comes one record, if they are a problem
                    //in database, the last is taken.
                    while (reader.Read())
                    {
                       
                        result.StudentName = reader.GetString(0);
                        result.Dob = reader.GetString(1);
                        result.HomeMail = reader.GetString(2);
                        result.WorkMail = reader.GetString(3);
                        result.Phone1 =  reader.IsDBNull(4)? string.Empty: reader.GetString(4);
                        result.PhoneInt1 = reader.GetBoolean(5);
                        result.Phone1Type = reader.GetString(6);
                        result.Phone2 = reader.IsDBNull(7) ? string.Empty : reader.GetString(7);
                        result.PhoneInt2 = reader.GetBoolean(8);
                        result.Phone2Type = reader.IsDBNull(9) ? string.Empty : reader.GetString(9);
                        result.IsAddressInt1 = reader.GetBoolean(10);
                        result.Address1 = reader.GetString(11);
                        result.Address2 = reader.IsDBNull(12) ? string.Empty : reader.GetString(12); 
                        result.City = reader.GetString(13);
                        result.State = reader.GetString(14);
                        result.OtherState = reader.IsDBNull(15) ? string.Empty : reader.GetString(15);
                        result.Zip = reader.IsDBNull(16) ? string.Empty : reader.GetString(16);
                        result.Country = reader.GetString(17);
                        result.IsDataValid = true;
                    }

                }

                reader.Close();
                return result;
            }
        }
    }
}