﻿namespace Christoc.Modules.MyProfile.Data.OutputData
{
    /// <summary>
    /// Student Info Output. store the student information if exists.
    /// </summary>
    public class StudentInfo
    {
        /// <summary>
        /// This booolean value return true if the object
        /// has valid information otherwise return false.
        /// </summary>
        public bool IsDataValid { get; set; }
        public string StudentName { get; set; }
        public string Dob { get; set; }
        public string HomeMail { get; set; }
        public string WorkMail { get; set; }
        public string Phone1 { get; set; }
        public bool PhoneInt1 { get; set; }
        public string Phone1Type { get; set; }
        public string Phone2 { get; set; }
        public bool PhoneInt2 { get; set; }
        public string Phone2Type { get; set; }
        public bool IsAddressInt1 { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string OtherState { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
    }
}