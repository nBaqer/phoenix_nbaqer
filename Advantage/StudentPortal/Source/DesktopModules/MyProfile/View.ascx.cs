﻿using System;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Web.UI.WebControls;
using System.Net;
using System.Net.Mail;
using Christoc.Modules.MyProfile.Data;
using DotNetNuke.Entities.Users;
using DotNetNuke.Services.Exceptions;
using PortalData;
using Telerik.Web.UI;

using DotNetNuke.Security;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;

namespace Christoc.Modules.MyProfile
{
    public partial class View : MyProfileModuleBase, IActionable
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            lblHiddenUserName.Text = UserController.GetCurrentUserInfo().Username;

            if (!IsPostBack)
            {
                try
                {

                    var service = new StudentServices();
                    var studentInfo = service.GetStudentInfo(lblHiddenUserName.Text);
                    if (studentInfo.IsDataValid == false)
                    {
                        ProfileWarningLabel.Text = Constant.X_WARNING_USER_HAS_NO_RECORDS;
                        RadPanelBar1.Enabled = false;
                        return;
                    }

                    // fill html fields...
                    var lblFullName = (Label)RadPanelBar1.FindItemByValue("templateHolder").FindControl("txtFName");
                    lblFullName.Text = studentInfo.StudentName;
                    lblHiddenFullName.Text = studentInfo.StudentName;

                    var lblDateOfBirth = (Label)RadPanelBar1.FindItemByValue("templateHolder").FindControl("txtDOB");
                    lblDateOfBirth.Text = studentInfo.Dob;

                    var homeEmailtxt =
                        (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder1").FindControl("txtHomeEmail");
                    homeEmailtxt.Text = studentInfo.HomeMail;
                    lblHiddenHomeEmail.Text = studentInfo.HomeMail;

                    RadPanelBar1.FindItemByValue("templateHolder1").FindControl("lbltxtHomeEmailErrMsg").Visible = false;

                    var workEmailtxt =
                        (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder1").FindControl("txtWorkEmail");
                    workEmailtxt.Text = studentInfo.WorkMail;
                    lblHiddenWorkEmail.Text = studentInfo.WorkMail;

                    RadPanelBar1.FindItemByValue("templateHolder1").FindControl("lbltxtWorkEmailErrMsg").Visible = false;

                    var phone1Txt =
                        (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("txtPhone1");
                    phone1Txt.Text = studentInfo.Phone1;
                    lblHiddenPhone1.Text = studentInfo.Phone1;

                    var phone1Intltxt =
                        (CheckBox)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("chkPhone1Intl");
                    phone1Intltxt.Checked = studentInfo.PhoneInt1;
                    lblHiddernPhone1Intl.Checked = studentInfo.PhoneInt1;

                    var phoneType1Txt =
                        (RadDropDownList)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("ddlPhoneType1");
                    phoneType1Txt.SelectedText = studentInfo.Phone1Type;
                    lblHiddenPhone1Type.Text = studentInfo.Phone1Type;

                    var phone2Txt =
                        (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("txtPhone2");
                    phone2Txt.Text = studentInfo.Phone2;
                    lblHiddenPhone2.Text = studentInfo.Phone2;

                    var phone2Intltxt =
                        (CheckBox)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("chkPhone2Intl");
                    phone2Intltxt.Checked = studentInfo.PhoneInt2;
                    lblHiddernPhone2Intl.Checked = studentInfo.PhoneInt2;

                    var phoneType2Txt =
                        (RadDropDownList)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("ddlPhoneType2");
                    phoneType2Txt.SelectedText = studentInfo.Phone2Type;
                    lblHiddenPhone2Type.Text = studentInfo.Phone2Type;

                    var addressIntltxt =
                        (CheckBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("chkIntlAddress");
                    addressIntltxt.Checked = studentInfo.IsAddressInt1;
                    lblHiddenAddressIntl.Checked = studentInfo.IsAddressInt1;

                    var address1Txt =
                        (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("txtAddress1");
                    address1Txt.Text = studentInfo.Address1;
                    lblHiddenAddress1.Text = studentInfo.Address1;

                    var address2Txt =
                        (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("txtAddress2");
                    address2Txt.Text = studentInfo.Address2;
                    lblHiddenAddress2.Text = studentInfo.Address2;

                    var citytxt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("txtCity");
                    citytxt.Text = studentInfo.City;
                    lblHiddenCity.Text = studentInfo.City;

                    var statetxt =
                        (RadDropDownList)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("ddlState");
                    statetxt.SelectedText = studentInfo.State;
                    lblHiddenState.Text = studentInfo.State;

                    var otherStatetxt =
                        (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("txtOtherState");
                    otherStatetxt.Text = studentInfo.OtherState;
                    lblHiddenOtherState.Text = studentInfo.OtherState;

                    var ziptxt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("txtZip");
                    ziptxt.Text = studentInfo.Zip;
                    lblHiddenZip.Text = studentInfo.Zip;

                    var countrytxt =
                        (RadDropDownList)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("ddlCountries");
                    countrytxt.SelectedText = studentInfo.Country;
                    lblHiddenCountry.Text = studentInfo.Country;

                    var otherStatelbl =
                        (Label)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("lblOtherState");

                    phone1Txt.MaxLength = phone1Intltxt.Checked == false ? 10 : 16;
                    phone2Txt.MaxLength = phone2Intltxt.Checked == false ? 10 : 16;
                    ziptxt.MaxLength = addressIntltxt.Checked == false ? 5 : 20;

                    if (addressIntltxt.Checked)
                    {
                        statetxt.Enabled = false;
                        otherStatelbl.Visible = true;
                        otherStatetxt.Visible = true;
                    }

                }
                catch (Exception exc)
                {
                    Exceptions.ProcessModuleLoadException(this, exc);
                }
            }
        }

        public ModuleActionCollection ModuleActions
        {
            get
            {
                var actions = new ModuleActionCollection
                    {
                        {
                            GetNextActionID(), Localization.GetString("EditModule", LocalResourceFile), "", "", "",
                            EditUrl(), false, SecurityAccessLevel.Edit, true, false
                        }
                    };
                return actions;
            }
        }

        protected void btnEditEmail_Click(object sender, EventArgs e)
        {

            Label Note1txt = (Label)RadPanelBar1.FindItemByValue("templateHolder1").FindControl("lblNote1");
            Note1txt.Text = @"Please do the changes below and click 'Request Email Change' button for processing your request.";
            RadTextBox HomeEmailtxt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder1").FindControl("txtHomeEmail");
            HomeEmailtxt.Enabled = true;
            RadTextBox WorkEmailtxt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder1").FindControl("txtWorkEmail");
            WorkEmailtxt.Enabled = true;
            Button EditEmailbtn = (Button)RadPanelBar1.FindItemByValue("templateHolder1").FindControl("btnEditEmail");
            EditEmailbtn.Visible = false;
            Button ChangeEmailbtn = (Button)RadPanelBar1.FindItemByValue("templateHolder1").FindControl("btnChangeEmail");
            ChangeEmailbtn.Visible = true;
        }

        protected void btnEditPhone_Click(object sender, EventArgs e)
        {
            Label Note2txt = (Label)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("lblNote2");
            Note2txt.Text = "Please do the changes below and click 'Request Phone Change' button for processing your request.";
            RadTextBox Phone1txt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("txtPhone1");
            Phone1txt.Enabled = true;
            CheckBox Phone1Intlchk = (CheckBox)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("chkPhone1Intl");
            Phone1Intlchk.Enabled = true;
            RadDropDownList Phone1Typetxt = (RadDropDownList)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("ddlPhoneType1");
            Phone1Typetxt.Enabled = true;
            RadTextBox Phone2txt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("txtPhone2");
            Phone2txt.Enabled = true;
            CheckBox Phone2Intlchk = (CheckBox)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("chkPhone2Intl");
            Phone2Intlchk.Enabled = true;
            RadDropDownList Phone2Typetxt = (RadDropDownList)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("ddlPhoneType2");
            Phone2Typetxt.Enabled = true;
            Button EditPhonebtn = (Button)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("btnEditPhone");
            EditPhonebtn.Visible = false;
            Button ChangePhonebtn = (Button)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("btnChangePhone");
            ChangePhonebtn.Visible = true;

            Phone1txt.MaxLength = Phone1Intlchk.Checked == false ? 10 : 16;
            Phone2txt.MaxLength = Phone2Intlchk.Checked == false ? 10 : 16;
        }

        protected void btnEditAddress_Click(object sender, EventArgs e)
        {
            Label Note3txt = (Label)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("lblNote3");
            Note3txt.Text = "Please do the changes below and click 'Request Address Change' button for processing your request.";
            CheckBox AddressIntltxt = (CheckBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("chkIntlAddress");
            AddressIntltxt.Enabled = true;
            RadTextBox Address1txt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("txtAddress1");
            Address1txt.Enabled = true;
            RadTextBox Address2txt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("txtAddress2");
            Address2txt.Enabled = true;
            RadTextBox Citytxt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("txtCity");
            Citytxt.Enabled = true;
            RadDropDownList Statetxt = (RadDropDownList)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("ddlState");
            Statetxt.Enabled = true;
            Label OtherStatelbl = (Label)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("lblOtherState");
            RadTextBox OtherStatetxt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("txtOtherState");
            RadTextBox Ziptxt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("txtZip");
            Ziptxt.Enabled = true;
            RadDropDownList Countrytxt = (RadDropDownList)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("ddlCountries");
            Countrytxt.Enabled = true;
            Button EditAddressbtn = (Button)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("btnEditAddress");
            EditAddressbtn.Visible = false;
            Button ChangeAddressbtn = (Button)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("btnChangeAddress");
            ChangeAddressbtn.Visible = true;

            if (AddressIntltxt.Checked == true)
            {
                Statetxt.Enabled = false;
                OtherStatelbl.Visible = true;
                OtherStatetxt.Visible = true;
            }
            else
            {
                Statetxt.Enabled = true;
                OtherStatelbl.Visible = false;
                OtherStatetxt.Visible = false;
            }
        }

        protected void btnChangeEmail_Click(object sender, EventArgs e)
        {
            RadTextBox HomeEmailtxt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder1").FindControl("txtHomeEmail");
            RadTextBox WorkEmailtxt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder1").FindControl("txtWorkEmail");
            Label HomeEmailErrMsg = (Label)RadPanelBar1.FindItemByValue("templateHolder1").FindControl("lbltxtHomeEmailErrMsg");
            Label WorkEmailErrMsg = (Label)RadPanelBar1.FindItemByValue("templateHolder1").FindControl("lbltxtWorkEmailErrMsg");

            if (!string.IsNullOrEmpty(HomeEmailtxt.Text))
            {
                HomeEmailErrMsg.Visible = (IsValidEmailAddress(HomeEmailtxt.Text) == false) ? true : false;
            }

            if (!string.IsNullOrEmpty(WorkEmailtxt.Text))
            {
                WorkEmailErrMsg.Visible = (IsValidEmailAddress(WorkEmailtxt.Text) == false) ? true : false;
            }

            if (HomeEmailErrMsg.Visible == false && WorkEmailErrMsg.Visible == false && (lblHiddenHomeEmail.Text != HomeEmailtxt.Text || lblHiddenWorkEmail.Text != WorkEmailtxt.Text))
            {
                RadPanelBar1.FindItemByValue("templateHolder1").FindControl("lblReqEmailStatus").Visible = true;
                EmailRequestSendEmail(HomeEmailtxt.Text, WorkEmailtxt.Text);
            }
        }

        protected void btnChangePhone_Click(object sender, EventArgs e)
        {
            RadTextBox Phone1txt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("txtPhone1");
            Phone1txt.Enabled = true;
            CheckBox Phone1Intlchk = (CheckBox)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("chkPhone1Intl");
            Phone1Intlchk.Enabled = true;
            RadDropDownList Phone1Typetxt = (RadDropDownList)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("ddlPhoneType1");
            Phone1Typetxt.Enabled = true;
            RadTextBox Phone2txt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("txtPhone2");
            Phone2txt.Enabled = true;
            CheckBox Phone2Intlchk = (CheckBox)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("chkPhone2Intl");
            Phone2Intlchk.Enabled = true;
            RadDropDownList Phone2Typetxt = (RadDropDownList)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("ddlPhoneType2");
            Phone2Typetxt.Enabled = true;

            if (Phone1txt.Text != lblHiddenPhone1.Text || Phone1Intlchk.Text != Phone1Intlchk.Text || Phone1Typetxt.SelectedText != Phone1Typetxt.SelectedText ||
                Phone2txt.Text != lblHiddenPhone2.Text || Phone2Intlchk.Text != Phone2Intlchk.Text || Phone2Typetxt.SelectedText != Phone2Typetxt.SelectedText)
            {
                RadPanelBar1.FindItemByValue("templateHolder2").FindControl("lblReqPhoneStatus").Visible = true;
                PhoneRequestSendEmail(Phone1txt.Text, Phone1Intlchk.Checked, Phone1Typetxt.SelectedText, Phone2txt.Text, Phone2Intlchk.Checked, Phone2Typetxt.SelectedText);
            }
        }

        protected void btnChangeAddress_Click(object sender, EventArgs e)
        { 
            CheckBox AddressIntltxt = (CheckBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("chkIntlAddress");
            RadTextBox Address1txt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("txtAddress1");
            RadTextBox Address2txt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("txtAddress2");
            RadTextBox Citytxt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("txtCity");
            RadDropDownList Statetxt = (RadDropDownList)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("ddlState");
            RadTextBox OtherStatetxt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("txtOtherState");
            RadTextBox Ziptxt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("txtZip");
            RadDropDownList Countrytxt = (RadDropDownList)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("ddlCountries");

            if (AddressIntltxt.Checked != lblHiddenAddressIntl.Checked || Address1txt.Text != lblHiddenAddress1.Text || Address2txt.Text != lblHiddenAddress2.Text ||
                Citytxt.Text != lblHiddenCity.Text || lblHiddenState.Text != Statetxt.SelectedText || lblHiddenOtherState.Text != OtherStatetxt.Text ||
                Ziptxt.Text != lblHiddenZip.Text || lblHiddenCountry.Text != Countrytxt.SelectedText)
            {
                RadPanelBar1.FindItemByValue("templateHolder3").FindControl("lblReqAddressStatus").Visible = true;
                AddressRequestSendEmail(AddressIntltxt.Checked, Address1txt.Text, Address2txt.Text, Citytxt.Text,
                Statetxt.SelectedText, OtherStatetxt.Text, Ziptxt.Text, Countrytxt.SelectedText);
            }
        }

        public bool IsValidEmailAddress(string email)
        {
            var regex = new Regex(@"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$");
            return regex.IsMatch(email) && !email.EndsWith(".");
        }

        protected void EmailRequestSendEmail(string HomeEmail, string WorkEmail)
        {
            string RecipientEmail = ConfigurationManager.AppSettings["SMTPUserName"];

            using (MailMessage mailMessage = new MailMessage(RecipientEmail, RecipientEmail))
            {
                mailMessage.Subject = "Request for changing Email Information received from " +
                                      lblHiddenFullName.Text + " at " + DateTime.Now.ToString();
                mailMessage.Body = (" \r\n " +
                                    " Below is the request for changing email information has been received. " +
                                    " \r\n \r\n");
                mailMessage.Body = mailMessage.Body + " \r\n " + "Student Number : " + lblHiddenUserName.Text;

                if (lblHiddenHomeEmail.Text != HomeEmail)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n \r\n " + "Home Email : " + HomeEmail;
                }

                if (lblHiddenWorkEmail.Text != WorkEmail)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n " + "Work Email : " + WorkEmail;
                }

                mailMessage.IsBodyHtml = false;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["SMTPHost"];
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred =
                    new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"],
                        ConfigurationManager.AppSettings["SMTPPassword"]);
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["port"]);
                smtp.Send(mailMessage);
            }
        }

        protected void PhoneRequestSendEmail(string phone1, bool phone1Intl, string phone1Type, string phone2, bool phone2Intl, string phone2Type)
        {
            string RecipientEmail = ConfigurationManager.AppSettings["SMTPUserName"];

            using (MailMessage mailMessage = new MailMessage(RecipientEmail, RecipientEmail))
            {
                mailMessage.Subject = "Request for changing Phone Information received from " +
                                      lblHiddenFullName.Text + " at " + DateTime.Now.ToString();
                mailMessage.Body = (" \r\n " +
                                    " Below is the request for changing email information has been received. " +
                                    " \r\n \r\n");
                mailMessage.Body = mailMessage.Body + " \r\n " + "Student Number : " + lblHiddenUserName.Text;

                if (lblHiddenPhone1.Text != phone1)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n \r\n " + "Phone 1 : " + phone1;
                }
                if (lblHiddernPhone1Intl.Checked != phone1Intl && phone1Intl == true)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n " + "Phone 1 International : Yes ";
                }
                if (lblHiddernPhone1Intl.Checked != phone1Intl && phone1Intl == false)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n " + "Phone 1 International : No ";
                }
                if (lblHiddenPhone1Type.Text != phone1Type)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n " + "Phone 1 Type : " + phone1Type;
                }
                if (lblHiddenPhone2.Text != phone2)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n \r\n " + "Phone 2 : " + phone2;
                }
                if (lblHiddernPhone2Intl.Checked != phone2Intl && phone2Intl == true)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n " + "Phone 2 International : Yes ";
                }
                if (lblHiddernPhone2Intl.Checked != phone2Intl && phone2Intl == false)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n " + "Phone 2 International : No ";
                }
                if (lblHiddenPhone2Type.Text != phone2Type)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n " + "Phone 2 Type : " + phone2Type;
                }
                mailMessage.IsBodyHtml = false;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = ConfigurationManager.AppSettings["SMTPHost"];
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred =
                    new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"],
                        ConfigurationManager.AppSettings["SMTPPassword"]);
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["port"]);
                smtp.Send(mailMessage);
            }
        }

        protected void AddressRequestSendEmail(bool AddIntl, string Address1, string Address2, string City, string State, string OtherState, string Zip, string Country)
        {
            string RecipientEmail = ConfigurationManager.AppSettings["SMTPUserName"];

            using (var mailMessage = new MailMessage(RecipientEmail, RecipientEmail))
            {
                mailMessage.Subject = "Request for changing Address Information received from " +
                                      lblHiddenFullName.Text + " at " + DateTime.Now.ToString();
                mailMessage.Body = (" \r\n " +
                                    " Below is the request for changing address information has been received. " +
                                    " \r\n \r\n");
                mailMessage.Body = mailMessage.Body + " \r\n " + "Student Number : " + lblHiddenUserName.Text;

                if (lblHiddenAddressIntl.Checked != AddIntl && AddIntl == true)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n \r\n  " + "Address International : Yes ";
                }
                if (lblHiddenAddressIntl.Checked != AddIntl && AddIntl == false)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n " + "Address International : No ";
                }
                if (lblHiddenAddress1.Text != Address1)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n " + "Address 1 : " + Address1;
                }
                if (lblHiddenAddress2.Text != Address2)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n " + "Address 2 : " + Address2;
                }
                if (lblHiddenCity.Text != City)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n " + "City : " + City;
                }
                if (lblHiddenState.Text != State)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n " + "State : " + State;
                }
                if (lblHiddenOtherState.Text != OtherState)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n " + "Other State : " + OtherState;
                }
                if (lblHiddenZip.Text != Zip)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n " + "Zip : " + Zip;
                }
                if (lblHiddenCountry.Text != Country)
                {
                    mailMessage.Body = mailMessage.Body + " \r\n " + "Country : " + Country;
                }
                mailMessage.IsBodyHtml = false;
                var smtp = new SmtpClient
                {
                    Host = ConfigurationManager.AppSettings["SMTPHost"],
                    EnableSsl = true
                };
                var networkCred =
                    new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"],
                        ConfigurationManager.AppSettings["SMTPPassword"]);
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = networkCred;
                smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["port"]);
                smtp.Send(mailMessage);
            }
        }

        protected void chkIntlAddress_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox AddressIntltxt = (CheckBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("chkIntlAddress");
            RadDropDownList Statetxt = (RadDropDownList)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("ddlState");
            Label OtherStatelbl = (Label)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("lblOtherState");
            RadTextBox OtherStatetxt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("txtOtherState");
            RadTextBox Ziptxt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder3").FindControl("txtZip");

            Ziptxt.Text = string.Empty;
            Ziptxt.MaxLength = AddressIntltxt.Checked == false ? 5 : 20;

            if (AddressIntltxt.Checked == true)
            {
                Statetxt.Enabled = false;
                OtherStatelbl.Visible = true;
                OtherStatetxt.Visible = true;
            }
            else
            {
                Statetxt.Enabled = true;
                OtherStatelbl.Visible = false;
                OtherStatetxt.Visible = false;
            }
        }

        protected void chkPhone1Intl_CheckedChanged(object sender, EventArgs e)
        {
            RadTextBox Phone1txt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("txtPhone1");
            Phone1txt.Text = string.Empty;
            CheckBox Phone1Intlchk = (CheckBox)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("chkPhone1Intl");
            Phone1txt.MaxLength = Phone1Intlchk.Checked == false ? 10 : 16;
        }

        protected void chkPhone2Intl_CheckedChanged(object sender, EventArgs e)
        {
            RadTextBox Phone2txt = (RadTextBox)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("txtPhone2");
            Phone2txt.Text = string.Empty;
            CheckBox Phone2Intlchk = (CheckBox)RadPanelBar1.FindItemByValue("templateHolder2").FindControl("chkPhone2Intl");
            Phone2txt.MaxLength = Phone2Intlchk.Checked == false ? 10 : 16;
        }
    }
}