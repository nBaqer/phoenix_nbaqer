﻿

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Caching;
using System.Web.UI.WebControls;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Services.Localization;
using FAME.Advantage.Common;
using FAME.AdvantageV1.BusinessFacade;
using FAME.AdvantageV1.Common;
using PortalData;
using Telerik.Web.UI;


namespace Christoc.Modules.DegreeAudit
{
    /// <summary>
    /// Degree Audit Report Page
    /// </summary>
    public partial class View : DegreeAuditModuleBase, IActionable
    {
        protected string StudentNumber;
        private static readonly object Lock = new object();
        public string PStudentNumber;
        public string PStudentName;
        protected bool IsProgramModified = false;

        #region Load Events, and Buttons Clicks (Get Report and Export To Pdf)
        /// <summary>
        /// Page Load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                StudentNumber = DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo().Username;
                if (HttpContext.Current.Session["PortalAdvAppSettings"] == null)
                {
                    HttpContext.Current.Session["PortalAdvAppSettings"] = AdvAppSettings.GetAppSettings();
                }

               if (!IsPostBack)
                {
                    var hasCourses = PortalFacade.AcademicFacade.IsStudentAssignedCourses(StudentNumber);
                    if (hasCourses == false)
                    {
                        DegreeAuditWarningMessage.InnerText = Constant.X_WARNING_USER_HAS_NO_RECORDS;
                        btnSearch.Enabled = false;
                        return;
                    }

                    btnSearch.Enabled = true;
                    DegreeAuditWarningMessage.InnerText = string.Empty;
                    BindDropDowns();
                }
            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException("An error occured while your load the page", this, ex);
            }
            //RadAjaxManager1.AjaxSettings.AddAjaxSetting(ddlEnrollment, masterGrid);
            //RadAjaxManager1.AjaxSettings.AddAjaxSetting(<ajaxified control>, <updated control>, <LoadingPanel> or null if none);
        }

        /// <summary>
        /// Get the transcript Button Event Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            GenerateGraduateAuditTable();
        }

        /// <summary>
        /// Export to Pdf Button Click event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BtnExportToPdfClick(object sender, EventArgs e)
        {
            const string HEADER_MIDDLE_CELL = "<h2>Sushi Bar Menu</h2>Hot Offers";
            try
            {
                masterGrid.ExportSettings.Pdf.PageHeight = Unit.Parse("215mm");
                masterGrid.ExportSettings.Pdf.PageWidth = Unit.Parse("280mm");
                masterGrid.ExportSettings.ExportOnlyData = true;
                masterGrid.ExportSettings.OpenInNewWindow = true;
                masterGrid.MasterTableView.BorderWidth = 1;
                masterGrid.MasterTableView.ShowHeader = true;
                masterGrid.ExportSettings.Pdf.PageHeader.MiddleCell.Text = HEADER_MIDDLE_CELL;
                masterGrid.ExportSettings.Pdf.PageHeader.MiddleCell.TextAlign =
                    GridPdfPageHeaderFooterCell.CellTextAlign.Center;
                masterGrid.MasterTableView.ExportToPdf();
            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException("An error occured while attempting to retrieve the filter options", this, ex);
            }
        }

        protected void ddlEnrollment_SelectedIndexChanged(object sender, EventArgs e)
        {
            masterGrid.DataSource = null;
            masterGrid.DataBind();
        }

        #endregion


        public ModuleActionCollection ModuleActions
        {
            get
            {
                var actions = new ModuleActionCollection
                    {
                        {
                            GetNextActionID(), Localization.GetString("EditModule", LocalResourceFile), "", "", "",
                            EditUrl(), false, SecurityAccessLevel.Edit, true, false
                        }
                    };
                return actions;
            }
        }

        #region Future ExporToPdf
        //private void GetAndExportReport()
        //{

        //    Byte[] getReportAsBytes = null;
        //    int intTabId = 0;

        //    string programId = ddlEnrollment.SelectedValue;


        //    string strReportPath = ConfigurationManager.AppSettings["Reports.ReportsFolder"];

        //    try
        //    {
        //        // getReportAsBytes = (new MyScheduleReportGenerator()).RenderReport("pdf", strReportPath, programId, courseId, termId, ", ", studentNumber, ");
        //        // getReportAsBytes = (new MyProgressReportGenerator()).RenderReport("pdf", strReportPath, programId);
        //    }
        //    catch (Exception messageException)
        //    {
        //        throw messageException.InnerException;
        //    }


        //    ExportReport("pdf", getReportAsBytes);

        //}

        //private void ExportReport(string exportFormat, Byte[] getReportAsBytes)
        //{
        //    string strExtension;
        //    string strMimeType;
        //    switch (exportFormat.ToLower())
        //    {
        //        case "pdf":
        //            strExtension = "pdf";
        //            strMimeType = "application/pdf";
        //            break; 


        //        case "excel":
        //            strExtension = "xls";
        //            strMimeType = "application/vnd.excel";
        //            break;



        //        case "csv":
        //            strExtension = "csv";
        //            strMimeType = "text/csv";
        //            break;
        //        default:
        //            throw new Exception("Unrecognized type. Type must be PDF, Excel or Image, HTML.");
        //    }
        //    Session["SSRS_FileExtension"] = strExtension;
        //    Session["SSRS_MimeType"] = strMimeType;
        //    Session["SSRS_ReportOutput"] = getReportAsBytes;

        //    //This code builds the URL and renders the report
        //    //In the module definition for user control  - DisplayReport.ascx create a key - MyScheduleReport
        //    //Detailed documentation available at http://www.adefwebserver.com/DotNetNukeHELP/NavigateURL/ website
        //    string myUrl = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "MyDegreeAuditReport", "mid=" + ModuleId.ToString(CultureInfo.InvariantCulture));
        //    string script11 = "window.open('";
        //    string script12 = myUrl;
        //    string script13 = "','SSRSReport1','resizable=yes,left=200px,top=200px,modal=no');";
        //    string jsScript = script11 + script12 + script13;
        //    ScriptManager.RegisterStartupScript(Page, typeof(Page), "myscript", jsScript, true);

        //    //This code works - balaji 6.19.2014
        //    // Response.Redirect(Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "MyScheduleReport", "mid=" + ModuleId.ToString()));

        //    //This code also works balaji 6.19.2014
        //    //string myUrl = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "MyScheduleReport", "mid=" + ModuleId.ToString());
        //    //string script1 = string.Format("window.open('http://localhost/advantageportal/My-Info/My-Schedule/ctl/MyScheduleReport/mid/2494','SSRSReport1','resizable=yes,left=200px,top=200px,modal=no');", DateTime.Now.ToLongTimeString());
        //    //ScriptManager.RegisterStartupScript(Page, typeof(Page), "myscript", script1, true);



        //}
        #endregion

        #region GraduateAudit

        /// <summary>
        /// Generate the Audittable from Advantage Db.
        /// </summary>
        private void GenerateGraduateAuditTable()
        {
            //Get info for report....
            IList<string> headerTitles = new List<string>();
            var headerData = new GraduateAuditGroupDto();
            IList<GraduateAuditGroupDto> detailDataList = new List<GraduateAuditGroupDto>();
            var lstConfigOutputs1 = PortalFacade.AcademicFacade.GetConfigSettings(ddlEnrollment.SelectedValue);
            var lstConfigOutputs2 = PortalFacade.AcademicFacade.GetNameAndNumber(ddlEnrollment.SelectedValue);

            foreach (var s in lstConfigOutputs2)
            {
                PStudentNumber = s.Studentnumber;
                PStudentName = s.Name;
            }

            string gradeCourseRepetitionsMethod = null, gradesFormat = null, transcriptType = null, addcreditsbyservice = null;
            bool includeHoursForFailingGrade = false;

            foreach (var p in lstConfigOutputs1)
            {
                gradeCourseRepetitionsMethod = p.GradeCourseRepetitionsMethod;
                gradesFormat = p.GradesFormat;
                transcriptType = p.TranscriptType;
                addcreditsbyservice = p.AddCreditsByService;
                includeHoursForFailingGrade = p.IncludeHoursForFailingGrade;
            }

            GraduateAuditInfo auditInfo = null;
            try
            {
                auditInfo = new GraduateAuditInfo
                {
                    StuEnrollmentId = ddlEnrollment.SelectedValue,
                    ItemCollection = ddlEnrollment.Items,
                    GradeReps = gradeCourseRepetitionsMethod,
                    IncludeHours = includeHoursForFailingGrade,
                    StrGradesFormat = gradesFormat,
                    TranscripType = transcriptType,
                    Addcreditsbyservice = addcreditsbyservice,
                    Dsgradesysdetailids = (new TranscriptFacade()).GetGradesSysDetailId()
                };
            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException("An error occured while attempting to retrieve the report", this, ex);
            }

            var gridDataSource = new List<GraduateAuditGroupRow>();

            // Cache operation... We need to define the object cache using the Enrollment code as key.
            var listDegreeAudit = ddlEnrollment.SelectedValue;
            var gridHeadertitle = listDegreeAudit + "Title";


            //var degreeAuditItems = (List<GraduateAuditGroupRow>)Cache[listDegreeAudit];
            List<GraduateAuditGroupRow> degreeAuditItems;

            lock (Lock)
            {
                // Ensure that the data was not loaded by a concurrent thread 
                // while waiting for lock.
                degreeAuditItems = (List<GraduateAuditGroupRow>)Cache[listDegreeAudit];
                if (degreeAuditItems == null)
                {
                    var facade = new GraduateAuditFacade();
                    facade.CreateReportList(auditInfo, ref headerTitles, ref headerData, ref detailDataList);
                    gridDataSource.Add(GraduateAuditGroupRow.AddGraduateAuditGroupDto(headerData));
                    gridDataSource.Add(GraduateAuditGroupRow.Factory());
                    gridDataSource.AddRange(from dto in detailDataList select GraduateAuditGroupRow.AddGraduateAuditGroupDto(dto));

                    degreeAuditItems = gridDataSource;

                    Cache.Add(listDegreeAudit, degreeAuditItems, null, DateTime.Now.AddSeconds(60),
                        Cache.NoSlidingExpiration, CacheItemPriority.High, null);

                    Cache.Add(gridHeadertitle, headerTitles, null, DateTime.Now.AddSeconds(60),
                        Cache.NoSlidingExpiration, CacheItemPriority.High, null);
                }
            }

            masterGrid.MasterTableView.RowIndicatorColumn.Visible = false;
            masterGrid.MasterTableView.ExpandCollapseColumn.Visible = false;
            masterGrid.MasterTableView.ShowHeader = true;

            var index = 0;
            if (headerTitles.Count == 0)
            {
                headerTitles = (IList<string>)Cache[gridHeadertitle];
            }
            foreach (string s in headerTitles)
            {
                if (masterGrid.Columns == null)
                {
                }
                else
                    masterGrid.Columns[index].HeaderText = s;
                index = index + 1;
            }

            //Change first row text to sub total
            degreeAuditItems[0].Descrip = "Sub Total";

            foreach (var element in degreeAuditItems.ToList())
            {
                element.Descrip = "Sub total";
                break;

            }

            masterGrid.DataSource = degreeAuditItems;
            masterGrid.DataBind();
        }

        protected void MasterGridItemCreated(object sender, GridItemEventArgs e)
        {
            if (e.Item is GridHeaderItem)
            {
                e.Item.Style["font-size"] = "8pt";
                e.Item.Style["background-color"] = "#E1E1E1";
                e.Item.Style["font-weight"] = "normal";
            }
            var dataItem = e.Item as GridDataItem;
            if (dataItem != null)
            {
                dynamic item = dataItem;
                foreach (TableCell cell in item.Cells)
                {
                    cell.Style["font-family"] = "Arial Unicode MS";
                    cell.Style["font-size"] = "8pt";
                    cell.Style["font-weight"] = "normal";

                }
                if ((item.RowIndex == 2))
                {
                    foreach (TableCell cell in item.Cells)
                    {
                        cell.Style["background-color"] = "#E2E6E8";
                        cell.Style["font-weight"] = "normal";

                    }
                }
            }
        }

        protected void GetGraduateAudit(object sender, EventArgs e)
        {
            IsProgramModified = true;
            GenerateGraduateAuditTable();
            SelectedEnrollment.Text = lblEnrollment.Text;
        }

        /// <summary>
        /// Get data for dropdown controls and bind the data
        /// </summary>
        private void BindDropDowns()
        {
            try
            {
                ddlEnrollment.DataTextField = "Enrollment";
                ddlEnrollment.DataValueField = "EnrollmentId";
                var enrollment = PortalFacade.AcademicFacade.GetEnrollments(StudentNumber); //  degData.GetEnrollments(StudentNumber);
                ddlEnrollment.DataSource = enrollment;
                ddlEnrollment.DataBind();
            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException("An error occured while attempting to retrieve the filter options", this, ex);
            }
        }
    }

        #endregion
}
//201109BARROD201