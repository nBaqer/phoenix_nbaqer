﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ScheduleData.cs" company="FameInc.com">
//  FameInc.com 
// </copyright>
// <summary>
//   Defines the ScheduleData type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Christoc.Modules.MySchedule.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Christoc.Modules.MySchedule.Data.OutputData;

    using MoreLinq;

    /// <summary>
    /// The schedule data.
    /// </summary>
    public class ScheduleData
    {
        #region Private Class Variables

        // linq data context object

        /// <summary>
        /// The db context.
        /// </summary>
        public readonly StudentScheduleLinqClassesDataContext dbContext = new StudentScheduleLinqClassesDataContext();

        #endregion

        #region Public Class Methods

        /// <summary>
        /// get the enrollments for the student using the student number
        /// </summary>
        /// <param name="studentNumber"></param>
        /// <returns></returns>
        public List<EnrollmentOutput> GetEnrollments(string studentNumber)
        {
            var data = (from e in this.dbContext.sp_getStuEnrollmentsPortal(studentNumber)
                        select new EnrollmentOutput()
                        {
                            EnrollmentId = e.StuEnrollId,
                            Enrollment = e.Enrollment
                        }).OrderBy(n => n.Enrollment).ToList();

            return data;
        }


        /// <summary>
        /// get the available courses for the student for the dropdown list
        /// </summary>
        /// <param name="stuEnrollId">
        /// </param>
        /// <param name="termId">
        /// The term Id.
        /// </param>
        /// <param name="instructorId">
        /// The instructor Id.
        /// </param>
        /// <returns>
        /// </returns>
        public List<CourseOutput> GetCourses(Guid stuEnrollId, Guid? termId, Guid? instructorId)
        {
            var data = from res in this.dbContext.arResults
                join enr in this.dbContext.arStuEnrollments on res.StuEnrollId equals enr.StuEnrollId
                join cs in this.dbContext.arClassSections on res.TestId equals cs.ClsSectionId
                join cst in this.dbContext.arClassSectionTerms on cs.ClsSectionId equals cst.ClsSectionId
                join t in this.dbContext.arTerms on cst.TermId equals t.TermId
                join r in this.dbContext.arReqs on cs.ReqId equals r.ReqId
                join cmpgrpcmps in this.dbContext.syCmpGrpCmps on cs.CampusId equals cmpgrpcmps.CampusId
                join u in this.dbContext.syUsers on cs.InstructorId equals u.UserId
                where enr.StuEnrollId == stuEnrollId
                select new CourseOutput()
                {
                    ReqId = r.ReqId,
                    Description = r.Descrip,
                    GradeSystemDetailId = res.arGradeSystemDetail.GrdSysDetailId,
                    TermId = cst.TermId,
                    InstructorId = cs.InstructorId
                };

            if (termId != null)
            {
                data = data.Where(n => n.TermId == termId);
            }

            if (instructorId != null)
            {
                data = data.Where(n => n.InstructorId == instructorId);
            }

            data = data.Distinct();
            

            // remove the items that are dropped courses
            var outputData = new List<CourseOutput>();
            foreach (var d in data)
            {
                if (d.GradeSystemDetailId != null)
                {
                    var arGrSystemDtl = this.dbContext.arGradeSystemDetails.FirstOrDefault(n => n.GrdSysDetailId == d.GradeSystemDetailId);
                    var isDrop = arGrSystemDtl != null && arGrSystemDtl.IsDrop;

                    if (!isDrop)
                    {
                        outputData.Add(d);
                    }
                }
                else
                {
                    outputData.Add(d);
                }
            }

            return outputData.Distinct().OrderBy(n => n.Description).ToList(); 
        }

        /// <summary>
        /// get the available instructors for the student schedule dropdown list
        /// </summary>
        /// <param name="stuEnrollId"></param>
        /// <param name="termId"></param>
        /// <returns></returns>
        public List<InstructorOutput> GetInstructors(Guid stuEnrollId, Guid? termId )
        {
            var data = from res in this.dbContext.arResults
                join enr in this.dbContext.arStuEnrollments on res.StuEnrollId equals enr.StuEnrollId
                join cs in this.dbContext.arClassSections on res.TestId equals cs.ClsSectionId
                join cst in this.dbContext.arClassSectionTerms on cs.ClsSectionId equals cst.ClsSectionId
                join t in this.dbContext.arTerms on cst.TermId equals t.TermId
                join r in this.dbContext.arReqs on cs.ReqId equals r.ReqId
                join cmpgrpcmps in this.dbContext.syCmpGrpCmps on cs.CampusId equals cmpgrpcmps.CampusId
                join u in this.dbContext.syUsers on cs.InstructorId equals u.UserId
                where enr.StuEnrollId == stuEnrollId
                select new InstructorOutput()
                {
                    InstructorId = u.UserId,
                    Description = u.FullName,
                    TermId = cst.TermId
                };

            if (termId != null)
            {
                data = data.Where(n => n.TermId == termId);
            }

            List<InstructorOutput> outputData = data.ToList();

            outputData = outputData.DistinctBy(n => n.Description).OrderBy(n => n.Description).ToList();

            return outputData.ToList();


        }

        /// <summary>
        /// get the available terms for the student schedule for the dropdown list
        /// </summary>
        /// <param name="stuEnrollId"></param>
        /// <returns></returns>
        public List<TermOutput> GetTerms(Guid stuEnrollId)
        {
            var data = (from res in this.dbContext.arResults
                        join enr in this.dbContext.arStuEnrollments on res.StuEnrollId equals enr.StuEnrollId
                        join cs in this.dbContext.arClassSections on res.TestId equals cs.ClsSectionId
                        join cst in this.dbContext.arClassSectionTerms on cs.ClsSectionId equals cst.ClsSectionId
                        join t in this.dbContext.arTerms on cst.TermId equals t.TermId
                        join r in this.dbContext.arReqs on cs.ReqId equals r.ReqId
                        join cmpgrpcmps in this.dbContext.syCmpGrpCmps on cs.CampusId equals cmpgrpcmps.CampusId
                        where enr.StuEnrollId == stuEnrollId
                        select new TermOutput() { TermId = t.TermId, Description = t.TermDescrip }).Distinct()
                .OrderBy(n => n.Description)
                .ToList();

            return data;
        }

        /// <summary>
        /// get the student schedule using the parameters passed in
        /// </summary>
        /// <param name="stuEnrollId"></param>
        /// <param name="instructorId"></param>
        /// <param name="reqId"></param>
        /// <param name="termId"></param>
        /// <param name="stDate"></param>
        /// <param name="eDate"></param>
        /// <returns></returns>
        public List<ScheduleOutput> GetStudentSchedule( Guid stuEnrollId, Guid? instructorId, Guid? reqId, Guid? termId, DateTime? stDate, DateTime? eDate )
        {
            var data = from res in this.dbContext.arResults
                join enr in this.dbContext.arStuEnrollments on res.StuEnrollId equals enr.StuEnrollId
                join cs in this.dbContext.arClassSections on res.TestId equals cs.ClsSectionId
                join cst in this.dbContext.arClassSectionTerms on cs.ClsSectionId equals cst.ClsSectionId
                join t in this.dbContext.arTerms on cst.TermId equals t.TermId
                join r in this.dbContext.arReqs on cs.ReqId equals r.ReqId
                join cmpgrpcmps in this.dbContext.syCmpGrpCmps on cs.CampusId equals cmpgrpcmps.CampusId
                join u in this.dbContext.syUsers on cs.InstructorId equals u.UserId
                       where enr.StuEnrollId == stuEnrollId
                       select new ScheduleOutput()
                {
                    SectionId = cs.ClsSectionId,
                    TermId = t.TermId,
                    Term = t.TermDescrip,
                    StartDate = cs.StartDate,
                    EndDate = cs.EndDate,
                    ReqId = r.ReqId,
                    Course = r.Descrip,
                    Code = r.Code,
                    Section = cs.ClsSection,
                    Credits = r.Credits,
                    Hours = r.Hours,
                    Duration = string.Format("{0} - {1}", cs.StartDate.ToShortDateString(), cs.EndDate.ToShortDateString()),
                    InstructorId = u.UserId,
                    Instructor = u.FullName,
                    GradeSystemDetailId = res.GrdSysDetailId
                };

            if (instructorId != null)
            {
                data = data.Where(n => n.InstructorId == instructorId);
            }

            if (reqId != null)
            {
                data = data.Where(n => n.ReqId == reqId);
            }

            if (termId != null)
            {
                data = data.Where(n => n.TermId == termId);
            }

            data = data.Distinct();


            var outputData = new List<ScheduleOutput>();
            //if (stDate != null && eDate != null)
            //{
            //    outputData.AddRange(data.ToList().Where(d => this.AllowedToAdd(stDate, eDate, d)));
            //    outputData = outputData.Distinct().OrderBy(n => n.StartDate).ToList();
            //}
            //else
            //{
            //    outputData = data.Distinct().OrderByDescending(n => n.StartDate).ThenBy(n => n.Course).ToList();
            //}

            outputData.AddRange(data.ToList().Where(d => this.AllowedToAdd(stDate, eDate, d)));
            outputData = outputData.Distinct().OrderBy(n => n.StartDate).ThenBy(n => n.Course).ToList();
            //outputData = data.Distinct().OrderByDescending(n => n.StartDate).ThenBy(n => n.Course).ToList();

            // remove the items that are dropped courses
            var outputData2 = new List<ScheduleOutput>();
            foreach (var d in outputData)
            {
                if (d.GradeSystemDetailId != null)
                {
                    var arGrSystemDtl = this.dbContext.arGradeSystemDetails.FirstOrDefault(n => n.GrdSysDetailId == d.GradeSystemDetailId);
                    var isDrop = arGrSystemDtl != null && arGrSystemDtl.IsDrop;
                    var grade = arGrSystemDtl != null ? arGrSystemDtl.Grade : string.Empty;

                    if (!isDrop)
                    { 
                        d.Grade = grade;
                        outputData2.Add(d);
                    }
                }
                else
                {
                    outputData2.Add(d);
                }
            }

            return outputData2;

        }

        /// <summary>
        /// The get schedule detail.
        /// </summary>
        /// <param name="sectionId">
        /// The section id.
        /// </param>
        /// <param name="stuEnrollid">
        /// The student enrollid.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        public List<ScheduleDetailOutput> GetScheduleDetail(Guid sectionId, Guid stuEnrollid)
        {
            var data = from res in this.dbContext.arResults
                       join enr in this.dbContext.arStuEnrollments on res.StuEnrollId equals enr.StuEnrollId
                       join cs in this.dbContext.arClassSections on res.TestId equals cs.ClsSectionId
                       join cst in this.dbContext.arClassSectionTerms on cs.ClsSectionId equals cst.ClsSectionId
                       join t in this.dbContext.arTerms on cst.TermId equals t.TermId
                       join r in this.dbContext.arReqs on cs.ReqId equals r.ReqId
                       join cmpgrpcmps in this.dbContext.syCmpGrpCmps on cs.CampusId equals cmpgrpcmps.CampusId
                       join csm in this.dbContext.arClsSectMeetings on res.TestId equals csm.ClsSectionId
                       join p in this.dbContext.syPeriods on csm.PeriodId equals p.PeriodId
                       where enr.StuEnrollId == stuEnrollid
                             && cs.ClsSectionId == sectionId
                       select new ScheduleDetailOutput()
                       {
                           Building = csm.arRoom.arBuilding.BldgDescrip,
                           Period = p.PeriodDescrip,
                           Room = csm.arRoom.Descrip
                       };

            return data.Distinct().OrderBy(n => n.Period).ToList();
        }

        #endregion

        #region Private Class Methods

        /// <summary>
        /// The allowed to add.
        /// </summary>
        /// <param name="startDate">
        /// The start date.
        /// </param>
        /// <param name="endDate">
        /// The end date.
        /// </param>
        /// <param name="newItem">
        /// The new item.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        private bool AllowedToAdd(DateTime? startDate, DateTime? endDate, ScheduleOutput newItem)
        {
            startDate = startDate ?? DateTime.MinValue;
            endDate = endDate ?? DateTime.MaxValue;
            DateTime itemStartDate = newItem.StartDate ?? DateTime.MinValue;
            DateTime itemEndDate = newItem.EndDate ?? DateTime.MaxValue;

            if ((!(itemStartDate >= startDate)) || (!(itemStartDate <= endDate)))
            {
                return false;
            }

            if (itemEndDate > endDate)
            {
                return false;
            }

            return true;
        }

        #endregion
    }
}