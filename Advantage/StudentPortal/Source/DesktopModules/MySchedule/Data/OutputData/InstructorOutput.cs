﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.MySchedule.Data.OutputData
{
    public class InstructorOutput
    {
        public Guid InstructorId { get; set; }
        public string Description { get; set; }
        public Guid? TermId { get; set; }
    }
}