﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.MySchedule.Data.OutputData
{
    public class TermOutput
    {
        public Guid TermId { get; set; }
        public string Description { get; set; }
    }
}