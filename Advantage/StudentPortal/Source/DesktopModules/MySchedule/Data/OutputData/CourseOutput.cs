﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.MySchedule.Data.OutputData
{
    public class CourseOutput
    {
        public Guid ReqId { get; set; }
        public string Description { get; set; }
        public Guid? GradeSystemDetailId { get; set; }
        public Guid? TermId { get; set; }
        public Guid? InstructorId { get; set; }
    }
}