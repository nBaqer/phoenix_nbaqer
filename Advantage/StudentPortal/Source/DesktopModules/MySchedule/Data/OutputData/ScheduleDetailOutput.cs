﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.MySchedule.Data.OutputData
{
    public class ScheduleDetailOutput
    {
        public string Building { get; set; }
        public string Room { get; set; }
        public string Period { get; set; }
        public Guid? GradeSystemDetailId { get; set; }
        
    }
}