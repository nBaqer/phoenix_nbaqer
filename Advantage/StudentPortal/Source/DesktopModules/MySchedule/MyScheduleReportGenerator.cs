﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MyScheduleReportGenerator.cs" company="FameInc.com">
//  FameInc.com 
// </copyright>
// <summary>
//   The my schedule report generator.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace Christoc.Modules.MySchedule
{
    using System;
    using System.Configuration;
    using System.Net;
    using System.Runtime.CompilerServices;
    using System.Threading;

    using FAME.Advantage.Reporting.ReportExecution2005;
    using FAME.Advantage.Reporting.ReportService2005;

    using DataSourceCredentials = FAME.Advantage.Reporting.ReportService2005.DataSourceCredentials;
    using ReportExecutionParameterValue = FAME.Advantage.Reporting.ReportExecution2005.ParameterValue;
    using ReportParameter = FAME.Advantage.Reporting.ReportService2005.ReportParameter;
    using ReportServiceParameterValue = FAME.Advantage.Reporting.ReportService2005.ParameterValue;
    using Warning = FAME.Advantage.Reporting.ReportExecution2005.Warning;

    /// <summary>
    /// The my schedule report generator.
    /// </summary>
    public class MyScheduleReportGenerator
    {
        /// <summary>
        /// The boolean for rendering.
        /// </summary>
        private const bool BoolForRendering = false;

        /// <summary>
        /// The string extension.
        /// </summary>
        private static string strExtension = string.Empty;

        /// <summary>
        /// The string mime type.
        /// </summary>
        private static string strMimeType = string.Empty;

        /// <summary>
        /// The credentials.
        /// </summary>
        private readonly DataSourceCredentials[] credentials = null;

        // Variables needed to render report

        /// <summary>
        /// The device info.
        /// </summary>
        private readonly string deviceInfo = null;

        /// <summary>
        /// The report execution service.
        /// </summary>
        private readonly ReportExecutionService rptExecutionService = new ReportExecutionService();

        /// <summary>
        /// The report parameter values.
        /// </summary>
        private readonly ReportServiceParameterValue[] rptParameterValues = null;

        /// <summary>
        /// The report service.
        /// </summary>
        private readonly ReportingService2005 rptService = new ReportingService2005();

        /// <summary>
        /// The string history id.
        /// </summary>
        private readonly string strHistoryId = null;

        /// <summary>
        /// The connection string.
        /// </summary>
        private readonly string connectionString = ConfigurationManager.ConnectionStrings["ClientConnectionString"].ConnectionString;

        /// <summary>
        /// The encoding.
        /// </summary>
        private string encoding = string.Empty;

        /// <summary>
        /// The generate report as bytes.
        /// </summary>
        private byte[] generateReportAsBytes;

        /// <summary>
        /// The report execution service parameter values.
        /// </summary>
        private ReportExecutionParameterValue[] rptExecutionServiceParameterValues;

        /// <summary>
        /// The report parameters.
        /// </summary>
        private ReportParameter[] rptParameters;

        /// <summary>
        /// The stream i ds.
        /// </summary>
        private string[] streamIDs;

        /// <summary>
        /// The report name.
        /// </summary>
        private string reportName;

        /// <summary>
        /// The warnings.
        /// </summary>
        private Warning[] warnings;

        /// <summary>
        /// Gets the server name.
        /// </summary>
        public string ServerName { get; private set; }

        /// <summary>
        /// Gets the database name.
        /// </summary>
        public string DatabaseName { get; private set; }

        /// <summary>
        /// Gets the user name.
        /// </summary>
        public string UserName { get; private set; }

        /// <summary>
        /// Gets the password.
        /// </summary>
        public string Password { get; private set; }

        /// <summary>
        /// Gets the report execution info object.
        /// </summary>
        public ExecutionInfo RptExecutionInfoObj { get; private set; }

        /// <summary>
        /// The get database name.
        /// </summary>
        /// <param name="connString">
        /// The conn string.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetDatabaseName(string connString)
        {
            dynamic localConnString = connString.ToLower();

            // if this is a Jet database, find the index of the "data source" setting
            int startIndex = localConnString.IndexOf("initial catalog=");
            if (startIndex > -1)
            {
                startIndex += 16;
            }

            // if the "database", "data source" or "initial catalog" values are not 
            // found, return an empty string
            if (startIndex == -1)
            {
                return string.Empty;
            }

            // find where the database name/path ends
            int endIndex = localConnString.IndexOf(";", startIndex);
            if (endIndex == -1)
            {
                endIndex = localConnString.Length;
            }

            // return the substring with the database name/path
            return connString.Substring(startIndex, endIndex - startIndex).Trim();
        }

        /// <summary>
        /// The get password.
        /// </summary>
        /// <param name="connString">
        /// The conn string.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetPassword(string connString)
        {
            dynamic localConnString = connString.ToLower();

            // if this is a Jet database, find the index of the "data source" setting
            int startIndex = localConnString.IndexOf("password=");
            if (startIndex > -1)
            {
                startIndex += 9;
            }

            // if the "uid" values are not 
            // found, return an empty string
            if (startIndex == -1)
            {
                return string.Empty;
            }

            // find where the database name/path ends
            int endIndex = localConnString.IndexOf(";", startIndex);
            if (endIndex == -1)
            {
                endIndex = localConnString.Length;
            }

            // return the substring with the database name/path
            return connString.Substring(startIndex, endIndex - startIndex).Trim();
        }

        /// <summary>
        /// The get server name.
        /// </summary>
        /// <param name="connString">
        /// The conn string.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetServerName(string connString)
        {
            dynamic localConnString = connString.ToLower();

            // if this is a Jet database, find the index of the "data source" setting
            int startIndex = localConnString.IndexOf("data source=");
            if (startIndex > -1)
            {
                startIndex += 12;
            }

            // if the "database", "data source" or "initial catalog" values are not 
            // found, return an empty string
            if (startIndex == -1)
            {
                return string.Empty;
            }

            // find where the database name/path ends
            int endIndex = localConnString.IndexOf(";", startIndex);
            if (endIndex == -1)
            {
                endIndex = localConnString.Length;
            }

            // return the substring with the database name/path
            return connString.Substring(startIndex, endIndex - startIndex).Trim();
        }

        /// <summary>
        /// The get user name.
        /// </summary>
        /// <param name="connString">
        /// The conn string.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        public string GetUserName(string connString)
        {
            dynamic localConnString = connString.ToLower();

            // if this is a Jet database, find the index of the "data source" setting
            int startIndex = localConnString.IndexOf("user id=");
            if (startIndex > -1)
            {
                startIndex += 8;
            }

            // if the "uid" values are not 
            // found, return an empty string
            if (startIndex == -1)
            {
                return string.Empty;
            }

            // find where the database name/path ends
            int endIndex = localConnString.IndexOf(";", startIndex);
            if (endIndex == -1)
            {
                endIndex = localConnString.Length;
            }

            // return the substring with the database name/path
            return connString.Substring(startIndex, endIndex - startIndex).Trim();
        }

        // public Byte[] RenderReport(string _format, string strReportPath, string strProgramId, string strReqId, string strTermId, string strclassstartdate, string strclassenddate, string strstudentid, string strClassSectionId)

        /// <summary>
        /// The render report.
        /// </summary>
        /// <param name="format">
        /// The format.
        /// </param>
        /// <param name="myScheduleReportSelectionValues">
        /// The my schedule report selection values.
        /// </param>
        /// <returns>
        /// The <see cref="byte "/>.
        /// </returns>
        public byte[] RenderReport(string format, MyScheduleReportSelectionValues myScheduleReportSelectionValues)
        {
            // Initialize the report services and report execution services
            this.InitializeWebServices();

            // Set the path of report
            this.reportName = myScheduleReportSelectionValues.ReportPath + "StudentSchedule/MyScheduleMain";

            // "/Advantage Reports/" + getReportParametersObj.Environment + "/Security/ManageSecurity" 'getReportParametersObj.ReportPath   '"/Advantage Reports/StudentAccounts/IPEDS/IPEDSMissingDataReport"

            // Use the Report Services: GetReportParameters method to get a list of parameters for a specified report
            // This is the only location where the reportingservices2005 is used
            this.rptParameters = this.rptService.GetReportParameters(this.reportName, this.strHistoryId, BoolForRendering, this.rptParameterValues,  this.credentials);

            // Need to load the specific report before passing the parameter values
             this.RptExecutionInfoObj  = new ExecutionInfo();
            this.RptExecutionInfoObj = this.rptExecutionService.LoadReport(this.reportName, this.strHistoryId);

            // Prepare report parameter.    
            this.rptExecutionServiceParameterValues = this.BuildReportParameters(this.rptParameters, myScheduleReportSelectionValues);
           
            this.rptExecutionService.Timeout = Timeout.Infinite;

            // Set Report Parameters
            this.rptExecutionService.SetExecutionParameters(this.rptExecutionServiceParameterValues, "en-us");

            // Call ConfigureExportFormat to get the file extensions
            ConfigureExportFormat(ref format);

            // Render the report
            this.generateReportAsBytes = this.rptExecutionService.Render(
                format, 
                this.deviceInfo, 
                ref strExtension, 
                ref this.encoding, 
                ref strMimeType, 
                ref this.warnings, 
                ref this.streamIDs);

            return this.generateReportAsBytes;
        }

        /// <summary>
        /// The configure export format.
        /// </summary>
        /// <param name="strType">
        /// The string type.
        /// </param>
        private static void ConfigureExportFormat(ref string strType)
        {
            switch (strType.ToUpper())
            {
                case "PDF":
                    strExtension = "pdf";
                    strMimeType = "application/pdf";
                    break; 

                case "EXCEL":
                case "XLS":
                    strExtension = "xls";
                    strMimeType = "application/vnd.excel";
                    break; 

                case "WORD":
                    strExtension = "doc";
                    strMimeType = "application/vnd.ms-word";
                    break;
                case "CSV":
                    strExtension = "csv";
                    strMimeType = "text/csv";
                    break;
                default:
                    throw new Exception("Unrecognized type: " + strType + ". Type must be PDF, Excel or Image, HTML.");
            }
        }

        // ReSharper disable once InconsistentNaming

        /// <summary>
        /// The build report parameters.
        /// </summary>
        /// <param name="rptServicesParameters">
        /// The report services parameters.
        /// </param>
        /// <param name="myScheduleReportSelectionValues">
        /// The my schedule report selection values.
        /// </param>
        /// <returns>
        /// The <see cref="FAME.Advantage.Reporting.ReportExecution2005.ParameterValue"/>.
        /// </returns>
        private ReportExecutionParameterValue[] BuildReportParameters(ReportParameter[] rptServicesParameters, MyScheduleReportSelectionValues myScheduleReportSelectionValues)
        {
             // !!!!!!!!!!!!!!! Warning - SSRS Report Parameters are Case Sensitive !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            var rptExecutionParamValues = new ReportExecutionParameterValue[rptServicesParameters.Length];
            
            if (rptServicesParameters.Length > 0)
            {
                // Service Name       0
                int indexParam = 0;
                rptExecutionParamValues[indexParam] = new ReportExecutionParameterValue
                {
                    Label = "servername",
                    Name = "servername",
                    
                    // Value = string.IsNullOrEmpty(myScheduleReportSelectionValues.ServerName) ? null : this.ServerName           // this.GetServerName(this.connectionString)
                    Value = this.ServerName           // this.GetServerName(this.connectionString)
                };
                
                // Database Name   1
                indexParam += 1;
                rptExecutionParamValues[indexParam] = new ReportExecutionParameterValue
                {
                    Label = "databasename",
                    Name = "databasename",
                    
                    // Value = string.IsNullOrEmpty(myScheduleReportSelectionValues.DatabaseName) ? null : this.DatabaseName       // this.GetDatabaseName(this.connectionString)
                    Value = this.DatabaseName       // this.GetDatabaseName(this.connectionString)
                };
                
                // User id --  uid    2
                indexParam += 1;
                rptExecutionParamValues[indexParam] = new ReportExecutionParameterValue
                {
                    Label = "uid",
                    Name = "uid",
                    
                    // Value = string.IsNullOrEmpty(myScheduleReportSelectionValues.UserName) ? null : this.UserName               // this.GetUserName(this.connectionString)
                    Value = this.UserName               // this.GetUserName(this.connectionString)
                };
                
                // password    3
                indexParam += 1;
                rptExecutionParamValues[indexParam] = new ReportExecutionParameterValue
                {
                    Label = "password",
                    Name = "password",
                    
                    // Value = string.IsNullOrEmpty(myScheduleReportSelectionValues.Password) ? null : this.Password               // this.GetPassword(this.connectionString)
                    Value = this.Password               // this.GetPassword(this.connectionString)
                };
                
                // StuEnrollId   4
                indexParam += 1;
                rptExecutionParamValues[indexParam] = new ReportExecutionParameterValue
                {
                    Label = "StuEnrollId",
                    Name = "StuEnrollId",
                    Value = string.IsNullOrEmpty(myScheduleReportSelectionValues.StuEnrollId.ToString()) ? null : myScheduleReportSelectionValues.StuEnrollId.ToString()
                };
                
                // InstructorId    5
                indexParam += 1;
                rptExecutionParamValues[indexParam] = new ReportExecutionParameterValue
                {
                    Label = "InstructorId",
                    Name = "InstructorId",
                    Value = string.IsNullOrEmpty(myScheduleReportSelectionValues.InstructorId.ToString()) ? null : myScheduleReportSelectionValues.InstructorId.ToString()
                };

                // ReqId           6
                indexParam += 1;
                rptExecutionParamValues[indexParam] = new ReportExecutionParameterValue
                {
                    Label = "ReqId",
                    Name = "ReqId",
                    Value = string.IsNullOrEmpty(myScheduleReportSelectionValues.ReqId.ToString()) ? null : myScheduleReportSelectionValues.ReqId.ToString()
                };

                // TermId        7
                indexParam += 1;                                                        
                rptExecutionParamValues[indexParam] = new ReportExecutionParameterValue
                {
                    Label = "TermId",
                    Name = "TermId",
                    Value = string.IsNullOrEmpty(myScheduleReportSelectionValues.TermId.ToString()) ? null : myScheduleReportSelectionValues.TermId.ToString()
                };

                // StartDate      8
                indexParam += 1;
                rptExecutionParamValues[indexParam] = new ReportExecutionParameterValue
                {
                    Label = "Start Date",
                    Name = "StartDate",
                    Value = string.IsNullOrEmpty(myScheduleReportSelectionValues.StartDate.ToString()) ? null : myScheduleReportSelectionValues.StartDate.ToString()
                };

                // EndDate         9
                indexParam += 1;
                rptExecutionParamValues[indexParam] = new ReportExecutionParameterValue
                {
                    Label = "End Date",
                    Name = "EndDate",
                    Value = string.IsNullOrEmpty(myScheduleReportSelectionValues.EndDate.ToString()) ? null : myScheduleReportSelectionValues.EndDate.ToString()
                };
            }

            return rptExecutionParamValues;
        }

        /// <summary>
        /// The initialize web services.
        /// </summary>
        private void InitializeWebServices()
        {
            this.ServerName = this.GetServerName(this.connectionString);
            this.DatabaseName = this.GetDatabaseName(this.connectionString);
            this.UserName = this.GetUserName(this.connectionString);
            this.Password = this.GetPassword(this.connectionString);

            // Set the credentials and url for the report services
            // The report service object is needed to get the parameter details associated with a specific report
            this.rptService.Credentials = CredentialCache.DefaultCredentials;
            this.rptService.Url = ConfigurationManager.AppSettings["ReportServices"];

            // Set the credentials and url for the report execution services
            this.rptExecutionService.Credentials = CredentialCache.DefaultCredentials;
            this.rptExecutionService.Url = ConfigurationManager.AppSettings["ReportExecutionServices"];
        }
    }
}
