﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="View.ascx.cs" company="FameInc.com">
//  FameInc.com 
// </copyright>
// <summary>
//   Defines the View type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace Christoc.Modules.MySchedule
{
    using System;
    using System.Configuration;
    using System.Web.UI;
    using System.Web.UI.WebControls;

    using Christoc.Modules.MySchedule.Data;

    using DotNetNuke.Common;
    using DotNetNuke.Entities.Modules;
    using DotNetNuke.Entities.Modules.Actions;
    using DotNetNuke.Security;
    using DotNetNuke.Services.Exceptions;
    using DotNetNuke.Services.Localization;

    using PortalData;

    using Telerik.Web.UI;
    using Telerik.Web.UI.Calendar;

    /// <summary>
    /// The view.
    /// </summary>
    public partial class View : MyScheduleModuleBase, IActionable
    {
        /// <summary>
        /// The schedule data.
        /// </summary>
        private readonly ScheduleData schData = new ScheduleData();

        /// <summary>
        /// The student number.
        /// </summary>
        private readonly string studentNumber = DotNetNuke.Entities.Users.UserController.GetCurrentUserInfo().Username;

        // required method for DotNetNuke

        /// <summary>
        /// Gets the module actions.
        /// </summary>
        public ModuleActionCollection ModuleActions
        {
            get
            {
                var actions = new ModuleActionCollection
                                  {
                                      {
                                          this.GetNextActionID(), 
                                          Localization.GetString(
                                              "EditModule", 
                                              this.LocalResourceFile), 
                                          string.Empty, string.Empty, 
                                          string.Empty, this.EditUrl(), false, 
                                          SecurityAccessLevel.Edit, true, false
                                      }
                                  };
                return actions;
            }
        }

        /// <summary>
        /// The button clear_ on click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void ButtonClear_OnClick(object sender, EventArgs e)
        {
            this.ddlEnrollment.SelectedIndex = 0;
            this.ddlCourses.SelectedIndex = 0;
            this.ddlInstructor.SelectedIndex = 0;
            this.ddlTerms.SelectedIndex = 0;
            this.rdStartDate.SelectedDate = null;
            this.rdEndDate.SelectedDate = null;
            this.valDates.ErrorMessage = string.Empty;

            this.BindMasterData(true);
        }

        /// <summary>
        /// The button export to pdf click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void ButtonExportToPdfClick(object sender, EventArgs e)
        {
            byte[] getReportAsBytes = null;

            // Do nothing if does not exists enrollments.
            if (this.ddlEnrollment.Items.Count == 0)
            {
                return;
            }

            string reportPath = ConfigurationManager.AppSettings["Reports.ReportsFolder"];
            Guid stuEnrollId = new Guid(this.ddlEnrollment.SelectedValue);
            Guid? instructorId = null;
            Guid? reqId = null;
            Guid? termId = null;
            DateTime? startDate = null;
            DateTime? endDate = null;
            if (this.ddlInstructor.SelectedIndex > 0)
            {
                instructorId = new Guid(this.ddlInstructor.SelectedValue);
            }

            if (this.ddlCourses.SelectedIndex > 0)
            {
                reqId = new Guid(this.ddlCourses.SelectedValue);
            }

            if (this.ddlTerms.SelectedIndex > 0)
            {
                termId = new Guid(this.ddlTerms.SelectedValue);
            }

            //if (this.rdStartDate.SelectedDate != null && this.rdEndDate.SelectedDate != null)
            //{
            //    startDate = this.rdStartDate.SelectedDate;
            //    endDate = this.rdEndDate.SelectedDate;
            //}

            startDate = this.rdStartDate.SelectedDate;
            endDate = this.rdEndDate.SelectedDate;
           
            MyScheduleReportSelectionValues myScheduleReportSelectionValues =
                new MyScheduleReportSelectionValues(
                    reportPath, 
                    stuEnrollId, 
                    instructorId, 
                    reqId, 
                    termId, 
                    startDate, 
                    endDate);
            try
            {
                getReportAsBytes = (new MyScheduleReportGenerator()).RenderReport(
                    "pdf", 
                    myScheduleReportSelectionValues);
            }
            catch (Exception messageException)
            {
                Exceptions.ProcessModuleLoadException(
                    messageException.InnerException.ToString(), 
                    this, 
                    messageException);
            }

            this.ExportReport("pdf", getReportAsBytes);
        }

        /// <summary>
        /// The button search_ on click.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void ButtonSearch_OnClick(object sender, EventArgs e)
        {
            this.BindMasterData(true);
        }

        /// <summary>
        /// The date_ validate.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="args">
        /// The args.
        /// </param>
        protected void DateValidate(object source, ServerValidateEventArgs args)
        {
            if (this.rdStartDate.SelectedDate != null && this.rdEndDate.SelectedDate != null)
            {
                DateTime startDate = this.rdStartDate.SelectedDate.Value;
                DateTime endDate = this.rdEndDate.SelectedDate.Value;
                if (DateTime.Compare(startDate, endDate) > 0)
                {
                    this.valDates.ErrorMessage = @"Start Date must be before End Date";
                    args.IsValid = false;
                }
            }
        }

        /// <summary>
        /// The drop down list courses_ on selected index changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void DdlCourses_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindMasterData(true);
        }

        /// <summary>
        /// The drop down list enrollment_ on selected index changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void DdlEnrollment_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindTerms();
            this.BindInstructors();
            this.BindCourses();
            this.BindMasterData(true);
        }

        /// <summary>
        /// The drop down list instructor_ on selected index changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void DdlInstructor_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindCourses();
            this.BindMasterData(true);
        }

        /// <summary>
        /// The drop down list terms_ on selected index changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void DdlTerms_OnSelectedIndexChanged(object sender, EventArgs e)
        {
            this.BindInstructors();
            this.BindCourses();
            this.BindMasterData(true);
        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.BindEnrollments();
                if (this.ddlEnrollment.Items.Count > 0)
                {
                    this.BindTerms();
                    this.BindInstructors();
                    this.BindCourses();
                }
            }
        }

        /// <summary>
        /// The rd schedule_ detail table data bind.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RdScheduleDetailTableDataBind(object source, GridDetailTableDataBindEventArgs e)
        {
            this.BindDetailData(e);
        }

        /// <summary>
        /// The rd schedule need data source.
        /// </summary>
        /// <param name="source">
        /// The source.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void RdScheduleNeedDataSource(object source, GridNeedDataSourceEventArgs e)
        {
            if (!e.IsFromDetailTable && this.valDates.IsValid)
            {
                this.BindMasterData(false);
            }
        }

        /// <summary>
        /// The rd schedule on item command.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected virtual void RdScheduleOnItemCommand(object sender, GridCommandEventArgs e)
        {
            if (e.CommandName != null && e.CommandName == RadGrid.ExportToExcelCommandName)
            {
                this.rdSchedule.ExportSettings.ExportOnlyData = false;
                this.rdSchedule.ExportSettings.FileName = "MySchedule.xls";
                this.rdSchedule.ExportSettings.HideStructureColumns = true;
                this.rdSchedule.ExportSettings.OpenInNewWindow = true;
                this.rdSchedule.ExportSettings.UseItemStyles = true;
                this.rdSchedule.MasterTableView.CommandItemDisplay = GridCommandItemDisplay.None;

                this.rdSchedule.Skin = "Metro";
                this.rdSchedule.GridLines = GridLines.Horizontal;
                this.rdSchedule.MasterTableView.HierarchyDefaultExpanded = true;

                this.rdSchedule.Rebind();
            }
        }

        /// <summary>
        /// Event fired during initial rendering of radGrid.
        /// Set the widths of the columns
        /// </summary>
        /// <param name="sender">
        /// sender
        /// </param>
        /// <param name="e">
        /// e
        /// </param>
        protected void RdSchedulePreRender(object sender, EventArgs e)
        {
            GridColumn gridCol = this.rdSchedule.MasterTableView.GetColumn("Section");
            gridCol.HeaderStyle.Width = Unit.Pixel(80);

            GridColumn gridCol2 = this.rdSchedule.MasterTableView.GetColumn("Course");
            gridCol2.HeaderStyle.Width = Unit.Pixel(240);

            GridColumn gridCol3 = this.rdSchedule.MasterTableView.GetColumn("Code");
            gridCol3.HeaderStyle.Width = Unit.Pixel(70);

            GridColumn gridCol4 = this.rdSchedule.MasterTableView.GetColumn("Term");
            gridCol4.HeaderStyle.Width = Unit.Pixel(300);

            GridColumn gridCol5 = this.rdSchedule.MasterTableView.GetColumn("Duration");
            gridCol5.HeaderStyle.Width = Unit.Pixel(170);

            GridColumn gridCol6 = this.rdSchedule.MasterTableView.GetColumn("Instructor");
            gridCol6.HeaderStyle.Width = Unit.Pixel(150);
        }

        /// <summary>
        /// The selection changed.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void SelectionChanged(object sender, SelectedDateChangedEventArgs e)
        {
            this.BindMasterData(true);
        }

        /// <summary>
        /// The bind courses.
        /// </summary>
        private void BindCourses()
        {
            Guid? termId = null;
            if (this.ddlTerms.SelectedIndex > 0)
            {
                termId = new Guid(this.ddlTerms.SelectedValue);
            }

            Guid? instructorId = null;
            if (this.ddlInstructor.SelectedIndex > 0)
            {
                instructorId = new Guid(this.ddlInstructor.SelectedValue);
            }

            this.ddlCourses.DataTextField = "Description";
            this.ddlCourses.DataValueField = "ReqId";
            this.ddlCourses.DataSource = this.schData.GetCourses(
                new Guid(this.ddlEnrollment.SelectedValue), 
                termId, 
                instructorId);
            this.ddlCourses.DataBind();
            this.ddlCourses.Items.Insert(0, new ListItem("All Courses", "0"));
        }

        /// <summary>
        /// The bind detail data.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BindDetailData(GridDetailTableDataBindEventArgs e)
        {
            try
            {
                var dataItem = e.DetailTableView.ParentItem;
                if (e.DetailTableView.Name.ToUpper() == "SCHEDULEDETAILS")
                {
                    var sectionId = new Guid(dataItem.GetDataKeyValue("SectionId").ToString());
                    var stuEnrollId = new Guid(this.ddlEnrollment.SelectedValue);
                    e.DetailTableView.DataSource = this.schData.GetScheduleDetail(sectionId, stuEnrollId);
                }
            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException(
                    "An error occured while attempting to retrieve the details for this class.", 
                    this, 
                    ex);
            }
        }

        /// <summary>
        /// The bind enrollments.
        /// </summary>
        private void BindEnrollments()
        {
            this.ddlEnrollment.DataTextField = "Enrollment";
            this.ddlEnrollment.DataValueField = "EnrollmentId";
            var enrollment = this.schData.GetEnrollments(this.studentNumber);
            if (enrollment.Count == 0)
            {
                this.ScheduleWarningMessage.InnerText = Constant.X_WARNING_USER_HAS_NO_RECORDS;
                this.btnSearch.Enabled = false;
                this.btnClear.Enabled = false;
                this.btnPrint.Enabled = false;
            }
            else
            {
                this.btnSearch.Enabled = true;
                this.btnClear.Enabled = true;
                this.btnPrint.Enabled = true;
                this.ScheduleWarningMessage.InnerText = string.Empty;
                this.ddlEnrollment.DataSource = enrollment;
                this.ddlEnrollment.DataBind();
            }
        }

        /// <summary>
        /// The bind instructors.
        /// </summary>
        private void BindInstructors()
        {
            Guid? termId = null;
            if (this.ddlTerms.SelectedIndex > 0)
            {
                termId = new Guid(this.ddlTerms.SelectedValue);
            }

            this.ddlInstructor.DataTextField = "Description";
            this.ddlInstructor.DataValueField = "InstructorId";
            this.ddlInstructor.DataSource = this.schData.GetInstructors(
                new Guid(this.ddlEnrollment.SelectedValue), 
                termId);
            this.ddlInstructor.DataBind();
            this.ddlInstructor.Items.Insert(0, new ListItem("All Instructors", "0"));
        }

        /// <summary>
        /// The bind master data.
        /// </summary>
        /// <param name="isDataBind">
        /// The is data bind.
        /// </param>
        private void BindMasterData(bool isDataBind)
        {
            try
            {
                // Do nothing if does not exists enrollments.
                if (this.ddlEnrollment.Items.Count == 0)
                {
                    return;
                }

                Guid? instructorId = null;
                Guid? reqId = null;
                Guid? termId = null;
                DateTime? startDate = null;
                DateTime? endDate = null;

                if (this.ddlInstructor.SelectedIndex > 0)
                {
                    instructorId = new Guid(this.ddlInstructor.SelectedValue);
                }

                if (this.ddlCourses.SelectedIndex > 0)
                {
                    reqId = new Guid(this.ddlCourses.SelectedValue);
                }

                if (this.ddlTerms.SelectedIndex > 0)
                {
                    termId = new Guid(this.ddlTerms.SelectedValue);
                }

                //if (this.rdStartDate.SelectedDate != null && this.rdEndDate.SelectedDate != null)
                //{
                //    startDate = this.rdStartDate.SelectedDate;
                //    endDate = this.rdEndDate.SelectedDate;
                //}

                startDate = this.rdStartDate.SelectedDate;
                endDate = this.rdEndDate.SelectedDate;

                this.rdSchedule.DataSource = this.schData.GetStudentSchedule(
                    new Guid(this.ddlEnrollment.SelectedValue), 
                    instructorId, 
                    reqId, 
                    termId,
                    startDate,
                    endDate);

                if (isDataBind)
                {
                    this.rdSchedule.DataBind();
                }
            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException(
                    "An error occured while attempting to retrieve the list of classes.", 
                    this, 
                    ex);
            }
        }

        /// <summary>
        /// The bind terms.
        /// </summary>
        private void BindTerms()
        {
            this.ddlTerms.DataTextField = "Description";
            this.ddlTerms.DataValueField = "TermId";
            this.ddlTerms.DataSource = this.schData.GetTerms(new Guid(this.ddlEnrollment.SelectedValue));
            this.ddlTerms.DataBind();
            this.ddlTerms.Items.Insert(0, new ListItem("All Terms", "0"));
        }

        /// <summary>
        /// The export report.
        /// </summary>
        /// <param name="exportFormat">
        /// The export format.
        /// </param>
        /// <param name="getReportAsBytes">
        /// The get report as bytes.
        /// </param>
        private void ExportReport(string exportFormat, byte[] getReportAsBytes)
        {
            string strExtension;
            string strMimeType;
            switch (exportFormat.ToLower())
            {
                case "pdf":
                    strExtension = "pdf";
                    strMimeType = "application/pdf";
                    break;

                case "excel":
                    strExtension = "xls";
                    strMimeType = "application/vnd.excel";
                    break;

                case "csv":
                    strExtension = "csv";
                    strMimeType = "text/csv";
                    break;
                default:
                    throw new Exception("Unrecognized type. Type must be PDF, Excel or Image, HTML.");
            }

            this.Session["SSRS_FileExtension"] = strExtension;
            this.Session["SSRS_MimeType"] = strMimeType;
            this.Session["SSRS_ReportOutput"] = getReportAsBytes;

            // This code builds the URL and renders the report
            // In the module definition for user control  - DisplayReport.ascx create a key - MyScheduleReport
            // Detailed documentation available at http://www.adefwebserver.com/DotNetNukeHELP/NavigateURL/ website
            string myUrl = Globals.NavigateURL(
                this.PortalSettings.ActiveTab.TabID, 
                "MyScheduleReport", 
                "mid=" + this.ModuleId);
            string script11 = "window.open('";
            string script12 = myUrl;
            string script13 = "','SSRSReport1','resizable=yes,left=200px,top=200px,modal=no');";
            var script = script11 + script12 + script13;
            ScriptManager.RegisterStartupScript(this.Page, typeof(Page), "myscript", script, true);

            // This code works - balaji 6.19.2014
            // Response.Redirect(Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "MyScheduleReport", "mid=" + ModuleId.ToString()));

            // This code also works balaji 6.19.2014
            // string myUrl = Globals.NavigateURL(PortalSettings.ActiveTab.TabID, "MyScheduleReport", "mid=" + ModuleId.ToString());
            // string script1 = string.Format("window.open('http://localhost/advantageportal/My-Info/My-Schedule/ctl/MyScheduleReport/mid/2494','SSRSReport1','resizable=yes,left=200px,top=200px,modal=no');", DateTime.Now.ToLongTimeString());
            // ScriptManager.RegisterStartupScript(Page, typeof(Page), "myscript", script1, true);
        }
    }
}

// 201109BARROD201