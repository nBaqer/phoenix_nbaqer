﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="MyScheduleReportSelectionValues.cs" company="FameInc.com">
//  FameInc.com
// </copyright>
// <summary>
//   The my schedule report selection values.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

 namespace Christoc.Modules.MySchedule
{
    using System;

     /// <summary>
    /// The my schedule report selection values.
    /// </summary>
    public class MyScheduleReportSelectionValues
    {
        #region "Constructor"

        /// <summary>
        /// Initializes a new instance of the <see cref="MyScheduleReportSelectionValues"/> class.
        /// </summary>
        /// <param name="reportPath">
        /// The report Path.
        /// </param>
        /// <param name="stuEnrollId">
        /// The stu Enroll Id.
        /// </param>
        /// <param name="instructorId">
        /// The instructor Id.
        /// </param>
        /// <param name="reqId">
        /// The requirement Id.
        /// </param>
        /// <param name="termId">
        /// The term Id.
        /// </param>
        /// <param name="startDate">
        /// The start Date.
        /// </param>
        /// <param name="endDate">
        /// The end Date.
        /// </param>
        public MyScheduleReportSelectionValues(string reportPath, Guid stuEnrollId, Guid? instructorId, Guid? reqId, Guid? termId, DateTime? startDate, DateTime? endDate)
        {
            // General 
            this.ReportPath = reportPath;
            this.Environment = string.Empty;
            this.ServerName = string.Empty;
            this.DatabaseName = string.Empty;
            this.UserName = string.Empty;
            this.Password = string.Empty;

            // main parameter 
            this.StuEnrollId = stuEnrollId;

            // Filter 
            this.InstructorId = instructorId;
            this.ReqId = reqId;
            this.TermId = termId;
            this.StartDate = startDate;
            this.EndDate = endDate;
        }

        #endregion
        #region "Properties"

        /// <summary>
        /// Gets or sets the end point.
        /// </summary>
        public string ReportPath { get; set; }
 
        /// <summary>
        /// Gets or sets the environment.
        /// </summary>
        public string Environment { get; set; }

        /// <summary>
        /// Gets or sets the server name.
        /// </summary>
        public string ServerName { get; set; }

        /// <summary>
        /// Gets or sets the database name.
        /// </summary>
        public string DatabaseName { get; set; }

        /// <summary>
        /// Gets or sets the user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets the password.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets the stu enroll id.
        /// </summary>
        public Guid StuEnrollId { get; set; }

        /// <summary>
        /// Gets or sets the instructor id.
        /// </summary>
        public Guid? InstructorId { get; set; }

        /// <summary>
        /// Gets or sets the requirement id.
        /// </summary>
        public Guid? ReqId { get; set; }

        /// <summary>
        /// Gets or sets the term id.
        /// </summary>
        public Guid? TermId { get; set; }

        /// <summary>
        /// Gets or sets the start date.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Gets or sets the end date.
        /// </summary>
        public DateTime? EndDate { get; set; }

        #endregion
    }
}
