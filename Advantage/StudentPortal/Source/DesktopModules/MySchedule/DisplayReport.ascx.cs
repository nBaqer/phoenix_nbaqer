﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using DotNetNuke.Entities.Modules;

namespace Christoc.Modules.MySchedule
{
    public partial class DisplayReport : MyScheduleModuleBase
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //Response.Write("This is a new test");
           
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ContentType = (string)Session["SSRS_MimeType"];
            HttpContext.Current.Response.BinaryWrite((byte[])Session["SSRS_ReportOutput"]);
           
        }
    }
}