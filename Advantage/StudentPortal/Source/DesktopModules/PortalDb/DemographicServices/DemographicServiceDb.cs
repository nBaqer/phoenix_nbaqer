﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Configuration;
using PortalData.Demographic;

namespace PortalDb.DemographicServices
{
    public class DemographicServiceDb
    {
         #region Private Class Variables
        
        //linq data context object
        private readonly DemographicDataContext dbContext; 

        #endregion

        public DemographicServiceDb()
        {
            var conn = WebConfigurationManager.ConnectionStrings["ClientConnectionString"].ConnectionString;
            dbContext = new  DemographicDataContext(new SqlConnection(conn));
        }

        #region Public Class Methods

        /// <summary>
        /// get the enrollments for the student using the student number
        /// </summary>
        /// <returns></returns>
        public List<StateOutput> GetActiveStates()
        {
            var data =  (from res in dbContext.syStates
                         join sta in dbContext.syStatuses on res.StatusId equals sta.StatusId
                         where sta.Status == "Active"
                         orderby res.StateDescrip
                         select new StateOutput
                        {
                            StateGuid = res.StateId,
                            StateDescription = res.StateDescrip
                        });
            return data.ToList();
        }

        #endregion

        /// <summary>
        /// Get list of active counties.
        /// </summary>
        /// <returns>Ilist of counties</returns>
        public IList<CountyOutput> GetActiveCounties()
        {
            var data = (from res in dbContext.adCounties
                        join sta in dbContext.syStatuses on res.StatusId equals sta.StatusId
                        where sta.Status == "Active"
                        orderby res.CountyDescrip
                        select new CountyOutput
                        {
                            CountyGuid = res.CountyId,
                            CountyDescription = res.CountyDescrip
                        });

            return data.ToList();
        }

        /// <summary>
        /// REturn a list of countries of Lead.
        /// </summary>
        /// <returns></returns>
        public IList<CountryOutput> GetActiveCountries()
        {
            var data = (from res in dbContext.adCountries
                        join sta in dbContext.syStatuses on res.StatusId equals sta.StatusId
                        where sta.Status == "Active"
                        orderby res.CountryDescrip
                        select new CountryOutput
                        {
                            CountryGuid = res.CountryId,
                            CountryDescription = res.CountryDescrip
                        });

            return data.ToList();
        }
    }
}
