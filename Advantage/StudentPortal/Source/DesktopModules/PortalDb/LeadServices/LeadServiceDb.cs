﻿using System;
using System.Data.SqlClient;
using System.Web.Configuration;
using PortalData.Lead;

namespace PortalDb.LeadServices
{
    public class LeadServiceDb
    {
        #region Private Class Variables

        //linq data context object
        private readonly LeadDataContext dbContext;

        #endregion

        public LeadServiceDb()
        {
            var conn = WebConfigurationManager.ConnectionStrings["ClientConnectionString"].ConnectionString;
            dbContext = new LeadDataContext(new SqlConnection(conn));
        }

        /// <summary>
        /// Insert a lead in table adLead, also change the status of the lead to the new status.
        /// </summary>
        /// <returns>A string with the result of the operation.</returns>
        public string InsertLeadInPortal(LeadInfo info)
        {
            string leadEntryMsg = String.Empty;

            // Convert to Guid the to ID parameters.
            var campusGuid = (string.IsNullOrEmpty(info.CampusId)) ? (Guid?)null : Guid.Parse(info.CampusId);
            var progGuid = (string.IsNullOrEmpty(info.ProgId)) ? (Guid?)null : Guid.Parse(info.ProgId);

            dbContext.usp_AdvPortalInsertLead(
                campusGuid,
                progGuid,
                info.SourceTypeId,
                info.PrevEduLvlId,
                info.FirstName,
                info.MiddleName,
                info.LastName,
                info.BirthDate,
                info.GenderId,
                info.PhoneNumber,
                info.ForeignPhone,
                info.PhoneNumber2,
                info.ForeignPhone2,
                info.Email,
                info.AlternativeEmail,
                info.Address1,
                info.Address2,
                info.City,
                info.StateId,
                info.OtherState,
                info.Zip,
                info.ForeignZip,
                info.CountyId,
                info.CountryId,
                info.RaceId,
                info.NationalityId,
                info.Citizen,
                info.MaritalStatus,
                info.FamilyIncome,
                info.Children,
                info.DrivLicStateID,
                info.DrivLicNumber,
                info.Comments,
                ref leadEntryMsg
                );

            return leadEntryMsg;
        }

    }
}
