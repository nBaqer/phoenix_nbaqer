﻿using System.Globalization;
using Christoc.Modules.CourseListing.Data;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Actions;
using DotNetNuke.Security;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.Services.Localization;
using System;
using System.Web.UI.WebControls;
using Telerik.Web.UI;

namespace Christoc.Modules.CourseListing
{
    public partial class View : CourseListingModuleBase, IActionable
    {
        #region Class Events

        private CourseListingData _courseListData = null;

        /// <summary>
        /// Page load event
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            _courseListData = new CourseListingData();

            if (!this.IsPostBack)
                BindDropdowns();
        }

        /// <summary>
        /// Event fired whenever the radGrid detects that it needs data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rgCourseList_NeedDataSource(object sender, Telerik.Web.UI.GridNeedDataSourceEventArgs e)
        {
            BindData(false);
        }

        /// <summary>
        /// Event fired during initial rendering of radGrid.
        /// Set the widths of the columns
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rgCourseList_PreRender(object sender, EventArgs e)
        {
            GridColumn gridCol = rgCourseList.MasterTableView.GetColumn("Course");
            gridCol.HeaderStyle.Width = Unit.Pixel(375);

            GridColumn gridCol2 = rgCourseList.MasterTableView.GetColumn("Code");
            gridCol2.HeaderStyle.Width = Unit.Pixel(70);

            GridColumn gridCol3 = rgCourseList.MasterTableView.GetColumn("Credits");
            gridCol3.HeaderStyle.Width = Unit.Pixel(70);

            GridColumn gridCol4 = rgCourseList.MasterTableView.GetColumn("Program");
            gridCol4.HeaderStyle.Width = Unit.Pixel(375);
            
            
        }

        /// <summary>
        /// Event fired when user clicks to search.
        /// Get the data for radGrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_OnClick(object sender, EventArgs e)
        {
            BindData(false);
        }

        /// <summary>
        /// Event fired when user clicks to clear the filters
        /// Reset controls and re-bind the grid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClear_OnClick(object sender, EventArgs e)
        {
            lstCampus.SelectedIndex = 0;
            lstPrograms.SelectedIndex = 0;
            txtCourseDescription.Text = "";
            BindData(true);
        }

        #endregion

        #region Class Methods

        /// <summary>
        /// DNN required method 
        /// </summary>
        public ModuleActionCollection ModuleActions
        {
            get
            {
                var actions = new ModuleActionCollection
                    {
                        {
                            GetNextActionID(), Localization.GetString("EditModule", LocalResourceFile), "", "", "",
                            EditUrl(), false, SecurityAccessLevel.Edit, true, false
                        }
                    };
                return actions;
            }
        }


        /// <summary>
        /// Get the data for courses and program 
        /// and bind the dropdown lists
        /// </summary>
        private void BindDropdowns()
        {
            try
            {
                lstPrograms.DataTextField = "Description";
                lstPrograms.DataValueField = "ProgramGroupId";
                lstPrograms.DataSource = _courseListData.GetProgramList();
                lstPrograms.DataBind();

                lstCampus.DataTextField = "Campus";
                lstCampus.DataValueField = "CampusId";
                lstCampus.DataSource = _courseListData.GetCampusList();
                lstCampus.DataBind();

                lstPrograms.Items.Insert(0, new ListItem("All Programs", ""));
                lstCampus.Items.Insert(0, new ListItem("All Campuses", ""));

            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException("An error occured during the initial load of this page.",this, ex);
            }
        }

        
        /// <summary>
        /// Get Course data and bind radGrid
        /// </summary>
        /// <param name="isDataBind"></param>
        private void BindData(bool isDataBind)
        {
            Guid? cmpId = null;
            Guid? prgGrpId = null;
            string cDespription = null;

            if(lstCampus.SelectedIndex > 0)
                cmpId = new Guid(lstCampus.SelectedValue);

            if (lstPrograms.SelectedIndex > 0)
                prgGrpId = new Guid(lstPrograms.SelectedValue);

            cDespription = txtCourseDescription.Text;


            try
            {
                rgCourseList.DataSource = _courseListData.GetCourseListing(cDespription, prgGrpId, cmpId);

                if (isDataBind)
                    rgCourseList.DataBind();
                
            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException("An error occured while attempting to retrieve courses.", this, ex);
            }
        }

        #endregion

    }
}
