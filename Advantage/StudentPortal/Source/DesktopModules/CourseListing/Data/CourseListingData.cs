﻿using Christoc.Modules.CourseListing.Data.OutputData;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Christoc.Modules.CourseListing.Data
{
    public class CourseListingData
    {
        /// <summary>
        /// Get the list of Courses using the parameters passed in.
        /// The linq query will use the parameters to filter the query
        /// is they are not null, otherwise will bring backk all courses
        /// </summary>
        /// <param name="courseDescription"></param>
        /// <param name="progGroupId"></param>
        /// <param name="campusId"></param>
        /// <returns></returns>
        public List<CourseListingOutput> GetCourseListing(string courseDescription, Guid? progGroupId, Guid? campusId )
        {

            //get initial data
            using (var dbContext = new CourseListingLinqClassesDataContext())
            {
                var data = (from r in dbContext.arReqs
                    join cs in dbContext.arClassSections on r.ReqId equals cs.ReqId
                    join c in dbContext.syCampuses on cs.CampusId equals c.CampusId
                    join pVerDef in dbContext.arProgVerDefs on cs.ReqId equals pVerDef.ReqId
                    join pVer in dbContext.arPrgVersions on new {pVerDef.PrgVerId, r.CampGrpId} equals
                        new {pVer.PrgVerId, pVer.CampGrpId}
                    where r.ReqTypeId == 1
                          && r.syStatuse.StatusCode == "A"
                    select new CourseListingOutput
                    {
                        Campus = cs.syCampuse.CampDescrip,
                        Program = pVer.arPrgGrp.PrgGrpDescrip,
                        Code = r.Code,
                        Course = r.Descrip,
                        Credits = r.Credits,
                        ProgGroupId = pVer.PrgGrpId,
                        CampusId = cs.syCampuse.CampusId,
                        CourseCategory = r.arCourseCategory.Descrip
                        
                    }).Union
                    (
                        from r in dbContext.arReqs
                        join cs in dbContext.arClassSections on r.ReqId equals cs.ReqId
                        join c in dbContext.syCampuses on cs.CampusId equals c.CampusId
                        join pVerDef in dbContext.arProgVerDefs on r.ReqId equals pVerDef.ReqId
                        join rGrpDef in dbContext.arReqGrpDefs on r.ReqId equals rGrpDef.ReqId
                        join pVer in dbContext.arPrgVersions on new {pVerDef.PrgVerId, r.CampGrpId} equals
                        new {pVer.PrgVerId, pVer.CampGrpId}
                        where r.ReqId==rGrpDef.ReqId
                        && r.ReqTypeId==2
                        select new CourseListingOutput
                        {
                            Campus = cs.syCampuse.CampDescrip,
                            Program = pVer.arPrgGrp.PrgGrpDescrip,
                            Code = r.Code,
                            Course = r.Descrip,
                            Credits = r.Credits,
                            ProgGroupId = pVer.PrgGrpId,
                            CampusId = cs.syCampuse.CampusId,
                            CourseCategory = r.arCourseCategory.Descrip

                        }
                    );

                //filter results if parameters are not null
                if (!string.IsNullOrEmpty(courseDescription))
                    data = data.Where(n => n.Course.Contains(courseDescription));

                if (progGroupId != null)
                    data = data.Where(n => n.ProgGroupId == progGroupId);

                if (campusId != null)
                    data = data.Where(n => n.CampusId == campusId);

                //return the final data
                return data.Distinct().OrderBy(n => n.Campus).ThenBy(n => n.Program).ThenBy(n => n.Code).ToList();
            }

        }

        /// <summary>
        /// Get the list of active programs
        /// </summary>
        /// <returns></returns>
        public List<ProgramOutput> GetProgramList()
        {
            using (var dbContext = new CourseListingLinqClassesDataContext())
            {
                return (from p in dbContext.arPrgGrps
                    join pv in dbContext.arPrgVersions on p.PrgGrpId equals pv.PrgGrpId
                    where pv.syStatuse.StatusCode == "A"
                    select new ProgramOutput()
                    {
                        ProgramGroupId = p.PrgGrpId,
                        Description = p.PrgGrpDescrip
                    }).Distinct().OrderBy(n => n.Description).ToList();
            }
        }


        /// <summary>
        /// Get the list of active campuses
        /// </summary>
        /// <returns></returns>
        public List<CampusOutput> GetCampusList()
        {
            using (var dbContext = new CourseListingLinqClassesDataContext())
            {
                return (from c in dbContext.syCampuses
                    where c.syStatuse.StatusCode == "A"
                    select new CampusOutput()
                    {
                        CampusId = c.CampusId,
                        Campus = c.CampDescrip
                    }).OrderBy(n => n.Campus).ToList();
            }
        }

    }

}