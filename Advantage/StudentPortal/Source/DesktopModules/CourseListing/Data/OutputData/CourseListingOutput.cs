﻿

using System;

namespace Christoc.Modules.CourseListing.Data
{
    public class CourseListingOutput
    {
        public string Campus     { get; set; }
        public Guid? ProgGroupId { get; set; }
        public string Program { get; set; }
        public string Code { get; set; }
        public string Course { get; set; }
        public string Degree { get; set; }
        public decimal? Credits { get; set; }
        public Guid CampusId { get; set; }
        public string CourseCategory { get; set; }
        public string Duration { get; set; }
        

    }
}