﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.CourseListing.Data
{
    public class ProgramOutput
    {
        public Guid ProgramGroupId { get; set; }
        public string Description { get; set; }
    }
}