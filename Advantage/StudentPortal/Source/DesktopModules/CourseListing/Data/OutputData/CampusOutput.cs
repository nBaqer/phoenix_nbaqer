﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Christoc.Modules.CourseListing.Data.OutputData
{
    public class CampusOutput
    {
        public Guid CampusId { get; set; }
        public string Campus { get; set; }
    }
}