-- ============================================
-- Consolidated Script Portal version 1.0.0
-- ============================================

-- ============================================
-- Insert a new lead 
-- ============================================

IF EXISTS ( SELECT  *
			FROM    sys.objects
			WHERE   object_id = OBJECT_ID(N'[dbo].[usp_AdvPortalInsertLead]')
					AND type IN ( N'P', N'PC' ) ) 
DROP PROCEDURE [dbo].[usp_AdvPortalInsertLead]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_AdvPortalInsertLead]
	@CampusId UNIQUEIDENTIFIER ,
	@ProgId UNIQUEIDENTIFIER ,
	@SourceTypeId VARCHAR(50) = NULL ,
	@PrevEduLvlId VARCHAR(50) = NULL ,
	@FirstName VARCHAR(50) ,
	@MiddleName VARCHAR(50) = NULL ,
	@LastName VARCHAR(50) ,
	@BirthDate DATETIME = NULL ,
	@GenderId VARCHAR(50) = NULL ,
	@PhoneNumber VARCHAR(50) = NULL ,
	@ForeignPhone BIT = 0 ,
	@PhoneNumber2 VARCHAR(50) = NULL ,
	@ForeignPhone2 BIT = 0 ,
	@Email VARCHAR(50) ,
	@AlternativeEmail VARCHAR(50) = NULL ,
	@Address1 VARCHAR(50) = NULL ,
	@Address2 VARCHAR(50) = NULL ,
	@City VARCHAR(50) = NULL ,
	@StateId VARCHAR(50) = NULL ,
	@OtherState VARCHAR(50) = NULL ,
	@Zip VARCHAR(50) = NULL ,
	@ForeignZip BIT = 0 ,
	@CountyId VARCHAR(50) = NULL ,
	@CountryId VARCHAR(50) = NULL ,
	@RaceId VARCHAR(50) = NULL ,
	@NationalityId VARCHAR(50) = NULL ,
	@Citizen VARCHAR(50) = NULL ,
	@MaritalStatus VARCHAR(50) = NULL ,
	@FamilyIncome VARCHAR(50) = NULL ,
	@Children VARCHAR(50) = NULL ,
	@DrivLicStateID VARCHAR(50) = NULL ,
	@DrivLicNumber VARCHAR(50) = NULL ,
	@Comments VARCHAR(250) = NULL ,
	@LeadEntryMsg VARCHAR(150) OUTPUT
AS
	BEGIN
		SET NOCOUNT ON

	--/* Check if lead is duplicate */

		SET @LeadEntryMsg = ''

		IF EXISTS ( SELECT  *
					FROM    dbo.adLeads
					WHERE   FirstName = @FirstName
							AND LastName = @LastName )
			BEGIN
				SET @LeadEntryMsg = 'Your first and last name already exists in our system. Please contact school associate for further information.'
			END
		ELSE
			IF EXISTS ( SELECT  *
						FROM    dbo.adLeads
						WHERE   FirstName = @FirstName
								AND ( HomeEmail = @Email
									  OR WorkEmail = @Email
									) )
				BEGIN
					SET @LeadEntryMsg = 'Your first name and email address already exists in our system. Please contact school associate for futher information.'
				END
			ELSE 
		-- ==========================================================
		-- Use transaction to do this. Two tables must be modified
		-- ==========================================================
		BEGIN TRY
			BEGIN TRANSACTION T1

			-- INSERT new lead into Leads table 
				BEGIN 
					INSERT  INTO dbo.adLeads
							( FirstName ,
							  MiddleName ,
							  LastName ,
							  BirthDate ,
							  Gender ,
							  ModUser ,
							  ModDate ,
							  Phone ,
							  ForeignPhone ,
							  Phone2 ,
							  ForeignPhone2 ,
							  HomeEmail ,
							  WorkEmail ,
							  Address1 ,
							  Address2 ,
							  City ,
							  StateId ,
							  OtherState ,
							  Zip ,
							  ForeignZip ,
							  County ,
							  LeadStatus ,
							  AdmissionsRep ,
							  AssignedDate ,
							  SourceCategoryID ,
							  SourceTypeID ,
							  SourceDate ,
							  ProgramID ,
							  AreaID ,
							  Comments ,
							  CampusId ,
							  Country ,
							  PreviousEducation ,
							  Race ,
							  Nationality ,
							  Citizen ,
							  MaritalStatus ,
							  FamilyIncome ,
							  Children ,
							  DrivLicStateID ,
							  DrivLicNumber
							)
					VALUES  ( @FirstName ,
							  @MiddleName ,
							  @LastName ,
							  @BirthDate ,
							  @GenderId ,
							  'sa' ,
							  GETDATE() ,
							  CASE WHEN ( @PhoneNumber <> '' )
								   THEN @PhoneNumber
								   ELSE NULL
							  END ,
							  @ForeignPhone ,
							  CASE WHEN ( @PhoneNumber2 <> '' )
								   THEN @PhoneNumber2
								   ELSE NULL
							  END ,
							  @ForeignPhone2 ,
							  @Email ,
							  CASE WHEN ( @AlternativeEmail <> '' )
								   THEN @AlternativeEmail
								   ELSE NULL
							  END ,
							  CASE WHEN ( @Address1 <> '' ) THEN @Address1
								   ELSE NULL
							  END ,
							  CASE WHEN ( @Address2 <> '' ) THEN @Address2
								   ELSE NULL
							  END ,
							  CASE WHEN ( @City <> '' ) THEN @City
								   ELSE NULL
							  END ,
							  CASE WHEN ( @StateId = '' ) THEN NULL
								   ELSE @StateId
							  END ,
							  @OtherState ,
							  CASE WHEN ( @Zip <> '' ) THEN @Zip
								   ELSE NULL
							  END ,
							  @ForeignZip ,
							  CASE WHEN ( @CountyId <> '' ) THEN @CountyId
								   ELSE NULL
							  END ,
							  ( SELECT  UPPER(LeadStatusId)
								FROM    dbo.syCampuses
								WHERE   CampusId = @CampusId
							  ) ,
							  ( SELECT  UPPER(AdmRepId)
								FROM    dbo.syCampuses
								WHERE   CampusId = @CampusId
							  ) ,
							  GETDATE() ,
							  CASE WHEN ( @SourceTypeId = '' ) THEN NULL
								   ELSE ( SELECT    SourceCatagoryId
										  FROM      dbo.adSourceType
										  WHERE     SourceTypeId = @SourceTypeId
										)
							  END ,
							  CASE WHEN ( @SourceTypeId = '' ) THEN NULL
								   ELSE @SourceTypeId
							  END ,
							  GETDATE() ,
							  UPPER(@ProgId) ,
							  ( SELECT  PrgGrpId
								FROM    dbo.arPrgGrp
								WHERE   PrgGrpId IN (
										SELECT TOP 1
												PrgGrpId
										FROM    dbo.arPrgVersions
										WHERE   ProgId = @ProgId )
							  ) ,
							  @Comments ,
							  UPPER(@CampusId) ,
							  CASE WHEN ( @CountryId = '' ) THEN NULL
								   ELSE UPPER(@CountryId)
							  END ,
							  CASE WHEN ( @PrevEduLvlId = '' ) THEN NULL
								   ELSE UPPER(@PrevEduLvlId)
							  END ,
							  CASE WHEN ( @RaceId <> '' ) THEN @RaceId
								   ELSE NULL
							  END ,
							  CASE WHEN ( @NationalityId <> '' )
								   THEN @NationalityId
								   ELSE NULL
							  END ,
							  CASE WHEN ( @Citizen <> '' ) THEN @Citizen
								   ELSE NULL
							  END ,
							  CASE WHEN ( @MaritalStatus <> '' )
								   THEN @MaritalStatus
								   ELSE NULL
							  END ,
							  CASE WHEN ( @FamilyIncome <> '' )
								   THEN @FamilyIncome
								   ELSE NULL
							  END ,
							  CASE WHEN ( @Children <> '' ) THEN @Children
								   ELSE NULL
							  END ,
							  CASE WHEN ( @DrivLicStateID <> '' )
								   THEN @DrivLicStateID
								   ELSE NULL
							  END ,
							  CASE WHEN ( @DrivLicNumber <> '' )
								   THEN @DrivLicNumber
								   ELSE NULL
							  END
							)

			-- INSERT new prospect status into LeadStatusesChanges table 

					INSERT  INTO syLeadStatusesChanges
							( StatusChangeId ,
							  LeadId ,
							  OrigStatusId ,
							  NewStatusId ,
							  ModUser ,
							  ModDate
							)
					VALUES  ( NEWID() ,
							  ( SELECT  LEADID
								FROM    adLeads
								WHERE   FirstName = @FirstName
										AND LastName = @LastName
										AND HomeEmail = @Email
										AND ProgramID = UPPER(@ProgId)
							  ) ,
							  NULL ,
							  ( SELECT  UPPER(LeadStatusId)
								FROM    dbo.syCampuses
								WHERE   CampusId = @CampusId
							  ) ,
							  'sa' ,
							  GETDATE()
							)
					SET @LeadEntryMsg = 'Your request for ''' + ( SELECT
															  ProgDescrip
															  FROM
															  arprograms
															  WHERE
															  ProgId = @ProgId
															  )
						+ ''' program is received. You will receive a confirmation email shortly.'
				END

			COMMIT TRANSACTION T1
		END TRY
		BEGIN CATCH
			 IF @@TRANCOUNT > 0
				ROLLBACK TRANSACTION T1
			 SET @LeadEntryMsg = ERROR_MESSAGE();

		END CATCH

		SELECT  @LeadEntryMsg AS LeadEntryMsg 
	END
GO


-- =======================================================================================
-- sp_GetGradeBookResults - Grade Book Portal Page.
-- =======================================================================================


IF EXISTS ( SELECT  *
			FROM    sys.objects
			WHERE   object_id = OBJECT_ID(N'[dbo].[sp_GetGradeBookResults]')
					AND type IN ( N'P', N'PC' ) ) 
DROP PROCEDURE [dbo].[sp_GetGradeBookResults]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetGradeBookResults] 

	@StuEnrollId VARCHAR(38),

	@TermId VARCHAR(38)=NULL,
	
	@ClsSectionId VARCHAR(38)=NULL
AS

BEGIN

SET FMTONLY OFF;

IF OBJECT_ID('tempdb..#tmpGradeBookResults') IS NOT NULL

DROP TABLE #tmpGradeBookResults

CREATE TABLE #tmpGradeBookResults
	(
		[StuEnrollId] VARCHAR (38),

		 [Term] VARCHAR(50),

		 [TermId] VARCHAR(38),

		 [ClassStartDate] DATETIME,

		 [ClassEndDate] DATETIME,

		 [CourseName] VARCHAR(50),

		 [ClsSectionId] VARCHAR(100),

		 [InstructorName] VARCHAR(100),

		 [CourseCode] VARCHAR(12),

		 [Credits] DECIMAL(6,2),

		 [Hours] DECIMAL(8,2),

		 [Grade] CHAR(5)

	)



INSERT INTO #tmpGradeBookResults

SELECT DISTINCT @StuEnrollId,t3.TermDescrip AS Term,t4.TermId,t4.StartDate AS ClassStartDate,t4.EndDate AS ClassEndDate,t2.Descrip AS CourseName, t4.ClsSectionId,Users.FullName AS [InstructorName]

,t2.Code AS CourseCode,t2.Credits, t2.HOURS,(SELECT Grade FROM arGradeSystemDetails WHERE GrdSysDetailId = t1.GrdSysDetailId) AS Grade

FROM   arResults t1

INNER JOIN arClassSections t4 ON t1.TestId = t4.ClsSectionId

INNER JOIN arClassSectionTerms t5 ON t4.ClsSectionId = t5.ClsSectionId 

INNER JOIN arTerm t3 ON t5.TermId = t3.TermId 

INNER JOIN  arReqs t2 ON t4.ReqId = t2.ReqId 

INNER JOIN  arStuEnrollments t6 ON t1.StuEnrollId = t6.StuEnrollId 

INNER JOIN syusers Users ON t4.InstructorId=Users.UserId  

LEFT JOIN dbo.arGrdBkResults GrdResult ON t1.StuEnrollId=GrdResult.StuEnrollId AND t1.TestId=GrdResult.ClsSectionId 

LEFT JOIN dbo.arGradeSystemDetails GrdSysDetail ON t1.GrdSysDetailId=GrdSysDetail.GrdSysDetailId

LEFT JOIN dbo.arGrdBkWgtDetails WgtDetail ON GrdResult.InstrGrdBkWgtDetailId=WgtDetail.InstrGrdBkWgtDetailId

WHERE      

(t1.GrdSysDetailId IS NOT NULL OR (t1.GrdSysDetailId IS NULL AND t1.isClinicsSatisfied=1)  OR (t1.GrdSysDetailId IS NULL  

AND  (t1.ISClinicsSatISfied=0 OR t1.ISClinicsSatISfied IS NULL) AND (SELECT COUNT(*) FROM argrdBkResults WHERE StuEnrollId=t1.StuEnrollId AND ClsSectionId=t1.TestId AND Score IS NOT NULL) >=1))      

AND t1.StuEnrollId = @StuEnrollId



UNION 



SELECT DISTINCT @StuEnrollId,t30.TermDescrip AS Term,t10.TermId,'1/1/1900' AS ClassStartDate,'1/1/1900' AS ClassEndDate,t20.Descrip AS CourseName,NULL,NULL,t20.Code AS CourseCode,       

t20.Credits,T20.Hours,(SELECT Grade FROM arGradeSystemDetails WHERE GrdSysDetailId = t10.GrdSysDetailId) AS Grade

FROM arTransferGrades t10

INNER JOIN arTerm t30 

ON t10.TermId = t30.TermId 

INNER JOIN  arReqs t20

ON t10.ReqId = t20.ReqId

WHERE 

(t10.GrdSysDetailId IS NOT NULL OR (t10.GrdSysDetailId IS NULL AND t10.ISClinicsSatISfied=1) OR (t10.GrdSysDetailId IS NULL AND (t10.ISClinicsSatISfied=0 OR t10.ISClinicsSatISfied IS NULL)   

AND (SELECT COUNT (*) FROM argrdBkResults WHERE StuEnrollId=t10.StuEnrollId AND ClsSectionId IN (SELECT DISTINCT ClsSectionId FROM arClASsSections WHERE ReqId=t10.ReqId AND TermId=t10.TermId) AND ScORe IS NOT NULL) >=1))        

AND t10.StuEnrollId = @StuEnrollId 

 

UNION  

 

SELECT DISTINCT @StuEnrollId,t3.TermDescrip AS Term,t4.TermId,t4.StartDate AS ClASsStartDate,t4.EndDate  AS ClASsEndDate,t2.Descrip AS [CourseName],NULL,NULL,t2.Code AS CourseCode, t2.Credits,T2.Hours,

 (SELECT Grade FROM arGradeSystemDetails WHERE GrdSysDetailId = t1.GrdSysDetailId) AS Grade

FROM  

(SELECT DISTINCT t700.ReqId AS EqReqId,t3.ReqId,t100.StuEnrollId  FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600 ,arCourseEquivalent t700 

WHERE

(t100.StudentId = t500.StudentId)  AND t3.ReqId = t400.ReqId AND t100.PrgVerId = t400.PrgVerId AND t100.PrgVerId = t600.PrgVerId  AND t3.Reqid=t700.EquivReqId AND t100.StuEnrollId = @StuEnrollId

AND t3.Reqid NOT IN (SELECT ReqId FROM arResults a, arClASsSections b WHERE a.TestId=b.ClsSectionId  AND StuEnrollId = @StuEnrollId AND ReqId=t400.ReqId)) S, arResults t1, arReqs t2,  

arTerm t3, arClASsSections t4, arClASsSectionTerms t5, arStuEnrollments t6  WHERE  t1.TestId = t4.ClsSectionId AND t4.ClsSectionId = t5.ClsSectionId     

AND t5.TermId = t3.TermId AND t4.ReqId = t2.ReqId  AND t1.StuEnrollId = t6.StuEnrollId AND  t1.GrdSysDetailId IS NOT NULL AND t1.StuEnrollId = S.StuEnrollId  AND t2.Reqid=S.EqReqId 



ORDER BY Term DESC,ClassStartDate,ClassEndDate 



BEGIN



	IF(@TermId = '00000000-0000-0000-0000-000000000000' AND @ClsSectionId = '00000000-0000-0000-0000-000000000000')



		BEGIN

			SELECT DISTINCT (CourseCode+'-'+CourseName) AS Course,Term,ClassStartDate,ClassEndDate,InstructorName,Grade,Credits,Hours,TermId,ClsSectionId,StuEnrollId FROM #tmpGradeBookResults 

		END



	ELSE IF(@TermId IS NOT NULL AND @ClsSectionId = '00000000-0000-0000-0000-000000000000')



		BEGIN

			SELECT DISTINCT (CourseCode+'-'+CourseName) AS Course,Term,ClassStartDate,ClassEndDate,InstructorName,Grade,Credits,Hours,TermId,ClsSectionId,StuEnrollId FROM #tmpGradeBookResults WHERE [Termid] = @TermId 

		END



	ELSE IF(@TermId ='00000000-0000-0000-0000-000000000000' AND @ClsSectionId IS NOT NULL)



		BEGIN	

			SELECT DISTINCT (CourseCode+'-'+CourseName) AS Course,Term,ClassStartDate,ClassEndDate,InstructorName,Grade,Credits,Hours,TermId,ClsSectionId,StuEnrollId FROM #tmpGradeBookResults WHERE [ClsSectionId]=@ClsSectionId

		END



	ELSE



		BEGIN 

			SELECT DISTINCT (CourseCode+'-'+CourseName) AS Course,Term,ClassStartDate,ClassEndDate,InstructorName,Grade,Credits,Hours,TermId,ClsSectionId,StuEnrollId FROM #tmpGradeBookResults 
			WHERE [TermId]=@TermId AND [ClsSectionId]=@ClsSectionId AND [StuEnrollId]=@StuEnrollId

		END

END
END

GO


-- =============================================================================
-- sp_GetGradeBookComponents - GradeBook portal page.
-- =============================================================================

IF EXISTS ( SELECT  *
			FROM    sys.objects
			WHERE   object_id = OBJECT_ID(N'[dbo].[sp_GetGradeBookComponents]')
					AND type IN ( N'P', N'PC' ) ) 
DROP PROCEDURE [dbo].[sp_GetGradeBookComponents]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetGradeBookComponents] 

	@StuEnrollId VARCHAR(38),
	@ClsSectionId VARCHAR(38),
	@TermId VARCHAR(38)

AS

BEGIN

SET FMTONLY OFF;

IF OBJECT_ID('tempdb..#tmpGradeBookComponents') IS NOT NULL

DROP TABLE #tmpGradeBookComponents

CREATE TABLE #tmpGradeBookComponents

	(	 [StuEnrollId] VARCHAR (38),
		 [Term] VARCHAR(50),
		 [TermId] VARCHAR(38),
		 [ClassStartDate] DATETIME,
		 [ClassEndDate] DATETIME,
		 [CourseName] VARCHAR(100),
		 [ClsSectionId] VARCHAR(38),
		 [InstructorName] VARCHAR(100),
		 [CourseCode] VARCHAR(12),
		 [Credits] DECIMAL(6,2),
		 [Hours] DECIMAL(6,2),
		 [GradeBookComponent] VARCHAR(50),
		 [ComponentType] VARCHAR(50),
		 [Score] DECIMAL(6,2),
		 [Grade] CHAR(5),
		 [Weight] VARCHAR(50)

	)

INSERT INTO #tmpGradeBookComponents

SELECT DISTINCT t1.StuEnrollId,t3.TermDescrip AS Term,t4.TermId,t4.StartDate AS ClassStartDate,t4.EndDate AS ClassEndDate,t2.Descrip AS CourseName, t4.ClsSectionId,Users.FullName AS [InstructorName]
,t2.Code AS CourseCode,t2.Credits, t2.Hours,WgtDetail.Descrip AS GradebookComponent,CType.Descrip AS ComponentType, GrdResult.Score,(SELECT Grade FROM arGradeSystemDetails WHERE GrdSysDetailId = t1.GrdSysDetailId) AS Grade,CAST(WgtDetail.Weight AS INTEGER) AS Weight
FROM   arResults t1
INNER JOIN arClassSections t4 ON t1.TestId = t4.ClsSectionId
INNER JOIN arClassSectionTerms t5 ON t4.ClsSectionId = t5.ClsSectionId 
INNER JOIN arTerm t3 ON t5.TermId = t3.TermId 
INNER JOIN  arReqs t2 ON t4.ReqId = t2.ReqId 
INNER JOIN  arStuEnrollments t6 ON t1.StuEnrollId = t6.StuEnrollId 
INNER JOIN syusers Users ON t4.InstructorId=Users.UserId  
LEFT JOIN dbo.arGrdBkResults GrdResult ON t1.StuEnrollId=GrdResult.StuEnrollId AND t1.TestId=GrdResult.ClsSectionId 
LEFT JOIN dbo.arGradeSystemDetails GrdSysDetail ON t1.GrdSysDetailId=GrdSysDetail.GrdSysDetailId
LEFT JOIN dbo.arGrdBkWgtDetails WgtDetail ON GrdResult.InstrGrdBkWgtDetailId=WgtDetail.InstrGrdBkWgtDetailId
LEFT JOIN arGrdComponentTypes CType ON WgtDetail.GrdComponentTypeId=CType.GrdComponentTypeId
WHERE      
(t1.GrdSysDetailId IS NOT NULL OR (t1.GrdSysDetailId IS NULL AND t1.isClinicsSatisfied=1)  OR (t1.GrdSysDetailId IS NULL  
AND  (t1.ISClinicsSatISfied=0 OR t1.ISClinicsSatISfied IS NULL) AND (SELECT COUNT(*) FROM argrdBkResults WHERE StuEnrollId=t1.StuEnrollId AND ClsSectionId=t1.TestId AND Score IS NOT NULL) >=1))      
AND t1.StuEnrollId = @StuEnrollId

UNION 

SELECT DISTINCT t10.StuEnrollId,t30.TermDescrip AS Term,t10.TermId,'1/1/1900' AS ClassStartDate,'1/1/1900' AS ClassEndDate,t20.Descrip AS CourseName,NULL,NULL,t20.Code AS CourseCode,       
t20.Credits,T20.Hours,NULL,NULL,NULL,(SELECT Grade FROM arGradeSystemDetails WHERE GrdSysDetailId = t10.GrdSysDetailId) AS Grade,NULL  
FROM arTransferGrades t10
INNER JOIN arTerm t30 
ON t10.TermId = t30.TermId 
INNER JOIN  arReqs t20
ON t10.ReqId = t20.ReqId
WHERE 
(t10.GrdSysDetailId IS NOT NULL OR (t10.GrdSysDetailId IS NULL AND t10.ISClinicsSatISfied=1) OR (t10.GrdSysDetailId IS NULL AND (t10.ISClinicsSatISfied=0 OR t10.ISClinicsSatISfied IS NULL)   
AND (SELECT COUNT (*) FROM argrdBkResults WHERE StuEnrollId=t10.StuEnrollId AND ClsSectionId IN (SELECT DISTINCT ClsSectionId FROM arClASsSections WHERE ReqId=t10.ReqId AND TermId=t10.TermId) AND ScORe IS NOT NULL) >=1))        
AND t10.StuEnrollId = @StuEnrollId
 
UNION  
 
SELECT DISTINCT t1.StuEnrollId,t3.TermDescrip AS Term,t4.TermId,t4.StartDate AS ClASsStartDate,t4.EndDate  AS ClASsEndDate,t2.Descrip AS [CourseName],NULL,NULL,t2.Code AS CourseCode, t2.Credits,T2.Hours,NULL,NULL,NULL, 
 (SELECT Grade FROM arGradeSystemDetails WHERE GrdSysDetailId = t1.GrdSysDetailId) AS Grade,NULL
FROM  
(SELECT DISTINCT t700.ReqId AS EqReqId,t3.ReqId,t100.StuEnrollId  FROM arStuEnrollments t100, arReqs t3,arProgVerDef t400, arStudent t500, arPrgVersions t600 ,arCourseEquivalent t700 
WHERE
(t100.StudentId = t500.StudentId)  AND t3.ReqId = t400.ReqId AND t100.PrgVerId = t400.PrgVerId AND t100.PrgVerId = t600.PrgVerId  AND t3.Reqid=t700.EquivReqId AND t100.StuEnrollId = @StuEnrollId
AND t3.Reqid NOT IN (SELECT ReqId FROM arResults a, arClASsSections b WHERE a.TestId=b.ClsSectionId  AND StuEnrollId =  @StuEnrollId AND ReqId=t400.ReqId)) S, arResults t1, arReqs t2,  
arTerm t3, arClASsSections t4, arClASsSectionTerms t5, arStuEnrollments t6  WHERE  t1.TestId = t4.ClsSectionId AND t4.ClsSectionId = t5.ClsSectionId       
AND t5.TermId = t3.TermId AND t4.ReqId = t2.ReqId  AND t1.StuEnrollId = t6.StuEnrollId AND  t1.GrdSysDetailId IS NOT NULL AND t1.StuEnrollId = S.StuEnrollId  AND t2.Reqid=S.EqReqId 

ORDER BY Term DESC,ClassStartDate,ClassEndDate 

BEGIN 
	SELECT DISTINCT GradeBookComponent,ComponentType,Score,(Weight+'%') AS Weight FROM #tmpGradeBookComponents WHERE GradeBookComponent IS NOT NULL
	AND StuEnrollId=@StuEnrollId AND ClsSectionId=@ClsSectionId AND TermId=@TermId
END

END


GO

/* Anu Kanakam added above scripts */

/* 

=============================================================================================================

================ */





-- ==========================================================================
-- usp_AdvPortalCampuses
-- ==========================================================================

GO

IF EXISTS ( SELECT  *
			FROM    sys.objects
			WHERE   object_id = OBJECT_ID(N'[dbo].[usp_AdvPortalCampuses]')
					AND type IN ( N'P', N'PC' ) ) 
DROP PROCEDURE [dbo].[usp_AdvPortalCampuses]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_AdvPortalCampuses]

AS

BEGIN

DECLARE @PortalCampuses TABLE
(
	SerialNum INT,
	CampusId VARCHAR(255)
)

 INSERT INTO @PortalCampuses

 SELECT	DISTINCT ROW_NUMBER() OVER (ORDER BY CampusId),AppSettingValue.CampusId
 FROM syConfigAppSettings Appsetting
 INNER JOIN dbo.syConfigAppSetValues AppSettingValue
 ON Appsetting.SettingId = AppSettingValue.SettingId
 WHERE KeyName='PortalCustomer'
 AND AppSettingValue.Value='Yes'

DECLARE @PortalCampusDescrip TABLE
(
	  CampusId VARCHAR(255),
	  CampusDescrip VARCHAR(255)
)

DECLARE @i INT
SET @i=1

 WHILE (@i <= (SELECT MAX(SerialNum) FROM @PortalCampuses))

 BEGIN

	IF(SELECT CampusId FROM @PortalCampuses WHERE SerialNum=@i) IS NULL

		BEGIN

			 INSERT INTO @PortalCampusDescrip
			 SELECT DISTINCT CampusId,CampDescrip FROM dbo.syCampuses Campus
			 INNER JOIN dbo.syStatuses Statuses
			 ON Campus.StatusId=Statuses.StatusId
			 WHERE Statuses.Status='Active'
			 AND Campus.LeadStatusId IS NOT NULL
			 AND Campus.AdmRepId IS NOT NULL

		 SET @i=@i+1

		END 

	ELSE IF (SELECT CampusId FROM @PortalCampuses WHERE SerialNum=@i) IS NOT NULL

		BEGIN

			 INSERT INTO @PortalCampusDescrip
			 SELECT DISTINCT PC.CampusId,Camp.CampDescrip FROM @PortalCampuses PC
			 INNER JOIN dbo.syCampuses Camp
			 ON PC.CampusId = Camp.CampusId
			 INNER JOIN dbo.syStatuses Statuses
			 ON Camp.StatusId=Statuses.StatusId
			 WHERE Statuses.Status='Active'
			 AND Camp.LeadStatusId IS NOT NULL
			 AND Camp.AdmRepId IS NOT NULL

		 SET @i=@i+1

		END 
 END

SELECT DISTINCT * FROM @PortalCampusDescrip

ORDER BY CampusDescrip

END

GO


/* ============================================================================================================================= */

IF EXISTS ( SELECT  *
			FROM    sys.objects
			WHERE   object_id = OBJECT_ID(N'[dbo].[sp_GetCourseList]')
					AND type IN ( N'P', N'PC' ) ) 
DROP PROCEDURE [dbo].[sp_GetCourseList]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sp_GetCourseList] 

AS

BEGIN

SET NOCOUNT ON;

SELECT  DISTINCT
	 Campus.CampDescrip AS Campus
	,PrgGrp.PrgGrpDescrip AS Program
	,Req.Code
	,Req.Descrip AS Course
	,Degree.DegreeCode AS Degree
FROM dbo.arReqs Req
INNER JOIN dbo.arClassSections CSection ON Req.ReqId = CSection.ReqId 
INNER JOIN dbo.syCampuses Campus ON CSection.CampusId = Campus.CampusId 
INNER JOIN dbo.arProgVerDef PrVerDef ON CSection.ReqId = PrVerDef.ReqId 
INNER JOIN dbo.arPrgVersions PrgVer ON PrVerDef.PrgVerId = PrgVer.PrgVerId AND Req.CampGrpId = PrgVer.CampGrpId 
INNER JOIN dbo.arDegrees Degree ON PrgVer.DegreeId=Degree.DegreeId
INNER JOIN arPrgGrp PrgGrp ON PrgVer.PrgGrpId = PrgGrp.PrgGrpId 
WHERE Req.ReqTypeId = 1 --Test
AND Req.StatusID = (SELECT StatusId FROM dbo.syStatuses WHERE Status='Active')
ORDER BY Campus.CampDescrip,PrgGrp.PrgGrpDescrip,Req.Code

END

GO

/* ============================================================================================================================= */

IF EXISTS ( SELECT  *
			FROM    sys.objects
			WHERE   object_id = OBJECT_ID(N'[dbo].[sp_GetStuEnrollments]')
					AND type IN ( N'P', N'PC' ) ) 
DROP PROCEDURE [dbo].[sp_GetStuEnrollments]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE sp_GetStuEnrollments
	@StudentNumber VARCHAR(50)
AS

BEGIN

SELECT DISTINCT
	 E.StuEnrollId
	,Program.ProgDescrip AS Enrollment
FROM arstuEnrollments AS E
			INNER JOIN arStudent AS S ON E.StudentId = S.StudentId
			INNER JOIN arPrgVersions AS V ON E.PrgVerId = V.PrgVerId
			INNER JOIN dbo.arPrograms Program ON V.ProgId = Program.ProgId
			INNER JOIN syStatuses AS ST ON V.StatusId = ST.StatusId 
			INNER JOIN syCampuses AS C ON E.CampusId = C.CampusId
			INNER JOIN arShifts AS sh on e.Shiftid = sh.Shiftid
			INNER JOIN systatuscodes AS SC ON E.StatusCodeId = SC.StatusCodeId
			INNER JOIN sysysstatus AS SS ON SC.SysStatusId = SS.SysStatusId
			WHERE(SS.StatusLevelId = 2)
			AND SS.SysStatusId IN (9,10,11,12,14,19,20,21,22)
			AND S.StudentNumber=@StudentNumber
			AND E.StartDate <= GETDATE()
			ORDER BY E.StuEnrollId DESC
END

GO	

/* ============================================================================================================================= */

IF EXISTS ( SELECT  *
			FROM    sys.objects
			WHERE   object_id = OBJECT_ID(N'[dbo].[sp_GetStuEnrollmentTerms]')
					AND type IN ( N'P', N'PC' ) ) 
DROP PROCEDURE [dbo].[sp_GetStuEnrollmentTerms]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE sp_GetStuEnrollmentTerms 

	@StuEnrollId VARCHAR(100)

AS

BEGIN
SET FMTONLY OFF;

IF OBJECT_ID('tempdb..#tmpStuEnrollmentTerms') IS NOT NULL

DROP TABLE #tmpStuEnrollmentTerms


CREATE TABLE #tmpStuEnrollmentTerms
	(
		 TermId VARCHAR(100)
		,TermDescrip VARCHAR(100)
		,StartDate DATETIME
	)

INSERT INTO #tmpStuEnrollmentTerms
	SELECT DISTINCT
		 C.TermId
		,C.TermDescrip
		,C.StartDate
	FROM  arResults A
	INNER JOIN arClassSections B ON A.TestId=B.ClsSectionId
	INNER JOIN arTerm AS C ON B.TermId=C.TermId
	INNER JOIN syStatuses AS S ON C.StatusId = S.StatusId
	WHERE  A.StuEnrollId = @StuEnrollId
	AND  C.StartDate <= GETDATE()
	AND S.Status = 'Active'

IF (SELECT COUNT(*) FROM #tmpStuEnrollmentTerms) > 1

	BEGIN	
		INSERT INTO #tmpStuEnrollmentTerms VALUES('00000000-0000-0000-0000-000000000000','All',GETDATE())
	END

SELECT TermId,TermDescrip FROM #tmpStuEnrollmentTerms ORDER BY StartDate DESC

END

GO	

/* ============================================================================================================================= */

IF EXISTS ( SELECT  *
			FROM    sys.objects
			WHERE   object_id = OBJECT_ID(N'[dbo].[sp_GetClsSectionsForTerm]')
					AND type IN ( N'P', N'PC' ) ) 
DROP PROCEDURE [dbo].[sp_GetClsSectionsForTerm]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE sp_GetClsSectionsForTerm

	@StuEnrollId VARCHAR(100),

	@TermId VARCHAR(100)

AS

BEGIN

SET FMTONLY OFF;

IF OBJECT_ID('tempdb..#tmpClsSectionsForTerm') IS NOT NULL
DROP TABLE #tmpClsSectionsForTerm

CREATE TABLE #tmpClsSectionsForTerm
	(
		 ClsSectionID VARCHAR(100)
		,ClsSection VARCHAR(100)
	)

IF(@TermId <> '00000000-0000-0000-0000-000000000000')

BEGIN

INSERT INTO #tmpClsSectionsForTerm

	SELECT DISTINCT

	B.ClsSectionId,('('+R.Code+')'+' - '+ R.Descrip) AS ClsSection

	FROM arResults A

	INNER JOIN arClassSections B ON A.TestId=B.ClsSectionId

	INNER JOIN arTerm  C ON B.TermId=C.TermId

	INNER JOIN syStatuses  S ON C.StatusId = S.StatusId

	INNER JOIN dbo.arReqs R ON B.ReqId=R.ReqId

WHERE 

	B.TermId=@TermId 

	AND A.StuEnrollId=@StuEnrollId

	ORDER BY B.ClsSectionID
END

ELSE 

BEGIN

INSERT INTO #tmpClsSectionsForTerm

	SELECT DISTINCT

	B.ClsSectionID,('('+R.Code+')'+' - '+ R.Descrip) AS ClsSection

	FROM arResults A

	INNER JOIN arClassSections B ON A.TestId=B.ClsSectionId

	INNER JOIN arTerm  C ON B.TermId=C.TermId

	INNER JOIN syStatuses  S ON C.StatusId = S.StatusId

	INNER JOIN dbo.arReqs R ON B.ReqId=R.ReqId

WHERE 

	A.StuEnrollId=@StuEnrollId

	ORDER BY B.ClsSectionID

END

IF (SELECT COUNT(*) FROM #tmpClsSectionsForTerm) > 1

BEGIN	

INSERT INTO #tmpClsSectionsForTerm VALUES('00000000-0000-0000-0000-000000000000','All')

END

SELECT * FROM #tmpClsSectionsForTerm ORDER BY 1 

END

GO	

/* ============================================================================================================================= */


/* ============================================================================================================================= */




IF EXISTS ( SELECT  *
			FROM    sys.objects
			WHERE   object_id = OBJECT_ID(N'[dbo].[sp_getStuEnrollmentsPortal]')
					AND type IN ( N'P', N'PC' ) ) 
DROP PROCEDURE [dbo].[sp_getStuEnrollmentsPortal]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:	Ginzo, John
-- Create date: 06/18/2014
-- Description:	Get Student Enrollments for portal
-- =============================================
CREATE PROCEDURE [dbo].[sp_getStuEnrollmentsPortal]
@StudentNumber VARCHAR(50)
AS
BEGIN
-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;

   SELECT	DISTINCT	E.StuEnrollId, Program.ProgDescrip AS Enrollment
FROM	arstuEnrollments E
INNER JOIN	arStudent S ON E.StudentId = S.StudentId
INNER JOIN	arPrgVersions V ON E.PrgVerId = V.PrgVerId
INNER JOIN	dbo.arPrograms Program ON V.ProgId = Program.ProgId
   INNER JOIN	syStatuses ST ON V.StatusId = ST.StatusId 
INNER JOIN	syCampuses C ON E.CampusId = C.CampusId
   INNER JOIN	arShifts sh on e.Shiftid = sh.Shiftid
   INNER JOIN	systatuscodes SC ON E.StatusCodeId = SC.StatusCodeId
   INNER JOIN	sysysstatus SS ON SC.SysStatusId = SS.SysStatusId
   WHERE	SS.StatusLevelId = 2
   AND	SS.SysStatusId IN (7,9,10,11,12,14,19,20,21,22)
   AND	S.StudentNumber=@StudentNumber
   AND	E.StartDate <= GETDATE()
   ORDER BY	E.StuEnrollId DESC
END

GO

/****** Object:  StoredProcedure [dbo].[usp_getstudentnameandnumber]    Script Date: 8/4/2014 3:51:02 PM ******/
IF EXISTS ( SELECT  *
			FROM    sys.objects
			WHERE   object_id = OBJECT_ID(N'[dbo].[usp_getstudentnameandnumber]')
					AND type IN ( N'P', N'PC' ) ) 
DROP PROCEDURE [dbo].[usp_getstudentnameandnumber]
GO

CREATE PROC [dbo].[usp_getstudentnameandnumber]
@StuEnrollId VARCHAR(50)
AS
BEGIN
	DECLARE @StudentNumber VARCHAR(50)
	DECLARE @Name VARCHAR(50)

	SELECT (S.FirstName + ' ' + ISNULL(S.MiddleName,'') + ' ' + S.LastName) AS Name, S.StudentNumber FROM arStudent S INNER JOIN arStuEnrollments SE ON S.StudentId=SE.StudentId 
				WHERE SE.StuEnrollId = @StuEnrollId
	
END
 
GO
/****** Object:  StoredProcedure [dbo].[usp_configsettings_getlist]    Script Date: 8/4/2014 2:39:07 PM ******/
IF EXISTS ( SELECT  *
			FROM    sys.objects
			WHERE   object_id = OBJECT_ID(N'[dbo].[usp_configsettings_getlist]')
					AND type IN ( N'P', N'PC' ) ) 
DROP PROCEDURE [dbo].[usp_configsettings_getlist]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[usp_configsettings_getlist]
@StuEnrollId VARCHAR(50)
AS
BEGIN
	DECLARE @CampusId UNIQUEIDENTIFIER
	
	SET @CampusId = (SELECT TOP 1 CampusId FROM arStuEnrollments WHERE StuEnrollId=@StuEnrollId)

	DECLARE @GradeCourseRepetitionsMethod VARCHAR(50),@GradeCourseRepetitionsMethodId INT
	SET @GradeCourseRepetitionsMethodId=(SELECT TOP 1 SettingId FROM syConfigAppSettings WHERE KeyName='GradeCourseRepetitionsMethod')
	IF ( SELECT COUNT(*)
			 FROM   syConfigAppSetValues
			 WHERE  SettingId = @GradeCourseRepetitionsMethodId
					AND CampusId = @CampusId
		   ) >= 1 
			BEGIN
				SET @GradeCourseRepetitionsMethod = ( SELECT TOP 1 VALUE
										  FROM      dbo.syConfigAppSetValues
										  WHERE     SettingId = @GradeCourseRepetitionsMethodId
													AND CampusId = @CampusId
										)  
								
			END 
		ELSE 
			BEGIN
					SET @GradeCourseRepetitionsMethod = ( SELECT TOP 1 VALUE
										  FROM      dbo.syConfigAppSetValues
										  WHERE     SettingId = @GradeCourseRepetitionsMethodId
													AND CampusId IS NULL
										)  
			END

			

	DECLARE @IncludeHoursForFailingGrade BIT,@IncludeHoursForFailingGradeId INT
	SET @IncludeHoursForFailingGradeId=(SELECT TOP 1 SettingId FROM syConfigAppSettings WHERE KeyName='IncludeHoursForFailingGrade')
	IF ( SELECT COUNT(*)
			 FROM   syConfigAppSetValues
			 WHERE  SettingId = @IncludeHoursForFailingGradeId
					AND CampusId = @CampusId
		   ) >= 1 
			BEGIN
				SET @IncludeHoursForFailingGrade = ( SELECT  TOP 1 VALUE
										  FROM      dbo.syConfigAppSetValues
										  WHERE     SettingId = @IncludeHoursForFailingGradeId
													AND CampusId = @CampusId
										)  
								
			END 
		ELSE 
			BEGIN
					SET @IncludeHoursForFailingGrade = ( SELECT TOP 1   VALUE
										  FROM      dbo.syConfigAppSetValues
										  WHERE     SettingId = @IncludeHoursForFailingGradeId
													AND CampusId IS NULL
										)  
			END

	DECLARE @GradesFormat VARCHAR(50),@GradesFormatId INT
	SET @GradesFormatId=(SELECT TOP 1 SettingId FROM syConfigAppSettings WHERE KeyName='GradesFormat')
	IF ( SELECT COUNT(*)
			 FROM   syConfigAppSetValues
			 WHERE  SettingId = @GradesFormatId
					AND CampusId = @CampusId
		   ) >= 1 
			BEGIN
				SET @GradesFormat = ( SELECT TOP 1 VALUE
										  FROM      dbo.syConfigAppSetValues
										  WHERE     SettingId = @GradesFormatId
													AND CampusId = @CampusId
										)  
								
			END 
		ELSE 
			BEGIN
					SET @GradesFormat = ( SELECT TOP 1 VALUE
										  FROM      dbo.syConfigAppSetValues
										  WHERE     SettingId = @GradesFormatId
													AND CampusId IS NULL
										)  
			END

	DECLARE @TranscriptType VARCHAR(50),@TranscriptTypeId INT
	SET @TranscriptTypeId=(SELECT TOP 1 SettingId FROM syConfigAppSettings WHERE KeyName='TranscriptType')
	IF ( SELECT COUNT(*)
			 FROM   syConfigAppSetValues
			 WHERE  SettingId = @TranscriptTypeId
					AND CampusId = @CampusId
		   ) >= 1 
			BEGIN
				SET @TranscriptType = ( SELECT TOP 1 VALUE
										  FROM      dbo.syConfigAppSetValues
										  WHERE     SettingId = @TranscriptTypeId
													AND CampusId = @CampusId
										)  
								
			END 
		ELSE 
			BEGIN
					SET @TranscriptType = ( SELECT TOP 1 VALUE
										  FROM      dbo.syConfigAppSetValues
										  WHERE     SettingId = @TranscriptTypeId
													AND CampusId IS NULL
										)  
			END

	DECLARE @addcreditsbyservice VARCHAR(50),@addcreditsbyserviceId INT
	SET @addcreditsbyserviceId=(SELECT TOP 1 SettingId FROM syConfigAppSettings WHERE KeyName='addcreditsbyservice')
	IF ( SELECT COUNT(*)
			 FROM   syConfigAppSetValues
			 WHERE  SettingId = @addcreditsbyserviceId
					AND CampusId = @CampusId
		   ) >= 1 
			BEGIN
				SET @addcreditsbyservice = ( SELECT TOP 1 VALUE
										  FROM      dbo.syConfigAppSetValues
										  WHERE     SettingId = @addcreditsbyserviceId
													AND CampusId = @CampusId
										)  
								
			END 
		ELSE 
			BEGIN
					SET @addcreditsbyservice = ( SELECT TOP 1 VALUE
										  FROM      dbo.syConfigAppSetValues
										  WHERE     SettingId = @addcreditsbyserviceId
													AND CampusId IS NULL
										)  
			END

	SELECT 
				@GradeCourseRepetitionsMethod AS GradeCourseRepetitionsMethod,
				@IncludeHoursForFailingGrade AS IncludeHoursForFailingGrade,
				@GradesFormat AS GradesFormat,
				@TranscriptType AS TranscriptType,
				@addcreditsbyservice AS AddCreditsByService

END

GO





IF EXISTS ( SELECT  *
			FROM    sys.objects
			WHERE   object_id = OBJECT_ID(N'[dbo].[usp_GetLeadDocsStatus]')
					AND type IN ( N'P', N'PC' ) ) 
DROP PROCEDURE [dbo].[usp_GetLeadDocsStatus]
GO



SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[usp_GetLeadDocsStatus]

	 @FirstName VARCHAR(50)
	,@LastName VARCHAR(50)
	,@Email VARCHAR(50)

AS

BEGIN

	DECLARE @LeadID VARCHAR(50)
	SET @LeadID=(SELECT LeadId FROM AdLeads WHERE FirstName=@FirstName AND LastName=@LastName AND (HomeEmail=@Email OR WorkEmail=@Email))

	DECLARE @CampusGrps TABLE(CampusGrpId VARCHAR(50))
	INSERT INTO @CampusGrps	SELECT DISTINCT CampGrpId FROM dbo.syCmpGrpCmps WHERE CampusId IN (SELECT TOP 1 CampusId FROM adLeads WHERE FirstName=@FirstName AND LastName=@LastName AND (HomeEmail=@Email OR WorkEmail=@Email))

	DECLARE @LeadDocs TABLE( DocumentId VARCHAR(50)	,DocumentDescrip VARCHAR(50),StartDate DATETIME	,EndDate DATETIME,DocSubmittedCount INT,
							[Required] INT,DocStatusDescrip VARCHAR(50),DateReceived DATETIME)

	INSERT	INTO @LeadDocs

	SELECT		adReqId AS  DocumentId,
				Descrip AS DocumentDescrip,
				startDate,
				ENDDate,
				DocSubmittedCount,
				[Required],    
				CASE WHEN DocStatusDescrip='Approved' THEN 'Approved' ELSE 'Currently Due' END AS DocStatusDescrip,        
				(SELECT DISTINCT ReceiveDate  FROM adLeadDocsReceived WHERE DocumentId=adReqId AND LeadId=@LeadID) AS DateReceived            
	FROM        (SELECT  DISTINCT adReqId, Descrip,StartDate,ENDDate, [Required],DocSubmittedCount,DocStatusDescrip,CampGrpId,COUNT(*) OVER ( PARTITION BY adReqId ) AS DupCount            
				FROM	(SELECT DISTINCT t1.adReqId, t1.Descrip,GETDATE() AS CurrentDate,t2.StartDate,t2.ENDDate,  	           	
						(SELECT Count(*) FROM adLeadDocsReceived WHERE DocumentId=t1.adReqId AND LeadId=@LeadID) AS DocSubmittedCount, 	           	
						1 AS Required, 
				(SELECT DISTINCT s1.DocStatusDescrip FROM sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3 
				WHERE  s1.SysDocStatusId = s2.sysDocStatusId 
				AND s2.DocStatusId = s3.DocStatusId 
				AND s3.LeadId=@LeadID 
				AND s3.DocumentId = t1.adReqId) AS DocStatusDescrip,
				t1.CampGrpId           
	FROM		adReqs t1, 
				adReqsEffectiveDates t2            
	WHERE		t1.adReqId = t2.adReqId 
	AND			t1.reqfOREnrollment = 1 
	AND			t2.MANDatORyRequirement=1 
	AND			t1.adReqTypeId IN (3)  
	AND			t1.StatusId=(SELECT TOP 1 StatusId 
							FROM dbo.syStatuses 
							WHERE Status='Active')) R1 WHERE R1.CurrentDate >= R1.StartDate 
							AND (R1.CurrentDate <= R1.EndDate OR R1.EndDate IS NULL)  
						
UNION			 

	SELECT		adReqId,            
				Descrip,            
				StartDate,            
				ENDDate,            
				[Required], 	           
				DocSubmittedCount, 	           
				DocStatusDescrip,
				CampGrpId,COUNT(*) OVER ( PARTITION BY adReqId ) AS DupCount            
	FROM		(SELECT DISTINCT t1.adReqId, t1.Descrip,  GETDATE() AS CurrentDate, t2.StartDate,t2.ENDDate,   	           	
				(SELECT Count(*) FROM adLeadDocsReceived WHERE DocumentId=t1.adReqId AND LeadId=@LeadID)  AS DocSubmittedCount,  	           	
				ISNULL(t3.ISRequired,0) AS [Required],  	           
				(SELECT DISTINCT s1.DocStatusDescrip FROM sySysDocStatuses s1,syDocStatuses s2,adLeadDocsReceived s3  
				WHERE  s1.SysDocStatusId = s2.sysDocStatusId 
				AND s2.DocStatusId = s3.DocStatusId 
				AND  s3.LeadId= @LeadID 
				AND s3.DocumentId = t1.adReqId) AS DocStatusDescrip,
				t1.CampGrpId            
	FROM 		adReqs t1, adReqsEffectiveDates t2, adReqLeadGroups t3  
	WHERE		t1.adReqId = t2.adReqId 
				AND t2.adReqEffectiveDateId = t3.adReqEffectiveDateId 
				AND t1.reqfOREnrollment = 1 		
				AND t3.LeadGrpId IN (SELECT LeadGrpId 
									FROM adLeadByLeadGroups 
									WHERE LeadId=@LeadID) 
				AND t1.adReqTypeId IN (3) 
				AND t2.MANDatORyRequirement <> 1  
				AND t1.StatusId=(SELECT TOP 1 StatusId 
								FROM dbo.syStatuses 
								WHERE Status='Active')) R1 
	WHERE		R1.CurrentDate >= R1.StartDate 
				AND (R1.CurrentDate <= R1.ENDDate OR R1.ENDDate IS NULL) ) R2  
	WHERE		((DupCount >1 
				AND  REQUIRED=1) OR (DupCount=1 AND REQUIRED IN (0,1)))   
				AND R2.CampGrpId IN (SELECT DISTINCT CampGrpId FROM @CampusGrps)
	ORDER BY	R2.Descrip

	
	--IF (SELECT COUNT(*) FROM @LeadDocs WHERE DocStatusDescrip ='Currently Due' AND Required=1) > 0
	--	BEGIN
	--		SELECT DISTINCT DocumentDescrip,DocStatusDescrip FROM @LeadDocs WHERE DocStatusDescrip='Currently Due' AND Required=1
	--	END
	--ELSE IF (SELECT COUNT(*) FROM @LeadDocs WHERE DocStatusDescrip ='Approved' AND Required=1) > 0
	--	BEGIN
	--		SELECT DISTINCT DocumentDescrip,DocStatusDescrip FROM @LeadDocs WHERE DocStatusDescrip='Approved' AND Required=1
	--	END

	SELECT DocumentDescrip,DocStatusDescrip FROM @LeadDocs
END
GO
-- ===================================================================================================================
--  MySchedule Print facility stored procedure for report
-- ===================================================================================================================
/****** Object:  StoredProcedure [dbo].[USP_StudentSchedule_GetList]    Script Date: 8/13/2014 2:33:10 PM ******/
 

IF EXISTS ( SELECT  *
			FROM    sys.objects
			WHERE   object_id = OBJECT_ID(N'[dbo].[USP_StudentSchedule_GetList]')
					AND type IN ( N'P', N'PC' ) ) 
DROP PROCEDURE [dbo].[USP_StudentSchedule_GetList]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[USP_StudentSchedule_GetList]
@studentid VARCHAR(50)
AS
SELECT DISTINCT
	cs.ClsSectionId, t.TermId, t.TermDescrip, cs.StartDate,cs.EndDate, r.ReqId,r.Descrip,r.Code,
	cs.ClsSection,r.Credits,r.Hours,u.UserId,u.FullName,gsd.Grade,
	s.FirstName + ' ' + s.LastName AS StudentName,
	s.StudentNumber AS StudentNumber
FROM  
	arResults res
				INNER JOIN  arStuEnrollments enr ON res.StuEnrollId = enr.StuEnrollId
				INNER JOIN  arClassSections cs ON res.TestId = cs.ClsSectionId
				INNER JOIN  arClassSectionTerms cst ON cs.ClsSectionId = cst.ClsSectionId
				INNER JOIN  arTerm t ON cst.TermId = t.TermId
				INNER JOIN  arReqs r ON cs.ReqId = r.ReqId
				INNER JOIN  syCmpGrpCmps cmpgrpcmps ON cs.CampusId = cmpgrpcmps.CampusId
				LEFT JOIN  arGradeSystemDetails gsd ON res.GrdSysDetailId = gsd.GrdSysDetailId 
				LEFT JOIN  arClsSectMeetings csm ON res.TestId = csm.ClsSectionId
				INNER JOIN  syUsers u ON cs.InstructorId = u.UserId
				INNER JOIN arStudent s ON s.studentid = enr.studentid
				INNER JOIN arPrgVersions PV ON enr.PrgVerId = PV.PrgVerId
				INNER JOIN arPrograms P ON PV.ProgId = P.ProgId
 WHERE 
		s.studentnumber = @studentid
ORDER BY
		 cs.startdate DESC, r.descrip, gsd.grade

GO


/****** Object:  StoredProcedure [dbo].[USP_Rooms_GetList]    Script Date: 8/13/2014 2:40:24 PM ******/

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

IF EXISTS ( SELECT  *
			FROM    sys.objects
			WHERE   object_id = OBJECT_ID(N'[dbo].[USP_Rooms_GetList]')
					AND type IN ( N'P', N'PC' ) ) 
DROP PROCEDURE [dbo].[USP_Rooms_GetList]
GO

CREATE PROC [dbo].[USP_Rooms_GetList]
@studentid VARCHAR(50),
@clssectionid UNIQUEIDENTIFIER
AS
SELECT DISTINCT
	p.PeriodDescrip,
	B.BldgDescrip,
	R1.Descrip AS Room
FROM arResults res
					   INNER JOIN arStuEnrollments enr ON res.StuEnrollId = enr.StuEnrollId
					   INNER JOIN arClassSections cs ON res.TestId = cs.ClsSectionId
					   INNER JOIN arClassSectionTerms cst ON cs.ClsSectionId = cst.ClsSectionId
					   INNER JOIN arTerm t ON cst.TermId = t.TermId
					   INNER JOIN arReqs r ON cs.ReqId = r.ReqId
					   INNER JOIN syCmpGrpCmps cmpgrpcmps ON cs.CampusId = cmpgrpcmps.CampusId
					   LEFT JOIN arGradeSystemDetails gsd ON res.GrdSysDetailId = gsd.GrdSysDetailId
					   LEFT JOIN arClsSectMeetings csm ON res.TestId = csm.ClsSectionId
					   INNER JOIN syPeriods p ON csm.PeriodId = p.PeriodId
					   INNER JOIN arStudent s ON s.studentid = enr.studentid
					   INNER JOIN arRooms R1 ON csm.RoomId = R1.RoomId
					   INNER JOIN arBuildings B ON R1.BldgId = B.BldgId
					   WHERE 
							 s.studentnumber = @studentid
							 AND cs.ClsSectionId = @clssectionid

GO
-- ===================================================================================================================
--  END MySchedule Print facility stored procedure for report 
-- ===================================================================================================================

