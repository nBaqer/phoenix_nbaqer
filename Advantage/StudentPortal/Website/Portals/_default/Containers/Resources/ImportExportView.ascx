<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ImportExportView.ascx.cs" Inherits="TanLD.Modules.ImportExportUsers.ImportExportView" %>

<div class="tanld">
<h2>Import/Export Users</h2>

<p style="color: Red;">
    ** It is STRONGLY recommended that you make a backup of your DNN site BEFORE importing any users. **
</p>

<table cellspacing="5" cellpadding="15" border="0">
    <tr>
        <td width="50%" valign="top">
            <fieldset style="width: 99%;">
                <legend>IMPORT USERS</legend>
                <ol class="form">
                    <li>
                        <label style="font-weight: bold;">Excel File (*.xls, *.csv):</label>
                        <span>
                            <asp:FileUpload ID="fupImportFile" runat="server" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="fupImportFile" ErrorMessage="*.xls, *.csv files only" ValidationExpression=".*\.(xls|XLS|csv|CSV)$"> *.xls, *.csv files only</asp:RegularExpressionValidator>
                        </span>
                    </li>
                    <li style="padding-top: 5px;">
                        <span>
                            <asp:Button ID="btnImport" runat="server" Text="Import" OnClick="btnImport_Click" />
                        </span>
                    </li>
                </ol>     
            </fieldset>  
            <div style="margin: 10px 0px;">
                <strong>Download Example Excel Files:</strong>
            </div>
            &nbsp;&nbsp;- <a href="<%= ControlPath %>Templates/Excel-Sample.xls">Excel template sample</a>
            <br />&nbsp;&nbsp;- <a href="<%= ControlPath %>Templates/CSV-Sample.csv">CSV template sample</a>                    
            
        </td>        
    
        <td valign="top">
            <fieldset style="width: 99%;">
                <legend>EXPORT USERS</legend>
                <ol class="form">
                    <li>
                        <label style="font-weight: bold;">Role:</label>
                        <span>
                            <asp:DropDownList ID="cboRoles" runat="server" Width="90%" />
                        </span>
                    </li>
                    <li style="padding-top: 5px;">
                        <span>
                            <asp:Button ID="btnExport" runat="server" Text="Export Excel" OnClick="btnExport_Click" />
                        </span>
                        <span>
                            <asp:Button ID="btnExportCSV" runat="server" Text="Export Csv" OnClick="btnExportCSV_Click" />
                        </span>
                    </li>
                </ol>     
            </fieldset>  
        </td>
    </tr>
</table>

<div class="breadcrumb">
    <asp:Label ID="lblSuscessMsg" runat="server" Text=""></asp:Label>
</div>


<asp:Repeater ID="rptFailedUser" runat="server">
    <HeaderTemplate>
        <div class="breadcrumb">
            <span style="font-weight: bold;">Failed:</span>
        </div>
        <table class="grid gridLight" cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th style="width: 20px; text-align: center;">
                    No.
                </th>
                <th>
                    Username
                </th>
                <th>
                    Reason
                </th>               
            </tr>
        </thead>
        <tbody>
    </HeaderTemplate>
    <ItemTemplate>
        <tr <%# Container.ItemIndex % 2 == 1 ? "class=\"alt\"" : "" %>>
            <td style="text-align: center;">
                <%# Container.ItemIndex + 1 %>
            </td>
            <td nowrap="nowrap">
                <%# Eval("UserName")%>
            </td>
            <td nowrap="nowrap">
                <%# Eval("Status")%>
            </td>
        </tr>
    </ItemTemplate>
    
    <FooterTemplate>
            </tbody>
        </table>
    </FooterTemplate>
</asp:Repeater>    
</div>
