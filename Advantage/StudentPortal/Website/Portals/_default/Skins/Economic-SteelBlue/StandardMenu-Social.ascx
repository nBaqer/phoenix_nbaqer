<%@ Control language="vb" CodeBehind="~/admin/Skins/skin.vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="CURRENTDATE" Src="~/Admin/Skins/CurrentDate.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="Meta" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TEXT" Src="~/Admin/Skins/Text.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
<%@ Register TagPrefix="dnn" TagName="CONTROLPANEL" Src="~/Admin/Skins/controlpanel.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<dnn:STYLES runat="server" ID="StylesIE8" Name="IE8Minus" StyleSheet="css/ie8style.css" Condition="IE 8" UseSkinPath="true" />
<dnn:Meta runat="server" Name="viewport" Content="width=device-width, minimum-scale=1.0, maximum-scale=2.0" />
<div id="ControlPanelWrapper">
  <dnn:CONTROLPANEL runat="server" id="cp" IsDockable="True" />
</div>
<div class="skin_wrapper economic_skin">
  <section class="skin_user">
    <div class="skin_width clearafter">
      <%If not Request.IsAuthenticated Then%>
      <div class="user_style">
        <dnn:LANGUAGE runat="server" id="dnnLANGUAGE"  showMenu="False" showLinks="True" />
        <dnn:USER runat="server" id="dnnUSER" cssclass="User" />
        <dnn:LOGIN runat="server" id="dnnLOGIN" cssclass="Login" />
      </div>
      <% End If%>
      <%If Request.IsAuthenticated Then%>
      <div class="user_style">
        <dnn:LANGUAGE runat="server" id="dnnLANGUAGE2"  showMenu="False" showLinks="True" />
        <div id="Login">
          <dnn:USER runat="server" id="dnnUSER2" LegacyMode="false" />
          <dnn:LOGIN runat="server" id="dnnLOGIN2" LegacyMode="false" />
        </div>
      </div>
      <% End If%>
    </div>
  </section>
  <section class="skin_body">
    <header class="skin_header">
      <div class="skin_width">
        <div class="header_style clearafter">
          <div class="logo_style">
            <div class="site_logo">
              <dnn:LOGO runat="server" id="dnnLOGO" />
            </div>
            <div class="mobile_nav"><a href="#" class="menuclick"><img alt="Menu" class="click_img" src="<%=SkinPath %>images/blank.gif" /></a></div>
          </div>
          <nav class="menu_style">
            <dnn:MENU MenuStyle="StandardMenu" runat="server"></dnn:MENU>
          </nav>
        </div>
      </div>
    </header>
    <!--end skin header-->
    <!--start page name-->
    <section class="page_name">
      <div class="skin_width">
        <div class="pagename_style clearafter">
          <h1><%=PortalSettings.ActiveTab.TabName %></h1>
          <div class="breadcrumb_style">
            <dnn:BREADCRUMB runat="server" id="dnnBREADCRUMB" Separator="&nbsp;&nbsp;/&nbsp;&nbsp;" cssclass="Breadcrumb" RootLevel="0" />
          </div>
        </div>
      </div>
    </section>
    <!--end page name-->
    <!--start skin main-->
    <section class="skin_main">
      <div class="skin_width">
        <div class="socialprofile_content">
          <div class="threeColSocial">
            <div id="leftPane" class="SocialLeftPane" runat="server"></div>
            <div id="centerPane" class="SocialCenterPane" runat="server"></div>
            <div id="rightPane" class="SocialRightPane" runat="server"></div>
            <div class="clear"></div>
          </div>
          <div id="ContentPane" runat="server"></div>
        </div>
      </div>
    </section>
    <!--end skin main-->
    <!--start skin footer-->
    <section class="footer_top">
      <div class="skin_width">
        <div class="footerpane_style skin_main_padding">
          <div class="row dnnpane">
            <div runat="server" id="FooterGrid4A" class="footer_grid4a col-sm-4"></div>
            <div runat="server" id="FooterGrid4B" class="footer_grid4b col-sm-4"></div>
            <div runat="server" id="FooterGrid4C" class="footer_grid4c col-sm-4"></div>
          </div>
          <div class="row dnnpane">
            <div runat="server" id="FooterPane" class="footerpane col-sm-12"></div>
          </div>
        </div>
      </div>
    </section>
    <!--end footer top-->
    <!--start footer-->
    <footer class="copyright_bg">
      <div class="skin_width">
        <div class="footer_bottom clearafter">
          <div class="copyright_style">
            <dnn:COPYRIGHT runat="server" id="dnnCOPYRIGHT" cssclass="Footer" />
          </div>
          <div class="footer_right">
            <dnn:PRIVACY runat="server" id="dnnPRIVACY" cssclass="Footer" />
            |
            <dnn:TERMS runat="server" id="dnnTERMS" cssclass="Footer" />
          </div>
        </div>
      </div>
    </footer>
  </section>
  <section class="footer_shadow">
    <div class="skin_width">
      <div class="footer_shadow_style"><img src="<%= SkinPath %>images/footer_shadow.png" /></div>
    </div>
  </section>
  <!--end skin footer-->
  <a href="#top" id="top-link" title="Top"> </a> </div>
<!--#include file="commonparts/StyleCustom.ascx"-->
<!--#include file="commonparts/AddScripts.ascx"-->
