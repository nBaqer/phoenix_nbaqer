/*
 * JS Settings For DotNetNuke Skin by bestdnnskins.com
 * Copyright 2014 By BESTDNNSKINS.COM
 */
//Window Phone Compatible:
(function() {
if ("-ms-user-select" in document.documentElement.style &&
(navigator.userAgent.match(/IEMobile/) ||
navigator.userAgent.match(/ZuneWP7/) ||
navigator.userAgent.match(/WPDesktop/))) {
var msViewportStyle = document.createElement("style");
msViewportStyle.appendChild(
document.createTextNode("@-ms-viewport{width:auto!important}")
);
document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
}
})();

//Retina:
jQuery(document).ready(function() {
	$('.retina').retinise();
});

//For Mobile Menu:
jQuery(document).ready(function(){
	  $(".menuclick").click(function(event) {
	    event.preventDefault();
	    $(".menu_style").slideToggle("fast");
			return false;
	  });
});

//For Fancy Lightbox:
jQuery(document).ready(function() {
	$(".fancybox").fancybox({
	    openEffect:'elastic',closeEffect:'fade',nextEffect:'fade', prevEffect:'fade'
	});
});

//For Trans-banner Slider:
jQuery(document).ready(function($) {		
		$('.TB_Wrapper').TransBanner({
			slide_delaytime: 6,
			slide_transition: 2,
			navigation_type: 3,
			button_size: 26,
			caption_bg_color: '#000',
			caption_bg_opacity: .2,
			caption_bg_blur: 10,
			responsive : true,
			responsive_limit_autoplay : '', 
			responsive_limit_caption : 480,
			responsive_limit_navigation : 480,
			responsive_limit_navigation_type : 2, 
			responsive_screen_based_limits : true 
		});
});

//For Slideshow Banner:
jQuery(window).load(function() {
	  $('.flexslider').flexslider({animation:"slide",slideshowSpeed: 6000, animationSpeed: 500, pauseOnHover: true, start: function(slider){} });
	  $('.flexslider2').flexslider({animation:"fade",slideshowSpeed: 6000, animationSpeed: 500, pauseOnHover: true, start: function(slider){} });
});

//For CarouFredSel Style:
jQuery(document).ready(function() {
	$("#carouFredSel").carouFredSel({
		responsive: true,
		width: "100%",
		prev: "#carousel_prev",
		next: "#carousel_next",
		mousewheel: true,
		scroll: {
			'items': 1,
			'duration': 1000
		},
		items: {
			width:240,
             height:260,
			//	height: '30%',	//	optionally resize item-height
			visible: {
				min: 1,
				max: 4
			}
		}
	});
});

//For CarouFredSel Style:
jQuery(document).ready(function() {
	$("#carouFredSel02").carouFredSel({
		responsive: true,
		width: "100%",
		height: 'auto',
		prev: "#caroul_prev02",
		next: "#caroul_next02",
		mousewheel: true,
		swipe: {
			onMouse: true,
			onTouch: true
		},
		scroll: {
			'items': 1,
			'duration': 800
		},
		items: {
			width:370,
			//	height: '30%',	//	optionally resize item-height
			visible: {
				min: 1,
				max: 3
			}
		}
	});
});

//For CarouFredSel2 Style:
jQuery(document).ready(function() {
  $("#carousel_up").carouFredSel({
	items : 2,
	direction	: "up",
	scroll: {
		'items': 1,
		'duration': 1000
		},
   });
});	

//For Quovolver Style:
jQuery(document).ready(function($) {
    $("#quovolver").quovolver({
        children: "li",
        transitionSpeed: 600,
        autoPlay: true,
        autoPlaySpeed: 5000,
        pauseOnHover: true,
        equalHeight: false,
        navPosition: "above",
        navNum: true
    })
});

//For Shake Style:
jQuery(document).ready(function() {
    $(".imgshake img").each(function(k,img){
	  new JumpObj(img,4);
     $(img).hover(function(){this.parentNode.parentNode.className="hover"});
    })
});


//For LayerSlider:
jQuery(document).ready(function() {
    $("#layerslider").layerSlider({
			responsive: false,
			responsiveUnder: 1280,
			layersContainer: 1280,
			skin: 'v5',
			skinsPath: '/Portals/_default/Skins/Economic-SteelBlue/css/layerslider/skins/',
			thumbnailNavigation : 'none',
			navButtons: false,
			navStartStop: false
		});
}); 

//For Unoslider Banner:
jQuery(window).load(function(){
		$('#slider').unoslider({
		width: 1600,
		height: 550,
        tooltip: true,
        indicator: { autohide: false },
        navigation: { autohide: true },
        slideshow: { hoverPause: true, continuous: true, timer: true, speed:8, infinite: true, autostart: true },
        responsive: true,
        responsiveLayers: false,
        preset: ['sq_flyoff', 'sq_drop', 'sq_squeeze', 'sq_random', 'sq_diagonal_rev', 'sq_diagonal', 'sq_fade_random', 'sq_fade_diagonal_rev', 'sq_fade_diagonal', 'explode', 'implode', 'fountain', 'shot_right', 'shot_left', 'zipper_right', 'zipper_left', 'bar_slide_random', 'bar_slide_bottomright', 'bar_slide_bottomright', 'bar_slide_topright', 'bar_slide_topleft'],
        order: 'random',
        block: {
            vertical: 10,
            horizontal: 4
        },
        animation: {
            speed: 500,
            delay: 50,
            transition: 'grow',
            variation: 'topleft',
            pattern: 'diagonal',
            direction: 'topleft'
        }
		});
}); 

//For Unoslider Banner2:
jQuery(window).load(function(){
	$('#slider2').unoslider({
		width: 1140,
		height: 350,
        tooltip: true,
        indicator: { autohide: false },
        navigation: { autohide: true },
        slideshow: { hoverPause: true, continuous: true, timer: true, speed: 9, infinite: true, autostart: true },
        responsive: true,
        responsiveLayers: false,
        preset: ['sq_flyoff', 'sq_drop', 'sq_squeeze', 'sq_random', 'sq_diagonal_rev', 'sq_diagonal', 'sq_fade_random', 'sq_fade_diagonal_rev', 'sq_fade_diagonal', 'explode', 'implode', 'fountain', 'shot_right', 'shot_left', 'zipper_right', 'zipper_left', 'bar_slide_random', 'bar_slide_bottomright', 'bar_slide_bottomright', 'bar_slide_topright', 'bar_slide_topleft'],
        order: 'random',
        block: {
            vertical: 10,
            horizontal: 4
        },
        animation: {
            speed: 500,
            delay: 50,
            transition: 'grow',
            variation: 'topleft',
            pattern: 'diagonal',
            direction: 'topleft'
        }
		});
}); 

//For Unoslider Banner3:
jQuery(window).load(function(){
	$('#slider3').unoslider({
		width: 850,
		height: 350,
        tooltip: true,
        indicator: { autohide: false },
        navigation: { autohide: true },
        slideshow: { hoverPause: true, continuous: true, timer: true, speed: 9, infinite: true, autostart: true },
        responsive: true,
        responsiveLayers: false,
        preset: ['sq_flyoff', 'sq_drop', 'sq_squeeze', 'sq_random', 'sq_diagonal_rev', 'sq_diagonal', 'sq_fade_random', 'sq_fade_diagonal_rev', 'sq_fade_diagonal', 'explode', 'implode', 'fountain', 'shot_right', 'shot_left', 'zipper_right', 'zipper_left', 'bar_slide_random', 'bar_slide_bottomright', 'bar_slide_bottomright', 'bar_slide_topright', 'bar_slide_topleft'],
        order: 'random',
        block: {
            vertical: 10,
            horizontal: 4
        },
        animation: {
            speed: 500,
            delay: 50,
            transition: 'grow',
            variation: 'topleft',
            pattern: 'diagonal',
            direction: 'topleft'
        }
		});
}); 

//For Accordion Style:
jQuery(document).ready(function() { 
    $( ".accordion2" ).accordion({  
            collapsible: true,
			autoHeight: false
    });  
}); 

 //For Isotope Style:
jQuery(document).ready(function() {
			  
			  var $container = $('#container');
		
			  $container.isotope({
				itemSelector : '.element'
			  });
			  
			  var $optionSets = $('#options .option-set'),
				  $optionLinks = $optionSets.find('a');
		
			  $optionLinks.click(function(){
				var $this = $(this);
				// don't proceed if already selected
				if ( $this.hasClass('selected') ) {
				  return false;
				}
				var $optionSet = $this.parents('.option-set');
				$optionSet.find('.selected').removeClass('selected');
				$this.addClass('selected');
		  
				// make option object dynamically, i.e. { filter: '.my-filter-class' }
				var options = {},
					key = $optionSet.attr('data-option-key'),
					value = $this.attr('data-option-value');
				// parse 'false' as false boolean
				value = value === 'false' ? false : value;
				options[ key ] = value;
				if ( key === 'layoutMode' && typeof changeLayoutMode === 'function' ) {
				  // changes in layout modes need extra logic
				  changeLayoutMode( $this, options )
				} else {
				  // otherwise, apply new options
				  $container.isotope( options );
				}
				
				return false;
			  });   
});

/*
	By Osvaldas Valutis, www.osvaldas.info
	Available for use under the MIT License
*/
;(function( $, window, document, undefined )
{
	$.fn.doubleTapToGo = function( params )
	{
		if( !( 'ontouchstart' in window ) &&
			!navigator.msMaxTouchPoints &&
			!navigator.userAgent.toLowerCase().match( /windows phone os 7/i ) ) return false;

		this.each( function()
		{
			var curItem = false;

			$( this ).on( 'click', function( e )
			{
				var item = $( this );
				if( item[ 0 ] != curItem[ 0 ] )
				{
					e.preventDefault();
					curItem = item;
				}
			});

			$( document ).on( 'click touchstart MSPointerDown', function( e )
			{
				var resetItem = true,
					parents	  = $( e.target ).parents();

				for( var i = 0; i < parents.length; i++ )
					if( parents[ i ] == curItem[ 0 ] )
						resetItem = false;

				if( resetItem )
					curItem = false;
			});
		});
		return this;
	};
})( jQuery, window, document );

/* For tablet double tap */	
jQuery(document).ready(function() {
	if ($(window).width() >= 768){
	   $( '#standardMenu .rootMenu li.haschild' ).doubleTapToGo();
	   $( '#megaMenu > .root > li.haschild' ).doubleTapToGo();
	 }
});