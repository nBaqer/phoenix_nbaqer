﻿/*
Name: 			Style Custom Tool
Written by: 	Bestdnnskins.com - (http://www.bestdnnskins.com)
Version: 		1.0
*/

var customTool = {
	initialized: false,
	initialize: function() {
		var $this = this;
		if (this.initialized) return;
		this.initialized = true;		
		$this.build();
	},

	build: function() {
		var $this = this;
		var switcher = $("<a />")
			.addClass("getcss_btn")
			.attr("href", "#getCSSModal")
			.html("Get CSS")			
		$("#customcss").append(switcher);

		var modalHTML = '<div id="getCSSModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="cssModalLabel" aria-hidden="true"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button><h4 id="cssModalLabel">Custom CSS</h4></div><div class="modal-body"><div class="alert alert-info fade in" id="noCustomInfo">There are no custom styles that can be found. Please customize styles at first.</div><div class="alert alert-info fade in" id="saveCustomInfo">If you would like to save the custom styles. Please copy the below codes, then go to Admin >> Site-Settings >> Stylesheet Editor tab, paste the codes in the box and save.</div><textarea id="getCSSText" class="get-css" readonly="readonly"></textarea></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Close</button></div></div></div></div>';
		$("body").append(modalHTML);
		
		$('head').append('<style id="customcolors" type="text/css"></style>');
		$('#custom-button').click(
			function () {
				if ($('#custom_wrapper').css('left') != '0px') {
					$('#custom_wrapper').animate({ "left": "0px" }, { duration: 300 });
					$(this).animate({ "left": "150px" }, { duration: 300 });
				}
				else {
					$('#custom_wrapper').animate({ "left": "-155px" }, { duration: 300 });
					$('#custom-button').animate({ "left": "0px" }, { duration: 300 });
				}
			}
		);
		
		// set custom color
		$('#bg_color').ColorPicker({
			onShow: function(colpkr) {
				$(colpkr).fadeIn("fast");
				return false
			},
			onHide: function(colpkr) {
				$(colpkr).fadeOut("fast");
				return false
			},
			onChange: function(hsb, hex, rgb) {
				var color = hex;
			    $('#bg_color').css({backgroundColor: '#' + color});
				$('style#customcolors').html('A:link{color:#' + color + ';}\nA:visited{color:#' + color + ';}\nA:active{color:#' + color + ';}\n.color_text{color:#' + color + ';}\n.color_bg{background-color:#' + color + ';}\nh1{color:#' + color + ';}\nh2{color:#' + color + ';}\nh3{color:#' + color + ';}\nh4{color:#' + color + ';}\nh5{color:#' + color + ';}\nh6{color:#' + color + ';}\nA.Login:hover{color:#' + color + ';}\nA.User:hover{color:#' + color + ';}\n\n.economic_skin .pre_defined_color{background-color:#' + color + ';}\n.economic_skin .page_name{background-color:#' + color + ';}\n\n.economic_skin #standardMenu > .rootMenu > li.selected > a span, .economic_skin #standardMenu > .rootMenu > li.breadcrumb > a span, .economic_skin #standardMenu > .rootMenu > li:hover > a span, .economic_skin #standardMenu > .rootMenu > li > a:hover span{color:#' + color + ';}\n.economic_skin #standardMenu .subMenu{border-color:#' + color + ';}\n.economic_skin #standardMenu .subMenu li.selected > a span,.economic_skin #standardMenu .subMenu li.breadcrumb > a span,.economic_skin #standardMenu .subMenu li.item a:hover span{color:#' + color + ';}\n.economic_skin #megaMenu > .root > li.selected > a span,.economic_skin #megaMenu > .root > li.breadcrumb > a span, .economic_skin #megaMenu > .root > li:hover > a span, .economic_skin #megaMenu > .root > li > a:hover span {color:#' + color + ';}\n.economic_skin #megaMenu .category{border-color:#' + color + ';}\n.economic_skin #megaMenu .category li.selected > a span,.economic_skin #megaMenu .category li.item a:hover span{color:#' + color + ';}\n.economic_skin #leftMenu .leftSub li.selected > a span,.economic_skin #leftMenu .leftSub li a:hover span{background-color:#' + color + ';}\n.economic_skin #leftMenu .level1 li.selected > a span,.economic_skin #leftMenu .level1 li a:hover span{color:#' + color + ';}\n.economic_skin #leftMenu .level1 li a:hover span{color:#' + color + ';}\n\n.economic_skin #Login .registerGroup a:hover,.economic_skin #Login .loginGroup a:hover{color:#' + color + ';}\n.economic_skin .nav-tabs > li.active > a,.economic_skin .nav-tabs > li.active > a:hover,.economic_skin .nav-tabs > li.active > a:focus{color:#' + color + ';}\n.economic_skin .nav-tabs > li > a:hover{color:#' + color + ';}\n.economic_skin .tab_container_right:hover h3{color:#' + color + ';}\n.economic_skin .quovolve-box ul.quovolve{background-color:#' + color + ';}\n.economic_skin .quovolve-box ul li cite:before{border-left-color:#' + color + ';}\n.economic_skin .quovolve-box ul li cite span{color:#' + color + ';}\n.economic_skin .quovolve-box .quovolve-nav .nav-numbers li.active a{background-color:#' + color + ';}\n.economic_skin .quovolve-box .quovolve-nav .nav-numbers li a:hover{background-color:#' + color + ';}\n.economic_skin .icon_hover .icon-effect-1 .glyphicon{background-color:#' + color + ';}\n.economic_skin .icon_hover .icon-effect-2 .glyphicon{background-color:#' + color + ';}\n.economic_skin .icon_hover .icon-effect-3 .glyphicon:after{background-color:#' + color + ';}\n.economic_skin .icon_hover .icon-effect-4 .glyphicon{background-color:#' + color + ';}\n.economic_skin .icon_hover .icon-effect-5 .glyphicon{background-color:#' + color + ';}\n.economic_skin .icon-effect-6 .glyphicon{background-color:#' + color + ';}\n.economic_skin .icon_effect:hover .icon-effect-5 .glyphicon{background-color:#' + color + ';}\n.economic_skin .carouFredSel ul li:hover{border-color:#' + color + ';}\n.economic_skin a#carousel_prev{background-color:#' + color + ';}\n.economic_skin a#carousel_next{background-color:#' + color + ' ;}\n.economic_skin .carouFredSel ul li:hover h3.carousel_subtitle{color:#' + color + ';}\n.economic_skin .caroul_prev02{background-color:#' + color + ';}\n.economic_skin .caroul_next02{background-color:#' + color + ';}\n.economic_skin .carousel_up ul li .carousel_up_text a{color:#' + color + ';}\n.economic_skin .portfolio-list li > .portfolio-overlay{background-color:#' + color + ';}\n.economic_skin .content_colorbg{background-color:#' + color + ';}\n.economic_skin .top_icons a{background-color:#' + color + ';}\n.economic_skin .main_top:hover h3{color:#' + color + ';}\n.economic_skin .main_top .readmore a{color:#' + color + ';}\n.economic_skin .post_right p a:hover{color:#' + color + ';}\n.economic_skin .home_list li:hover a, .page_list li:hover a span{color:#' + color + ';}\n.economic_skin .logo_list h3:hover{color:#' + color + ';}\n.economic_skin .dynamic_bar .progress-bar-success3{background-color:#' + color + ';}\n.economic_skin .our_services:hover h3{color:#' + color + ';}\n.economic_skin .home_team:hover h3{color:#' + color + ';}\n.economic_skin .testimonials_style{border-left-color:#' + color + ';}\n.economic_skin .testimonials_style .testimonials_right span{color:#' + color + ';}\n.economic_skin .features_list span{color:#' + color + '; border-color:#' + color + ';}\n.economic_skin .features_list:hover .features_list_right h3{color:#' + color + ';}\n.economic_skin .service_list:hover{border-color:#' + color + ';}\n.economic_skin .service_list .glyphicon{color:#' + color + ';}\n.economic_skin .service_list:hover h3{color:#' + color + ';}\n.economic_skin .price-table .head{background-color:#' + color + ';}\n.economic_skin .price_bottom:hover{background-color:#' + color + ';}\n.economic_skin .price_bottom_success{background-color:#' + color + ';}\n.economic_skin .sidebar_tag ul li a:hover{background-color:#' + color + ';border-color:#' + color + ';}\n.economic_skin .categories_style li a:hover{color:#' + color + ';}\n.economic_skin .error_page .sorry_title h3:hover{color:#' + color + ';}\n.economic_skin .contact_number .btn-primary{background-color:#' + color + ';}\n.economic_skin .testimonials_page{border-left-color:#' + color + ';}\n.economic_skin .page_number_style .pagination > .active > a,.economic_skin .pagination > .active > span, .pagination > .active > a:hover,.economic_skin .pagination > .active > span:hover,.economic_skin .pagination > .active > a:focus,.economic_skin .pagination > .active > span:focus{background-color:#' + color + ';border-color:#' + color + ';}\n.economic_skin .sidebar_content p a{color:#' + color + ';}\n.economic_skin .sidebar_content .sidebar_blockquote{border-left-color:#' + color + ';}\n.economic_skin .sidebar_content_right p a{color:#' + color + ';}\n.economic_skin .sidebar_content_right .sidebar_blockquote{border-left-color:#' + color + ';}\n.economic_skin .full_width p a{color:#' + color + ';}\n.economic_skin .full_width .sidebar_blockquote{border-left-color:#' + color + ';}\n.economic_skin .banner_readmore .readmore_button{background-color:#' + color + ';}\n.economic_skin .isotope_img img:hover{color:#' + color + ';}\n.economic_skin #options a:hover{color:#' + color + ';}\n.economic_skin .unoslider_indicator a.unoslider_indicator_active{background-color:#' + color + ';}\n\n.economic_skin .c_title_SpringGreen{color:#' + color + ';}\n.economic_skin .BlackTitle_SpringGreen:hover .c_title_black{color:#' + color + ';}\n.economic_skin .BlackTitle02_SpringGreen:hover .c_title_black{color:#' + color + ';}\n.economic_skin .BlackTitle03_SpringGreen:hover .c_title_black{color:#' + color + ';}\n.economic_skin .BlackTitle03_SpringGreen:hover .c_title{border-color:#' + color + '; color:#' + color + ';}\n.economic_skin .BlackTitle04_SpringGreen:hover .c_title_black{color:#' + color + ';}\n.economic_skin .SpringGreen02_top_bg{border-color:#' + color + ';}\n.economic_skin .SpringGreen03_top_bg{background-color:#' + color + ';}\n.economic_skin .SpringGreen04_style:hover .SpringGreen04_top_bg{background-color:#' + color + ';}\n.economic_skin .SpringGreen05_top_bg{border-color:#' + color + ';}\n.economic_skin .SpringGreen06_top_bg{border-color:#' + color + ';}\n.economic_skin .SpringGreen07_style{border-color:#' + color + ';}\n.economic_skin .SpringGreen07_top_bg{background-color: #' + color + ';}\n.economic_skin .SpringGreen08_top_bg{background-color: #' + color + ';}\n\n.economic_skin .icon_hover .icon-effect-1 .glyphicon:after{box-shadow: 0 0 0 2px #' + color + ';}\n.economic_skin .icon_hover .icon-effect-2 .glyphicon:after{box-shadow: 0 0 0 2px #' + color + ';}\n.economic_skin .icon_hover .icon-effect-3 .glyphicon{box-shadow: 0 0 0 2px #' + color + ';}\n\n.economic_skin .threeColSocial .console-mouseon{background-color:#' + color + ';}\n.economic_skin .threeColSocial a.dnnPrimaryAction{background-color:#' + color + ';}\n.economic_skin .threeColSocial .selectDrop.active > a{background-color:#' + color + ';}\n.economic_skin .threeColSocial .dnnButtonGroup > li > a.active{border-color:#' + color + ';background-color:#' + color + ';}\n.economic_skin .threeColSocial .dnnButtonGroup > li > a.disabled:hover{border-color:#' + color + ';background-color:#' + color + ';}\n.economic_skin .threeColSocial .alpha > a:hover{background-color:#' + color + ';border-color:#' + color + ';}\n.economic_skin .threeColSocial .alpha > a,.economic_skin .threeColSocial .alpha > a:hover,.economic_skin .threeColSocial .alpha > a.active{border-color:#' + color + ';}\n.economic_skin .threeColSocial a.dnnSecondaryAction.ArchiveItems:hover{background-color:#' + color + ';}\n.economic_skin .threeColSocial a.dnnSecondaryAction.ArchiveItems:hover{background-color:#' + color + ';}\n.economic_skin .threeColSocial .dnnTertiaryAction:hover,.economic_skin a.dnnTertiaryAction:hover{background-color:#' + color + ';}\n\n@media handheld,only screen and (max-width:767px){\n.economic_skin #standardMenu > .rootMenu > li.selected > a, .economic_skin #standardMenu > .rootMenu > li.breadcrumb > a, .economic_skin #standardMenu > .rootMenu > li:hover > a, .economic_skin #standardMenu > .rootMenu > li > a:hover{background-color:#' + color + ';}\n.economic_skin #megaMenu > .root > li.selected > a,.economic_skin #megaMenu > .root > li.breadcrumb > a,.economic_skin #megaMenu > .root > li:hover > a,.economic_skin #megaMenu > .root > li > a:hover{background-color:#' + color + ';}\n}');									
				$.cookie("custom_color", color);
			},
			color: '#64A6A5'
		});
				
		var color = $.cookie("custom_color");
		if (color) {
		   $('#bg_color').css({backgroundColor: '#' + color});
		   $('style#customcolors').html('A:link{color:#' + color + ';}\nA:visited{color:#' + color + ';}\nA:active{color:#' + color + ';}\n.color_text{color:#' + color + ';}\n.color_bg{background-color:#' + color + ';}\nh1{color:#' + color + ';}\nh2{color:#' + color + ';}\nh3{color:#' + color + ';}\nh4{color:#' + color + ';}\nh5{color:#' + color + ';}\nh6{color:#' + color + ';}\nA.Login:hover{color:#' + color + ';}\nA.User:hover{color:#' + color + ';}\n\n.economic_skin .pre_defined_color{background-color:#' + color + ';}\n.economic_skin .page_name{background-color:#' + color + ';}\n\n.economic_skin #standardMenu > .rootMenu > li.selected > a span, .economic_skin #standardMenu > .rootMenu > li.breadcrumb > a span, .economic_skin #standardMenu > .rootMenu > li:hover > a span, .economic_skin #standardMenu > .rootMenu > li > a:hover span{color:#' + color + ';}\n.economic_skin #standardMenu .subMenu{border-color:#' + color + ';}\n.economic_skin #standardMenu .subMenu li.selected > a span,.economic_skin #standardMenu .subMenu li.breadcrumb > a span,.economic_skin #standardMenu .subMenu li.item a:hover span{color:#' + color + ';}\n.economic_skin #megaMenu > .root > li.selected > a span,.economic_skin #megaMenu > .root > li.breadcrumb > a span, .economic_skin #megaMenu > .root > li:hover > a span, .economic_skin #megaMenu > .root > li > a:hover span {color:#' + color + ';}\n.economic_skin #megaMenu .category{border-color:#' + color + ';}\n.economic_skin #megaMenu .category li.selected > a span,.economic_skin #megaMenu .category li.item a:hover span{color:#' + color + ';}\n.economic_skin #leftMenu .leftSub li.selected > a span,.economic_skin #leftMenu .leftSub li a:hover span{background-color:#' + color + ';}\n.economic_skin #leftMenu .level1 li.selected > a span,.economic_skin #leftMenu .level1 li a:hover span{color:#' + color + ';}\n.economic_skin #leftMenu .level1 li a:hover span{color:#' + color + ';}\n\n.economic_skin #Login .registerGroup a:hover,.economic_skin #Login .loginGroup a:hover{color:#' + color + ';}\n.economic_skin .nav-tabs > li.active > a,.economic_skin .nav-tabs > li.active > a:hover,.economic_skin .nav-tabs > li.active > a:focus{color:#' + color + ';}\n.economic_skin .nav-tabs > li > a:hover{color:#' + color + ';}\n.economic_skin .tab_container_right:hover h3{color:#' + color + ';}\n.economic_skin .quovolve-box ul.quovolve{background-color:#' + color + ';}\n.economic_skin .quovolve-box ul li cite:before{border-left-color:#' + color + ';}\n.economic_skin .quovolve-box ul li cite span{color:#' + color + ';}\n.economic_skin .quovolve-box .quovolve-nav .nav-numbers li.active a{background-color:#' + color + ';}\n.economic_skin .quovolve-box .quovolve-nav .nav-numbers li a:hover{background-color:#' + color + ';}\n.economic_skin .icon_hover .icon-effect-1 .glyphicon{background-color:#' + color + ';}\n.economic_skin .icon_hover .icon-effect-2 .glyphicon{background-color:#' + color + ';}\n.economic_skin .icon_hover .icon-effect-3 .glyphicon:after{background-color:#' + color + ';}\n.economic_skin .icon_hover .icon-effect-4 .glyphicon{background-color:#' + color + ';}\n.economic_skin .icon_hover .icon-effect-5 .glyphicon{background-color:#' + color + ';}\n.economic_skin .icon-effect-6 .glyphicon{background-color:#' + color + ';}\n.economic_skin .icon_effect:hover .icon-effect-5 .glyphicon{background-color:#' + color + ';}\n.economic_skin .carouFredSel ul li:hover{border-color:#' + color + ';}\n.economic_skin a#carousel_prev{background-color:#' + color + ';}\n.economic_skin a#carousel_next{background-color:#' + color + ' ;}\n.economic_skin .carouFredSel ul li:hover h3.carousel_subtitle{color:#' + color + ';}\n.economic_skin .caroul_prev02{background-color:#' + color + ';}\n.economic_skin .caroul_next02{background-color:#' + color + ';}\n.economic_skin .carousel_up ul li .carousel_up_text a{color:#' + color + ';}\n.economic_skin .portfolio-list li > .portfolio-overlay{background-color:#' + color + ';}\n.economic_skin .content_colorbg{background-color:#' + color + ';}\n.economic_skin .top_icons a{background-color:#' + color + ';}\n.economic_skin .main_top:hover h3{color:#' + color + ';}\n.economic_skin .main_top .readmore a{color:#' + color + ';}\n.economic_skin .post_right p a:hover{color:#' + color + ';}\n.economic_skin .home_list li:hover a, .page_list li:hover a span{color:#' + color + ';}\n.economic_skin .logo_list h3:hover{color:#' + color + ';}\n.economic_skin .dynamic_bar .progress-bar-success3{background-color:#' + color + ';}\n.economic_skin .our_services:hover h3{color:#' + color + ';}\n.economic_skin .home_team:hover h3{color:#' + color + ';}\n.economic_skin .testimonials_style{border-left-color:#' + color + ';}\n.economic_skin .testimonials_style .testimonials_right span{color:#' + color + ';}\n.economic_skin .features_list span{color:#' + color + '; border-color:#' + color + ';}\n.economic_skin .features_list:hover .features_list_right h3{color:#' + color + ';}\n.economic_skin .service_list:hover{border-color:#' + color + ';}\n.economic_skin .service_list .glyphicon{color:#' + color + ';}\n.economic_skin .service_list:hover h3{color:#' + color + ';}\n.economic_skin .price-table .head{background-color:#' + color + ';}\n.economic_skin .price_bottom:hover{background-color:#' + color + ';}\n.economic_skin .price_bottom_success{background-color:#' + color + ';}\n.economic_skin .sidebar_tag ul li a:hover{background-color:#' + color + ';border-color:#' + color + ';}\n.economic_skin .categories_style li a:hover{color:#' + color + ';}\n.economic_skin .error_page .sorry_title h3:hover{color:#' + color + ';}\n.economic_skin .contact_number .btn-primary{background-color:#' + color + ';}\n.economic_skin .testimonials_page{border-left-color:#' + color + ';}\n.economic_skin .page_number_style .pagination > .active > a,.economic_skin .pagination > .active > span, .pagination > .active > a:hover,.economic_skin .pagination > .active > span:hover,.economic_skin .pagination > .active > a:focus,.economic_skin .pagination > .active > span:focus{background-color:#' + color + ';border-color:#' + color + ';}\n.economic_skin .sidebar_content p a{color:#' + color + ';}\n.economic_skin .sidebar_content .sidebar_blockquote{border-left-color:#' + color + ';}\n.economic_skin .sidebar_content_right p a{color:#' + color + ';}\n.economic_skin .sidebar_content_right .sidebar_blockquote{border-left-color:#' + color + ';}\n.economic_skin .full_width p a{color:#' + color + ';}\n.economic_skin .full_width .sidebar_blockquote{border-left-color:#' + color + ';}\n.economic_skin .banner_readmore .readmore_button{background-color:#' + color + ';}\n.economic_skin .isotope_img img:hover{color:#' + color + ';}\n.economic_skin #options a:hover{color:#' + color + ';}\n.economic_skin .unoslider_indicator a.unoslider_indicator_active{background-color:#' + color + ';}\n\n.economic_skin .c_title_SpringGreen{color:#' + color + ';}\n.economic_skin .BlackTitle_SpringGreen:hover .c_title_black{color:#' + color + ';}\n.economic_skin .BlackTitle02_SpringGreen:hover .c_title_black{color:#' + color + ';}\n.economic_skin .BlackTitle03_SpringGreen:hover .c_title_black{color:#' + color + ';}\n.economic_skin .BlackTitle03_SpringGreen:hover .c_title{border-color:#' + color + '; color:#' + color + ';}\n.economic_skin .BlackTitle04_SpringGreen:hover .c_title_black{color:#' + color + ';}\n.economic_skin .SpringGreen02_top_bg{border-color:#' + color + ';}\n.economic_skin .SpringGreen03_top_bg{background-color:#' + color + ';}\n.economic_skin .SpringGreen04_style:hover .SpringGreen04_top_bg{background-color:#' + color + ';}\n.economic_skin .SpringGreen05_top_bg{border-color:#' + color + ';}\n.economic_skin .SpringGreen06_top_bg{border-color:#' + color + ';}\n.economic_skin .SpringGreen07_style{border-color:#' + color + ';}\n.economic_skin .SpringGreen07_top_bg{background-color: #' + color + ';}\n.economic_skin .SpringGreen08_top_bg{background-color: #' + color + ';}\n\n.economic_skin .icon_hover .icon-effect-1 .glyphicon:after{box-shadow: 0 0 0 2px #' + color + ';}\n.economic_skin .icon_hover .icon-effect-2 .glyphicon:after{box-shadow: 0 0 0 2px #' + color + ';}\n.economic_skin .icon_hover .icon-effect-3 .glyphicon{box-shadow: 0 0 0 2px #' + color + ';}\n\n.economic_skin .threeColSocial .console-mouseon{background-color:#' + color + ';}\n.economic_skin .threeColSocial a.dnnPrimaryAction{background-color:#' + color + ';}\n.economic_skin .threeColSocial .selectDrop.active > a{background-color:#' + color + ';}\n.economic_skin .threeColSocial .dnnButtonGroup > li > a.active{border-color:#' + color + ';background-color:#' + color + ';}\n.economic_skin .threeColSocial .dnnButtonGroup > li > a.disabled:hover{border-color:#' + color + ';background-color:#' + color + ';}\n.economic_skin .threeColSocial .alpha > a:hover{background-color:#' + color + ';border-color:#' + color + ';}\n.economic_skin .threeColSocial .alpha > a,.economic_skin .threeColSocial .alpha > a:hover,.economic_skin .threeColSocial .alpha > a.active{border-color:#' + color + ';}\n.economic_skin .threeColSocial a.dnnSecondaryAction.ArchiveItems:hover{background-color:#' + color + ';}\n.economic_skin .threeColSocial a.dnnSecondaryAction.ArchiveItems:hover{background-color:#' + color + ';}\n.economic_skin .threeColSocial .dnnTertiaryAction:hover,.economic_skin a.dnnTertiaryAction:hover{background-color:#' + color + ';}\n\n@media handheld,only screen and (max-width:767px){\n.economic_skin #standardMenu > .rootMenu > li.selected > a, .economic_skin #standardMenu > .rootMenu > li.breadcrumb > a, .economic_skin #standardMenu > .rootMenu > li:hover > a, .economic_skin #standardMenu > .rootMenu > li > a:hover{background-color:#' + color + ';}\n.economic_skin #megaMenu > .root > li.selected > a,.economic_skin #megaMenu > .root > li.breadcrumb > a,.economic_skin #megaMenu > .root > li:hover > a,.economic_skin #megaMenu > .root > li > a:hover{background-color:#' + color + ';}\n}');	
		}
		
		// set pattern
		$('#bg_pattern a.pattern-box').each(function(i) {
			var a = $(this);
			var patternUrl = 'url(/Portals/_default/Skins/Economic-SteelBlue/images/bg_pattern/' + a.attr('data-pattern') + '.png)';
			a.css({
				backgroundImage: patternUrl
			})
		});
		$('#bg_pattern a.pattern-box').click(function(e) {
			e.preventDefault();
			var patternUrl = 'url(/Portals/_default/Skins/Economic-SteelBlue/images/bg_pattern/' + $(this).attr('data-pattern') + '.png)';
			$('#Body').css({
				backgroundImage: patternUrl,
				backgroundRepeat: "repeat"
			});
			$.cookie("custom_pattern", patternUrl)
		});
		$('#bg_pattern a.pattern-box2').each(function(i) {
			var a = $(this);
			var patternUrl = 'url(/Portals/_default/Skins/Economic-SteelBlue/images/bg_pattern/' + a.attr('data-pattern') + '.jpg)';
			a.css({
				backgroundImage: patternUrl,
				backgroundSize: "auto 100%"
			})
		});
		$('#bg_pattern a.pattern-box2').click(function(e) {
			e.preventDefault();
			var patternUrl = 'url(/Portals/_default/Skins/Economic-SteelBlue/images/bg_pattern/' + $(this).attr('data-pattern') + '.jpg)';
			$('#Body').css({
				backgroundImage: patternUrl,
				backgroundRepeat: "repeat"
			});
			$.cookie("custom_pattern", patternUrl)
		});
		var pattern = $.cookie("custom_pattern");
		if (pattern) {
			$('#Body').css({
				backgroundImage: pattern,
				backgroundRepeat: "repeat"
			})
		}
			
		// set layout styles
		$('#boxed_button').click(function (e) { 
			e.preventDefault();
			location.reload();
			var boxedWidth = "1140";
			setTimeout(function(){
		       $(".skin_wrapper").css("width","1140px");
			   $(".skin_user").css("background","none");
			   $(".skin_body").css("box-shadow","0px 0px 3px 0px rgba(0,0,0,.1);");
			   $(".footer_shadow").css("display","block");
			   $(".user_style").css("padding","5px 0");
			   $(".logo_style").css("margin-top","0");
		    },1000);
		  	$.cookie("boxed", boxedWidth);
			$.cookie("stretched", null);
		});
		var boxed = $.cookie("boxed");
		if (boxed) {
			$(".skin_wrapper").css({"width":"1140px"});	
			$(".skin_user").css("background","none");
			$(".skin_body").css("box-shadow","0px 0px 3px 0px rgba(0,0,0,.1);");
			$(".footer_shadow").css("display","block");
			$(".user_style").css("padding","5px 0");
			$(".logo_style").css("margin-top","0");
		}
		$('#stretched_button').click(function (e) {
			e.preventDefault();
			location.reload();
			var stretchedWidth = "100%";
			setTimeout(function(){
		       $(".skin_wrapper").css("width","100%");
			   $(".skin_user").css("background","#fff");
			   $(".skin_body").css("box-shadow","none");
			   $(".footer_shadow").css("display","none");
			   $(".user_style").css("padding","5px 0 0");
			   $(".logo_style").css("margin-top","-20px");
		    },1000);
			$.cookie("boxed", null);
			$.cookie("stretched", stretchedWidth);
		});
		var stretched =  $.cookie("stretched");
		if (stretched) {
			$(".skin_wrapper").css({"width":"100%"});
			$(".skin_user").css("background","#fff");
			$(".skin_body").css("box-shadow","none");
			$(".footer_shadow").css("display","none");
			$(".user_style").css("padding","5px 0 0");
			$(".logo_style").css("margin-top","-20px");
		}

		// mega columns
		$('#one_columns').click(function (e) {
			e.preventDefault();
			var megaOnecol = "200";
			$("#megaMenu .category").css("width","200px");
			$.cookie("twocolumn", null);
			$.cookie("threecolumn", null);
			$.cookie("onecolumn", megaOnecol);
		});
		var onecolumn =  $.cookie("onecolumn");
		if (onecolumn) {
			$("#megaMenu .category").css({"width":"200px"});
		}
		$('#two_columns').click(function (e) { 
			e.preventDefault();
			var megaTwocol = "380";
		    $("#megaMenu .category").css("width","380px");
		  	$.cookie("threecolumn", null);
			$.cookie("onecolumn", null);
			$.cookie("twocolumn", megaTwocol);
		});
		var twocolumn = $.cookie("twocolumn");
		if (twocolumn) {
			$("#megaMenu .category").css({"width":"380px"});	
		}
		$('#three_columns').click(function (e) {
			e.preventDefault();		
			var megaThreecol = "560";
			$("#megaMenu .category").css("width","560px");
			$.cookie("twocolumn", null);
			$.cookie("onecolumn", null);
			$.cookie("threecolumn", megaThreecol);
		});
		var threecolumn =  $.cookie("threecolumn");
		if (threecolumn) {
			$("#megaMenu .category").css({"width":"560px"});
		}
		
		// reset
		$('#custom-reset').click(function(event) {
			event.preventDefault();
			$.cookie("custom_pattern", null);
			$.cookie("custom_color", null);
			$.cookie("boxed", null);
			$.cookie("stretched", null);
			$.cookie("onecolumn", null);
			$.cookie("twocolumn", null);
			$.cookie("threecolumn", null);
			location.reload();
		});


		// Get CSS
		$("a.getcss_btn").click(function(e) {
			e.preventDefault();
			$this.getCss();
		});

	},

	getCss: function() {
		var customStyle = "";
		var customLayout = "";
		var customBg = "";
		var megacolumn = "";
		
		var stretched = $.cookie("stretched");
		if(stretched) {
			customLayout = '.economic_skin.skin_wrapper { width: 100%; }\n.economic_skin .skin_user { background:#fff; }\n.economic_skin .skin_body { box-shadow:none; }\n.economic_skin .footer_shadow { display:none; }\n.economic_skin .user_style { padding::5px 0 0;}\n.economic_skin .logo_style { margin-top:-20px;}\n\n';
		}

		var pattern = $.cookie("custom_pattern");
		if(pattern) {
			customBg = '#Body { background-image: ' + $.cookie("custom_pattern") + ';}\n\n';
		}
				
		var onecolumn =  $.cookie("onecolumn");
		if(onecolumn) {
			megacolumn = '.economic_skin #megaMenu .category { width: 200px;}\n\n';
		}
		
		var threecolumn =  $.cookie("threecolumn");
		if(threecolumn) {
			megacolumn = '.economic_skin #megaMenu .category { width: 560px;}\n\n';
		}

		$("#getCSSText").text("");
		$("#getCSSText").text($("#customcolors").text());
		$("#getCSSModal").modal("show");

		customStyle = customBg + customLayout + megacolumn + $("#getCSSText").text();
		$("#getCSSText").text(customStyle);
		
		if($("#getCSSText").text() == "" ) {
			$("#noCustomInfo").show();
			$("#saveCustomInfo").hide();
		} else {
			$("#noCustomInfo").hide();
			$("#saveCustomInfo").show();
		}
	}
};

customTool.initialize();
