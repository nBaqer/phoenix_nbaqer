<%@ Control language="vb" CodeBehind="~/admin/Skins/skin.vb" AutoEventWireup="false" Explicit="True" Inherits="DotNetNuke.UI.Skins.Skin" %>
<%@ Register TagPrefix="dnn" TagName="CURRENTDATE" Src="~/Admin/Skins/CurrentDate.ascx" %>
<%@ Register TagPrefix="dnn" TagName="STYLES" Src="~/Admin/Skins/Styles.ascx" %>
<%@ Register TagPrefix="dnn" TagName="Meta" Src="~/Admin/Skins/Meta.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LANGUAGE" Src="~/Admin/Skins/Language.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGO" Src="~/Admin/Skins/Logo.ascx" %>
<%@ Register TagPrefix="dnn" TagName="MENU" src="~/DesktopModules/DDRMenu/Menu.ascx" %>
<%@ Register TagPrefix="dnn" TagName="SEARCH" Src="~/Admin/Skins/Search.ascx" %>
<%@ Register TagPrefix="dnn" TagName="USER" Src="~/Admin/Skins/User.ascx" %>
<%@ Register TagPrefix="dnn" TagName="LOGIN" Src="~/Admin/Skins/Login.ascx" %>
<%@ Register TagPrefix="dnn" TagName="COPYRIGHT" Src="~/Admin/Skins/Copyright.ascx" %>
<%@ Register TagPrefix="dnn" TagName="PRIVACY" Src="~/Admin/Skins/Privacy.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TERMS" Src="~/Admin/Skins/Terms.ascx" %>
<%@ Register TagPrefix="dnn" TagName="TEXT" Src="~/Admin/Skins/Text.ascx" %>
<%@ Register TagPrefix="dnn" TagName="BREADCRUMB" Src="~/Admin/Skins/BreadCrumb.ascx" %>
<%@ Register TagPrefix="dnn" TagName="CONTROLPANEL" Src="~/Admin/Skins/controlpanel.ascx" %>
<%@ Register TagPrefix="dnn" Namespace="DotNetNuke.Web.Client.ClientResourceManagement" Assembly="DotNetNuke.Web.Client" %>
<!--[if lt IE 9]>
<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->

<dnn:STYLES runat="server" ID="StylesIE8" Name="IE8Minus" StyleSheet="css/ie8style.css" Condition="IE 8" UseSkinPath="true" />
<dnn:Meta runat="server" Name="viewport" Content="width=device-width, minimum-scale=1.0, maximum-scale=2.0" />
<div id="ControlPanelWrapper">
  <dnn:CONTROLPANEL runat="server" id="cp" IsDockable="True" />
</div>
<div class="skin_wrapper economic_skin">
  <section class="skin_user">
    <div class="skin_width clearafter">
      <%If not Request.IsAuthenticated Then%>
      <div class="user_style">
        <dnn:LANGUAGE runat="server" id="dnnLANGUAGE"  showMenu="False" showLinks="True" />
        <dnn:USER runat="server" id="dnnUSER" cssclass="User" />
        <dnn:LOGIN runat="server" id="dnnLOGIN" cssclass="Login" />
      </div>
      <% End If%>
      <%If Request.IsAuthenticated Then%>
      <div class="user_style">
        <dnn:LANGUAGE runat="server" id="dnnLANGUAGE2"  showMenu="False" showLinks="True" />
        <div id="Login">
          <dnn:USER runat="server" id="dnnUSER2" LegacyMode="false" />
          <dnn:LOGIN runat="server" id="dnnLOGIN2" LegacyMode="false" />
        </div>
      </div>
      <% End If%>
    </div>
  </section>
  <section class="skin_body">
    <header class="skin_header">
      <div class="skin_width">
        <div class="header_style clearafter">
          <div class="logo_style">
            <div class="site_logo">
              <dnn:LOGO runat="server" id="dnnLOGO" />
            </div>
            <div class="mobile_nav"><a href="#" class="menuclick"><img alt="Menu" class="click_img" src="<%=SkinPath %>images/blank.gif" /></a></div>
          </div>
          <nav class="menu_style">
            <dnn:MENU MenuStyle="StandardMenu" runat="server"></dnn:MENU>
          </nav>
        </div>
      </div>
    </header>
    <!--end skin header-->
    <!--start skin banner-->
    <section class="skin_banner">
      <div class="banner_style">
        <div runat="server" id="BannerPane" class="bannerpane"></div>
      </div>
    </section>
    <!--end skin banner-->
    <!--start skin main-->
    <div class="skin_main">
      <section class="content_colorbg">
        <div class="skin_width">
          <div class="skin_main_padding">
            <div class="row dnnpane">
              <div runat="server" id="ColorGrid12" class="color_grid12 col-sm-12"></div>
            </div>
          </div>
        </div>
      </section>
      <section class="content_whitebg">
        <div class="skin_width">
          <div class="skin_content">
            <div class="row dnnpane">
              <div runat="server" id="ContentPane" class="content_grid12 col-sm-12"></div>
            </div>
            <div class="row dnnpane">
              <div runat="server" id="OneGrid4" class="one_grid4 col-sm-4"></div>
              <div runat="server" id="OneGrid8" class="one_grid8 col-sm-8"></div>
            </div>
            <div class="row dnnpane">
              <div runat="server" id="TwoGrid6A" class="two_grid6a col-sm-6"></div>
              <div runat="server" id="TwoGrid6B" class="two_grid6b col-sm-6"></div>
            </div>
            <div class="row dnnpane">
              <div runat="server" id="ThreeGrid4A" class="three_grid4a col-sm-4"></div>
              <div runat="server" id="ThreeGrid4B" class="three_grid4b col-sm-4"></div>
              <div runat="server" id="ThreeGrid4C" class="three_grid4c col-sm-4"></div>
            </div>
            <div class="row dnnpane">
              <div runat="server" id="FourGrid3A" class="four_grid3a col-sm-3"></div>
              <div runat="server" id="FourGrid3B" class="four_grid3b col-sm-3"></div>
              <div runat="server" id="FourGrid3C" class="four_grid3c col-sm-3"></div>
              <div runat="server" id="FourGrid3D" class="four_grid3d col-sm-3"></div>
            </div>
            <div class="row dnnpane">
              <div runat="server" id="FiveGrid12" class="five_grid12 col-sm-12"></div>
            </div>
            <div class="row dnnpane">
              <div runat="server" id="SixGrid8" class="six_grid8 col-sm-8"></div>
              <div runat="server" id="SixGrid4" class="six_grid4 col-sm-4"></div>
            </div>
            <div class="row dnnpane">
              <div runat="server" id="SevenGrid3A" class="seven_grid3a col-sm-3"></div>
              <div runat="server" id="SevenGrid3B" class="seven_grid3b col-sm-3"></div>
              <div runat="server" id="SevenGrid3C" class="seven_grid3c col-sm-3"></div>
              <div runat="server" id="SevenGrid3D" class="seven_grid3d col-sm-3"></div>
            </div>
            <div class="row dnnpane">
              <div runat="server" id="EightGrid4A" class="eight_grid4a col-sm-4"></div>
              <div runat="server" id="EightGrid4B" class="eight_grid4b col-sm-4"></div>
              <div runat="server" id="EightGrid4C" class="eight_grid4c col-sm-4"></div>
            </div>
            <div class="row dnnpane">
              <div runat="server" id="NineGrid6A" class="nine_grid6a col-sm-6"></div>
              <div runat="server" id="NineGrid6B" class="nine_grid6b col-sm-6"></div>
            </div>
            <div class="row dnnpane">
              <div runat="server" id="TenGrid12" class="ten_grid12 col-sm-12"></div>
            </div>
          </div>
        </div>
      </section>
      <section class="content_graybg">
        <div class="skin_width">
          <div class="skin_main_padding">
            <div class="row dnnpane">
              <div runat="server" id="GrayGrid12" class="graygrid12 col-sm-12"></div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <!--end skin main-->
    <!--start skin footer-->
    <section class="footer_top">
      <div class="skin_width">
        <div class="footerpane_style skin_main_padding">
          <div class="row dnnpane">
            <div runat="server" id="FooterGrid4A" class="footer_grid4a col-sm-4"></div>
            <div runat="server" id="FooterGrid4B" class="footer_grid4b col-sm-4"></div>
            <div runat="server" id="FooterGrid4C" class="footer_grid4c col-sm-4"></div>
          </div>
          <div class="row dnnpane">
            <div runat="server" id="FooterPane" class="footerpane col-sm-12"></div>
          </div>
        </div>
      </div>
    </section>
    <!--end footer top-->
    <!--start footer-->
    <footer class="copyright_bg">
      <div class="skin_width">
        <div class="footer_bottom clearafter">
          <div class="copyright_style">
            <dnn:COPYRIGHT runat="server" id="dnnCOPYRIGHT" cssclass="Footer" />
          </div>
          <div class="footer_right">
            <dnn:PRIVACY runat="server" id="dnnPRIVACY" cssclass="Footer" />
            |
            <dnn:TERMS runat="server" id="dnnTERMS" cssclass="Footer" />
          </div>
        </div>
      </div>
    </footer>
  </section>
  <section class="footer_shadow">
    <div class="skin_width">
      <div class="footer_shadow_style"><img src="<%= SkinPath %>images/footer_shadow.png" /></div>
    </div>
  </section>
  <!--end skin footer-->
  <a href="#top" id="top-link" title="Top"> </a> </div>
<!--#include file="commonparts/StyleCustom.ascx"-->
<!--#include file="commonparts/AddScripts.ascx"-->
