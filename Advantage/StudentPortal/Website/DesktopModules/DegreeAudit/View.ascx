﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Christoc.Modules.DegreeAudit.View" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<div style="width: 100%; margin: 0 auto">
    <p></p>
    <div style="text-align: left; font-size: 12px;">
        The information shown below reflects your current academic progress. The information provides a snapshot of your progress, grades,
        registered courses, in-progress courses and completed courses. To process degree audit for another program please select the program from the program 
        list and press the get degree audit button. 
    </div>
    <p></p>
    <table style="margin: 0 auto">
        <tr>
            <td>
                <asp:Label ID="lblEnrollment" runat="server" Text="Program:&nbsp;" Style="font-weight: bold;"></asp:Label></td>

            <td>
                <asp:DropDownList ID="ddlEnrollment" runat="server" Width="400px"></asp:DropDownList>
            </td>

            <td>
                <asp:Button ID="btnSearch" runat="server" Text="Get Degree Audit" OnClick="btnSearch_OnClick"></asp:Button>
            </td>
            <td>
                <telerik:RadButton ID="btnExportPdf" runat="server" Text="Print" ImageUrl="~/Images/Pdf.png" OnClick="BtnExportToPdfClick" Visible="false"></telerik:RadButton>
            </td>
        </tr>
    </table>

    <p></p>

    <p>
    <asp:Label ID="SelectedEnrollment" runat="server" ></asp:Label>
    </p>
    <table style="width: 100%;">
        <tr>
            <td>
                <telerik:RadGrid runat="server" ID="masterGrid" AutoGenerateColumns="false" OnItemDataBound="MasterGridItemCreated" >
                    <mastertableview commanditemdisplay="Top">
                                                                 <CommandItemTemplate>
                                                                      <%--<div style="height: 20px;">
                                                                          <table>
                                                                              <tr>
                                                                                    <td  style="width:400px;">
                                                                                      <asp:Label ID="Label2" runat="server" Font-Size="12px" Font-Bold="true" Style="text-align:center">Degree Audit</asp:Label>
                                                                                  </td >
                                                                                   <td  style="width:300px;">
                                                                                      <asp:Label ID="Label1" runat="server" Font-Size="12px" Font-Bold="true">Name: <%=pStudentName %></asp:Label>
                                                                                  </td>
                                                                                  <td style="width:300px;">
                                                                                      <asp:Label ID="Label3" runat="server" Font-Size="12px" Font-Bold="true">Student number: <%=pStudentNumber %></asp:Label>
                                                                                  </td>
                                                                              </tr>
                                                                          </table>
                                                                        </div>--%>
                                                                    </CommandItemTemplate>
                                                                <Columns>
                                                                    <telerik:GridBoundColumn DataField="Descrip" HeaderText="" UniqueName="Descrip" />
                                                                    <telerik:GridBoundColumn DataField="EquivCourseDesc" HeaderText="" UniqueName="EquivCourseDesc" ItemStyle-HorizontalAlign="Left" Visible="false" />
                                                                    <telerik:GridBoundColumn DataField="Credits" HeaderText="" UniqueName="Credits"  ItemStyle-HorizontalAlign="Left" />
                                                                    <telerik:GridBoundColumn DataField="Hours" HeaderText="" UniqueName="Hours"  ItemStyle-HorizontalAlign="Left"/>
                                                                    <telerik:GridBoundColumn DataField="Grade" HeaderText="" UniqueName="Grade"  ItemStyle-HorizontalAlign="Center"/>
                                                                    <telerik:GridBoundColumn DataField="CreditsEarned" HeaderText="" UniqueName="CreditsEarned"  ItemStyle-HorizontalAlign="Left"/>
                                                                    <telerik:GridBoundColumn DataField="CreditsRemaining" HeaderText="" UniqueName="CreditsRemained"  ItemStyle-HorizontalAlign="Left"/>
                                                                    <telerik:GridBoundColumn DataField="HoursCompleted" HeaderText="" UniqueName="HoursCompleted"  ItemStyle-HorizontalAlign="Left"/>
                                                                    <telerik:GridBoundColumn DataField="HoursRemaining" HeaderText="" UniqueName="HoursRemaining"  ItemStyle-HorizontalAlign="Left"/>
                                                                </Columns>
                                                            </mastertableview>
                </telerik:RadGrid>

            </td>
        </tr>

    </table>
    <br/>
    <br/>
    <div id="DegreeAuditWarningMessage" runat="server" style="width: 100%; text-align: center; color: red; font-weight: bold "></div>
    
    <telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
        <ajaxsettings>
         <telerik:AjaxSetting AjaxControlID="btnSearch">
             <UpdatedControls>
                 <telerik:AjaxUpdatedControl ControlID="masterGrid" LoadingPanelID="RadAjaxLoadingPanel1"/>
             </UpdatedControls>
        </telerik:AjaxSetting>
        
     </ajaxsettings>
    </telerik:RadAjaxManager>
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Height="150px" HorizontalAlign="Center"
        Width="100%" IsSticky="true" MinDisplayTime="500">
        <img alt="Loading Degree Audit..." src='<%= RadAjaxLoadingPanel.GetWebResourceUrl(Page, "Telerik.Web.UI.Skins.Default.Ajax.loading.gif") %>'
            style="border: 0; margin: 0 auto" />
    </telerik:RadAjaxLoadingPanel>
</div>
