﻿using System;
using System.Collections.Generic;
using System.Linq;
using PortalData.Demographic;

namespace PortalFacade
{
    public static class DemographicFacade
    {
        /// <summary>
        /// Return the states guid and description
        /// and a extra record named 'Other State' with Guid empty
        /// </summary>
        /// <returns>
        /// The list of states and guid.
        /// </returns>
        public static List<StateOutput> GetActiveStatesAndOtherState()
        {
            var db = new PortalDb.DemographicServices.DemographicServiceDb();
            var list = db.GetActiveStates();
            // Add Other State items
            list.Add(new StateOutput {StateGuid = Guid.Empty, StateDescription = "Other State" });
            list =  list.OrderBy(x => x.StateDescription).ToList();
            return list;
        }

        /// <summary>
        /// Get the list of counties
        /// </summary>
        /// <returns>IList of counties</returns>
        public static IList<CountyOutput> GetActiveCounties()
        {
            var db = new PortalDb.DemographicServices.DemographicServiceDb();
            var list = db.GetActiveCounties();
            return list;
        }

        public static IList<CountryOutput> GetActiveCountries()
        {
            var db = new PortalDb.DemographicServices.DemographicServiceDb();
            var list = db.GetActiveCountries();
            return list;
        }
    }
}
