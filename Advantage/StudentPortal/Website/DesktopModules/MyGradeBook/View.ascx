﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Christoc.Modules.MyGradeBook.View" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManager runat="server">
    <ajaxsettings>
            <telerik:AjaxSetting AjaxControlID="ddlEnrollment">
                <updatedcontrols>
                    <telerik:AjaxUpdatedControl ControlID="ddlTerm" UpdatePanelCssClass="" />
                    <telerik:AjaxUpdatedControl ControlID="ddlClsSection" UpdatePanelCssClass="" />
                    <telerik:AjaxUpdatedControl ControlID="rgStudentGradeBook" UpdatePanelCssClass="" />
                </updatedcontrols>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ddlTerm">
                <updatedcontrols>
                    <telerik:AjaxUpdatedControl ControlID="ddlClsSection" UpdatePanelCssClass="" />
                    <telerik:AjaxUpdatedControl ControlID="rgStudentGradeBook" UpdatePanelCssClass="" />
                </updatedcontrols>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="ddlClsSection">
                <updatedcontrols>
                    <telerik:AjaxUpdatedControl ControlID="rgStudentGradeBook" UpdatePanelCssClass="" />
                </updatedcontrols>
            </telerik:AjaxSetting>
            <telerik:AjaxSetting AjaxControlID="rgStudentGradeBook">
                <UpdatedControls>
                    <telerik:AjaxUpdatedControl ControlID="rgStudentGradeBook" LoadingPanelID="RadAjaxLoadingPanel1"></telerik:AjaxUpdatedControl>
                </UpdatedControls>
            </telerik:AjaxSetting>
        </ajaxsettings>
</telerik:RadAjaxManager>

<div>
    <table>
        <tr>
            <td class="enrollmentStyle">
                <asp:Label ID="lblEnrollment" runat="server" Text="Program:" Font-Names="Verdana" Font-Size="10pt" Font-Bold="True"></asp:Label>
            </td>
            <td class="spaceStyle"></td>
            <td class="termStyle">
                <asp:Label ID="lblTerm" runat="server" Text="Term:" Font-Names="Verdana" Font-Size="10pt" Font-Bold="True"></asp:Label>
            </td>
            <td class="spaceStyle"></td>
            <td class="classStyle">
                <asp:Label ID="lblClsSection" runat="server" Text="Class:" Font-Names="Verdana" Font-Size="10pt" Font-Bold="True"></asp:Label>
            </td>
        </tr>
        <tr>
            <td class="enrollmentStyle">
                <telerik:RadDropDownList ID="ddlEnrollment" runat="server" DataSourceID="dsEnrollments" DataTextField="Enrollment" DataValueField="StuEnrollId" AutoPostBack="True" OnSelectedIndexChanged="ddlTerm_visible" Font-Names="Verdana" Font-Size="10pt" Width="360px" Skin="Metro">
                </telerik:RadDropDownList>
                <asp:SqlDataSource ID="dsEnrollments" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>" SelectCommand="sp_GetStuEnrollments" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="lblHiddenUserName" DefaultValue="" Name="StudentNumber" PropertyName="Text" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td class="spaceStyle"></td>
            <td class="termStyle">
                <telerik:RadDropDownList ID="ddlTerm" runat="server" DataSourceID="dsTerms" AutoPostBack="True" OnSelectedIndexChanged="ddlClsSection_visible" DataTextField="TermDescrip" DataValueField="TermId" Font-Names="Verdana" Font-Size="10pt" Width="290px" Skin="Metro">
                </telerik:RadDropDownList>
                <asp:SqlDataSource ID="dsTerms" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>" SelectCommand="sp_GetStuEnrollmentTerms" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlEnrollment" Name="StuEnrollId" PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td class="spaceStyle"></td>
            <td class="classStyle">
                <telerik:RadDropDownList ID="ddlClsSection" runat="server" DataSourceID="dsClsSections" 
                    DataTextField="ClsSection" DataValueField="ClsSectionID" 
                    AutoPostBack="True" Font-Names="Verdana" Font-Size="10pt" Width="420px" Skin="Metro"
                    OnSelectedIndexChanged="ddlClsSection_IndexChanged"
                    >
                </telerik:RadDropDownList>
                <asp:SqlDataSource ID="dsClsSections" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>" SelectCommand="sp_GetClsSectionsForTerm" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="ddlEnrollment" Name="StuEnrollId" PropertyName="SelectedValue" Type="String" />
                        <asp:ControlParameter ControlID="ddlTerm" Name="TermId" PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
        </tr>
    </table>
</div>

<%--Hidden Variables--%>

<div>
    <asp:Label ID="lblHiddenUserName" runat="server" Visible="False"></asp:Label>
    <asp:Label ID="lblHiddenStuEnrollId" runat="server" Visible="False"></asp:Label>
    <asp:Label ID="lblHiddenClsSectionId" runat="server" Visible="False"></asp:Label>
    <asp:Label ID="lblHiddenTermId" runat="server" Visible="False"></asp:Label>
    <asp:Label ID="lblHiddenTxtMsg" runat="server" Visible="False"></asp:Label>
</div>

<%--Hidden Variables--%>

<p>
    <br />
</p>
<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>

<telerik:RadGrid ID="rgStudentGradeBook" runat="server" AllowPaging="True" AllowMultiRowSelection="True" 
    AutoGenerateColumns="False" CellSpacing="0"
    Font-Names="Arial" GridLines="None" 
    CssClass="MyGridClass" 
    OnDetailTableDataBind="rgStudentGradeBook_DetailTableDataBind" 
    OnNeedDataSource="rgStudentGradeBook_NeedDataSource"
    OnPreRender="rgStudentGradeBook_PreRender"
    >
    <clientsettings>
        <selecting allowrowselect="True" />
    </clientsettings>
    <mastertableview  name="Class" font-names="Arial" datakeynames="StuEnrollId,ClsSectionId,TermId" showfooter="True" pagesize="10">
        <detailtables>
            <telerik:GridTableView runat="server" AllowPaging="False" AutoGenerateColumns="True">
                <PagerStyle ShowPagerText="False" Visible="False" />
                <HeaderStyle BackColor="#00B4FF" />
            </telerik:GridTableView>
        </detailtables>
        <Columns>
            <telerik:GridBoundColumn DataField="Course" FilterControlAltText="Filter Course column" HeaderText="Course" ReadOnly="True" SortExpression="Course" UniqueName="Course">
                
            <columnvalidationsettings><modelerrormessage text=""></modelerrormessage></columnvalidationsettings></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Term" FilterControlAltText="Filter Term column" HeaderText="Term" SortExpression="Term" UniqueName="Term">
                
            <columnvalidationsettings><modelerrormessage text=""></modelerrormessage></columnvalidationsettings></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="ClassStartDate" DataType="System.DateTime" FilterControlAltText="Filter ClassStartDate column" HeaderText="Start Date" SortExpression="ClassStartDate" UniqueName="ClassStartDate" DataFormatString="{0:d}">
                
            <columnvalidationsettings><modelerrormessage text=""></modelerrormessage></columnvalidationsettings></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="ClassEndDate" DataType="System.DateTime" FilterControlAltText="Filter ClassEndDate column" HeaderText="End Date" SortExpression="ClassEndDate" UniqueName="ClassEndDate" DataFormatString="{0:d}">
                
            <columnvalidationsettings><modelerrormessage text=""></modelerrormessage></columnvalidationsettings></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="InstructorName" FilterControlAltText="Filter InstructorName column" HeaderText="Instructor" SortExpression="InstructorName" UniqueName="InstructorName">
                
            <columnvalidationsettings><modelerrormessage text=""></modelerrormessage></columnvalidationsettings></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Grade" FilterControlAltText="Filter Grade column" HeaderText="Grade" SortExpression="Grade" UniqueName="Grade">
                
            <columnvalidationsettings><modelerrormessage text=""></modelerrormessage></columnvalidationsettings></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Credits" DataType="System.Decimal" FilterControlAltText="Filter Credits column" HeaderText="Credits" SortExpression="Credits" UniqueName="Credits">
                
            <columnvalidationsettings><modelerrormessage text=""></modelerrormessage></columnvalidationsettings></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="Hours" DataType="System.Decimal" FilterControlAltText="Filter Hours column" HeaderText="Hours" SortExpression="Hours" UniqueName="Hours">
                
            <columnvalidationsettings><modelerrormessage text=""></modelerrormessage></columnvalidationsettings></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="TermId" FilterControlAltText="Filter TermId column" HeaderText="TermId" SortExpression="TermId" UniqueName="TermId" Display="False">
                
            <columnvalidationsettings><modelerrormessage text=""></modelerrormessage></columnvalidationsettings></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="ClsSectionId" FilterControlAltText="Filter ClsSectionId column" HeaderText="ClsSectionId" SortExpression="ClsSectionId" UniqueName="ClsSectionId" Display="False">
                
            <columnvalidationsettings><modelerrormessage text=""></modelerrormessage></columnvalidationsettings></telerik:GridBoundColumn>
            <telerik:GridBoundColumn DataField="StuEnrollId" Display="False" FilterControlAltText="Filter StuEnrollId column" HeaderText="StuEnrollId" SortExpression="StuEnrollId" UniqueName="StuEnrollId">
                <columnvalidationsettings>
                    <modelerrormessage text=""></modelerrormessage>
                </columnvalidationsettings>
            </telerik:GridBoundColumn>
        </Columns>
        <HeaderStyle Wrap="False" BackColor="#00B4FF" />
    </mastertableview>
    <footerstyle forecolor="White" />
    <headerstyle font-names="Verdana" BackColor="#00B4FF" />
    <pagerstyle backcolor="#00B4FF" bordercolor="White" borderstyle="Solid" borderwidth="1px" font-names="Verdana" font-size="10pt" forecolor="White" />
</telerik:RadGrid>

<%--<asp:SqlDataSource ID="dsMaterView" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>" SelectCommand="sp_GetGradeBookResults" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="ddlEnrollment" Name="StuEnrollId" PropertyName="SelectedValue" Type="String" />
        <asp:ControlParameter ControlID="ddlTerm" Name="TermId" PropertyName="SelectedValue" Type="String" />
        <asp:ControlParameter ControlID="ddlClsSection" Name="ClsSectionId" PropertyName="SelectedValue" Type="String" />
    </SelectParameters>
</asp:SqlDataSource>--%>

<%--<asp:SqlDataSource ID="dsChildView" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>" SelectCommand="sp_GetGradeBookComponents" SelectCommandType="StoredProcedure">
    <SelectParameters>
        <asp:ControlParameter ControlID="lblHiddenStuEnrollId" Name="StuEnrollId" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="lblHiddenClsSectionId" Name="ClsSectionId" PropertyName="Text" Type="String" />
        <asp:ControlParameter ControlID="lblHiddenTermId" Name="TermId" PropertyName="Text" Type="String" />
    </SelectParameters>
</asp:SqlDataSource>--%>

<br/>
<br/>
<div id="GradeBookWarningLabel" runat="server" style="width: 100%; text-align: center; color: red; font-weight: bold "></div>
