﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Christoc.Modules.MyProfile.View" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:ScriptManagerProxy ID="ScriptManagerProxy1" runat="server"></asp:ScriptManagerProxy>

<script type="text/javascript" id="telerikClientEvents1">

    function txtPhone_OnKeyPress(sender, args) {
        var regexp = new RegExp("[0-9 \b]{1}");
        if (!args.get_keyCharacter().match(regexp))
            args.set_cancel(true);
    }

    function txtZip_OnKeyPress(sender, args) {
        var regexp = new RegExp("[a-zA-Z0-9 \b]");
        if (!args.get_keyCharacter().match(regexp))
            args.set_cancel(true);
    }
</script>

<telerik:RadAjaxManager runat="server" UpdatePanelsRenderMode="Inline" DefaultLoadingPanelID="RadAjaxLoadingPanel1">
    <ajaxsettings>
        <telerik:AjaxSetting AjaxControlID="btnEditEmail">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="lblNote1" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="btnEditEmail" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="txtHomeEmail" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="txtWorkEmail" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="btnChangeEmail" UpdatePanelCssClass="" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnChangeEmail">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="lbltxtHomeEmailErrMsg" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="lbltxtWorkEmailErrMsg" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="lblReqEmailStatus" UpdatePanelCssClass="" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEditPhone">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="lblNote2" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="btnEditPhone" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="txtPhone1" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="chkPhone1Intl" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="ddlPhoneType1" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="txtPhone2" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="chkPhone2Intl" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="ddlPhoneType2" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="btnChangePhone" UpdatePanelCssClass="" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="chkPhone1Intl">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="txtPhone1" UpdatePanelCssClass="" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="chkPhone2Intl">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="txtPhone2" UpdatePanelCssClass="" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnChangePhone">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="lblReqPhoneStatus" UpdatePanelCssClass="" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnEditAddress">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="lblNote3" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="btnEditAddress" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="chkIntlAddress" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="txtAddress1" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="txtAddress2" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="txtCity" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="ddlState" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="lblOtherState" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="txtOtherState" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="txtZip" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="ddlCountries" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="btnChangeAddress" UpdatePanelCssClass="" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="chkIntlAddress">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="ddlState" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="lblOtherState" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="txtOtherState" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="txtZip" UpdatePanelCssClass="" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="btnChangeAddress">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="lblReqAddressStatus" UpdatePanelCssClass="" />
            </updatedcontrols>
        </telerik:AjaxSetting>
    </ajaxsettings>
</telerik:RadAjaxManager>

<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server"></telerik:RadAjaxLoadingPanel>

<telerik:RadPanelBar ID="RadPanelBar1" Runat="server" Width="1100px" CssClass="mainTable" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Skin="Metro">
    <items>
    <telerik:RadPanelItem runat="server" Text="Profile Information" Expanded="True" Font-Names="Verdana" Font-Size="12pt" Font-Bold="True" ChildGroupCssClass="applyBackground" Selected="True" BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px">
        <items>
                <telerik:RadPanelItem runat="server" Expanded="True" Value="templateHolder">
                    <ItemTemplate>
                        <div>
                            <table>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        <asp:Label ID="lblFName" runat="server" Font-Size="10pt" Font-Bold="True" Width="70px" Font-Names="Arial" ForeColor="Black" > Full Name: </asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label ID="txtFName" runat="server" Font-Size="10pt" Font-Bold="True" Width="300px" Font-Names="Arial" ForeColor="Black" >   </asp:Label>
                                    </td>
                                    <td style="padding-left: 3.5em">
                                        &nbsp;
                                    </td>
                                    <td style="padding-left: 35.5em">
                                        <asp:Label ID="lblDOB" runat="server" Font-Size="10pt" Font-Bold="True" Width="85px" Font-Names="Arial" ForeColor="Black" > Date of Birth: </asp:Label>
                                    </td>
                                     <td>
                                        <asp:Label ID="txtDOB" runat="server" Font-Size="10pt" Font-Bold="True" Width="70px" Font-Names="Arial" ForeColor="Black" >  </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                               <div style="padding-left: 3.5em">
                                    <asp:GridView ID="rgStuEnrollments" runat="server" AutoGenerateColumns="False" DataSourceID="dsEnrollments" BackColor="White" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" CellPadding="3" Font-Names="Verdana" Font-Size="10pt" Width="1000px">
                                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                                <HeaderStyle BackColor="#00B4FF" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                <RowStyle ForeColor="Black"/>
                                                <SelectedRowStyle BackColor="White" Font-Bold="True" ForeColor="White" />
                                                <SortedAscendingCellStyle BackColor="White" />
                                                <SortedAscendingHeaderStyle BackColor="White" />
                                                <SortedDescendingCellStyle BackColor="White" />
                                                <SortedDescendingHeaderStyle BackColor="White" />
                                        <Columns>
                                            <asp:BoundField DataField="Program" HeaderText="Program" SortExpression="Program" />
                                            <asp:BoundField DataField="Current Status" HeaderText="Current Status" SortExpression="Current Status" />
                                            <asp:BoundField DataField="Start Date" HeaderText="Start Date" ReadOnly="True" SortExpression="Start Date" />
                                            <asp:BoundField DataField="Expected Graduation Date" HeaderText="Expected Graduation Date" ReadOnly="True" SortExpression="Expected Graduation Date" />
                                        </Columns>
                                    </asp:GridView>
                                    <asp:SqlDataSource ID="dsEnrollments" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>
                                        " SelectCommand="   SELECT DISTINCT Program.ProgDescrip AS Program,SS.SysStatusDescrip AS [Current Status],
                                                            CONVERT (VARCHAR(10),e.StartDate,101) AS [Start Date],CONVERT (VARCHAR(10),e.ExpGradDate,101) AS [Expected Graduation Date] 
                                                            FROM arstuEnrollments as E 
                                                            INNER JOIN arStudent as S ON E.StudentId = S.StudentId 
                                                            INNER JOIN arPrgVersions as V ON E.PrgVerId = V.PrgVerId 
                                                            INNER JOIN dbo.arPrograms Program ON V.ProgId = Program.ProgId
                                                            INNER JOIN syStatuses as ST ON V.StatusId = ST.StatusId 
                                                            INNER JOIN syCampuses as C ON E.CampusId = C.CampusId 
                                                            INNER JOIN systatuscodes as SC ON E.StatusCodeId = SC.StatusCodeId 
                                                            INNER JOIN sysysstatus as SS ON SC.SysStatusId = SS.SysStatusId 
                                                            WHERE(SS.StatusLevelId = 2) AND S.StudentNumber=@StudentNumber ">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="lblHiddenUserName" Name="StudentNumber" PropertyName="Text" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                            </div>
                            <p>
                              <br/>
                            </p>
                    </ItemTemplate>
                </telerik:RadPanelItem>
            </items>
    </telerik:RadPanelItem>
    <telerik:RadPanelItem runat="server" Text="Email Information" Font-Names="Verdana" Font-Size="12pt" Font-Bold="True" ChildGroupCssClass="applyBackground" BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px">
        <items>
                <telerik:RadPanelItem Expanded="True" Value="templateHolder1">
                    <ItemTemplate>
                         <p>
                            <br/>
                        </p>
                        <div>
                            <table>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        <asp:Label ID="lblNote1" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="#000099" Visible="True" Text=" Please click 'Edit' button if you prefer to make changes in the below information."></asp:Label>
                                    </td>
                                    <td style="padding-left: 33em">
                                        <asp:Button ID="btnEditEmail" runat="server" Text="Edit" CssClass="cssButtons" Font-Size="12pt" Font-Names="Arial" Width="100px" OnClick="btnEditEmail_Click"/>
                                    </td>
                                </tr>
                            </table>
                            </div>
                            <div>
                             <table>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        <asp:Label ID="lblHomeEmail" runat="server" Font-Size="10pt" Font-Bold="True" Font-Names="Arial" ForeColor="Black"> Home Email   </asp:Label>
                                    </td>
                                    <td style="padding-left: 2em">
                                        <telerik:RadTextBox ID="txtHomeEmail" Runat="server" MaxLength="50" Width="275px" Wrap="False" Font-Names="Arial" Font-Size="10pt" Font-Bold="True" ViewStateMode="Enabled" Enabled="False" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></telerik:RadTextBox>
                                    </td>
                                    <td style="padding-left: 3.5em">
                                        <asp:Label ID="lbltxtHomeEmailErrMsg" runat="server" Font-Names="Arial" Width="250px" Font-Size="8pt" ForeColor="Red" Font-Bold="True" Text="Please enter valid email address."> </asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        <asp:Label ID="lblWorkEmail" runat="server" Font-Size="10pt" Font-Bold="True" Width="85px" Font-Names="Arial" ForeColor="Black"> Work Email   </asp:Label>
                                    </td>
                                    <td style="padding-left: 2em">
                                        <telerik:RadTextBox ID="txtWorkEmail" Runat="server" MaxLength="50" Width="275px" Wrap="False" Font-Names="Arial" Font-Size="10pt"  Font-Bold="True" Enabled="False" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"> </telerik:RadTextBox>
                                    </td>
                                    <td style="padding-left: 3.5em">
                                        <asp:Label ID="lbltxtWorkEmailErrMsg" runat="server" Font-Names="Arial" Font-Size="8pt" ForeColor="Red" Font-Bold="True" Text="Please enter valid email address."> </asp:Label>
                                    </td>
                                    <td style="padding-left: 20em">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <div style="padding-left: 3.5em">
                                <asp:Label ID="lblReqEmailStatus" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="#000099" Visible="False" Text="Your request has been submitted successfully."></asp:Label>
                            </div>
                            <div style="padding-left: 35em">
                                <asp:Button ID="btnChangeEmail" runat="server" Text="Request Email Change" CssClass="cssButtons" Font-Size="12pt" Font-Names="Arial" Width="225px" Visible="False" OnClick="btnChangeEmail_Click"/>
                            </div>
                            <p>
                                <br />
                            </p>
                        </div>
                    </ItemTemplate>
                </telerik:RadPanelItem>
            </items>
    </telerik:RadPanelItem>
    <telerik:RadPanelItem runat="server" Text="Phone Information" Font-Names="Verdana" Font-Size="12pt" Font-Bold="True" ChildGroupCssClass="applyBackground" BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px">
        <items>
                <telerik:RadPanelItem runat="server" Expanded="True" Value="templateHolder2">
                    <ItemTemplate>
                        <p>
                            <br/>
                        </p>
                        <div>
                            <table>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        <asp:Label ID="lblNote2" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="#000099" Visible="True" Text=" Please click 'Edit' button if you prefer to make changes in the below information."></asp:Label>
                                    </td>
                                    <td style="padding-left: 33em">
                                        <asp:Button ID="btnEditPhone" runat="server" Text="Edit" CssClass="cssButtons" Font-Size="12pt" Font-Names="Arial" Width="100px" OnClick="btnEditPhone_Click"/>
                                    </td>
                                </tr>
                            </table>
                            </div>
                            <div>
                             <table>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        <asp:Label ID="lblPhone1" runat="server" Font-Size="10pt" Font-Bold="True" Font-Names="Arial" ForeColor="Black"> Phone 1   </asp:Label>
                                    </td>
                                    <td style="padding-left: 2em">
                                        <telerik:RadTextBox ID="txtPhone1" Runat="server" Skin="Metro" Width="200px" Wrap="False" Font-Names="Arial" Font-Size="10pt" Font-Bold="True"  Enabled="False" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" >
                                            <clientevents onkeypress="txtPhone_OnKeyPress" />
                                        </telerik:RadTextBox> &nbsp;&nbsp;&nbsp;
                                        <asp:CheckBox ID="chkPhone1Intl" runat="server" Font-Names="Arial" Enabled="False" Font-Size="10pt" ForeColor="Blue" BackColor="White" BorderStyle="None" AutoPostBack="True" Text="Check if International" ToolTip="Check if international only" OnCheckedChanged="chkPhone1Intl_CheckedChanged" />
                                    </td>
                                    <td style="padding-left:  3em">
                                        <asp:Label ID="lblPType1" runat="server" Font-Size="10pt" Font-Bold="True" Font-Names="Arial" ForeColor="Black"> Phone Type   </asp:Label>
                                        </td>
                                    <td style="padding-left: 2em">
                                        <telerik:RadDropDownList ID="ddlPhoneType1" runat="server" DataSourceID="dsPhoneType" DataTextField="PhoneTypeDescrip" DataValueField="PhoneTypeId" DefaultMessage="--Select--" DropDownHeight="175px" DropDownWidth="150px" ExpandDirection="Down" Width="150px" Font-Names="Arial" Font-Size="10pt"  Font-Bold="True" Enabled="False" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></telerik:RadDropDownList>
                                        <asp:SqlDataSource ID="dsPhoneType" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>
                                        " SelectCommand="  SELECT DISTINCT PType.PhoneTypeId, PType.PhoneTypeDescrip FROM dbo.syPhoneType PType
                                                            INNER JOIN dbo.syStatuses sStatus ON PType.StatusId=sStatus.StatusId
                                                            WHERE sStatus.Status='Active' ORDER BY 2"></asp:SqlDataSource>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        <asp:Label ID="lblPhone2" runat="server" Font-Size="10pt" Font-Bold="True" Width="85px" Font-Names="Arial" ForeColor="Black"> Phone 2   </asp:Label>
                                    </td>
                                    <td style="padding-left: 2em">
                                        <telerik:RadTextBox ID="txtPhone2" Runat="server" Skin="Metro" Width="200px" Wrap="False" Font-Names="Arial" Font-Size="10pt" Font-Bold="True"  Enabled="False" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                            <clientevents onkeypress="txtPhone_OnKeyPress" />
                                        </telerik:RadTextBox> &nbsp;&nbsp;&nbsp;
                                        <asp:CheckBox ID="chkPhone2Intl" runat="server" Font-Names="Arial" Enabled="False" Font-Size="10pt" ForeColor="Blue" BackColor="White" BorderStyle="None" AutoPostBack="True" Text="Check if International" ToolTip="Check if international only" OnCheckedChanged="chkPhone2Intl_CheckedChanged" />
                                    </td>
                                    <td style="padding-left:  3em">
                                        <asp:Label ID="lblPType2" runat="server" Font-Size="10pt" Font-Bold="True" Font-Names="Arial" ForeColor="Black"> Phone Type   </asp:Label>
                                        </td>
                                    <td style="padding-left: 2em">
                                        <telerik:RadDropDownList ID="ddlPhoneType2" runat="server" DataSourceID="dsPhoneType" DataTextField="PhoneTypeDescrip" DataValueField="PhoneTypeId" DefaultMessage="--Select--" DropDownHeight="175px" DropDownWidth="150px" ExpandDirection="Down" Width="150px" Font-Names="Arial" Font-Size="10pt"  Font-Bold="True" Enabled="False" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></telerik:RadDropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <div style="padding-left: 3.5em">
                                <asp:Label ID="lblReqPhoneStatus" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="#000099" Visible="False" Text="Your request has been submitted successfully."></asp:Label>
                            </div>
                             <div style="padding-left: 35em">
                                <asp:Button ID="btnChangePhone" runat="server" Text="Request Phone Change" CssClass="cssButtons" Font-Size="12pt" Font-Names="Arial" Width="225px" Visible="False" OnClick="btnChangePhone_Click"/>
                            </div>
                            <p>
                                <br />
                            </p>
		                </div>
                    </ItemTemplate>
                </telerik:RadPanelItem>
            </items>
    </telerik:RadPanelItem>
    <telerik:RadPanelItem runat="server" Text="Address Information" Font-Names="Verdana" Font-Size="12pt" Font-Bold="True" ChildGroupCssClass="applyBackground" BorderColor="Gray" BorderStyle="Solid" BorderWidth="1px">
        <items>
                <telerik:RadPanelItem runat="server" Expanded="True" Value="templateHolder3">
                    <ItemTemplate>
                        <p>
                            <br/>
                        </p>
                        <div>
                            <table>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        <asp:Label ID="lblNote3" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="#000099" Visible="True" Text=" Please click 'Edit' button if you prefer to make changes in the below information."></asp:Label>
                                    </td>
                                    <td style="padding-left: 33em">
                                        <asp:Button ID="btnEditAddress"  runat="server" Text="Edit" CssClass="cssButtons" Font-Size="12pt" Font-Names="Arial" Width="100px" OnClick="btnEditAddress_Click"/>
                                    </td>
                                </tr>
                            </table>
                            </div>
                            <div>
                             <table>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        <asp:CheckBox ID="chkIntlAddress" runat="server" Font-Names="Arial" Enabled="False" Font-Size="10pt" ForeColor="Blue" BackColor="White" BorderStyle="None" Text="Check if International Address" ToolTip="Check if international only" AutoPostBack="True" OnCheckedChanged="chkIntlAddress_CheckedChanged" />
                                    </td>
                                </tr>
                                 <tr>
                                    <td style="padding-left: 3.5em">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        <asp:Label ID="lblAddress1"  runat="server" Font-Size="10pt" Font-Bold="True" Font-Names="Arial" ForeColor="Black" Width="100px"> Address 1   </asp:Label>
                                        <telerik:RadTextBox ID="txtAddress1" Runat="server" MaxLength="50" Width="300px" Wrap="False" Font-Names="Arial" Font-Size="10pt"  Font-Bold="True" Enabled="False" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></telerik:RadTextBox>
                                    </td>
                                </tr>
                                <tr>
                                     <td style="padding-left: 3.5em">
                                        <asp:Label ID="lblAddress2" runat="server" Font-Size="10pt" Font-Bold="True" Font-Names="Arial" ForeColor="Black" Width="100px"> Address 2   </asp:Label>
                                         <telerik:RadTextBox ID="txtAddress2" Runat="server" MaxLength="50" Width="300px" Wrap="False" Font-Names="Arial" Font-Size="10pt"  Font-Bold="True" Enabled="False" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></telerik:RadTextBox>
                                    </td>
                                </tr>
                                <tr>
                                     <td style="padding-left: 3.5em">
                                        <asp:Label ID="lblCity" runat="server" Font-Size="10pt" Font-Bold="True" Font-Names="Arial" ForeColor="Black" Width="100px"> City   </asp:Label>
                                         <telerik:RadTextBox ID="txtCity" Runat="server" MaxLength="50" Width="250px" Wrap="False" Font-Names="Arial" Font-Size="10pt"  Font-Bold="True" Enabled="False" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></telerik:RadTextBox>
                                    </td>
                                </tr>
                                <tr>
                                <td style="padding-left: 3.5em">
                                    <asp:Label ID="lblState" runat="server" Font-Bold="True" Font-Size="10pt" Font-Names="Arial" ForeColor="Black" Width="100px"> State  </asp:Label>
                                    <telerik:RadDropDownList ID="ddlState" runat="server" DataSourceID="dsStates" DataTextField="StateDescrip" DataValueField="StateId" DefaultMessage="--Select--" DropDownHeight="200px" DropDownWidth="250px" ExpandDirection="Down" Width="250px" Font-Names="Arial" Font-Size="10pt"  Font-Bold="True" Enabled="False" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></telerik:RadDropDownList>
                                        <asp:SqlDataSource ID="dsStates" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>
                                         " SelectCommand="  SELECT DISTINCT StateId,StateDescrip FROM syStates States
                                                            INNER JOIN syStatuses Statuses ON States.StatusId = Statuses.StatusId
                                                            WHERE Statuses.Status='Active' 
                                                            ORDER BY StateDescrip "></asp:SqlDataSource>
                                </td>
                                <td>
                                    <asp:Label ID="lblOtherState" runat="server" Font-Bold="True" Font-Size="10pt" Font-Names="Arial" ForeColor="Black" Visible="False" Width="100px"> Specify State  </asp:Label>
                                    <telerik:RadTextBox ID="txtOtherState" Runat="server" MaxLength="50" Width="200px" Wrap="False" Font-Names="Arial" Font-Size="10pt"  Font-Bold="True" Visible="False" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></telerik:RadTextBox>
                                </td>
                                </tr>
                                <tr>
                                     <td style="padding-left: 3.5em">
                                        <asp:Label ID="lblZip" runat="server" Font-Size="10pt" Font-Bold="True" Font-Names="Arial" ForeColor="Black" Width="100px"> Zip   </asp:Label>
                                         <telerik:RadTextBox ID="txtZip" Runat="server" Skin="Metro" Width="250px" Wrap="False" Font-Names="Arial" Font-Size="10pt"  Font-Bold="True" Enabled="False" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px">
                                             <clientevents onkeypress="txtZip_OnKeyPress" />
                                         </telerik:RadTextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                        <asp:Label ID="lblCountry" runat="server" Font-Size="10pt" Font-Bold="True" Font-Names="Arial" ForeColor="Black" Width="100px"> Country   </asp:Label>
                                        <telerik:RadDropDownList ID="ddlCountries" runat="server" DataSourceID="dsCountries" DataTextField="CountryDescrip" DataValueField="CountryId" DefaultMessage="--Select--" DropDownHeight="200px" DropDownWidth="250px" ExpandDirection="Down" Width="250px" Font-Names="Arial" Font-Size="10pt"  Font-Bold="True" Enabled="False" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px"></telerik:RadDropDownList>
                                        <asp:SqlDataSource ID="dsCountries" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>
                                        " SelectCommand="  SELECT DISTINCT CountryId,CountryDescrip FROM adCountries Country
                                                           INNER JOIN syStatuses Stat ON Country.StatusId=Stat.StatusId
                                                           WHERE Stat.Status='Active' ORDER BY CountryDescrip"></asp:SqlDataSource>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding-left: 3.5em">
                                    </td>
                                </tr>
                            </table>
                                <p>
                                    <br/>
                                </p>
                            <div style="padding-left: 3.5em">
                                <asp:Label ID="lblReqAddressStatus" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt" ForeColor="#000099" Visible="False" Text="Your request has been submitted successfully."></asp:Label>
                            </div>
                             <div style="padding-left: 35em">
                                <asp:Button ID="btnChangeAddress" runat="server" Text="Request Address Change" CssClass="cssButtons" Font-Size="12pt" Font-Names="Arial" Width="225px" Visible="False" OnClick="btnChangeAddress_Click"/>
                            </div>
                            <p>
                                <br />
                            </p>
		                </div>
                    </ItemTemplate>
                </telerik:RadPanelItem>
            </items>
    </telerik:RadPanelItem>
    </items>
   
</telerik:RadPanelBar>
<br/>
<br/>
 <asp:Label ID="ProfileWarningLabel" runat="server" Text="" ForeColor="Red" Font-Bold="True" Width="100%" style="text-align: center"></asp:Label>

<%--Hidden Variables--%>
<asp:Label ID="lblHiddenUserName" runat="server" Visible="False"></asp:Label>
<asp:Label ID="lblHiddenFullName" runat="server" Visible="False"></asp:Label>
<asp:Label ID="lblHiddenHomeEmail" runat="server" Visible="False"></asp:Label>
<asp:Label ID="lblHiddenWorkEmail" runat="server" Visible="False"></asp:Label>
<asp:Label ID="lblHiddenPhone1" runat="server" Visible="False"></asp:Label>
<asp:CheckBox ID="lblHiddernPhone1Intl" runat="server" Visible="False"></asp:CheckBox>
<asp:Label ID="lblHiddenPhone1Type" runat="server" Visible="False"></asp:Label>
<asp:Label ID="lblHiddenPhone2" runat="server" Visible="False"></asp:Label>
<asp:CheckBox ID="lblHiddernPhone2Intl" runat="server" Visible="False"></asp:CheckBox>
<asp:Label ID="lblHiddenPhone2Type" runat="server" Visible="False"></asp:Label>
<asp:CheckBox ID="lblHiddenAddressIntl" runat="server" Visible="False"></asp:CheckBox>
<asp:Label ID="lblHiddenAddress1" runat="server" Visible="False"></asp:Label>
<asp:Label ID="lblHiddenAddress2" runat="server" Visible="False"></asp:Label>
<asp:Label ID="lblHiddenCity" runat="server" Visible="False"></asp:Label>
<asp:Label ID="lblHiddenState" runat="server" Visible="False"></asp:Label>
<asp:Label ID="lblHiddenOtherState" runat="server" Visible="False"></asp:Label>
<asp:Label ID="lblHiddenZip" runat="server" Visible="False"></asp:Label>
<asp:Label ID="lblHiddenCountry" runat="server" Visible="False"></asp:Label>
<%--Hidden Variables--%>

