﻿<%@ Control AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Christoc.Modules.RegisterStudent.View" Language="C#" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<telerik:RadAjaxManager runat="server">
    <ajaxsettings>
        <telerik:AjaxSetting AjaxControlID="btnRegister">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="lbltxtLNameErrMsg" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="lbltxtStuIDErrMsg" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="lbltxtPWordErrMsg" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="lbltxtPWordConfirmErrMsg" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="lblStatus" UpdatePanelCssClass="" />
            </updatedcontrols>
        </telerik:AjaxSetting>
    </ajaxsettings>
</telerik:RadAjaxManager>

<div>
    <table style="width: 100%; border: solid 1px; background-color: lightgray;">
        <tr>
            <td>
                <img alt="" height="28" src="Images/Info.gif" width="28" />
            </td>
            <td>
                <asp:Label ID="lblNote" runat="server" Text="Welcome to the Student Registration Portal. Please fill out all the fields below to register and once you submit the form, your account will be activated immediately. Once your account is activated, you can access Portal by logging in with your student identifier number and password." Font-Size="10pt" ForeColor="#000099" Font-Names="Arial"></asp:Label>
            </td>
        </tr>
    </table>
</div>

<p>
    <br />
    <br />
</p>

<div class="tblHeader">
    <asp:Label ID="lblHeader" runat="server" Text="Student Registration" ForeColor="White" Font-Size="12pt" Font-Names="Arial"></asp:Label>
</div>

<div class="mainTable">
    <p>
        <br />
    </p>
    <table>
        <tr>
            <td style="width: 0.1%">
                <asp:Label ID="lblAsterik1" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red">* </asp:Label>
            </td>
            <td style="width: 10%">
                <asp:Label ID="lblLName" runat="server" Font-Size="10pt" Font-Bold="True" Width="95%" Font-Names="Arial" ForeColor="Black"> Last Name :   </asp:Label>
            </td>
            <td style="width: 20%">
                <telerik:RadTextBox id="txtLName" runat="server" MaxLength="50" Wrap="False" Width="99%" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Skin="Metro"></telerik:RadTextBox>
            </td>
            <td style="width: 15%">
                <asp:Label ID="lbltxtLNameErrMsg" runat="server" Font-Size="8pt" ForeColor="Red" Width="90%" Font-Names="Arial" Visible="False"> Last Name is required. </asp:Label>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 0.1%">
                <asp:Label ID="lblAsterik2" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red">* </asp:Label>
            </td>
            <td style="width: 10%">
                <asp:Label ID="lblStuID" runat="server" Font-Size="10pt" Font-Bold="True" Width="95%" Font-Names="Arial" ForeColor="Black"> Student ID :   </asp:Label>
            </td>
            <td style="width: 20%">
                <telerik:RadTextBox ID="txtStuID" runat="server" MaxLength="50" Wrap="False" Width="99%" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Skin="Metro"></telerik:RadTextBox>
            </td>
            <td style="width: 15%">
                <asp:Label ID="lbltxtStuIDErrMsg" runat="server" Font-Size="8pt" ForeColor="Red" Width="90%" Font-Names="Arial" Visible="False"> Student ID is required. </asp:Label>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 0.1%">
                <asp:Label ID="lblAsterik3" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red">* </asp:Label>
            </td>
            <td style="width: 10%">
                <asp:Label ID="lblPWord" runat="server" Font-Size="10pt" Font-Bold="True" Width="95%" Font-Names="Arial" ForeColor="Black" ToolTip="Password should be atleast 7 characters long."> Password :   </asp:Label>
            </td>
            <td style="width: 20%">
                <telerik:RadTextBox ID="txtPWord" runat="server" MaxLength="50" Wrap="False" Width="99%" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Skin="Metro" TextMode="Password" ToolTip="Password should be atleast 7 characters long."></telerik:RadTextBox>
            </td>
            <td style="width: 15%">
                <asp:Label ID="lbltxtPWordErrMsg" runat="server" Font-Size="8pt" ForeColor="Red" Width="90%" Font-Names="Arial" Visible="False"> Password is required. </asp:Label>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 0.1%">
                <asp:Label ID="lblAsterik4" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red">* </asp:Label>
            </td>
            <td style="width: 10%">
                <asp:Label ID="lblPWordConfirm" runat="server" Font-Size="10pt" Font-Bold="True" Width="95%" Font-Names="Arial" ForeColor="Black"> Confirm Password :   </asp:Label>
            </td>
            <td style="width: 20%">
                <telerik:RadTextBox ID="txtPWordConfirm" runat="server" MaxLength="50" Wrap="False" Width="99%" Font-Names="Arial" Font-Size="10pt" ForeColor="Black" BorderColor="Black" BorderStyle="Solid" BorderWidth="1px" Skin="Metro" TextMode="Password"></telerik:RadTextBox>
            </td>
            <td style="width: 15%">
                <asp:Label ID="lbltxtPWordConfirmErrMsg" runat="server" Font-Size="8pt" ForeColor="Red" Width="90%" Font-Names="Arial" Visible="False"> Password Confirmation is required. </asp:Label>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    <div>
        <asp:Label ID="lblStatus" runat="server" Font-Bold="True" Font-Names="Arial" Font-Size="10pt"></asp:Label>
    </div>
    <p>
        <br />
    </p>
    <div style="padding-left: 32% !important">
        <asp:Button ID="btnRegister" runat="server" Text="Register" CssClass="cssButtons" Font-Size="12pt" Font-Names="Arial" Width="100px" OnClick="btnRegister_Click" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="cssButtons" Font-Size="12pt" Font-Names="Arial" Width="100px"  OnClick="btnCancel_Click"/>
    </div>
    <p>
        <br />
    </p>
</div>
