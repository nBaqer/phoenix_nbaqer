﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Christoc.Modules.MySchedule.View" %>
<%@ Register assembly="Telerik.Web.UI" namespace="Telerik.Web.UI" tagPrefix="telerik" %>

<div>
    <table style="padding: 3px; width: 100%; margin:3px" >
        <tr>
            <td style="width: 400px; vertical-align: top">
                 <table style="padding: 3px; margin:3px" >
                    <tr>
                        <td>
                            <asp:Label ID="lblEnrollment" runat="server" Text="Program: " ></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlEnrollment" runat="server" Width="400px" AutoPostBack="True" OnSelectedIndexChanged="DdlEnrollment_OnSelectedIndexChanged" ></asp:DropDownList>    
                        </td>
            
                    </tr>
                      <tr>
                        <td>
                            <asp:Label ID="lblTerm" runat="server" Text="Terms: "  ></asp:Label>
                        </td>
                        <td>
                             <asp:DropDownList ID="ddlTerms" runat="server" Width="400px" AutoPostBack="True" OnSelectedIndexChanged="DdlTerms_OnSelectedIndexChanged"></asp:DropDownList>    
                        </td>
                    </tr>
                    <tr>
                         <td>
                            <asp:Label ID="lblInstructor" runat="server" Text="Instructors: " ></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlInstructor" runat="server" Width="400px" AutoPostBack="True" OnSelectedIndexChanged="DdlInstructor_OnSelectedIndexChanged"></asp:DropDownList>    
                        </td>
                    </tr>
                   
                </table>
            </td>
            <td  style="vertical-align: top">
                <table  style="padding: 3px; margin:3px">
                    <tr>
                        <td>
                            <asp:Label ID="lblCourses" runat="server" Text="Courses: "  ></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlCourses" runat="server" Width="400px" AutoPostBack="True" OnSelectedIndexChanged="DdlCourses_OnSelectedIndexChanged" ></asp:DropDownList>    
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblClassStart" runat="server" Text="Date Range: " ></asp:Label>
                        </td>
                        <td colspan="3">
                            <telerik:RadDatePicker runat="server" ID="rdStartDate" Width="200px" OnSelectedDateChanged="SelectionChanged" AutoPostBack="True"></telerik:RadDatePicker >
                            <telerik:RadDatePicker runat="server" ID="rdEndDate" Width="200px" OnSelectedDateChanged="SelectionChanged" AutoPostBack="True"></telerik:RadDatePicker ><br/>
                            <asp:CustomValidator ID="valDates" runat="server" ControlToValidate="rdEndDate"    
                                Display="Dynamic" ErrorMessage="Start and End Dates are not Valid" ForeColor="Red"
                                OnServerValidate="DateValidate" >
                            </asp:CustomValidator>
                        </td>
                    </tr>
                     <tr>
                        <td >&nbsp;</td>
                        <td > <asp:Button ID="btnSearch" runat="server" Text="Search" Width="55px" OnClick="ButtonSearch_OnClick" />
                            &nbsp;<asp:Button ID="btnClear" runat="server" Text="Clear Filters"  OnClick="ButtonClear_OnClick" />
                            &nbsp;<asp:Button ID="btnPrint" runat="server" Text="Print" Width="55px" OnClick="ButtonExportToPdfClick" /></td>
                         
                    </tr>
                </table>
            </td>
        </tr>
    </table>    
            
     

    <telerik:RadGrid ID="rdSchedule" runat="server" AllowMultiRowSelection="True" AllowPaging="False" AllowSorting="False" 
        AutoGenerateColumns="False" AutoGenerateHierarchy="False" CellSpacing="0"  GridLines="None"   ShowHeader="true" 
        OnNeedDataSource="RdScheduleNeedDataSource" 
        OnDetailTableDataBind="RdScheduleDetailTableDataBind" 
        OnPreRender="RdSchedulePreRender" 
        OnItemCommand="RdScheduleOnItemCommand">
        <clientsettings>
            <selecting allowrowselect="True" />
            <scrolling allowscroll="True" usestaticheaders="True" />
        </clientsettings>
       
        <mastertableview allowmulticolumnsorting="True" allownaturalsort="False" autogeneratecolumns="False" DataKeyNames="SectionId" ShowHeader="true"  >
            <detailtables>
                <telerik:GridTableView runat="server" AllowFilteringByColumn="False" AllowNaturalSort="False" AllowPaging="False" AllowSorting="False" Name="ScheduleDetails" AutoGenerateColumns="False" >
                    <Columns>
                        <telerik:GridBoundColumn DataField="Building" HeaderText="Building" UniqueName="Building" Visible="True" ></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Period" HeaderText="Period" UniqueName="Period" Visible="True"></telerik:GridBoundColumn>
                        <telerik:GridBoundColumn DataField="Room" HeaderText="Room" UniqueName="Room" Visible="True"></telerik:GridBoundColumn>
                    </Columns>
                </telerik:GridTableView>
            </detailtables>
            <Columns>
                <telerik:GridBoundColumn DataField="SectionId" HeaderText="SectionId" UniqueName="SectionId" Visible="false">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Section" HeaderText="Class" UniqueName="Section" >
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Course" FilterControlAltText="Filter Course column" HeaderText="Course" SortExpression="Course" UniqueName="Course">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Code" FilterControlAltText="Filter CourseCode column" HeaderText="Code" SortExpression="Course Code" UniqueName="Code">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Term" FilterControlAltText="Filter Term column" HeaderText="Term Offered" SortExpression="Term" UniqueName="Term">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Duration" FilterControlAltText="Filter Duration column" HeaderText="Duration" SortExpression="Term" UniqueName="Duration">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Instructor" FilterControlAltText="Filter Instructor column" HeaderText="Instructor" ReadOnly="True" SortExpression="Instructor" UniqueName="Instructor">
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Grade" FilterControlAltText="Filter Grade column" HeaderText="Grade" ReadOnly="True" SortExpression="Grade" UniqueName="Grade">
                </telerik:GridBoundColumn>
            </Columns>
            <ItemStyle Font-Size="10pt" />
            <AlternatingItemStyle Font-Size="10pt" />
            <HeaderStyle BackColor="#D4E6F2" BorderColor="#98C1DE" ForeColor="#1E81AF" />
        </mastertableview>
        <headerstyle backcolor="#D4E6F2" bordercolor="#98C1DE" forecolor="#1E81AF" />
        <selecteditemstyle forecolor="Yellow" />
    </telerik:RadGrid>
    <br/>
    <br/>
    <div id="ScheduleWarningMessage" runat="server" style="width: 100%; text-align: center; color: red; font-weight: bold "></div>
   </div>

















