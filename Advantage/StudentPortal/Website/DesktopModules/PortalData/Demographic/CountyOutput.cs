﻿using System;

namespace PortalData.Demographic
{
    /// <summary>
    /// Support County information for comboboxs
    /// </summary>
    public class CountyOutput
    {
        public Guid CountyGuid { get; set; }
        public string CountyDescription { get; set; }
    }
}
