﻿using System;

namespace PortalData.Demographic
{
    /// <summary>
    /// Support the country information for combobox
    /// </summary>
    public class CountryOutput
    {
        public Guid CountryGuid { get; set; }
        public string CountryDescription { get; set; }
    }
}
