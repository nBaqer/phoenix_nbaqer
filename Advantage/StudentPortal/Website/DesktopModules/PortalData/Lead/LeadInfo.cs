﻿using System;

namespace PortalData.Lead
{
    /// <summary>
    /// LEad info. hold the info information for one Lead.
    /// </summary>
    public class LeadInfo
    {
         public string CampusId{ get; set; }
         public string ProgId{ get; set; }
         public string SourceTypeId{ get; set; }
         public string PrevEduLvlId{ get; set; }
         public string FirstName{ get; set; }
         public string MiddleName{ get; set; }
         public string LastName{ get; set; }
         public DateTime? BirthDate{ get; set; }
         public string GenderId{ get; set; }
         public string PhoneNumber{ get; set; }
         public bool? ForeignPhone{ get; set; }
         public string PhoneNumber2{ get; set; }
         public bool? ForeignPhone2{ get; set; }
         public string Email{ get; set; }
         public string AlternativeEmail{ get; set; }
         public string Address1{ get; set; }
         public string Address2{ get; set; }
         public string City{ get; set; }
         public string StateId{ get; set; }
         public string OtherState{ get; set; }
         public string Zip{ get; set; }
         public bool? ForeignZip{ get; set; }
         public string CountyId{ get; set; }
         public string CountryId{ get; set; }
         public string RaceId{ get; set; }
         public string NationalityId{ get; set; }
         public string Citizen{ get; set; }
         public string MaritalStatus{ get; set; }
         public string FamilyIncome{ get; set; }
         public string Children{ get; set; }
         public string DrivLicStateID{ get; set; }
         public string DrivLicNumber{ get; set; }
         public string Comments { get; set; }
    }
}
