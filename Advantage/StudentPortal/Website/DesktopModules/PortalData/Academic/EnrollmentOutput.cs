﻿using System;

namespace PortalData.Academic
{
    public class EnrollmentOutput
    {

        //E.StuEnrollId,Program.ProgDescrip AS Enrollment,SS.SysStatusDescrip AS [Status],sh.ShiftDescrip Shift,e.StartDate,e.ExpGradDate

        public Guid? EnrollmentId { get; set; }
        public string Enrollment { get; set; }
        public string Status { get; set; }
        public string Shift  { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? ExpectedGradDate { get; set; }
        public string Name { get; set; }
        public string Studentnumber { get; set; }

    }
}