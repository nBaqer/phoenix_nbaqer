﻿using System;

namespace PortalData.Academic
{
    public class CourseOutput
    {
        public Guid ReqId { get; set; }
        public string Description { get; set; }
        public Guid? GradeSystemDetailId { get; set; }
        public Guid? TermId { get; set; }
        public Guid? InstructorId { get; set; }
    }
}