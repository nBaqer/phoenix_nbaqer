﻿using System;

namespace PortalData.Academic
{
    public class InstructorOutput
    {
        public Guid InstructorId { get; set; }
        public string Description { get; set; }
        public Guid? TermId { get; set; }
    }
}