﻿using System;

namespace PortalData.Academic
{
    public class ScheduleOutput
    {
        public Guid SectionId { get; set; }
        public string ClassSection { get; set; }
        public Guid? TermId { get; set; }
        public string Term { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public Guid? ReqId { get; set; }
        public string Course { get; set; }
        public string Code { get; set; }
        public string Section { get; set; }
        public decimal? Credits { get; set; }
        public decimal? Hours { get; set; }
        public Guid? InstructorId { get; set; }
        public string Instructor { get; set; }
        public string Duration { get; set; }
        public string Grade { get; set; }
        public Guid? GradeSystemDetailId { get; set; }

    }
}