﻿using System;

namespace PortalData.Academic
{
    public class ScheduleDetailOutput
    {
        public string Building { get; set; }
        public string Room { get; set; }
        public string Period { get; set; }
        public Guid? GradeSystemDetailId { get; set; }
        
    }
}