﻿namespace PortalData
{
    public class Constant
    {
        /// <summary>
        /// This constant is used when the student has not recor to show in his My Info section
        /// </summary>
        public const string X_WARNING_USER_HAS_NO_RECORDS = "You have no record to show";

        /// <summary>
        /// This constant is used when a user has Program but not Term or Classes scheduled.
        /// </summary>
        public const string X_WARNING_USER_HAS_NO_ATTENDANCE = "You have no Attendance posted";
    }
}
