﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Christoc.Modules.CourseListing.View" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<asp:ScriptManagerProxy ID="smpCourseListing" runat="server">
</asp:ScriptManagerProxy>

<telerik:RadAjaxManager ID="ramCourseListing" runat="server">
    <ajaxsettings>
        <telerik:AjaxSetting AjaxControlID="btnClrFltr">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="rgCourseList" UpdatePanelCssClass="" />
            </updatedcontrols>
        </telerik:AjaxSetting>
        <telerik:AjaxSetting AjaxControlID="rgCourseList">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="rgCourseList" UpdatePanelCssClass="" />
            </updatedcontrols>
        </telerik:AjaxSetting>
    </ajaxsettings>
</telerik:RadAjaxManager>
<div>
    <h2>Course Listing</h2>
    <p>
        <span style="font-size: 14px;" >Thank you for considering our school for your educational needs.
        The following is the list of courses offered at our campuses.</span>
        <br /><br/>
    </p>
</div>
<table cellpadding="3" cellspacing="3" width="100%" align="center" >
    <tr>
        <td style="width:29%">
            <asp:Label runat="server" Text="Course Description"></asp:Label>
        </td>
        <td style="width:30%">
            <asp:Label ID="lblProgram" runat="server" Text="Program"></asp:Label>
        </td >
        <td style="width:30%">
            <asp:Label ID="Label1" runat="server" Text="Campus"></asp:Label>
        </td>
        <td style="width:4%">&nbsp;</td>
        <td style="width:7%">&nbsp;</td>
    </tr>
    <tr>
        <td>
            <asp:TextBox runat="server" ID="txtCourseDescription" Width="100%"></asp:TextBox>
        </td>
        
        <td>
            <asp:DropDownList runat="server" ID="lstPrograms" Width="100%"></asp:DropDownList>
        </td>
        <td>
            <asp:DropDownList runat="server" ID="lstCampus" Width="100%"></asp:DropDownList>
        </td>
        <td>
            <asp:Button ID="btnSearch" runat="server" Text="Search" Width="55px" OnClick="btnSearch_OnClick" />
        </td>
        <td>
            <asp:Button ID="btnClear" runat="server" Text="Clear Filters" Width="85px" OnClick="btnClear_OnClick"  />
        </td>
    </tr>
    
    
</table>
<div>
    <telerik:RadGrid ID="rgCourseList" runat="server" AllowMultiRowSelection="True" 
        AllowSorting="True" AutoGenerateHierarchy="True" CellSpacing="0" GridLines="None" Width="100%" Skin="Metro" OnNeedDataSource="rgCourseList_NeedDataSource" OnPreRender="rgCourseList_PreRender"
        AutoGenerateColumns="False" EnableViewState="False" ShowFooter="true">
        <groupingsettings casesensitive="false" />
        <exportsettings>
            <csv enclosedatawithquotes="False" />
        </exportsettings>
        <clientsettings>
            <selecting allowrowselect="True" />
            <scrolling allowscroll="True" usestaticheaders="True" />
        </clientsettings>
        <alternatingitemstyle bordercolor="#98C1DE" />
        <groupheaderitemstyle bordercolor="#98C1DE" />
        <mastertableview allowmulticolumnsorting="True" allownaturalsort="False" nodetailrecordstext="No courses are found to display." nomasterrecordstext="No courses are found to display.">
            <Columns>
                 <telerik:GridBoundColumn DataField="Course" FilterControlAltText="Filter Course column" HeaderText="Course" SortExpression="Course" UniqueName="Course" >
                    <columnvalidationsettings>
                        <modelerrormessage text=""></modelerrormessage>
                    </columnvalidationsettings>
                </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Code" FilterControlAltText="Filter Code column" HeaderText="Code" SortExpression="Code" UniqueName="Code">
                    <columnvalidationsettings>
                        <modelerrormessage text=""></modelerrormessage>
                    </columnvalidationsettings>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn DataField="Credits" DataType="System.Decimal" FilterControlAltText="Filter Credits column" HeaderText="Credits" SortExpression="Credits" UniqueName="Credits">
                    <columnvalidationsettings>
                        <modelerrormessage text=""></modelerrormessage>
                    </columnvalidationsettings>
                </telerik:GridBoundColumn>
                 <telerik:GridBoundColumn DataField="Program" FilterControlAltText="Filter Program column" HeaderText="Program" SortExpression="Program" UniqueName="Program">
                    <columnvalidationsettings>
                        <modelerrormessage text=""></modelerrormessage>
                    </columnvalidationsettings>
                </telerik:GridBoundColumn>
                <telerik:GridBoundColumn  DataField="Campus" FilterControlAltText="Filter Campus column" HeaderText="Campus" SortExpression="Campus" UniqueName="Campus" Aggregate="Count" FooterText="Number of Courses: " >
                    <columnvalidationsettings>
                        <modelerrormessage text=""></modelerrormessage>
                    </columnvalidationsettings>
                </telerik:GridBoundColumn>
               
                <telerik:GridBoundColumn DataField="CourseCategory" Visible="false" FilterControlAltText="Filter Code column" HeaderText="Category" SortExpression="CourseCategory" UniqueName="CourseCategory">
                    <columnvalidationsettings>
                        <modelerrormessage text=""></modelerrormessage>
                    </columnvalidationsettings>
                </telerik:GridBoundColumn>
               
               
                
            </Columns>
        <ItemStyle Font-Size="10pt" />
            <AlternatingItemStyle Font-Size="10pt" />
            <PagerStyle AlwaysVisible="True" />
           
        </mastertableview>
        <footerstyle font-bold="True" />
        
        <pagerstyle alwaysvisible="True" />
        <selecteditemstyle forecolor="Yellow" />
    </telerik:RadGrid>
    
</div>
<asp:SqlDataSource ID="dsCourseList" runat="server" ConnectionString="<%$ ConnectionStrings:ClientConnectionString %>"
    SelectCommand="sp_GetCourseList" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
