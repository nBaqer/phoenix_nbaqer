﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Configuration;
using PortalData.Academic;

namespace PortalDb.AcademicServices
{
    public class GradeBookServiceDb
    {
         #region Private Class Variables
        
        //linq data context object
        private readonly StudentScheduleLinqClassesDataContext dbContext; 

        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        public GradeBookServiceDb()
        {
            var conn = WebConfigurationManager.ConnectionStrings["ClientConnectionString"].ConnectionString;
            dbContext = new StudentScheduleLinqClassesDataContext(new SqlConnection(conn));
        }
        #endregion

        /// <summary>
        /// get the enrollments for the student using the student number
        /// </summary>
        /// <param name="stuEnrollmentsId"></param>
        /// <param name="termId"></param>
        /// <param name="clsSectionId"></param>
        /// <returns></returns>
        public IList<GradeBookResultOutput> GetGradeBookResults(string stuEnrollmentsId, string termId, string clsSectionId)
        {
            var data = (from e in dbContext.sp_GetGradeBookResults(stuEnrollmentsId, termId, clsSectionId)
                        select new GradeBookResultOutput
                        {
                            StuEnrollId = e.StuEnrollId,
                            Course =  e.Course,
                            Term = e.Term,
                            ClassStartDate = e.ClassStartDate,
                            ClassEndDate = e.ClassEndDate,
                            InstructorName = e.InstructorName,
                            Grade = e.Grade,
                            Credits = e.Credits,
                            Hours = e.Hours,
                            TermId = e.TermId,
                            ClsSectionId = e.ClsSectionId,
                        }).OrderBy(n => n.Course).ToList();

            return data;
        }

        /// <summary>
        /// Get detail information for grade book.
        /// </summary>
        /// <param name="stuEnrollmentsId"></param>
        /// <param name="termId"></param>
        /// <param name="clsSectionId"></param>
        /// <returns></returns>
        public IList<GradeBookComponentsOutput> GetGradeBookComponents(string stuEnrollmentsId, string termId, string clsSectionId)
        {
            var data = (from e in dbContext.sp_GetGradeBookComponents(stuEnrollmentsId, clsSectionId, termId)
                        select new GradeBookComponentsOutput
                        {
                            ComponentType = e.ComponentType,
                            Score = e.Score,
                            Weight = e.Weight,
                            GradeBookComponent = e.GradeBookComponent,
                        }).OrderBy(n => n.GradeBookComponent).ToList();

            return data;
        }


    }
}
