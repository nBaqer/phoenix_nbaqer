﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Configuration;
using MoreLinq;
using PortalData.Academic;

namespace PortalDb.AcademicServices
{
    public class EnrollmentServiceDb
    {
        #region Private Class Variables
        
        //linq data context object
        private readonly StudentScheduleLinqClassesDataContext dbContext; 

        #endregion

        public EnrollmentServiceDb()
        {
            var conn = WebConfigurationManager.ConnectionStrings["ClientConnectionString"].ConnectionString;
            dbContext = new StudentScheduleLinqClassesDataContext(new SqlConnection(conn));
        }

        #region Public Class Methods

        /// <summary>
        /// get the enrollments for the student using the student number
        /// </summary>
        /// <param name="studentNumber"></param>
        /// <returns></returns>
        public List<EnrollmentOutput> GetEnrollments(string studentNumber)
        {
            var data = (from e in dbContext.sp_getStuEnrollmentsPortal(studentNumber)
                        select new EnrollmentOutput
                        {
                            EnrollmentId = e.StuEnrollId,
                            Enrollment = e.Enrollment
                        }).OrderBy(n => n.Enrollment).ToList();

            return data;
        }


        /// <summary>
        /// get the available courses for the student for the dropdown list
        /// </summary>
        /// <param name="stuEnrollId"></param>
        /// <param name="termId"></param>
        /// <param name="instructorId"></param>
        /// <returns></returns>
        public List<CourseOutput> GetCourses(Guid stuEnrollId, Guid? termId, Guid? instructorId)
        {
            var data = (from res in dbContext.arResults
                join enr in dbContext.arStuEnrollments on res.StuEnrollId equals enr.StuEnrollId
                join cs in dbContext.arClassSections on res.TestId equals cs.ClsSectionId
                join cst in dbContext.arClassSectionTerms on cs.ClsSectionId equals cst.ClsSectionId
                join t in dbContext.arTerms on cst.TermId equals t.TermId
                join r in dbContext.arReqs on cs.ReqId equals r.ReqId
                join cmpgrpcmps in dbContext.syCmpGrpCmps on cs.CampusId equals cmpgrpcmps.CampusId
                join u in dbContext.syUsers on cs.InstructorId equals u.UserId
                where enr.StuEnrollId == stuEnrollId
                select new CourseOutput
                {
                    ReqId = r.ReqId,
                    Description = r.Descrip,
                    GradeSystemDetailId = res.arGradeSystemDetail.GrdSysDetailId,
                    TermId = cst.TermId,
                    InstructorId = cs.InstructorId
                });

            if (termId != null)
                data = data.Where(n => n.TermId == termId);

            if (instructorId != null)
                data = data.Where(n => n.InstructorId == instructorId);

            data = data.Distinct();
            

            //remove the items that are dropped courses
            var outputData = new List<CourseOutput>();
            foreach (var d in data)
            {
                if (d.GradeSystemDetailId != null)
                {
                    var arGrSystemDtl = dbContext.arGradeSystemDetails.FirstOrDefault(n => n.GrdSysDetailId == d.GradeSystemDetailId);
                    var isDrop = arGrSystemDtl != null && arGrSystemDtl.IsDrop;

                    if (!isDrop)
                        outputData.Add(d);
                }
                else
                {
                    outputData.Add(d);
                }
            }

            return outputData.Distinct().OrderBy(n => n.Description).ToList(); 
        }

        /// <summary>
        /// Determine the quantity of courses for a determinate enrollment
        /// </summary>
        /// <param name="enrollGuid"></param>
        /// <returns></returns>
        public bool HasCourses(Guid enrollGuid)
        {
            var data = (from res in dbContext.arResults
                join enr in dbContext.arStuEnrollments on res.StuEnrollId equals enr.StuEnrollId
                join cs in dbContext.arClassSections on res.TestId equals cs.ClsSectionId
                join cst in dbContext.arClassSectionTerms on cs.ClsSectionId equals cst.ClsSectionId
                join t in dbContext.arTerms on cst.TermId equals t.TermId
                join r in dbContext.arReqs on cs.ReqId equals r.ReqId
                join cmpgrpcmps in dbContext.syCmpGrpCmps on cs.CampusId equals cmpgrpcmps.CampusId
                join u in dbContext.syUsers on cs.InstructorId equals u.UserId
                where enr.StuEnrollId == enrollGuid
                select new CourseOutput
                {
                    ReqId = r.ReqId,
                    Description = r.Descrip,
                    GradeSystemDetailId = res.arGradeSystemDetail.GrdSysDetailId,
                    TermId = cst.TermId,
                    InstructorId = cs.InstructorId
                }).ToArray();

            // True if has course for the enrollment
            return (data.Length > 0);
        }

  

        /// <summary>
        /// get the available instructors for the student schedule dropdown list
        /// </summary>
        /// <param name="stuEnrollId"></param>
        /// <param name="termId"></param>
        /// <returns></returns>
        public List<InstructorOutput> GetInstructors(Guid stuEnrollId, Guid? termId )
        {
            var data = (from res in dbContext.arResults
                join enr in dbContext.arStuEnrollments on res.StuEnrollId equals enr.StuEnrollId
                join cs in dbContext.arClassSections on res.TestId equals cs.ClsSectionId
                join cst in dbContext.arClassSectionTerms on cs.ClsSectionId equals cst.ClsSectionId
                join t in dbContext.arTerms on cst.TermId equals t.TermId
                join r in dbContext.arReqs on cs.ReqId equals r.ReqId
                join cmpgrpcmps in dbContext.syCmpGrpCmps on cs.CampusId equals cmpgrpcmps.CampusId
                join u in dbContext.syUsers on cs.InstructorId equals u.UserId
                where enr.StuEnrollId == stuEnrollId
                select new InstructorOutput
                {
                    InstructorId = u.UserId,
                    Description = u.FullName,
                    TermId = cst.TermId
                });

            if (termId != null)
                data = data.Where(n => n.TermId == termId);

            List<InstructorOutput> outputData = data.ToList();

            outputData = outputData.DistinctBy(n => n.Description).OrderBy(n => n.Description).ToList();

            return outputData.ToList();


        }

        /// <summary>
        /// get the available terms for the student schedule for the dropdown list
        /// </summary>
        /// <param name="stuEnrollId"></param>
        /// <returns></returns>
        public List<TermOutput> GetTerms(Guid stuEnrollId)
        {
            var data = (from res in dbContext.arResults
                        join enr in dbContext.arStuEnrollments on res.StuEnrollId equals enr.StuEnrollId
                        join cs in dbContext.arClassSections on res.TestId equals cs.ClsSectionId
                        join cst in dbContext.arClassSectionTerms on cs.ClsSectionId equals cst.ClsSectionId
                        join t in dbContext.arTerms on cst.TermId equals t.TermId
                        join r in dbContext.arReqs on cs.ReqId equals r.ReqId
                        join cmpgrpcmps in dbContext.syCmpGrpCmps on cs.CampusId equals cmpgrpcmps.CampusId                        
                        where enr.StuEnrollId == stuEnrollId
                        select new TermOutput
                        {
                            TermId = t.TermId,
                            Description = t.TermDescrip

                        }).Distinct().OrderBy(n => n.Description).ToList();

          

            return data;
        }

        /// <summary>
        /// get the student schedule using the parameters passed in
        /// </summary>
        /// <param name="stuEnrollId"></param>
        /// <param name="instructorId"></param>
        /// <param name="reqId"></param>
        /// <param name="termId"></param>
        /// <param name="stDate"></param>
        /// <param name="eDate"></param>
        /// <returns></returns>
        public List<ScheduleOutput> GetStudentSchedule( Guid stuEnrollId, Guid? instructorId, Guid? reqId, Guid? termId, DateTime? stDate, DateTime? eDate )
        {
            var data = (from res in dbContext.arResults
                join enr in dbContext.arStuEnrollments on res.StuEnrollId equals enr.StuEnrollId
                join cs in dbContext.arClassSections on res.TestId equals cs.ClsSectionId
                join cst in dbContext.arClassSectionTerms on cs.ClsSectionId equals cst.ClsSectionId
                join t in dbContext.arTerms on cst.TermId equals t.TermId
                join r in dbContext.arReqs on cs.ReqId equals r.ReqId
                join cmpgrpcmps in dbContext.syCmpGrpCmps on cs.CampusId equals cmpgrpcmps.CampusId
                //join csm in _dbContext.arClsSectMeetings on res.TestId equals csm.ClsSectionId
                join u in dbContext.syUsers on cs.InstructorId equals u.UserId
                where enr.StuEnrollId == stuEnrollId
                select new ScheduleOutput
                {
                    SectionId = cs.ClsSectionId,
                    TermId = t.TermId,
                    Term = t.TermDescrip,
                    StartDate = cs.StartDate,
                    EndDate = cs.EndDate,
                    ReqId = r.ReqId,
                    Course = r.Descrip,
                    Code = r.Code,
                    Section = cs.ClsSection,
                    Credits = r.Credits,
                    Hours = r.Hours,
                    Duration = string.Format("{0} - {1}", cs.StartDate.ToShortDateString(),cs.EndDate.ToShortDateString()),
                    InstructorId = u.UserId,
                    Instructor = u.FullName,
                    GradeSystemDetailId = res.GrdSysDetailId
                });


            if (instructorId != null)
                data = data.Where(n => n.InstructorId == instructorId);

            if(reqId != null)
                data = data.Where(n => n.ReqId == reqId);

            if (termId != null)
                data = data.Where(n => n.TermId == termId);


            data = data.Distinct();


            var outputData = new List<ScheduleOutput>();
            if (stDate != null && eDate != null)
            {
                outputData.AddRange(data.ToList().Where(d => AllowedToAdd(stDate, eDate, d)));
                outputData = outputData.Distinct().OrderBy(n => n.StartDate).ToList();
            }
            else
            {
                outputData = data.Distinct().OrderByDescending(n => n.StartDate).ThenBy(n=>n.Course).ToList();
            }

            //remove the items that are dropped courses
            var outputData2 = new List<ScheduleOutput>();
            foreach (var d in outputData)
            {
                if (d.GradeSystemDetailId != null)
                {
                    var arGrSystemDtl = dbContext.arGradeSystemDetails.FirstOrDefault(n => n.GrdSysDetailId == d.GradeSystemDetailId);
                    var isDrop = arGrSystemDtl != null && arGrSystemDtl.IsDrop;
                    var grade = arGrSystemDtl != null ? arGrSystemDtl.Grade : "";

                    if (!isDrop)
                    { 
                        d.Grade = grade;
                        outputData2.Add(d);
                    }
                }
                else
                {
                    outputData2.Add(d);
                }
            }

            return outputData2;

        }


        /// <summary>
        /// get the room and period info for the specific class
        /// </summary>
        /// <param name="sectionId"></param>
        /// <param name="stuEnrollid"></param>
        /// <returns></returns>
        public List<ScheduleDetailOutput> GetScheduleDetail(Guid sectionId, Guid stuEnrollid)
        {
            var data = from res in dbContext.arResults
                       join enr in dbContext.arStuEnrollments on res.StuEnrollId equals enr.StuEnrollId
                       join cs in dbContext.arClassSections on res.TestId equals cs.ClsSectionId
                       join cst in dbContext.arClassSectionTerms on cs.ClsSectionId equals cst.ClsSectionId
                       join t in dbContext.arTerms on cst.TermId equals t.TermId
                       join r in dbContext.arReqs on cs.ReqId equals r.ReqId
                       join cmpgrpcmps in dbContext.syCmpGrpCmps on cs.CampusId equals cmpgrpcmps.CampusId
                       join csm in dbContext.arClsSectMeetings on res.TestId equals csm.ClsSectionId
                       join p in dbContext.syPeriods on csm.PeriodId equals p.PeriodId
                       where enr.StuEnrollId == stuEnrollid
                             && cs.ClsSectionId == sectionId
                       select new ScheduleDetailOutput
                       {
                           Building = csm.arRoom.arBuilding.BldgDescrip,
                           Period = p.PeriodDescrip,
                           Room = csm.arRoom.Descrip
                       };

            

            return data.Distinct().OrderBy(n => n.Period).ToList();
        }

        public List<EnrollmentOutput> GetNameAndNumber(string stuenrollid)
        {
            var data = (from e in dbContext.usp_getstudentnameandnumber(stuenrollid)
                        select new EnrollmentOutput
                        {
                            Name = e.Name,
                            Studentnumber = e.StudentNumber
                        }).ToList();

            return data;
        }

        public List<ConfigOutput> GetConfigSettings(string stuEnrollId)
        {
            var data = (from e in dbContext.usp_configsettings_getlist(stuEnrollId)
                        select new ConfigOutput
                        {
                            GradeCourseRepetitionsMethod = e.GradeCourseRepetitionsMethod,
                            IncludeHoursForFailingGrade = e.IncludeHoursForFailingGrade != null && (bool)e.IncludeHoursForFailingGrade,
                            GradesFormat = e.GradesFormat,
                            TranscriptType = e.TranscriptType,
                            AddCreditsByService = e.AddCreditsByService
                        }).ToList();

            return data;
        }

        #endregion

        #region Private Class Methods

        /// <summary>
        /// check if the start and end date range falls in the class start and end dates
        /// </summary>
        /// <param name="stDate"></param>
        /// <param name="eDate"></param>
        /// <param name="newItem"></param>
        /// <returns></returns>
        private bool AllowedToAdd( DateTime? stDate, DateTime? eDate, ScheduleOutput newItem)
        {
            return stDate <= newItem.StartDate &&
                   newItem.StartDate <= (eDate ?? DateTime.MaxValue)
                   ||
                   (stDate <= (newItem.EndDate ?? DateTime.MaxValue) &&
                    (newItem.EndDate ?? DateTime.MaxValue) <= (eDate ?? DateTime.MaxValue))
                   ||
                   (newItem.StartDate <= stDate &&
                    stDate <= (newItem.EndDate ?? DateTime.MaxValue))
                   ||
                   (newItem.StartDate <= (eDate ?? DateTime.MaxValue) &&
                    (eDate ?? DateTime.MaxValue) <= (newItem.EndDate ?? DateTime.MaxValue));
        }

        #endregion


    }
}