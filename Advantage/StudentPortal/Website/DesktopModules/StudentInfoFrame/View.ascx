﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Christoc.Modules.StudentInfoFrame.View" %>

<div>
    <table class="tblStyle">
        <tr>
            <td class="cssStudentName">
                <asp:Image ID="imgStudent" runat="server" />
            </td>
            <td class="cssStudentID">
            </td>
            <td class="cssEmail">
            </td>
            <td class="cssPhone">
            </td>
        </tr>
        <tr>
            <td class="cssStudentName">
                <asp:Label ID="lblStuName" runat="server" ForeColor="White" Font-Names="Arial" Font-Size="Medium" Font-Bold="True"></asp:Label>
            </td>
            <td class="cssStudentID">
                <asp:Label ID="lblStuID" runat="server" ForeColor="White" Font-Names="Arial" Font-Size="Medium" Font-Bold="True"></asp:Label>
            </td>
               <td class="cssEmail">
                <asp:Label ID="lblEmail" runat="server" ForeColor="White" Font-Names="Arial" Font-Size="Medium" Font-Bold="True"></asp:Label>
            </td>
            <td class="cssPhone">
                <asp:Label ID="lblPhone" runat="server" ForeColor="White" Font-Names="Arial" Font-Size="Medium" Font-Bold="True"></asp:Label>
            </td>
        </tr>
    </table>
</div>


