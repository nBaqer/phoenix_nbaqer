﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="View.ascx.cs" Inherits="Christoc.Modules.ApplicationStatus.View" %>
<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>


<telerik:radajaxmanager id="RadAjaxManager1" runat="server">
    <ajaxsettings>
        <telerik:AjaxSetting AjaxControlID="btnSumbit">
            <updatedcontrols>
                <telerik:AjaxUpdatedControl ControlID="lbltxtFNameErrMsg" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="lbltxtLNameErrMsg" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="lbltxtEmailErrMsg" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="lblNoRecords" UpdatePanelCssClass="" />
                <telerik:AjaxUpdatedControl ControlID="rgdDocs" UpdatePanelCssClass="" />
            </updatedcontrols>
        </telerik:AjaxSetting>
    </ajaxsettings>
</telerik:radajaxmanager>


<br />
<div>
    <h2>Application Status</h2>
    <p>
        <span style="font-size: 14px;" > Thank you for considering our school for your educational needs. Please check your submitted application's documents status below.</span>
        <br/><br/>
    </p>
</div>

<div class="tblHeader">
    <asp:Label ID="lblHeader" runat="server" Text="Check Application Status" ForeColor="White" Font-Bold="True" Font-Names="Arial" Font-Size="Medium"></asp:Label>
</div>
<div class="mainTable">
    <table>
        <tr style="height:45px;">
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 130px;">
                <asp:Label ID="lblAsterik1" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red">* </asp:Label>
                <asp:Label ID="lblFName" runat="server" Font-Size="10pt" Font-Bold="True" Font-Names="Arial" Width="100px"> First Name :   </asp:Label>
            </td>
            <td style="width: 275px">
                <telerik:radtextbox id="txtFName" runat="server" maxlength="50" width="250px" wrap="False" font-names="Arial" font-size="12pt" skin="Metro"></telerik:radtextbox>
            </td>
            <td style="width: 225px">
                <asp:RequiredFieldValidator ID="valFirstName" runat="server" ControlToValidate="txtFName" Display="Dynamic" ErrorMessage="First Name is required" Font-Size="12px" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 130px;">
                <asp:Label ID="lblAsterik2" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red">* </asp:Label>
                <asp:Label ID="lblLName" runat="server" Font-Size="10pt" Font-Bold="True" Font-Names="Arial" Width="100px"> Last Name :   </asp:Label>
            </td>
            <td style="width: 275px">
                <telerik:radtextbox id="txtLName" runat="server" maxlength="50" width="250px" wrap="False" font-names="Arial" font-size="12pt" skin="Metro"></telerik:radtextbox>
            </td>
            <td style="width: 225px">
                <asp:RequiredFieldValidator ID="valLastName" runat="server" ControlToValidate="txtLName" Display="Dynamic" ErrorMessage="Last Name is required" Font-Size="12px" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 130px;">
                <asp:Label ID="lblAsterik3" runat="server" Font-Bold="False" Font-Names="Arial" Font-Size="12pt" ForeColor="Red">* </asp:Label>
                <asp:Label ID="lblEmail" runat="server" Font-Size="10pt" Font-Bold="True" Font-Names="Arial" Width="100px"> Email :   </asp:Label>
            </td>
            <td style="width: 275px">
                <telerik:radtextbox id="txtEmail" runat="server" maxlength="50" width="250px" wrap="False" font-names="Arial" font-size="12pt" skin="Metro"></telerik:radtextbox>
            </td>
            <td style="width: 225px">
                <asp:RequiredFieldValidator ID="valEmail" runat="server" ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Email is required" Font-Size="12px" ForeColor="Red"></asp:RequiredFieldValidator>
            </td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 120px;"></td>
            <td style="width: 225px; text-align: center;">
                <asp:Button ID="btnSumbit" runat="server" CssClass="btnSave" Text="Check Status" OnClick="btnSumbit_Click" />
            </td>
            <td style="width: 225px"></td>
        </tr>
        <tr>
            <td>&nbsp;
            </td>
        </tr>
    </table>
    <p>
        <asp:Label ID="lblNoRecords" runat="server" Font-Names="Arial" Font-Size="10pt" ForeColor="#000099" Font-Bold="True"></asp:Label>
    </p>
    <p>
        <br />  
    </p>
</div>















































