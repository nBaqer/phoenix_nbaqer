﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="R2T4Calculator.aspx.cs" Inherits="R2T4Calculator.R2T4Calculator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>Florida Association for Media in Education</title>
    <!-- Bootstrap core CSS -->
    <link href="Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/css/custom.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>
    <script src="Scripts/AppScripts/AppJs.js"></script>
    <script>
        $(document).ready(function () {
            $('.clickable').on('click', function () {
                $('.clickable').not(this).next('.panel-body').addClass('show');
                $('.clickable').not(this).find('i').addClass('fa-plus').removeClass('fa-minus');

                $(this).next('.panel-body').toggleClass('show');
                $(this).find('i').toggleClass('fa-plus fa-minus');
            });

            $('#btnCalculate').click(function () {
                calculateStep1();
            });

        });
    </script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="row parent">
                <div class="page-header">
                    <img src="Content/images/logo.png" />
                    <p class="title">Treatment of Title IV Funds When a Student Withdraws from a Clock-Hour Program</p>
                </div>
                <div class="well well-sm">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group-sm">
                                    <label>Student's Name</label>
                                    <input type="text" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group-sm">
                                    <label>Social Security Number</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group-sm">
                                    <label>Date Form Completed</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="col-md-4">
                                <div class="form-group-sm">
                                    <label>
                                        Date of school's determination<br>
                                        that student withdrew</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control">
                                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Period used for calculation (check one)</label>
                                <div class="clearfix"></div>
                                <input type="checkbox" checked>
                                Payment period
                                <input type="checkbox">
                                Period of enrollment
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="row bottom-no-margin">
                    <div class="col-md-12">
                        <ul class="list-group">
                            <li class="list-group-item">Monetary amounts should be in dollars and cents (rounded to the nearest penny).</li>
                            <li class="list-group-item">When calculating percentages, round to three decimal places.  (For example, .4486 would be .449, or 44.9%)</li>
                        </ul>
                    </div>
                </div>
                <div class="alert alert-success">
                    <strong>Success!</strong> Indicates Success Message <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                </div>
                <div class="row">
                    <div class="col-md-8 no-padding">
                        <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong><span class="badge">STEP 1:</span> Student's Title IV Aid Information</strong></div>
                                <div class="panel-body no-padding">
                                    <div class="col-md-12 no-padding">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped bottom-no-margin">
                                                <thead>
                                                    <tr>
                                                        <th width="3%"></th>
                                                        <th width="20%">Title IV Grant Programs</th>
                                                        <th width="20%" class="non-bold-sm">Amount Disbursed</th>
                                                        <th width="20%" class="non-bold-sm">Amount That Could Have Been Disbursed</th>
                                                        <th width="37%"><span class="badge">E</span> Total Title IV aid disbursed for the payment period or period of enrollment.</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1.</td>
                                                        <td>Pell Grant</td>
                                                        <td>
                                                            <asp:TextBox ID="txtPellA" runat="server" CssClass="form-control input-xs" placeholder="0.0" />
                                                            <%-- <input type="text" class="form-control input-xs input-field-error"  placeholder="0.0"><span class="field-error">Field level error message</span></td>--%>
                                                            <td>
                                                                <asp:TextBox ID="txtPellB" runat="server" class="form-control input-xs" placeholder="0.0" /></td>
                                                            <td rowspan="3">
                                                                <table class="table">
                                                                    <tbody>
                                                                        <tr>
                                                                            <th width="5%"></th>
                                                                            <th width="10%">A.</th>
                                                                            <td width="85%">
                                                                                <asp:TextBox ID="txtEA" runat="server" type="text" disabled="disabled" class="form-control input-xs" placeholder="0.0" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th width="5%"><i class="fa fa-plus"></i></th>
                                                                            <th width="10%">B.</th>
                                                                            <td width="85%">
                                                                                <asp:TextBox ID="txtEB" runat="server" disabled="disabled" class="form-control input-xs" placeholder="0.0" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th width="5%">
                                                                                <h4>=</h4>
                                                                            </th>
                                                                            <th width="10%">E.</th>
                                                                            <td width="85%">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                                                    <asp:TextBox ID="txtResultE" runat="server" disabled="disabled" class="form-control input-xs" placeholder="0.0" />
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2.</td>
                                                        <td>Academic Competitiveness Grant</td>
                                                        <td>
                                                            <asp:TextBox ID="txtAcademicA" runat="server" class="form-control input-xs" placeholder="0.0" /></td>
                                                        <td>
                                                            <asp:TextBox ID="txtAcademicB" runat="server" class="form-control input-xs" placeholder="0.0" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3.</td>
                                                        <td>National SMART Grant</td>
                                                        <td>
                                                            <asp:TextBox ID="txtSmartA" runat="server" class="form-control input-xs" placeholder="0.0" /></td>
                                                        <td>
                                                            <asp:TextBox ID="txtSmartB" runat="server" class="form-control input-xs" placeholder="0.0" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>4.</td>
                                                        <td>FSEOG</td>
                                                        <td>
                                                            <asp:TextBox ID="txtFSEOGA" runat="server" class="form-control input-xs" placeholder="0.0" /></td>
                                                        <td>
                                                            <asp:TextBox ID="txtFSEOGB" runat="server" class="form-control input-xs" placeholder="0.0" /></td>
                                                        <td><span class="badge">F</span> <strong>Total Title IV grant aid disbursed and that could have been disbursed for the period.</strong></td>
                                                    </tr>
                                                    <tr>
                                                        <td>5.</td>
                                                        <td>TEACH Grant</td>
                                                        <td>
                                                            <asp:TextBox ID="txtTeachA" runat="server" class="form-control input-xs" placeholder="0.0" /></td>
                                                        <td>
                                                            <asp:TextBox ID="txtTeachB" runat="server" class="form-control input-xs" placeholder="0.0" /></td>
                                                        <td rowspan="3">
                                                            <table class="table">
                                                                <tbody>
                                                                    <tr>
                                                                        <th width="5%"></th>
                                                                        <th width="10%">A.</th>
                                                                        <td width="85%">
                                                                            <asp:TextBox ID="txtFA" runat="server" disabled="disabled" class="form-control input-xs" placeholder="0.0" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="5%"><i class="fa fa-plus"></i></th>
                                                                        <th width="10%">C.</th>
                                                                        <td width="85%">
                                                                            <asp:TextBox ID="txtFC" runat="server" disabled="disabled" class="form-control input-xs" value="878.00" placeholder="0.0" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="5%">
                                                                            <h4>=</h4>
                                                                        </th>
                                                                        <th width="10%">F.</th>
                                                                        <td width="85%">
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                                                <input type="text" disabled="disabled" class="form-control input-xs" value="6,798.00" placeholder="0.0">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>6.</td>
                                                        <td>Iraq Afghanistan Service Grant</td>
                                                        <td>
                                                            <asp:TextBox ID="txtIraqAfganA" runat="server" class="form-control input-xs" placeholder="0.0" /></td>
                                                        <td>
                                                            <asp:TextBox ID="txtIraqAfganB" runat="server" class="form-control input-xs" placeholder="0.0" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2"></td>
                                                        <td align="center">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><strong>A</strong></span>
                                                                <asp:TextBox ID="txtSubTotalA" runat="server" class="form-control input-xs" placeholder="0.0" />
                                                            </div>
                                                            <span>Subtotal</span>
                                                        </td>
                                                        <td align="center">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><strong>C</strong></span>
                                                                <asp:TextBox ID="txtSubTotalB" runat="server" class="form-control input-xs" placeholder="0.0" />
                                                            </div>
                                                            <span>Subtotal</span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                <thead>
                                                    <tr>
                                                        <th width="3%"></th>
                                                        <th width="20%">Title IV Loan Programs</th>
                                                        <th width="20%" class="non-bold-sm">Net Amount Disbursed</th>
                                                        <th width="20%" class="non-bold-sm">Net Amount That Could Have  Been Disbursed</th>
                                                        <th width="37%"><span class="badge">G</span> Total Title IV aid disbursed and that could have been disbursed for the period.</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>7.</td>
                                                        <td>Unsubsidized FFEL/Direct Stafford Loan</td>
                                                        <td>
                                                            <input type="text" class="form-control input-xs" value="1,000.00" placeholder="0.0"></td>
                                                        <td>
                                                            <input type="text" class="form-control input-xs" value="969.00" placeholder="0.0"></td>
                                                        <td rowspan="6">
                                                            <table class="table">
                                                                <tbody>
                                                                    <tr>
                                                                        <th width="5%"></th>
                                                                        <th width="10%">A.</th>
                                                                        <td width="85%">
                                                                            <input type="text" disabled="disabled" class="form-control input-xs" placeholder="0.0"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="5%"></th>
                                                                        <th width="10%">B.</th>
                                                                        <td width="85%">
                                                                            <input type="text" disabled="disabled" class="form-control input-xs" placeholder="0.0"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="5%"></th>
                                                                        <th width="10%">C.</th>
                                                                        <td width="85%">
                                                                            <input type="text" disabled="disabled" class="form-control input-xs" placeholder="0.0"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="5%"><i class="fa fa-plus"></i></th>
                                                                        <th width="10%">D.</th>
                                                                        <td width="85%">
                                                                            <input type="text" disabled="disabled" class="form-control input-xs" placeholder="0.0"></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="5%">
                                                                            <h4>=</h4>
                                                                        </th>
                                                                        <th width="10%">G.</th>
                                                                        <td width="85%">
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                                                <input type="text" disabled="disabled" class="form-control input-xs" placeholder="0.0">
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <asp:Button ID="btnCalculate" runat="server" Text="Calculate" OnClick="btnCalculate_Click" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>8.</td>
                                                        <td>Subsidized FFEL/Direct Stafford Loan</td>
                                                        <td>
                                                            <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                                        <td>
                                                            <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>9.</td>
                                                        <td>Perkins Loan</td>
                                                        <td>
                                                            <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                                        <td>
                                                            <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>10.</td>
                                                        <td>FFEL/Direct PLUS (Graduate Student)</td>
                                                        <td>
                                                            <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                                        <td>
                                                            <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                                    </tr>
                                                    <tr>
                                                        <td>11.</td>
                                                        <td>FFEL/Direct PLUS (Parent)</td>
                                                        <td>
                                                            <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                                        <td>
                                                            <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2"></td>
                                                        <td align="center">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><strong>B</strong></span>
                                                                <input type="text" class="form-control input-xs" value="1,000.00" placeholder="0.0">
                                                            </div>
                                                            <span>Subtotal</span>
                                                        </td>
                                                        <td align="center">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><strong>D</strong></span>
                                                                <input type="text" class="form-control input-xs" placeholder="0.0">
                                                            </div>
                                                            <span>Subtotal</span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <%--<div class="col-md-12"><h3>Balance Remaining: <i class="fa fa-usd"></i> 350.90</h3></div>--%>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading clickable"><strong><span class="badge">STEP 2:</span> Percentage of Title IV Aid Earned</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                            <div class="panel-body no-padding">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th width="3%"></th>
                                                <th width="97%" colspan="5">Withdrawal date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th>H.</th>
                                                <td colspan="5"><strong>Determine the percentage of the period completed:</strong></td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td colspan="5">Divide the clock hours scheduled to have been completed as of the withdrawal date in the period the total clock hours in the period.</td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td>
                                                    <input type="text" class="form-control input-xs" value="130" placeholder="0.0"></td>
                                                <td>
                                                    <img src="images/divider.png"></td>
                                                <td>
                                                    <input type="text" class="form-control input-xs" value="450" placeholder="0.0"></td>
                                                <td>
                                                    <h4>=</h4>
                                                </td>
                                                <td>
                                                    <input type="text" disabled="disabled" class="form-control input-xs" value="0.28889" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <th><i class="fa fa-hand-o-right"></i></th>
                                                <td colspan="5">If this percentage is greater than 60%, enter 100% in box H and proceed to Step 3.</td>
                                            </tr>
                                            <tr>
                                                <th><i class="fa fa-hand-o-right"></i></th>
                                                <td colspan="5">If this percentage is less than or equal to 60%, enter that percentage in Box H, and proceed to Step 3.</td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td colspan="5">
                                                    <div class="input-group">
                                                        <div class="input-group-addon">H</div>
                                                        <input type="text" class="form-control input-xs" value="28.9" placeholder="0.0" disabled="disabled">
                                                        <div class="input-group-addon input-group-addon-nobg">%</div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading clickable"><strong><span class="badge">STEP 3:</span> Amount of Title IV Aid Earned by the Student</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                            <div class="panel-body no-padding">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tbody>
                                            <tr>
                                                <th></th>
                                                <td colspan="6">Multiply the percentage of Title IV aid earned  (Box H)  by the total of the Title IV aid disbursed and the Title IV aid that could have been disbursed for the period (Box G).</td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td>
                                                    <input type="text" class="form-control input-xs" value="28.9" placeholder="0.0"></td>
                                                <td>
                                                    <h4>x</h4>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control input-xs" value="10,499.00" placeholder="0.0"></td>
                                                <td>
                                                    <h4>=</h4>
                                                </td>
                                                <td><strong>I.</strong></td>
                                                <td>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                        <input type="text" class="form-control input-xs" value="3,034.21" placeholder="0.0" disabled="disabled">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td>Box H</td>
                                                <td></td>
                                                <td>Box G</td>
                                                <td colspan="3"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading clickable"><strong><span class="badge">STEP 4:</span> Total Title IV Aid to be Disbursed or Returned</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                            <div class="panel-body no-padding">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tbody>
                                            <tr>
                                                <th><i class="fa fa-hand-o-right"></i></th>
                                                <td colspan="6">If the amount in Box I is greater than the amount in Box E, go to Post-withdrawal disbursement (Item J).</td>
                                            </tr>
                                            <tr>
                                                <th><i class="fa fa-hand-o-right"></i></th>
                                                <td colspan="6">If the amount in Box I is less than the amount in Box E, go to Title IV aid to be returned (Item K).</td>
                                            </tr>
                                            <tr>
                                                <th>J.</th>
                                                <td colspan="6"><strong>Post-withdrawal disbursement</strong></td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td colspan="6">From the Amount of Title IV aid earned by the student (Box I) subtract the Total Title IV aid disbursed for the period (Box E).  This is the amount of the post-withdrawal disbursement. Stop here, and enter the amount in Box 1 on Page 3 (Post-withdrawal disbursement tracking sheet).</td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td>
                                                    <input type="text" class="form-control input-xs" placeholder="0.0" disabled="disabled"></td>
                                                <td><i class="fa fa-minus"></i></td>
                                                <td>
                                                    <input type="text" class="form-control input-xs" placeholder="0.0" disabled="disabled"></td>
                                                <td>
                                                    <h4>=</h4>
                                                </td>
                                                <td><strong>J.</strong></td>
                                                <td>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                        <input type="text" class="form-control input-xs" placeholder="0.0" disabled="disabled">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td>Box I</td>
                                                <td></td>
                                                <td>Box E</td>
                                                <td colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <th>K.</th>
                                                <td colspan="6"><strong>Title IV aid to be returned</strong></td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td colspan="6">Subtract the amount of Title IV aid earned (Box I) from the Total Title IV aid disbursed for the period (Box E). This is the amount of Title IV aid that must be returned.</td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td>
                                                    <input type="text" class="form-control input-xs" value="6,920.00" placeholder="0.0" disabled="disabled"></td>
                                                <td><i class="fa fa-minus"></i></td>
                                                <td>
                                                    <input type="text" class="form-control input-xs" value="3,034.21" placeholder="0.0" disabled="disabled"></td>
                                                <td>
                                                    <h4>=</h4>
                                                </td>
                                                <td><strong>K.</strong></td>
                                                <td>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                        <input type="text" class="form-control input-xs" value="3,885.79" placeholder="0.0" disabled="disabled">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td>Box E</td>
                                                <td></td>
                                                <td>Box I</td>
                                                <td colspan="3"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading clickable"><strong><span class="badge">STEP 5:</span> Amount of Unearned Title IV Aid Due from the School</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                            <div class="panel-body no-padding">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tbody>
                                            <tr>
                                                <th>L</th>
                                                <td colspan="2">Institutional charges for the payment period or period of enrollment</td>
                                                <td>Tuition</td>
                                                <td colspan="3">
                                                    <input type="text" class="form-control input-xs" value="1,000.00" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td colspan="2"></td>
                                                <td>Room</td>
                                                <td colspan="3">
                                                    <input type="text" class="form-control input-xs" value="200.00" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td colspan="2"></td>
                                                <td>Board</td>
                                                <td colspan="3">
                                                    <input type="text" class="form-control input-xs" value="200.00" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td colspan="2" align="right"><i class="fa fa-minus"></i></td>
                                                <td>Other</td>
                                                <td colspan="3">
                                                    <input type="text" class="form-control input-xs" value="500.00" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td colspan="2"><strong>Total Institutional Charges</strong><br>
                                                    (Add all the charges together)</td>
                                                <td align="right"><strong>L.</strong></td>
                                                <td colspan="3">
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                        <input type="text" class="form-control input-xs" value="1,900.00" placeholder="0.0" disabled="disabled">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>M.</th>
                                                <td colspan="6"><strong>Percentage of unearned Title IV aid</strong></td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td>100%</td>
                                                <td><i class="fa fa-minus"></i></td>
                                                <td>28.9%</td>
                                                <td>
                                                    <h4>=</h4>
                                                </td>
                                                <td><strong>M.</strong></td>
                                                <td>71.1%</td>
                                            </tr>
                                            <tr>
                                                <td colspan="7" align="center">Box H</td>
                                            </tr>
                                            <tr>
                                                <th>N.</th>
                                                <td colspan="6"><strong>Amount of unearned charges</strong></td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td colspan="6">Multiply institutional charges for the period (Box L) times the percentage of unearned Title IV aid (Box M)</td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td><strong>1,900.00</strong></td>
                                                <td>
                                                    <h4>x</h4>
                                                </td>
                                                <td><strong>71.1%</strong></td>
                                                <td>
                                                    <h4>=</h4>
                                                </td>
                                                <td><strong>N.</strong></td>
                                                <td nowrap="nowrap"><i class="fa fa-usd"></i><strong>1,350,00</strong></td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td>Box L</td>
                                                <td></td>
                                                <td>Box M</td>
                                                <td colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <th>O.</th>
                                                <td colspan="6"><strong>Amount for school to return</strong></td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td colspan="6">Compare the amount of Title IV aid to be returned (Box K) to amount of unearned charges (Box N),and enter the lesser amount.</td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td colspan="5" align="right"><strong>O.</strong></td>
                                                <td nowrap="nowrap"><i class="fa fa-usd"></i><strong>1,350,00</strong></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 no-padding">
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading clickable"><strong><span class="badge">STEP 6:</span> Return of Funds by the School</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                            <div class="panel-body no-padding">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tbody>
                                            <tr>
                                                <td colspan="4">The school must return the unearned aid for which the school is responsible (Box O) by repaying funds to the following sources, in order, up to the total net amount disbursed from each source.</td>
                                            </tr>
                                            <tr>
                                                <th width="3%"></th>
                                                <th width="47%">Title IV Programs</th>
                                                <th width="50%" colspan="2" class="non-bold-sm">Amount for School to Return</th>
                                            </tr>
                                            <tr>
                                                <td>1.</td>
                                                <td>Unsubsidized FFEL/Direct Stafford Loan</td>
                                                <td colspan="2">
                                                    <input type="text" class="form-control input-xs" value="1,000.00" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <td>2.</td>
                                                <td>Subsidized FFEL/Direct Stafford Loan</td>
                                                <td colspan="2">
                                                    <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <td>3.</td>
                                                <td>Perkins Loan</td>
                                                <td colspan="2">
                                                    <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <td>4.</td>
                                                <td>FFEL/Direct PLUS (Graduate Student)</td>
                                                <td colspan="2">
                                                    <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <td>5.</td>
                                                <td>FFEL/Direct PLUS (Parent)</td>
                                                <td colspan="2">
                                                    <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td><strong>Total loans the school must return</strong></td>
                                                <td><strong>P.</strong></td>
                                                <td>
                                                    <div class="input-group">
                                                        <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                        <input type="text" class="form-control input-xs" placeholder="0.0" value="1,000.00" disabled="disabled">
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>6.</td>
                                                <td>Pell Grant</td>
                                                <td colspan="2">
                                                    <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <td>7.</td>
                                                <td>Academic Competitiveness Grant</td>
                                                <td colspan="2">
                                                    <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <td>8.</td>
                                                <td>National SMART Grant</td>
                                                <td colspan="2">
                                                    <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <td>9.</td>
                                                <td>FSEOG</td>
                                                <td colspan="2">
                                                    <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <td>10.</td>
                                                <td>TEACH Grant</td>
                                                <td colspan="2">
                                                    <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <td>11.</td>
                                                <td>Iraq Afghanistan Service Grant</td>
                                                <td colspan="2">
                                                    <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading clickable"><strong><span class="badge">STEP 7:</span> Initial Amount of Unearned Title IV Aid Due from the Student</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                            <div class="panel-body no-padding">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tbody>
                                            <tr>
                                                <td colspan="7">Subtract the amount of Title IV aid due from the school (Box O) from the amount of Title IV aid to be returned (Box K).</td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td><strong>3,885.79</strong></td>
                                                <td><i class="fa fa-minus"></i></td>
                                                <td><strong>1,350.90</strong></td>
                                                <td>
                                                    <h4>=</h4>
                                                </td>
                                                <td><strong>Q.</strong></td>
                                                <td nowrap="nowrap"><i class="fa fa-usd"></i><strong>2,534.89</strong></td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td>Box K</td>
                                                <td></td>
                                                <td>Box O</td>
                                                <td colspan="3"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading clickable"><strong><span class="badge">STEP 8:</span> Repayment of the Student's loans</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                            <div class="panel-body no-padding">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tbody>
                                            <tr>
                                                <th></th>
                                                <td colspan="6">Subtract the Total loans the school must return (Box P) from the Net loans disbursed to the student (Box B) to find the amount of Title IV loans the student is still responsible for repaying (Box R).</td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td colspan="6">These outstanding loans consist either of loan funds the student has earned, or unearned loan funds that the school is not responsible for repaying, or both; and they are repaid to the loan holders according to the terms of the borrower's promissory note.</td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td><strong>1,000.00</strong></td>
                                                <td><i class="fa fa-minus"></i></td>
                                                <td><strong>1,000.00</strong></td>
                                                <td>
                                                    <h4>=</h4>
                                                </td>
                                                <td><strong>R.</strong></td>
                                                <td><i class="fa fa-usd"></i>0.0</td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td>Box B</td>
                                                <td></td>
                                                <td>Box P</td>
                                                <td colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <th><i class="fa fa-hand-o-right"></i></th>
                                                <td colspan="6">If Box Q is less than or equal to Box R, <strong>STOP</strong>.<br>
                                                    <em>The only action a school must take is to notify the holders of the loans of the student's withdrawal date.</em></td>
                                            </tr>
                                            <tr>
                                                <th><i class="fa fa-hand-o-right"></i></th>
                                                <td colspan="6">If Box Q is greater than Box R, proceed to Step 9.X116</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading clickable"><strong><span class="badge">STEP 9:</span> Grant Funds to be Returned</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                            <div class="panel-body no-padding">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tbody>
                                            <tr>
                                                <th>S.</th>
                                                <th colspan="6">Initial amount of Title IV grants for student to return</th>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td colspan="6">Subtract the amount of loans to be repaid by the student (Box R) from the initial amount of unearned Title IV aid due from the  student (Box Q).</td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td><strong>2,534.89</strong></td>
                                                <td><i class="fa fa-minus"></i></td>
                                                <td><strong>0.00</strong></td>
                                                <td>
                                                    <h4>=</h4>
                                                </td>
                                                <td><strong>S.</strong></td>
                                                <td><i class="fa fa-usd"></i>2,534.89</td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td>Box Q</td>
                                                <td></td>
                                                <td>Box R</td>
                                                <td colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <th>T.</th>
                                                <th colspan="6">Amount of Title IV grant protection</th>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td colspan="6">Multiply the total of Title IV grant aid that was disbursed and could have been disbursed for the payment period or period of enrollment (Box F) by 50%.</td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td><strong>6,798.00</strong></td>
                                                <td>
                                                    <h4>x</h4>
                                                </td>
                                                <td><strong>50%</strong></td>
                                                <td>
                                                    <h4>=</h4>
                                                </td>
                                                <td><strong>T.</strong></td>
                                                <td><i class="fa fa-usd"></i>3,399.00</td>
                                            </tr>
                                            <tr>
                                                <td colspan="7" align="center">Box F</td>
                                            </tr>
                                            <tr>
                                                <th>U.</th>
                                                <th colspan="6">Title IV grant funds for student to return</th>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td colspan="6">Subtract the protected amount of Title IV grants (Box T) from the initial amount of Title IV grants for student to return (Box S).</td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td><strong>2,534.89</strong></td>
                                                <td><i class="fa fa-minus"></i></td>
                                                <td><strong>3,399.00</strong></td>
                                                <td>
                                                    <h4>=</h4>
                                                </td>
                                                <td><strong>U.</strong></td>
                                                <td><span class="red"><i class="fa fa-usd"></i>-864.11</span></td>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <td>Box S</td>
                                                <td></td>
                                                <td>Box T</td>
                                                <td colspan="3"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading clickable"><strong><span class="badge">STEP 10:</span> Return of Grant Funds by the Student</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                            <div class="panel-body no-padding">
                                <div class="table-responsive">
                                    <table class="table table-bordered table-striped">
                                        <tbody>
                                            <tr>
                                                <td colspan="3">Except as noted below, the student must return the unearned grant funds for which he or she is responsible (Box U). The grant funds returned by the student are applied to the following sources in the order indicated, up to the total amount disbursed from that grant program minus any grant funds the school is responsible for returning to that program in Step 6.</td>
                                            </tr>
                                            <tr>
                                                <th colspan="3">Note that the student is not responsible for returning funds to any program to which the student owes $50.00 or less.</th>
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <th colspan="2">Title IV Grant Programs</th>
                                            </tr>
                                            <tr>
                                                <td width="3%">1.</td>
                                                <td>Pell Grant</td>
                                                <td>
                                                    <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <td width="3%">2.</td>
                                                <td>Academic Competitiveness Grant</td>
                                                <td>
                                                    <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <td width="3%">3.</td>
                                                <td>National SMART Grant</td>
                                                <td>
                                                    <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <td width="3%">4.</td>
                                                <td>FSEOG</td>
                                                <td>
                                                    <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <td width="3%">5.</td>
                                                <td>TEACH Grant</td>
                                                <td>
                                                    <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                            </tr>
                                            <tr>
                                                <td width="3%">6.</td>
                                                <td>Iraq Afghanistan Service Grant</td>
                                                <td>
                                                    <input type="text" class="form-control input-xs" placeholder="0.0"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <h3>Balance Remaining: <i class="fa fa-usd"></i>350.90</h3>
                </div>
            </div>
        </div>
        <hr style="margin-bottom: 0;">
        <div class="footer">
            Copyright &copy;2017, Florida Association for Media in Education
        </div>
        </div>
        <!-- /container -->
    </form>
</body>
</html>
