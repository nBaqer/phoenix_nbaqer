﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(R2T4Calculator.Startup))]
namespace R2T4Calculator
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
