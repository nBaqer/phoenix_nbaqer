﻿using System;
using System.Collections.Generic;
using System.Globalization;
using R2T4Engine;
using R2T4Engine.Entities;
using R2T4Engine.InputOutput;
using R2T4Engine.Interfaces;
using Microsoft.Practices.Unity;
using R2T4Engine.Steps;
using R2T4Engine.Common;

namespace R2T4Calculator
{
    public partial class R2T4Calculator : System.Web.UI.Page
    {
        R2T4Input inputInfo;
        Step1 step;
        R2T4Facade facade = new R2T4Facade();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (hdnNoAttend.Value == "noAttend")
            {
                chkNAttend.Checked = true;
            }
            else
            {
                chkNAttend.Checked = false;

            }
            //if (hdnTuiton.Value == "checked")
            //{
            //    chkTuitionCharged.Checked = true;
            //}
            //else
            //    chkTuitionCharged.Checked = false;
        }

        public void calculate()
        {
            DependecyManager.ManageDi();
            step = DependecyManager.Container.Resolve<Step1>();
            inputInfo = DependecyManager.Container.Resolve<R2T4Input>();
            if (rdbClock.Checked)
                inputInfo.ProgramType = ProgramType.ClockHour;
            else if (rdbCredit.Checked)
                inputInfo.ProgramType = ProgramType.CreditHour;

            if (rdbPayPeriod.Checked)
                inputInfo.PaymentType = PaymentTypes.PaymentPeriod;
            else if (rdbPeriodEnroll.Checked)
                inputInfo.PaymentType = PaymentTypes.PeriodOfEnrollment;

            #region student
            Student student = new Student();
            if (rdbClock.Checked)
            {
                student.ScheduledHours = Convert.ToInt32(txtHoursCompleted.Text);
                student.TotalHours = Convert.ToInt32(txttotalHours.Text);
            }
            else if (rdbCredit.Checked)
            {
                if (!chkNAttend.Checked)
                {
                    student.IsAttendanceRequired = true;
                    student.ScheduledDays = Convert.ToInt32(txtCompletedDays.Text);
                    student.TotalDays = Convert.ToInt32(txtTotalDays.Text);
                }
                else
                {
                    student.IsAttendanceRequired = false;
                    student.ScheduledDays = 0.00m;
                    student.TotalDays = 0.00m;
                }
            }

            student.InstitutionalCharges = new List<InstitutionalCharges>();


            if (txt5Tuition.Text != string.Empty)
            {
                InstitutionalCharges charge = new InstitutionalCharges("Tution", Convert.ToDecimal(txt5Tuition.Text.ToString()));
                student.InstitutionalCharges.Add(charge);
            }
            if (txt5Room.Text != string.Empty)
            {
                InstitutionalCharges charge = new InstitutionalCharges("Room", Convert.ToDecimal(txt5Room.Text.ToString()));
                student.InstitutionalCharges.Add(charge);
            }
            if (txt5Board.Text != string.Empty)
            {
                InstitutionalCharges charge = new InstitutionalCharges("Board", Convert.ToDecimal(txt5Board.Text.ToString()));
                student.InstitutionalCharges.Add(charge);
            }
            if (txt5Other.Text != string.Empty)
            {
                InstitutionalCharges charge = new InstitutionalCharges("Other", Convert.ToDecimal(txt5Other.Text.ToString()));
                student.InstitutionalCharges.Add(charge);
            }
            if (chkTuitionCharged.Checked)
            {
                student.IsTutionCharged = false;
            }
            else
            {
                student.IsTutionCharged = true;
                if (txtNRefunded.Text != string.Empty)
                    student.CreditBalanceRefunded = Convert.ToDecimal(txtNRefunded.Text);
                else
                    student.CreditBalanceRefunded = 0.00m;
            }
            #endregion

            #region grants

            int grantPriority = 0;
            List<TitleIVGrants> lstGrants = new List<TitleIVGrants>();
            TitleIVGrants pellGrant = new TitleIVGrants();
            pellGrant.GrantName = "Pell";
            if (txtPellA.Text != string.Empty)
            {
                pellGrant.AmountDisbursed = Convert.ToDecimal(txtPellA.Text);              
               
            }
            else
                pellGrant.AmountDisbursed = 0.00m;

            if (txtPellB.Text != string.Empty)
                pellGrant.AmountExpected = Convert.ToDecimal(txtPellB.Text);
            else
                pellGrant.AmountExpected = 0.00m;
            pellGrant.Priority = 1;
            lstGrants.Add(pellGrant);

            TitleIVGrants FSEOG = new TitleIVGrants();
            FSEOG.GrantName = "FSEOG";
            if (txtFSEOGA.Text != string.Empty)
            {
                FSEOG.AmountDisbursed = Convert.ToDecimal(txtFSEOGA.Text);
               
                //FSEOG.Priority = grantPriority + 1;
                //grantPriority += 1;
            }
            else
                FSEOG.AmountDisbursed = 0.00m;
            if (txtFSEOGB.Text != string.Empty)
                FSEOG.AmountExpected = Convert.ToDecimal(txtFSEOGB.Text);
            else
                FSEOG.AmountExpected = 0.00m;
            FSEOG.Priority = 2;
            lstGrants.Add(FSEOG);

            TitleIVGrants teach = new TitleIVGrants();
            teach.GrantName = "TEACH Grant";
            if (txtTeachA.Text != string.Empty)
            {
                teach.AmountDisbursed = Convert.ToDecimal(txtTeachA.Text);               
                //teach.Priority = grantPriority + 1;
                //grantPriority += 1;
            }
            else
                teach.AmountDisbursed = 0.00m;
            if (txtTeachB.Text != string.Empty)
                teach.AmountExpected = Convert.ToDecimal(txtTeachB.Text);
            else
                teach.AmountExpected = 0.00m;
            teach.Priority = 3;
            lstGrants.Add(teach);

            TitleIVGrants IASG = new TitleIVGrants();
            IASG.GrantName = "Iraq Afghanistan Service Grant";
            if (txtIraqAfganA.Text != string.Empty)
            {
                IASG.AmountDisbursed = Convert.ToDecimal(txtIraqAfganA.Text);
                //IASG.Priority = 4;
                //IASG.Priority = grantPriority + 1;
                //grantPriority += 1;
            }
            else
                IASG.AmountDisbursed = 0.00m;
            if (txtIraqAfganB.Text != string.Empty)
                IASG.AmountExpected = Convert.ToDecimal(txtIraqAfganB.Text);
            else
                IASG.AmountExpected = 0.00m;
            IASG.Priority = 4;
            lstGrants.Add(IASG);
            #endregion

            #region loans
            int loanPriority = 0;
            List<TitleIVLoans> Loans = new List<TitleIVLoans>();
            TitleIVLoans unSubFFEL = new TitleIVLoans();
            unSubFFEL.LoanName = "Unsubsidized FFEL/Direct Stafford Loan";
            if (txtUnSubFFEL.Text != string.Empty)
            {
                unSubFFEL.AmountDisbursed = Convert.ToDecimal(txtUnSubFFEL.Text);
                unSubFFEL.Priority = loanPriority + 1;
                loanPriority += 1;
            }
            else
                unSubFFEL.AmountDisbursed = 0.00m;

            if (txtUnSubFFElM.Text != string.Empty)
                unSubFFEL.AmountExpected = Convert.ToDecimal(txtUnSubFFElM.Text);
            else
                unSubFFEL.AmountExpected = 0.00m;
            Loans.Add(unSubFFEL);

            TitleIVLoans SubFFEL = new TitleIVLoans();
            SubFFEL.LoanName = "Subsidized FFEL/Direct Stafford Loan";
            if (txtSubFFEL.Text != string.Empty)
            {
                SubFFEL.AmountDisbursed = Convert.ToDecimal(txtSubFFEL.Text);
                SubFFEL.Priority = loanPriority + 1;
                loanPriority += 1;
            }
            else
                SubFFEL.AmountDisbursed = 0.00m;

            if (txtSubFFElM.Text != string.Empty)
                SubFFEL.AmountExpected = Convert.ToDecimal(txtSubFFElM.Text);
            else
                SubFFEL.AmountExpected = 0.00m;
            Loans.Add(SubFFEL);

            TitleIVLoans Perkins = new TitleIVLoans();
            Perkins.LoanName = "Perkins Loan";
            if (txtPerkins.Text != string.Empty)
            {
                Perkins.AmountDisbursed = Convert.ToDecimal(txtPerkins.Text);
                Perkins.Priority = loanPriority + 1;
                loanPriority += 1;
            }
            else
                Perkins.AmountDisbursed = 0.00m;

            if (txtPerkinsM.Text != string.Empty)
                Perkins.AmountExpected = Convert.ToDecimal(txtPerkinsM.Text);
            else
                Perkins.AmountExpected = 0.00m;
            Loans.Add(Perkins);

            TitleIVLoans FFElStudent = new TitleIVLoans();
            FFElStudent.LoanName = "FFEL/Direct PLUS Student";
            if (txtFFElStudent.Text != string.Empty)
            {
                FFElStudent.AmountDisbursed = Convert.ToDecimal(txtFFElStudent.Text);
                FFElStudent.Priority = loanPriority + 1;
                loanPriority += 1;
            }
            else
                FFElStudent.AmountDisbursed = 0.00m;

            if (txtFFELStudentM.Text != string.Empty)
                FFElStudent.AmountExpected = Convert.ToDecimal(txtFFELStudentM.Text);
            else
                FFElStudent.AmountExpected = 0.00m;
            Loans.Add(FFElStudent);

            TitleIVLoans FFELParent = new TitleIVLoans();
            FFELParent.LoanName = "FFEL/Direct PLUS Parent";
            if (txtFFELParent.Text != string.Empty)
            {
                FFELParent.AmountDisbursed = Convert.ToDecimal(txtFFELParent.Text);
                FFELParent.Priority = loanPriority + 1;
                loanPriority += 1;
            }
            else
                FFELParent.AmountDisbursed = 0.00m;

            if (txtFFELParentM.Text != string.Empty)
                FFELParent.AmountExpected = Convert.ToDecimal(txtFFELParentM.Text);
            else
                FFELParent.AmountExpected = 0.00m;
            Loans.Add(FFELParent);

            #endregion

            inputInfo.Grants = lstGrants;
            inputInfo.Loans = Loans;
            inputInfo.Student = student;

            var result = facade.CalculateR2T4(inputInfo);
            DisplayStep1Results(result);
            DisplayStep2Results(result);
            DisplayStep3Results(result);
            DisplayStep4Results(result);
            if (this.inputInfo.PaymentType == PaymentTypes.PaymentPeriod)
            {
                if (this.inputInfo.Student.IsTutionCharged)
                {
                    chkTuitionCharged.Checked = false;
                    txtNRefunded.Enabled = true;
                }
                else
                {
                    chkTuitionCharged.Checked = true;
                    txtNRefunded.Enabled = false;
                }
            }
            else
            {
                chkTuitionCharged.Checked = false;
                txtNRefunded.Enabled = true;
            }

            if (inputInfo.IsCalculationRequired)
            {

                DisplayStep5Results(result);
                DisplayStep6Results(result);
                DisplayStep7Results(result);
                DisplayStep8Results(result);
            }
            if (inputInfo.IsStep9Required)
            {
                DisplayStep9Results(result);
            }
            if (inputInfo.IsStep10Required)
            {
                DisplayStep10Results(result);
            }
        }

        public void DisplayStep1Results(IR2T4Results result)
        {
            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxA))
            {
                txtSubTotalA.Text = result.StepResults[Constants.ResultKeyBoxA].ToString();
                txtEA.Text = result.StepResults[Constants.ResultKeyBoxA].ToString();
                txtFA.Text = result.StepResults[Constants.ResultKeyBoxA].ToString();
                txtStep1FA.Text = result.StepResults[Constants.ResultKeyBoxA].ToString();
            }
            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxC))
            {
                txtSubTotalC.Text = result.StepResults[Constants.ResultKeyBoxC].ToString();
                txtFC.Text = result.StepResults[Constants.ResultKeyBoxC].ToString();
                txtStep1FC.Text = result.StepResults[Constants.ResultKeyBoxC].ToString();
            }

            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxB))
            {
                txtSubTotalB.Text = result.StepResults[Constants.ResultKeyBoxB].ToString();
                txtEB.Text = result.StepResults[Constants.ResultKeyBoxB].ToString();
                txtStep1FB.Text = result.StepResults[Constants.ResultKeyBoxB].ToString();
            }

            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxD))
            {
                txtSubtotalD.Text = result.StepResults[Constants.ResultKeyBoxD].ToString();
                txtStep1FD.Text = result.StepResults[Constants.ResultKeyBoxD].ToString();
            }
            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxE))
            {
                txtResultE.Text = result.StepResults[Constants.ResultKeyBoxE].ToString();
            }
            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxF))
            {
                txtstep1F.Text = result.StepResults[Constants.ResultKeyBoxF].ToString();
            }
            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxG))
            {
                txtStep1G.Text = result.StepResults[Constants.ResultKeyBoxG].ToString();
            }
        }
        public void DisplayStep2Results(IR2T4Results result)
        {
            if (inputInfo.ProgramType == ProgramType.ClockHour)
            {
                txtHoursPercent.Text = result.StepResults[Constants.ResultKeyBoxH].ToString();
                txtActAttendence.Text = result.StepResults[Constants.ResultKeyActualAttendance].ToString();
            }
            else if (inputInfo.ProgramType == ProgramType.CreditHour)
            {
                if (inputInfo.Student.IsAttendanceRequired == true)
                {
                    txtCreditActual.Text = result.StepResults[Constants.ResultKeyActualAttendance].ToString();
                    txtCreditAttendencePerc.Text = result.StepResults[Constants.ResultKeyBoxH].ToString();
                }
                else
                    txtCreditAttendencePerc.Text = result.StepResults[Constants.ResultKeyBoxH].ToString();
            }
        }
        public void DisplayStep3Results(IR2T4Results result)
        {
            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxG))
            {
                txtBoxG.Text = result.StepResults[Constants.ResultKeyBoxG].ToString();
            }
            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxH))
            {
                txtBoxH.Text = result.StepResults[Constants.ResultKeyBoxH].ToString();
            }
            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxI))
            {
                txtBoxI.Text = result.StepResults[Constants.ResultKeyBoxI].ToString();
            }
        }
        public void DisplayStep4Results(IR2T4Results result)
        {
            if (inputInfo.IsStep4Required)
            {
                if (result.StepResults.ContainsKey(Constants.ResultKeyBoxJ))
                {
                    if (result.StepResults.ContainsKey(Constants.ResultKeyBoxI))
                    {
                        txt4JI.Text = result.StepResults[Constants.ResultKeyBoxI].ToString(CultureInfo.CurrentCulture);
                    }
                    if (result.StepResults.ContainsKey(Constants.ResultKeyBoxE))
                    {
                        txt4JE.Text = result.StepResults[Constants.ResultKeyBoxE].ToString(CultureInfo.CurrentCulture);
                    }
                    txtResultJ.Text = result.StepResults[Constants.ResultKeyBoxJ].ToString(CultureInfo.CurrentCulture);
                }
                if (result.StepResults.ContainsKey(Constants.ResultKeyBoxK))
                {
                    if (result.StepResults.ContainsKey(Constants.ResultKeyBoxI))
                    {
                        txt4KI.Text = result.StepResults[Constants.ResultKeyBoxI].ToString(CultureInfo.CurrentCulture);
                    }
                    if (result.StepResults.ContainsKey(Constants.ResultKeyBoxE))
                    {
                        txt4KE.Text = result.StepResults[Constants.ResultKeyBoxE].ToString(CultureInfo.CurrentCulture);
                    }
                    txtResultK.Text = result.StepResults[Constants.ResultKeyBoxK].ToString(CultureInfo.CurrentCulture);
                }
            }
        }
        public void DisplayStep5Results(IR2T4Results result)
        {
            if (Convert.ToDecimal(result.StepResults[Constants.ResultKeyBoxL].ToString()) >= 0)
            {

                if (result.StepResults.ContainsKey(Constants.ResultKeyBoxH))
                    txt5BoxH.Text = result.StepResults[Constants.ResultKeyBoxH].ToString();

                if (result.StepResults.ContainsKey(Constants.TotalinstitutionalCharges))
                    txt5L.Text = result.StepResults[Constants.TotalinstitutionalCharges].ToString();

                if (result.StepResults.ContainsKey(Constants.ResultKeyBoxL))
                    txt5BoxL.Text = result.StepResults[Constants.ResultKeyBoxL].ToString();
                if (result.StepResults.ContainsKey(Constants.ResultKeyBoxM))
                {
                    txt5BoxM.Text = result.StepResults[Constants.ResultKeyBoxM].ToString();
                    txt5BoxMResult.Text = result.StepResults[Constants.ResultKeyBoxM].ToString();
                }
                if (result.StepResults.ContainsKey(Constants.ResultKeyBoxN))
                    txt5BoxNResult.Text = result.StepResults[Constants.ResultKeyBoxN].ToString();
                if (result.StepResults.ContainsKey(Constants.ResultKeyBoxO))
                    txt5BoxOresult.Text = result.StepResults[Constants.ResultKeyBoxO].ToString();
            }

        }
        public void DisplayStep6Results(IR2T4Results result)
        {
            if (result.StepResults.ContainsKey(Constants.UnsubsidizedFfelOrDirectStaffordLoan))
            {
                txtxl.Text = result.StepResults[Constants.UnsubsidizedFfelOrDirectStaffordLoan].ToString();
            }
            if (result.StepResults.ContainsKey(Constants.SubsidizedFfelOrDirectStaffordLoan))
            {
                txtk.Text = result.StepResults[Constants.SubsidizedFfelOrDirectStaffordLoan].ToString();
            }
            if (result.StepResults.ContainsKey(Constants.PerkinsLoan))
            {
                txtj.Text = result.StepResults[Constants.PerkinsLoan].ToString();
            }
            if (result.StepResults.ContainsKey(Constants.FfelOrDirectPlusStudent))
            {
                txtl.Text = result.StepResults[Constants.FfelOrDirectPlusStudent].ToString();
            }
            if (result.StepResults.ContainsKey(Constants.FfelOrDirectPlusParent))
            {
                txtm.Text = result.StepResults[Constants.FfelOrDirectPlusParent].ToString();
            }
            if (result.StepResults.ContainsKey(Constants.PellGrant))
            {
                txtn.Text = result.StepResults[Constants.PellGrant].ToString();
            }
            if (result.StepResults.ContainsKey(Constants.Fseog))
            {
                txtq.Text = result.StepResults[Constants.Fseog].ToString();
            }
            if (result.StepResults.ContainsKey(Constants.TeachGrant))
            {
                txtr.Text = result.StepResults[Constants.TeachGrant].ToString();
            }
            if (result.StepResults.ContainsKey(Constants.IraqAfghanistanServiceGrant))
            {
                txts.Text = result.StepResults[Constants.IraqAfghanistanServiceGrant].ToString();
            }
            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxP))
            {
                txtStep6P.Text = result.StepResults[Constants.ResultKeyBoxP].ToString();
            }
        }
        public void DisplayStep7Results(IR2T4Results result)
        {
            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxK))
            {
                txt7K.Text = result.StepResults[Constants.ResultKeyBoxK].ToString(CultureInfo.CurrentCulture);
            }
            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxO))
            {
                txt7O.Text = result.StepResults[Constants.ResultKeyBoxO].ToString(CultureInfo.CurrentCulture);
            }
            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxQ))
            {
                txtStep7Result.Text = result.StepResults[Constants.ResultKeyBoxQ].ToString(CultureInfo.CurrentCulture);
            }
        }

        public void DisplayStep8Results(IR2T4Results result)
        {
            if (inputInfo.Results.StepResults.ContainsKey(Constants.ResultKeyBoxR))
            {
                if (inputInfo.Results.StepResults.ContainsKey(Constants.ResultKeyBoxB))
                    txt8boxB.Text = inputInfo.Results.StepResults[Constants.ResultKeyBoxB].ToString();
                if (inputInfo.Results.StepResults.ContainsKey(Constants.ResultKeyBoxP))
                    txt8P.Text = inputInfo.Results.StepResults[Constants.ResultKeyBoxP].ToString();
                if (inputInfo.Results.StepResults.ContainsKey(Constants.ResultKeyBoxR)) // && inputInfo.Results.StepResults[Constants.ResultKeyBoxR] > 0)
                    txt8BoxR.Text = inputInfo.Results.StepResults[Constants.ResultKeyBoxR].ToString();
            }
        }

        public void DisplayStep9Results(IR2T4Results result)
        {
            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxQ))
            {
                txt9BoxQ.Text = result.StepResults[Constants.ResultKeyBoxQ].ToString(CultureInfo.CurrentCulture);
            }
            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxR))
            {
                txt9BoxR.Text = result.StepResults[Constants.ResultKeyBoxR].ToString(CultureInfo.CurrentCulture);
            }
            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxS))
            {
                txt9BoxSResult.Text = result.StepResults[Constants.ResultKeyBoxS].ToString(CultureInfo.CurrentCulture);
                txt9Boxs.Text = result.StepResults[Constants.ResultKeyBoxS].ToString(CultureInfo.CurrentCulture);
            }
            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxF))
            {
                txt9BoxF.Text = result.StepResults[Constants.ResultKeyBoxF].ToString(CultureInfo.CurrentCulture);
            }
            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxT))
            {
                txt9BoxTResult.Text = result.StepResults[Constants.ResultKeyBoxT].ToString(CultureInfo.CurrentCulture);
                txt9BoxT.Text = result.StepResults[Constants.ResultKeyBoxT].ToString(CultureInfo.CurrentCulture);
            }
            if (result.StepResults.ContainsKey(Constants.ResultKeyBoxU))
            {
                txtBox9Result.Text = result.StepResults[Constants.ResultKeyBoxU] < 0 ? "(" + Math.Abs(result.StepResults[Constants.ResultKeyBoxU]) + ")" : result.StepResults[Constants.ResultKeyBoxU].ToString(CultureInfo.CurrentCulture);
            }
        }

        public void DisplayStep10Results(IR2T4Results result)
        {
            if (inputInfo.Results.StepResults.ContainsKey(Constants.ResultTotalGrantsReturnedByStudent) && inputInfo.Results.StepResults[Constants.ResultTotalGrantsReturnedByStudent] > 0)
            {
                foreach (var grant in inputInfo.Grants)
                {
                    if (grant.AmountToBeReturnedByStudent > 50)
                    {
                        switch (grant.GrantName)
                        {
                            case Constants.PellGrant:
                                if(grant.AmountToBeReturnedByStudent > Constants.MinimumRefund)
                                    txt10Pell.Text = grant.AmountToBeReturnedByStudent.ToString();
                                break;
                            case Constants.Fseog:
                                if (grant.AmountToBeReturnedByStudent > Constants.MinimumRefund)
                                    txt10FSEOG.Text = grant.AmountToBeReturnedByStudent.ToString();
                                break;
                            case Constants.TeachGrant:
                                if (grant.AmountToBeReturnedByStudent > Constants.MinimumRefund)
                                    txt10TG.Text = grant.AmountToBeReturnedByStudent.ToString();
                                break;
                            case Constants.IraqAfghanistanServiceGrant:
                                if (grant.AmountToBeReturnedByStudent > Constants.MinimumRefund)
                                    txt10IFSG.Text = grant.AmountToBeReturnedByStudent.ToString();
                                break;
                        }
                    }
                }
            }
        }

        protected void btnCalculate_Click(object sender, EventArgs e)
        {
            if (hdnValidate.Value == "true")
            {
                calculate();
            }
            else
            {
                hdnValidate.Value = string.Empty;
            }
        }
    }
}