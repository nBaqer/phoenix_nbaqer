﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="R2T4Calculator.aspx.cs" Inherits="R2T4Calculator.R2T4Calculator" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="" />
    <meta name="author" content="" />

    <title>FAME</title>
    <!-- Bootstrap core CSS -->
    <link href="Content/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/css/custom.css" rel="stylesheet" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/jquery-1.10.2.min.js"></script>
    <script src="Scripts/jqueryUI.js"></script>
    <link href="css/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/moment.min.js"></script>
    <script>
        var valEntered = false;
        var dateEntered = false;
        var tutionEntered = false;
        var isValidPeriod = false;
        var isValidDates = false;
        var dateErrorMessage = '';
        var periodErrorMessage = '';
        $(document).ready(function () {
            enableOrDisableCreditBalance();
            
            //Disable right click
            $("body").on("contextmenu", function (e) {
                return false;
            });
            //Disable cut copy paste
            $('body').bind('cut copy paste', function (e) {
                e.preventDefault();
            });
            $('.clickable').on('click', function () {
                $(this).next('.panel-body').toggleClass('hide');
                $(this).find('i').toggleClass('fa-minus fa-minus');
            });

            //$('#txtFormCompleted').datepicker(); $('#txtWithdrawalDate').datepicker(); $('#txt2Withdrawdate').datepicker(); $('#txtStartDate').datepicker(); $('#txtEndDate').datepicker(); $('#txt2CreditWithdrawalDate').datepicker();

            if ($('#txt2CreditWithdrawalDate').val() != '' || $('#txtCompletedDays').val() != '' || $('#txtTotalDays').val() != '') {
                $('#chkNAttend').prop("disabled", true);
            }

            if ($('#hdnNoAttend').val() == "noAttend") {
                $('#chkNAttend').prop("checked", true);
                $('#txt2CreditWithdrawalDate').prop("disabled", true);
                $('#txtCompletedDays').prop("disabled", true);
                $('#txtTotalDays').prop("disabled", true);
            }

            $(".form-calender").datepicker();

            if ($('#rdbCredit').is(':checked')) {
                $('#rdbClock').prop("checked", false);
                $('#divStep2Clock').hide();
                $('#divStep2Credit').show();
                //$('#chkNAttend').prop("checked", false);
            }
            else {
                $('#rdbClock').prop("checked", true);
                $('#divStep2Clock').show();
                $('#divStep2Credit').hide();
                $('#chkNAttend').prop("checked", false);
            }

            if ($('#rdbPeriodEnroll').is(':checked')) {
                $('#rdbPeriodEnroll').prop("checked", true);
                $('#rdbPayPeriod').prop("checked", false);
                $('#trHidden').hide();
                $('#trHidden1').hide();
            }
            else {
                $('#rdbPayPeriod').prop("checked", true);
                $('#trHidden').show();
                $('#trHidden1').show();
            }

            $('#rdbPayPeriod').click(function () {
                $('#rdbPeriodEnroll').prop("checked", false);
                $('#trHidden').show();
                $('#trHidden1').show();
                $('#chkTuitionCharged').prop("checked", false);
                $('#txtNRefunded').prop("disabled", false);
            });

            $('#rdbPeriodEnroll').click(function () {
                $('#rdbPayPeriod').prop("checked", false);
                $('#chkTuitionCharged').prop("checked", false);
                $('#txtNRefunded').val('');
                $('#txtNRefunded').prop("disabled", false);
                $('#trHidden').hide();
                $('#trHidden1').hide();
            });

            $('#rdbClock').click(function () {
                $('#rdbCredit').prop("checked", false);
                $('#divStep2Clock').show();
                $('#divStep2Credit').hide();
                $('#txt2Withdrawdate').val('');
                $('#txtHoursCompleted').val('');
                $('#txttotalHours').val('');
                $('#txtActAttendence').val('');
                $('#txtHoursPercent').val('');
            });

            $('#rdbCredit').click(function () {
                $('#rdbClock').prop("checked", false);
                $('#divStep2Clock').hide();
                $('#divStep2Credit').show();
                $('#txtStartDate').val('');
                $('#txtEndDate').val('');
                $('#txt2CreditWithdrawalDate').val('');
                $('#txtCompletedDays').val('');
                $('#chkNAttend').prop("checked", false);
                $('#chkNAttend').prop("disabled", false);
                $('#txtTotalDays').val('');
                $('#hdnNoAttend').val('');
                $('#txtCompletedDays').prop("disabled", false);
                $('#txtTotalDays').prop("disabled", false);
                $('#txt2CreditWithdrawalDate').prop("disabled", false);
                $('#txtCreditActual').val('');
                $('#txtCreditAttendencePerc').val('');
            });

            $('#btnCalculate').prop("Disabled", true);

            // function to postfix 00 after decimal point
            $('.input-xs1,.input-xs3,.input-xs4').blur(function (event) {
                var $this = $(this);
                var text = $(this).val();
                if (text != '' && (text.indexOf('.') == -1))
                    $this.val(text.concat('.00'));
                if (text.indexOf('.') != -1) {
                    var decimalPosition = $this.val().indexOf('.') + 1;
                    if (text.substring(decimalPosition, decimalPosition + 2).length == 1) {
                        $this.val(text.concat('0'));
                    }
                    if (text.substring(decimalPosition, decimalPosition + 2).length == 0) {
                        $this.val(text.concat('00'));
                    }
                }
                if (text.indexOf('.') == 0) {
                    var zeroPrefixedText = '0' + $this.val();
                    $this.val(zeroPrefixedText);
                }
            });

            $('input[type=text]').keydown(function (e) {
                if (e.ctrlKey && e.key === 'z') {
                    e.preventDefault();
                }
            });

            $('.input-xs').keypress(function (event) {
                var $this = $(this);
                if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
                   ((event.which < 48 || event.which > 57) &&
                   (event.which != 0 && event.which != 8))) {
                    event.preventDefault();
                }

                var text = $(this).val();
                if ((event.which == 46) && (text.indexOf('.') == -1)) {
                    setTimeout(function () {
                        if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                            $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                        }
                    }, 1);
                }

                if ((text.indexOf('.') != -1) &&
                    (text.substring(text.indexOf('.')).length > 2) &&
                    (event.which != 0 && event.which != 8) &&
                    ($(this)[0].selectionStart >= text.length - 2)) {
                    event.preventDefault();
                }
            });

            $('.input-xs1').keypress(function (event) {
                var $this = $(this);
                if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
                   ((event.which < 48 || event.which > 57) &&
                   (event.which != 0 && event.which != 8))) {
                    event.preventDefault();
                }

                var text = $(this).val();
                if ((event.which == 46) && (text.indexOf('.') == -1)) {
                    setTimeout(function () {
                        if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                            $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                        }
                    }, 1);
                }

                if ((text.indexOf('.') != -1) &&
                    (text.substring(text.indexOf('.')).length > 2) &&
                    (event.which != 0 && event.which != 8) &&
                    ($(this)[0].selectionStart >= text.length - 2)) {
                    event.preventDefault();
                }
            });

            $('.input-xs2').keypress(function (e) {

                if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                    //display error message
                    //$("#errmsg").html("Digits Only").show().fadeOut("slow");
                    return false;
                }
            });

            $('.input-xs3').keypress(function (event) {
                var $this = $(this);
                if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
                   ((event.which < 48 || event.which > 57) &&
                   (event.which != 0 && event.which != 8))) {
                    event.preventDefault();
                }

                var text = $(this).val();
                if ((event.which == 46) && (text.indexOf('.') == -1)) {
                    setTimeout(function () {
                        if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                            $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                        }
                    }, 1);
                }

                if ((text.indexOf('.') != -1) &&
                    (text.substring(text.indexOf('.')).length > 2) &&
                    (event.which != 0 && event.which != 8) &&
                    ($(this)[0].selectionStart >= text.length - 2)) {
                    event.preventDefault();
                }
            });

            $('.input-xs4').keypress(function (event) {
                var $this = $(this);
                if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
                   ((event.which < 48 || event.which > 57) &&
                   (event.which != 0 && event.which != 8))) {
                    event.preventDefault();
                }

                var text = $(this).val();
                if ((event.which == 46) && (text.indexOf('.') == -1)) {
                    setTimeout(function () {
                        if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                            $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
                        }
                    }, 1);
                }

                if ((text.indexOf('.') != -1) &&
                    (text.substring(text.indexOf('.')).length > 2) &&
                    (event.which != 0 && event.which != 8) &&
                    ($(this)[0].selectionStart >= text.length - 2)) {
                    event.preventDefault();
                }
            });

            $('#btnCalculate').click(function (e) {
                if (!validate()) {
                    var errMsg = '';
                    if (dateErrorMessage != '')
                        errMsg = dateErrorMessage + '\n';
                    if (valEntered == false) {
                        errMsg += "<%=R2T4Calculator.App_LocalResources.UIMessages.StepOneEmptyFields %>\n";
                    }
                    if (dateEntered == false) {
                        if ($('#rdbCredit').is(':checked')) {
                            errMsg += "<%=R2T4Calculator.App_LocalResources.UIMessages.StepTwoDatesHoursFiledsEmpty %>\n";
                        }
                        if ($('#rdbClock').is(':checked')) {
                            errMsg += "<%=R2T4Calculator.App_LocalResources.UIMessages.StepTwoDatesHoursFiledsEmpty %>\n";
                        }
                    }
                    if (tutionEntered == false)
                        errMsg += '<%=R2T4Calculator.App_LocalResources.UIMessages.StepFiveTutionEnteredEmptyFileds %>\n';
                    if (periodErrorMessage != '')
                        errMsg += periodErrorMessage + '\n';
                   
                    if ($('#hdnNoAttend').val() == "noAttend") {
                        $('#chkNAttend').prop("checked", true);
                        $('#txt2CreditWithdrawalDate').prop("disabled", true);
                        $('#txtCompletedDays').prop("disabled", true);
                        $('#txtTotalDays').prop("disabled", true);
                    }

                    enableOrDisableCreditBalance();
                    alert(errMsg, false);
                    $('#hdnValidate').val('false');
                }
            });

            $('#btnReset').click(function () {
                $("input:text").val("");
                $('#rdbClock').prop("checked", true);
                $('#rdbCredit').prop("checked", false);
                $('#rdbPayPeriod').prop("checked", true);
                $('#rdbPeriodEnroll').prop("checked", false);
                $('#rdbPeriodEnroll').prop("checked", false);
                $('#chkTuitionCharged').prop("checked", false);
                $('#chkNAttend').prop("checked", false);
                $('#txtNRefunded').prop("disabled", false);
                $('#txtNRefunded').val('');
                $('#txtNRefunded').prop("disabled", false);
                $('#trHidden').show();
                $('#trHidden1').show();
                $('#txt2CreditWithdrawalDate').prop("disabled", false);
                $('#txtCompletedDays').prop("disabled", false);
                $('#txtTotalDays').prop("disabled", false);

                $('#divStep2Clock').show();
                $('#divStep2Credit').hide();
            });

            $('#chkTuitionCharged').change(function() {
                if (this.checked) {
                    $('#txtNRefunded').prop("disabled", true);
                    $('#txtNRefunded').val("");
                } else {
                    $('#txtNRefunded').prop("disabled", false);
                }
            });

            $('#chkNAttend').change(function () {
                if (this.checked) {
                    $('#hdnNoAttend').val('noAttend');
                    $('#txt2CreditWithdrawalDate').prop("disabled", true);
                    $('#txtCompletedDays').prop("disabled", true);
                    $('#txtTotalDays').prop("disabled", true);
                }
                else {
                    $('#hdnNoAttend').val('');
                    $('#txt2CreditWithdrawalDate').prop("disabled", false);
                    $('#txtCompletedDays').prop("disabled", false);
                    $('#txtTotalDays').prop("disabled", false);
                }
            });

            $('#txt2CreditWithdrawalDate').change(function () {
                if ($('#txtCompletedDays').val() != "" || $('#txtTotalDays').val() != "" || $('#txt2CreditWithdrawalDate').val() != "") {
                    $('#chkNAttend').prop("disabled", true);
                }
                else {
                    $('#chkNAttend').prop("disabled", false);
                }
            });

            $('#txtCompletedDays, #txtTotalDays').keyup(function () {
                if ($('#txtCompletedDays').val() != "" || $('#txtTotalDays').val() != "" || $('#txt2CreditWithdrawalDate').val() != "") {
                    $('#chkNAttend').prop("disabled", true);
                }
                else {
                    $('#chkNAttend').prop("disabled", false);
                }
            });
        });

        function enableOrDisableCreditBalance() {
            if ($('#chkTuitionCharged').is(':checked')) {
                $('#txtNRefunded').prop("disabled", true);
                $('#txtNRefunded').val("");
            }
            else {
                $('#txtNRefunded').prop("disabled", false);
            }
        }

        function validateDate(enteredDate) {
            if (moment(enteredDate, 'MM/DD/YYYY').isValid())
                return true;
            else
                return false;
        }

        function validate() {

            var valid = false;
            var valcon = $('.input-xs1');
            valcon.each(function () {

                if ($(this).val() > 0) {
                    valEntered = true;
                }
            });

            isValidDates = ValidateDates();
            isValidPeriod = ValidatePeriod();


            if ($('#rdbClock').is(':checked')) {
                if ($('#txtHoursCompleted').val() > 0 && $('#txttotalHours').val() > 0 && $('#txt2Withdrawdate').val() != "") {
                    dateEntered = true;
                }
            }
            else if ($('#rdbCredit').is(':checked')) {
                if ($('#txtEndDate').val() != "" && $('#txtStartDate').val() != "") {
                    if (!$('#chkNAttend').is(':checked')) {
                        if ($('#txtCompletedDays').val() > 0 && $('#txtTotalDays').val() > 0 && $('#txt2CreditWithdrawalDate').val() != "") {
                            dateEntered = true;
                            $('#hdnNoAttend').val('');
                        }
                    }
                    else if ($('#chkNAttend').is(':checked')) {
                        dateEntered = true;
                        $('#hdnNoAttend').val('noAttend');
                    }
                }
            }


            var fees = $('.input-xs3');
            fees.each(function () {
                if ($(this).val() != '' && $(this).val() >= 0) {
                    tutionEntered = true;
                }
            });

            if (valEntered == true && dateEntered == true && tutionEntered == true && isValidDates == true && isValidPeriod == true) {
                $('#hdnValidate').val('true');
                valid = true;
            }
            else {
                $('#hdnValidate').val('false');
            }
            return valid;
        }

        function ValidateDates() {
            var isValid = true;
            if ($('#txtFormCompleted').val() != '') {
                valid = validateDate($('#txtFormCompleted').val());
                if (!valid) {
                    dateErrorMessage = '<%=R2T4Calculator.App_LocalResources.UIMessages.dateErrorMessage %>';
                    isValid = false;

                }
            }

            if ($('#txtWithdrawalDate').val() != '') {
                valid = validateDate($('#txtWithdrawalDate').val());
                if (!valid) {
                    dateErrorMessage = '<%=R2T4Calculator.App_LocalResources.UIMessages.determinationDateErrorMessage %>';
                    isValid = false;

                }
            }
            if ($('#rdbClock').is(':checked')) {
                if ($('#txt2Withdrawdate').val() != '') {
                    valid = validateDate($('#txt2Withdrawdate').val());
                    if (!valid) {
                        dateErrorMessage = '<%=R2T4Calculator.App_LocalResources.UIMessages.WithdrawalDateErrorMessage %>';
                        isValid = false;

                    }
                }
            }
            if ($('#rdbCredit').is(':checked')) {
                if ($('#txtStartDate').val() != '') {
                    valid = validateDate($('#txtStartDate').val());
                    if (!valid) {
                        dateErrorMessage = '<%=R2T4Calculator.App_LocalResources.UIMessages.StartDateErrorMessage %>';
                        isValid = false;

                    }
                }

                if ($('#txtEndDate').val() != '') {
                    valid = validateDate($('#txtEndDate').val());
                    if (!valid) {
                        dateErrorMessage = '<%=R2T4Calculator.App_LocalResources.UIMessages.EndDateErrorMessage %>';
                        isValid = false;

                    }
                }

                if ($('#txt2CreditWithdrawalDate').val() != '') {
                    valid = validateDate($('#txt2CreditWithdrawalDate').val());
                    if (!valid) {
                        dateErrorMessage = '<%=R2T4Calculator.App_LocalResources.UIMessages.WithdrawalDateErrorMessage %>';
                        isValid = false;
                        return isValid;
                    }
                }
            }
            return isValid;
        }

        function ValidatePeriod() {
            var isValidPeriod = true;
            if ($('#rdbClock').is(':checked')) {
                if (~~$('#txttotalHours').val() < ~~$('#txtHoursCompleted').val()) {
                    periodErrorMessage = '<%=R2T4Calculator.App_LocalResources.UIMessages.periodErrorMessage_Hours %>';
                    isValidPeriod = false;
                }
            }
            else if ($('#rdbCredit').is(':checked')) {
                if (~~$('#txtTotalDays').val() < ~~$('#txtCompletedDays').val()) {
                    periodErrorMessage = '<%=R2T4Calculator.App_LocalResources.UIMessages.periodErrorMessage_Days %>';
                        isValidPeriod = false;
                    }
                }

            return isValidPeriod;
        }
    </script>
</head>

<body>
    <form runat="server" id="form1">
        <div class="container">
            <div class="row parent">
                <div class="page-header">
                    <img src="Content/Images/logo.png">
                    <p class="title">Treatment of Title IV Funds When a Student Withdraws from a Title IV Program</p>
                </div>
                <div class="well well-sm">
                    <div class="row bottom-no-margin">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <div class="form-group-sm">
                                    <label>Student's Name</label>
                                    <asp:TextBox ID="txtName" runat="server" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group-sm">
                                    <label>Social Security Number</label>
                                    <asp:TextBox ID="txtSSN" runat="server" class="form-control" />
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group-sm">
                                    <label>Date Form Completed</label>
                                    <div class="input-group">
                                        <asp:TextBox ID="txtFormCompleted" runat="server" class="form-control form-calender" />
                                        <%-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group-sm">
                                    <label>Date of determination</label>
                                    <div class="input-group">
                                        <asp:TextBox ID="txtWithdrawalDate" runat="server" class="form-control form-calender" />
                                        <%--<div class="input-group-addon"><i class="fa fa-calendar"></i></div>--%>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <label>Period used for calculation:</label>
                                <div class="clearfix"></div>
                                <%--<input type="radio" value="Clock Hour" id="rdbPayPeriod"  />--%>
                                <asp:RadioButton runat="server" ID="rdbPayPeriod" />
                                Payment period
                          <%--  <input type="radio" value="Credit Hour" id="rdbPeriodEnroll"  />--%>
                                <asp:RadioButton runat="server" ID="rdbPeriodEnroll" />
                                Period of enrollment

                            </div>
                        </div>
                    </div>
                </div>
                <div class="well well-sm">
                    <div class="row bottom-no-margin">
                        <div class="col-md-12">
                            <div class="col-md-3">
                                <label>Program type :</label>
                                <div class="clearfix"></div>
                                <%-- <input type="radio" value="Clock Hour" id="rdbClock"  />--%>
                                <asp:RadioButton ID="rdbClock" runat="server" />
                                Clock Hour
                            <%--<input type="radio" value="Credit Hour" id="rdbCredit" />--%>
                                <asp:RadioButton ID="rdbCredit" runat="server" />
                                Credit Hour
                            </div>

                            <div class="col-md-3">
                                <div class="form-group-sm">
                                    <asp:HiddenField ID="hdnValidate" runat="server" />
                                    <asp:Button ID="btnCalculate" runat="server" Text="Calculate" class="btn" OnClick="btnCalculate_Click" />
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group-sm">
                                    <input type="button" id="btnReset" value="Reset" class="btn" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="alert alert-success" style="display: none">
                    <strong>Success!</strong> Indicates Success Message <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                </div>
                <div class="alert alert-danger" style="display: none">
                    <strong>Error!</strong> Indicates Error Message <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                </div>
                <div class="alert alert-warning" style="display: none">
                    <strong>Warning!</strong> Indicates Warning Message <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                </div>
                <div class="alert alert-info" style="display: none">
                    <strong>Info Message!</strong> Indicates Information Message <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                </div>
                <div class="row">
                    <div class="col-md-7 no-padding">
                        <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading"><strong><span class="badge">STEP 1:</span> Student's Title IV Aid Information</strong></div>
                                <div class="panel-body no-padding">
                                    <div id="divStep1" class="col-md-12 no-padding" runat="server">
                                        <div class="table-responsive">
                                            <table class="table table-bordered table-striped bottom-no-margin">
                                                <thead>
                                                    <tr>
                                                        <th width="3%"></th>
                                                        <th width="20%">Title IV Grant Programs</th>
                                                        <th width="20%" class="non-bold-sm">Amount Disbursed</th>
                                                        <th width="20%" class="non-bold-sm">Amount That Could Have Been Disbursed</th>
                                                        <th width="37%"><span class="badge">E</span> Total Title IV aid disbursed for the period.</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>1.</td>
                                                        <td>Pell Grant</td>
                                                        <td>
                                                            <asp:TextBox ID="txtPellA" runat="server" CssClass="form-control input-xs1" />
                                                            <%-- <asp:TextBox class="form-control input-xs input-field-error"  /><span class="field-error">Field level error message</span></td>--%>
                                                            <td>
                                                                <asp:TextBox ID="txtPellB" runat="server" class="form-control input-xs1" /></td>
                                                            <td rowspan="2">
                                                                <table class="table">
                                                                    <tbody>
                                                                        <tr>
                                                                            <th width="5%"></th>
                                                                            <th width="10%">A.</th>
                                                                            <td width="85%">
                                                                                <asp:TextBox ID="txtEA" runat="server" type="text" disabled="disabled" class="form-control input-xs" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th width="5%"><i class="fa fa-plus"></i></th>
                                                                            <th width="10%">B.</th>
                                                                            <td width="85%">
                                                                                <asp:TextBox ID="txtEB" runat="server" class="form-control input-xs" disabled="disabled" /></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <th width="5%">
                                                                                <h4>=</h4>
                                                                            </th>
                                                                            <th width="10%">E.</th>
                                                                            <td width="85%">
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                                                    <asp:TextBox ID="txtResultE" runat="server" class="form-control input-xs" disabled="disabled" />
                                                                                </div>
                                                                            </td>
                                                                        </tr>


                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                    </tr>
                                                    <tr>
                                                        <td>2.</td>
                                                        <td>FSEOG</td>
                                                        <td>
                                                            <asp:TextBox ID="txtFSEOGA" runat="server" class="form-control input-xs1" /></td>
                                                        <td>
                                                            <asp:TextBox ID="txtFSEOGB" runat="server" class="form-control input-xs1" /></td>

                                                    </tr>

                                                    <tr>
                                                        <td>3.</td>
                                                        <td>TEACH Grant</td>
                                                        <td>
                                                            <asp:TextBox ID="txtTeachA" runat="server" class="form-control input-xs1" /></td>
                                                        <td>
                                                            <asp:TextBox ID="txtTeachB" runat="server" class="form-control input-xs1" /></td>
                                                        <td rowspan="2">
                                                            <table class="table">
                                                                <tbody>
                                                                    <tr>
                                                                        <th width="5%" ><span class="badge">F</span></th>
                                                                        <td width="95%"><strong class="fcontent">Total Title IV grant aid disbursed and that could have been disbursed for the period.</strong></td>
                                                                    </tr>
                                                                    
                                                                    <tr>
                                                                        <th width="5%" class="headerFA">A.</th>
                                                                        <td width="95%"> <asp:TextBox ID="txtFA" runat="server" disabled="disabled" class="form-control input-xs" Style="display: inline"  /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="5%"><i class="fa fa-plus"></i><span class="headerFC">C.</span></th>
                                                                        <th width="95%"> <asp:TextBox ID="txtFC" runat="server" disabled="disabled" class="form-control input-xs" Style="display: inline"  /></th>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="5%">
                                                                            <h4>=</h4>
                                                                            <span class="headerF">F.</span>
                                                                        </th>
                                                                        <th width="95%"><div class="input-group">
                                                                                <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                                                <asp:TextBox ID="txtstep1F" runat="server" disabled="disabled" class="form-control input-xs" Style="display: inline"  />
                                                                            </div>
                                                                        </th>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>4.</td>
                                                        <td>Iraq Afghanistan Service Grant</td>
                                                        <td>
                                                            <asp:TextBox ID="txtIraqAfganA" runat="server" class="form-control input-xs1" /></td>
                                                        <td>
                                                            <asp:TextBox ID="txtIraqAfganB" runat="server" class="form-control input-xs1" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2"></td>
                                                        <td align="center">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><strong>A</strong></span>
                                                                <asp:TextBox ID="txtSubTotalA" runat="server" class="form-control input-xs" disabled="disabled" />
                                                            </div>
                                                            <span>Subtotal</span>
                                                        </td>
                                                        <td align="center">
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><strong>C</strong></span>
                                                                <asp:TextBox ID="txtSubTotalC" runat="server" class="form-control input-xs" disabled="disabled" />
                                                            </div>
                                                            <span>Subtotal</span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                                <thead>
                                                    <tr>
                                                        <th width="3%"></th>
                                                        <th width="20%">Title IV Loan Programs</th>
                                                        <th width="20%" class="non-bold-sm">Net Amount Disbursed</th>
                                                        <th width="20%" class="non-bold-sm">Net Amount That Could Have  Been Disbursed</th>
                                                        <th width="37%"><span class="badge">G</span> Total Title IV aid disbursed and that could have been disbursed for the period.</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>5.</td>
                                                        <td>Unsubsidized FFEL/Direct Stafford Loan</td>
                                                        <td>
                                                            <asp:TextBox runat="server" class="form-control input-xs1" ID="txtUnSubFFEL" /></td>
                                                        <td>
                                                            <asp:TextBox runat="server" class="form-control input-xs1" ID="txtUnSubFFElM" /></td>
                                                        <td rowspan="6">
                                                            <table class="table">
                                                                <tbody>
                                                                    <tr>
                                                                        <th width="5%"></th>
                                                                        <th width="10%">A.</th>
                                                                        <td width="85%">
                                                                            <asp:TextBox runat="server" disabled="disabled" ID="txtStep1FA" class="form-control input-xs" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="5%"></th>
                                                                        <th width="10%">B.</th>
                                                                        <td width="85%">
                                                                            <asp:TextBox runat="server" disabled="disabled" ID="txtStep1FB" class="form-control input-xs" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="5%"></th>
                                                                        <th width="10%">C.</th>
                                                                        <td width="85%">
                                                                            <asp:TextBox runat="server" disabled="disabled" class="form-control input-xs" ID="txtStep1FC" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="5%"><i class="fa fa-plus"></i></th>
                                                                        <th width="10%">D.</th>
                                                                        <td width="85%">
                                                                            <asp:TextBox runat="server" disabled="disabled" class="form-control input-xs" ID="txtStep1FD" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <th width="5%">
                                                                            <h4>=</h4>
                                                                        </th>
                                                                        <th width="10%">G.</th>
                                                                        <td width="85%">
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                                                <asp:TextBox ID="txtStep1G" runat="server" disabled="disabled" class="form-control input-xs" />
                                                                            </div>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>6.</td>
                                                        <td>Subsidized FFEL/Direct Stafford Loan</td>
                                                        <td>
                                                            <asp:TextBox ID="txtSubFFEL" runat="server" class="form-control input-xs1" /></td>
                                                        <td>
                                                            <asp:TextBox ID="txtSubFFElM" runat="server" class="form-control input-xs1" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>7.</td>
                                                        <td>Perkins Loan</td>
                                                        <td>
                                                            <asp:TextBox ID="txtPerkins" runat="server" class="form-control input-xs1" /></td>
                                                        <td>
                                                            <asp:TextBox ID="txtPerkinsM" runat="server" class="form-control input-xs1" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>8.</td>
                                                        <td>FFEL/Direct PLUS (Graduate Student)</td>
                                                        <td>
                                                            <asp:TextBox ID="txtFFElStudent" runat="server" class="form-control input-xs1" /></td>
                                                        <td>
                                                            <asp:TextBox ID="txtFFELStudentM" runat="server" class="form-control input-xs1" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td>9.</td>
                                                        <td>FFEL/Direct PLUS (Parent)</td>
                                                        <td>
                                                            <asp:TextBox ID="txtFFELParent" runat="server" class="form-control input-xs1" /></td>
                                                        <td>
                                                            <asp:TextBox ID="txtFFELParentM" runat="server" class="form-control input-xs1" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2"></td>
                                                        <td>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><strong>B</strong></span>
                                                                <asp:TextBox ID="txtSubTotalB" runat="server" class="form-control input-xs" disabled="disabled" />
                                                            </div>
                                                            <span>Subtotal</span>
                                                        </td>
                                                        <td>
                                                            <div class="input-group">
                                                                <span class="input-group-addon"><strong>D</strong></span>
                                                                <asp:TextBox ID="txtSubtotalD" runat="server" class="form-control input-xs" disabled="disabled" />
                                                            </div>
                                                            <span>Subtotal</span>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <%--himaraj--%>
                            </div>
                        </div>
                        <div class="col-md-12" id="divStep2Clock">
                            <div class="panel panel-primary">
                                <div class="panel-heading clickable"><strong><span class="badge">STEP 2:</span> Percentage of Title IV Aid Earned</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                                <div class="panel-body no-padding">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th width="3%"></th>
                                                    <th width="97%" colspan="5">Withdrawal date
                                                    <div class="input-group">
                                                        <asp:TextBox ID="txt2Withdrawdate" runat="server" class="form-control form-calender" />
                                                        <%-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div>--%>
                                                    </div>
                                                    </th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th>H.</th>
                                                    <td colspan="5"><strong>Determine the percentage of the period completed:</strong></td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="5">Divide the clock hours scheduled to have been completed as of the withdrawal date in the period by the total clock hours in the period.</td>
                                                </tr>

                                                <tr>
                                                    <th></th>
                                                    <td>
                                                        <asp:TextBox ID="txtHoursCompleted" runat="server" class="form-control input-xs2" /></td>
                                                    <td>
                                                        <img src="Content/images/divider.png"></td>
                                                    <td>
                                                        <asp:TextBox ID="txttotalHours" runat="server" class="form-control input-xs2" /></td>
                                                    <td>
                                                        <h4>=</h4>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" disabled="disabled" class="form-control input-xs" ID="txtActAttendence" /></td>
                                                </tr>
                                                <tr>
                                                    <th>&nbsp;</th>
                                                    <td>
                                                        <asp:Label ID="txtschHours" runat="server" Text="Hours scheduled to complete"> </asp:Label></td>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="Label1" runat="server" Text="Total Hours in period"> </asp:Label></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <th><i class="fa fa-hand-o-right"></i></th>
                                                    <td colspan="5">If this percentage is greater than 60%, enter 100% in box H and proceed to Step 3.</td>
                                                </tr>
                                                <tr>
                                                    <th><i class="fa fa-hand-o-right"></i></th>
                                                    <td colspan="5">If this percentage is less than or equal to 60%, enter that percentage in Box H, and proceed to Step 3.</td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">H</div>
                                                            <asp:TextBox ID="txtHoursPercent" runat="server" class="form-control input-xs" disabled="disabled" />
                                                            <div class="input-group-addon input-group-addon-nobg">%</div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12" id="divStep2Credit" runat="server" style="display: none">
                            <div class="panel panel-primary">
                                <div class="panel-heading clickable"><strong><span class="badge">STEP 2:</span> Percentage of Title IV Aid Earned</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                                <div class="panel-body no-padding">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th width="3%"></th>
                                                    <th>Start date
                                                    
                                                     <div class="input-group">
                                                         <asp:TextBox ID="txtStartDate" runat="server" class="form-control form-calender" />
                                                         <%--<div class="input-group-addon"><i class="fa fa-calendar"></i></div>--%>
                                                     </div>
                                                    </th>

                                                    <th width="3%"></th>
                                                    <th>End date
                                                     <div class="input-group">
                                                         <asp:TextBox ID="txtEndDate" runat="server" class="form-control form-calender" />
                                                         <%--<div class="input-group-addon"><i class="fa fa-calendar"></i></div>--%>
                                                     </div>
                                                    </th>

                                                    <th width="3%"></th>
                                                    <th>Withdrawal date
                                                   
                                                     <div class="input-group">
                                                         <asp:TextBox ID="txt2CreditWithdrawalDate" runat="server" class="form-control form-calender" />
                                                         <%-- <div class="input-group-addon"><i class="fa fa-calendar"></i></div>--%>
                                                     </div>
                                                    </th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="6">A school that is not required to take attendance may, for a student who withdraws without notification, enter 50% in
                                                        Box H and proceed to Step 3. Or, the school may enter the last date of attendance at an academically related activity
                                                        for the “withdrawal date,” and proceed with the calculation as instructed. For a student who officially withdraws, enter
                                                        the withdrawal date.
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="6">
                                                        <asp:CheckBox ID="chkNAttend" runat="server" Text="&nbsp; Not required to take attendance" />
                                                        <asp:HiddenField ID="hdnNoAttend" runat="server" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>H.</th>
                                                    <td colspan="6"><strong>Percentage of payment period or period of enrollment completed</strong></td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="6">Divide the calendar days completed in the period by the total calendar days in the period (excluding scheduled
                                                        breaks of five days or more AND days that the student was on an approved leave of absence).
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td>
                                                        <asp:TextBox ID="txtCompletedDays" runat="server" class="form-control input-xs2" /></td>
                                                    <td>
                                                        <img src="Content/images/divider.png"></td>
                                                    <td>
                                                        <asp:TextBox ID="txtTotalDays" runat="server" class="form-control input-xs2" /></td>
                                                    <td>
                                                        <h4>=</h4>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox runat="server" disabled="disabled" class="form-control input-xs2" ID="txtCreditActual" /></td>
                                                </tr>
                                                <tr>
                                                    <th>&nbsp;</th>
                                                    <td>
                                                        <asp:Label ID="Label2" runat="server" Text="Completed Days"> </asp:Label></td>
                                                    <td>&nbsp;</td>
                                                    <td>
                                                        <asp:Label ID="Label3" runat="server" Text="Total Days"> </asp:Label></td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <th><i class="fa fa-hand-o-right"></i></th>
                                                    <td colspan="6">If this percentage is greater than 60%, enter 100% in Box H and proceed to Step 3.</td>
                                                </tr>
                                                <tr>
                                                    <th><i class="fa fa-hand-o-right"></i></th>
                                                    <td colspan="6">If this percentage is less than or equal to 60%, enter that percentage in Box H, and proceed to Step 3.</td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="5">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">H</div>
                                                            <asp:TextBox ID="txtCreditAttendencePerc" runat="server" class="form-control input-xs" disabled="disabled" />
                                                            <div class="input-group-addon input-group-addon-nobg">%</div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading clickable"><strong><span class="badge">STEP 3:</span> Amount of Title IV Aid Earned by the Student</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                                <div class="panel-body no-padding">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <tbody>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="6">Multiply the percentage of Title IV aid earned  (Box H)  by the total of the Title IV aid disbursed and the Title IV aid that could have been disbursed for the period (Box G).</td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td>
                                                        <div class="input-group">
                                                            <asp:TextBox ID="txtBoxH" runat="server" class="form-control input-xs" disabled="disabled" /><div class="input-group-addon input-group-addon-nobg">%</div></div>
                                                    </td>
                                                    <td>
                                                        <h4>x</h4>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="txtBoxG" runat="server" class="form-control input-xs" disabled="disabled" /></td>
                                                    <td>
                                                        <h4>=</h4>
                                                    </td>
                                                    <td><strong>I.</strong></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                            <asp:TextBox ID="txtBoxI" runat="server" class="form-control input-xs" disabled="disabled" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td>Box H</td>
                                                    <td></td>
                                                    <td>Box G</td>
                                                    <td colspan="3"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading clickable"><strong><span class="badge">STEP 4:</span> Total Title IV Aid to be Disbursed or Returned</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                                <div class="panel-body no-padding">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <tbody>
                                                <tr>
                                                    <th><i class="fa fa-hand-o-right"></i></th>
                                                    <td colspan="6">If the amount in Box I is greater than the amount in Box E, go to Post-withdrawal disbursement (Item J).</td>
                                                </tr>
                                                <tr>
                                                    <th><i class="fa fa-hand-o-right"></i></th>
                                                    <td colspan="6">If the amount in Box I is less than the amount in Box E, go to Title IV aid to be returned (Item K).</td>
                                                </tr>
                                                <tr>
                                                    <th><i class="fa fa-hand-o-right"></i></th>
                                                    <td colspan="6">If the amounts in Box I and Box E are equal, STOP.No further action is necessary.</td>
                                                </tr>
                                                <tr>
                                                    <th>J.</th>
                                                    <td colspan="6"><strong>Post-withdrawal disbursement</strong></td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="6">From the Amount of Title IV aid earned by the student (Box I) subtract the Total Title IV aid disbursed for the period (Box E). This is the amount of the postwithdrawal disbursement.If there’s an entry for “J,” Stop here, and enter the amount in Box 1 on Page 3 (Post-withdrawal disbursement tracking sheet).</td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td>
                                                        <asp:TextBox ID="txt4JI" runat="server" class="form-control input-xs" disabled="disabled" /></td>
                                                    <td><i class="fa fa-minus"></i></td>
                                                    <td>
                                                        <asp:TextBox runat="server" class="form-control input-xs" disabled="disabled" ID="txt4JE" /></td>
                                                    <td>
                                                        <h4>=</h4>
                                                    </td>
                                                    <td><strong>J.</strong></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                            <asp:TextBox ID="txtResultJ" runat="server" class="form-control input-xs" disabled="disabled" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td>Box I</td>
                                                    <td></td>
                                                    <td>Box E</td>
                                                    <td colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <th>K.</th>
                                                    <td colspan="6"><strong>Title IV aid to be returned</strong></td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="6">Subtract the amount of Title IV aid earned (Box I) from the Total Title IV aid disbursed for the period (Box E). This is the amount of Title IV aid that must be returned.</td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td>
                                                        <asp:TextBox runat="server" class="form-control input-xs" disabled="disabled" ID="txt4KE" /></td>
                                                    <td><i class="fa fa-minus"></i></td>
                                                    <td>
                                                        <asp:TextBox ID="txt4KI" runat="server" class="form-control input-xs" disabled="disabled" /></td>
                                                    <td>
                                                        <h4>=</h4>
                                                    </td>
                                                    <td><strong>K.</strong></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                            <asp:TextBox ID="txtResultK" runat="server" class="form-control input-xs" disabled="disabled" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td>Box E</td>
                                                    <td></td>
                                                    <td>Box I</td>
                                                    <td colspan="3"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading clickable"><strong><span class="badge">STEP 5:</span> Amount of Unearned Title IV Aid Due from the School</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                                <div class="panel-body no-padding">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <tbody>
                                                <tr>
                                                    <th>L</th>
                                                    <td colspan="2">Institutional charges for the payment period or period of enrollment</td>
                                                    <td>Tuition</td>
                                                    <td colspan="3">
                                                        <asp:TextBox runat="server" ID="txt5Tuition" class="form-control input-xs3" /></td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="2"></td>
                                                    <td>Room</td>
                                                    <td colspan="3">
                                                        <asp:TextBox runat="server" ID="txt5Room" class="form-control input-xs3" /></td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="2"></td>
                                                    <td>Board</td>
                                                    <td colspan="3">
                                                        <asp:TextBox runat="server" ID="txt5Board" class="form-control input-xs3" /></td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="2" align="right"><i class="fa fa-plus"></i></td>
                                                    <td>Other</td>
                                                    <td colspan="3">
                                                        <asp:TextBox ID="txt5Other" runat="server" class="form-control input-xs3" /></td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="2"><strong>Total Institutional Charges</strong><br>
                                                        (Add all the charges together)</td>
                                                    <td align="right"><strong>L.</strong></td>
                                                    <td colspan="3">
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                            <asp:TextBox runat="server" CssClass="form-control input-xs" ID="txt5L" class="form-control input-xs" disabled="disabled" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th>M.</th>
                                                    <td colspan="6"><strong>Percentage of unearned Title IV aid</strong></td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td>100%</td>
                                                    <td><i class="fa fa-minus"></i></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <asp:TextBox runat="server" class="form-control input-xs" ID="txt5BoxH" disabled="disabled" /><div class="input-group-addon input-group-addon-nobg">%</div></div>
                                                    </td>
                                                    <td>
                                                        <h4>=</h4>
                                                    </td>
                                                    <td><strong>M.</strong></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <asp:TextBox runat="server" class="form-control input-xs" ID="txt5BoxMResult" disabled="disabled" /><div class="input-group-addon input-group-addon-nobg">%</div></div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="7" align="center">Box H</td>
                                                </tr>
                                                <tr>
                                                    <th>N.</th>
                                                    <td colspan="6"><strong>Amount of unearned charges</strong></td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="6">Multiply institutional charges for the period (Box L) times the percentage of unearned Title IV aid (Box M)</td>
                                                </tr>
                                                <tr id="trHidden">
                                                    <th></th>
                                                    <td colspan="6"><strong>
                                                        <asp:CheckBox ID="chkTuitionCharged" runat="server" Text="Tuition charged by payment period" CssClass="" /></strong>
                                                    </td>
                                                </tr>
                                                <tr id="trHidden1">
                                                    <th></th>
                                                    <td colspan="2" class="creditBalanceText"><strong>Title IV credit balance refunded to student:</strong></td>
                                                    <td colspan="4" >
                                                        <div class="input-group balanceRefundField">
                                                            <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                            <asp:TextBox ID="txtNRefunded" CssClass="form-control input-xs4" Width="150px" runat="server" />
                                                        </div>
                                                    </td>
                                                </tr>



                                                <tr>
                                                    <th></th>
                                                    <td><strong>
                                                        <asp:TextBox runat="server" class="form-control input-xs" ID="txt5BoxL" disabled="disabled" /></strong></td>
                                                    <td>
                                                        <h4>x</h4>
                                                    </td>
                                                    <td><div class="input-group">
                                                            <asp:TextBox runat="server" class="form-control input-xs" ID="txt5BoxM" disabled="disabled" /><div class="input-group-addon input-group-addon-nobg">%</div></div>
                                                    </td>
                                                    <td>
                                                        <h4>=</h4>
                                                    </td>
                                                    <td><strong>N.</strong></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                            <strong>
                                                                <asp:TextBox runat="server" class="form-control input-xs" ID="txt5BoxNResult" disabled="disabled" /></strong>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td>Box L</td>
                                                    <td></td>
                                                    <td>Box M</td>
                                                    <td colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <th>O.</th>
                                                    <td colspan="6"><strong>Amount for school to return</strong></td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="6">Compare the amount of Title IV aid to be returned (Box K) to amount of unearned charges (Box N),and enter the lesser amount.</td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="5" align="right"><strong>O.</strong></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                            <strong>
                                                                <asp:TextBox runat="server" class="form-control input-xs" ID="txt5BoxOresult" disabled="disabled" /></strong>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 no-padding">
                        <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading clickable"><strong><span class="badge">STEP 6:</span> Return of Funds by the School</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                                <div class="panel-body no-padding">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <tbody>
                                                <tr>
                                                    <td colspan="4">The school must return the unearned aid for which the school is responsible (Box O) by repaying funds to the following sources, in order, up to the total net amount disbursed from each source.</td>
                                                </tr>
                                                <tr>
                                                    <th width="3%"></th>
                                                    <th width="47%">Title IV Programs</th>
                                                    <th width="50%" colspan="2" class="non-bold-sm">Amount for School to Return</th>
                                                </tr>
                                                <tr>
                                                    <td>1.</td>
                                                    <td>Unsubsidized FFEL/Direct Stafford Loan</td>
                                                    <td colspan="2">
                                                        <asp:TextBox runat="server" class="form-control input-xs" ID="txtxl" disabled="disabled" /></td>
                                                </tr>
                                                <tr>
                                                    <td>2.</td>
                                                    <td>Subsidized FFEL/Direct Stafford Loan</td>
                                                    <td colspan="2">
                                                        <asp:TextBox runat="server" class="form-control input-xs" ID="txtk" disabled="disabled" /></td>
                                                </tr>
                                                <tr>
                                                    <td>3.</td>
                                                    <td>Perkins Loan</td>
                                                    <td colspan="2">
                                                        <asp:TextBox runat="server" class="form-control input-xs" ID="txtj" disabled="disabled" /></td>
                                                </tr>
                                                <tr>
                                                    <td>4.</td>
                                                    <td>FFEL/Direct PLUS (Graduate Student)</td>
                                                    <td colspan="2">
                                                        <asp:TextBox runat="server" class="form-control input-xs" ID="txtl" disabled="disabled" /></td>
                                                </tr>
                                                <tr>
                                                    <td>5.</td>
                                                    <td>FFEL/Direct PLUS (Parent)</td>
                                                    <td colspan="2">
                                                        <asp:TextBox runat="server" class="form-control input-xs" ID="txtm" disabled="disabled" /></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td><strong>Total loans the school must return</strong></td>
                                                    <td><strong>P.</strong></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                            <asp:TextBox runat="server" class="form-control input-xs" ID="txtStep6P" disabled="disabled" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>6.</td>
                                                    <td>Pell Grant</td>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="txtn" runat="server" class="form-control input-xs" disabled="disabled" /></td>
                                                </tr>
                                                <!--<tr>
                                                    <td>7.</td>
                                                    <td>Academic Competitiveness Grant</td>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="txto" runat="server" class="form-control input-xs"  disabled="disabled" /></td>
                                                </tr>
                                                <tr>
                                                    <td>8.</td>
                                                    <td>National SMART Grant</td>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="txtp" runat="server" class="form-control input-xs"  disabled="disabled" /></td>
                                                </tr>-->
                                                <tr>
                                                    <td>7.</td>
                                                    <td>FSEOG</td>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="txtq" runat="server" class="form-control input-xs" disabled="disabled" /></td>
                                                </tr>
                                                <tr>
                                                    <td>8.</td>
                                                    <td>TEACH Grant</td>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="txtr" runat="server" class="form-control input-xs" disabled="disabled" /></td>
                                                </tr>
                                                <tr>
                                                    <td>9.</td>
                                                    <td>Iraq Afghanistan Service Grant</td>
                                                    <td colspan="2">
                                                        <asp:TextBox ID="txts" runat="server" class="form-control input-xs" disabled="disabled" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading clickable"><strong><span class="badge">STEP 7:</span> Initial Amount of Unearned Title IV Aid Due from the Student</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                                <div class="panel-body no-padding">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <tbody>
                                                <tr>
                                                    <td colspan="7">Subtract the amount of Title IV aid due from the school (Box O) from the amount of Title IV aid to be returned (Box K).</td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td><strong>
                                                        <asp:TextBox ID="txt7K" runat="server" class="form-control input-xs" disabled="disabled" />
                                                    </strong></td>
                                                    <td><i class="fa fa-minus"></i></td>
                                                    <td><strong>
                                                        <asp:TextBox ID="txt7O" runat="server" class="form-control input-xs" disabled="disabled" /></strong></td>
                                                    <td>
                                                        <h4>=</h4>
                                                    </td>
                                                    <td><strong>Q.</strong></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                            <strong>
                                                                <asp:TextBox ID="txtStep7Result" runat="server" class="form-control input-xs" disabled="disabled" /></strong>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td>Box K</td>
                                                    <td></td>
                                                    <td>Box O</td>
                                                    <td colspan="3"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading clickable"><strong><span class="badge">STEP 8:</span> Repayment of the Student's loans</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                                <div class="panel-body no-padding">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <tbody>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="6">Subtract the Total loans the school must return (Box P) from the Net loans disbursed to the student (Box B) to find the amount of Title IV loans the student is still responsible for repaying (Box R).</td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="6">These outstanding loans consist either of loan funds the student has earned, or unearned loan funds that the school is not responsible for repaying, or both; and they are repaid to the loan holders according to the terms of the borrower's promissory note.</td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td><strong>
                                                        <asp:TextBox ID="txt8boxB" runat="server" class="form-control input-xs" disabled="disabled" /></strong></td>
                                                    <td><i class="fa fa-minus"></i></td>
                                                    <td><strong>
                                                        <asp:TextBox ID="txt8P" runat="server" class="form-control input-xs" disabled="disabled" /></strong></td>
                                                    <td>
                                                        <h4>=</h4>
                                                    </td>
                                                    <td><strong>R.</strong></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                            <asp:TextBox ID="txt8BoxR" runat="server" class="form-control input-xs" disabled="disabled" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td>Box B</td>
                                                    <td></td>
                                                    <td>Box P</td>
                                                    <td colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <th><i class="fa fa-hand-o-right"></i></th>
                                                    <td colspan="6">If Box Q is less than or equal to Box R, <strong>STOP</strong>.<br>
                                                        <em>The only action a school must take is to notify the holders of the loans of the student's withdrawal date.</em></td>
                                                </tr>
                                                <tr>
                                                    <th><i class="fa fa-hand-o-right"></i></th>
                                                    <td colspan="6">If Box Q is greater than Box R, proceed to Step 9</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading clickable"><strong><span class="badge">STEP 9:</span> Grant Funds to be Returned</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                                <div class="panel-body no-padding">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <tbody>
                                                <tr>
                                                    <th>S.</th>
                                                    <th colspan="6">Initial amount of Title IV grants for student to return</th>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="6">Subtract the amount of loans to be repaid by the student (Box R) from the initial amount of unearned Title IV aid due from the  student (Box Q).</td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td><strong>
                                                        <asp:TextBox ID="txt9BoxQ" runat="server" class="form-control input-xs" disabled="disabled" /></strong></td>
                                                    <td><i class="fa fa-minus"></i></td>
                                                    <td><strong>
                                                        <asp:TextBox ID="txt9BoxR" runat="server" class="form-control input-xs" disabled="disabled" /></strong></td>
                                                    <td>
                                                        <h4>=</h4>
                                                    </td>
                                                    <td><strong>S.</strong></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                            <asp:TextBox ID="txt9BoxSResult" runat="server" class="form-control input-xs" disabled="disabled" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td>Box Q</td>
                                                    <td></td>
                                                    <td>Box R</td>
                                                    <td colspan="3"></td>
                                                </tr>
                                                <tr>
                                                    <th>T.</th>
                                                    <th colspan="6">Amount of Title IV grant protection</th>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="6">Multiply the total of Title IV grant aid that was disbursed and could have been disbursed for the payment period or period of enrollment (Box F) by 50%.</td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td><strong>
                                                        <asp:TextBox ID="txt9BoxF" runat="server" class="form-control input-xs" disabled="disabled" /></strong></td>
                                                    <td>
                                                        <h4>x</h4>
                                                    </td>
                                                    <td><strong>
                                                        <asp:Label ID="lbl50" runat="server" Font-Bold="True">50%</asp:Label></strong></td>
                                                    <td>
                                                        <h4>=</h4>
                                                    </td>
                                                    <td><strong>T.</strong></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                            <asp:TextBox ID="txt9BoxTResult" runat="server" class="form-control input-xs" disabled="disabled" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td align="center">Box F</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <th>U.</th>
                                                    <th colspan="6">Title IV grant funds for student to return</th>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td colspan="6">Subtract the protected amount of Title IV grants (Box T) from the initial amount of Title IV grants for student to return (Box S).</td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td><strong>
                                                        <asp:TextBox ID="txt9Boxs" runat="server" class="form-control input-xs" disabled="disabled" /></strong></td>
                                                    <td><i class="fa fa-minus"></i></td>
                                                    <td><strong>
                                                        <asp:TextBox ID="txt9BoxT" runat="server" class="form-control input-xs" disabled="disabled" /></strong></td>
                                                    <td>
                                                        <h4>=</h4>
                                                    </td>
                                                    <td><strong>U.</strong></td>
                                                    <td>
                                                        <div class="input-group">
                                                            <div class="input-group-addon"><i class="fa fa-usd"></i></div>
                                                            <asp:TextBox ID="txtBox9Result" runat="server" class="form-control input-xs" disabled="disabled" />
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <td>Box S</td>
                                                    <td></td>
                                                    <td>Box T</td>
                                                    <td colspan="3"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="panel panel-primary">
                                <div class="panel-heading clickable"><strong><span class="badge">STEP 10:</span> Return of Grant Funds by the Student</strong> <span class="pull-right"><i class="fa fa-minus"></i></span></div>
                                <div class="panel-body no-padding">
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                            <tbody>
                                                <tr>
                                                    <td colspan="3">Except as noted below, the student must return the unearned grant funds for which he or she is responsible (Box U). The grant funds returned by the student are applied to the following sources in the order indicated, up to the total amount disbursed from that grant program minus any grant funds the school is responsible for returning to that program in Step 6.</td>
                                                </tr>
                                                <tr>
                                                    <th colspan="3">Note that the student is not responsible for returning funds to any program to which the student owes $50.00 or less.</th>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <th colspan="2">Title IV Grant Programs</th>
                                                </tr>
                                                <tr>
                                                    <td width="3%">1.</td>
                                                    <td>Pell Grant</td>
                                                    <td>
                                                        <asp:TextBox ID="txt10Pell" runat="server" class="form-control input-xs" disabled="disabled" /></td>
                                                </tr>
                                                <!-- <tr>
                                                    <td width="3%">2.</td>
                                                    <td>Academic Competitiveness Grant</td>
                                                    <td>
                                                        <asp:TextBox ID="txt10ACG" runat="server" class="form-control input-xs"  disabled="disabled" /></td>
                                                </tr>
                                                <tr>
                                                    <td width="3%">3.</td>
                                                    <td>National SMART Grant</td>
                                                    <td>
                                                        <asp:TextBox ID="txt10NSG" runat="server" class="form-control input-xs"  disabled="disabled" /></td>
                                                </tr>-->
                                                <tr>
                                                    <td width="3%">2.</td>
                                                    <td>FSEOG</td>
                                                    <td>
                                                        <asp:TextBox ID="txt10FSEOG" runat="server" class="form-control input-xs" disabled="disabled" /></td>
                                                </tr>
                                                <tr>
                                                    <td width="3%">3.</td>
                                                    <td>TEACH Grant</td>
                                                    <td>
                                                        <asp:TextBox ID="txt10TG" runat="server" class="form-control input-xs" disabled="disabled" /></td>
                                                </tr>
                                                <tr>
                                                    <td width="3%">4.</td>
                                                    <td>Iraq Afghanistan Service Grant</td>
                                                    <td>
                                                        <asp:TextBox ID="txt10IFSG" runat="server" class="form-control input-xs" disabled="disabled" /></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%-- <div class="col-md-12">
                        <h3>Balance Remaining: <i class="fa fa-usd"></i>350.90</h3>
                    </div>--%>
                </div>
            </div>
            <hr style="margin-bottom: 0;">
            <div class="footer">
                Copyright &copy; 2005 - 2017 FAME. All Rights Reserved.
            </div>
        </div>
        <!-- /container -->
    </form>
</body>
</html>
