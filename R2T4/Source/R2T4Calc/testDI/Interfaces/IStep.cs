﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="IStep.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the IStep type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace R2T4Engine.Interfaces
{
    /// <summary>
    /// The Step interface.
    /// </summary>
    public interface IStep
    {
        /// <summary>
        /// Gets the step id.
        /// </summary>
        int StepId { get;  }

        /// <summary>
        /// Gets the sequence.
        /// </summary>
        int Sequence { get; }

        /// <summary>
        /// The calculate.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="IR2T4Results"/>.
        /// </returns>
        IR2T4Results Calculate(IR2T4Input input);

        /// <summary>
        /// The validate.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        bool Validate(IR2T4Input input);
    }
}
