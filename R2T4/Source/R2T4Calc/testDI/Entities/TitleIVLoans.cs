﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TitleIVLoans.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the TitleIVLoans type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace R2T4Engine.Entities
{
    /// <summary>
    /// The title iv loans.
    /// </summary>
    // ReSharper disable once InconsistentNaming
    public class TitleIVLoans
    {
        /// <summary>
        /// Gets or sets the loan name.
        /// </summary>
        public string LoanName { get; set; }

        /// <summary>
        /// Gets or sets the amount disbursed.
        /// </summary>
        public decimal AmountDisbursed { get; set; }

        /// <summary>
        /// Gets or sets the amount expected.
        /// </summary>
        public decimal AmountExpected { get; set; }

        /// <summary>
        /// Gets or sets the Priority.
        /// </summary>
        public int Priority { get; set; }

        /// <summary>
        /// Gets or sets the amount to be returned back towards loan.
        /// </summary>
        public decimal AmountToBeReturned { get; set; }
    }
}
