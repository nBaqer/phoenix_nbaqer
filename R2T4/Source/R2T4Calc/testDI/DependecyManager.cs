﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="DependecyManager.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the DependecyManager Type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace R2T4Engine
{
    using InputOutput;
    using Interfaces;
    using Microsoft.Practices.Unity;
    using Steps;

    /// <summary>
    /// The dependecy manager.
    /// </summary>
    public static class DependecyManager
    {
        /// <summary>
        /// Gets or sets the container.
        /// </summary>
        public static UnityContainer Container { get; set; } = new UnityContainer();

        /// <summary>
        /// The manage di.
        /// </summary>
        public static void ManageDi()
        {
            Container.RegisterType(typeof(IStep), typeof(Step1));
            Container.RegisterType(typeof(IStep), typeof(Step2));
            Container.RegisterType(typeof(IStep), typeof(Step3));
            Container.RegisterType(typeof(IStep), typeof(Step4));
            Container.RegisterType(typeof(IStep), typeof(Step5));
            Container.RegisterType(typeof(IStep), typeof(Step6));
            Container.RegisterType(typeof(IStep), typeof(Step7));
            Container.RegisterType(typeof(IStep), typeof(Step9));
            Container.RegisterType(typeof(IR2T4Input), typeof(R2T4Input));
            Container.RegisterType(typeof(IR2T4Results), typeof(R2T4Results));
        }
    }
}
