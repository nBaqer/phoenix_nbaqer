﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4Results.cs" company="FAME Inc.">
//   FAME Inc.
// </copyright>
// <summary>
//   Defines the R2T4Results type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace R2T4Engine.InputOutput
{
    using Interfaces;
    using System.Collections.Generic;

    /// <summary>
    /// The r 2 t 4 results.
    /// </summary>
    public class R2T4Results : IR2T4Results
    {
        /// <summary>
        /// Gets or sets the validation result.
        /// </summary>
        public ValidationResult ValidationResult { get; set; }

        /// <summary>
        /// Gets or sets the validation message.
        /// </summary>
        public string ValidationMessage { get; set; }

        /// <summary>
        /// Gets or sets the step results.
        /// </summary>
        public Dictionary<string, decimal> StepResults { get; set; }
    }
}
