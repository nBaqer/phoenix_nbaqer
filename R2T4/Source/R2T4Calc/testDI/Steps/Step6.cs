﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Step6.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the Step6 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace R2T4Engine.Steps
{
    using System;
    using System.Linq;
    using Common;
    using Interfaces;

    /// <summary>
    /// The step 6.
    /// </summary>
    public class Step6 : IStep
    {
        /// <summary>
        /// Gets the sequence.
        /// </summary>
        public int Sequence => Constants.Step6;

        /// <summary>
        /// Gets the step id.
        /// </summary>
        public int StepId => Constants.Step6;

        /// <summary>
        /// This method calculates the step 6 results
        /// </summary>
        /// <param name="input">
        /// The input <see cref="IR2T4Input"/>.
        /// </param>
        /// <returns>
        /// The <see cref="IR2T4Results"/>.
        /// </returns>
        public IR2T4Results Calculate(IR2T4Input input)
        {
            input.Results.ValidationMessage = string.Empty;
            try
            {
                if (this.Validate(input))
                {
                    decimal boxOValue = input.Results.StepResults[Constants.ResultKeyBoxO];
                    foreach (var loan in input.Loans.OrderBy(priority => priority.Priority))
                    {
                        if (loan.AmountDisbursed > 0)
                        {
                            if (boxOValue > loan.AmountDisbursed)
                            {
                                loan.AmountToBeReturned = loan.AmountDisbursed;
                                boxOValue = boxOValue - loan.AmountToBeReturned;
                            }
                            else
                            {
                                loan.AmountToBeReturned = boxOValue;
                                boxOValue = 0.00m;
                            }

                            if (loan.AmountToBeReturned > 0)
                            {
                                input.Results.StepResults.Add(loan.LoanName, loan.AmountToBeReturned);
                            }
                        }
                    }

                    decimal totalLoansReturned = input.Loans.Sum(charge => charge.AmountToBeReturned);
                    input.Results.StepResults.Add(
                        Constants.ResultKeyBoxP,
                        totalLoansReturned == 0 ? 0.00m : totalLoansReturned);

                    foreach (var grant in input.Grants.OrderBy(priority => priority.Priority))
                    {
                        if (grant.AmountDisbursed > 0)
                        {
                            if (boxOValue > grant.AmountDisbursed)
                            {
                                grant.AmountToBeReturned = grant.AmountDisbursed;
                                boxOValue = boxOValue - grant.AmountToBeReturned;
                            }
                            else
                            {
                                grant.AmountToBeReturned = boxOValue;
                                boxOValue = 0.00m;
                            }

                            if (grant.AmountToBeReturned > 0)
                            {
                                input.Results.StepResults.Add(grant.GrantName, grant.AmountToBeReturned);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                input.Results.ValidationResult = ValidationResult.Exception;
                input.Results.ValidationMessage = ex.Message;
            }

            return input.Results;
        }

        /// <summary>
        /// Validates the required fields in input for calculation of step 6
        /// </summary>
        /// <param name="input">
        /// The input <see cref="IR2T4Input"/>
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>
        /// </returns>
        public bool Validate(IR2T4Input input)
        {
            var isValid = (input.Grants != null && input.Loans != null) &&
                          (input.Grants.Any(x => x.AmountDisbursed > 0 || x.AmountExpected > 0) ||
                           input.Loans.Any(y => y.AmountDisbursed > 0 || y.AmountExpected > 0));

            if (!isValid)
            {
                input.Results.ValidationResult = ValidationResult.Warning;
                input.Results.ValidationMessage = Messages.InputValuesNull;
            }

            return isValid;
        }
    }
}
