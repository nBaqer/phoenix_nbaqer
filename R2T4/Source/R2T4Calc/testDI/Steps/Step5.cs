﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Step5.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the Step3 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace R2T4Engine.Steps
{
    using System;
    using System.Linq;
    using R2T4Engine.Common;
    using R2T4Engine.Interfaces;

    /// <summary>
    /// The step 5.
    /// </summary>
    public class Step5 : IStep
    {
      /// <summary>
      /// Gets the sequence.
      /// </summary>
        public int Sequence => Constants.Step5;

        /// <summary>
        /// Gets the step id.
        /// </summary>
        public int StepId => Constants.Step5;

        /// <summary>
        /// The method calculates the step 2 results like Box H and actual attendence for clock and credit hour programs
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="IR2T4Results"/>.
        /// </returns>
        public IR2T4Results Calculate(IR2T4Input input)
        {
            if (this.Validate(input))
            {
                try
                {
                    var boxKValue = input.Results.StepResults[Constants.ResultKeyBoxK];
                    decimal totalCharges = input.Student.InstitutionalCharges.Sum(charge => charge.AmountCharged);
                    decimal boxMValue = Constants.TotalPercent
                                        - Convert.ToDecimal(input.Results.StepResults[Constants.ResultKeyBoxH]);
                    input.Results.StepResults.Add(Constants.TotalinstitutionalCharges, totalCharges);
                    input.Results.StepResults.Add(Constants.ResultKeyBoxM, boxMValue);
                    if (input.PaymentType == PaymentTypes.PaymentPeriod && input.Student.IsTutionCharged)
                    {
                        decimal boxLValueInN = input.Results.StepResults[Constants.ResultKeyBoxE]
                                               - input.Student.CreditBalanceRefunded;
                        input.Results.StepResults.Add(
                            Constants.ResultKeyBoxL,
                            boxLValueInN > totalCharges ? boxLValueInN : totalCharges);
                    }
                    else
                    {
                        input.Results.StepResults.Add(Constants.ResultKeyBoxL, totalCharges);
                    }

                    decimal boxNValue = Math.Round(
                        input.Results.StepResults[Constants.ResultKeyBoxL]
                        * (input.Results.StepResults[Constants.ResultKeyBoxM] / 100),
                        2);
                    input.Results.StepResults.Add(Constants.ResultKeyBoxN, boxNValue);
                    input.Results.StepResults.Add(Constants.ResultKeyBoxO, boxKValue > input.Results.StepResults[Constants.ResultKeyBoxN] ? input.Results.StepResults[Constants.ResultKeyBoxN] : input.Results.StepResults[Constants.ResultKeyBoxK]);
                }
                catch (Exception ex)
                {
                    input.Results.ValidationResult = ValidationResult.Exception;
                    input.Results.ValidationMessage = ex.Message;
                }
            }

            return input.Results;
        }

        /// <summary>
        /// The method validates whther the provided input hours/days vales are greater than zero or not
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Validate(IR2T4Input input)
        {
            var isValid = input.Student.InstitutionalCharges != null && input.Student.InstitutionalCharges.Any(x => x.AmountCharged >= 0) && 
                input.Results.StepResults.ContainsKey(Constants.ResultKeyBoxH) &&
                input.Results.StepResults.ContainsKey(Constants.ResultKeyBoxE) && 
                input.Results.StepResults.ContainsKey(Constants.ResultKeyBoxK);
            if (!isValid)
            {
                input.Results.ValidationResult = ValidationResult.Warning;
                input.Results.ValidationMessage = Messages.InputValuesNull;
            }

            return isValid;
        }
    }
}
