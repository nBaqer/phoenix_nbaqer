﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Step8.cs" company="FAME Inc.">
//   FANE Inc. 2017
// </copyright>
// <summary>
//   Defines the Step1 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace R2T4Engine.Steps
{
    using System;
    using R2T4Engine.Common;
    using R2T4Engine.Interfaces;

    /// <summary>
    /// The step 8.
    /// </summary>
    public class Step8 : IStep
    {
        /// <summary>
        /// Gets the sequence.
        /// </summary>
        public int Sequence => Constants.Step8;

        /// <summary>
        /// Gets the step id.
        /// </summary>
        public int StepId => Constants.Step8;

        /// <summary>
        /// The method calculates the step 8 results Box R 
        /// </summary>
        /// <param name="input">
        /// The input object of IR2T4Inout
        /// </param>
        /// <returns>
        /// The <see cref="IR2T4Results"/>.
        /// </returns>
        public IR2T4Results Calculate(IR2T4Input input)
        {
            if (this.Validate(input))
            {
                try
                {
                    decimal difference = 0.0m;
                    if (input.Results.StepResults[Constants.ResultKeyBoxQ] > 0)
                    {
                        if (input.Results.StepResults.ContainsKey(Constants.ResultKeyBoxP)
                            && input.Results.StepResults.ContainsKey(Constants.ResultKeyBoxB))
                        {
                            difference = input.Results.StepResults[Constants.ResultKeyBoxB]
                                         - input.Results.StepResults[Constants.ResultKeyBoxP];
                        }
                        else if (input.Results.StepResults.ContainsKey(Constants.ResultKeyBoxB))
                        {
                            difference = input.Results.StepResults[Constants.ResultKeyBoxB];
                        }

                        input.Results.StepResults.Add(Constants.ResultKeyBoxR, difference);
                        input.IsStep10Required = true;
                        input.IsStep9Required = true;
                    }
                    else if (input.Results.StepResults[Constants.ResultKeyBoxQ] <= 0)
                    {
                        input.IsStep10Required = false;
                        input.IsStep9Required = false;
                    }
                }
                catch (Exception ex)
                {
                    input.Results.ValidationResult = ValidationResult.Exception;
                    input.Results.ValidationMessage = ex.Message;
                }
            }

            return input.Results;
        }

        /// <summary>
        /// The method validates whether the provided input has BoxB and BoxP keys
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Validate(IR2T4Input input)
        {
            var isValid = input.Results.StepResults != null;
            if (!isValid)
            {
                input.Results.ValidationResult = ValidationResult.Warning;
                input.Results.ValidationMessage = Messages.InputValuesNull;
            }

            return isValid;
        }
    }
}
