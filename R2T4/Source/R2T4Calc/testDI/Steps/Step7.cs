﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Step7.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the Step7 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace R2T4Engine.Steps
{
    using System;

    using Common;

    using Interfaces;

    /// <summary>
    /// The step 7.
    /// </summary>
    public class Step7 : IStep
    {
        /// <summary>
        /// Gets the sequence.
        /// </summary>
        public int Sequence => Constants.Step7;

        /// <summary>
        /// Gets the step id.
        /// </summary>
        public int StepId => Constants.Step7;

        /// <summary>
        /// This method calculates the step 7 results for Box Q
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="IR2T4Results"/>.
        /// </returns>
        public IR2T4Results Calculate(IR2T4Input input)
        {
            input.Results.ValidationMessage = string.Empty;
            try
            {
                if (this.Validate(input))
                {
                    input.IsStep4Required = true;
                    var boxKResultValue = input.Results.StepResults[Constants.ResultKeyBoxK];
                    var boxOResultValue = input.Results.StepResults[Constants.ResultKeyBoxO];

                    if (boxKResultValue > 0)
                    {
                        var boxQValue = Math.Round(boxKResultValue - boxOResultValue, 2);
                        input.Results.StepResults.Add(Constants.ResultKeyBoxQ, boxQValue);
                    }
                }
            }
            catch (Exception ex)
            {
                input.Results.ValidationResult = ValidationResult.Exception;
                input.Results.ValidationMessage = ex.Message;
            }

            return input.Results;
        }

        /// <summary>
        /// Validates the required fields in input for calculation of step 7
        /// </summary>
        /// <param name="input">
        /// The input <see cref="IR2T4Input"/>
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>
        /// </returns>
        public bool Validate(IR2T4Input input)
        {
            var isValid = input.Results.StepResults != null && input.Results.StepResults.ContainsKey(Constants.ResultKeyBoxK) &&
                          input.Results.StepResults.ContainsKey(Constants.ResultKeyBoxO);
            if (!isValid)
            {
                input.Results.ValidationResult = ValidationResult.Warning;
                input.Results.ValidationMessage = Messages.InputValuesNull;
            }

            return isValid;
        }
    }
}
