﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Step1.cs" company="FAME Inc.">
//   FANE Inc. 2017
// </copyright>
// <summary>
//   Defines the Step1 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace R2T4Engine.Steps
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Common;
    using Interfaces;

    /// <summary>
    /// The step 1.
    /// </summary>
    public class Step1 : IStep
    {
        /// <summary>
        /// Gets the step id.
        /// </summary>
        public int StepId => Constants.Step1;

        /// <summary>
        /// Gets the sequence.
        /// </summary>
        public int Sequence => Constants.Step1;

        /// <summary>
        /// This method calculates the results of A,B,C,D,E,F,G box vlues using the input grants and loans provided 
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="IR2T4Results"/>.
        /// </returns>
        public IR2T4Results Calculate(IR2T4Input input)
        {
            input.Results.ValidationMessage = string.Empty;
            if (input.Results.StepResults == null)
            {
                input.Results.StepResults = new Dictionary<string, decimal>(2);
            }

            if (this.Validate(input))
            {
                try
                {
                    decimal subTotalA = input.Grants.Sum(grant => grant.AmountDisbursed);
                    decimal subTotalC = input.Grants.Sum(grant => grant.AmountExpected);
                    decimal subTotalB = input.Loans.Sum(loan => loan.AmountDisbursed);
                    decimal subTotalD = input.Loans.Sum(loan => loan.AmountExpected);

                    input.Results.StepResults.Add(Constants.ResultKeyBoxA, subTotalA);
                    input.Results.StepResults.Add(Constants.ResultKeyBoxC, subTotalC);
                    input.Results.StepResults.Add(Constants.ResultKeyBoxB, subTotalB);
                    input.Results.StepResults.Add(Constants.ResultKeyBoxD, subTotalD);

                    input.Results.StepResults.Add(Constants.ResultKeyBoxE, subTotalA + subTotalB);
                    input.Results.StepResults.Add(Constants.ResultKeyBoxF, subTotalA + subTotalC);
                    input.Results.StepResults.Add(Constants.ResultKeyBoxG, subTotalA + subTotalB + subTotalC + subTotalD);
                    input.Results.ValidationResult = ValidationResult.Pass;
                }
                catch (Exception ex)
                {
                    input.Results.ValidationResult = ValidationResult.Exception;
                    input.Results.ValidationMessage = ex.Message;
                }
            }

            return input.Results;
        }

        /// <summary>
        /// The validate. this method validates the input values of grants and amounts to be disbursed are valid or not. 
        /// This also checks whether the input parameters have atleast one value in grants or loans.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>
        /// </returns>
        public bool Validate(IR2T4Input input)
        {
            var isValid = (input.Grants != null && input.Loans != null) &&
                           (input.Grants.Any(x => x.AmountDisbursed > 0 || x.AmountExpected > 0) ||
                            input.Loans.Any(y => y.AmountDisbursed > 0 || y.AmountExpected > 0));

            if (!isValid)
            {
                input.Results.ValidationResult = ValidationResult.Warning;
                input.Results.ValidationMessage = Messages.InputValuesNull;
            }

            return isValid;
        }
    }
}
