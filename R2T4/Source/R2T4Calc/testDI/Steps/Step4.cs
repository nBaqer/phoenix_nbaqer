﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Step4.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the Step4 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace R2T4Engine.Steps
{
    using System;

    using Common;

    using Interfaces;

    /// <summary>
    /// The step 4.
    /// </summary>
    public class Step4 : IStep
    {
        /// <summary>
        /// Gets the sequence.
        /// </summary>
        public int Sequence => Constants.Step4;

        /// <summary>
        /// Gets the step id.
        /// </summary>
        public int StepId => Constants.Step4;

        /// <summary>
        /// This method calculates the step 4 results for Box J and Box K
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="IR2T4Results"/>.
        /// </returns>
        public IR2T4Results Calculate(IR2T4Input input)
        {
            input.Results.ValidationMessage = string.Empty;
            try
            {
                if (this.Validate(input))
                {
                    input.IsStep4Required = true;
                    decimal boxIValue = input.Results.StepResults[Constants.ResultKeyBoxI];
                    decimal boxEValue = input.Results.StepResults[Constants.ResultKeyBoxE];

                    // Post-withdrawal disbursement
                    if (boxIValue > boxEValue)
                    {
                        // calculate and assign the results to Box J.
                        decimal boxJResult = Math.Round(boxIValue - boxEValue, 2);
                        input.Results.StepResults.Add(Constants.ResultKeyBoxJ, boxJResult);
                        input.IsCalculationRequired = false;
                    }
                    else if (boxIValue < boxEValue)
                    {
                        // calculate and assign the results to Box K.
                        decimal boxKValue = Math.Round(boxEValue - boxIValue, 2);
                        input.Results.StepResults.Add(Constants.ResultKeyBoxK, boxKValue);
                        input.IsCalculationRequired = true;
                    }
                    else if (boxIValue == boxEValue)
                    {
                        input.IsStep4Required = false;
                    }
                }
            }
            catch (Exception ex)
            {
                input.Results.ValidationResult = ValidationResult.Exception;
                input.Results.ValidationMessage = ex.Message;
            }

            return input.Results;
        }

        /// <summary>
        /// This method validates the input values of Box I and Box E whether the values are greater than 0 or not.
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Validate(IR2T4Input input)
        {
            var isValid = input.Results?.StepResults != null
                          && input.Results.StepResults.ContainsKey(Constants.ResultKeyBoxI)
                          && input.Results.StepResults.ContainsKey(Constants.ResultKeyBoxE) && (input.Results.StepResults[Constants.ResultKeyBoxI] >= 0
                          || input.Results.StepResults[Constants.ResultKeyBoxE] >= 0);

            if (!isValid && input.Results != null)
            {
                input.Results.ValidationResult = ValidationResult.Warning;
                input.Results.ValidationMessage = Messages.InputValuesNull;
            }

            return isValid;
        }
    }
}
