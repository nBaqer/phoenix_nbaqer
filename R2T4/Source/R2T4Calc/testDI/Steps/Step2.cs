﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Step2.cs" company="FAME Inc.">
//   FAME Inc. 2017
// </copyright>
// <summary>
//   Defines the Step2 type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace R2T4Engine.Steps
{
    using System;
    using Common;
    using Interfaces;

    /// <summary>
    /// The step 2.
    /// </summary>
    public class Step2 : IStep
    {
        /// <summary>
        /// Gets the sequence.
        /// </summary>
        public int Sequence => Constants.Step2;

        /// <summary>
        /// Gets the step id.
        /// </summary>
        public int StepId => Constants.Step2;

        /// <summary>
        /// The method calculates the step 2 results like Box H and actual attendence for clock and credit hour programs
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="IR2T4Results"/>.
        /// </returns>
        public IR2T4Results Calculate(IR2T4Input input)
        {
            if (this.Validate(input))
            {
                try
                {
                    decimal actualAttendence = 0.00m;
                    decimal attendencePercentage = 0.00m;
                    if (input.ProgramType == ProgramType.ClockHour)
                    {
                        actualAttendence = Math.Round(input.Student.ScheduledHours / input.Student.TotalHours, 3);
                        attendencePercentage = Math.Round(actualAttendence * 100, 2);
                        if (attendencePercentage > 60)
                        {
                            attendencePercentage = 100.00m;
                        }
                    }
                    else if (input.ProgramType == ProgramType.CreditHour)
                    {
                        if (input.Student.IsAttendanceRequired)
                        {
                            actualAttendence = Math.Round(input.Student.ScheduledDays / input.Student.TotalDays, 3);
                            attendencePercentage = Math.Round(actualAttendence * 100, 2);
                            if (attendencePercentage > 60)
                            {
                                attendencePercentage = 100.00m;
                            }
                        }
                        else
                        {
                            attendencePercentage = 50.0m;
                        }
                    }

                    input.Results.StepResults.Add(Constants.ResultKeyBoxH, Math.Round(attendencePercentage, 1));
                    input.Results.StepResults.Add(Constants.ResultKeyActualAttendance, decimal.Parse(actualAttendence.ToString("N3")));
                }
                catch (Exception ex)
                {
                    input.Results.ValidationResult = ValidationResult.Exception;
                    input.Results.ValidationMessage = ex.Message;
                }
            }

            return input.Results;
        }

        /// <summary>
        /// The method validates whther the provided input hours/days vales are greater than zero or not
        /// </summary>
        /// <param name="input">
        /// The input.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        public bool Validate(IR2T4Input input)
        {
            bool isValid = true;

            // Validating for nullability of input and if scheduled hours is greater than total hours
            if (input.ProgramType == ProgramType.ClockHour)
            {
                if (input.Student.ScheduledHours <= 0 || input.Student.TotalHours <= 0)
                {
                    isValid = false;
                    input.Results.ValidationResult = ValidationResult.Warning;
                    input.Results.ValidationMessage = Messages.InvalidInputsStep2;
                }
                else if (input.Student.ScheduledHours > input.Student.TotalHours)
                {
                    isValid = false;
                    input.Results.ValidationResult = ValidationResult.Warning;
                    input.Results.ValidationMessage = Messages.ScheduledGreater;
                }
            }

            if (input.ProgramType == ProgramType.CreditHour)
            {
                if (input.Student.IsAttendanceRequired)
                {
                    if (input.Student.ScheduledDays <= 0 || input.Student.TotalDays <= 0)
                    {
                        isValid = false;
                        input.Results.ValidationResult = ValidationResult.Warning;
                        input.Results.ValidationMessage = Messages.InvalidInputsStep2;
                    }
                    else if (input.Student.ScheduledDays > input.Student.TotalDays)
                    {
                        isValid = false;
                        input.Results.ValidationResult = ValidationResult.Warning;
                        input.Results.ValidationMessage = Messages.ScheduledGreater;
                    }
                }
                else
                {
                    isValid = true;
                }
            }

            return isValid;
        }
    }
}
