﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="R2T4Facade.cs" company="FAME Inc.">
//   FAME Inc, 2017
// </copyright>
// <summary>
//   Defines the R2T4Facade type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace R2T4Engine
{
    using System.Collections.Generic;
    using System.Linq;

    using Common;

    using InputOutput;

    using Interfaces;

    using Microsoft.Practices.Unity;

    using Steps;

    /// <summary>
    /// The r 2 t 4 facade.
    /// </summary>
    public class R2T4Facade
    {
        /// <summary>
        /// The steps.
        /// </summary>
        private IList<IStep> steps;

        /// <summary>
        /// Initializes a new instance of the <see cref="R2T4Facade"/> class.
        /// </summary>
        public R2T4Facade()
        {
            this.steps = new List<IStep>
            {
                new Step1(),
                new Step2(),
                new Step3(),
                new Step4(),
                new Step5(),
                new Step6(),
                new Step7(),
                new Step8(),
                new Step9(),
                new Step10()
            };
        }

        /// <summary>
        /// The calculate r 2 t 4.
        /// </summary>
        /// <param name="inputInfo">
        /// The input info.
        /// </param>
        /// <returns>
        /// The <see cref="IR2T4Results"/>.
        /// </returns>
        public IR2T4Results CalculateR2T4(IR2T4Input inputInfo)
        {
            IR2T4Input input = inputInfo;
            IR2T4Results results = DependecyManager.Container.Resolve<R2T4Results>();
            results.StepResults = new Dictionary<string, decimal>();
            input.Results = results;
            foreach (IStep step in this.steps.OrderBy(x => x.StepId))
            {
                if (step.StepId > Constants.Step4 && (!input.IsCalculationRequired || !inputInfo.IsStep4Required))
                {
                    break;
                }
                else if (step.StepId > Constants.Step8 && (!input.IsStep9Required || !inputInfo.IsStep10Required))
                {
                    break;
                }
                else if (step.StepId > Constants.Step9 && !input.IsStep10Required)
                {
                    break;
                }

                input.Results = step.Calculate(input);
                if (input.Results.ValidationResult != ValidationResult.Pass)
                {
                    break;
                }
            }

            return input.Results;
        }
    }
}
