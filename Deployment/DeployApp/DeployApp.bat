@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
SETLOCAL
GOTO Start

:Usage
ECHO ------------------------------------------------------
ECHO -                      U S A G E                     -
ECHO ------------------------------------------------------
ECHO.
ECHO DeployApp:
ECHO   Deploys a drop build application to a target share
ECHO.
ECHO Usage:
ECHO   DeployApp TargetFolder [SourceFolder] [Environment] 
ECHO             [SkipFileSpecs] [DelFileSpecs] [DeleteTarget]
ECHO             [LogFile]
ECHO.
ECHO Arguments:
ECHO   TargetFolder	UNC of the deployment target application folder
ECHO			      Ex: \\qafa\inetpub\wwwroot\eSolutions\Dev\Phase3
ECHO   SourceFolder	Optional: Path to the TFS build drop folder
ECHO                  Ex: 
ECHO            	   \\vm-tfs1\Builds\Internal\Online System - Main\
ECHO				   Online System - Main_YYYY.MM.DD.XX
ECHO                  Default: current directory
ECHO                  Note: Typically DeployApp.bat would be in the 
ECHO                  TFS build drop folder, in which case you only need 
ECHO                  	to specify subfolder
ECHO   Environment	Optional: Name of the deployment environment. 
ECHO			      Ex: Prod
ECHO			      Note: This will be used for getting and setting the proper config files
ECHO                  Default: QA
ECHO   SkipFileSpecs Optional: Space delimited list of file names and/or 
ECHO                   folders to be skipped during the copying process.
ECHO                  Ex: "FA.Application Application\Logs\"
ECHO   DelFileSpecs Optional: Space delimited list of file names and/or 
ECHO                   folders to be deleted.
ECHO                  Ex: "FA.Application *.Manifest *.Pdb Application\Logs"
ECHO                  Note (files): Wild card operator (*) can be used
ECHO                  Note (folders): RD is perfomerd only on current folder.
ECHO                   For sub-folders, pass the folder structured (bottom to top)
ECHO                   as separate items in the FileSpecs argument. To remove the 
ECHO                   "Test" folder tree and its files "Temp\Test\Suite\Failed" set
ECHO                   set FileSpecs to 
ECHO                   "Temp\Test\Suite\Failed" "Temp\Test\Suite" "Temp\Test"
ECHO   DeleteTarget	Optional: Y/N
ECHO                  Y=Delete files on target application folder 
ECHO                  N=Leave files on target application folder 
ECHO                  Default: N
ECHO   LogFile	    Optional: Y/N
ECHO                  Y=Send output to log file
ECHO                  N=Send output to console
ECHO                  Default: N
GOTO End

:Start
SET TargetFolder=%~1
SET SourceFolder=%~2
SET Environment=%~3
SET SkipFileSpecs=%~4
SET DelFileSpecs=%~5
SET DeleteTarget=%~6
SET LogFile=%~7

IF "%TargetFolder%"=="?" GOTO Usage
IF "%TargetFolder%"=="/?" GOTO Usage

IF "%SourceFolder%"=="" SET SourceFolder=%~dp0 
REM We will remove the current folder name in case it's used by the exclusion list
REM Also, we will remove the \.. since removing the current folder name will set us
REM to the folder where we want to copy files from
SET SourceFolder=%SourceFolder:_Deploy\=%
SET SourceFolder=%SourceFolder:..=%
	
IF "%TargetFolder%"=="" GOTO Usage
IF "%Environment%"=="" SET Environment=QA
IF "%LogFile%"=="" SET LogFile=N
IF "%DeleteTarget%"=="" SET DeleteTarget=N

SET ExitCode=0
SET OutputFolder=%~dp0
SET LogFileName=%OutputFolder%%~n0.log
SET ExclusionFile=%TEMP%\ExclList.txt

IF NOT "%SourceFolder:~-1%"=="\" SET SourceFolder=%SourceFolder%\
IF NOT "%TargetFolder:~-1%"=="\" SET TargetFolder=%TargetFolder%\


REM -- Create exclusion list file
IF EXIST %ExclusionFile% DEL %ExclusionFile%
ECHO. > %ExclusionFile%
IF NOT "%ExitCode%"=="0" GOTO Error

IF NOT "%SkipFileSpecs%"=="" (

	IF EXIST %ExclusionFile% DEL %ExclusionFile%
	SET Remaining=%SkipFileSpecs%
	
	:Loop
	FOR /F "tokens=1*" %%a IN ("%Remaining%") DO (

		SET Remaining=%%b
		ECHO %%a >> %ExclusionFile%

		SET ExitCode=%ERRORLEVEL%
		IF NOT "%ExitCode%"=="0" GOTO Error
		
	)
	IF DEFINED Remaining GOTO Loop
)

SET DelTarget=DEL "%TargetFolder%*.*" /f/s/q
SET PubApp=XCOPY "%SourceFolder%*.*" "%TargetFolder%" /q/i/s/e/k/r/h/y /EXCLUDE:%ExclusionFile%
SET EnvConfig="%~dp0ConfigEnv" "%TargetFolder%" "%Environment%" %LogFile%
SET DelExt="%~dp0DeployCleanup" "%TargetFolder%" "%DelFileSpecs%"

IF /i NOT %Debug%==ON GOTO SkipDebug
ECHO ------------------------------------------------------
ECHO -                     D E B U G                      -
ECHO ------------------------------------------------------
ECHO.
ECHO TargetFolder		%TargetFolder%
ECHO SourceFolder		%SourceFolder%
ECHO Environment		%Environment%
ECHO SkipFileSpecs		%SkipFileSpecs%
ECHO DelFileSpecs		%DelFileSpecs%
ECHO DeleteTarget		%DeleteTarget%
ECHO.
ECHO LogFile			%LogFile%
ECHO LogFileName      	%LogFileName%
ECHO.
ECHO DelTarget:
ECHO %DelTarget%
ECHO.
ECHO PubApp:
ECHO %PubApp%
ECHO.
ECHO Config Env:
ECHO %EnvConfig%
ECHO.
ECHO DelExt:
ECHO %DelExt%
ECHO.
ECHO.
PAUSE
:SkipDebug

SET ExitCode=0
SET RedirLog=
IF /i "%LogFile%"=="Y" SET RedirLog=^>^> "%LogFileName%"

ECHO Publishing Application %SourceFolder% To %TargetFolder% %RedirLog%

IF /i "%DeleteTarget%"=="Y" (
ECHO Deleting contents of target folder... %RedirLog%
%DelTarget% > NUL
)

ECHO Copying drop files to target folder... %RedirLog%
%PubApp% %RedirLog%
SET ExitCode=%ERRORLEVEL%
IF NOT "%ExitCode%"=="0" GOTO Error

ECHO Setting up config files for environment %RedirLog%
CALL %EnvConfig%
SET ExitCode=%ERRORLEVEL%
IF NOT "%ExitCode%"=="0" GOTO Error

IF NOT "%DelFileSpecs%"=="" (
	CALL %DelExt%
	SET ExitCode=%ERRORLEVEL%
	IF NOT "%ExitCode%"=="0" GOTO Error
)

ECHO. %RedirLog%

GOTO End

:Error
ECHO Terminating with ExitCode %ExitCode% %RedirLog%
ECHO. %RedirLog%

:End
EXIT /b %ExitCode%
ENDLOCAL
