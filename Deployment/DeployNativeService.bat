ECHO off & cls

SETLOCAL

SET LoopCounter=0
SET LoopCounterLimit=30

GOTO Start

:Usage
ECHO.
ECHO DeployService:
ECHO   Deploys a Native Windows service
ECHO.
ECHO Usage:
ECHO   DeployService SourceFolder TargetFolder ServiceExe ConfigEnv 
ECHO     TargetServer ServiceUser ServicePass ServiceName 
ECHO
ECHO Arguments:
ECHO   SourceFolder  UNC of service drop folder, usually under 
ECHO                 _PublishedApplications.
ECHO                 Ex: _PublishedApplications\eSolServices.Client.ESP.EOUTWatcher
ECHO   TargetFolder  Path to where the service will be installed.
ECHO                 Ex: C:\FAME\eSolServices.Client.ESP.EOUTWatcher
ECHO   ServiceExe    File name of servcice executable
ECHO                 Ex: FAME.eSolServices.Client.ESP.EOUTWatcher.exe
ECHO   ConfigEnv     Environment ID of config files to use
ECHO                 Ex: Dev, QA, Demo, Prod, etc.
ECHO                 Default: none
ECHO   TargetServer  Name of the target server where the service will
ECHO                 be installed.
ECHO                 Default: none - local server
ECHO   ServiceUser   Domain User Name used for authentication to the target server
ECHO                
ECHO   ServicePass   Password for ServiceUser Domain User Name used for authentication to the target server
ECHO
ECHO   ServiceName   Name used in the service registry
ECHO.
ECHO   Log         Optional: Y/N
ECHO                   Y=Send output to log file
ECHO                   N=Send output to console
ECHO                   Default: N                 
GOTO End


:waitWhileServiceIsStarted
	for /F "tokens=3 delims=: " %%M in ('sc \\%TargetServer% query "%ServiceName%" ^| findstr "        STATE"') do (
		if /I "%%M" == "START_PENDING" (
			IF %LoopCounter% LSS %LoopCounterLimit% (
				goto :waitWhileServiceIsStarted
			) ELSE (
				SET LoopCounter=0
			)
		) else (
			if /I "%%M" == "STOPPED" (
				ECHO "Unable to start the service automatically. Check Service configuratoin" %RedirLog%
				SET ExitCode=2
			)
		)
		SET LoopCounter=%LoopCounter%+1;
	)
	IF ERRORLEVEL 1 SET ExitCode=1
	EXIT /B %ExitCode%

:Start
SET SourceFolder=%~1
SET TargetFolder=%~2
SET ServiceExe=%~3
SET ConfigEnv=%~4
SET TargetServer=%~5
SET ServiceUser=%~6
SET ServicePass=%~7
SET ServiceName=%~8
SET LogFile=%~9
SET ExitCode=0

IF "%SourceFolder%"=="?" GOTO Usage
IF "%SourceFolder%"=="/?" GOTO Usage
IF "%SourceFolder%"=="" GOTO Usage
IF "%LogFile%"=="" SET LogFile=N

SET LogFileName=%SourceFolder%\DeployNativeService.log

SET RedirLog=
IF /i "%LogFile%"=="Y" SET RedirLog=^>^> "%LogFileName%"

IF EXIST \\%TargetServer% (
	ECHO Disconnecting previous connection %TargetServer%  %LogFileName%
	net use /delete \\%TargetServer%
)

ECHO Connecting to target server %TargetServer%  %RedirLog%
net use \\%TargetServer% %ServicePass% /user:%ServiceUser%

SET isInstalled="0"

ECHO Cheking If "%ServiceName%" Service is runnning  %RedirLog%

for /F "tokens=3 delims=: " %%H in ('sc \\%TargetServer% query "%ServiceName%" ^| findstr "        STATE"') do (
  if /I "%%H" == "RUNNING" (
   sc \\%TargetServer% stop "%ServiceName%"
   CALL :whileIsStopping
  )
  SET isInstalled="1"
)


IF %isInstalled% == "1" (
	ECHO Service is Installed  %RedirLog%
	
	ECHO Copying Files...  %RedirLog%
	
	XCOPY "%SourceFolder%" "%TargetFolder%" /S /E /Y /I
	
	ECHO Source Files were copied to Target folder  %RedirLog%
	
	ECHO Copying Config Files  %RedirLog%
	
	ECHO XCOPY "%TargetFolder%\Configs\%ConfigEnv%" "%TargetFolder%\Configs" /S /E /Y /I
	
	ECHO Config files were updated  %RedirLog%
	
	ECHO Starting Service...  %RedirLog%
	
	sc \\%TargetServer% start "%ServiceName%"
	
	ECHO Waiting for Service to start...  %RedirLog%
	
	CALL :waitWhileServiceIsStarted
	
	ECHO "%ServiceName%" was started  %RedirLog%
	)
IF %isInstalled% == "0" (
	ECHO Service is Not Installed  %RedirLog%
	
	ECHO Copying Files...  %RedirLog%
	
	XCOPY "%SourceFolder%" "%TargetFolder%" /S /E /Y /I

	ECHO Source Files were copied to Target folder  %RedirLog%

	ECHO Copying Config Files  %RedirLog%
	
	ECHO XCOPY "%TargetFolder%\Configs\%ConfigEnv%" "%TargetFolder%\Configs" /S /E /Y /I

	ECHO Config files were updated  %RedirLog%
	
	ECHO Creating Service "%ServiceName%" ...  %RedirLog%
	
	sc \\%TargetServer% create "%ServiceName%" binpath= "%TargetFolder%\%ServiceExe%"
	
	ECHO Starting Service...  %RedirLog%
	
	sc \\%TargetServer% start "%ServiceName%"
	
	ECHO Waiting for Service to start...  %RedirLog%
	
	CALL :waitWhileServiceIsStarted
	
	ECHO "%ServiceName%" was started  %RedirLog%
	)
IF %ExitCode%==1 (
		ECHO Deployment of Service "%ServiceName%" has completed wth errors. %RedirLog%
	) ELSE (
		IF %ExitCode%==2 (
			SET ExitCode=0
			ECHO Deployment of Service "%ServiceName%" has completed wth warnings. %RedirLog%
		) ELSE (
			ECHO Deployment of Service "%ServiceName%" has completed succesfully. %RedirLog%
		)
	)
	
CALL :END

:End
	IF EXIST \\%TargetServerName% (
		ECHO Disconnecting previous connection %TargetServerName%  %LogFileName%
		net use /delete \\%TargetServerName%
	)

	IF ERRORLEVEL 1 (
		SET ExitCode=1
	) 
	EXIT /B %ExitCode%

:whileIsStopping 
   for /F "tokens=3 delims=: " %%M in ('sc \\%TargetServer% query "%ServiceName%" ^| findstr "        STATE"') do (
		if /I "%%M" == "STOP_PENDING" (
			IF %LoopCounter% LSS %LoopCounterLimit% (
				goto :whileIsStopping
			) ELSE (
				SET LoopCounter=0
			)
		)
		SET LoopCounter=%LoopCounter%+1;
   )
	IF ERRORLEVEL 1 SET ExitCode=1
	EXIT /B %ExitCode%
