@ECHO OFF & cls

SETLOCAL
GOTO Start

:Usage
ECHO.
ECHO DeployAdvantage:
ECHO   This deployment script will deploy an advantage release package to a tartget server.
ECHO   The deployment steps are:
ECHO		Deployment of the Host Site
ECHO		--It relaplace the enviroment config for the Host Web Site		
ECHO.
ECHO		Deployment of the Advantage Site
ECHO		--It relaplace the enviroment config for the Advantage Web Site
ECHO.
ECHO		Deployment of the Services Site
ECHO		--It relaplace the enviroment config for the Services Web Site
ECHO.
ECHO		Deployment of the WapiServices Site
ECHO		--It relaplace the enviroment config for the WapiServices Web Site
ECHO.
ECHO		Deployment of the API
ECHO		--It relaplace the enviroment config for the API		
ECHO.
ECHO Usage:
ECHO   DropFolder DeployFolder Enviroment Branch SqlServer TargetDb ReportServer ReportFolder LogFile
ECHO.
ECHO Arguments:
ECHO   DropFolder     
ECHO		The drop folder is the location path where all the necessary files for a deployment are located. 
ECHO		When a deployment package (or artifact) is downloaded from teamcity you will receive a Zip file 
ECHO		containing everything that is needed to deploy that release.
ECHO		The drop folder location is the path where that zip file was unzip.
ECHO.
ECHO   DeployFolder	
ECHO		The Location path of the server where you want to install the deployment package. 
ECHO		Ex: \\SERVERNAME\inetpub\wwwroot\Advantage\Production\3.10.0.70\
ECHO.
ECHO   Enviroment
ECHO		The enviroment name that represents the configuration path or settings to use.
ECHO		Ex: QA, Prod, Sales, Test
ECHO		Given you pass the QA as enviroment then during deployment a folder at the DeployFolder will
ECHO		be created with the name QA appended at the end.
ECHO.
ECHO		Given you pass the QA as enviroment then each application that is deployed, the QA folder with configs will be used. 
ECHO		For example Host\App_Data\Configs\QA\ the configs inside there will be placed into Host\App_Data\Configs\
ECHO.
ECHO   Branch
ECHO		The branch that is been deployed, branch will be used on the creation of the target folder, similar to Enviroment. 
ECHO		Ex: DeployFolder is \\SERVER1\inetpub\wwwroot\Advantage Enviroment is QA and branch is Master 
ECHO 		the when deployment there will be a folder created as:
ECHO			\\SERVER1\inetpub\wwwroot\Advantage\QA\Master
ECHO.
ECHO   SqlServer 
ECHO		The SQL Server instance name where the databases changes will be deployed. 
ECHO		The user running this batch file must have SA permissions on that sql server.
ECHO.
ECHO   TargetDb				
ECHO		A comma delimeted list of databases to deploy the changes. The  content must be enclosed within doubles quotes and no spaces.
ECHO		Ex: "DB1,DB2,DB3,DB4"
ECHO.
ECHO   ReportServer
ECHO		The report server URL where the reports will be deployed.
ECHO		Ex: http://reportserverurl/Report_Server
ECHO.
ECHO   ReportFolder
ECHO		The target folder to deploy the reports.
ECHO		Ex: Advantage\
ECHO.
ECHO   WebServerName 
ECHO        The Web Server Name is used as the target server to deploy the WapiWindowsService.
ECHO.
ECHO   Consolidated Script folder:
ECHO        The folder name (version folder name) of the consolidated script folder that will be used to run all the consolidated scripts that are inside of PublishedDatabases\DatabaseName\Scripts\
ECHO        Example: "3.10"
ECHO.
ECHO   LogFile (Optional: Y/N)
ECHO        Y=Send output to log file
ECHO        N=Send output to console
GOTO End


:Start
SET DropFolder=%~1
SET DeployFolder=%~2
SET Enviroment=%~3
SET Branch=%~4
SET SqlServer=%~5
SET TargetDb=%~6
SET ReportServer=%~7
SET ReportFolder=%~8
SET WebServerName=%~9
SHIFT
SET ConsolidatedScriptFolder=%~9
SHIFT
SET LogFile=%~9

IF "%DropFolder%"=="?" GOTO :Usage
IF "%DeployFolder%"=="" GOTO :BadDeployFolder
IF "%Enviroment%"=="" GOTO :BadEnviroment
IF "%Branch%"=="" GOTO :BadBranch
IF "%SqlServer%"=="" GOTO :BadSqlServer
IF "%TargetDb%"=="" GOTO :BadTargetDb
IF "%ReportServer%"=="" GOTO :BadReportServer
IF "%ReportFolder%"=="" GOTO :BadReportFolder
IF "%ConsolidatedScriptFolder%"=="" GOTO :BadReportFolder
IF "%LogFile%"=="" SET LogFile=N

SET ExitCode=0

IF NOT "%DropFolder:~-1%"=="\" SET DropFolder=%DropFolder%\
IF NOT "%DeployFolder:~-1%"=="\" SET DeployFolder=%DeployFolder%\

ECHO Deploying Advantage Host
CALL :DeployWebsite "Host" "Host"

ECHO Deploying Advantage Services
CALL :DeployWebsite "Services" "Services"

ECHO Deploying Advantage Site
CALL :DeployWebsite "Site" "Site"

ECHO Deploying Advantage WapiServices
CALL :DeployWebsite "WapiServices" "WapiServices"

ECHO Deploying Advantage API
CALL :DeployAPI "API" "API"

ECHO Deploying Advantage WapiWindowsService
ECHO CALL :DeployWindowsService "WapiWindowsService" "WapiWindowsService" "WapiWindowsService.exe" "%WebServerName%" "WapiWindowsService"
CALL :DeployWindowsService "WapiWindowsService" "WapiWindowsService" "WapiWindowsService.exe" "%WebServerName%" "Advantage Wapi Windows Service"

ECHO Deploying Advantage Database Schema to Target Databases
CALL :DeployDatabases Advantage "%TargetDb%"

ECHO Deploying Advantage Reports
CALL :DeployReports "%ReportFolder%"

ECHO Deployment Completed.
ECHO.

EXIT /B %ExitCode%

:DeployWebsite
	CALL "%DropFolder%_PublishedWebsites\_Deployment\DeployWebsite.bat" "%DropFolder%_PublishedWebsites\%~1" "%DeployFolder%%~2" "%Enviroment%" %LogFile%
	IF ERRORLEVEL 1 SET ExitCode=1
EXIT /B %ExitCode%

:DeployAPI
	REM DeployWebsite DropWebsite TargetWebsite [WebConfigMaster] [LogFile]
	ECHO CALL "%DropFolder%_PublishedWebsites\_Deployment\DeployAPI.bat" "%DropFolder%_PublishedWebsites\%~1" "%DeployFolder%%~2" "%WebServerName%" "Internal\TFSBuild" "TFS.Build" "%Enviroment%" %LogFile%
	CALL "%DropFolder%_PublishedWebsites\_Deployment\DeployAPI.bat" "%DropFolder%_PublishedWebsites\%~1" "%DeployFolder%%~2" "%WebServerName%" "Internal\TFSBuild" "TFS.Build" "%Enviroment%" %LogFile%
	IF ERRORLEVEL 1 SET ExitCode=1
EXIT /B %ExitCode%

REM :DeployDatabases SourceFolder TargetDatabases
:DeployDatabases
	SET Databases=%~2
	:NextDatabase
		FOR /f "tokens=1* delims=," %%a in ("%Databases%") DO (
			CALL :DeployDatabase "%DropFolder%_PublishedDatabases\%~1" %%a
			IF ERRORLEVEL 1 EXIT /B 1
			SET Databases=%%b
		)
IF NOT "%Databases%"=="" goto :NextDatabase
EXIT /B 0

REM :DeployDatabase SourceFolder TargetDatabase
:DeployDatabase
	REM DeployDatabaseRemote SourceFolder TargetServer TargetDatabase [SyncSchema] [SyncData] [LogFile] [SqlCmpOptions] [SqlUser] [SqlPass] [SqlCompareExe] [SqlDataCmpExe] [RedGateServer]
	ECHO CALL "%DropFolder%_PublishedDatabases\_Deployment\DeployDatabase.bat" "%~1\Source" "%SqlServer%" "%~2" Y Y %LogFile%
	CALL "%DropFolder%_PublishedDatabases\_Deployment\DeployDatabase.bat" "%~1\Source" "%SqlServer%" "%~2" Y Y %LogFile%
	
	IF ERRORLEVEL 1 (
		SET ExitCode=1
	) ELSE (
		REM TargetServer TargetDatabase [SourceFolder] [SourceFileSpec] [LogFile] [Timeout] [SqlCmdExe]
		ECHO CALL "%DropFolder%_PublishedDatabases\_Deployment\DeployScriptsRemote.bat" "%SqlServer%" "%~2" "%~1\Scripts\%ConsolidatedScriptFolder%\Advantage" "*_consolidated.sql" %LogFile%
		CALL "%DropFolder%_PublishedDatabases\_Deployment\DeployScriptsRemote.bat" "%SqlServer%" "%~2" "%~1\Scripts\%ConsolidatedScriptFolder%\Advantage" "*_consolidated.sql" %LogFile%
		
		IF ERRORLEVEL 1 SET ExitCode=1
	)
EXIT /B %ExitCode%

REM :DeployWindowsService ApplicationFolderName FolderToDeployService ServiceExeName TargetServerName ServiceName
:DeployWindowsService
	SET ServiceExecutableName=%~3
	SET ServerName=%~4
	SET ServiceName=%~5
	
	ECHO CALL "%DropFolder%_PublishedApplications\_Deployment\DeployNativeService.bat" "%DropFolder%_PublishedApplications\%~1" "%DeployFolder%%~2" "%ServiceExecutableName%" "%Enviroment%" "%ServerName%" "Internal\TFSBuild" "TFS.Build" "%ServiceName%" %LogFile%
	
	CALL "%DropFolder%_PublishedApplications\_Deployment\DeployNativeService.bat" "%DropFolder%_PublishedApplications\%~1" "%DeployFolder%%~2" "%ServiceExecutableName%" "%Enviroment%" "%ServerName%" "Internal\TFSBuild" "TFS.Build" "%ServiceName%" %LogFile%
	
	IF ERRORLEVEL 1 SET ExitCode=1
EXIT /B %ExitCode%

REM :DeployReports TargetFolder
:DeployReports
	REM DeployReports TargetServer TargetFolder [SourceFolder] [SourceFileSpec] [LogFile] [User] [Password] [Timeout] [DeployScript] [RsExe]
	ECHO CALL "%DropFolder%_PublishedReports\DeployReports.bat" %ReportServer% "%~1" "%DropFolder%_PublishedReports\" "*.rdl" %LogFile% "Internal\TFSBuild" "*********"
	CALL "%DropFolder%_PublishedReports\DeployReports.bat" %ReportServer% "%~1" "%DropFolder%_PublishedReports" "*.rdl" %LogFile% "Internal\TFSBuild" "TFS.Build"
	IF ERRORLEVEL 1 SET ExitCode=1
EXIT /B %ExitCode%

:BadDeployFolder
	SET ExitCode=1
	ECHO Must specify Deploy Folder agument
GOTO :End

:BadEnviroment
	SET ExitCode=1
	ECHO Must specify Enviroment agument
GOTO :End

:BadBranch
	SET ExitCode=1
	ECHO Must specify branch name agument
GOTO :End

:BadSqlServer
	SET ExitCode=1
	ECHO Must specify SQL Server agument
GOTO :End

:BadTargetDb
	SET ExitCode=1
	ECHO Must specify Target Database agument
GOTO :End

:BadReportServer
	SET ExitCode=1
	ECHO Must specify Report Server agument
GOTO :End

:BadReportFolder
	SET ExitCode=1
	ECHO Must specify Report Folder agument
GOTO :End
:BadConsolidatedSriptFolder
	SET ExitCode=1
	ECHO Must specify Consolidated Script Folder agument
GOTO :End


:End
EXIT /B %ExitCode%