@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
SETLOCAL
GOTO Start

:Usage
ECHO.
ECHO DeployWebsite:
ECHO   Deploys a drop build website to a target share
ECHO.
ECHO Usage:
ECHO   DeployWebsite DropWebsite TargetWebsite [WebConfigMaster] [LogFile]
ECHO.
ECHO Arguments:
ECHO   DropWebsite     Optional: Path to the _PublishedWebSites folder
ECHO                   Ex: 
ECHO                   C:\Builds\eSolutions\Phase3\1.3.1104.4\
ECHO                   _PublishedWebSites\FaaWeb
ECHO                   Default: current directory
ECHO                   Note: Typically DeployWebsite.bat would be in the 
ECHO                   _PublishedWebsites folder, in which case you only need
ECHO                   to specify subfolder (i.e. FaaWeb)
ECHO   TargetWebsite   UNC of the target website
ECHO                   Ex: \\qafa\inetpub\wwwroot\eSolutions\Dev\Phase3
ECHO   WebConfigMaster Optional: Name of the web config master template to be 
ECHO                   renamed
ECHO                   Default: web.config.dev
ECHO   LogFile         Optional: Y/N
ECHO                   Y=Send output to log file
ECHO                   N=Send output to console
ECHO                   Default: N
GOTO End

:Start
SET DropWebSite=%~1
SET TargetWebsite=%~2
SET WebConfigMaster=%~3
SET LogFile=%~4

IF "%DropWebSite%"=="?" GOTO Usage
IF "%DropWebSite%"=="/?" GOTO Usage

IF "%DropWebSite%"=="" SET DropWebSite=%~dp0
IF "%TargetWebsite%"=="" GOTO Usage
IF "%WebConfigMaster%"=="" SET WebConfigMaster=web.config.dev
IF "%LogFile%"=="" SET LogFile=N

SET OutputFolder=%~dp0
SET LogFileName=%OutputFolder%%~n0.log

IF NOT "%DropWebSite:~-1%"=="\" SET DropWebSite=%DropWebSite%\
IF NOT "%TargetWebsite:~-1%"=="\" SET TargetWebsite=%TargetWebsite%\

SET ConfigEnv=%WebConfigMaster:web.config.=%

SET DelWeb=IF EXIST "%TargetWebsite%" DEL "%TargetWebsite%*.*" /f/s/q
SET PubWeb=XCOPY "%DropWebSite%*.*" "%TargetWebsite%" /q/i/s/e/k/r/h/y

REM Default to using old web.config.env files
SET ConWeb1=ATTRIB -r "%TargetWebsite%web.config"
SET ConWeb2=COPY /y "%TargetWebsite%%WebConfigMaster%" "%TargetWebsite%web.config"
SET ConWeb3=

REM If App_Data\Configs folder exists, use new external config files instead
IF NOT EXIST "%DropWebSite%App_Data\Configs" GOTO SkipConfigDir
SET ConWeb1=ATTRIB -r "%TargetWebsite%App_Data\Configs\*.*"
SET ConWeb2=COPY /y "%TargetWebsite%App_Data\Configs\%ConfigEnv%\*.*" "%TargetWebsite%App_Data\Configs"
SET ConWeb3=IF EXIST "%TargetWebsite%App_Data\Configs\web.config" MOVE /y "%TargetWebsite%App_Data\Configs\web.config" "%TargetWebsite%web.config"
:SkipConfigDir

IF /i NOT %Debug%==ON GOTO SkipDebug
ECHO.
ECHO DropWebSite      %DropWebSite%
ECHO TargetWebsite    %TargetWebsite%
ECHO WebConfigMaster  %WebConfigMaster%
ECHO LogFile          %LogFile%
ECHO.
ECHO LogFileName      %LogFileName%
ECHO.
ECHO DelWeb:
ECHO %DelWeb%
ECHO.
ECHO PubWeb:
ECHO %PubWeb%
ECHO.
ECHO ConWeb:
ECHO %ConWeb1%
ECHO %ConWeb2%
IF NOT "%ConWeb3%"=="" ECHO %ConWeb3%
ECHO.
PAUSE
:SkipDebug

SET RedirLog=
IF /i "%LogFile%"=="Y" SET RedirLog=^>^> "%LogFileName%"

ECHO Publishing Website %DropWebSite% To %TargetWebsite% %RedirLog%
ECHO Deleting contents of target folder... %RedirLog%
%DelWeb% > NUL
ECHO Copying drop files to target folder... %RedirLog%
%PubWeb% %RedirLog%
ECHO Setting up web.config for environment %RedirLog%
%ConWeb1% %RedirLog%
%ConWeb2% %RedirLog%
IF "%ConWeb3%"=="" GOTO End
%ConWeb3% %RedirLog%
ECHO. %RedirLog%

:End
ENDLOCAL
