@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
GOTO Start

:Usage
ECHO.
ECHO DeployDrop:
ECHO   Deploys a drop build produced using FameDefaultTemplate
ECHO.
ECHO Usage:
ECHO   DeployDrop
ECHO.
ECHO   Note: you must set the following enviroment variables before calling
ECHO   DeployDrop. This is typically done by creating a batch file that 
ECHO   contains SET commands to set the values for the variables below and  
ECHO   then calls DeployDrop.
ECHO.
ECHO Variables:
ECHO   DropWebsite     Optional: Website folder under _PublishedWebSites to be
ECHO                   published (set DropWebsite2, DropWebsite3 for 
ECHO                   additional sites)
ECHO                   Ex: FaaWeb
ECHO   TargetWebsite   Optional: UNC of the target website (set TargetWebsite2,
ECHO                   TargetWebsite3 for additional sites)
ECHO                   Ex: \\qafa\inetpub\wwwroot\eSolutions\QA\Main
ECHO   WebConfigMaster Optional: Name of the web config master template to be 
ECHO                   renamed
ECHO                   Default: web.config.dev
ECHO   DropPreScripts  Optional: SQL scripts folder under _PublishedScripts
ECHO                   (set DropPreScripts2, DropPreScripts3 for additional 
ECHO                   folders)
ECHO   DropSql         Optional: SQL source control scripts folder under
ECHO                   _PublishedDatabases
ECHO                   (set DropSql2, DropSql3 for additional folders)
ECHO                   Ex: Main
ECHO   TargetSqlServer Optional: Name of target SQL Server
ECHO                   (set TargetSqlServer2, TargetSqlServer3 for additional 
ECHO                   servers)
ECHO                   Ex: Dev2
ECHO   TargetDatabase  Optional: Name of target database
ECHO                   (set TargetDatabase2, TargetDatabase3 for additional 
ECHO                   Databases)
ECHO                   Ex: Main
ECHO   RedGateServer   Optional: Server where RedGate SQL Compare Pro is
ECHO                   installed. If specified, DeployDatabase.bat will be 
ECHO                   launced remotely on that server using PsExe.exe.
ECHO                   Default: none (use local machine)
ECHO   DropScripts     Optional: SQL scripts folder under _PublishedScripts
ECHO                   (set DropScripts2, DropScripts3 for additional folders)
ECHO                   Ex: Main
ECHO   ScriptsFileSpec Optional: File spec of source scripts to deploy
ECHO                   (set ScriptsFileSpec2, ScriptsFileSpec3 for additional 
ECHO                   folders)
ECHO                   Default: *.sql
ECHO   DropSrs         Optional: Reports folder under _PublishedReports
ECHO                   Ex: Main
ECHO   TargetSrsServer Optional: URL of target SRS server
ECHO                   Ex: http://qafa:8080/ReportServer
ECHO   TargetSrsFolder Optional: Folder on target SRS server
ECHO                   Ex: Main
ECHO   TargetSrsBin    Optional: Bin folder on target SRS server
ECHO                   Ex: \\qafa\Reporting Services\ReportServer\bin
ECHO   DropBuildFolder Optional: Build drop folder
ECHO                   Ex: C:\MyBuilds\eSolutions\Main\1.3.1104.4
ECHO                   Default: current directory
ECHO   LogFile         Optional: Y/N
ECHO                   Y=Send output to log file
ECHO                   N=Send output to console
ECHO                   Default: N
GOTO End

:Start
IF "%DropWebSite%"=="?" GOTO Usage
IF "%DropWebSite%"=="/?" GOTO Usage

REM IF "%DropWebSite%"=="" GOTO Usage
REM IF "%TargetWebsite%"=="" GOTO Usage
IF "%WebConfigMaster%"=="" SET WebConfigMaster=web.config.dev
REM IF "%DropSql%"=="" GOTO Usage
REM IF "%TargetSqlServer%"=="" GOTO Usage
REM IF "%TargetDatabase%"=="" GOTO Usage
REM IF "%RedGateServer%"=="" GOTO Usage
REM IF "%DropScripts%"=="" GOTO Usage
REM IF "%ScriptsFileSpec%"=="" GOTO Usage
REM IF "%DropSrs%"=="" GOTO Usage
REM IF "%TargetSrsServer%"=="" GOTO Usage
REM IF "%TargetSrsFolder%"=="" GOTO Usage
REM IF "%TargetSrsBin%"=="" GOTO Usage

IF "%DropBuildFolder%"=="" SET DropBuildFolder=%~dp0
IF "%LogFile%"=="" SET LogFile=N

SET OutputFolder=%~dp0
SET LogFileName=%OutputFolder%%~n0.log

SET PubWeb=ECHO     Nothing to publish
SET PubWeb2=ECHO     Nothing to publish
SET PubWeb3=ECHO     Nothing to publish
SET PubWeb4=ECHO     Nothing to publish
SET PubRpt=ECHO     Nothing to publish
SET PubPre=ECHO     Nothing to publish
SET PubPre2=ECHO     Nothing to publish
SET PubPre3=ECHO     Nothing to publish
SET PubSql=ECHO     Nothing to publish
SET PubSql2=ECHO     Nothing to publish
SET PubSql3=ECHO     Nothing to publish
SET PubScr=ECHO     Nothing to publish
SET PubScr2=ECHO     Nothing to publish
SET PubScr3=ECHO     Nothing to publish
SET PubBin=ECHO     Nothing to publish

IF "%DropWebSite%"=="" GOTO SkipWebsite
IF "%TargetWebsite%"=="" GOTO SkipWebsite
SET PubWeb=CALL "%DropBuildFolder%_PublishedWebsites\DeployWebsite.bat" "%DropBuildFolder%_PublishedWebsites\%DropWebSite%" "%TargetWebsite%" %WebConfigMaster% %LogFile%
:SkipWebsite

IF "%DropWebSite2%"=="" GOTO SkipWebsite2
IF "%TargetWebsite2%"=="" GOTO SkipWebsite2
SET PubWeb2=CALL "%DropBuildFolder%_PublishedWebsites\DeployWebsite.bat" "%DropBuildFolder%_PublishedWebsites\%DropWebSite2%" "%TargetWebsite2%" %WebConfigMaster% %LogFile%
:SkipWebsite2

IF "%DropWebSite3%"=="" GOTO SkipWebsite3
IF "%TargetWebsite3%"=="" GOTO SkipWebsite3
SET PubWeb3=CALL "%DropBuildFolder%_PublishedWebsites\DeployWebsite.bat" "%DropBuildFolder%_PublishedWebsites\%DropWebSite3%" "%TargetWebsite3%" %WebConfigMaster% %LogFile%
:SkipWebsite3

IF "%DropWebSite4%"=="" GOTO SkipWebsite4
IF "%TargetWebsite4%"=="" GOTO SkipWebsite4
SET PubWeb4=CALL "%DropBuildFolder%_PublishedWebsites\DeployWebsite.bat" "%DropBuildFolder%_PublishedWebsites\%DropWebSite4%" "%TargetWebsite4%" %WebConfigMaster% %LogFile%
:SkipWebsite4

IF "%DropSrs%"=="" GOTO SkipSrs
IF "%TargetSrsServer%"=="" GOTO SkipSrs
IF "%TargetSrsFolder%"=="" GOTO SkipSrs
SET PubRpt=CALL "%DropBuildFolder%_PublishedReports\%DropSrs%\DeployReports.bat" %TargetSrsServer% "%TargetSrsFolder%" "%DropBuildFolder%_PublishedReports\%DropSrs%\Sources" "" %LogFile% %SrsVersion%
:SkipSrs

IF "%TargetSrsBin%"=="" GOTO SkipBin
SET PubBin=XCOPY "%DropBuildFolder%_PublishedReports\Bin\*.*" "%TargetSrsBin%" /q/i/k/r/h/y
:SkipBin

IF "%DropPreScripts%"=="" GOTO SkipPreScripts
IF "%TargetSqlServer%"=="" GOTO SkipPreScripts
IF "%TargetDatabase%"=="" GOTO SkipPreScripts
IF "%ScriptsFileSpec%"=="" SET ScriptsFileSpec=*.sql
SET PubPre=CALL "%DropBuildFolder%_PublishedScripts\%DropPreScripts%\DeployScripts.bat" "%TargetSqlServer%" "%TargetDatabase%" "%DropBuildFolder%_PublishedScripts\%DropPreScripts%\Sources" "%ScriptsFileSpec%" %LogFile%
:SkipPreScripts

IF "%DropPreScripts2%"=="" GOTO SkipPreScripts2
IF "%TargetSqlServer2%"=="" GOTO SkipPreScripts2
IF "%TargetDatabase2%"=="" GOTO SkipPreScripts2
IF "%ScriptsFileSpec2%"=="" SET ScriptsFileSpec2=*.sql
SET PubPre2=CALL "%DropBuildFolder%_PublishedScripts\%DropPreScripts2%\DeployScripts.bat" "%TargetSqlServer2%" "%TargetDatabase2%" "%DropBuildFolder%_PublishedScripts\%DropPreScripts2%\Sources" "%ScriptsFileSpec2%" %LogFile%
:SkipPreScripts2

IF "%DropPreScripts3%"=="" GOTO SkipPreScripts3
IF "%TargetSqlServer3%"=="" GOTO SkipPreScripts3
IF "%TargetDatabase3%"=="" GOTO SkipPreScripts3
IF "%ScriptsFileSpec3%"=="" SET ScriptsFileSpec3=*.sql
SET PubPre3=CALL "%DropBuildFolder%_PublishedScripts\%DropPreScripts3%\DeployScripts.bat" "%TargetSqlServer3%" "%TargetDatabase3%" "%DropBuildFolder%_PublishedScripts\%DropPreScripts3%\Sources" "%ScriptsFileSpec3%" %LogFile%
:SkipPreScripts3

IF "%DropSql%"=="" GOTO SkipSql
IF "%TargetSqlServer%"=="" GOTO SkipSql
IF "%TargetDatabase%"=="" GOTO SkipSql
SET PubSql="CALL %DropBuildFolder%_PublishedDatabases\%DropSql%\DeployDatabaseRemote.bat" "%DropBuildFolder%_PublishedDatabases\%DropSql%\Sources" "%TargetSqlServer%" "%TargetDatabase%" Y Y %LogFile% "" "" %RedGateServer%
rem IF NOT "%RedGateServer%"=="" IF /i "%RedGateServer%"=="%COMPUTERNAME%" SET RedGateServer=
rem IF "%RedGateServer%"=="" (SET PubSql=CALL %PubSql%) ELSE (SET PubSql="%~dp0psexec" \\dev -u "Internal\TFSBuild" -p TFS.Build %PubSql%)
:SkipSql

IF "%DropSql2%"=="" GOTO SkipSql2
IF "%TargetSqlServer2%"=="" GOTO SkipSql2
IF "%TargetDatabase2%"=="" GOTO SkipSql2
SET PubSql2="CALL %DropBuildFolder%_PublishedDatabases\%DropSql2%\DeployDatabaseRemote.bat" "%DropBuildFolder%_PublishedDatabases\%DropSql2%\Sources" "%TargetSqlServer2%" "%TargetDatabase2%" Y Y %LogFile% "" "" %RedGateServer%
rem IF NOT "%RedGateServer%"=="" IF /i "%RedGateServer%"=="%COMPUTERNAME%" SET RedGateServer=
rem IF "%RedGateServer%"=="" (SET PubSql2=CALL %PubSql2%) ELSE (SET PubSql2="%~dp0psexec" \\dev -u "Internal\TFSBuild" -p TFS.Build %PubSql2%)
:SkipSql2

IF "%DropSql3%"=="" GOTO SkipSql3
IF "%TargetSqlServer3%"=="" GOTO SkipSql3
IF "%TargetDatabase3%"=="" GOTO SkipSql3
SET PubSql3="CALL %DropBuildFolder%_PublishedDatabases\%DropSql3%\DeployDatabaseRemote.bat" "%DropBuildFolder%_PublishedDatabases\%DropSql3%\Sources" "%TargetSqlServer3%" "%TargetDatabase3%" Y Y %LogFile% "" "" %RedGateServer%
rem IF NOT "%RedGateServer%"=="" IF /i "%RedGateServer%"=="%COMPUTERNAME%" SET RedGateServer=
rem IF "%RedGateServer%"=="" (SET PubSql3=CALL %PubSql3%) ELSE (SET PubSql3="%~dp0psexec" \\dev -u "Internal\TFSBuild" -p TFS.Build %PubSql3%)
:SkipSql3

IF "%DropScripts%"=="" GOTO SkipScripts
IF "%TargetSqlServer%"=="" GOTO SkipScripts
IF "%TargetDatabase%"=="" GOTO SkipScripts
IF "%ScriptsFileSpec%"=="" SET ScriptsFileSpec=*.sql
SET PubScr=CALL "%DropBuildFolder%_PublishedScripts\%DropScripts%\DeployScripts.bat" "%TargetSqlServer%" "%TargetDatabase%" "%DropBuildFolder%_PublishedScripts\%DropScripts%\Sources" "%ScriptsFileSpec%" %LogFile%
:SkipScripts

IF "%DropScripts2%"=="" GOTO SkipScripts2
IF "%TargetSqlServer2%"=="" GOTO SkipScripts2
IF "%TargetDatabase2%"=="" GOTO SkipScripts2
IF "%ScriptsFileSpec2%"=="" SET ScriptsFileSpec2=*.sql
SET PubScr=CALL "%DropBuildFolder%_PublishedScripts\%DropScripts2%\DeployScripts.bat" "%TargetSqlServer2%" "%TargetDatabase2%" "%DropBuildFolder%_PublishedScripts\%DropScripts2%\Sources" "%ScriptsFileSpec2%" %LogFile%
:SkipScripts2

IF "%DropScripts3%"=="" GOTO SkipScripts3
IF "%TargetSqlServer3%"=="" GOTO SkipScripts3
IF "%TargetDatabase3%"=="" GOTO SkipScripts3
IF "%ScriptsFileSpec3%"=="" SET ScriptsFileSpec3=*.sql
SET PubScr=CALL "%DropBuildFolder%_PublishedScripts\%DropScripts3%\DeployScripts.bat" "%TargetSqlServer3%" "%TargetDatabase3%" "%DropBuildFolder%_PublishedScripts\%DropScripts3%\Sources" "%ScriptsFileSpec3%" %LogFile%
:SkipScripts3

IF /i NOT %Debug%==ON GOTO SkipDebug
ECHO.
ECHO DropWebSite      %DropWebSite%
ECHO DropWebSite2     %DropWebSite2%
ECHO DropWebSite3     %DropWebSite3%
ECHO DropWebSite4     %DropWebSite4%
ECHO TargetWebsite    %TargetWebsite%
ECHO TargetWebsite2   %TargetWebsite2%
ECHO TargetWebsite3   %TargetWebsite3%
ECHO TargetWebsite4   %TargetWebsite4%
ECHO WebConfigMaster  %WebConfigMaster%
ECHO DropPreScripts   %DropPreScripts%
ECHO DropPreScripts2  %DropPreScripts2%
ECHO DropPreScripts3  %DropPreScripts3%
ECHO DropSql          %DropSql%
ECHO DropSql2         %DropSql2%
ECHO DropSql3         %DropSql3%
ECHO TargetSqlServer  %TargetSqlServer%
ECHO TargetSqlServer2 %TargetSqlServer2%
ECHO TargetSqlServer3 %TargetSqlServer3%
ECHO TargetDatabase   %TargetDatabase%
ECHO TargetDatabase2  %TargetDatabase2%
ECHO TargetDatabase3  %TargetDatabase3%
ECHO RedGateServer    %RedGateServer%
ECHO DropScripts      %DropScripts%
ECHO DropScripts2     %DropScripts2%
ECHO DropScripts3     %DropScripts3%
ECHO ScriptsFileSpec  %ScriptsFileSpec%
ECHO ScriptsFileSpec2 %ScriptsFileSpec2%
ECHO ScriptsFileSpec3 %ScriptsFileSpec3%
ECHO DropSrs          %DropSrs%
ECHO TargetSrsServer  %TargetSrsServer%
ECHO TargetSrsFolder  %TargetSrsFolder%
ECHO TargetSrsBin     %TargetSrsBin%
ECHO.
ECHO DropBuildFolder  %DropBuildFolder%
ECHO LogFile          %LogFile%
ECHO.
ECHO LogFileName      %LogFileName%
ECHO.
ECHO PubWeb:
ECHO %PubWeb%
ECHO %PubWeb2%
ECHO %PubWeb3%
ECHO %PubWeb4%
ECHO.
ECHO PubRpt:
ECHO %PubRpt%
ECHO.
ECHO PubBin:
ECHO %PubBin%
ECHO.
ECHO PubPre:
ECHO %PubPre%
ECHO %PubPre2%
ECHO %PubPre3%
ECHO.
ECHO PubSql:
ECHO %PubSql%
ECHO.
ECHO PubScr:
ECHO %PubScr%
ECHO %PubScr2%
ECHO %PubScr3%
PAUSE
:SkipDebug

SET RedirLog=
IF /i "%LogFile%"=="Y" SET RedirLog=^>^> "%LogFileName%"

ECHO. %RedirLog%
ECHO Deploying %DropBuildFolder% %RedirLog%
IF "%LogFile%"=="Y" ECHO Deploying %DropBuildFolder%
ECHO. %RedirLog%
IF "%LogFile%"=="Y" ECHO.
ECHO Publishing Website %RedirLog%
IF "%LogFile%"=="Y" ECHO Publishing Website
%PubWeb% %RedirLog%
ECHO Publishing Website2 %RedirLog%
IF "%LogFile%"=="Y" ECHO Publishing Website2
%PubWeb2% %RedirLog%
ECHO Publishing Website3 %RedirLog%
IF "%LogFile%"=="Y" ECHO Publishing Website3
%PubWeb3% %RedirLog%
ECHO Publishing Website4 %RedirLog%
IF "%LogFile%"=="Y" ECHO Publishing Website4
%PubWeb4% %RedirLog%
ECHO Publishing SRS Reports %RedirLog%
IF "%LogFile%"=="Y" ECHO Publishing SRS Reports
%PubRpt% %RedirLog%
ECHO Publishing SRS Bin %RedirLog%
IF "%LogFile%"=="Y" ECHO Publishing SRS Bin
%PubBin% %RedirLog%
ECHO Publishing Pre-Scripts %RedirLog%
IF "%LogFile%"=="Y" ECHO Publishing Pre-Scripts
%PubPre% %RedirLog%
ECHO Publishing Pre-Scripts2 %RedirLog%
IF "%LogFile%"=="Y" ECHO Publishing Pre-Scripts2
%PubPre2% %RedirLog%
ECHO Publishing Pre-Scripts3 %RedirLog%
IF "%LogFile%"=="Y" ECHO Publishing Pre-Scripts3
%PubPre3% %RedirLog%
ECHO Publishing Database %RedirLog%
IF "%LogFile%"=="Y" ECHO Publishing Database
%PubSql% %RedirLog%
ECHO Publishing Database2 %RedirLog%
IF "%LogFile%"=="Y" ECHO Publishing Database2
%PubSql2% %RedirLog%
ECHO Publishing Database3 %RedirLog%
IF "%LogFile%"=="Y" ECHO Publishing Database3
%PubSql3% %RedirLog%
ECHO Publishing Scripts %RedirLog%
IF "%LogFile%"=="Y" ECHO Publishing Scripts
%PubScr% %RedirLog%
ECHO Publishing Scripts2 %RedirLog%
IF "%LogFile%"=="Y" ECHO Publishing Scripts2
%PubScr2% %RedirLog%
ECHO Publishing Scripts3 %RedirLog%
IF "%LogFile%"=="Y" ECHO Publishing Scripts3
%PubScr3% %RedirLog%


:End