@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
GOTO Start

:Usage
ECHO.
ECHO DeployDropEnv:
ECHO   Deploys a drop build produced using FameDefaultTemplate to one of the 
ECHO   standard environments by preparing the necessary variables and calling 
ECHO   DeployDrop.bat.  DeployDropEnv.bat is expected to be in the same folder
ECHO   as DeployDrop.bat.
ECHO.
ECHO Usage:
ECHO   DeployDropEnv Product Branch [Environment] [LogFile] [TargetBranch] 
ECHO     [DropBuildFolder]
ECHO.
ECHO Variables:
ECHO   Product         Product name
ECHO                   Ex: eSolutions
ECHO   Branch          Branch name
ECHO                   Ex: Phase3
ECHO   Environment     Optional: Environment name (Dev, QA, Demo, etc.)
ECHO                   Default: QA
ECHO   LogFile         Optional: Y/N
ECHO                   Y=Send output to log file
ECHO                   N=Send output to console
ECHO                   Default: N
ECHO   TargetBranch    Optional: used to deploy to a target other then Branch.
ECHO                   For example, to deploy the 3.1 source branch to the 
ECHO                   Release target environement.
ECHO                   Default: Branch
ECHO   DropBuildFolder Optional: Build drop folder. 
ECHO                   Ex: C:\Builds\eSolutions\Phase3\1.3.1104.4
ECHO                   Default: current directory
GOTO End

:Start
SET Product=%~1
SET Branch=%~2
SET Environment=%~3
SET SubEnv=
SET LogFile=%~4
SET TargetBranch=%~5
SET DropBuildFolder=%~6


IF "%Product%"=="?" GOTO Usage
IF "%Product%"=="/?" GOTO Usage
IF "%Product%"=="" GOTO Usage
IF "%Branch%"=="" GOTO Usage

IF "%Environment%"=="" SET Environment=QA
IF /i "%Environment:~0,4%"=="Beta" SET Environment=Beta
IF /i "%Environment:~0,3%"=="SB1" SET Environment=SB1
IF /i "%Environment:~0,3%"=="Dev" SET Environment=Dev
IF /i "%Environment:~0,4%"=="Demo" SET Environment=Demo
IF /i "%Environment:~0,5%"=="Train" SET Environment=Train
IF /i "%Environment:~0,5%"=="Dbg" SET Environment=Dbg
IF /i "%Environment:~0,5%"=="Debug" SET Environment=Dbg
IF /i "%Environment:~0,2%"=="QA" SET SubEnv=%Environment:~2%
IF /i "%Environment:~0,2%"=="QA" SET Environment=QA

IF "%LogFile%"=="" SET LogFile=N
IF "%TargetBranch%"=="" SET TargetBranch=%Branch%
IF "%DropBuildFolder%"=="" SET DropBuildFolder=%~dp0

SET LogFileName=%~dp0DeployDrop.log

IF /i "%Product%"=="Advantage" GOTO AdvSettings

rem ******************** eSolutions Settings ********************

SET Product=AFA

:EsoSettings
SET DropWebSite=FaaWeb
SET TargetWebRoot=%Product%
SET DropWebSite2=FAMEServices
SET TargetWebRoot2=FAMEServices
SET DropWebSite3=
SET TargetWebRoot3=
SET DropPreScripts=PrePubDB
SET DropPreScripts2=
SET DropPreScripts3=
SET DropSql=%Branch%
SET DropSql2=
SET DropSql3=
SET DropScripts=PostPubDB
SET DropScripts2=
SET DropScripts3=
SET ScriptsFileSpec=*.sql
SET RedGateServer=Dev
SET TargetSrsBin=
SET TargetDatabase=%Product:~0,3%%Environment%%TargetBranch%%SubEnv%


IF /i "%Environment%"=="Beta" GOTO EsoSettingsBeta
IF /i "%Environment%"=="SB1" GOTO EsoSettingsSB1
IF /i "%Environment%"=="Dev" GOTO EsoSettingsDev
IF /i "%Environment%"=="QA" GOTO EsoSettingsQA
IF /i "%Environment%"=="Demo" GOTO EsoSettingsDemo
IF /i "%Environment%"=="Train" GOTO EsoSettingsTrain
IF /i "%Environment%"=="Dbg" GOTO EsoSettingsTrain
GOTO Usage

:EsoSettingsBeta
SET SqlServer=esolBetaDB01
SET WebServer=cs1-esbeta-ws2
SET SrsServer=cs1-esbeta-ws2:8080
SET SrsInstance=
SET TargetSrsBin=\\%WebServer%\Reporting Services\ReportServer\bin
GOTO CommonSettings

:EsoSettingsSB1
SET SqlServer=ITTEST100
SET WebServer=IT-Test200
SET SrsServer=IT-Test200:8080
SET SrsInstance=
SET TargetSrsBin=\\%WebServer%\Reporting Services\ReportServer\bin
GOTO CommonSettings

:EsoSettingsDev
SET SqlServer=dev2\esolutions
SET WebServer=qafa
SET SrsServer=qafa:8080
SET SrsInstance=
SET TargetDatabase=%Environment%%TargetBranch%%SubEnv%
GOTO CommonSettings

:EsoSettingsQA
SET SqlServer=QAesolDB01
SET WebServer=QAesolWS1
SET SrsServer=QAesolWS1:8080
SET SrsInstance=
SET TargetSrsBin=\\%WebServer%\Reporting Services\ReportServer\bin
SET TargetDatabase=%Environment%%TargetBranch%%SubEnv%
GOTO CommonSettings

:EsoSettingsDemo
SET SqlServer=demo1\sqldev2012
SET WebServer=demo1
SET SrsServer=demo1:8082
SET SrsInstance=
GOTO CommonSettings

:EsoSettingsTrain
SET SqlServer=demo1\sqldev2012
SET WebServer=demo1
SET SrsServer=demo1:8082
SET SrsInstance=
GOTO CommonSettings

:EsoSettingsDbg
SET SqlServer=Dev2\eSolutions
SET WebServer=qafa
SET SrsServer=qafa
SET SrsInstance=
GOTO CommonSettings

rem ******************** Advantage Settings ********************

:AdvSettings
SET DropWebSite=AdvWeb
SET TargetWebRoot=
SET DropWebSite2=
SET TargetWebRoot2=
SET DropWebSite3=
SET TargetWebRoot3=
SET DropPreScripts=Advantage
SET DropPreScripts2=MultiTenantHost
SET DropPreScripts3=
SET DropSql=
SET DropSql2=
SET DropSql3=
SET DropScripts=
SET DropScripts2=
SET DropScripts3=
SET RedGateServer=Dev
SET TargetSrsBin=

IF %Branch% LEQ "3.3" GOTO SetAdvEnv
SET DropWebSite2=FAME.Advantage.MultiTenantHost
SET DropWebSite3=FAME.Advantage.Services
SET TargetWebRoot=Site
SET TargetWebRoot2=Host
SET TargetWebRoot3=Services
SET TargetDatabase2=TenantAuthDB


:SetAdvEnv
IF /i "%Environment%"=="Dev" GOTO AdvSettingsDev
IF /i "%Environment%"=="QA" GOTO AdvSettingsQA
IF /i "%Environment%"=="Demo" GOTO AdvSettingsDemo
IF /i "%Environment%"=="Train" GOTO AdvSettingsTrain
GOTO Usage

:AdvSettingsDev
SET SqlServer=dev2\SQL2008R2
SET WebServer=dev2
SET SrsServer=dev2
SET SrsInstance=
GOTO CommonSettings

:AdvSettingsQA
SET SqlServer=qa2008r2
SET WebServer=VM-QAADVWS1
SET SrsServer=qa2008r2
SET SrsInstance=
GOTO CommonSettings

:AdvSettingsDemo
SET SqlServer=demo1\sql2008r2
SET WebServer=demo1
SET SrsServer=demo1
SET SrsInstance=
GOTO CommonSettings

:AdvSettingsTrain
SET SqlServer=demo1\sql2008r2
SET WebServer=demo1
SET SrsServer=demo1
SET SrsInstance=
GOTO CommonSettings



:CommonSettings
SET TargetWebsite=\\%WebServer%\inetpub\wwwroot\%Product%\%Environment%\%TargetBranch%%SubEnv%\%TargetWebRoot%
IF NOT "%DropWebSite2%=="" IF NOT "%TargetWebRoot2%=="" SET TargetWebsite2=\\%WebServer%\inetpub\wwwroot\%Product%\%Environment%\%TargetBranch%%SubEnv%\%TargetWebRoot2%
IF NOT "%DropWebSite3%=="" IF NOT "%TargetWebRoot3%=="" SET TargetWebsite3=\\%WebServer%\inetpub\wwwroot\%Product%\%Environment%\%TargetBranch%%SubEnv%\%TargetWebRoot3%
SET WebConfigMaster=web.config.%Environment%%SubEnv%
IF NOT "%TargetBranch%"=="%Branch%" SET WebConfigMaster=%WebConfigMaster%.%TargetBranch%
SET DropSrs=%Branch%
SET TargetSrsServer=http://%SrsServer%/ReportServer%SrsInstance%
SET TargetSrsFolder=/%Product%/%Environment%/%TargetBranch%
SET TargetSqlServer=%SqlServer%
SET TargetSqlServer2=%SqlServer%
IF "%TargetDatabase%"=="" SET TargetDatabase=%Environment%%TargetBranch%%SubEnv%

IF /i NOT %Debug%==ON GOTO SkipDebug
ECHO.
ECHO Product          %Product%
ECHO Branch           %Branch%
ECHO Environment      %Environment%
ECHO SubEnv           %SubEnv%
ECHO LogFile          %LogFile%
ECHO TargetBranch     %TargetBranch%
ECHO DropBuildFolder  %DropBuildFolder%
ECHO.
ECHO SqlServer        %SqlServer%
ECHO WebServer        %WebServer%
ECHO SrsServer        %SrsServer%
ECHO.
ECHO DropWebSite      %DropWebSite%
ECHO DropWebSite2     %DropWebSite%
ECHO DropWebSite3     %DropWebSite%
ECHO TargetWebsite    %TargetWebsite%
ECHO TargetWebsite2   %TargetWebsite%
ECHO TargetWebsite3   %TargetWebsite%
ECHO WebConfigMaster  %WebConfigMaster%
ECHO.
ECHO DropSrs          %DropSrs%
ECHO TargetSrsServer  %TargetSrsServer%
ECHO TargetSrsFolder  %TargetSrsFolder%
ECHO TargetSrsBin     %TargetSrsBin%
ECHO.
ECHO DropPreScripts   %DropScripts%
ECHO DropPreScripts2  %DropScripts%
ECHO DropPreScripts3  %DropScripts%
ECHO.
ECHO ScriptsFileSpec  %ScriptsFileSpec%
ECHO ScriptsFileSpec2 %ScriptsFileSpec%
ECHO ScriptsFileSpec3 %ScriptsFileSpec%
ECHO.
ECHO DropSql          %DropSql%
ECHO TargetSqlServer  %TargetSqlServer%
ECHO TargetDatabase   %TargetDatabase%
ECHO DropSql2         %DropSql%
ECHO TargetSqlServer2 %TargetSqlServer%
ECHO TargetDatabase2  %TargetDatabase%
ECHO DropSql3         %DropSql%
ECHO TargetSqlServer3 %TargetSqlServer%
ECHO TargetDatabase3  %TargetDatabase%
ECHO RedGateServer    %RedGateServer%
ECHO.
ECHO DropScripts      %DropScripts%
ECHO DropScripts2     %DropScripts%
ECHO DropScripts3     %DropScripts%
ECHO.
PAUSE
:SkipDebug

SET RedirLog=
IF /i "%LogFile%"=="Y" SET RedirLog=^>^> "%LogFileName%"

ECHO. %RedirLog%
ECHO Deploying %Product% %Branch% to %Environment%%SubEnv% %TargetBranch% environment... %RedirLog%
CALL "%~dp0DeployDrop.bat"

:End

