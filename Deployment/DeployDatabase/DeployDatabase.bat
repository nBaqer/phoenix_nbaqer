@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
SETLOCAL
GOTO Start

:Usage
ECHO.
ECHO DeployDatabase:
ECHO   Deploys a SQL Source Control database to a SQL Server
ECHO.
ECHO Usage:
ECHO   DeployDatabase SourceFolder TargetServer TargetDatabase [SyncSchema] 
ECHO     [SyncData] [LogFile] [SqlCmpOptions] [SqlUser] [SqlPass]
ECHO     [SqlCompareExe] [SqlDataCmpExe]
ECHO.
ECHO Arguments:
ECHO   SourceFolder   Folder containing SQL Source Control scripted database files
ECHO                  Ex: C:\_tfs\eSolutions\DBSources\Development\Phase3\Master
ECHO   TargetServer   Name of the target SQL Server
ECHO                  Ex: Dev2
ECHO   TargetDatabase Name of database on target server
ECHO                  Ex: Phase3
ECHO   SyncSchema     Optional: Y/N/S
ECHO                    Y=Sync target schema
ECHO                    N=Don't sync target schema
ECHO                    S=Just generate change scripts
ECHO                  Default: N
ECHO   SyncData       Optional: Y/N/S
ECHO                    Y=Sync target schema
ECHO                    N=Don't sync target schema
ECHO                    S=Just generate change scripts
ECHO                  Default: N
ECHO   LogFile        Optional: Y/N
ECHO                    Y=Send output to log file
ECHO                    N=Send output to console
ECHO                  Default: N
ECHO   SqlCmpOptions  Optional: SQLCompare Options (see SQL Compare command
ECHO                  line help for /Options:)
ECHO                  Default: NoTransactions
ECHO   SqlUser        Optional: SQL user to connect to target database
ECHO                  Default: Current windows user
ECHO   SqlPass        Optional: SQL user password
ECHO                  Default: none
ECHO   SqlCompareExe  Optional: Fully qualified path of sqlcompare.exe
ECHO                  Default: default install path
ECHO   SqlDataCmpExe  Optional: Fully qualified path of sqldatacompare.exe
ECHO                  Default: default install path
ECHO.
ECHO Note:
ECHO   DeployDatabase will create SQL change scripts and optionally a log file
ECHO   in the same folder as the batch file. Hence, do not put this batch file
ECHO   in SourceFolder as the generated SQL change scripts will confuse the sync.
GOTO End

:Start
SET SourceFolder=%~1
SET TargetServer=%~2
SET TargetDatabase=%~3
SET SyncSchema=%~4
SET SyncData=%~5
SET LogFile=%~6
SET SqlCmpOptions=%~7
SET SqlUser=%~8
SET SqlPass=%~9

SET OutputFolder=%~dp0
SET LogFileName=%OutputFolder%%~n0.log
SET ErrFileName=%OutputFolder%%~n0.error

SHIFT
SET SqlCompareExe=%~s9
SHIFT
SET SqlDataCmpExe=%~s9

IF "%SourceFolder%"=="" GOTO Usage
IF "%SourceFolder%"=="?" GOTO Usage
IF "%SourceFolder%"=="/?" GOTO Usage
IF "%TargetServer%"=="" GOTO Usage
IF "%TargetDatabase%"=="" GOTO Usage
IF "%SyncSchema%"=="" SET SyncSchema=N
IF "%SyncData%"=="" SET SyncData=N
IF "%LogFile%"=="" SET LogFile=N
IF "%SqlCmpOptions%"=="" SET SqlCmpOptions=Default

IF "%SqlCompareExe%"=="" SET SqlCompareExe=C:\Program Files\Red Gate\SQL Compare 13\sqlcompare.exe
IF NOT EXIST "%SqlCompareExe%" SET SqlCompareExe=C:\Program Files (x86)\Red Gate\SQL Compare 13\sqlcompare.exe
IF NOT EXIST "%SqlCompareExe%" SET SqlCompareExe=C:\Program Files\Red Gate\SQL Compare 12\sqlcompare.exe
IF NOT EXIST "%SqlCompareExe%" SET SqlCompareExe=C:\Program Files (x86)\Red Gate\SQL Compare 12\sqlcompare.exe
IF NOT EXIST "%SqlCompareExe%" SET SqlCompareExe=C:\Program File\Red Gate\SQL Compare 11\sqlcompare.exe
IF NOT EXIST "%SqlCompareExe%" SET SqlCompareExe=C:\Program Files (x86)\Red Gate\SQL Compare 11\sqlcompare.exe
IF NOT EXIST "%SqlCompareExe%" SET SqlCompareExe=C:\Program File\Red Gate\SQL Compare 10\sqlcompare.exe
IF NOT EXIST "%SqlCompareExe%" SET SqlCompareExe=C:\Program Files (x86)\Red Gate\SQL Compare 10\sqlcompare.exe

IF "%SqlDataCmpExe%"=="" SET SqlDataCmpExe=C:\Program Files\Red Gate\SQL Data Compare 13\sqldatacompare.exe
IF NOT EXIST "%SqlDataCmpExe%" SET SqlDataCmpExe=C:\Program Files (x86)\Red Gate\SQL Data Compare 13\sqldatacompare.exe
IF NOT EXIST "%SqlDataCmpExe%" SET SqlDataCmpExe=C:\Program Files\Red Gate\SQL Data Compare 12\sqldatacompare.exe
IF NOT EXIST "%SqlDataCmpExe%" SET SqlDataCmpExe=C:\Program Files (x86)\Red Gate\SQL Data Compare 12\sqldatacompare.exe
IF NOT EXIST "%SqlDataCmpExe%" SET SqlDataCmpExe=C:\Program Files\Red Gate\SQL Data Compare 11\sqldatacompare.exe
IF NOT EXIST "%SqlDataCmpExe%" SET SqlDataCmpExe=C:\Program Files (x86)\Red Gate\SQL Data Compare 11\sqldatacompare.exe
IF NOT EXIST "%SqlDataCmpExe%" SET SqlDataCmpExe=C:\Program Files\Red Gate\SQL Data Compare 10\sqldatacompare.exe
IF NOT EXIST "%SqlDataCmpExe%" SET SqlDataCmpExe=C:\Program Files (x86)\Red Gate\SQL Data Compare 10\sqldatacompare.exe

IF /i "%OutputFolder%"=="C:\Windows\System32\" (
	IF NOT EXIST "C:\Windows\Temp\SqlCompare" MD "C:\Windows\Temp\SqlCompare"
	IF NOT EXIST "C:\Windows\Temp\SqlCompare\%TargetDatabase%" MD "C:\Windows\Temp\SqlCompare\%TargetDatabase%"
	SET OutputFolder=C:\Windows\Temp\SqlCompare\%TargetDatabase%\
)

SET SyncSchemaArg=
SET SyncDataArg=
SET OutputWidth=
IF /i "%SyncSchema%"=="Y" SET SyncSchemaArg=/Synchronize
IF /i "%SyncData%"=="Y" SET SyncDataArg=/Synchronize
IF /i "%LogFile%"=="Y" SET OutputWidth=/OutputWidth:150
IF NOT "%SqlCmpOptions%"=="" SET SqlCmpOptionsArg=/Options:%SqlCmpOptions%
IF NOT "%SqlUser%"=="" SET SqlUserArg=/UserName2:%SqlUser% /Password2:%SqlPass%

SET SqlCompareCmd="%SqlCompareExe%" /Scripts1:"%SourceFolder%" /Server2:%TargetServer% /Database2:%TargetDatabase% %SqlUserArg% /ScriptFile:"%OutputFolder%%TargetDatabase%_SqlCompare.sql" /Force %SyncSchemaArg% %SqlCmpOptionsArg% /Report:"%OutputFolder%%TargetDatabase%_SqlCompare.htm" /ReportType:Interactive %OutputWidth%
SET SqlDataCmpCmd="%SqlDataCmpExe%" /Scripts1:"%SourceFolder%" /Server2:%TargetServer% /Database2:%TargetDatabase% %SqlUserArg% /ScriptFile:"%OutputFolder%%TargetDatabase%_SqlDataCmp.sql" /Force %SyncDataArg% /Options:Default,ForceBinaryCollation /Export:"%OutputFolder%%TargetDatabase%_SqlDataCmp" %OutputWidth%

IF /i NOT %Debug%==ON GOTO SkipDebug
ECHO.
ECHO SourceFolder    %SourceFolder%
ECHO TargetServer    %TargetServer%
ECHO TargetDatabase  %TargetDatabase%
ECHO SyncSchema      %SyncSchema%
ECHO SyncData        %SyncData%
ECHO LogFile         %LogFile%
ECHO SqlCmpOptions   %SqlCmpOptions%
ECHO SqlUser         %SqlUser%
ECHO SqlPass         %SqlPass%
ECHO SqlCompareExe   %SqlCompareExe%
ECHO SqlDataCmpExe   %SqlDataCmpExe%
ECHO.
ECHO LogFileName     %LogFileName%
ECHO.
ECHO SqlCompareCmd:
ECHO %SqlCompareCmd%
ECHO.
ECHO SqlDataCmpCmd:
ECHO %SqlDataCmpCmd%
ECHO.
PAUSE
:SkipDebug

SET Pass=0
SET MaxPass=2

:Loop
SET /a Pass+=1
SET ExitCode1=
SET ExitCode2=

IF EXIST "%ErrFileName%" DEL "%ErrFileName%" /f/q
IF /i "%LogFile%"=="Y" (
	rem IF EXIST "%LogFileName%" DEL "%LogFileName%" /f/q
	SET RedirLog=^>^> "%LogFileName%" 2^>^&1
) ELSE (
	SET RedirLog=2^>^&1
)

ECHO Deploying: %SourceFolder% %RedirLog%
ECHO To: %TargetServer%\%TargetDatabase% %RedirLog%
ECHO Pass %Pass% of %MaxPass% %RedirLog%
ECHO. %RedirLog%
ECHO Syncing schema... %RedirLog%
ECHO. %RedirLog%

:SchemaSync
IF /i NOT "%SyncSchema%"=="Y" IF /i NOT "%SyncSchema%"=="S" (
	SET ExitCode1=0
	GOTO DataSync
)
%SqlCompareCmd% %RedirLog% 2>&1
SET ExitCode1=%ERRORLEVEL%
ECHO. %RedirLog%
IF "%ExitCode1%"=="63" SET ExitCode1=0
IF "%ExitCode1%"=="0" GOTO DataSync
ECHO Exit Code: %ExitCode1% %RedirLog%
ECHO Exit Code: %ExitCode1% %RedirLog%

:DataSync
IF /i NOT "%SyncData%"=="Y" IF /i NOT "%SyncData%"=="S" (
	SET ExitCode2=0
	GOTO End
)
ECHO Syncing data... %RedirLog%
ECHO. %RedirLog%
%SqlDataCmpCmd% %RedirLog% 2>&1
SET ExitCode2=%ERRORLEVEL%
ECHO. %RedirLog%
IF "%ExitCode2%"=="63" SET ExitCode2=0
IF "%ExitCode2%"=="0" GOTO End
ECHO Exit Code: %ExitCode2% %RedirLog%
ECHO Exit Code: %ExitCode2% %RedirLog%
GOTO End

:End
rem If Pass not initialized, we never got into the loop, just exit
IF NOT DEFINED Pass GOTO EndLoop
IF "%Pass%"=="" GOTO EndLoop
IF %Pass%==0 GOTO EndLoop

rem If ExitCodes are 0, sync was successful, skip remaining passes and exit
IF "%ExitCode1%%ExitCode2%"=="00" GOTO EndLoop

rem If not at MaxPass yet, loop for another pass
IF %Pass% LSS %MaxPass% GOTO Loop
:EndLoop

rem If MaxPasses exceeded, return exit code 
SET ExitCode=0
IF "%ExitCode1%"=="" SET ExitCode1=0
IF "%ExitCode2%"=="" SET ExitCode2=0
IF NOT "%ExitCode1%"=="0" SET ExitCode=%ExitCode1%
IF NOT "%ExitCode2%"=="0" SET ExitCode=%ExitCode2%
EXIT /b %ExitCode%
ENDLOCAL
