@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
SETLOCAL
GOTO Start

:Usage
ECHO.
ECHO InstallNSB:
ECHO   Installs a NServiceBus host service
ECHO.
ECHO Usage:
ECHO   InstallNSB SourceFolder TargetFolder [ConfigEnv] [ServiceUser] 
ECHO      [ServicePass] [ServiceName] [Description] [LogFile] [LogFileName]
ECHO.
ECHO Arguments:
ECHO   SourceFolder  UNC of service drop folder, usually under 
ECHO                 _PublishedApplications.
ECHO                 Ex: _PublishedApplications\Backend.Service.MessageService
ECHO   TargetFolder  Path to where the service will be installed.  This must
ECHO                 be a local path on the target server, not a UNC.
ECHO                 Ex: C:\FAME\Backend.Service.MessageService
ECHO   ConfigEnv     Optional: Environment ID of config files to use
ECHO                 Ex: Dev, QA, Demo, Prod, etc.
ECHO                 Default: none
ECHO   ServiceUser   Optional: User name under which service will run
ECHO                 Default: none - Local Service
ECHO   ServicePass   Optional: Password for ServiceUser
ECHO                 Default: none
ECHO   ServiceName   Optional: Name used in the service registry
ECHO                 Default: Name space of initialization class
ECHO   Description   Optional: Description displayed in Service Manager
ECHO                 Default: none
ECHO   LogFile       Optional: Y/N
ECHO                 Y=Send output to log file
ECHO                 N=Send output to console
ECHO                 Default: N
ECHO   LogFileName   Optional: Fully qualified path and name of the log file
ECHO                 Default: Same as this bach file, but with .log extension
ECHO   Uninstall     Optional: Y/N
ECHO                 Y=Uninstall existing service before installing
ECHO                 N=Do not uninstall first.  Avoids problems with SCM getting
ECHO                   stuck and maintains any custom service configuration.
ECHO                   However, service path, exe, etc. must remain the same.
ECHO                 Default: N
GOTO End

:Start
SET SourceFolder=%~1
SET TargetFolder=%~2
SET ConfigEnv=%~3
SET ServiceUser=%~4
SET ServicePass=%~5
SET ServiceName=%~6
SET Description=%~7
SET LogFile=%~8
SET LogFileName=%~9

IF "%SourceFolder%"=="?" GOTO Usage
IF "%SourceFolder%"=="/?" GOTO Usage

IF "%SourceFolder%"=="" GOTO Usage
IF "%TargetFolder%"=="" GOTO Usage
REM IF "%ConfigEnv%"=="" GOTO Usage
REM IF "%ServiceName%"=="" GOTO Usage
REM IF "%Description%"=="" GOTO Usage
IF "%LogFile%"=="" SET LogFile=N

SET PubAppsFolder=%~dp0
IF "%LogFileName%"=="" SET LogFileName=%PubAppsFolder%%~n0.log
SET TmpLogFolder=%~nx2

IF /i "%PubAppsFolder%"=="C:\Windows\System32\" (
	IF NOT EXIST "C:\Windows\Temp\%TmpLogFolder%" MD "C:\Windows\Temp\%TmpLogFolder%"
	SET LogFileName=C:\Windows\Temp\%TmpLogFolder%\%~n0.log
	DEL /F/Q "C:\Windows\Temp\%TmpLogFolder%\*.*"
)

IF NOT EXIST "%SourceFolder%" SET SourceFolder=%PubAppsFolder%\%SourceFolder%

SHIFT
SET Unistall=%~9
SET Delete=Y
IF /i "%Unistall%"=="" SET Unistall=N
IF /i "%Unistall:~0,1%"=="Y" SET Unistall=Y
IF NOT EXIST "%TargetFolder%" SET Unistall=N
IF NOT EXIST "%TargetFolder%" SET Delete=N

SET UnistSvc="%TargetFolder%\NServiceBus.Host.exe" /uninstall /serviceName:"%ServiceName%"
SET DelSvc=DEL "%TargetFolder%\*.*" /f/s/q

SET CopySvc=XCOPY "%SourceFolder%\*.*" "%TargetFolder%" /q/i/s/e/k/r/h/y

IF NOT "%ServiceUser%"=="" SET UserArg=/username:"%ServiceUser%"
IF NOT "%ServiceUser%"=="" IF NOT "%ServicePass%"=="" SET PassArg=/password:"%ServicePass%"
SET InstSvc="NServiceBus.Host.exe" /install /serviceName:"%ServiceName%" /displayName:"%ServiceName%" /description:"%Description%" %UserArg% %PassArg% NServiceBus.Production

IF /i NOT %Debug%==ON GOTO SkipDebug
ECHO.
ECHO SourceFolder    %SourceFolder%
ECHO TargetFolder    %TargetFolder%
ECHO ConfigEnv       %ConfigEnv%
ECHO ServiceUser     %ServiceUser%
ECHO ServicePass     %ServicePass%
ECHO ServiceName     %ServiceName%
ECHO Description     %Description%
ECHO LogFile         %LogFile%
ECHO.
ECHO LogFileName     %LogFileName%
ECHO.
ECHO UnistSvc:       %Unistall%
ECHO %UnistSvc%
ECHO.
ECHO DelSvc:         %Delete%
ECHO %DelSvc%
ECHO.
ECHO CopySvc:
ECHO %CopySvc%
ECHO.
ECHO InstSvc:
ECHO %InstSvc%
ECHO.
IF /i NOT %Debug%==ON GOTO SkipDebug
PAUSE
:SkipDebug

SET ExitCode=1
IF NOT EXIST "%SourceFolder%" GOTO Error

SET ExitCode=0
SET RedirLog=
IF /i "%LogFile%"=="Y" SET RedirLog=^>^> "%LogFileName%"
IF EXIST "%LogFileName%" DEL /F "%LogFileName%"

ECHO Installing service %ServiceName% To %TargetFolder% %RedirLog%

ECHO Stopping existing service... %RedirLog%
CALL %PubAppsFolder%SvcCtl.bat "%ServiceName%" stop "" 3 10 "%LogFile%"
SET ExitCode=%ERRORLEVEL%
IF NOT "%ExitCode%"=="0" GOTO Error

IF NOT "%Unistall%"=="Y" GOTO DelSvc
ECHO Uninstalling existing service... %RedirLog%
%UnistSvc% %RedirLog%

:DelSvc
IF NOT "%Delete%"=="Y" GOTO CopySvc
ECHO Deleting contents of target folder... %RedirLog%
%DelSvc% > NUL

:CopySvc
ECHO Copying drop files to target folder... %RedirLog%
%CopySvc% %RedirLog%
SET ExitCode=%ERRORLEVEL%
IF NOT "%ExitCode%"=="0" GOTO Error

ECHO Setting up config for environment... %RedirLog%
CALL "%PubAppsFolder%ConfigEnv.bat" "%TargetFolder%" "%ConfigEnv%" %LogFile%
SET ExitCode=%ERRORLEVEL%
IF NOT "%ExitCode%"=="0" GOTO Error

:InstSvc
IF NOT "%Unistall%"=="Y" GOTO StartSvc
ECHO Installing new service... %RedirLog%
PUSHD %TargetFolder%
%InstSvc% %RedirLog%
POPD
SET ExitCode=%ERRORLEVEL%
IF NOT "%ExitCode%"=="0" GOTO Error

:StartSvc
ECHO Starting new service... %RedirLog%
CALL %PubAppsFolder%SvcCtl.bat "%ServiceName%" start "" 3 10 %LogFile%
SET ExitCode=%ERRORLEVEL%
IF NOT "%ExitCode%"=="0" GOTO Error

ECHO. %RedirLog%
ECHO To uninstall service: %RedirLog%
ECHO %UnistSvc% %RedirLog%
ECHO. %RedirLog%
ECHO To reinstall service: %RedirLog%
ECHO %InstSvc% %RedirLog%
ECHO. %RedirLog%
GOTO End

:Error
ECHO Terminating with ExitCode %ExitCode% %RedirLog%
ECHO. %RedirLog%

:End
EXIT /b %ExitCode%
ENDLOCAL
