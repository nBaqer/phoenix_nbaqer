@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
SETLOCAL
GOTO Start

:Usage
ECHO.
ECHO DeployNSB:
ECHO   Deploys a NServiceBus host service
ECHO.
ECHO Usage:
ECHO   DeployNSB SourceFolder TargetFolder [ConfigEnv] [TargetServer]  
ECHO     [ServiceUser] [ServicePass] [ServiceName] [Description] [LogFile]
ECHO     [Uninstall]
ECHO.
ECHO Arguments:
ECHO   SourceFolder  UNC of service drop folder, usually under 
ECHO                 _PublishedApplications.
ECHO                 Ex: _PublishedApplications\Backend.Service.MessageService
ECHO   TargetFolder  Path to where the service will be installed.  This must
ECHO                 be a local path on the target server, not a UNC.
ECHO                 Ex: C:\FAME\Backend.Service.MessageService
ECHO   ConfigEnv     Optional: Environment ID of config files to use
ECHO                 Ex: Dev, QA, Demo, Prod, etc.
ECHO                 Default: none
ECHO   TargetServer  Optional: Name of the target server where the service will
ECHO                 be installed.
ECHO                 Default: none - local server
ECHO   ServiceUser   Optional: User name under which service will run
ECHO                 Default: none - Local Service
ECHO   ServicePass   Optional: Password for ServiceUser
ECHO                 Default: none
ECHO   ServiceName   Optional: Name used in the service registry
ECHO                 Default: Name space of initialization class
ECHO   Description   Optional: Description displayed in Service Manager
ECHO                 Default: none
ECHO   LogFile       Optional: Y/N
ECHO                 Y=Send output to log file
ECHO                 N=Send output to console
ECHO                 Default: N
ECHO   Uninstall     Optional: Y/N
ECHO                 Y=Uninstall existing service before installing
ECHO                 N=Do not uninstall first.  Avoids problems with SCM getting
ECHO                   stuck and maintains any custom service configuration.
ECHO                   However, service path, exe, etc. must remain the same.
ECHO                 Default: N
GOTO End

:Start
SET SourceFolder=%~1
SET TargetFolder=%~2
SET ConfigEnv=%~3
SET TargetServer=%~4
SET ServiceUser=%~5
SET ServicePass=%~6
SET ServiceName=%~7
SET Description=%~8
SET LogFile=%~9

IF "%SourceFolder%"=="?" GOTO Usage
IF "%SourceFolder%"=="/?" GOTO Usage

IF "%SourceFolder%"=="" GOTO Usage
IF "%TargetFolder%"=="" GOTO Usage
REM IF "%ConfigEnv%"=="" GOTO Usage
REM IF "%TargetServer%"=="" GOTO Usage
REM IF "%ServiceName%"=="" GOTO Usage
REM IF "%Description%"=="" GOTO Usage
IF "%LogFile%"=="" SET LogFile=N

SET PubAppsFolder=%~dp0
SET LogFileName=%PubAppsFolder%%~n0.log
SET TmpLogFolder=%~nx2

SHIFT
SET Unistall=%~9

IF NOT EXIST "%SourceFolder%" SET SourceFolder=%PubAppsFolder%%SourceFolder%
IF NOT "%TargetServer%"=="" IF /i "%TargetServer%"=="%COMPUTERNAME%" SET TargetServer=

rem If we are running from TFS Build and RedGate is on a remote server, TFS 
rem won't capture console output from PSExec, so we need to send console to a  
rem file and then TYPE it to the screen
IF NOT "%TargetServer%"=="" IF NOT "%TF_BUILD%"=="" SET TfsRemote=Y

SET PSExec=psexec
IF NOT "%ProgramFiles(x86)%"=="" IF EXIST "%PubAppsFolder%psexec64.exe" SET PSExec=psexec64
SET PSExecSwitches=-u "Internal\TFSBuild" -p TFS.Build -h -accepteula -w C:\Windows\Temp cmd /c

SET InstallNSBBat=%PubAppsFolder%InstallNSBCmd.bat
SET InstallNSBLog=%LogFile%
IF "%TargetServer%"=="" (
	SET SET InstallNSB=%InstallNSBBat%
	SET TargetServerDesc=local machine
) ELSE (
	SET InstallNSB="%PubAppsFolder%psexec" \\%TargetServer% %PSExecSwitches% "%InstallNSBBat%" 2^>^&1
	IF "%TfsRemote%"=="Y" SET InstallNSBLog=N
	SET TargetServerDesc=%TargetServer%
)
SET InstallNSBCmd=@"%PubAppsFolder%InstallNSB.bat" "%SourceFolder%" "%TargetFolder%" "%ConfigEnv%" "%ServiceUser%" "%ServicePass%" "%ServiceName%" "%Description%" "%InstallNSBLog%" "" "%Unistall%"

IF /i NOT %Debug%==ON GOTO SkipDebug
ECHO.
ECHO SourceFolder    %SourceFolder%
ECHO TargetFolder    %TargetFolder%
ECHO ConfigEnv       %ConfigEnv%
ECHO TargetServer    %TargetServer%
ECHO ServiceUser     %ServiceUser%
ECHO ServicePass     %ServicePass%
ECHO ServiceName     %ServiceName%
ECHO Description     %Description%
ECHO LogFile         %LogFile%
ECHO Unistall        %Unistall%
ECHO.
ECHO LogFileName     %LogFileName%
ECHO.
ECHO InstallNSBCmd:
ECHO %InstallNSBCmd%
ECHO.
ECHO InstallNSB:
ECHO %InstallNSB%
ECHO.
PAUSE
:SkipDebug

SET ExitCode=1
IF NOT EXIST "%SourceFolder%" GOTO End

SET ExitCode=0
SET RedirLog=
IF /i "%LogFile%"=="Y" SET RedirLog=^>^> "%LogFileName%"
SET RedirCmd=
SET RedirCmdLog=%PubAppsFolder%InstallService.log
IF "%TfsRemote%"=="Y" SET RedirCmd=^> "%RedirCmdLog%"

ECHO. %RedirLog%
ECHO Executing installation batch file on %TargetServerDesc%... %RedirLog%
IF EXIST "%InstallNSBBat%" DEL /Q/F "%InstallNSBBat%"
IF EXIST "%InstallNSBBat%" (SET ExitCode=1 & GOTO Error)

ECHO %InstallNSBCmd% ^%RedirCmd% > "%InstallNSBBat%"
IF NOT EXIST "%InstallNSBBat%" (SET ExitCode=1 & GOTO Error)
IF "%TfsRemote%"=="Y" IF EXIST "%RedirCmdLog%" DEL /Q/F "%RedirCmdLog%"
ECHO %InstallNSB% %RedirLog%

CALL %InstallNSB%
SET ExitCode=%ERRORLEVEL%
IF "%TfsRemote%"=="Y" IF EXIST "%RedirCmdLog%" TYPE "%RedirCmdLog%"
IF NOT "%ExitCode%"=="0" GOTO Error

ECHO Installation batch file completed successfully %RedirLog%
ECHO. %RedirLog%
GOTO End

:Error
ECHO Terminating with ExitCode %ExitCode% %RedirLog%
ECHO. %RedirLog%

:End

ECHO. %RedirLog%
EXIT /b %ExitCode%
ENDLOCAL
