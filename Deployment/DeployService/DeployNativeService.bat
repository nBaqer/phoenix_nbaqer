@ECHO off & cls

SETLOCAL
GOTO Start

:Usage
ECHO.
ECHO DeployService:
ECHO   Deploys a Native Windows service
ECHO.
ECHO Usage:
ECHO   DeployService SourceFolder TargetFolder ServiceExe ConfigEnv 
ECHO     TargetServer ServiceUser ServicePass ServiceName 
ECHO
ECHO Arguments:
ECHO   SourceFolder  UNC of service drop folder, usually under 
ECHO                 _PublishedApplications.
ECHO                 Ex: _PublishedApplications\eSolServices.Client.ESP.EOUTWatcher
ECHO   TargetFolder  Path to where the service will be installed.
ECHO                 Ex: C:\FAME\eSolServices.Client.ESP.EOUTWatcher
ECHO   ServiceExe    File name of servcice executable
ECHO                 Ex: FAME.eSolServices.Client.ESP.EOUTWatcher.exe
ECHO   ConfigEnv     Environment ID of config files to use
ECHO                 Ex: Dev, QA, Demo, Prod, etc.
ECHO                 Default: none
ECHO   TargetServer  Name of the target server where the service will
ECHO                 be installed.
ECHO                 Default: none - local server
ECHO   ServiceUser   Domain User Name used for authentication to the target server
ECHO                 
ECHO   ServicePass   Password for ServiceUser Domain User Name used for authentication to the target server

ECHO   ServiceName   Name used in the service registry
ECHO                 
GOTO End



:Start
SET SourceFolder=%~1
SET TargetFolder=%~2
SET ServiceExe=%~3
SET ConfigEnv=%~4
SET TargetServer=%~5
SET ServiceUser=%~6
SET ServicePass=%~7
SET ServiceName=%~8
SET ExitCode=0

IF "%SourceFolder%"=="?" GOTO Usage
IF "%SourceFolder%"=="/?" GOTO Usage
IF "%SourceFolder%"=="" GOTO Usage

SET LogFile=%SourceFolder%\DeployNativeService.log


ECHO Disconnecting previous connection %TargetServer%
@ECHO Disconnecting previous connection %TargetServer% >> %LogFile%
net use /delete \\%TargetServer%

ECHO Connecting to target server %TargetServer%
@ECHO Connecting to target server %TargetServer% >> %LogFile%
net use \\%TargetServer% %ServicePass% /user:%ServiceUser%

SET isInstalled="0"

ECHO Cheking If %ServiceName% Service is runnning
@ECHO Cheking If %ServiceName% Service is runnning >> %LogFile%

for /F "tokens=3 delims=: " %%H in ('sc \\%TargetServer% query %ServiceName% ^| findstr "        STATE"') do (
  ECHO Service State is: %%H
  @ECHO Service State is: %%H >> %LogFile%
  if /I "%%H" == "RUNNING" (
   ECHO Stopping Service
   @ECHO Stopping Service >> %LogFile%
   sc \\%TargetServer% stop %ServiceName%
   :whileIsStopping
   for /F "tokens=3 delims=: " %%M in ('sc \\%TargetServer% query %ServiceName% ^| findstr "        STATE"') do (
		ECHO Service State is: %%M
		@ECHO Service State is: %%M >> %LogFile%
		if /I "%%M" == "STOP_PENDING" (
			goto :whileIsStopping
		)
   )
  )
  SET isInstalled="1"
)



IF %isInstalled% == "1" (
	ECHO Service is Installed
	@ECHO Service is Installed >> %LogFile%
	
	ECHO Copying Files...
	@ECHO Copying Files... >> %LogFile%
	
	XCOPY "%SourceFolder%" "%TargetFolder%" /S /E /Y /I
	
	ECHO Source Files were copied to Target folder
	@ECHO Source Files were copied to Target folder >> %LogFile%
	
	ECHO Copying Config Files
	@ECHO Copying Config Files >> %LogFile%
	
	XCOPY "%TargetFolder%\Configs\%ConfigEnv%" "%TargetFolder%\Configs" /S /E /Y /I
	
	ECHO Config files were updated
	@ECHO Config files were updated >> %LogFile%
	
	ECHO Starting Service...
	@ECHO Starting Service... >> %LogFile%
	
	sc \\%TargetServer% start %ServiceName%
	
	:whileIsStaringInstalled
	for /F "tokens=3 delims=: " %%M in ('sc \\%TargetServer% query %ServiceName% ^| findstr "        STATE"') do (
		ECHO Service State is: %%M
		@ECHO Service State is: %%M >> %LogFile%
		if /I "%%M" == "START_PENDING" (
			goto :whileIsStaringInstalled
		)
	)
	
	ECHO %ServiceName% was started
	@ECHO %ServiceName% was started >> %LogFile%
	)
IF %isInstalled% == "0" (
	ECHO Service is Not Installed
	@ECHO Service is Not Installed >> %LogFile%
	
	ECHO Copying Files...
	@ECHO Copying Files... >> %LogFile%
	
	XCOPY "%SourceFolder%" "%TargetFolder%" /S /E /Y /I
	
	ECHO Source Files were copied to Target folder
	@ECHO Source Files were copied to Target folder >> %LogFile%

	ECHO Copying Config Files
	@ECHO Copying Config Files >> %LogFile%
	
	XCOPY "%TargetFolder%\Configs\%ConfigEnv%" "%TargetFolder%\Configs" /S /E /Y /I

	ECHO Config files were updated
	@ECHO Config files were updated >> %LogFile%
	
	ECHO Creating Service %ServiceName% ...
	@ECHO Creating Service %ServiceName% ... >> %LogFile%
	
	sc \\%TargetServer% create %ServiceName% binpath= "%TargetFolder%\%ServiceExe%"
	
	ECHO Starting Service...
	@ECHO Starting Service... >> %LogFile%
	
	sc \\%TargetServer% start %ServiceName%
	
	:whileIsStaringNotInstalled
	for /F "tokens=3 delims=: " %%M in ('sc \\%TargetServer% query %ServiceName% ^| findstr "        STATE"') do (
		ECHO Service State is: %%M
		@ECHO Service State is: %%M >> %LogFile%
		if /I "%%M" == "START_PENDING" (
			goto :whileIsStaringNotInstalled
		)
	)
	
	ECHO %ServiceName% was started
	@ECHO %ServiceName% was started >> %LogFile%
	)

ECHO Deployment Completed.
@ECHO Deployment Completed. >> %LogFile%

:End

ENDLOCAL