@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
SETLOCAL
GOTO Start

:Usage
ECHO.
ECHO InstallService:
ECHO   Installs a TopShelf Windows service
ECHO.
ECHO Usage:
ECHO   InstallService SourceFolder TargetFolder ServiceExe [ConfigEnv] 
ECHO    [ServiceUser] [ServicePass] [ServiceName] [Description] [LogFile]
ECHO    [LogFileName] [AutoStart] [Uninstall]
ECHO
ECHO Arguments:
ECHO   SourceFolder  UNC of service drop folder, usually under 
ECHO                 _PublishedApplications.
ECHO                 Ex: _PublishedApplications\eSolServices.Client.ESP.EOUTWatcher
ECHO   TargetFolder  Path to where the service will be installed.  This must
ECHO                 be a local path on the target server, not a UNC.
ECHO                 Ex: C:\FAME\eSolServices.Client.ESP.EOUTWatcher
ECHO   ServiceExe    File name of servcice executable
ECHO                 Ex: FAME.eSolServices.Client.ESP.EOUTWatcher.exe
ECHO   ConfigEnv     Optional: Environment ID of config files to use
ECHO                 Ex: Dev, QA, Demo, Prod, etc.
ECHO                 Default: none
ECHO   ServiceUser   Optional: User name under which service will run
ECHO                 Default: none - Local Service
ECHO   ServicePass   Optional: Password for ServiceUser
ECHO                 Default: none
ECHO   ServiceName   Optional: Name used in the service registry
ECHO                 Default: Name space of initialization class
ECHO   Description   Optional: Description displayed in Service Manager
ECHO                 Default: none
ECHO   LogFile       Optional: Y/N
ECHO                 Y=Send output to log file
ECHO                 N=Send output to console
ECHO                 Default: N
ECHO   LogFileName   Optional: Fully qualified path and name of the log file
ECHO                 Default: Same as this bach file, but with .log extension
ECHO   AutoStart     Optional: Start service after installation
ECHO                 Y=Start service
ECHO                 N=Don't start service
ECHO                 Default: Y (start service)
ECHO   Uninstall     Optional: Y/N
ECHO                 Y=Uninstall existing service before installing
ECHO                 N=Do not uninstall first.  Avoids problems with SCM getting
ECHO                   stuck and maintains any custom service configuration.
ECHO                   However, service path, exe, etc. must remain the same.
ECHO                 Default: N
GOTO End

:Start
SET SourceFolder=%~1
SET TargetFolder=%~2
SET ServiceExe=%~3
SET ConfigEnv=%~4
SET ServiceUser=%~5
SET ServicePass=%~6
SET ServiceName=%~7
SET Description=%~8
SET LogFile=%~9

SET PubAppsFolder=%~dp0
SET TmpLogFolder=%~nx2
SET LogName=%~n0.log
SET DfltLogFileName=%PubAppsFolder%%LogName%

SHIFT
SET LogFileName=%~9
IF "%LogFileName%"=="" SET LogFileName=%DfltLogFileName%
SHIFT
SET AutoStart=%~9
IF "%AutoStart%"=="" SET AutoStart=Y
IF /i "%AutoStart%"=="Y" SET AutoStart=Y
SHIFT
SET Unistall=%~9
IF "%Unistall%"=="" SET Unistall=N
IF /i "%Unistall%"=="Y" SET Unistall=Y

IF "%SourceFolder%"=="?" GOTO Usage
IF "%SourceFolder%"=="/?" GOTO Usage

IF "%SourceFolder%"=="" GOTO Usage
IF "%TargetFolder%"=="" GOTO Usage
IF "%ServiceExe%"=="" GOTO Usage
REM IF "%ConfigEnv%"=="" GOTO Usage
REM IF "%ServiceUser%"=="" GOTO Usage
REM IF "%ServicePass%"=="" GOTO Usage
REM IF "%ServiceName%"=="" GOTO Usage
REM IF "%Description%"=="" GOTO Usage
IF "%LogFile%"=="" SET LogFile=N

IF /i "%PubAppsFolder%"=="C:\Windows\System32\" (
	IF NOT EXIST "C:\Windows\Temp\%TmpLogFolder%" MD "C:\Windows\Temp\%TmpLogFolder%"
	SET LogFileName=C:\Windows\Temp\%TmpLogFolder%\%LogName%
	DEL /F/Q "C:\Windows\Temp\%TmpLogFolder%\*.*"
)
IF NOT EXIST "%SourceFolder%" SET SourceFolder=%PubAppsFolder%%SourceFolder%
IF NOT "%SourceFolder:~-1%"=="\" SET SourceFolder=%SourceFolder%\

SET Delete=Y
IF NOT EXIST "%TargetFolder%" SET Unistall=N 
IF NOT EXIST "%TargetFolder%" SET Delete=N 

SET DisplayName=%ServiceName%
SET ServiceName=%ServiceName: =%

IF NOT "%ServiceName%"=="" (
	SET DspNameArg=-displayname "%DisplayName%"
	SET SvcNameArg=-servicename "%ServiceName%"
)


rem SET StopSvc="%TargetFolder%\%ServiceExe%" stop %SvcNameArg%
SET StopSvc=CALL "%PubAppsFolder%SvcCtl.bat" "%ServiceName%" stop "" 3 10
SET UnistSvc="%ServiceExe%" uninstall %SvcNameArg%
SET DelSvc=DEL "%TargetFolder%\*.*" /f/s/q

SET CopySvc=XCOPY "%SourceFolder%*.*" "%TargetFolder%" /q/i/s/e/k/r/h/y

IF NOT "%Description%"=="" SET SvcDescArg=-description "%Description%"
IF NOT "%ServiceUser%"=="" SET UserArg=-username "%ServiceUser%"
IF NOT "%ServiceUser%"=="" IF NOT "%ServicePass%"=="" SET PassArg=-password "%ServicePass%"
SET InstSvc="%ServiceExe%" install %SvcNameArg% %DspNameArg% %SvcDescArg% %UserArg% %PassArg%

rem SET StartSvc="%TargetFolder%\%ServiceExe%" start %SvcNameArg%
SET StartSvc=CALL "%PubAppsFolder%SvcCtl.bat" "%ServiceName%" start "" 3 10
SET ConSvc=CALL "%PubAppsFolder%ConfigEnv.bat" "%TargetFolder%" "%ConfigEnv%"

IF /i NOT %Debug%==ON GOTO SkipDebug
ECHO.
ECHO SourceFolder    %SourceFolder%
ECHO TargetFolder    %TargetFolder%
ECHO ServiceExe      %ServiceExe%
ECHO ConfigEnv       %ConfigEnv%
ECHO ServiceUser     %ServiceUser%
ECHO ServicePass     %ServicePass%
ECHO ServiceName     %ServiceName%
ECHO Description     %Description%
ECHO LogFile         %LogFile%
ECHO LogFileName     %LogFileName%
ECHO AutoStart       %AutoStart%
ECHO.
ECHO StopSvc:
ECHO %StopSvc%
ECHO.
ECHO UnistSvc:       %Unistall%
ECHO %UnistSvc%
ECHO.
ECHO DelSvc:         %Delete%
ECHO %DelSvc%
ECHO.
ECHO CopySvc:
ECHO %CopySvc%
ECHO.
ECHO ConSvc:
ECHO %ConSvc%
ECHO.
ECHO InstSvc:
ECHO %InstSvc%
ECHO.
ECHO StartSvc:
IF %AutoStart%==Y ECHO %StartSvc%
ECHO.
PAUSE
:SkipDebug

SET ExitCode=1
IF NOT EXIST "%SourceFolder%" GOTO Error

SET ExitCode=0
SET RedirLog=
IF /i "%LogFile%"=="Y" SET RedirLog=^>^> "%LogFileName%"
IF EXIST "%LogFileName%" DEL /F "%LogFileName%"

ECHO Installing service to %TargetFolder% %RedirLog%

ECHO Stopping existing service... %RedirLog%
%StopSvc% %RedirLog% 
SET ExitCode=%ERRORLEVEL%
IF NOT "%ExitCode%"=="0" GOTO Error

IF NOT "%Unistall%"=="Y" GOTO DelSvc
ECHO Uninstalling existing service... %RedirLog%
PUSHD %TargetFolder%
%UnistSvc% %RedirLog%
POPD

:DelSvc
IF NOT "%Delete%"=="Y" GOTO CopySvc
ECHO Deleting contents of target folder... %RedirLog%
%DelSvc% > NUL

:CopySvc
ECHO Copying drop files to target folder... %RedirLog%
%CopySvc% %RedirLog%
SET ExitCode=%ERRORLEVEL%
IF NOT "%ExitCode%"=="0" GOTO Error

ECHO Setting up config for %ConfigEnv% environment %RedirLog%
%ConSvc%
SET ExitCode=%ERRORLEVEL%
IF NOT "%ExitCode%"=="0" GOTO Error

:InstSvc
IF NOT "%Unistall%"=="Y" GOTO StartSvc
ECHO Installing new service... %RedirLog%
PUSHD %TargetFolder%
%InstSvc% %RedirLog%
SET ExitCode=%ERRORLEVEL%
POPD
IF NOT "%ExitCode%"=="0" GOTO Error

:StartSvc
IF NOT %AutoStart%==Y GOTO CmdLineSum
ECHO Starting new service... %RedirLog%
%StartSvc% %RedirLog% 
SET ExitCode=%ERRORLEVEL%
IF NOT "%ExitCode%"=="0" GOTO Error

GOTO CmdLineSum

:Error
ECHO. %RedirLog%
ECHO Terminating with ExitCode %ExitCode% %RedirLog%

:CmdLineSum
ECHO. %RedirLog%
ECHO. %RedirLog%
ECHO To uninstall service: %RedirLog%
ECHO %UnistSvc% %RedirLog%
ECHO. %RedirLog%
ECHO To reinstall service: %RedirLog%
ECHO %InstSvc:CALL =% %RedirLog%
ECHO. %RedirLog%
GOTO End

:End
ECHO. %RedirLog%
EXIT /b %ExitCode%
ENDLOCAL
