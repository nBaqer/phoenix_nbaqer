@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
SETLOCAL
GOTO Start

:Usage
ECHO.
ECHO DeployService:
ECHO   Deploys a TopShelf Windows service
ECHO.
ECHO Usage:
ECHO   DeployService SourceFolder TargetFolder ServiceExe [ConfigEnv] 
ECHO     [TargetServer] [ServiceUser] [ServicePass] [ServiceName] [Description]
ECHO     [LogFile] [AutoStart] [Uninstall]
ECHO
ECHO Arguments:
ECHO   SourceFolder  UNC of service drop folder, usually under 
ECHO                 _PublishedApplications.
ECHO                 Ex: _PublishedApplications\eSolServices.Client.ESP.EOUTWatcher
ECHO   TargetFolder  Path to where the service will be installed.  This must
ECHO                 be a local path on the target server, not a UNC.
ECHO                 Ex: C:\FAME\eSolServices.Client.ESP.EOUTWatcher
ECHO   ServiceExe    File name of servcice executable
ECHO                 Ex: FAME.eSolServices.Client.ESP.EOUTWatcher.exe
ECHO   ConfigEnv     Optional: Environment ID of config files to use
ECHO                 Ex: Dev, QA, Demo, Prod, etc.
ECHO                 Default: none
ECHO   TargetServer  Optional: Name of the target server where the service will
ECHO                 be installed.
ECHO                 Default: none - local server
ECHO   ServiceUser   Optional: User name under which service will run
ECHO                 Default: none - Local Service
ECHO   ServicePass   Optional: Password for ServiceUser
ECHO                 Default: none
ECHO   ServiceName   Optional: Name used in the service registry
ECHO                 Default: Name space of initialization class
ECHO   Description   Optional: Description displayed in Service Manager
ECHO                 Default: none
ECHO   LogFile       Optional: Y/N
ECHO                 Y=Send output to log file
ECHO                 N=Send output to console
ECHO                 Default: N
ECHO   AutoStart     Optional: Start service after installation
ECHO                 Y=Start service
ECHO                 N=Don't start service
ECHO                 Default: Y (start service)
ECHO   Uninstall     Optional: Y/N
ECHO                 Y=Uninstall existing service before installing
ECHO                 N=Do not uninstall first.  Avoids problems with SCM getting
ECHO                   stuck and maintains any custom service configuration.
ECHO                   However, service path, exe, etc. must remain the same.
GOTO End

:Start
SET SourceFolder=%~1
SET TargetFolder=%~2
SET ServiceExe=%~3
SET ConfigEnv=%~4
SET TargetServer=%~5
SET ServiceUser=%~6
SET ServicePass=%~7
SET ServiceName=%~8
SET Description=%~9

SET PubAppsFolder=%~dp0
SET LogFileName=%PubAppsFolder%%~n0.log
SET TmpLogFolder=%~nx2

SHIFT
SET LogFile=%~9
SHIFT
SET AutoStart=%~9
IF "%AutoStart%"=="" SET AutoStart=Y
IF /i "%AutoStart%"=="Y" SET AutoStart=Y
SHIFT
SET Unistall=%~9
IF "%Unistall%"=="" SET Unistall=N
IF /i "%Unistall%"=="Y" SET Unistall=Y

IF "%SourceFolder%"=="?" GOTO Usage
IF "%SourceFolder%"=="/?" GOTO Usage

IF "%SourceFolder%"=="" GOTO Usage
IF "%TargetFolder%"=="" GOTO Usage
REM IF "%ConfigEnv%"=="" GOTO Usage
REM IF "%TargetServer%"=="" GOTO Usage
REM IF "%ServiceName%"=="" GOTO Usage
REM IF "%Description%"=="" GOTO Usage
IF "%LogFile%"=="" SET LogFile=N

SET ExitCode=1

IF NOT EXIST "%SourceFolder%" SET SourceFolder=%PubAppsFolder%%SourceFolder%
IF NOT "%TargetServer%"=="" IF /i "%TargetServer%"=="%COMPUTERNAME%" SET TargetServer=

rem If we are running from TFS Build and RedGate is on a remote server, TFS 
rem won't capture console output from PSExec, so we need to send console to a  
rem file and then TYPE it to the screen
IF NOT "%TargetServer%"=="" IF NOT "%TF_BUILD%"=="" SET TfsRemote=Y

SET PSExec=psexec
IF NOT "%ProgramFiles(x86)%"=="" IF EXIST "%PubAppsFolder%psexec64.exe" SET PSExec=psexec64
SET PSExecSwitches=-u "Internal\TFSBuild" -p TFS.Build -h -accepteula -w C:\Windows\Temp cmd /c

SET InstallSvcBat=%PubAppsFolder%InstallScvCmd.bat
SET InstallSvcLog=%LogFile%
IF "%TargetServer%"=="" (
	SET InstallService="%InstallSvcBat%"
	SET TargetServerDesc=local machine
) ELSE (
	SET InstallService="%PubAppsFolder%%PSExec%" \\%TargetServer%. %PSExecSwitches% "%InstallSvcBat%" 2^>^&1
	IF "%TfsRemote%"=="Y" SET InstallSvcLog=N
	SET TargetServerDesc=%TargetServer%
)
SET InstallSvcCmd=@"%PubAppsFolder%InstallService.bat" "%SourceFolder%" "%TargetFolder%" "%ServiceExe%" "%ConfigEnv%" "%ServiceUser%" "%ServicePass%" "%ServiceName%" "%Description%" "%InstallSvcLog%" "" "%AutoStart%" "%Unistall%"

IF /i NOT %Debug%==ON GOTO SkipDebug
ECHO.
ECHO SourceFolder    %SourceFolder%
ECHO TargetFolder    %TargetFolder%
ECHO ServiceExe      %ServiceExe%
ECHO ConfigEnv       %ConfigEnv%
ECHO TargetServer    %TargetServer%
ECHO ServiceUser     %ServiceUser%
ECHO ServicePass     %ServicePass%
ECHO ServiceName     %ServiceName%
ECHO Description     %Description%
ECHO LogFile         %LogFile%
ECHO AutoStart       %AutoStart%
ECHO.
ECHO LogFileName     %LogFileName%
ECHO.
ECHO InstallService:
ECHO %InstallService%
ECHO.
PAUSE
:SkipDebug

SET ExitCode=1
IF NOT EXIST "%SourceFolder%" GOTO End

SET ExitCode=0
SET RedirLog=
IF /i "%LogFile%"=="Y" SET RedirLog=^>^> "%LogFileName%"
SET RedirCmd=
SET RedirCmdLog=%PubAppsFolder%InstallService.log
IF "%TfsRemote%"=="Y" SET RedirCmd=^> "%RedirCmdLog%"

ECHO. %RedirLog%
ECHO Executing installation batch file on %TargetServerDesc%... %RedirLog%
IF EXIST "%InstallSvcBat%" DEL /Q/F "%InstallSvcBat%"
IF EXIST "%InstallSvcBat%" (SET ExitCode=1 & GOTO Error)

ECHO %InstallSvcCmd% ^%RedirCmd% > "%InstallSvcBat%"
IF NOT EXIST "%InstallSvcBat%" (SET ExitCode=1 & GOTO Error)
IF "%TfsRemote%"=="Y" IF EXIST "%RedirCmdLog%" DEL /Q/F "%RedirCmdLog%"
ECHO %InstallService% %RedirLog%

CALL %InstallService%
SET ExitCode=%ERRORLEVEL%
IF "%TfsRemote%"=="Y" IF EXIST "%RedirCmdLog%" TYPE "%RedirCmdLog%"
IF NOT "%ExitCode%"=="0" GOTO Error

ECHO Installation batch file completed successfully %RedirLog%
ECHO. %RedirLog%
GOTO End

:Error
ECHO Terminating with ExitCode %ExitCode% %RedirLog%
ECHO. %RedirLog%

:End

ECHO. %RedirLog%
EXIT /b %ExitCode%
ENDLOCAL
