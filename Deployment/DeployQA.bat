@ECHO OFF

SET Branch=%~1
SET BranchClean=%Branch:.= %
IF "%Branch%"=="" GOTO :BadBranch

SET WebRootDir=\\vm-qaadvws1\inetpub\wwwroot\Advantage\QA\%Branch%
SET ConfigEnv=QA
SET SqlServer=Dev2\Adv
SET TargetDbs=IDTI%BranchClean%,AMC%BranchClean%,CHAT%BranchClean%
SET ReportServer=http://dev2/ReportServer_ADV
SET ReportFolder=/Advantage/Dev/%Branch%

SET ExitCode=0
CALL "%~dp0Deploy.bat"
GOTO :End

:BadBranch
SET ExitCode=1
ECHO Must specify branch name agument
GOTO :End

:End
EXIT /B %ExitCode%


REM Deploy.bat "\\vm-tfs1\Builds\TeamCity\Advantage\QA\develop\3.10.0.76" "\\vm-qaadvws1\inetpub\wwwroot\Advantage\QA\3.10.0" QA develop "Dev2\ADV" "CHAT310" "http://dev2/ReportServer_ADV" /Advantage/Dev/3.10.0

REM Deploy.bat "\\vm-tfs1\Builds\TeamCity\Advantage\QA\develop\3.10.0.76" "\\vm-qaadvws1\inetpub\wwwroot\Advantage\QA\3.10.0" QA develop "Dev2\ADV" "CHAT310,MAU310,IDTI310,HMC310" "http://dev2/ReportServer_ADV" /Advantage/Dev/3.10.0 N