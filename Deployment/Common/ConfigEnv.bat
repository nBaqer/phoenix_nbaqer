@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
SETLOCAL
GOTO Start

:Usage
ECHO ------------------------------------------------------
ECHO -                      U S A G E                     -
ECHO ------------------------------------------------------
ECHO.
ECHO ConfigEnv:
ECHO   Updates config files for sepcified environment
ECHO.
ECHO Usage:
ECHO   ConfigEnv AppRoot ConfigEnv [LogFile] [LogFileName]
ECHO.
ECHO Arguments:
ECHO   AppRoot       Path to the deployed app root folder
ECHO                 Note: the configs are expected to be in one of the
ECHO                 following folders under the app root:
ECHO                 \conf
ECHO                 \Configs
ECHO                 \App_Data\conf
ECHO                 \App_Data\Configs
ECHO   ConfigEnv     Environment ID of config files to use
ECHO                 Ex: Dev, QA, Demo, Prod, etc.
ECHO   LogFile       Optional: Y/N
ECHO                 Y=Send output to log file
ECHO                 N=Send output to console
ECHO                 Default: N
ECHO   LogFileName   Optional: Fully qualified path and name of the log file
ECHO                 Default: Same as this bach file, but with .log extension
GOTO End

:Start
SET AppRoot=%~1
SET ConfigEnv=%~2
SET LogFile=%~3
SET LogFileName=%~4

IF "%AppRoot%"=="?" GOTO Usage
IF "%AppRoot%"=="/?" GOTO Usage
IF "%AppRoot%"=="" GOTO Usage
IF "%ConfigEnv%"=="" GOTO Usage

IF NOT "%AppRoot:~-1%"=="\" SET AppRoot=%AppRoot%\

IF "%LogFile%"=="" SET LogFile=N
IF "%LogFileName%"=="" SET LogFileName=%~dp0%~n0.log
SET RedirLog=
IF /i "%LogFile%"=="Y" SET RedirLog=^>^> "%LogFileName%"

SET Configure=N
IF "%ConfigEnv%"=="" GOTO SkipConfig
IF EXIST "%AppRoot%conf\%ConfigEnv%" SET ConfDir=%AppRoot%conf
IF EXIST "%AppRoot%Configs\%ConfigEnv%" SET ConfDir=%AppRoot%Configs
IF EXIST "%AppRoot%App_Data\conf\%ConfigEnv%" SET ConfDir=%AppRoot%App_Data\conf
IF EXIST "%AppRoot%App_Data\Configs\%ConfigEnv%" SET ConfDir=%AppRoot%App_Data\Configs
IF NOT "%ConfDir%"=="" SET Configure=Y
IF NOT %Configure%==Y GOTO SkipConfig
SET ConfCmd1=ATTRIB -r "%ConfDir%\*.*"
SET ConfCmd2=COPY /y "%ConfDir%\%ConfigEnv%\*.*" "%ConfDir%"
:SkipConfig

IF /i NOT %Debug%==ON GOTO SkipDebug
ECHO ------------------------------------------------------
ECHO -                     D E B U G                      -
ECHO ------------------------------------------------------
ECHO.
ECHO AppRoot    %AppRoot%
ECHO ConfigEnv  %ConfigEnv%
ECHO.
ECHO ConfCmd:
IF %Configure%==Y ECHO %ConfCmd1%
IF %Configure%==Y ECHO %ConfCmd2%
ECHO.
PAUSE
:SkipDebug

SET ExitCode=0

IF NOT %Configure%==Y GOTO Error
ECHO Setting up config for %ConfigEnv% environment %RedirLog%
%ConfCmd1% %RedirLog%
%ConfCmd2% %RedirLog%
SET ExitCode=%ERRORLEVEL%
IF NOT "%ExitCode%"=="0" GOTO Error
GOTO End

:Error
IF "%ExitCode%"=="" SET ExitCode=1
IF "%ExitCode%"=="0" SET ExitCode=1
ECHO Terminating with ExitCode %ExitCode% %RedirLog%
ECHO. %RedirLog%

:End
EXIT /b %ExitCode%
ENDLOCAL
