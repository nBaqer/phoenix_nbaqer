@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
SETLOCAL
GOTO Start

:Usage
ECHO.
ECHO RestartSvc:
ECHO   Restarts a service and verifies if operation was successful, 
ECHO   with optional retry on failure.
ECHO.
ECHO   NOTE: Expects SvcCtl.bat to be in the same folder as RestartSvc.bat
ECHO.
ECHO Usage:
ECHO   RestartSvc ServiceName [ServerName] [MaxAttempts] [AttemptWait]
ECHO.
ECHO Arguments:
ECHO   ServiceName   Service Name
ECHO   ServerName    Optional: Server name
ECHO                 Default: "" (local server)
ECHO   MaxAttempts   Optional: Number of times to attempt the operation
ECHO                 Default: 3
ECHO   AttemptWait   Optional: Number of seconds to wait between attempts
ECHO                 Default: 10
ECHO.
ECHO ExitCode:       0=Success, otherwise failure
ECHO.
GOTO End

:Start
SET ServiceName=%~1
SET ServerName=%~2
SET MaxAttempts=%~3
SET AttemptWait=%~4

SET ExitCode=1

IF "%ServiceName%"=="?" GOTO Usage
IF "%ServiceName%"=="/?" GOTO Usage
IF "%ServiceName%"=="" GOTO Usage

SET ExitCode=0

SET SvcCtl="%~dp0SvcCtl.bat"

:Stop
CALL %SvcCtl% "%ServiceName%" stop %MaxAttempts% %AttemptWait%
SET ExitCode=%ERRORLEVEL%
IF NOT "%ExitCode%"=="0" GOTO Error

:Start
CALL %SvcCtl% "%ServiceName%" start %MaxAttempts% %AttemptWait%
SET ExitCode=%ERRORLEVEL%
IF NOT "%ExitCode%"=="0" GOTO Error

:Success
ECHO RestartSvc successful
GOTO End

:Error
IF "%ExitCode%"=="" SET ExitCode=1
IF "%ExitCode%"=="0" SET ExitCode=1
ECHO RestartSvc terminating with ExitCode %ExitCode%

:End
EXIT /b %ExitCode%
ENDLOCAL