@ECHO OFF
SETLOCAL

GOTO :Start

:Usage
ECHO.
ECHO CleanDropDirs:
ECHO   Deletes old drop directories, keeping the specified number of 
ECHO   directories.  Will only consider directories where the name contains 
ECHO   only numbers and dots (ex: ##.##.##.####)
ECHO.
ECHO Usage:
ECHO   CleanDropDirs BuildRoot NumToKeep
ECHO.
ECHO Arguments:
ECHO   BuildRoot  Parent folder of build number drop folders 
ECHO              Ex: \\vm-tfs1\Builds\Internal\Fame.InternalServices.Bus - Dev
ECHO   NumToKeep  Number of build folders to keep.  Sub folders of BuildRoot
ECHO              will be sorted descending by name.  Top NumToKeep will be 
ECHO              kept and all other build folders will be delted.
GOTO End
 
:Start
SET BuildRoot=%~1
IF "%BuildRoot%"=="" GOTO :Usage
IF "%BuildRoot%"=="?" GOTO :Usage
IF "%BuildRoot%"=="/?" GOTO :Usage
IF NOT EXIST "%BuildRoot%" GOTO :Usage

SET /A NumToKeep=%~2 || SET NumToKeep=
IF NOT "%NumToKeep%"=="%~2" GOTO :Usage

SET Kept=0

PUSHD "%BuildRoot%" && (FOR /f "delims=" %%a IN ('DIR "*.*" /B /O:-N /A:D') DO CALL :DelBuildDir "%%a") & POPD

EXIT /B 0
ENDLOCAL

:DelBuildDir
SET DelBuildDir=%~1
SET ChkNum=%DelBuildDir:.=%
SET /A Number=%ChkNum% || SET Number=
IF NOT "%Number%"=="%ChkNum%" EXIT /B 0
IF %Kept% LSS %NumToKeep% (SET /A Kept=%Kept%+1 & EXIT  /B 0)
IF %Number%==%ChkNum% IF %ChkNum% GTR 0 RD /s /q "%DelBuildDir%"
EXIT /B 0

:End
