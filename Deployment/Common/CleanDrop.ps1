<#
.SYNOPSIS
    Cleans the drop folder of unneded files and folders.

.DESCRIPTION
    MSBuild dumps all binaries and output for a solution into the agent's bin 
    folder, which then gets copied into the drop folder.  This creates a mess 
    of unneeded files in the drop folder.  Use this script to clean up this 
    mess.

    All files in the drop root will be deleted.  By default, all folders other 
    than the standard _Published* will be deleted.  Use the Keep or 
    Delete options to sepcify which folders under the drop root will be kept 
    or deleted.

    Note: the TFS Logs folder will always be kept, regardless of Keep or
    Delete options.

.PARAMETER Keep
    The directory names to keep separated by a comma. All other directories 
	except logs will be deleted. Defaults to the standard _Published* folders.

.PARAMETER Delete
    The directory names to delete separated by a comma. All other directories 
	will be kept.

.PARAMETER Test
    Use with -Verbose to see which folders would be deleted in the build 
	diagnostics log without actually deleteing them.

.PARAMETER Disable
    Specifies whether the script is disabled and will take no action.
#>
[CmdletBinding(DefaultParameterSetName = 'Keep')]
param(
    [Parameter(ParameterSetName = 'Keep')]
    [string] $Keep = "_PublishedApplications,_PublishedDatabases,_PublishedReports,_PublishedScripts,_PublishedWebsites",
    [Parameter(ParameterSetName = 'Delete')]
    [string] $Delete,
    [switch] $Test,
    [switch] $Disable
)

#-----------------------------------------------------------------------------
# GLOBAL VARIABLES

$script:DeleteDirectories = @()

#-----------------------------------------------------------------------------
# INTERNAL FUNCTIONS

# Get directories to delete based on specified directory names to keep.
function ProcessKeepDirectories
{
    param(
        [string] $Path,
        [string[]] $Keeps
    )

    if ($Keeps -icontains (Split-Path $Path -Leaf))
    {
        # current directory included
        return $true
    }

    # check for child directory included
    $keepChild = $false
    $deleteChildren = @()
    Get-ChildItem $Path -Directory | foreach {
        if (-not (ProcessKeepDirectories -Path $_.FullName -Keeps $Keeps))
        {
            $deleteChildren += $_.FullName
        }
        else
        {
            $keepChild = $true
        }
    }

    if ($keepChild)
    {
        # Don't delete children in the keep list
        $deleteChildren | foreach { $script:DeleteDirectories += $_ }
    }

    return $keepChild
}

# Get directories to delete based on specified directory names to exclude.
function ProcessDeleteDirectories
{
    param(
        [string] $Path,
        [string[]] $Deletes
    )

    if ((-not $Path.EndsWith("logs")) -and ($Deletes -icontains (Split-Path $Path -Leaf)))
    {
        # current directory exclude
        $script:DeleteDirectories += $Path

        return
    }

    # process child directories
    Get-ChildItem $Path -Directory | foreach {
        ProcessDeleteDirectories -Path $_.FullName -Deletes $Deletes
    }
}

#-----------------------------------------------------------------------------
# MAIN

if ($Disable)
{
    Write-Verbose "Script disabled; no action will be taken."
    exit 0
}

# if the script is not running on a build server, remind user to set environment variables
if (-not $env:TF_BUILD -or -not $env:TF_BUILD_DROPLOCATION)
{
    Write-Error 'You must set the following environement variables to test this script interactively: $env:TF_BUILD, $env:TF_BUILD_DROPLOCATION.'
    exit 1
}

$DropFolder = $env:TF_BUILD_DROPLOCATION

# make sure path to drop folder is available
if (-not (Test-Path $DropFolder))
{
    Write-Error "Directory '${$DropFolder}' doesn't exists."
    exit 1
}

# find excluded directories and included web site directories
switch ($PSCmdlet.ParameterSetName)
{
    'Keep' {
        $keeps = $Keep.Split(',')
        $keeps += 'logs'

        ProcessKeepDirectories -Path $DropFolder -Keeps $keeps | Out-Null
    }
    'Delete' {
        ProcessDeleteDirectories -Path $DropFolder -Deletes ($Delete.Split(',')) | Out-Null
    }
}

Write-Verbose "Files to delete:"
get-item -Path ($DropFolder + "\*.*") | foreach { Write-Verbose $_ }

Write-Verbose "Folders to delete:"
$DeleteDirectories | foreach { Write-Verbose $_ }

if (-not $Test.IsPresent)
{
    Remove-Item -Path ($DropFolder + "\*.*") -Force

    if ($DeleteDirectories.Length -gt 0)
    {
	    $DeleteDirectories | Remove-Item -Recurse -Force

        Write-Verbose "Drop folder cleaned."
    }
}
else
{
    Write-Verbose "Test mode.  No files or folders were deleted."
}

exit 0

