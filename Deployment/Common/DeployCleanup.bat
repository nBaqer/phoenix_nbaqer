@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
SETLOCAL ENABLEDELAYEDEXPANSION
GOTO Start

:Usage
ECHO ------------------------------------------------------
ECHO -                      U S A G E                     -
ECHO ------------------------------------------------------
ECHO.
ECHO Cleanup:
ECHO   Cleans up a folder, removing files with the extensions 
ECHO   matching the FileExtensions parameter.
ECHO.
ECHO Usage:
ECHO   Cleanup Folder FileSpecs [LogFile]
ECHO.
ECHO Arguments:
ECHO   Folder     UNC or path of the target folder where  
ECHO                 files/folders will be deleted from.
ECHO                Ex: \\DEV2\Online System\Dev\
ECHO                Ex: C:\Online System\
ECHO   FileSpecs  Space delimited list of file names and/or 
ECHO                 folders to be deleted.
ECHO                Ex: "FA.Application *.Manifest *.Pdb Application\Logs"
ECHO                Note (files): Wild card operator (*) can be used
ECHO                Note (folders): RD is perfomerd only on current folder.
ECHO                 For sub-folders, pass the folder structured (bottom to top)
ECHO                 as separate items in the FileSpecs argument. To remove the 
ECHO                 "Test" folder tree and its files "Temp\Test\Suite\Failed" set
ECHO                 set FileSpecs to 
ECHO                 "Temp\Test\Suite\Failed" "Temp\Test\Suite" "Temp\Test"
ECHO   LogFile    Optional: Y/N
ECHO                Y=Send output to log file
ECHO                N=Send output to console
ECHO                Default: N
GOTO End

:Start
SET Folder=%~1
SET FileSpecs=%~2
SET LogFile=%~3

IF "%Folder%"=="?" GOTO Usage
IF "%Folder%"=="/?" GOTO Usage
IF "%Folder%"=="" GOTO Usage
IF "%FileSpecs%"=="" GOTO Usage

REM If there is no back slash on the path, added it to it
IF /i NOT "%Folder:~-1%"=="\" SET Folder=%Folder%\

REM Configure Debug Screen
IF /i NOT %Debug%==ON GOTO SkipDebug
ECHO ------------------------------------------------------
ECHO -                     D E B U G                      -
ECHO ------------------------------------------------------
ECHO.
ECHO Folder         %Folder%
ECHO FileSpecs      %FileSpecs%
ECHO LogFile        %LogFile%
ECHO.
ECHO LogFileName    %LogFileName%
ECHO.
ECHO.
PAUSE
:SkipDebug

REM Configure local variables
SET ExitCode=0
SET RedirLog=
SET LogFileName=%Folder%%~n0.log
IF /i "%LogFile%"=="Y" SET RedirLog=^>^> "%LogFileName%"

REM Perform clean-up
ECHO Performing clean-up on %Folder% %RedirLog%
ECHO with the following File Specs: %RedirLog%
ECHO %FileSpecs%. %RedirLog%
ECHO.

SET Remaining=%FileSpecs%
:Loop
FOR /F "tokens=1*" %%a IN ("%Remaining%") DO (

	ECHO Processing %%a  %RedirLog%
	SET Remaining=%%b
	
	REM check whether the file spec is a file or folder
	IF EXIST "%Folder%%%a\*" (
	
		REM -- folder
		DEL "%Folder%%%a\*.*" /f/q
		RD "%Folder%%%a" /q
		
	) ELSE (
	
		REM -- files
		IF EXIST "%Folder%%%a" ( 
			DEL "%Folder%%%a" /f/q 
		)ELSE (
			ECHO %%a does not exist.
		)
		
		
	)
	
	SET ExitCode=%ERRORLEVEL%
	IF NOT "%ExitCode%"=="0" GOTO Error

)
IF DEFINED Remaining GOTO Loop

GOTO End

:Error
ECHO Terminating with ExitCode %ExitCode% %RedirLog%
ECHO. %RedirLog%

:End
EXIT /b %ExitCode%
ENDLOCAL
