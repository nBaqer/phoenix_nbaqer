<#
.SYNOPSIS
	Runs build scripts from TFS Build

.DESCRIPTION
	Intended to be used via TFS pre/post build/test script hooks to run a
	specified build script.  Manages interaction with TFS Build and provides
	helpful control features, which avoids the need for repeated TFS Build 
	related code in the actual build scripts.

.PARAMETER Script
	Build script to execute.
	
	The script could be anything that can be executed via PowerShell's call 
	operator (&).  To indicate a failure to BuildScriptHost, the script 
	should thow an error or return a non-0 exit code.
	
	When the script is executed byt TFS Build, the current working folder is 
	the sources root folder in the agent's workspace, as defined in the build
	definition source settings.  So you can specify a fully qualified path 
	or a path relative to the agent's sources folder.  To specify a path 
	relative the the drop folder, include the DropScript switch (see below).

.PARAMETER ScriptArgs
	Arguments to be passed to script.

.PARAMETER DropScript
	By default, the script path is assumed to be relative to the sources 
	folder.  Specify DropScript to use a path relative to the drop folder.
	Typically, pre-build/test scripts will run from the agent's workspace,
	while post-test deployment scripts typicaly run from the drop folder.

.PARAMETER ContinueOnCompFailue
	By default, the script will not be executed if build compilation failed.

.PARAMETER ContinueOnTestFailue
	By default, the script will not be executed if build tests failed.

.PARAMETER ContinueBuildOnFailue
	By default, the build will fail if the script fails (thows an error or 
	returns a non-0 exit code).

.PARAMETER Disable
	Disable execution of the script.  Useful to tempararily disable script 
	execution in the build definition without deleteing script process 
	arguments.

.PARAMETER Verbose
	Includes additional information in the build diagnostic log, such as: 
	BuildScriptHost parameter values, all the TFS_BUILD environment variable
	values and a few other useful items.
	
.EXAMPLE
	BuildScriptHost.ps1 'DeployQA.ps1'
	Runs DeployQA.ps1.  DeployQA.ps1 is assumed to be in the same folder as
	BuildScriptHost.ps1.  The build will fail if DeployQA.ps1 fails.

.EXAMPLE
	BuildScriptHost.ps1 'DeployQA.ps1' -ContinueOnTestFailue -Verbose
	Runs DeployQA.ps1.  DeployQA.ps1 will be executed even if build tests 
	failed.  Additional info will appear in the build diagnostic log.
	
.EXAMPLE
	BuildScriptHost.ps1 'DeployQA.ps1' -ContinueOnTestFailue -Verbose -Disable
	DeployQA.ps1 will not be run and BuildScriptHost will return success
	to TFS build.
	
.NOTES
	You can use %EnvVar% tokens in the Script and ScriptArgs arguments, where
	EnvVar is any environment variable name.  These tokens will be replaced
	with the value of the environment variable.  This is not limited to the 
	TF_BUILD environment variables mentioned below, any environment variable 
	can be used.
	
	The following environment variables are available when this script is 
	run	from TFS Build.  If not running from TFS Build remember to set any 
	environment variables required by your scripts prior to calling this 
	script.
	
	TF_BUILD
	TF_BUILD_BINARIESDIRECTORY
	TF_BUILD_BUILDDEFINITIONNAME
	TF_BUILD_BUILDDIRECTORY
	TF_BUILD_BUILDNUMBER
	TF_BUILD_BUILDREASON
	TF_BUILD_BUILDURI
	TF_BUILD_COLLECTIONURI - TFS BUG: this one is never populated - see below
	TF_BUILD_DROPLOCATION
	TF_BUILD_SOURCEGETVERSION
	TF_BUILD_SOURCESDIRECTORY
	TF_BUILD_TESTRESULTSDIRECTORY

	See http://msdn.microsoft.com/en-us/library/vstudio/hh850448.aspx for details
	
	Also, this script will create additional environment variables not 
	natively provided by TFS Build:

	TF_BUILD_COLLECTIONURI - hard coded to http://vm-tfs1:8080/tfs/development
	TF_BUILD_COMPILATIONSTATUS
	TF_BUILD_TESTSTATUS

	Valid status values are: Succeeded, Failed or Unknown
#>

[CmdletBinding()]
param(
	[Parameter(Mandatory=$True,Position=0)]
	[string] $Script,
	[Parameter(Position=1)]
	[string] $ScriptArgs,
	[switch] $DropScript = $False,
	[switch] $ContinueOnCompFailue = $False,
	[switch] $ContinueOnTestFailue = $False,
	[switch] $ContinueBuildOnFailue = $False,
	[switch] $Disable = $False
)


#-----------------------------------------------------------------------------
# INTERNAL FUNCTIONS
#-----------------------------------------------------------------------------
function Get-BuildDetail
{
	[CmdletBinding()]
	param(
		[Parameter(Mandatory=$True,Position=1)] [string] $CollectionUrl,
		[Parameter(Mandatory=$True,Position=2)] [string] $BuildUri
	)

	[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.TeamFoundation.Client") 
	[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.TeamFoundation.Build.Client") 

	$tfs = [Microsoft.TeamFoundation.Client.TeamFoundationServerFactory]::GetServer($CollectionUrl)
	$tfsBuild = $tfs.GetService([Microsoft.TeamFoundation.Build.Client.IBuildServer])
	return $tfsBuild.GetBuild($BuildUri)
}


function Set-TfsEnvVars
{
	#Get-ChildItem Env:

	Write-Verbose '----------------------------------------------'
	Write-Verbose "TF_BUILD environment variables created by TFS:"
	Write-Verbose '----------------------------------------------'
	Write-Verbose "TF_BUILD: $env:TF_BUILD"
	Write-Verbose "TF_BUILD_BINARIESDIRECTORY: $env:TF_BUILD_BINARIESDIRECTORY"
	Write-Verbose "TF_BUILD_BUILDDEFINITIONNAME: $env:TF_BUILD_BUILDDEFINITIONNAME"
	Write-Verbose "TF_BUILD_BUILDDIRECTORY: $env:TF_BUILD_BUILDDIRECTORY"
	Write-Verbose "TF_BUILD_BUILDNUMBER: $env:TF_BUILD_BUILDNUMBER"
	Write-Verbose "TF_BUILD_BUILDREASON: $env:TF_BUILD_BUILDREASON"
	Write-Verbose "TF_BUILD_BUILDURI: $env:TF_BUILD_BUILDURI"
	Write-Verbose "TF_BUILD_COLLECTIONURI: $env:TF_BUILD_COLLECTIONURI"
	Write-Verbose "TF_BUILD_DROPLOCATION: $env:TF_BUILD_DROPLOCATION"
	Write-Verbose "TF_BUILD_SOURCEGETVERSION: $env:TF_BUILD_SOURCEGETVERSION"
	Write-Verbose "TF_BUILD_SOURCESDIRECTORY: $env:TF_BUILD_SOURCESDIRECTORY"
	Write-Verbose "TF_BUILD_TESTRESULTSDIRECTORY: $env:TF_BUILD_TESTRESULTSDIRECTORY"

	#Create build status environment variables that MS forgot to include
	$BuildDetail = Get-BuildDetail $env:TF_BUILD_COLLECTIONURI $env:TF_BUILD_BUILDURI
	$env:TF_BUILD_COMPILATIONSTATUS = $BuildDetail.CompilationStatus
	$env:TF_BUILD_TESTSTATUS = $BuildDetail.TestStatus

	Write-Verbose '----------------------------------------------------------'
	Write-Verbose "TF_BUILD environment variables created by BuildScriptHost:"
	Write-Verbose '----------------------------------------------------------'
	Write-Verbose "TF_BUILD_COLLECTIONURI: $env:TF_BUILD_COLLECTIONURI"
	Write-Verbose "TF_BUILD_COMPILATIONSTATUS: $env:TF_BUILD_COMPILATIONSTATUS"
	Write-Verbose "TF_BUILD_TESTSTATUS: $env:TF_BUILD_TESTSTATUS"
}


function Exit-Script
{
	[CmdletBinding()]
	param (
		[string]$Message = '',
		[switch]$Error = $False
	)

	If ($Error)
	{
		If ($Message -eq '')
			{ $Message = 'An unhandled error occured' }

		If ($ContinueBuildOnFailue)
		{
			throw $Message
		}
		else 
		{
			Write-Error $Message
			exit 1
		}
	}
	else 
	{
		If ($Message -ne '')
			{ Write-Host $Message }
		exit 0
	}
}


#-----------------------------------------------------------------------------
# MAIN
#-----------------------------------------------------------------------------
Try
{
	$CurDir = (Get-Item -Path ".\" -Verbose).FullName

	Write-Verbose '--------------------------'
	Write-Verbose 'BuildScriptHost Arguments'
	Write-Verbose '--------------------------'
	Write-Verbose "Script: $Script"
	Write-Verbose "ScriptArgs: $ScriptArgs"
	Write-Verbose "DropScript: $DropScript"
	Write-Verbose "ContinueOnCompFailue: $ContinueOnCompFailue"
	Write-Verbose "ContinueOnTestFailue: $ContinueOnTestFailue"
	Write-Verbose "ContinueBuildOnFailue: $ContinueBuildOnFailue"
	Write-Verbose "Disable: $Disable"

	if ($Disable)
		{ Exit-Script 'Script disabled - skipping all script actions' }

	if (-not $env:TF_BUILD_COLLECTIONURI)
		{ $env:TF_BUILD_COLLECTIONURI = 'http://vm-tfs1:8080/tfs/development' }

	if (-not ($env:TF_BUILD_COLLECTIONURI -and $env:TF_BUILD_BUILDURI))
		{ Exit-Script 'TF_BUILD_COLLECTIONURI and TF_BUILD_BUILDURI environment variables must be set' -Error }

	Set-TfsEnvVars

	if (-not $ContinueOnCompFailue -and $env:TF_BUILD_COMPILATIONSTATUS -eq 'Failed')
		{ Exit-Script 'Compilation failed - skipping all script actions' }

	if (-not $ContinueOnTestFailue -and $env:TF_BUILD_TESTSTATUS -eq 'Failed')
		{ Exit-Script 'Tests failed - skipping all script actions' }

	if ($DropScript)
		{ $Script = Join-Path -path $env:TF_BUILD_DROPLOCATION -ChildPath $Script }

	if ($ScriptArgs)
	{
		foreach ($EnvVar in (get-childitem -path env:*))
			{ $ScriptArgs = $ScriptArgs -replace ('%' + $EnvVar.Name + '%'), $EnvVar.Value }	
	}
		
	Write-Verbose '--------------------------'
	Write-Verbose ' Misc'
	Write-Verbose '--------------------------'
	Write-Verbose "CurDir: $CurDir"
	Write-Verbose "PSScriptRoot: $PSScriptRoot"
	Write-Verbose ('Execution Policy: ' + (Get-ExecutionPolicy))
	Write-Verbose ('Script file exists: ' + (Test-Path $Script))
	if ($DropScript)
		{ Write-Verbose ('Drop folder exists: ' + (Test-Path $env:TF_BUILD_DROPLOCATION)) }
	
	if ($ScriptArgs)
	{
		#$Command = '& $Script ' + $ScriptArgs
		#Write-Host "Invoking $Command"
		#Invoke-Expression $Command

		Write-Host "Calling $Script $ScriptArgs"
		& $Script $ScriptArgs

	}
	else
	{ 
		Write-Host "Calling $Script"
		& $Script
	}
	
	if ($LastExitCode -ne 0)
		{ Exit-Script "Script returned exit code: $LastExitCode" -Error }
	else
		{ Exit-Script 'Script completed successfully' }
}
Catch [Exception]
{
	Write-Error $_.Exception|format-list -force
	Exit-Script -Error
}
