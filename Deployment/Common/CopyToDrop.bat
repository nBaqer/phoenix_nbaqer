@ECHO OFF
IF NOT "%Debug%"=="ON" SET Debug=OFF
SETLOCAL
GOTO Start

:Usage
ECHO.
ECHO CopyToDrop:
ECHO   Copies items from the build agent's working folders to the drop folder
ECHO.
ECHO   Depends on the following TFS build environment variables being set 
ECHO   prior to calling:
ECHO      TF_BUILD_SOURCESDIRECTORY
ECHO      TF_BUILD_DROPLOCATION
ECHO.
ECHO Usage:
ECHO   CopyToDrop Source [Destination]
ECHO.
ECHO Arguments:
ECHO   Source        Source folder, relative to agent's sources root folder.
ECHO                 May include wild cards.
ECHO   Destination   Target folder under the drop folder.  Leave blank to 
ECHO                 copy directly into the drop root folder.
ECHO.
ECHO ExitCode:       0=Success, otherwise failure
ECHO.
GOTO End

:Start
SET Source=%~1
SET Destination=%~2

SET ExitCode=1

IF "%Source%"=="?" GOTO Usage
IF "%Source%"=="/?" GOTO Usage

IF "%Source%"=="" GOTO Usage

IF "%Destination%"=="" (
	SET Destination=%TF_BUILD_DROPLOCATION%
) ELSE (
	SET Destination=%TF_BUILD_DROPLOCATION%\%Destination%
)

XCOPY "%TF_BUILD_SOURCESDIRECTORY%\%Source%" "%Destination%" /S/E/V/I/Q/H/R/Y
SET ExitCode=%ERRORLEVEL%
IF NOT "%ExitCode%"=="0" GOTO Error

:Success
ECHO CopyToDrop successful
GOTO End

:Error
IF "%ExitCode%"=="" SET ExitCode=1
IF "%ExitCode%"=="0" SET ExitCode=1
ECHO CopyToDrop terminating with ExitCode %ExitCode%

:End
EXIT /b %ExitCode%
ENDLOCAL