@ECHO OFF & cls

SETLOCAL 
GOTO Start

:Usage
ECHO.
ECHO Automatically Generated Installer Tool:
ECHO 	This script generates the synchronize schema changes script automatically as part of the nightly build
ECHO.
ECHO Usage
ECHO 	SourceControlPath TargetServer TargetDatabase Username Password DropLocation WriteLog
ECHO .
ECHO Arguments:
ECHO 	SourceControlPath
ECHO 		The path for the new version.
ECHO 	TargetServer
ECHO 		The target server
ECHO 	TargetDatabase
ECHO 		The targeted database on the target server
ECHO 	Username
ECHO 		The username for the server and database
ECHO 	Password
ECHO 		The password username for the server and database
ECHO 	DropLocation
ECHO 		The drop folder is the location path where all the necessary files are located
ECHO 	WriteLog (Optional Y/N)
ECHO 		Y=Write log to file
ECHO 		N=Write log to console.
GOTO End



:start
SET SourceControlPath=%~1
SET TargetServer=%~2
SET TargetDatabase=%~3
SET Username=%~4
SET Password=%~5
SET DropLocation=%~6
SET WriteLog=%~7

IF "%SourceControlPath%"=="?" GOTO :Usage
IF "%TargetServer%"=="" GOTO :Usage
IF "%TargetDatabase%"=="" GOTO :Usage
IF "%Username%"=="" GOTO :Usage
IF "%Password%"=="" GOTO :Usage
IF "%DropLocation%"=="" GOTO :Usage
IF "%WriteLog%"=="" SET LogFile=N


ECHO %WriteLog%
SET SqlUserArg=/UserName2:%Username% /Password2:%Password%

IF "%SqlCompareExe%"=="" SET SqlCompareExe=C:\Program Files\Red Gate\SQL Compare 13\sqlcompare.exe
IF NOT EXIST "%SqlCompareExe%" SET SqlCompareExe=C:\Program Files (x86)\Red Gate\SQL Compare 13\sqlcompare.exe
IF NOT EXIST "%SqlCompareExe%" SET SqlCompareExe=C:\Program Files\Red Gate\SQL Compare 12\sqlcompare.exe
IF NOT EXIST "%SqlCompareExe%" SET SqlCompareExe=C:\Program Files (x86)\Red Gate\SQL Compare 12\sqlcompare.exe
IF NOT EXIST "%SqlCompareExe%" SET SqlCompareExe=C:\Program File\Red Gate\SQL Compare 11\sqlcompare.exe
IF NOT EXIST "%SqlCompareExe%" SET SqlCompareExe=C:\Program Files (x86)\Red Gate\SQL Compare 11\sqlcompare.exe
IF NOT EXIST "%SqlCompareExe%" SET SqlCompareExe=C:\Program File\Red Gate\SQL Compare 10\sqlcompare.exe
IF NOT EXIST "%SqlCompareExe%" SET SqlCompareExe=C:\Program Files (x86)\Red Gate\SQL Compare 10\sqlcompare.exe


REM make sure files do not exist
IF EXIST "%DropLocation%_SqlCompare.sql" DEL "%DropLocation%_SqlCompare.sql" 
IF EXIST "%DropLocation%_SqlCompare.htm" DEL "%DropLocation%_SqlCompare.htm" 


SET SqlCompareCmd="%SqlCompareExe%" /Scripts1:"%SourceControlPath%" /Server2:%TargetServer% /Database2:%TargetDatabase% %SqlUserArg% /ScriptFile:"%DropLocation%.sql" /Report:"%DropLocation%.htm" /Options:IgnoreStatistics,IgnoreStatisticsNorecompute,IgnoreUserProperties,IgnoreWhiteSpace
ECHO %SqlCompareCmd%
%SqlCompareCmd%

REM if Write Log set to N delete the files...
IF /i "%WriteLog%"=="N" (
IF EXIST "%DropLocation%_SqlCompare.sql" DEL "%DropLocation%_SqlCompare.sql" 
IF EXIST "%DropLocation%_SqlCompare.htm" DEL "%DropLocation%_SqlCompare.htm" 
)
IF /i "%WriteLog%"=="n" (
IF EXIST "%DropLocation%_SqlCompare.sql" DEL "%DropLocation%_SqlCompare.sql" 
IF EXIST "%DropLocation%_SqlCompare.htm" DEL "%DropLocation%_SqlCompare.htm" 
)


ECHO.
PAUSE
CLS

:End
EXIT /B %ExitCode%