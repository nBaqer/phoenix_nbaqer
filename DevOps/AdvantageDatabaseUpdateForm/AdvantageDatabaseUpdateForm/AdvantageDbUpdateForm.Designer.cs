﻿namespace AdvantageDatabaseUpdateForm
{
    partial class AdvantageDbUpdateForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtServerName = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtTenantDatabaseName = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtAdvantageDatabaseName = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnTestCredentials = new System.Windows.Forms.Button();
            this.txtScriptFolder = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnVerifyCurrentVersion = new System.Windows.Forms.Button();
            this.btnUpdateDatabase = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(68, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(554, 89);
            this.label1.TabIndex = 0;
            this.label1.Text = "WARNING: Only use this application if you are planing to update an advantage data" +
    "base to version 3.9SP3 and the AdvantageDbTool is failing due to missing Assembl" +
    "y dependency";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 115);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "SQL Server Name";
            // 
            // txtServerName
            // 
            this.txtServerName.Location = new System.Drawing.Point(122, 111);
            this.txtServerName.Name = "txtServerName";
            this.txtServerName.Size = new System.Drawing.Size(272, 20);
            this.txtServerName.TabIndex = 2;
            // 
            // txtUserName
            // 
            this.txtUserName.Location = new System.Drawing.Point(122, 147);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(272, 20);
            this.txtUserName.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 151);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 13);
            this.label3.TabIndex = 3;
            this.label3.Text = "SQL User Name";
            // 
            // txtPassword
            // 
            this.txtPassword.Location = new System.Drawing.Point(122, 183);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(272, 20);
            this.txtPassword.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 187);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(102, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "SQL User Password";
            // 
            // txtTenantDatabaseName
            // 
            this.txtTenantDatabaseName.Location = new System.Drawing.Point(158, 222);
            this.txtTenantDatabaseName.Name = "txtTenantDatabaseName";
            this.txtTenantDatabaseName.Size = new System.Drawing.Size(236, 20);
            this.txtTenantDatabaseName.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 226);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(124, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Tenant Database Name:";
            // 
            // txtAdvantageDatabaseName
            // 
            this.txtAdvantageDatabaseName.Location = new System.Drawing.Point(158, 258);
            this.txtAdvantageDatabaseName.Name = "txtAdvantageDatabaseName";
            this.txtAdvantageDatabaseName.Size = new System.Drawing.Size(236, 20);
            this.txtAdvantageDatabaseName.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 262);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(142, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Advantage Database Name:";
            // 
            // btnTestCredentials
            // 
            this.btnTestCredentials.Location = new System.Drawing.Point(416, 182);
            this.btnTestCredentials.Name = "btnTestCredentials";
            this.btnTestCredentials.Size = new System.Drawing.Size(114, 23);
            this.btnTestCredentials.TabIndex = 11;
            this.btnTestCredentials.Text = "Test Credentials";
            this.btnTestCredentials.UseVisualStyleBackColor = true;
            this.btnTestCredentials.Click += new System.EventHandler(this.BtnTestCredentialsClick);
            // 
            // txtScriptFolder
            // 
            this.txtScriptFolder.Location = new System.Drawing.Point(16, 323);
            this.txtScriptFolder.Name = "txtScriptFolder";
            this.txtScriptFolder.Size = new System.Drawing.Size(378, 20);
            this.txtScriptFolder.TabIndex = 13;
            this.txtScriptFolder.Text = "C:\\Program Files (x86)\\FAME Inc\\AdvantageDBTool\\Scripts\\";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 298);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(202, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Adv Db Tool Installation path with scripts:";
            // 
            // btnVerifyCurrentVersion
            // 
            this.btnVerifyCurrentVersion.Location = new System.Drawing.Point(16, 368);
            this.btnVerifyCurrentVersion.Name = "btnVerifyCurrentVersion";
            this.btnVerifyCurrentVersion.Size = new System.Drawing.Size(99, 48);
            this.btnVerifyCurrentVersion.TabIndex = 14;
            this.btnVerifyCurrentVersion.Text = "Verify Current Database Version";
            this.btnVerifyCurrentVersion.UseVisualStyleBackColor = true;
            this.btnVerifyCurrentVersion.Click += new System.EventHandler(this.BtnVerifyCurrentVersionClick);
            // 
            // btnUpdateDatabase
            // 
            this.btnUpdateDatabase.Location = new System.Drawing.Point(590, 368);
            this.btnUpdateDatabase.Name = "btnUpdateDatabase";
            this.btnUpdateDatabase.Size = new System.Drawing.Size(99, 48);
            this.btnUpdateDatabase.TabIndex = 15;
            this.btnUpdateDatabase.Text = "Update Database";
            this.btnUpdateDatabase.UseVisualStyleBackColor = true;
            this.btnUpdateDatabase.Click += new System.EventHandler(this.BtnUpdateDatabaseClick);
            // 
            // AdvantageDbUpdateForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(701, 453);
            this.Controls.Add(this.btnUpdateDatabase);
            this.Controls.Add(this.btnVerifyCurrentVersion);
            this.Controls.Add(this.txtScriptFolder);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnTestCredentials);
            this.Controls.Add(this.txtAdvantageDatabaseName);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtTenantDatabaseName);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtPassword);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtUserName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtServerName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.Name = "AdvantageDbUpdateForm";
            this.Text = "Advantage Database Winter IPED Update Tool for version 3.9 SP3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtServerName;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtTenantDatabaseName;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtAdvantageDatabaseName;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnTestCredentials;
        private System.Windows.Forms.TextBox txtScriptFolder;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btnVerifyCurrentVersion;
        private System.Windows.Forms.Button btnUpdateDatabase;
    }
}

