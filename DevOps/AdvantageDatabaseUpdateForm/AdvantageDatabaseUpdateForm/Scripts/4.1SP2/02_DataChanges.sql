﻿-- ===============================================================================================
-- START Consolidated Script Version 4.1SP2
-- Data Changes Zone
-- Please do not deploy your schema changes in this area
-- Please use SQL Prompt format before insert here please
-- Please run after Schema changes....
-- ===============================================================================================
--=================================================================================================
-- START - AD-15204 Storage Configuration page
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION fileStorageSettings;
BEGIN TRY
    DECLARE @fileStorageResourceId INT = 882;

    IF NOT EXISTS (
                  SELECT *
                  FROM   dbo.syFileConfigurationFeatures
                  )
        BEGIN
            INSERT INTO dbo.syFileConfigurationFeatures (
                                                        FeatureId
                                                       ,FeatureCode
                                                       ,FeatureDescription
                                                       ,StatusId
                                                        )
            VALUES ( 1, 'TC'                                -- FeatureCode - varchar(50)
                    ,'Time Clock'                           -- FeatureDescription - varchar(100)
                    ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                )
                  ,( 2, 'LI'                                -- FeatureCode - varchar(50)
                    ,'Lead Import'                          -- FeatureDescription - varchar(100)
                    ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                  )
                  ,( 3, 'DOC'                               -- FeatureCode - varchar(50)
                    ,'Documents'                            -- FeatureDescription - varchar(100)
                    ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                  )
                  ,( 4, 'PHOTO'                             -- FeatureCode - varchar(50)
                    ,'Photos'                               -- FeatureDescription - varchar(100)
                    ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                  )
                  ,( 5, 'R2T4'                              -- FeatureCode - varchar(50)
                    ,'R2T4'                                 -- FeatureDescription - varchar(100)
                    ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                  );
        END;


    IF NOT EXISTS (
                  SELECT *
                  FROM   dbo.syResources
                  WHERE  ResourceID = @fileStorageResourceId
                  )
        BEGIN


            INSERT INTO dbo.syResources (
                                        ResourceID
                                       ,Resource
                                       ,ResourceTypeID
                                       ,ResourceURL
                                       ,SummListId
                                       ,ChildTypeId
                                       ,ModDate
                                       ,ModUser
                                       ,AllowSchlReqFlds
                                       ,MRUTypeId
                                       ,UsedIn
                                       ,TblFldsId
                                       ,DisplayName
                                        )
            VALUES ( @fileStorageResourceId          -- ResourceID - smallint
                    ,'File Storage Configuration'    -- Resource - varchar(200)
                    ,4                               -- ResourceTypeID - tinyint
                    ,'~/SY/FileStorageSettings.aspx' -- ResourceURL - varchar(100)
                    ,NULL                            -- SummListId - smallint
                    ,NULL                            -- ChildTypeId - tinyint
                    ,GETDATE()                       -- ModDate - datetime
                    ,'sa'                            -- ModUser - varchar(50)
                    ,0                               -- AllowSchlReqFlds - bit
                    ,1                               -- MRUTypeId - smallint
                    ,207                             -- UsedIn - int
                    ,NULL                            -- TblFldsId - int
                    ,'File Storage Configuration'    -- DisplayName - varchar(200)
                );
        END;

    DECLARE @systemGeneralOptionsMenu INT = (
                                            SELECT TOP 1 MenuItemId
                                            FROM   dbo.syMenuItems
                                            WHERE  MenuName = 'General Options'
                                                   AND DisplayName = 'General Options'
                                                   AND ResourceId = 707
                                            );
    IF NOT EXISTS (
                  SELECT *
                  FROM   dbo.syMenuItems
                  WHERE  ResourceId = @fileStorageResourceId
                  )
        BEGIN


            INSERT INTO dbo.syMenuItems (
                                        MenuName
                                       ,DisplayName
                                       ,Url
                                       ,MenuItemTypeId
                                       ,ParentId
                                       ,DisplayOrder
                                       ,IsPopup
                                       ,ModDate
                                       ,ModUser
                                       ,IsActive
                                       ,ResourceId
                                       ,HierarchyId
                                       ,ModuleCode
                                       ,MRUType
                                       ,HideStatusBar
                                        )
            VALUES ( 'File Storage Configuration'   -- MenuName - varchar(250)
                    ,'File Storage Configuration'   -- DisplayName - varchar(250)
                    ,'/SY/FileStorageSettings.aspx' -- Url - nvarchar(250)
                    ,4                              -- MenuItemTypeId - smallint
                    ,@systemGeneralOptionsMenu      -- ParentId - int
                    ,170                            -- DisplayOrder - int
                    ,0                              -- IsPopup - bit
                    ,GETDATE()                      -- ModDate - datetime
                    ,'sa'                           -- ModUser - varchar(50)
                    ,1                              -- IsActive - bit
                    ,@fileStorageResourceId         -- ResourceId - smallint
                    ,NEWID()                        -- HierarchyId - uniqueidentifier
                    ,NULL                           -- ModuleCode - varchar(5)
                    ,NULL                           -- MRUType - int
                    ,NULL                           -- HideStatusBar - bit
                );

        END;

    IF NOT EXISTS (
                  SELECT *
                  FROM   dbo.syNavigationNodes
                  WHERE  ResourceId = @fileStorageResourceId
                  )
        BEGIN
            DECLARE @fileStorageHiearchyId UNIQUEIDENTIFIER = (
                                                              SELECT TOP 1 HierarchyId
                                                              FROM   dbo.syMenuItems
                                                              WHERE  DisplayName = 'File Storage Configuration'
                                                                     AND ResourceId = 882
                                                              );
            DECLARE @parentHiearchyId UNIQUEIDENTIFIER = (
                                                         SELECT TOP 1 HierarchyId
                                                         FROM   dbo.syMenuItems
                                                         WHERE  ResourceId = 707
                                                         );

            INSERT INTO dbo.syNavigationNodes (
                                              HierarchyId
                                             ,HierarchyIndex
                                             ,ResourceId
                                             ,ParentId
                                             ,ModUser
                                             ,ModDate
                                             ,IsPopupWindow
                                             ,IsShipped
                                              )
            VALUES ( @fileStorageHiearchyId -- HierarchyId - uniqueidentifier
                    ,3                      -- HierarchyIndex - smallint
                    ,@fileStorageResourceId -- ResourceId - smallint
                    ,@parentHiearchyId      -- ParentId - uniqueidentifier
                    ,'sa'                   -- ModUser - varchar(50)
                    ,GETDATE()              -- ModDate - datetime
                    ,0                      -- IsPopupWindow - bit
                    ,1                      -- IsShipped - bit
                );

        END;
END TRY
BEGIN CATCH
    SET @error = 1;
    --ROLLBACK TRANSACTION titleIvSap;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION fileStorageSettings;
        PRINT 'Failed to create file storage configuration setup.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION fileStorageSettings;
        PRINT 'Successful setup of file storage configuration.';
    END;
GO
--=================================================================================================
-- END - AD-15204 Storage Configuration page
--=================================================================================================
--=================================================================================================
-- START - AD-16709 script for updating existing records that have termId null in satransactions
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION termTransaction;
BEGIN TRY
    UPDATE     st
    SET        st.TermId = t.TermId
    FROM       dbo.arTerm t
    INNER JOIN dbo.arPrgVersions pv ON pv.PrgVerId = t.ProgramVersionId
    INNER JOIN dbo.arStuEnrollments e ON e.PrgVerId = pv.PrgVerId
    INNER JOIN dbo.saTransactions st ON st.StuEnrollId = e.StuEnrollId
    WHERE      st.TermId IS NULL;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION termTransaction;
        PRINT 'Failed to update termId in satransaction.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION termTransaction;
        PRINT 'Successful supdate of termID on satransaction.';
    END;
GO
--=================================================================================================
-- END - AD-16709 script for updating existing records that have termId null in satransactions
--=================================================================================================

--=================================================================================================
-- START - TECH Create default file storage settings
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION defaultFileStorage;
BEGIN TRY
    DECLARE @Rows INT;
    DECLARE @leadImportPath VARCHAR(200);
    DECLARE @timeClockPath VARCHAR(200);
    DECLARE @documentsPath VARCHAR(200);
    DECLARE @r2t4Path VARCHAR(200);
    DECLARE @campusId UNIQUEIDENTIFIER;
    DECLARE @campusGroupId UNIQUEIDENTIFIER;
    DECLARE @campusFileConfigurationId UNIQUEIDENTIFIER;

    DECLARE @campusesToProcess TABLE
        (
            CampusId UNIQUEIDENTIFIER
        );


    INSERT INTO @campusesToProcess (
                                   CampusId
                                   )
                SELECT CampusId
                FROM   dbo.syCampuses;



    WHILE EXISTS (
                 SELECT *
                 FROM   @campusesToProcess
                 )
        BEGIN

            SELECT TOP 1 @campusId = CampusId
            FROM   @campusesToProcess;


            SET @leadImportPath = (
                                  SELECT TOP 1 ILArchivePath
                                  FROM   dbo.syCampuses
                                  WHERE  CampusId = @campusId
                                  );
            SET @timeClockPath = (
                                 SELECT TOP 1 TCTargetPath
                                 FROM   dbo.syCampuses
                                 WHERE  CampusId = @campusId
                                 );
            SET @documentsPath = (
                                 SELECT     TOP 1 sv.Value
                                 FROM       dbo.syConfigAppSettings aps
                                 INNER JOIN dbo.syConfigAppSetValues sv ON sv.SettingId = aps.SettingId
                                 WHERE      aps.KeyName = 'CreateDocumentPath'
                                 );
            SET @r2t4Path = (
                            SELECT     TOP 1 sv.Value
                            FROM       dbo.syConfigAppSettings aps
                            INNER JOIN dbo.syConfigAppSetValues sv ON sv.SettingId = aps.SettingId
                            WHERE      aps.KeyName = 'R2T4'
                            );
            SET @campusGroupId = (
                                 SELECT     TOP 1 cgc.CampGrpId
                                 FROM       dbo.syCmpGrpCmps cgc
                                 INNER JOIN dbo.syCampGrps cg ON cg.CampGrpId = cgc.CampGrpId
                                 WHERE      cgc.CampusId = @campusId
                                            AND cg.IsAllCampusGrp = 0
                                 );

            IF ( @campusGroupId IS NOT NULL )
                BEGIN
                    IF NOT EXISTS (
                                  SELECT 1
                                  FROM   dbo.syCampusFileConfiguration
                                  WHERE  CampusGroupId = @campusGroupId
                                  )
                        BEGIN
                            INSERT INTO dbo.syCampusFileConfiguration (
                                                                      CampusFileConfigurationId
                                                                     ,CampusGroupId
                                                                     ,FileStorageType
                                                                     ,AppliesToAllFeatures
                                                                     ,UserName
                                                                     ,Password
                                                                     ,Path
                                                                     ,CloudKey
                                                                     ,ModUser
                                                                     ,ModDate
                                                                      )
                            VALUES ( NEWID(), @campusGroupId -- CampusGroupId - uniqueidentifier
                                    ,2                       -- FileStorageType - int
                                    ,0                       -- AppliesToAllFeatures - bit
                                    ,NULL                    -- UserName - nvarchar(100)
                                    ,NULL                    -- Password - nvarchar(100)
                                    ,NULL                    -- Path - nvarchar(150)
                                    ,NULL                    -- CloudKey - nvarchar(200)
                                    ,N'SA'                   -- ModUser - nvarchar(150)
                                    ,GETDATE()               -- ModDate - datetime
                                );

                        END;

                    SET @campusFileConfigurationId = (
                                                     SELECT TOP 1 CampusFileConfigurationId
                                                     FROM   dbo.syCampusFileConfiguration
                                                     WHERE  @campusGroupId = @campusGroupId
                                                     );

                    IF ( @campusFileConfigurationId IS NOT NULL )
                        BEGIN

                            IF (
                               @timeClockPath IS NOT NULL
                               AND @timeClockPath <> ''
                               )
                                BEGIN
                                    IF NOT EXISTS (
                                                  SELECT 1
                                                  FROM   dbo.syCustomFeatureFileConfiguration
                                                  WHERE  CampusConfigurationId = @campusFileConfigurationId
                                                         AND FeatureId = 1
                                                  )
                                        BEGIN
                                            INSERT INTO dbo.syCustomFeatureFileConfiguration (
                                                                                             CustomFeatureConfigurationId
                                                                                            ,FeatureId
                                                                                            ,CampusConfigurationId
                                                                                            ,FileStorageType
                                                                                            ,UserName
                                                                                            ,Password
                                                                                            ,Path
                                                                                            ,CloudKey
                                                                                            ,ModUser
                                                                                            ,ModDate
                                                                                             )
                                            VALUES ( NEWID()                    -- CustomFeatureConfigurationId - uniqueidentifier
                                                    ,1                          -- FeatureId - int
                                                    ,@campusFileConfigurationId -- CampusConfigurationId - uniqueidentifier
                                                    ,2                          -- FileStorageType - int
                                                    ,NULL                       -- UserName - nvarchar(100)
                                                    ,NULL                       -- Password - nvarchar(100)
                                                    ,@timeClockPath             -- Path - nvarchar(150)
                                                    ,NULL                       -- CloudKey - nvarchar(200)
                                                    ,N'SA'                      -- ModUser - nvarchar(150)
                                                    ,GETDATE()                  -- ModDate - datetime
                                                );
                                        END;
                                END;


                            IF (
                               @leadImportPath IS NOT NULL
                               AND @leadImportPath <> ''
                               )
                                BEGIN
                                    IF NOT EXISTS (
                                                  SELECT 1
                                                  FROM   dbo.syCustomFeatureFileConfiguration
                                                  WHERE  CampusConfigurationId = @campusFileConfigurationId
                                                         AND FeatureId = 2
                                                  )
                                        BEGIN
                                            INSERT INTO dbo.syCustomFeatureFileConfiguration (
                                                                                             CustomFeatureConfigurationId
                                                                                            ,FeatureId
                                                                                            ,CampusConfigurationId
                                                                                            ,FileStorageType
                                                                                            ,UserName
                                                                                            ,Password
                                                                                            ,Path
                                                                                            ,CloudKey
                                                                                            ,ModUser
                                                                                            ,ModDate
                                                                                             )
                                            VALUES ( NEWID()                    -- CustomFeatureConfigurationId - uniqueidentifier
                                                    ,2                          -- FeatureId - int
                                                    ,@campusFileConfigurationId -- CampusConfigurationId - uniqueidentifier
                                                    ,2                          -- FileStorageType - int
                                                    ,NULL                       -- UserName - nvarchar(100)
                                                    ,NULL                       -- Password - nvarchar(100)
                                                    ,@leadImportPath            -- Path - nvarchar(150)
                                                    ,NULL                       -- CloudKey - nvarchar(200)
                                                    ,N'SA'                      -- ModUser - nvarchar(150)
                                                    ,GETDATE()                  -- ModDate - datetime
                                                );
                                        END;
                                END;

                            IF (
                               @documentsPath IS NOT NULL
                               AND @documentsPath <> ''
                               )
                                BEGIN
                                    IF NOT EXISTS (
                                                  SELECT 1
                                                  FROM   dbo.syCustomFeatureFileConfiguration
                                                  WHERE  CampusConfigurationId = @campusFileConfigurationId
                                                         AND FeatureId = 3
                                                  )
                                        BEGIN
                                            INSERT INTO dbo.syCustomFeatureFileConfiguration (
                                                                                             CustomFeatureConfigurationId
                                                                                            ,FeatureId
                                                                                            ,CampusConfigurationId
                                                                                            ,FileStorageType
                                                                                            ,UserName
                                                                                            ,Password
                                                                                            ,Path
                                                                                            ,CloudKey
                                                                                            ,ModUser
                                                                                            ,ModDate
                                                                                             )
                                            VALUES ( NEWID()                    -- CustomFeatureConfigurationId - uniqueidentifier
                                                    ,3                          -- FeatureId - int
                                                    ,@campusFileConfigurationId -- CampusConfigurationId - uniqueidentifier
                                                    ,2                          -- FileStorageType - int
                                                    ,NULL                       -- UserName - nvarchar(100)
                                                    ,NULL                       -- Password - nvarchar(100)
                                                    ,@documentsPath             -- Path - nvarchar(150)
                                                    ,NULL                       -- CloudKey - nvarchar(200)
                                                    ,N'SA'                      -- ModUser - nvarchar(150)
                                                    ,GETDATE()                  -- ModDate - datetime
                                                );
                                        END;
                                END;

                            IF (
                               @r2t4Path IS NOT NULL
                               AND @r2t4Path <> ''
                               )
                                BEGIN
                                    IF NOT EXISTS (
                                                  SELECT 1
                                                  FROM   dbo.syCustomFeatureFileConfiguration
                                                  WHERE  CampusConfigurationId = @campusFileConfigurationId
                                                         AND FeatureId = 5
                                                  )
                                        BEGIN
                                            INSERT INTO dbo.syCustomFeatureFileConfiguration (
                                                                                             CustomFeatureConfigurationId
                                                                                            ,FeatureId
                                                                                            ,CampusConfigurationId
                                                                                            ,FileStorageType
                                                                                            ,UserName
                                                                                            ,Password
                                                                                            ,Path
                                                                                            ,CloudKey
                                                                                            ,ModUser
                                                                                            ,ModDate
                                                                                             )
                                            VALUES ( NEWID()                    -- CustomFeatureConfigurationId - uniqueidentifier
                                                    ,5                          -- FeatureId - int
                                                    ,@campusFileConfigurationId -- CampusConfigurationId - uniqueidentifier
                                                    ,2                          -- FileStorageType - int
                                                    ,NULL                       -- UserName - nvarchar(100)
                                                    ,NULL                       -- Password - nvarchar(100)
                                                    ,@r2t4Path                  -- Path - nvarchar(150)
                                                    ,NULL                       -- CloudKey - nvarchar(200)
                                                    ,N'SA'                      -- ModUser - nvarchar(150)
                                                    ,GETDATE()                  -- ModDate - datetime
                                                );
                                        END;
                                END;


                        END;

                END;
            DELETE @campusesToProcess
            WHERE CampusId = @campusId;

        END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION defaultFileStorage;
        PRINT 'Failed to create default file storage settings.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION defaultFileStorage;
        PRINT 'Default file storage settings created.';
    END;
GO
--=================================================================================================
-- END - TECH Create default file storage settings
--=================================================================================================
--=================================================================================================
-- START AD-16228 : Add Final Course Grade to Adhoc
--=================================================================================================

DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION FinalCourseGrade;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId = (
                 SELECT MAX(FldId)
                 FROM   syFields
                 ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId = (
                     SELECT FldTypeId
                     FROM   syFieldTypes
                     WHERE  FldType = 'Decimal'
                     );
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syFields
                  WHERE  FldName = 'FinalCourseGrade'
                  )
        BEGIN
            INSERT INTO syFields (
                                 FldId
                                ,FldName
                                ,FldTypeId
                                ,FldLen
                                ,DDLId
                                ,DerivedFld
                                ,SchlReq
                                ,LogChanges
                                ,Mask
                                 )
            VALUES ( @fldId             -- FldId - int
                    ,'FinalCourseGrade' -- FldName - varchar(200)
                    ,@fldTypeId         -- FldTypeId - int
                    ,10                 -- FldLen - int
                    ,NULL               -- DDLId - int
                    ,NULL               -- DerivedFld - bit
                    ,NULL               -- SchlReq - bit
                    ,NULL               -- LogChanges - bit
                    ,NULL               -- Mask - varchar(50)
                );
            DECLARE @categoryId INT;
            SET @categoryId = (
                              SELECT CategoryId
                              FROM   syFldCategories
                              WHERE  Descrip = 'Course'
                                     AND EntityId = 394
                              );
            DECLARE @TblFldsId INT;
            SET @TblFldsId = (
                             SELECT MAX(TblFldsId)
                             FROM   syTblFlds
                             ) + 1;
            DECLARE @tblId INT;
            SET @tblId = (
                         SELECT TblId
                         FROM   syTables
                         WHERE  TblName = 'arStuEnrollments'
                         );
            DECLARE @FldCapId INT;
            SET @FldCapId = (
                            SELECT MAX(FldCapId)
                            FROM   syFldCaptions
                            ) + 1;
            INSERT INTO syFldCaptions (
                                      FldCapId
                                     ,FldId
                                     ,LangId
                                     ,Caption
                                     ,FldDescrip
                                      )
            VALUES ( @FldCapId            -- FldCapId - int
                    ,@fldId               -- FldId - int
                    ,1                    -- LangId - tinyint
                    ,'FinalCourseGrade'   -- Caption - varchar(100)
                    ,'Final Course Grade' -- FldDescrip - varchar(150)
                );
            INSERT INTO syTblFlds (
                                  TblFldsId
                                 ,TblId
                                 ,FldId
                                 ,CategoryId
                                 ,FKColDescrip
                                  )
            VALUES ( @TblFldsId  -- TblFldsId - int
                    ,@tblId      -- TblId - int
                    ,@fldId      -- FldId - int
                    ,@categoryId -- CategoryId - int
                    ,NULL        -- FKColDescrip - varchar(50)
                );
            INSERT INTO syFieldCalculation (
                                           FldId
                                          ,CalculationSql
                                           )
            VALUES ( @fldId                                                                                                     -- FldId - int
                    ,' dbo.CalculateStudentAverage(arStuEnrollments.StuEnrollId,NULL,NULL,arClassSections.ClsSectionId,NULL)  ' -- CalculationSql - varchar(8000)
                );
        END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION FinalCourseGrade;
        PRINT 'Failed to Add FinalCourseGrade to Adhoc Report';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION FinalCourseGrade;
        PRINT 'Added FinalCourseGrade to Adhoc Report';
    END;
GO
--=================================================================================================
-- END  AD-16228 : Add Final Course Grade to Adhoc
--=================================================================================================
-- ===============================================================================================
-- END Consolidated Script Version 4.1SP2
-- ===============================================================================================