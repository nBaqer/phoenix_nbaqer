﻿/*
Run this script on:

        dev-com-db1\Adv.PaultMichellLive    -  This database will be modified

to synchronize it with a database with the schema represented by:

        Source

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.7.10021 from Red Gate Software Ltd at 9/3/2019 3:42:52 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
/*
* Use this Pre-Deployment script to perform tasks before the deployment of the project.
* Read more at https://www.red-gate.com/SOC7/pre-deployment-script-help
*/
UPDATE dbo.arClsSectMeetings
SET PeriodId = NULL
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)

DELETE FROM syPeriodsWorkDays
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[UserAccessPerResource]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAccessPerResource]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[UserAccessPerResource]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[UserAccessPerResourceForItems]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAccessPerResourceForItems]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[UserAccessPerResourceForItems]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_NACCAS_CohortGrid]'
GO
IF OBJECT_ID(N'[dbo].[USP_NACCAS_CohortGrid]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_NACCAS_CohortGrid]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_PR_Main_DBConfigs]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Main_DBConfigs]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_PR_Main_DBConfigs]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[CalculateStudentAverage]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateStudentAverage]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[CalculateStudentAverage]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[syCampusFileConfiguration]'
GO
IF OBJECT_ID(N'[dbo].[syCampusFileConfiguration]', 'U') IS NULL
CREATE TABLE [dbo].[syCampusFileConfiguration]
(
[CampusFileConfigurationId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syCampusFileConfiguration_CampusFileConfigurationId] DEFAULT (newid()),
[CampusGroupId] [uniqueidentifier] NOT NULL,
[FileStorageType] [int] NOT NULL,
[AppliesToAllFeatures] [bit] NOT NULL CONSTRAINT [DF_syCampusFileConfiguration_AppliesToAllFeatures] DEFAULT ((0)),
[UserName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Path] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CloudKey] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_syCampusFileConfiguration_CampusFileConfigurationId] on [dbo].[syCampusFileConfiguration]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK_syCampusFileConfiguration_CampusFileConfigurationId]', 'PK') AND parent_object_id = OBJECT_ID(N'[dbo].[syCampusFileConfiguration]', 'U'))
ALTER TABLE [dbo].[syCampusFileConfiguration] ADD CONSTRAINT [PK_syCampusFileConfiguration_CampusFileConfigurationId] PRIMARY KEY CLUSTERED  ([CampusFileConfigurationId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[syCustomFeatureFileConfiguration]'
GO
IF OBJECT_ID(N'[dbo].[syCustomFeatureFileConfiguration]', 'U') IS NULL
CREATE TABLE [dbo].[syCustomFeatureFileConfiguration]
(
[CustomFeatureConfigurationId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syCustomFeatureFileConfiguration_CustomFeatureConfigurationId] DEFAULT (newid()),
[FeatureId] [int] NOT NULL,
[CampusConfigurationId] [uniqueidentifier] NOT NULL,
[FileStorageType] [int] NOT NULL,
[UserName] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Password] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Path] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[CloudKey] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [nvarchar] (150) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_syCustomFeatureFileConfiguration_CustomFeatureConfigurationId] on [dbo].[syCustomFeatureFileConfiguration]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK_syCustomFeatureFileConfiguration_CustomFeatureConfigurationId]', 'PK') AND parent_object_id = OBJECT_ID(N'[dbo].[syCustomFeatureFileConfiguration]', 'U'))
ALTER TABLE [dbo].[syCustomFeatureFileConfiguration] ADD CONSTRAINT [PK_syCustomFeatureFileConfiguration_CustomFeatureConfigurationId] PRIMARY KEY CLUSTERED  ([CustomFeatureConfigurationId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CalculateStudentAverage]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateStudentAverage]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'-- =============================================
-- Author:		FAME Inc.
-- Create date: 6/11/2019
-- Description:	Calculated Student GPA Based on a set of parameters - Numeric ( Weighted & Unweighted currently implemented)
--Referenced in :
--[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents] -> via db function CalculateStudentAverage
--[Usp_PR_Sub3_Terms_forAllEnrolmnents] -> via db function CalculateStudentAverage
--ANY CHANGES TO THIS FILE MUST BE REFLECTED IN SP GPA_Calculator
-- =============================================
CREATE FUNCTION [dbo].[CalculateStudentAverage]
    (
        @EnrollmentId VARCHAR(50)
       ,@BeginDate DATETIME = NULL
       ,@EndDate DATETIME = NULL
       ,@ClassId UNIQUEIDENTIFIER = NULL
       ,@TermId UNIQUEIDENTIFIER = NULL
    )
RETURNS DECIMAL(16, 2)
AS
    BEGIN
        DECLARE @GPAResult AS DECIMAL(16, 2);
        DECLARE @UseWeightForGPA BIT = 1;

        IF ( @EnrollmentId IS NULL )
            RETURN @GPAResult;

        SET @UseWeightForGPA = (
                               SELECT TOP 1 PV.DoCourseWeightOverallGPA
                               FROM   dbo.arStuEnrollments E
                               JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                               WHERE  E.StuEnrollId = @EnrollmentId
                               );

        -----------------Numeric GPA Grades Format------------------------

        SET @GPAResult = ISNULL(
                         (
                         SELECT ( CASE WHEN @UseWeightForGPA = 1 THEN
                                           ROUND(( SUM(b.CourseWeight * b.WeightedCourseAverage / 100) / SUM(b.CourseWeight)) * 100, 2)
                                       ELSE AVG(b.UnweightedCourseAverage)
                                  END
                                ) AS WeightedGPA
                         FROM   (
                                SELECT   SUM(OurSingleClassGradeFactor) AS CourseFactor
                                        ,SUM(a.GradeWeight) AS GradeWeight
                                        ,SUM(a.Score) AS SumOfScores
                                        ,CASE WHEN SUM(a.GradeWeight) > 0 THEN ( SUM(OurSingleClassGradeFactor) / SUM(a.GradeWeight)) * 100
                                                     ELSE null
                                                END AS WeightedCourseAverage
                                        ,( AVG(a.Score)) AS UnweightedCourseAverage
                                        ,ClsSectionId
                                        ,a.CourseWeight
                                FROM     (
                                         SELECT ( c.Weight * a.Score / 100 ) AS OurSingleClassGradeFactor
                                               ,c.Weight AS GradeWeight
                                               ,a.Score
                                               ,a.ClsSectionId
                                               ,CASE WHEN d.CourseWeight = 0
                                                          AND NOT EXISTS (
                                                                         SELECT 1
                                                                         FROM   dbo.arProgVerDef CPVC
                                                                         WHERE  CPVC.CourseWeight > 0
                                                                                AND CPVC.PrgVerId = d.PrgVerId
                                                                         )
                                                          AND PV.DoCourseWeightOverallGPA = 0 THEN 100 / (
                                                                                                         SELECT COUNT(*)
                                                                                                         FROM   dbo.arGrdBkResults Cn
                                                                                                         WHERE  Cn.StuEnrollId = a.StuEnrollId
                                                                                                                AND Cn.ClsSectionId = a.ClsSectionId
                                                                                                         )
                                                     ELSE d.CourseWeight
                                                END AS courseweight
                                               ,c.Descrip AS ClassDescrip
                                         FROM   (
                                                SELECT   a.StuEnrollId
                                                        ,a.ClsSectionId
                                                        ,a.InstrGrdBkWgtDetailId
                                                        ,AVG(Score) AS Score
                                                FROM     dbo.arGrdBkResults a
                                                JOIN     dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                                JOIN     dbo.arGrdComponentTypes f ON f.GrdComponentTypeId = c.GrdComponentTypeId
                                                JOIN     dbo.arClassSections ON arClassSections.ClsSectionId = a.ClsSectionId
                                                WHERE    a.StuEnrollId = @EnrollmentId
                                                         --AND a.IsCompGraded = 1
                                                         AND a.Score >= 0
                                                         AND a.Score IS NOT NULL
                                                         AND a.PostDate IS NOT NULL
                                                         AND (
                                                             @EndDate IS NULL
                                                             OR ( a.PostDate <= @EndDate )
                                                             )
                                                         AND (
                                                             @ClassId IS NULL
                                                             OR ( @ClassId = a.ClsSectionId )
                                                             )
                                                         AND (
                                                             @TermId IS NULL
                                                             OR ( @TermId = TermId )
                                                             )
                                                GROUP BY StuEnrollId
                                                        ,a.ClsSectionId
                                                        ,a.InstrGrdBkWgtDetailId
                                                ) a -- students grades
                                         JOIN   dbo.arClassSections b ON b.ClsSectionId = a.ClsSectionId
                                         JOIN   dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                         JOIN   dbo.arProgVerDef d ON d.ReqId = b.ReqId
                                         JOIN   dbo.arStuEnrollments E ON E.StuEnrollId = a.StuEnrollId
                                         JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId                                       
                                         ) a
                                WHERE    a.CourseWeight > 0
                                GROUP BY ClsSectionId
                                        ,a.CourseWeight


                                --ORDER BY CourseGPA DESC
                                ) b
                         )
                        ,0.00
                               );
        --END;
        RETURN @GPAResult;
    END;








'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[syFileConfigurationFeatures]'
GO
IF OBJECT_ID(N'[dbo].[syFileConfigurationFeatures]', 'U') IS NULL
CREATE TABLE [dbo].[syFileConfigurationFeatures]
(
[FeatureId] [int] NOT NULL,
[FeatureCode] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[FeatureDescription] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[StatusId] [uniqueidentifier] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_syFileConfigurationFeatures_FeatureId] on [dbo].[syFileConfigurationFeatures]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK_syFileConfigurationFeatures_FeatureId]', 'PK') AND parent_object_id = OBJECT_ID(N'[dbo].[syFileConfigurationFeatures]', 'U'))
ALTER TABLE [dbo].[syFileConfigurationFeatures] ADD CONSTRAINT [PK_syFileConfigurationFeatures_FeatureId] PRIMARY KEY CLUSTERED  ([FeatureId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[UserAccessPerResource]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAccessPerResource]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'-- ===========================================
-- Author:Kimberly Befera FAME Inc
-- Create date: 8/22/2019
-- Description: Returns the campus groups the user has access edit to for the given page  
CREATE   FUNCTION [dbo].[UserAccessPerResource]
(
    @UserId UNIQUEIDENTIFIER,
    @ResourceID VARCHAR(50)
)
RETURNS @Results TABLE
(
    CampGrpId UNIQUEIDENTIFIER,
    CampGrpDescrip VARCHAR(50),
    IsAllCampusGrp BIT,
    AccessLevel INT
)
AS
BEGIN
    DECLARE @AccessLevel TABLE
    (
        CampGrpId UNIQUEIDENTIFIER,
        CampGrpDescrip VARCHAR(50),
        IsAllCampusGrp BIT,
        AccessLevel INT
    );

    DECLARE @HasAllAccess INT;
    DECLARE @IsSupport INT;
    SET @IsSupport = 0;
    SET @HasAllAccess = 0;
    SET @IsSupport =
    (
        SELECT COUNT(UserName)
        FROM dbo.syUsers
        WHERE UserId = @UserId
              AND IsAdvantageSuperUser = 1
    );
    SET @HasAllAccess =
    (
        SELECT COUNT(*)
        FROM syUsersRolesCampGrps UTCG
            JOIN syCampGrps CG
                ON CG.CampGrpId = UTCG.CampGrpId
        WHERE IsAllCampusGrp = 1
              AND UserId = @UserId
    );


    IF (@IsSupport + @HasAllAccess) > 0
    BEGIN
        INSERT @Results
        SELECT DISTINCT
               CG.CampGrpId,
               CG.CampGrpDescrip,
               CG.IsAllCampusGrp,
               15 AS AccessLevel
        FROM syCampGrps CG
            JOIN dbo.syStatuses ST
                ON ST.StatusId = CG.StatusId
        WHERE ST.Status = ''Active''
        ORDER BY CG.IsAllCampusGrp DESC,
                 CG.CampGrpDescrip;

    END;
    ELSE
    BEGIN

        INSERT @Results
        SELECT DISTINCT
               URCG.CampGrpId,
               CG.CampGrpDescrip,
               CG.IsAllCampusGrp,
               MAX(RRL.AccessLevel) AS AccessLevel
        FROM syCampGrps CG
            JOIN syUsersRolesCampGrps URCG
                ON URCG.CampGrpId = CG.CampGrpId
            JOIN syRlsResLvls RRL
                ON RRL.RoleId = URCG.RoleId
            JOIN syResources R
                ON R.ResourceID = RRL.ResourceID
            JOIN dbo.syStatuses ST
                ON ST.StatusId = CG.StatusId
        WHERE ST.Status = ''Active''
              AND URCG.UserId = @UserId
              AND RRL.ResourceID = @ResourceID
        GROUP BY URCG.CampGrpId,
                 CG.CampGrpDescrip,
                 CG.IsAllCampusGrp
        ORDER BY CG.IsAllCampusGrp DESC,
                 CG.CampGrpDescrip;


    END;

    RETURN;
END;

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[UserAccessPerResourceForItems]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UserAccessPerResourceForItems]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'-- ===========================================
-- Author:Kimberly Befera FAME Inc
-- Create date: 8/22/2019
-- Description: Returns the campus groups the user has access edit to for the given page for the Items  
CREATE   FUNCTION [dbo].[UserAccessPerResourceForItems]
(
    @UserId UNIQUEIDENTIFIER,
    @CampusId UNIQUEIDENTIFIER,
    @ResourceID VARCHAR(50)
)
RETURNS @Results TABLE
(
    CampusId UNIQUEIDENTIFIER,
    CampGrpId UNIQUEIDENTIFIER,
    CampGrpDescrip VARCHAR(50),
    AccessLevel INT
)
AS
BEGIN

    DECLARE @HasAllAccess INT;
    DECLARE @IsSupport INT;
    SET @IsSupport = 0;
    SET @HasAllAccess = 0;
    SET @IsSupport =
    (
        SELECT COUNT(UserName)
        FROM dbo.syUsers
        WHERE UserId = @UserId
              AND IsAdvantageSuperUser = 1
    );
    SET @HasAllAccess =
    (
        SELECT COUNT(*)
        FROM syUsersRolesCampGrps UTCG
            JOIN syCampGrps CG
                ON CG.CampGrpId = UTCG.CampGrpId
        WHERE IsAllCampusGrp = 1
              AND UserId = @UserId
    );


    IF (@IsSupport) > 0
    BEGIN
        INSERT @Results
        SELECT DISTINCT
               CmpGrp.CampusId,
               CG.CampGrpId,
               CG.CampGrpDescrip,
               15 AS AccessLevel
        FROM syCampGrps CG
            JOIN dbo.syCmpGrpCmps CmpGrp
                ON CmpGrp.CampGrpId = CG.CampGrpId
            JOIN dbo.syCampuses CAMP
                ON CAMP.CampusId = CmpGrp.CampusId
        WHERE CmpGrp.CampusId = @CampusId
        ORDER BY CG.CampGrpDescrip;

    END;
    ELSE
    BEGIN
        INSERT @Results
        SELECT DISTINCT
               CGC.CampusId,
               CG.CampGrpId,
               CG.CampGrpDescrip,
               MAX(RRL.AccessLevel) AS AccessLevel
        FROM syCmpGrpCmps CGC
            JOIN syUsersRolesCampGrps URCG
                ON URCG.CampGrpId = CGC.CampGrpId
            JOIN syCampGrps CG
                ON CG.CampGrpId = CGC.CampGrpId
            JOIN syRlsResLvls RRL
                ON RRL.RoleId = URCG.RoleId
            JOIN syResources R
                ON R.ResourceID = RRL.ResourceID
        WHERE URCG.UserId = @UserId
              AND CGC.CampusId = @CampusId
              AND RRL.ResourceID = @ResourceID
        GROUP BY CGC.CampusId,
                 CG.CampGrpId,
                 CG.CampGrpDescrip
        ORDER BY CG.CampGrpDescrip;
    END;

    RETURN;
END;

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_NACCAS_CohortGrid]'
GO
IF OBJECT_ID(N'[dbo].[USP_NACCAS_CohortGrid]', 'P') IS NULL
EXEC sp_executesql N'

CREATE PROCEDURE [dbo].[USP_NACCAS_CohortGrid]
    @PrgIdList VARCHAR(MAX) = NULL
   ,@CampusId VARCHAR(50)
   ,@ReportYear INT
   ,@ReportType VARCHAR(50)
AS
    DECLARE @StartMonth INT = 1
           ,@StartDay INT = 1
           ,@EndMonth INT = 12
           ,@EndDay INT = 31
           ,@FutureStart INT = 7
           ,@CurrentlyAttending INT = 9
           ,@LOA INT = 10
           ,@Suspension INT = 11
           ,@Dropped INT = 12
           ,@Graduated INT = 14
           ,@Transfer INT = 19
           ,@Preliminary VARCHAR(50) = ''Preliminary''
           ,@FallAnnual VARCHAR(50) = ''FallAnnual''
           ,@MilitaryTransfer UNIQUEIDENTIFIER
           ,@CallToActiveDuty UNIQUEIDENTIFIER
           ,@Deceased UNIQUEIDENTIFIER
           ,@TemporarilyDisabled UNIQUEIDENTIFIER
           ,@PermanentlyDisabled UNIQUEIDENTIFIER
           ,@TransferredToEquivalent UNIQUEIDENTIFIER;

    BEGIN

        SET @MilitaryTransfer = (
                                SELECT TOP 1 NACCASDropReasonId
                                FROM   dbo.syNACCASDropReasons
                                WHERE  Name = ''Military Transfer''
                                );
        SET @CallToActiveDuty = (
                                SELECT TOP 1 NACCASDropReasonId
                                FROM   dbo.syNACCASDropReasons
                                WHERE  Name = ''Call to Active Duty''
                                );
        SET @Deceased = (
                        SELECT TOP 1 NACCASDropReasonId
                        FROM   dbo.syNACCASDropReasons
                        WHERE  Name = ''Deceased''
                        );
        SET @TemporarilyDisabled = (
                                   SELECT TOP 1 NACCASDropReasonId
                                   FROM   dbo.syNACCASDropReasons
                                   WHERE  Name = ''Temporarily disabled''
                                   );
        SET @PermanentlyDisabled = (
                                   SELECT TOP 1 NACCASDropReasonId
                                   FROM   dbo.syNACCASDropReasons
                                   WHERE  Name = ''Permanently disabled''
                                   );
        SET @TransferredToEquivalent = (
                                       SELECT TOP 1 NACCASDropReasonId
                                       FROM   dbo.syNACCASDropReasons
                                       WHERE  Name = ''Transferred to an equivalent program at another school with same accreditation''
                                       );

        DECLARE @Enrollments TABLE
            (
                PrgVerID UNIQUEIDENTIFIER
               ,PrgVerDescrip VARCHAR(50)
               ,ProgramHours DECIMAL(9, 2)
               ,ProgramCredits DECIMAL(18, 2)
               ,TotalTransferHours DECIMAL(18, 2)
               ,TransferredProgram UNIQUEIDENTIFIER
               ,TransferHoursFromProgram DECIMAL(18, 2)
               ,SSN VARCHAR(50)
               ,PhoneNumber VARCHAR(50)
               ,Email VARCHAR(100)
               ,Student VARCHAR(50)
               ,LeadId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,EnrollmentID NVARCHAR(50)
               ,Status VARCHAR(80)
               ,SysStatusId INT
               ,DropReasonId UNIQUEIDENTIFIER
               ,StartDate DATETIME
               ,ContractedGradDate DATETIME
               ,ExpectedGradDate DATETIME
               ,LDA DATETIME
               ,LicensureWrittenAllParts BIT
               ,LicensureLastPartWrittenOn DATETIME
               ,LicensurePassedAllParts BIT
               ,AllowsMoneyOwed BIT
               ,AllowsIncompleteReq BIT
            );

        DECLARE @EnrollmentsWithLastStatus TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,SysStatusId INT
               ,DropReasonId UNIQUEIDENTIFIER
               ,StatusCodeDescription VARCHAR(80)
            );
        INSERT INTO @EnrollmentsWithLastStatus
                    SELECT lastStatusOfYear.StuEnrollId
                          ,lastStatusOfYear.SysStatusId
                          ,lastStatusOfYear.DropReasonId
                          ,lastStatusOfYear.StatusCodeDescrip
                    FROM   (
                           SELECT     StuEnrollId
                                     ,syStatusCodes.SysStatusId
                                     ,StatusCodeDescrip
                                     ,DateOfChange
                                     ,DropReasonId
                                     ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId
                                                          ORDER BY DateOfChange DESC
                                                        ) rn
                           FROM       dbo.syStudentStatusChanges
                           INNER JOIN dbo.syStatusCodes ON syStatusCodes.StatusCodeId = syStudentStatusChanges.NewStatusId
                           WHERE      DATEPART(YEAR, DateOfChange) <= ( @ReportYear - 1 )
                           ) lastStatusOfYear
                    WHERE  rn = 1;





        INSERT INTO @Enrollments
                    SELECT     Prog.ProgId AS PrgVerID
                              ,Prog.ProgDescrip AS PrgVerDescrip
                              ,PV.Hours AS ProgramHours
                              ,PV.Credits AS ProgramCredits
                              ,SE.TransferHours
                              ,SE.TransferHoursFromThisSchoolEnrollmentId
                              ,SE.TotalTransferHoursFromThisSchool
                              ,LD.SSN
                              ,ISNULL((
                                      SELECT   TOP 1 Phone
                                      FROM     dbo.adLeadPhone
                                      WHERE    LeadId = LD.LeadId
                                               AND (
                                                   IsBest = 1
                                                   OR IsShowOnLeadPage = 1
                                                   )
                                      ORDER BY Position
                                      )
                                     ,''''
                                     ) AS PhoneNumber
                              ,ISNULL((
                                      SELECT   TOP 1 EMail
                                      FROM     dbo.AdLeadEmail
                                      WHERE    LeadId = LD.LeadId
                                               AND (
                                                   IsPreferred = 1
                                                   OR IsShowOnLeadPage = 1
                                                   )
                                      ORDER BY IsPreferred DESC
                                              ,IsShowOnLeadPage DESC
                                      )
                                     ,''''
                                     ) AS Email
                              ,LD.LastName + '', '' + LD.FirstName + '' '' + ISNULL(LD.MiddleName, '''') AS Student
                              ,LD.LeadId
                              ,SE.StuEnrollId
                              ,LD.StudentNumber AS EnrollmentID
                              ,[@EnrollmentsWithLastStatus].StatusCodeDescription AS Status
                              ,[@EnrollmentsWithLastStatus].SysStatusId AS SysStatusId
                              ,[@EnrollmentsWithLastStatus].DropReasonId AS DropReasonId
                              ,SE.StartDate AS StartDate
                              ,SE.ContractedGradDate AS ContractedGradDate
                              ,dbo.GetGraduatedDate(SE.StuEnrollId) AS ExpectedGradDate
                              ,dbo.GetLDA(SE.StuEnrollId) AS LDA
                              ,SE.LicensureWrittenAllParts AS LicensureWrittenAllParts
                              ,SE.LicensureLastPartWrittenOn AS LicensureLastPartWrittenOn
                              ,SE.LicensurePassedAllParts AS LicensurePassedAllParts
                              ,camp.AllowGraduateAndStillOweMoney AS AllowsMoneyOwed
                              ,camp.AllowGraduateWithoutFullCompletion AS AllowsIncompleteReq
                    FROM       dbo.arStuEnrollments AS SE
                    INNER JOIN dbo.adLeads AS LD ON LD.LeadId = SE.LeadId
                    INNER JOIN dbo.arPrgVersions AS PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arPrograms AS Prog ON Prog.ProgId = PV.ProgId
                    INNER JOIN dbo.syApprovedNACCASProgramVersion NaccasApproved ON NaccasApproved.ProgramVersionId = SE.PrgVerId
                    INNER JOIN dbo.syNaccasSettings NaccasSettings ON NaccasSettings.NaccasSettingId = NaccasApproved.NaccasSettingId
                    INNER JOIN dbo.syCampuses AS camp ON camp.CampusId = SE.CampusId
                    INNER JOIN @EnrollmentsWithLastStatus ON [@EnrollmentsWithLastStatus].StuEnrollId = SE.StuEnrollId
                    WHERE      SE.CampusId = @CampusId
                               AND NaccasSettings.CampusId = @CampusId
                               --that have a contracted graduation date between January 1 of reporting year minus 1 year and December 31st of reporting year minus 1 year
                               -- Not a third party contract
                               AND NOT SE.ThirdPartyContract = 1
                               AND NaccasApproved.IsApproved = 1
                               AND SE.ContractedGradDate
                               BETWEEN CONVERT(DATETIME, CONVERT(VARCHAR(50), (( @ReportYear - 1 ) * 10000 + @StartMonth * 100 + @StartDay )), 112) AND CONVERT(
                                                                                                                                                                   DATETIME
                                                                                                                                                                  ,CONVERT(
                                                                                                                                                                              VARCHAR(50)
                                                                                                                                                                             ,(( @ReportYear
                                                                                                                                                                                 - 1
                                                                                                                                                                               )
                                                                                                                                                                               * 10000
                                                                                                                                                                               + @EndMonth
                                                                                                                                                                               * 100
                                                                                                                                                                               + @EndDay
                                                                                                                                                                              )
                                                                                                                                                                          )
                                                                                                                                                                  ,112
                                                                                                                                                               );
        --Base Case 
        WITH A
        AS ( SELECT se.PrgVerID
                   ,se.PrgVerDescrip
                   ,se.ProgramHours
                   ,se.ProgramCredits
                   ,se.SSN
                   ,se.PhoneNumber
                   ,se.Email
                   ,se.Student
                   ,se.LeadId
                   ,se.StuEnrollId
                   ,se.EnrollmentID
                   ,se.Status
                   ,se.SysStatusId
                   ,se.DropReasonId
                   ,se.StartDate
                   ,se.ContractedGradDate
                   ,se.ExpectedGradDate
                   ,se.LDA
                   ,se.LicensureWrittenAllParts
                   ,se.LicensureLastPartWrittenOn
                   ,se.LicensurePassedAllParts
                   ,se.AllowsMoneyOwed
                   ,se.AllowsIncompleteReq
             FROM   (
                    SELECT *
                          ,ROW_NUMBER() OVER ( PARTITION BY LeadId
                                                           ,PrgVerID
                                                           ,SysStatusId
                                               ORDER BY StartDate DESC
                                             ) RN
                    FROM   @Enrollments
                    ) se
             --Is currently attending the school or on LOA or Suspended or is enrolled but have not yet started
             WHERE  se.SysStatusId IN ( @FutureStart, @CurrentlyAttending, @Suspension, @LOA )
                    AND se.RN = 1
                    AND se.StuEnrollId NOT IN ((
                                               SELECT     se.TransferredProgram
                                               FROM       @Enrollments se
                                               INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
                                               WHERE      (
                                                          (
                                                          se.TotalTransferHours IS NOT NULL
                                                          AND se.TotalTransferHours <> 0
                                                          )
                                                          AND (( se.TransferHoursFromProgram / pe.ProgramHours ) = 1 )
                                                          )
                                               UNION
                                               SELECT     pe.TransferredProgram
                                               FROM       @Enrollments se
                                               INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
                                               WHERE      (
                                                          (
                                                          se.TotalTransferHours IS NOT NULL
                                                          AND se.TotalTransferHours <> 0
                                                          )
                                                          AND (( se.TransferHoursFromProgram / pe.ProgramHours ) <> 1 )
                                                          )
                                               )
                                              ))

            --Was enrolled in two different NACCAS approved programs in the same school and graduated/licensed/placed from both the programs in the same report period
            ,B
        AS ( SELECT se.PrgVerID
                   ,se.PrgVerDescrip
                   ,se.ProgramHours
                   ,se.ProgramCredits
                   ,se.SSN
                   ,se.PhoneNumber
                   ,se.Email
                   ,se.Student
                   ,se.LeadId
                   ,se.StuEnrollId
                   ,se.EnrollmentID
                   ,se.Status
                   ,se.SysStatusId
                   ,se.DropReasonId
                   ,se.StartDate
                   ,se.ContractedGradDate
                   ,se.ExpectedGradDate
                   ,se.LDA
                   ,se.LicensureWrittenAllParts
                   ,se.LicensureLastPartWrittenOn
                   ,se.LicensurePassedAllParts
                   ,se.AllowsMoneyOwed
                   ,se.AllowsIncompleteReq
             FROM   (
                    SELECT *
                          ,COUNT(1) OVER ( PARTITION BY LeadId
                                                       ,SysStatusId
                                         ) AS cnt
                    FROM   @Enrollments
                    ) se
             WHERE  se.SysStatusId IN ( @Graduated ))
            ,C
        AS (
           --Has dropped from a program whose length is less than one academic year of 900 hours after being enrolled for more than 15 calendar days
           --Has dropped from a program whose length is equal to or more than one academic year of 900 hours after being enrolled for more than 30 calendar DAYS
           SELECT se.PrgVerID
                 ,se.PrgVerDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND (
                      (
                      se.ProgramHours < 900
                      AND DATEDIFF(DAY, se.StartDate, se.LDA) > 15
                      AND NOT EXISTS (
                                     SELECT     *
                                     FROM       dbo.syNACCASDropReasonsMapping nm
                                     INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                                     WHERE      ns.CampusId = @CampusId
                                                AND se.DropReasonId = nm.ADVDropReasonId
                                                AND nm.NACCASDropReasonId IN ( @CallToActiveDuty, @MilitaryTransfer, @Deceased, @TemporarilyDisabled
                                                                              ,@PermanentlyDisabled, @TransferredToEquivalent
                                                                             )
                                     )
                      )
                      OR (
                         se.ProgramHours >= 900
                         AND DATEDIFF(DAY, se.StartDate, se.LDA) > 30
                         AND NOT EXISTS (
                                        SELECT     *
                                        FROM       dbo.syNACCASDropReasonsMapping nm
                                        INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                                        WHERE      ns.CampusId = @CampusId
                                                   AND se.DropReasonId = nm.ADVDropReasonId
                                                   AND nm.NACCASDropReasonId IN ( @CallToActiveDuty, @MilitaryTransfer, @Deceased, @TemporarilyDisabled
                                                                                 ,@PermanentlyDisabled, @TransferredToEquivalent
                                                                                )
                                        )
                         )
                      )
                  AND se.StuEnrollId NOT IN ((
                                             SELECT     se.TransferredProgram
                                             FROM       @Enrollments se
                                             INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
                                             WHERE      (
                                                        (
                                                        se.TotalTransferHours IS NOT NULL
                                                        AND se.TotalTransferHours <> 0
                                                        )
                                                        AND (( se.TransferHoursFromProgram / pe.ProgramHours ) = 1 )
                                                        )
                                             )
                                            ))
            --Was transferred from the school for which the report is generated to another school and both the schools are a branch of the same parent.
            -- Such a student would be included in the report generated for the original school as did not complete
            ,D
        AS ( SELECT     se.PrgVerID
                       ,se.PrgVerDescrip
                       ,se.ProgramHours
                       ,se.ProgramCredits
                       ,se.SSN
                       ,se.PhoneNumber
                       ,se.Email
                       ,se.Student
                       ,se.LeadId
                       ,se.StuEnrollId
                       ,se.EnrollmentID
                       ,se.Status
                       ,se.SysStatusId
                       ,se.DropReasonId
                       ,se.StartDate
                       ,se.ContractedGradDate
                       ,se.ExpectedGradDate
                       ,se.LDA
                       ,se.LicensureWrittenAllParts
                       ,se.LicensureLastPartWrittenOn
                       ,se.LicensurePassedAllParts
                       ,se.AllowsMoneyOwed
                       ,se.AllowsIncompleteReq
             FROM       @Enrollments se
             INNER JOIN dbo.arTrackTransfer tt ON se.StuEnrollId = tt.StuEnrollId
             INNER JOIN dbo.syCampuses srcCmp ON srcCmp.CampusId = tt.SourceCampusId
             INNER JOIN dbo.syCampuses targetCmp ON targetCmp.CampusId = tt.TargetCampusId
             INNER JOIN dbo.arPrgVersions targetPV ON targetPV.PrgVerId = tt.TargetPrgVerId
             WHERE      se.SysStatusId IN ( @Transfer )
                        AND ((
                             srcCmp.IsBranch = 1
                             AND targetCmp.IsBranch = 1
                             AND srcCmp.ParentCampusId <> targetCmp.ParentCampusId
                             )
                            ))
            ,E
        AS (
           -- select from PE id not all hours are transffered else select from SE
           SELECT     se.PrgVerID
                     ,se.PrgVerDescrip
                     ,se.ProgramHours
                     ,se.ProgramCredits
                     ,se.SSN
                     ,se.PhoneNumber
                     ,se.Email
                     ,se.Student
                     ,se.LeadId
                     ,se.StuEnrollId
                     ,se.EnrollmentID
                     ,se.Status
                     ,se.SysStatusId
                     ,se.DropReasonId
                     ,se.StartDate
                     ,se.ContractedGradDate
                     ,se.ExpectedGradDate
                     ,se.LDA
                     ,se.LicensureWrittenAllParts
                     ,se.LicensureLastPartWrittenOn
                     ,se.LicensurePassedAllParts
                     ,se.AllowsMoneyOwed
                     ,se.AllowsIncompleteReq
           FROM       @Enrollments se
           INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
           WHERE      (
                      (
                      se.TotalTransferHours IS NOT NULL
                      AND se.TotalTransferHours <> 0
                      )
                      AND (( se.TransferHoursFromProgram / pe.ProgramHours ) = 1 )
                      )
           UNION
           SELECT     pe.PrgVerID
                     ,pe.PrgVerDescrip
                     ,pe.ProgramHours
                     ,pe.ProgramCredits
                     ,pe.SSN
                     ,pe.PhoneNumber
                     ,pe.Email
                     ,pe.Student
                     ,pe.LeadId
                     ,pe.StuEnrollId
                     ,pe.EnrollmentID
                     ,pe.Status
                     ,pe.SysStatusId
                     ,pe.DropReasonId
                     ,pe.StartDate
                     ,pe.ContractedGradDate
                     ,pe.ExpectedGradDate
                     ,pe.LDA
                     ,pe.LicensureWrittenAllParts
                     ,pe.LicensureLastPartWrittenOn
                     ,pe.LicensurePassedAllParts
                     ,pe.AllowsMoneyOwed
                     ,pe.AllowsIncompleteReq
           FROM       @Enrollments se
           INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
           WHERE      (
                      (
                      se.TotalTransferHours IS NOT NULL
                      AND se.TotalTransferHours <> 0
                      )
                      AND (( se.TransferHoursFromProgram / pe.ProgramHours ) <> 1 )
                      ))
        SELECT   *
        FROM     (
                 SELECT    allData.PrgVerID
                          ,allData.PrgVerDescrip
                          ,allData.ProgramHours
                          ,allData.ProgramCredits
                          ,allData.SSN
                          ,(
                           SELECT STUFF(STUFF(STUFF(allData.PhoneNumber, 1, 0, ''(''), 5, 0, '')''), 9, 0, ''-'')
                           ) AS PhoneNumber
                          ,allData.Email
                          ,allData.Student
                          ,allData.LeadId
                          ,allData.StuEnrollId
                          ,allData.EnrollmentID
                          ,allData.Status
                          ,allData.SysStatusId
                          ,allData.DropReasonId
                          ,allData.StartDate
                          ,allData.ContractedGradDate
                          ,allData.ExpectedGradDate
                          ,allData.LDA
                          ,allData.LicensureWrittenAllParts
                          ,allData.LicensureLastPartWrittenOn
                          ,allData.LicensurePassedAllParts
                          ,allData.AllowsMoneyOwed
                          ,allData.AllowsIncompleteReq
                          ,allData.Graduated
                          ,allData.OwesMoney
                          ,allData.MissingRequired
                          ,allData.GraduatedProgram
                          ,allData.DateGraduated
                          ,allData.PlacementStatus
                          ,allData.IneligibilityReason
                          ,allData.Placed
                          ,allData.SatForAllExamParts
                          ,allData.PassedAllParts
                          ,ISNULL(pemp.EmployerDescrip, '''') AS EmployerName
                          ,ISNULL(pemp.Address1 + '' '' + pemp.Address2, '''') AS EmployerAddress
                          ,ISNULL(pemp.City, '''') AS EmployerCity
                          ,ISNULL(states.StateDescrip, '''') AS EmployerState
                          ,ISNULL(pemp.Zip, '''') AS EmployerZip
                          ,ISNULL(pemp.Phone, '''') AS EmployerPhone
                          ,ROW_NUMBER() OVER ( PARTITION BY allData.StuEnrollId
                                               ORDER BY psp.PlacedDate DESC
                                             ) AS numOfJobs
                 FROM      (
                           SELECT *
                                 ,(
                                  SELECT CASE WHEN satForExam.SatForAllExamParts = ''Y'' THEN CASE WHEN satForExam.LicensurePassedAllParts = 1 THEN ''Y''
                                                                                                 ELSE ''N''
                                                                                            END
                                              ELSE ''N/A''
                                         END
                                  ) PassedAllParts
                           FROM   (
                                  SELECT *
                                        ,(
                                         SELECT CASE WHEN placed.GraduatedProgram = ''Y'' THEN
                                                         CASE WHEN placed.LicensureWrittenAllParts = 0 THEN ''N''
                                                              ELSE
                                                                  CASE WHEN @ReportType = @Preliminary THEN
                                                                           CASE WHEN placed.LicensureLastPartWrittenOn < CONVERT(
                                                                                                                                    DATETIME
                                                                                                                                   ,CONVERT(
                                                                                                                                               VARCHAR(50)
                                                                                                                                              ,(( @ReportYear ) * 10000
                                                                                                                                                + 3 * 100 + 30
                                                                                                                                               )
                                                                                                                                           )
                                                                                                                                   ,112
                                                                                                                                ) THEN ''Y''
                                                                                ELSE ''N''
                                                                           END
                                                                       ELSE
                                                                           CASE WHEN placed.LicensureLastPartWrittenOn < CONVERT(
                                                                                                                                    DATETIME
                                                                                                                                   ,CONVERT(
                                                                                                                                               VARCHAR(50)
                                                                                                                                              ,(( @ReportYear ) * 10000
                                                                                                                                                + 11 * 100 + 30
                                                                                                                                               )
                                                                                                                                           )
                                                                                                                                   ,112
                                                                                                                                ) THEN ''Y''
                                                                                ELSE ''N''
                                                                           END
                                                                  END
                                                         END
                                                     ELSE ''N/A''
                                                END
                                         ) AS SatForAllExamParts
                                  FROM   (
                                         SELECT *
                                               ,(
                                                SELECT CASE WHEN placementStat.PlacementStatus = ''E'' THEN
                                                                CASE WHEN EXISTS (
                                                                                 SELECT     1
                                                                                 FROM       dbo.PlStudentsPlaced
                                                                                 INNER JOIN dbo.plFldStudy ON plFldStudy.FldStudyId = PlStudentsPlaced.FldStudyId
                                                                                 WHERE      dbo.PlStudentsPlaced.StuEnrollId = placementStat.StuEnrollId
                                                                                            AND (
                                                                                                dbo.plFldStudy.FldStudyCode = ''Yes''
                                                                                                OR dbo.plFldStudy.FldStudyCode = ''Related''
                                                                                                )
                                                                                 ) THEN ''Y''
                                                                     ELSE ''N''
                                                                END
                                                            ELSE ''N/A''
                                                       END
                                                ) AS Placed
                                         FROM   (
                                                SELECT *
                                                      ,(
                                                       SELECT CASE WHEN graduated.GraduatedProgram = ''Y'' THEN CONVERT(VARCHAR, graduated.ExpectedGradDate, 101)
                                                                   ELSE ''N/A''
                                                              END
                                                       ) AS DateGraduated
                                                      ,(
                                                       SELECT CASE WHEN graduated.GraduatedProgram = ''Y'' THEN
                                                                       CASE WHEN (
                                                                                 SELECT   TOP 1 Eligible
                                                                                 FROM     dbo.plExitInterview
                                                                                 WHERE    EnrollmentId = graduated.StuEnrollId
                                                                                 ORDER BY COALESCE(ExitInterviewDate, graduated.ExpectedGradDate) DESC
                                                                                 ) = ''Yes'' THEN ''E''
                                                                            ELSE ''I''
                                                                       END
                                                                   ELSE ''N/A''
                                                              END
                                                       ) AS PlacementStatus
                                                      ,(
                                                       SELECT   TOP 1 COALESCE(Reason, '''')
                                                       FROM     dbo.plExitInterview
                                                       WHERE    EnrollmentId = graduated.StuEnrollId
                                                       ORDER BY COALESCE(ExitInterviewDate, graduated.ExpectedGradDate) DESC
                                                       ) AS IneligibilityReason
                                                FROM   (
                                                       SELECT *
                                                             ,(
                                                              SELECT CASE WHEN didGraduate.Graduated = 0 THEN ''N''
                                                                          ELSE CASE WHEN didGraduate.OwesMoney = 1
                                                                                         AND didGraduate.AllowsMoneyOwed = 0 THEN ''N''
                                                                                    WHEN didGraduate.MissingRequired = 1
                                                                                         AND didGraduate.AllowsIncompleteReq = 0 THEN ''N''
                                                                                    ELSE ''Y''
                                                                               END
                                                                     END
                                                              ) AS GraduatedProgram
                                                       FROM   (
                                                              SELECT *
                                                                    ,(
                                                                     SELECT CASE WHEN @ReportType = @Preliminary THEN
                                                                                     CASE WHEN result.ExpectedGradDate < CONVERT(
                                                                                                                                    DATETIME
                                                                                                                                   ,CONVERT(
                                                                                                                                               VARCHAR(50)
                                                                                                                                              ,(( @ReportYear ) * 10000 + 3 * 100
                                                                                                                                                + 30
                                                                                                                                               )
                                                                                                                                           )
                                                                                                                                   ,112
                                                                                                                                ) THEN 1
                                                                                          ELSE 0
                                                                                     END
                                                                                 ELSE
                                                                                     CASE WHEN result.ExpectedGradDate < CONVERT(
                                                                                                                                    DATETIME
                                                                                                                                   ,CONVERT(
                                                                                                                                               VARCHAR(50)
                                                                                                                                              ,(( @ReportYear ) * 10000 + 11 * 100
                                                                                                                                                + 30
                                                                                                                                               )
                                                                                                                                           )
                                                                                                                                   ,112
                                                                                                                                ) THEN 1
                                                                                          ELSE 0
                                                                                     END
                                                                            END
                                                                     ) AS Graduated
                                                                    ,dbo.DoesEnrollmentHavePendingBalance(result.StuEnrollId) AS OwesMoney
                                                                    ,~ ( dbo.CompletedRequiredCourses(result.StuEnrollId)) AS MissingRequired
                                                              FROM   (
                                                                     SELECT *
                                                                     FROM   A
                                                                     UNION
                                                                     SELECT *
                                                                     FROM   B
                                                                     UNION
                                                                     SELECT *
                                                                     FROM   C
                                                                     UNION
                                                                     SELECT *
                                                                     FROM   D
                                                                     UNION
                                                                     SELECT *
                                                                     FROM   E
                                                                     ) AS result
                                                              WHERE  (
                                                                     @PrgIdList IS NULL
                                                                     OR result.PrgVerID IN (
                                                                                           SELECT Val
                                                                                           FROM   MultipleValuesForReportParameters(@PrgIdList, '','', 1)
                                                                                           )
                                                                     )
                                                              ) didGraduate
                                                       ) graduated
                                                ) placementStat
                                         ) placed
                                  ) satForExam
                           ) allData
                 LEFT JOIN dbo.PlStudentsPlaced psp ON psp.StuEnrollId = allData.StuEnrollId
                 LEFT JOIN dbo.plEmployerJobs pej ON pej.EmployerJobId = psp.EmployerJobId
                 LEFT JOIN dbo.plEmployers pemp ON pemp.EmployerId = pej.EmployerId
                 LEFT JOIN dbo.syStates states ON states.StateId = pemp.StateId
                 ) dataWithNumOfJobs
        WHERE    dataWithNumOfJobs.numOfJobs = 1
        ORDER BY dataWithNumOfJobs.PrgVerDescrip
                ,dataWithNumOfJobs.Student;



    END;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_PR_Main_DBConfigs]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Main_DBConfigs]', 'P') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[Usp_PR_Main_DBConfigs]
    @CampusId UNIQUEIDENTIFIER
   ,@EnrollmentId UNIQUEIDENTIFIER = 0x0
AS
    BEGIN
        DECLARE @VaribleTable TABLE
            (
                SchoolName VARCHAR(1000)
               ,DisplayAttendanceUnitForProgressReportByClass VARCHAR(1000)
               ,StudentIdentifier VARCHAR(1000)
               ,GPAMethod VARCHAR(1000)
               ,GradesFormat VARCHAR(1000)
               ,TrackSapAttendance VARCHAR(1000)
               ,GradeBookWeightingLevel VARCHAR(1000)
            );

        INSERT @VaribleTable (
                             SchoolName
                            ,DisplayAttendanceUnitForProgressReportByClass
                            ,StudentIdentifier
                            ,GPAMethod
                            ,GradesFormat
                            ,TrackSapAttendance
                            ,GradeBookWeightingLevel
                             )
        VALUES (
        -- SchoolName - varchar(1000)
        ISNULL((
               SELECT RTRIM(LTRIM(SchoolName))
               FROM   dbo.syCampuses
               WHERE  CampusId = @CampusId
               )
              ,(
               SELECT RTRIM(LTRIM(C.SchoolName))
               FROM   dbo.syCampuses C
               WHERE  C.CampusId IN (
                                    SELECT E.CampusId
                                    FROM   dbo.arStuEnrollments E
                                    WHERE  E.StuEnrollId = @EnrollmentId
                                    )
               )
              )
       -- DisplayAttendanceUnitForProgressReportByClass - varchar(1000)   -- Display Hours
       ,( dbo.GetAppSettingValueByKeyName(''DisplayAttendanceUnitForProgressReportByClass'', @CampusId))
       -- StudentIdentifier - varchar(1000)
       ,( dbo.GetAppSettingValueByKeyName(''StudentIdentifier'', @CampusId))
       -- GPAMethod - varchar(1000)
       ,( dbo.GetAppSettingValueByKeyName(''GPAMethod'', @CampusId))
       -- GradesFormat - varchar(1000)
       ,( dbo.GetAppSettingValueByKeyName(''GradesFormat'', @CampusId))
       -- TrackSapAttendance - varchar(1000)
       ,( dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', @CampusId))
       -- SetGradeBookAt - varchar(1000)
       ,( dbo.GetAppSettingValueByKeyName(''GradeBookWeightingLevel'', @CampusId)));

        SELECT TOP 1 VT.SchoolName
                    ,VT.DisplayAttendanceUnitForProgressReportByClass
                    ,VT.StudentIdentifier
                    ,VT.GPAMethod
                    ,VT.GradesFormat
                    ,VT.TrackSapAttendance
                    ,VT.GradeBookWeightingLevel
        FROM   @VaribleTable AS VT;

    END;






'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[syCustomFeatureFileConfiguration]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_syCustomFeatureFileConfiguration_syCampusFileConfiguration_CampusFileConfigurationId_CampusFileConfigurationId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[syCustomFeatureFileConfiguration]', 'U'))
ALTER TABLE [dbo].[syCustomFeatureFileConfiguration] ADD CONSTRAINT [FK_syCustomFeatureFileConfiguration_syCampusFileConfiguration_CampusFileConfigurationId_CampusFileConfigurationId] FOREIGN KEY ([CampusConfigurationId]) REFERENCES [dbo].[syCampusFileConfiguration] ([CampusFileConfigurationId])
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_syCustomFeatureFileConfiguration_syFeatureFileConfiguration_FeatureId_FeatureId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[syCustomFeatureFileConfiguration]', 'U'))
ALTER TABLE [dbo].[syCustomFeatureFileConfiguration] ADD CONSTRAINT [FK_syCustomFeatureFileConfiguration_syFeatureFileConfiguration_FeatureId_FeatureId] FOREIGN KEY ([FeatureId]) REFERENCES [dbo].[syFileConfigurationFeatures] ([FeatureId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[syCampusFileConfiguration]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_syCampusFileConfiguration_syCampGrps_CampGrpId_CampGrpId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[syCampusFileConfiguration]', 'U'))
ALTER TABLE [dbo].[syCampusFileConfiguration] ADD CONSTRAINT [FK_syCampusFileConfiguration_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampusGroupId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[syFileConfigurationFeatures]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_syFileConfigurationFeatures_syStatuses_StatusId_StatusId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[syFileConfigurationFeatures]', 'U'))
ALTER TABLE [dbo].[syFileConfigurationFeatures] ADD CONSTRAINT [FK_syFileConfigurationFeatures_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
