﻿--=================================================================================================
-- START AD-16460 : Set Ar Result Scores to Zero for Future Start students
--=================================================================================================

DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION ArResultZeroScores;
BEGIN TRY
    DECLARE @status INT;
    SET @status = (
                  SELECT SysStatusId
                  FROM   dbo.sySysStatus
                  WHERE  SysStatusDescrip = 'Future Start'
                  );

    IF ( @status > 0 )
        BEGIN
            UPDATE     ar
            SET        ar.Score = NULL
            FROM       dbo.arStuEnrollments enrollments
            JOIN       dbo.arStudent art ON art.StudentId = enrollments.StudentId
            INNER JOIN dbo.syStatusCodes statuses ON statuses.StatusCodeId = enrollments.StatusCodeId
            INNER JOIN dbo.sySysStatus advStatuses ON advStatuses.SysStatusId = statuses.SysStatusId
            JOIN       dbo.arResults ar ON ar.StuEnrollId = enrollments.StuEnrollId
            JOIN       dbo.arGrdBkResults gbr ON gbr.StuEnrollId = enrollments.StuEnrollId
            WHERE      ar.Score = 0
                       AND advStatuses.SysStatusId = @status
                       AND gbr.Score IS NULL
                       AND gbr.PostDate IS NULL;

        END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION ArResultZeroScores;
        PRINT 'Failed to Set Ar Result Scores to Zero for Future Start students';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION ArResultZeroScores;
        PRINT 'Set Ar Result Scores to Zero for Future Start students';
    END;
GO
--=================================================================================================
-- END  AD-16460 : Tricoci - Cannot No Start Student
--=================================================================================================
--=================================================================================================
-- START AD-16516 TimeClock Automation
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION AD16516;
BEGIN TRY

    --update file configuration features
    IF EXISTS (
              SELECT *
              FROM   dbo.syFileConfigurationFeatures
              WHERE  FeatureCode = 'TC'
              )
        BEGIN
            UPDATE dbo.syFileConfigurationFeatures
            SET    FeatureCode = 'TCA'
                  ,FeatureDescription = 'Time Clock Archive'
            WHERE  FeatureCode = 'TC';
        END;

    IF NOT EXISTS (
                  SELECT *
                  FROM   dbo.syFileConfigurationFeatures
                  WHERE  FeatureCode = 'TCI'
                  )
        BEGIN
            INSERT INTO dbo.syFileConfigurationFeatures (
                                                        FeatureId
                                                       ,FeatureCode
                                                       ,FeatureDescription
                                                       ,StatusId
                                                        )
            VALUES ( 6, 'TCI'                               -- FeatureCode - varchar(50)
                    ,'Time Clock Import'                    -- FeatureDescription - varchar(100)
                    ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                );
        END;

    --insert new config setting to turn on automatic time clock imports per campus 
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syConfigAppSettings
                  WHERE  KeyName = 'AutomatedTimeClockImport'
                  )
        BEGIN
            DECLARE @settingId INT;
            DECLARE @modDate DATETIME;
            SET @modDate = GETDATE();

            SET @settingId = (
                             SELECT MAX(SettingId) + 1
                             FROM   dbo.syConfigAppSetValues
                             );

            SET IDENTITY_INSERT dbo.syConfigAppSettings ON;

            INSERT INTO syConfigAppSettings (
                                            KeyName
                                           ,Description
                                           ,ModUser
                                           ,ModDate
                                           ,CampusSpecific
                                           ,ExtraConfirmation
                                           ,SettingId
                                            )
            VALUES ( 'AutomatedTimeClockImport'                                             -- KeyName - varchar(200)
                    ,'Yes/No campus specific value to enable automatic time clock imports.' -- Description - varchar(1000)
                    ,'Support'                                                              -- ModUser - varchar(50)
                    ,@modDate                                                               -- ModDate - datetime
                    ,1                                                                      -- CampusSpecific - bit
                    ,0, @settingId                                                          -- ExtraConfirmation - bit
                );

            SET IDENTITY_INSERT dbo.syConfigAppSettings OFF;

            SET @settingId = (
                             SELECT SettingId
                             FROM   syConfigAppSettings
                             WHERE  KeyName = 'AutomatedTimeClockImport'
                             );

            INSERT INTO syConfigAppSet_Lookup (
                                              LookUpId
                                             ,SettingId
                                             ,ValueOptions
                                             ,ModUser
                                             ,ModDate
                                              )
            VALUES ( NEWID()    -- LookUpId - uniqueidentifier
                    ,@settingId -- SettingId - int
                    ,'No'       -- ValueOptions - varchar(50)
                    ,'Support'  -- ModUser - varchar(50)
                    ,@modDate   -- ModDate - datetime
                );
            INSERT INTO syConfigAppSet_Lookup (
                                              LookUpId
                                             ,SettingId
                                             ,ValueOptions
                                             ,ModUser
                                             ,ModDate
                                              )
            VALUES ( NEWID()    -- LookUpId - uniqueidentifier
                    ,@settingId -- SettingId - int
                    ,'Yes'      -- ValueOptions - varchar(50)
                    ,'Support'  -- ModUser - varchar(50)
                    ,@modDate   -- ModDate - datetime
                );
            INSERT INTO syConfigAppSetValues (
                                             ValueId
                                            ,SettingId
                                            ,CampusId
                                            ,Value
                                            ,ModUser
                                            ,ModDate
                                            ,Active
                                             )
            VALUES ( NEWID()    -- ValueId - uniqueidentifier
                    ,@settingId -- SettingId - int
                    ,NULL       -- CampusId - uniqueidentifier
                    ,'No'       -- Value - varchar(1000)
                    ,'Support'  -- ModUser - varchar(50)
                    ,@modDate   -- ModDate - datetime
                    ,1          -- Active - bit
                );
        END;

    --insert new service settings

    IF ( NOT EXISTS (
                    SELECT *
                    FROM   dbo.syWapiExternalOperationMode
                    WHERE  Code = 'TC_FILEWATCH_SERVICE'
                    )
       )
        BEGIN
            INSERT INTO dbo.syWapiExternalOperationMode (
                                                        Code
                                                       ,Description
                                                       ,IsActive
                                                        )
            VALUES ( 'TC_FILEWATCH_SERVICE', 'This operation is for filewatching automated time clock imports paths', 1 );
        END;

    DECLARE @ExternalOperationId INT = (
                                       SELECT Id
                                       FROM   dbo.syWapiExternalOperationMode
                                       WHERE  Code = 'TC_FILEWATCH_SERVICE'
                                       );

    --reuse advantage api 
    DECLARE @AllowedServiceId INT = (
                                    SELECT Id
                                    FROM   dbo.syWapiAllowedServices
                                    WHERE  Code = 'ADVANTAGE_API'
                                    );

    DECLARE @ExternalCompanyId INT = (
                                     SELECT IdExtCompany
                                     FROM   dbo.syWapiSettings
                                     WHERE  CodeOperation = 'ADVANTAGE_API'
                                     );

    IF ( NOT EXISTS (
                    SELECT *
                    FROM   dbo.syWapiSettings
                    WHERE  CodeOperation = 'TC_FILEWATCH_SERVICE'
                    )
       )
        BEGIN
            INSERT INTO dbo.syWapiSettings (
                                           CodeOperation
                                          ,ExternalUrl
                                          ,IdExtCompany
                                          ,IdExtOperation
                                          ,IdAllowedService
                                          ,IdSecondAllowedService
                                          ,FirstAllowedServiceQueryString
                                          ,SecondAllowedServiceQueryString
                                          ,ConsumerKey
                                          ,PrivateKey
                                          ,OperationSecondTimeInterval
                                          ,PollSecondForOnDemandOperation
                                          ,FlagOnDemandOperation
                                          ,FlagRefreshConfiguration
                                          ,IsActive
                                          ,DateMod
                                          ,DateLastExecution
                                          ,UserMod
                                          ,UserName
                                           )
            VALUES ( 'TC_FILEWATCH_SERVICE', 'http://localhost/Advantage/Current/Site/ServiceMethods.aspx/', @ExternalCompanyId, @ExternalOperationId
                    ,@AllowedServiceId, NULL, NULL, '', '', '', 36000000, 100000, 0, 0, 0, GETDATE(), N'2019-04-11T10:14:45', 'SUPPORT', '' );
        END;


END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION AD16516;
        PRINT 'Failed transaction AD16516.';
    END;
ELSE
    BEGIN
        COMMIT TRANSACTION AD16516;
        PRINT 'successful transaction AD16516.';
    END;
GO
--=================================================================================================
-- END AD-16516 TimeClock Automation
--=================================================================================================