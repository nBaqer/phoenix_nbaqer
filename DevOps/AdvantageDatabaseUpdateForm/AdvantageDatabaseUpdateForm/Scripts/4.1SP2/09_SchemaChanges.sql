﻿/*
Run this script on:

        dev-com-db1\Adv.AvedaLive    -  This database will be modified

to synchronize it with a database with the schema represented by:

        Source

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.7.10021 from Red Gate Software Ltd at 10/1/2019 6:20:56 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
/*
* Use this Pre-Deployment script to perform tasks before the deployment of the project.
* Read more at https://www.red-gate.com/SOC7/pre-deployment-script-help
*/
UPDATE dbo.arClsSectMeetings
SET PeriodId = NULL
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)

DELETE FROM syPeriodsWorkDays
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[arStuEnrollments_Integration_Tracking] from [dbo].[arStuEnrollments]'
GO
IF OBJECT_ID(N'[dbo].[arStuEnrollments_Integration_Tracking]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[arStuEnrollments_Integration_Tracking]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[usp_GetStudentLedgerReportSummary]'
GO
IF OBJECT_ID(N'[dbo].[usp_GetStudentLedgerReportSummary]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[usp_GetStudentLedgerReportSummary]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_TitleIV_GetTriggerMetDate]'
GO
IF OBJECT_ID(N'[dbo].[USP_TitleIV_GetTriggerMetDate]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_TitleIV_GetTriggerMetDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_UpdateFutureStartsToCurrentlyAttending]'
GO
IF OBJECT_ID(N'[dbo].[USP_UpdateFutureStartsToCurrentlyAttending]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_UpdateFutureStartsToCurrentlyAttending]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_FA_9010DataExport]'
GO
IF OBJECT_ID(N'[dbo].[USP_FA_9010DataExport]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_FA_9010DataExport]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[IntegrationEnrollmentTracking]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[IntegrationEnrollmentTracking]', N'RetryCount') IS NULL
ALTER TABLE [dbo].[IntegrationEnrollmentTracking] ADD[RetryCount] [int] NOT NULL CONSTRAINT [DF_IntegrationEnrollmentTracking_RetryCount] DEFAULT ((0))
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudent]'
GO
IF OBJECT_ID(N'[dbo].[arStudent]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[arStuEnrollments_Integration_Tracking] on [dbo].[arStuEnrollments]'
GO
IF OBJECT_ID(N'[dbo].[arStuEnrollments_Integration_Tracking]', 'TR') IS NULL
EXEC sp_executesql N'--BEGIN	
--CREATE TABLE dbo.IntegrationEnrollmentTracking
--(
-- EnrollmentTrackingId UNIQUEIDENTIFIER CONSTRAINT [PK_IntegrationEnrollmentTracking_EnrollmentTrackingId] PRIMARY KEY CLUSTERED 
-- CONSTRAINT [DF_IntegrationEnrollmentTracking_EnrollmentTrackingId] DEFAULT NEWID(),
-- StuEnrollId UNIQUEIDENTIFIER CONSTRAINT[FK_IntegrationEnrollmentTracking_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY REFERENCES dbo.arStuEnrollments NOT NULL,
--  EnrollmentStatus VARCHAR(100),
-- ModDate DATETIME NOT NULL ,
-- ModUser NVARCHAR(100),
-- Processed BIT CONSTRAINT [DF_IntegrationEnrollmentTracking_Processed] DEFAULT 0 NOT NULL 	

--)
--END
--DROP TABLE dbo.IntegrationEnrollmentTracking
CREATE TRIGGER [dbo].[arStuEnrollments_Integration_Tracking]
ON [dbo].[arStuEnrollments]
AFTER UPDATE
AS
DECLARE @INS INT
       ,@DEL INT;

SELECT @INS = COUNT(*)
FROM   INSERTED;
SELECT @DEL = COUNT(*)
FROM   DELETED;

IF @INS > 0
   AND @DEL > 0
    BEGIN
        SET NOCOUNT ON;

        IF UPDATE(StatusCodeId)
           OR UPDATE(LDA)
            BEGIN
                IF EXISTS (
                          SELECT     1
                          FROM       INSERTED i
                          INNER JOIN DELETED d ON d.StuEnrollId = i.StuEnrollId
                          WHERE      ( i.StatusCodeId <> d.StatusCodeId )
                                     OR ( i.LDA <> d.LDA )
                          )
                    BEGIN
                        IF EXISTS (
                                  SELECT     1
                                  FROM       dbo.IntegrationEnrollmentTracking IET
                                  INNER JOIN inserted i ON i.StuEnrollId = IET.StuEnrollId
                                  )
                            BEGIN


                                UPDATE     IET
                                SET        EnrollmentStatus = ss.SysStatusDescrip
                                          ,ModDate = i.ModDate
                                          ,ModUser = i.ModUser
                                          ,Processed = 0
                                          ,RetryCount = 0
                                FROM       dbo.IntegrationEnrollmentTracking IET
                                INNER JOIN INSERTED i ON i.StuEnrollId = IET.StuEnrollId
                                INNER JOIN dbo.arStuEnrollments se ON i.StuEnrollId = se.StuEnrollId
                                INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                INNER JOIN dbo.sySysStatus ss ON ss.SysStatusId = sc.SysStatusId
                                INNER JOIN dbo.adLeads l ON l.LeadId = se.LeadId
                                WHERE      IET.StuEnrollId = i.StuEnrollId
                                           AND l.AfaStudentId IS NOT NULL;
                            END;


                        INSERT INTO dbo.IntegrationEnrollmentTracking (
                                                                      EnrollmentTrackingId
                                                                     ,StuEnrollId
                                                                     ,EnrollmentStatus
                                                                     ,ModDate
                                                                     ,ModUser
                                                                     ,Processed
                                                                      )
                                    SELECT     NEWID()
                                              ,i.StuEnrollId
                                              ,ss.SysStatusDescrip
                                              ,i.ModDate
                                              ,i.ModUser
                                              ,0
                                    FROM       INSERTED i
                                    INNER JOIN dbo.arStuEnrollments se ON i.StuEnrollId = se.StuEnrollId
                                    INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                    INNER JOIN dbo.sySysStatus ss ON ss.SysStatusId = sc.SysStatusId
                                    INNER JOIN dbo.adLeads l ON l.LeadId = se.LeadId
                                    LEFT JOIN  dbo.IntegrationEnrollmentTracking iet ON iet.StuEnrollId = se.StuEnrollId
                                    WHERE      l.AfaStudentId IS NOT NULL
                                               AND iet.EnrollmentTrackingId IS NULL;



                    END;

            END;

        SET NOCOUNT OFF;

    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_FA_9010DataExport]'
GO
IF OBJECT_ID(N'[dbo].[USP_FA_9010DataExport]', 'P') IS NULL
EXEC sp_executesql N'-- =============================================
-- Author:		FAME Inc.
-- Create date: 1/7/2019
-- Description:	Generates data for 90/10 report export. Each sheet for excel export is returned as data table. Two tables returned, first one is sheet 1, second one sheet 2. 
--Referenced in :DataExport9010Service
-- =============================================
CREATE PROCEDURE [dbo].[USP_FA_9010DataExport]
    (
        @StuEnrollmentId UNIQUEIDENTIFIER
       ,@CampusIds VARCHAR(MAX)
       ,@FiscalYearStart DATETIME2
       ,@FiscalYearEnd DATETIME2
       ,@GroupIds VARCHAR(MAX)
       ,@ProgramIds VARCHAR(MAX)
       ,@ActivitiesConductedAmount DECIMAL(16, 2)
    )
AS
    BEGIN
        IF ( @CampusIds IS NULL )
            BEGIN
                SET @CampusIds = '''';
            END;

        IF ( @GroupIds IS NULL )
            BEGIN
                SET @GroupIds = ''''; -- default parameter value to empty
            END;

        IF ( @ProgramIds IS NULL )
            BEGIN
                SET @ProgramIds = ''''; -- default parameter value to empty
            END;

        -- declaring filter variables
        DECLARE @tblCampusIds TABLE
            (
                CampusId VARCHAR(50)
            );

        DECLARE @tblGroupIds TABLE
            (
                GroupId VARCHAR(50)
            );

        DECLARE @tblProgramIds TABLE
            (
                ProgramId VARCHAR(50)
            );

        DECLARE @IsAllGroups BIT = 1;
        DECLARE @IsAllPrograms BIT = 1;
        DECLARE @IsAllStudents BIT = 1;

        INSERT INTO @tblCampusIds (
                                  CampusId
                                  )
                    (SELECT strval
                     FROM   dbo.SPLIT(@CampusIds) );

        INSERT INTO @tblGroupIds (
                                 GroupId
                                 )
                    (SELECT strval
                     FROM   dbo.SPLIT(@GroupIds) );

        INSERT INTO @tblProgramIds (
                                   ProgramId
                                   )
                    (SELECT strval
                     FROM   dbo.SPLIT(@ProgramIds) );

        IF (
           @StuEnrollmentId IS NOT NULL
           AND @StuEnrollmentId <> ''00000000-0000-0000-0000-000000000000''
           ) --if not default guid or null, parameter was passed
            BEGIN
                SET @IsAllStudents = 0;
            END;

        IF NOT EXISTS (
                      SELECT TOP 1 GroupId
                      FROM   @tblGroupIds
                      WHERE  GroupId LIKE ''%All%''
                      ) --if ''all'' group is not found
            SET @IsAllGroups = 0;
        ELSE
            DELETE FROM @tblGroupIds;


        IF NOT EXISTS (
                      SELECT TOP 1 ProgramId
                      FROM   @tblProgramIds
                      WHERE  ProgramId LIKE ''%All%''
                      ) --if ''all'' program is not found
            SET @IsAllPrograms = 0;
        ELSE
            DELETE FROM @tblProgramIds;


        --************************************* BEGIN - List of enrollments********************************************
        DECLARE @EnrollmentList TABLE
            (
                RowNumber INT
               ,FirstName VARCHAR(50)
               ,MiddleName VARCHAR(50)
               ,LastName VARCHAR(50)
               ,StudentNumber NVARCHAR(50)
               ,PrgVerDescrip VARCHAR(50)
               ,StuEnrollId UNIQUEIDENTIFIER
               ,IsTitleIVProgram BIT
            );

        -- student list is all the students that had a transaction within the fiscal year and filtered by parameters
        INSERT INTO @EnrollmentList
                    SELECT   ROW_NUMBER() OVER ( ORDER BY ( StudentNumber )) AS RowNumber
                            ,FirstName
                            ,MiddleName
                            ,LastName
                            ,adLeads.StudentNumber
                            ,PrgVerDescrip
                            ,arStuEnrollments.StuEnrollId
                            ,(
                             SELECT TOP 1 IsTitleIV
                             FROM   dbo.arCampusPrgVersions
                             WHERE  arCampusPrgVersions.PrgVerId = arStuEnrollments.PrgVerId
                             ) AS IsTitleIVProgram
                    FROM     dbo.arStuEnrollments
                    JOIN     dbo.adLeads ON adLeads.LeadId = arStuEnrollments.LeadId
                    JOIN     dbo.arPrgVersions ON arPrgVersions.PrgVerId = dbo.arStuEnrollments.PrgVerId
                    JOIN     (
                             SELECT   StuEnrollId
                             FROM     dbo.saTransactions
                             WHERE    TransDate >= @FiscalYearStart
                                      AND TransDate <= @FiscalYearEnd
                             GROUP BY StuEnrollId
                             ) saTransactions ON saTransactions.StuEnrollId = arStuEnrollments.StuEnrollId
                    WHERE    adLeads.CampusId IN (
                                                 SELECT CampusId
                                                 FROM   @tblCampusIds
                                                 )
                             AND (
                                 @IsAllPrograms = 1 -- filter programs if not all
                                 OR dbo.arPrgVersions.ProgId IN (
                                                                SELECT ProgramId
                                                                FROM   @tblProgramIds
                                                                )
                                 )
                             AND (
                                 @IsAllGroups = 1 -- filter student groups if not all 
                                 OR LeadgrpId IN (
                                                 SELECT GroupId
                                                 FROM   @tblGroupIds
                                                 )
                                 )
                             AND (
                                 @IsAllStudents = 1
                                 OR arStuEnrollments.StuEnrollId = @StuEnrollmentId
                                 )
                    ORDER BY StudentNumber;

        --************************************* END - List of students********************************************

        --************************************* 9010 Mapping Id Variables********************************************

        DECLARE @OutsideGrantId UNIQUEIDENTIFIER = (
                                                   SELECT AwardType9010Id
                                                   FROM   dbo.syAwardTypes9010
                                                   WHERE  Name = ''Outside Grant''
                                                   );
        DECLARE @JobTrainingGrantId UNIQUEIDENTIFIER = (
                                                       SELECT AwardType9010Id
                                                       FROM   dbo.syAwardTypes9010
                                                       WHERE  Name = ''Job Training Grant''
                                                       );
        DECLARE @EducationalSavingsId UNIQUEIDENTIFIER = (
                                                         SELECT AwardType9010Id
                                                         FROM   dbo.syAwardTypes9010
                                                         WHERE  Name = ''Educational Savings''
                                                         );
        DECLARE @SchoolGrantId UNIQUEIDENTIFIER = (
                                                  SELECT AwardType9010Id
                                                  FROM   dbo.syAwardTypes9010
                                                  WHERE  Name = ''School Grant''
                                                  );

        DECLARE @TuitionDiscount9010Id UNIQUEIDENTIFIER = (
                                                          SELECT AwardType9010Id
                                                          FROM   dbo.syAwardTypes9010
                                                          WHERE  Name = ''Tuition Discount''
                                                          );

        DECLARE @NoneAwardId UNIQUEIDENTIFIER = (
                                                SELECT AwardType9010Id
                                                FROM   dbo.syAwardTypes9010
                                                WHERE  Name = ''None''
                                                );

        DECLARE @SchoolLoanAwardId UNIQUEIDENTIFIER = (
                                                      SELECT AwardType9010Id
                                                      FROM   dbo.syAwardTypes9010
                                                      WHERE  Name = ''School Loan''
                                                      );

        DECLARE @SEOGScholarshipMatchAwardId UNIQUEIDENTIFIER = (
                                                                SELECT AwardType9010Id
                                                                FROM   dbo.syAwardTypes9010
                                                                WHERE  Name = ''SEOGScholarshipMatch''
                                                                );

        DECLARE @OutsideLoanAwardId UNIQUEIDENTIFIER = (
                                                       SELECT AwardType9010Id
                                                       FROM   dbo.syAwardTypes9010
                                                       WHERE  Name = ''Outside Loan''
                                                       );

        DECLARE @StudentPayment9010MappingId UNIQUEIDENTIFIER = (
                                                                SELECT AwardType9010Id
                                                                FROM   dbo.syAwardTypes9010
                                                                WHERE  Name = ''Student Payment''
                                                                );

        DECLARE @StudentStipend9010MappingId UNIQUEIDENTIFIER = (
                                                                SELECT AwardType9010Id
                                                                FROM   dbo.syAwardTypes9010
                                                                WHERE  Name = ''Student Stipend''
                                                                );

        DECLARE @RefundToStudent9010MappingId UNIQUEIDENTIFIER = (
                                                                 SELECT AwardType9010Id
                                                                 FROM   dbo.syAwardTypes9010
                                                                 WHERE  Name = ''Refund to Student''
                                                                 );


        --*************************************END 9010 Mapping Id Variables********************************************

        --*******************************************Column Calculations ***********************************************

        --@TitleIVCreditBalance
        DECLARE @TitleIVCreditBalance TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @TitleIVCreditBalance -- negated instutional charge balance being used
                    SELECT StuEnrollId
                          ,0
                    FROM   @EnrollmentList;

        --@InstitutionalChargeBalance
        DECLARE @InstitutionalChargeBalance TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @InstitutionalChargeBalance
                    SELECT StuEnrollId
                          ,(
                           SELECT (
                                  SELECT    ISNULL(SUM(TransAmount), 0) AS InstitutionalChargeBalance
                                  FROM      dbo.saTransactions
                                  LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                  WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                            AND (
                                                (
                                                IsInstCharge = 1
                                                AND PaymentCodeId IS NULL
                                                )
                                                OR SysTransCodeId = 15
                                                )
                                            AND Voided = 0
                                            AND TransDate < @FiscalYearStart
                                  )
                                  - ((
                                     SELECT    ISNULL(SUM(TransAmount) * -1, 0) AS TitleIVFunds
                                     FROM      dbo.saTransactions
                                     LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                     WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                               AND TitleIV = 1
                                               AND Voided = 0
                                               AND TransDate < @FiscalYearStart
                                     )
                                     + (
                                       SELECT    ISNULL(SUM(TransAmount) * -1, 0) AS FirstAppliedAmount
                                       FROM      dbo.saTransactions
                                       LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                       JOIN      dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                       JOIN      dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                       WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                 AND Voided = 0
                                                 AND dbo.syAwardTypes9010.AwardType9010Id IN ( @SchoolGrantId, @OutsideGrantId, @JobTrainingGrantId
                                                                                              ,@EducationalSavingsId
                                                                                             )
                                                 AND TransDate < @FiscalYearStart
                                       ) + (
                                           SELECT    ISNULL( --add back tuition is counts here for previous year
                                                               SUM(TransAmount) * -1, 0
                                                           ) AS InstitutionalMaxChargeLimitThisYear
                                           FROM      dbo.saTransactions
                                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                           JOIN      dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                           JOIN      dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                           WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                     AND Voided = 0
                                                     AND dbo.syAwardTypes9010.AwardType9010Id IN ( @TuitionDiscount9010Id )
                                                     AND TransDate < @FiscalYearStart
                                           )
                                    ) AS InstitutionalChargeBalance
                           ) AS Amount
                    FROM   @EnrollmentList;

        --@InstitutionalMaxChargeLimitThisYear
        DECLARE @InstitutionalMaxChargeLimitThisYear TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @InstitutionalMaxChargeLimitThisYear
                    SELECT [@EnrollmentList].StuEnrollId
                          ,(
                           SELECT    ( ISNULL(SUM(TransAmount), 0))
                                     - (
                                       SELECT    ISNULL(SUM(TransAmount) * -1, 0) AS InstitutionalMaxChargeLimitThisYear
                                       FROM      dbo.saTransactions
                                       LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                       LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                       LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                       LEFT JOIN dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                       WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                 AND Voided = 0
                                                 AND dbo.syAwardTypes9010.AwardType9010Id IN ( @TuitionDiscount9010Id )
                                                 AND TransDate >= @FiscalYearStart
                                                 AND TransDate <= @FiscalYearEnd
                                                 AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                       ) AS InstitutionalMaxChargeLimitThisYear
                           FROM      dbo.saTransactions
                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                           WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                     AND Voided = 0
                                     AND (
                                         (
                                         IsInstCharge = 1
                                         AND PaymentCodeId IS NULL
                                         )
                                         OR SysTransCodeId = 15
                                         )
                                     AND TransDate >= @FiscalYearStart
                                     AND TransDate <= @FiscalYearEnd
                           ) AS Amount
                    FROM   @EnrollmentList;

        --@TotalInstitutionalMaxChargeLimit
        DECLARE @TotalInstitutionalMaxChargeLimit TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO @TotalInstitutionalMaxChargeLimit
                    SELECT StuEnrollId
                          ,( ''SUM(E'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '':F'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '')'' ) AS ExcelFormula
                    FROM   @EnrollmentList;

        --Sub Loan calculation
        DECLARE @SubLoan TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @SubLoan
                    SELECT [@EnrollmentList].StuEnrollId
                          ,(( CASE WHEN ( [@InstitutionalChargeBalance].Amount * -1 ) <= 0 THEN 0 -- if titleiv credit balance is less than or equal to 0 no carry over subloan
                                   WHEN ((
                                         SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan -- if subloan in previous year <= titleivcreditbalance add carry over to subloan of this year
                                         FROM      dbo.saTransactions
                                         LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                         LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                         LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                         LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                         WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                   AND RefundAmount IS NULL
                                                   AND TitleIV = 1
                                                   AND Voided = 0
                                                   AND Descrip IN ( ''DL - SUB'', ''FFEL - SUB'' )
                                                   AND TransDate < @FiscalYearStart
                                         ) <= ( [@InstitutionalChargeBalance].Amount * -1 )
                                        ) THEN (
                                               SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan -- subloan in previous year <= titleivcreditbalance
                                               FROM      dbo.saTransactions
                                               LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                               LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                               LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                               LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                               WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                         AND RefundAmount IS NULL
                                                         AND TitleIV = 1
                                                         AND Voided = 0
                                                         AND Descrip IN ( ''DL - SUB'', ''FFEL - SUB'' )
                                                         AND TransDate < @FiscalYearStart
                                               )
                                   ELSE ( [@InstitutionalChargeBalance].Amount * -1 )             -- title iv credit balance > 0 but less than subloan previous total, take all of credit iv credit balance as sub loan
                              END
                            ) + (
                                SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                FROM      dbo.saTransactions
                                LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                          AND RefundAmount IS NULL
                                          AND TitleIV = 1
                                          AND Voided = 0
                                          AND Descrip IN ( ''DL - SUB'', ''FFEL - SUB'' )
                                          AND TransDate >= @FiscalYearStart
                                          AND TransDate <= @FiscalYearEnd
                                )
                           ) AS Amount
                    FROM   @EnrollmentList
                    JOIN   @InstitutionalChargeBalance ON [@InstitutionalChargeBalance].StuEnrollId = [@EnrollmentList].StuEnrollId;


        --Unsub Loan calculation
        DECLARE @UnSubLoan TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @UnSubLoan
                    SELECT [@EnrollmentList].StuEnrollId
                          ,(( CASE WHEN (( [@InstitutionalChargeBalance].Amount * -1 )
                                         - (
                                           SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan -- subloan in previous year <= titleivcreditbalance
                                           FROM      dbo.saTransactions
                                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                           LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                           LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                           WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                     AND RefundAmount IS NULL
                                                     AND TitleIV = 1
                                                     AND Voided = 0
                                                     AND Descrip IN ( ''DL - SUB'', ''FFEL - SUB'' )
                                                     AND TransDate < @FiscalYearStart
                                           )
                                        ) <= 0 THEN 0
                                   WHEN ((
                                         SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) -- Subloan + Unsubloan previous <= titleivcreditbalance then take full unsub
                                         FROM      dbo.saTransactions
                                         LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                         LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                         LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                         LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                         WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                   AND RefundAmount IS NULL
                                                   AND TitleIV = 1
                                                   AND Voided = 0
                                                   AND Descrip IN ( ''DL - SUB'', ''FFEL - SUB'', ''DL - UNSUB'', ''FFEL - UNSUB'', ''DL – Additional UNSUB''
                                                                   ,''FFEL – Additional UNSUB''
                                                                  )
                                                   AND TransDate < @FiscalYearStart
                                         ) <= ( [@InstitutionalChargeBalance].Amount * -1 )
                                        ) THEN (
                                               SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                               FROM      dbo.saTransactions
                                               LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                               LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                               LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                               LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                               WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                         AND RefundAmount IS NULL
                                                         AND TitleIV = 1
                                                         AND Voided = 0
                                                         AND Descrip IN ( ''DL - UNSUB'', ''FFEL - UNSUB'', ''DL – Additional UNSUB'', ''FFEL – Additional UNSUB'' )
                                                         AND TransDate < @FiscalYearStart
                                               )
                                   ELSE -- TitleIvCreditBalance - subloan  = amount of unsub to carry over
                          (( [@InstitutionalChargeBalance].Amount * -1 )
                           - (
                             SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan -- subloan in previous year <= titleivcreditbalance
                             FROM      dbo.saTransactions
                             LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                             LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                             LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                             LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                             WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                       AND RefundAmount IS NULL
                                       AND TitleIV = 1
                                       AND Voided = 0
                                       AND Descrip IN ( ''DL - SUB'', ''FFEL - SUB'' )
                                       AND TransDate < @FiscalYearStart
                             )
                          ) -- title iv credit balance > 0 but less than subloan previous total, take all of credit iv credit balance as sub loan
                              END
                            ) + (
                                SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                FROM      dbo.saTransactions
                                LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                          AND RefundAmount IS NULL
                                          AND TitleIV = 1
                                          AND Voided = 0
                                          AND Descrip IN ( ''DL - UNSUB'', ''FFEL - UNSUB'', ''DL – Additional UNSUB'', ''FFEL – Additional UNSUB'' )
                                          AND TransDate >= @FiscalYearStart
                                          AND TransDate <= @FiscalYearEnd
                                )
                           ) AS Amount
                    FROM   @EnrollmentList
                    JOIN   @InstitutionalChargeBalance ON [@InstitutionalChargeBalance].StuEnrollId = [@EnrollmentList].StuEnrollId;

        --Plus calculation
        DECLARE @Plus TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @Plus
                    SELECT [@EnrollmentList].StuEnrollId
                          ,(( CASE WHEN (( [@InstitutionalChargeBalance].Amount * -1 )
                                         - (
                                           SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan -- subloan in previous year <= titleivcreditbalance
                                           FROM      dbo.saTransactions
                                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                           LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                           LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                           WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                     AND RefundAmount IS NULL
                                                     AND TitleIV = 1
                                                     AND Voided = 0
                                                     AND Descrip IN ( ''DL - SUB'', ''FFEL - SUB'', ''DL - UNSUB'', ''FFEL - UNSUB'', ''DL – Additional UNSUB''
                                                                     ,''FFEL – Additional UNSUB''
                                                                    )
                                                     AND TransDate < @FiscalYearStart
                                           )
                                        ) <= 0 THEN 0
                                   WHEN ((
                                         SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) -- Subloan + Unsubloan + plus  <= titleivcreditbalance then take full plus
                                         FROM      dbo.saTransactions
                                         LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                         LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                         LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                         LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                         WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                   AND RefundAmount IS NULL
                                                   AND TitleIV = 1
                                                   AND Voided = 0
                                                   AND Descrip IN ( ''DL - SUB'', ''FFEL - SUB'', ''DL - UNSUB'', ''FFEL - UNSUB'', ''DL – Additional UNSUB''
                                                                   ,''FFEL – Additional UNSUB'', ''DL - PLUS'', ''FFEL - PLUS''
                                                                  )
                                                   AND TransDate < @FiscalYearStart
                                         ) <= ( [@InstitutionalChargeBalance].Amount * -1 )
                                        ) THEN (
                                               SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                               FROM      dbo.saTransactions
                                               LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                               LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                               LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                               LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                               WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                         AND RefundAmount IS NULL
                                                         AND TitleIV = 1
                                                         AND Voided = 0
                                                         AND Descrip IN ( ''DL - PLUS'', ''FFEL - PLUS'' )
                                                         AND TransDate < @FiscalYearStart
                                               )
                                   ELSE -- TitleIvCreditBalance - sub, unsub  = amount of unsub to carry over
                          (( [@InstitutionalChargeBalance].Amount * -1 )
                           - (
                             SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan -- subloan in previous year <= titleivcreditbalance
                             FROM      dbo.saTransactions
                             LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                             LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                             LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                             LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                             WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                       AND RefundAmount IS NULL
                                       AND TitleIV = 1
                                       AND Voided = 0
                                       AND Descrip IN ( ''DL - SUB'', ''FFEL - SUB'', ''DL - UNSUB'', ''FFEL - UNSUB'', ''DL – Additional UNSUB'', ''FFEL – Additional UNSUB'' )
                                       AND TransDate < @FiscalYearStart
                             )
                          ) -- title iv credit balance > 0 but less than subloan + sunsub previous total, take all of credit iv credit balance as plus loan
                              END
                            ) + (
                                SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                FROM      dbo.saTransactions
                                LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                          AND RefundAmount IS NULL
                                          AND TitleIV = 1
                                          AND Voided = 0
                                          AND Descrip IN ( ''DL - PLUS'', ''FFEL - PLUS'' )
                                          AND TransDate >= @FiscalYearStart
                                          AND TransDate <= @FiscalYearEnd
                                )
                           ) AS Amount
                    FROM   @EnrollmentList
                    JOIN   @InstitutionalChargeBalance ON [@InstitutionalChargeBalance].StuEnrollId = [@EnrollmentList].StuEnrollId;


        --Pell grant calculation
        DECLARE @PellGrant TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @PellGrant
                    SELECT [@EnrollmentList].StuEnrollId
                          ,(( CASE WHEN (( [@InstitutionalChargeBalance].Amount * -1 )
                                         - (
                                           SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                           FROM      dbo.saTransactions
                                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                           LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                           LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                           WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                     AND RefundAmount IS NULL
                                                     AND TitleIV = 1
                                                     AND Voided = 0
                                                     AND Descrip IN ( ''DL - SUB'', ''FFEL - SUB'', ''DL - UNSUB'', ''FFEL - UNSUB'', ''DL – Additional UNSUB''
                                                                     ,''FFEL – Additional UNSUB'', ''DL - PLUS'', ''FFEL - PLUS''
                                                                    )
                                                     AND TransDate < @FiscalYearStart
                                           )
                                        ) <= 0 THEN 0
                                   WHEN ((
                                         SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) -- Subloan + Unsubloan + plus + pell  <= titleivcreditbalance then take full pell
                                         FROM      dbo.saTransactions
                                         LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                         LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                         LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                         LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                         WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                   AND RefundAmount IS NULL
                                                   AND TitleIV = 1
                                                   AND Voided = 0
                                                   AND Descrip IN ( ''DL - SUB'', ''FFEL - SUB'', ''DL - UNSUB'', ''FFEL - UNSUB'', ''DL – Additional UNSUB''
                                                                   ,''FFEL – Additional UNSUB'', ''DL - PLUS'', ''FFEL - PLUS'', ''PELL''
                                                                  )
                                                   AND TransDate < @FiscalYearStart
                                         ) <= ( [@InstitutionalChargeBalance].Amount * -1 )
                                        ) THEN (
                                               SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                               FROM      dbo.saTransactions
                                               LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                               LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                               LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                               LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                               WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                         AND RefundAmount IS NULL
                                                         AND TitleIV = 1
                                                         AND Voided = 0
                                                         AND Descrip IN ( ''PELL'' )
                                                         AND TransDate < @FiscalYearStart
                                               )
                                   ELSE -- TitleIvCreditBalance - sub, unsub, plus  = amount of pell to carry over
                          (( [@InstitutionalChargeBalance].Amount * -1 )
                           - (
                             SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                             FROM      dbo.saTransactions
                             LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                             LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                             LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                             LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                             WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                       AND RefundAmount IS NULL
                                       AND TitleIV = 1
                                       AND Voided = 0
                                       AND Descrip IN ( ''DL - SUB'', ''FFEL - SUB'', ''DL - UNSUB'', ''FFEL - UNSUB'', ''DL – Additional UNSUB'', ''FFEL – Additional UNSUB''
                                                       ,''DL - PLUS'', ''FFEL - PLUS''
                                                      )
                                       AND TransDate < @FiscalYearStart
                             )
                          )
                              END
                            ) + (
                                SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                FROM      dbo.saTransactions
                                LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                          AND TitleIV = 1
                                          AND Voided = 0
                                          AND RefundAmount IS NULL
                                          AND Descrip IN ( ''PELL'' )
                                          AND TransDate >= @FiscalYearStart
                                          AND TransDate <= @FiscalYearEnd
                                )
                           ) AS Amount
                    FROM   @EnrollmentList
                    JOIN   @InstitutionalChargeBalance ON [@InstitutionalChargeBalance].StuEnrollId = [@EnrollmentList].StuEnrollId;

        --FSEOG  calculation
        DECLARE @FSEOG TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @FSEOG
                    SELECT [@EnrollmentList].StuEnrollId
                          ,(( CASE WHEN (( [@InstitutionalChargeBalance].Amount * -1 )
                                         - (
                                           SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                           FROM      dbo.saTransactions
                                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                           LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                           LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                           WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                     AND RefundAmount IS NULL
                                                     AND TitleIV = 1
                                                     AND Voided = 0
                                                     AND Descrip IN ( ''DL - SUB'', ''FFEL - SUB'', ''DL - UNSUB'', ''FFEL - UNSUB'', ''DL – Additional UNSUB''
                                                                     ,''FFEL – Additional UNSUB'', ''DL - PLUS'', ''FFEL - PLUS'', ''PELL''
                                                                    )
                                                     AND TransDate < @FiscalYearStart
                                           )
                                        ) <= 0 THEN 0
                                   WHEN ((
                                         SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) -- Subloan + Unsubloan + plus + pell + SEOG  <= titleivcreditbalance then take full SEOG
                                         FROM      dbo.saTransactions
                                         LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                         LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                         LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                         LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                         WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                   AND RefundAmount IS NULL
                                                   AND TitleIV = 1
                                                   AND Voided = 0
                                                   AND Descrip IN ( ''DL - SUB'', ''FFEL - SUB'', ''DL - UNSUB'', ''FFEL - UNSUB'', ''DL – Additional UNSUB''
                                                                   ,''FFEL – Additional UNSUB'', ''DL - PLUS'', ''FFEL - PLUS'', ''PELL'', ''SEOG''
                                                                  )
                                                   AND TransDate < @FiscalYearStart
                                         ) <= ( [@InstitutionalChargeBalance].Amount * -1 )
                                        ) THEN (
                                               SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                               FROM      dbo.saTransactions
                                               LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                               LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                               LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                               LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                               WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                         AND RefundAmount IS NULL
                                                         AND TitleIV = 1
                                                         AND Voided = 0
                                                         AND Descrip IN ( ''SEOG'' )
                                                         AND TransDate < @FiscalYearStart
                                               )
                                   ELSE -- TitleIvCreditBalance - SUM(sub, unsub, plus, pell)  = amount of SEOG to carry over
                          (( [@InstitutionalChargeBalance].Amount * -1 )
                           - (
                             SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                             FROM      dbo.saTransactions
                             LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                             LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                             LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                             LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                             WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                       AND RefundAmount IS NULL
                                       AND TitleIV = 1
                                       AND Voided = 0
                                       AND Descrip IN ( ''DL - SUB'', ''FFEL - SUB'', ''DL - UNSUB'', ''FFEL - UNSUB'', ''DL – Additional UNSUB'', ''FFEL – Additional UNSUB''
                                                       ,''DL - PLUS'', ''FFEL - PLUS'', ''PELL''
                                                      )
                                       AND TransDate < @FiscalYearStart
                             )
                          )
                              END
                            ) + (
                                SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                FROM      dbo.saTransactions
                                LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                          AND RefundAmount IS NULL
                                          AND TitleIV = 1
                                          AND Voided = 0
                                          AND Descrip IN ( ''SEOG'' )
                                          AND TransDate >= @FiscalYearStart
                                          AND TransDate <= @FiscalYearEnd
                                )
                           ) AS Amount
                    FROM   @EnrollmentList
                    JOIN   @InstitutionalChargeBalance ON [@InstitutionalChargeBalance].StuEnrollId = [@EnrollmentList].StuEnrollId;

        --FWS  calculation
        DECLARE @FWS TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @FWS
                    SELECT [@EnrollmentList].StuEnrollId
                          ,(( CASE WHEN (( [@InstitutionalChargeBalance].Amount * -1 )
                                         - (
                                           SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                           FROM      dbo.saTransactions
                                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                           LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                           LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                           WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                     AND RefundAmount IS NULL
                                                     AND TitleIV = 1
                                                     AND Voided = 0
                                                     AND Descrip IN ( ''DL - SUB'', ''FFEL - SUB'', ''DL - UNSUB'', ''FFEL - UNSUB'', ''DL – Additional UNSUB''
                                                                     ,''FFEL – Additional UNSUB'', ''DL - PLUS'', ''FFEL - PLUS'', ''PELL'', ''SEOG''
                                                                    )
                                                     AND TransDate < @FiscalYearStart
                                           )
                                        ) <= 0 THEN 0
                                   WHEN ((
                                         SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) -- SUM(Subloan + Unsubloan + plus + pell + SEOG + FWS)  <= titleivcreditbalance then take full FWS
                                         FROM      dbo.saTransactions
                                         LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                         LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                         LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                         LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                         WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                   AND RefundAmount IS NULL
                                                   AND TitleIV = 1
                                                   AND Voided = 0
                                                   AND Descrip IN ( ''DL - SUB'', ''FFEL - SUB'', ''DL - UNSUB'', ''FFEL - UNSUB'', ''DL – Additional UNSUB''
                                                                   ,''FFEL – Additional UNSUB'', ''DL - PLUS'', ''FFEL - PLUS'', ''PELL'', ''SEOG'', ''FWS''
                                                                  )
                                                   AND TransDate < @FiscalYearStart
                                         ) <= ( [@InstitutionalChargeBalance].Amount * -1 )
                                        ) THEN (
                                               SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                               FROM      dbo.saTransactions
                                               LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                               LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                               LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                               LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                               WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                         AND RefundAmount IS NULL
                                                         AND TitleIV = 1
                                                         AND Voided = 0
                                                         AND Descrip IN ( ''FWS'' )
                                                         AND TransDate < @FiscalYearStart
                                               )
                                   ELSE -- TitleIvCreditBalance - SUM(sub, unsub, plus, pell, SEOG)  = amount of FWS to carry over
                          (( [@InstitutionalChargeBalance].Amount * -1 )
                           - (
                             SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                             FROM      dbo.saTransactions
                             LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                             LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                             LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                             LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                             WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                       AND RefundAmount IS NULL
                                       AND TitleIV = 1
                                       AND Voided = 0
                                       AND Descrip IN ( ''DL - SUB'', ''FFEL - SUB'', ''DL - UNSUB'', ''FFEL - UNSUB'', ''DL – Additional UNSUB'', ''FFEL – Additional UNSUB''
                                                       ,''DL - PLUS'', ''FFEL - PLUS'', ''PELL'', ''SEOG''
                                                      )
                                       AND TransDate < @FiscalYearStart
                             )
                          )
                              END
                            ) + (
                                SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS SubLoan
                                FROM      dbo.saTransactions
                                LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                                LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                          AND RefundAmount IS NULL
                                          AND TitleIV = 1
                                          AND Voided = 0
                                          AND Descrip IN ( ''FWS'' )
                                          AND TransDate >= @FiscalYearStart
                                          AND TransDate <= @FiscalYearEnd
                                )
                           ) AS Amount
                    FROM   @EnrollmentList
                    JOIN   @InstitutionalChargeBalance ON [@InstitutionalChargeBalance].StuEnrollId = [@EnrollmentList].StuEnrollId;

        --Title IV Amt Disbursed  calculation
        DECLARE @TitleIVAmtDisbursed TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO @TitleIVAmtDisbursed
                    SELECT StuEnrollId
                          ,( ''SUM(H'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '':M'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '')'' ) AS ExcelFormula
                    FROM   @EnrollmentList;

        --@AdjSubLoan  calculation
        DECLARE @AdjSubLoan TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        --@Adj Ubsub Loan  calculation
        DECLARE @AdjUnSubLoan TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );


        --@Adj Plus Loan  calculation
        DECLARE @AdjPlus TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        --@Adj Pell grant  calculation
        DECLARE @AdjPellGrant TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        --@Ad FSEOG  calculation
        DECLARE @AdjFSEOG TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @AdjFSEOG
                    SELECT StuEnrollId
                          ,(
                           SELECT    CASE WHEN (((
                                                 SELECT TOP 1 FSEOGMatchType
                                                 FROM   dbo.syCampuses
                                                 WHERE  CampusId IN (
                                                                    SELECT CampusId
                                                                    FROM   @tblCampusIds
                                                                    )
                                                 ) = ''C''
                                                )
                                               ) THEN ISNULL(( SUM(TransAmount) * -.75 ), 0)
                                          ELSE ISNULL(( SUM(TransAmount) * -1 ), 0)
                                     END AS Amount
                           FROM      dbo.saTransactions
                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                           LEFT JOIN dbo.syAdvFundSources ON syAdvFundSources.AdvFundSourceId = saFundSources.AdvFundSourceId
                           LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                           WHERE     RefundTypeId IS NULL
                                     AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                     AND TitleIV = 1
                                     AND Voided = 0
                                     AND Descrip IN ( ''SEOG'' )
                                     AND TransDate >= @FiscalYearStart
                                     AND TransDate <= @FiscalYearEnd
                           ) AS Amount
                    FROM   @EnrollmentList;

        --@AdjFWS calculation
        DECLARE @AdjFWS TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        --@TitleIVAmtAdjusted calculation
        DECLARE @TitleIVAmtAdjusted TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO @TitleIVAmtAdjusted
                    SELECT StuEnrollId
                          ,( ''SUM(O'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '':T'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '')'' ) AS ExcelFormula
                    FROM   @EnrollmentList;

        --@RevenueAdjustment calculation
        DECLARE @RevenueAdjustment TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO @RevenueAdjustment
                    SELECT StuEnrollId
                          ,( ''IF((U'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''-W'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''>(G'' + LTRIM(RTRIM(STR(RowNumber + 1)))
                             + ''-AJ'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '')),U'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''-(G'' + LTRIM(RTRIM(STR(RowNumber + 1)))
                             + ''-AJ'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '')-W'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '',0)''
                           ) AS ExcelFormula
                    FROM   @EnrollmentList;

        --@TitleIVRefunds calculation
        DECLARE @TitleIVRefunds TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @TitleIVRefunds
                    SELECT StuEnrollId
                          ,(
                           SELECT    ISNULL(( SUM(TransAmount)), 0) AS TitleIVRefunds
                           FROM      dbo.saTransactions
                           LEFT JOIN (
                                     SELECT    ( CASE WHEN saTransactions.FundSourceId IS NULL THEN saRefunds.FundSourceId
                                                      ELSE saTransactions.FundSourceId
                                                 END
                                               ) AS FundSourceId
                                              ,saTransactions.TransactionId
                                              ,TransCodeId
                                     FROM      dbo.saTransactions
                                     LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                     ) fundSourceTransactionIds ON fundSourceTransactionIds.TransactionId = saTransactions.TransactionId
                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = fundSourceTransactionIds.FundSourceId
                           LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = fundSourceTransactionIds.TransactionId
                           WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                     AND TitleIV = 1
                                     AND RefundTypeId IS NOT NULL
                                     AND Voided = 0
                                     AND TransDate >= @FiscalYearStart
                                     AND TransDate <= @FiscalYearEnd
                           ) AS Amount
                    FROM   @EnrollmentList;

        --@AdjTitleIVRevenue calculation
        DECLARE @AdjTitleIVRevenue TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO @AdjTitleIVRevenue
                    SELECT StuEnrollId
                          ,''IF(U'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''-V'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''-W'' + LTRIM(RTRIM(STR(RowNumber + 1)))
                           + ''>0,U'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''-V'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''-W'' + LTRIM(RTRIM(STR(RowNumber + 1)))
                           + '',0)'' AS ExcelFormula
                    FROM   @EnrollmentList;


        --@OutsideGrant calculation
        DECLARE @OutsideGrant TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @OutsideGrant
                    SELECT StuEnrollId
                          ,(
                           SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                           FROM      dbo.saTransactions
                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                           LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                           WHERE     Voided = 0
                                     AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                     AND AwardType9010Id = @OutsideGrantId
                                     AND TransDate >= @FiscalYearStart
                                     AND TransDate <= @FiscalYearEnd
                                     AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                           ) AS Amount
                    FROM   @EnrollmentList;


        --@JobTrainingGrant calculation
        DECLARE @JobTrainingGrant TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @JobTrainingGrant
                    SELECT StuEnrollId
                          ,(
                           SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                           FROM      dbo.saTransactions
                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                           LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                           WHERE     Voided = 0
                                     AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                     AND AwardType9010Id = @JobTrainingGrantId
                                     AND TransDate >= @FiscalYearStart
                                     AND TransDate <= @FiscalYearEnd
                                     AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                           ) AS Amount
                    FROM   @EnrollmentList;




        --@EducationSavingPlan calculation
        DECLARE @EducationSavingPlan TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @EducationSavingPlan
                    SELECT StuEnrollId
                          ,(
                           SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                           FROM      dbo.saTransactions
                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                           LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                           WHERE     Voided = 0
                                     AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                     AND AwardType9010Id = @EducationalSavingsId
                                     AND TransDate >= @FiscalYearStart
                                     AND TransDate <= @FiscalYearEnd
                                     AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                           ) AS Amount
                    FROM   @EnrollmentList;

        --@SchoolGrant calculation
        DECLARE @SchoolGrant TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @SchoolGrant
                    SELECT StuEnrollId
                          ,(
                           SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                           FROM      dbo.saTransactions
                           LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                           LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                           LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                           WHERE     Voided = 0
                                     AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                     AND AwardType9010Id = @SchoolGrantId
                                     AND TransDate >= @FiscalYearStart
                                     AND TransDate <= @FiscalYearEnd
                                     AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                           )
                    FROM   @EnrollmentList;


        --@FirstAppliedFund calculation
        DECLARE @FirstAppliedFund TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO @FirstAppliedFund
                    SELECT StuEnrollId
                          ,( ''SUM(Y'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '':AB'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '')'' ) AS ExcelFormula
                    FROM   @EnrollmentList;

        --@StuPayandOutsideLn calculation
        DECLARE @StuPayandOutsideLn TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @StuPayandOutsideLn -- All student payments(anything with 90/10 mapping for student payment, if no fund source systranscodeid 13) - (RefundsToStudents + StudentStipdends
                    SELECT [@EnrollmentList].StuEnrollId
                          ,((
                            SELECT ISNULL(( SUM(TransAmount)), 0) * -1
                            FROM   dbo.saTransactions
                            WHERE  StuEnrollId = [@EnrollmentList].StuEnrollId
                                   AND TransDate >= @FiscalYearStart
                                   AND TransDate <= @FiscalYearEnd
                                   AND Voided = 0
                                   AND (
                                       (
                                       saTransactions.FundSourceId IS NOT NULL
                                       AND saTransactions.FundSourceId IN (
                                                                          SELECT saFundSources.FundSourceId
                                                                          FROM   dbo.saFundSources
                                                                          JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                                          JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                          WHERE  syAwardTypes9010.AwardType9010Id IN ( @OutsideLoanAwardId
                                                                                                                      ,@StudentPayment9010MappingId
                                                                                                                      ,@SchoolLoanAwardId
                                                                                                                     )
                                                                                 AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                          )
                                       )
                                       OR (
                                          saTransactions.FundSourceId IS NULL
                                          AND saTransactions.TransCodeId IN (
                                                                            SELECT saTransCodes.TransCodeId
                                                                            FROM   dbo.saTransCodes
                                                                            JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.TransCodeId = saTransCodes.TransCodeId
                                                                            JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                            WHERE  syAwardTypes9010.AwardType9010Id = @StudentPayment9010MappingId
                                                                                   AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                            )
                                          )
                                       )
                            )
                            - (
                              SELECT    ISNULL(( SUM(TransAmount)), 0)
                              FROM      dbo.saTransactions
                              LEFT JOIN (
                                        SELECT    ( CASE WHEN saTransactions.FundSourceId IS NULL THEN saRefunds.FundSourceId
                                                         ELSE saTransactions.FundSourceId
                                                    END
                                                  ) AS FundSourceId
                                                 ,saTransactions.TransactionId
                                                 ,TransCodeId
                                        FROM      dbo.saTransactions
                                        LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                        ) fundSourceTransactionIds ON fundSourceTransactionIds.TransactionId = saTransactions.TransactionId
                              WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                        AND TransDate >= @FiscalYearStart
                                        AND TransDate <= @FiscalYearEnd
                                        AND Voided = 0
                                        AND (
                                            (
                                            fundSourceTransactionIds.FundSourceId IS NOT NULL
                                            AND fundSourceTransactionIds.FundSourceId IN (
                                                                                         SELECT saFundSources.FundSourceId
                                                                                         FROM   dbo.saFundSources
                                                                                         JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                                                         JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                                         WHERE  syAwardTypes9010.AwardType9010Id IN ( @StudentStipend9010MappingId
                                                                                                                                     ,@RefundToStudent9010MappingId
                                                                                                                                    )
                                                                                                AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                                         )
                                            )
                                            OR (
                                               fundSourceTransactionIds.FundSourceId IS NULL
                                               AND fundSourceTransactionIds.TransCodeId IN (
                                                                                           SELECT saTransCodes.TransCodeId
                                                                                           FROM   dbo.saTransCodes
                                                                                           JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.TransCodeId = saTransCodes.TransCodeId
                                                                                           JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                                           WHERE  syAwardTypes9010.AwardType9010Id IN ( @StudentStipend9010MappingId
                                                                                                                                       ,@RefundToStudent9010MappingId
                                                                                                                                      )
                                                                                                  AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                                           )
                                               )
                                            )
                              )
                            - (
                              SELECT    ISNULL(( SUM(TransAmount)), 0)
                              FROM      dbo.saTransactions
                              LEFT JOIN (
                                        SELECT    ( CASE WHEN saTransactions.FundSourceId IS NULL THEN saRefunds.FundSourceId
                                                         ELSE saTransactions.FundSourceId
                                                    END
                                                  ) AS FundSourceId
                                                 ,saTransactions.TransactionId
                                                 ,TransCodeId
                                        FROM      dbo.saTransactions
                                        LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                        ) fundSourceTransactionIds ON fundSourceTransactionIds.TransactionId = saTransactions.TransactionId
                              WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                        AND TransDate >= @FiscalYearStart
                                        AND TransDate <= @FiscalYearEnd
                                        AND Voided = 0
                                        AND (
                                            (
                                            fundSourceTransactionIds.FundSourceId IS NOT NULL
                                            AND fundSourceTransactionIds.FundSourceId IN (
                                                                                         SELECT saFundSources.FundSourceId
                                                                                         FROM   dbo.saFundSources
                                                                                         JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                                                         JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                                         WHERE  syAwardTypes9010.AwardType9010Id IN ( @OutsideLoanAwardId
                                                                                                                                     ,@SchoolLoanAwardId
                                                                                                                                    )
                                                                                                AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                                         )
                                            )
                                            OR (
                                               fundSourceTransactionIds.FundSourceId IS NULL
                                               AND fundSourceTransactionIds.TransCodeId IN (
                                                                                           SELECT saTransCodes.TransCodeId
                                                                                           FROM   dbo.saTransCodes
                                                                                           JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.TransCodeId = saTransCodes.TransCodeId
                                                                                           JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                                           WHERE  syAwardTypes9010.AwardType9010Id IN ( @RefundToStudent9010MappingId )
                                                                                           )
                                               )
                                            )
                              )
                           )
                    FROM   @EnrollmentList;

        --@TotalNonTitleIVRevenue calculation
        DECLARE @TotalNonTitleIVRevenue TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO @TotalNonTitleIVRevenue
                    SELECT StuEnrollId
                          ,( ''SUM(AC'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '':AD'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '')'' ) AS ExcelFormula
                    FROM   @EnrollmentList;

        --@AdjOutsideGrant calculation
        DECLARE @AdjOutsideGrant TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @AdjOutsideGrant
                    SELECT [@EnrollmentList].StuEnrollId
                          ,CASE WHEN (((( CASE WHEN [@InstitutionalChargeBalance].Amount >= 0 THEN [@InstitutionalChargeBalance].Amount
                                               ELSE 0
                                          END
                                        ) + [@InstitutionalMaxChargeLimitThisYear].Amount
                                       )
                                      ) <= 0
                                     ) THEN ( 0 )
                                WHEN ((( CASE WHEN [@InstitutionalChargeBalance].Amount >= 0 THEN [@InstitutionalChargeBalance].Amount
                                              ELSE 0
                                         END
                                       ) + [@InstitutionalMaxChargeLimitThisYear].Amount
                                      ) < (
                                          SELECT    ISNULL( -- Total Institutional Max Charge Limit < First Applied Funds then adjust else nothing
                                                              SUM(TransAmount) * -1, 0
                                                          ) AS FirstAppliedAmount
                                          FROM      dbo.saTransactions
                                          LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                          LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                          LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                          LEFT JOIN dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                          WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                    AND Voided = 0
                                                    AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                    AND dbo.syAwardTypes9010.AwardType9010Id IN ( @SchoolGrantId, @OutsideGrantId, @JobTrainingGrantId
                                                                                                 ,@EducationalSavingsId
                                                                                                )
                                                    AND TransDate < @FiscalYearStart
                                          )
                                     ) THEN -- adjust 
                                    CASE WHEN (((( CASE WHEN [@InstitutionalChargeBalance].Amount >= 0 THEN -- total max inst - outsidegrantamount > 0 then take full outside grant amount
                                                            [@InstitutionalChargeBalance].Amount
                                                        ELSE 0
                                                   END
                                                 ) + [@InstitutionalMaxChargeLimitThisYear].Amount
                                                )
                                               ) - (
                                                   SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                                   FROM      dbo.saTransactions
                                                   LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                                   LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                                   LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                   WHERE     Voided = 0
                                                             AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                                             AND AwardType9010Id = @OutsideGrantId
                                                             AND TransDate >= @FiscalYearStart
                                                             AND TransDate <= @FiscalYearEnd
                                                             AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                   ) > 0
                                              ) THEN (
                                                     SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                                     FROM      dbo.saTransactions
                                                     LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                                     LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                                     LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                     WHERE     Voided = 0
                                                               AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                                               AND AwardType9010Id = @OutsideGrantId
                                                               AND TransDate >= @FiscalYearStart
                                                               AND TransDate <= @FiscalYearEnd
                                                               AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                     )
                                         ELSE
                          (((( CASE WHEN [@InstitutionalChargeBalance].Amount >= 0 THEN -- total max inst - outsidegrantamount > 0 then take full outside grant amount
                                        [@InstitutionalChargeBalance].Amount
                                    ELSE 0
                               END
                             ) + [@InstitutionalMaxChargeLimitThisYear].Amount
                            )
                           )
                          )
                                    END
                                ELSE (
                                     SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                     FROM      dbo.saTransactions
                                     LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                     LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                     LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                     WHERE     Voided = 0
                                               AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                               AND AwardType9010Id = @OutsideGrantId
                                               AND TransDate >= @FiscalYearStart
                                               AND TransDate <= @FiscalYearEnd
                                               AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                     )
                           END AS TotalInstitutionalMaxChargeLimit
                    FROM   @EnrollmentList
                    JOIN   @InstitutionalMaxChargeLimitThisYear ON [@InstitutionalMaxChargeLimitThisYear].StuEnrollId = [@EnrollmentList].StuEnrollId
                    JOIN   @InstitutionalChargeBalance ON [@InstitutionalChargeBalance].StuEnrollId = [@EnrollmentList].StuEnrollId;

        --@AdjJobTrainingGrant calculation
        DECLARE @AdjJobTrainingGrant TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @AdjJobTrainingGrant
                    SELECT [@EnrollmentList].StuEnrollId
                          ,CASE WHEN (((( CASE WHEN [@InstitutionalChargeBalance].Amount >= 0 THEN [@InstitutionalChargeBalance].Amount
                                               ELSE 0
                                          END
                                        ) + [@InstitutionalMaxChargeLimitThisYear].Amount
                                       )
                                      ) <= 0
                                     ) THEN ( 0 )
                                WHEN ((( CASE WHEN [@InstitutionalChargeBalance].Amount >= 0 THEN [@InstitutionalChargeBalance].Amount
                                              ELSE 0
                                         END
                                       ) + [@InstitutionalMaxChargeLimitThisYear].Amount
                                      ) < (
                                          SELECT    ISNULL( -- Total Institutional Max Charge Limit < First Applied Funds then adjust else nothing
                                                              SUM(TransAmount) * -1, 0
                                                          ) AS FirstAppliedAmount
                                          FROM      dbo.saTransactions
                                          LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                          LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                          LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                          LEFT JOIN dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                          WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                    AND Voided = 0
                                                    AND dbo.syAwardTypes9010.AwardType9010Id IN ( @SchoolGrantId, @OutsideGrantId, @JobTrainingGrantId
                                                                                                 ,@EducationalSavingsId
                                                                                                )
                                                    AND TransDate < @FiscalYearStart
                                                    AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                          )
                                     ) THEN -- adjust 
                                    CASE WHEN (((( CASE WHEN [@InstitutionalChargeBalance].Amount >= 0 THEN -- total max inst - sum(outsidegrantamount, jobtraining) > 0 then take full job training grant amount
                                                            [@InstitutionalChargeBalance].Amount
                                                        ELSE 0
                                                   END
                                                 ) + [@InstitutionalMaxChargeLimitThisYear].Amount
                                                )
                                               ) - (
                                                   SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                                   FROM      dbo.saTransactions
                                                   LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                                   LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                                   LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                   WHERE     Voided = 0
                                                             AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                                             AND AwardType9010Id IN ( @OutsideGrantId, @JobTrainingGrantId )
                                                             AND TransDate >= @FiscalYearStart
                                                             AND TransDate <= @FiscalYearEnd
                                                             AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                   ) > 0
                                              ) THEN (
                                                     SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                                     FROM      dbo.saTransactions
                                                     LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                                     LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                                     LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                     WHERE     Voided = 0
                                                               AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                                               AND AwardType9010Id = @JobTrainingGrantId
                                                               AND TransDate >= @FiscalYearStart
                                                               AND TransDate <= @FiscalYearEnd
                                                               AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                     )
                                         ELSE
                          (((( CASE WHEN [@InstitutionalChargeBalance].Amount >= 0 THEN -- total max inst - outsidegrantamount
                                        [@InstitutionalChargeBalance].Amount
                                    ELSE 0
                               END
                             ) + [@InstitutionalMaxChargeLimitThisYear].Amount
                            )
                           ) - (
                               SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                               FROM      dbo.saTransactions
                               LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                               LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                               LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                               WHERE     Voided = 0
                                         AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                         AND AwardType9010Id = @OutsideGrantId
                                         AND TransDate >= @FiscalYearStart
                                         AND TransDate <= @FiscalYearEnd
                                         AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                               )
                          )
                                    END
                                ELSE (
                                     SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                     FROM      dbo.saTransactions
                                     LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                     LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                     LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                     WHERE     Voided = 0
                                               AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                               AND AwardType9010Id = @JobTrainingGrantId
                                               AND TransDate >= @FiscalYearStart
                                               AND TransDate <= @FiscalYearEnd
                                               AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                     )
                           END AS TotalInstitutionalMaxChargeLimit
                    FROM   @EnrollmentList
                    JOIN   @InstitutionalMaxChargeLimitThisYear ON [@InstitutionalMaxChargeLimitThisYear].StuEnrollId = [@EnrollmentList].StuEnrollId
                    JOIN   @InstitutionalChargeBalance ON [@InstitutionalChargeBalance].StuEnrollId = [@EnrollmentList].StuEnrollId;



        --@AdjEducationSavingPlan calculation
        DECLARE @AdjEducationSavingPlan TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @AdjEducationSavingPlan
                    SELECT [@EnrollmentList].StuEnrollId
                          ,CASE WHEN (((( CASE WHEN [@InstitutionalChargeBalance].Amount >= 0 THEN [@InstitutionalChargeBalance].Amount
                                               ELSE 0
                                          END
                                        ) + [@InstitutionalMaxChargeLimitThisYear].Amount
                                       )
                                      ) <= 0
                                     ) THEN ( 0 )
                                WHEN ((( CASE WHEN [@InstitutionalChargeBalance].Amount >= 0 THEN [@InstitutionalChargeBalance].Amount
                                              ELSE 0
                                         END
                                       ) + [@InstitutionalMaxChargeLimitThisYear].Amount
                                      ) < (
                                          SELECT    ISNULL( -- Total Institutional Max Charge Limit < First Applied Funds then adjust else nothing
                                                              SUM(TransAmount) * -1, 0
                                                          ) AS FirstAppliedAmount
                                          FROM      dbo.saTransactions
                                          LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                          LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                          LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                          LEFT JOIN dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                          WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                    AND Voided = 0
                                                    AND dbo.syAwardTypes9010.AwardType9010Id IN ( @SchoolGrantId, @OutsideGrantId, @JobTrainingGrantId
                                                                                                 ,@EducationalSavingsId
                                                                                                )
                                                    AND TransDate < @FiscalYearStart
                                                    AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                          )
                                     ) THEN -- adjust 
                                    CASE WHEN (((( CASE WHEN [@InstitutionalChargeBalance].Amount >= 0 THEN -- total max inst - sum(outsidegrantamount, jobtraining, education saving) > 0 then take full job training grant amount
                                                            [@InstitutionalChargeBalance].Amount
                                                        ELSE 0
                                                   END
                                                 ) + [@InstitutionalMaxChargeLimitThisYear].Amount
                                                )
                                               ) - (
                                                   SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                                   FROM      dbo.saTransactions
                                                   LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                                   LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                                   LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                   WHERE     Voided = 0
                                                             AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                                             AND AwardType9010Id IN ( @OutsideGrantId, @JobTrainingGrantId, @EducationalSavingsId )
                                                             AND TransDate >= @FiscalYearStart
                                                             AND TransDate <= @FiscalYearEnd
                                                             AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                   ) > 0
                                              ) THEN (
                                                     SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                                     FROM      dbo.saTransactions
                                                     LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                                     LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                                     LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                     WHERE     Voided = 0
                                                               AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                                               AND AwardType9010Id = @EducationalSavingsId
                                                               AND TransDate >= @FiscalYearStart
                                                               AND TransDate <= @FiscalYearEnd
                                                               AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                     )
                                         ELSE
                          (((( CASE WHEN [@InstitutionalChargeBalance].Amount >= 0 THEN -- total max inst - sum(outsidegrantamount, job traininggrant)
                                        [@InstitutionalChargeBalance].Amount
                                    ELSE 0
                               END
                             ) + [@InstitutionalMaxChargeLimitThisYear].Amount
                            )
                           ) - (
                               SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                               FROM      dbo.saTransactions
                               LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                               LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                               LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                               WHERE     Voided = 0
                                         AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                         AND AwardType9010Id IN ( @OutsideGrantId, @JobTrainingGrantId )
                                         AND TransDate >= @FiscalYearStart
                                         AND TransDate <= @FiscalYearEnd
                                         AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                               )
                          )
                                    END
                                ELSE (
                                     SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                     FROM      dbo.saTransactions
                                     LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                     LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                     LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                     WHERE     Voided = 0
                                               AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                               AND AwardType9010Id = @EducationalSavingsId
                                               AND TransDate >= @FiscalYearStart
                                               AND TransDate <= @FiscalYearEnd
                                               AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                     )
                           END AS TotalInstitutionalMaxChargeLimit
                    FROM   @EnrollmentList
                    JOIN   @InstitutionalMaxChargeLimitThisYear ON [@InstitutionalMaxChargeLimitThisYear].StuEnrollId = [@EnrollmentList].StuEnrollId
                    JOIN   @InstitutionalChargeBalance ON [@InstitutionalChargeBalance].StuEnrollId = [@EnrollmentList].StuEnrollId;


        --@AdjSchoolGrant calculation
        DECLARE @AdjSchoolGrant TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @AdjSchoolGrant
                    SELECT [@EnrollmentList].StuEnrollId
                          ,CASE WHEN (((( CASE WHEN [@InstitutionalChargeBalance].Amount >= 0 THEN [@InstitutionalChargeBalance].Amount
                                               ELSE 0
                                          END
                                        ) + [@InstitutionalMaxChargeLimitThisYear].Amount
                                       )
                                      ) <= 0
                                     ) THEN ( 0 )
                                WHEN ((( CASE WHEN [@InstitutionalChargeBalance].Amount >= 0 THEN [@InstitutionalChargeBalance].Amount
                                              ELSE 0
                                         END
                                       ) + [@InstitutionalMaxChargeLimitThisYear].Amount
                                      ) < (
                                          SELECT    ISNULL( -- Total Institutional Max Charge Limit < First Applied Funds then adjust else nothing
                                                              SUM(TransAmount) * -1, 0
                                                          ) AS FirstAppliedAmount
                                          FROM      dbo.saTransactions
                                          LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                          LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                          LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                          LEFT JOIN dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                          WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                                    AND Voided = 0
                                                    AND dbo.syAwardTypes9010.AwardType9010Id IN ( @SchoolGrantId, @OutsideGrantId, @JobTrainingGrantId
                                                                                                 ,@EducationalSavingsId
                                                                                                )
                                                    AND TransDate < @FiscalYearStart
                                                    AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                          )
                                     ) THEN -- adjust 
                                    CASE WHEN (((( CASE WHEN [@InstitutionalChargeBalance].Amount >= 0 THEN -- total max inst - sum(outsidegrantamount, jobtraining, education saving, school grant) > 0 then take full job training grant amount
                                                            [@InstitutionalChargeBalance].Amount
                                                        ELSE 0
                                                   END
                                                 ) + [@InstitutionalMaxChargeLimitThisYear].Amount
                                                )
                                               ) - (
                                                   SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                                   FROM      dbo.saTransactions
                                                   LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                                   LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                                   LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                   WHERE     Voided = 0
                                                             AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                                             AND AwardType9010Id IN ( @OutsideGrantId, @JobTrainingGrantId, @EducationalSavingsId, @SchoolGrantId )
                                                             AND TransDate >= @FiscalYearStart
                                                             AND TransDate <= @FiscalYearEnd
                                                             AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                   ) > 0
                                              ) THEN (
                                                     SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                                     FROM      dbo.saTransactions
                                                     LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                                     LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                                     LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                     WHERE     Voided = 0
                                                               AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                                               AND AwardType9010Id = @SchoolGrantId
                                                               AND TransDate >= @FiscalYearStart
                                                               AND TransDate <= @FiscalYearEnd
                                                               AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                     )
                                         ELSE
                          (((( CASE WHEN [@InstitutionalChargeBalance].Amount >= 0 THEN -- total max inst - sum(outsidegrantamount, job traininggrant, educationsaving)
                                        [@InstitutionalChargeBalance].Amount
                                    ELSE 0
                               END
                             ) + [@InstitutionalMaxChargeLimitThisYear].Amount
                            )
                           ) - (
                               SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                               FROM      dbo.saTransactions
                               LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                               LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                               LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                               WHERE     Voided = 0
                                         AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                         AND AwardType9010Id IN ( @OutsideGrantId, @JobTrainingGrantId, @EducationalSavingsId )
                                         AND TransDate >= @FiscalYearStart
                                         AND TransDate <= @FiscalYearEnd
                                         AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                               )
                          )
                                    END
                                ELSE (
                                     SELECT    ISNULL(( SUM(TransAmount) * -1 ), 0) AS Amount
                                     FROM      dbo.saTransactions
                                     LEFT JOIN dbo.saTransCodes ON saTransCodes.TransCodeId = saTransactions.TransCodeId
                                     LEFT JOIN dbo.saFundSources ON saFundSources.FundSourceId = saTransactions.FundSourceId
                                     LEFT JOIN dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                     WHERE     Voided = 0
                                               AND saTransactions.StuEnrollId = [@EnrollmentList].StuEnrollId
                                               AND AwardType9010Id = @SchoolGrantId
                                               AND TransDate >= @FiscalYearStart
                                               AND TransDate <= @FiscalYearEnd
                                               AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                     )
                           END AS TotalInstitutionalMaxChargeLimit
                    FROM   @EnrollmentList
                    JOIN   @InstitutionalMaxChargeLimitThisYear ON [@InstitutionalMaxChargeLimitThisYear].StuEnrollId = [@EnrollmentList].StuEnrollId
                    JOIN   @InstitutionalChargeBalance ON [@InstitutionalChargeBalance].StuEnrollId = [@EnrollmentList].StuEnrollId;

        --@AdjFirstAppliedFund calculation
        DECLARE @AdjFirstAppliedFund TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO @AdjFirstAppliedFund
                    SELECT StuEnrollId
                          ,( ''SUM(AF'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '':AI'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '')'' ) AS ExcelFormula
                    FROM   @EnrollmentList;

        --@AdjStuPayandOutsideLn calculation
        DECLARE @AdjStuPayandOutsideLn TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO @AdjStuPayandOutsideLn
                    SELECT StuEnrollId
                          ,''IF(AD'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''>0,IF(G'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''-(AJ''
                           + LTRIM(RTRIM(STR(RowNumber + 1))) + ''+X'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '')>AD'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '',AD''
                           + LTRIM(RTRIM(STR(RowNumber + 1))) + '',G'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''-(AJ'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''+X''
                           + LTRIM(RTRIM(STR(RowNumber + 1))) + '')),0)'' AS ExcelFormula
                    FROM   @EnrollmentList;

        --@AdjTotalNonTitleIVRevenue calculation
        DECLARE @AdjTotalNonTitleIVRevenue TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO @AdjTotalNonTitleIVRevenue
                    SELECT StuEnrollId
                          ,( ''SUM(AJ'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '':AK'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '')'' ) AS ExcelFormula
                    FROM   @EnrollmentList;

        --@ActivitiesConducted calculation
        DECLARE @ActivitiesConducted TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @ActivitiesConducted
                    SELECT StuEnrollId
                          ,0
                    FROM   @EnrollmentList;

        --@PaymentsOutsideLoanAndOtherSourceNonTitleIV calculation
        DECLARE @PaymentsOutsideLoanAndOtherSourceNonTitleIV TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @PaymentsOutsideLoanAndOtherSourceNonTitleIV
                    SELECT [@EnrollmentList].StuEnrollId
                          ,((
                            SELECT ISNULL(( SUM(TransAmount)), 0) * -1
                            FROM   dbo.saTransactions
                            WHERE  StuEnrollId = [@EnrollmentList].StuEnrollId
                                   AND TransDate >= @FiscalYearStart
                                   AND TransDate <= @FiscalYearEnd
                                   AND IsTitleIVProgram = 0
                                   AND Voided = 0
                                   AND (
                                       (
                                       saTransactions.FundSourceId IS NOT NULL
                                       AND dbo.saTransactions.FundSourceId IN (
                                                                              SELECT saFundSources.FundSourceId
                                                                              FROM   dbo.saFundSources
                                                                              JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                                              JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                              WHERE  syAwardTypes9010.AwardType9010Id IN ( @OutsideLoanAwardId
                                                                                                                          ,@StudentPayment9010MappingId
                                                                                                                          ,@SchoolLoanAwardId
                                                                                                                         )
                                                                                     AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                              )
                                       )
                                       OR (
                                          saTransactions.FundSourceId IS NULL
                                          AND saTransactions.TransCodeId IN (
                                                                            SELECT saTransCodes.TransCodeId
                                                                            FROM   dbo.saTransCodes
                                                                            JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.TransCodeId = saTransCodes.TransCodeId
                                                                            JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                            WHERE  syAwardTypes9010.AwardType9010Id = @StudentPayment9010MappingId
                                                                                   AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                            )
                                          )
                                       )
                            )
                            - (
                              SELECT    ISNULL(( SUM(TransAmount)), 0)
                              FROM      dbo.saTransactions
                              LEFT JOIN (
                                        SELECT    ( CASE WHEN saTransactions.FundSourceId IS NULL THEN saRefunds.FundSourceId
                                                         ELSE saTransactions.FundSourceId
                                                    END
                                                  ) AS FundSourceId
                                                 ,saTransactions.TransactionId
                                                 ,TransCodeId
                                        FROM      dbo.saTransactions
                                        LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                        ) fundSourceTransactionIds ON fundSourceTransactionIds.TransactionId = saTransactions.TransactionId
                              WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                        AND TransDate >= @FiscalYearStart
                                        AND TransDate <= @FiscalYearEnd
                                        AND IsTitleIVProgram = 0
                                        AND Voided = 0
                                        AND (
                                            (
                                            fundSourceTransactionIds.FundSourceId IS NOT NULL
                                            AND fundSourceTransactionIds.FundSourceId IN (
                                                                                         SELECT saFundSources.FundSourceId
                                                                                         FROM   dbo.saFundSources
                                                                                         JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                                                         JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                                         WHERE  syAwardTypes9010.AwardType9010Id IN ( @StudentStipend9010MappingId
                                                                                                                                     ,@RefundToStudent9010MappingId
                                                                                                                                    )
                                                                                                AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                                         )
                                            )
                                            OR (
                                               fundSourceTransactionIds.FundSourceId IS NULL
                                               AND fundSourceTransactionIds.TransCodeId IN (
                                                                                           SELECT saTransCodes.TransCodeId
                                                                                           FROM   dbo.saTransCodes
                                                                                           JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.TransCodeId = saTransCodes.TransCodeId
                                                                                           JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                                           WHERE  syAwardTypes9010.AwardType9010Id IN ( @StudentStipend9010MappingId
                                                                                                                                       ,@RefundToStudent9010MappingId
                                                                                                                                      )
                                                                                                  AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                                           )
                                               )
                                            )
                              )
                            - (
                              SELECT    ISNULL(( SUM(TransAmount)), 0)
                              FROM      dbo.saTransactions
                              LEFT JOIN (
                                        SELECT    ( CASE WHEN saTransactions.FundSourceId IS NULL THEN saRefunds.FundSourceId
                                                         ELSE saTransactions.FundSourceId
                                                    END
                                                  ) AS FundSourceId
                                                 ,saTransactions.TransactionId
                                                 ,TransCodeId
                                        FROM      dbo.saTransactions
                                        LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = saTransactions.TransactionId
                                        ) fundSourceTransactionIds ON fundSourceTransactionIds.TransactionId = saTransactions.TransactionId
                              LEFT JOIN dbo.saRefunds ON saRefunds.TransactionId = fundSourceTransactionIds.TransactionId
                              WHERE     StuEnrollId = [@EnrollmentList].StuEnrollId
                                        AND TransDate >= @FiscalYearStart
                                        AND TransDate <= @FiscalYearEnd
                                        AND IsTitleIVProgram = 0
                                        AND Voided = 0
                                        AND RefundTypeId IS NOT NULL
                                        AND (
                                            (
                                            fundSourceTransactionIds.FundSourceId IS NOT NULL
                                            AND fundSourceTransactionIds.FundSourceId IN (
                                                                                         SELECT saFundSources.FundSourceId
                                                                                         FROM   dbo.saFundSources
                                                                                         JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.FundSourceId = saFundSources.FundSourceId
                                                                                         JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                                         WHERE  syAwardTypes9010.AwardType9010Id IN ( @OutsideLoanAwardId
                                                                                                                                     ,@SchoolLoanAwardId
                                                                                                                                    )
                                                                                                AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                                         )
                                            )
                                            OR (
                                               fundSourceTransactionIds.FundSourceId IS NULL
                                               AND fundSourceTransactionIds.TransCodeId IN (
                                                                                           SELECT saTransCodes.TransCodeId
                                                                                           FROM   dbo.saTransCodes
                                                                                           JOIN   dbo.syAwardTypes9010Mapping ON syAwardTypes9010Mapping.TransCodeId = saTransCodes.TransCodeId
                                                                                           JOIN   dbo.syAwardTypes9010 ON syAwardTypes9010.AwardType9010Id = syAwardTypes9010Mapping.AwardType9010Id
                                                                                           WHERE  syAwardTypes9010.AwardType9010Id IN ( @StudentPayment9010MappingId )
                                                                                                  AND dbo.syAwardTypes9010Mapping.CampusId = dbo.saTransactions.CampusId
                                                                                           )
                                               )
                                            )
                              )
                           )
                    FROM   @EnrollmentList;

        --@TotalOtherSourceRevenue calculation
        DECLARE @TotalOtherSourceRevenue TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO @TotalOtherSourceRevenue
                    SELECT StuEnrollId
                          ,( ''SUM(AM'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '':AN'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '')'' ) AS ExcelFormula
                    FROM   @EnrollmentList;

        --@AdjActivitiesConducted calculation
        DECLARE @AdjActivitiesConducted TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,Amount DECIMAL(16, 2)
            );

        INSERT INTO @AdjActivitiesConducted
                    SELECT StuEnrollId
                          ,0
                    FROM   @EnrollmentList;

        --@AdjPaymentsOutsideLoanAndOtherSourceNonTitleIV calculation
        DECLARE @AdjPaymentsOutsideLoanAndOtherSourceNonTitleIV TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO @AdjPaymentsOutsideLoanAndOtherSourceNonTitleIV
                    SELECT StuEnrollId
                          ,''IF(AN'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''>0,IF(G'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''-(AJ''
                           + LTRIM(RTRIM(STR(RowNumber + 1))) + ''+X'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '')>AN'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '',AN''
                           + LTRIM(RTRIM(STR(RowNumber + 1))) + '',G'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''-(AJ'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''+X''
                           + LTRIM(RTRIM(STR(RowNumber + 1))) + '')),0)'' AS ExcelFormula
                    FROM   @EnrollmentList;

        --@@AdjTotalOtherSourceRevenue calculation
        DECLARE @AdjTotalOtherSourceRevenue TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO @AdjTotalOtherSourceRevenue
                    SELECT StuEnrollId
                          ,( ''SUM(AP'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '':AQ'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '')'' ) AS ExcelFormula
                    FROM   @EnrollmentList;

        --@@AdjTotalOtherSourceRevenue calculation
        DECLARE @Numerator TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO @Numerator
                    SELECT StuEnrollId
                          ,( ''X'' + LTRIM(RTRIM(STR(RowNumber + 1)))) AS ExcelFormula
                    FROM   @EnrollmentList;

        --@AdjTotalOtherSourceRevenue calculation
        DECLARE @Denominator TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO @Denominator
                    SELECT StuEnrollId
                          ,( ''X'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''+AL'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''+AR'' + LTRIM(RTRIM(STR(RowNumber + 1))) + '''' ) AS ExcelFormula
                    FROM   @EnrollmentList;

        --@AdjTotalOtherSourceRevenue calculation
        DECLARE @Percentage TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ExcelFormula VARCHAR(200)
            );

        INSERT INTO @Percentage
                    SELECT StuEnrollId
                          ,( ''IF(AT'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''>0, AS'' + LTRIM(RTRIM(STR(RowNumber + 1))) + ''/AT''
                             + LTRIM(RTRIM(STR(RowNumber + 1))) + '',0)''
                           ) AS ExcelFormula
                    FROM   @EnrollmentList;

        DECLARE @Sheet1 TABLE
            (
                Column1 VARCHAR(100)
               ,Column2 VARCHAR(100)
               ,Column3 VARCHAR(100)
               ,Column4 VARCHAR(100)
            );

        INSERT INTO @Sheet1
        VALUES ( '''', ''Amount Disbursed'', '''', ''AdjustedAmount'' )
              ,( ''Adjusted Student Title IV Revenue'', '''', '''', '''' )
              ,( '''', '''', '''', '''' )
              ,( ''Subsidized Loan'', ''SUM(''''90-10 Student Ledger''''!H2:H100000)'', '''', ''SUM(''''90-10 Student Ledger''''!O2:O10000)'' )
              ,( ''Unsubsidized Loan'', ''SUM(''''90-10 Student Ledger''''!I2:I10000)'', '''', ''SUM(''''90-10 Student Ledger''''!P2:P10000)'' )
              ,( ''PLUS'', ''SUM(''''90-10 Student Ledger''''!J2:J10000)'', '''', ''SUM(''''90-10 Student Ledger''''!Q2:Q10000)'' )
              ,( ''Federal Pell Grants'', ''SUM(''''90-10 Student Ledger''''!K2:K10000)'', '''', ''SUM(''''90-10 Student Ledger''''!R2:R10000)'' )
              ,( ''FSEOG(subject to matching deduction)'', ''SUM(''''90-10 Student Ledger''''!L2:L10000)'', '''', ''SUM(''''90-10 Student Ledger''''!S2:S10000)'' )
              ,( ''FWS(subject to matching deduction'', ''SUM(''''90-10 Student Ledger''''!M2:M10000)'', '''', ''SUM(''''90-10 Student Ledger''''!T2:T10000)'' )
              ,( ''Student Title IV Revenue'', ''SUM(B4:B9)'', '''', ''SUM(D4:D9)'' )
              ,( '''', '''', '''', '''' )
              ,( ''Revenue Adjustment'', '''', '''', ''SUM(''''90-10 Student Ledger''''!V2:V10000)'' )
              ,( ''Title IV funds returned for a student under 34 C.F.R. 668.22'', '''', '''', ''SUM(''''90-10 Student Ledger''''!W2:W10000)'' )
              ,( '''', '''', '''', '''' )
              ,( '''', '''', '''', '''' )
              ,( ''Adjusted Student Title IV Revenue'', '''', '''', ''D10-D12-D13'' )
              ,( '''', '''', '''', '''' )
              ,( ''Student Non-Title IV Revenue'', '''', '''', '''' )
              ,( '''', '''', '''', '''' )
              ,( ''Grant funds for the student from non-Federal public'', '''', '''', '''' )
              ,( ''agencies or private sources independent of the institution'', ''SUM(''''90-10 Student Ledger''''!AF2:AF10000)'', '''', '''' )
              ,( '''', '''', '''', '''' )
              ,( ''Funds provided for the student under a contractual'', '''', '''', '''' )
              ,( ''arrangement with a Federal, State, or local government'', '''', '''', '''' )
              ,( ''agency for the purpose of providing job training to low-'', '''', '''', '''' )
              ,( ''income individuals'', ''SUM(''''90-10 Student Ledger''''!AG2:AG10000)'', '''', '''' )
              ,( '''', '''', '''', '''' )
              ,( ''Funds used by a student from savings plans for educational'', '''', '''', '''' )
              ,( ''expenses established by or on behalf of the student that'', '''', '''', '''' )
              ,( ''quality for special tax treatment under the Internal'', '''', '''', '''' )
              ,( ''Revenue Code'', ''SUM(''''90-10 Student Ledger''''!AH2:AH10000)'', '''', '''' )
              ,( '''', '''', '''', '''' )
              ,( ''Institutional Scholarships disbursed to the student'', ''SUM(''''90-10 Student Ledger''''!AI2:AI10000)'', '''', '''' )
              ,( '''', '''', '''', '''' )
              ,( '''', '''', '''', '''' )
              ,( ''Student Payments on current charges'', ''SUM(''''90-10 Student Ledger''''!AK2:AK10000)'', '''', '''' )
              ,( '''', '''', '''', '''' )
              ,( ''Total Student Non-Title IV Revenue'', ''SUM(B21:B36)'', '''', '''' )
              ,( '''', '''', '''', '''' )
              ,( ''Revenue from Other Sources'', '''', '''', '''' )
              ,( '''', '''', '''', '''' )
              ,( ''Activities conducted by the Institution that are necessary for'', '''', '''', '''' )
              ,( ''education and training (34 CFR 668.28(a)(3)(ii))'', (
                                                                     SELECT CONVERT(VARCHAR, CONVERT(DECIMAL(16, 2), @ActivitiesConductedAmount))
                                                                     ) , '''', '''' )
              ,( '''', '''', '''', '''' )
              ,( ''Funds paid by a student, on or behalf of a student, by a party'', '''', '''', '''' )
              ,( ''other than the school for an education or training program'', '''', '''', '''' )
              ,( ''that is not Title IV eligible (34 CFR 668.28(a)(3)(iii))'', ''SUM(''''90-10 Student Ledger''''!AQ2:AQ10000)'', '''', '''' )
              ,( '''', '''', '''', '''' )
              ,( ''Allowable student payments and amounts from accounts'', '''', '''', '''' )
              ,( ''receivable or institutional loan sales net of any required'', '''', '''', '''' )
              ,( ''payments under a recourse agreement'', '''', '''', '''' )
              ,( '''', '''', '''', '''' )
              ,( ''Total Revenue from Other Sources'', ''SUM(B43:B51)'', '''', '''' )
              ,( '''', '''', '''', '''' )
              ,( '''', '''', '''', '''' )
              ,( '''', '''', '''', '''' )
              ,( ''Numerator'', ''D16'', '''', ''IF(B58>0,B57/B58,0)'' )
              ,( ''Denominator'', ''D16+B38+B53'', '''', '''' );


        SELECT *
        FROM   @Sheet1;

        SELECT    [@EnrollmentList].StudentNumber AS ''Student Id''
                 ,ISNULL(LastName, '''') + '', '' + ISNULL(FirstName, '''') + '' '' + ISNULL(MiddleName, '''') AS ''Name''
                 ,( CASE WHEN [@EnrollmentList].IsTitleIVProgram = 1 THEN ''Yes - '' + PrgVerDescrip
                         ELSE ''No - '' + PrgVerDescrip
                    END
                  ) AS ''Title IV Program''
                 ,( CASE WHEN ( ISNULL([@InstitutionalChargeBalance].Amount, 0) > 0 ) THEN 0
                         ELSE ISNULL([@InstitutionalChargeBalance].Amount, 0) * -1
                    END
                  ) AS ''Title IV Credit Balance''
                                                                                                                                                          --( CASE WHEN ( ISNULL([@TitleIVCreditBalance].Amount, 0) <= 0 ) THEN
                                                                                                                                                          --                   0
                                                                                                                                                          --               ELSE ISNULL([@TitleIVCreditBalance].Amount, 0)
                                                                                                                                                          --          END ) AS ''Title IV Credit Balance'' ,
                 ,( CASE WHEN ( ISNULL([@InstitutionalChargeBalance].Amount, 0) <= 0 ) THEN 0
                         ELSE ISNULL([@InstitutionalChargeBalance].Amount, 0)
                    END
                  ) AS ''Institutional Charge Balance''
                 ,( CASE WHEN ISNULL([@InstitutionalMaxChargeLimitThisYear].Amount, 0) <= 0 THEN 0
                         ELSE ISNULL([@InstitutionalMaxChargeLimitThisYear].Amount, 0)
                    END
                  ) AS ''Institutional Max Charge Limit this year''
                 ,[@TotalInstitutionalMaxChargeLimit].ExcelFormula AS ''Total Institutional Max Charge Limit''                                              --formula field
                 ,ISNULL([@SubLoan].Amount, 0) AS ''Sub Loan''
                 ,ISNULL([@UnSubLoan].Amount, 0) AS ''UnSub Loan''
                 ,ISNULL([@Plus].Amount, 0) AS ''Plus''
                 ,ISNULL([@PellGrant].Amount, 0) AS ''Pell Grant''
                 ,ISNULL([@FSEOG].Amount, 0) AS ''FSEOG''
                 ,ISNULL([@FWS].Amount, 0) AS ''FWS''
                 ,[@TitleIVAmtDisbursed].ExcelFormula AS ''Title IV Amt Disbursed''                                                                         -- formula field
                 ,ISNULL([@SubLoan].Amount, 0) AS ''Adj Sub Loan''
                 ,ISNULL([@UnSubLoan].Amount, 0) AS ''Adj UnSub Loan''
                 ,ISNULL([@Plus].Amount, 0) AS ''Adj Plus''
                 ,ISNULL([@PellGrant].Amount, 0) AS ''Adj Pell Grant''
                 ,ISNULL([@AdjFSEOG].Amount, 0) AS ''Adj FSEOG''
                 ,ISNULL([@FWS].Amount, 0) AS ''Adj FWS''
                 ,[@TitleIVAmtAdjusted].ExcelFormula AS ''Title IV Amt Adjusted''                                                                           --formula field
                 ,[@RevenueAdjustment].ExcelFormula AS ''Revenue Adjustment''                                                                               --formula field
                 ,ISNULL([@TitleIVRefunds].Amount, 0) AS ''Title IV Refunds''
                 ,[@AdjTitleIVRevenue].ExcelFormula AS ''Adj Title IV Revenue''                                                                             --formula field
                 ,ISNULL([@OutsideGrant].Amount, 0) AS ''Outside Grant''
                 ,ISNULL([@JobTrainingGrant].Amount, 0) AS ''Job Training Grant''
                 ,ISNULL([@EducationSavingPlan].Amount, 0) AS ''Education Saving Plan''
                 ,ISNULL([@SchoolGrant].Amount, 0) AS ''School Grant''
                 ,[@FirstAppliedFund].ExcelFormula AS ''First Applied Fund''                                                                                --formula field
                 ,CASE WHEN ISNULL([@StuPayandOutsideLn].Amount, 0) > 0 THEN ISNULL([@StuPayandOutsideLn].Amount, 0)
                       ELSE 0
                  END AS ''Stu Pay and Outside Ln''
                 ,[@TotalNonTitleIVRevenue].ExcelFormula AS ''Total Non Title IV Revenue''                                                                  --formula field
                 ,ISNULL([@AdjOutsideGrant].Amount, 0) AS ''Adj Outside Grant''
                 ,ISNULL([@AdjJobTrainingGrant].Amount, 0) AS ''Adj Job Training Grant''
                 ,ISNULL([@AdjEducationSavingPlan].Amount, 0) AS ''Adj Education Saving Plan''
                 ,ISNULL([@AdjSchoolGrant].Amount, 0) AS ''Adj School Grant''
                 ,[@AdjFirstAppliedFund].ExcelFormula AS ''Adj First Applied Fund''                                                                         --formula field
                 ,[@AdjStuPayandOutsideLn].ExcelFormula AS ''Adj Stu Pay and Outside Ln''                                                                   --formula field
                 ,[@AdjTotalNonTitleIVRevenue].ExcelFormula AS ''Adj Total Non Title IV Revenue''                                                           --formula field
                 ,0 AS ''Activities Conducted''
                 ,ISNULL([@PaymentsOutsideLoanAndOtherSourceNonTitleIV].Amount, 0) AS ''Payments, Outside Loan and Other Source (Non Title IV Program)''
                 ,[@TotalOtherSourceRevenue].ExcelFormula AS ''Total Other Source Revenue''                                                                 --formula field
                 ,0 AS ''Adj Activities conducted''
                 ,[@AdjPaymentsOutsideLoanAndOtherSourceNonTitleIV].ExcelFormula AS ''Adj Payments, Outside Loan, and Other Source (Non Title IV Program)'' --formula field
                 ,[@AdjTotalOtherSourceRevenue].ExcelFormula AS ''Adj Total Other Source Revenue''                                                          --formula field
                 ,[@Numerator].ExcelFormula AS ''Numerator''                                                                                                --formula field
                 ,[@Denominator].ExcelFormula AS ''Denominator''                                                                                            --formula field
                 ,[@Percentage].ExcelFormula AS ''Percentage''                                                                                              --formula field
        FROM      @EnrollmentList
        LEFT JOIN @TitleIVCreditBalance ON [@TitleIVCreditBalance].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @InstitutionalChargeBalance ON [@InstitutionalChargeBalance].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @InstitutionalMaxChargeLimitThisYear ON [@InstitutionalMaxChargeLimitThisYear].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @TotalInstitutionalMaxChargeLimit ON [@TotalInstitutionalMaxChargeLimit].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @SubLoan ON [@SubLoan].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @UnSubLoan ON [@UnSubLoan].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @Plus ON [@Plus].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @PellGrant ON [@PellGrant].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @FSEOG ON [@FSEOG].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @FWS ON [@FWS].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @TitleIVAmtDisbursed ON [@TitleIVAmtDisbursed].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @AdjSubLoan ON [@AdjSubLoan].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @AdjUnSubLoan ON [@AdjUnSubLoan].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @AdjPlus ON [@AdjPlus].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @AdjPellGrant ON [@AdjPellGrant].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @AdjFSEOG ON [@AdjFSEOG].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @AdjFWS ON [@AdjFWS].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @TitleIVAmtAdjusted ON [@TitleIVAmtAdjusted].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @RevenueAdjustment ON [@RevenueAdjustment].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @TitleIVRefunds ON [@TitleIVRefunds].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @AdjTitleIVRevenue ON [@AdjTitleIVRevenue].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @OutsideGrant ON [@OutsideGrant].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @JobTrainingGrant ON [@JobTrainingGrant].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @EducationSavingPlan ON [@EducationSavingPlan].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @SchoolGrant ON [@SchoolGrant].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @FirstAppliedFund ON [@FirstAppliedFund].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @StuPayandOutsideLn ON [@StuPayandOutsideLn].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @TotalNonTitleIVRevenue ON [@TotalNonTitleIVRevenue].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @AdjOutsideGrant ON [@AdjOutsideGrant].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @AdjJobTrainingGrant ON [@AdjJobTrainingGrant].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @AdjEducationSavingPlan ON [@AdjEducationSavingPlan].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @AdjSchoolGrant ON [@AdjSchoolGrant].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @AdjFirstAppliedFund ON [@AdjFirstAppliedFund].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @AdjStuPayandOutsideLn ON [@AdjStuPayandOutsideLn].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @AdjTotalNonTitleIVRevenue ON [@AdjTotalNonTitleIVRevenue].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @ActivitiesConducted ON [@ActivitiesConducted].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @PaymentsOutsideLoanAndOtherSourceNonTitleIV ON [@PaymentsOutsideLoanAndOtherSourceNonTitleIV].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @TotalOtherSourceRevenue ON [@TotalOtherSourceRevenue].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @AdjActivitiesConducted ON [@AdjActivitiesConducted].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @AdjPaymentsOutsideLoanAndOtherSourceNonTitleIV ON [@AdjPaymentsOutsideLoanAndOtherSourceNonTitleIV].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @AdjTotalOtherSourceRevenue ON [@AdjTotalOtherSourceRevenue].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @Numerator ON [@Numerator].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @Denominator ON [@Denominator].StuEnrollId = [@EnrollmentList].StuEnrollId
        LEFT JOIN @Percentage ON [@Percentage].StuEnrollId = [@EnrollmentList].StuEnrollId;
    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[usp_GetStudentLedgerReportSummary]'
GO
IF OBJECT_ID(N'[dbo].[usp_GetStudentLedgerReportSummary]', 'P') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[usp_GetStudentLedgerReportSummary]
    @StudentEnrollmentId UNIQUEIDENTIFIER
AS
    SELECT   RTRIM(LTRIM(Summary.FundSourceDescrip)) AS Description
            ,Summary.AwardTypeId
            ,FORMAT((
                    SELECT SUM(GrossAmount) - SUM(TA.LoanFees)
                    FROM   dbo.faStudentAwards TA
                    WHERE  TA.AwardTypeId = Summary.AwardTypeId
                           AND TA.StuEnrollId = @StudentEnrollmentId
                    )
                   ,''C''
                   ) AS Amount
            ,FORMAT(SUM(   CASE WHEN Summary.IsDisbursement = 1 THEN Summary.Amount
                                ELSE 0
                           END
                       )
                   ,''C''
                   ) AS Received
            ,FORMAT(SUM(   CASE WHEN Summary.IsDisbursement = 0 THEN Summary.Amount
                                ELSE 0
                           END
                       )
                   ,''C''
                   ) AS Refunded
				   ,ROW_NUMBER() OVER (ORDER BY Summary.FundSourceDescrip) AS RowNumber
    FROM     (
             SELECT    D.PmtDisbRelId AS Id
                      ,D.Amount AS Amount
                      ,S.StudentAwardId AS AwardId
                      ,A.AwardTypeId
                      ,FS.FundSourceDescrip
                      ,A.GrossAmount
                      ,1 AS IsDisbursement
             FROM      dbo.faStudentAwardSchedule S
             LEFT JOIN dbo.saPmtDisbRel D ON S.AwardScheduleId = D.AwardScheduleId
             LEFT JOIN dbo.faStudentAwards A ON A.StudentAwardId = S.StudentAwardId
             LEFT JOIN dbo.saFundSources FS ON FS.FundSourceId = A.AwardTypeId
             LEFT JOIN dbo.saTransactions T ON T.TransactionId = D.TransactionId
             WHERE     A.StuEnrollId = @StudentEnrollmentId
                       AND (
                           T.Voided = 0
                           OR T.TransactionId IS NULL
                           )
             UNION
             SELECT    R.TransactionId AS Id
                      ,R.RefundAmount AS Amount
                      ,S.StudentAwardId AS AwardId
                      ,A.AwardTypeId
                      ,FS.FundSourceDescrip
                      ,A.GrossAmount
                      ,0 AS IsDisbursement
             FROM      dbo.faStudentAwardSchedule S
             LEFT JOIN dbo.saRefunds R ON S.AwardScheduleId = R.AwardScheduleId
             LEFT JOIN dbo.faStudentAwards A ON A.StudentAwardId = S.StudentAwardId
             LEFT JOIN dbo.saFundSources FS ON FS.FundSourceId = A.AwardTypeId
             LEFT JOIN dbo.saTransactions T ON T.TransactionId = R.TransactionId
             WHERE     A.StuEnrollId = @StudentEnrollmentId
                       AND (
                           T.Voided = 0
                           OR T.TransactionId IS NULL
                           )
             ) Summary
    GROUP BY Summary.AwardTypeId
            ,Summary.FundSourceDescrip;






'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_TitleIV_GetTriggerMetDate]'
GO
IF OBJECT_ID(N'[dbo].[USP_TitleIV_GetTriggerMetDate]', 'P') IS NULL
EXEC sp_executesql N'-- =============================================
-- Author:		Edwin Sosa
-- Create date: 7/9/2018
-- Description:	Used to calculate the trigger date for attendance or credits attempted triggers for Title IV Sap
-- =============================================
CREATE PROCEDURE [dbo].[USP_TitleIV_GetTriggerMetDate]
    -- Add the parameters for the stored procedure here
    @StuEnrollId UNIQUEIDENTIFIER = NULL
   ,@StuEnrollCampusId UNIQUEIDENTIFIER = NULL
   ,@TriggerValue DECIMAL(18, 2)
   ,@TriggerUnitTypeId INTEGER
   ,@IncludeExternship BIT
   ,@IncludeTransferHours BIT
   ,@TriggerMetDate DATETIME OUTPUT
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @TrackSapAttendance VARCHAR(1000);
        DECLARE @TrigUnitTypeDescrip VARCHAR(250);
        DECLARE @UnitTypeDescrip VARCHAR(250);
        SET @TrigUnitTypeDescrip = (
                                   SELECT LTRIM(RTRIM(TUT.TrigUnitTypDescrip))
                                   FROM   arTrigUnitTyps AS TUT
                                   WHERE  TUT.TrigUnitTypId = @TriggerUnitTypeId
                                   );

        SET @UnitTypeDescrip = (
                               SELECT     LTRIM(RTRIM(AAUT.UnitTypeDescrip))
                               FROM       arAttUnitType AS AAUT
                               INNER JOIN arPrgVersions AS APV ON APV.UnitTypeId = AAUT.UnitTypeId
                               INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                               WHERE      ASE.StuEnrollId = @StuEnrollId
                               );
        IF ( @StuEnrollCampusId IS NULL )
            BEGIN
                SET @StuEnrollCampusId = COALESCE((
                                                  SELECT ASE.CampusId
                                                  FROM   arStuEnrollments AS ASE
                                                  WHERE  ASE.StuEnrollId = @StuEnrollId
                                                  )
                                                 ,NULL
                                                 );
            END;

        SET @TrackSapAttendance = dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', @StuEnrollCampusId);
        IF ( @TrackSapAttendance IS NOT NULL )
            BEGIN
                SET @TrackSapAttendance = LOWER(LTRIM(RTRIM(@TrackSapAttendance)));
            END;

        DECLARE @hoursBucket AS TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER NOT NULL
               ,MeetDate DATETIME
               ,Actual DECIMAL(18, 2)
               ,Scheduled DECIMAL(18, 2)
            );


        DECLARE @creditsBucket AS TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,DateCompleted DATETIME
               ,CreditsAttempted DECIMAL(18, 2)
            );

        -- Insert statements for procedure here
        --DECLARE @trigger INTEGER = 5;
        --DECLARE @StuEnrollId UNIQUEIDENTIFIER = ''433F60AB-1596-4616-8A8E-7DA51E3BDDED'';

        --We are calculating a trigger based on actual or scheduled hours
        IF @TrigUnitTypeDescrip IN ( ''Actual Hours'', ''Scheduled Hours'' )
            BEGIN
                IF @UnitTypeDescrip IN ( ''none'', ''present absent'' )
                   AND @TrackSapAttendance = ''byclass''
                    BEGIN
                        INSERT INTO @hoursBucket
                                    SELECT nPaByClass.StuEnrollId
                                          ,nPaByClass.MeetDate
                                          ,( CASE WHEN ( nPaByClass.Actual >= 1 ) THEN DATEDIFF(MI, nPaByClass.StartTime, nPaByClass.EndTime)
                                                  ELSE 0
                                             END
                                           ) / 60 AS Actual
                                          ,( CASE WHEN ( nPaByClass.Scheduled >= 1 ) THEN DATEDIFF(MI, nPaByClass.StartTime, nPaByClass.EndTime)
                                                  ELSE 0
                                             END
                                           ) / 60 AS Scheduled
                                    FROM   (
                                           SELECT *
                                                 ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                           FROM   (
                                                  SELECT     DISTINCT t1.StuEnrollId AS StuEnrollId
                                                                     ,t1.ClsSectionId
                                                                     ,t1.MeetDate AS MeetDate
                                                                     ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                                     ,t4.StartDate
                                                                     ,t4.EndDate
                                                                     ,t5.PeriodDescrip
                                                                     ,t1.Actual AS Actual
                                                                     ,t1.Excused
                                                                     ,CASE WHEN (
                                                                                t1.Actual = 0
                                                                                AND t1.Excused = 0
                                                                                ) THEN t1.Scheduled
                                                                           ELSE CASE WHEN (
                                                                                          t1.Actual <> 9999.00
                                                                                          AND t1.Actual < t1.Scheduled
                                                                                          --AND t1.Excused <> 1
                                                                                          ) THEN ( t1.Scheduled - t1.Actual )
                                                                                     ELSE 0
                                                                                END
                                                                      END AS Absent
                                                                     ,t1.Scheduled AS Scheduled
                                                                     ,CASE WHEN (
                                                                                t1.Actual > 0
                                                                                AND t1.Actual < t1.Scheduled
                                                                                ) THEN ( t1.Scheduled - t1.Actual )
                                                                           ELSE 0
                                                                      END AS TardyMinutes
                                                                     ,t1.Tardy AS Tardy
                                                                     ,t3.TrackTardies
                                                                     ,t3.TardiesMakingAbsence
                                                                     ,t3.PrgVerId
                                                                     ,t6.TimeIntervalDescrip AS StartTime
                                                                     ,t7.TimeIntervalDescrip AS EndTime
                                                  FROM       atClsSectAttendance t1
                                                  INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                  INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                                  INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                                  INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                                     AND (
                                                                                         CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                                         AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                                         )
                                                                                     AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                                  INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                                  INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                                  INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                                  INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                                  INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                                  INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                                  WHERE      t2.StuEnrollId = @StuEnrollId
                                                             AND (
                                                                 AAUT1.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                                                 OR AAUT2.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                                                 )
                                                             AND t1.Actual <> 9999
                                                  ) dt
                                           ) nPaByClass;


                    END;
                ELSE IF @UnitTypeDescrip IN ( ''minutes'', ''clock hours'' )
                        AND @TrackSapAttendance = ''byclass''
                         BEGIN

                             INSERT INTO @hoursBucket
                                         SELECT minChByClass.StuEnrollId
                                               ,minChByClass.MeetDate
                                               ,minChByClass.Actual / 60
                                               ,minChByClass.Scheduled / 60
                                         FROM   (
                                                SELECT *
                                                      ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                                FROM   (
                                                       SELECT     DISTINCT t1.StuEnrollId AS StuEnrollId
                                                                          ,t1.ClsSectionId
                                                                          ,t1.MeetDate AS MeetDate
                                                                          ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                                          ,t4.StartDate
                                                                          ,t4.EndDate
                                                                          ,t5.PeriodDescrip
                                                                          ,t1.Actual AS Actual
                                                                          ,t1.Excused
                                                                          ,CASE WHEN (
                                                                                     t1.Actual = 0
                                                                                     AND t1.Excused = 0
                                                                                     ) THEN t1.Scheduled
                                                                                ELSE CASE WHEN (
                                                                                               t1.Actual <> 9999.00
                                                                                               AND t1.Actual < t1.Scheduled
                                                                                               ) THEN ( t1.Scheduled - t1.Actual )
                                                                                          ELSE 0
                                                                                     END
                                                                           END AS Absent
                                                                          ,t1.Scheduled AS Scheduled
                                                                          ,CASE WHEN (
                                                                                     t1.Actual > 0
                                                                                     AND t1.Actual < t1.Scheduled
                                                                                     ) THEN ( t1.Scheduled - t1.Actual )
                                                                                ELSE 0
                                                                           END AS TardyMinutes
                                                                          ,t1.Tardy AS Tardy
                                                                          ,t3.TrackTardies
                                                                          ,t3.TardiesMakingAbsence
                                                                          ,t3.PrgVerId
                                                       FROM       atClsSectAttendance t1
                                                       INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                       INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                                       INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                                       INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                                          AND (
                                                                                              CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                                              AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                                              )
                                                                                          AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                                       INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                                       INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                                       INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                                       INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                                       INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                                       INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                                       WHERE      t2.StuEnrollId = @StuEnrollId
                                                                  AND (
                                                                      AAUT1.UnitTypeDescrip IN ( ''Minutes'', ''Clock Hours'' )
                                                                      OR AAUT2.UnitTypeDescrip IN ( ''Minutes'', ''Clock Hours'' )
                                                                      )
                                                                  AND t1.Actual <> 9999
                                                       UNION
                                                       SELECT     DISTINCT t1.StuEnrollId
                                                                          ,NULL AS ClsSectionId
                                                                          ,t1.RecordDate AS MeetDate
                                                                          ,DATENAME(dw, t1.RecordDate) AS WeekDay
                                                                          ,NULL AS StartDate
                                                                          ,NULL AS EndDate
                                                                          ,NULL AS PeriodDescrip
                                                                          ,t1.ActualHours
                                                                          ,NULL AS Excused
                                                                          ,CASE WHEN ( t1.ActualHours = 0 ) THEN t1.SchedHours
                                                                                ELSE 0
                                                                           --ELSE 
                                                                           --Case when (t1.ActualHours <> 9999.00 and t1.ActualHours < t1.SchedHours)
                                                                           --		THEN (t1.SchedHours - t1.ActualHours)
                                                                           --		ELSE 
                                                                           --			0
                                                                           --		End
                                                                           END AS Absent
                                                                          ,t1.SchedHours AS ScheduledMinutes
                                                                          ,CASE WHEN (
                                                                                     t1.ActualHours <> 9999.00
                                                                                     AND t1.ActualHours > 0
                                                                                     AND t1.ActualHours < t1.SchedHours
                                                                                     ) THEN ( t1.SchedHours - t1.ActualHours )
                                                                                ELSE 0
                                                                           END AS TardyMinutes
                                                                          ,NULL AS Tardy
                                                                          ,t3.TrackTardies
                                                                          ,t3.TardiesMakingAbsence
                                                                          ,t3.PrgVerId
                                                       FROM       arStudentClockAttendance t1
                                                       INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                       INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                                       WHERE      t2.StuEnrollId = @StuEnrollId
                                                                  AND t1.Converted = 1
                                                                  AND t1.ActualHours <> 9999
                                                       ) dt
                                                ) minChByClass;
                         END;
                ELSE IF @UnitTypeDescrip IN ( ''minutes'' )
                        AND @TrackSapAttendance = ''byday''
                         BEGIN
                             INSERT INTO @hoursBucket
                                         SELECT minByDay.StuEnrollId
                                               ,minByDay.MeetDate
                                               ,minByDay.Actual
                                               ,minByDay.Scheduled
                                         FROM   (
                                                SELECT     t1.StuEnrollId AS StuEnrollId
                                                          ,NULL AS ClsSectionId
                                                          ,t1.RecordDate AS MeetDate
                                                          ,t1.ActualHours AS Actual
                                                          ,t1.SchedHours AS Scheduled
                                                          ,CASE WHEN (
                                                                     (
                                                                     t1.SchedHours >= 1
                                                                     AND t1.SchedHours NOT IN ( 999, 9999 )
                                                                     )
                                                                     AND t1.ActualHours = 0
                                                                     ) THEN t1.SchedHours
                                                                ELSE 0
                                                           END AS Absent
                                                          ,t1.isTardy
                                                          ,(
                                                           SELECT ISNULL(SUM(SchedHours), 0)
                                                           FROM   arStudentClockAttendance
                                                           WHERE  StuEnrollId = t1.StuEnrollId
                                                                  AND RecordDate <= t1.RecordDate
                                                                  AND (
                                                                      t1.SchedHours >= 1
                                                                      AND t1.SchedHours NOT IN ( 999, 9999 )
                                                                      AND t1.ActualHours NOT IN ( 999, 9999 )
                                                                      )
                                                           ) AS ActualRunningScheduledHours
                                                          ,(
                                                           SELECT SUM(ActualHours)
                                                           FROM   arStudentClockAttendance
                                                           WHERE  StuEnrollId = t1.StuEnrollId
                                                                  AND RecordDate <= t1.RecordDate
                                                                  AND (
                                                                      t1.SchedHours >= 1
                                                                      AND t1.SchedHours NOT IN ( 999, 9999 )
                                                                      )
                                                                  AND ActualHours >= 1
                                                                  AND ActualHours NOT IN ( 999, 9999 )
                                                           ) AS ActualRunningPresentHours
                                                          ,(
                                                           SELECT COUNT(ActualHours)
                                                           FROM   arStudentClockAttendance
                                                           WHERE  StuEnrollId = t1.StuEnrollId
                                                                  AND RecordDate <= t1.RecordDate
                                                                  AND (
                                                                      t1.SchedHours >= 1
                                                                      AND t1.SchedHours NOT IN ( 999, 9999 )
                                                                      )
                                                                  AND ActualHours = 0
                                                                  AND ActualHours NOT IN ( 999, 9999 )
                                                           ) AS ActualRunningAbsentHours
                                                          ,(
                                                           SELECT SUM(ActualHours)
                                                           FROM   arStudentClockAttendance
                                                           WHERE  StuEnrollId = t1.StuEnrollId
                                                                  AND RecordDate <= t1.RecordDate
                                                                  AND SchedHours = 0
                                                                  AND ActualHours >= 1
                                                                  AND ActualHours NOT IN ( 999, 9999 )
                                                           ) AS ActualRunningMakeupHours
                                                          ,(
                                                           SELECT SUM(SchedHours - ActualHours)
                                                           FROM   arStudentClockAttendance
                                                           WHERE  StuEnrollId = t1.StuEnrollId
                                                                  AND RecordDate <= t1.RecordDate
                                                                  AND (
                                                                      (
                                                                      t1.SchedHours >= 1
                                                                      AND t1.SchedHours NOT IN ( 999, 9999 )
                                                                      )
                                                                      AND ActualHours >= 1
                                                                      AND ActualHours NOT IN ( 999, 9999 )
                                                                      )
                                                                  AND isTardy = 1
                                                           ) AS ActualRunningTardyHours
                                                          ,t3.TrackTardies
                                                          ,t3.TardiesMakingAbsence
                                                          ,t3.PrgVerId
                                                          ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
                                                FROM       arStudentClockAttendance t1
                                                INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                                INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                                --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                                                WHERE      AAUT1.UnitTypeDescrip IN ( ''Minutes'' )
                                                           AND t2.StuEnrollId = @StuEnrollId
                                                           AND t1.ActualHours <> 9999.00
                                                ) minByDay;
                         END;
                ELSE IF @UnitTypeDescrip IN ( ''present absent'', ''clock hours'' )
                        AND @TrackSapAttendance = ''byday''
                         BEGIN
                             ---- PRINT ''By Day inside day'';
                             INSERT INTO @hoursBucket
                                         SELECT paChByDay.StuEnrollId
                                               ,paChByDay.MeetDate
                                               ,paChByDay.Actual
                                               ,paChByDay.Scheduled
                                         FROM   (
                                                SELECT     t1.StuEnrollId AS StuEnrollId
                                                          ,t1.RecordDate AS MeetDate
                                                          ,t1.ActualHours AS Actual
                                                          ,t1.SchedHours AS Scheduled
                                                          ,CASE WHEN (
                                                                     (
                                                                     t1.SchedHours >= 1
                                                                     AND t1.SchedHours NOT IN ( 999, 9999 )
                                                                     )
                                                                     AND t1.ActualHours = 0
                                                                     ) THEN t1.SchedHours
                                                                ELSE 0
                                                           END AS Absent
                                                          ,t1.isTardy
                                                          ,t3.TrackTardies
                                                          ,t3.TardiesMakingAbsence
                                                FROM       arStudentClockAttendance t1
                                                INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                                INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                                --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                                                WHERE -- Unit Types: Present Absent and Clock Hour
                                                           AAUT1.UnitTypeDescrip IN ( ''present absent'', ''clock hours'' )
                                                           AND ActualHours <> 9999.00
                                                           AND t2.StuEnrollId = @StuEnrollId
                                                ) paChByDay;
                         END;
                IF ( @IncludeTransferHours = 1 )
                    BEGIN
                        INSERT INTO @hoursBucket
                                    SELECT th.StuEnrollId
                                          ,th.MeetDate
                                          ,th.Actual
                                          ,0
                                    FROM   (
                                           SELECT SE.StuEnrollId AS StuEnrollId
                                                 ,SE.StartDate AS MeetDate
                                                 ,SE.TransferHours AS Actual
                                           FROM   dbo.arStuEnrollments SE
                                           WHERE  SE.StuEnrollId = @StuEnrollId
                                           ) th;

                    END;

                IF ( @IncludeExternship = 1 )
                    BEGIN
                        INSERT INTO @hoursBucket
                                    SELECT th.StuEnrollId
                                          ,th.MeetDate
                                          ,th.Actual
                                          ,0
                                    FROM   (
                                           SELECT SE.StuEnrollId AS StuEnrollId
                                                 ,SE.AttendedDate AS MeetDate
                                                 ,SE.HoursAttended AS Actual
                                           FROM   dbo.arExternshipAttendance SE
                                           WHERE  SE.StuEnrollId = @StuEnrollId
                                           ) th;

                    END;

                BEGIN
                    IF @TrigUnitTypeDescrip = ''Actual Hours''
                        BEGIN
                            DECLARE @ActualHoursSum DECIMAL(18, 2);
                            SET @ActualHoursSum = (
                                                  SELECT SUM(t1.Actual)
                                                  FROM   @hoursBucket t1
                                                  WHERE  t1.StuEnrollId = @StuEnrollId
                                                  );
                            --return trigger met date as null if trigger has not been met yet
                            IF ( @ActualHoursSum < @TriggerValue )
                                BEGIN
                                    SET @TriggerMetDate = NULL;
                                END;
                            ELSE
                                SET @TriggerMetDate = (
                                                      SELECT TOP 1 temp.TriggerMet
                                                      FROM   (
                                                             SELECT   MIN(a.MeetDate) AS TriggerMet
                                                                     ,a.StuEnrollId
                                                             FROM     (
                                                                      SELECT     SUM(t1.Actual) AS actualHrs
                                                                                ,t2.MeetDate
                                                                                ,t2.StuEnrollId
                                                                      FROM       @hoursBucket t1
                                                                      INNER JOIN @hoursBucket t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                                                                    AND t1.MeetDate <= t2.MeetDate
                                                                      GROUP BY   t2.MeetDate
                                                                                ,t2.StuEnrollId
                                                                      HAVING     SUM(t1.Actual) >= @TriggerValue
                                                                      ) a
                                                             GROUP BY a.StuEnrollId
                                                             ) temp
                                                      );
                        END;
                    ELSE
                        BEGIN
                            DECLARE @ScheduledHoursSum DECIMAL(18, 2);
                            SET @ScheduledHoursSum = (
                                                     SELECT SUM(Scheduled)
                                                     FROM   @hoursBucket t1
                                                     WHERE  t1.StuEnrollId = @StuEnrollId
                                                     );
                            --return trigger met date as null if trigger has not been met yet
                            IF ( @ScheduledHoursSum < @TriggerValue )
                                BEGIN
                                    SET @TriggerMetDate = NULL;
                                END;
                            ELSE
                                SET @TriggerMetDate = (
                                                      SELECT TOP 1 temp.TriggerMet
                                                      FROM   (
                                                             SELECT   MIN(a.MeetDate) AS TriggerMet
                                                                     ,a.StuEnrollId
                                                             FROM     (
                                                                      SELECT     SUM(t1.Scheduled) AS scheduledHrs
                                                                                ,t2.MeetDate
                                                                                ,t2.StuEnrollId
                                                                      FROM       @hoursBucket t1
                                                                      INNER JOIN @hoursBucket t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                                                                    AND t1.MeetDate <= t2.MeetDate
                                                                      GROUP BY   t2.MeetDate
                                                                                ,t2.StuEnrollId
                                                                      HAVING     SUM(t1.Scheduled) >= @TriggerValue
                                                                      ) a
                                                             GROUP BY a.StuEnrollId
                                                             ) temp
                                                      );
                        END;
                END;

            END;

        ELSE IF @TrigUnitTypeDescrip IN ( ''Credits Attempted'' )
                 BEGIN
                     --find offset date for credits attempted here



                     INSERT INTO @creditsBucket
                                 SELECT credsAttempted.StuEnrollId
                                       ,credsAttempted.DateCompleted
                                       ,credsAttempted.CreditsAttempted
                                 FROM   (
                                        SELECT     DISTINCT SE.StuEnrollId
                                                           ,T.TermId
                                                           ,T.TermDescrip
                                                           ,T.StartDate
                                                           ,R.ReqId
                                                           ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                                           ,RES.Score AS FinalScore
                                                           ,RES.GrdSysDetailId AS FinalGrade
                                                           ,RES.DateCompleted AS DateCompleted
                                                           ,GCT.SysComponentTypeId
                                                           ,R.Credits AS CreditsAttempted
                                                           ,CS.ClsSectionId
                                                           ,GSD.Grade
                                                           ,GSD.IsPass
                                                           ,GSD.IsCreditsAttempted
                                                           ,GSD.IsCreditsEarned
                                                           ,SE.PrgVerId
                                                           ,GSD.IsInGPA
                                                           ,R.FinAidCredits AS FinAidCredits
                                                           ,RES.IsCourseCompleted
                                        FROM       arStuEnrollments SE
                                        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                        INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                                        INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                                        INNER JOIN arTerm T ON CS.TermId = T.TermId
                                        INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                        LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                        LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                        LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                        LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                        WHERE      SE.StuEnrollId = @StuEnrollId
                                                   AND (
                                                       GSD.GrdSysDetailId IS NOT NULL
                                                       AND GSD.IsCreditsAttempted = 1
                                                       )
                                                   AND RES.DateCompleted IS NOT NULL
                                        UNION
                                        (SELECT     DISTINCT SE.StuEnrollId
                                                            ,T.TermId
                                                            ,T.TermDescrip
                                                            ,T.StartDate
                                                            ,R.ReqId
                                                            ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                                            ,RES.Score AS FinalScore
                                                            ,RES.GrdSysDetailId AS FinalGrade
                                                            ,RES.CompletedDate AS DateCompleted
                                                            ,NULL
                                                            ,R.Credits AS CreditsAttempted
                                                            ,NULL AS ClsSectionId
                                                            ,GSD.Grade
                                                            ,GSD.IsPass
                                                            ,GSD.IsCreditsAttempted
                                                            ,GSD.IsCreditsEarned
                                                            ,SE.PrgVerId
                                                            ,GSD.IsInGPA
                                                            ,R.FinAidCredits AS FinAidCredits
                                                            ,RES.IsTransferred
                                         FROM       arStuEnrollments SE
                                         INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                         INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                                         --INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId and RES.TermId=CS.TermId 
                                         INNER JOIN arTerm T ON RES.TermId = T.TermId
                                         INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                                         --LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId=GBR.ClsSectionId
                                         --LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                         --LEFT JOIN arGrdComponentTypes GCT on GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId 
                                         LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                         WHERE      SE.StuEnrollId = @StuEnrollId
                                                    AND (
                                                        GSD.GrdSysDetailId IS NOT NULL
                                                        AND GSD.IsCreditsAttempted = 1
                                                        )
                                                    AND RES.CompletedDate IS NOT NULL)
                                        ) credsAttempted;

                     DECLARE @CreditsAttemptedSum DECIMAL(18, 2);
                     SET @CreditsAttemptedSum = (
                                                SELECT SUM(t1.CreditsAttempted)
                                                FROM   @creditsBucket t1
                                                WHERE  t1.StuEnrollId = @StuEnrollId
                                                );
                     --return trigger met date as null if trigger has not been met yet
                     IF ( @CreditsAttemptedSum < @TriggerValue )
                         BEGIN
                             SET @TriggerMetDate = NULL;
                         END;
                     ELSE
                         BEGIN
                             SET @TriggerMetDate = (
                                                   SELECT TOP 1 temp.TriggerMet
                                                   FROM   (
                                                          SELECT   MIN(a.DateCompleted) AS TriggerMet
                                                                  ,a.StuEnrollId
                                                          FROM     (
                                                                   SELECT     SUM(t1.CreditsAttempted) AS creditsAttempted
                                                                             ,t2.DateCompleted
                                                                             ,t2.StuEnrollId
                                                                   FROM       @creditsBucket t1
                                                                   INNER JOIN @creditsBucket t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                                                                   AND t1.DateCompleted <= t2.DateCompleted
                                                                   GROUP BY   t2.DateCompleted
                                                                             ,t2.StuEnrollId
                                                                   HAVING     SUM(t1.CreditsAttempted) >= @TriggerValue
                                                                   ) a
                                                          GROUP BY a.StuEnrollId
                                                          ) temp
                                                   );
                         END;

                 END;

    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_UpdateFutureStartsToCurrentlyAttending]'
GO
IF OBJECT_ID(N'[dbo].[USP_UpdateFutureStartsToCurrentlyAttending]', 'P') IS NULL
EXEC sp_executesql N'
--=================================================================================================
-- USP_UpdateFutureStartsToCurrentlyAttending
--=================================================================================================
-- =============================================
-- Author:		Ginzo, John
-- Create date: 03/31/2015
-- Description:	Update statuses for future starts
--				to currently attending based on 
--				attendance posted or start date
--				in the past
-- =============================================
CREATE PROCEDURE [dbo].[USP_UpdateFutureStartsToCurrentlyAttending]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        ------------------------------------------------
        -- GET currently attending status code
        ------------------------------------------------
        DECLARE @curAttending UNIQUEIDENTIFIER;
        DECLARE @transDate DATETIME = GETDATE();
        DECLARE @CampusGroupStatuses TABLE
            (
                CampGrpId UNIQUEIDENTIFIER NOT NULL
               ,StatusCodeId UNIQUEIDENTIFIER NOT NULL
            );

        SET @curAttending = (
                            SELECT     TOP 1 StatusCodeId
                            FROM       dbo.syStatusCodes sc
                            INNER JOIN dbo.sySysStatus ss ON sc.SysStatusId = ss.SysStatusId
                            WHERE      ss.SysStatusId = 9
                            );
        ------------------------------------------------


        ------------------------------------------------
        -- GET currently attending status code
        ------------------------------------------------
        CREATE TABLE #MinAttendance1
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,StudentAttendedDate DATETIME
            );



        INSERT INTO #MinAttendance1
                    SELECT   StuEnrollId
                            ,MAX(StudentAttendedDate) AS StudentAttendedDate
                    FROM     dbo.syStudentAttendanceSummary
                    WHERE    ActualDays != 9999.0
                    GROUP BY StuEnrollId;



        CREATE TABLE #EnrollmentsIsAttendanceTaking
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,IsAttendanceTaking BIT
               ,StartDate DATETIME
               ,StudentAttendedDate DATETIME
               ,ReEnrollDate DATETIME
            );

        INSERT INTO #EnrollmentsIsAttendanceTaking
                    SELECT     e.StuEnrollId
                              ,CASE WHEN ut.UnitTypeDescrip IS NULL
                                         OR ut.UnitTypeDescrip = ''None'' THEN 0
                                    ELSE 1
                               END AS IsAttendanceTaking
                              ,ISNULL(e.StartDate, e.ExpStartDate) AS StartDate
                              ,ma.StudentAttendedDate
                              ,e.ReEnrollmentDate
                    FROM       dbo.arStuEnrollments e
                    INNER JOIN dbo.arPrgVersions pv ON e.PrgVerId = pv.PrgVerId
                    LEFT JOIN  dbo.arAttUnitType ut ON pv.UnitTypeId = ut.UnitTypeId
                    INNER JOIN dbo.syStatusCodes sc ON e.StatusCodeId = sc.StatusCodeId
                    INNER JOIN dbo.sySysStatus SS ON sc.SysStatusId = SS.SysStatusId
                    LEFT JOIN  #MinAttendance1 ma ON e.StuEnrollId = ma.StuEnrollId
                    WHERE      sc.SysStatusId = 7;


        CREATE TABLE #EnrollmentsToChange
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ChangeToCurrentStatus BIT
               ,OriginalStatusId UNIQUEIDENTIFIER
               ,NewStatusId UNIQUEIDENTIFIER
               ,CampusId UNIQUEIDENTIFIER
               ,DateOfChange DATETIME
            );

        INSERT INTO #EnrollmentsToChange
                    SELECT     e.StuEnrollId
                              ,( CASE WHEN ISNULL(e.StartDate, e.ExpStartDate) < GETDATE() THEN 1
                                      WHEN ia.IsAttendanceTaking = 1 THEN
                              ( CASE WHEN ia.StudentAttendedDate IS NOT NULL THEN CASE WHEN ia.ReEnrollDate IS NULL THEN 1
                                                                                       --WHEN ia.ReEnrollDate > ia.StudentAttendedDate THEN 0
                                                                                       WHEN ia.ReEnrollDate IS NOT NULL
                                                                                            AND ia.ReEnrollDate <= GETDATE() THEN 1
                                                                                       ELSE 0
                                                                                  END
                                     ELSE 0
                                END
                              )
                                      ELSE CASE WHEN ia.ReEnrollDate IS NOT NULL THEN CASE WHEN ia.ReEnrollDate <= GETDATE() THEN 1
                                                                                           ELSE 0
                                                                                      END
                                                ELSE 0
                                           END
                                 END
                               ) AS ChangeToCurrentStatus
                              ,e.StatusCodeId
                              ,@curAttending
                              ,e.CampusId
                              ,CASE WHEN IsAttendanceTaking = 1 AND Ia.StudentAttendedDate IS NOT null THEN ia.StudentAttendedDate -- when is posting attendance
                                    WHEN e.StartDate <= @transDate AND ia.IsAttendanceTaking = 1 THEN @transDate --when running job and startdate has passed or is same as execution date
                                    ELSE ISNULL(e.StartDate, ExpStartDate)
                               END AS DateOfChange
                    FROM       arStuEnrollments e
                    INNER JOIN #EnrollmentsIsAttendanceTaking ia ON e.StuEnrollId = ia.StuEnrollId
                    INNER JOIN dbo.syStatusCodes sc ON e.StatusCodeId = sc.StatusCodeId
                    INNER JOIN dbo.sySysStatus SS ON sc.SysStatusId = SS.SysStatusId
                    WHERE      sc.SysStatusId = 7;
   
        INSERT INTO @CampusGroupStatuses
                    SELECT   pv.CampGrpId
                            ,StatusCodeId = dbo.GetCurrentlyAttendingDefaultStatusIdForCampusGroup(pv.CampGrpId)
                    FROM     #EnrollmentsToChange e
                    JOIN     dbo.arStuEnrollments b ON e.StuEnrollId = b.StuEnrollId
                    JOIN     dbo.arPrgVersions pv ON b.PrgVerId = pv.PrgVerId
                    WHERE    b.StuEnrollId IN (
                                              SELECT StuEnrollId
                                              FROM   #EnrollmentsToChange
                                              WHERE  ChangeToCurrentStatus = 1
                                              )
                    GROUP BY pv.CampGrpId;



        ------------------------------------------------
        -- update the necessary tables
        ------------------------------------------------
        BEGIN TRAN UpdateFutureStarts;

        DECLARE @curDateTime DATETIME; --datetime to use in updated
        SET @curDateTime = GETDATE();

        ------------------------------------------------
        -- update the status in enrollments
        ------------------------------------------------
        UPDATE e
        SET    StatusCodeId = cgs.StatusCodeId
              ,ModDate = @curDateTime
              ,ModUser = ''support''
        FROM   #EnrollmentsToChange etu
        JOIN   dbo.arStuEnrollments e ON e.StuEnrollId = etu.StuEnrollId
        JOIN   dbo.arPrgVersions pv ON pv.PrgVerId = e.PrgVerId
        JOIN   @CampusGroupStatuses cgs ON cgs.CampGrpId = pv.CampGrpId
        WHERE  e.StuEnrollId IN (
                                SELECT StuEnrollId
                                FROM   #EnrollmentsToChange
                                WHERE  ChangeToCurrentStatus = 1
                                );

        IF @@error <> 0
            BEGIN
                ROLLBACK TRAN UpdateFutureStarts;
                RETURN -1;
            END;
        ------------------------------------------------

        ------------------------------------------------
        -- update the status change history table
        ------------------------------------------------
        INSERT INTO dbo.syStudentStatusChanges (
                                               StudentStatusChangeId
                                              ,StuEnrollId
                                              ,OrigStatusId
                                              ,NewStatusId
                                              ,CampusId
                                              ,ModDate
                                              ,ModUser
                                              ,IsReversal
                                              ,DropReasonId
                                              ,DateOfChange
                                               )
                    SELECT NEWID()
                          ,ia.StuEnrollId
                          ,ia.OriginalStatusId
                          ,cgs.StatusCodeId
                          ,ia.CampusId
                          ,@curDateTime
                          ,''support''
                          ,0
                          ,NULL
                          ,ia.DateOfChange
                    FROM   #EnrollmentsToChange ia
                    JOIN   dbo.arStuEnrollments e ON e.StuEnrollId = ia.StuEnrollId
                    JOIN   dbo.arPrgVersions pv ON pv.PrgVerId = e.PrgVerId
                    JOIN   @CampusGroupStatuses cgs ON cgs.CampGrpId = pv.CampGrpId
                    WHERE  ia.ChangeToCurrentStatus = 1;


        IF @@error <> 0
            BEGIN
                ROLLBACK TRAN UpdateFutureStarts;
                RETURN -1;
            END;
        ------------------------------------------------

        ------------------------------------------------
        -- ALL GOOD - commit and drop temps
        ------------------------------------------------
        COMMIT TRAN UpdateFutureStarts;

        DROP TABLE #MinAttendance1;
        DROP TABLE #EnrollmentsIsAttendanceTaking;
        DROP TABLE #EnrollmentsToChange;
        ------------------------------------------------

        RETURN 0;
    END;
--=================================================================================================
-- END  --  USP_UpdateFutureStartsToCurrentlyAttending
--=================================================================================================

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[GlTransView1]'
GO
IF OBJECT_ID(N'[dbo].[GlTransView1]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[GlTransView1]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[GlTransView2]'
GO
IF OBJECT_ID(N'[dbo].[GlTransView2]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[GlTransView2]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[NewStudentSearch]'
GO
IF OBJECT_ID(N'[dbo].[NewStudentSearch]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[NewStudentSearch]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[SettingStdScoreLimit]'
GO
IF OBJECT_ID(N'[dbo].[SettingStdScoreLimit]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[SettingStdScoreLimit]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[StudentEnrollmentSearch]'
GO
IF OBJECT_ID(N'[dbo].[StudentEnrollmentSearch]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[StudentEnrollmentSearch]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END

/*
Run this script on:

        dev-com-db1\Adv.AvedaLive    -  This database will be modified

to synchronize it with a database with the schema represented by:

        Source

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.7.10021 from Red Gate Software Ltd at 10/10/2019 2:09:58 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
/*
* Use this Pre-Deployment script to perform tasks before the deployment of the project.
* Read more at https://www.red-gate.com/SOC7/pre-deployment-script-help
*/
UPDATE dbo.arClsSectMeetings
SET PeriodId = NULL
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)

DELETE FROM syPeriodsWorkDays
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_FA_PostZerosForStudents]'
GO
IF OBJECT_ID(N'[dbo].[USP_FA_PostZerosForStudents]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_FA_PostZerosForStudents]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_FA_PostZerosForStudents]'
GO
IF OBJECT_ID(N'[dbo].[USP_FA_PostZerosForStudents]', 'P') IS NULL
EXEC sp_executesql N'-- =============================================
-- Author:		FAME Inc.
-- Create date: 5/23/2019
-- Description:	For arStudentClockAttendanceSchools posting zeros. Given enroll id list and record date, posts zeros for students on this date. Will post zero''s for all students in @StuEnrollIdList
--				wihout checking students in school status. Filtering of students should be down prior to the execution of this SP.	   				
-- =============================================
CREATE   PROCEDURE [dbo].[USP_FA_PostZerosForStudents]
    @StuEnrollIdList VARCHAR(MAX) = NULL
   ,@Date DATETIME
   ,@ModUser VARCHAR(50)
AS
    BEGIN
        DECLARE @ModDate DATETIME = GETDATE();

        --update placeholder records if they exist for given student enrollments on given day
        UPDATE dbo.arStudentClockAttendance
              SET	ActualHours = 0
                  ,SchedHours = a.CalculatedScheduledHours
                  ,ModDate = @ModDate
                  ,ModUser = @ModUser
                  ,isTardy = 0
                  ,PostByException = ''no''
                  ,comments = NULL
                  ,TardyProcessed = 0
                  ,Converted = 0
              FROM   dbo.arStudentClockAttendance
              JOIN   dbo.StudentScheduledHours(@StuEnrollIdList, @Date) a ON a.StuEnrollId = arStudentClockAttendance.StuEnrollId
              WHERE  CAST(RecordDate AS DATE) = CAST(@Date AS DATE)
                     AND ActualHours IN(99.0, 999.0, 9999.0) AND arStudentClockAttendance.StuEnrollId IN (SELECT Val
                                   FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)); 

        --create records if they do not exist for given student enrollments on given day 
        INSERT INTO dbo.arStudentClockAttendance (
                                                 StuEnrollId
                                                ,ScheduleId
                                                ,RecordDate
                                                ,SchedHours
                                                ,ActualHours
                                                ,ModDate
                                                ,ModUser
                                                ,isTardy
                                                ,PostByException
                                                ,comments
                                                ,TardyProcessed
                                                ,Converted
                                                 )
                    SELECT StuEnrollList.StuEnrollId  -- StuEnrollId - uniqueidentifier
                          ,a.ScheduleId               -- ScheduleId - uniqueidentifier
                          ,CAST(@Date AS DATE)		  -- RecordDate - smalldatetime
                          ,a.CalculatedScheduledHours -- SchedHours - decimal(18, 2)
                          ,0                          -- ActualHours - decimal(18, 2)
                          ,@ModDate                   -- ModDate - smalldatetime
                          ,@ModUser                   -- ModUser - varchar(50)
                          ,0                          -- isTardy - bit
                          ,''no''                       -- PostByException - varchar(10)
                          ,NULL                       -- comments - varchar(240)
                          ,0                          -- TardyProcessed - bit
                          ,0                          -- Converted - bit
                    FROM   (
                           SELECT CAST(Val AS UNIQUEIDENTIFIER) AS StuEnrollId
                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                           ) StuEnrollList
                    JOIN   dbo.StudentScheduledHours(@StuEnrollIdList, @Date) a ON a.StuEnrollId = StuEnrollList.StuEnrollId
                    WHERE  NOT EXISTS (
                                      SELECT *
                                      FROM   dbo.arStudentClockAttendance
                                      WHERE  StuEnrollId = StuEnrollList.StuEnrollId
                                             AND CAST(RecordDate AS DATE) = CAST(@Date AS DATE)
                                      );
    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
