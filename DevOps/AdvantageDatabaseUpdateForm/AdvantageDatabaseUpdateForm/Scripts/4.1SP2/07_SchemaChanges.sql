﻿/*
Run this script on:

        dev-com-db1\Adv.AvedaLive    -  This database will be modified

to synchronize it with a database with the schema represented by:

        Source

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.7.10021 from Red Gate Software Ltd at 9/24/2019 8:41:47 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_GetAbsentToday]'
GO
IF OBJECT_ID(N'[dbo].[USP_GetAbsentToday]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_GetAbsentToday]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[usp_GetStudentLedgerReportSummary]'
GO
IF OBJECT_ID(N'[dbo].[usp_GetStudentLedgerReportSummary]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[usp_GetStudentLedgerReportSummary]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[usp_GetStudentLedgerDataGrid]'
GO
IF OBJECT_ID(N'[dbo].[usp_GetStudentLedgerDataGrid]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[usp_GetStudentLedgerDataGrid]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_TR_Sub10_Courses]'
GO
IF OBJECT_ID(N'[dbo].[Usp_TR_Sub10_Courses]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_TR_Sub10_Courses]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[usp_GetClassroomWorkServices]'
GO
IF OBJECT_ID(N'[dbo].[usp_GetClassroomWorkServices]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[usp_GetClassroomWorkServices]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_NACCAS_CohortGrid]'
GO
IF OBJECT_ID(N'[dbo].[USP_NACCAS_CohortGrid]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_NACCAS_CohortGrid]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudent]'
GO
IF OBJECT_ID(N'[dbo].[arStudent]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[arScheduledHoursAdjustments]'
GO
IF OBJECT_ID(N'[dbo].[arScheduledHoursAdjustments]', 'U') IS NULL
CREATE TABLE [dbo].[arScheduledHoursAdjustments]
(
[ScheduledHoursAdjustmentId] [uniqueidentifier] NOT NULL,
[StuEnrollId] [uniqueidentifier] NOT NULL,
[RecordDate] [smalldatetime] NOT NULL,
[AdjustmentAmount] [decimal] (18, 2) NULL,
[ModDate] [smalldatetime] NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_arScheduledHoursAdjustments_ScheduledHoursAdjustmentId] on [dbo].[arScheduledHoursAdjustments]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK_arScheduledHoursAdjustments_ScheduledHoursAdjustmentId]', 'PK') AND parent_object_id = OBJECT_ID(N'[dbo].[arScheduledHoursAdjustments]', 'U'))
ALTER TABLE [dbo].[arScheduledHoursAdjustments] ADD CONSTRAINT [PK_arScheduledHoursAdjustments_ScheduledHoursAdjustmentId] PRIMARY KEY CLUSTERED  ([ScheduledHoursAdjustmentId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_GetAbsentToday]'
GO
IF OBJECT_ID(N'[dbo].[USP_GetAbsentToday]', 'P') IS NULL
EXEC sp_executesql N'

--=================================================================================================
-- USP_GetAbsentToday
--=================================================================================================
-- =============================================
-- Author:		Kimberly 
-- Create date: 05/13/2019
-- Description: Get List of students that are Absent Today
--             
--                 
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAbsentToday]
    @DateRun DATE,
    @CampusId UNIQUEIDENTIFIER
AS
BEGIN

    DECLARE @isTimeClock BIT;
    SET @isTimeClock = 1;
    IF EXISTS
    (
        SELECT *
        FROM dbo.arPrgVersions prg
            JOIN dbo.arStuEnrollments ste
                ON ste.PrgVerId = prg.PrgVerId
        WHERE ste.CampusId = @CampusId
              AND UseTimeClock = 0
    )
    BEGIN
        SET @isTimeClock = 0;
    END;


    IF (@isTimeClock = 0)
    BEGIN
        
        SELECT DISTINCT
               LTRIM(RTRIM(cp.CampDescrip)) AS CampDescrip,
               l.StudentNumber,
               (FirstName + '' '' + LastName) AS FullName,
               FirstName,
               LastName,
               (
                   SELECT STUFF(STUFF(STUFF(
                                      (
                                          SELECT TOP (1)
                                                 Phone
                                          FROM adLeadPhone
                                          WHERE LeadId = l.LeadId
                                          ORDER BY IsBest DESC,
                                                   IsShowOnLeadPage DESC,
                                                   Position DESC
                                      ),
                                      1,
                                      0,
                                      ''(''
                                           ),
                                      5,
                                      0,
                                      '') ''
                                     ),
                                10,
                                0,
                                ''-''
                               )
               ) AS Phone,
               sc.StatusCodeDescrip,
               (
                   SELECT TOP (1)
                          CONVERT(DATE, RecordDate)
                   FROM arStudentClockAttendance
                   WHERE SchedHours > 0
                         AND ActualHours > 0
                         AND StuEnrollId = e.StuEnrollId
                   ORDER BY RecordDate DESC
               ) AS LDA
        FROM dbo.arStuEnrollments e
            JOIN syStatusCodes sc
                ON sc.StatusCodeId = e.StatusCodeId
            JOIN dbo.arStudentSchedules ss
                ON ss.StuEnrollId = e.StuEnrollId
            JOIN dbo.arProgSchedules ps
                ON ps.ScheduleId = ss.ScheduleId
            JOIN dbo.arProgScheduleDetails psd
                ON psd.ScheduleId = ss.ScheduleId
            JOIN arStudentClockAttendance sca
                ON sca.StuEnrollId = e.StuEnrollId
            JOIN adLeads l
                ON l.LeadId = e.LeadId
            JOIN dbo.adLeadPhone lp
                ON lp.LeadId = l.LeadId
            JOIN dbo.syCampuses cp
                ON cp.CampusId = e.CampusId
        WHERE e.ExpStartDate < @DateRun
              AND e.ExpGradDate > @DateRun
              AND cp.CampusId = @CampusId
              AND e.StuEnrollId IN
                  (
                      SELECT DISTINCT
                             sc.StuEnrollId
                      FROM arStudentClockAttendance sc
                          JOIN dbo.arStuEnrollments e
                              ON e.StuEnrollId = sc.StuEnrollId
                      WHERE CONVERT(DATE, sc.RecordDate) = @DateRun
                            AND sc.ActualHours = 0
                            AND sc.SchedHours > 0
                  );
        RETURN;

    END;

    IF (@isTimeClock = 1)
    BEGIN

        DECLARE @dayOfWeek INT;
        SET @dayOfWeek =
        (
            SELECT CASE DATENAME(WEEKDAY, @DateRun)
                       WHEN ''Monday'' THEN
                           1
                       WHEN ''Tuesday'' THEN
                           2
                       WHEN ''Wednesday'' THEN
                           3
                       WHEN ''Thursday'' THEN
                           4
                       WHEN ''Friday'' THEN
                           5
                       WHEN ''Saturday'' THEN
                           6
                       WHEN ''Sunday'' THEN
                           7
                   END AS wDay
        );

        SELECT DISTINCT
               LTRIM(RTRIM(cp.CampDescrip)) AS CampDescrip,
               l.StudentNumber,
               (FirstName + '' '' + LastName) AS FullName,
               FirstName,
               LastName,
               (
                   SELECT STUFF(STUFF(STUFF(
                                      (
                                          SELECT TOP (1)
                                                 Phone
                                          FROM adLeadPhone
                                          WHERE LeadId = l.LeadId
                                          ORDER BY IsBest DESC,
                                                   IsShowOnLeadPage DESC,
                                                   Position DESC
                                      ),
                                      1,
                                      0,
                                      ''(''
                                           ),
                                      5,
                                      0,
                                      '') ''
                                     ),
                                10,
                                0,
                                ''-''
                               )
               ) AS Phone,
               sc.StatusCodeDescrip,
               (
                   SELECT TOP (1)
                          CONVERT(DATE, RecordDate)
                   FROM arStudentClockAttendance
                   WHERE SchedHours > 0
                         AND ActualHours > 0
                         AND StuEnrollId = e.StuEnrollId
                   ORDER BY RecordDate DESC
               ) AS LDA
        FROM dbo.arStuEnrollments e
            JOIN syStatusCodes sc
                ON sc.StatusCodeId = e.StatusCodeId
            JOIN dbo.arStudentSchedules ss
                ON ss.StuEnrollId = e.StuEnrollId
            JOIN dbo.arProgSchedules ps
                ON ps.ScheduleId = ss.ScheduleId
            JOIN dbo.arProgScheduleDetails psd
                ON psd.ScheduleId = ss.ScheduleId
            JOIN arStudentTimeClockPunches p
                ON p.StuEnrollId = e.StuEnrollId
            JOIN adLeads l
                ON l.LeadId = e.LeadId
            JOIN dbo.adLeadPhone lp
                ON lp.LeadId = l.LeadId
            JOIN dbo.syCampuses cp
                ON cp.CampusId = e.CampusId
        WHERE e.ExpStartDate < @DateRun
              AND e.ExpGradDate > @DateRun
              AND psd.dw = @dayOfWeek
              AND psd.total > 0
              AND p.Status = 1
              AND e.CampusId = @CampusId
              AND e.StuEnrollId NOT IN
                  (
                      SELECT DISTINCT
                             sp.StuEnrollId
                      FROM arStudentTimeClockPunches sp
                          JOIN dbo.arStuEnrollments e
                              ON e.StuEnrollId = sp.StuEnrollId
                      WHERE CONVERT(DATE, PunchTime) = @DateRun
                  )
              AND SysStatusId IN ( 9, 20, 6 )
        ORDER BY l.LastName,
                 l.FirstName;
        RETURN;
    END;


END;
--=================================================================================================
-- END  --  USP_GetAbsentToday
--=================================================================================================


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[usp_GetClassroomWorkServices]'
GO
IF OBJECT_ID(N'[dbo].[usp_GetClassroomWorkServices]', 'P') IS NULL
EXEC sp_executesql N'/* 
PURPOSE: 
Get component summaries for a selected student enrollment, class section and component type

CREATED: 


MODIFIED:
10/25/2013 WP	US4547 Refactor Post Services by Student process

ResourceId
Homework = 499
LabWork = 500
Exam = 501
Final = 502
LabHours = 503
Externship = 544

*/

CREATE PROCEDURE [dbo].[usp_GetClassroomWorkServices]
    (
        @StuEnrollId UNIQUEIDENTIFIER
       ,@ClsSectionId UNIQUEIDENTIFIER
       ,@GradeBookComponentType INT
    )
AS
    SET NOCOUNT ON;


    DECLARE @ReqId AS UNIQUEIDENTIFIER;
    SELECT @ReqId = ReqId
    FROM   arClassSections
    WHERE  ClsSectionId = @ClsSectionId;

    SELECT *
    INTO   #PostedServices
    FROM   arGrdBkResults
    WHERE  StuEnrollId = @StuEnrollId
	AND PostDate IS NOT NULL AND Score IS NOT null
           AND ClsSectionId = @ClsSectionId
           AND InstrGrdBkWgtDetailId IN (
                                        SELECT InstrGrdBkWgtDetailId
                                        FROM   arGrdBkWgtDetails posted_gbwd
                                        JOIN   arGrdComponentTypes posted_gct ON posted_gct.GrdComponentTypeId = posted_gbwd.GrdComponentTypeId
                                        WHERE  posted_gct.SysComponentTypeId = @GradeBookComponentType
                                        );

    --SELECT * FROM #PostedServices

    DECLARE @ClinicScoresEnabled BIT = (
                                       SELECT CASE WHEN ISNULL(
                                                                  dbo.GetAppSettingValueByKeyName(
                                                                                                     ''ClinicServicesScoresEnabled''
                                                                                                    ,(
                                                                                                    SELECT CampusId
                                                                                                    FROM   dbo.arStuEnrollments
                                                                                                    WHERE  StuEnrollId = @StuEnrollId
                                                                                                    )
                                                                                                 )
                                                                 ,''''
                                                              ) = ''No'' THEN 0
                                                   ELSE 1
                                              END
                                       );

    SELECT    gct.Descrip
             ,ISNULL(gbwd.Number, 0) AS Required
             ,CASE WHEN @GradeBookComponentType = 503 THEN ISNULL((
                                                                  SELECT SUM(Score)
                                                                  FROM   #PostedServices
                                                                  WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                                                  )
                                                                 ,0
                                                                 )
                   ELSE ISNULL((
                               SELECT COUNT(*)
                               FROM   #PostedServices
                               WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                               )
                              ,0
                              )
              END AS Completed
             ,CASE WHEN @GradeBookComponentType = 503 THEN CASE WHEN ( ISNULL(gbwd.Number, 0) - ISNULL(( SUM(ps.Score)), 0)) <= 0 THEN 0
                                                                ELSE ( ISNULL(gbwd.Number, 0) - ISNULL(( SUM(ps.Score)), 0))
                                                           END
                   ELSE CASE WHEN ( ISNULL(gbwd.Number, 0) - ISNULL((
                                                                    SELECT COUNT(*)
                                                                    FROM   #PostedServices
                                                                    WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                                                    )
                                                                   ,0
                                                                   )
                                  ) <= 0 THEN 0
                             ELSE ( ISNULL(gbwd.Number, 0) - ISNULL((
                                                                    SELECT COUNT(*)
                                                                    FROM   #PostedServices
                                                                    WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                                                    )
                                                                   ,0
                                                                   )
                                  )
                        END
              END AS Remaining
             ,CASE WHEN @GradeBookComponentType = 503 THEN ISNULL((
                                                                  SELECT SUM(Score)
                                                                  FROM   #PostedServices
                                                                  WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                                                         AND PostDate IS NOT NULL
                                                                         AND Score IS NOT NULL
                                                                  )
                                                                 ,0
                                                                 )
                   WHEN @ClinicScoresEnabled = 1 THEN AVG(ps.Score)
                   ELSE ( ISNULL((
                                 SELECT COUNT(*)
                                 FROM   #PostedServices
                                 WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                        AND PostDate IS NOT NULL
                                        AND Score IS NOT NULL
                                 )
                                ,0
                                )
                        )
              END AS Average
             ,se.EnrollDate
             ,gbwd.InstrGrdBkWgtDetailId
             ,@ClsSectionId AS ClsSectionId
             ,gbwd.Seq
    FROM      arGrdBkWeights gbw
    LEFT JOIN arGrdBkWgtDetails gbwd ON gbwd.InstrGrdBkWgtId = gbw.InstrGrdBkWgtId
    LEFT JOIN arGrdComponentTypes gct ON gct.GrdComponentTypeId = gbwd.GrdComponentTypeId
    LEFT JOIN arClassSections cs ON cs.ReqId = gbw.ReqId
    LEFT JOIN arResults ar ON ar.TestId = cs.ClsSectionId
    LEFT JOIN arStuEnrollments se ON ar.StuEnrollId = se.StuEnrollId
    LEFT JOIN #PostedServices ps ON ps.InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
    WHERE     gct.SysComponentTypeId = @GradeBookComponentType
              AND gbw.ReqId = @ReqId
              AND ar.TestId = @ClsSectionId
              AND ar.StuEnrollId = @StuEnrollId
    GROUP BY  gct.Descrip
             ,gbwd.Number
             ,se.EnrollDate
             ,gbwd.InstrGrdBkWgtDetailId
             ,gbwd.Seq
    ORDER BY  gbwd.Seq
             ,gct.Descrip;



    DROP TABLE #PostedServices;









'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[usp_GetStudentLedgerDataGrid]'
GO
IF OBJECT_ID(N'[dbo].[usp_GetStudentLedgerDataGrid]', 'P') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[usp_GetStudentLedgerDataGrid]
    @StudentEnrollmentId UNIQUEIDENTIFIER
AS
    SELECT    T.TransDate AS TransactionDate
             ,( CASE WHEN P.PaymentTypeId = 1 THEN ''Cash''
                     WHEN P.PaymentTypeId = 2 THEN ''Check Number:''
                     WHEN P.PaymentTypeId = 3 THEN ''C/C Authorization:''
                     WHEN P.PaymentTypeId = 4 THEN ''EFT Number:''
                     WHEN P.PaymentTypeId = 5 THEN ''Money Order Number:''
                     WHEN P.PaymentTypeId = 6 THEN ''Non Cash Reference #:''
                     ELSE ''''
                END
              ) + P.CheckNumber AS Document
             ,CASE WHEN RTRIM(LTRIM(T.TransDescrip)) = '''' THEN TC.TransCodeDescrip
                   ELSE RTRIM(LTRIM(T.TransDescrip)) + ( CASE WHEN TC.TransCodeId IS NOT NULL
                                                                   AND TC.SysTransCodeId IN ( 11, 16 ) THEN '' ('' + TC.TransCodeDescrip + '')''
                                                              ELSE ''''
                                                         END
                                                       )
              END AS TransactionDescription
             ,FORMAT(T.TransAmount, ''C'') AS Amount
             ,FORMAT(   SUM(T.TransAmount) OVER ( ORDER BY T.TransDate asc
														  ,t.ModDate asc
														  ,T.TransactionId
                                                )
                       ,''C''
                    ) AS Balance
             ,CASE WHEN T.PaymentPeriodNumber IS NOT NULL THEN ''Payment Period''
                   ELSE ''''
              END AS PeriodType
             ,CASE WHEN T.PaymentPeriodNumber IS NOT NULL THEN T.PaymentPeriodNumber
                   ELSE NULL
              END AS Period
    FROM      dbo.saTransactions T
    LEFT JOIN dbo.saPayments P ON P.TransactionId = T.TransactionId
    LEFT JOIN dbo.saTransCodes TC ON TC.TransCodeId = T.TransCodeId
    WHERE     T.StuEnrollId = @StudentEnrollmentId
              AND T.Voided = 0
    ORDER BY  TransactionDate ASC, t.ModDate ASC, T.TransactionId;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[usp_GetStudentLedgerReportSummary]'
GO
IF OBJECT_ID(N'[dbo].[usp_GetStudentLedgerReportSummary]', 'P') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[usp_GetStudentLedgerReportSummary]
    @StudentEnrollmentId UNIQUEIDENTIFIER
AS
    SELECT   RTRIM(LTRIM(Summary.FundSourceDescrip)) AS Description
            ,Summary.AwardTypeId
            ,FORMAT((
                    SELECT SUM(GrossAmount) - SUM(TA.LoanFees)
                    FROM   dbo.faStudentAwards TA
                    WHERE  TA.AwardTypeId = Summary.AwardTypeId
                           AND TA.StuEnrollId = @StudentEnrollmentId
                    )
                   ,''C''
                   ) AS Amount
            ,FORMAT(SUM(   CASE WHEN Summary.IsDisbursement = 1 THEN Summary.Amount
                                ELSE 0
                           END
                       )
                   ,''C''
                   ) AS Received
            ,FORMAT(SUM(   CASE WHEN Summary.IsDisbursement = 0 THEN Summary.Amount
                                ELSE 0
                           END
                       )
                   ,''C''
                   ) AS Refunded
    FROM     (
             SELECT    D.PmtDisbRelId AS Id
                      ,D.Amount AS Amount
                      ,S.StudentAwardId AS AwardId
                      ,A.AwardTypeId
                      ,FS.FundSourceDescrip
                      ,A.GrossAmount
                      ,1 AS IsDisbursement
             FROM      dbo.faStudentAwardSchedule S
             LEFT JOIN dbo.saPmtDisbRel D ON S.AwardScheduleId = D.AwardScheduleId
             LEFT JOIN dbo.faStudentAwards A ON A.StudentAwardId = S.StudentAwardId
             LEFT JOIN dbo.saFundSources FS ON FS.FundSourceId = A.AwardTypeId
             LEFT JOIN dbo.saTransactions T ON T.TransactionId = D.TransactionId
             WHERE     A.StuEnrollId = @StudentEnrollmentId
                       AND (
                           T.Voided = 0
                           OR T.TransactionId IS NULL
                           )
                       AND FS.TitleIV = 1
             UNION
             SELECT    R.TransactionId AS Id
                      ,R.RefundAmount AS Amount
                      ,S.StudentAwardId AS AwardId
                      ,A.AwardTypeId
                      ,FS.FundSourceDescrip
                      ,A.GrossAmount
                      ,0 AS IsDisbursement
             FROM      dbo.faStudentAwardSchedule S
             LEFT JOIN dbo.saRefunds R ON S.AwardScheduleId = R.AwardScheduleId
             LEFT JOIN dbo.faStudentAwards A ON A.StudentAwardId = S.StudentAwardId
             LEFT JOIN dbo.saFundSources FS ON FS.FundSourceId = A.AwardTypeId
             LEFT JOIN dbo.saTransactions T ON T.TransactionId = R.TransactionId
             WHERE     A.StuEnrollId = @StudentEnrollmentId
                       AND (
                           T.Voided = 0
                           OR T.TransactionId IS NULL
                           )
                       AND FS.TitleIV = 1
             ) Summary
    GROUP BY Summary.AwardTypeId
            ,Summary.FundSourceDescrip;





'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_NACCAS_CohortGrid]'
GO
IF OBJECT_ID(N'[dbo].[USP_NACCAS_CohortGrid]', 'P') IS NULL
EXEC sp_executesql N'

CREATE PROCEDURE [dbo].[USP_NACCAS_CohortGrid]
    @PrgIdList VARCHAR(MAX) = NULL
   ,@CampusId VARCHAR(50)
   ,@ReportYear INT
   ,@ReportType VARCHAR(50)
AS
    DECLARE @StartMonth INT = 1
           ,@StartDay INT = 1
           ,@EndMonth INT = 12
           ,@EndDay INT = 31
           ,@FutureStart INT = 7
           ,@CurrentlyAttending INT = 9
           ,@LOA INT = 10
           ,@Suspension INT = 11
           ,@Dropped INT = 12
           ,@Graduated INT = 14
           ,@Transfer INT = 19
           ,@Preliminary VARCHAR(50) = ''Preliminary''
           ,@FallAnnual VARCHAR(50) = ''FallAnnual''
           ,@MilitaryTransfer UNIQUEIDENTIFIER
           ,@CallToActiveDuty UNIQUEIDENTIFIER
           ,@Deceased UNIQUEIDENTIFIER
           ,@TemporarilyDisabled UNIQUEIDENTIFIER
           ,@PermanentlyDisabled UNIQUEIDENTIFIER
           ,@TransferredToEquivalent UNIQUEIDENTIFIER;

    BEGIN

        SET @MilitaryTransfer = (
                                SELECT TOP 1 NACCASDropReasonId
                                FROM   dbo.syNACCASDropReasons
                                WHERE  Name = ''Military Transfer''
                                );
        SET @CallToActiveDuty = (
                                SELECT TOP 1 NACCASDropReasonId
                                FROM   dbo.syNACCASDropReasons
                                WHERE  Name = ''Call to Active Duty''
                                );
        SET @Deceased = (
                        SELECT TOP 1 NACCASDropReasonId
                        FROM   dbo.syNACCASDropReasons
                        WHERE  Name = ''Deceased''
                        );
        SET @TemporarilyDisabled = (
                                   SELECT TOP 1 NACCASDropReasonId
                                   FROM   dbo.syNACCASDropReasons
                                   WHERE  Name = ''Temporarily disabled''
                                   );
        SET @PermanentlyDisabled = (
                                   SELECT TOP 1 NACCASDropReasonId
                                   FROM   dbo.syNACCASDropReasons
                                   WHERE  Name = ''Permanently disabled''
                                   );
        SET @TransferredToEquivalent = (
                                       SELECT TOP 1 NACCASDropReasonId
                                       FROM   dbo.syNACCASDropReasons
                                       WHERE  Name = ''Transferred to an equivalent program at another school with same accreditation''
                                       );

        DECLARE @Enrollments TABLE
            (
                PrgVerID UNIQUEIDENTIFIER
               ,PrgVerDescrip VARCHAR(50)
               ,ProgramHours DECIMAL(9, 2)
               ,ProgramCredits DECIMAL(18, 2)
               ,TotalTransferHours DECIMAL(18, 2)
               ,TransferredProgram UNIQUEIDENTIFIER
               ,TransferHoursFromProgram DECIMAL(18, 2)
               ,SSN VARCHAR(50)
               ,PhoneNumber VARCHAR(50)
               ,Email VARCHAR(100)
               ,Student VARCHAR(50)
               ,LeadId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,EnrollmentID NVARCHAR(50)
               ,Status VARCHAR(80)
               ,SysStatusId INT
               ,DropReasonId UNIQUEIDENTIFIER
               ,StartDate DATETIME
               ,ContractedGradDate DATETIME
               ,ExpectedGradDate DATETIME
               ,LDA DATETIME
               ,LicensureWrittenAllParts BIT
               ,LicensureLastPartWrittenOn DATETIME
               ,LicensurePassedAllParts BIT
               ,AllowsMoneyOwed BIT
               ,AllowsIncompleteReq BIT
            );

        DECLARE @EnrollmentsWithLastStatus TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,SysStatusId INT
               ,DropReasonId UNIQUEIDENTIFIER
               ,StatusCodeDescription VARCHAR(80)
            );
        INSERT INTO @EnrollmentsWithLastStatus
                    SELECT lastStatusOfYear.StuEnrollId
                          ,lastStatusOfYear.SysStatusId
                          ,lastStatusOfYear.DropReasonId
                          ,lastStatusOfYear.StatusCodeDescrip
                    FROM   (
                           SELECT     StuEnrollId
                                     ,syStatusCodes.SysStatusId
                                     ,StatusCodeDescrip
                                     ,DateOfChange
                                     ,DropReasonId
                                     ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId
                                                          ORDER BY DateOfChange DESC
                                                        ) rn
                           FROM       dbo.syStudentStatusChanges
                           INNER JOIN dbo.syStatusCodes ON syStatusCodes.StatusCodeId = syStudentStatusChanges.NewStatusId
                           WHERE      DATEPART(YEAR, DateOfChange) <= ( @ReportYear - 1 )
                           ) lastStatusOfYear
                    WHERE  rn = 1;





        INSERT INTO @Enrollments
                    SELECT     Prog.ProgId AS PrgVerID
                              ,Prog.ProgDescrip AS PrgVerDescrip
                              ,PV.Hours AS ProgramHours
                              ,PV.Credits AS ProgramCredits
                              ,SE.TransferHours
                              ,SE.TransferHoursFromThisSchoolEnrollmentId
                              ,SE.TotalTransferHoursFromThisSchool
                              ,LD.SSN
                              ,ISNULL((
                                      SELECT   TOP 1 Phone
                                      FROM     dbo.adLeadPhone
                                      WHERE    LeadId = LD.LeadId
                                               AND (
                                                   IsBest = 1
                                                   OR IsShowOnLeadPage = 1
                                                   )
                                      ORDER BY Position
                                      )
                                     ,''''
                                     ) AS PhoneNumber
                              ,ISNULL((
                                      SELECT   TOP 1 EMail
                                      FROM     dbo.AdLeadEmail
                                      WHERE    LeadId = LD.LeadId
                                               AND (
                                                   IsPreferred = 1
                                                   OR IsShowOnLeadPage = 1
                                                   )
                                      ORDER BY IsPreferred DESC
                                              ,IsShowOnLeadPage DESC
                                      )
                                     ,''''
                                     ) AS Email
                              ,LD.LastName + '', '' + LD.FirstName + '' '' + ISNULL(LD.MiddleName, '''') AS Student
                              ,LD.LeadId
                              ,SE.StuEnrollId
                              ,LD.StudentNumber AS EnrollmentID
                              ,[@EnrollmentsWithLastStatus].StatusCodeDescription AS Status
                              ,[@EnrollmentsWithLastStatus].SysStatusId AS SysStatusId
                              ,[@EnrollmentsWithLastStatus].DropReasonId AS DropReasonId
                              ,SE.StartDate AS StartDate
                              ,SE.ContractedGradDate AS ContractedGradDate
                              ,dbo.GetGraduatedDate(SE.StuEnrollId) AS ExpectedGradDate
                              ,dbo.GetLDA(SE.StuEnrollId) AS LDA
                              ,SE.LicensureWrittenAllParts AS LicensureWrittenAllParts
                              ,SE.LicensureLastPartWrittenOn AS LicensureLastPartWrittenOn
                              ,SE.LicensurePassedAllParts AS LicensurePassedAllParts
                              ,camp.AllowGraduateAndStillOweMoney AS AllowsMoneyOwed
                              ,camp.AllowGraduateWithoutFullCompletion AS AllowsIncompleteReq
                    FROM       dbo.arStuEnrollments AS SE
                    INNER JOIN dbo.adLeads AS LD ON LD.LeadId = SE.LeadId
                    INNER JOIN dbo.arPrgVersions AS PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arPrograms AS Prog ON Prog.ProgId = PV.ProgId
                    INNER JOIN dbo.syApprovedNACCASProgramVersion NaccasApproved ON NaccasApproved.ProgramVersionId = SE.PrgVerId
                    INNER JOIN dbo.syNaccasSettings NaccasSettings ON NaccasSettings.NaccasSettingId = NaccasApproved.NaccasSettingId
                    INNER JOIN dbo.syCampuses AS camp ON camp.CampusId = SE.CampusId
                    INNER JOIN @EnrollmentsWithLastStatus ON [@EnrollmentsWithLastStatus].StuEnrollId = SE.StuEnrollId
                    WHERE      SE.CampusId = @CampusId
                               AND NaccasSettings.CampusId = @CampusId
                               --that have a contracted graduation date between January 1 of reporting year minus 1 year and December 31st of reporting year minus 1 year
                               -- Not a third party contract
                               AND NOT SE.ThirdPartyContract = 1
                               AND NaccasApproved.IsApproved = 1
                               AND SE.ContractedGradDate
                               BETWEEN CONVERT(DATETIME, CONVERT(VARCHAR(50), (( @ReportYear - 1 ) * 10000 + @StartMonth * 100 + @StartDay )), 112) AND CONVERT(
                                                                                                                                                                   DATETIME
                                                                                                                                                                  ,CONVERT(
                                                                                                                                                                              VARCHAR(50)
                                                                                                                                                                             ,(( @ReportYear
                                                                                                                                                                                 - 1
                                                                                                                                                                               )
                                                                                                                                                                               * 10000
                                                                                                                                                                               + @EndMonth
                                                                                                                                                                               * 100
                                                                                                                                                                               + @EndDay
                                                                                                                                                                              )
                                                                                                                                                                          )
                                                                                                                                                                  ,112
                                                                                                                                                               );
        --Base Case 
        WITH A
        AS ( SELECT se.PrgVerID
                   ,se.PrgVerDescrip
                   ,se.ProgramHours
                   ,se.ProgramCredits
                   ,se.SSN
                   ,se.PhoneNumber
                   ,se.Email
                   ,se.Student
                   ,se.LeadId
                   ,se.StuEnrollId
                   ,se.EnrollmentID
                   ,se.Status
                   ,se.SysStatusId
                   ,se.DropReasonId
                   ,se.StartDate
                   ,se.ContractedGradDate
                   ,se.ExpectedGradDate
                   ,se.LDA
                   ,se.LicensureWrittenAllParts
                   ,se.LicensureLastPartWrittenOn
                   ,se.LicensurePassedAllParts
                   ,se.AllowsMoneyOwed
                   ,se.AllowsIncompleteReq
             FROM   (
                    SELECT *
                          ,ROW_NUMBER() OVER ( PARTITION BY LeadId
                                                           ,PrgVerID
                                                           ,SysStatusId
                                               ORDER BY StartDate DESC
                                             ) RN
                    FROM   @Enrollments
                    ) se
             --Is currently attending the school or on LOA or Suspended or is enrolled but have not yet started
             WHERE  se.SysStatusId IN ( @FutureStart, @CurrentlyAttending, @Suspension, @LOA )
                    AND se.RN = 1
                    AND se.StuEnrollId NOT IN ((
                                               SELECT     se.TransferredProgram
                                               FROM       @Enrollments se
                                               INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
                                               WHERE      (
                                                          (
                                                          se.TotalTransferHours IS NOT NULL
                                                          AND se.TotalTransferHours <> 0
                                                          )
                                                          AND (( se.TransferHoursFromProgram / pe.ProgramHours ) = 1 )
                                                          )
                                               UNION
                                               SELECT     pe.TransferredProgram
                                               FROM       @Enrollments se
                                               INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
                                               WHERE      (
                                                          (
                                                          se.TotalTransferHours IS NOT NULL
                                                          AND se.TotalTransferHours <> 0
                                                          )
                                                          AND (( se.TransferHoursFromProgram / pe.ProgramHours ) <> 1 )
                                                          )
                                               )
                                              ))

            --Was enrolled in two different NACCAS approved programs in the same school and graduated/licensed/placed from both the programs in the same report period
            ,B
        AS ( SELECT se.PrgVerID
                   ,se.PrgVerDescrip
                   ,se.ProgramHours
                   ,se.ProgramCredits
                   ,se.SSN
                   ,se.PhoneNumber
                   ,se.Email
                   ,se.Student
                   ,se.LeadId
                   ,se.StuEnrollId
                   ,se.EnrollmentID
                   ,se.Status
                   ,se.SysStatusId
                   ,se.DropReasonId
                   ,se.StartDate
                   ,se.ContractedGradDate
                   ,se.ExpectedGradDate
                   ,se.LDA
                   ,se.LicensureWrittenAllParts
                   ,se.LicensureLastPartWrittenOn
                   ,se.LicensurePassedAllParts
                   ,se.AllowsMoneyOwed
                   ,se.AllowsIncompleteReq
             FROM   (
                    SELECT *
                          ,COUNT(1) OVER ( PARTITION BY LeadId
                                                       ,SysStatusId
                                         ) AS cnt
                    FROM   @Enrollments
                    ) se
             WHERE  se.SysStatusId IN ( @Graduated ))
            ,C
        AS (
           --Has dropped from a program whose length is less than one academic year of 900 hours after being enrolled for more than 15 calendar days
           --Has dropped from a program whose length is equal to or more than one academic year of 900 hours after being enrolled for more than 30 calendar DAYS
           SELECT se.PrgVerID
                 ,se.PrgVerDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND (
                      (
                      se.ProgramHours < 900
                      AND DATEDIFF(DAY, se.StartDate, se.LDA) > 15
                      AND NOT EXISTS (
                                     SELECT     *
                                     FROM       dbo.syNACCASDropReasonsMapping nm
                                     INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                                     WHERE      ns.CampusId = @CampusId
                                                AND se.DropReasonId = nm.ADVDropReasonId
                                                AND nm.NACCASDropReasonId IN ( @CallToActiveDuty, @MilitaryTransfer, @Deceased, @TemporarilyDisabled
                                                                              ,@PermanentlyDisabled, @TransferredToEquivalent
                                                                             )
                                     )
                      )
                      OR (
                         se.ProgramHours >= 900
                         AND DATEDIFF(DAY, se.StartDate, se.LDA) > 30
                         AND NOT EXISTS (
                                        SELECT     *
                                        FROM       dbo.syNACCASDropReasonsMapping nm
                                        INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                                        WHERE      ns.CampusId = @CampusId
                                                   AND se.DropReasonId = nm.ADVDropReasonId
                                                   AND nm.NACCASDropReasonId IN ( @CallToActiveDuty, @MilitaryTransfer, @Deceased, @TemporarilyDisabled
                                                                                 ,@PermanentlyDisabled, @TransferredToEquivalent
                                                                                )
                                        )
                         )
                      )
                  AND se.StuEnrollId NOT IN ((
                                             SELECT     se.TransferredProgram
                                             FROM       @Enrollments se
                                             INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
                                             WHERE      (
                                                        (
                                                        se.TotalTransferHours IS NOT NULL
                                                        AND se.TotalTransferHours <> 0
                                                        )
                                                        AND (( se.TransferHoursFromProgram / pe.ProgramHours ) = 1 )
                                                        )
                                             )
                                            ))
            --Was transferred from the school for which the report is generated to another school and both the schools are a branch of the same parent.
            -- Such a student would be included in the report generated for the original school as did not complete
            ,D
        AS ( SELECT     se.PrgVerID
                       ,se.PrgVerDescrip
                       ,se.ProgramHours
                       ,se.ProgramCredits
                       ,se.SSN
                       ,se.PhoneNumber
                       ,se.Email
                       ,se.Student
                       ,se.LeadId
                       ,se.StuEnrollId
                       ,se.EnrollmentID
                       ,se.Status
                       ,se.SysStatusId
                       ,se.DropReasonId
                       ,se.StartDate
                       ,se.ContractedGradDate
                       ,se.ExpectedGradDate
                       ,se.LDA
                       ,se.LicensureWrittenAllParts
                       ,se.LicensureLastPartWrittenOn
                       ,se.LicensurePassedAllParts
                       ,se.AllowsMoneyOwed
                       ,se.AllowsIncompleteReq
             FROM       @Enrollments se
             INNER JOIN dbo.arTrackTransfer tt ON se.StuEnrollId = tt.StuEnrollId
             INNER JOIN dbo.syCampuses srcCmp ON srcCmp.CampusId = tt.SourceCampusId
             INNER JOIN dbo.syCampuses targetCmp ON targetCmp.CampusId = tt.TargetCampusId
             INNER JOIN dbo.arPrgVersions targetPV ON targetPV.PrgVerId = tt.TargetPrgVerId
             WHERE      se.SysStatusId IN ( @Transfer )
                        AND ((
                             srcCmp.IsBranch = 1
                             AND targetCmp.IsBranch = 1
                             AND srcCmp.ParentCampusId <> targetCmp.ParentCampusId
                             )
                            ))
            ,E
        AS (
           -- select from PE id not all hours are transffered else select from SE
           SELECT     se.PrgVerID
                     ,se.PrgVerDescrip
                     ,se.ProgramHours
                     ,se.ProgramCredits
                     ,se.SSN
                     ,se.PhoneNumber
                     ,se.Email
                     ,se.Student
                     ,se.LeadId
                     ,se.StuEnrollId
                     ,se.EnrollmentID
                     ,se.Status
                     ,se.SysStatusId
                     ,se.DropReasonId
                     ,se.StartDate
                     ,se.ContractedGradDate
                     ,se.ExpectedGradDate
                     ,se.LDA
                     ,se.LicensureWrittenAllParts
                     ,se.LicensureLastPartWrittenOn
                     ,se.LicensurePassedAllParts
                     ,se.AllowsMoneyOwed
                     ,se.AllowsIncompleteReq
           FROM       @Enrollments se
           INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
           WHERE      (
                      (
                      se.TotalTransferHours IS NOT NULL
                      AND se.TotalTransferHours <> 0
                      )
                      AND (( se.TransferHoursFromProgram / pe.ProgramHours ) = 1 )
                      )
           UNION
           SELECT     pe.PrgVerID
                     ,pe.PrgVerDescrip
                     ,pe.ProgramHours
                     ,pe.ProgramCredits
                     ,pe.SSN
                     ,pe.PhoneNumber
                     ,pe.Email
                     ,pe.Student
                     ,pe.LeadId
                     ,pe.StuEnrollId
                     ,pe.EnrollmentID
                     ,pe.Status
                     ,pe.SysStatusId
                     ,pe.DropReasonId
                     ,pe.StartDate
                     ,pe.ContractedGradDate
                     ,pe.ExpectedGradDate
                     ,pe.LDA
                     ,pe.LicensureWrittenAllParts
                     ,pe.LicensureLastPartWrittenOn
                     ,pe.LicensurePassedAllParts
                     ,pe.AllowsMoneyOwed
                     ,pe.AllowsIncompleteReq
           FROM       @Enrollments se
           INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
           WHERE      (
                      (
                      se.TotalTransferHours IS NOT NULL
                      AND se.TotalTransferHours <> 0
                      )
                      AND (( se.TransferHoursFromProgram / pe.ProgramHours ) <> 1 )
                      ))
        SELECT dataWithNumOfJobs.PrgVerID,
              dataWithNumOfJobs.PrgVerDescrip,
              dataWithNumOfJobs.ProgramHours,
              dataWithNumOfJobs.ProgramCredits,
              dataWithNumOfJobs.SSN,
              dataWithNumOfJobs.PhoneNumber,
              dataWithNumOfJobs.Email,
              dataWithNumOfJobs.Student,
              dataWithNumOfJobs.LeadId,
              dataWithNumOfJobs.StuEnrollId,
              dataWithNumOfJobs.EnrollmentID,
              dataWithNumOfJobs.Status,
              dataWithNumOfJobs.SysStatusId,
              dataWithNumOfJobs.DropReasonId,
              dataWithNumOfJobs.StartDate,
              dataWithNumOfJobs.ContractedGradDate,
              dataWithNumOfJobs.ExpectedGradDate,
              dataWithNumOfJobs.LDA,
              dataWithNumOfJobs.LicensureWrittenAllParts,
              dataWithNumOfJobs.LicensureLastPartWrittenOn,
              dataWithNumOfJobs.LicensurePassedAllParts,
              dataWithNumOfJobs.AllowsMoneyOwed,
              dataWithNumOfJobs.AllowsIncompleteReq,
              dataWithNumOfJobs.Graduated,
              dataWithNumOfJobs.OwesMoney,
              dataWithNumOfJobs.MissingRequired,
              dataWithNumOfJobs.GraduatedProgram,
              dataWithNumOfJobs.DateGraduated,
              dataWithNumOfJobs.PlacementStatus,
              dataWithNumOfJobs.IneligibilityReason,
              dataWithNumOfJobs.Placed,
              dataWithNumOfJobs.SatForAllExamParts,
              dataWithNumOfJobs.PassedAllParts,
              dataWithNumOfJobs.EmployerName,
              dataWithNumOfJobs.EmployerAddress,
              dataWithNumOfJobs.EmployerCity,
              dataWithNumOfJobs.EmployerState,
              dataWithNumOfJobs.EmployerZip,
              dataWithNumOfJobs.EmployerPhone
              
        FROM     (
                 SELECT    allData.PrgVerID
                          ,allData.PrgVerDescrip
                          ,allData.ProgramHours
                          ,allData.ProgramCredits
                          ,allData.SSN
                          ,(
                           SELECT STUFF(STUFF(STUFF(allData.PhoneNumber, 1, 0, ''(''), 5, 0, '')''), 9, 0, ''-'')
                           ) AS PhoneNumber
                          ,allData.Email
                          ,allData.Student
                          ,allData.LeadId
                          ,allData.StuEnrollId
                          ,allData.EnrollmentID
                          ,allData.Status
                          ,allData.SysStatusId
                          ,allData.DropReasonId
                          ,allData.StartDate
                          ,allData.ContractedGradDate
                          ,allData.ExpectedGradDate
                          ,allData.LDA
                          ,allData.LicensureWrittenAllParts
                          ,allData.LicensureLastPartWrittenOn
                          ,allData.LicensurePassedAllParts
                          ,allData.AllowsMoneyOwed
                          ,allData.AllowsIncompleteReq
                          ,allData.Graduated
                          ,allData.OwesMoney
                          ,allData.MissingRequired
                          ,allData.GraduatedProgram
                          ,allData.DateGraduated
                          ,allData.PlacementStatus
                          ,allData.IneligibilityReason
                          ,allData.Placed
                          ,allData.SatForAllExamParts
                          ,allData.PassedAllParts
                          ,ISNULL(pemp.EmployerDescrip, '''') AS EmployerName
                          ,ISNULL(pemp.Address1 + '' '' + pemp.Address2, '''') AS EmployerAddress
                          ,ISNULL(pemp.City, '''') AS EmployerCity
                          ,ISNULL(states.StateDescrip, '''') AS EmployerState
                          ,ISNULL(pemp.Zip, '''') AS EmployerZip
                          ,ISNULL(pemp.Phone, '''') AS EmployerPhone
                          ,ROW_NUMBER() OVER ( PARTITION BY allData.StuEnrollId
                                               ORDER BY psp.PlacedDate DESC
                                             ) AS numOfJobs
                 FROM      (
                           SELECT *
                                 ,(
                                  SELECT CASE WHEN satForExam.SatForAllExamParts = ''Y'' THEN CASE WHEN satForExam.LicensurePassedAllParts = 1 THEN ''Y''
                                                                                                 ELSE ''N''
                                                                                            END
                                              ELSE ''N/A''
                                         END
                                  ) PassedAllParts
                           FROM   (
                                  SELECT *
                                        ,(
                                         SELECT CASE WHEN placed.GraduatedProgram = ''Y'' THEN
                                                         CASE WHEN placed.LicensureWrittenAllParts = 0 THEN ''N''
                                                              ELSE
                                                                  CASE WHEN @ReportType = @Preliminary THEN
                                                                           CASE WHEN placed.LicensureLastPartWrittenOn < CONVERT(
                                                                                                                                    DATETIME
                                                                                                                                   ,CONVERT(
                                                                                                                                               VARCHAR(50)
                                                                                                                                              ,(( @ReportYear ) * 10000
                                                                                                                                                + 3 * 100 + 30
                                                                                                                                               )
                                                                                                                                           )
                                                                                                                                   ,112
                                                                                                                                ) THEN ''Y''
                                                                                ELSE ''N''
                                                                           END
                                                                       ELSE
                                                                           CASE WHEN placed.LicensureLastPartWrittenOn < CONVERT(
                                                                                                                                    DATETIME
                                                                                                                                   ,CONVERT(
                                                                                                                                               VARCHAR(50)
                                                                                                                                              ,(( @ReportYear ) * 10000
                                                                                                                                                + 11 * 100 + 30
                                                                                                                                               )
                                                                                                                                           )
                                                                                                                                   ,112
                                                                                                                                ) THEN ''Y''
                                                                                ELSE ''N''
                                                                           END
                                                                  END
                                                         END
                                                     ELSE ''N/A''
                                                END
                                         ) AS SatForAllExamParts
                                  FROM   (
                                         SELECT *
                                               ,(
                                                SELECT CASE WHEN placementStat.PlacementStatus = ''E'' THEN
                                                                CASE WHEN EXISTS (
                                                                                 SELECT     1
                                                                                 FROM       dbo.PlStudentsPlaced
                                                                                 INNER JOIN dbo.plFldStudy ON plFldStudy.FldStudyId = PlStudentsPlaced.FldStudyId
                                                                                 WHERE      dbo.PlStudentsPlaced.StuEnrollId = placementStat.StuEnrollId
                                                                                            AND (
                                                                                                dbo.plFldStudy.FldStudyCode = ''Yes''
                                                                                                OR dbo.plFldStudy.FldStudyCode = ''Related''
                                                                                                )
                                                                                 ) THEN ''Y''
                                                                     ELSE ''N''
                                                                END
                                                            ELSE ''N/A''
                                                       END
                                                ) AS Placed
                                         FROM   (
                                                SELECT *
                                                      ,(
                                                       SELECT CASE WHEN graduated.GraduatedProgram = ''Y'' THEN CONVERT(VARCHAR, graduated.ExpectedGradDate, 101)
                                                                   ELSE ''N/A''
                                                              END
                                                       ) AS DateGraduated
                                                      ,(
                                                       SELECT CASE WHEN graduated.GraduatedProgram = ''Y'' THEN
                                                                       CASE WHEN (
                                                                                 SELECT   TOP 1 Eligible
                                                                                 FROM     dbo.plExitInterview
                                                                                 WHERE    EnrollmentId = graduated.StuEnrollId
                                                                                 ORDER BY COALESCE(ExitInterviewDate, graduated.ExpectedGradDate) DESC
                                                                                 ) = ''Yes'' THEN ''E''
                                                                            ELSE ''I''
                                                                       END
                                                                   ELSE ''N/A''
                                                              END
                                                       ) AS PlacementStatus
                                                      ,(
                                                       SELECT   TOP 1 COALESCE(Reason, '''')
                                                       FROM     dbo.plExitInterview
                                                       WHERE    EnrollmentId = graduated.StuEnrollId
                                                       ORDER BY COALESCE(ExitInterviewDate, graduated.ExpectedGradDate) DESC
                                                       ) AS IneligibilityReason
                                                FROM   (
                                                       SELECT *
                                                             ,(
                                                              SELECT CASE WHEN didGraduate.Graduated = 0 THEN ''N''
                                                                          ELSE CASE WHEN didGraduate.OwesMoney = 1
                                                                                         AND didGraduate.AllowsMoneyOwed = 0 THEN ''N''
                                                                                    WHEN didGraduate.MissingRequired = 1
                                                                                         AND didGraduate.AllowsIncompleteReq = 0 THEN ''N''
                                                                                    ELSE ''Y''
                                                                               END
                                                                     END
                                                              ) AS GraduatedProgram
                                                       FROM   (
                                                              SELECT *
                                                                    ,(
                                                                     SELECT CASE WHEN @ReportType = @Preliminary THEN
                                                                                     CASE WHEN result.ExpectedGradDate < CONVERT(
                                                                                                                                    DATETIME
                                                                                                                                   ,CONVERT(
                                                                                                                                               VARCHAR(50)
                                                                                                                                              ,(( @ReportYear ) * 10000 + 3 * 100
                                                                                                                                                + 30
                                                                                                                                               )
                                                                                                                                           )
                                                                                                                                   ,112
                                                                                                                                ) THEN 1
                                                                                          ELSE 0
                                                                                     END
                                                                                 ELSE
                                                                                     CASE WHEN result.ExpectedGradDate < CONVERT(
                                                                                                                                    DATETIME
                                                                                                                                   ,CONVERT(
                                                                                                                                               VARCHAR(50)
                                                                                                                                              ,(( @ReportYear ) * 10000 + 11 * 100
                                                                                                                                                + 30
                                                                                                                                               )
                                                                                                                                           )
                                                                                                                                   ,112
                                                                                                                                ) THEN 1
                                                                                          ELSE 0
                                                                                     END
                                                                            END
                                                                     ) AS Graduated
                                                                    ,dbo.DoesEnrollmentHavePendingBalance(result.StuEnrollId) AS OwesMoney
                                                                    ,~ ( dbo.CompletedRequiredCourses(result.StuEnrollId)) AS MissingRequired
                                                              FROM   (
                                                                     SELECT *
                                                                     FROM   A
                                                                     UNION
                                                                     SELECT *
                                                                     FROM   B
                                                                     UNION
                                                                     SELECT *
                                                                     FROM   C
                                                                     UNION
                                                                     SELECT *
                                                                     FROM   D
                                                                     UNION
                                                                     SELECT *
                                                                     FROM   E
                                                                     ) AS result
                                                              WHERE  (
                                                                     @PrgIdList IS NULL
                                                                     OR result.PrgVerID IN (
                                                                                           SELECT Val
                                                                                           FROM   MultipleValuesForReportParameters(@PrgIdList, '','', 1)
                                                                                           )
                                                                     )
                                                              ) didGraduate
                                                       ) graduated
                                                ) placementStat
                                         ) placed
                                  ) satForExam
                           ) allData
                 LEFT JOIN dbo.PlStudentsPlaced psp ON psp.StuEnrollId = allData.StuEnrollId
                 LEFT JOIN dbo.plEmployerJobs pej ON pej.EmployerJobId = psp.EmployerJobId
                 LEFT JOIN dbo.plEmployers pemp ON pemp.EmployerId = pej.EmployerId
                 LEFT JOIN dbo.syStates states ON states.StateId = pemp.StateId
                 ) dataWithNumOfJobs
        WHERE    dataWithNumOfJobs.numOfJobs = 1
        ORDER BY dataWithNumOfJobs.PrgVerDescrip
                ,dataWithNumOfJobs.Student;



    END;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]', 'P') IS NULL
EXEC sp_executesql N'
-- ========================================================================================================= 
-- Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents 
-- ========================================================================================================= 
CREATE PROCEDURE [dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]
    @StuEnrollIdList VARCHAR(MAX)
   ,@TermId VARCHAR(50) = NULL
   ,@SysComponentTypeId VARCHAR(50) = NULL
AS -- source usp_pr_main_ssrs_subreport3_byterm  
    BEGIN
        DECLARE @GradesFormat AS VARCHAR(50);
        DECLARE @GPAMethod AS VARCHAR(50);
        DECLARE @GradeBookAt AS VARCHAR(50);
        DECLARE @StuEnrollCampusId AS UNIQUEIDENTIFIER;
        DECLARE @Counter AS INT;
        DECLARE @times AS INT;
        DECLARE @TermStartDate1 AS DATETIME;
        DECLARE @Score AS DECIMAL(18, 2);
        DECLARE @GrdCompDescrip AS VARCHAR(50);

        DECLARE @curId AS UNIQUEIDENTIFIER;
        DECLARE @curReqId AS UNIQUEIDENTIFIER;
        DECLARE @curStuEnrollId AS UNIQUEIDENTIFIER;
        DECLARE @curDescrip AS VARCHAR(50);
        DECLARE @curNumber AS INT;
        DECLARE @curGrdComponentTypeId AS INT;
        DECLARE @curMinResult AS DECIMAL(18, 2);
        DECLARE @curGrdComponentDescription AS VARCHAR(50);
        DECLARE @curClsSectionId AS UNIQUEIDENTIFIER;
        DECLARE @curRownumber AS INT;


        CREATE TABLE #tempTermWorkUnitCount
            (
                TermId UNIQUEIDENTIFIER
               ,ReqId UNIQUEIDENTIFIER
               ,sysComponentTypeId INT
               ,WorkUnitCount INT
            );
        CREATE TABLE #Temp21
            (
                Id UNIQUEIDENTIFIER
               ,TermId UNIQUEIDENTIFIER
               ,ReqId UNIQUEIDENTIFIER
               ,GradeBookDescription VARCHAR(50)
               ,Number INT
               ,GradeBookSysComponentTypeId INT
               ,GradeBookScore DECIMAL(18, 2)
               ,MinResult DECIMAL(18, 2)
               ,GradeComponentDescription VARCHAR(50)
               ,ClsSectionId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,RowNumber INT
            );
        CREATE TABLE #Temp22
            (
                ReqId UNIQUEIDENTIFIER
               ,EffectiveDate DATETIME
            );
        SET @StuEnrollCampusId = COALESCE((
                                          SELECT TOP 1 ASE.CampusId
                                          FROM   arStuEnrollments AS ASE
                                          WHERE  ASE.StuEnrollId IN (
                                                                    SELECT Val
                                                                    FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                    )
                                          )
                                         ,NULL
                                         );
        SET @GradesFormat = dbo.GetAppSettingValueByKeyName(''GradesFormat'', @StuEnrollCampusId);
        IF ( @GradesFormat IS NOT NULL )
            BEGIN
                SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
            END;
        SET @GPAMethod = dbo.GetAppSettingValueByKeyName(''GPAMethod'', @StuEnrollCampusId);
        IF ( @GPAMethod IS NOT NULL )
            BEGIN
                SET @GPAMethod = LOWER(LTRIM(RTRIM(@GPAMethod)));
            END;
        SET @GradeBookAt = dbo.GetAppSettingValueByKeyName(''GradeBookWeightingLevel'', @StuEnrollCampusId);
        IF ( @GradeBookAt IS NOT NULL )
            BEGIN
                SET @GradeBookAt = LOWER(LTRIM(RTRIM(@GradeBookAt)));
            END;

        IF LOWER(@GradeBookAt) = ''instructorlevel''
            BEGIN
                INSERT INTO #tempTermWorkUnitCount
                            SELECT   dt.TermId
                                    ,dt.ReqId
                                    ,dt.GradeBookSysComponentTypeId
                                    ,COUNT(dt.GradeBookDescription)
                            FROM     (
                                     SELECT     d.ReqId
                                               ,d.TermId
                                               ,CASE WHEN a.Descrip IS NULL THEN e.Descrip
                                                     --THEN (  
                                                     --      SELECT   Resource  
                                                     --      FROM     syResources  
                                                     --      WHERE    ResourceID = e.SysComponentTypeId  
                                                     --     )  
                                                     ELSE a.Descrip
                                                END AS GradeBookDescription
                                               ,( CASE e.SysComponentTypeId
                                                       WHEN 500 THEN a.Number
                                                       WHEN 503 THEN a.Number
                                                       WHEN 544 THEN a.Number
                                                       ELSE (
                                                            SELECT MIN(MinVal)
                                                            FROM   arGradeScaleDetails GSD
                                                                  ,arGradeSystemDetails GSS
                                                            WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                   AND GSS.IsPass = 1
                                                                   AND GSD.GrdScaleId = d.GrdScaleId
                                                            )
                                                  END
                                                ) AS MinResult
                                               ,a.Required
                                               ,a.MustPass
                                               ,ISNULL(e.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId
                                               ,a.Number
                                               ,(
                                                SELECT Resource
                                                FROM   syResources
                                                WHERE  ResourceID = e.SysComponentTypeId
                                                ) AS GradeComponentDescription
                                               ,a.InstrGrdBkWgtDetailId
                                               ,c.StuEnrollId
                                               ,0 AS IsExternShip
                                               ,CASE e.SysComponentTypeId
                                                     WHEN 544 THEN (
                                                                   SELECT ISNULL(SUM(HoursAttended), ''0.00'')
                                                                   FROM   arExternshipAttendance
                                                                   WHERE  StuEnrollId = c.StuEnrollId
                                                                   )
                                                     ELSE (
                                                          SELECT SUM(AGBR.Score)
                                                          FROM   arGrdBkResults AS AGBR
                                                          WHERE  AGBR.StuEnrollId = c.StuEnrollId
                                                                 AND AGBR.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                                                 AND AGBR.ClsSectionId = d.ClsSectionId
                                                          )
                                                END AS GradeBookScore
                                               ,ROW_NUMBER() OVER ( PARTITION BY ST.StuEnrollId
                                                                                --T.TermId, R.ReqId ORDER BY ST.CampusId, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, RES.Resource, e.Descrip) AS rownumber  
                                                                                ,T.TermId
                                                                                ,R.ReqId
                                                                    ORDER BY e.SysComponentTypeId
                                                                            ,a.Descrip
                                                                  ) AS rownumber
                                     FROM       arGrdBkWgtDetails AS a
                                     INNER JOIN arGrdBkWeights AS b ON b.InstrGrdBkWgtId = a.InstrGrdBkWgtId
                                     INNER JOIN arClassSections AS d ON d.InstrGrdBkWgtId = a.InstrGrdBkWgtId
                                     INNER JOIN arResults AS c ON c.TestId = d.ClsSectionId
                                     INNER JOIN arGrdComponentTypes AS e ON e.GrdComponentTypeId = a.GrdComponentTypeId
                                     INNER JOIN arStuEnrollments AS ST ON ST.StuEnrollId = c.StuEnrollId
                                     INNER JOIN arTerm AS T ON T.TermId = d.TermId
                                     INNER JOIN arReqs AS R ON R.ReqId = d.ReqId
                                     INNER JOIN syResources AS RES ON RES.ResourceID = e.SysComponentTypeId
                                     WHERE      c.StuEnrollId IN (
                                                                 SELECT Val
                                                                 FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                 )
                                                AND (
                                                    @SysComponentTypeId IS NULL
                                                    OR e.SysComponentTypeId IN (
                                                                               SELECT Val
                                                                               FROM   MultipleValuesForReportParameters(@SysComponentTypeId, '','', 1)
                                                                               )
                                                    )
                                     UNION
                                     SELECT     R.ReqId
                                               ,T.TermId
                                               ,CASE WHEN GBW.Descrip IS NULL THEN (
                                                                                   SELECT Resource
                                                                                   FROM   syResources
                                                                                   WHERE  ResourceID = GCT.SysComponentTypeId
                                                                                   )
                                                     ELSE GBW.Descrip
                                                END AS GradeBookDescription
                                               ,( CASE GCT.SysComponentTypeId
                                                       WHEN 500 THEN GBWD.Number
                                                       WHEN 503 THEN GBWD.Number
                                                       WHEN 544 THEN GBWD.Number
                                                       ELSE (
                                                            SELECT MIN(MinVal)
                                                            FROM   arGradeScaleDetails GSD
                                                                  ,arGradeSystemDetails GSS
                                                            WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                   AND GSS.IsPass = 1
                                                                   AND GSD.GrdScaleId = CSC.GrdScaleId
                                                            )
                                                  END
                                                ) AS MinResult
                                               ,GBWD.Required
                                               ,GBWD.MustPass
                                               ,ISNULL(GCT.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId
                                               ,GBWD.Number
                                               ,(
                                                SELECT Resource
                                                FROM   syResources
                                                WHERE  ResourceID = GCT.SysComponentTypeId
                                                ) AS GradeComponentDescription
                                               ,GBWD.InstrGrdBkWgtDetailId
                                               ,GBCR.StuEnrollId
                                               ,0 AS IsExternShip
                                               ,CASE GCT.SysComponentTypeId
                                                     WHEN 544 THEN (
                                                                   SELECT ISNULL(SUM(HoursAttended), ''0.00'')
                                                                   FROM   arExternshipAttendance
                                                                   WHERE  StuEnrollId = SE.StuEnrollId
                                                                   )
                                                     ELSE (
                                                          SELECT ISNULL(SUM(AGBR.Score), 0.00)
                                                          FROM   arGrdBkResults AS AGBR
                                                          WHERE  AGBR.StuEnrollId = SE.StuEnrollId
                                                                 AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                 AND ClsSectionId = CSC.ClsSectionId
                                                          )
                                                END AS GradeBookScore
                                               ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId
                                                                                ,T.TermId
                                                                                ,R.ReqId
                                                                    ORDER BY SE.CampusId
                                                                            ,T.StartDate
                                                                            ,T.EndDate
                                                                            ,T.TermId
                                                                            ,T.TermDescrip
                                                                            ,R.ReqId
                                                                            ,R.Descrip
                                                                            ,SYRES.Resource
                                                                            ,GCT.Descrip
                                                                  ) AS rownumber
                                     FROM       arGrdBkConversionResults GBCR
                                     INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                     INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                     INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                     INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                     INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                     INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                          AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                     INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                      AND GBCR.ReqId = GBW.ReqId
                                     INNER JOIN (
                                                SELECT   ReqId
                                                        ,MAX(EffectiveDate) AS EffectiveDate
                                                FROM     arGrdBkWeights
                                                GROUP BY ReqId
                                                ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                                     INNER JOIN (
                                                SELECT Resource
                                                      ,ResourceID
                                                FROM   syResources
                                                WHERE  ResourceTypeID = 10
                                                ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                     INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                     INNER JOIN arClassSections CSC ON CSC.TermId = T.TermId
                                                                       AND CSC.ReqId = R.ReqId
                                     WHERE      MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                                                AND SE.StuEnrollId IN (
                                                                      SELECT Val
                                                                      FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                      )
                                                AND (
                                                    @SysComponentTypeId IS NULL
                                                    OR GCT.SysComponentTypeId IN (
                                                                                 SELECT Val
                                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeId, '','', 1)
                                                                                 )
                                                    )
                                     ) AS dt
                            GROUP BY dt.TermId
                                    ,dt.ReqId
                                    ,dt.GradeBookSysComponentTypeId;
            END;
        ELSE
            BEGIN
                SET @Counter = 0;
                SET @TermStartDate1 = (
                                      SELECT AT.StartDate
                                      FROM   dbo.arTerm AS AT
                                      WHERE  AT.TermId = @TermId
                                      );
                INSERT INTO #Temp22
                            SELECT     AGBW.ReqId
                                      ,MAX(AGBW.EffectiveDate) AS EffectiveDate
                            FROM       dbo.arGrdBkWeights AS AGBW
                            INNER JOIN dbo.syCreditSummary AS SCS ON SCS.ReqId = AGBW.ReqId
                            WHERE      SCS.TermId = @TermId
                                       AND SCS.StuEnrollId IN (
                                                              SELECT Val
                                                              FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                              )
                                       AND AGBW.EffectiveDate <= @TermStartDate1
                            GROUP BY   AGBW.ReqId;

                DECLARE getUsers_Cursor CURSOR FOR
                    SELECT   dt.ID
                            ,dt.ReqId
                            ,dt.Descrip
                            ,dt.Number
                            ,dt.SysComponentTypeId
                            ,dt.MinResult
                            ,dt.GradeComponentDescription
                            ,dt.ClsSectionId
                            ,dt.StuEnrollId
                            ,ROW_NUMBER() OVER ( PARTITION BY dt.StuEnrollId
                                                             ,@TermId
                                                             ,dt.SysComponentTypeId
                                                 ORDER BY dt.SysComponentTypeId
                                                         ,dt.Descrip
                                               ) AS rownumber
                    FROM     (
                             SELECT     ISNULL(AGBWD.InstrGrdBkWgtDetailId, NEWID()) AS ID
                                       ,AR.ReqId
                                       ,AGBWD.Descrip
                                       ,AGBWD.Number
                                       ,AGCT.SysComponentTypeId
                                       ,( CASE WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN AGBWD.Number
                                               ELSE (
                                                    SELECT     MIN(AGSD.MinVal) AS MinVal
                                                    FROM       dbo.arGradeScaleDetails AS AGSD
                                                    INNER JOIN dbo.arGradeSystemDetails AS AGSDetails ON AGSDetails.GrdSysDetailId = AGSD.GrdSysDetailId
                                                    WHERE      AGSDetails.IsPass = 1
                                                               AND AGSD.GrdScaleId = ACS.GrdScaleId
                                                    )
                                          END
                                        ) AS MinResult
                                       ,SR.Resource AS GradeComponentDescription
                                       ,ACS.ClsSectionId
                                       ,AGBR.StuEnrollId
                             FROM       dbo.arGrdBkResults AS AGBR
                             INNER JOIN dbo.arGrdBkWgtDetails AS AGBWD ON AGBWD.InstrGrdBkWgtDetailId = AGBR.InstrGrdBkWgtDetailId
                             INNER JOIN dbo.arGrdBkWeights AS AGBW ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                             INNER JOIN dbo.arGrdComponentTypes AS AGCT ON AGCT.GrdComponentTypeId = AGBWD.GrdComponentTypeId
                             INNER JOIN dbo.syResources AS SR ON SR.ResourceID = AGCT.SysComponentTypeId
                             --INNER JOIN #Temp22 AS T2 ON T2.ReqId = AGBW.ReqId 
                             INNER JOIN dbo.arReqs AS AR ON AR.ReqId = AGBW.ReqId
                             INNER JOIN dbo.arClassSections AS ACS ON ACS.ReqId = AR.ReqId
                                                                      AND ACS.ClsSectionId = AGBR.ClsSectionId
                             INNER JOIN dbo.arResults AS AR2 ON AR2.TestId = ACS.ClsSectionId
                                                                AND AR2.StuEnrollId = AGBR.StuEnrollId --AND AR2.TestId = AGBR.ClsSectionId 

                             WHERE      (
                                        @StuEnrollIdList IS NULL
                                        OR AGBR.StuEnrollId IN (
                                                               SELECT Val
                                                               FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                               )
                                        )
                                        --AND T2.EffectiveDate = AGBW.EffectiveDate 
                                        AND AGBWD.Number > 0
                             ) dt
                    ORDER BY SysComponentTypeId
                            ,rownumber;
                OPEN getUsers_Cursor;
                FETCH NEXT FROM getUsers_Cursor
                INTO @curId
                    ,@curReqId
                    ,@curDescrip
                    ,@curNumber
                    ,@curGrdComponentTypeId
                    ,@curMinResult
                    ,@curGrdComponentDescription
                    ,@curClsSectionId
                    ,@curStuEnrollId
                    ,@curRownumber;
                SET @Counter = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        --PRINT @curNumber  
                        SET @times = 1;
                        WHILE @times <= @curNumber
                            BEGIN
                                --  PRINT @times  
                                IF @curNumber > 1
                                    BEGIN
                                        SET @GrdCompDescrip = @curDescrip + CAST(@times AS CHAR);


                                        IF @curGrdComponentTypeId = 544
                                            BEGIN
                                                SET @Score = (
                                                             SELECT ISNULL(SUM(HoursAttended), ''0.00'')
                                                             FROM   arExternshipAttendance
                                                             WHERE  StuEnrollId = @curStuEnrollId
                                                             );
                                            END;
                                        ELSE
                                            BEGIN
                                                SET @Score = (
                                                             SELECT ISNULL(SUM(Score), 0.00)
                                                             FROM   arGrdBkResults
                                                             WHERE  StuEnrollId = @curStuEnrollId
                                                                    AND InstrGrdBkWgtDetailId = @curId
                                                                    AND ResNum = @times
                                                                    AND ClsSectionId = @curClsSectionId
                                                             );
                                            END;

                                        SET @curRownumber = @times;
                                    END;
                                ELSE
                                    BEGIN
                                        SET @GrdCompDescrip = @curDescrip;

                                        IF @curGrdComponentTypeId = 544
                                            BEGIN
                                                SET @Score = (
                                                             SELECT ISNULL(SUM(HoursAttended), ''0.00'')
                                                             FROM   arExternshipAttendance
                                                             WHERE  StuEnrollId = @curStuEnrollId
                                                             );
                                            END;
                                        ELSE
                                            BEGIN
                                                SET @Score = (
                                                             SELECT ISNULL(SUM(Score), 0.00)
                                                             FROM   arGrdBkResults
                                                             WHERE  StuEnrollId = @curStuEnrollId
                                                                    AND InstrGrdBkWgtDetailId = @curId
                                                                    AND ResNum = @times
                                                             );
                                            END;


                                    END;
                                --PRINT @Score  
                                INSERT INTO #Temp21
                                VALUES ( @curId, @TermId, @curReqId, @GrdCompDescrip, @curNumber, @curGrdComponentTypeId, @Score, @curMinResult
                                        ,@curGrdComponentDescription, @curClsSectionId, @curStuEnrollId, @curRownumber );

                                SET @times = @times + 1;
                            END;
                        FETCH NEXT FROM getUsers_Cursor
                        INTO @curId
                            ,@curReqId
                            ,@curDescrip
                            ,@curNumber
                            ,@curGrdComponentTypeId
                            ,@curMinResult
                            ,@curGrdComponentDescription
                            ,@curClsSectionId
                            ,@curStuEnrollId
                            ,@curRownumber;
                    END;
                CLOSE getUsers_Cursor;
                DEALLOCATE getUsers_Cursor;

                INSERT INTO #tempTermWorkUnitCount
                            SELECT   dt.TermId
                                    ,dt.ReqId
                                    ,dt.GradeBookSysComponentTypeId
                                    ,COUNT(dt.GradeBookDescription)
                            FROM     (
                                     SELECT T21.TermId
                                           ,T21.ReqId
                                           ,T21.GradeBookDescription
                                           ,T21.GradeBookSysComponentTypeId
                                     FROM   #Temp21 AS T21
                                     UNION
                                     SELECT     T.TermId
                                               ,GBCR.ReqId
                                               ,GCT.Descrip AS GradeBookDescription
                                               ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                     FROM       arGrdBkConversionResults GBCR
                                     INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                     INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                     INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                     INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                     INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                     INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                          AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                     INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                      AND GBCR.ReqId = GBW.ReqId
                                     INNER JOIN (
                                                SELECT   ReqId
                                                        ,MAX(EffectiveDate) AS EffectiveDate
                                                FROM     arGrdBkWeights
                                                GROUP BY ReqId
                                                ) AS MED ON MED.ReqId = GBCR.ReqId -- MED --> MaxEffectiveDatesByCourse  
                                     INNER JOIN (
                                                SELECT Resource
                                                      ,ResourceID
                                                FROM   syResources
                                                WHERE  ResourceTypeID = 10
                                                ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                     INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                     WHERE      MED.EffectiveDate <= T.StartDate
                                                AND SE.StuEnrollId IN (
                                                                      SELECT Val
                                                                      FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                      )
                                                AND T.TermId = @TermId
                                                AND (
                                                    @SysComponentTypeId IS NULL
                                                    OR GCT.SysComponentTypeId IN (
                                                                                 SELECT Val
                                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeId, '','', 1)
                                                                                 )
                                                    )
                                     ) dt
                            GROUP BY dt.TermId
                                    ,dt.ReqId
                                    ,dt.GradeBookSysComponentTypeId;
            END;

        DROP TABLE #Temp22;
        DROP TABLE #Temp21;

        -- SELECT * FROM #tempTermWorkUnitCount AS TTWUC  ORDER BY TTWUC.TermId  -- , TTWUC.StuEnrollId  


        SELECT   dt1.PrgVerId
                ,dt1.PrgVerDescrip
                ,dt1.PrgVersionTrackCredits
                ,dt1.TermId
                ,dt1.TermDescription
                ,dt1.TermStartDate
                ,dt1.TermEndDate
                ,dt1.CourseId
                ,dt1.CouseStartDate
                ,dt1.CourseCode
                ,dt1.CourseDescription
                ,dt1.CourseCodeDescription
                ,dt1.CourseCredits
                ,dt1.CourseFinAidCredits
                ,dt1.MinVal
                ,dt1.CourseScore
                ,dt1.GradeBook_ResultId
                ,dt1.GradeBookDescription
                ,dt1.GradeBookScore
                ,dt1.GradeBookPostDate
                ,dt1.GradeBookPassingGrade
                ,dt1.GradeBookWeight
                ,dt1.GradeBookRequired
                ,dt1.GradeBookMustPass
                ,dt1.GradeBookSysComponentTypeId
                ,dt1.GradeBookHoursRequired
                ,dt1.GradeBookHoursCompleted
                ,dt1.StuEnrollId
                ,dt1.MinResult
                ,dt1.GradeComponentDescription -- Student data     
                ,dt1.CreditsAttempted
                ,dt1.CreditsEarned
                ,dt1.Completed
                ,dt1.CurrentScore
                ,dt1.CurrentGrade
                ,CASE WHEN (
                           SELECT TOP 1 P.ACId
                           FROM   dbo.arStuEnrollments E
                           JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                           JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
                           WHERE  E.StuEnrollId = dt1.StuEnrollId
                           ) = 5
                           AND @GradesFormat = ''numeric'' THEN dbo.CalculateStudentAverage(dt1.StuEnrollId, NULL, NULL, dt1.ClsSectionId, NULL, NULL)
                      ELSE dt1.FinalScore
                 END AS FinalScore
                ,dt1.FinalGrade
                ,dt1.CampusId
                ,dt1.CampDescrip
                ,dt1.rownumber
                ,dt1.FirstName
                ,dt1.LastName
                ,dt1.MiddleName
                ,dt1.GrdBkWgtDetailsCount
                ,dt1.ClockHourProgram
                ,dt1.GradesFormat
                ,dt1.GPAMethod
                ,dt1.WorkUnitCount
                ,dt1.WorkUnitCount_501
                ,dt1.WorkUnitCount_544
                ,dt1.WorkUnitCount_502
                ,dt1.WorkUnitCount_499
                ,dt1.WorkUnitCount_503
                ,dt1.WorkUnitCount_500
                ,dt1.WorkUnitCount_533
                ,dt1.rownumber2
        FROM     (
                 SELECT dt.PrgVerId
                       ,dt.PrgVerDescrip
                       ,dt.PrgVersionTrackCredits
                       ,dt.TermId
                       ,dt.TermDescription
                       ,dt.TermStartDate
                       ,dt.TermEndDate
                       ,dt.CourseId
                       ,dt.CouseStartDate
                       ,dt.CourseCode
                       ,dt.CourseDescription
                       ,dt.CourseCodeDescription
                       ,dt.CourseCredits
                       ,dt.CourseFinAidCredits
                       ,dt.MinVal
                       ,dt.CourseScore
                       ,dt.GradeBook_ResultId
                       ,dt.GradeBookDescription
                       ,dt.GradeBookScore
                       ,dt.GradeBookPostDate
                       ,dt.GradeBookPassingGrade
                       ,dt.GradeBookWeight
                       ,dt.GradeBookRequired
                       ,dt.GradeBookMustPass
                       ,dt.GradeBookSysComponentTypeId
                       ,dt.GradeBookHoursRequired
                       ,dt.GradeBookHoursCompleted
                       ,dt.StuEnrollId
                       ,dt.MinResult
                       ,dt.GradeComponentDescription -- Student data     
                       ,dt.CreditsAttempted
                       ,dt.CreditsEarned
                       ,dt.Completed
                       ,dt.CurrentScore
                       ,dt.CurrentGrade
                       ,dt.FinalScore
                       ,dt.FinalGrade
                       ,dt.CampusId
                       ,dt.CampDescrip
                       ,dt.rownumber
                       ,dt.FirstName
                       ,dt.LastName
                       ,dt.MiddleName
                       ,dt.GrdBkWgtDetailsCount
                       ,dt.ClockHourProgram
                       ,@GradesFormat AS GradesFormat
                       ,@GPAMethod AS GPAMethod
                       ,(
                        SELECT SUM(WorkUnitCount)
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  T.TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId IN ( 501, 544, 502, 499, 503, 500, 533 )
                        ) AS WorkUnitCount
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 501
                        ) AS WorkUnitCount_501
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 544
                        ) AS WorkUnitCount_544
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 502
                        ) AS WorkUnitCount_502
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 499
                        ) AS WorkUnitCount_499
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 503
                        ) AS WorkUnitCount_503
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 500
                        ) AS WorkUnitCount_500
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 533
                        ) AS WorkUnitCount_533
                       ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId
                                                        ,TermId
                                                        ,CourseId
                                            ORDER BY TermStartDate
                                                    ,TermEndDate
                                                    ,TermDescription
                                                    ,CourseDescription
                                          ) AS rownumber2
                       ,dt.ClsSectionId
					   ,dt.ReqSeq
                 FROM   (
                        SELECT     DISTINCT PV.PrgVerId
                                           ,PV.PrgVerDescrip
                                           ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                                 ELSE 0
                                            END AS PrgVersionTrackCredits
                                           ,T.TermId
                                           ,T.TermDescrip AS TermDescription
                                           ,T.StartDate AS TermStartDate
                                           ,T.EndDate AS TermEndDate
                                           ,CS.ReqId AS CourseId
                                           ,CS.StartDate AS CouseStartDate
                                           ,R.Code AS CourseCode
                                           ,R.Descrip AS CourseDescription
                                           ,''('' + R.Code + '') '' + R.Descrip AS CourseCodeDescription
                                           ,R.Credits AS CourseCredits
                                           ,R.FinAidCredits AS CourseFinAidCredits
                                           ,(
                                            SELECT MIN(MinVal)
                                            FROM   arGradeScaleDetails GCD
                                                  ,arGradeSystemDetails GSD
                                            WHERE  GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                                   AND GSD.IsPass = 1
                                                   AND GCD.GrdScaleId = CS.GrdScaleId
                                            ) AS MinVal
                                           ,RES.Score AS CourseScore
                                           ,NULL AS GradeBook_ResultId
                                           ,NULL AS GradeBookDescription
                                           ,NULL AS GradeBookScore
                                           ,NULL AS GradeBookPostDate
                                           ,NULL AS GradeBookPassingGrade
                                           ,NULL AS GradeBookWeight
                                           ,NULL AS GradeBookRequired
                                           ,NULL AS GradeBookMustPass
                                           ,NULL AS GradeBookSysComponentTypeId
                                           ,NULL AS GradeBookHoursRequired
                                           ,NULL AS GradeBookHoursCompleted
                                           ,SE.StuEnrollId
                                           ,NULL AS MinResult
                                           ,NULL AS GradeComponentDescription -- Student data     
                                           ,ISNULL(SCS.CreditsAttempted, 0) AS CreditsAttempted
                                           ,ISNULL(SCS.CreditsEarned, 0) AS CreditsEarned
                                           ,SCS.Completed AS Completed
                                           ,SCS.CurrentScore AS CurrentScore
                                           ,SCS.CurrentGrade AS CurrentGrade
                                           ,SCS.FinalScore AS FinalScore
                                           ,SCS.FinalGrade AS FinalGrade
                                           ,C.CampusId
                                           ,C.CampDescrip
                                           ,NULL AS rownumber
                                           ,S.FirstName AS FirstName
                                           ,S.LastName AS LastName
                                           ,S.MiddleName
                                           ,(
                                            SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                            FROM   arGrdBkResults
                                            WHERE  StuEnrollId = SE.StuEnrollId
                                                   AND ClsSectionId = RES.TestId
                                            ) AS GrdBkWgtDetailsCount
                                           ,CASE WHEN P.ACId = 5 THEN ''True''
                                                 ELSE ''False''
                                            END AS ClockHourProgram
                                           ,CS.ClsSectionId
										    ,PVD.ReqSeq
                        FROM       arClassSections CS
                        INNER JOIN arResults GBR ON CS.ClsSectionId = GBR.TestId
                        INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
                        INNER JOIN (
                                   SELECT StudentId
                                         ,FirstName
                                         ,LastName
                                         ,MiddleName
                                   FROM   adLeads
                                   ) S ON S.StudentId = SE.StudentId
                        INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                        INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                        INNER JOIN arTerm T ON CS.TermId = T.TermId
                        INNER JOIN arReqs R ON CS.ReqId = R.ReqId
						      LEFT JOIN  dbo.arProgVerDef PVD ON PVD.PrgVerId = PV.PrgVerId
                                                       AND PVD.ReqId = R.ReqId
                        INNER JOIN arResults RES ON RES.StuEnrollId = GBR.StuEnrollId
                                                    AND RES.TestId = CS.ClsSectionId
                        LEFT JOIN  syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                          AND T.TermId = SCS.TermId
                                                          AND R.ReqId = SCS.ReqId
                        WHERE      SE.StuEnrollId IN (
                                                     SELECT Val
                                                     FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                     )
                                   AND (
                                       @TermId IS NULL
                                       OR T.TermId = @TermId
                                       )
                                   AND R.IsAttendanceOnly = 0
                        UNION
                        SELECT     DISTINCT PV.PrgVerId
                                           ,PV.PrgVerDescrip
                                           ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                                 ELSE 0
                                            END AS PrgVersionTrackCredits
                                           ,T.TermId
                                           ,T.TermDescrip
                                           ,T.StartDate
                                           ,T.EndDate
                                           ,GBCR.ReqId
                                           ,T.StartDate AS CouseStartDate     -- tranfered  
                                           ,R.Code AS CourseCode
                                           ,R.Descrip AS CourseDescrip
                                           ,''('' + R.Code + '') '' + R.Descrip AS CourseCodeDescrip
                                           ,R.Credits
                                           ,R.FinAidCredits
                                           ,(
                                            SELECT MIN(MinVal)
                                            FROM   arGradeScaleDetails GCD
                                                  ,arGradeSystemDetails GSD
                                            WHERE  GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                                   AND GSD.IsPass = 1
                                            )
                                           ,ISNULL(GBCR.Score, 0)
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,NULL
                                           ,SE.StuEnrollId
                                           ,NULL AS MinResult
                                           ,NULL AS GradeComponentDescription -- Student data      
                                           ,ISNULL(SCS.CreditsAttempted, 0) AS CreditsAttempted
                                           ,ISNULL(SCS.CreditsEarned, 0) AS CreditsEarned
                                           ,SCS.Completed AS Completed
                                           ,SCS.CurrentScore AS CurrentScore
                                           ,SCS.CurrentGrade AS CurrentGrade
                                           ,SCS.FinalScore AS FinalScore
                                           ,SCS.FinalGrade AS FinalGrade
                                           ,C.CampusId
                                           ,C.CampDescrip
                                           ,NULL AS rownumber
                                           ,S.FirstName AS FirstName
                                           ,S.LastName AS LastName
                                           ,S.MiddleName
                                           ,(
                                            SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                            FROM   arGrdBkConversionResults
                                            WHERE  StuEnrollId = SE.StuEnrollId
                                                   AND TermId = GBCR.TermId
                                                   AND ReqId = GBCR.ReqId
                                            ) AS GrdBkWgtDetailsCount
                                           ,CASE WHEN P.ACId = 5 THEN ''True''
                                                 ELSE ''False''
                                            END AS ClockHourProgram
                                           ,SCS.ClsSectionId
										    ,PVD.ReqSeq
                        FROM       arTransferGrades GBCR
                        INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                        INNER JOIN (
                                   SELECT StudentId
                                         ,FirstName
                                         ,LastName
                                         ,MiddleName
                                   FROM   adLeads
                                   ) S ON S.StudentId = SE.StudentId
                        INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                        INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                        INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                        INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
						      LEFT JOIN  dbo.arProgVerDef PVD ON PVD.PrgVerId = PV.PrgVerId
                                                       AND PVD.ReqId = R.ReqId
                        --INNER JOIN arTransferGrades AR ON GBCR.StuEnrollId = AR.StuEnrollId  
                        LEFT JOIN  syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                          AND T.TermId = SCS.TermId
                                                          AND R.ReqId = SCS.ReqId
                        WHERE      SE.StuEnrollId IN (
                                                     SELECT Val
                                                     FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                     )
                                   AND (
                                       @TermId IS NULL
                                       OR T.TermId = @TermId
                                       )
                                   AND R.IsAttendanceOnly = 0
                        ) dt
                 ) dt1
        --WHERE rownumber2<=2  
        ORDER BY TermStartDate
                ,TermEndDate
                -- , TermDescription  

                ,CouseStartDate
                ,dt1.ReqSeq ASC
                ,CourseCode;
    --, CASE WHEN LTRIM(RTRIM(LOWER(@GradesFormat))) = ''letter''  
    --       THEN (RANK() OVER (ORDER BY FinalGrade DESC))  
    --  END  
    --, CASE WHEN LTRIM(RTRIM(LOWER(@GradesFormat))) <> ''letter''  
    --       THEN (RANK() OVER (ORDER BY FinalScore DESC))  
    --  END;  
    END;
-- ========================================================================================================= 
-- END  --  Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents 
-- ========================================================================================================= 


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_TR_Sub10_Courses]'
GO
IF OBJECT_ID(N'[dbo].[Usp_TR_Sub10_Courses]', 'P') IS NULL
EXEC sp_executesql N'
-- =========================================================================================================
-- Usp_TR_Sub10_Courses
-- =========================================================================================================
CREATE PROCEDURE [dbo].[Usp_TR_Sub10_Courses]
    @StuEnrollIdList VARCHAR(MAX)
   ,@TermId VARCHAR(50) = NULL
   ,@SysComponentTypeIdList VARCHAR(MAX) = NULL
AS -- source usp_pr_main_ssrs_subreport3_byterm

    DECLARE @GradesFormat AS VARCHAR(50);
    DECLARE @GPAMethod AS VARCHAR(50);
    DECLARE @GradeBookAt AS VARCHAR(50);
    DECLARE @StuEnrollCampusId AS UNIQUEIDENTIFIER;
    DECLARE @Counter AS INT;
    DECLARE @times AS INT;
    --DECLARE @TermStartDate1 AS DATETIME; 
    DECLARE @Score AS DECIMAL(18, 2);
    DECLARE @GrdCompDescrip AS VARCHAR(50);

    DECLARE @curId AS UNIQUEIDENTIFIER;
    DECLARE @curReqId AS UNIQUEIDENTIFIER;
    DECLARE @curStuEnrollId AS UNIQUEIDENTIFIER;
    DECLARE @curDescrip AS VARCHAR(50);
    DECLARE @curNumber AS INT;
    DECLARE @curGrdComponentTypeId AS INT;
    DECLARE @curMinResult AS DECIMAL(18, 2);
    DECLARE @curGrdComponentDescription AS VARCHAR(50);
    DECLARE @curClsSectionId AS UNIQUEIDENTIFIER;
    DECLARE @curTermId AS UNIQUEIDENTIFIER;
    DECLARE @curRownumber AS INT;
    DECLARE @DefaulSysComponentTypeIdList VARCHAR(MAX);

    SET @DefaulSysComponentTypeIdList = ''501,544,502,499,503,500,533'';


    CREATE TABLE #tempTermWorkUnitCount
        (
            TermId UNIQUEIDENTIFIER
           ,ReqId UNIQUEIDENTIFIER
           ,sysComponentTypeId INT
           ,WorkUnitCount INT
        );
    CREATE TABLE #Temp21
        (
            Id UNIQUEIDENTIFIER
           ,TermId UNIQUEIDENTIFIER
           ,ReqId UNIQUEIDENTIFIER
           ,GradeBookDescription VARCHAR(50)
           ,Number INT
           ,GradeBookSysComponentTypeId INT
           ,GradeBookScore DECIMAL(18, 2)
           ,MinResult DECIMAL(18, 2)
           ,GradeComponentDescription VARCHAR(50)
           ,ClsSectionId UNIQUEIDENTIFIER
           ,RowNumber INT
        );
    CREATE TABLE #Temp22
        (
            ReqId UNIQUEIDENTIFIER
           ,EffectiveDate DATETIME
        );

    IF @SysComponentTypeIdList IS NULL
        BEGIN
            SET @SysComponentTypeIdList = @DefaulSysComponentTypeIdList;
        END;
    SET @StuEnrollCampusId = COALESCE((
                                      SELECT TOP 1 ASE.CampusId
                                      FROM   arStuEnrollments AS ASE
                                      WHERE  ASE.StuEnrollId IN (
                                                                SELECT Val
                                                                FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                )
                                      )
                                     ,NULL
                                     );
    SET @GradesFormat = dbo.GetAppSettingValueByKeyName(''GradesFormat'', @StuEnrollCampusId);
    IF ( @GradesFormat IS NOT NULL )
        BEGIN
            SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
        END;
    SET @GPAMethod = dbo.GetAppSettingValueByKeyName(''GPAMethod'', @StuEnrollCampusId);
    IF ( @GPAMethod IS NOT NULL )
        BEGIN
            SET @GPAMethod = LOWER(LTRIM(RTRIM(@GPAMethod)));
        END;
    SET @GradeBookAt = dbo.GetAppSettingValueByKeyName(''GradeBookWeightingLevel'', @StuEnrollCampusId);
    IF ( @GradeBookAt IS NOT NULL )
        BEGIN
            SET @GradeBookAt = LOWER(LTRIM(RTRIM(@GradeBookAt)));
        END;

    IF LOWER(@GradeBookAt) = ''instructorlevel''
        BEGIN
            INSERT INTO #tempTermWorkUnitCount
                        SELECT   dt.TermId
                                ,dt.ReqId
                                ,dt.GradeBookSysComponentTypeId
                                ,COUNT(dt.GradeBookDescription)
                        FROM     (
                                 SELECT     d.ReqId
                                           ,d.TermId
                                           ,CASE WHEN a.Descrip IS NULL THEN e.Descrip
                                                 ELSE a.Descrip
                                            END AS GradeBookDescription
                                           ,( CASE WHEN e.SysComponentTypeId IN ( 500, 503, 544 ) THEN a.Number
                                                   ELSE (
                                                        SELECT MIN(MinVal)
                                                        FROM   arGradeScaleDetails GSD
                                                              ,arGradeSystemDetails GSS
                                                        WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                               AND GSS.IsPass = 1
                                                               AND GSD.GrdScaleId = d.GrdScaleId
                                                        )
                                              END
                                            ) AS MinResult
                                           ,a.Required
                                           ,a.MustPass
                                           ,ISNULL(e.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId
                                           ,a.Number
                                           ,RES.Resource AS GradeComponentDescription
                                           ,a.InstrGrdBkWgtDetailId
                                           ,c.StuEnrollId
                                           ,0 AS IsExternShip
                                           ,CASE e.SysComponentTypeId
                                                 WHEN 544 THEN (
                                                               SELECT SUM(ISNULL(AEA.HoursAttended, 0.0))
                                                               FROM   arExternshipAttendance AS AEA
                                                               WHERE  AEA.StuEnrollId = c.StuEnrollId
                                                               )
                                                 ELSE (
                                                      SELECT SUM(ISNULL(AGBR1.Score, 0.0))
                                                      FROM   arGrdBkResults AS AGBR1
                                                      WHERE  AGBR1.StuEnrollId = c.StuEnrollId
                                                             AND AGBR1.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                                             AND AGBR1.ClsSectionId = d.ClsSectionId
                                                      )
                                            END AS GradeBookScore
                                           ,ROW_NUMBER() OVER ( PARTITION BY ST.StuEnrollId
                                                                            ,T.TermId
                                                                            ,R.ReqId
                                                                ORDER BY e.SysComponentTypeId
                                                                        ,a.Descrip
                                                              ) AS rownumber
                                 FROM       arGrdBkWgtDetails AS a
                                 INNER JOIN arGrdBkResults AS AGBR ON AGBR.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                 --INNER JOIN arGrdBkWeights AS b ON b.InstrGrdBkWgtId = a.InstrGrdBkWgtId
                                 INNER JOIN arClassSections AS d ON d.ClsSectionId = AGBR.ClsSectionId
                                 INNER JOIN arResults AS c ON c.TestId = d.ClsSectionId
                                 INNER JOIN arGrdComponentTypes AS e ON e.GrdComponentTypeId = a.GrdComponentTypeId
                                 INNER JOIN arStuEnrollments AS ST ON ST.StuEnrollId = c.StuEnrollId
                                 INNER JOIN arTerm AS T ON T.TermId = d.TermId
                                 INNER JOIN arReqs AS R ON R.ReqId = d.ReqId
                                 INNER JOIN syResources AS RES ON RES.ResourceID = e.SysComponentTypeId
                                 WHERE      c.StuEnrollId IN (
                                                             SELECT Val
                                                             FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                             )
                                            AND e.SysComponentTypeId IN (
                                                                        SELECT Val
                                                                        FROM   MultipleValuesForReportParameters(@SysComponentTypeIdList, '','', 1)
                                                                        )
                                 UNION
                                 SELECT     R.ReqId
                                           ,T.TermId
                                           ,CASE WHEN GBWD.Descrip IS NULL THEN (
                                                                                SELECT Resource
                                                                                FROM   syResources
                                                                                WHERE  ResourceID = GCT.SysComponentTypeId
                                                                                )
                                                 ELSE GBWD.Descrip
                                            END AS GradeBookDescription
                                           ,CASE WHEN GCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN GBWD.Number
                                                 ELSE (
                                                      SELECT MIN(MinVal)
                                                      FROM   arGradeScaleDetails GSD
                                                            ,arGradeSystemDetails GSS
                                                      WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                             AND GSS.IsPass = 1
                                                             AND GSD.GrdScaleId = CSC.GrdScaleId
                                                      )
                                            END AS MinResult
                                           ,GBWD.Required
                                           ,GBWD.MustPass
                                           ,ISNULL(GCT.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId
                                           ,GBWD.Number
                                           ,(
                                            SELECT Resource
                                            FROM   syResources
                                            WHERE  ResourceID = GCT.SysComponentTypeId
                                            ) AS GradeComponentDescription
                                           ,GBWD.InstrGrdBkWgtDetailId
                                           ,GBCR.StuEnrollId
                                           ,0 AS IsExternShip
                                           ,CASE GCT.SysComponentTypeId
                                                 WHEN 544 THEN (
                                                               SELECT SUM(ISNULL(AEA.HoursAttended, 0.0))
                                                               FROM   arExternshipAttendance AS AEA
                                                               WHERE  AEA.StuEnrollId = SE.StuEnrollId
                                                               )
                                                 ELSE (
                                                      SELECT SUM(ISNULL(AGBR.Score, 0.0))
                                                      FROM   arGrdBkResults AS AGBR
                                                      WHERE  AGBR.StuEnrollId = SE.StuEnrollId
                                                             AND AGBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                             AND AGBR.ClsSectionId = CSC.ClsSectionId
                                                      )
                                            END AS GradeBookScore
                                           ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId
                                                                            ,T.TermId
                                                                            ,R.ReqId
                                                                ORDER BY SE.CampusId
                                                                        ,T.StartDate
                                                                        ,T.EndDate
                                                                        ,T.TermId
                                                                        ,T.TermDescrip
                                                                        ,R.ReqId
                                                                        ,R.Descrip
                                                                        ,SYRES.Resource
                                                                        ,GCT.Descrip
                                                              ) AS rownumber
                                 FROM       arGrdBkConversionResults GBCR
                                 INNER JOIN arStuEnrollments SE ON SE.StuEnrollId = GBCR.StuEnrollId
                                 INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                 INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                 INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                 INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                 INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                      AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                 INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                  AND GBCR.ReqId = GBW.ReqId
                                 INNER JOIN (
                                            SELECT   AGBW.ReqId
                                                    ,MAX(AGBW.EffectiveDate) AS EffectiveDate
                                            FROM     arGrdBkWeights AS AGBW
                                            GROUP BY AGBW.ReqId
                                            ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                                 INNER JOIN (
                                            SELECT SR.Resource
                                                  ,SR.ResourceID
                                            FROM   syResources AS SR
                                            WHERE  SR.ResourceTypeID = 10
                                            ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                 INNER JOIN syCampuses C ON C.CampusId = SE.CampusId
                                 INNER JOIN arClassSections CSC ON CSC.TermId = T.TermId
                                                                   AND CSC.ReqId = R.ReqId
                                 LEFT JOIN  arGrdBkResults AS AGBR ON AGBR.StuEnrollId = SE.StuEnrollId
                                                                      AND AGBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                      AND AGBR.ClsSectionId = CSC.ClsSectionId
                                 WHERE      MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                                            AND SE.StuEnrollId IN (
                                                                  SELECT Val
                                                                  FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                  )
                                            AND GCT.SysComponentTypeId IN (
                                                                          SELECT Val
                                                                          FROM   MultipleValuesForReportParameters(@SysComponentTypeIdList, '','', 1)
                                                                          )
                                 ) AS dt
                        GROUP BY dt.TermId
                                ,dt.ReqId
                                ,dt.GradeBookSysComponentTypeId;
        END;
    ELSE
        BEGIN
            SET @Counter = 0;
            --SET @TermStartDate1 = (
            --                       SELECT   AT.StartDate
            --                       FROM     arTerm AS AT
            --                       WHERE    AT.TermId = @TermId
            --                      );
            INSERT INTO #Temp22
                        SELECT     AGBW.ReqId
                                  ,MAX(AGBW.EffectiveDate) AS EffectiveDate
                        FROM       arGrdBkWeights AS AGBW
                        INNER JOIN syCreditSummary AS SCS ON SCS.ReqId = AGBW.ReqId
                        WHERE      SCS.StuEnrollId IN (
                                                      SELECT Val
                                                      FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                      )
                        -- AND SCS.TermId = @TermId
                        -- AND AGBW.EffectiveDate <= @TermStartDate1
                        GROUP BY   AGBW.ReqId;

            SELECT     AGBWD.InstrGrdBkWgtDetailId
                      ,AGBWD.InstrGrdBkWgtId
                      ,AGBWD.Code
                      ,AGBWD.Descrip
                      ,AGBWD.Weight
                      ,AGBWD.Seq
                      ,AGBWD.ModUser
                      ,AGBWD.ModDate
                      ,AGBWD.GrdComponentTypeId
                      ,AGBWD.Parameter
                      ,AGBWD.Number
                      ,AGBWD.GrdPolicyId
                      ,AGBWD.Required
                      ,AGBWD.MustPass
                      ,AGBWD.CreditsPerService
            INTO       #tmpGrdBkWgtDetails
            FROM       arGrdBkWgtDetails AS AGBWD --INNER JOIN arGrdBkWeights AS AGBW ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
            INNER JOIN arGrdBkResults AS AGBR ON AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
            WHERE      AGBR.StuEnrollId IN (
                                           SELECT Val
                                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                           );


            DECLARE getUsers_Cursor CURSOR FAST_FORWARD FORWARD_ONLY FOR
                SELECT   dt.ID
                        ,dt.ReqId
                        ,dt.Descrip
                        ,dt.Number
                        ,dt.SysComponentTypeId
                        ,dt.MinResult
                        ,dt.GradeComponentDescription
                        ,dt.ClsSectionId
                        ,dt.StuEnrollId
                        ,dt.TermId
                        ,ROW_NUMBER() OVER ( PARTITION BY dt.StuEnrollId
                                                         ,@TermId
                                                         ,dt.SysComponentTypeId
                                             ORDER BY dt.SysComponentTypeId
                                                     ,dt.Descrip
                                           ) AS rownumber
                FROM     (
                         SELECT     DISTINCT ISNULL(GD.InstrGrdBkWgtDetailId, NEWID()) AS ID
                                            ,CS.ReqId --AR.ReqId
                                            ,GC.Descrip
                                            ,GD.Number
                                            ,GC.SysComponentTypeId
                                            ,( CASE WHEN GC.SysComponentTypeId IN ( 500, 503, 544 ) THEN GD.Number
                                                    ELSE (
                                                         SELECT MIN(MinVal)
                                                         FROM   arGradeScaleDetails GSD
                                                               ,arGradeSystemDetails GSS
                                                         WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                AND GSS.IsPass = 1
                                                                AND GSD.GrdScaleId = CS.GrdScaleId
                                                         )
                                               END
                                             ) AS MinResult
                                            ,S.Resource AS GradeComponentDescription
                                            ,CS.ClsSectionId
                                            ,RES.StuEnrollId
                                            ,T.TermId
                         FROM       arGrdComponentTypes AS GC
                         INNER JOIN #tmpGrdBkWgtDetails AS GD ON GD.GrdComponentTypeId = GC.GrdComponentTypeId
                         INNER JOIN arGrdBkWeights AS AGBW1 ON AGBW1.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
                         INNER JOIN arReqs AS AR ON AR.ReqId = AGBW1.ReqId
                         INNER JOIN arClassSections AS CS ON CS.ReqId = AR.ReqId
                         INNER JOIN syResources AS S ON S.ResourceID = GC.SysComponentTypeId
                         INNER JOIN arResults AS RES ON RES.TestId = CS.ClsSectionId
                         INNER JOIN arTerm AS T ON T.TermId = CS.TermId
                         WHERE      RES.StuEnrollId IN (
                                                       SELECT Val
                                                       FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                       )
                                    AND GD.Number > 0
                         ) dt
                ORDER BY SysComponentTypeId
                        ,rownumber;
            OPEN getUsers_Cursor;
            FETCH NEXT FROM getUsers_Cursor
            INTO @curId
                ,@curReqId
                ,@curDescrip
                ,@curNumber
                ,@curGrdComponentTypeId
                ,@curMinResult
                ,@curGrdComponentDescription
                ,@curClsSectionId
                ,@curStuEnrollId
                ,@curTermId
                ,@curRownumber;
            SET @Counter = 0;
            WHILE @@FETCH_STATUS = 0
                BEGIN
                    --PRINT @curNumber
                    SET @times = 1;
                    WHILE @times <= @curNumber
                        BEGIN
                            --  PRINT @times
                            IF @curNumber > 1
                                BEGIN
                                    SET @GrdCompDescrip = @curDescrip + CAST(@times AS CHAR);
                                    IF @curGrdComponentTypeId = 544
                                        BEGIN
                                            SET @Score = (
                                                         SELECT SUM(ISNULL(AEA.HoursAttended, 0.0))
                                                         FROM   arExternshipAttendance AS AEA
                                                         WHERE  AEA.StuEnrollId = @curStuEnrollId
                                                         );
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @Score = (
                                                         SELECT SUM(ISNULL(AGBR.Score, 0.0))
                                                         FROM   arGrdBkResults AS AGBR
                                                         WHERE  AGBR.StuEnrollId = @curStuEnrollId
                                                                AND AGBR.InstrGrdBkWgtDetailId = @curId
                                                                AND AGBR.ResNum = @times
                                                                AND AGBR.ClsSectionId = @curClsSectionId
                                                         );
                                        END;
                                    SET @curRownumber = @times;
                                END;
                            ELSE
                                BEGIN
                                    SET @GrdCompDescrip = @curDescrip;
                                    SET @Score = (
                                                 SELECT TOP 1 Score
                                                 FROM   arGrdBkResults
                                                 WHERE  StuEnrollId = @curStuEnrollId
                                                        AND InstrGrdBkWgtDetailId = @curId
                                                        AND ResNum = @times
                                                 );
                                    IF @Score IS NULL
                                        BEGIN
                                            SET @Score = (
                                                         SELECT TOP 1 Score
                                                         FROM   arGrdBkResults
                                                         WHERE  StuEnrollId = @curStuEnrollId
                                                                AND InstrGrdBkWgtDetailId = @curId
                                                                AND ResNum = ( @times - 1 )
                                                         );
                                        END;
                                    IF @curGrdComponentTypeId = 544
                                        BEGIN
                                            SET @Score = (
                                                         SELECT SUM(ISNULL(AEA.HoursAttended, 0.0))
                                                         FROM   arExternshipAttendance AS AEA
                                                         WHERE  AEA.StuEnrollId = @curStuEnrollId
                                                         );
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @Score = (
                                                         SELECT SUM(ISNULL(AGBR.Score, 0.0))
                                                         FROM   arGrdBkResults AS AGBR
                                                         WHERE  AGBR.StuEnrollId = @curStuEnrollId
                                                                AND AGBR.InstrGrdBkWgtDetailId = @curId
                                                                AND AGBR.ResNum = @times
                                                         );
                                        END;
                                END;
                            --PRINT @Score
                            INSERT INTO #Temp21
                            VALUES ( @curId, @curTermId, @curReqId, @GrdCompDescrip, @curNumber, @curGrdComponentTypeId, @Score, @curMinResult
                                    ,@curGrdComponentDescription, @curClsSectionId, @curRownumber );

                            SET @times = @times + 1;
                        END;
                    FETCH NEXT FROM getUsers_Cursor
                    INTO @curId
                        ,@curReqId
                        ,@curDescrip
                        ,@curNumber
                        ,@curGrdComponentTypeId
                        ,@curMinResult
                        ,@curGrdComponentDescription
                        ,@curClsSectionId
                        ,@curStuEnrollId
                        ,@curTermId
                        ,@curRownumber;
                END;
            CLOSE getUsers_Cursor;
            DEALLOCATE getUsers_Cursor;



            INSERT INTO #tempTermWorkUnitCount
                        SELECT   dt.TermId
                                ,dt.ReqId
                                ,dt.GradeBookSysComponentTypeId
                                ,COUNT(dt.GradeBookDescription)
                        FROM     (
                                 SELECT T21.TermId
                                       ,T21.ReqId
                                       ,T21.GradeBookDescription
                                       ,T21.GradeBookSysComponentTypeId
                                 FROM   #Temp21 AS T21
                                 UNION
                                 SELECT     T.TermId
                                           ,GBCR.ReqId
                                           ,GCT.Descrip AS GradeBookDescription
                                           ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                 FROM       arGrdBkConversionResults GBCR
                                 INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                 INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                 INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                 INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                 INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                 INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                      AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                 --INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                 --                                 AND GBCR.ReqId = GBW.ReqId
                                 INNER JOIN (
                                            SELECT   ReqId
                                                    ,MAX(EffectiveDate) AS EffectiveDate
                                            FROM     arGrdBkWeights
                                            GROUP BY ReqId
                                            ) AS MED ON MED.ReqId = GBCR.ReqId -- MED --> MaxEffectiveDatesByCourse
                                 INNER JOIN (
                                            SELECT Resource
                                                  ,ResourceID
                                            FROM   syResources
                                            WHERE  ResourceTypeID = 10
                                            ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                 INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                 WHERE      MED.EffectiveDate <= T.StartDate
                                            AND SE.StuEnrollId IN (
                                                                  SELECT Val
                                                                  FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                  )
                                            AND T.TermId = @TermId
                                            AND GCT.SysComponentTypeId IN (
                                                                          SELECT Val
                                                                          FROM   MultipleValuesForReportParameters(@SysComponentTypeIdList, '','', 1)
                                                                          )
                                 ) dt
                        GROUP BY dt.TermId
                                ,dt.ReqId
                                ,dt.GradeBookSysComponentTypeId;
        END;



    DROP TABLE #Temp22;
    DROP TABLE #Temp21;




    SELECT   dt1.PrgVerId
            ,dt1.PrgVerDescrip
            ,dt1.PrgVersionTrackCredits
            ,dt1.TermId
            ,dt1.TermDescription
            ,dt1.TermStartDate
            ,dt1.TermEndDate
            ,dt1.CourseId
            ,dt1.CouseStartDate
            ,dt1.CourseCode
            ,dt1.CourseDescription
            ,dt1.CourseCodeDescription
            ,dt1.CourseCredits
            ,dt1.CourseFinAidCredits
            ,dt1.MinVal
            ,dt1.CourseScore
            ,dt1.GradeBook_ResultId
            ,dt1.GradeBookDescription
            ,dt1.GradeBookScore
            ,dt1.GradeBookPostDate
            ,dt1.GradeBookPassingGrade
            ,dt1.GradeBookWeight
            ,dt1.GradeBookRequired
            ,dt1.GradeBookMustPass
            ,dt1.GradeBookSysComponentTypeId
            ,dt1.GradeBookHoursRequired
            ,dt1.GradeBookHoursCompleted
            ,dt1.StuEnrollId
            ,dt1.MinResult
            ,dt1.GradeComponentDescription -- Student data   
            ,dt1.CreditsAttempted
            ,dt1.CreditsEarned
            ,dt1.Completed
            ,dt1.CurrentScore
            ,dt1.CurrentGrade
            ,dt1.FinalScore
            ,dt1.FinalGrade
            ,dt1.CampusId
            ,dt1.CampDescrip
            ,dt1.rownumber
            ,dt1.FirstName
            ,dt1.LastName
            ,dt1.MiddleName
            ,dt1.GrdBkWgtDetailsCount
            ,dt1.ClockHourProgram
            ,dt1.GradesFormat
            ,dt1.GPAMethod
            ,dt1.WorkUnitCount
            ,dt1.WorkUnitCount_501
            ,dt1.WorkUnitCount_544
            ,dt1.WorkUnitCount_502
            ,dt1.WorkUnitCount_499
            ,dt1.WorkUnitCount_503
            ,dt1.WorkUnitCount_500
            ,dt1.WorkUnitCount_533
            ,dt1.rownumber2
    FROM     (
             SELECT dt.PrgVerId
                   ,dt.PrgVerDescrip
                   ,dt.PrgVersionTrackCredits
                   ,dt.TermId
                   ,dt.TermDescription
                   ,dt.TermStartDate
                   ,dt.TermEndDate
                   ,dt.CourseId
                   ,dt.CouseStartDate
                   ,dt.CourseCode
                   ,dt.CourseDescription
                   ,dt.CourseCodeDescription
                   ,dt.CourseCredits
                   ,dt.CourseFinAidCredits
                   ,dt.MinVal
                   ,dt.CourseScore
                   ,dt.GradeBook_ResultId
                   ,dt.GradeBookDescription
                   ,dt.GradeBookScore
                   ,dt.GradeBookPostDate
                   ,dt.GradeBookPassingGrade
                   ,dt.GradeBookWeight
                   ,dt.GradeBookRequired
                   ,dt.GradeBookMustPass
                   ,dt.GradeBookSysComponentTypeId
                   ,dt.GradeBookHoursRequired
                   ,dt.GradeBookHoursCompleted
                   ,dt.StuEnrollId
                   ,dt.MinResult
                   ,dt.GradeComponentDescription -- Student data   
                   ,dt.CreditsAttempted
                   ,dt.CreditsEarned
                   ,dt.Completed
                   ,dt.CurrentScore
                   ,dt.CurrentGrade
                   ,dt.FinalScore
                   ,dt.FinalGrade
                   ,dt.CampusId
                   ,dt.CampDescrip
                   ,dt.rownumber
                   ,dt.FirstName
                   ,dt.LastName
                   ,dt.MiddleName
                   ,dt.GrdBkWgtDetailsCount
                   ,dt.ClockHourProgram
                   ,@GradesFormat AS GradesFormat
                   ,@GPAMethod AS GPAMethod
                   ,(
                    SELECT SUM(WorkUnitCount)
                    FROM   #tempTermWorkUnitCount AS T
                    WHERE  T.TermId = dt.TermId
                           AND T.ReqId = dt.CourseId
                           AND sysComponentTypeId IN ( 501, 544, 502, 499, 503, 500, 533 )
                    ) AS WorkUnitCount
                   ,(
                    SELECT WorkUnitCount
                    FROM   #tempTermWorkUnitCount AS T
                    WHERE  TermId = dt.TermId
                           AND T.ReqId = dt.CourseId
                           AND sysComponentTypeId = 501
                    ) AS WorkUnitCount_501
                   ,(
                    SELECT WorkUnitCount
                    FROM   #tempTermWorkUnitCount AS T
                    WHERE  TermId = dt.TermId
                           AND T.ReqId = dt.CourseId
                           AND sysComponentTypeId = 544
                    ) AS WorkUnitCount_544
                   ,(
                    SELECT WorkUnitCount
                    FROM   #tempTermWorkUnitCount AS T
                    WHERE  TermId = dt.TermId
                           AND T.ReqId = dt.CourseId
                           AND sysComponentTypeId = 502
                    ) AS WorkUnitCount_502
                   ,(
                    SELECT WorkUnitCount
                    FROM   #tempTermWorkUnitCount AS T
                    WHERE  TermId = dt.TermId
                           AND T.ReqId = dt.CourseId
                           AND sysComponentTypeId = 499
                    ) AS WorkUnitCount_499
                   ,(
                    SELECT WorkUnitCount
                    FROM   #tempTermWorkUnitCount AS T
                    WHERE  TermId = dt.TermId
                           AND T.ReqId = dt.CourseId
                           AND sysComponentTypeId = 503
                    ) AS WorkUnitCount_503
                   ,(
                    SELECT WorkUnitCount
                    FROM   #tempTermWorkUnitCount AS T
                    WHERE  TermId = dt.TermId
                           AND T.ReqId = dt.CourseId
                           AND sysComponentTypeId = 500
                    ) AS WorkUnitCount_500
                   ,(
                    SELECT WorkUnitCount
                    FROM   #tempTermWorkUnitCount AS T
                    WHERE  TermId = dt.TermId
                           AND T.ReqId = dt.CourseId
                           AND sysComponentTypeId = 533
                    ) AS WorkUnitCount_533
                   ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId
                                                    ,TermId
                                                    ,CourseId
                                        ORDER BY TermStartDate
                                                ,TermEndDate
                                                ,TermDescription
                                                ,CourseDescription
                                      ) AS rownumber2
                   ,dt.ReqSeq
             FROM   (
                    SELECT     DISTINCT PV.PrgVerId AS PrgVerId
                                       ,PV.PrgVerDescrip AS PrgVerDescrip
                                       ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                             ELSE 0
                                        END AS PrgVersionTrackCredits
                                       ,T.TermId
                                       ,T.TermDescrip AS TermDescription
                                       ,T.StartDate AS TermStartDate
                                       ,T.EndDate AS TermEndDate
                                       ,CS.ReqId AS CourseId
                                       ,CS.StartDate AS CouseStartDate
                                       ,R.Code AS CourseCode
                                       ,R.Descrip AS CourseDescription
                                       ,''('' + R.Code + '') '' + R.Descrip AS CourseCodeDescription
                                       ,R.Credits AS CourseCredits
                                       ,R.FinAidCredits AS CourseFinAidCredits
                                       ,(
                                        SELECT MIN(MinVal)
                                        FROM   arGradeScaleDetails GCD
                                              ,arGradeSystemDetails GSD
                                        WHERE  GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                               AND GSD.IsPass = 1
                                               AND GCD.GrdScaleId = CS.GrdScaleId
                                        ) AS MinVal
                                       ,RES.Score AS CourseScore
                                       ,NULL AS GradeBook_ResultId
                                       ,NULL AS GradeBookDescription
                                       ,NULL AS GradeBookScore
                                       ,NULL AS GradeBookPostDate
                                       ,NULL AS GradeBookPassingGrade
                                       ,NULL AS GradeBookWeight
                                       ,NULL AS GradeBookRequired
                                       ,NULL AS GradeBookMustPass
                                       ,NULL AS GradeBookSysComponentTypeId
                                       ,NULL AS GradeBookHoursRequired
                                       ,NULL AS GradeBookHoursCompleted
                                       ,SE.StuEnrollId
                                       ,NULL AS MinResult
                                       ,NULL AS GradeComponentDescription -- Student data   
                                       ,ISNULL(SCS.CreditsAttempted, 0) AS CreditsAttempted
                                       ,ISNULL(SCS.CreditsEarned, 0) AS CreditsEarned
                                       ,SCS.Completed AS Completed
                                       ,SCS.CurrentScore AS CurrentScore
                                       ,SCS.CurrentGrade AS CurrentGrade
                                       ,SCS.FinalScore AS FinalScore
                                       ,SCS.FinalGrade AS FinalGrade
                                       ,C.CampusId
                                       ,C.CampDescrip
                                       ,NULL AS rownumber
                                       ,S.FirstName AS FirstName
                                       ,S.LastName AS LastName
                                       ,S.MiddleName
                                       ,(
                                        SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                        FROM   arGrdBkResults
                                        WHERE  StuEnrollId = SE.StuEnrollId
                                               AND ClsSectionId = RES.TestId
                                        ) AS GrdBkWgtDetailsCount
                                       ,CASE WHEN P.ACId = 5 THEN ''True''
                                             ELSE ''False''
                                        END AS ClockHourProgram
                                       ,PVD.ReqSeq
                    FROM       arClassSections CS
                    INNER JOIN arResults RES ON RES.TestId = CS.ClsSectionId
                    INNER JOIN arStuEnrollments SE ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arStudent S ON S.StudentId = SE.StudentId
                    INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                    INNER JOIN arTerm T ON CS.TermId = T.TermId
                    INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                    LEFT JOIN  dbo.arProgVerDef PVD ON PVD.PrgVerId = PV.PrgVerId
                                                       AND PVD.ReqId = R.ReqId
                    LEFT JOIN  syCreditSummary SCS ON SCS.StuEnrollId = SE.StuEnrollId
                                                      AND SCS.TermId = T.TermId
                                                      AND SCS.ReqId = R.ReqId
                                                      AND SCS.ClsSectionId = CS.ClsSectionId
                    WHERE      SE.StuEnrollId IN (
                                                 SELECT Val
                                                 FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                 )
                               AND (
                                   @TermId IS NULL
                                   OR T.TermId = @TermId
                                   )
                               --AND R.IsAttendanceOnly = 0
                               AND (
                                   RES.GrdSysDetailId IS NOT NULL
                                   OR RES.Score IS NOT NULL
                                   )
                               AND SCS.FinalGrade IS NOT NULL
                    UNION
                    SELECT     DISTINCT PV.PrgVerId AS PrgVerId
                                       ,PV.PrgVerDescrip AS PrgVerDescrip
                                       ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                             ELSE 0
                                        END AS PrgVersionTrackCredits
                                       ,T.TermId
                                       ,T.TermDescrip
                                       ,T.StartDate
                                       ,T.EndDate
                                       ,GBCR.ReqId
                                       ,T.StartDate AS CouseStartDate     -- tranfered
                                       ,R.Code AS CourseCode
                                       ,R.Descrip AS CourseDescrip
                                       ,''('' + R.Code + '') '' + R.Descrip AS CourseCodeDescrip
                                       ,R.Credits
                                       ,R.FinAidCredits
                                       ,(
                                        SELECT MIN(MinVal)
                                        FROM   arGradeScaleDetails GCD
                                              ,arGradeSystemDetails GSD
                                        WHERE  GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                               AND GSD.IsPass = 1
                                        )
                                       ,ISNULL(GBCR.Score, 0)
                                       ,NULL
                                       ,NULL
                                       ,NULL
                                       ,NULL
                                       ,NULL
                                       ,NULL
                                       ,NULL
                                       ,NULL
                                       ,NULL
                                       ,NULL
                                       ,NULL
                                       ,SE.StuEnrollId
                                       ,NULL AS MinResult
                                       ,NULL AS GradeComponentDescription -- Student data    
                                       ,ISNULL(SCS.CreditsAttempted, 0) AS CreditsAttempted
                                       ,ISNULL(SCS.CreditsEarned, 0) AS CreditsEarned
                                       ,SCS.Completed AS Completed
                                       ,SCS.CurrentScore AS CurrentScore
                                       ,SCS.CurrentGrade AS CurrentGrade
                                       ,SCS.FinalScore AS FinalScore
                                       ,SCS.FinalGrade AS FinalGrade
                                       ,C.CampusId
                                       ,C.CampDescrip
                                       ,NULL AS rownumber
                                       ,S.FirstName AS FirstName
                                       ,S.LastName AS LastName
                                       ,S.MiddleName
                                       ,(
                                        SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                        FROM   arGrdBkConversionResults
                                        WHERE  StuEnrollId = SE.StuEnrollId
                                               AND TermId = GBCR.TermId
                                               AND ReqId = GBCR.ReqId
                                        ) AS GrdBkWgtDetailsCount
                                       ,CASE WHEN P.ACId = 5 THEN ''True''
                                             ELSE ''False''
                                        END AS ClockHourProgram
                                       ,PVD.ReqSeq
                    FROM       arTransferGrades GBCR
                    INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arStudent S ON S.StudentId = SE.StudentId
                    INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                    INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                    INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                    LEFT JOIN  dbo.arProgVerDef PVD ON PVD.PrgVerId = PV.PrgVerId
                                                       AND PVD.ReqId = R.ReqId
                    --INNER JOIN arTransferGrades AR ON GBCR.StuEnrollId = AR.StuEnrollId
                    LEFT JOIN  syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                      AND T.TermId = SCS.TermId
                                                      AND R.ReqId = SCS.ReqId
                    WHERE      SE.StuEnrollId IN (
                                                 SELECT Val
                                                 FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                 )
                               AND (
                                   @TermId IS NULL
                                   OR T.TermId = @TermId
                                   )
                               --AND R.IsAttendanceOnly = 0
                               AND ( GBCR.GrdSysDetailId IS NOT NULL )
                               AND SCS.FinalGrade IS NOT NULL
                    ) dt
             ) dt1
    --WHERE rownumber2<=2
    ORDER BY TermStartDate
            ,TermEndDate
            -- , TermDescription
            ,CouseStartDate
            ,dt1.ReqSeq ASC
            ,CourseCode;
-- =========================================================================================================
-- END  --  Usp_TR_Sub10_Courses 
-- =========================================================================================================



'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[arScheduledHoursAdjustments]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_arScheduledHoursAdjustments_arStuEnrollments_StuEnrollId_StuEnrollId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[arScheduledHoursAdjustments]', 'U'))
ALTER TABLE [dbo].[arScheduledHoursAdjustments] ADD CONSTRAINT [FK_arScheduledHoursAdjustments_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END

