-- ===============================================================================================
-- START Consolidated Script Version 4.1SP2
-- Data Changes Zone
-- Please do not deploy your schema changes in this area
-- Please use SQL Prompt format before insert here please
-- Please run after Schema changes....
-- ===============================================================================================
--=================================================================================================
-- START - AD-15204 Storage Configuration page
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION fileStorageSettings;
BEGIN TRY
    DECLARE @fileStorageResourceId INT = 882;

    IF NOT EXISTS (
                  SELECT *
                  FROM   dbo.syFileConfigurationFeatures
                  )
        BEGIN
            INSERT INTO dbo.syFileConfigurationFeatures (
                                                        FeatureId
                                                       ,FeatureCode
                                                       ,FeatureDescription
                                                       ,StatusId
                                                        )
            VALUES ( 1, 'TC'                                -- FeatureCode - varchar(50)
                    ,'Time Clock'                           -- FeatureDescription - varchar(100)
                    ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                )
                  ,( 2, 'LI'                                -- FeatureCode - varchar(50)
                    ,'Lead Import'                          -- FeatureDescription - varchar(100)
                    ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                  )
                  ,( 3, 'DOC'                               -- FeatureCode - varchar(50)
                    ,'Documents'                            -- FeatureDescription - varchar(100)
                    ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                  )
                  ,( 4, 'PHOTO'                             -- FeatureCode - varchar(50)
                    ,'Photos'                               -- FeatureDescription - varchar(100)
                    ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                  )
                  ,( 5, 'R2T4'                              -- FeatureCode - varchar(50)
                    ,'R2T4'                                 -- FeatureDescription - varchar(100)
                    ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                  );
        END;


    IF NOT EXISTS (
                  SELECT *
                  FROM   dbo.syResources
                  WHERE  ResourceID = @fileStorageResourceId
                  )
        BEGIN


            INSERT INTO dbo.syResources (
                                        ResourceID
                                       ,Resource
                                       ,ResourceTypeID
                                       ,ResourceURL
                                       ,SummListId
                                       ,ChildTypeId
                                       ,ModDate
                                       ,ModUser
                                       ,AllowSchlReqFlds
                                       ,MRUTypeId
                                       ,UsedIn
                                       ,TblFldsId
                                       ,DisplayName
                                        )
            VALUES ( @fileStorageResourceId          -- ResourceID - smallint
                    ,'File Storage Configuration'    -- Resource - varchar(200)
                    ,4                               -- ResourceTypeID - tinyint
                    ,'~/SY/FileStorageSettings.aspx' -- ResourceURL - varchar(100)
                    ,NULL                            -- SummListId - smallint
                    ,NULL                            -- ChildTypeId - tinyint
                    ,GETDATE()                       -- ModDate - datetime
                    ,'sa'                            -- ModUser - varchar(50)
                    ,0                               -- AllowSchlReqFlds - bit
                    ,1                               -- MRUTypeId - smallint
                    ,207                             -- UsedIn - int
                    ,NULL                            -- TblFldsId - int
                    ,'File Storage Configuration'    -- DisplayName - varchar(200)
                );
        END;

    DECLARE @systemGeneralOptionsMenu INT = (
                                            SELECT TOP 1 MenuItemId
                                            FROM   dbo.syMenuItems
                                            WHERE  MenuName = 'General Options'
                                                   AND DisplayName = 'General Options'
                                                   AND ResourceId = 707
                                            );
    IF NOT EXISTS (
                  SELECT *
                  FROM   dbo.syMenuItems
                  WHERE  ResourceId = @fileStorageResourceId
                  )
        BEGIN


            INSERT INTO dbo.syMenuItems (
                                        MenuName
                                       ,DisplayName
                                       ,Url
                                       ,MenuItemTypeId
                                       ,ParentId
                                       ,DisplayOrder
                                       ,IsPopup
                                       ,ModDate
                                       ,ModUser
                                       ,IsActive
                                       ,ResourceId
                                       ,HierarchyId
                                       ,ModuleCode
                                       ,MRUType
                                       ,HideStatusBar
                                        )
            VALUES ( 'File Storage Configuration'   -- MenuName - varchar(250)
                    ,'File Storage Configuration'   -- DisplayName - varchar(250)
                    ,'/SY/FileStorageSettings.aspx' -- Url - nvarchar(250)
                    ,4                              -- MenuItemTypeId - smallint
                    ,@systemGeneralOptionsMenu      -- ParentId - int
                    ,170                            -- DisplayOrder - int
                    ,0                              -- IsPopup - bit
                    ,GETDATE()                      -- ModDate - datetime
                    ,'sa'                           -- ModUser - varchar(50)
                    ,1                              -- IsActive - bit
                    ,@fileStorageResourceId         -- ResourceId - smallint
                    ,NEWID()                        -- HierarchyId - uniqueidentifier
                    ,NULL                           -- ModuleCode - varchar(5)
                    ,NULL                           -- MRUType - int
                    ,NULL                           -- HideStatusBar - bit
                );

        END;

    IF NOT EXISTS (
                  SELECT *
                  FROM   dbo.syNavigationNodes
                  WHERE  ResourceId = @fileStorageResourceId
                  )
        BEGIN
            DECLARE @fileStorageHiearchyId UNIQUEIDENTIFIER = (
                                                              SELECT TOP 1 HierarchyId
                                                              FROM   dbo.syMenuItems
                                                              WHERE  DisplayName = 'File Storage Configuration'
                                                                     AND ResourceId = 882
                                                              );
            DECLARE @parentHiearchyId UNIQUEIDENTIFIER = (
                                                         SELECT TOP 1 HierarchyId
                                                         FROM   dbo.syMenuItems
                                                         WHERE  ResourceId = 707
                                                         );

            INSERT INTO dbo.syNavigationNodes (
                                              HierarchyId
                                             ,HierarchyIndex
                                             ,ResourceId
                                             ,ParentId
                                             ,ModUser
                                             ,ModDate
                                             ,IsPopupWindow
                                             ,IsShipped
                                              )
            VALUES ( @fileStorageHiearchyId -- HierarchyId - uniqueidentifier
                    ,3                      -- HierarchyIndex - smallint
                    ,@fileStorageResourceId -- ResourceId - smallint
                    ,@parentHiearchyId      -- ParentId - uniqueidentifier
                    ,'sa'                   -- ModUser - varchar(50)
                    ,GETDATE()              -- ModDate - datetime
                    ,0                      -- IsPopupWindow - bit
                    ,1                      -- IsShipped - bit
                );

        END;
END TRY
BEGIN CATCH
    SET @error = 1;
    --ROLLBACK TRANSACTION titleIvSap;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION fileStorageSettings;
        PRINT 'Failed to create file storage configuration setup.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION fileStorageSettings;
        PRINT 'Successful setup of file storage configuration.';
    END;
GO
--=================================================================================================
-- END - AD-15204 Storage Configuration page
--=================================================================================================
--=================================================================================================
-- START - AD-16709 script for updating existing records that have termId null in satransactions
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION termTransaction;
BEGIN TRY
    UPDATE     st
    SET        st.TermId = t.TermId
    FROM       dbo.arTerm t
    INNER JOIN dbo.arPrgVersions pv ON pv.PrgVerId = t.ProgramVersionId
    INNER JOIN dbo.arStuEnrollments e ON e.PrgVerId = pv.PrgVerId
    INNER JOIN dbo.saTransactions st ON st.StuEnrollId = e.StuEnrollId
    WHERE      st.TermId IS NULL;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION termTransaction;
        PRINT 'Failed to update termId in satransaction.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION termTransaction;
        PRINT 'Successful supdate of termID on satransaction.';
    END;
GO
--=================================================================================================
-- END - AD-16709 script for updating existing records that have termId null in satransactions
--=================================================================================================

--=================================================================================================
-- START - TECH Create default file storage settings
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION defaultFileStorage;
BEGIN TRY
    DECLARE @Rows INT;
    DECLARE @leadImportPath VARCHAR(200);
    DECLARE @timeClockPath VARCHAR(200);
    DECLARE @documentsPath VARCHAR(200);
    DECLARE @r2t4Path VARCHAR(200);
    DECLARE @campusId UNIQUEIDENTIFIER;
    DECLARE @campusGroupId UNIQUEIDENTIFIER;
    DECLARE @campusFileConfigurationId UNIQUEIDENTIFIER;

    DECLARE @campusesToProcess TABLE
        (
            CampusId UNIQUEIDENTIFIER
        );


    INSERT INTO @campusesToProcess (
                                   CampusId
                                   )
                SELECT CampusId
                FROM   dbo.syCampuses;



    WHILE EXISTS (
                 SELECT *
                 FROM   @campusesToProcess
                 )
        BEGIN

            SELECT TOP 1 @campusId = CampusId
            FROM   @campusesToProcess;


            SET @leadImportPath = (
                                  SELECT TOP 1 ILArchivePath
                                  FROM   dbo.syCampuses
                                  WHERE  CampusId = @campusId
                                  );
            SET @timeClockPath = (
                                 SELECT TOP 1 TCTargetPath
                                 FROM   dbo.syCampuses
                                 WHERE  CampusId = @campusId
                                 );
            SET @documentsPath = (
                                 SELECT     TOP 1 sv.Value
                                 FROM       dbo.syConfigAppSettings aps
                                 INNER JOIN dbo.syConfigAppSetValues sv ON sv.SettingId = aps.SettingId
                                 WHERE      aps.KeyName = 'CreateDocumentPath'
                                 );
            SET @r2t4Path = (
                            SELECT     TOP 1 sv.Value
                            FROM       dbo.syConfigAppSettings aps
                            INNER JOIN dbo.syConfigAppSetValues sv ON sv.SettingId = aps.SettingId
                            WHERE      aps.KeyName = 'R2T4'
                            );
            SET @campusGroupId = (
                                 SELECT     TOP 1 cgc.CampGrpId
                                 FROM       dbo.syCmpGrpCmps cgc
                                 INNER JOIN dbo.syCampGrps cg ON cg.CampGrpId = cgc.CampGrpId
                                 WHERE      cgc.CampusId = @campusId
                                            AND cg.IsAllCampusGrp = 0
                                 );

            IF ( @campusGroupId IS NOT NULL )
                BEGIN
                    IF NOT EXISTS (
                                  SELECT 1
                                  FROM   dbo.syCampusFileConfiguration
                                  WHERE  CampusGroupId = @campusGroupId
                                  )
                        BEGIN
                            INSERT INTO dbo.syCampusFileConfiguration (
                                                                      CampusFileConfigurationId
                                                                     ,CampusGroupId
                                                                     ,FileStorageType
                                                                     ,AppliesToAllFeatures
                                                                     ,UserName
                                                                     ,Password
                                                                     ,Path
                                                                     ,CloudKey
                                                                     ,ModUser
                                                                     ,ModDate
                                                                      )
                            VALUES ( NEWID(), @campusGroupId -- CampusGroupId - uniqueidentifier
                                    ,2                       -- FileStorageType - int
                                    ,0                       -- AppliesToAllFeatures - bit
                                    ,NULL                    -- UserName - nvarchar(100)
                                    ,NULL                    -- Password - nvarchar(100)
                                    ,NULL                    -- Path - nvarchar(150)
                                    ,NULL                    -- CloudKey - nvarchar(200)
                                    ,N'SA'                   -- ModUser - nvarchar(150)
                                    ,GETDATE()               -- ModDate - datetime
                                );

                        END;

                    SET @campusFileConfigurationId = (
                                                     SELECT TOP 1 CampusFileConfigurationId
                                                     FROM   dbo.syCampusFileConfiguration
                                                     WHERE  @campusGroupId = @campusGroupId
                                                     );

                    IF ( @campusFileConfigurationId IS NOT NULL )
                        BEGIN

                            IF (
                               @timeClockPath IS NOT NULL
                               AND @timeClockPath <> ''
                               )
                                BEGIN
                                    IF NOT EXISTS (
                                                  SELECT 1
                                                  FROM   dbo.syCustomFeatureFileConfiguration
                                                  WHERE  CampusConfigurationId = @campusFileConfigurationId
                                                         AND FeatureId = 1
                                                  )
                                        BEGIN
                                            INSERT INTO dbo.syCustomFeatureFileConfiguration (
                                                                                             CustomFeatureConfigurationId
                                                                                            ,FeatureId
                                                                                            ,CampusConfigurationId
                                                                                            ,FileStorageType
                                                                                            ,UserName
                                                                                            ,Password
                                                                                            ,Path
                                                                                            ,CloudKey
                                                                                            ,ModUser
                                                                                            ,ModDate
                                                                                             )
                                            VALUES ( NEWID()                    -- CustomFeatureConfigurationId - uniqueidentifier
                                                    ,1                          -- FeatureId - int
                                                    ,@campusFileConfigurationId -- CampusConfigurationId - uniqueidentifier
                                                    ,2                          -- FileStorageType - int
                                                    ,NULL                       -- UserName - nvarchar(100)
                                                    ,NULL                       -- Password - nvarchar(100)
                                                    ,@timeClockPath             -- Path - nvarchar(150)
                                                    ,NULL                       -- CloudKey - nvarchar(200)
                                                    ,N'SA'                      -- ModUser - nvarchar(150)
                                                    ,GETDATE()                  -- ModDate - datetime
                                                );
                                        END;
                                END;


                            IF (
                               @leadImportPath IS NOT NULL
                               AND @leadImportPath <> ''
                               )
                                BEGIN
                                    IF NOT EXISTS (
                                                  SELECT 1
                                                  FROM   dbo.syCustomFeatureFileConfiguration
                                                  WHERE  CampusConfigurationId = @campusFileConfigurationId
                                                         AND FeatureId = 2
                                                  )
                                        BEGIN
                                            INSERT INTO dbo.syCustomFeatureFileConfiguration (
                                                                                             CustomFeatureConfigurationId
                                                                                            ,FeatureId
                                                                                            ,CampusConfigurationId
                                                                                            ,FileStorageType
                                                                                            ,UserName
                                                                                            ,Password
                                                                                            ,Path
                                                                                            ,CloudKey
                                                                                            ,ModUser
                                                                                            ,ModDate
                                                                                             )
                                            VALUES ( NEWID()                    -- CustomFeatureConfigurationId - uniqueidentifier
                                                    ,2                          -- FeatureId - int
                                                    ,@campusFileConfigurationId -- CampusConfigurationId - uniqueidentifier
                                                    ,2                          -- FileStorageType - int
                                                    ,NULL                       -- UserName - nvarchar(100)
                                                    ,NULL                       -- Password - nvarchar(100)
                                                    ,@leadImportPath            -- Path - nvarchar(150)
                                                    ,NULL                       -- CloudKey - nvarchar(200)
                                                    ,N'SA'                      -- ModUser - nvarchar(150)
                                                    ,GETDATE()                  -- ModDate - datetime
                                                );
                                        END;
                                END;

                            IF (
                               @documentsPath IS NOT NULL
                               AND @documentsPath <> ''
                               )
                                BEGIN
                                    IF NOT EXISTS (
                                                  SELECT 1
                                                  FROM   dbo.syCustomFeatureFileConfiguration
                                                  WHERE  CampusConfigurationId = @campusFileConfigurationId
                                                         AND FeatureId = 3
                                                  )
                                        BEGIN
                                            INSERT INTO dbo.syCustomFeatureFileConfiguration (
                                                                                             CustomFeatureConfigurationId
                                                                                            ,FeatureId
                                                                                            ,CampusConfigurationId
                                                                                            ,FileStorageType
                                                                                            ,UserName
                                                                                            ,Password
                                                                                            ,Path
                                                                                            ,CloudKey
                                                                                            ,ModUser
                                                                                            ,ModDate
                                                                                             )
                                            VALUES ( NEWID()                    -- CustomFeatureConfigurationId - uniqueidentifier
                                                    ,3                          -- FeatureId - int
                                                    ,@campusFileConfigurationId -- CampusConfigurationId - uniqueidentifier
                                                    ,2                          -- FileStorageType - int
                                                    ,NULL                       -- UserName - nvarchar(100)
                                                    ,NULL                       -- Password - nvarchar(100)
                                                    ,@documentsPath             -- Path - nvarchar(150)
                                                    ,NULL                       -- CloudKey - nvarchar(200)
                                                    ,N'SA'                      -- ModUser - nvarchar(150)
                                                    ,GETDATE()                  -- ModDate - datetime
                                                );
                                        END;
                                END;

                            IF (
                               @r2t4Path IS NOT NULL
                               AND @r2t4Path <> ''
                               )
                                BEGIN
                                    IF NOT EXISTS (
                                                  SELECT 1
                                                  FROM   dbo.syCustomFeatureFileConfiguration
                                                  WHERE  CampusConfigurationId = @campusFileConfigurationId
                                                         AND FeatureId = 5
                                                  )
                                        BEGIN
                                            INSERT INTO dbo.syCustomFeatureFileConfiguration (
                                                                                             CustomFeatureConfigurationId
                                                                                            ,FeatureId
                                                                                            ,CampusConfigurationId
                                                                                            ,FileStorageType
                                                                                            ,UserName
                                                                                            ,Password
                                                                                            ,Path
                                                                                            ,CloudKey
                                                                                            ,ModUser
                                                                                            ,ModDate
                                                                                             )
                                            VALUES ( NEWID()                    -- CustomFeatureConfigurationId - uniqueidentifier
                                                    ,5                          -- FeatureId - int
                                                    ,@campusFileConfigurationId -- CampusConfigurationId - uniqueidentifier
                                                    ,2                          -- FileStorageType - int
                                                    ,NULL                       -- UserName - nvarchar(100)
                                                    ,NULL                       -- Password - nvarchar(100)
                                                    ,@r2t4Path                  -- Path - nvarchar(150)
                                                    ,NULL                       -- CloudKey - nvarchar(200)
                                                    ,N'SA'                      -- ModUser - nvarchar(150)
                                                    ,GETDATE()                  -- ModDate - datetime
                                                );
                                        END;
                                END;


                        END;

                END;
            DELETE @campusesToProcess
            WHERE CampusId = @campusId;

        END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION defaultFileStorage;
        PRINT 'Failed to create default file storage settings.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION defaultFileStorage;
        PRINT 'Default file storage settings created.';
    END;
GO
--=================================================================================================
-- END - TECH Create default file storage settings
--=================================================================================================
--=================================================================================================
-- START AD-16228 : Add Final Course Grade to Adhoc
--=================================================================================================

DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION FinalCourseGrade;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId = (
                 SELECT MAX(FldId)
                 FROM   syFields
                 ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId = (
                     SELECT FldTypeId
                     FROM   syFieldTypes
                     WHERE  FldType = 'Decimal'
                     );
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syFields
                  WHERE  FldName = 'FinalCourseGrade'
                  )
        BEGIN
            INSERT INTO syFields (
                                 FldId
                                ,FldName
                                ,FldTypeId
                                ,FldLen
                                ,DDLId
                                ,DerivedFld
                                ,SchlReq
                                ,LogChanges
                                ,Mask
                                 )
            VALUES ( @fldId             -- FldId - int
                    ,'FinalCourseGrade' -- FldName - varchar(200)
                    ,@fldTypeId         -- FldTypeId - int
                    ,10                 -- FldLen - int
                    ,NULL               -- DDLId - int
                    ,NULL               -- DerivedFld - bit
                    ,NULL               -- SchlReq - bit
                    ,NULL               -- LogChanges - bit
                    ,NULL               -- Mask - varchar(50)
                );
            DECLARE @categoryId INT;
            SET @categoryId = (
                              SELECT CategoryId
                              FROM   syFldCategories
                              WHERE  Descrip = 'Course'
                                     AND EntityId = 394
                              );
            DECLARE @TblFldsId INT;
            SET @TblFldsId = (
                             SELECT MAX(TblFldsId)
                             FROM   syTblFlds
                             ) + 1;
            DECLARE @tblId INT;
            SET @tblId = (
                         SELECT TblId
                         FROM   syTables
                         WHERE  TblName = 'arStuEnrollments'
                         );
            DECLARE @FldCapId INT;
            SET @FldCapId = (
                            SELECT MAX(FldCapId)
                            FROM   syFldCaptions
                            ) + 1;
            INSERT INTO syFldCaptions (
                                      FldCapId
                                     ,FldId
                                     ,LangId
                                     ,Caption
                                     ,FldDescrip
                                      )
            VALUES ( @FldCapId            -- FldCapId - int
                    ,@fldId               -- FldId - int
                    ,1                    -- LangId - tinyint
                    ,'Final Course Grade' -- Caption - varchar(100)
                    ,'Final Course Grade' -- FldDescrip - varchar(150)
                );
            INSERT INTO syTblFlds (
                                  TblFldsId
                                 ,TblId
                                 ,FldId
                                 ,CategoryId
                                 ,FKColDescrip
                                  )
            VALUES ( @TblFldsId  -- TblFldsId - int
                    ,@tblId      -- TblId - int
                    ,@fldId      -- FldId - int
                    ,@categoryId -- CategoryId - int
                    ,NULL        -- FKColDescrip - varchar(50)
                );
            INSERT INTO syFieldCalculation (
                                           FldId
                                          ,CalculationSql
                                           )
            VALUES ( @fldId                                                                                                                                           -- FldId - int
                    ,' isNull(dbo.CalculateStudentAverage(arStuEnrollments.StuEnrollId,NULL,NULL,arClassSections.ClsSectionId,NULL,NULL),0) AS [Final Course Grade] ' -- CalculationSql - varchar(8000)
                );
        END;
    ELSE
        BEGIN
            DECLARE @CoursefldId INT;
            SET @CoursefldId = (
                               SELECT FldId
                               FROM   syFields
                               WHERE  FldName = 'FinalCourseGrade'
                               );
            UPDATE syFieldCalculation
            SET    CalculationSql = ' isNull(dbo.CalculateStudentAverage(arStuEnrollments.StuEnrollId,NULL,NULL,arClassSections.ClsSectionId,NULL,NULL),0) AS [Final Course Grade] '
            WHERE  FldId = @CoursefldId;
        END;


END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION FinalCourseGrade;
        PRINT 'Failed to Add FinalCourseGrade to Adhoc Report';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION FinalCourseGrade;
        PRINT 'Added FinalCourseGrade to Adhoc Report';
    END;
GO
---
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION WrittenExamAvg;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId = (
                 SELECT MAX(FldId)
                 FROM   syFields
                 ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId = (
                     SELECT FldTypeId
                     FROM   syFieldTypes
                     WHERE  FldType = 'Decimal'
                     );
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syFields
                  WHERE  FldName = 'WrittenExamAvg'
                  )
        BEGIN
            INSERT INTO syFields (
                                 FldId
                                ,FldName
                                ,FldTypeId
                                ,FldLen
                                ,DDLId
                                ,DerivedFld
                                ,SchlReq
                                ,LogChanges
                                ,Mask
                                 )
            VALUES ( @fldId           -- FldId - int
                    ,'WrittenExamAvg' -- FldName - varchar(200)
                    ,@fldTypeId       -- FldTypeId - int
                    ,10               -- FldLen - int
                    ,NULL             -- DDLId - int
                    ,NULL             -- DerivedFld - bit
                    ,NULL             -- SchlReq - bit
                    ,NULL             -- LogChanges - bit
                    ,NULL             -- Mask - varchar(50)
                );
            DECLARE @categoryId INT;
            SET @categoryId = (
                              SELECT CategoryId
                              FROM   syFldCategories
                              WHERE  Descrip = 'Enrollment'
                                     AND EntityId = 394
                              );
            DECLARE @TblFldsId INT;
            SET @TblFldsId = (
                             SELECT MAX(TblFldsId)
                             FROM   syTblFlds
                             ) + 1;
            DECLARE @tblId INT;
            SET @tblId = (
                         SELECT TblId
                         FROM   syTables
                         WHERE  TblName = 'arStuEnrollments'
                         );
            DECLARE @FldCapId INT;
            SET @FldCapId = (
                            SELECT MAX(FldCapId)
                            FROM   syFldCaptions
                            ) + 1;
            INSERT INTO syFldCaptions (
                                      FldCapId
                                     ,FldId
                                     ,LangId
                                     ,Caption
                                     ,FldDescrip
                                      )
            VALUES ( @FldCapId              -- FldCapId - int
                    ,@fldId                 -- FldId - int
                    ,1                      -- LangId - tinyint
                    ,'Written Exam Average' -- Caption - varchar(100)
                    ,'Written Exam Average' -- FldDescrip - varchar(150)
                );
            INSERT INTO syTblFlds (
                                  TblFldsId
                                 ,TblId
                                 ,FldId
                                 ,CategoryId
                                 ,FKColDescrip
                                  )
            VALUES ( @TblFldsId  -- TblFldsId - int
                    ,@tblId      -- TblId - int
                    ,@fldId      -- FldId - int
                    ,@categoryId -- CategoryId - int
                    ,NULL        -- FKColDescrip - varchar(50)
                );
            INSERT INTO syFieldCalculation (
                                           FldId
                                          ,CalculationSql
                                           )
            VALUES ( @fldId                                                                                                                                            -- FldId - int
                    ,' ISNULL(dbo.CalculateStudentAverage(arStuEnrollments.StuEnrollId,NULL,NULL,arClassSections.ClsSectionId,NULL,501),0) AS [Written Exam Average] ' -- CalculationSql - varchar(8000)
                );
        END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION WrittenExamAvg;
        PRINT 'Failed to Add WrittenExamAvg to Adhoc Report';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION WrittenExamAvg;
        PRINT 'Added WrittenExamAvg to Adhoc Report';
    END;
GO
---

DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION PracticalExamAvg;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId = (
                 SELECT MAX(FldId)
                 FROM   syFields
                 ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId = (
                     SELECT FldTypeId
                     FROM   syFieldTypes
                     WHERE  FldType = 'Decimal'
                     );
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syFields
                  WHERE  FldName = 'PracticalExamAvg'
                  )
        BEGIN
            INSERT INTO syFields (
                                 FldId
                                ,FldName
                                ,FldTypeId
                                ,FldLen
                                ,DDLId
                                ,DerivedFld
                                ,SchlReq
                                ,LogChanges
                                ,Mask
                                 )
            VALUES ( @fldId             -- FldId - int
                    ,'PracticalExamAvg' -- FldName - varchar(200)
                    ,@fldTypeId         -- FldTypeId - int
                    ,10                 -- FldLen - int
                    ,NULL               -- DDLId - int
                    ,NULL               -- DerivedFld - bit
                    ,NULL               -- SchlReq - bit
                    ,NULL               -- LogChanges - bit
                    ,NULL               -- Mask - varchar(50)
                );
            DECLARE @categoryId INT;
            SET @categoryId = (
                              SELECT CategoryId
                              FROM   syFldCategories
                              WHERE  Descrip = 'Enrollment'
                                     AND EntityId = 394
                              );
            DECLARE @TblFldsId INT;
            SET @TblFldsId = (
                             SELECT MAX(TblFldsId)
                             FROM   syTblFlds
                             ) + 1;
            DECLARE @tblId INT;
            SET @tblId = (
                         SELECT TblId
                         FROM   syTables
                         WHERE  TblName = 'arStuEnrollments'
                         );
            DECLARE @FldCapId INT;
            SET @FldCapId = (
                            SELECT MAX(FldCapId)
                            FROM   syFldCaptions
                            ) + 1;
            INSERT INTO syFldCaptions (
                                      FldCapId
                                     ,FldId
                                     ,LangId
                                     ,Caption
                                     ,FldDescrip
                                      )
            VALUES ( @FldCapId                -- FldCapId - int
                    ,@fldId                   -- FldId - int
                    ,1                        -- LangId - tinyint
                    ,'Practical Exam Average' -- Caption - varchar(100)
                    ,'Practical Exam Average' -- FldDescrip - varchar(150)
                );
            INSERT INTO syTblFlds (
                                  TblFldsId
                                 ,TblId
                                 ,FldId
                                 ,CategoryId
                                 ,FKColDescrip
                                  )
            VALUES ( @TblFldsId  -- TblFldsId - int
                    ,@tblId      -- TblId - int
                    ,@fldId      -- FldId - int
                    ,@categoryId -- CategoryId - int
                    ,NULL        -- FKColDescrip - varchar(50)
                );
            INSERT INTO syFieldCalculation (
                                           FldId
                                          ,CalculationSql
                                           )
            VALUES ( @fldId                                                                                                                                              -- FldId - int
                    ,' ISNULL(dbo.CalculateStudentAverage(arStuEnrollments.StuEnrollId,NULL,NULL,arClassSections.ClsSectionId,NULL,533),0) AS [Practical Exam Average] ' -- CalculationSql - varchar(8000)
                );
        END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION PracticalExamAvg;
        PRINT 'Failed to Add PracticalExamAvg to Adhoc Report';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION PracticalExamAvg;
        PRINT 'Added PracticalExamAvg to Adhoc Report';
    END;
GO
-----------

DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION HomeworkAvg;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId = (
                 SELECT MAX(FldId)
                 FROM   syFields
                 ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId = (
                     SELECT FldTypeId
                     FROM   syFieldTypes
                     WHERE  FldType = 'Decimal'
                     );
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syFields
                  WHERE  FldName = 'HomeworkAvg'
                  )
        BEGIN
            INSERT INTO syFields (
                                 FldId
                                ,FldName
                                ,FldTypeId
                                ,FldLen
                                ,DDLId
                                ,DerivedFld
                                ,SchlReq
                                ,LogChanges
                                ,Mask
                                 )
            VALUES ( @fldId        -- FldId - int
                    ,'HomeworkAvg' -- FldName - varchar(200)
                    ,@fldTypeId    -- FldTypeId - int
                    ,10            -- FldLen - int
                    ,NULL          -- DDLId - int
                    ,NULL          -- DerivedFld - bit
                    ,NULL          -- SchlReq - bit
                    ,NULL          -- LogChanges - bit
                    ,NULL          -- Mask - varchar(50)
                );
            DECLARE @categoryId INT;
            SET @categoryId = (
                              SELECT CategoryId
                              FROM   syFldCategories
                              WHERE  Descrip = 'Enrollment'
                                     AND EntityId = 394
                              );
            DECLARE @TblFldsId INT;
            SET @TblFldsId = (
                             SELECT MAX(TblFldsId)
                             FROM   syTblFlds
                             ) + 1;
            DECLARE @tblId INT;
            SET @tblId = (
                         SELECT TblId
                         FROM   syTables
                         WHERE  TblName = 'arStuEnrollments'
                         );
            DECLARE @FldCapId INT;
            SET @FldCapId = (
                            SELECT MAX(FldCapId)
                            FROM   syFldCaptions
                            ) + 1;
            INSERT INTO syFldCaptions (
                                      FldCapId
                                     ,FldId
                                     ,LangId
                                     ,Caption
                                     ,FldDescrip
                                      )
            VALUES ( @FldCapId          -- FldCapId - int
                    ,@fldId             -- FldId - int
                    ,1                  -- LangId - tinyint
                    ,'Homework Average' -- Caption - varchar(100)
                    ,'Homework Average' -- FldDescrip - varchar(150)
                );
            INSERT INTO syTblFlds (
                                  TblFldsId
                                 ,TblId
                                 ,FldId
                                 ,CategoryId
                                 ,FKColDescrip
                                  )
            VALUES ( @TblFldsId  -- TblFldsId - int
                    ,@tblId      -- TblId - int
                    ,@fldId      -- FldId - int
                    ,@categoryId -- CategoryId - int
                    ,NULL        -- FKColDescrip - varchar(50)
                );
            INSERT INTO syFieldCalculation (
                                           FldId
                                          ,CalculationSql
                                           )
            VALUES ( @fldId                                                                                                                                        -- FldId - int
                    ,' ISNULL(dbo.CalculateStudentAverage(arStuEnrollments.StuEnrollId,NULL,NULL,arClassSections.ClsSectionId,NULL,499),0) AS [Homework Average] ' -- CalculationSql - varchar(8000)
                );
        END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION HomeworkAvg;
        PRINT 'Failed to Add HomeworkAvg to Adhoc Report';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION HomeworkAvg;
        PRINT 'Added HomeworkAvg to Adhoc Report';
    END;
GO
---------------
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION LabworkAvg;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId = (
                 SELECT MAX(FldId)
                 FROM   syFields
                 ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId = (
                     SELECT FldTypeId
                     FROM   syFieldTypes
                     WHERE  FldType = 'Decimal'
                     );
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syFields
                  WHERE  FldName = 'LabworkAvg'
                  )
        BEGIN
            INSERT INTO syFields (
                                 FldId
                                ,FldName
                                ,FldTypeId
                                ,FldLen
                                ,DDLId
                                ,DerivedFld
                                ,SchlReq
                                ,LogChanges
                                ,Mask
                                 )
            VALUES ( @fldId       -- FldId - int
                    ,'LabworkAvg' -- FldName - varchar(200)
                    ,@fldTypeId   -- FldTypeId - int
                    ,10           -- FldLen - int
                    ,NULL         -- DDLId - int
                    ,NULL         -- DerivedFld - bit
                    ,NULL         -- SchlReq - bit
                    ,NULL         -- LogChanges - bit
                    ,NULL         -- Mask - varchar(50)
                );
            DECLARE @categoryId INT;
            SET @categoryId = (
                              SELECT CategoryId
                              FROM   syFldCategories
                              WHERE  Descrip = 'Enrollment'
                                     AND EntityId = 394
                              );
            DECLARE @TblFldsId INT;
            SET @TblFldsId = (
                             SELECT MAX(TblFldsId)
                             FROM   syTblFlds
                             ) + 1;
            DECLARE @tblId INT;
            SET @tblId = (
                         SELECT TblId
                         FROM   syTables
                         WHERE  TblName = 'arStuEnrollments'
                         );
            DECLARE @FldCapId INT;
            SET @FldCapId = (
                            SELECT MAX(FldCapId)
                            FROM   syFldCaptions
                            ) + 1;
            INSERT INTO syFldCaptions (
                                      FldCapId
                                     ,FldId
                                     ,LangId
                                     ,Caption
                                     ,FldDescrip
                                      )
            VALUES ( @FldCapId         -- FldCapId - int
                    ,@fldId            -- FldId - int
                    ,1                 -- LangId - tinyint
                    ,'Labwork Average' -- Caption - varchar(100)
                    ,'Labwork Average' -- FldDescrip - varchar(150)
                );
            INSERT INTO syTblFlds (
                                  TblFldsId
                                 ,TblId
                                 ,FldId
                                 ,CategoryId
                                 ,FKColDescrip
                                  )
            VALUES ( @TblFldsId  -- TblFldsId - int
                    ,@tblId      -- TblId - int
                    ,@fldId      -- FldId - int
                    ,@categoryId -- CategoryId - int
                    ,NULL        -- FKColDescrip - varchar(50)
                );
            INSERT INTO syFieldCalculation (
                                           FldId
                                          ,CalculationSql
                                           )
            VALUES ( @fldId                                                                                                                                       -- FldId - int
                    ,' ISNULL(dbo.CalculateStudentAverage(arStuEnrollments.StuEnrollId,NULL,NULL,arClassSections.ClsSectionId,NULL,500),0) AS [Labwork Average] ' -- CalculationSql - varchar(8000)
                );
        END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION LabworkAvg;
        PRINT 'Failed to Add LabworkAvg to Adhoc Report';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION LabworkAvg;
        PRINT 'Added LabworkAvg to Adhoc Report';
    END;
GO
---------------
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION FinalAvg;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId = (
                 SELECT MAX(FldId)
                 FROM   syFields
                 ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId = (
                     SELECT FldTypeId
                     FROM   syFieldTypes
                     WHERE  FldType = 'Decimal'
                     );
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syFields
                  WHERE  FldName = 'FinalAvg'
                  )
        BEGIN
            INSERT INTO syFields (
                                 FldId
                                ,FldName
                                ,FldTypeId
                                ,FldLen
                                ,DDLId
                                ,DerivedFld
                                ,SchlReq
                                ,LogChanges
                                ,Mask
                                 )
            VALUES ( @fldId     -- FldId - int
                    ,'FinalAvg' -- FldName - varchar(200)
                    ,@fldTypeId -- FldTypeId - int
                    ,10         -- FldLen - int
                    ,NULL       -- DDLId - int
                    ,NULL       -- DerivedFld - bit
                    ,NULL       -- SchlReq - bit
                    ,NULL       -- LogChanges - bit
                    ,NULL       -- Mask - varchar(50)
                );
            DECLARE @categoryId INT;
            SET @categoryId = (
                              SELECT CategoryId
                              FROM   syFldCategories
                              WHERE  Descrip = 'Enrollment'
                                     AND EntityId = 394
                              );
            DECLARE @TblFldsId INT;
            SET @TblFldsId = (
                             SELECT MAX(TblFldsId)
                             FROM   syTblFlds
                             ) + 1;
            DECLARE @tblId INT;
            SET @tblId = (
                         SELECT TblId
                         FROM   syTables
                         WHERE  TblName = 'arStuEnrollments'
                         );
            DECLARE @FldCapId INT;
            SET @FldCapId = (
                            SELECT MAX(FldCapId)
                            FROM   syFldCaptions
                            ) + 1;
            INSERT INTO syFldCaptions (
                                      FldCapId
                                     ,FldId
                                     ,LangId
                                     ,Caption
                                     ,FldDescrip
                                      )
            VALUES ( @FldCapId       -- FldCapId - int
                    ,@fldId          -- FldId - int
                    ,1               -- LangId - tinyint
                    ,'Final Average' -- Caption - varchar(100)
                    ,'Final Average' -- FldDescrip - varchar(150)
                );
            INSERT INTO syTblFlds (
                                  TblFldsId
                                 ,TblId
                                 ,FldId
                                 ,CategoryId
                                 ,FKColDescrip
                                  )
            VALUES ( @TblFldsId  -- TblFldsId - int
                    ,@tblId      -- TblId - int
                    ,@fldId      -- FldId - int
                    ,@categoryId -- CategoryId - int
                    ,NULL        -- FKColDescrip - varchar(50)
                );
            INSERT INTO syFieldCalculation (
                                           FldId
                                          ,CalculationSql
                                           )
            VALUES ( @fldId                                                                                                                                     -- FldId - int
                    ,' ISNULL(dbo.CalculateStudentAverage(arStuEnrollments.StuEnrollId,NULL,NULL,arClassSections.ClsSectionId,NULL,502),0) AS [Final Average] ' -- CalculationSql - varchar(8000)
                );
        END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION FinalAvg;
        PRINT 'Failed to Add FinalAvg to Adhoc Report';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION FinalAvg;
        PRINT 'Added FinalAvg to Adhoc Report';
    END;
GO
---------------
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION LabHrAvg;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId = (
                 SELECT MAX(FldId)
                 FROM   syFields
                 ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId = (
                     SELECT FldTypeId
                     FROM   syFieldTypes
                     WHERE  FldType = 'Decimal'
                     );
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syFields
                  WHERE  FldName = 'LabHourAvg'
                  )
        BEGIN
            INSERT INTO syFields (
                                 FldId
                                ,FldName
                                ,FldTypeId
                                ,FldLen
                                ,DDLId
                                ,DerivedFld
                                ,SchlReq
                                ,LogChanges
                                ,Mask
                                 )
            VALUES ( @fldId       -- FldId - int
                    ,'LabHourAvg' -- FldName - varchar(200)
                    ,@fldTypeId   -- FldTypeId - int
                    ,10           -- FldLen - int
                    ,NULL         -- DDLId - int
                    ,NULL         -- DerivedFld - bit
                    ,NULL         -- SchlReq - bit
                    ,NULL         -- LogChanges - bit
                    ,NULL         -- Mask - varchar(50)
                );
            DECLARE @categoryId INT;
            SET @categoryId = (
                              SELECT CategoryId
                              FROM   syFldCategories
                              WHERE  Descrip = 'Enrollment'
                                     AND EntityId = 394
                              );
            DECLARE @TblFldsId INT;
            SET @TblFldsId = (
                             SELECT MAX(TblFldsId)
                             FROM   syTblFlds
                             ) + 1;
            DECLARE @tblId INT;
            SET @tblId = (
                         SELECT TblId
                         FROM   syTables
                         WHERE  TblName = 'arStuEnrollments'
                         );
            DECLARE @FldCapId INT;
            SET @FldCapId = (
                            SELECT MAX(FldCapId)
                            FROM   syFldCaptions
                            ) + 1;
            INSERT INTO syFldCaptions (
                                      FldCapId
                                     ,FldId
                                     ,LangId
                                     ,Caption
                                     ,FldDescrip
                                      )
            VALUES ( @FldCapId          -- FldCapId - int
                    ,@fldId             -- FldId - int
                    ,1                  -- LangId - tinyint
                    ,'Lab Hour Average' -- Caption - varchar(100)
                    ,'Lab Hour Average' -- FldDescrip - varchar(150)
                );
            INSERT INTO syTblFlds (
                                  TblFldsId
                                 ,TblId
                                 ,FldId
                                 ,CategoryId
                                 ,FKColDescrip
                                  )
            VALUES ( @TblFldsId  -- TblFldsId - int
                    ,@tblId      -- TblId - int
                    ,@fldId      -- FldId - int
                    ,@categoryId -- CategoryId - int
                    ,NULL        -- FKColDescrip - varchar(50)
                );
            INSERT INTO syFieldCalculation (
                                           FldId
                                          ,CalculationSql
                                           )
            VALUES ( @fldId                                                                                                                                        -- FldId - int
                    ,' ISNULL(dbo.CalculateStudentAverage(arStuEnrollments.StuEnrollId,NULL,NULL,arClassSections.ClsSectionId,NULL,503),0) AS [Lab Hour Average] ' -- CalculationSql - varchar(8000)
                );
        END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION LabHrAvg;
        PRINT 'Failed to Add LabHourAvg to Adhoc Report';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION LabHrAvg;
        PRINT 'Added LabHourAvg to Adhoc Report';
    END;
GO
---------------

DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION ExternshipAvg;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId = (
                 SELECT MAX(FldId)
                 FROM   syFields
                 ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId = (
                     SELECT FldTypeId
                     FROM   syFieldTypes
                     WHERE  FldType = 'Decimal'
                     );
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syFields
                  WHERE  FldName = 'ExternshipAvg'
                  )
        BEGIN
            INSERT INTO syFields (
                                 FldId
                                ,FldName
                                ,FldTypeId
                                ,FldLen
                                ,DDLId
                                ,DerivedFld
                                ,SchlReq
                                ,LogChanges
                                ,Mask
                                 )
            VALUES ( @fldId          -- FldId - int
                    ,'ExternshipAvg' -- FldName - varchar(200)
                    ,@fldTypeId      -- FldTypeId - int
                    ,10              -- FldLen - int
                    ,NULL            -- DDLId - int
                    ,NULL            -- DerivedFld - bit
                    ,NULL            -- SchlReq - bit
                    ,NULL            -- LogChanges - bit
                    ,NULL            -- Mask - varchar(50)
                );
            DECLARE @categoryId INT;
            SET @categoryId = (
                              SELECT CategoryId
                              FROM   syFldCategories
                              WHERE  Descrip = 'Enrollment'
                                     AND EntityId = 394
                              );
            DECLARE @TblFldsId INT;
            SET @TblFldsId = (
                             SELECT MAX(TblFldsId)
                             FROM   syTblFlds
                             ) + 1;
            DECLARE @tblId INT;
            SET @tblId = (
                         SELECT TblId
                         FROM   syTables
                         WHERE  TblName = 'arStuEnrollments'
                         );
            DECLARE @FldCapId INT;
            SET @FldCapId = (
                            SELECT MAX(FldCapId)
                            FROM   syFldCaptions
                            ) + 1;
            INSERT INTO syFldCaptions (
                                      FldCapId
                                     ,FldId
                                     ,LangId
                                     ,Caption
                                     ,FldDescrip
                                      )
            VALUES ( @FldCapId            -- FldCapId - int
                    ,@fldId               -- FldId - int
                    ,1                    -- LangId - tinyint
                    ,'Externship Average' -- Caption - varchar(100)
                    ,'Externship Average' -- FldDescrip - varchar(150)
                );
            INSERT INTO syTblFlds (
                                  TblFldsId
                                 ,TblId
                                 ,FldId
                                 ,CategoryId
                                 ,FKColDescrip
                                  )
            VALUES ( @TblFldsId  -- TblFldsId - int
                    ,@tblId      -- TblId - int
                    ,@fldId      -- FldId - int
                    ,@categoryId -- CategoryId - int
                    ,NULL        -- FKColDescrip - varchar(50)
                );
            INSERT INTO syFieldCalculation (
                                           FldId
                                          ,CalculationSql
                                           )
            VALUES ( @fldId                                                                                                                                          -- FldId - int
                    ,' ISNULL(dbo.CalculateStudentAverage(arStuEnrollments.StuEnrollId,NULL,NULL,arClassSections.ClsSectionId,NULL,544),0) AS [Externship Average] ' -- CalculationSql - varchar(8000)
                );
        END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION ExternshipAvg;
        PRINT 'Failed to Add ExternshipAvg to Adhoc Report';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION ExternshipAvg;
        PRINT 'Added ExternshipAvg to Adhoc Report';
    END;
GO
---------------

DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION DictationSpeedAvg;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId = (
                 SELECT MAX(FldId)
                 FROM   syFields
                 ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId = (
                     SELECT FldTypeId
                     FROM   syFieldTypes
                     WHERE  FldType = 'Decimal'
                     );
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syFields
                  WHERE  FldName = 'DictationSpeedAvg'
                  )
        BEGIN
            INSERT INTO syFields (
                                 FldId
                                ,FldName
                                ,FldTypeId
                                ,FldLen
                                ,DDLId
                                ,DerivedFld
                                ,SchlReq
                                ,LogChanges
                                ,Mask
                                 )
            VALUES ( @fldId              -- FldId - int
                    ,'DictationSpeedAvg' -- FldName - varchar(200)
                    ,@fldTypeId          -- FldTypeId - int
                    ,10                  -- FldLen - int
                    ,NULL                -- DDLId - int
                    ,NULL                -- DerivedFld - bit
                    ,NULL                -- SchlReq - bit
                    ,NULL                -- LogChanges - bit
                    ,NULL                -- Mask - varchar(50)
                );
            DECLARE @categoryId INT;
            SET @categoryId = (
                              SELECT CategoryId
                              FROM   syFldCategories
                              WHERE  Descrip = 'Enrollment'
                                     AND EntityId = 394
                              );
            DECLARE @TblFldsId INT;
            SET @TblFldsId = (
                             SELECT MAX(TblFldsId)
                             FROM   syTblFlds
                             ) + 1;
            DECLARE @tblId INT;
            SET @tblId = (
                         SELECT TblId
                         FROM   syTables
                         WHERE  TblName = 'arStuEnrollments'
                         );
            DECLARE @FldCapId INT;
            SET @FldCapId = (
                            SELECT MAX(FldCapId)
                            FROM   syFldCaptions
                            ) + 1;
            INSERT INTO syFldCaptions (
                                      FldCapId
                                     ,FldId
                                     ,LangId
                                     ,Caption
                                     ,FldDescrip
                                      )
            VALUES ( @FldCapId                      -- FldCapId - int
                    ,@fldId                         -- FldId - int
                    ,1                              -- LangId - tinyint
                    ,'Dictation Speed Test Average' -- Caption - varchar(100)
                    ,'Dictation Speed Test Average' -- FldDescrip - varchar(150)
                );
            INSERT INTO syTblFlds (
                                  TblFldsId
                                 ,TblId
                                 ,FldId
                                 ,CategoryId
                                 ,FKColDescrip
                                  )
            VALUES ( @TblFldsId  -- TblFldsId - int
                    ,@tblId      -- TblId - int
                    ,@fldId      -- FldId - int
                    ,@categoryId -- CategoryId - int
                    ,NULL        -- FKColDescrip - varchar(50)
                );
            INSERT INTO syFieldCalculation (
                                           FldId
                                          ,CalculationSql
                                           )
            VALUES ( @fldId                                                                                                                                                    -- FldId - int
                    ,' ISNULL(dbo.CalculateStudentAverage(arStuEnrollments.StuEnrollId,NULL,NULL,arClassSections.ClsSectionId,NULL,612),0) AS [Dictation Speed Test Average] ' -- CalculationSql - varchar(8000)
                );
        END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION LabHrAvg;
        PRINT 'Failed to Add LabHourAvg to Adhoc Report';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION LabHrAvg;
        PRINT 'Added LabHourAvg to Adhoc Report';
    END;
GO
---------------
--=================================================================================================
-- END  AD-16228 : Add Final Course Grade to Adhoc
--=================================================================================================
--=================================================================================================
-- START AD-16460 : Set Ar Result Scores to Zero for Future Start students
--=================================================================================================

DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION ArResultZeroScores;
BEGIN TRY
    DECLARE @status INT;
    SET @status = (
                  SELECT SysStatusId
                  FROM   dbo.sySysStatus
                  WHERE  SysStatusDescrip = 'Future Start'
                  );

    IF ( @status > 0 )
        BEGIN
            UPDATE     ar
            SET        ar.Score = NULL
            FROM       dbo.arStuEnrollments enrollments
            JOIN       dbo.arStudent art ON art.StudentId = enrollments.StudentId
            INNER JOIN dbo.syStatusCodes statuses ON statuses.StatusCodeId = enrollments.StatusCodeId
            INNER JOIN dbo.sySysStatus advStatuses ON advStatuses.SysStatusId = statuses.SysStatusId
            JOIN       dbo.arResults ar ON ar.StuEnrollId = enrollments.StuEnrollId
            JOIN       dbo.arGrdBkResults gbr ON gbr.StuEnrollId = enrollments.StuEnrollId
            WHERE      ar.Score = 0
                       AND advStatuses.SysStatusId = @status
                       AND gbr.Score IS NULL
                       AND gbr.PostDate IS NULL;

        END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION ArResultZeroScores;
        PRINT 'Failed to Set Ar Result Scores to Zero for Future Start students';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION ArResultZeroScores;
        PRINT 'Set Ar Result Scores to Zero for Future Start students';
    END;
GO
--=================================================================================================
-- END  AD-16460 : Tricoci - Cannot No Start Student
--=================================================================================================
--=================================================================================================
-- START AD-16516 TimeClock Automation
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION AD16516;
BEGIN TRY

    --update file configuration features
    IF EXISTS (
              SELECT *
              FROM   dbo.syFileConfigurationFeatures
              WHERE  FeatureCode = 'TC'
              )
        BEGIN
            UPDATE dbo.syFileConfigurationFeatures
            SET    FeatureCode = 'TCA'
                  ,FeatureDescription = 'Time Clock Archive'
            WHERE  FeatureCode = 'TC';
        END;

    IF NOT EXISTS (
                  SELECT *
                  FROM   dbo.syFileConfigurationFeatures
                  WHERE  FeatureCode = 'TCI'
                  )
        BEGIN
            INSERT INTO dbo.syFileConfigurationFeatures (
                                                        FeatureId
                                                       ,FeatureCode
                                                       ,FeatureDescription
                                                       ,StatusId
                                                        )
            VALUES ( 6, 'TCI'                               -- FeatureCode - varchar(50)
                    ,'Time Clock Import'                    -- FeatureDescription - varchar(100)
                    ,'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965' -- StatusId - uniqueidentifier
                );
        END;

    --insert new config setting to turn on automatic time clock imports per campus 
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syConfigAppSettings
                  WHERE  KeyName = 'AutomatedTimeClockImport'
                  )
        BEGIN
            DECLARE @settingId INT;
            DECLARE @modDate DATETIME;
            SET @modDate = GETDATE();

            SET @settingId = (
                             SELECT MAX(SettingId) + 1
                             FROM   dbo.syConfigAppSetValues
                             );

            SET IDENTITY_INSERT dbo.syConfigAppSettings ON;

            INSERT INTO syConfigAppSettings (
                                            KeyName
                                           ,Description
                                           ,ModUser
                                           ,ModDate
                                           ,CampusSpecific
                                           ,ExtraConfirmation
                                           ,SettingId
                                            )
            VALUES ( 'AutomatedTimeClockImport'                                             -- KeyName - varchar(200)
                    ,'Yes/No campus specific value to enable automatic time clock imports.' -- Description - varchar(1000)
                    ,'Support'                                                              -- ModUser - varchar(50)
                    ,@modDate                                                               -- ModDate - datetime
                    ,1                                                                      -- CampusSpecific - bit
                    ,0, @settingId                                                          -- ExtraConfirmation - bit
                );

            SET IDENTITY_INSERT dbo.syConfigAppSettings OFF;

            SET @settingId = (
                             SELECT SettingId
                             FROM   syConfigAppSettings
                             WHERE  KeyName = 'AutomatedTimeClockImport'
                             );

            INSERT INTO syConfigAppSet_Lookup (
                                              LookUpId
                                             ,SettingId
                                             ,ValueOptions
                                             ,ModUser
                                             ,ModDate
                                              )
            VALUES ( NEWID()    -- LookUpId - uniqueidentifier
                    ,@settingId -- SettingId - int
                    ,'No'       -- ValueOptions - varchar(50)
                    ,'Support'  -- ModUser - varchar(50)
                    ,@modDate   -- ModDate - datetime
                );
            INSERT INTO syConfigAppSet_Lookup (
                                              LookUpId
                                             ,SettingId
                                             ,ValueOptions
                                             ,ModUser
                                             ,ModDate
                                              )
            VALUES ( NEWID()    -- LookUpId - uniqueidentifier
                    ,@settingId -- SettingId - int
                    ,'Yes'      -- ValueOptions - varchar(50)
                    ,'Support'  -- ModUser - varchar(50)
                    ,@modDate   -- ModDate - datetime
                );
            INSERT INTO syConfigAppSetValues (
                                             ValueId
                                            ,SettingId
                                            ,CampusId
                                            ,Value
                                            ,ModUser
                                            ,ModDate
                                            ,Active
                                             )
            VALUES ( NEWID()    -- ValueId - uniqueidentifier
                    ,@settingId -- SettingId - int
                    ,NULL       -- CampusId - uniqueidentifier
                    ,'No'       -- Value - varchar(1000)
                    ,'Support'  -- ModUser - varchar(50)
                    ,@modDate   -- ModDate - datetime
                    ,1          -- Active - bit
                );
        END;

    --insert new service settings

    IF ( NOT EXISTS (
                    SELECT *
                    FROM   dbo.syWapiExternalOperationMode
                    WHERE  Code = 'TC_FILEWATCH_SERVICE'
                    )
       )
        BEGIN
            INSERT INTO dbo.syWapiExternalOperationMode (
                                                        Code
                                                       ,Description
                                                       ,IsActive
                                                        )
            VALUES ( 'TC_FILEWATCH_SERVICE', 'This operation is for filewatching automated time clock imports paths', 1 );
        END;

    DECLARE @ExternalOperationId INT = (
                                       SELECT Id
                                       FROM   dbo.syWapiExternalOperationMode
                                       WHERE  Code = 'TC_FILEWATCH_SERVICE'
                                       );

    --reuse advantage api 
    DECLARE @AllowedServiceId INT = (
                                    SELECT Id
                                    FROM   dbo.syWapiAllowedServices
                                    WHERE  Code = 'ADVANTAGE_API'
                                    );

    DECLARE @ExternalCompanyId INT = (
                                     SELECT IdExtCompany
                                     FROM   dbo.syWapiSettings
                                     WHERE  CodeOperation = 'ADVANTAGE_API'
                                     );

    IF ( NOT EXISTS (
                    SELECT *
                    FROM   dbo.syWapiSettings
                    WHERE  CodeOperation = 'TC_FILEWATCH_SERVICE'
                    )
       )
        BEGIN
            INSERT INTO dbo.syWapiSettings (
                                           CodeOperation
                                          ,ExternalUrl
                                          ,IdExtCompany
                                          ,IdExtOperation
                                          ,IdAllowedService
                                          ,IdSecondAllowedService
                                          ,FirstAllowedServiceQueryString
                                          ,SecondAllowedServiceQueryString
                                          ,ConsumerKey
                                          ,PrivateKey
                                          ,OperationSecondTimeInterval
                                          ,PollSecondForOnDemandOperation
                                          ,FlagOnDemandOperation
                                          ,FlagRefreshConfiguration
                                          ,IsActive
                                          ,DateMod
                                          ,DateLastExecution
                                          ,UserMod
                                          ,UserName
                                           )
            VALUES ( 'TC_FILEWATCH_SERVICE', 'http://localhost/Advantage/Current/Site/ServiceMethods.aspx/', @ExternalCompanyId, @ExternalOperationId
                    ,@AllowedServiceId, NULL, NULL, '', '', '', 36000000, 100000, 0, 0, 0, GETDATE(), N'2019-04-11T10:14:45', 'SUPPORT', '' );
        END;


END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION AD16516;
        PRINT 'Failed transaction AD16516.';
    END;
ELSE
    BEGIN
        COMMIT TRANSACTION AD16516;
        PRINT 'successful transaction AD16516.';
    END;
GO
--=================================================================================================
-- END AD-16516 TimeClock Automation
--=================================================================================================
--=================================================================================================
-- START AD-16904 Multi Column Progress Report
-- This also updates the sequence numbers for school that does not have set them up, if they already
-- have a sequence it will keep them in current sequence
--=================================================================================================
DECLARE @ErrorAddingSettingMultiColumnPR AS INT;
SET @ErrorAddingSettingMultiColumnPR = 0;
BEGIN TRANSACTION addSettingMulticolProgressReport;
BEGIN TRY
    DECLARE @settingKeyName VARCHAR(MAX) = 'EnableMultiColumnProgressReport';
    IF NOT EXISTS (
                  SELECT 1
                  FROM   dbo.syConfigAppSettings
                  WHERE  KeyName = @settingKeyName
                  )
        BEGIN
            INSERT INTO dbo.syConfigAppSettings (
                                                KeyName
                                               ,Description
                                               ,ModUser
                                               ,ModDate
                                               ,CampusSpecific
                                               ,ExtraConfirmation
                                                )
            VALUES ( @settingKeyName                                                                                     -- KeyName - varchar(200)
                    ,'Enable(Yes) or Disabled(No) the option to allow printing Progress Reports in multi-column format.' -- Description - varchar(1000)
                    ,'sa'                                                                                                -- ModUser - varchar(50)
                    ,GETDATE()                                                                                           -- ModDate - datetime
                    ,0                                                                                                   -- CampusSpecific - bit
                    ,0                                                                                                   -- ExtraConfirmation - bit
                );
        END;



    DECLARE @multiColumnPRSettingId INT = (
                                          SELECT TOP 1 SettingId
                                          FROM   dbo.syConfigAppSettings
                                          WHERE  KeyName = @settingKeyName
                                          );

    UPDATE dbo.syConfigAppSettings
    SET    Description = 'Enable(Yes) or Disabled(No) the option to allow printing Progress Reports and Transcript in multi-column format.'
    WHERE  SettingId = @multiColumnPRSettingId;

    IF NOT EXISTS (
                  SELECT 1
                  FROM   dbo.syConfigAppSetValues
                  WHERE  SettingId = @multiColumnPRSettingId
                  )
        BEGIN
            INSERT INTO dbo.syConfigAppSetValues (
                                                 ValueId
                                                ,SettingId
                                                ,CampusId
                                                ,Value
                                                ,ModUser
                                                ,ModDate
                                                ,Active
                                                 )
            VALUES ( NEWID()                 -- ValueId - uniqueidentifier
                    ,@multiColumnPRSettingId -- SettingId - int
                    ,NULL                    -- CampusId - uniqueidentifier
                    ,'Yes'                   -- Value - varchar(1000)
                    ,'SA'                    -- ModUser - varchar(50)
                    ,GETDATE()               -- ModDate - datetime
                    ,1                       -- Active - bit
                );
        END;

    UPDATE syConfigAppSetValues
    SET    Value = CASE WHEN EXISTS (
                                    SELECT 1
                                    FROM   dbo.arPrgVersions
                                    WHERE  ProgramRegistrationType = 1
                                    ) THEN 'Yes'
                        ELSE 'No'
                   END
    WHERE  SettingId = @multiColumnPRSettingId;

    --updating sequence
    DECLARE @NewVals TABLE
        (
            InstrGrdBkWgtDetailId UNIQUEIDENTIFIER
           ,seq INT NULL
           ,newSeq INT
        );
    INSERT INTO @NewVals
                SELECT   InstrGrdBkWgtDetailId
                        ,Seq
                        ,DENSE_RANK() OVER ( PARTITION BY InstrGrdBkWgtId
                                             ORDER BY Seq
                                                     ,Descrip
                                           ) AS newSeq
                FROM     dbo.arGrdBkWgtDetails
                ORDER BY InstrGrdBkWgtId
                        ,Seq;

    UPDATE    D
    SET       D.Seq = NewVals.newSeq
    FROM      arGrdBkWgtDetails D
    LEFT JOIN @NewVals NewVals ON NewVals.InstrGrdBkWgtDetailId = D.InstrGrdBkWgtDetailId;


END TRY
BEGIN CATCH
    SET @ErrorAddingSettingMultiColumnPR = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @ErrorAddingSettingMultiColumnPR > 0
    BEGIN
        ROLLBACK TRANSACTION addSettingMulticolProgressReport;
        PRINT 'Failed transcation addSettingMulticolProgressReport.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION addSettingMulticolProgressReport;
        PRINT 'successful transaction addSettingMulticolProgressReport.';
    END;
GO
--=================================================================================================
-- END AD-16904 Multi Column Progress Report
--=================================================================================================
--=================================================================================================
-- START AD-10994 Add Ability to Adjust Scheduled Hours For Single Day
--=================================================================================================
DECLARE @Error AS INT;
SET @Error = 0;
BEGIN TRANSACTION AdjustSchedHours;
BEGIN TRY

    DECLARE @AdjustSchedResourceId INT = 878;

    IF NOT EXISTS (
                  SELECT *
                  FROM   dbo.syResources
                  WHERE  ResourceID = @AdjustSchedResourceId
                  )
        BEGIN

            -- Create new resource
            INSERT INTO dbo.syResources (
                                        ResourceID
                                       ,Resource
                                       ,ResourceTypeID
                                       ,ResourceURL
                                       ,SummListId
                                       ,ChildTypeId
                                       ,ModDate
                                       ,ModUser
                                       ,AllowSchlReqFlds
                                       ,MRUTypeId
                                       ,UsedIn
                                       ,TblFldsId
                                       ,DisplayName
                                        )
            VALUES ( @AdjustSchedResourceId           -- ResourceID - smallint
                    ,'Adjust Scheduled Hours'         -- Resource - varchar(200)
                    ,3                                -- ResourceTypeID - tinyint
                    ,'~/AR/AdjustScheduledHours.aspx' -- ResourceURL - varchar(100)
                    ,NULL                             -- SummListId - smallint
                    ,NULL                             -- ChildTypeId - tinyint
                    ,GETDATE()                        -- ModDate - datetime
                    ,'sa'                             -- ModUser - varchar(50)
                    ,0                                -- AllowSchlReqFlds - bit
                    ,1                                -- MRUTypeId - smallint
                    ,4                                -- UsedIn - int
                    ,NULL                             -- TblFldsId - int
                    ,'Adjust Scheduled Hours'         -- DisplayName - varchar(200)
                );

            -- get parent id of sibling Reports -> Financial Aid -> General Reports 
            DECLARE @GeneralOptionsMenuItemId INT = (
                                                    SELECT MenuItemId
                                                    FROM   dbo.syMenuItems
                                                    WHERE  DisplayName = 'General Options'
                                                           AND ParentId = (
                                                                          SELECT a.MenuItemId
                                                                          FROM   dbo.syMenuItems a
                                                                          WHERE  a.DisplayName = 'Common Tasks'
                                                                                 AND a.ParentId = (
                                                                                                  SELECT MenuItemId
                                                                                                  FROM   dbo.syMenuItems
                                                                                                  WHERE  DisplayName = 'Academics'
                                                                                                         AND MenuItemTypeId = 1
                                                                                                  )
                                                                          )
                                                    );

            -- Reports -> Financial aid -> 90/10 Reports menu item
            INSERT INTO dbo.syMenuItems (
                                        MenuName
                                       ,DisplayName
                                       ,Url
                                       ,MenuItemTypeId
                                       ,ParentId
                                       ,DisplayOrder
                                       ,IsPopup
                                       ,ModDate
                                       ,ModUser
                                       ,IsActive
                                       ,ResourceId
                                       ,HierarchyId
                                       ,ModuleCode
                                       ,MRUType
                                       ,HideStatusBar
                                        )
            VALUES ( 'Adjust Scheduled Hours'        -- MenuName - varchar(250)
                    ,'Adjust Scheduled Hours'        -- DisplayName - varchar(250)
                    ,'/AR/AdjustScheduledHours.aspx' -- Url - nvarchar(250)
                    ,4                               -- MenuItemTypeId - smallint
                    ,@GeneralOptionsMenuItemId       -- ParentId - int
                    ,800                             -- DisplayOrder - int
                    ,0                               -- IsPopup - bit
                    ,GETDATE()                       -- ModDate - datetime
                    ,'sa'                            -- ModUser - varchar(50)
                    ,1                               -- IsActive - bit
                    ,@AdjustSchedResourceId          -- ResourceId - smallint
                    ,NEWID()                         -- HierarchyId - uniqueidentifier
                    ,NULL                            -- ModuleCode - varchar(5)
                    ,NULL                            -- MRUType - int
                    ,NULL                            -- HideStatusBar - bit
                );

            --syNavigation nodes - security

            DECLARE @parentHierarchy UNIQUEIDENTIFIER = (
                                                        SELECT ParentId
                                                        FROM   dbo.syNavigationNodes
                                                        WHERE  HierarchyId = (
                                                                             SELECT TOP 1 HierarchyId
                                                                             FROM   dbo.syMenuItems
                                                                             WHERE  ParentId = @GeneralOptionsMenuItemId
                                                                                    AND MenuName = 'Class Schedules'
                                                                             )
                                                        );

            DECLARE @AdjustSchedHoursHierarchyId UNIQUEIDENTIFIER = (
                                                                    SELECT TOP 1 HierarchyId
                                                                    FROM   dbo.syMenuItems
                                                                    WHERE  MenuName = 'Adjust Scheduled Hours'
                                                                           AND ResourceId = @AdjustSchedResourceId
                                                                    );

            -- for 90/10 data export item
            INSERT INTO dbo.syNavigationNodes (
                                              HierarchyId
                                             ,HierarchyIndex
                                             ,ResourceId
                                             ,ParentId
                                             ,ModUser
                                             ,ModDate
                                             ,IsPopupWindow
                                             ,IsShipped
                                              )
            VALUES ( @AdjustSchedHoursHierarchyId -- HierarchyId - uniqueidentifier
                    ,28                           -- HierarchyIndex - smallint
                    ,@AdjustSchedResourceId       -- ResourceId - smallint
                    ,@parentHierarchy             -- ParentId - uniqueidentifier
                    ,'sa'                         -- ModUser - varchar(50)
                    ,GETDATE()                    -- ModDate - datetime
                    ,0                            -- IsPopupWindow - bit
                    ,1                            -- IsShipped - bit
                );


        END;

END TRY
BEGIN CATCH
    SET @Error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN
        ROLLBACK TRANSACTION AdjustSchedHours;
        PRINT 'Failed transaction AdjustSchedHours.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION AdjustSchedHours;
        PRINT 'Successful transaction AdjustSchedHours.';
    END;
GO
--=================================================================================================
-- END AD-10994 Add Ability to Adjust Scheduled Hours For Single Day
--=================================================================================================
--=================================================================================================
-- START AD-8846 Restrict pay periods per academic year to 2 incase of Non-term and Clock hour Title IV programs	
--=================================================================================================
DECLARE @Error AS INT;
SET @Error = 0;
BEGIN TRANSACTION RestrictPayPeriods;
BEGIN TRY


    IF EXISTS (
              SELECT     pv.PrgVerId
              FROM       dbo.arPrgVersions pv
              INNER JOIN dbo.arPrograms pr ON pr.ProgId = pv.ProgId
              INNER JOIN dbo.arCampusPrgVersions cpv ON cpv.PrgVerId = pv.PrgVerId
              WHERE      (
                         pr.ACId = 6
                         OR pr.ACId = 5
                         )
                         AND cpv.IsTitleIV = 1
                         AND pv.PayPeriodPerAcYear > 2
              )
        BEGIN

            UPDATE dbo.arPrgVersions
            SET    PayPeriodPerAcYear = 2
            WHERE  PrgVerId IN (
                               SELECT     pv.PrgVerId
                               FROM       dbo.arPrgVersions pv
                               INNER JOIN dbo.arPrograms pr ON pr.ProgId = pv.ProgId
                               INNER JOIN dbo.arCampusPrgVersions cpv ON cpv.PrgVerId = pv.PrgVerId
                               WHERE      (
                                          pr.ACId = 6
                                          OR pr.ACId = 5
                                          )
                                          AND cpv.IsTitleIV = 1
                                          AND pv.PayPeriodPerAcYear > 2
                               );
        END;



END TRY
BEGIN CATCH
    SET @Error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN
        ROLLBACK TRANSACTION RestrictPayPeriods;
        PRINT 'Failed transcation RestrictPayPeriods.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION RestrictPayPeriods;
        PRINT 'successful transaction RestrictPayPeriods.';
    END;
GO
--=================================================================================================
-- END AD-8846 Restrict pay periods per academic year to 2 incase of Non-term and Clock hour Title IV programs
--=================================================================================================
-- ===============================================================================================
-- END Consolidated Script Version 4.1SP2
-- ===============================================================================================

