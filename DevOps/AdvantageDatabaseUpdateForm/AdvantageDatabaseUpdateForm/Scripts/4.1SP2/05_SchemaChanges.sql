﻿/*
Run this script on:

        dev-com-db1\Adv.AvedaLive    -  This database will be modified

to synchronize it with a database with the schema represented by:

        Source

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.7.10021 from Red Gate Software Ltd at 9/17/2019 5:29:42 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
/*
* Use this Pre-Deployment script to perform tasks before the deployment of the project.
* Read more at https://www.red-gate.com/SOC7/pre-deployment-script-help
*/
UPDATE dbo.arClsSectMeetings
SET PeriodId = NULL
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)

DELETE FROM syPeriodsWorkDays
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[Trigger_UpdateProgressOfCourses] from [dbo].[arGrdBkResults]'
GO
IF OBJECT_ID(N'[dbo].[Trigger_UpdateProgressOfCourses]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[Trigger_UpdateProgressOfCourses]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_PR_Sub3_Terms_forAllEnrolmnents]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub3_Terms_forAllEnrolmnents]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_PR_Sub3_Terms_forAllEnrolmnents]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[usp_GetStudentLedgerReportSummary]'
GO
IF OBJECT_ID(N'[dbo].[usp_GetStudentLedgerReportSummary]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[usp_GetStudentLedgerReportSummary]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[usp_GetStudentLedgerDataGrid]'
GO
IF OBJECT_ID(N'[dbo].[usp_GetStudentLedgerDataGrid]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[usp_GetStudentLedgerDataGrid]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_ProcessPaymentPeriods]'
GO
IF OBJECT_ID(N'[dbo].[USP_ProcessPaymentPeriods]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_ProcessPaymentPeriods]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_GPACalculator]'
GO
IF OBJECT_ID(N'[dbo].[USP_GPACalculator]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_GPACalculator]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_TR_Sub07_TotalCourses]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub07_TotalCourses]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_TR_Sub07_TotalCourses]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[CalculateStudentAverage]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateStudentAverage]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[CalculateStudentAverage]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudent]'
GO
IF OBJECT_ID(N'[dbo].[arStudent]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CalculateStudentAverage]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateStudentAverage]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'-- =============================================  
-- Author:  FAME Inc.  
-- Create date: 6/11/2019  
-- Description: Calculated Student GPA Based on a set of parameters - Numeric ( Weighted & Unweighted currently implemented)  
--Referenced in :  
--[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents] -> via db function CalculateStudentAverage  
--[Usp_PR_Sub3_Terms_forAllEnrolmnents] -> via db function CalculateStudentAverage  
--ANY CHANGES TO THIS FILE MUST BE REFLECTED IN SP GPA_Calculator  
-- Also this is also referenced in the table syFieldCalculation in the column CalculationSql for adhoc report for few columns..
-- =============================================  
CREATE FUNCTION [dbo].[CalculateStudentAverage]
    (
        @EnrollmentId VARCHAR(50)
       ,@BeginDate DATETIME = NULL
       ,@EndDate DATETIME = NULL
       ,@ClassId UNIQUEIDENTIFIER = NULL
       ,@TermId UNIQUEIDENTIFIER = NULL
       ,@SysComponentTypeId SMALLINT = NULL
    )
RETURNS DECIMAL(16, 2)
AS
    BEGIN
        DECLARE @GPAResult AS DECIMAL(16, 2);
        DECLARE @UseWeightForGPA BIT = 1;

        IF ( @EnrollmentId IS NULL )
            RETURN @GPAResult;

        SET @UseWeightForGPA = (
                               SELECT TOP 1 PV.DoCourseWeightOverallGPA
                               FROM   dbo.arStuEnrollments E
                               JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                               WHERE  E.StuEnrollId = @EnrollmentId
                               );

        -----------------Numeric GPA Grades Format------------------------  

        SET @GPAResult = (
                         SELECT ( CASE WHEN @UseWeightForGPA = 1 THEN
                                           ROUND(( SUM(b.courseweight * b.WeightedCourseAverage / 100) / SUM(b.courseweight)) * 100, 2)
                                       ELSE AVG(b.UnweightedCourseAverage)
                                  END
                                ) AS WeightedGPA
                         FROM   (
                                SELECT   SUM(OurSingleClassGradeFactor) AS CourseFactor
                                        ,SUM(a.GradeWeight) AS GradeWeight
                                        ,SUM(a.Score) AS SumOfScores
                                        ,CASE WHEN SUM(a.GradeWeight) > 0 THEN ( SUM(OurSingleClassGradeFactor) / SUM(a.GradeWeight)) * 100
                                              ELSE NULL
                                         END AS WeightedCourseAverage
                                        ,( AVG(a.Score)) AS UnweightedCourseAverage
                                        ,ClsSectionId
                                        ,a.courseweight
                                FROM     (
                                         SELECT ( c.Weight * a.Score / 100 ) AS OurSingleClassGradeFactor
                                               ,c.Weight AS GradeWeight
                                               ,a.Score
                                               ,a.ClsSectionId
                                               ,CASE WHEN d.CourseWeight = 0
                                                          AND NOT EXISTS (
                                                                         SELECT 1
                                                                         FROM   dbo.arProgVerDef CPVC
                                                                         WHERE  CPVC.CourseWeight > 0
                                                                                AND CPVC.PrgVerId = d.PrgVerId
                                                                         )
                                                          AND PV.DoCourseWeightOverallGPA = 0 THEN 100 / (
                                                                                                         SELECT COUNT(*)
                                                                                                         FROM   dbo.arGrdBkResults Cn
                                                                                                         WHERE  Cn.StuEnrollId = a.StuEnrollId
                                                                                                                AND Cn.ClsSectionId = a.ClsSectionId
                                                                                                         )
                                                     ELSE d.CourseWeight
                                                END AS courseweight
                                               ,c.Descrip AS ClassDescrip
                                         FROM   (
                                                SELECT   a.StuEnrollId
                                                        ,a.ClsSectionId
                                                        ,a.InstrGrdBkWgtDetailId
                                                        ,AVG(Score) AS Score
                                                FROM     dbo.arGrdBkResults a
                                                JOIN     dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                                JOIN     dbo.arGrdComponentTypes f ON f.GrdComponentTypeId = c.GrdComponentTypeId
                                                JOIN     dbo.arClassSections ON arClassSections.ClsSectionId = a.ClsSectionId
                                                WHERE    a.StuEnrollId = @EnrollmentId
                                                         --AND a.IsCompGraded = 1  
                                                         AND a.Score >= 0
                                                         AND a.Score IS NOT NULL
                                                         AND a.PostDate IS NOT NULL
                                                         AND (
                                                             @EndDate IS NULL
                                                             OR ( a.PostDate <= @EndDate )
                                                             )
                                                         AND (
                                                             @ClassId IS NULL
                                                             OR ( @ClassId = a.ClsSectionId )
                                                             )
                                                         AND (
                                                             @TermId IS NULL
                                                             OR ( @TermId = TermId )
                                                             )
                                                         AND (
                                                             @SysComponentTypeId IS NULL
                                                             OR @SysComponentTypeId = f.SysComponentTypeId
                                                             )
                                                GROUP BY StuEnrollId
                                                        ,a.ClsSectionId
                                                        ,a.InstrGrdBkWgtDetailId
                                                ) a -- students grades  
                                         JOIN   dbo.arClassSections b ON b.ClsSectionId = a.ClsSectionId
                                         JOIN   dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                         JOIN   dbo.arProgVerDef d ON d.ReqId = b.ReqId
                                         JOIN   dbo.arStuEnrollments E ON E.StuEnrollId = a.StuEnrollId
                                         JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                                         ) a
                                WHERE    a.courseweight > 0
                                GROUP BY ClsSectionId
                                        ,a.courseweight


                                --ORDER BY CourseGPA DESC  
                                ) b
                         );
        --END;  
        RETURN @GPAResult;
    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[Trigger_UpdateProgressOfCourses] on [dbo].[arGrdBkResults]'
GO
IF OBJECT_ID(N'[dbo].[Trigger_UpdateProgressOfCourses]', 'TR') IS NULL
EXEC sp_executesql N'
CREATE TRIGGER [dbo].[Trigger_UpdateProgressOfCourses]
ON [dbo].[arGrdBkResults]
FOR INSERT, UPDATE
AS
SET NOCOUNT ON;

    BEGIN
        DECLARE @Error AS INTEGER;

        SET @Error = 0;

        BEGIN TRANSACTION FixCoursesScore;
        BEGIN TRY

            DECLARE @CourseResultsToUpdate AS TABLE
                (
                    CourseScore DECIMAL(18, 2) NULL
                   ,NumberOfComponents INT NULL
                   ,NumberOfComponentsScored INT NULL
                   ,ClsSectionId UNIQUEIDENTIFIER NULL
                   ,StuEnrollId UNIQUEIDENTIFIER NULL
                   ,GrdSystemId UNIQUEIDENTIFIER NULL
                );

            DECLARE @FilteredCourses AS TABLE
                (
                    StuEnrollId UNIQUEIDENTIFIER NOT NULL
                   ,ClsSectionId VARCHAR(50) NOT NULL
                );

            DECLARE @CampusId UNIQUEIDENTIFIER = (
                                                 SELECT TOP ( 1 ) CampusId
                                                 FROM   dbo.arStuEnrollments
                                                 WHERE  StuEnrollId = (
                                                                      SELECT TOP 1 Inserted.StuEnrollId
                                                                      FROM   Inserted
                                                                      )
                                                 );

            DECLARE @GradeRounding BIT = ( CASE WHEN LOWER(ISNULL(dbo.GetAppSettingValueByKeyName(''GradeRounding'', @CampusId), '''')) = ''yes'' THEN 1
                                                ELSE 0
                                           END
                                         );

            INSERT INTO @FilteredCourses
                        SELECT DISTINCT StuEnrollId
                                       ,ClsSectionId
                        FROM   Inserted;

            INSERT INTO @CourseResultsToUpdate (
                                               CourseScore
                                              ,NumberOfComponents
                                              ,NumberOfComponentsScored
                                              ,ClsSectionId
                                              ,StuEnrollId
                                              ,GrdSystemId
                                               )
                        SELECT     CASE WHEN @GradeRounding = 1 THEN
                                            ROUND(dbo.CalculateStudentAverage(courses.StuEnrollId, NULL, NULL, courses.ClsSectionId, NULL, NULL), 2)
                                        ELSE dbo.CalculateStudentAverage(courses.StuEnrollId, NULL, NULL, courses.ClsSectionId, NULL, NULL)
                                   END AS CourseScore
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   (
                                          SELECT   StuEnrollId
                                          FROM     dbo.arGrdBkResults
                                          WHERE    StuEnrollId = courses.StuEnrollId
                                                   AND ClsSectionId = courses.ClsSectionId
                                          GROUP BY StuEnrollId
                                                  ,ClsSectionId
                                                  ,InstrGrdBkWgtDetailId
                                          ) components
                                   ) AS NumberOfComponents
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   (
                                          SELECT   StuEnrollId
                                          FROM     dbo.arGrdBkResults
                                          WHERE    StuEnrollId = courses.StuEnrollId
                                                   AND ClsSectionId = courses.ClsSectionId
                                                   AND Score IS NOT NULL
                                                   AND PostDate IS NOT NULL
                                          GROUP BY StuEnrollId
                                                  ,ClsSectionId
                                                  ,InstrGrdBkWgtDetailId
                                          ) components
                                   ) AS NumberOfComponentsScored
                                  ,courses.ClsSectionId
                                  ,courses.StuEnrollId
                                  ,gradeSystem.GrdSystemId
                        FROM       @FilteredCourses courses
                        JOIN       dbo.arStuEnrollments enrollments ON enrollments.StuEnrollId = courses.StuEnrollId
                        INNER JOIN dbo.arPrgVersions programVersion ON programVersion.PrgVerId = enrollments.PrgVerId
                        INNER JOIN dbo.arGradeSystems gradeSystem ON gradeSystem.GrdSystemId = programVersion.GrdSystemId
                        WHERE      programVersion.ProgramRegistrationType = 1;

            UPDATE     courses
            SET        courses.Score = coursesToUpdate.CourseScore
                      ,courses.IsInComplete = CASE WHEN coursesToUpdate.NumberOfComponents = coursesToUpdate.NumberOfComponentsScored THEN 0
                                                   ELSE 1
                                              END
                      ,courses.DateCompleted = CASE WHEN courses.DateCompleted IS NULL
                                                         AND coursesToUpdate.NumberOfComponents = coursesToUpdate.NumberOfComponentsScored THEN GETDATE()
                                                    ELSE courses.DateCompleted
                                               END
                      ,courses.ModDate = GETDATE()
                      ,courses.IsCourseCompleted = CASE WHEN coursesToUpdate.NumberOfComponents = coursesToUpdate.NumberOfComponentsScored THEN 1
                                                        ELSE 0
                                                   END
                      ,courses.GrdSysDetailId = (
                                                SELECT TOP ( 1 ) gradeScale.GrdSysDetailId
                                                FROM   dbo.arGradeSystemDetails gsrdSysDetail
                                                JOIN   dbo.arGradeScaleDetails gradeScale ON gradeScale.GrdSysDetailId = gsrdSysDetail.GrdSysDetailId
                                                WHERE  gsrdSysDetail.GrdSystemId = coursesToUpdate.GrdSystemId
                                                       AND gradeScale.MaxVal >= coursesToUpdate.CourseScore
                                                       AND gradeScale.MinVal <= coursesToUpdate.CourseScore
                                                )
            FROM       dbo.arResults courses
            INNER JOIN @CourseResultsToUpdate coursesToUpdate ON coursesToUpdate.ClsSectionId = courses.TestId
                                                                 AND coursesToUpdate.StuEnrollId = courses.StuEnrollId
            WHERE      courses.IsGradeOverridden = 0;

            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;

        END TRY
        BEGIN CATCH
            DECLARE @msg NVARCHAR(MAX);
            DECLARE @severity INT;
            DECLARE @state INT;
            SELECT @msg = ERROR_MESSAGE()
                  ,@severity = ERROR_SEVERITY()
                  ,@state = ERROR_STATE();
            RAISERROR(@msg, @severity, @state);
            SET @Error = 1;
        END CATCH;

        IF ( @Error = 0 )
            BEGIN
                COMMIT TRANSACTION FixCoursesScore;

            END;
        ELSE
            BEGIN
                ROLLBACK TRANSACTION FixCoursesScore;
            END;

    END;


SET NOCOUNT OFF;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[SyStudentStatusChanges_RemoveScheduledForTerminatedOrGrad] on [dbo].[syStudentStatusChanges]'
GO
IF OBJECT_ID(N'[dbo].[SyStudentStatusChanges_RemoveScheduledForTerminatedOrGrad]', 'TR') IS NULL
EXEC sp_executesql N'

--==========================================================================================  
-- TRIGGER [SyStudentStatusChanges_RemoveScheduledForTerminatedOrGrad]e  
-- UPDATE,INSERT  Remove Scheduled and Absent Hours After LDA 
--==========================================================================================  
CREATE   TRIGGER [dbo].[SyStudentStatusChanges_RemoveScheduledForTerminatedOrGrad]
ON [dbo].[syStudentStatusChanges]
FOR UPDATE, INSERT
AS
BEGIN

    SET NOCOUNT ON;
    --Remove Scheduled and Absent Hours After LDA

    BEGIN
        UPDATE asca
        SET SchedHours = 0
        FROM arStudentClockAttendance asca
            JOIN syStudentStatusChanges ssc
                ON ssc.StuEnrollId = asca.StuEnrollId
            JOIN Inserted ins
                ON ins.StuEnrollId = asca.StuEnrollId
            JOIN syStatusCodes sc
                ON sc.StatusCodeId = ins.NewStatusId
        WHERE sc.SysStatusId IN ( 14, 12 )
              AND RecordDate >
              (
                  SELECT dbo.GetLDA(ssc.StuEnrollId)
              );

    END;


    SET NOCOUNT OFF;
END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[usp_GetStudentLedgerDataGrid]'
GO
IF OBJECT_ID(N'[dbo].[usp_GetStudentLedgerDataGrid]', 'P') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[usp_GetStudentLedgerDataGrid]
@StudentEnrollmentId UNIQUEIDENTIFIER
AS 
SELECT    T.TransDate AS TransactionDate
         ,( CASE WHEN P.PaymentTypeId = 1 THEN ''Cash''
                 WHEN P.PaymentTypeId = 2 THEN ''Check Number:''
                 WHEN P.PaymentTypeId = 3 THEN ''C/C Authorization:''
                 WHEN P.PaymentTypeId = 4 THEN ''EFT Number:''
                 WHEN P.PaymentTypeId = 5 THEN ''Money Order Number:''
                 WHEN P.PaymentTypeId = 6 THEN ''Non Cash Reference #:''
                 ELSE ''''
            END
          ) + P.CheckNumber AS Document
         ,RTRIM(LTRIM(T.TransDescrip))  + ( CASE WHEN TC.TransCodeId IS NOT NULL AND tc.SysTransCodeId IN (11, 16 ) THEN '' ('' + tc.TransCodeDescrip + '')'' ELSE '''' END ) AS TransactionDescription
         ,FORMAT(T.TransAmount, ''C'') AS Amount
         ,FORMAT(SUM(T.TransAmount) OVER ( ORDER BY T.TransDate
                                            ,T.TransDescrip ASC
                                  ),''C'') AS Balance
         ,CASE WHEN T.PaymentPeriodNumber IS NOT NULL THEN ''Payment Period''
               ELSE ''''
          END AS PeriodType
         ,CASE WHEN T.PaymentPeriodNumber IS NOT NULL THEN T.PaymentPeriodNumber
               ELSE NULL
          END AS Period
FROM      dbo.saTransactions T
LEFT JOIN dbo.saPayments P ON P.TransactionId = T.TransactionId
LEFT JOIN dbo.saTransCodes TC ON TC.TransCodeId = T.TransCodeId
WHERE     T.StuEnrollId = @StudentEnrollmentId AND T.Voided = 0
ORDER BY  TransactionDate ASC;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[usp_GetStudentLedgerReportSummary]'
GO
IF OBJECT_ID(N'[dbo].[usp_GetStudentLedgerReportSummary]', 'P') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[usp_GetStudentLedgerReportSummary]
    @StudentEnrollmentId UNIQUEIDENTIFIER
AS
    SELECT   Summary.FundSourceDescrip AS Description
            ,Summary.AwardTypeId
            ,FORMAT(Summary.GrossAmount, ''C'') AS Amount
            ,FORMAT(SUM(   CASE WHEN Summary.IsDisbursement = 1 THEN Summary.Amount
                                ELSE 0
                           END
                       )
                   ,''C''
                   ) AS Received
            ,FORMAT(SUM(   CASE WHEN Summary.IsDisbursement = 0 THEN Summary.Amount
                                ELSE 0
                           END
                       )
                   ,''C''
                   ) AS Refunded
    FROM     (
             SELECT    D.PmtDisbRelId AS Id
                      ,D.Amount AS Amount
                      ,S.StudentAwardId AS AwardId
                      ,A.AwardTypeId
                      ,FS.FundSourceDescrip
                      ,A.GrossAmount
                      ,1 AS IsDisbursement
             FROM      dbo.saPmtDisbRel D
             LEFT JOIN dbo.faStudentAwardSchedule S ON S.AwardScheduleId = D.AwardScheduleId
             LEFT JOIN dbo.faStudentAwards A ON A.StudentAwardId = S.StudentAwardId
             LEFT JOIN dbo.saFundSources FS ON FS.FundSourceId = A.AwardTypeId
             LEFT JOIN dbo.saTransactions T ON T.TransactionId = D.TransactionId
             WHERE     A.StuEnrollId = @StudentEnrollmentId
                       AND T.TransactionId IS NOT NULL
                       AND (
                           T.Voided = 0
                           OR T.TransactionId IS NULL
                           )
                       AND FS.TitleIV = 1
             UNION
             SELECT    R.TransactionId AS Id
                      ,R.RefundAmount AS Amount
                      ,S.StudentAwardId AS AwardId
                      ,A.AwardTypeId
                      ,FS.FundSourceDescrip
                      ,A.GrossAmount
                      ,0 AS IsDisbursement
             FROM      dbo.saRefunds R
             LEFT JOIN dbo.faStudentAwardSchedule S ON S.AwardScheduleId = R.AwardScheduleId
             LEFT JOIN dbo.faStudentAwards A ON A.StudentAwardId = S.StudentAwardId
             LEFT JOIN dbo.saFundSources FS ON FS.FundSourceId = A.AwardTypeId
             LEFT JOIN dbo.saTransactions T ON T.TransactionId = R.TransactionId
             WHERE     A.StuEnrollId = @StudentEnrollmentId
                       AND T.TransactionId IS NOT NULL
                       AND (
                           T.Voided = 0
                           OR T.TransactionId IS NULL
                           )
                       AND FS.TitleIV = 1
             ) Summary
    GROUP BY Summary.AwardTypeId
            ,Summary.FundSourceDescrip
            ,Summary.GrossAmount;



'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_GPACalculator]'
GO
IF OBJECT_ID(N'[dbo].[USP_GPACalculator]', 'P') IS NULL
EXEC sp_executesql N'-- =============================================
-- Author:		FAME Inc.
-- Create date: 11/1/2018
-- Description:	Calculated Student GPA Based on a set of parameters - Numeric ( Weighted & Unweighted currently implemented)
--Referenced in :
--[Usp_PR_Sub2_Enrollment_Summary]
--[Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId]
--[USP_TitleIV_QualitativeAndQuantitative]
--CreditSummaryDB.vb
--Core Web API - EnrollmentService.GetEnrollmentProgramSummary
--[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents] -> via db function CalculateStudentAverage
--[Usp_PR_Sub3_Terms_forAllEnrolmnents] -> via db function CalculateStudentAverage
-- =============================================
CREATE PROCEDURE [dbo].[USP_GPACalculator]
    (
        @EnrollmentId VARCHAR(50) = NULL
       ,@BeginDate DATETIME = NULL
       ,@EndDate DATETIME = NULL
       ,@ClassId UNIQUEIDENTIFIER = NULL
       ,@TermId UNIQUEIDENTIFIER = NULL
       ,@StudentGPA AS DECIMAL(16, 2) OUTPUT
    )
AS
    BEGIN
        DECLARE @GPAResult AS DECIMAL(16, 2);
        -----------------Numeric GPA Grades Format------------------------

        SET @GPAResult = (
                         SELECT TOP 1 dbo.CalculateStudentAverage(@EnrollmentId, @BeginDate, @EndDate, @ClassId, @TermId,NULL)
                         );

        --END;
        SET @StudentGPA = @GPAResult;
    END;





'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_PR_Sub3_Terms_forAllEnrolmnents]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub3_Terms_forAllEnrolmnents]', 'P') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[Usp_PR_Sub3_Terms_forAllEnrolmnents]
    @StuEnrollIdList VARCHAR(MAX)
   ,@TermId VARCHAR(MAX) = NULL
   ,@TermStartDate VARCHAR(50) = NULL
   ,@TermStartDateModifier VARCHAR(10) = NULL
   ,@ShowAllEnrollments BIT = 0
AS
    DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
    DECLARE @GradesFormat VARCHAR(50);
    DECLARE @GPAMethod VARCHAR(50);
    IF @TermStartDate IS NULL
       OR @TermStartDate = ''''
        BEGIN
            SET @TermStartDate = CONVERT(VARCHAR(10), GETDATE(), 120);
        END;
    SET @StuEnrollCampusId = COALESCE((
                                      SELECT TOP 1 ASE.CampusId
                                      FROM   arStuEnrollments AS ASE
                                      WHERE  ASE.StuEnrollId IN (
                                                                SELECT Val
                                                                FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                )
                                      )
                                     ,NULL
                                     );
    SET @GradesFormat = dbo.GetAppSettingValueByKeyName(''GradesFormat'', @StuEnrollCampusId);
    IF ( @GradesFormat IS NOT NULL )
        BEGIN
            SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
        END;
    SET @GPAMethod = dbo.GetAppSettingValueByKeyName(''GPAMethod'', @StuEnrollCampusId);
    IF ( @GPAMethod IS NOT NULL )
        BEGIN
            SET @GPAMethod = LOWER(LTRIM(RTRIM(@GPAMethod)));
        END;

    DECLARE @TermCourseDataVar TABLE
        (
            StudentId UNIQUEIDENTIFIER
           ,TermId UNIQUEIDENTIFIER
           ,CourseId UNIQUEIDENTIFIER
           ,CreditsEarned DECIMAL(18, 2)
           ,CreditsAttempted DECIMAL(18, 2)
           ,Completed BIT
           ,FACreditsEarned DECIMAL(18, 2)
        );


    DECLARE @TermDataVar TABLE
        (
            PrgVerId UNIQUEIDENTIFIER
           ,PrgVerDescrip VARCHAR(100)
           ,PrgVersionTrackCredits BIT
           ,TermId UNIQUEIDENTIFIER
           ,TermDescription VARCHAR(100)
           ,TermStartDate DATETIME
           ,TermEndDate DATETIME
           ,CourseId UNIQUEIDENTIFIER
           ,CourseCode VARCHAR(100)
           ,CourseDescription VARCHAR(100)
           ,CourseCodeDescription VARCHAR(100)
           ,CourseCredits DECIMAL(18, 2)
           ,CourseFinAidCredits DECIMAL(18, 2)
           ,MinVal DECIMAL(18, 2)
           ,CourseScore DECIMAL(18, 2)
           ,StuEnrollId UNIQUEIDENTIFIER
           ,CreditsAttempted DECIMAL(18, 2)
           ,CreditsEarned DECIMAL(18, 2)
           ,Completed BIT
           ,CurrentScore DECIMAL(18, 2)
           ,FinalScore DECIMAL(18, 2)
           ,FinalGrade VARCHAR(10)
           ,WeightedAverage_GPA DECIMAL(18, 2)
           ,SimpleAverage_GPA DECIMAL(18, 2)
           ,CampusId UNIQUEIDENTIFIER
           ,CampDescrip VARCHAR(100)
           ,FirstName VARCHAR(100)
           ,LastName VARCHAR(100)
           ,MiddleName VARCHAR(100)
           ,TermGPA_Simple DECIMAL(18, 2)
           ,TermGPA_Weighted DECIMAL(18, 2)
           ,Term_CreditsAttempted DECIMAL(18, 2)
           ,Term_CreditsEarned DECIMAL(18, 2)
           ,termAverage DECIMAL(18, 2)
           ,GrdBkWgtDetailsCount INT
           ,ClockHourProgram BIT
           ,GradesFormat VARCHAR(50)
           ,GPAMethod VARCHAR(50)
           ,StudentId UNIQUEIDENTIFIER
           ,WasRegisteredByProgramType BIT
           ,rownumber1 INT
        );


    INSERT INTO @TermDataVar
                SELECT dt.PrgVerId
                      ,dt.PrgVerDescrip
                      ,dt.PrgVersionTrackCredits
                      ,dt.TermId
                      ,dt.TermDescription
                      ,dt.TermStartDate
                      ,dt.TermEndDate
                      ,dt.CourseId
                      ,dt.CourseCode
                      ,dt.CourseDescription
                      ,dt.CourseCodeDescription
                      ,dt.CourseCredits
                      ,dt.CourseFinAidCredits
                      ,dt.MinVal
                      ,dt.CourseScore
                      ,dt.StuEnrollId
                      ,dt.CreditsAttempted
                      ,dt.CreditsEarned
                      ,dt.Completed
                      ,dt.CurrentScore
                      ,dt.FinalScore
                      ,dt.FinalGrade
                      ,dt.WeightedAverage_GPA
                      ,dt.SimpleAverage_GPA
                      ,dt.CampusId
                      ,dt.CampDescrip
                      ,dt.FirstName
                      ,dt.LastName
                      ,dt.MiddleName
                      ,dt.TermGPA_Simple
                      ,dt.TermGPA_Weighted
                      ,dt.Term_CreditsAttempted
                      ,dt.Term_CreditsEarned
                      ,dt.termAverage
                      ,dt.GrdBkWgtDetailsCount
                      ,dt.ClockHourProgram
                      ,@GradesFormat AS GradesFormat
                      ,@GPAMethod AS GPAMethod
                      ,dt.StudentId AS StudentId
                      ,dt.WasRegisteredByProgramType AS WasRegisteredByProgramType
                      ,ROW_NUMBER() OVER ( PARTITION BY TermId
                                           ORDER BY TermStartDate
                                                   ,TermEndDate
                                                   ,TermDescription
                                         ) AS rownumber1
                FROM   (
                       SELECT     DISTINCT PV.PrgVerId AS PrgVerId
                                 ,PV.PrgVerDescrip AS PrgVerDescrip
                                 ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                       ELSE 0
                                  END AS PrgVersionTrackCredits
                                 ,T.TermId AS TermId
                                 ,T.TermDescrip AS TermDescription
                                 ,T.StartDate AS TermStartDate
                                 ,T.EndDate AS TermEndDate
                                 ,CS.ReqId AS CourseId
                                 ,R.Code AS CourseCode
                                 ,R.Descrip AS CourseDescription
                                 ,''('' + R.Code + '')'' + R.Descrip AS CourseCodeDescription
                                 ,R.Credits AS CourseCredits
                                 ,CASE WHEN ( @ShowAllEnrollments = 1 ) THEN R.FinAidCredits
                                       ELSE (
                                            SELECT SUM(SCS1.FACreditsEarned)
                                            FROM   syCreditSummary AS SCS1
                                            WHERE  SCS1.StuEnrollId = SCS.StuEnrollId
                                                   AND SCS1.TermId = SCS.TermId
                                            )
                                  END AS CourseFinAidCredits
                                 ,(
                                  SELECT MIN(MinVal)
                                  FROM   arGradeScaleDetails GCD
                                        ,arGradeSystemDetails GSD
                                  WHERE  GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                         AND GSD.IsPass = 1
                                         AND GCD.GrdScaleId = CS.GrdScaleId
                                  ) AS MinVal
                                 ,RES.Score AS CourseScore
                                 ,SE.StuEnrollId AS StuEnrollId
                                 --, NULL AS MinResult
                                 --, NULL AS GradeComponentDescription -- Student data   
                                 ,SCS.CreditsAttempted AS CreditsAttempted
                                 ,SCS.CreditsEarned AS CreditsEarned
                                 ,SCS.Completed AS Completed
                                 ,SCS.CurrentScore AS CurrentScore
                                 ,SCS.CurrentGrade AS CurrentGrade
                                 ,SCS.FinalScore AS FinalScore
                                 ,SCS.FinalGrade AS FinalGrade
                                 ,( CASE WHEN (
                                              SELECT SUM(SCS2.Count_WeightedAverage_Credits)
                                              FROM   syCreditSummary AS SCS2
                                              WHERE  SCS2.StuEnrollId = SE.StuEnrollId
                                                     AND SCS2.TermId = T.TermId
                                              ) > 0 THEN (
                                                         SELECT SUM(SCS2.Product_WeightedAverage_Credits_GPA) / SUM(SCS2.Count_WeightedAverage_Credits)
                                                         FROM   syCreditSummary AS SCS2
                                                         WHERE  SCS2.StuEnrollId = SE.StuEnrollId
                                                                AND SCS2.TermId = T.TermId
                                                         )
                                         ELSE 0
                                    END
                                  ) AS WeightedAverage_GPA
                                 ,( CASE WHEN (
                                              SELECT SUM(Count_SimpleAverage_Credits)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = SE.StuEnrollId
                                                     AND TermId = T.TermId
                                              ) > 0 THEN (
                                                         SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                         )
                                         ELSE 0
                                    END
                                  ) AS SimpleAverage_GPA
                                 --, NULL AS WeightedAverage_CumGPA
                                 --, NULL AS SimpleAverage_CumGPA
                                 ,C.CampusId AS CampusId
                                 ,C.CampDescrip AS CampDescrip
                                 --, NULL AS rownumber
                                 ,AST.FirstName AS FirstName
                                 ,AST.LastName AS LastName
                                 ,AST.MiddleName AS MiddleName
                                 -- Newly added fields
                                 ,SCS.TermGPA_Simple AS TermGPA_Simple
                                 ,SCS.TermGPA_Weighted AS TermGPA_Weighted
                                 --, NULL AS newcol
                                 ,(
                                  SELECT SUM(ISNULL(CreditsAttempted, 0))
                                  FROM   syCreditSummary
                                  WHERE  TermId = T.TermId
                                         AND StuEnrollId = SE.StuEnrollId
                                  ) AS Term_CreditsAttempted
                                 ,(
                                  SELECT SUM(ISNULL(CreditsEarned, 0))
                                  FROM   syCreditSummary
                                  WHERE  TermId = T.TermId
                                         AND StuEnrollId = SE.StuEnrollId
                                  ) AS Term_CreditsEarned
                                 --Newly added ends here
                                 ,(
                                  SELECT TOP 1 Average
                                  FROM   syCreditSummary
                                  WHERE  StuEnrollId = SE.StuEnrollId
                                         AND TermId = T.TermId
                                  ) AS termAverage
                                 ,(
                                  SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                  FROM   arGrdBkResults
                                  WHERE  StuEnrollId = SE.StuEnrollId
                                         AND ClsSectionId = RES.TestId
                                  ) AS GrdBkWgtDetailsCount
                                 ,CASE WHEN P.ACId = 5 THEN ''True''
                                       ELSE ''False''
                                  END AS ClockHourProgram
                                 ,AST.StudentId
                                 ,CASE WHEN PV.ProgramRegistrationType IS NOT NULL
                                            AND PV.ProgramRegistrationType = 1 THEN 1
                                       ELSE 0
                                  END AS WasRegisteredByProgramType
                       FROM       arClassSections CS
                       INNER JOIN arResults GBR ON CS.ClsSectionId = GBR.TestId
                       INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
                       INNER JOIN adLeads AS AST ON AST.StudentId = SE.StudentId
                       INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                       INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                       INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                       INNER JOIN arTerm T ON CS.TermId = T.TermId
                       INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                       INNER JOIN arResults RES ON RES.StuEnrollId = GBR.StuEnrollId
                                                   AND RES.TestId = CS.ClsSectionId
                       LEFT JOIN  syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                         AND T.TermId = SCS.TermId
                                                         AND R.ReqId = SCS.ReqId
                       WHERE -- SE.StuEnrollId = @StuEnrollId
                                  SE.StuEnrollId IN (
                                                    SELECT Val
                                                    FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                    )
                                  AND (
                                      @TermStartDate IS NULL
                                      OR @TermStartDateModifier IS NULL
                                      OR T.StartDate IS NULL
                                      OR (
                                         (
                                         ( @TermStartDateModifier <> ''='' )
                                         OR ( T.StartDate = @TermStartDate )
                                         )
                                         AND (
                                             ( @TermStartDateModifier <> ''>'' )
                                             OR ( T.StartDate > @TermStartDate )
                                             )
                                         AND (
                                             ( @TermStartDateModifier <> ''<'' )
                                             OR ( T.StartDate < @TermStartDate )
                                             )
                                         AND (
                                             ( @TermStartDateModifier <> ''>='' )
                                             OR ( T.StartDate >= @TermStartDate )
                                             )
                                         AND (
                                             ( @TermStartDateModifier <> ''<='' )
                                             OR ( T.StartDate <= @TermStartDate )
                                             )
                                         )
                                      )
                                  AND (
                                      @TermId IS NULL
                                      OR T.TermId IN (
                                                     SELECT Val
                                                     FROM   MultipleValuesForReportParameters(@TermId, '','', 1)
                                                     )
                                      )
                       UNION
                       SELECT     DISTINCT PV.PrgVerId AS PrgVerId
                                 ,PV.PrgVerDescrip AS PrgVerDescrip
                                 ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                       ELSE 0
                                  END AS PrgVersionTrackCredits
                                 ,T.TermId AS TermId
                                 ,T.TermDescrip AS TermDescription
                                 ,T.StartDate AS TermStartDate
                                 ,T.EndDate AS TermEndDate
                                 ,GBCR.ReqId AS CourseId
                                 ,R.Code AS CourseCode
                                 ,R.Descrip AS CourseDescrip
                                 ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                 ,R.Credits AS CourseCredits
                                 ,CASE WHEN ( @ShowAllEnrollments = 1 ) THEN R.FinAidCredits
                                       ELSE (
                                            SELECT SUM(SCS1.FACreditsEarned)
                                            FROM   syCreditSummary AS SCS1
                                            WHERE  SCS1.StuEnrollId = SCS.StuEnrollId
                                                   AND SCS1.TermId = SCS.TermId
                                            )
                                  END AS CourseFinAidCredits
                                 ,(
                                  SELECT MIN(MinVal)
                                  FROM   arGradeScaleDetails GCD
                                        ,arGradeSystemDetails GSD
                                  WHERE  GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                         AND GSD.IsPass = 1
                                  ) AS MinVal
                                 ,GBCR.Score AS CourseScore
                                 ,SE.StuEnrollId AS StuEnrollId
                                 --, NULL AS MinResult
                                 --, NULL AS GradeComponentDescription -- Student data    
                                 ,SCS.CreditsAttempted AS CreditsAttempted
                                 ,SCS.CreditsEarned AS CreditsEarned
                                 ,SCS.Completed AS Completed
                                 ,SCS.CurrentScore AS CurrentScore
                                 ,SCS.CurrentGrade AS CurrentGrade
                                 ,SCS.FinalScore AS FinalScore
                                 ,SCS.FinalGrade AS FinalGrade
                                 ,( CASE WHEN (
                                              SELECT SUM(Count_WeightedAverage_Credits)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = SE.StuEnrollId
                                                     AND TermId = T.TermId
                                              ) > 0 THEN (
                                                         SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                         )
                                         ELSE 0
                                    END
                                  ) AS WeightedAverage_GPA
                                 ,( CASE WHEN (
                                              SELECT SUM(Count_SimpleAverage_Credits)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = SE.StuEnrollId
                                                     AND TermId = T.TermId
                                              ) > 0 THEN (
                                                         SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                         )
                                         ELSE 0
                                    END
                                  ) AS SimpleAverage_GPA
                                 --, NULL AS WeightedAverage_CumGPA
                                 --, NULL AS SimpleAverage_CumGPA
                                 ,C.CampusId
                                 ,C.CampDescrip
                                 --, NULL AS rownumber
                                 ,AST.FirstName AS FirstName
                                 ,AST.LastName AS LastName
                                 ,AST.MiddleName AS MiddleName
                                 -- Newly added fields
                                 ,SCS.TermGPA_Simple AS TermGPA_Simple
                                 ,SCS.TermGPA_Weighted AS TermGPA_Weighted
                                 --, NULL AS newcol
                                 ,(
                                  SELECT SUM(ISNULL(CreditsAttempted, 0))
                                  FROM   syCreditSummary
                                  WHERE  TermId = T.TermId
                                         AND StuEnrollId = SE.StuEnrollId
                                  ) AS Term_CreditsAttempted
                                 ,(
                                  SELECT SUM(ISNULL(CreditsEarned, 0))
                                  FROM   syCreditSummary
                                  WHERE  TermId = T.TermId
                                         AND StuEnrollId = SE.StuEnrollId
                                  ) AS Term_CreditsEarned
                                 --Newly added ends here
                                 ,(
                                  SELECT TOP 1 Average
                                  FROM   syCreditSummary
                                  WHERE  StuEnrollId = SE.StuEnrollId
                                         AND TermId = T.TermId
                                  ) AS termAverage
                                 ,(
                                  SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                  FROM   arGrdBkConversionResults
                                  WHERE  StuEnrollId = SE.StuEnrollId
                                         AND TermId = T.TermId
                                         AND ReqId = R.ReqId
                                  ) AS GrdBkWgtDetailsCount
                                 ,CASE WHEN P.ACId = 5 THEN ''True''
                                       ELSE ''False''
                                  END AS ClockHourProgram
                                 ,AST.StudentId
                                 ,CASE WHEN PV.ProgramRegistrationType IS NOT NULL
                                            AND PV.ProgramRegistrationType = 1 THEN 1
                                       ELSE 0
                                  END AS WasRegisteredByProgramType
                       FROM       arTransferGrades GBCR
                       INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                       INNER JOIN adLeads AS AST ON AST.StudentId = SE.StudentId
                       INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                       INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                       INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                       INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                       INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                       --INNER JOIN arResults AR ON GBCR.StuEnrollId = AR.StuEnrollId
                       LEFT JOIN  syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                         AND T.TermId = SCS.TermId
                                                         AND R.ReqId = SCS.ReqId
                       WHERE -- SE.StuEnrollId = @StuEnrollId
                                  SE.StuEnrollId IN (
                                                    SELECT Val
                                                    FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                    )
                                  AND (
                                      @TermStartDate IS NULL
                                      OR @TermStartDateModifier IS NULL
                                      OR T.StartDate IS NULL
                                      OR (
                                         (
                                         ( @TermStartDateModifier <> ''='' )
                                         OR ( T.StartDate = @TermStartDate )
                                         )
                                         AND (
                                             ( @TermStartDateModifier <> ''>'' )
                                             OR ( T.StartDate > @TermStartDate )
                                             )
                                         AND (
                                             ( @TermStartDateModifier <> ''<'' )
                                             OR ( T.StartDate < @TermStartDate )
                                             )
                                         AND (
                                             ( @TermStartDateModifier <> ''>='' )
                                             OR ( T.StartDate >= @TermStartDate )
                                             )
                                         AND (
                                             ( @TermStartDateModifier <> ''<='' )
                                             OR ( T.StartDate <= @TermStartDate )
                                             )
                                         )
                                      )
                                  AND (
                                      @TermId IS NULL
                                      OR T.TermId IN (
                                                     SELECT Val
                                                     FROM   MultipleValuesForReportParameters(@TermId, '','', 1)
                                                     )
                                      )
                       ) dt;

    --SELECT * FROM @TermDataVar

    IF @ShowAllEnrollments = 1
        BEGIN
            INSERT INTO @TermCourseDataVar
                        SELECT DISTINCT StudentId
                              ,TermId
                              ,CourseId
                              ,CreditsEarned
                              ,CreditsAttempted
                              ,Completed
                              ,CourseFinAidCredits
                        FROM   @TermDataVar
                        WHERE  Completed = 1;

            SELECT   PrgVerId
                    ,PrgVerDescrip
                    ,PrgVersionTrackCredits
                    ,TermId
                    ,TermDescription
                    ,TermStartDate
                    ,TermEndDate
                    ,CourseId
                    ,CourseCode
                    ,CourseDescription
                    ,CourseCodeDescription
                    ,CourseCredits
                    ,(
                     SELECT   SUM(FACreditsEarned)
                     FROM     @TermCourseDataVar
                     WHERE    TermId = [@TermDataVar].TermId
                     GROUP BY StudentId
                     ) AS CourseFinAidCredits
                    --,(SELECT SUM(SCS1.FACreditsEarned)
                    --                         FROM   syCreditSummary AS SCS1
                    --                         WHERE  SCS1.StuEnrollId = ''0235B76B-B4AB-40C1-85C1-64ED042D1205'' 
                    --                                AND SCS1.TermId = ''E7430FF3-B021-4189-805D-5B54544D9615'')
                    ,MinVal
                    ,CourseScore
                    ,StuEnrollId
                    ,CreditsAttempted
                    ,CreditsEarned
                    ,Completed
                    ,CurrentScore
                    ,FinalScore
                    ,FinalGrade
                    ,WeightedAverage_GPA
                    ,SimpleAverage_GPA
                    ,CampusId
                    ,CampDescrip
                    ,FirstName
                    ,LastName
                    ,MiddleName
                    ,TermGPA_Simple
                    ,TermGPA_Weighted
                    ,(
                     SELECT   SUM(CreditsAttempted)
                     FROM     @TermCourseDataVar
                     WHERE    TermId = [@TermDataVar].TermId
                     GROUP BY StudentId
                     ) AS Term_CreditsAttempted
                    ,(
                     SELECT   SUM(CreditsEarned)
                     FROM     @TermCourseDataVar
                     WHERE    TermId = [@TermDataVar].TermId
                     GROUP BY StudentId
                     ) AS Term_CreditsEarned
                    ,CASE WHEN (
                               SELECT TOP 1 P.ACId
                               FROM   dbo.arStuEnrollments E
                               JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                               JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
                               WHERE  E.StuEnrollId = StuEnrollId
                               ) = 5
                               AND @GradesFormat = ''numeric'' THEN dbo.CalculateStudentAverage(StuEnrollId, NULL, NULL, NULL, TermId,NULL)
                          ELSE termAverage
                     END AS termAverage
                    ,GrdBkWgtDetailsCount
                    ,ClockHourProgram
                    ,GradesFormat
                    ,GPAMethod
                    ,WasRegisteredByProgramType
                    ,rownumber1
            FROM     @TermDataVar
            WHERE    rownumber1 = 1
            ORDER BY PrgVerDescrip
                    ,TermStartDate
                    ,TermEndDate
                    ,TermDescription
                    ,FinalGrade DESC
                    ,FinalScore DESC
                    ,CourseCode;
        END;
    ELSE
        BEGIN
            SELECT   PrgVerId
                    ,PrgVerDescrip
                    ,PrgVersionTrackCredits
                    ,TermId
                    ,TermDescription
                    ,TermStartDate
                    ,TermEndDate
                    ,CourseId
                    ,CourseCode
                    ,CourseDescription
                    ,CourseCodeDescription
                    ,CourseCredits
                    ,CourseFinAidCredits
                    ,MinVal
                    ,CourseScore
                    ,StuEnrollId
                    ,CreditsAttempted
                    ,CreditsEarned
                    ,Completed
                    ,CurrentScore
                    ,FinalScore
                    ,FinalGrade
                    ,WeightedAverage_GPA
                    ,SimpleAverage_GPA
                    ,CampusId
                    ,CampDescrip
                    ,FirstName
                    ,LastName
                    ,MiddleName
                    ,TermGPA_Simple
                    ,TermGPA_Weighted
                    ,Term_CreditsAttempted
                    ,Term_CreditsEarned
                    ,CASE WHEN (
                               SELECT TOP 1 P.ACId
                               FROM   dbo.arStuEnrollments E
                               JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                               JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
                               WHERE  E.StuEnrollId = StuEnrollId
                               ) = 5
                               AND @GradesFormat = ''numeric'' THEN dbo.CalculateStudentAverage(StuEnrollId, NULL, NULL, NULL, TermId,NULL)
                          ELSE termAverage
                     END AS termAverage
                    ,GrdBkWgtDetailsCount
                    ,ClockHourProgram
                    ,GradesFormat
                    ,GPAMethod
                    ,StudentId
                    ,WasRegisteredByProgramType
                    ,rownumber1
            FROM     @TermDataVar
            WHERE    rownumber1 = 1
            ORDER BY PrgVerDescrip
                    ,TermStartDate
                    ,TermEndDate
                    ,TermDescription
                    ,FinalGrade DESC
                    ,FinalScore DESC
                    ,CourseCode;
        END;

--SELECT * FROM @TermCourseDataVar








'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]', 'P') IS NULL
EXEC sp_executesql N'
-- ========================================================================================================= 
-- Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents 
-- ========================================================================================================= 
CREATE PROCEDURE [dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]
    @StuEnrollIdList VARCHAR(MAX)
   ,@TermId VARCHAR(50) = NULL
   ,@SysComponentTypeId VARCHAR(50) = NULL
AS -- source usp_pr_main_ssrs_subreport3_byterm  
    BEGIN
        DECLARE @GradesFormat AS VARCHAR(50);
        DECLARE @GPAMethod AS VARCHAR(50);
        DECLARE @GradeBookAt AS VARCHAR(50);
        DECLARE @StuEnrollCampusId AS UNIQUEIDENTIFIER;
        DECLARE @Counter AS INT;
        DECLARE @times AS INT;
        DECLARE @TermStartDate1 AS DATETIME;
        DECLARE @Score AS DECIMAL(18, 2);
        DECLARE @GrdCompDescrip AS VARCHAR(50);

        DECLARE @curId AS UNIQUEIDENTIFIER;
        DECLARE @curReqId AS UNIQUEIDENTIFIER;
        DECLARE @curStuEnrollId AS UNIQUEIDENTIFIER;
        DECLARE @curDescrip AS VARCHAR(50);
        DECLARE @curNumber AS INT;
        DECLARE @curGrdComponentTypeId AS INT;
        DECLARE @curMinResult AS DECIMAL(18, 2);
        DECLARE @curGrdComponentDescription AS VARCHAR(50);
        DECLARE @curClsSectionId AS UNIQUEIDENTIFIER;
        DECLARE @curRownumber AS INT;


        CREATE TABLE #tempTermWorkUnitCount
            (
                TermId UNIQUEIDENTIFIER
               ,ReqId UNIQUEIDENTIFIER
               ,sysComponentTypeId INT
               ,WorkUnitCount INT
            );
        CREATE TABLE #Temp21
            (
                Id UNIQUEIDENTIFIER
               ,TermId UNIQUEIDENTIFIER
               ,ReqId UNIQUEIDENTIFIER
               ,GradeBookDescription VARCHAR(50)
               ,Number INT
               ,GradeBookSysComponentTypeId INT
               ,GradeBookScore DECIMAL(18, 2)
               ,MinResult DECIMAL(18, 2)
               ,GradeComponentDescription VARCHAR(50)
               ,ClsSectionId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,RowNumber INT
            );
        CREATE TABLE #Temp22
            (
                ReqId UNIQUEIDENTIFIER
               ,EffectiveDate DATETIME
            );
        SET @StuEnrollCampusId = COALESCE((
                                          SELECT TOP 1 ASE.CampusId
                                          FROM   arStuEnrollments AS ASE
                                          WHERE  ASE.StuEnrollId IN (
                                                                    SELECT Val
                                                                    FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                    )
                                          )
                                         ,NULL
                                         );
        SET @GradesFormat = dbo.GetAppSettingValueByKeyName(''GradesFormat'', @StuEnrollCampusId);
        IF ( @GradesFormat IS NOT NULL )
            BEGIN
                SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
            END;
        SET @GPAMethod = dbo.GetAppSettingValueByKeyName(''GPAMethod'', @StuEnrollCampusId);
        IF ( @GPAMethod IS NOT NULL )
            BEGIN
                SET @GPAMethod = LOWER(LTRIM(RTRIM(@GPAMethod)));
            END;
        SET @GradeBookAt = dbo.GetAppSettingValueByKeyName(''GradeBookWeightingLevel'', @StuEnrollCampusId);
        IF ( @GradeBookAt IS NOT NULL )
            BEGIN
                SET @GradeBookAt = LOWER(LTRIM(RTRIM(@GradeBookAt)));
            END;

        IF LOWER(@GradeBookAt) = ''instructorlevel''
            BEGIN
                INSERT INTO #tempTermWorkUnitCount
                            SELECT   dt.TermId
                                    ,dt.ReqId
                                    ,dt.GradeBookSysComponentTypeId
                                    ,COUNT(dt.GradeBookDescription)
                            FROM     (
                                     SELECT     d.ReqId
                                               ,d.TermId
                                               ,CASE WHEN a.Descrip IS NULL THEN e.Descrip
                                                     --THEN (  
                                                     --      SELECT   Resource  
                                                     --      FROM     syResources  
                                                     --      WHERE    ResourceID = e.SysComponentTypeId  
                                                     --     )  
                                                     ELSE a.Descrip
                                                END AS GradeBookDescription
                                               ,( CASE e.SysComponentTypeId
                                                       WHEN 500 THEN a.Number
                                                       WHEN 503 THEN a.Number
                                                       WHEN 544 THEN a.Number
                                                       ELSE (
                                                            SELECT MIN(MinVal)
                                                            FROM   arGradeScaleDetails GSD
                                                                  ,arGradeSystemDetails GSS
                                                            WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                   AND GSS.IsPass = 1
                                                                   AND GSD.GrdScaleId = d.GrdScaleId
                                                            )
                                                  END
                                                ) AS MinResult
                                               ,a.Required
                                               ,a.MustPass
                                               ,ISNULL(e.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId
                                               ,a.Number
                                               ,(
                                                SELECT Resource
                                                FROM   syResources
                                                WHERE  ResourceID = e.SysComponentTypeId
                                                ) AS GradeComponentDescription
                                               ,a.InstrGrdBkWgtDetailId
                                               ,c.StuEnrollId
                                               ,0 AS IsExternShip
                                               ,CASE e.SysComponentTypeId
                                                     WHEN 544 THEN (
                                                                   SELECT ISNULL(SUM(HoursAttended), ''0.00'')
                                                                   FROM   arExternshipAttendance
                                                                   WHERE  StuEnrollId = c.StuEnrollId
                                                                   )
                                                     ELSE (
                                                          SELECT SUM(AGBR.Score)
                                                          FROM   arGrdBkResults AS AGBR
                                                          WHERE  AGBR.StuEnrollId = c.StuEnrollId
                                                                 AND AGBR.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                                                 AND AGBR.ClsSectionId = d.ClsSectionId
                                                          )
                                                END AS GradeBookScore
                                               ,ROW_NUMBER() OVER ( PARTITION BY ST.StuEnrollId
                                                                                --T.TermId, R.ReqId ORDER BY ST.CampusId, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, RES.Resource, e.Descrip) AS rownumber  
                                                                                ,T.TermId
                                                                                ,R.ReqId
                                                                    ORDER BY e.SysComponentTypeId
                                                                            ,a.Descrip
                                                                  ) AS rownumber
                                     FROM       arGrdBkWgtDetails AS a
                                     INNER JOIN arGrdBkWeights AS b ON b.InstrGrdBkWgtId = a.InstrGrdBkWgtId
                                     INNER JOIN arClassSections AS d ON d.InstrGrdBkWgtId = a.InstrGrdBkWgtId
                                     INNER JOIN arResults AS c ON c.TestId = d.ClsSectionId
                                     INNER JOIN arGrdComponentTypes AS e ON e.GrdComponentTypeId = a.GrdComponentTypeId
                                     INNER JOIN arStuEnrollments AS ST ON ST.StuEnrollId = c.StuEnrollId
                                     INNER JOIN arTerm AS T ON T.TermId = d.TermId
                                     INNER JOIN arReqs AS R ON R.ReqId = d.ReqId
                                     INNER JOIN syResources AS RES ON RES.ResourceID = e.SysComponentTypeId
                                     WHERE      c.StuEnrollId IN (
                                                                 SELECT Val
                                                                 FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                 )
                                                AND (
                                                    @SysComponentTypeId IS NULL
                                                    OR e.SysComponentTypeId IN (
                                                                               SELECT Val
                                                                               FROM   MultipleValuesForReportParameters(@SysComponentTypeId, '','', 1)
                                                                               )
                                                    )
                                     UNION
                                     SELECT     R.ReqId
                                               ,T.TermId
                                               ,CASE WHEN GBW.Descrip IS NULL THEN (
                                                                                   SELECT Resource
                                                                                   FROM   syResources
                                                                                   WHERE  ResourceID = GCT.SysComponentTypeId
                                                                                   )
                                                     ELSE GBW.Descrip
                                                END AS GradeBookDescription
                                               ,( CASE GCT.SysComponentTypeId
                                                       WHEN 500 THEN GBWD.Number
                                                       WHEN 503 THEN GBWD.Number
                                                       WHEN 544 THEN GBWD.Number
                                                       ELSE (
                                                            SELECT MIN(MinVal)
                                                            FROM   arGradeScaleDetails GSD
                                                                  ,arGradeSystemDetails GSS
                                                            WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                   AND GSS.IsPass = 1
                                                                   AND GSD.GrdScaleId = CSC.GrdScaleId
                                                            )
                                                  END
                                                ) AS MinResult
                                               ,GBWD.Required
                                               ,GBWD.MustPass
                                               ,ISNULL(GCT.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId
                                               ,GBWD.Number
                                               ,(
                                                SELECT Resource
                                                FROM   syResources
                                                WHERE  ResourceID = GCT.SysComponentTypeId
                                                ) AS GradeComponentDescription
                                               ,GBWD.InstrGrdBkWgtDetailId
                                               ,GBCR.StuEnrollId
                                               ,0 AS IsExternShip
                                               ,CASE GCT.SysComponentTypeId
                                                     WHEN 544 THEN (
                                                                   SELECT ISNULL(SUM(HoursAttended), ''0.00'')
                                                                   FROM   arExternshipAttendance
                                                                   WHERE  StuEnrollId = SE.StuEnrollId
                                                                   )
                                                     ELSE (
                                                          SELECT ISNULL(SUM(AGBR.Score), 0.00)
                                                          FROM   arGrdBkResults AS AGBR
                                                          WHERE  AGBR.StuEnrollId = SE.StuEnrollId
                                                                 AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                 AND ClsSectionId = CSC.ClsSectionId
                                                          )
                                                END AS GradeBookScore
                                               ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId
                                                                                ,T.TermId
                                                                                ,R.ReqId
                                                                    ORDER BY SE.CampusId
                                                                            ,T.StartDate
                                                                            ,T.EndDate
                                                                            ,T.TermId
                                                                            ,T.TermDescrip
                                                                            ,R.ReqId
                                                                            ,R.Descrip
                                                                            ,SYRES.Resource
                                                                            ,GCT.Descrip
                                                                  ) AS rownumber
                                     FROM       arGrdBkConversionResults GBCR
                                     INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                     INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                     INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                     INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                     INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                     INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                          AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                     INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                      AND GBCR.ReqId = GBW.ReqId
                                     INNER JOIN (
                                                SELECT   ReqId
                                                        ,MAX(EffectiveDate) AS EffectiveDate
                                                FROM     arGrdBkWeights
                                                GROUP BY ReqId
                                                ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                                     INNER JOIN (
                                                SELECT Resource
                                                      ,ResourceID
                                                FROM   syResources
                                                WHERE  ResourceTypeID = 10
                                                ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                     INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                     INNER JOIN arClassSections CSC ON CSC.TermId = T.TermId
                                                                       AND CSC.ReqId = R.ReqId
                                     WHERE      MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                                                AND SE.StuEnrollId IN (
                                                                      SELECT Val
                                                                      FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                      )
                                                AND (
                                                    @SysComponentTypeId IS NULL
                                                    OR GCT.SysComponentTypeId IN (
                                                                                 SELECT Val
                                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeId, '','', 1)
                                                                                 )
                                                    )
                                     ) AS dt
                            GROUP BY dt.TermId
                                    ,dt.ReqId
                                    ,dt.GradeBookSysComponentTypeId;
            END;
        ELSE
            BEGIN
                SET @Counter = 0;
                SET @TermStartDate1 = (
                                      SELECT AT.StartDate
                                      FROM   dbo.arTerm AS AT
                                      WHERE  AT.TermId = @TermId
                                      );
                INSERT INTO #Temp22
                            SELECT     AGBW.ReqId
                                      ,MAX(AGBW.EffectiveDate) AS EffectiveDate
                            FROM       dbo.arGrdBkWeights AS AGBW
                            INNER JOIN dbo.syCreditSummary AS SCS ON SCS.ReqId = AGBW.ReqId
                            WHERE      SCS.TermId = @TermId
                                       AND SCS.StuEnrollId IN (
                                                              SELECT Val
                                                              FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                              )
                                       AND AGBW.EffectiveDate <= @TermStartDate1
                            GROUP BY   AGBW.ReqId;

                DECLARE getUsers_Cursor CURSOR FOR
                    SELECT   dt.ID
                            ,dt.ReqId
                            ,dt.Descrip
                            ,dt.Number
                            ,dt.SysComponentTypeId
                            ,dt.MinResult
                            ,dt.GradeComponentDescription
                            ,dt.ClsSectionId
                            ,dt.StuEnrollId
                            ,ROW_NUMBER() OVER ( PARTITION BY dt.StuEnrollId
                                                             ,@TermId
                                                             ,dt.SysComponentTypeId
                                                 ORDER BY dt.SysComponentTypeId
                                                         ,dt.Descrip
                                               ) AS rownumber
                    FROM     (
                             SELECT     ISNULL(AGBWD.InstrGrdBkWgtDetailId, NEWID()) AS ID
                                       ,AR.ReqId
                                       ,AGBWD.Descrip
                                       ,AGBWD.Number
                                       ,AGCT.SysComponentTypeId
                                       ,( CASE WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN AGBWD.Number
                                               ELSE (
                                                    SELECT     MIN(AGSD.MinVal) AS MinVal
                                                    FROM       dbo.arGradeScaleDetails AS AGSD
                                                    INNER JOIN dbo.arGradeSystemDetails AS AGSDetails ON AGSDetails.GrdSysDetailId = AGSD.GrdSysDetailId
                                                    WHERE      AGSDetails.IsPass = 1
                                                               AND AGSD.GrdScaleId = ACS.GrdScaleId
                                                    )
                                          END
                                        ) AS MinResult
                                       ,SR.Resource AS GradeComponentDescription
                                       ,ACS.ClsSectionId
                                       ,AGBR.StuEnrollId
                             FROM       dbo.arGrdBkResults AS AGBR
                             INNER JOIN dbo.arGrdBkWgtDetails AS AGBWD ON AGBWD.InstrGrdBkWgtDetailId = AGBR.InstrGrdBkWgtDetailId
                             INNER JOIN dbo.arGrdBkWeights AS AGBW ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                             INNER JOIN dbo.arGrdComponentTypes AS AGCT ON AGCT.GrdComponentTypeId = AGBWD.GrdComponentTypeId
                             INNER JOIN dbo.syResources AS SR ON SR.ResourceID = AGCT.SysComponentTypeId
                             --INNER JOIN #Temp22 AS T2 ON T2.ReqId = AGBW.ReqId 
                             INNER JOIN dbo.arReqs AS AR ON AR.ReqId = AGBW.ReqId
                             INNER JOIN dbo.arClassSections AS ACS ON ACS.ReqId = AR.ReqId
                                                                      AND ACS.ClsSectionId = AGBR.ClsSectionId
                             INNER JOIN dbo.arResults AS AR2 ON AR2.TestId = ACS.ClsSectionId
                                                                AND AR2.StuEnrollId = AGBR.StuEnrollId --AND AR2.TestId = AGBR.ClsSectionId 

                             WHERE      (
                                        @StuEnrollIdList IS NULL
                                        OR AGBR.StuEnrollId IN (
                                                               SELECT Val
                                                               FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                               )
                                        )
                                        --AND T2.EffectiveDate = AGBW.EffectiveDate 
                                        AND AGBWD.Number > 0
                             ) dt
                    ORDER BY SysComponentTypeId
                            ,rownumber;
                OPEN getUsers_Cursor;
                FETCH NEXT FROM getUsers_Cursor
                INTO @curId
                    ,@curReqId
                    ,@curDescrip
                    ,@curNumber
                    ,@curGrdComponentTypeId
                    ,@curMinResult
                    ,@curGrdComponentDescription
                    ,@curClsSectionId
                    ,@curStuEnrollId
                    ,@curRownumber;
                SET @Counter = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        --PRINT @curNumber  
                        SET @times = 1;
                        WHILE @times <= @curNumber
                            BEGIN
                                --  PRINT @times  
                                IF @curNumber > 1
                                    BEGIN
                                        SET @GrdCompDescrip = @curDescrip + CAST(@times AS CHAR);


                                        IF @curGrdComponentTypeId = 544
                                            BEGIN
                                                SET @Score = (
                                                             SELECT ISNULL(SUM(HoursAttended), ''0.00'')
                                                             FROM   arExternshipAttendance
                                                             WHERE  StuEnrollId = @curStuEnrollId
                                                             );
                                            END;
                                        ELSE
                                            BEGIN
                                                SET @Score = (
                                                             SELECT ISNULL(SUM(Score), 0.00)
                                                             FROM   arGrdBkResults
                                                             WHERE  StuEnrollId = @curStuEnrollId
                                                                    AND InstrGrdBkWgtDetailId = @curId
                                                                    AND ResNum = @times
                                                                    AND ClsSectionId = @curClsSectionId
                                                             );
                                            END;

                                        SET @curRownumber = @times;
                                    END;
                                ELSE
                                    BEGIN
                                        SET @GrdCompDescrip = @curDescrip;

                                        IF @curGrdComponentTypeId = 544
                                            BEGIN
                                                SET @Score = (
                                                             SELECT ISNULL(SUM(HoursAttended), ''0.00'')
                                                             FROM   arExternshipAttendance
                                                             WHERE  StuEnrollId = @curStuEnrollId
                                                             );
                                            END;
                                        ELSE
                                            BEGIN
                                                SET @Score = (
                                                             SELECT ISNULL(SUM(Score), 0.00)
                                                             FROM   arGrdBkResults
                                                             WHERE  StuEnrollId = @curStuEnrollId
                                                                    AND InstrGrdBkWgtDetailId = @curId
                                                                    AND ResNum = @times
                                                             );
                                            END;


                                    END;
                                --PRINT @Score  
                                INSERT INTO #Temp21
                                VALUES ( @curId, @TermId, @curReqId, @GrdCompDescrip, @curNumber, @curGrdComponentTypeId, @Score, @curMinResult
                                        ,@curGrdComponentDescription, @curClsSectionId, @curStuEnrollId, @curRownumber );

                                SET @times = @times + 1;
                            END;
                        FETCH NEXT FROM getUsers_Cursor
                        INTO @curId
                            ,@curReqId
                            ,@curDescrip
                            ,@curNumber
                            ,@curGrdComponentTypeId
                            ,@curMinResult
                            ,@curGrdComponentDescription
                            ,@curClsSectionId
                            ,@curStuEnrollId
                            ,@curRownumber;
                    END;
                CLOSE getUsers_Cursor;
                DEALLOCATE getUsers_Cursor;

                INSERT INTO #tempTermWorkUnitCount
                            SELECT   dt.TermId
                                    ,dt.ReqId
                                    ,dt.GradeBookSysComponentTypeId
                                    ,COUNT(dt.GradeBookDescription)
                            FROM     (
                                     SELECT T21.TermId
                                           ,T21.ReqId
                                           ,T21.GradeBookDescription
                                           ,T21.GradeBookSysComponentTypeId
                                     FROM   #Temp21 AS T21
                                     UNION
                                     SELECT     T.TermId
                                               ,GBCR.ReqId
                                               ,GCT.Descrip AS GradeBookDescription
                                               ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                     FROM       arGrdBkConversionResults GBCR
                                     INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                     INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                     INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                     INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                     INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                     INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                          AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                     INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                      AND GBCR.ReqId = GBW.ReqId
                                     INNER JOIN (
                                                SELECT   ReqId
                                                        ,MAX(EffectiveDate) AS EffectiveDate
                                                FROM     arGrdBkWeights
                                                GROUP BY ReqId
                                                ) AS MED ON MED.ReqId = GBCR.ReqId -- MED --> MaxEffectiveDatesByCourse  
                                     INNER JOIN (
                                                SELECT Resource
                                                      ,ResourceID
                                                FROM   syResources
                                                WHERE  ResourceTypeID = 10
                                                ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                     INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                     WHERE      MED.EffectiveDate <= T.StartDate
                                                AND SE.StuEnrollId IN (
                                                                      SELECT Val
                                                                      FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                      )
                                                AND T.TermId = @TermId
                                                AND (
                                                    @SysComponentTypeId IS NULL
                                                    OR GCT.SysComponentTypeId IN (
                                                                                 SELECT Val
                                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeId, '','', 1)
                                                                                 )
                                                    )
                                     ) dt
                            GROUP BY dt.TermId
                                    ,dt.ReqId
                                    ,dt.GradeBookSysComponentTypeId;
            END;

        DROP TABLE #Temp22;
        DROP TABLE #Temp21;

        -- SELECT * FROM #tempTermWorkUnitCount AS TTWUC  ORDER BY TTWUC.TermId  -- , TTWUC.StuEnrollId  


        SELECT   dt1.PrgVerId
                ,dt1.PrgVerDescrip
                ,dt1.PrgVersionTrackCredits
                ,dt1.TermId
                ,dt1.TermDescription
                ,dt1.TermStartDate
                ,dt1.TermEndDate
                ,dt1.CourseId
                ,dt1.CouseStartDate
                ,dt1.CourseCode
                ,dt1.CourseDescription
                ,dt1.CourseCodeDescription
                ,dt1.CourseCredits
                ,dt1.CourseFinAidCredits
                ,dt1.MinVal
                ,dt1.CourseScore
                ,dt1.GradeBook_ResultId
                ,dt1.GradeBookDescription
                ,dt1.GradeBookScore
                ,dt1.GradeBookPostDate
                ,dt1.GradeBookPassingGrade
                ,dt1.GradeBookWeight
                ,dt1.GradeBookRequired
                ,dt1.GradeBookMustPass
                ,dt1.GradeBookSysComponentTypeId
                ,dt1.GradeBookHoursRequired
                ,dt1.GradeBookHoursCompleted
                ,dt1.StuEnrollId
                ,dt1.MinResult
                ,dt1.GradeComponentDescription -- Student data     
                ,dt1.CreditsAttempted
                ,dt1.CreditsEarned
                ,dt1.Completed
                ,dt1.CurrentScore
                ,dt1.CurrentGrade
                ,CASE WHEN (
                           SELECT TOP 1 P.ACId
                           FROM   dbo.arStuEnrollments E
                           JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                           JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
                           WHERE  E.StuEnrollId = dt1.StuEnrollId
                           ) = 5
                           AND @GradesFormat = ''numeric'' THEN dbo.CalculateStudentAverage(dt1.StuEnrollId, NULL, NULL, dt1.ClsSectionId, NULL,NULL)
                      ELSE dt1.FinalScore
                 END AS FinalScore
                ,dt1.FinalGrade
                ,dt1.CampusId
                ,dt1.CampDescrip
                ,dt1.rownumber
                ,dt1.FirstName
                ,dt1.LastName
                ,dt1.MiddleName
                ,dt1.GrdBkWgtDetailsCount
                ,dt1.ClockHourProgram
                ,dt1.GradesFormat
                ,dt1.GPAMethod
                ,dt1.WorkUnitCount
                ,dt1.WorkUnitCount_501
                ,dt1.WorkUnitCount_544
                ,dt1.WorkUnitCount_502
                ,dt1.WorkUnitCount_499
                ,dt1.WorkUnitCount_503
                ,dt1.WorkUnitCount_500
                ,dt1.WorkUnitCount_533
                ,dt1.rownumber2
        FROM     (
                 SELECT dt.PrgVerId
                       ,dt.PrgVerDescrip
                       ,dt.PrgVersionTrackCredits
                       ,dt.TermId
                       ,dt.TermDescription
                       ,dt.TermStartDate
                       ,dt.TermEndDate
                       ,dt.CourseId
                       ,dt.CouseStartDate
                       ,dt.CourseCode
                       ,dt.CourseDescription
                       ,dt.CourseCodeDescription
                       ,dt.CourseCredits
                       ,dt.CourseFinAidCredits
                       ,dt.MinVal
                       ,dt.CourseScore
                       ,dt.GradeBook_ResultId
                       ,dt.GradeBookDescription
                       ,dt.GradeBookScore
                       ,dt.GradeBookPostDate
                       ,dt.GradeBookPassingGrade
                       ,dt.GradeBookWeight
                       ,dt.GradeBookRequired
                       ,dt.GradeBookMustPass
                       ,dt.GradeBookSysComponentTypeId
                       ,dt.GradeBookHoursRequired
                       ,dt.GradeBookHoursCompleted
                       ,dt.StuEnrollId
                       ,dt.MinResult
                       ,dt.GradeComponentDescription -- Student data     
                       ,dt.CreditsAttempted
                       ,dt.CreditsEarned
                       ,dt.Completed
                       ,dt.CurrentScore
                       ,dt.CurrentGrade
                       ,dt.FinalScore
                       ,dt.FinalGrade
                       ,dt.CampusId
                       ,dt.CampDescrip
                       ,dt.rownumber
                       ,dt.FirstName
                       ,dt.LastName
                       ,dt.MiddleName
                       ,dt.GrdBkWgtDetailsCount
                       ,dt.ClockHourProgram
                       ,@GradesFormat AS GradesFormat
                       ,@GPAMethod AS GPAMethod
                       ,(
                        SELECT SUM(WorkUnitCount)
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  T.TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId IN ( 501, 544, 502, 499, 503, 500, 533 )
                        ) AS WorkUnitCount
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 501
                        ) AS WorkUnitCount_501
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 544
                        ) AS WorkUnitCount_544
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 502
                        ) AS WorkUnitCount_502
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 499
                        ) AS WorkUnitCount_499
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 503
                        ) AS WorkUnitCount_503
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 500
                        ) AS WorkUnitCount_500
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 533
                        ) AS WorkUnitCount_533
                       ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId
                                                        ,TermId
                                                        ,CourseId
                                            ORDER BY TermStartDate
                                                    ,TermEndDate
                                                    ,TermDescription
                                                    ,CourseDescription
                                          ) AS rownumber2
                       ,dt.ClsSectionId
                 FROM   (
                        SELECT     DISTINCT PV.PrgVerId
                                  ,PV.PrgVerDescrip
                                  ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                        ELSE 0
                                   END AS PrgVersionTrackCredits
                                  ,T.TermId
                                  ,T.TermDescrip AS TermDescription
                                  ,T.StartDate AS TermStartDate
                                  ,T.EndDate AS TermEndDate
                                  ,CS.ReqId AS CourseId
                                  ,CS.StartDate AS CouseStartDate
                                  ,R.Code AS CourseCode
                                  ,R.Descrip AS CourseDescription
                                  ,''('' + R.Code + '') '' + R.Descrip AS CourseCodeDescription
                                  ,R.Credits AS CourseCredits
                                  ,R.FinAidCredits AS CourseFinAidCredits
                                  ,(
                                   SELECT MIN(MinVal)
                                   FROM   arGradeScaleDetails GCD
                                         ,arGradeSystemDetails GSD
                                   WHERE  GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                          AND GSD.IsPass = 1
                                          AND GCD.GrdScaleId = CS.GrdScaleId
                                   ) AS MinVal
                                  ,RES.Score AS CourseScore
                                  ,NULL AS GradeBook_ResultId
                                  ,NULL AS GradeBookDescription
                                  ,NULL AS GradeBookScore
                                  ,NULL AS GradeBookPostDate
                                  ,NULL AS GradeBookPassingGrade
                                  ,NULL AS GradeBookWeight
                                  ,NULL AS GradeBookRequired
                                  ,NULL AS GradeBookMustPass
                                  ,NULL AS GradeBookSysComponentTypeId
                                  ,NULL AS GradeBookHoursRequired
                                  ,NULL AS GradeBookHoursCompleted
                                  ,SE.StuEnrollId
                                  ,NULL AS MinResult
                                  ,NULL AS GradeComponentDescription -- Student data     
                                  ,ISNULL(SCS.CreditsAttempted, 0) AS CreditsAttempted
                                  ,ISNULL(SCS.CreditsEarned, 0) AS CreditsEarned
                                  ,SCS.Completed AS Completed
                                  ,SCS.CurrentScore AS CurrentScore
                                  ,SCS.CurrentGrade AS CurrentGrade
                                  ,SCS.FinalScore AS FinalScore
                                  ,SCS.FinalGrade AS FinalGrade
                                  ,C.CampusId
                                  ,C.CampDescrip
                                  ,NULL AS rownumber
                                  ,S.FirstName AS FirstName
                                  ,S.LastName AS LastName
                                  ,S.MiddleName
                                  ,(
                                   SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                   FROM   arGrdBkResults
                                   WHERE  StuEnrollId = SE.StuEnrollId
                                          AND ClsSectionId = RES.TestId
                                   ) AS GrdBkWgtDetailsCount
                                  ,CASE WHEN P.ACId = 5 THEN ''True''
                                        ELSE ''False''
                                   END AS ClockHourProgram
                                  ,CS.ClsSectionId
                        FROM       arClassSections CS
                        INNER JOIN arResults GBR ON CS.ClsSectionId = GBR.TestId
                        INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
                        INNER JOIN (
                                   SELECT StudentId
                                         ,FirstName
                                         ,LastName
                                         ,MiddleName
                                   FROM   adLeads
                                   ) S ON S.StudentId = SE.StudentId
                        INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                        INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                        INNER JOIN arTerm T ON CS.TermId = T.TermId
                        INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                        INNER JOIN arResults RES ON RES.StuEnrollId = GBR.StuEnrollId
                                                    AND RES.TestId = CS.ClsSectionId
                        LEFT JOIN  syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                          AND T.TermId = SCS.TermId
                                                          AND R.ReqId = SCS.ReqId
                        WHERE      SE.StuEnrollId IN (
                                                     SELECT Val
                                                     FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                     )
                                   AND (
                                       @TermId IS NULL
                                       OR T.TermId = @TermId
                                       )
                                   AND R.IsAttendanceOnly = 0
                        UNION
                        SELECT     DISTINCT PV.PrgVerId
                                  ,PV.PrgVerDescrip
                                  ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                        ELSE 0
                                   END AS PrgVersionTrackCredits
                                  ,T.TermId
                                  ,T.TermDescrip
                                  ,T.StartDate
                                  ,T.EndDate
                                  ,GBCR.ReqId
                                  ,T.StartDate AS CouseStartDate     -- tranfered  
                                  ,R.Code AS CourseCode
                                  ,R.Descrip AS CourseDescrip
                                  ,''('' + R.Code + '') '' + R.Descrip AS CourseCodeDescrip
                                  ,R.Credits
                                  ,R.FinAidCredits
                                  ,(
                                   SELECT MIN(MinVal)
                                   FROM   arGradeScaleDetails GCD
                                         ,arGradeSystemDetails GSD
                                   WHERE  GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                          AND GSD.IsPass = 1
                                   )
                                  ,ISNULL(GBCR.Score, 0)
                                  ,NULL
                                  ,NULL
                                  ,NULL
                                  ,NULL
                                  ,NULL
                                  ,NULL
                                  ,NULL
                                  ,NULL
                                  ,NULL
                                  ,NULL
                                  ,NULL
                                  ,SE.StuEnrollId
                                  ,NULL AS MinResult
                                  ,NULL AS GradeComponentDescription -- Student data      
                                  ,ISNULL(SCS.CreditsAttempted, 0) AS CreditsAttempted
                                  ,ISNULL(SCS.CreditsEarned, 0) AS CreditsEarned
                                  ,SCS.Completed AS Completed
                                  ,SCS.CurrentScore AS CurrentScore
                                  ,SCS.CurrentGrade AS CurrentGrade
                                  ,SCS.FinalScore AS FinalScore
                                  ,SCS.FinalGrade AS FinalGrade
                                  ,C.CampusId
                                  ,C.CampDescrip
                                  ,NULL AS rownumber
                                  ,S.FirstName AS FirstName
                                  ,S.LastName AS LastName
                                  ,S.MiddleName
                                  ,(
                                   SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                   FROM   arGrdBkConversionResults
                                   WHERE  StuEnrollId = SE.StuEnrollId
                                          AND TermId = GBCR.TermId
                                          AND ReqId = GBCR.ReqId
                                   ) AS GrdBkWgtDetailsCount
                                  ,CASE WHEN P.ACId = 5 THEN ''True''
                                        ELSE ''False''
                                   END AS ClockHourProgram
                                  ,SCS.ClsSectionId
                        FROM       arTransferGrades GBCR
                        INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                        INNER JOIN (
                                   SELECT StudentId
                                         ,FirstName
                                         ,LastName
                                         ,MiddleName
                                   FROM   adLeads
                                   ) S ON S.StudentId = SE.StudentId
                        INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                        INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                        INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                        INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                        --INNER JOIN arTransferGrades AR ON GBCR.StuEnrollId = AR.StuEnrollId  
                        LEFT JOIN  syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                          AND T.TermId = SCS.TermId
                                                          AND R.ReqId = SCS.ReqId
                        WHERE      SE.StuEnrollId IN (
                                                     SELECT Val
                                                     FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                     )
                                   AND (
                                       @TermId IS NULL
                                       OR T.TermId = @TermId
                                       )
                                   AND R.IsAttendanceOnly = 0
                        ) dt
                 ) dt1
        --WHERE rownumber2<=2  
        ORDER BY TermStartDate
                ,TermEndDate
                -- , TermDescription  
                ,CouseStartDate
                ,CourseCode;
    --, CASE WHEN LTRIM(RTRIM(LOWER(@GradesFormat))) = ''letter''  
    --       THEN (RANK() OVER (ORDER BY FinalGrade DESC))  
    --  END  
    --, CASE WHEN LTRIM(RTRIM(LOWER(@GradesFormat))) <> ''letter''  
    --       THEN (RANK() OVER (ORDER BY FinalScore DESC))  
    --  END;  
    END;
-- ========================================================================================================= 
-- END  --  Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents 
-- ========================================================================================================= 

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]', 'P') IS NULL
EXEC sp_executesql N'-- =========================================================================================================
-- Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId
-- =========================================================================================================
CREATE PROCEDURE [dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]
    @StuEnrollIdList VARCHAR(MAX),
    @TermId VARCHAR(50) = NULL,
    @SysComponentTypeIdList VARCHAR(50) = NULL,
    @ReqId VARCHAR(50),
    @SysComponentTypeId VARCHAR(10) = NULL,
    @ShowIncomplete BIT = 1
AS
BEGIN
    DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
    DECLARE @SetGradeBookAt VARCHAR(50);
    DECLARE @CharInt INT;
    DECLARE @Counter AS INT;
    DECLARE @times AS INT;
    DECLARE @TermStartDate1 AS DATETIME;
    DECLARE @Score AS DECIMAL(18, 2);
    DECLARE @GrdCompDescrip AS VARCHAR(50);


    DECLARE @curId AS UNIQUEIDENTIFIER;
    DECLARE @curReqId AS UNIQUEIDENTIFIER;
    DECLARE @curStuEnrollId AS UNIQUEIDENTIFIER;
    DECLARE @curDescrip AS VARCHAR(50);
    DECLARE @curNumber AS INT;
    DECLARE @curGrdComponentTypeId AS INT;
    DECLARE @curMinResult AS DECIMAL(18, 2);
    DECLARE @curGrdComponentDescription AS VARCHAR(50);
    DECLARE @curClsSectionId AS UNIQUEIDENTIFIER;
    DECLARE @curRownumber AS INT;
		 DECLARE @curRowSeq AS INT;
    DECLARE @ExecutedSproc BIT = 0;

    CREATE TABLE #Temp21
    (
        Id UNIQUEIDENTIFIER,
        TermId UNIQUEIDENTIFIER,
        ReqId UNIQUEIDENTIFIER,
        GradeBookDescription VARCHAR(50),
        Number INT,
        GradeBookSysComponentTypeId INT,
        GradeBookScore DECIMAL(18, 2),
        MinResult DECIMAL(18, 2),
        GradeComponentDescription VARCHAR(50),
        ClsSectionId UNIQUEIDENTIFIER,
        StuEnrollId UNIQUEIDENTIFIER,
        RowNumber INT
			,Seq int
    );
    CREATE TABLE #Temp22
    (
        ReqId UNIQUEIDENTIFIER,
        EffectiveDate DATETIME
    );

    IF @SysComponentTypeIdList IS NULL
    BEGIN
        SET @SysComponentTypeIdList = ''501,544,502,499,503,500,533'';
    END;

    SET @StuEnrollCampusId = COALESCE(
                             (
                                 SELECT TOP 1
                                        ASE.CampusId
                                 FROM dbo.arStuEnrollments AS ASE
                                 WHERE EXISTS
                                 (
                                     SELECT Val
                                     FROM dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                     WHERE Val = ASE.StuEnrollId
                                 )
                             ),
                             NULL
                                     );
    SET @SetGradeBookAt = (dbo.GetAppSettingValueByKeyName(''GradeBookWeightingLevel'', @StuEnrollCampusId));
    IF (@SetGradeBookAt IS NOT NULL)
    BEGIN
        SET @SetGradeBookAt = LOWER(LTRIM(RTRIM(@SetGradeBookAt)));
    END;


    SET @CharInt =
    (
        SELECT CHARINDEX(@SysComponentTypeId, @SysComponentTypeIdList)
    );

    IF @SetGradeBookAt = ''instructorlevel''
    BEGIN
        SELECT CASE
                   WHEN GradeBookDescription IS NULL THEN
                       GradeComponentDescription
                   ELSE
                       GradeBookDescription
               END AS GradeBookDescription,
               MinResult,
               GradeBookScore,
               GradeComponentDescription,
               GradeBookSysComponentTypeId,
               StuEnrollId,
               rownumber
        FROM
        (
            SELECT AR2.ReqId,
                   AT.TermId,
                   CASE
                       WHEN AGBWD.Descrip IS NULL THEN
                           AGCT.Descrip
                       ELSE
                           AGBWD.Descrip
                   END AS GradeBookDescription,
                   (CASE
                        WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN
                            AGBWD.Number
                        ELSE
                    (
                        SELECT MIN(MinVal)
                        FROM arGradeScaleDetails GSD,
                             arGradeSystemDetails GSS
                        WHERE GSD.GrdSysDetailId = GSS.GrdSysDetailId
                              AND GSS.IsPass = 1
                              AND GSD.GrdScaleId = ACS.GrdScaleId
                    )
                    END
                   ) AS MinResult,
                   AGBWD.Required,
                   AGBWD.MustPass,
                   ISNULL(AGCT.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId,
                   AGBWD.Number,
                   SR.Resource AS GradeComponentDescription,
                   AGBWD.InstrGrdBkWgtDetailId,
                   ASE.StuEnrollId,
                   0 AS IsExternShip,
                   (CASE AGCT.SysComponentTypeId
                        WHEN 544 THEN
                        (
                            SELECT SUM(HoursAttended)
                            FROM arExternshipAttendance
                            WHERE StuEnrollId = ASE.StuEnrollId
                        )
                        ELSE
                    (
                        SELECT SUM(ISNULL(Score, 0))
                        FROM arGrdBkResults
                        WHERE StuEnrollId = ASE.StuEnrollId
                              AND InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                              AND ClsSectionId = ACS.ClsSectionId
                    )
                    END
                   ) AS GradeBookScore,
				   AGBWD.Seq,
                   ROW_NUMBER() OVER (PARTITION BY ASE.StuEnrollId,
                                                   AT.TermId,
                                                   AR2.ReqId
                                      ORDER BY AGCT.SysComponentTypeId,
                                               AGBWD.Descrip
                                     ) AS rownumber
            FROM arGrdBkWgtDetails AS AGBWD
                INNER JOIN arGrdBkWeights AS AGBW
                    ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                INNER JOIN arClassSections AS ACS
                    ON ACS.InstrGrdBkWgtId = AGBW.InstrGrdBkWgtId
                INNER JOIN arResults AS AR
                    ON AR.TestId = ACS.ClsSectionId
                INNER JOIN arGrdComponentTypes AS AGCT
                    ON AGCT.GrdComponentTypeId = AGBWD.GrdComponentTypeId
                INNER JOIN arStuEnrollments AS ASE
                    ON ASE.StuEnrollId = AR.StuEnrollId
                INNER JOIN arTerm AS AT
                    ON AT.TermId = ACS.TermId
                INNER JOIN arReqs AS AR2
                    ON AR2.ReqId = ACS.ReqId
                INNER JOIN syResources AS SR
                    ON SR.ResourceID = AGCT.SysComponentTypeId
                LEFT JOIN arGrdBkResults AS AGBR
                    ON AGBR.StuEnrollId = ASE.StuEnrollId
                       AND AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                       AND AGBR.ClsSectionId = ACS.ClsSectionId
            WHERE AR.StuEnrollId IN
                  (
                      SELECT Val
                      FROM MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                  )
                  AND AT.TermId = @TermId
                  AND AR2.ReqId = @ReqId
                  AND
                  (
                      @SysComponentTypeIdList IS NULL
                      OR AGCT.SysComponentTypeId IN
                         (
                             SELECT Val
                             FROM MultipleValuesForReportParameters(@SysComponentTypeIdList, '','', 1)
                         )
                  )
            UNION
            SELECT AR2.ReqId,
                   AT.TermId,
                   CASE
                       WHEN AGBW.Descrip IS NULL THEN
                       (
                           SELECT Resource
                           FROM syResources
                           WHERE ResourceID = AGCT.SysComponentTypeId
                       )
                       ELSE
                           AGBW.Descrip
                   END AS GradeBookDescription,
                   (CASE
                        WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN
                            AGBWD.Number
                        ELSE
                    (
                        SELECT MIN(MinVal)
                        FROM arGradeScaleDetails GSD,
                             arGradeSystemDetails GSS
                        WHERE GSD.GrdSysDetailId = GSS.GrdSysDetailId
                              AND GSS.IsPass = 1
                              AND GSD.GrdScaleId = ACS.GrdScaleId
                    )
                    END
                   ) AS MinResult,
                   AGBWD.Required,
                   AGBWD.MustPass,
                   ISNULL(AGCT.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId,
                   AGBWD.Number,
                   (
                       SELECT Resource
                       FROM syResources
                       WHERE ResourceID = AGCT.SysComponentTypeId
                   ) AS GradeComponentDescription,
                   AGBWD.InstrGrdBkWgtDetailId,
                   ASE.StuEnrollId,
                   0 AS IsExternShip,
                   (CASE AGCT.SysComponentTypeId
                        WHEN 544 THEN
                        (
                            SELECT SUM(HoursAttended)
                            FROM arExternshipAttendance
                            WHERE StuEnrollId = ASE.StuEnrollId
                        )
                        ELSE
                    (
                        SELECT SUM(ISNULL(Score, 0))
                        FROM arGrdBkResults
                        WHERE StuEnrollId = ASE.StuEnrollId
                              AND InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                              AND ClsSectionId = ACS.ClsSectionId
                    )
                    END
                   ) AS GradeBookScore,
				   AGBWD.seq,
                   ROW_NUMBER() OVER (PARTITION BY ASE.StuEnrollId,
                                                   AT.TermId,
                                                   AR2.ReqId
                                      ORDER BY AGCT.SysComponentTypeId,
                                               AGCT.Descrip
                                     ) AS rownumber
            FROM arGrdBkConversionResults GBCR
                INNER JOIN arStuEnrollments AS ASE
                    ON ASE.StuEnrollId = GBCR.StuEnrollId
                INNER JOIN adLeads AS AST
                    ON AST.StudentId = ASE.StudentId
                INNER JOIN arPrgVersions AS APV
                    ON APV.PrgVerId = ASE.PrgVerId
                INNER JOIN arTerm AS AT
                    ON AT.TermId = GBCR.TermId
                INNER JOIN arReqs AS AR2
                    ON AR2.ReqId = GBCR.ReqId
                INNER JOIN arGrdComponentTypes AS AGCT
                    ON AGCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWgtDetails AS AGBWD
                    ON AGBWD.GrdComponentTypeId = AGCT.GrdComponentTypeId
                       AND AGBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWeights AS AGBW
                    ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                       AND AGBW.ReqId = GBCR.ReqId
                INNER JOIN
                (
                    SELECT ReqId,
                           MAX(EffectiveDate) AS EffectiveDate
                    FROM arGrdBkWeights AS AGBW
                    GROUP BY ReqId
                ) AS MaxEffectiveDatesByCourse
                    ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                INNER JOIN
                (
                    SELECT Resource,
                           ResourceID
                    FROM syResources
                    WHERE ResourceTypeID = 10
                ) SYRES
                    ON SYRES.ResourceID = AGCT.SysComponentTypeId
                INNER JOIN syCampuses AS SC
                    ON SC.CampusId = ASE.CampusId
                INNER JOIN arClassSections AS ACS
                    ON ACS.TermId = AT.TermId
                       AND ACS.ReqId = AR2.ReqId
                LEFT JOIN arGrdBkResults AS AGBR
                    ON AGBR.StuEnrollId = ASE.StuEnrollId
                       AND AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                       AND AGBR.ClsSectionId = ACS.ClsSectionId
            WHERE MaxEffectiveDatesByCourse.EffectiveDate <= AT.StartDate
                  AND ASE.StuEnrollId IN
                      (
                          SELECT Val
                          FROM MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                      )
                  AND AT.TermId = @TermId
                  AND AR2.ReqId = @ReqId
                  AND
                  (
                      @SysComponentTypeIdList IS NULL
                      OR AGCT.SysComponentTypeId IN
                         (
                             SELECT Val
                             FROM MultipleValuesForReportParameters(@SysComponentTypeIdList, '','', 1)
                         )
                  )
        ) dt
        WHERE (
                  @SysComponentTypeId IS NULL
                  OR dt.GradeBookSysComponentTypeId = @SysComponentTypeId
              )
              AND
              (
                  @ShowIncomplete = 1
                  OR
                  (
                      @ShowIncomplete = 0
                      AND dt.GradeBookScore IS NOT NULL
                  )
              )
        ORDER BY seq,GradeComponentDescription,
                 rownumber,
                 GradeBookDescription;
    END;
    ELSE
    BEGIN

        SET @TermStartDate1 =
        (
            SELECT AT.StartDate FROM dbo.arTerm AS AT WHERE AT.TermId = @TermId
        );
        INSERT INTO #Temp22
        SELECT AGBW.ReqId,
               MAX(AGBW.EffectiveDate) AS EffectiveDate
        FROM dbo.arGrdBkWeights AS AGBW
            INNER JOIN dbo.syCreditSummary AS SCS
                ON SCS.ReqId = AGBW.ReqId
        WHERE SCS.TermId = @TermId
              AND SCS.StuEnrollId IN
                  (
                      SELECT Val
                      FROM dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                  )
              AND AGBW.EffectiveDate <= @TermStartDate1
        GROUP BY AGBW.ReqId;


        DECLARE getUsers_Cursor CURSOR FOR
        SELECT dt.ID,
               dt.ReqId,
               dt.Descrip,
               dt.Number,
               dt.SysComponentTypeId,
               dt.MinResult,
               dt.GradeComponentDescription,
               dt.ClsSectionId,
               dt.StuEnrollId,
               ROW_NUMBER() OVER (PARTITION BY dt.StuEnrollId,
                                               @TermId,
                                               dt.SysComponentTypeId
                                  ORDER BY dt.SysComponentTypeId,
                                           dt.Descrip
                                 ) AS rownumber,
								 dt.Seq
        FROM
        (
            SELECT ISNULL(AGBWD.InstrGrdBkWgtDetailId, NEWID()) AS ID,
                   AR.ReqId,
                   AGBWD.Descrip,
                   AGBWD.Number,
                   AGCT.SysComponentTypeId,
                   (CASE
                        WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN
                            AGBWD.Number
                        ELSE
                    (
                        SELECT MIN(AGSD.MinVal) AS MinVal
                        FROM dbo.arGradeScaleDetails AS AGSD
                            INNER JOIN dbo.arGradeSystemDetails AS AGSDetails
                                ON AGSDetails.GrdSysDetailId = AGSD.GrdSysDetailId
                        WHERE AGSDetails.IsPass = 1
                              AND AGSD.GrdScaleId = ACS.GrdScaleId
                    )
                    END
                   ) AS MinResult,
                   SR.Resource AS GradeComponentDescription,
                   ACS.ClsSectionId,
                   AGBR.StuEnrollId,
				   AGBWD.Seq
            FROM dbo.arGrdBkResults AS AGBR
                INNER JOIN dbo.arGrdBkWgtDetails AS AGBWD
                    ON AGBWD.InstrGrdBkWgtDetailId = AGBR.InstrGrdBkWgtDetailId
                INNER JOIN dbo.arGrdBkWeights AS AGBW
                    ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                INNER JOIN dbo.arGrdComponentTypes AS AGCT
                    ON AGCT.GrdComponentTypeId = AGBWD.GrdComponentTypeId
                INNER JOIN dbo.syResources AS SR
                    ON SR.ResourceID = AGCT.SysComponentTypeId
                --                             INNER JOIN #Temp22 AS T2 ON T2.ReqId = AGBW.ReqId
                INNER JOIN dbo.arReqs AS AR
                    ON AR.ReqId = AGBW.ReqId
                INNER JOIN dbo.arClassSections AS ACS
                    ON ACS.ReqId = AR.ReqId
                       AND ACS.ClsSectionId = AGBR.ClsSectionId
                INNER JOIN dbo.arResults AS AR2
                    ON AR2.TestId = ACS.ClsSectionId
                       AND AR2.StuEnrollId = AGBR.StuEnrollId --AND AR2.TestId = AGBR.ClsSectionId

            WHERE (
                      @StuEnrollIdList IS NULL
                      OR AGBR.StuEnrollId IN
                         (
                             SELECT Val
                             FROM dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                         )
                  )
                  --                                        AND T2.EffectiveDate = AGBW.EffectiveDate
                  AND AGBWD.Number > 0
                  AND AR.ReqId = @ReqId
        ) dt
        ORDER BY SysComponentTypeId,
                 rownumber;

        DECLARE @schoolTypeSetting VARCHAR(50);
        SET @schoolTypeSetting = dbo.GetAppSettingValueByKeyName(''gradesformat'', NULL);


        OPEN getUsers_Cursor;
        FETCH NEXT FROM getUsers_Cursor
        INTO @curId,
             @curReqId,
             @curDescrip,
             @curNumber,
             @curGrdComponentTypeId,
             @curMinResult,
             @curGrdComponentDescription,
             @curClsSectionId,
             @curStuEnrollId,
             @curRownumber,
			 @curRowSeq
        SET @Counter = 0;
        WHILE @@FETCH_STATUS = 0
        BEGIN
            --PRINT @Number; 

            IF @schoolTypeSetting = ''Numeric''
            BEGIN
                SET @times = 1;

                SET @GrdCompDescrip = @curDescrip;
                IF (@curGrdComponentTypeId <> 500 AND @curGrdComponentTypeId <> 503)
                BEGIN
                    SET @Score =
                    (
                        SELECT TOP 1
                               Score
                        FROM arGrdBkResults
                        WHERE StuEnrollId = @curStuEnrollId
                              AND InstrGrdBkWgtDetailId = @curId
                              AND ResNum = @times
                    );
                    IF @Score IS NULL
                    BEGIN
                        SET @Score =
                        (
                            SELECT TOP 1
                                   Score
                            FROM arGrdBkResults
                            WHERE StuEnrollId = @curStuEnrollId
                                  AND InstrGrdBkWgtDetailId = @curId
                                  AND ResNum = (@times - 1)
                        );
                    END;

                    IF @curGrdComponentTypeId = 544
                    BEGIN
                        SET @Score =
                        (
                            SELECT SUM(HoursAttended)
                            FROM arExternshipAttendance
                            WHERE StuEnrollId = @curStuEnrollId
                        );
                    END;
                    INSERT INTO #Temp21
                    VALUES
                    (@curId, @TermId, @curReqId, @GrdCompDescrip, @curNumber, @curGrdComponentTypeId, @Score,
                     @curMinResult, @curGrdComponentDescription, @curClsSectionId, @curStuEnrollId, @curRownumber, @curRowSeq);
                END;
                ELSE
                BEGIN
                    IF (@curGrdComponentTypeId = 500 OR @curGrdComponentTypeId = 503)
                       AND @ExecutedSproc = 0
                    BEGIN


                        DECLARE @Services TABLE
                        (
                            Descrip VARCHAR(50),
                            Required DECIMAL(18, 2) NULL,
                            Completed INT NULL,
                            Remaining DECIMAL(18, 2) NULL,
                            Average DECIMAL(18, 2) NULL,
                            EnrollDate DATETIME NULL,
                            InstrGrdBkWgtDetailId UNIQUEIDENTIFIER,
                            ClsSectionId UNIQUEIDENTIFIER,
							Seq INT NULL
                        );


                        INSERT INTO @Services
                        (
                            Descrip,
                            Required,
                            Completed,
                            Remaining,
                            Average,
                            EnrollDate,
                            InstrGrdBkWgtDetailId,
                            ClsSectionId,
							Seq
                        )
                        EXEC dbo.usp_GetClassroomWorkServices @StuEnrollId = @curStuEnrollId,   -- uniqueidentifier
                                                              @ClsSectionId = @curClsSectionId, -- uniqueidentifier
                                                              @GradeBookComponentType = @curGrdComponentTypeId;    -- int



                        INSERT INTO #Temp21
                        SELECT @curId,
                               @TermId,
                               @curReqId,
                                LTRIM(RTRIM(Descrip)) + '' ('' + CAST(Completed AS VARCHAR(10)) + ''/''
                               +( CASE WHEN @curGrdComponentTypeId = 500 THEN CAST(CONVERT(INT,Required)  AS VARCHAR(10)) ELSE CAST(Required  AS VARCHAR(10)) END) + '')'',
                               @curNumber,
                               @curGrdComponentTypeId,
                               Average,
                               [Required],
                               @curGrdComponentDescription,
                               @curClsSectionId,
                               @curStuEnrollId,
                               @curRownumber,
							   Seq
                        FROM @Services;


                        SET @ExecutedSproc = 1;
                    END;
                END;



            END;
            ELSE
            BEGIN

                SET @times = 1;
                WHILE @times <= @curNumber
                BEGIN
                    --PRINT @times; 
                    IF @curNumber > 1
                    BEGIN
                        SET @GrdCompDescrip = @curDescrip + CAST(@times AS CHAR);

                        IF @curGrdComponentTypeId = 544
                        BEGIN
                            SET @Score =
                            (
                                SELECT SUM(HoursAttended)
                                FROM arExternshipAttendance
                                WHERE StuEnrollId = @curStuEnrollId
                            );
                        END;
                        ELSE
                        BEGIN
                            SET @Score =
                            (
                                SELECT Score
                                FROM arGrdBkResults
                                WHERE StuEnrollId = @curStuEnrollId
                                      AND InstrGrdBkWgtDetailId = @curId
                                      AND ResNum = @times
                                      AND ClsSectionId = @curClsSectionId
                            );
                        END;


                        SET @curRownumber = @times;
                    END;
                    ELSE
                    BEGIN
                        SET @GrdCompDescrip = @curDescrip;
                        SET @Score =
                        (
                            SELECT TOP 1
                                   Score
                            FROM arGrdBkResults
                            WHERE StuEnrollId = @curStuEnrollId
                                  AND InstrGrdBkWgtDetailId = @curId
                                  AND ResNum = @times
                        );
                        IF @Score IS NULL
                        BEGIN
                            SET @Score =
                            (
                                SELECT TOP 1
                                       Score
                                FROM arGrdBkResults
                                WHERE StuEnrollId = @curStuEnrollId
                                      AND InstrGrdBkWgtDetailId = @curId
                                      AND ResNum = (@times - 1)
                            );
                        END;

                        IF @curGrdComponentTypeId = 544
                        BEGIN
                            SET @Score =
                            (
                                SELECT SUM(HoursAttended)
                                FROM arExternshipAttendance
                                WHERE StuEnrollId = @curStuEnrollId
                            );
                        END;

                    END;
                    --PRINT @Score; 
                    INSERT INTO #Temp21
                    VALUES
                    (@curId, @TermId, @curReqId, @GrdCompDescrip, @curNumber, @curGrdComponentTypeId, @Score,
                     @curMinResult, @curGrdComponentDescription, @curClsSectionId, @curStuEnrollId, @curRownumber);

                    SET @times = @times + 1;
                END;

            END;
            FETCH NEXT FROM getUsers_Cursor
            INTO @curId,
                 @curReqId,
                 @curDescrip,
                 @curNumber,
                 @curGrdComponentTypeId,
                 @curMinResult,
                 @curGrdComponentDescription,
                 @curClsSectionId,
                 @curStuEnrollId,
                 @curRownumber,
				 @curRowSeq
        END;
        CLOSE getUsers_Cursor;
        DEALLOCATE getUsers_Cursor;

        SET @CharInt =
        (
            SELECT CHARINDEX(@SysComponentTypeId, @SysComponentTypeIdList)
        );

        IF (@SysComponentTypeIdList IS NULL)
           OR (@CharInt >= 1)
        BEGIN
            SELECT Id,
                   --, StuEnrollId 
                   TermId,
                   GradeBookDescription,
                   Number,
                   GradeBookSysComponentTypeId,
                   GradeBookScore,
                   MinResult,
                   GradeComponentDescription,
                   RowNumber,
                   ClsSectionId
				      ,Seq
            FROM #Temp21
            WHERE GradeBookSysComponentTypeId = @SysComponentTypeId
			AND
              (
                  @ShowIncomplete = 1
                  OR
                  (
                      @ShowIncomplete = 0
                      AND GradeBookScore IS NOT NULL
                  )
              )
            UNION
            SELECT GBWD.InstrGrdBkWgtDetailId,
                   --, SE.StuEnrollId 
                   T.TermId,
                   GCT.Descrip AS GradeBookDescription,
                   GBWD.Number,
                   GCT.SysComponentTypeId,
                   GBCR.Score,
                   GBCR.MinResult,
                   SYRES.Resource,
                   ROW_NUMBER() OVER (PARTITION BY --SE.StuEnrollId,  
                                          T.TermId,
                                          GCT.SysComponentTypeId
                                      ORDER BY GCT.SysComponentTypeId,
                                               GCT.Descrip
                                     ) AS rownumber,
                   (
                       SELECT TOP 1
                              ClsSectionId
                       FROM arClassSections
                       WHERE TermId = T.TermId
                             AND ReqId = R.ReqId
                   ) AS ClsSectionId
				      ,GBWD.Seq
            FROM arGrdBkConversionResults GBCR
                INNER JOIN arStuEnrollments SE
                    ON GBCR.StuEnrollId = SE.StuEnrollId
                INNER JOIN
                (SELECT StudentId, FirstName, LastName, MiddleName FROM adLeads) S
                    ON S.StudentId = SE.StudentId
                INNER JOIN arPrgVersions PV
                    ON SE.PrgVerId = PV.PrgVerId
                INNER JOIN arTerm T
                    ON GBCR.TermId = T.TermId
                INNER JOIN arReqs R
                    ON GBCR.ReqId = R.ReqId
                INNER JOIN arGrdComponentTypes GCT
                    ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWgtDetails GBWD
                    ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                       AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWeights GBW
                    ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                       AND GBCR.ReqId = GBW.ReqId
                INNER JOIN
                (
                    SELECT ReqId,
                           MAX(EffectiveDate) AS EffectiveDate
                    FROM arGrdBkWeights
                    GROUP BY ReqId
                ) AS MaxEffectiveDatesByCourse
                    ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                INNER JOIN
                (
                    SELECT Resource,
                           ResourceID
                    FROM syResources
                    WHERE ResourceTypeID = 10
                ) SYRES
                    ON SYRES.ResourceID = GCT.SysComponentTypeId
                INNER JOIN syCampuses C
                    ON SE.CampusId = C.CampusId
            WHERE MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                  AND SE.StuEnrollId IN
                      (
                          SELECT Val
                          FROM MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                      )
                  AND T.TermId = @TermId
                  AND R.ReqId = @ReqId
                  AND
                  (
                      @SysComponentTypeIdList IS NULL
                      OR GCT.SysComponentTypeId IN
                         (
                             SELECT Val
                             FROM MultipleValuesForReportParameters(@SysComponentTypeIdList, '','', 1)
                         )
                  )
                  AND
                  (
                      @SysComponentTypeId IS NULL
                      OR GCT.SysComponentTypeId = @SysComponentTypeId
                  )
				  AND
              (
                  @ShowIncomplete = 1
                  OR
                  (
                      @ShowIncomplete = 0
                      AND GBCR.Score IS NOT NULL
                  )
              )
            ORDER BY GradeBookSysComponentTypeId,
			seq,
                     GradeBookDescription,
                     RowNumber;
        END;
    END;
    DROP TABLE #Temp22;
    DROP TABLE #Temp21;
END;
-- =========================================================================================================
-- END  --  Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId
-- =========================================================================================================






'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_ProcessPaymentPeriods]'
GO
IF OBJECT_ID(N'[dbo].[USP_ProcessPaymentPeriods]', 'P') IS NULL
EXEC sp_executesql N'--=================================================================================================
-- USP_ProcessPaymentPeriods
--=================================================================================================
-- =============================================
-- Author: Ginzo, John
-- Create date: 11/13/2014
-- Description: Create Payment Period Batch and post to ledger
-- =============================================
CREATE PROCEDURE [dbo].[USP_ProcessPaymentPeriods]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;


        BEGIN TRAN PAYMENTPERIODTRANSACTION;


        ------------------------------------------------------------------------------------------------------------
        ----Table to store Student population
        -- @MasterStudentPopulation table - RowNumber, RunningCreditsEarned, RunningCreditsAttempted columns added
        ------------------------------------------------------------------------------------------------------------
        DECLARE @MasterStudentPopulationSummary TABLE
            (
                RowNumber INT
               ,StudentId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,PrgVerCode VARCHAR(250)
               ,ClsSection VARCHAR(250)
               ,TermId UNIQUEIDENTIFIER
               ,StartDate DATETIME
               ,EndDate DATETIME
               ,Code VARCHAR(250)
               ,Credits DECIMAL
               ,CreditsAttempted DECIMAL
               ,CreditsEarned DECIMAL
               ,IsCourseCompleted BIT
               ,IsCreditsAttempted BIT
               ,IsCreditsEarned BIT
            );


        DECLARE @MasterStudentPopulation TABLE
            (
                RowNumber INT
               ,StudentId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,PrgVerCode VARCHAR(250)
               ,ClsSection VARCHAR(250)
               ,TermId UNIQUEIDENTIFIER
               ,StartDate DATETIME
               ,EndDate DATETIME
               ,Code VARCHAR(250)
               ,Credits DECIMAL
               ,CreditsAttempted DECIMAL
               ,CreditsEarned DECIMAL
               ,IsCourseCompleted BIT
               ,IsCreditsAttempted BIT
               ,IsCreditsEarned BIT
               ,RunningCreditsEarned DECIMAL(18, 2)
               ,RunningCreditsAttempted DECIMAL(18, 2)
            );


        -----------------------------------------------------------------
        -- Insert student population into table
        -----------------------------------------------------------------
        INSERT INTO @MasterStudentPopulationSummary
                    SELECT   ROW_NUMBER() OVER (PARTITION BY SE.StuEnrollId
                                                ORDER BY SE.StuEnrollId
                                               ) AS RowNumber
                            ,SE.StudentId
                            ,SE.StuEnrollId
                            ,PV.PrgVerId
                            ,PV.PrgVerCode
                            ,CS.ClsSection
                            ,tm.TermId
                            ,tm.StartDate
                            ,tm.EndDate
                            ,R.Code
                            ,R.Credits
                            --CASE WHEN IsCreditsAttempted=1 THEN r.credits ELSE 0 END AS CreditsAttempted,
                            ,CASE WHEN CS.StartDate <= GETDATE() THEN R.Credits
                                  ELSE 0
                             END AS CreditsAttempted
                            ,CASE WHEN IsCreditsEarned = 1 THEN R.Credits
                                  ELSE 0
                             END AS CreditsEarned
                            ,RES.IsCourseCompleted
                            ,CASE WHEN CS.StartDate <= GETDATE() THEN 1
                                  ELSE 0
                             END AS IsCreditsAttempted
                            ,g.IsCreditsEarned
                    FROM     arStuEnrollments SE
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                    INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                    INNER JOIN dbo.arTerm tm ON CS.TermId = tm.TermId
                    LEFT OUTER JOIN dbo.arGradeSystemDetails g ON RES.GrdSysDetailId = g.GrdSysDetailId
                    INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                    INNER JOIN dbo.syStatuses ST ON PV.StatusId = ST.StatusId
                    WHERE    SC.SysStatusId IN ( 9, 20 )
					--removed due to this condition being required for by term or course charges and auto post is for payment period charging
                             --AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(SE.StuEnrollId, RES.TestId) --Added by Troy as part of fix for US8005
                             AND SE.DisableAutoCharge <> 1 --Exclude enrollments with auto charge disabled 
					--AND IsCourseCompleted=1
                    --AND SE.StuEnrollId=''5182EBE8-DF7A-47A9-8B48-3F2949EA56E4''
                    ORDER BY SE.StuEnrollId
                            ,tm.StartDate;






        -----------------------------------------------------------------
        -- Insert student population into table for transfer grades
        -----------------------------------------------------------------
        INSERT INTO @MasterStudentPopulationSummary
                    SELECT ROW_NUMBER() OVER (PARTITION BY SE.StuEnrollId
                                              ORDER BY SE.StuEnrollId
                                             ) AS RowNumber
                          ,SE.StudentId
                          ,SE.StuEnrollId
                          ,PV.PrgVerId
                          ,PV.PrgVerCode
                          ,NULL AS ClsSection
                          ,tm.TermId
                          ,tm.StartDate
                          ,tm.EndDate
                          ,R.Code
                          ,R.Credits
                          --CASE WHEN IsCreditsAttempted=1 THEN r.credits ELSE 0 END AS CreditsAttempted,
                          ,CASE WHEN tm.StartDate <= GETDATE() THEN R.Credits
                                ELSE 0
                           END AS CreditsAttempted
                          ,CASE WHEN IsCreditsEarned = 1 THEN R.Credits
                                ELSE 0
                           END AS CreditsEarned
                          ,RES.IsCourseCompleted
                          --g.IsCreditsAttempted,
                          ,CASE WHEN tm.StartDate <= GETDATE() THEN 1
                                ELSE 0
                           END AS IsCreditsAttempted
                          ,g.IsCreditsEarned
                    FROM   arStuEnrollments SE
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                    INNER JOIN dbo.arTerm tm ON RES.TermId = tm.TermId
                    LEFT OUTER JOIN dbo.arGradeSystemDetails g ON RES.GrdSysDetailId = g.GrdSysDetailId
                    INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                    INNER JOIN dbo.syStatuses ST ON PV.StatusId = ST.StatusId
                    WHERE  SC.SysStatusId IN ( 9, 20 )
						   --removed due to this condition being required for by term or course charges and auto post is for payment period charging
                           --AND 1 = dbo.UDF_ShouldCourseBeChargedOnThisEnrollment(SE.StuEnrollId, RES.ReqId) --Added by Troy as part of fix for US8005
        				   AND SE.DisableAutoCharge <> 1 --Exclude enrollments with auto charge disabled 
		--AND IsCourseCompleted=1 
        --AND SE.StuEnrollId=''5182EBE8-DF7A-47A9-8B48-3F2949EA56E4''
        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error creating Student Population.'', 16, 1);
                RETURN;
            END;
        -----------------------------------------------------------------






        INSERT INTO @MasterStudentPopulation
                    SELECT *
                          ,(
                           SELECT SUM(CreditsEarned)
                           FROM   @MasterStudentPopulationSummary
                           WHERE  StuEnrollId = a.StuEnrollId
                                  AND RowNumber <= a.RowNumber
                           ) AS RunningCreditsEarned
                          ,(
                           SELECT SUM(CreditsAttempted)
                           FROM   @MasterStudentPopulationSummary
                           WHERE  StuEnrollId = a.StuEnrollId
                                  AND RowNumber <= a.RowNumber
                           ) AS RunningCreditsAttempted
                    FROM   @MasterStudentPopulationSummary a;
        --WHERE a.StuEnrollId=''4E0018F6-7EEA-41F4-A04B-09EC018ACCF8''




        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error creating Student Population.'', 16, 1);
                RETURN;
            END;
        -----------------------------------------------------------------


        -----------------------------------------------------------------
        --Table to store sum of credits
        -----------------------------------------------------------------
        DECLARE @CreditAttemptedEarned TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,CreditsAttempted DECIMAL
               ,CreditsEarned DECIMAL
            );
        -----------------------------------------------------------------




        -----------------------------------------------------------------
        -- GET sum of credits for students
        -----------------------------------------------------------------
        INSERT INTO @CreditAttemptedEarned
                    SELECT   T.StuEnrollId
                            ,T.PrgVerId
                            ,SUM(T.CreditsAttempted) AS TotalCreditsAttempted
                            ,SUM(T.CreditsEarned) AS TotalCreditsEarned
                    FROM     @MasterStudentPopulation T
                    GROUP BY T.StuEnrollId
                            ,T.PrgVerId
                    ORDER BY T.PrgVerId;


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error creating Student credits summary.'', 16, 1);
                RETURN;
            END;
        -----------------------------------------------------------------


        -----------------------------------------------------------------
        -- CREATE Batch Header record for this batch
        -----------------------------------------------------------------
        DECLARE @BatchNumber INT;
        SET @BatchNumber = (
                           SELECT ISNULL(MAX(PmtPeriodBatchHeaderId), 0) + 1
                           FROM   saPmtPeriodBatchHeaders
                           );


        INSERT INTO saPmtPeriodBatchHeaders (
                                            BatchName
                                            )
                    SELECT ''Batch '' + CAST(@BatchNumber AS VARCHAR(20)) + '' (Credits) - '' + LEFT(CONVERT(VARCHAR, GETDATE(), 101), 10) + '' ''
                           + LEFT(CONVERT(VARCHAR, GETDATE(), 108), 10);
        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error creating Batch header.'', 16, 1);
                RETURN;
            END;


        DECLARE @InsertedHeaderId INT;
        SET @InsertedHeaderId = SCOPE_IDENTITY();
        -----------------------------------------------------------------
        -----------------------------------------------------------------
        -- Get Students that satisfy requirements of payment periods and
        -- insert into batch table
        -----------------------------------------------------------------
        INSERT INTO saPmtPeriodBatchItems (
                                          PmtPeriodId
                                         ,PmtPeriodBatchHeaderId
                                         ,StuEnrollId
                                         ,ChargeAmount
                                         ,CreditsHoursValue
                                         ,TransactionReference
                                         ,TransactionDescription
                                         ,ModUser
                                         ,ModDate
                                         ,TermId
                                          )
                    SELECT DISTINCT (p.PmtPeriodId)
                          ,@InsertedHeaderId
                          ,e.StuEnrollId
                          ,p.ChargeAmount
                          ,(CASE WHEN i.IncrementType = 2 THEN CAE.CreditsAttempted
                                 WHEN i.IncrementType = 3 THEN CAE.CreditsEarned
                                 ELSE 0
                            END
                           ) AS CreditsHoursValue
                          ,''Payment Period '' + CAST(p.PeriodNumber AS VARCHAR(5)) AS TransactionReference
                          ,''Tuition: '' + CAST(p.CumulativeValue AS VARCHAR(20)) + '' '' + i.IncrementName AS TransactionDescription
                          ,''AutoPost'' AS ModUser
                          ,GETDATE() AS ModDate
                          ,(CASE WHEN i.IncrementType = 2 THEN (
                                                               SELECT TOP 1 TermId
                                                               FROM   @MasterStudentPopulation
                                                               WHERE  StuEnrollId = e.StuEnrollId
                                                                      AND RunningCreditsAttempted >= p.CumulativeValue
                                                               )
                                 WHEN i.IncrementType = 3 THEN (
                                                               SELECT TOP 1 TermId
                                                               FROM   @MasterStudentPopulation
                                                               WHERE  StuEnrollId = e.StuEnrollId
                                                                      AND RunningCreditsEarned >= p.CumulativeValue
                                                               )
                                 ELSE NULL
                            END
                           ) AS TermId
                    FROM   saPmtPeriods p
                    INNER JOIN dbo.saIncrements i ON p.IncrementId = i.IncrementId
                    INNER JOIN dbo.saBillingMethods B ON i.BillingMethodId = B.BillingMethodId
                    INNER JOIN dbo.arPrgVersions prg ON B.BillingMethodId = prg.BillingMethodId
                    INNER JOIN dbo.arStuEnrollments e ON prg.PrgVerId = e.PrgVerId
                    INNER JOIN dbo.arStudent st ON e.StudentId = st.StudentId
                    INNER JOIN @CreditAttemptedEarned CAE ON e.StuEnrollId = CAE.StuEnrollId
                    WHERE  B.BillingMethod = 3
                           AND e.StartDate >= i.EffectiveDate
                           AND e.StuEnrollId NOT IN (
                                                    SELECT StuEnrollId
                                                    FROM   dbo.saTransactions
                                                    WHERE  StuEnrollId = e.StuEnrollId
                                                           AND PmtPeriodId = p.PmtPeriodId
                                                    )
                           AND e.StuEnrollId NOT IN (
                                                    SELECT StuEnrollId
                                                    FROM   dbo.saPmtPeriodBatchItems i
                                                    INNER JOIN saPmtPeriodBatchHeaders h ON i.PmtPeriodBatchHeaderId = h.PmtPeriodBatchHeaderId
                                                    WHERE  StuEnrollId = e.StuEnrollId
                                                           AND PmtPeriodId = p.PmtPeriodId
                                                           AND h.IsPosted = 0
                                                    )
                           AND (CASE WHEN i.IncrementType = 2 THEN CAE.CreditsAttempted
                                     WHEN i.IncrementType = 3 THEN CAE.CreditsEarned
                                     ELSE 0
                                END
                               ) >= p.CumulativeValue;


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error creating batch items.'', 16, 1);
                RETURN;
            END;


        -----------------------------------------------------------------




        -----------------------------------------------------------------
        -- UPDATE Batch Item count in header table
        -----------------------------------------------------------------
        UPDATE saPmtPeriodBatchHeaders
        SET    BatchItemCount = (
                                SELECT COUNT(*)
                                FROM   saPmtPeriodBatchHeaders h
                                INNER JOIN saPmtPeriodBatchItems i ON h.PmtPeriodBatchHeaderId = i.PmtPeriodBatchHeaderId
                                WHERE  h.PmtPeriodBatchHeaderId = @InsertedHeaderId
                                )
        WHERE  PmtPeriodBatchHeaderId = @InsertedHeaderId;


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error updating batch count.'', 16, 1);
                RETURN;
            END;
        -----------------------------------------------------------------
        -----------------------------------------------------------------
        -- GET AutoPost config setting
        -----------------------------------------------------------------
        DECLARE @IsAutoPost VARCHAR(50);
        SET @IsAutoPost = (
                          SELECT TOP 1 v.Value
                          FROM   dbo.syConfigAppSettings c
                          INNER JOIN dbo.syConfigAppSetValues v ON c.SettingId = v.SettingId
                          WHERE  c.KeyName = ''PaymentPeriodAutoPost''
                          );
        -----------------------------------------------------------------


        -----------------------------------------------------------------
        -- INSERT Transactions from Batch
        -----------------------------------------------------------------
        IF @IsAutoPost = ''yes''
            BEGIN
                IF OBJECTPROPERTY(OBJECT_ID(''Trigger_Insert_Check_saTransactions''), ''IsTrigger'') = 1
                    BEGIN
                        ALTER TABLE dbo.saTransactions DISABLE TRIGGER Trigger_Insert_Check_saTransactions;
                    END;


                INSERT INTO dbo.saTransactions (
                                               StuEnrollId
                                              ,TermId
                                              ,CampusId
                                              ,TransDate
                                              ,TransCodeId
                                              ,TransReference
                                              ,AcademicYearId
                                              ,TransDescrip
                                              ,TransAmount
                                              ,TransTypeId
                                              ,IsPosted
                                              ,CreateDate
                                              ,BatchPaymentId
                                              ,ViewOrder
                                              ,IsAutomatic
                                              ,ModUser
                                              ,ModDate
                                              ,Voided
                                              ,FeeLevelId
                                              ,FeeId
                                              ,PaymentCodeId
                                              ,FundSourceId
                                              ,StuEnrollPayPeriodId
                                              ,DisplaySequence
                                              ,SecondDisplaySequence
                                              ,PmtPeriodId
                                               )
                            SELECT   BI.StuEnrollId AS StuEnrollId
                                    ,BI.TermId AS TermId
                                    ,SE.CampusId
                                    ,GETDATE() AS TransDate
                                    ,(
                                     SELECT TOP 1 TransCodeId
                                     FROM   dbo.saTransCodes
                                     WHERE  TransCodeCode = ''AUTOPOSTTUIT''
                                     ) AS TranscodeId
                                    ,BI.TransactionReference
                                    ,NULL AS AcademicYearId
                            --??? HOW TO GET THIS?,
                                    ,BI.TransactionDescription
                                    ,BI.ChargeAmount
                                    ,(
                                     SELECT TOP 1 TransTypeId
                                     FROM   dbo.saTransTypes
                                     WHERE  Description = ''Charge''
                                     ) AS TransTypeId
                                    ,1 AS IsPosted
                                    ,GETDATE() AS CreatedDate
                                    ,NULL AS BatchPaymentId
                                    ,NULL AS ViewOrder
                                    ,0 AS IsAutomatic
                                    ,''AutoPost'' AS ModUser
                                    ,GETDATE() AS ModDate
                                    ,0 AS Voided
                                    ,NULL AS FeeLevelId
                                    ,NULL AS FeeId
                                    ,NULL AS PaymentCodeId
                                    ,NULL AS FundSourceId
                                    ,NULL AS StuEnrollPayPeriodId
                                    ,NULL AS DisplaySequence
                                    ,NULL AS SecondDisplaySequence
                                    ,BI.PmtPeriodId AS PmtPeriodId
                            FROM     saPmtPeriodBatchItems BI
                            INNER JOIN saPmtPeriodBatchHeaders BH ON BI.PmtPeriodBatchHeaderId = BH.PmtPeriodBatchHeaderId
                            INNER JOIN dbo.arStuEnrollments SE ON BI.StuEnrollId = SE.StuEnrollId
                            WHERE    BH.PmtPeriodBatchHeaderId = @InsertedHeaderId
                                     AND BI.IncludedInPost = 1
                                     AND SE.StuEnrollId NOT IN (
                                                               SELECT StuEnrollId
                                                               FROM   dbo.saTransactions
                                                               WHERE  StuEnrollId = SE.StuEnrollId
                                                                      AND PmtPeriodId = BI.PmtPeriodId
                                                               )
                            ORDER BY SE.StuEnrollId
                                    ,BI.TransactionReference;


                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error inserting transactions.'', 16, 1);
                        RETURN;
                    END;


                IF OBJECTPROPERTY(OBJECT_ID(''Trigger_Insert_Check_saTransactions''), ''IsTrigger'') = 1
                    BEGIN
                        ALTER TABLE dbo.saTransactions ENABLE TRIGGER Trigger_Insert_Check_saTransactions;
                    END;


                -----------------------------------------------------------------
                -- update batch items with the transaction id from the ledger
                -----------------------------------------------------------------
                UPDATE saPmtPeriodBatchItems
                SET    TransactionId = T.TransactionId
                FROM   dbo.saTransactions T
                INNER JOIN dbo.saPmtPeriodBatchItems BI ON T.PmtPeriodId = BI.PmtPeriodId
                                                           AND T.StuEnrollId = BI.StuEnrollId
                INNER JOIN dbo.saPmtPeriodBatchHeaders BH ON BI.PmtPeriodBatchHeaderId = BH.PmtPeriodBatchHeaderId
                WHERE  BH.PmtPeriodBatchHeaderId = @InsertedHeaderId;


                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error updating transaction ids in batch.'', 16, 1);
                        RETURN;
                    END;
                -----------------------------------------------------------------


                -----------------------------------------------------------------
                -- Set Batch to IsPosted
                -----------------------------------------------------------------
                UPDATE dbo.saPmtPeriodBatchHeaders
                SET    IsPosted = 1
                      ,PostedDate = GETDATE()
                WHERE  PmtPeriodBatchHeaderId = @InsertedHeaderId;


                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error updating batch is posted flag.'', 16, 1);
                        RETURN;
                    END;
                -----------------------------------------------------------------




                -----------------------------------------------------------------
                -- Set IsCharged flag for payment periods from this batch
                -----------------------------------------------------------------
                UPDATE dbo.saPmtPeriods
                SET    IsCharged = 1
                FROM   saPmtPeriodBatchItems I
                INNER JOIN dbo.saPmtPeriods P ON I.PmtPeriodId = P.PmtPeriodId
                WHERE  I.PmtPeriodBatchHeaderId = @InsertedHeaderId;


                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error updating batch is posted flag.'', 16, 1);
                        RETURN;
                    END;
                -----------------------------------------------------------------


                -----------------------------------------------------------------
                -- Set IsBillingMethodCharged flag for Program Version from this batch
                -----------------------------------------------------------------
                UPDATE P
                SET    P.IsBillingMethodCharged = 1
                FROM   dbo.arPrgVersions P
                INNER JOIN dbo.arStuEnrollments E ON P.PrgVerId = E.PrgVerId
                INNER JOIN saPmtPeriodBatchItems I ON E.StuEnrollId = I.StuEnrollId
                INNER JOIN dbo.saPmtPeriodBatchHeaders H ON I.PmtPeriodBatchHeaderId = H.PmtPeriodBatchHeaderId
                WHERE  H.PmtPeriodBatchHeaderId = @InsertedHeaderId
                       AND I.TransactionId IS NOT NULL
                       AND I.IncludedInPost = 1;
                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error updating program version ischarged flag.'', 16, 1);
                        RETURN;
                    END;
                -----------------------------------------------------------------




                COMMIT TRAN PAYMENTPERIODTRANSACTION;


            END;
    END;
--=================================================================================================
-- END  --  USP_ProcessPaymentPeriods
--=================================================================================================
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_TR_Sub07_TotalCourses]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub07_TotalCourses]', 'P') IS NULL
EXEC sp_executesql N'
-- =========================================================================================================  
-- USP_TR_Sub07_TotalCourses   
-- =========================================================================================================  
CREATE PROCEDURE [dbo].[USP_TR_Sub07_TotalCourses]
    @CoursesTakenTableName NVARCHAR(200)
   ,@GPASummaryTableName NVARCHAR(200)
   ,@StuEnrollIdList NVARCHAR(MAX)
   ,@TermId UNIQUEIDENTIFIER = NULL
   ,@ShowMultipleEnrollments BIT = 0
AS --  
    BEGIN

        -- 0) Declare varialbles and Set initial values  
        BEGIN
            --DECLARE @SQL01 AS NVARCHAR(MAX);  
            --DECLARE @SQL02 AS NVARCHAR(MAX);  

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#CoursesTaken'')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            CREATE TABLE #CoursesTaken
                (
                    CoursesTakenId INTEGER NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,ReqId UNIQUEIDENTIFIER
                   ,ReqCode VARCHAR(100)
                   ,ReqDescription VARCHAR(100)
                   ,ReqCodeDescrip VARCHAR(100)
                   ,ReqCredits DECIMAL(18, 2)
                   ,MinVal DECIMAL(18, 2)
                   ,ClsSectionId VARCHAR(50)
                   ,CourseStarDate DATETIME
                   ,CourseEndDate DATETIME
                   ,SCS_CreditsAttempted DECIMAL(18, 2)
                   ,SCS_CreditsEarned DECIMAL(18, 2)
                   ,SCS_CurrentScore DECIMAL(18, 2)
                   ,SCS_CurrentGrade VARCHAR(10)
                   ,SCS_FinalScore DECIMAL(18, 2)
                   ,SCS_FinalGrade VARCHAR(10)
                   ,SCS_Completed BIT
                   ,SCS_FinalGPA DECIMAL(18, 2)
                   ,SCS_Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_WeightedAverage_Credits DECIMAL(18, 2)
                   ,SCS_Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_SimpleAverage_Credits DECIMAL(18, 2)
                   ,SCS_ModUser VARCHAR(50)
                   ,SCS_ModDate DATETIME
                   ,SCS_TermGPA_Simple DECIMAL(18, 2)
                   ,SCS_TermGPA_Weighted DECIMAL(18, 2)
                   ,SCS_coursecredits DECIMAL(18, 2)
                   ,SCS_CumulativeGPA DECIMAL(18, 2)
                   ,SCS_CumulativeGPA_Simple DECIMAL(18, 2)
                   ,SCS_FACreditsEarned DECIMAL(18, 2)
                   ,SCS_Average DECIMAL(18, 2)
                   ,SCS_CumAverage DECIMAL(18, 2)
                   ,ScheduleDays DECIMAL(18, 2)
                   ,ActualDay DECIMAL(18, 2)
                   ,FinalGPA_Calculations DECIMAL(18, 2)
                   ,FinalGPA_TermCalculations DECIMAL(18, 2)
                   ,CreditsEarned_Calculations DECIMAL(18, 2)
                   ,ActualDay_Calculations DECIMAL(18, 2)
                   ,GrdBkWgtDetailsCount INTEGER
                   ,CountMultipleEnrollment INTEGER
                   ,RowNumberMultipleEnrollment INTEGER
                   ,CountRepeated INTEGER
                   ,RowNumberRetaked INTEGER
                   ,SameTermCountRetaken INTEGER
                   ,RowNumberSameTermRetaked INTEGER
                   ,RowNumberMultEnrollNoRetaken INTEGER
                   ,RowNumberOverAllByClassSection INTEGER
                   ,IsCreditsEarned BIT
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#GPASummary'')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
            CREATE TABLE #GPASummary
                (
                    GPASummaryId INTEGER NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,LastName VARCHAR(50)
                   ,FirstName VARCHAR(50)
                   ,MiddleName VARCHAR(50)
                   ,SSN VARCHAR(11)
                   ,StudentNumber VARCHAR(50)
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,EnrollmentID VARCHAR(50)
                   ,PrgVerId UNIQUEIDENTIFIER
                   ,PrgVerDescrip VARCHAR(50)
                   ,PrgVersionTrackCredits BIT
                   ,GrdSystemId UNIQUEIDENTIFIER
                   ,AcademicType VARCHAR(50)
                   ,ClockHourProgram VARCHAR(10)
                   ,IsMakingSAP BIT
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,TermEndDate DATETIME
                   ,DescripXTranscript VARCHAR(250)
                   ,TermAverage DECIMAL(18, 2)
                   ,EnroTermSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPACredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPA DECIMAL(18, 2)
                   ,EnroTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPACredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPA DECIMAL(18, 2)
                   ,StudTermSimple_CourseCredits DECIMAL(18, 2)
                   ,StudTermSimple_GPACredits DECIMAL(18, 2)
                   ,StudTermSimple_GPA DECIMAL(18, 2)
                   ,StudTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudTermWeight_GPACredits DECIMAL(18, 2)
                   ,StudTermWeight_GPA DECIMAL(18, 2)
                   ,EnroSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroSimple_GPACredits DECIMAL(18, 2)
                   ,EnroSimple_GPA DECIMAL(18, 2)
                   ,EnroWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroWeight_GPACredits DECIMAL(18, 2)
                   ,EnroWeight_GPA DECIMAL(18, 2)
                   ,StudSimple_CourseCredits DECIMAL(18, 2)
                   ,StudSimple_GPACredits DECIMAL(18, 2)
                   ,StudSimple_GPA DECIMAL(18, 2)
                   ,StudWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudWeight_GPACredits DECIMAL(18, 2)
                   ,StudWeight_GPA DECIMAL(18, 2)
                   ,GradesFormat VARCHAR(50)
                   ,GPAMethod VARCHAR(50)
                   ,GradeBookAt VARCHAR(50)
                   ,RetakenPolicy VARCHAR(50)
                );
        END;
        -- END --  0) Declare varialbles and Set initial values  

        --  1)Fill temp tables  
        BEGIN
            INSERT INTO #CoursesTaken
            EXEC USP_TR_Sub03_GetPreparedGPATable @TableName = @CoursesTakenTableName;

            INSERT INTO #GPASummary
            EXEC USP_TR_Sub03_GetPreparedGPATable @TableName = @GPASummaryTableName;
        END;
        -- END  --  1)Fill temp tables  

        -- to Test  
        -- SELECT * FROM #CoursesTaken AS CT ORDER BY CT.StudentId, CT.StuEnrollId, CT.TermId  
        -- SELECT * FROM #GPASummary AS GS ORDER BY GS.StudentId, GS.StuEnrollId, GS.TermId  

        DECLARE @includeRepeatedCourses VARCHAR(50) = (
                                                      SELECT dbo.GetAppSettingValueByKeyName(''IncludeCreditsAttemptedForRepeatedCourses'', NULL)
                                                      );

        --  2) Selet Totals  
        BEGIN
            IF ( @ShowMultipleEnrollments = 0 )
                BEGIN
           
                    SELECT     GS.StuEnrollId AS Id
                              ,MIN(GS.GradesFormat) AS GradesFormat
                              ,MIN(GS.GPAMethod) AS GPAMethod
                              ,MIN(   CASE WHEN (
                                                dbo.GetACIdForStudent(GS.StuEnrollId) = 5
                                                AND dbo.GetGradesFormatGivenStudentEnrollId(GS.StuEnrollId) = ''numeric''
                                                ) THEN dbo.CalculateStudentAverage(GS.StuEnrollId, NULL, NULL, NULL, @TermId,NULL)
                                           ELSE GS.EnroSimple_GPA
                                      END
                                  ) AS EnrollmentSimpleGPA
                              ,MIN(   CASE WHEN (
                                                dbo.GetACIdForStudent(GS.StuEnrollId) = 5
                                                AND dbo.GetGradesFormatGivenStudentEnrollId(GS.StuEnrollId) = ''numeric''
                                                ) THEN dbo.CalculateStudentAverage(GS.StuEnrollId, NULL, NULL, NULL, @TermId,NULL)
                                           ELSE GS.EnroWeight_GPA
                                      END
                                  ) AS EnrollmentWeightedGPA
                              ,MAX(CONVERT(INTEGER, GS.IsMakingSAP)) AS IsMakingSAP
                              ,MIN(GS.AcademicType) AS AcademicType
                              ,MIN(CONVERT(INTEGER, GS.PrgVersionTrackCredits)) AS PrgVersionTrackCredits
                              ,CASE WHEN @includeRepeatedCourses = ''Yes'' THEN COUNT(CT.CoursesTakenId)
                                    ELSE COUNT(DISTINCT CT.ReqId)
                               END AS CoursesTaked
                              ,CASE WHEN @includeRepeatedCourses = ''Yes'' THEN SUM(ISNULL(CT.SCS_CreditsAttempted, 0))
                                    ELSE (
                                         SELECT SUM(CA.SCS_CreditsAttempted)
                                         FROM   (
                                                SELECT DISTINCT CAT.ReqId
                                                               ,CAT.SCS_CreditsAttempted
                                                FROM   #CoursesTaken CAT
                                                WHERE  CAT.StuEnrollId = GS.StuEnrollId
                                                ) CA
                                         )
                               END AS CreditsAttempted
                              ,SUM(ISNULL(CT.CreditsEarned_Calculations, 0)) AS CreditsEarned
                              ,CASE WHEN @includeRepeatedCourses = ''Yes'' THEN SUM(ISNULL(CT.ScheduleDays, 0))
                                    ELSE (
                                         SELECT SUM(SD.ScheduleDays)
                                         FROM   (
                                                SELECT DISTINCT SDT.ReqId
                                                               ,SDT.ScheduleDays
                                                FROM   #CoursesTaken SDT
                                                WHERE  SDT.StuEnrollId = GS.StuEnrollId
                                                ) SD
                                         )
                               END AS ScheduleDays
                              ,SUM(ISNULL(CT.ActualDay_Calculations, 0)) AS ActualDay
                              ,SUM(ISNULL(CT.FinalGPA_Calculations, 0)) AS GradePoints
                    FROM       #GPASummary AS GS
                    INNER JOIN #CoursesTaken AS CT ON CT.StuEnrollId = GS.StuEnrollId
                                                      AND CT.TermId = GS.TermId
                    WHERE      EXISTS (
                                      SELECT Val
                                      FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                      WHERE  CONVERT(UNIQUEIDENTIFIER, Val) = CT.StuEnrollId
                                      )
                               AND (
                                   @TermId IS NULL
                                   OR GS.TermId = @TermId
                                   )
                    GROUP BY   GS.StuEnrollId;
                END;
            ELSE
                BEGIN
                    SELECT     GS.StudentId AS Id
                              ,MIN(GS.GradesFormat) AS GradesFormat
                              ,MIN(GS.GPAMethod) AS GPAMethod
                              ,MIN(GS.StudSimple_GPA) AS EnrollmentSimpleGPA
                              ,MIN(GS.StudWeight_GPA) AS EnrollmentWeightedGPA
                              ,MAX(CONVERT(INTEGER, GS.IsMakingSAP)) AS IsMakingSAP
                              ,MIN(GS.AcademicType) AS AcademicType
                              ,MIN(CONVERT(INTEGER, GS.PrgVersionTrackCredits)) AS PrgVersionTrackCredits
                              ,CASE WHEN @includeRepeatedCourses = ''Yes'' THEN COUNT(CT.CoursesTakenId)
                                    ELSE COUNT(DISTINCT CT.ReqId)
                               END AS CoursesTaked
                              ,(
                               SELECT SUM(CA.SCS_CreditsAttempted)
                               FROM   (
                                      SELECT DISTINCT CAT.ReqId
                                                     ,CAT.SCS_CreditsAttempted
                                      FROM   #CoursesTaken CAT
                                      WHERE  CAT.StudentId = GS.StudentId
                                      ) CA
                               ) AS CreditsAttempted
                              ,SUM(ISNULL(CT.CreditsEarned_Calculations, 0)) AS CreditsEarned
                              ,CASE WHEN @includeRepeatedCourses = ''Yes'' THEN SUM(ISNULL(CT.ScheduleDays, 0))
                                    ELSE (
                                         SELECT SUM(SD.ScheduleDays)
                                         FROM   (
                                                SELECT DISTINCT SDT.ReqId
                                                               ,SDT.ScheduleDays
                                                FROM   #CoursesTaken SDT
                                                WHERE  SDT.StudentId = GS.StudentId
                                                ) SD
                                         )
                               END AS ScheduleDays
                              ,SUM(ISNULL(CT.ActualDay_Calculations, 0)) AS ActualDay
                              ,SUM(ISNULL(CT.FinalGPA_Calculations, 0)) AS GradePoints
                    FROM       #GPASummary AS GS
                    INNER JOIN #CoursesTaken AS CT ON CT.StuEnrollId = GS.StuEnrollId
                                                      AND CT.TermId = GS.TermId
                    WHERE      CT.RowNumberMultEnrollNoRetaken = 1
                               AND EXISTS (
                                          SELECT Val
                                          FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                          WHERE  CONVERT(UNIQUEIDENTIFIER, Val) = CT.StuEnrollId
                                          )
                               AND (
                                   @TermId IS NULL
                                   OR GS.TermId = @TermId
                                   )
                    GROUP BY   GS.StudentId;
                END;
        END;
        -- END  --  2) Selet Totals  

        -- 3) drop temp tables  
        BEGIN
            -- Drop temp tables  
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#CoursesTaken'')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#GPASummary'')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
        END;
    -- END  --  3) drop temp tables  
    END;
-- =========================================================================================================  
-- END  --  USP_TR_Sub07_TotalCourses   
-- =========================================================================================================  



'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
