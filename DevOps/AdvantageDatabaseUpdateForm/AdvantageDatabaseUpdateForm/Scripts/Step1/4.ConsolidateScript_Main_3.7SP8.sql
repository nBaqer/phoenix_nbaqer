-- ********************************************************************************************************
-- Consolidated Script Version 3.7SP8
-- Schema Changes Zone
-- Please deploy your schema changes in this area
-- (Format the code with TSQl Format before insert here please)
-- ********************************************************************************************************
---------------------------------------------------------------------------------------------------------------------
--Troy: US9608: Create fields in Sycampuses to store Token and Kiss Code
----------------------------------------------------------------------------------------------------------------------
--Add the fields to the syCampuses table
IF NOT EXISTS ( SELECT  *
                FROM    sys.columns
                WHERE   name = N'Token1098TService'
                        AND object_id = OBJECT_ID(N'syCampuses') )
    BEGIN
        ALTER TABLE dbo.syCampuses ADD Token1098TService VARCHAR(38);
    END;
IF NOT EXISTS ( SELECT  *
                FROM    sys.columns
                WHERE   name = N'SchoolCodeKissSchoolId'
                        AND object_id = OBJECT_ID(N'syCampuses') )
    BEGIN
        ALTER TABLE dbo.syCampuses ADD SchoolCodeKissSchoolId VARCHAR(20);
    END;
GO


--Add Token1098TService field to the metadata
DECLARE @fldid INT;
DECLARE @fldcapid INT;

IF NOT EXISTS ( SELECT  *
                FROM    syFields
                WHERE   FldName = 'Token1098TService' )
    BEGIN
        SET @fldid = ( (
                         SELECT MAX(FldId)
                         FROM   dbo.syFields
                       ) + 1 );
        SET @fldcapid = ( (
                            SELECT  MAX(FldCapId)
                            FROM    dbo.syFldCaptions
                          ) + 1 );
    
		--add record to syfields
        INSERT  INTO dbo.syFields
                (
                 FldId
                ,FldName
                ,FldTypeId
                ,FldLen
                ,DDLId
                ,DerivedFld
                ,SchlReq
                ,LogChanges
                ,Mask
		        )
                SELECT  @fldid
                       , -- FldId - int
                        'Token1098TService'
                       , -- FldName - varchar(200)
                        200
                       , -- FldTypeId - int
                        38
                       , -- FldLen - int
                        NULL
                       , -- DDLId - int
                        NULL
                       , -- DerivedFld - bit
                        0
                       , -- SchlReq - bit
                        0
                       , -- LogChanges - bit
                        NULL;  -- Mask - varchar(50)

		--add record to field captions
        INSERT  INTO dbo.syFldCaptions
                (
                 FldCapId
                ,FldId
                ,LangId
                ,Caption
                ,FldDescrip
		        )
        VALUES  (
                 @fldcapid
                , -- FldCapId - int
                 @fldid
                , -- FldId - int
                 1
                , -- LangId - tinyint
                 'Token'
                , -- Caption - varchar(100)
                 '1098T Token'  -- FldDescrip - varchar(150)
		        );

    END;  
GO




--Add SchoolCodeKissSchoolId field to the metadata
DECLARE @fldid INT;
DECLARE @fldcapid INT;

IF NOT EXISTS ( SELECT  *
                FROM    syFields
                WHERE   FldName = 'SchoolCodeKissSchoolId' )
    BEGIN
        SET @fldid = ( (
                         SELECT MAX(FldId)
                         FROM   dbo.syFields
                       ) + 1 );
        SET @fldcapid = ( (
                            SELECT  MAX(FldCapId)
                            FROM    dbo.syFldCaptions
                          ) + 1 );
		
		--add record to fields table
        INSERT  INTO dbo.syFields
                (
                 FldId
                ,FldName
                ,FldTypeId
                ,FldLen
                ,DDLId
                ,DerivedFld
                ,SchlReq
                ,LogChanges
                ,Mask
		        )
                SELECT  @fldid
                       , -- FldId - int
                        'SchoolCodeKissSchoolId'
                       , -- FldName - varchar(200)
                        200
                       , -- FldTypeId - int
                        20
                       , -- FldLen - int
                        NULL
                       , -- DDLId - int
                        NULL
                       , -- DerivedFld - bit
                        0
                       , -- SchlReq - bit
                        0
                       , -- LogChanges - bit
                        NULL;  -- Mask - varchar(50)

		--add record to field captions
        INSERT  INTO dbo.syFldCaptions
                (
                 FldCapId
                ,FldId
                ,LangId
                ,Caption
                ,FldDescrip
		        )
        VALUES  (
                 @fldcapid
                , -- FldCapId - int
                 @fldid
                , -- FldId - int
                 1
                , -- LangId - tinyint
                 'Kiss Code'
                , -- Caption - varchar(100)
                 '1098T Kiss Code'  -- FldDescrip - varchar(150)
		        );


    END;  
GO

--Add the Token field to the tables metadata
DECLARE @tokenfldid INT;
DECLARE @tblid INT;
DECLARE @tblfldsid INT;

SET @tokenfldid = (
                    SELECT  FldId
                    FROM    syFields
                    WHERE   FldName = 'Token1098TService'
                  );
SET @tblid = (
               SELECT   TblId
               FROM     syTables
               WHERE    TblName = 'syCampuses'
             );
SET @tblfldsid = ( (
                     SELECT MAX(TblFldsId)
                     FROM   dbo.syTblFlds
                   ) + 1 );

IF NOT EXISTS ( SELECT  *
                FROM    syTblFlds
                WHERE   TblId = @tblid
                        AND FldId = @tokenfldid )
    BEGIN
        INSERT  INTO dbo.syTblFlds
                (
                 TblFldsId
                ,TblId
                ,FldId
                ,CategoryId
                ,FKColDescrip
		        )
                SELECT  @tblfldsid
                       , -- TblFldsId - int
                        @tblid
                       , -- TblId - int
                        @tokenfldid
                       , -- FldId - int
                        NULL
                       , -- CategoryId - int
                        NULL;  -- FKColDescrip - varchar(50)

  
    END;  
GO


--Add the KISS Code field to the tables metadata
DECLARE @kisscodefldid INT;
DECLARE @tblid INT;
DECLARE @tblfldsid INT;

SET @kisscodefldid = (
                       SELECT   FldId
                       FROM     syFields
                       WHERE    FldName = 'SchoolCodeKissSchoolId'
                     );
SET @tblid = (
               SELECT   TblId
               FROM     syTables
               WHERE    TblName = 'syCampuses'
             );
SET @tblfldsid = ( (
                     SELECT MAX(TblFldsId)
                     FROM   dbo.syTblFlds
                   ) + 1 );

IF NOT EXISTS ( SELECT  *
                FROM    syTblFlds
                WHERE   TblId = @tblid
                        AND FldId = @kisscodefldid )
    BEGIN
        INSERT  INTO dbo.syTblFlds
                (
                 TblFldsId
                ,TblId
                ,FldId
                ,CategoryId
                ,FKColDescrip
		        )
                SELECT  @tblfldsid
                       , -- TblFldsId - int
                        @tblid
                       , -- TblId - int
                        @kisscodefldid
                       , -- FldId - int
                        NULL
                       , -- CategoryId - int
                        NULL;  -- FKColDescrip - varchar(50)

  
    END;  
GO


--Add the Token field to resources metadata for the campuses page
DECLARE @fldid INT;
DECLARE @tblid INT;
DECLARE @resourceid INT;
DECLARE @tblfldsid INT;
DECLARE @resdefid INT;

SET @resourceid = (
                    SELECT  ResourceID
                    FROM    syResources
                    WHERE   Resource = 'Campus'
                  );
SET @fldid = (
               SELECT   FldId
               FROM     dbo.syFields
               WHERE    FldName = 'Token1098TService'
             );
SET @tblid = (
               SELECT   TblId
               FROM     syTables
               WHERE    TblName = 'syCampuses'
             );

SET @tblfldsid = (
                   SELECT   TblFldsId
                   FROM     syTblFlds
                   WHERE    TblId = @tblid
                            AND FldId = @fldid
                 );

IF NOT EXISTS ( SELECT  *
                FROM    dbo.syResTblFlds
                WHERE   TblFldsId = @tblfldsid )
    BEGIN
        SET @resdefid = ( (
                            SELECT  MAX(ResDefId)
                            FROM    dbo.syResTblFlds
                          ) + 1 );
    
        INSERT  INTO dbo.syResTblFlds
                (
                 ResDefId
                ,ResourceId
                ,TblFldsId
                ,Required
                ,SchlReq
                ,ControlName
                ,UsePageSetup
		        )
                SELECT  @resdefid
                       , -- ResDefId - int
                        @resourceid
                       , -- ResourceId - smallint
                        @tblfldsid
                       , -- TblFldsId - int
                        0
                       , -- Required - bit
                        0
                       , -- SchlReq - bit
                        NULL
                       , -- ControlName - varchar(50)
                        1;  -- UsePageSetup - bit	  
  
    END;  

GO



--Add the KISS Code field to resources metadata for the campuses page
DECLARE @fldid INT;
DECLARE @tblid INT;
DECLARE @resourceid INT;
DECLARE @tblfldsid INT;
DECLARE @resdefid INT;

SET @resourceid = (
                    SELECT  ResourceID
                    FROM    syResources
                    WHERE   Resource = 'Campus'
                  );
SET @fldid = (
               SELECT   FldId
               FROM     dbo.syFields
               WHERE    FldName = 'SchoolCodeKissSchoolId'
             );
SET @tblid = (
               SELECT   TblId
               FROM     syTables
               WHERE    TblName = 'syCampuses'
             );

SET @tblfldsid = (
                   SELECT   TblFldsId
                   FROM     syTblFlds
                   WHERE    TblId = @tblid
                            AND FldId = @fldid
                 );

IF NOT EXISTS ( SELECT  *
                FROM    dbo.syResTblFlds
                WHERE   TblFldsId = @tblfldsid )
    BEGIN
        SET @resdefid = ( (
                            SELECT  MAX(ResDefId)
                            FROM    dbo.syResTblFlds
                          ) + 1 );
    
        INSERT  INTO dbo.syResTblFlds
                (
                 ResDefId
                ,ResourceId
                ,TblFldsId
                ,Required
                ,SchlReq
                ,ControlName
                ,UsePageSetup
		        )
                SELECT  @resdefid
                       , -- ResDefId - int
                        @resourceid
                       , -- ResourceId - smallint
                        @tblfldsid
                       , -- TblFldsId - int
                        0
                       , -- Required - bit
                        0
                       , -- SchlReq - bit
                        NULL
                       , -- ControlName - varchar(50)
                        1;  -- UsePageSetup - bit	  
  
    END;  

GO
---------------------------------------------------------------------------------------------------------------------
--End US9608: Create fields in Sycampuses to store Token and Kiss Code
----------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------
--Troy US9610:Pull a Default Address for 1098T
------------------------------------------------------------------------------------------------------
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   type = 'P'
                    AND name = 'USP_SA_Get1098TDataForTaxYear' )
    DROP PROCEDURE USP_SA_Get1098TDataForTaxYear;
GO

CREATE PROCEDURE dbo.USP_SA_Get1098TDataForTaxYear
    @TaxYear AS CHAR(4)
   ,@CampusId AS UNIQUEIDENTIFIER

	-------------------------------------------------------------------------------------------------
	--Testing Purposes Only
	--------------------------------------------------------------------------------------------------
	--DECLARE @TaxYear AS CHAR(4)
	--declare @CampusId AS UNIQUEIDENTIFIER
  
	--SET @TaxYear='2016' 
	--SET @CampusId='DC42A60A-5EB9-49B6-ADF6-535966F2E34A' 
	-----------------------------------------------------------------------------------------------
AS /*----------------------------------------------------------------------------------------------------
	Author : Troy Richards
	Create date : 09/24/2014
	Procedure Name : USP_FA_SummaryReportByStudent9010
	Objective : Get DataSet for the 1098T for the specified tax year and campus    
	Parameters : @TaxYear is the tax year for which the 1098T is being run
				 @CampusId is the campus for which the 1098T is being run          
	Output : Returns the 1098T report dataset
						
--*/-----------------------------------------------------------------------------------------------------    
    BEGIN
        DECLARE @strFirstQuarter AS VARCHAR(10) = '03/31/' + CONVERT(VARCHAR(10),CONVERT(INT,@TaxYear) + 1);
        DECLARE @strFirstCalendarDay AS VARCHAR(10) = '01/01/' + @TaxYear;
        DECLARE @intPrevYear AS INTEGER = CONVERT(INT,@TaxYear) - 1;
        DECLARE @intCurrentTaxYear AS INTEGER = CONVERT(INT,@TaxYear);        
        DECLARE @EndOfYear AS VARCHAR(10) = '12/31/' + @TaxYear;
        DECLARE @StartOfYear AS VARCHAR(10) = '1/1/' + @TaxYear;
	
        SELECT DISTINCT
                StuEnrollId
               ,t1.EnrollmentId
               ,CONVERT(CHAR(10),StartDate,101) AS StartDate
               ,(
                  SELECT    CONVERT(CHAR(10),MAX(LDA),101)
                  FROM      arStuEnrollments
                  WHERE     StuEnrollId = t1.StuEnrollId
                            AND LDA IS NOT NULL
                ) AS LDA
               ,(
                  SELECT    CASE WHEN COUNT(*) > 0 THEN 1
                                 ELSE 0
                            END
                  FROM      arStuEnrollments se
                           ,syStatusCodes sc
                  WHERE     se.StatusCodeId = sc.StatusCodeId
                            AND sc.SysStatusId = 8 -- no start status
                            AND se.StuEnrollId = t1.StuEnrollId
                ) AS IsStudentNoStart
               ,(
                  SELECT    CONVERT(CHAR(10),MAX(TransDate),101)
                  FROM      saTransactions
                  WHERE     StuEnrollId = t1.StuEnrollId
                            AND TransDate IS NOT NULL
                            AND Voided = 0
                            AND TransTypeId = 2
                ) AS TransDate
               ,t2.FirstName
               ,t2.LastName
               ,t2.SSN
               ,( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 ) THEN (
                                                                          SELECT    RTRIM(sa2.Address1 + ' ' + ISNULL(sa2.Address2,''))
                                                                          FROM      dbo.arStudAddresses sa2
                                                                          WHERE     sa2.StudentId = t2.StudentId
                                                                                    AND sa2.default1 = 1
                                                                        )
                       ELSE (
                              SELECT TOP 1
                                        RTRIM(sa2.Address1 + ' ' + ISNULL(sa2.Address2,''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS Address1
               ,( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 ) THEN (
                                                                          SELECT    RTRIM(ISNULL(sa2.City,''))
                                                                          FROM      dbo.arStudAddresses sa2
                                                                          WHERE     sa2.StudentId = t2.StudentId
                                                                                    AND sa2.default1 = 1
                                                                        )
                       ELSE (
                              SELECT TOP 1
                                        RTRIM(ISNULL(sa2.City,''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS City
               ,( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 ) THEN (
                                                                          SELECT    RTRIM(ISNULL(ss.StateCode,''))
                                                                          FROM      dbo.arStudAddresses sa2
                                                                                   ,syStates ss
                                                                          WHERE     sa2.StudentId = t2.StudentId
                                                                                    AND sa2.StateId = ss.StateId
                                                                                    AND sa2.default1 = 1
                                                                        )
                       ELSE (
                              SELECT TOP 1
                                        RTRIM(ISNULL(ss.StateCode,''))
                              FROM      dbo.arStudAddresses sa2
                                       ,syStates ss
                              WHERE     sa2.StudentId = t2.StudentId
                                        AND sa2.StateId = ss.StateId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS State
               ,( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 ) THEN (
                                                                          SELECT    RTRIM(ISNULL(sa2.Zip,''))
                                                                          FROM      dbo.arStudAddresses sa2
                                                                          WHERE     sa2.StudentId = t2.StudentId
                                                                                    AND sa2.default1 = 1
                                                                        )
                       ELSE (
                              SELECT TOP 1
                                        RTRIM(ISNULL(sa2.Zip,''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS Zip
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,dbo.saTransCodes tc
                  WHERE     SQ1.TransCodeId = tc.TransCodeId
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 0
                            AND SQ1.Voided = 0
                            AND tc.Is1098T = 1
                ) AS TotalCost
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 2
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                            AND (
                                  EXISTS ( SELECT   tc.*
                                           FROM     saTransCodes tc
                                           WHERE    tc.TransCodeId = SQ1.TransCodeId
                                                    AND tc.Is1098T = 1 )
                                  OR EXISTS ( SELECT    ap.*
                                              FROM      dbo.saAppliedPayments ap
                                                       ,saTransactions tr2
                                                       ,dbo.saTransCodes tc
                                              WHERE     ap.ApplyToTransId = tr2.TransactionId
                                                        AND tr2.TransCodeId = tc.TransCodeId
                                                        AND ap.TransactionId = SQ1.TransactionId
                                                        AND tc.Is1098T = 1 )
                                )
                ) AS TotalPaid
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 2
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) < CONVERT(INT,@TaxYear)
                            AND (
                                  EXISTS ( SELECT   tc.*
                                           FROM     saTransCodes tc
                                           WHERE    tc.TransCodeId = SQ1.TransCodeId
                                                    AND tc.Is1098T = 1 )
                                  OR EXISTS ( SELECT    ap.*
                                              FROM      dbo.saAppliedPayments ap
                                                       ,saTransactions tr2
                                                       ,dbo.saTransCodes tc
                                              WHERE     ap.ApplyToTransId = tr2.TransactionId
                                                        AND tr2.TransCodeId = tc.TransCodeId
                                                        AND ap.TransactionId = SQ1.TransactionId
                                                        AND tc.Is1098T = 1 )
                                )
                ) AS PriorYearPayments
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,saRefunds SR
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransactionId = SR.TransactionId
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) < CONVERT(INT,@TaxYear)
                ) AS PriorYearRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 2
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                            AND (
                                  EXISTS ( SELECT   tc.*
                                           FROM     saTransCodes tc
                                           WHERE    tc.TransCodeId = SQ1.TransCodeId
                                                    AND tc.Is1098T = 1 )
                                  OR EXISTS ( SELECT    ap.*
                                              FROM      dbo.saAppliedPayments ap
                                                       ,saTransactions tr2
                                                       ,dbo.saTransCodes tc
                                              WHERE     ap.ApplyToTransId = tr2.TransactionId
                                                        AND tr2.TransCodeId = tc.TransCodeId
                                                        AND ap.TransactionId = SQ1.TransactionId
                                                        AND tc.Is1098T = 1 )
                                )
                ) AS GrossPayments
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,saRefunds SQ2
                  WHERE     SQ1.TransactionId = SQ2.TransactionId
                            AND SQ1.Voided = 0
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                            AND (
                                  EXISTS ( SELECT   tc.*
                                           FROM     saTransCodes tc
                                           WHERE    tc.TransCodeId = SQ1.TransCodeId
                                                    AND tc.Is1098T = 1 )
                                  OR EXISTS ( SELECT    ap.*
                                              FROM      dbo.saAppliedPayments ap
                                                       ,saTransactions tr2
                                                       ,dbo.saTransCodes tc
                                              WHERE     ap.ApplyToTransId = tr2.TransactionId
                                                        AND tr2.TransCodeId = tc.TransCodeId
                                                        AND ap.TransactionId = SQ1.TransactionId
                                                        AND tc.Is1098T = 1 )
                                )
                ) AS GrossRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions A1
                           ,saFundSources A5
                           ,saRefunds A6
                  WHERE     A1.TransactionId = A6.TransactionId
                            AND A5.FundSourceId = A6.FundSourceId
                            AND A1.Voided = 0
                            AND A1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                            AND A5.AwardTypeId = 1
                            AND (
                                  EXISTS ( SELECT   tc.*
                                           FROM     saTransCodes tc
                                           WHERE    tc.TransCodeId = A1.TransCodeId
                                                    AND tc.Is1098T = 1 )
                                  OR EXISTS ( SELECT    ap.*
                                              FROM      dbo.saAppliedPayments ap
                                                       ,saTransactions tr2
                                                       ,dbo.saTransCodes tc
                                              WHERE     ap.ApplyToTransId = tr2.TransactionId
                                                        AND tr2.TransCodeId = tc.TransCodeId
                                                        AND ap.TransactionId = A1.TransactionId
                                                        AND tc.Is1098T = 1 )
                                )
                ) AS GrantRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions A1
                           ,saFundSources A5
                           ,saRefunds A6
                  WHERE     A1.TransactionId = A6.TransactionId
                            AND A5.FundSourceId = A6.FundSourceId
                            AND A1.Voided = 0
                            AND A1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                            AND A6.RefundTypeId = 1
                            AND A5.AwardTypeId = 2
                            AND (
                                  EXISTS ( SELECT   tc.*
                                           FROM     saTransCodes tc
                                           WHERE    tc.TransCodeId = A1.TransCodeId
                                                    AND tc.Is1098T = 1 )
                                  OR EXISTS ( SELECT    ap.*
                                              FROM      dbo.saAppliedPayments ap
                                                       ,saTransactions tr2
                                                       ,dbo.saTransCodes tc
                                              WHERE     ap.ApplyToTransId = tr2.TransactionId
                                                        AND tr2.TransCodeId = tc.TransCodeId
                                                        AND ap.TransactionId = A1.TransactionId
                                                        AND tc.Is1098T = 1 )
                                )
                ) AS LoanRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,saRefunds SQ2
                  WHERE     SQ1.TransactionId = SQ2.TransactionId
                            AND SQ1.Voided = 0
                            AND SQ2.RefundTypeId = 0
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                            AND (
                                  EXISTS ( SELECT   tc.*
                                           FROM     saTransCodes tc
                                           WHERE    tc.TransCodeId = SQ1.TransCodeId
                                                    AND tc.Is1098T = 1 )
                                  OR EXISTS ( SELECT    ap.*
                                              FROM      dbo.saAppliedPayments ap
                                                       ,saTransactions tr2
                                                       ,dbo.saTransCodes tc
                                              WHERE     ap.ApplyToTransId = tr2.TransactionId
                                                        AND tr2.TransCodeId = tc.TransCodeId
                                                        AND ap.TransactionId = SQ1.TransactionId
                                                        AND tc.Is1098T = 1 )
                                )
                ) AS StudentRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      (
                              SELECT    A1.TransDate
                                       ,A1.TransAmount
                                       ,A1.TransDescrip
                                       ,A1.FundSourceId
                              FROM      saTransactions A1
                                       ,saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                                        AND A5.AwardTypeId = 1
                                        AND (
                                              EXISTS ( SELECT   tc.*
                                                       FROM     saTransCodes tc
                                                       WHERE    tc.TransCodeId = A1.TransCodeId
                                                                AND tc.Is1098T = 1 )
                                              OR EXISTS ( SELECT    ap.*
                                                          FROM      dbo.saAppliedPayments ap
                                                                   ,saTransactions tr2
                                                                   ,dbo.saTransCodes tc
                                                          WHERE     ap.ApplyToTransId = tr2.TransactionId
                                                                    AND tr2.TransCodeId = tc.TransCodeId
                                                                    AND ap.TransactionId = A1.TransactionId
                                                                    AND tc.Is1098T = 1 )
                                            )
                            ) R
                ) AS GrantPayments
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      (
                              SELECT    A1.TransDate
                                       ,A1.TransAmount
                                       ,A1.TransDescrip
                                       ,A1.FundSourceId
                              FROM      saTransactions A1
                                       ,saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                                        AND A5.AwardTypeId = 2
                                        AND (
                                              EXISTS ( SELECT   tc.*
                                                       FROM     saTransCodes tc
                                                       WHERE    tc.TransCodeId = A1.TransCodeId
                                                                AND tc.Is1098T = 1 )
                                              OR EXISTS ( SELECT    ap.*
                                                          FROM      dbo.saAppliedPayments ap
                                                                   ,saTransactions tr2
                                                                   ,dbo.saTransCodes tc
                                                          WHERE     ap.ApplyToTransId = tr2.TransactionId
                                                                    AND tr2.TransCodeId = tc.TransCodeId
                                                                    AND ap.TransactionId = A1.TransactionId
                                                                    AND tc.Is1098T = 1 )
                                            )
                            ) R
                ) AS LoanPayments
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      (
                              SELECT    A1.TransDate
                                       ,A1.TransAmount
                                       ,A1.TransDescrip
                                       ,A1.FundSourceId
                              FROM      saTransactions A1
                                       ,saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                                        AND A5.AwardTypeId IN ( 3,4,5 )
                                        AND (
                                              EXISTS ( SELECT   tc.*
                                                       FROM     saTransCodes tc
                                                       WHERE    tc.TransCodeId = A1.TransCodeId
                                                                AND tc.Is1098T = 1 )
                                              OR EXISTS ( SELECT    ap.*
                                                          FROM      dbo.saAppliedPayments ap
                                                                   ,saTransactions tr2
                                                                   ,dbo.saTransCodes tc
                                                          WHERE     ap.ApplyToTransId = tr2.TransactionId
                                                                    AND tr2.TransCodeId = tc.TransCodeId
                                                                    AND ap.TransactionId = A1.TransactionId
                                                                    AND tc.Is1098T = 1 )
                                            )
                            ) R
                ) AS Scholarships
               ,(
                  SELECT DISTINCT
                            IsContinuingEd
                  FROM      arPrgVersions
                  WHERE     PrgVerId = t1.PrgVerId
                ) AS IsContinuingEd
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      (
                              SELECT    A1.TransDate
                                       ,A1.TransAmount
                                       ,A1.TransDescrip
                                       ,A1.FundSourceId
                              FROM      saTransactions A1
                                       ,saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                                        AND A5.AwardTypeId = 6
                                        AND (
                                              EXISTS ( SELECT   tc.*
                                                       FROM     saTransCodes tc
                                                       WHERE    tc.TransCodeId = A1.TransCodeId
                                                                AND tc.Is1098T = 1 )
                                              OR EXISTS ( SELECT    ap.*
                                                          FROM      dbo.saAppliedPayments ap
                                                                   ,saTransactions tr2
                                                                   ,dbo.saTransCodes tc
                                                          WHERE     ap.ApplyToTransId = tr2.TransactionId
                                                                    AND tr2.TransCodeId = tc.TransCodeId
                                                                    AND ap.TransactionId = A1.TransactionId
                                                                    AND tc.Is1098T = 1 )
                                            )
                            ) R
                ) AS StudentPayments
               ,(
                  SELECT    COUNT(*)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.Voided = 0
                            AND TransTypeId = 0
                ) AS CostForEnrollmentExist
        FROM    arStuEnrollments t1
        INNER JOIN arStudent t2 ON t1.StudentId = t2.StudentId
        INNER JOIN syStatusCodes t5 ON t1.StatusCodeId = t5.StatusCodeId
        INNER JOIN arPrgVersions t7 ON t1.PrgVerId = t7.PrgVerId
        INNER JOIN arPrograms t6 ON t6.ProgId = t7.ProgId
        WHERE   t1.CampusId = @CampusId
                AND t6.Is1098T = '1'
                AND (
                      --This part mostly handles the attendance taking schools. The student must have started
            --before the end of the current year and must have attendance for the current year.
                      (
                        t1.StartDate <= @EndOfYear
                        AND t1.LDA >= @StartOfYear
                      )
                      OR
			--Student enrolled in the current year but starts in the first quarter of next year
                      (
                        t1.StartDate > @EndOfYear
                        AND t1.StartDate <= @strFirstQuarter
                        AND YEAR(t1.EnrollDate) = CONVERT(INT,@TaxYear)
                      )
                      OR
			--This part mostly handles schools that do not not take attendance
			--Students who are still in school - currently attending, LOA, Suspended, Academic Probation, Suspension
			--Note that we include future starts.                                
                      (
                        t1.StartDate <= @EndOfYear
                        AND t5.SysStatusId IN ( 7,9,10,11,20,21,22 )
                      )
                      OR
			--Include no starts who started during the year. They will be added to the exclusions table.
                      (
                        t1.StartDate BETWEEN @StartOfYear AND @EndOfYear
                        AND t5.SysStatusId = 8
                      )
                      OR
			--Students who graduated during the current year and the school
                      (
                        t1.StartDate <= @EndOfYear
                        AND t5.SysStatusId = 14
                        AND t1.ExpGradDate > @StartOfYear
                      )
                    )
            --The student must have a record in the student addresses table
                AND EXISTS ( SELECT *
                             FROM   dbo.arStudAddresses sa
                             WHERE  sa.StudentId = t2.StudentId )
        ORDER BY LastName;

    END;
GO

------------------------------------------------------------------------------------------------------
--End Troy US9610:Pull a Default Address for 1098T
------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------
--Start Balaji US9611:1098T Exceptions Report
------------------------------------------------------------------------------------------------------
UPDATE  arPrograms
SET     Is1098T = 0
WHERE   Is1098T IS NULL;
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   type = 'P'
                    AND name = 'USP_Programs_No1098T' )
    BEGIN
        DROP PROCEDURE USP_Programs_No1098T;
    END;
GO
CREATE PROCEDURE USP_Programs_No1098T
    @CampusId UNIQUEIDENTIFIER
AS
    SELECT  DISTINCT
            P.ProgDescrip
    FROM    dbo.arPrograms P
    INNER JOIN dbo.syCmpGrpCmps CGC ON CGC.CampGrpId = P.CampGrpId
    INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
    WHERE   P.Is1098T = 0
            AND P.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
            AND C.CampusId = @CampusId
    ORDER BY ProgDescrip; 
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   type = 'P'
                    AND name = 'USP_TransCodes_No1098T' )
    BEGIN
        DROP PROCEDURE USP_TransCodes_No1098T;
    END;
GO
CREATE PROCEDURE USP_TransCodes_No1098T
    @CampusId UNIQUEIDENTIFIER
AS
    DECLARE @ApplicantFeeId INT;
    SET @ApplicantFeeId = (
                            SELECT TOP 1
                                    SysTransCodeId
                            FROM    dbo.saSysTransCodes
                            WHERE   LOWER(Description) = 'applicant fee'
                          ); 

    SELECT  DISTINCT
            P.TransCodeCode
           ,P.TransCodeDescrip
    FROM    dbo.saTransCodes P
    INNER JOIN dbo.syCmpGrpCmps CGC ON CGC.CampGrpId = P.CampGrpId
    INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
    WHERE   P.Is1098T = 0
            AND P.SysTransCodeId <> @ApplicantFeeId
            AND P.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
            AND C.CampusId = @CampusId
    ORDER BY TransCodeDescrip;
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   type = 'P'
                    AND name = 'USP_Students_AddressMissing' )
    BEGIN
        DROP PROCEDURE USP_Students_AddressMissing;
    END;
GO
CREATE PROCEDURE USP_Students_AddressMissing @CampusId VARCHAR(50)
AS
    DECLARE @SettingId INT
       ,@SettingValue VARCHAR(50);
    SET @SettingId = (
                       SELECT   SettingId
                       FROM     dbo.syConfigAppSettings
                       WHERE    LOWER(KeyName) = 'studentidentifier'
                     );
    SET @SettingValue = (
                          SELECT TOP 1
                                    Value
                          FROM      dbo.syConfigAppSetValues
                          WHERE     SettingId = @SettingId
                        );

    PRINT @SettingValue;

    SELECT DISTINCT
            CASE WHEN LOWER(@SettingValue) = 'ssn' THEN CASE WHEN LEN(S.SSN) = 9 THEN '***-**-' + SUBSTRING(S.SSN,6,4)
                                                             ELSE S.SSN
                                                        END
                 ELSE S.StudentNumber
            END AS StudentID
           ,S.LastName
           ,S.MiddleName
           ,S.FirstName
           ,CASE WHEN SA.Address1 IS NULL THEN 'X'
                 ELSE ''
            END AS Address1Missing
           ,CASE WHEN SA.City IS NULL THEN 'X'
                 ELSE ''
            END AS CityMissing
           ,CASE WHEN SA.StateId IS NULL THEN 'X'
                 ELSE ''
            END AS StateMissing
           ,CASE WHEN (
                        S.FirstName IS NULL
                        OR S.LastName IS NULL
                      ) THEN 'X'
                 ELSE ''
            END AS NameMissing
           ,CASE WHEN (
                        S.SSN IS NULL
                        OR LEN(ISNULL(S.SSN,0)) < 9
                      ) THEN 'X'
                 ELSE ''
            END AS SSNMissing
           ,CASE WHEN (
                        SA.Zip IS NULL
                        OR (
                             SA.Zip IS NOT NULL
                             AND ( LEN(SA.Zip) < 5 )
                           ) -- If Zip less than 5 digits
                        OR (
                             SA.Zip IS NOT NULL
                             AND (
                                   (
                                     LEN(SA.Zip) > 5
                                     AND LEN(SA.Zip) < 9
                                   )
                                   OR LEN(SA.Zip) > 9
                                 )
                           ) -- if zip is not 9 characters long
                        OR (
                             SA.Zip IS NOT NULL
                             AND LEN(SA.Zip) >= 5
                             AND PATINDEX('%[A-Z]%',SUBSTRING(SA.Zip,1,5)) > 0
                           )
                      ) THEN 'X'
                 ELSE ''
            END AS ZipMissing
    FROM    arStudent S
    INNER JOIN (
                 SELECT *
                       ,ROW_NUMBER() OVER ( PARTITION BY StudentId ORDER BY default1 DESC, ModDate DESC ) AS RowNumber
                 FROM   dbo.arStudAddresses
               ) SA ON SA.StudentId = S.StudentId
    INNER JOIN dbo.arStuEnrollments SE ON S.StudentId = SE.StudentId
    INNER JOIN dbo.arPrgVersions PV ON PV.PrgVerId = SE.PrgVerId
    INNER JOIN dbo.arPrograms P ON P.ProgId = PV.ProgId
    WHERE   SE.CampusId = @CampusId
            AND SA.RowNumber = 1 -- If there are multiple addresses bring the default address or the latest one
            AND SA.ForeignZip = 0
            AND P.Is1098T = 1
            AND (
                  SA.Address1 IS NULL
                  OR SA.Zip IS NULL
                  OR (
                       SA.Zip IS NOT NULL
                       AND ( LEN(SA.Zip) < 5 )
                     ) -- If Zip less than 5 digits
                  OR (
                       SA.Zip IS NOT NULL
                       AND (
                             (
                               LEN(SA.Zip) > 5
                               AND LEN(SA.Zip) < 9
                             )
                             OR LEN(SA.Zip) > 9
                           )
                     ) -- if zip is not 9 characters long
                  OR (
                       SA.Zip IS NOT NULL
                       AND LEN(SA.Zip) >= 5
                       AND PATINDEX('%[A-Z]%',SUBSTRING(SA.Zip,1,5)) > 0
                     ) -- if there is a character in the first 5 positions
                  OR SA.City IS NULL
                  OR SA.StateId IS NULL
                  OR (
                       S.SSN IS NULL
                       OR LEN(ISNULL(S.SSN,0)) < 9
                     )
                  OR (
                       S.FirstName IS NULL
                       OR S.LastName IS NULL
                     )
                )
            --AND S.StudentStatus = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
    ORDER BY S.LastName; 
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   type = 'P'
                    AND name = 'USP_InternationalStudents_List' )
    BEGIN
        DROP PROCEDURE USP_InternationalStudents_List;
    END;
GO
CREATE PROCEDURE USP_InternationalStudents_List @CampusId VARCHAR(50)
AS
    DECLARE @SettingId INT
       ,@SettingValue VARCHAR(50);
    SET @SettingId = (
                       SELECT   SettingId
                       FROM     dbo.syConfigAppSettings
                       WHERE    LOWER(KeyName) = 'studentidentifier'
                     );
    
    SET @SettingValue = (
                          SELECT TOP 1
                                    Value
                          FROM      dbo.syConfigAppSetValues
                          WHERE     SettingId = @SettingId
                        );

    SELECT DISTINCT
            CASE WHEN LOWER(@SettingValue) = 'ssn' THEN CASE WHEN LEN(S.SSN) = 9 THEN '***-**-' + SUBSTRING(S.SSN,6,4)
                                                             ELSE S.SSN
                                                        END
                 ELSE S.StudentNumber
            END AS StudentID
           ,S.LastName
           ,S.MiddleName
           ,S.FirstName
    FROM    arStudent S
    INNER JOIN dbo.arStuEnrollments SE ON S.StudentId = SE.StudentId
    INNER JOIN dbo.arPrgVersions PV ON PV.PrgVerId = SE.PrgVerId
    INNER JOIN dbo.arPrograms P ON P.ProgId = PV.ProgId
    INNER JOIN dbo.adLeadByLeadGroups LLG ON LLG.StuEnrollId = SE.StuEnrollId
    INNER JOIN dbo.adLeadGroups LG ON LG.LeadGrpId = LLG.LeadGrpId
    LEFT OUTER JOIN dbo.adCitizenships C ON S.Citizen = C.CitizenshipId
    WHERE   SE.CampusId = @CampusId
            AND P.Is1098T = 1
            --AND S.StudentStatus = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
            AND (
                  LG.Descrip LIKE 'Int%' -- International Lead Group
                  OR C.IPEDSValue = 65
                )  --Non-Citizen
    ORDER BY S.LastName;
GO
------------------------------------------------------------------------------------------------------
--End Balaji US9611:1098T Exceptions Report
------------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------------
--Troy: Related to US9775. The token should be 36 characters. Initial Script provided used 38.
------------------------------------------------------------------------------------------------------
IF EXISTS ( SELECT  *
            FROM    sys.columns
            WHERE   name = N'Token1098TService'
                    AND object_id = OBJECT_ID(N'syCampuses') )
    AND ( SELECT    COL_LENGTH('syCampuses', 'Token1098TService')
        ) <> 36
    BEGIN		
        ALTER TABLE dbo.syCampuses ALTER COLUMN Token1098TService VARCHAR(36);
    END;
GO

------------------------------------------------------------------------------------------------------
--Troy: Related to US9775. The Kiss Code should be 18 characters. Initial Script provided used 20.
------------------------------------------------------------------------------------------------------
IF EXISTS ( SELECT  *
            FROM    sys.columns
            WHERE   name = N'SchoolCodeKissSchoolId'
                    AND object_id = OBJECT_ID(N'syCampuses') )
    AND ( SELECT    COL_LENGTH('syCampuses', 'SchoolCodeKissSchoolId')
        ) <> 18
    BEGIN
        ALTER TABLE dbo.syCampuses ALTER COLUMN SchoolCodeKissSchoolId VARCHAR(18);
    END;
GO

----------------------------------------------------------------------------------------------------------
--Troy: Related to US9775. Updaste metadata. The token should be 36 characters. Initial Script provided used 38. 
-----------------------------------------------------------------------------------------------------------
DECLARE @fldid INT;

IF EXISTS ( SELECT  *
            FROM    syFields
            WHERE   FldName = 'Token1098TService'
                    AND FldLen <> 36 )
    BEGIN
        SET @fldid = ( SELECT   FldId
                       FROM     dbo.syFields
                       WHERE    FldName = 'Token1098TService'
                     );

        UPDATE  dbo.syFields
        SET     FldLen = 36
        WHERE   FldId = @fldid;

    END;
GO

------------------------------------------------------------------------------------------------------------
--Troy: Related to US9775. Update metadata. The Kiss Code should be 18 characters. Initial Script provided used 20.
-------------------------------------------------------------------------------------------------------------
DECLARE @fldid INT;

IF EXISTS ( SELECT  *
            FROM    syFields
            WHERE   FldName = 'SchoolCodeKissSchoolId'
                    AND FldLen <> 18 )
    BEGIN
        SET @fldid = ( SELECT   FldId
                       FROM     dbo.syFields
                       WHERE    FldName = 'SchoolCodeKissSchoolId'
                     );

        UPDATE  dbo.syFields
        SET     FldLen = 18
        WHERE   FldId = @fldid;

    END;
GO
------------------------------------------------------------------------------------------------------------
--Balaji DE13165
-------------------------------------------------------------------------------------------------------------
UPDATE syMenuItems SET MenuName='1098T Processing',DisplayName='1098T Processing' WHERE MenuName='1098-T Processing'
GO

-----------------------------------------------------------------------------------------------------------------------
--Troy: US9849:1098-T Box 1 Calculations with Clarification from the Internal Revenue Service
-----------------------------------------------------------------------------------------------------------------------
/****** Object:  StoredProcedure [dbo].[USP_SA_Get1098TDataForTaxYear]    Script Date: 11/22/2016 10:16:06 AM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   type = 'P'
                    AND name = 'USP_SA_Get1098TDataForTaxYear' )
    DROP PROCEDURE USP_SA_Get1098TDataForTaxYear;
GO

/****** Object:  StoredProcedure [dbo].[USP_SA_Get1098TDataForTaxYear]    Script Date: 11/22/2016 10:16:06 AM ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

CREATE PROCEDURE dbo.USP_SA_Get1098TDataForTaxYear
    @TaxYear AS CHAR(4) ,
    @CampusId AS UNIQUEIDENTIFIER

	-------------------------------------------------------------------------------------------------
	--Testing Purposes Only
	--------------------------------------------------------------------------------------------------
	--DECLARE @TaxYear AS CHAR(4)
	--declare @CampusId AS UNIQUEIDENTIFIER
  
	--SET @TaxYear='2016' 
	--SET @CampusId='DC42A60A-5EB9-49B6-ADF6-535966F2E34A' 
	-----------------------------------------------------------------------------------------------
AS -----------------------------------------------------------------------------------------------------    
    BEGIN
        DECLARE @strFirstQuarter AS VARCHAR(10) = '03/31/'
            + CONVERT(VARCHAR(10), CONVERT(INT, @TaxYear) + 1);
        DECLARE @strFirstCalendarDay AS VARCHAR(10) = '01/01/' + @TaxYear;
        DECLARE @intPrevYear AS INTEGER = CONVERT(INT, @TaxYear) - 1;
        DECLARE @intCurrentTaxYear AS INTEGER = CONVERT(INT, @TaxYear);        
        DECLARE @EndOfYear AS VARCHAR(10) = '12/31/' + @TaxYear;
        DECLARE @StartOfYear AS VARCHAR(10) = '1/1/' + @TaxYear;
	
        SELECT DISTINCT
                StuEnrollId ,
                t1.EnrollmentId ,
                CONVERT(CHAR(10), StartDate, 101) AS StartDate ,
                ( SELECT    CONVERT(CHAR(10), MAX(LDA), 101)
                  FROM      arStuEnrollments
                  WHERE     StuEnrollId = t1.StuEnrollId
                            AND LDA IS NOT NULL
                ) AS LDA ,
                ( SELECT    CASE WHEN COUNT(*) > 0 THEN 1
                                 ELSE 0
                            END
                  FROM      arStuEnrollments se ,
                            syStatusCodes sc
                  WHERE     se.StatusCodeId = sc.StatusCodeId
                            AND sc.SysStatusId = 8 -- no start status
                            AND se.StuEnrollId = t1.StuEnrollId
                ) AS IsStudentNoStart ,
                ( SELECT    CONVERT(CHAR(10), MAX(TransDate), 101)
                  FROM      saTransactions
                  WHERE     StuEnrollId = t1.StuEnrollId
                            AND TransDate IS NOT NULL
                            AND Voided = 0
                            AND TransTypeId = 2
                ) AS TransDate ,
                t2.FirstName ,
                t2.LastName ,
                t2.SSN ,
                ( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 )
                       THEN ( SELECT    RTRIM(sa2.Address1 + ' '
                                              + ISNULL(sa2.Address2, ''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                                        AND sa2.default1 = 1
                            )
                       ELSE ( SELECT TOP 1
                                        RTRIM(sa2.Address1 + ' '
                                              + ISNULL(sa2.Address2, ''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS Address1 ,
                ( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 )
                       THEN ( SELECT    RTRIM(ISNULL(sa2.City, ''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                                        AND sa2.default1 = 1
                            )
                       ELSE ( SELECT TOP 1
                                        RTRIM(ISNULL(sa2.City, ''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS City ,
                ( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 )
                       THEN ( SELECT    RTRIM(ISNULL(ss.StateCode, ''))
                              FROM      dbo.arStudAddresses sa2 ,
                                        syStates ss
                              WHERE     sa2.StudentId = t2.StudentId
                                        AND sa2.StateId = ss.StateId
                                        AND sa2.default1 = 1
                            )
                       ELSE ( SELECT TOP 1
                                        RTRIM(ISNULL(ss.StateCode, ''))
                              FROM      dbo.arStudAddresses sa2 ,
                                        syStates ss
                              WHERE     sa2.StudentId = t2.StudentId
                                        AND sa2.StateId = ss.StateId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS State ,
                ( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 )
                       THEN ( SELECT    RTRIM(ISNULL(sa2.Zip, ''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                                        AND sa2.default1 = 1
                            )
                       ELSE ( SELECT TOP 1
                                        RTRIM(ISNULL(sa2.Zip, ''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS Zip ,
                ( SELECT    ISNULL(SUM(TransAmount), 0)
                  FROM      saTransactions SQ1 ,
                            dbo.saTransCodes tc
                  WHERE     SQ1.TransCodeId = tc.TransCodeId
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 0
                            AND SQ1.Voided = 0
                            AND tc.Is1098T = 1
                ) AS TotalCost ,
                ( SELECT    ISNULL(SUM(TransAmount * -1), 0)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 2
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                ) AS TotalPaid ,
                ( SELECT    ISNULL(SUM(TransAmount * -1), 0)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 2
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) < CONVERT(INT, @TaxYear)
                ) AS PriorYearPayments ,
                ( SELECT    ISNULL(SUM(TransAmount), 0)
                  FROM      saTransactions SQ1 ,
                            saRefunds SR
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransactionId = SR.TransactionId
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) < CONVERT(INT, @TaxYear)
                ) AS PriorYearRefunds ,
                ( SELECT    ISNULL(SUM(TransAmount * -1), 0)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 2
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                ) AS GrossPayments ,
                ( SELECT    ISNULL(SUM(TransAmount), 0)
                  FROM      saTransactions SQ1 ,
                            saRefunds SQ2
                  WHERE     SQ1.TransactionId = SQ2.TransactionId
                            AND SQ1.Voided = 0
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                ) AS GrossRefunds ,
                ( SELECT    ISNULL(SUM(TransAmount), 0)
                  FROM      saTransactions A1 ,
                            saFundSources A5 ,
                            saRefunds A6
                  WHERE     A1.TransactionId = A6.TransactionId
                            AND A5.FundSourceId = A6.FundSourceId
                            AND A1.Voided = 0
                            AND A1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                            AND A5.AwardTypeId = 1
                ) AS GrantRefunds ,
                ( SELECT    ISNULL(SUM(TransAmount), 0)
                  FROM      saTransactions A1 ,
                            saFundSources A5 ,
                            saRefunds A6
                  WHERE     A1.TransactionId = A6.TransactionId
                            AND A5.FundSourceId = A6.FundSourceId
                            AND A1.Voided = 0
                            AND A1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                            AND A6.RefundTypeId = 1
                            AND A5.AwardTypeId = 2
                ) AS LoanRefunds ,
                ( SELECT    ISNULL(SUM(TransAmount), 0)
                  FROM      saTransactions SQ1 ,
                            saRefunds SQ2
                  WHERE     SQ1.TransactionId = SQ2.TransactionId
                            AND SQ1.Voided = 0
                            AND SQ2.RefundTypeId = 0
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                ) AS StudentRefunds ,
                ( SELECT    ISNULL(SUM(TransAmount * -1), 0)
                  FROM      ( SELECT    A1.TransDate ,
                                        A1.TransAmount ,
                                        A1.TransDescrip ,
                                        A1.FundSourceId
                              FROM      saTransactions A1 ,
                                        saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                                        AND A5.AwardTypeId = 1
                            ) R
                ) AS GrantPayments ,
                ( SELECT    ISNULL(SUM(TransAmount * -1), 0)
                  FROM      ( SELECT    A1.TransDate ,
                                        A1.TransAmount ,
                                        A1.TransDescrip ,
                                        A1.FundSourceId
                              FROM      saTransactions A1 ,
                                        saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                                        AND A5.AwardTypeId = 2
                            ) R
                ) AS LoanPayments ,
                ( SELECT    ISNULL(SUM(TransAmount * -1), 0)
                  FROM      ( SELECT    A1.TransDate ,
                                        A1.TransAmount ,
                                        A1.TransDescrip ,
                                        A1.FundSourceId
                              FROM      saTransactions A1 ,
                                        saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                                        AND A5.AwardTypeId IN ( 3, 4, 5 )
                            ) R
                ) AS Scholarships ,
                ( SELECT DISTINCT
                            IsContinuingEd
                  FROM      arPrgVersions
                  WHERE     PrgVerId = t1.PrgVerId
                ) AS IsContinuingEd ,
                ( SELECT    ISNULL(SUM(TransAmount * -1), 0)
                  FROM      ( SELECT    A1.TransDate ,
                                        A1.TransAmount ,
                                        A1.TransDescrip ,
                                        A1.FundSourceId
                              FROM      saTransactions A1 ,
                                        saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                                        AND A5.AwardTypeId = 6
                            ) R
                ) AS StudentPayments ,
                ( SELECT    COUNT(*)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.Voided = 0
                            AND TransTypeId = 0
                ) AS CostForEnrollmentExist ,
                ( SELECT    ISNULL(SUM(TransAmount), 0)
                  FROM      saTransactions SQ1 ,
                            dbo.saTransCodes tc
                  WHERE     SQ1.TransCodeId = tc.TransCodeId
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 0
                            AND SQ1.Voided = 0
                            AND tc.Is1098T = 1
                            AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                ) AS CurrentYrInstitutionalCharges ,
                ( SELECT    ISNULL(SUM(TransAmount), 0)
                  FROM      saTransactions SQ1 ,
                            dbo.saTransCodes tc
                  WHERE     SQ1.TransCodeId = tc.TransCodeId
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 0
                            AND SQ1.Voided = 0
                            AND tc.Is1098T = 1
                            AND YEAR(TransDate) < CONVERT(INT, @TaxYear)
                ) AS PriorYrInstitutionalCharges
        FROM    arStuEnrollments t1
                INNER JOIN arStudent t2 ON t1.StudentId = t2.StudentId
                INNER JOIN syStatusCodes t5 ON t1.StatusCodeId = t5.StatusCodeId
                INNER JOIN arPrgVersions t7 ON t1.PrgVerId = t7.PrgVerId
                INNER JOIN arPrograms t6 ON t6.ProgId = t7.ProgId
        WHERE   t1.CampusId = @CampusId
                AND t6.Is1098T = '1'
                AND (
                      --This part mostly handles the attendance taking schools. The student must have started
            --before the end of the current year and must have attendance for the current year.
                      ( t1.StartDate <= @EndOfYear
                        AND t1.LDA >= @StartOfYear
                      )
                      OR
			--Student enrolled in the current year but starts in the first quarter of next year
                      ( t1.StartDate > @EndOfYear
                        AND t1.StartDate <= @strFirstQuarter
                        AND YEAR(t1.EnrollDate) = CONVERT(INT, @TaxYear)
                      )
                      OR
			--This part mostly handles schools that do not not take attendance
			--Students who are still in school - currently attending, LOA, Suspended, Academic Probation, Suspension
			--Note that we include future starts.                                
                      ( t1.StartDate <= @EndOfYear
                        AND t5.SysStatusId IN ( 7, 9, 10, 11, 20, 21, 22 )
                      )
                      OR
			--Include no starts who started during the year. They will be added to the exclusions table.
                      ( t1.StartDate BETWEEN @StartOfYear AND @EndOfYear
                        AND t5.SysStatusId = 8
                      )
                      OR
			--Students who graduated during the current year and the school
                      ( t1.StartDate <= @EndOfYear
                        AND t5.SysStatusId = 14
                        AND t1.ExpGradDate > @StartOfYear
                      )
                    )
            --The student must have a record in the student addresses table
                AND EXISTS ( SELECT *
                             FROM   dbo.arStudAddresses sa
                             WHERE  sa.StudentId = t2.StudentId )
        ORDER BY LastName;

    END;
GO
----------------------------------------------------------------------------------------------------------------------
--End Troy: US9849:1098-T Box 1 Calculations with Clarification from the Internal Revenue Service
-----------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------------
--Troy: Take account of charges posted as adjustments (TransTypeId=1)
-----------------------------------------------------------------------------------------------------------------------
/****** Object:  StoredProcedure [dbo].[USP_SA_Get1098TDataForTaxYear]    Script Date: 12/5/2016 10:15:06 AM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   type = 'P'
                    AND name = 'USP_SA_Get1098TDataForTaxYear' )
    DROP PROCEDURE USP_SA_Get1098TDataForTaxYear;
GO

/****** Object:  StoredProcedure [dbo].[USP_SA_Get1098TDataForTaxYear]    Script Date: 12/5/2016 10:15:06 AM ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

CREATE PROCEDURE dbo.USP_SA_Get1098TDataForTaxYear
    @TaxYear AS CHAR(4) ,
    @CampusId AS UNIQUEIDENTIFIER

	-------------------------------------------------------------------------------------------------
	--Testing Purposes Only
	--------------------------------------------------------------------------------------------------
	--DECLARE @TaxYear AS CHAR(4)
	--declare @CampusId AS UNIQUEIDENTIFIER
  
	--SET @TaxYear='2016' 
	--SET @CampusId='3F5E839A-589A-4B2A-B258-35A1A8B3B819' 
	-----------------------------------------------------------------------------------------------
AS
    BEGIN
        DECLARE @strFirstQuarter AS VARCHAR(10) = '03/31/'
            + CONVERT(VARCHAR(10), CONVERT(INT, @TaxYear) + 1);
        DECLARE @strFirstCalendarDay AS VARCHAR(10) = '01/01/' + @TaxYear;
        DECLARE @intPrevYear AS INTEGER = CONVERT(INT, @TaxYear) - 1;
        DECLARE @intCurrentTaxYear AS INTEGER = CONVERT(INT, @TaxYear);        
        DECLARE @EndOfYear AS VARCHAR(10) = '12/31/' + @TaxYear;
        DECLARE @StartOfYear AS VARCHAR(10) = '1/1/' + @TaxYear;
	
        SELECT DISTINCT
                StuEnrollId ,
                t1.EnrollmentId ,
                CONVERT(CHAR(10), StartDate, 101) AS StartDate ,
                ( SELECT    CONVERT(CHAR(10), MAX(LDA), 101)
                  FROM      arStuEnrollments
                  WHERE     StuEnrollId = t1.StuEnrollId
                            AND LDA IS NOT NULL
                ) AS LDA ,
                ( SELECT    CASE WHEN COUNT(*) > 0 THEN 1
                                 ELSE 0
                            END
                  FROM      arStuEnrollments se ,
                            syStatusCodes sc
                  WHERE     se.StatusCodeId = sc.StatusCodeId
                            AND sc.SysStatusId = 8 -- no start status
                            AND se.StuEnrollId = t1.StuEnrollId
                ) AS IsStudentNoStart ,
                ( SELECT    CONVERT(CHAR(10), MAX(TransDate), 101)
                  FROM      saTransactions
                  WHERE     StuEnrollId = t1.StuEnrollId
                            AND TransDate IS NOT NULL
                            AND Voided = 0
                            AND TransTypeId = 2
                ) AS TransDate ,
                t2.FirstName ,
                t2.LastName ,
                t2.SSN ,
                ( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 )
                       THEN ( SELECT    RTRIM(sa2.Address1 + ' '
                                              + ISNULL(sa2.Address2, ''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                                        AND sa2.default1 = 1
                            )
                       ELSE ( SELECT TOP 1
                                        RTRIM(sa2.Address1 + ' '
                                              + ISNULL(sa2.Address2, ''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS Address1 ,
                ( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 )
                       THEN ( SELECT    RTRIM(ISNULL(sa2.City, ''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                                        AND sa2.default1 = 1
                            )
                       ELSE ( SELECT TOP 1
                                        RTRIM(ISNULL(sa2.City, ''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS City ,
                ( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 )
                       THEN ( SELECT    RTRIM(ISNULL(ss.StateCode, ''))
                              FROM      dbo.arStudAddresses sa2 ,
                                        syStates ss
                              WHERE     sa2.StudentId = t2.StudentId
                                        AND sa2.StateId = ss.StateId
                                        AND sa2.default1 = 1
                            )
                       ELSE ( SELECT TOP 1
                                        RTRIM(ISNULL(ss.StateCode, ''))
                              FROM      dbo.arStudAddresses sa2 ,
                                        syStates ss
                              WHERE     sa2.StudentId = t2.StudentId
                                        AND sa2.StateId = ss.StateId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS State ,
                ( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 )
                       THEN ( SELECT    RTRIM(ISNULL(sa2.Zip, ''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                                        AND sa2.default1 = 1
                            )
                       ELSE ( SELECT TOP 1
                                        RTRIM(ISNULL(sa2.Zip, ''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS Zip ,
                ( SELECT    ISNULL(SUM(TransAmount), 0)
                  FROM      saTransactions SQ1 ,
                            dbo.saTransCodes tc
                  WHERE     SQ1.TransCodeId = tc.TransCodeId
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND ( SQ1.TransTypeId = 0
                                  OR ( SQ1.TransTypeId = 1
                                       AND tc.SysTransCodeId <> 16
                                     )
                                )
                            AND SQ1.Voided = 0
                            AND tc.Is1098T = 1
                ) AS TotalCost ,
                ( SELECT    ISNULL(SUM(TransAmount * -1), 0)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 2
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                ) AS TotalPaid ,
                ( SELECT    ISNULL(SUM(TransAmount * -1), 0)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 2
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) < CONVERT(INT, @TaxYear)
                ) AS PriorYearPayments ,
                ( SELECT    ISNULL(SUM(TransAmount), 0)
                  FROM      saTransactions SQ1 ,
                            saRefunds SR
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransactionId = SR.TransactionId
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) < CONVERT(INT, @TaxYear)
                ) AS PriorYearRefunds ,
                ( SELECT    ISNULL(SUM(TransAmount * -1), 0)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 2
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                ) AS GrossPayments ,
                ( SELECT    ISNULL(SUM(TransAmount), 0)
                  FROM      saTransactions SQ1 ,
                            saRefunds SQ2
                  WHERE     SQ1.TransactionId = SQ2.TransactionId
                            AND SQ1.Voided = 0
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                ) AS GrossRefunds ,
                ( SELECT    ISNULL(SUM(TransAmount), 0)
                  FROM      saTransactions A1 ,
                            saFundSources A5 ,
                            saRefunds A6
                  WHERE     A1.TransactionId = A6.TransactionId
                            AND A5.FundSourceId = A6.FundSourceId
                            AND A1.Voided = 0
                            AND A1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                            AND A5.AwardTypeId = 1
                ) AS GrantRefunds ,
                ( SELECT    ISNULL(SUM(TransAmount), 0)
                  FROM      saTransactions A1 ,
                            saFundSources A5 ,
                            saRefunds A6
                  WHERE     A1.TransactionId = A6.TransactionId
                            AND A5.FundSourceId = A6.FundSourceId
                            AND A1.Voided = 0
                            AND A1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                            AND A6.RefundTypeId = 1
                            AND A5.AwardTypeId = 2
                ) AS LoanRefunds ,
                ( SELECT    ISNULL(SUM(TransAmount), 0)
                  FROM      saTransactions SQ1 ,
                            saRefunds SQ2
                  WHERE     SQ1.TransactionId = SQ2.TransactionId
                            AND SQ1.Voided = 0
                            AND SQ2.RefundTypeId = 0
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                ) AS StudentRefunds ,
                ( SELECT    ISNULL(SUM(TransAmount * -1), 0)
                  FROM      ( SELECT    A1.TransDate ,
                                        A1.TransAmount ,
                                        A1.TransDescrip ,
                                        A1.FundSourceId
                              FROM      saTransactions A1 ,
                                        saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                                        AND A5.AwardTypeId = 1
                            ) R
                ) AS GrantPayments ,
                ( SELECT    ISNULL(SUM(TransAmount * -1), 0)
                  FROM      ( SELECT    A1.TransDate ,
                                        A1.TransAmount ,
                                        A1.TransDescrip ,
                                        A1.FundSourceId
                              FROM      saTransactions A1 ,
                                        saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                                        AND A5.AwardTypeId = 2
                            ) R
                ) AS LoanPayments ,
                ( SELECT    ISNULL(SUM(TransAmount * -1), 0)
                  FROM      ( SELECT    A1.TransDate ,
                                        A1.TransAmount ,
                                        A1.TransDescrip ,
                                        A1.FundSourceId
                              FROM      saTransactions A1 ,
                                        saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                                        AND A5.AwardTypeId IN ( 3, 4, 5 )
                            ) R
                ) AS Scholarships ,
                ( SELECT DISTINCT
                            IsContinuingEd
                  FROM      arPrgVersions
                  WHERE     PrgVerId = t1.PrgVerId
                ) AS IsContinuingEd ,
                ( SELECT    ISNULL(SUM(TransAmount * -1), 0)
                  FROM      ( SELECT    A1.TransDate ,
                                        A1.TransAmount ,
                                        A1.TransDescrip ,
                                        A1.FundSourceId
                              FROM      saTransactions A1 ,
                                        saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                                        AND A5.AwardTypeId = 6
                            ) R
                ) AS StudentPayments ,
                ( SELECT    COUNT(*)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.Voided = 0
                            AND TransTypeId = 0
                ) AS CostForEnrollmentExist ,
                ( SELECT    ISNULL(SUM(TransAmount), 0)
                  FROM      saTransactions SQ1 ,
                            dbo.saTransCodes tc
                  WHERE     SQ1.TransCodeId = tc.TransCodeId
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND ( SQ1.TransTypeId = 0
                                  OR ( SQ1.TransTypeId = 1
                                       AND tc.SysTransCodeId <> 16
                                     )
                                )
                            AND SQ1.Voided = 0
                            AND tc.Is1098T = 1
                            AND YEAR(TransDate) = CONVERT(INT, @TaxYear)
                ) AS CurrentYrInstitutionalCharges ,
                ( SELECT    ISNULL(SUM(TransAmount), 0)
                  FROM      saTransactions SQ1 ,
                            dbo.saTransCodes tc
                  WHERE     SQ1.TransCodeId = tc.TransCodeId
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND ( SQ1.TransTypeId = 0
                                  OR ( SQ1.TransTypeId = 1
                                       AND tc.SysTransCodeId <> 16
                                     )
                                )
                            AND SQ1.Voided = 0
                            AND tc.Is1098T = 1
                            AND YEAR(TransDate) < CONVERT(INT, @TaxYear)
                ) AS PriorYrInstitutionalCharges
        FROM    arStuEnrollments t1
                INNER JOIN arStudent t2 ON t1.StudentId = t2.StudentId
                INNER JOIN syStatusCodes t5 ON t1.StatusCodeId = t5.StatusCodeId
                INNER JOIN arPrgVersions t7 ON t1.PrgVerId = t7.PrgVerId
                INNER JOIN arPrograms t6 ON t6.ProgId = t7.ProgId
        WHERE   t1.CampusId = @CampusId
                --AND t6.Is1098T = '1'
                AND (
                      --This part mostly handles the attendance taking schools. The student must have started
            --before the end of the current year and must have attendance for the current year.
                      ( t1.StartDate <= @EndOfYear
                        AND t1.LDA >= @StartOfYear
                      )
                      OR
			--Student enrolled in the current year but starts in the first quarter of next year
                      ( t1.StartDate > @EndOfYear
                        AND t1.StartDate <= @strFirstQuarter
                        AND YEAR(t1.EnrollDate) = CONVERT(INT, @TaxYear)
                      )
                      OR
			--This part mostly handles schools that do not not take attendance
			--Students who are still in school - currently attending, LOA, Suspended, Academic Probation, Suspension
			--Note that we include future starts.                                
                      ( t1.StartDate <= @EndOfYear
                        AND t5.SysStatusId IN ( 7, 9, 10, 11, 20, 21, 22 )
                      )
                      OR
			--Include no starts who started during the year. They will be added to the exclusions table.
                      ( t1.StartDate BETWEEN @StartOfYear AND @EndOfYear
                        AND t5.SysStatusId = 8
                      )
                      OR
			--Students who graduated during the current year and the school
                      ( t1.StartDate <= @EndOfYear
                        AND t5.SysStatusId = 14
                        AND t1.ExpGradDate > @StartOfYear
                      )
                    )
            --The student must have a record in the student addresses table
                AND EXISTS ( SELECT *
                             FROM   dbo.arStudAddresses sa
                             WHERE  sa.StudentId = t2.StudentId )
        ORDER BY LastName;

    END;
GO
-----------------------------------------------------------------------------------------------------------------------
--End Troy: Take account of charges posted as adjustments (TransTypeId=1)
-----------------------------------------------------------------------------------------------------------------------


