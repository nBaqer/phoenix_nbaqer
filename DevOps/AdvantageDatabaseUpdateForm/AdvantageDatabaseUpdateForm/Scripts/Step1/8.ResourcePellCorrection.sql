﻿-- Delete resources 834 and 837 if exists (and associated records)
BEGIN TRANSACTION Resource834;
DECLARE @Key INT = 834;
BEGIN TRY
    IF EXISTS ( SELECT  *
                FROM    dbo.syResources
                WHERE   ResourceID = @Key )
        BEGIN

            IF EXISTS ( SELECT  *
                        FROM    dbo.syRlsResLvls
                        WHERE   ResourceID = @Key )
                BEGIN
                    DELETE  dbo.syRlsResLvls
                    WHERE   ResourceID = @Key;
                END;

            IF EXISTS ( SELECT  *
                        FROM    dbo.syNavigationNodes
                        WHERE   ResourceId = @Key )
                BEGIN
                    DELETE  dbo.syNavigationNodes
                    WHERE   ResourceId = @Key;
                END;

            IF EXISTS ( SELECT  *
                        FROM    dbo.syReports
                        WHERE   ResourceId = @Key )
                BEGIN
                    DELETE  dbo.syReports
                    WHERE   ResourceId = @Key;
                END;

            IF EXISTS ( SELECT  *
                        FROM    dbo.syMenuItems
                        WHERE   ResourceId = @Key )
                BEGIN
                    DELETE  dbo.syMenuItems
                    WHERE   ResourceId = @Key;
                END;

            IF EXISTS ( SELECT  *
                        FROM    dbo.syReportTabs
                        WHERE   DisplayName = 'Custom Options Set for PellRecipientsandStaffordReport' )
                BEGIN
                    DELETE  dbo.syReportTabs
                    WHERE   DisplayName = 'Custom Options Set for PellRecipientsandStaffordReport';
                END;

            DELETE  dbo.syResources
            WHERE   ResourceID = @Key;
        END;
    COMMIT TRANSACTION Resource834;
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION Resource834;
    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT
       ,@LastLine INT;

    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE()
           ,@LastLine = ERROR_LINE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION UpdateMenu;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH;
GO
-- DELETE 837
BEGIN TRANSACTION Resource837;
DECLARE @Key INT = 837;
BEGIN TRY
    IF EXISTS ( SELECT  *
                FROM    dbo.syResources
                WHERE   ResourceID = @Key )
        BEGIN

            IF EXISTS ( SELECT  *
                        FROM    dbo.syRlsResLvls
                        WHERE   ResourceID = @Key )
                BEGIN
                    DELETE  dbo.syRlsResLvls
                    WHERE   ResourceID = @Key;
                END;

            IF EXISTS ( SELECT  *
                        FROM    dbo.syNavigationNodes
                        WHERE   ResourceId = @Key )
                BEGIN
                    DELETE  dbo.syNavigationNodes
                    WHERE   ResourceId = @Key;
                END;

            IF EXISTS ( SELECT  *
                        FROM    dbo.syReports
                        WHERE   ResourceId = @Key )
                BEGIN
                    DELETE  dbo.syReports
                    WHERE   ResourceId = @Key;
                END;

            IF EXISTS ( SELECT  *
                        FROM    dbo.syMenuItems
                        WHERE   ResourceId = @Key )
                BEGIN
                    DELETE  dbo.syMenuItems
                    WHERE   ResourceId = @Key;
                END;

            IF EXISTS ( SELECT  *
                        FROM    dbo.syReportTabs
                        WHERE   DisplayName = 'Custom Options Set for PellRecipientsandStaffordReport' )
                BEGIN
                    DELETE  dbo.syReportTabs
                    WHERE   DisplayName = 'Custom Options Set for PellRecipientsandStaffordReport';
                END;
 
            DELETE  dbo.syResources
            WHERE   ResourceID = @Key;
        END;
    COMMIT TRANSACTION Resource837;
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION Resource837;
    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT
       ,@LastLine INT;

    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE()
           ,@LastLine = ERROR_LINE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION UpdateMenu;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH;
GO

