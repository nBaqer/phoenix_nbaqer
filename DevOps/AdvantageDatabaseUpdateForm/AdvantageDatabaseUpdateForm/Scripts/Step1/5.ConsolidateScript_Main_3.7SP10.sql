-- ********************************************************************************************************
-- Consolidated Script Version 3.7SP8
-- Schema Changes Zone
-- Please deploy your schema changes in this area
-- (Format the code with TSQl Format before insert here please)
-- ********************************************************************************************************
---------------------------------------------------------------------------------------------------------------------
--Troy: US9608: Create fields in Sycampuses to store Token and Kiss Code
----------------------------------------------------------------------------------------------------------------------
--Add the fields to the syCampuses table
IF NOT EXISTS ( SELECT  *
                FROM    sys.columns
                WHERE   name = N'Token1098TService'
                        AND object_id = OBJECT_ID(N'syCampuses') )
    BEGIN
        ALTER TABLE dbo.syCampuses ADD Token1098TService VARCHAR(38);
    END;
IF NOT EXISTS ( SELECT  *
                FROM    sys.columns
                WHERE   name = N'SchoolCodeKissSchoolId'
                        AND object_id = OBJECT_ID(N'syCampuses') )
    BEGIN
        ALTER TABLE dbo.syCampuses ADD SchoolCodeKissSchoolId VARCHAR(20);
    END;
GO


--Add Token1098TService field to the metadata
DECLARE @fldid INT;
DECLARE @fldcapid INT;

IF NOT EXISTS ( SELECT  *
                FROM    syFields
                WHERE   FldName = 'Token1098TService' )
    BEGIN
        SET @fldid = ( (
                         SELECT MAX(FldId)
                         FROM   dbo.syFields
                       ) + 1 );
        SET @fldcapid = ( (
                            SELECT  MAX(FldCapId)
                            FROM    dbo.syFldCaptions
                          ) + 1 );
    
		--add record to syfields
        INSERT  INTO dbo.syFields
                (
                 FldId
                ,FldName
                ,FldTypeId
                ,FldLen
                ,DDLId
                ,DerivedFld
                ,SchlReq
                ,LogChanges
                ,Mask
		        )
                SELECT  @fldid
                       , -- FldId - int
                        'Token1098TService'
                       , -- FldName - varchar(200)
                        200
                       , -- FldTypeId - int
                        38
                       , -- FldLen - int
                        NULL
                       , -- DDLId - int
                        NULL
                       , -- DerivedFld - bit
                        0
                       , -- SchlReq - bit
                        0
                       , -- LogChanges - bit
                        NULL;  -- Mask - varchar(50)

		--add record to field captions
        INSERT  INTO dbo.syFldCaptions
                (
                 FldCapId
                ,FldId
                ,LangId
                ,Caption
                ,FldDescrip
		        )
        VALUES  (
                 @fldcapid
                , -- FldCapId - int
                 @fldid
                , -- FldId - int
                 1
                , -- LangId - tinyint
                 'Token'
                , -- Caption - varchar(100)
                 '1098T Token'  -- FldDescrip - varchar(150)
		        );

    END;  
GO




--Add SchoolCodeKissSchoolId field to the metadata
DECLARE @fldid INT;
DECLARE @fldcapid INT;

IF NOT EXISTS ( SELECT  *
                FROM    syFields
                WHERE   FldName = 'SchoolCodeKissSchoolId' )
    BEGIN
        SET @fldid = ( (
                         SELECT MAX(FldId)
                         FROM   dbo.syFields
                       ) + 1 );
        SET @fldcapid = ( (
                            SELECT  MAX(FldCapId)
                            FROM    dbo.syFldCaptions
                          ) + 1 );
		
		--add record to fields table
        INSERT  INTO dbo.syFields
                (
                 FldId
                ,FldName
                ,FldTypeId
                ,FldLen
                ,DDLId
                ,DerivedFld
                ,SchlReq
                ,LogChanges
                ,Mask
		        )
                SELECT  @fldid
                       , -- FldId - int
                        'SchoolCodeKissSchoolId'
                       , -- FldName - varchar(200)
                        200
                       , -- FldTypeId - int
                        20
                       , -- FldLen - int
                        NULL
                       , -- DDLId - int
                        NULL
                       , -- DerivedFld - bit
                        0
                       , -- SchlReq - bit
                        0
                       , -- LogChanges - bit
                        NULL;  -- Mask - varchar(50)

		--add record to field captions
        INSERT  INTO dbo.syFldCaptions
                (
                 FldCapId
                ,FldId
                ,LangId
                ,Caption
                ,FldDescrip
		        )
        VALUES  (
                 @fldcapid
                , -- FldCapId - int
                 @fldid
                , -- FldId - int
                 1
                , -- LangId - tinyint
                 'Kiss Code'
                , -- Caption - varchar(100)
                 '1098T Kiss Code'  -- FldDescrip - varchar(150)
		        );


    END;  
GO

--Add the Token field to the tables metadata
DECLARE @tokenfldid INT;
DECLARE @tblid INT;
DECLARE @tblfldsid INT;

SET @tokenfldid = (
                    SELECT  FldId
                    FROM    syFields
                    WHERE   FldName = 'Token1098TService'
                  );
SET @tblid = (
               SELECT   TblId
               FROM     syTables
               WHERE    TblName = 'syCampuses'
             );
SET @tblfldsid = ( (
                     SELECT MAX(TblFldsId)
                     FROM   dbo.syTblFlds
                   ) + 1 );

IF NOT EXISTS ( SELECT  *
                FROM    syTblFlds
                WHERE   TblId = @tblid
                        AND FldId = @tokenfldid )
    BEGIN
        INSERT  INTO dbo.syTblFlds
                (
                 TblFldsId
                ,TblId
                ,FldId
                ,CategoryId
                ,FKColDescrip
		        )
                SELECT  @tblfldsid
                       , -- TblFldsId - int
                        @tblid
                       , -- TblId - int
                        @tokenfldid
                       , -- FldId - int
                        NULL
                       , -- CategoryId - int
                        NULL;  -- FKColDescrip - varchar(50)

  
    END;  
GO


--Add the KISS Code field to the tables metadata
DECLARE @kisscodefldid INT;
DECLARE @tblid INT;
DECLARE @tblfldsid INT;

SET @kisscodefldid = (
                       SELECT   FldId
                       FROM     syFields
                       WHERE    FldName = 'SchoolCodeKissSchoolId'
                     );
SET @tblid = (
               SELECT   TblId
               FROM     syTables
               WHERE    TblName = 'syCampuses'
             );
SET @tblfldsid = ( (
                     SELECT MAX(TblFldsId)
                     FROM   dbo.syTblFlds
                   ) + 1 );

IF NOT EXISTS ( SELECT  *
                FROM    syTblFlds
                WHERE   TblId = @tblid
                        AND FldId = @kisscodefldid )
    BEGIN
        INSERT  INTO dbo.syTblFlds
                (
                 TblFldsId
                ,TblId
                ,FldId
                ,CategoryId
                ,FKColDescrip
		        )
                SELECT  @tblfldsid
                       , -- TblFldsId - int
                        @tblid
                       , -- TblId - int
                        @kisscodefldid
                       , -- FldId - int
                        NULL
                       , -- CategoryId - int
                        NULL;  -- FKColDescrip - varchar(50)

  
    END;  
GO


--Add the Token field to resources metadata for the campuses page
DECLARE @fldid INT;
DECLARE @tblid INT;
DECLARE @resourceid INT;
DECLARE @tblfldsid INT;
DECLARE @resdefid INT;

SET @resourceid = (
                    SELECT  ResourceID
                    FROM    syResources
                    WHERE   Resource = 'Campus'
                  );
SET @fldid = (
               SELECT   FldId
               FROM     dbo.syFields
               WHERE    FldName = 'Token1098TService'
             );
SET @tblid = (
               SELECT   TblId
               FROM     syTables
               WHERE    TblName = 'syCampuses'
             );

SET @tblfldsid = (
                   SELECT   TblFldsId
                   FROM     syTblFlds
                   WHERE    TblId = @tblid
                            AND FldId = @fldid
                 );

IF NOT EXISTS ( SELECT  *
                FROM    dbo.syResTblFlds
                WHERE   TblFldsId = @tblfldsid )
    BEGIN
        SET @resdefid = ( (
                            SELECT  MAX(ResDefId)
                            FROM    dbo.syResTblFlds
                          ) + 1 );
    
        INSERT  INTO dbo.syResTblFlds
                (
                 ResDefId
                ,ResourceId
                ,TblFldsId
                ,Required
                ,SchlReq
                ,ControlName
                ,UsePageSetup
		        )
                SELECT  @resdefid
                       , -- ResDefId - int
                        @resourceid
                       , -- ResourceId - smallint
                        @tblfldsid
                       , -- TblFldsId - int
                        0
                       , -- Required - bit
                        0
                       , -- SchlReq - bit
                        NULL
                       , -- ControlName - varchar(50)
                        1;  -- UsePageSetup - bit	  
  
    END;  

GO



--Add the KISS Code field to resources metadata for the campuses page
DECLARE @fldid INT;
DECLARE @tblid INT;
DECLARE @resourceid INT;
DECLARE @tblfldsid INT;
DECLARE @resdefid INT;

SET @resourceid = (
                    SELECT  ResourceID
                    FROM    syResources
                    WHERE   Resource = 'Campus'
                  );
SET @fldid = (
               SELECT   FldId
               FROM     dbo.syFields
               WHERE    FldName = 'SchoolCodeKissSchoolId'
             );
SET @tblid = (
               SELECT   TblId
               FROM     syTables
               WHERE    TblName = 'syCampuses'
             );

SET @tblfldsid = (
                   SELECT   TblFldsId
                   FROM     syTblFlds
                   WHERE    TblId = @tblid
                            AND FldId = @fldid
                 );

IF NOT EXISTS ( SELECT  *
                FROM    dbo.syResTblFlds
                WHERE   TblFldsId = @tblfldsid )
    BEGIN
        SET @resdefid = ( (
                            SELECT  MAX(ResDefId)
                            FROM    dbo.syResTblFlds
                          ) + 1 );
    
        INSERT  INTO dbo.syResTblFlds
                (
                 ResDefId
                ,ResourceId
                ,TblFldsId
                ,Required
                ,SchlReq
                ,ControlName
                ,UsePageSetup
		        )
                SELECT  @resdefid
                       , -- ResDefId - int
                        @resourceid
                       , -- ResourceId - smallint
                        @tblfldsid
                       , -- TblFldsId - int
                        0
                       , -- Required - bit
                        0
                       , -- SchlReq - bit
                        NULL
                       , -- ControlName - varchar(50)
                        1;  -- UsePageSetup - bit	  
  
    END;  

GO
---------------------------------------------------------------------------------------------------------------------
--End US9608: Create fields in Sycampuses to store Token and Kiss Code
----------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------
-- Troy US9610:Pull a Default Address for 1098T
-- DE 13327 : Tuition on 1098T exception list issue running upload
-- 1/27/2017
------------------------------------------------------------------------------------------------------
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

------------------------------------------------------------------------------------------------------
--End Troy US9610:Pull a Default Address for 1098T
------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------
--Start Balaji US9611:1098T Exceptions Report
------------------------------------------------------------------------------------------------------
UPDATE  arPrograms
SET     Is1098T = 0
WHERE   Is1098T IS NULL;
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   type = 'P'
                    AND name = 'USP_Programs_No1098T' )
    BEGIN
        DROP PROCEDURE USP_Programs_No1098T;
    END;
GO
CREATE PROCEDURE USP_Programs_No1098T
    @CampusId UNIQUEIDENTIFIER
AS
    SELECT  DISTINCT
            P.ProgDescrip
    FROM    dbo.arPrograms P
    INNER JOIN dbo.syCmpGrpCmps CGC ON CGC.CampGrpId = P.CampGrpId
    INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
    WHERE   P.Is1098T = 0
            AND P.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
            AND C.CampusId = @CampusId
    ORDER BY ProgDescrip; 
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   type = 'P'
                    AND name = 'USP_TransCodes_No1098T' )
    BEGIN
        DROP PROCEDURE USP_TransCodes_No1098T;
    END;
GO
CREATE PROCEDURE USP_TransCodes_No1098T
    @CampusId UNIQUEIDENTIFIER
AS
    DECLARE @ApplicantFeeId INT;
    SET @ApplicantFeeId = (
                            SELECT TOP 1
                                    SysTransCodeId
                            FROM    dbo.saSysTransCodes
                            WHERE   LOWER(Description) = 'applicant fee'
                          ); 

    SELECT  DISTINCT
            P.TransCodeCode
           ,P.TransCodeDescrip
    FROM    dbo.saTransCodes P
    INNER JOIN dbo.syCmpGrpCmps CGC ON CGC.CampGrpId = P.CampGrpId
    INNER JOIN syCampuses C ON C.CampusId = CGC.CampusId
    WHERE   P.Is1098T = 0
            AND P.SysTransCodeId <> @ApplicantFeeId
            AND P.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
            AND C.CampusId = @CampusId
    ORDER BY TransCodeDescrip;
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   type = 'P'
                    AND name = 'USP_Students_AddressMissing' )
    BEGIN
        DROP PROCEDURE USP_Students_AddressMissing;
    END;
GO
CREATE PROCEDURE USP_Students_AddressMissing @CampusId VARCHAR(50)
AS
    DECLARE @SettingId INT
       ,@SettingValue VARCHAR(50);
    SET @SettingId = (
                       SELECT   SettingId
                       FROM     dbo.syConfigAppSettings
                       WHERE    LOWER(KeyName) = 'studentidentifier'
                     );
    SET @SettingValue = (
                          SELECT TOP 1
                                    Value
                          FROM      dbo.syConfigAppSetValues
                          WHERE     SettingId = @SettingId
                        );

    PRINT @SettingValue;

    SELECT DISTINCT
            CASE WHEN LOWER(@SettingValue) = 'ssn' THEN CASE WHEN LEN(S.SSN) = 9 THEN '***-**-' + SUBSTRING(S.SSN,6,4)
                                                             ELSE S.SSN
                                                        END
                 ELSE S.StudentNumber
            END AS StudentID
           ,S.LastName
           ,S.MiddleName
           ,S.FirstName
           ,CASE WHEN SA.Address1 IS NULL THEN 'X'
                 ELSE ''
            END AS Address1Missing
           ,CASE WHEN SA.City IS NULL THEN 'X'
                 ELSE ''
            END AS CityMissing
           ,CASE WHEN SA.StateId IS NULL THEN 'X'
                 ELSE ''
            END AS StateMissing
           ,CASE WHEN (
                        S.FirstName IS NULL
                        OR S.LastName IS NULL
                      ) THEN 'X'
                 ELSE ''
            END AS NameMissing
           ,CASE WHEN (
                        S.SSN IS NULL
                        OR LEN(ISNULL(S.SSN,0)) < 9
                      ) THEN 'X'
                 ELSE ''
            END AS SSNMissing
           ,CASE WHEN (
                        SA.Zip IS NULL
                        OR (
                             SA.Zip IS NOT NULL
                             AND ( LEN(SA.Zip) < 5 )
                           ) -- If Zip less than 5 digits
                        OR (
                             SA.Zip IS NOT NULL
                             AND (
                                   (
                                     LEN(SA.Zip) > 5
                                     AND LEN(SA.Zip) < 9
                                   )
                                   OR LEN(SA.Zip) > 9
                                 )
                           ) -- if zip is not 9 characters long
                        OR (
                             SA.Zip IS NOT NULL
                             AND LEN(SA.Zip) >= 5
                             AND PATINDEX('%[A-Z]%',SUBSTRING(SA.Zip,1,5)) > 0
                           )
                      ) THEN 'X'
                 ELSE ''
            END AS ZipMissing
    FROM    arStudent S
    INNER JOIN (
                 SELECT *
                       ,ROW_NUMBER() OVER ( PARTITION BY StudentId ORDER BY default1 DESC, ModDate DESC ) AS RowNumber
                 FROM   dbo.arStudAddresses
               ) SA ON SA.StudentId = S.StudentId
    INNER JOIN dbo.arStuEnrollments SE ON S.StudentId = SE.StudentId
    INNER JOIN dbo.arPrgVersions PV ON PV.PrgVerId = SE.PrgVerId
    INNER JOIN dbo.arPrograms P ON P.ProgId = PV.ProgId
    WHERE   SE.CampusId = @CampusId
            AND SA.RowNumber = 1 -- If there are multiple addresses bring the default address or the latest one
            AND SA.ForeignZip = 0
            AND P.Is1098T = 1
            AND (
                  SA.Address1 IS NULL
                  OR SA.Zip IS NULL
                  OR (
                       SA.Zip IS NOT NULL
                       AND ( LEN(SA.Zip) < 5 )
                     ) -- If Zip less than 5 digits
                  OR (
                       SA.Zip IS NOT NULL
                       AND (
                             (
                               LEN(SA.Zip) > 5
                               AND LEN(SA.Zip) < 9
                             )
                             OR LEN(SA.Zip) > 9
                           )
                     ) -- if zip is not 9 characters long
                  OR (
                       SA.Zip IS NOT NULL
                       AND LEN(SA.Zip) >= 5
                       AND PATINDEX('%[A-Z]%',SUBSTRING(SA.Zip,1,5)) > 0
                     ) -- if there is a character in the first 5 positions
                  OR SA.City IS NULL
                  OR SA.StateId IS NULL
                  OR (
                       S.SSN IS NULL
                       OR LEN(ISNULL(S.SSN,0)) < 9
                     )
                  OR (
                       S.FirstName IS NULL
                       OR S.LastName IS NULL
                     )
                )
            --AND S.StudentStatus = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
    ORDER BY S.LastName; 
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   type = 'P'
                    AND name = 'USP_InternationalStudents_List' )
    BEGIN
        DROP PROCEDURE USP_InternationalStudents_List;
    END;
GO
CREATE PROCEDURE USP_InternationalStudents_List @CampusId VARCHAR(50)
AS
    DECLARE @SettingId INT
       ,@SettingValue VARCHAR(50);
    SET @SettingId = (
                       SELECT   SettingId
                       FROM     dbo.syConfigAppSettings
                       WHERE    LOWER(KeyName) = 'studentidentifier'
                     );
    
    SET @SettingValue = (
                          SELECT TOP 1
                                    Value
                          FROM      dbo.syConfigAppSetValues
                          WHERE     SettingId = @SettingId
                        );

    SELECT DISTINCT
            CASE WHEN LOWER(@SettingValue) = 'ssn' THEN CASE WHEN LEN(S.SSN) = 9 THEN '***-**-' + SUBSTRING(S.SSN,6,4)
                                                             ELSE S.SSN
                                                        END
                 ELSE S.StudentNumber
            END AS StudentID
           ,S.LastName
           ,S.MiddleName
           ,S.FirstName
    FROM    arStudent S
    INNER JOIN dbo.arStuEnrollments SE ON S.StudentId = SE.StudentId
    INNER JOIN dbo.arPrgVersions PV ON PV.PrgVerId = SE.PrgVerId
    INNER JOIN dbo.arPrograms P ON P.ProgId = PV.ProgId
    INNER JOIN dbo.adLeadByLeadGroups LLG ON LLG.StuEnrollId = SE.StuEnrollId
    INNER JOIN dbo.adLeadGroups LG ON LG.LeadGrpId = LLG.LeadGrpId
    LEFT OUTER JOIN dbo.adCitizenships C ON S.Citizen = C.CitizenshipId
    WHERE   SE.CampusId = @CampusId
            AND P.Is1098T = 1
            --AND S.StudentStatus = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
            AND (
                  LG.Descrip LIKE 'Int%' -- International Lead Group
                  OR C.IPEDSValue = 65
                )  --Non-Citizen
    ORDER BY S.LastName;
GO
------------------------------------------------------------------------------------------------------
--End Balaji US9611:1098T Exceptions Report
------------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------------
--Troy: Related to US9775. The token should be 36 characters. Initial Script provided used 38.
------------------------------------------------------------------------------------------------------
IF EXISTS ( SELECT  *
            FROM    sys.columns
            WHERE   name = N'Token1098TService'
                    AND object_id = OBJECT_ID(N'syCampuses') )
    AND (
          SELECT    COL_LENGTH('syCampuses','Token1098TService')
        ) <> 36
    BEGIN		
        ALTER TABLE dbo.syCampuses ALTER COLUMN Token1098TService VARCHAR(36);
    END;
GO

------------------------------------------------------------------------------------------------------
--Troy: Related to US9775. The Kiss Code should be 18 characters. Initial Script provided used 20.
------------------------------------------------------------------------------------------------------
IF EXISTS ( SELECT  *
            FROM    sys.columns
            WHERE   name = N'SchoolCodeKissSchoolId'
                    AND object_id = OBJECT_ID(N'syCampuses') )
    AND (
          SELECT    COL_LENGTH('syCampuses','SchoolCodeKissSchoolId')
        ) <> 18
    BEGIN
        ALTER TABLE dbo.syCampuses ALTER COLUMN SchoolCodeKissSchoolId VARCHAR(18);
    END;
GO

----------------------------------------------------------------------------------------------------------
--Troy: Related to US9775. Updaste metadata. The token should be 36 characters. Initial Script provided used 38. 
-----------------------------------------------------------------------------------------------------------
DECLARE @fldid INT;

IF EXISTS ( SELECT  *
            FROM    syFields
            WHERE   FldName = 'Token1098TService'
                    AND FldLen <> 36 )
    BEGIN
        SET @fldid = (
                       SELECT   FldId
                       FROM     dbo.syFields
                       WHERE    FldName = 'Token1098TService'
                     );

        UPDATE  dbo.syFields
        SET     FldLen = 36
        WHERE   FldId = @fldid;

    END;
GO

------------------------------------------------------------------------------------------------------------
--Troy: Related to US9775. Update metadata. The Kiss Code should be 18 characters. Initial Script provided used 20.
-------------------------------------------------------------------------------------------------------------
DECLARE @fldid INT;

IF EXISTS ( SELECT  *
            FROM    syFields
            WHERE   FldName = 'SchoolCodeKissSchoolId'
                    AND FldLen <> 18 )
    BEGIN
        SET @fldid = (
                       SELECT   FldId
                       FROM     dbo.syFields
                       WHERE    FldName = 'SchoolCodeKissSchoolId'
                     );

        UPDATE  dbo.syFields
        SET     FldLen = 18
        WHERE   FldId = @fldid;

    END;
GO
------------------------------------------------------------------------------------------------------------
--Balaji DE13165
-------------------------------------------------------------------------------------------------------------
UPDATE  syMenuItems
SET     MenuName = '1098T Processing'
       ,DisplayName = '1098T Processing'
WHERE   MenuName = '1098-T Processing';
GO

-----------------------------------------------------------------------------------------------------------------------
--Troy:US9610, US9849:1098-T Box 1 Calculations with Clarification from the Internal Revenue Service
--DE 13327 Bristol
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
--Troy: Take account of charges posted as adjustments (TransTypeId=1)
-----------------------------------------------------------------------------------------------------------------------
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   type = 'P'
                    AND name = 'USP_SA_Get1098TDataForTaxYear' )
    DROP PROCEDURE USP_SA_Get1098TDataForTaxYear;
GO
SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

CREATE PROCEDURE dbo.USP_SA_Get1098TDataForTaxYear
    @TaxYear AS CHAR(4)
   ,@CampusId AS UNIQUEIDENTIFIER

	-------------------------------------------------------------------------------------------------
	--Testing Purposes Only
	--------------------------------------------------------------------------------------------------
	-- Version Date 1.27.2017 : DE13327
	--DECLARE @TaxYear AS CHAR(4)
	--declare @CampusId AS UNIQUEIDENTIFIER
  
	--SET @TaxYear='2016' 
	--SET @CampusId='3F5E839A-589A-4B2A-B258-35A1A8B3B819' 
	-----------------------------------------------------------------------------------------------
AS
    BEGIN
        DECLARE @strFirstQuarter AS VARCHAR(10) = '03/31/' + CONVERT(VARCHAR(10),CONVERT(INT,@TaxYear) + 1);
        DECLARE @strFirstCalendarDay AS VARCHAR(10) = '01/01/' + @TaxYear;
        DECLARE @intPrevYear AS INTEGER = CONVERT(INT,@TaxYear) - 1;
        DECLARE @intCurrentTaxYear AS INTEGER = CONVERT(INT,@TaxYear);        
        DECLARE @EndOfYear AS VARCHAR(10) = '12/31/' + @TaxYear;
        DECLARE @StartOfYear AS VARCHAR(10) = '1/1/' + @TaxYear;
	
        SELECT DISTINCT
                StuEnrollId
               ,t1.EnrollmentId
               ,CONVERT(CHAR(10),StartDate,101) AS StartDate
               ,(
                  SELECT    CONVERT(CHAR(10),MAX(LDA),101)
                  FROM      arStuEnrollments
                  WHERE     StuEnrollId = t1.StuEnrollId
                            AND LDA IS NOT NULL
                ) AS LDA
               ,(
                  SELECT    CASE WHEN COUNT(*) > 0 THEN 1
                                 ELSE 0
                            END
                  FROM      arStuEnrollments se
                           ,syStatusCodes sc
                  WHERE     se.StatusCodeId = sc.StatusCodeId
                            AND sc.SysStatusId = 8 -- no start status
                            AND se.StuEnrollId = t1.StuEnrollId
                ) AS IsStudentNoStart
               ,(
                  SELECT    CONVERT(CHAR(10),MAX(TransDate),101)
                  FROM      saTransactions
                  WHERE     StuEnrollId = t1.StuEnrollId
                            AND TransDate IS NOT NULL
                            AND Voided = 0
                            AND TransTypeId = 2
                ) AS TransDate
               ,t2.FirstName
               ,t2.LastName
               ,t2.SSN
               ,( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 ) THEN (
                                                                          SELECT    RTRIM(sa2.Address1 + ' ' + ISNULL(sa2.Address2,''))
                                                                          FROM      dbo.arStudAddresses sa2
                                                                          WHERE     sa2.StudentId = t2.StudentId
                                                                                    AND sa2.default1 = 1
                                                                        )
                       ELSE (
                              SELECT TOP 1
                                        RTRIM(sa2.Address1 + ' ' + ISNULL(sa2.Address2,''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS Address1
               ,( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 ) THEN (
                                                                          SELECT    RTRIM(ISNULL(sa2.City,''))
                                                                          FROM      dbo.arStudAddresses sa2
                                                                          WHERE     sa2.StudentId = t2.StudentId
                                                                                    AND sa2.default1 = 1
                                                                        )
                       ELSE (
                              SELECT TOP 1
                                        RTRIM(ISNULL(sa2.City,''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS City
               ,( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 ) THEN (
                                                                          SELECT    RTRIM(ISNULL(ss.StateCode,''))
                                                                          FROM      dbo.arStudAddresses sa2
                                                                                   ,syStates ss
                                                                          WHERE     sa2.StudentId = t2.StudentId
                                                                                    AND sa2.StateId = ss.StateId
                                                                                    AND sa2.default1 = 1
                                                                        )
                       ELSE (
                              SELECT TOP 1
                                        RTRIM(ISNULL(ss.StateCode,''))
                              FROM      dbo.arStudAddresses sa2
                                       ,syStates ss
                              WHERE     sa2.StudentId = t2.StudentId
                                        AND sa2.StateId = ss.StateId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS State
               ,( CASE WHEN EXISTS ( SELECT *
                                     FROM   dbo.arStudAddresses sa1
                                     WHERE  sa1.StudentId = t2.StudentId
                                            AND sa1.default1 = 1 ) THEN (
                                                                          SELECT    RTRIM(ISNULL(sa2.Zip,''))
                                                                          FROM      dbo.arStudAddresses sa2
                                                                          WHERE     sa2.StudentId = t2.StudentId
                                                                                    AND sa2.default1 = 1
                                                                        )
                       ELSE (
                              SELECT TOP 1
                                        RTRIM(ISNULL(sa2.Zip,''))
                              FROM      dbo.arStudAddresses sa2
                              WHERE     sa2.StudentId = t2.StudentId
                              ORDER BY  sa2.ModDate DESC
                            )
                  END ) AS Zip
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,dbo.saTransCodes tc
                  WHERE     SQ1.TransCodeId = tc.TransCodeId
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND (
                                  SQ1.TransTypeId = 0
                                  OR (
                                       SQ1.TransTypeId = 1
                                       AND tc.SysTransCodeId <> 16
                                     )
                                )
                            AND SQ1.Voided = 0
                            AND tc.Is1098T = 1
                ) AS TotalCost
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 2
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                ) AS TotalPaid
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 2
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) < CONVERT(INT,@TaxYear)
                ) AS PriorYearPayments
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,saRefunds SR
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransactionId = SR.TransactionId
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) < CONVERT(INT,@TaxYear)
                ) AS PriorYearRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.TransTypeId = 2
                            AND SQ1.Voided = 0
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                ) AS GrossPayments
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,saRefunds SQ2
                  WHERE     SQ1.TransactionId = SQ2.TransactionId
                            AND SQ1.Voided = 0
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                ) AS GrossRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions A1
                           ,saFundSources A5
                           ,saRefunds A6
                  WHERE     A1.TransactionId = A6.TransactionId
                            AND A5.FundSourceId = A6.FundSourceId
                            AND A1.Voided = 0
                            AND A1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                            AND A5.AwardTypeId = 1
                ) AS GrantRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions A1
                           ,saFundSources A5
                           ,saRefunds A6
                  WHERE     A1.TransactionId = A6.TransactionId
                            AND A5.FundSourceId = A6.FundSourceId
                            AND A1.Voided = 0
                            AND A1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                            AND A6.RefundTypeId = 1
                            AND A5.AwardTypeId = 2
                ) AS LoanRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,saRefunds SQ2
                  WHERE     SQ1.TransactionId = SQ2.TransactionId
                            AND SQ1.Voided = 0
                            AND SQ2.RefundTypeId = 0
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                ) AS StudentRefunds
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      (
                              SELECT    A1.TransDate
                                       ,A1.TransAmount
                                       ,A1.TransDescrip
                                       ,A1.FundSourceId
                              FROM      saTransactions A1
                                       ,saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                                        AND A5.AwardTypeId = 1
                            ) R
                ) AS GrantPayments
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      (
                              SELECT    A1.TransDate
                                       ,A1.TransAmount
                                       ,A1.TransDescrip
                                       ,A1.FundSourceId
                              FROM      saTransactions A1
                                       ,saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                                        AND A5.AwardTypeId = 2
                            ) R
                ) AS LoanPayments
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      (
                              SELECT    A1.TransDate
                                       ,A1.TransAmount
                                       ,A1.TransDescrip
                                       ,A1.FundSourceId
                              FROM      saTransactions A1
                                       ,saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                                        AND A5.AwardTypeId IN ( 3,4,5 )
                            ) R
                ) AS Scholarships
               ,(
                  SELECT DISTINCT
                            IsContinuingEd
                  FROM      arPrgVersions
                  WHERE     PrgVerId = t1.PrgVerId
                ) AS IsContinuingEd
               ,(
                  SELECT    ISNULL(SUM(TransAmount * -1),0)
                  FROM      (
                              SELECT    A1.TransDate
                                       ,A1.TransAmount
                                       ,A1.TransDescrip
                                       ,A1.FundSourceId
                              FROM      saTransactions A1
                                       ,saFundSources A5
                              WHERE     A1.FundSourceId = A5.FundSourceId
                                        AND A1.Voided = 0
                                        AND A1.StuEnrollId = t1.StuEnrollId
                                        AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                                        AND A5.AwardTypeId = 6
                            ) R
                ) AS StudentPayments
               ,(
                  SELECT    COUNT(*)
                  FROM      saTransactions SQ1
                  WHERE     SQ1.StuEnrollId = t1.StuEnrollId
                            AND SQ1.Voided = 0
                            AND TransTypeId = 0
                ) AS CostForEnrollmentExist
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,dbo.saTransCodes tc
                  WHERE     SQ1.TransCodeId = tc.TransCodeId
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND (
                                  SQ1.TransTypeId = 0
                                  OR (
                                       SQ1.TransTypeId = 1
                                       AND tc.SysTransCodeId <> 16
                                     )
                                )
                            AND SQ1.Voided = 0
                            AND tc.Is1098T = 1
                            AND YEAR(TransDate) = CONVERT(INT,@TaxYear)
                ) AS CurrentYrInstitutionalCharges
               ,(
                  SELECT    ISNULL(SUM(TransAmount),0)
                  FROM      saTransactions SQ1
                           ,dbo.saTransCodes tc
                  WHERE     SQ1.TransCodeId = tc.TransCodeId
                            AND SQ1.StuEnrollId = t1.StuEnrollId
                            AND (
                                  SQ1.TransTypeId = 0
                                  OR (
                                       SQ1.TransTypeId = 1
                                       AND tc.SysTransCodeId <> 16
                                     )
                                )
                            AND SQ1.Voided = 0
                            AND tc.Is1098T = 1
                            AND YEAR(TransDate) < CONVERT(INT,@TaxYear)
                ) AS PriorYrInstitutionalCharges
        FROM    arStuEnrollments t1
        INNER JOIN arStudent t2 ON t1.StudentId = t2.StudentId
        INNER JOIN syStatusCodes t5 ON t1.StatusCodeId = t5.StatusCodeId
        INNER JOIN arPrgVersions t7 ON t1.PrgVerId = t7.PrgVerId
        INNER JOIN arPrograms t6 ON t6.ProgId = t7.ProgId
        WHERE   t1.CampusId = @CampusId
                AND t6.Is1098T = '1'
                AND (
                      --This part mostly handles the attendance taking schools. The student must have started
            --before the end of the current year and must have attendance for the current year.
                      (
                        t1.StartDate <= @EndOfYear
                        AND t1.LDA >= @StartOfYear
                      )
                      OR
			--Student enrolled in the current year but starts in the first quarter of next year
                      (
                        t1.StartDate > @EndOfYear
                        AND t1.StartDate <= @strFirstQuarter
                        AND YEAR(t1.EnrollDate) = CONVERT(INT,@TaxYear)
                      )
                      OR
			--This part mostly handles schools that do not not take attendance
			--Students who are still in school - currently attending, LOA, Suspended, Academic Probation, Suspension
			--Note that we include future starts.                                
                      (
                        t1.StartDate <= @EndOfYear
                        AND t5.SysStatusId IN ( 7,9,10,11,20,21,22 )
                      )
                      OR
			--Include no starts who started during the year. They will be added to the exclusions table.
                      (
                        t1.StartDate BETWEEN @StartOfYear AND @EndOfYear
                        AND t5.SysStatusId = 8
                      )
                      OR
			--Students who graduated during the current year and the school
                      (
                        t1.StartDate <= @EndOfYear
                        AND t5.SysStatusId = 14
                        AND t1.ExpGradDate > @StartOfYear
                      )
                    )
            --The student must have a record in the student addresses table
                AND EXISTS ( SELECT *
                             FROM   dbo.arStudAddresses sa
                             WHERE  sa.StudentId = t2.StudentId )
			--Only return students with a SSN
                AND (
                      t2.SSN IS NOT NULL
                      AND t2.SSN <> ''
                      AND t2.SSN <> '000000000'
                    )
        ORDER BY LastName;

    END;
GO
-----------------------------------------------------------------------------------------------------------------------
--End Troy: Take account of charges posted as adjustments (TransTypeId=1)
-----------------------------------------------------------------------------------------------------------------------
-- ********************************************************************************************************
-- Consolidated Script Version 3.7SP10
-- Schema Changes Zone
-- Please deploy your schema changes in this area
-- (Format the code with TSQl Format before insert here please)
-- ********************************************************************************************************
-------------------------------------------------------------------------------------------------------------------------------
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   type = 'P'
                    AND name = 'usp_IPEDS_Spring_MilitaryBenefits_DetailAndSummary' )
    BEGIN
        DROP PROCEDURE usp_IPEDS_Spring_MilitaryBenefits_DetailAndSummary;
    END;
GO
CREATE PROCEDURE dbo.usp_IPEDS_Spring_MilitaryBenefits_DetailAndSummary
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@CohortYear VARCHAR(10) = NULL
   ,@OrderBy VARCHAR(100)
   ,@StartDate DATETIME
   ,@EndDate DATETIME
   ,@SchoolType VARCHAR(50)
   ,@EndDate_Academic DATETIME = NULL
AS
    DECLARE @ReturnValue VARCHAR(100)
       ,@Value VARCHAR(100);
    DECLARE @SSN VARCHAR(10)
       ,@FirstName VARCHAR(100)
       ,@LastName VARCHAR(100)
       ,@StudentNumber VARCHAR(50)
       ,@TransferredOut INT
       ,@Exclusions INT;
    DECLARE @CitizenShip_IPEDSValue INT
       ,@ProgramType_IPEDSValue INT
       ,@Gender_IPEDSValue INT
       ,@FullTimePartTime_IPEDSValue INT
       ,@StudentId UNIQUEIDENTIFIER; 
    DECLARE @AcademicEndDate DATETIME
       ,@AcademicStartDate DATETIME; 
    DECLARE @AcadInstFirstTimeStartDate DATETIME;
-- Check if School tracks grades by letter or numeric 
    SET @Value = (
                   SELECT TOP 1
                            Value
                   FROM     dbo.syConfigAppSetValues
                   WHERE    SettingId = 47
                 );

--Here, we're determining if the school type is academic. If so, we will change the new var
--@AcademicEndDate to the @EndDate_Academic, if not, than use @EndDate. We will use the new
--@AcademicEndDate variable to help select the student list.  DD Rev 01/10/12

    DECLARE @DeptOfDefenseStartDate VARCHAR(50)
       ,@DeptofDefenseEndDate VARCHAR(50);
	
    IF LOWER(@SchoolType) = 'academic'
        BEGIN
            SET @AcademicEndDate = @EndDate_Academic;
            SET @AcademicStartDate = @EndDate_Academic;
			 --12/05/2012 DE8824 - Academic year options should have a cutoff start date
            SET @AcadInstFirstTimeStartDate = DATEADD(YEAR,-1,@EndDate_Academic);

            SET @DeptOfDefenseStartDate = '10/1/' + CONVERT(VARCHAR,YEAR(@AcademicStartDate));
            SET @DeptofDefenseEndDate = '9/30/' + CONVERT(VARCHAR,YEAR(@AcademicEndDate));

        END;
    ELSE
        BEGIN
            SET @AcademicEndDate = @EndDate;
            SET @AcademicStartDate = @StartDate;
            SET @DeptOfDefenseStartDate = '10/1/' + CONVERT(VARCHAR,YEAR(@AcademicStartDate));
            SET @DeptofDefenseEndDate = '9/30/' + CONVERT(VARCHAR,YEAR(@AcademicEndDate));
        END;


    IF @ProgId IS NOT NULL
        BEGIN
	
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
	
        END;
    ELSE
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );

            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
 
/*********Get the list of students that will be shown in the report - Starts Here ******************/			
    CREATE TABLE #StudentsList
        (
         StudentId UNIQUEIDENTIFIER
        ,SSN VARCHAR(10)
        ,FirstName VARCHAR(100)
        ,LastName VARCHAR(100)
        ,MiddleName VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,TransferredOut INT
        ,Exclusions INT
        ,CitizenShip_IPEDSValue INT
        ,ProgramType_IPEDSValue INT
        ,Gender_IPEDSValue INT
        ,FullTimePartTime_IPEDSValue INT
        ,GenderId UNIQUEIDENTIFIER
        ,RaceId UNIQUEIDENTIFIER
        ,CitizenId UNIQUEIDENTIFIER
        ,GenderDescription VARCHAR(50)
        ,RaceDescription VARCHAR(50)
        ,StudentGraduatedStatusCount INT
        ,ClockHourProgramCount INT
        );
		
		
		
    IF LOWER(@SchoolType) = 'program'
        BEGIN
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StudentId = t1.StudentId
                                        AND SQ3.SysStatusId = 19
                                        AND SQ1.TransferDate <= @AcademicEndDate
                            ) AS TransferredOut
                           ,(
						-- Check if the student was either dropped and if the drop reason is either
						-- deceased, active duty, foreign aid service, church mission
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StudentId = t1.StudentId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped
                                                    AND SQ1.DateDetermined <= @AcademicEndDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,t1.Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t4.IPEDSValue
                            ) AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StudentId = t1.StudentId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StudentId = t1.StudentId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61
                            AND -- Full Time
                            t12.IPEDSValue = 11
                            AND -- First Time
                            t9.IPEDSValue = 58
                            OR t9.IPEDSValue = 59 -- Under Graduate or Graduate
                            AND (
                                  t2.StartDate >= @AcademicStartDate
                                  AND t2.StartDate <= @AcademicEndDate
                                ) 
							--(t2.StartDate<=@AcademicEndDate) 
                            AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
							-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
							( SELECT    t1.StuEnrollId
                              FROM      arStuEnrollments t1
                                       ,syStatusCodes t2
                              WHERE     t1.StatusCodeId = t2.StatusCodeId
                                        AND StartDate <= @AcademicEndDate
                                        AND -- Student started before the end date range
                                        LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                        AND (
                                              @ProgId IS NULL
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            )
                                        AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
								-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                        AND (
                                              t1.DateDetermined < @AcademicStartDate
                                              OR ExpGradDate < @AcademicStartDate
                                              OR LDA < @AcademicStartDate
                                            ) ) 
						   -- If Student is enrolled in only one program version and if that program version 
						   -- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd );
        END;
    ELSE
        BEGIN
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StudentId = t1.StudentId
                                        AND SQ3.SysStatusId = 19
                                        AND SQ1.TransferDate <= @AcademicEndDate
                            ) AS TransferredOut
                           ,(
						-- Check if the student was either dropped and if the drop reason is either
						-- deceased, active duty, foreign aid service, church mission
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StudentId = t1.StudentId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped
                                                    AND SQ1.DateDetermined <= @AcademicEndDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,t1.Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t4.IPEDSValue
                            ) AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StudentId = t1.StudentId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StudentId = t1.StudentId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.DegCertSeekingId = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61
                            AND -- Full Time
                            t12.IPEDSValue = 11
                            AND -- First Time
                            t9.IPEDSValue = 58
                            OR t9.IPEDSValue = 59 -- Under Graduate or Graduate
                            AND 
							--12/05/2012 DE8824 - Academic year options should have a cutoff start date
							--t2.StartDate <= @AcademicEndDate
                            (
                              t2.StartDate > @AcadInstFirstTimeStartDate
                              AND t2.StartDate <= @AcademicEndDate
                            )
                            AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
							-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
							( SELECT    t1.StuEnrollId
                              FROM      arStuEnrollments t1
                                       ,syStatusCodes t2
                              WHERE     t1.StatusCodeId = t2.StatusCodeId
                                        AND StartDate <= @AcademicEndDate
                                        AND -- Student started before the end date range
                                        LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                        AND (
                                              @ProgId IS NULL
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            )
                                        AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
								-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                        AND (
                                              t1.DateDetermined < @AcademicStartDate
                                              OR ExpGradDate < @AcademicStartDate
                                              OR LDA < @AcademicStartDate
                                            ) ) 
						   -- If Student is enrolled in only one program version and if that program version 
						   -- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd );
        END;	


--SELECT * FROM #StudentsList		
		
    CREATE TABLE #StudentsWhoReceivedPost911AndDefenseTuition
        (
         StudentId UNIQUEIDENTIFIER
        ,StudentName VARCHAR(500)
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,PmtDisbRelId UNIQUEIDENTIFIER
        ,Amount DECIMAL(18,2)
        ,AdvFundSourceId INT
        ,IPEDSValue INT
        ,TransDate DATETIME
        );
		
    INSERT  INTO #StudentsWhoReceivedPost911AndDefenseTuition
            SELECT DISTINCT
                    S.StudentId
                   ,S.LastName + ', ' + S.FirstName + ' ' + ISNULL(MiddleName,'') AS StudentName
                   ,dbo.UDF_FormatSSN(S.SSN) AS SSN
                   ,S.StudentNumber
                   ,PDR.PmtDisbRelId
                   ,PDR.Amount
                   ,FS.AdvFundSourceId
                   ,PT.IPEDSValue
                   ,ST.TransDate
            FROM    saPmtDisbRel PDR
            INNER JOIN faStudentAwardSchedule SAS ON PDR.AwardScheduleId = SAS.AwardScheduleId
            INNER JOIN faStudentAwards SA ON SAS.StudentAwardId = SAS.StudentAwardId
            INNER JOIN saTransactions ST ON ST.TransactionId = PDR.TransactionId
            INNER JOIN arStuEnrollments SE ON SE.StuEnrollId = SA.StuEnrollId
                                              AND SE.StuEnrollId = ST.StuEnrollId
            INNER JOIN #StudentsList S ON SE.StudentId = S.StudentId
            INNER JOIN saFundSources FS ON SA.AwardTypeId = FS.FundSourceId
                                           AND FS.FundSourceId = ST.FundSourceId
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arProgTypes PT ON PV.ProgTypId = PT.ProgTypId
            INNER JOIN arPrograms t8 ON PV.ProgId = t8.ProgId
            WHERE   FS.AdvFundSourceId IN ( 24,25 ) -- Post 9/11, Dept of Defense
                    AND PT.IPEDSValue IN ( 58,59 ) -- Undergraduate and Graduate Students
                    AND SE.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
							-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
							( SELECT    t1.StuEnrollId
                              FROM      arStuEnrollments t1
                                       ,syStatusCodes t2
                              WHERE     t1.StatusCodeId = t2.StatusCodeId
                                        AND StartDate <= @AcademicEndDate
                                        AND -- Student started before the end date range
                                        LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                        AND (
                                              @ProgId IS NULL
                                              OR t8.ProgId IN ( SELECT  Val
                                                                FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                            )
                                        AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
								-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                        AND (
                                              t1.DateDetermined < @AcademicStartDate
                                              OR ExpGradDate < @AcademicStartDate
                                              OR LDA < @AcademicStartDate
                                            ) ); 
	 



    CREATE TABLE #FinalOutput
        (
         StudentName VARCHAR(200)
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,Post911_Undergraduate_DisbCount INT
        ,Post911_Undergraduate_DisbAmount DECIMAL(18,2)
        ,Post911_Graduate_DisbCount INT
        ,Post911_Graduate_DisbAmount INT
        ,DeptofDefense_Undergraduate_DisbCount INT
        ,DeptofDefense_Undergraduate_DisbAmount DECIMAL(18,2)
        ,DeptofDefense_Graduate_DisbCount INT
        ,DeptofDefense_Graduate_DisbAmount DECIMAL(18,2)
        );

    INSERT  INTO #FinalOutput
            SELECT 
	DISTINCT        StudentName
                   ,SSN
                   ,StudentNumber
                   ,(
                      SELECT    CASE WHEN COUNT(*) >= 1 THEN 1
                                     ELSE 0
                                END
                      FROM      #StudentsWhoReceivedPost911AndDefenseTuition SQ1
                      WHERE     SQ1.StudentId = PT.StudentId
                                AND AdvFundSourceId = 24 --Post 9/11
                                AND IPEDSValue = 58 -- Undergraduate
                                AND TransDate >= @StartDate
                                AND TransDate <= @EndDate
                    ) AS Post911_Undergraduate_DisbCount
                   ,(
                      SELECT    SUM(Amount)
                      FROM      #StudentsWhoReceivedPost911AndDefenseTuition SQ1
                      WHERE     SQ1.StudentId = PT.StudentId
                                AND AdvFundSourceId = 24 --Post 9/11
                                AND IPEDSValue = 58 -- Undergraduate
                                AND TransDate >= @StartDate
                                AND TransDate <= @EndDate
                    ) AS Post911_Undergraduate_DisbAmount
                   ,(
                      SELECT    CASE WHEN COUNT(*) >= 1 THEN 1
                                     ELSE 0
                                END
                      FROM      #StudentsWhoReceivedPost911AndDefenseTuition SQ1
                      WHERE     SQ1.StudentId = PT.StudentId
                                AND AdvFundSourceId = 24 --Post 9/11
                                AND IPEDSValue = 59 -- Graduate
                                AND TransDate >= @StartDate
                                AND TransDate <= @EndDate
                    ) AS Post911_Graduate_DisbCount
                   ,(
                      SELECT    SUM(Amount)
                      FROM      #StudentsWhoReceivedPost911AndDefenseTuition SQ1
                      WHERE     SQ1.StudentId = PT.StudentId
                                AND AdvFundSourceId = 24 --Post 9/11
                                AND IPEDSValue = 59 -- Graduate
                                AND TransDate >= @StartDate
                                AND TransDate <= @EndDate
                    ) AS Post911_Graduate_DisbAmount
                   ,(
                      SELECT    CASE WHEN COUNT(*) >= 1 THEN 1
                                     ELSE 0
                                END
                      FROM      #StudentsWhoReceivedPost911AndDefenseTuition SQ1
                      WHERE     SQ1.StudentId = PT.StudentId
                                AND AdvFundSourceId = 25 --Dept. of Defense Tuition
                                AND IPEDSValue = 58 -- Undergraduate
                                AND TransDate >= @DeptOfDefenseStartDate
                                AND TransDate <= @DeptofDefenseEndDate
                    ) AS DeptofDefense_Undergraduate_DisbCount
                   ,(
                      SELECT    SUM(Amount)
                      FROM      #StudentsWhoReceivedPost911AndDefenseTuition SQ1
                      WHERE     SQ1.StudentId = PT.StudentId
                                AND AdvFundSourceId = 25 --Dept. of Defense Tuition
                                AND IPEDSValue = 58 -- Undergraduate
                                AND TransDate >= @DeptOfDefenseStartDate
                                AND TransDate <= @DeptofDefenseEndDate
                    ) AS DeptofDefense_Undergraduate_DisbAmount
                   ,(
                      SELECT    CASE WHEN COUNT(*) >= 1 THEN 1
                                     ELSE 0
                                END
                      FROM      #StudentsWhoReceivedPost911AndDefenseTuition SQ1
                      WHERE     SQ1.StudentId = PT.StudentId
                                AND AdvFundSourceId = 25 --Dept. of Defense Tuition
                                AND IPEDSValue = 59 -- Graduate
                                AND TransDate >= @DeptOfDefenseStartDate
                                AND TransDate <= @DeptofDefenseEndDate
                    ) AS DeptofDefense_Graduate_DisbCount
                   ,(
                      SELECT    SUM(Amount)
                      FROM      #StudentsWhoReceivedPost911AndDefenseTuition SQ1
                      WHERE     SQ1.StudentId = PT.StudentId
                                AND AdvFundSourceId = 25 --Dept. of Defense Tuition
                                AND IPEDSValue = 59 -- Graduate
                                AND TransDate >= @DeptOfDefenseStartDate
                                AND TransDate <= @DeptofDefenseEndDate
                    ) AS DeptofDefense_Graduate_DisbAmount
            FROM    #StudentsWhoReceivedPost911AndDefenseTuition PT;

    SELECT  *
    FROM    #FinalOutput
    ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
             END
           ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
            END; 
    DROP TABLE #StudentsWhoReceivedPost911AndDefenseTuition;	
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   type = 'P'
                    AND name = 'USP_OutcomeMeasures_Detail' )
    BEGIN
        DROP PROCEDURE USP_OutcomeMeasures_Detail;
    END;
GO
CREATE PROCEDURE dbo.USP_OutcomeMeasures_Detail
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
   ,@FullOrPartTime INT
   ,@FirstTimeOrNot INT
AS
    DECLARE @ReturnValue VARCHAR(100);
    DECLARE @ContinuingStartDate DATETIME;

    SET @ContinuingStartDate = @StartDate;
	-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
	-- and Start Date should be blank
    IF DAY(@EndDate) = 15
        AND MONTH(@EndDate) = 10
        BEGIN
            SET @EndDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
            SET @StartDate = @StartDate;
        END;

    IF @ProgId IS NOT NULL
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
    ELSE
        BEGIN
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.CampGrpId IN ( SELECT    t1.CampGrpId
                                      FROM      syCmpGrpCmps
                                      WHERE     CampusId = @CampusId );
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);
        END;
	
	
	-----------------------------------------------------------------
	---John ginzo - start - get list of enrollments
	-----------------------------------------------------------------
    DECLARE @enrollments TABLE
        (
         StuEnrollId UNIQUEIDENTIFIER
        );
    DECLARE @InSchoolEnrolmentOutput TABLE
        (
         StuEnrollId UNIQUEIDENTIFIER
        ,DateOfChange DATETIME
        ,WithinPeriod BIT
        ,BeforePeriod BIT
        );
    DECLARE @StuEnrollId UNIQUEIDENTIFIER;

    INSERT  INTO @enrollments
            SELECT  StuEnrollId
            FROM    dbo.arStuEnrollments e
            INNER JOIN dbo.syStatusCodes st ON e.StatusCodeId = st.StatusCodeId
            INNER JOIN dbo.sySysStatus ss ON ss.SysStatusId = ss.SysStatusId
            WHERE   ss.InSchool = 0;
			

	--SELECT * FROM @enrollments

    DECLARE @enrollmentsbeforestart TABLE
        (
         StuEnrollId UNIQUEIDENTIFIER
        );

    INSERT  INTO @InSchoolEnrolmentOutput
            SELECT DISTINCT
                    ( e.StuEnrollId )
                   ,e2.inPeriodDateOfChange
                   ,1
                   ,0
            FROM    @enrollments e
            INNER JOIN (
                         SELECT StuEnrollId
                               ,MIN(DateOfChange) AS inPeriodDateOfChange
                         FROM   dbo.syStudentStatusChanges e
                         INNER JOIN dbo.syStatusCodes st ON e.NewStatusId = st.StatusCodeId
                         INNER JOIN dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
                         WHERE  ss.InSchool = 1
                                AND DateOfChange BETWEEN @StartDate AND @EndDate
                         GROUP BY StuEnrollId
                       ) e2 ON e.StuEnrollId = e2.StuEnrollId;        


	--SELECT * FROM @InSchoolEnrolmentOutput


    DECLARE ENROLLMENT_CHANGES CURSOR
    FOR
        SELECT  StuEnrollId
        FROM    @enrollments;

    OPEN ENROLLMENT_CHANGES;

    FETCH NEXT FROM ENROLLMENT_CHANGES INTO @StuEnrollId;

    WHILE @@FETCH_STATUS = 0
        BEGIN	
	   
            DECLARE @inSchoolDateofChange DATETIME;
            SET @inSchoolDateofChange = (
                                          SELECT    MAX(DateOfChange)
                                          FROM      dbo.syStudentStatusChanges e
                                          INNER JOIN dbo.syStatusCodes st ON e.NewStatusId = st.StatusCodeId
                                          INNER JOIN dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
                                          WHERE     ss.InSchool = 1
                                                    AND DateOfChange < @StartDate
                                                    AND StuEnrollId = @StuEnrollId
                                        );

            IF @inSchoolDateofChange IS NOT NULL
                BEGIN		
                    DECLARE @OutOFSchoolChange DATETIME;

                    IF EXISTS ( SELECT  DateOfChange
                                FROM    dbo.syStudentStatusChanges e
                                INNER JOIN dbo.syStatusCodes st ON e.NewStatusId = st.StatusCodeId
                                INNER JOIN dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
                                WHERE   ss.InSchool = 0
                                        AND DateOfChange >= @inSchoolDateofChange
                                        AND DateOfChange > @StartDate
                                        AND e.StuEnrollId = @StuEnrollId )
                        AND NOT EXISTS ( SELECT *
                                         FROM   @InSchoolEnrolmentOutput
                                         WHERE  StuEnrollId = @StuEnrollId )
                        BEGIN
                            INSERT  INTO @InSchoolEnrolmentOutput
                                    (
                                     StuEnrollId
                                    ,DateOfChange
                                    ,WithinPeriod
                                    ,BeforePeriod
                                    )
                            VALUES  (
                                     @StuEnrollId
                                    ,@inSchoolDateofChange
                                    ,0
                                    ,1
                                    );
                        END;
                END;

            FETCH NEXT FROM ENROLLMENT_CHANGES INTO @StuEnrollId;

        END;


    CLOSE ENROLLMENT_CHANGES;
    DEALLOCATE ENROLLMENT_CHANGES;

	-----------------------------------------------------------------
	--John ginzo - end 
	-----------------------------------------------------------------

    DECLARE @StatusAsOf2013 DATETIME
       ,@StatusAsOf2015 DATETIME;
    SET @StatusAsOf2013 = '08/31/' + CONVERT(CHAR(4),YEAR(GETDATE()) - 2); 
    SET @StatusAsOf2015 = '08/31/' + CONVERT(CHAR(4),YEAR(GETDATE()));


    DECLARE @ExclusionsAsOf2013 DATETIME
       ,@ExclusionsAsOf2015 DATETIME;
    SET @ExclusionsAsOf2013 = '09/01/' + CONVERT(CHAR(4),YEAR(GETDATE()) - 3); --2017 is Current Year and 3 Yr Prev is 2014
    SET @ExclusionsAsOf2015 = '08/31/' + CONVERT(CHAR(4),YEAR(GETDATE()) - 1); --2017 is Current Year and 1 Yr Prev is 2016


    CREATE TABLE #GraduatedStudentsAsOfAug312013
        (
         StudentId UNIQUEIDENTIFIER
        );

    CREATE TABLE #GraduatedStudentsAsOfAug312015
        (
         StudentId UNIQUEIDENTIFIER
        );

    CREATE TABLE #OutofSchoolStudentsAsOfAug312013
        (
         StuEnrollId UNIQUEIDENTIFIER
        );

    CREATE TABLE #OutofSchoolStudentsAsOfAug312015
        (
         StuEnrollId UNIQUEIDENTIFIER
        );

    INSERT  INTO #OutofSchoolStudentsAsOfAug312013
            SELECT  DISTINCT
                    e.StuEnrollId
            FROM    dbo.syStudentStatusChanges e
            INNER JOIN dbo.syStatusCodes st ON e.NewStatusId = st.StatusCodeId
            INNER JOIN dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
            WHERE   ss.InSchool = 0
                    AND DateOfChange < @StatusAsOf2013
                    AND e.DateOfChange = (
                                           SELECT   MAX(DateOfChange)
                                           FROM     dbo.syStudentStatusChanges
                                           WHERE    StuEnrollId = e.StuEnrollId
                                         );
			
                                        
    INSERT  INTO #OutofSchoolStudentsAsOfAug312015
            SELECT  DISTINCT
                    e.StuEnrollId
            FROM    dbo.syStudentStatusChanges e
            INNER JOIN dbo.syStatusCodes st ON e.NewStatusId = st.StatusCodeId
            INNER JOIN dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
            WHERE   ss.InSchool = 0
                    AND DateOfChange < @StatusAsOf2015
                    AND e.DateOfChange = (
                                           SELECT   MAX(DateOfChange)
                                           FROM     dbo.syStudentStatusChanges
                                           WHERE    StuEnrollId = e.StuEnrollId
                                         );
										--ORDER BY DateOfChange Desc

    INSERT  INTO #GraduatedStudentsAsOfAug312013
            SELECT  DISTINCT
                    S.StudentId
            FROM    dbo.syStudentStatusChanges e
            INNER JOIN dbo.syStatusCodes st ON e.NewStatusId = st.StatusCodeId
            INNER JOIN dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
            INNER JOIN arStuEnrollments SE ON SE.StuEnrollId = e.StuEnrollId
            INNER JOIN arStudent S ON S.StudentId = SE.StudentId
            WHERE   ss.InSchool = 0
                    AND DateOfChange < @StatusAsOf2013
                    AND e.DateOfChange = (
                                           SELECT   MAX(DateOfChange)
                                           FROM     dbo.syStudentStatusChanges
                                           WHERE    StuEnrollId = e.StuEnrollId
                                         )
                    AND ss.SysStatusId = 14; 

    INSERT  INTO #GraduatedStudentsAsOfAug312015
            SELECT  DISTINCT
                    S.StudentId
            FROM    dbo.syStudentStatusChanges e
            INNER JOIN dbo.syStatusCodes st ON e.NewStatusId = st.StatusCodeId
            INNER JOIN dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
            INNER JOIN arStuEnrollments SE ON SE.StuEnrollId = e.StuEnrollId
            INNER JOIN arStudent S ON S.StudentId = SE.StudentId
            WHERE   ss.InSchool = 0
                    AND DateOfChange < @StatusAsOf2015
                    AND e.DateOfChange = (
                                           SELECT   MAX(DateOfChange)
                                           FROM     dbo.syStudentStatusChanges
                                           WHERE    StuEnrollId = e.StuEnrollId
                                         )
                    AND ss.SysStatusId = 14;

    CREATE TABLE #EnrollmentDetail
        (
         RowNumber UNIQUEIDENTIFIER
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,StudentId UNIQUEIDENTIFIER
        ,EnrollmentID VARCHAR(50)
        ,Exclusions2013 INT
        ,Exclusions2015 INT
        ,RecAdAward2013 INT
        ,RecAdAward2015 INT
        ,RevisedCohort2015 INT
        ,RevisedCohort2013 INT
        );

    CREATE TABLE #EnrollmentDetailOutput
        (
         RowNumber UNIQUEIDENTIFIER
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,StudentId UNIQUEIDENTIFIER
        ,EnrollmentID VARCHAR(50)
        ,Exclusions2013 INT
        ,Exclusions2015 INT
        ,RecAdAward2013 INT
        ,RecAdAward2015 INT
        ,RevisedCohort2015 INT
        ,RevisedCohort2013 INT
        ,Exclusions2013_Total INT
        ,Exclusions2015_Total INT
        ,RecAdAward2013_Total INT
        ,RecAdAward2015_Total INT
        ,RevisedCohort2015_Total INT
        ,RevisedCohort2013_Total INT
        ,StillEnrolled_Total INT
        );
   

    INSERT  INTO #EnrollmentDetail
            SELECT  NEWID() AS RowNumber
                   ,dbo.UDF_FormatSSN(SSN) AS SSN
                   ,StudentNumber
                   ,StudentName
                   ,StudentId
                   ,EnrollmentID
                   ,( CASE WHEN (
                                  SELECT    COUNT(*)
                                  FROM      arStuEnrollments SQ1
                                           ,syStatusCodes SQ2
                                           ,dbo.sySysStatus SQ3
                                           ,arDropReasons SQ44
                                  WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                            AND SQ2.SysStatusId = SQ3.SysStatusId
                                            AND SQ1.DropReasonId = SQ44.DropReasonId
                                            AND SQ1.StuEnrollId = R1.StuEnrollId
                                            AND SQ3.SysStatusId IN ( 12 ) -- Dropped 
                                            AND SQ1.DateDetermined <= @StatusAsOf2013
                                            AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                ) >= 1 THEN 1
                           ELSE 0
                      END ) AS ExclusionsAsOf2013
                   ,( CASE WHEN (
                                  SELECT    COUNT(*)
                                  FROM      arStuEnrollments SQ1
                                           ,syStatusCodes SQ2
                                           ,dbo.sySysStatus SQ3
                                           ,arDropReasons SQ44
                                  WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                            AND SQ2.SysStatusId = SQ3.SysStatusId
                                            AND SQ1.DropReasonId = SQ44.DropReasonId
                                            AND SQ1.StuEnrollId = R1.StuEnrollId
                                            AND SQ3.SysStatusId IN ( 12 ) -- Dropped  
                                            --AND SQ1.DateDetermined <= @StatusAsOf2015
                                            AND (
                                                  SQ1.DateDetermined >= @ExclusionsAsOf2013
                                                  AND SQ1.DateDetermined <= @ExclusionsAsOf2015
                                                )
                                            AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                ) >= 1 THEN 1
                           ELSE 0
                      END ) AS ExclusionsAsOf2015
                   ,(
                      SELECT    COUNT(*)
                      FROM      #GraduatedStudentsAsOfAug312013 G2013
                      WHERE     G2013.StudentId = R1.StudentId
                    ) AS RecAdAward2013
                   ,(
                      SELECT    COUNT(*)
                      FROM      #GraduatedStudentsAsOfAug312015 G2015
                      WHERE     G2015.StudentId = R1.StudentId
                    ) AS RecAdAward2015
                   ,(
                      SELECT    COUNT(*)
                      FROM      arStuEnrollments
                      WHERE     StuEnrollId = R1.StuEnrollId
                                AND StuEnrollId NOT IN ( SELECT t1.StuEnrollId
                                                         FROM   arStuEnrollments t1
                                                               ,syStatusCodes t2
                                                         WHERE  t1.StatusCodeId = t2.StatusCodeId
                                                                AND StartDate <= @StatusAsOf2015
                                                                AND -- Student started before the end date range
                                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND t1.StuEnrollId IN ( SELECT  StuEnrollId
                                                                                        FROM    #OutofSchoolStudentsAsOfAug312015 ) )
                    ) AS RevisedCohort2015
                   ,1 AS RevisedCohort2013
            FROM    (
                      -- Check if there are any Foreign Students who are Full-Time Undergraduates
			-- If there are no foreign students in that category, insert a blank record
			-- with race 'Nonresident Alien'
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,t1.StudentId
                               ,t2.EnrollmentId
                               ,t2.StuEnrollId
                      FROM      arStudent t1
                      INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                      INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                      INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                   AND t6.SysStatusId NOT IN ( 8 )
                      INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                      INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                      INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                      INNER JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                      INNER JOIN adDegCertSeeking t16 ON t2.DegCertSeekingId = t16.DegCertSeekingId
                      WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                AND (
                                      @ProgId IS NULL
                                      OR t8.ProgId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                    )
                                AND ( t9.IPEDSValue = 58 ) -- Undergraduate
                                AND ( t10.IPEDSValue = @FullOrPartTime ) -- Full Time Or Part Time
                                AND t2.IsFirstTimeInSchool = @FirstTimeOrNot -- First Time or Non-First Time in School
								-- IF A Then B Else C is equivalent to ((Not A) OR B) AND (A OR C)
								--AND ((@CohortType<>'Full') OR (t2.StartDate>=@StartDate AND t2.StartDate<=@EndDate)) AND (@CohortType='Full' OR t2.StartDate <= @EndDate)
                                AND ( t2.StartDate <= @EndDate )
								-- Exclude students who are Dropped out/Transferred/Graduated/No Start 
								-- Who were Dropped or Graduated or LDA before report StartDate
                                AND StuEnrollId NOT IN ( SELECT t1.StuEnrollId
                                                         FROM   arStuEnrollments t1
                                                               ,syStatusCodes t2
                                                         WHERE  t1.StatusCodeId = t2.StatusCodeId
                                                                AND StartDate <= @EndDate
                                                                AND -- Student started before the end date range
                                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                                AND (
                                                                      t1.DateDetermined < @StartDate
                                                                      OR ExpGradDate < @StartDate
                                                                      OR LDA < @StartDate
                                                                    )
                                                                AND t1.StuEnrollId NOT IN ( SELECT  StuEnrollId
                                                                                            FROM    @InSchoolEnrolmentOutput ) )
					-- DE8492
					-- If Student is enrolled in only program version and if that program version 
					-- happens to be a continuing ed program exclude the student
                                AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                    StudentId
                                                          FROM      (
                                                                      SELECT    StudentId
                                                                               ,COUNT(*) AS RowCounter
                                                                      FROM      arStuEnrollments
                                                                      WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                              FROM      arPrgVersions
                                                                                              WHERE     IsContinuingEd = 1 )
                                                                      GROUP BY  StudentId
                                                                      HAVING    COUNT(*) = 1
                                                                    ) dtStudent_ContinuingEd )
                    ) R1;

    DECLARE @Studentidentifiervalue VARCHAR(50);
    SET @Studentidentifiervalue = (
                                    SELECT TOP 1
                                            Value
                                    FROM    dbo.syConfigAppSetValues
                                    WHERE   SettingId IN ( SELECT   SettingId
                                                           FROM     dbo.syConfigAppSettings
                                                           WHERE    KeyName = 'StudentIdentifier' )
                                  );
	
    IF LOWER(@Studentidentifiervalue) = 'ssn'
        OR LOWER(@Studentidentifiervalue) = 'studentnumber'
        BEGIN

            INSERT  INTO #EnrollmentDetailOutput
                    SELECT  NULL AS RowNumber
                           ,SSN
                           ,StudentNumber
                           ,StudentName
                           ,StudentId
                           ,NULL AS EnrollmetID
                           ,Exclusions2013
                           ,Exclusions2015
                           ,RecAdAward2013
                           ,RecAdAward2015
                           ,RevisedCohort2015
                           ,RevisedCohort2013
                           ,(
                              SELECT    COUNT(Exclusions2013)
                              FROM      #EnrollmentDetail SQ
                              WHERE     Exclusions2013 >= 1
                            ) AS Exclusions2013_Total
                           ,(
                              SELECT    COUNT(SQ.Exclusions2015)
                              FROM      #EnrollmentDetail SQ
                              WHERE     Exclusions2015 >= 1
                            ) AS Exclusions2015_Total
                           ,(
                              SELECT    COUNT(SQ.RecAdAward2013)
                              FROM      #EnrollmentDetail SQ
                              WHERE     RecAdAward2013 >= 1
                            ) AS RecAdAward2013_Total
                           ,(
                              SELECT    COUNT(SQ.RecAdAward2015)
                              FROM      #EnrollmentDetail SQ
                              WHERE     SQ.RecAdAward2015 >= 1
                            ) AS RecAdAward2015_Total
                           ,(
                              SELECT    COUNT(SQ.RevisedCohort2015)
                              FROM      #EnrollmentDetail SQ
                              WHERE     SQ.RevisedCohort2015 >= 1
                            ) AS RevisedCohort2015_Total
                           ,(
                              SELECT    COUNT(SQ.RevisedCohort2013)
                              FROM      #EnrollmentDetail SQ
                              WHERE     SQ.RevisedCohort2013 >= 1
                            ) AS RevisedCohort2013_Total
                           ,(
                              SELECT    COUNT(*)
                              FROM      #EnrollmentDetail SQ
                              WHERE     RecAdAward2015 = 0
                                        AND RevisedCohort2015 >= 1
                            ) AS StillEnrolled_Total
                    FROM    #EnrollmentDetail
                    ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
                             END
                           ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
                            END;

            IF LOWER(@OrderBy) = 'ssn'
                BEGIN
                    SELECT DISTINCT
                            *
                    FROM    #EnrollmentDetailOutput
                    ORDER BY SSN; 
                END;
            ELSE
                BEGIN
                    SELECT DISTINCT
                            *
                    FROM    #EnrollmentDetailOutput
                    ORDER BY StudentName; 
                END;
        END;
    ELSE
        BEGIN
            SELECT  RowNumber
                   ,SSN
                   ,StudentNumber
                   ,StudentName
                   ,StudentId
                   ,EnrollmentID
                   ,Exclusions2013
                   ,Exclusions2015
                   ,RecAdAward2013
                   ,RecAdAward2015
                   ,RevisedCohort2015
                   ,RevisedCohort2013
                   ,(
                      SELECT    COUNT(Exclusions2013)
                      FROM      #EnrollmentDetail SQ
                      WHERE     Exclusions2013 >= 1
                    ) AS Exclusions2013_Total
                   ,(
                      SELECT    COUNT(SQ.Exclusions2015)
                      FROM      #EnrollmentDetail SQ
                      WHERE     Exclusions2015 >= 1
                    ) AS Exclusions2015_Total
                   ,(
                      SELECT    COUNT(SQ.RecAdAward2013)
                      FROM      #EnrollmentDetail SQ
                      WHERE     RecAdAward2013 >= 1
                    ) AS RecAdAward2013_Total
                   ,(
                      SELECT    COUNT(SQ.RecAdAward2015)
                      FROM      #EnrollmentDetail SQ
                      WHERE     SQ.RecAdAward2015 >= 1
                    ) AS RecAdAward2015_Total
                   ,(
                      SELECT    COUNT(SQ.RevisedCohort2015)
                      FROM      #EnrollmentDetail SQ
                      WHERE     SQ.RevisedCohort2015 >= 1
                    ) AS RevisedCohort2015_Total
                   ,(
                      SELECT    COUNT(SQ.RevisedCohort2013)
                      FROM      #EnrollmentDetail SQ
                      WHERE     SQ.RevisedCohort2013 >= 1
                    ) AS RevisedCohort2013_Total
                   ,(
                      SELECT    COUNT(*)
                      FROM      #EnrollmentDetail SQ
                      WHERE     RecAdAward2015 = 0
                                AND RevisedCohort2015 >= 1
                    ) AS StillEnrolled_Total
            FROM    #EnrollmentDetail
            ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
                     END
                   ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
                    END;
		
        END;
GO

--------------------------------------------------------------------------
--Function to check if a program is an undergraduate program
--To be used for US7338: Maximum Time Frame
----------------------------------------------------------------------------
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'dbo.isProgramUndergraduate')
                    AND type IN ( N'FN',N'IF',N'TF',N'FS',N'FT' ) )
    DROP FUNCTION dbo.isProgramUndergraduate;
GO 

CREATE  FUNCTION dbo.isProgramUndergraduate ( @programId CHAR(36) )
RETURNS BIT
AS
    BEGIN
        DECLARE @returnValue BIT;
        DECLARE @ipedsvalue INT;
		
		
        SET @ipedsvalue = (
                            SELECT  IPEDSValue
                            FROM    arPrograms pg
                                   ,dbo.arProgCredential pc
                            WHERE   pg.CredentialLvlId = pc.CredentialId
                                    AND pg.ProgId = @programId
                          );
		
        IF (
             @ipedsvalue = 154
             OR @ipedsvalue = 155
             OR @ipedsvalue = 156
             OR @ipedsvalue = 157
           )
            BEGIN
                SET @returnValue = 1;
            END;
        ELSE
            BEGIN
                SET @returnValue = 0;
            END;	
        
        RETURN @returnValue;
    END;
GO

--------------------------------------------------------------------------
--Function to check if a program is a credit hour program
--To be used for US7338: Maximum Time Frame
----------------------------------------------------------------------------
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'dbo.isProgramCreditHour')
                    AND type IN ( N'FN',N'IF',N'TF',N'FS',N'FT' ) )
    DROP FUNCTION dbo.isProgramCreditHour;
GO 

CREATE  FUNCTION dbo.isProgramCreditHour ( @programId CHAR(36) )
RETURNS BIT
AS
    BEGIN
        DECLARE @returnValue BIT;
        DECLARE @acid INT;

        SET @acid = (
                      SELECT    ACId
                      FROM      arPrograms
                      WHERE     ProgId = @programId
                    );

        IF ( @acid <> 5 )
            BEGIN
                SET @returnValue = 1;
            END;	
        ELSE
            BEGIN
                SET @returnValue = 0;
            END;
        
        RETURN @returnValue;
    END;
GO


------------------------------------------------------------------------------------------------------------------------------------------
--US7338:Maximum Time Frame
------------------------------------------------------------------------------------------------------------------------------------------
/*
Definition of Maximum Time Frame:
---------------------------------
(1) For an undergraduate program measured in credit hours, a period that is no longer than 150 percent of the published length of the educational program, as measured in credit hours;

(2) For an undergraduate program measured in clock hours, a period that is no longer than 150 percent of the published length of the educational program, as measured by the cumulative number of clock hours the student is required to complete and expressed in calendar time; and

(3) For a graduate program, a period defined by the institution that is based on the length of the educational program.
*/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'dbo.GetMaximumTimeFrame')
                    AND type IN ( N'FN',N'IF',N'TF',N'FS',N'FT' ) )
    DROP FUNCTION dbo.GetMaximumTimeFrame;
GO
CREATE  FUNCTION dbo.GetMaximumTimeFrame ( @StuEnrollId CHAR(36) )
RETURNS VARCHAR(100)
--RETURNS DECIMAL(18,2)
AS
    BEGIN
        DECLARE @mtf DECIMAL(18,2);
        DECLARE @progid UNIQUEIDENTIFIER;
        DECLARE @undergradprogram BIT;
        DECLARE @credithourprogram BIT;

        SET @progid = (
                        SELECT  ProgId
                        FROM    arPrgVersions pv
                               ,dbo.arStuEnrollments se
                        WHERE   pv.PrgVerId = se.PrgVerId
                                AND se.StuEnrollId = @StuEnrollId
                      );

        SET @undergradprogram = (
                                  SELECT    dbo.isProgramUndergraduate(@progid)
                                );
        SET @credithourprogram = (
                                   SELECT   dbo.isProgramCreditHour(@progid)
                                 );

		--Undergrad and credit hour
        IF (
             @undergradprogram = 1
             AND @credithourprogram = 1
           )
            BEGIN
                SET @mtf = ( (
                               SELECT   pv.Credits
                               FROM     arPrgVersions pv
                                       ,dbo.arStuEnrollments se
                               WHERE    pv.PrgVerId = se.PrgVerId
                                        AND se.StuEnrollId = @StuEnrollId
                             ) * 1.50 );
				
            END;

		--undergrad and clock hour
        IF (
             @undergradprogram = 1
             AND @credithourprogram = 0
           )
            BEGIN
                SET @mtf = ( (
                               SELECT   pv.Hours
                               FROM     arPrgVersions pv
                                       ,dbo.arStuEnrollments se
                               WHERE    pv.PrgVerId = se.PrgVerId
                                        AND se.StuEnrollId = @StuEnrollId
                             ) * 1.50 );
            END;		

        
        RETURN @mtf;
    END;
GO
------------------------------------------------------------------------------------------------------------------------------------------
--End US7338:Maximum Time Frame
------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------
--Start US6393
--------------------------------------------------------------------------------------------------------------
IF NOT EXISTS ( SELECT  *
                FROM    sys.columns
                WHERE   object_id = OBJECT_ID(N'[dbo].[adReqs]')
                        AND name = 'IPEDSValue' )
    BEGIN
        ALTER TABLE adReqs ADD IPEDSValue INT;
    END;
GO
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   type = 'P'
                    AND name = 'USP_TestScores_SATCriticalReadingandMath_Detail' )
    DROP PROCEDURE USP_TestScores_SATCriticalReadingandMath_Detail;
GO
CREATE PROCEDURE dbo.USP_TestScores_SATCriticalReadingandMath_Detail
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
   ,@AreasOfInterest VARCHAR(4000) = NULL
AS
    BEGIN -- sycampuses

        DECLARE @ContinuingStartDate DATETIME;
        SET @ContinuingStartDate = @StartDate;
		-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
		-- and Start Date should be blank
        IF DAY(@EndDate) = 15
            AND MONTH(@EndDate) = 10
            BEGIN
                SET @EndDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
                SET @StartDate = @StartDate;
            END;

        DECLARE @LeadswithBothSATCriticalAndMathScores TABLE
            (
             LeadId UNIQUEIDENTIFIER
            );

        DECLARE @LeadWithSATCriticalReading TABLE
            (
             LeadId UNIQUEIDENTIFIER
            );

        DECLARE @LeadwithSATMathScores TABLE
            (
             LeadId UNIQUEIDENTIFIER
            );


        DECLARE @StudentswithBothSATCriticalAndMathScores TABLE
            (
             StudentId UNIQUEIDENTIFIER
            );

        DECLARE @StudentsWithSATCriticalReading TABLE
            (
             StudentId UNIQUEIDENTIFIER
            );

        DECLARE @StudentwithSATMathScores TABLE
            (
             StudentId UNIQUEIDENTIFIER
            );

        INSERT  INTO @LeadWithSATCriticalReading
                SELECT DISTINCT
                        LET.LeadId
                FROM    dbo.adLeadEntranceTest LET
                INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                WHERE   R.IPEDSValue IN ( 161 )
                        AND LET.ActualScore IS NOT NULL
                        AND LET.LeadId IS NOT NULL;

        INSERT  INTO @LeadwithSATMathScores
                SELECT DISTINCT
                        LET.LeadId
                FROM    dbo.adLeadEntranceTest LET
                INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                WHERE   R.IPEDSValue IN ( 162 )
                        AND LET.ActualScore IS NOT NULL
                        AND LET.LeadId IS NOT NULL;


        INSERT  INTO @LeadswithBothSATCriticalAndMathScores
                SELECT DISTINCT
                        LeadId
                FROM    dbo.adLeadEntranceTest LET
                WHERE   LeadId IN ( SELECT DISTINCT
                                            LeadId
                                    FROM    @LeadWithSATCriticalReading )
                        AND LeadId IN ( SELECT DISTINCT
                                                LeadId
                                        FROM    @LeadwithSATMathScores );

        INSERT  INTO @StudentsWithSATCriticalReading
                SELECT DISTINCT
                        LET.StudentId
                FROM    dbo.adLeadEntranceTest LET
                INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                WHERE   R.IPEDSValue IN ( 161 )
                        AND LET.ActualScore IS NOT NULL
                        AND LET.StudentId IS NOT NULL;

        INSERT  INTO @StudentwithSATMathScores
                SELECT DISTINCT
                        LET.StudentId
                FROM    dbo.adLeadEntranceTest LET
                INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                WHERE   R.IPEDSValue IN ( 162 )
                        AND LET.ActualScore IS NOT NULL
                        AND LET.StudentId IS NOT NULL;


        INSERT  INTO @StudentswithBothSATCriticalAndMathScores
                SELECT DISTINCT
                        LET.StudentId
                FROM    dbo.adLeadEntranceTest LET
                WHERE   LET.StudentId IN ( SELECT DISTINCT
                                                    StudentId
                                           FROM     @StudentsWithSATCriticalReading )
                        AND LET.StudentId IN ( SELECT DISTINCT
                                                        StudentId
                                               FROM     @StudentwithSATMathScores );


        SELECT  *
        FROM    (
                  SELECT    dbo.UDF_FormatSSN(t1.SSN) AS SSN
                           ,t1.StudentNumber
                           ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                           ,t3.IPEDSValue
                           ,t2.StartDate
                           ,t3.IPEDSSequence AS GenderSequence
                           ,
				-- Men --
                            CASE WHEN ( t3.IPEDSValue = 30 ) THEN 'X'
                                 ELSE ''
                            END AS ApplicantMen
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 'X'
                                 ELSE ''
                            END AS AdmissionMen
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 1
                                 ELSE 0
                            END AS ApplicantMenCount
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 1
                                 ELSE 0
                            END AS AdmissionMenCount
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 30
                                      ) THEN 'X'
                                 ELSE ''
                            END AS FullTimeMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 30
                                      ) THEN 'X'
                                 ELSE ''
                            END AS PartTimeMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 30
                                      ) THEN 1
                                 ELSE 0
                            END AS FullTimeCountMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 30
                                      ) THEN 1
                                 ELSE 0
                            END AS PartTimeCountMen
                           ,
				-- Men --
				-- Women --
                            CASE WHEN ( t3.IPEDSValue = 31 ) THEN 'X'
                                 ELSE ''
                            END AS ApplicantWomen
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 'X'
                                 ELSE ''
                            END AS AdmissionWomen
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 1
                                 ELSE 0
                            END AS ApplicantWomenCount
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 1
                                 ELSE 0
                            END AS AdmissionWomenCount
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 31
                                      ) THEN 'X'
                                 ELSE ''
                            END AS FullTimeWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 31
                                      ) THEN 'X'
                                 ELSE ''
                            END AS PartTimeWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 31
                                      ) THEN 1
                                 ELSE 0
                            END AS FullTimeCountWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 31
                                      ) THEN 1
                                 ELSE 0
                            END AS PartTimeCountWomen
                           ,(
                              SELECT TOP 1
                                        EnrollmentId
                              FROM      arStuEnrollments C1
                                       ,arPrgVersions C2
                                       ,arProgTypes C3
                              WHERE     C1.PrgVerId = C2.PrgVerId
                                        AND C2.ProgTypId = C3.ProgTypId
                                        AND C3.IPEDSValue = 58
                                        AND C1.StudentId = t1.StudentId
                              ORDER BY  C1.StartDate
                                       ,C1.EnrollDate
                            ) AS EnrollmentId
                           ,(
                              SELECT TOP 1
                                        ActualScore
                              FROM      dbo.adLeadEntranceTest LET
                              INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                              WHERE     (
                                          LET.LeadId = t2.LeadId
                                          OR LET.StudentId = t1.StudentId
                                        )
                                        AND R.IPEDSValue IN ( 161 ) -- SAT Critical
                                        AND LET.ActualScore IS NOT NULL
                              ORDER BY  LET.ActualScore DESC
                            ) AS Score
                  FROM      arStudent t1
                  LEFT JOIN adGenders t3 ON t1.Gender = t3.GenderId
                  LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                  INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                  LEFT JOIN adDegCertSeeking t11 ON t11.DegCertSeekingId = t2.DegCertSeekingId ---- For Degree Seeking --
                  INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                  INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                               AND t6.SysStatusId NOT IN ( 8 )
                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                  INNER JOIN arPrgGrp t12 ON t12.PrgGrpId = t7.PrgGrpId
                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                  LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                  WHERE     t2.CampusId = @CampusId
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t9.IPEDSValue = 58
                            AND -- Under Graduate
                            (
                              t10.IPEDSValue = 61
                              OR t10.IPEDSValue = 62
                            )
                            AND --Part Time or Full Time
                            (
                              t3.IPEDSValue = 30
                              OR t3.IPEDSValue = 31
                            )
                            AND -- Men or Women
                            t1.Race IS NOT NULL
                            AND t6.SysStatusId NOT IN ( 8 )  -- Exclude students who are No Start Students
                            AND t2.StartDate <= @EndDate
                            AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
					-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )
					--DE8492
					-- Transfer-In Students need to be excluded
                            AND t2.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                StuEnrollId
                                                        FROM    arStuEnrollments
                                                        WHERE   CampusId = LTRIM(RTRIM(@CampusId))
                                                                AND LeadId IS NOT NULL
                                                                AND StuEnrollId IN ( SELECT StuEnrollId
                                                                                     FROM   arStuEnrollments
                                                                                     WHERE  TransferHours > 0
                                                                                            AND CampusId = LTRIM(RTRIM(@CampusId))
                                                                                     UNION
                                                                                     SELECT StuEnrollId
                                                                                     FROM   arTransferGrades
                                                                                     WHERE  GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                                FROM    arGradeSystemDetails
                                                                                                                WHERE   IsTransferGrade = 1 ) ) )
                            AND ( t11.IPEDSValue = 11 ) -- Degree/Cert Seeking
                            AND (
                                  @AreasOfInterest IS NULL
                                  OR t12.PrgGrpId IN ( SELECT   Val
                                                       FROM     MultipleValuesForReportParameters(@AreasOfInterest,',',1) )
                                ) --Areas Of Intrest --
                            AND (
                                  t2.LeadId IN ( SELECT DISTINCT
                                                        LeadId
                                                 FROM   @LeadswithBothSATCriticalAndMathScores )
                                  OR t2.StudentId IN ( SELECT DISTINCT
                                                                StudentId
                                                       FROM     @StudentswithBothSATCriticalAndMathScores )
                                )
                ) dt
        ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
                 END
               ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
                END
               , -- LastName end,
                CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
                END
               ,CASE WHEN @OrderBy = 'EnrollmentId' THEN EnrollmentId
                END;
    END;

GO
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   type = 'P'
                    AND name = 'USP_TestScores_SATMath_Detail' )
    DROP PROCEDURE USP_TestScores_SATMath_Detail;
GO
CREATE PROCEDURE dbo.USP_TestScores_SATMath_Detail
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
   ,@AreasOfInterest VARCHAR(4000) = NULL
AS
    BEGIN -- sycampuses

        DECLARE @ContinuingStartDate DATETIME;
        SET @ContinuingStartDate = @StartDate;
-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
-- and Start Date should be blank
        IF DAY(@EndDate) = 15
            AND MONTH(@EndDate) = 10
            BEGIN
                SET @EndDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
                SET @StartDate = @StartDate;
            END;


        DECLARE @LeadswithScores TABLE
            (
             LeadId UNIQUEIDENTIFIER
            );

        DECLARE @StudentwithScores TABLE
            (
             StudentId UNIQUEIDENTIFIER
            );

        INSERT  INTO @LeadswithScores
                SELECT DISTINCT
                        LET.LeadId
                FROM    dbo.adLeadEntranceTest LET
                INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                WHERE   R.IPEDSValue IN ( 162 ) -- SAT Math
                        AND LET.ActualScore IS NOT NULL
                        AND LET.LeadId IS NOT NULL;

        INSERT  INTO @StudentwithScores
                SELECT DISTINCT
                        LET.StudentId
                FROM    dbo.adLeadEntranceTest LET
                INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                WHERE   R.IPEDSValue IN ( 162 ) -- SAT Math
                        AND LET.ActualScore IS NOT NULL
                        AND LET.StudentId IS NOT NULL;

        SELECT  *
        FROM    (
                  SELECT    dbo.UDF_FormatSSN(t1.SSN) AS SSN
                           ,t1.StudentNumber
                           ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                           ,t3.IPEDSValue
                           ,t2.StartDate
                           ,t3.IPEDSSequence AS GenderSequence
                           ,
				-- Men --
                            CASE WHEN ( t3.IPEDSValue = 30 ) THEN 'X'
                                 ELSE ''
                            END AS ApplicantMen
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 'X'
                                 ELSE ''
                            END AS AdmissionMen
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 1
                                 ELSE 0
                            END AS ApplicantMenCount
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 1
                                 ELSE 0
                            END AS AdmissionMenCount
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 30
                                      ) THEN 'X'
                                 ELSE ''
                            END AS FullTimeMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 30
                                      ) THEN 'X'
                                 ELSE ''
                            END AS PartTimeMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 30
                                      ) THEN 1
                                 ELSE 0
                            END AS FullTimeCountMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 30
                                      ) THEN 1
                                 ELSE 0
                            END AS PartTimeCountMen
                           ,
				-- Men --
				-- Women --
                            CASE WHEN ( t3.IPEDSValue = 31 ) THEN 'X'
                                 ELSE ''
                            END AS ApplicantWomen
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 'X'
                                 ELSE ''
                            END AS AdmissionWomen
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 1
                                 ELSE 0
                            END AS ApplicantWomenCount
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 1
                                 ELSE 0
                            END AS AdmissionWomenCount
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 31
                                      ) THEN 'X'
                                 ELSE ''
                            END AS FullTimeWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 31
                                      ) THEN 'X'
                                 ELSE ''
                            END AS PartTimeWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 31
                                      ) THEN 1
                                 ELSE 0
                            END AS FullTimeCountWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 31
                                      ) THEN 1
                                 ELSE 0
                            END AS PartTimeCountWomen
                           ,(
                              SELECT TOP 1
                                        EnrollmentId
                              FROM      arStuEnrollments C1
                                       ,arPrgVersions C2
                                       ,arProgTypes C3
                              WHERE     C1.PrgVerId = C2.PrgVerId
                                        AND C2.ProgTypId = C3.ProgTypId
                                        AND C3.IPEDSValue = 58
                                        AND C1.StudentId = t1.StudentId
                              ORDER BY  C1.StartDate
                                       ,C1.EnrollDate
                            ) AS EnrollmentId
                           ,(
                              SELECT TOP 1
                                        ActualScore
                              FROM      dbo.adLeadEntranceTest LET
                              INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                              WHERE     (
                                          LET.LeadId = t2.LeadId
                                          OR LET.StudentId = t1.StudentId
                                        )
                                        AND R.IPEDSValue IN ( 162 ) -- SAT Math
                                        AND LET.ActualScore IS NOT NULL
                              ORDER BY  LET.ActualScore DESC
                            ) AS Score
                  FROM      arStudent t1
                  LEFT JOIN adGenders t3 ON t1.Gender = t3.GenderId
                  LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                  INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                  LEFT JOIN adDegCertSeeking t11 ON t11.DegCertSeekingId = t2.DegCertSeekingId ---- For Degree Seeking --
                  INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                  INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                               AND t6.SysStatusId NOT IN ( 8 )
                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                  INNER JOIN arPrgGrp t12 ON t12.PrgGrpId = t7.PrgGrpId
                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                  LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                  WHERE     t2.CampusId = @CampusId
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t9.IPEDSValue = 58
                            AND -- Under Graduate
                            (
                              t10.IPEDSValue = 61
                              OR t10.IPEDSValue = 62
                            )
                            AND --Part Time or Full Time
                            (
                              t3.IPEDSValue = 30
                              OR t3.IPEDSValue = 31
                            )
                            AND -- Men or Women
                            t1.Race IS NOT NULL
                            AND t6.SysStatusId NOT IN ( 8 )  -- Exclude students who are No Start Students
                            AND t2.StartDate <= @EndDate
                            AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
					-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )
					--DE8492
					-- Transfer-In Students need to be excluded
                            AND t2.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                StuEnrollId
                                                        FROM    arStuEnrollments
                                                        WHERE   CampusId = LTRIM(RTRIM(@CampusId))
                                                                AND LeadId IS NOT NULL
                                                                AND StuEnrollId IN ( SELECT StuEnrollId
                                                                                     FROM   arStuEnrollments
                                                                                     WHERE  TransferHours > 0
                                                                                            AND CampusId = LTRIM(RTRIM(@CampusId))
                                                                                     UNION
                                                                                     SELECT StuEnrollId
                                                                                     FROM   arTransferGrades
                                                                                     WHERE  GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                                FROM    arGradeSystemDetails
                                                                                                                WHERE   IsTransferGrade = 1 ) ) )
                            AND ( t11.IPEDSValue = 11 ) -- Degree/Cert Seeking
                            AND (
                                  @AreasOfInterest IS NULL
                                  OR t12.PrgGrpId IN ( SELECT   Val
                                                       FROM     MultipleValuesForReportParameters(@AreasOfInterest,',',1) )
                                ) --Areas Of Intrest --
                            AND (
                                  t2.LeadId IN ( SELECT DISTINCT
                                                        LeadId
                                                 FROM   @LeadswithScores )
                                  OR t2.StudentId IN ( SELECT DISTINCT
                                                                StudentId
                                                       FROM     @StudentwithScores )
                                )
                ) dt
        ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
                 END
               ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
                END
               , -- LastName end,
                CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
                END
               ,CASE WHEN @OrderBy = 'EnrollmentId' THEN EnrollmentId
                END;
    END;
GO
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   type = 'P'
                    AND name = 'USP_TestScores_ACTComposite_Detail' )
    DROP PROCEDURE USP_TestScores_ACTComposite_Detail;
GO
CREATE PROCEDURE dbo.USP_TestScores_ACTComposite_Detail
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
   ,@AreasOfInterest VARCHAR(4000) = NULL
AS
    BEGIN -- sycampuses

        DECLARE @ContinuingStartDate DATETIME;
        SET @ContinuingStartDate = @StartDate;
-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
-- and Start Date should be blank
        IF DAY(@EndDate) = 15
            AND MONTH(@EndDate) = 10
            BEGIN
                SET @EndDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
                SET @StartDate = @StartDate;
            END;


        DECLARE @LeadswithScores TABLE
            (
             LeadId UNIQUEIDENTIFIER
            );

        DECLARE @LeadswithACTComposite TABLE
            (
             LeadId UNIQUEIDENTIFIER
            ); --164
        DECLARE @LeadswithACTEnglish TABLE
            (
             LeadId UNIQUEIDENTIFIER
            ); --165
        DECLARE @LeadswithACTMath TABLE
            (
             LeadId UNIQUEIDENTIFIER
            ); --166

        INSERT  INTO @LeadswithACTComposite
                SELECT DISTINCT
                        LET.LeadId
                FROM    dbo.adLeadEntranceTest LET
                INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                WHERE   R.IPEDSValue IN ( 164 )
                        AND LET.ActualScore IS NOT NULL
                        AND LET.LeadId IS NOT NULL;

        INSERT  INTO @LeadswithACTEnglish
                SELECT DISTINCT
                        LET.LeadId
                FROM    dbo.adLeadEntranceTest LET
                INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                WHERE   R.IPEDSValue IN ( 165 )
                        AND LET.ActualScore IS NOT NULL
                        AND LET.LeadId IS NOT NULL;

        INSERT  INTO @LeadswithACTMath
                SELECT DISTINCT
                        LET.LeadId
                FROM    dbo.adLeadEntranceTest LET
                INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                WHERE   R.IPEDSValue IN ( 166 )
                        AND LET.ActualScore IS NOT NULL
                        AND LET.LeadId IS NOT NULL;

        INSERT  INTO @LeadswithScores
                SELECT DISTINCT
                        LET.LeadId
                FROM    dbo.adLeadEntranceTest LET
                WHERE   LET.LeadId IN ( SELECT DISTINCT
                                                LeadId
                                        FROM    @LeadswithACTComposite )
                        AND LET.LeadId IN ( SELECT DISTINCT
                                                    LeadId
                                            FROM    @LeadswithACTEnglish )
                        AND LET.LeadId IN ( SELECT DISTINCT
                                                    LeadId
                                            FROM    @LeadswithACTMath );

        DECLARE @StudentswithScores TABLE
            (
             StudentId UNIQUEIDENTIFIER
            );

        DECLARE @StudentswithACTComposite TABLE
            (
             StudentId UNIQUEIDENTIFIER
            ); --164

        DECLARE @StudentswithACTEnglish TABLE
            (
             StudentId UNIQUEIDENTIFIER
            ); --165

        DECLARE @StudentswithACTMath TABLE
            (
             StudentId UNIQUEIDENTIFIER
            ); --166


        INSERT  INTO @StudentswithACTComposite
                SELECT DISTINCT
                        LET.StudentId
                FROM    adLeadEntranceTest LET
                INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                WHERE   R.IPEDSValue IN ( 164 )
                        AND LET.ActualScore IS NOT NULL
                        AND LET.StudentId IS NOT NULL;

        INSERT  INTO @StudentswithACTEnglish
                SELECT DISTINCT
                        LET.StudentId
                FROM    dbo.adLeadEntranceTest LET
                INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                WHERE   R.IPEDSValue IN ( 165 )
                        AND LET.ActualScore IS NOT NULL
                        AND LET.StudentId IS NOT NULL;

        INSERT  INTO @StudentswithACTMath
                SELECT DISTINCT
                        LET.StudentId
                FROM    dbo.adLeadEntranceTest LET
                INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                WHERE   R.IPEDSValue IN ( 166 )
                        AND LET.ActualScore IS NOT NULL
                        AND LET.StudentId IS NOT NULL;


        INSERT  INTO @StudentswithScores
                SELECT DISTINCT
                        LET.StudentId
                FROM    dbo.adLeadEntranceTest LET
                WHERE   LET.StudentId IN ( SELECT DISTINCT
                                                    StudentId
                                           FROM     @StudentswithACTComposite )
                        AND LET.StudentId IN ( SELECT DISTINCT
                                                        StudentId
                                               FROM     @StudentswithACTEnglish )
                        AND LET.StudentId IN ( SELECT DISTINCT
                                                        StudentId
                                               FROM     @StudentswithACTMath );

        SELECT  *
        FROM    (
                  SELECT    dbo.UDF_FormatSSN(t1.SSN) AS SSN
                           ,t1.StudentNumber
                           ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                           ,t3.IPEDSValue
                           ,t2.StartDate
                           ,t3.IPEDSSequence AS GenderSequence
                           ,
				-- Men --
                            CASE WHEN ( t3.IPEDSValue = 30 ) THEN 'X'
                                 ELSE ''
                            END AS ApplicantMen
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 'X'
                                 ELSE ''
                            END AS AdmissionMen
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 1
                                 ELSE 0
                            END AS ApplicantMenCount
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 1
                                 ELSE 0
                            END AS AdmissionMenCount
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 30
                                      ) THEN 'X'
                                 ELSE ''
                            END AS FullTimeMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 30
                                      ) THEN 'X'
                                 ELSE ''
                            END AS PartTimeMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 30
                                      ) THEN 1
                                 ELSE 0
                            END AS FullTimeCountMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 30
                                      ) THEN 1
                                 ELSE 0
                            END AS PartTimeCountMen
                           ,
				-- Men --
				-- Women --
                            CASE WHEN ( t3.IPEDSValue = 31 ) THEN 'X'
                                 ELSE ''
                            END AS ApplicantWomen
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 'X'
                                 ELSE ''
                            END AS AdmissionWomen
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 1
                                 ELSE 0
                            END AS ApplicantWomenCount
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 1
                                 ELSE 0
                            END AS AdmissionWomenCount
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 31
                                      ) THEN 'X'
                                 ELSE ''
                            END AS FullTimeWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 31
                                      ) THEN 'X'
                                 ELSE ''
                            END AS PartTimeWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 31
                                      ) THEN 1
                                 ELSE 0
                            END AS FullTimeCountWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 31
                                      ) THEN 1
                                 ELSE 0
                            END AS PartTimeCountWomen
                           ,(
                              SELECT TOP 1
                                        EnrollmentId
                              FROM      arStuEnrollments C1
                                       ,arPrgVersions C2
                                       ,arProgTypes C3
                              WHERE     C1.PrgVerId = C2.PrgVerId
                                        AND C2.ProgTypId = C3.ProgTypId
                                        AND C3.IPEDSValue = 58
                                        AND C1.StudentId = t1.StudentId
                              ORDER BY  C1.StartDate
                                       ,C1.EnrollDate
                            ) AS EnrollmentId
                           ,(
                              SELECT TOP 1
                                        ActualScore
                              FROM      dbo.adLeadEntranceTest LET
                              INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                              WHERE     (
                                          LET.LeadId = t2.LeadId
                                          OR LET.StudentId = t1.StudentId
                                        )
                                        AND R.IPEDSValue IN ( 164 ) -- ACT Composite
                                        AND LET.ActualScore IS NOT NULL
                              ORDER BY  LET.ActualScore DESC
                            ) AS Score
                  FROM      arStudent t1
                  LEFT JOIN adGenders t3 ON t1.Gender = t3.GenderId
                  LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                  INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                  LEFT JOIN adDegCertSeeking t11 ON t11.DegCertSeekingId = t2.DegCertSeekingId ---- For Degree Seeking --
                  INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                  INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                               AND t6.SysStatusId NOT IN ( 8 )
                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                  INNER JOIN arPrgGrp t12 ON t12.PrgGrpId = t7.PrgGrpId
                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                  LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                  WHERE     t2.CampusId = @CampusId
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t9.IPEDSValue = 58
                            AND -- Under Graduate
                            (
                              t10.IPEDSValue = 61
                              OR t10.IPEDSValue = 62
                            )
                            AND --Part Time or Full Time
                            (
                              t3.IPEDSValue = 30
                              OR t3.IPEDSValue = 31
                            )
                            AND -- Men or Women
                            t1.Race IS NOT NULL
                            AND t6.SysStatusId NOT IN ( 8 )  -- Exclude students who are No Start Students
                            AND t2.StartDate <= @EndDate
                            AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
					-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )
					--DE8492
					-- Transfer-In Students need to be excluded
                            AND t2.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                StuEnrollId
                                                        FROM    arStuEnrollments
                                                        WHERE   CampusId = LTRIM(RTRIM(@CampusId))
                                                                AND LeadId IS NOT NULL
                                                                AND StuEnrollId IN ( SELECT StuEnrollId
                                                                                     FROM   arStuEnrollments
                                                                                     WHERE  TransferHours > 0
                                                                                            AND CampusId = LTRIM(RTRIM(@CampusId))
                                                                                     UNION
                                                                                     SELECT StuEnrollId
                                                                                     FROM   arTransferGrades
                                                                                     WHERE  GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                                FROM    arGradeSystemDetails
                                                                                                                WHERE   IsTransferGrade = 1 ) ) )
                            AND ( t11.IPEDSValue = 11 ) -- Degree/Cert Seeking
                            AND (
                                  @AreasOfInterest IS NULL
                                  OR t12.PrgGrpId IN ( SELECT   Val
                                                       FROM     MultipleValuesForReportParameters(@AreasOfInterest,',',1) )
                                ) --Areas Of Intrest --
                            AND (
                                  t2.LeadId IN ( SELECT DISTINCT
                                                        LeadId
                                                 FROM   @LeadswithScores )
                                  OR t2.StudentId IN ( SELECT DISTINCT
                                                                StudentId
                                                       FROM     @StudentswithScores )
                                )
                ) dt
        ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
                 END
               ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
                END
               , -- LastName end,
                CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
                END
               ,CASE WHEN @OrderBy = 'EnrollmentId' THEN EnrollmentId
                END;
    END;
GO
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   type = 'P'
                    AND name = 'USP_TestScores_ACTMath_Detail' )
    DROP PROCEDURE USP_TestScores_ACTMath_Detail;
GO
CREATE PROCEDURE dbo.USP_TestScores_ACTMath_Detail
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
   ,@AreasOfInterest VARCHAR(4000) = NULL
AS
    BEGIN -- sycampuses

        DECLARE @ContinuingStartDate DATETIME;
        SET @ContinuingStartDate = @StartDate;
		-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
		-- and Start Date should be blank
        IF DAY(@EndDate) = 15
            AND MONTH(@EndDate) = 10
            BEGIN
                SET @EndDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
                SET @StartDate = @StartDate;
            END;


        DECLARE @LeadswithScores TABLE
            (
             LeadId UNIQUEIDENTIFIER
            );

        DECLARE @LeadswithACTMath TABLE
            (
             LeadId UNIQUEIDENTIFIER
            ); --166

        DECLARE @StudentswithScores TABLE
            (
             StudentId UNIQUEIDENTIFIER
            );

        DECLARE @StudentswithACTMath TABLE
            (
             StudentId UNIQUEIDENTIFIER
            ); --166

       
        INSERT  INTO @LeadswithACTMath
                SELECT DISTINCT
                        LET.LeadId
                FROM    dbo.adLeadEntranceTest LET
                INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                WHERE   R.IPEDSValue IN ( 166 )
                        AND LET.ActualScore IS NOT NULL
                        AND LET.LeadId IS NOT NULL;
       
        INSERT  INTO @LeadswithScores
                SELECT DISTINCT
                        LET.LeadId
                FROM    dbo.adLeadEntranceTest LET
                WHERE   LET.LeadId IN ( SELECT DISTINCT
                                                LeadId
                                        FROM    @LeadswithACTMath );

        INSERT  INTO @StudentswithACTMath
                SELECT DISTINCT
                        LET.StudentId
                FROM    dbo.adLeadEntranceTest LET
                INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                WHERE   R.IPEDSValue IN ( 166 )
                        AND LET.ActualScore IS NOT NULL
                        AND LET.StudentId IS NOT NULL;
       
        INSERT  INTO @StudentswithScores
                SELECT DISTINCT
                        LET.StudentId
                FROM    dbo.adLeadEntranceTest LET
                WHERE   LET.StudentId IN ( SELECT DISTINCT
                                                    StudentId
                                           FROM     @StudentswithACTMath );


        SELECT  *
        FROM    (
                  SELECT    dbo.UDF_FormatSSN(t1.SSN) AS SSN
                           ,t1.StudentNumber
                           ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                           ,t3.IPEDSValue
                           ,t2.StartDate
                           ,t3.IPEDSSequence AS GenderSequence
                           ,
				-- Men --
                            CASE WHEN ( t3.IPEDSValue = 30 ) THEN 'X'
                                 ELSE ''
                            END AS ApplicantMen
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 'X'
                                 ELSE ''
                            END AS AdmissionMen
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 1
                                 ELSE 0
                            END AS ApplicantMenCount
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 1
                                 ELSE 0
                            END AS AdmissionMenCount
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 30
                                      ) THEN 'X'
                                 ELSE ''
                            END AS FullTimeMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 30
                                      ) THEN 'X'
                                 ELSE ''
                            END AS PartTimeMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 30
                                      ) THEN 1
                                 ELSE 0
                            END AS FullTimeCountMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 30
                                      ) THEN 1
                                 ELSE 0
                            END AS PartTimeCountMen
                           ,
				-- Men --
				-- Women --
                            CASE WHEN ( t3.IPEDSValue = 31 ) THEN 'X'
                                 ELSE ''
                            END AS ApplicantWomen
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 'X'
                                 ELSE ''
                            END AS AdmissionWomen
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 1
                                 ELSE 0
                            END AS ApplicantWomenCount
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 1
                                 ELSE 0
                            END AS AdmissionWomenCount
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 31
                                      ) THEN 'X'
                                 ELSE ''
                            END AS FullTimeWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 31
                                      ) THEN 'X'
                                 ELSE ''
                            END AS PartTimeWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 31
                                      ) THEN 1
                                 ELSE 0
                            END AS FullTimeCountWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 31
                                      ) THEN 1
                                 ELSE 0
                            END AS PartTimeCountWomen
                           ,(
                              SELECT TOP 1
                                        EnrollmentId
                              FROM      arStuEnrollments C1
                                       ,arPrgVersions C2
                                       ,arProgTypes C3
                              WHERE     C1.PrgVerId = C2.PrgVerId
                                        AND C2.ProgTypId = C3.ProgTypId
                                        AND C3.IPEDSValue = 58
                                        AND C1.StudentId = t1.StudentId
                              ORDER BY  C1.StartDate
                                       ,C1.EnrollDate
                            ) AS EnrollmentId
                           ,(
                              SELECT TOP 1
                                        ActualScore
                              FROM      dbo.adLeadEntranceTest LET
                              INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                              WHERE     (
                                          LET.LeadId = t2.LeadId
                                          OR LET.StudentId = t1.StudentId
                                        )
                                        AND R.IPEDSValue IN ( 166 ) -- ACT Math
                                        AND LET.ActualScore IS NOT NULL
                              ORDER BY  LET.ActualScore DESC
                            ) AS Score
                  FROM      arStudent t1
                  LEFT JOIN adGenders t3 ON t1.Gender = t3.GenderId
                  LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                  INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                  LEFT JOIN adDegCertSeeking t11 ON t11.DegCertSeekingId = t2.DegCertSeekingId ---- For Degree Seeking --
                  INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                  INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                               AND t6.SysStatusId NOT IN ( 8 )
                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                  INNER JOIN arPrgGrp t12 ON t12.PrgGrpId = t7.PrgGrpId
                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                  LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                  WHERE     t2.CampusId = @CampusId
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t9.IPEDSValue = 58
                            AND -- Under Graduate
                            (
                              t10.IPEDSValue = 61
                              OR t10.IPEDSValue = 62
                            )
                            AND --Part Time or Full Time
                            (
                              t3.IPEDSValue = 30
                              OR t3.IPEDSValue = 31
                            )
                            AND -- Men or Women
                            t1.Race IS NOT NULL
                            AND t6.SysStatusId NOT IN ( 8 )  -- Exclude students who are No Start Students
                            AND t2.StartDate <= @EndDate
                            AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
					-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )
					--DE8492
					-- Transfer-In Students need to be excluded
                            AND t2.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                StuEnrollId
                                                        FROM    arStuEnrollments
                                                        WHERE   CampusId = LTRIM(RTRIM(@CampusId))
                                                                AND LeadId IS NOT NULL
                                                                AND StuEnrollId IN ( SELECT StuEnrollId
                                                                                     FROM   arStuEnrollments
                                                                                     WHERE  TransferHours > 0
                                                                                            AND CampusId = LTRIM(RTRIM(@CampusId))
                                                                                     UNION
                                                                                     SELECT StuEnrollId
                                                                                     FROM   arTransferGrades
                                                                                     WHERE  GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                                FROM    arGradeSystemDetails
                                                                                                                WHERE   IsTransferGrade = 1 ) ) )
                            AND ( t11.IPEDSValue = 11 ) -- Degree/Cert Seeking
                            AND (
                                  @AreasOfInterest IS NULL
                                  OR t12.PrgGrpId IN ( SELECT   Val
                                                       FROM     MultipleValuesForReportParameters(@AreasOfInterest,',',1) )
                                ) --Areas Of Intrest --
                            AND (
                                  t2.LeadId IN ( SELECT DISTINCT
                                                        LeadId
                                                 FROM   @LeadswithScores )
                                  OR t2.StudentId IN ( SELECT DISTINCT
                                                                StudentId
                                                       FROM     @StudentswithScores )
                                )
                ) dt
        ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
                 END
               ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
                END
               , -- LastName end,
                CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
                END
               ,CASE WHEN @OrderBy = 'EnrollmentId' THEN EnrollmentId
                END;
    END;
GO
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   type = 'P'
                    AND name = 'USP_TestScores_ACTEnglish_Detail' )
    DROP PROCEDURE USP_TestScores_ACTEnglish_Detail;
GO
CREATE PROCEDURE dbo.USP_TestScores_ACTEnglish_Detail
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
   ,@AreasOfInterest VARCHAR(4000) = NULL
AS
    BEGIN -- sycampuses

        DECLARE @ContinuingStartDate DATETIME;
        SET @ContinuingStartDate = @StartDate;
	-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
	-- and Start Date should be blank
        IF DAY(@EndDate) = 15
            AND MONTH(@EndDate) = 10
            BEGIN
                SET @EndDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
                SET @StartDate = @StartDate;
            END;


        DECLARE @LeadswithScores TABLE
            (
             LeadId UNIQUEIDENTIFIER
            );

        DECLARE @LeadswithACTEnglish TABLE
            (
             LeadId UNIQUEIDENTIFIER
            ); 

        INSERT  INTO @LeadswithACTEnglish
                SELECT DISTINCT
                        LET.LeadId
                FROM    dbo.adLeadEntranceTest LET
                INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                WHERE   R.IPEDSValue IN ( 165 )
                        AND LET.ActualScore IS NOT NULL
                        AND LET.LeadId IS NOT NULL;

        
        INSERT  INTO @LeadswithScores
                SELECT DISTINCT
                        LET.LeadId
                FROM    dbo.adLeadEntranceTest LET
                WHERE   LET.LeadId IN ( SELECT DISTINCT
                                                LeadId
                                        FROM    @LeadswithACTEnglish )
                        AND LET.LeadId IS NOT NULL;

        DECLARE @StudentswithScores TABLE
            (
             StudentId UNIQUEIDENTIFIER
            );

        DECLARE @StudentswithACTEnglish TABLE
            (
             StudentId UNIQUEIDENTIFIER
            ); 

        INSERT  INTO @StudentswithACTEnglish
                SELECT DISTINCT
                        LET.StudentId
                FROM    dbo.adLeadEntranceTest LET
                INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                WHERE   R.IPEDSValue IN ( 165 )
                        AND LET.ActualScore IS NOT NULL
                        AND LET.StudentId IS NOT NULL;

        
        INSERT  INTO @StudentswithScores
                SELECT DISTINCT
                        LET.StudentId
                FROM    dbo.adLeadEntranceTest LET
                WHERE   LET.StudentId IN ( SELECT DISTINCT
                                                    StudentId
                                           FROM     @StudentswithACTEnglish )
                        AND LET.StudentId IS NOT NULL;
    
        SELECT  *
        FROM    (
                  SELECT    dbo.UDF_FormatSSN(t1.SSN) AS SSN
                           ,t1.StudentNumber
                           ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                           ,t3.IPEDSValue
                           ,t2.StartDate
                           ,t3.IPEDSSequence AS GenderSequence
                           ,
				-- Men --
                            CASE WHEN ( t3.IPEDSValue = 30 ) THEN 'X'
                                 ELSE ''
                            END AS ApplicantMen
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 'X'
                                 ELSE ''
                            END AS AdmissionMen
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 1
                                 ELSE 0
                            END AS ApplicantMenCount
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 1
                                 ELSE 0
                            END AS AdmissionMenCount
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 30
                                      ) THEN 'X'
                                 ELSE ''
                            END AS FullTimeMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 30
                                      ) THEN 'X'
                                 ELSE ''
                            END AS PartTimeMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 30
                                      ) THEN 1
                                 ELSE 0
                            END AS FullTimeCountMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 30
                                      ) THEN 1
                                 ELSE 0
                            END AS PartTimeCountMen
                           ,
				-- Men --
				-- Women --
                            CASE WHEN ( t3.IPEDSValue = 31 ) THEN 'X'
                                 ELSE ''
                            END AS ApplicantWomen
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 'X'
                                 ELSE ''
                            END AS AdmissionWomen
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 1
                                 ELSE 0
                            END AS ApplicantWomenCount
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 1
                                 ELSE 0
                            END AS AdmissionWomenCount
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 31
                                      ) THEN 'X'
                                 ELSE ''
                            END AS FullTimeWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 31
                                      ) THEN 'X'
                                 ELSE ''
                            END AS PartTimeWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 31
                                      ) THEN 1
                                 ELSE 0
                            END AS FullTimeCountWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 31
                                      ) THEN 1
                                 ELSE 0
                            END AS PartTimeCountWomen
                           ,(
                              SELECT TOP 1
                                        EnrollmentId
                              FROM      arStuEnrollments C1
                                       ,arPrgVersions C2
                                       ,arProgTypes C3
                              WHERE     C1.PrgVerId = C2.PrgVerId
                                        AND C2.ProgTypId = C3.ProgTypId
                                        AND C3.IPEDSValue = 58
                                        AND C1.StudentId = t1.StudentId
                              ORDER BY  C1.StartDate
                                       ,C1.EnrollDate
                            ) AS EnrollmentId
                           ,(
                              SELECT TOP 1
                                        ActualScore
                              FROM      dbo.adLeadEntranceTest LET
                              INNER JOIN adReqs R ON R.adReqId = LET.EntrTestId
                              WHERE     (
                                          LET.LeadId = t2.LeadId
                                          OR LET.StudentId = t1.StudentId
                                        )
                                        AND R.IPEDSValue IN ( 165 ) -- ACT English
                                        AND LET.ActualScore IS NOT NULL
                              ORDER BY  LET.ActualScore DESC
                            ) AS Score
                  FROM      arStudent t1
                  LEFT JOIN adGenders t3 ON t1.Gender = t3.GenderId
                  LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                  INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                  LEFT JOIN adDegCertSeeking t11 ON t11.DegCertSeekingId = t2.DegCertSeekingId ---- For Degree Seeking --
                  INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                  INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                               AND t6.SysStatusId NOT IN ( 8 )
                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                  INNER JOIN arPrgGrp t12 ON t12.PrgGrpId = t7.PrgGrpId
                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                  LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                  WHERE     t2.CampusId = @CampusId
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t9.IPEDSValue = 58
                            AND -- Under Graduate
                            (
                              t10.IPEDSValue = 61
                              OR t10.IPEDSValue = 62
                            )
                            AND --Part Time or Full Time
                            (
                              t3.IPEDSValue = 30
                              OR t3.IPEDSValue = 31
                            )
                            AND -- Men or Women
                            t1.Race IS NOT NULL
                            AND t6.SysStatusId NOT IN ( 8 )  -- Exclude students who are No Start Students
                            AND t2.StartDate <= @EndDate
                            AND t2.StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
					-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )
					--DE8492
					-- Transfer-In Students need to be excluded
                            AND t2.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                StuEnrollId
                                                        FROM    arStuEnrollments
                                                        WHERE   CampusId = LTRIM(RTRIM(@CampusId))
                                                                AND LeadId IS NOT NULL
                                                                AND StuEnrollId IN ( SELECT StuEnrollId
                                                                                     FROM   arStuEnrollments
                                                                                     WHERE  TransferHours > 0
                                                                                            AND CampusId = LTRIM(RTRIM(@CampusId))
                                                                                     UNION
                                                                                     SELECT StuEnrollId
                                                                                     FROM   arTransferGrades
                                                                                     WHERE  GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                                FROM    arGradeSystemDetails
                                                                                                                WHERE   IsTransferGrade = 1 ) ) )
                            AND ( t11.IPEDSValue = 11 ) -- Degree/Cert Seeking
                            AND (
                                  @AreasOfInterest IS NULL
                                  OR t12.PrgGrpId IN ( SELECT   Val
                                                       FROM     MultipleValuesForReportParameters(@AreasOfInterest,',',1) )
                                ) --Areas Of Intrest --
                            AND (
                                  t2.LeadId IN ( SELECT DISTINCT
                                                        LeadId
                                                 FROM   @LeadswithScores )
                                  OR t2.StudentId IN ( SELECT DISTINCT
                                                                StudentId
                                                       FROM     @StudentswithScores )
                                )
                ) dt
        ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
                 END
               ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
                END
               , -- LastName end,
                CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
                END
               ,CASE WHEN @OrderBy = 'EnrollmentId' THEN EnrollmentId
                END;
    END;
GO
BEGIN
    DECLARE @ParamSetId INT;
    DECLARE @SetId INT;
    IF NOT EXISTS ( SELECT  *
                    FROM    ParamSet
                    WHERE   SetDisplayName = 'TestScoresReportSet' )
        BEGIN
            INSERT  INTO dbo.ParamSet
                    (
                     SetName
                    ,SetDisplayName
                    ,SetDescription
                    ,SetType
		            )
            VALUES  (
                     N'TestScoresReportCustomOptionsSet'  -- SetName - nvarchar(50)
                    ,N'TestScoresReportSet'  -- SetDisplayName - nvarchar(50)
                    ,N'Custom Options for IPEDS Test Score Report'  -- SetDescription - nvarchar(1000)
                    ,N'Custom'  -- SetType - nvarchar(50)
		            );
        END;
    SET @SetId = (
                   SELECT TOP 1
                            SetId
                   FROM     ParamSet
                   WHERE    SetDisplayName = 'TestScoresReportSet'
                 );
    IF NOT EXISTS ( SELECT  *
                    FROM    ParamSection
                    WHERE   SectionName = 'ParamsForTestScoresReport' )
        BEGIN

            INSERT  INTO dbo.ParamSection
                    (
                     SectionName
                    ,SectionCaption
                    ,SetId
                    ,SectionSeq
                    ,SectionDescription
                    ,SectionType
		            )
            VALUES  (
                     N'ParamsForTestScoresReport'  -- SectionName - nvarchar(50)
                    ,N'Test Scores Report Parameters'  -- SectionCaption - nvarchar(100)
                    ,@SetId  -- SetId - bigint
                    ,1  -- SectionSeq - int
                    ,N'Choose Test Scores Report Parameters'  -- SectionDescription - nvarchar(500)
                    ,0  -- SectionType - int
		            );
        END;
    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.ParamItem
                    WHERE   Caption = 'TestScoresReportOptions' )
        BEGIN
            INSERT  INTO dbo.ParamItem
                    (
                     Caption
                    ,ControllerClass
                    ,valueprop
                    ,ReturnValueName
                    ,IsItemOverriden
		            )
            VALUES  (
                     N'TestScoresReportOptions'
                    ,N'ParamIPEDS.ascx'
                    ,0
                    ,'TestScoresReportOptions'
                    ,NULL
		            );
        END;
    DECLARE @ItemId INT;
    SET @ItemId = (
                    SELECT  ItemId
                    FROM    ParamItem
                    WHERE   Caption = 'TestScoresReportOptions'
                  );
    IF NOT EXISTS ( SELECT  *
                    FROM    ParamItemProp
                    WHERE   itemid = @ItemId )
        BEGIN
            INSERT  INTO dbo.ParamItemProp
                    (
                     itemid
                    ,childcontrol
                    ,propname
                    ,value
                    ,valuetype
		            )
            VALUES  (
                     @ItemId  -- itemid - bigint
                    ,NULL  -- childcontrol - nvarchar(100)
                    ,N'IPEDSControlType'  -- propname - nvarchar(100)
                    ,21  -- value - nvarchar(max)
                    ,N'Enum'  -- valuetype - nvarchar(25)
		            );
        END;
    DECLARE @SectionId INT
       ,@DetailId INT;
    SET @SectionId = (
                       SELECT TOP 1
                                SectionId
                       FROM     ParamSection
                       WHERE    SectionName = 'ParamsForTestScoresReport'
                     );
    SET @ItemId = (
                    SELECT TOP 1
                            ItemId
                    FROM    dbo.ParamItem
                    WHERE   Caption = 'TestScoresReportOptions'
                  );
    IF NOT EXISTS ( SELECT  *
                    FROM    ParamDetail
                    WHERE   ItemId = @ItemId
                            AND SectionId = @SectionId )
        BEGIN
            INSERT  INTO dbo.ParamDetail
                    (
                     SectionId
                    ,ItemId
                    ,CaptionOverride
                    ,ItemSeq
		            )
            VALUES  (
                     @SectionId  -- SectionId - bigint
                    ,@ItemId  -- ItemId - bigint
                    ,N'Report Options'  -- CaptionOverride - nvarchar(50)
                    ,1  -- ItemSeq - int
		            );
        END;
    SET @DetailId = (
                      SELECT TOP 1
                                DetailId
                      FROM      ParamDetail
                      WHERE     SectionId = @SectionId
                                AND ItemId = @ItemId
                    );

    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.ParamDetailProp
                    WHERE   detailid = @DetailId
                            AND propname = 'IPEDSControlType' )
        BEGIN
            INSERT  INTO dbo.ParamDetailProp
                    (
                     detailid
                    ,childcontrol
                    ,propname
                    ,value
                    ,valuetype
		            )
            VALUES  (
                     @DetailId  -- detailid - bigint
                    ,NULL  -- childcontrol - nvarchar(100)
                    ,N'IPEDSControlType'  -- propname - nvarchar(100)
                    ,21  -- value - nvarchar(max)
                    ,N'Enum'  -- valuetype - nvarchar(25)
		            );
        END;
    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.ParamDetailProp
                    WHERE   detailid = @DetailId
                            AND propname = 'SelectedDatePartProgram' )
        BEGIN
            INSERT  INTO dbo.ParamDetailProp
                    (
                     detailid
                    ,childcontrol
                    ,propname
                    ,value
                    ,valuetype
		            )
            VALUES  (
                     @DetailId  -- detailid - bigint
                    ,NULL  -- childcontrol - nvarchar(100)
                    ,N'SelectedDatePartProgram'  -- propname - nvarchar(100)
                    ,'10/31/'  -- value - nvarchar(max)
                    ,N'String'  -- valuetype - nvarchar(25)
		            );
        END;
    DECLARE @ResourceId INT;
    SET @ResourceId = (
                        SELECT  ResourceID
                        FROM    syResources
                        WHERE   Resource = 'All Inst - Test Scores - Detail'
                      );
    IF NOT EXISTS ( SELECT  *
                    FROM    syReports
                    WHERE   ResourceId = @ResourceId )
        BEGIN
            INSERT  INTO dbo.syReports
                    (
                     ReportId
                    ,ResourceId
                    ,ReportName
                    ,ReportDescription
                    ,RDLName
                    ,AssemblyFilePath
                    ,ReportClass
                    ,CreationMethod
                    ,WebServiceUrl
                    ,RecordLimit
                    ,AllowedExportTypes
                    ,ReportTabLayout
                    ,ShowPerformanceMsg
                    ,ShowFilterMode
		            )
            VALUES  (
                     NEWID()  -- ReportId - uniqueidentifier
                    ,@ResourceId  -- ResourceId - int
                    ,'TestScoresDetailReport'  -- ReportName - varchar(50)
                    ,'All Inst - Test Scores - Detail'  -- ReportDescription - varchar(500)
                    ,'IPEDS/WINTER/TestScoresDetail'  -- RDLName - varchar(200)
                    ,'~/Bin/Reporting.dll'  -- AssemblyFilePath - varchar(200)
                    ,'FAME.Advantage.Reporting.Logic.IPEDSStudentsMissingData'  -- ReportClass - varchar(200)
                    ,'BuildReport'  -- CreationMethod - varchar(200)
                    ,'futurefield'  -- WebServiceUrl - varchar(200)
                    ,400  -- RecordLimit - bigint
                    ,0  -- AllowedExportTypes - int
                    ,1  -- ReportTabLayout - int
                    ,0  -- ShowPerformanceMsg - bit
                    ,0  -- ShowFilterMode - bit
		            );
        END;
    DECLARE @ReportId UNIQUEIDENTIFIER;
    SET @ReportId = (
                      SELECT    ReportId
                      FROM      syReports
                      WHERE     ReportName = 'TestScoresDetailReport'
                    );
    IF NOT EXISTS ( SELECT  *
                    FROM    syReportTabs
                    WHERE   ReportId = @ReportId )
        BEGIN
            INSERT  INTO syReportTabs
            VALUES  ( @ReportId,'RadRptPage_Options','ParamPanelBarSetCustomOptions',@SetId,'ParamSetPanelBarControl.ascx',
                      'Custom Options Set for TestScoresReport','TestScoresDetailReportCustomOptionsSet','Options' );
		
        END;
END;
GO
DECLARE @ResourceId INT;
SET @ResourceId = (
                    SELECT  ResourceID
                    FROM    syResources
                    WHERE   Resource = 'All Inst - Test Scores - Detail'
                  );
UPDATE  syReports
SET     ReportClass = 'FAME.Advantage.Reporting.Logic.IPEDSStudentsMissingData'
WHERE   ResourceId = @ResourceId;
GO
BEGIN
    DECLARE @ParamSetId INT;
    DECLARE @SetId INT;
    IF NOT EXISTS ( SELECT  *
                    FROM    ParamSet
                    WHERE   SetDisplayName = 'TestScoresSummaryReportSet' )
        BEGIN
            INSERT  INTO dbo.ParamSet
                    (
                     SetName
                    ,SetDisplayName
                    ,SetDescription
                    ,SetType
		            )
            VALUES  (
                     N'TestScoresSummaryReportCustomOptionsSet'  -- SetName - nvarchar(50)
                    ,N'TestScoresSummaryReportSet'  -- SetDisplayName - nvarchar(50)
                    ,N'Custom Options for IPEDS Test Score Report'  -- SetDescription - nvarchar(1000)
                    ,N'Custom'  -- SetType - nvarchar(50)
		            );
        END;
    SET @SetId = (
                   SELECT TOP 1
                            SetId
                   FROM     ParamSet
                   WHERE    SetDisplayName = 'TestScoresSummaryReportSet'
                 );
    IF NOT EXISTS ( SELECT  *
                    FROM    ParamSection
                    WHERE   SectionName = 'ParamsForTestScoresSummaryReport' )
        BEGIN

            INSERT  INTO dbo.ParamSection
                    (
                     SectionName
                    ,SectionCaption
                    ,SetId
                    ,SectionSeq
                    ,SectionDescription
                    ,SectionType
		            )
            VALUES  (
                     N'ParamsForTestScoresSummaryReport'  -- SectionName - nvarchar(50)
                    ,N'Test Scores Report Parameters'  -- SectionCaption - nvarchar(100)
                    ,@SetId  -- SetId - bigint
                    ,1  -- SectionSeq - int
                    ,N'Choose Test Scores Report Parameters'  -- SectionDescription - nvarchar(500)
                    ,0  -- SectionType - int
		            );
        END;
    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.ParamItem
                    WHERE   Caption = 'TestScoresSummaryReportOptions' )
        BEGIN
            INSERT  INTO dbo.ParamItem
                    (
                     Caption
                    ,ControllerClass
                    ,valueprop
                    ,ReturnValueName
                    ,IsItemOverriden
		            )
            VALUES  (
                     N'TestScoresSummaryReportOptions'
                    ,N'ParamIPEDS.ascx'
                    ,0
                    ,'TestScoresSummaryReportOptions'
                    ,NULL
		            );
        END;
    DECLARE @ItemId INT;
    SET @ItemId = (
                    SELECT  ItemId
                    FROM    ParamItem
                    WHERE   Caption = 'TestScoresSummaryReportOptions'
                  );
    IF NOT EXISTS ( SELECT  *
                    FROM    ParamItemProp
                    WHERE   itemid = @ItemId )
        BEGIN
            INSERT  INTO dbo.ParamItemProp
                    (
                     itemid
                    ,childcontrol
                    ,propname
                    ,value
                    ,valuetype
		            )
            VALUES  (
                     @ItemId  -- itemid - bigint
                    ,NULL  -- childcontrol - nvarchar(100)
                    ,N'IPEDSControlType'  -- propname - nvarchar(100)
                    ,21  -- value - nvarchar(max)
                    ,N'Enum'  -- valuetype - nvarchar(25)
		            );
        END;
    DECLARE @SectionId INT
       ,@DetailId INT;
    SET @SectionId = (
                       SELECT TOP 1
                                SectionId
                       FROM     ParamSection
                       WHERE    SectionName = 'ParamsForTestScoresSummaryReport'
                     );
    SET @ItemId = (
                    SELECT TOP 1
                            ItemId
                    FROM    dbo.ParamItem
                    WHERE   Caption = 'TestScoresSummaryReportOptions'
                  );
    IF NOT EXISTS ( SELECT  *
                    FROM    ParamDetail
                    WHERE   ItemId = @ItemId
                            AND SectionId = @SectionId )
        BEGIN
            INSERT  INTO dbo.ParamDetail
                    (
                     SectionId
                    ,ItemId
                    ,CaptionOverride
                    ,ItemSeq
		            )
            VALUES  (
                     @SectionId  -- SectionId - bigint
                    ,@ItemId  -- ItemId - bigint
                    ,N'Report Options'  -- CaptionOverride - nvarchar(50)
                    ,1  -- ItemSeq - int
		            );
        END;
    SET @DetailId = (
                      SELECT TOP 1
                                DetailId
                      FROM      ParamDetail
                      WHERE     SectionId = @SectionId
                                AND ItemId = @ItemId
                    );

    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.ParamDetailProp
                    WHERE   detailid = @DetailId
                            AND propname = 'IPEDSControlType' )
        BEGIN
            INSERT  INTO dbo.ParamDetailProp
                    (
                     detailid
                    ,childcontrol
                    ,propname
                    ,value
                    ,valuetype
		            )
            VALUES  (
                     @DetailId  -- detailid - bigint
                    ,NULL  -- childcontrol - nvarchar(100)
                    ,N'IPEDSControlType'  -- propname - nvarchar(100)
                    ,21  -- value - nvarchar(max)
                    ,N'Enum'  -- valuetype - nvarchar(25)
		            );
        END;
    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.ParamDetailProp
                    WHERE   detailid = @DetailId
                            AND propname = 'SelectedDatePartProgram' )
        BEGIN
            INSERT  INTO dbo.ParamDetailProp
                    (
                     detailid
                    ,childcontrol
                    ,propname
                    ,value
                    ,valuetype
		            )
            VALUES  (
                     @DetailId  -- detailid - bigint
                    ,NULL  -- childcontrol - nvarchar(100)
                    ,N'SelectedDatePartProgram'  -- propname - nvarchar(100)
                    ,'10/31/'  -- value - nvarchar(max)
                    ,N'String'  -- valuetype - nvarchar(25)
		            );
        END;
END;
GO


DECLARE @ResourceId INT;
SET @ResourceId = (
                    SELECT  ResourceID
                    FROM    syResources
                    WHERE   Resource = 'All Inst - Test Scores - Summary'
                  );
IF NOT EXISTS ( SELECT  *
                FROM    syReports
                WHERE   ResourceId = @ResourceId )
    BEGIN
        INSERT  INTO dbo.syReports
                (
                 ReportId
                ,ResourceId
                ,ReportName
                ,ReportDescription
                ,RDLName
                ,AssemblyFilePath
                ,ReportClass
                ,CreationMethod
                ,WebServiceUrl
                ,RecordLimit
                ,AllowedExportTypes
                ,ReportTabLayout
                ,ShowPerformanceMsg
                ,ShowFilterMode
		        )
        VALUES  (
                 NEWID()  -- ReportId - uniqueidentifier
                ,@ResourceId  -- ResourceId - int
                ,'TestScoresSummary'  -- ReportName - varchar(50)
                ,'All Inst - Test Scores - Summary'  -- ReportDescription - varchar(500)
                ,'IPEDS/WINTER/TestScoresSummary1'  -- RDLName - varchar(200)
                ,'~/Bin/Reporting.dll'  -- AssemblyFilePath - varchar(200)
                ,'FAME.Advantage.Reporting.Logic.IPEDSStudentsMissingData'  -- ReportClass - varchar(200)
                ,'BuildReport'  -- CreationMethod - varchar(200)
                ,'futurefield'  -- WebServiceUrl - varchar(200)
                ,400  -- RecordLimit - bigint
                ,0  -- AllowedExportTypes - int
                ,1  -- ReportTabLayout - int
                ,0  -- ShowPerformanceMsg - bit
                ,0  -- ShowFilterMode - bit
		        );
    END;
DECLARE @ReportId UNIQUEIDENTIFIER
   ,@SetId INT; 
SET @ReportId = (
                  SELECT    ReportId
                  FROM      syReports
                  WHERE     ReportName = 'TestScoresSummary'
                );
SET @SetId = (
               SELECT TOP 1
                        SetId
               FROM     ParamSet
               WHERE    SetDisplayName = 'TestScoresSummaryReportSet'
             );
IF NOT EXISTS ( SELECT  *
                FROM    syReportTabs
                WHERE   ReportId = @ReportId )
    BEGIN
        INSERT  INTO syReportTabs
        VALUES  ( @ReportId,'RadRptPage_Options','ParamPanelBarSetCustomOptions',@SetId,'ParamSetPanelBarControl.ascx','Custom Options Set for TestScoresReport',
                  'TestScoresSummaryReportCustomOptionsSet','Options' );
		
    END;
	GO
DECLARE @ResourceId INT;
SET @ResourceId = (
                    SELECT  ResourceID
                    FROM    syResources
                    WHERE   Resource = 'All Inst - Test Scores - Summary'
                  );
UPDATE  syReports
SET     ReportClass = 'FAME.Advantage.Reporting.Logic.IPEDSStudentsMissingData'
WHERE   ResourceId = @ResourceId;
GO
BEGIN
    DECLARE @ResId INT = 837
       ,@MenuItemId INT
       ,@ParentId INT; 
	
    IF NOT EXISTS ( SELECT  *
                    FROM    syResources
                    WHERE   Resource = 'Pell Recipients & Stafford Sub Recipients without Pell' )
        BEGIN
            --SET @ResId = (
            --               SELECT   MAX(ResourceID) + 1
            --               FROM     syResources
            --             );
            INSERT  INTO syResources
            VALUES  ( @ResId,'Pell Recipients & Stafford Sub Recipients without Pell',5,'~/sy/ParamReport.aspx',NULL,5,GETDATE(),'sa',0,1,975,NULL,NULL );
        END;	
    IF NOT EXISTS ( SELECT  *
                    FROM    syMenuItems
                    WHERE   MenuName = 'Pell Recipients & Stafford Sub Recipients without Pell' )
        BEGIN
            --SET @ResId = (
            --               SELECT TOP 1
            --                        ResourceID
            --               FROM     syResources
            --               WHERE    Resource = 'Pell Recipients & Stafford Sub Recipients without Pell'
            --             );
            SET @ParentId = (
                              SELECT    MenuItemId
                              FROM      syMenuItems
                              WHERE     MenuName = 'Winter - Admissions'
                            );
            SET @MenuItemId = (
                                SELECT  MAX(MenuItemId) + 1
                                FROM    dbo.syMenuItems
                              );
            INSERT  INTO dbo.syMenuItems
                    (
                     MenuName
                    ,DisplayName
                    ,Url
                    ,MenuItemTypeId
                    ,ParentId
                    ,DisplayOrder
                    ,IsPopup
                    ,ModDate
                    ,ModUser
                    ,IsActive
                    ,ResourceId
                    ,HierarchyId
                    ,ModuleCode
                    ,MRUType
                    ,HideStatusBar
		            )
            VALUES  (
                     'Pell Recipients & Stafford Sub Recipients without Pell'  -- MenuName - varchar(250)
                    ,'Pell Recipients & Stafford Sub Recipients without Pell'  -- DisplayName - varchar(250)
                    ,N'/sy/ParamReport.aspx'  -- Url - nvarchar(250)
                    ,4  -- MenuItemTypeId - smallint
                    ,@ParentId  -- ParentId - int
                    ,500  -- DisplayOrder - int
                    ,0  -- IsPopup - bit
                    ,GETDATE()  -- ModDate - datetime
                    ,'sa'  -- ModUser - varchar(50)
                    ,1  -- IsActive - bit
                    ,@ResId  -- ResourceId - smallint
                    ,NULL  -- HierarchyId - uniqueidentifier
                    ,NULL  -- ModuleCode - varchar(5)
                    ,0  -- MRUType - int
                    ,NULL  -- HideStatusBar - bit
		            );
        END;
END;
GO
BEGIN
    DECLARE @ParamSetId INT;
    DECLARE @SetId INT;
    IF NOT EXISTS ( SELECT  *
                    FROM    ParamSet
                    WHERE   SetDisplayName = 'PellRecipientsandStaffordReportSet' )
        BEGIN
            INSERT  INTO dbo.ParamSet
                    (
                     SetName
                    ,SetDisplayName
                    ,SetDescription
                    ,SetType
		            )
            VALUES  (
                     N'PellRecipientsandStaffordReportCustomOptionsSet'  -- SetName - nvarchar(50)
                    ,N'PellRecipientsandStaffordReportSet'  -- SetDisplayName - nvarchar(50)
                    ,N'Custom Options for Pell Recipients & Stafford Sub Recipients without Pell'  -- SetDescription - nvarchar(1000)
                    ,N'Custom'  -- SetType - nvarchar(50)
		            );
        END;
    SET @SetId = (
                   SELECT TOP 1
                            SetId
                   FROM     ParamSet
                   WHERE    SetDisplayName = 'PellRecipientsandStaffordReportSet'
                 );
    IF NOT EXISTS ( SELECT  *
                    FROM    ParamSection
                    WHERE   SectionName = 'ParamsForPellRecipientsandStaffordReport' )
        BEGIN

            INSERT  INTO dbo.ParamSection
                    (
                     SectionName
                    ,SectionCaption
                    ,SetId
                    ,SectionSeq
                    ,SectionDescription
                    ,SectionType
		            )
            VALUES  (
                     N'ParamsForPellRecipientsandStaffordReport'  -- SectionName - nvarchar(50)
                    ,N'Pell Recipients & Stafford Sub Recipients without Pell Report Parameters'  -- SectionCaption - nvarchar(100)
                    ,@SetId  -- SetId - bigint
                    ,1  -- SectionSeq - int
                    ,N'Choose Pell Recipients & Stafford Sub Recipients without Pell Report Parameters'  -- SectionDescription - nvarchar(500)
                    ,0  -- SectionType - int
		            );
        END;
    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.ParamItem
                    WHERE   Caption = 'PellRecipientsandStaffordReportOptions' )
        BEGIN
            INSERT  INTO dbo.ParamItem
                    (
                     Caption
                    ,ControllerClass
                    ,valueprop
                    ,ReturnValueName
                    ,IsItemOverriden
		            )
            VALUES  (
                     N'PellRecipientsandStaffordReportOptions'
                    ,N'ParamIPEDS.ascx'
                    ,0
                    ,'PellRecipientsandStaffordReportOptions'
                    ,NULL
		            );
        END;
    DECLARE @ItemId INT;
    SET @ItemId = (
                    SELECT  ItemId
                    FROM    ParamItem
                    WHERE   Caption = 'PellRecipientsandStaffordReportOptions'
                  );
    IF NOT EXISTS ( SELECT  *
                    FROM    ParamItemProp
                    WHERE   itemid = @ItemId )
        BEGIN
            INSERT  INTO dbo.ParamItemProp
                    (
                     itemid
                    ,childcontrol
                    ,propname
                    ,value
                    ,valuetype
		            )
            VALUES  (
                     @ItemId  -- itemid - bigint
                    ,NULL  -- childcontrol - nvarchar(100)
                    ,N'IPEDSControlType'  -- propname - nvarchar(100)
                    ,21  -- value - nvarchar(max)
                    ,N'Enum'  -- valuetype - nvarchar(25)
		            );
        END;
    DECLARE @SectionId INT
       ,@DetailId INT;
    SET @SectionId = (
                       SELECT TOP 1
                                SectionId
                       FROM     ParamSection
                       WHERE    SectionName = 'ParamsForPellRecipientsandStaffordReport'
                     );
    SET @ItemId = (
                    SELECT TOP 1
                            ItemId
                    FROM    dbo.ParamItem
                    WHERE   Caption = 'PellRecipientsandStaffordReportOptions'
                  );
    IF NOT EXISTS ( SELECT  *
                    FROM    ParamDetail
                    WHERE   ItemId = @ItemId
                            AND SectionId = @SectionId )
        BEGIN
            INSERT  INTO dbo.ParamDetail
                    (
                     SectionId
                    ,ItemId
                    ,CaptionOverride
                    ,ItemSeq
		            )
            VALUES  (
                     @SectionId  -- SectionId - bigint
                    ,@ItemId  -- ItemId - bigint
                    ,N'Report Options'  -- CaptionOverride - nvarchar(50)
                    ,1  -- ItemSeq - int
		            );
        END;
    SET @DetailId = (
                      SELECT TOP 1
                                DetailId
                      FROM      ParamDetail
                      WHERE     SectionId = @SectionId
                                AND ItemId = @ItemId
                    );

    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.ParamDetailProp
                    WHERE   detailid = @DetailId
                            AND propname = 'IPEDSControlType' )
        BEGIN
            INSERT  INTO dbo.ParamDetailProp
                    (
                     detailid
                    ,childcontrol
                    ,propname
                    ,value
                    ,valuetype
		            )
            VALUES  (
                     @DetailId  -- detailid - bigint
                    ,NULL  -- childcontrol - nvarchar(100)
                    ,N'IPEDSControlType'  -- propname - nvarchar(100)
                    ,21  -- value - nvarchar(max)
                    ,N'Enum'  -- valuetype - nvarchar(25)
		            );
        END;
    IF NOT EXISTS ( SELECT  *
                    FROM    dbo.ParamDetailProp
                    WHERE   detailid = @DetailId
                            AND propname = 'SelectedDatePartProgram' )
        BEGIN
            INSERT  INTO dbo.ParamDetailProp
                    (
                     detailid
                    ,childcontrol
                    ,propname
                    ,value
                    ,valuetype
		            )
            VALUES  (
                     @DetailId  -- detailid - bigint
                    ,NULL  -- childcontrol - nvarchar(100)
                    ,N'SelectedDatePartProgram'  -- propname - nvarchar(100)
                    ,'10/31/'  -- value - nvarchar(max)
                    ,N'String'  -- valuetype - nvarchar(25)
		            );
        END;
    DECLARE @ResourceId INT;
    SET @ResourceId = (
                        SELECT  ResourceID
                        FROM    syResources
                        WHERE   Resource = 'Pell Recipients & Stafford Sub Recipients without Pell'
                      );
    IF NOT EXISTS ( SELECT  *
                    FROM    syReports
                    WHERE   ResourceId = @ResourceId )
        BEGIN
            INSERT  INTO dbo.syReports
                    (
                     ReportId
                    ,ResourceId
                    ,ReportName
                    ,ReportDescription
                    ,RDLName
                    ,AssemblyFilePath
                    ,ReportClass
                    ,CreationMethod
                    ,WebServiceUrl
                    ,RecordLimit
                    ,AllowedExportTypes
                    ,ReportTabLayout
                    ,ShowPerformanceMsg
                    ,ShowFilterMode
		            )
            VALUES  (
                     NEWID()  -- ReportId - uniqueidentifier
                    ,@ResourceId  -- ResourceId - int
                    ,'PellRecipientsandStaffordDetailReport'  -- ReportName - varchar(50)
                    ,'Pell Recipients & Stafford Sub Recipients without Pell Detail Report'  -- ReportDescription - varchar(500)
                    ,'IPEDS/WINTER/Spring_GradRates_PellRecipients_DetailAndSummary'  -- RDLName - varchar(200)
                    ,'~/Bin/Reporting.dll'  -- AssemblyFilePath - varchar(200)
                    ,'FAME.Advantage.Reporting.Logic.IPEDSStudentsMissingData'  -- ReportClass - varchar(200)
                    ,'BuildReport'  -- CreationMethod - varchar(200)
                    ,'futurefield'  -- WebServiceUrl - varchar(200)
                    ,400  -- RecordLimit - bigint
                    ,0  -- AllowedExportTypes - int
                    ,1  -- ReportTabLayout - int
                    ,0  -- ShowPerformanceMsg - bit
                    ,0  -- ShowFilterMode - bit
		            );
        END;
    DECLARE @ReportId UNIQUEIDENTIFIER;
    SET @ReportId = (
                      SELECT    ReportId
                      FROM      syReports
                      WHERE     ReportName = 'PellRecipientsandStaffordDetailReport'
                    );
    IF NOT EXISTS ( SELECT  *
                    FROM    syReportTabs
                    WHERE   ReportId = @ReportId )
        BEGIN
            INSERT  INTO syReportTabs
            VALUES  ( @ReportId,'RadRptPage_Options','ParamPanelBarSetCustomOptions',@SetId,'ParamSetPanelBarControl.ascx',
                      'Custom Options Set for PellRecipientsandStaffordReport','PellRecipientsandStaffordDetailReportCustomOptionsSet','Options' );
		
        END;
END;
GO
DECLARE @ResourceId INT;
SET @ResourceId = (
                    SELECT  ResourceID
                    FROM    syResources
                    WHERE   Resource = 'Pell Recipients & Stafford Sub Recipients without Pell'
                  );
UPDATE  syReports
SET     ReportClass = 'FAME.Advantage.Reporting.Logic.IPEDSStudentsMissingData'
       ,ReportDescription = 'Pell Recipients & Stafford Sub Recipients without Pell Detail Report'
WHERE   ResourceId = @ResourceId;
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   type = 'P'
                    AND name = 'usp_IPEDS_Spring_GradRatesPELLRecipients_Detail' )
    BEGIN
        DROP PROCEDURE usp_IPEDS_Spring_GradRatesPELLRecipients_Detail;
    END;
GO
IF EXISTS ( SELECT  *
            FROM    sysobjects
            WHERE   type = 'P'
                    AND name = 'usp_IPEDS_Spring_GradRatesStafford_Detail' )
    BEGIN
        DROP PROCEDURE usp_IPEDS_Spring_GradRatesStafford_Detail;
    END;
GO
CREATE PROC dbo.usp_IPEDS_Spring_GradRatesStafford_Detail
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@CohortYear VARCHAR(10) = NULL
   ,@CohortPossible VARCHAR(20) = NULL
   ,@OrderBy VARCHAR(100)
   ,@StartDate DATETIME
   ,@EndDate DATETIME
AS --SET @CampusId='3F5E839A-589A-4B2A-B258-35A1A8B3B819'
--SET @ProgId=NULL
--SET @CohortYear='2013'
--SET @CohortPossible= 'full year'
--SET @OrderBy = 'SSN'
--SET @StartDate='09/01/2010'
--SET @EndDate='08/31/2011'


    DECLARE @AcadInstFirstTimeStartDate DATETIME;
    DECLARE @ReturnValue VARCHAR(100)
       ,@Value VARCHAR(100);  
    DECLARE @SSN VARCHAR(10)
       ,@FirstName VARCHAR(100)
       ,@LastName VARCHAR(100)
       ,@StudentNumber VARCHAR(50)
       ,@TransferredOut INT
       ,@Exclusions INT;  
    DECLARE @CitizenShip_IPEDSValue INT
       ,@ProgramType_IPEDSValue INT
       ,@Gender_IPEDSValue INT
       ,@FullTimePartTime_IPEDSValue INT
       ,@StudentId UNIQUEIDENTIFIER;   

    DECLARE @StatusDate DATETIME;

    SET @StatusDate = '08/31/' + CONVERT(CHAR(4),YEAR(GETDATE()) - 1);
  
-- Check if School tracks grades by letter or numeric   
--SET @Value = (SELECT TOP 1 Value FROM dbo.syConfigAppSetValues WHERE SettingId=47)  
-- 2/07/2013 - updated the @value 
    SET @Value = (
                   SELECT   dbo.GetAppSettingValue(47,@CampusId)
                 );  
 
    IF @ProgId IS NOT NULL
        BEGIN  
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );  
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);  
        END;  
    ELSE
        BEGIN  
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );  
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);  
        END;  
 
-- Create a temp table to hold the final output of this stored proc  
    CREATE TABLE #GraduationRate
        (
         RowNumber UNIQUEIDENTIFIER
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,Gender VARCHAR(50)
        ,Race VARCHAR(50)
        ,RevisedCohort VARCHAR(10)
        ,Exclusions VARCHAR(10)
        ,CopmPrg100Less2Yrs VARCHAR(10)
        ,CopmPrg150Less2Yrs VARCHAR(10)
        ,StillInProg150Percent VARCHAR(10)
        ,TransOut VARCHAR(10)
        ,RevisedCohortCount INT
        ,ExclusionsCount INT
        ,CopmPrg100Less2YrsCount INT
        ,CopmPrg150Less2YrsCount INT
        ,TransOutCount INT
        ,StillInProg150PercentCount INT
        ,GenderSequence INT
        ,RaceSequence INT
        ,StudentId UNIQUEIDENTIFIER
        ,StuEnrollId UNIQUEIDENTIFIER
        ,StartDate DATETIME
        );   
     
/*********Get the list of students that will be shown in the report - Starts Here ******************/     
    CREATE TABLE #StudentsList
        (
         StudentId UNIQUEIDENTIFIER
        ,StuENrollId UNIQUEIDENTIFIER
        ,SSN VARCHAR(10)
        ,FirstName VARCHAR(100)
        ,LastName VARCHAR(100)
        ,MiddleName VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,TransferredOut INT
        ,Exclusions INT
        ,CitizenShip_IPEDSValue INT
        ,ProgramType_IPEDSValue INT
        ,Gender_IPEDSValue INT
        ,FullTimePartTime_IPEDSValue INT
        ,GenderId UNIQUEIDENTIFIER
        ,RaceId UNIQUEIDENTIFIER
        ,CitizenId UNIQUEIDENTIFIER
        ,GenderDescription VARCHAR(50)
        ,RaceDescription VARCHAR(50)
        ,StudentGraduatedStatusCount INT
        ,ClockHourProgramCount INT
        );  

    CREATE TABLE #StudentsListPELL
        (
         StudentId UNIQUEIDENTIFIER
        ,StuENrollId UNIQUEIDENTIFIER
        ,SSN VARCHAR(10)
        ,FirstName VARCHAR(100)
        ,LastName VARCHAR(100)
        ,MiddleName VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,TransferredOut INT
        ,Exclusions INT
        ,CitizenShip_IPEDSValue INT
        ,ProgramType_IPEDSValue INT
        ,Gender_IPEDSValue INT
        ,FullTimePartTime_IPEDSValue INT
        ,GenderId UNIQUEIDENTIFIER
        ,RaceId UNIQUEIDENTIFIER
        ,CitizenId UNIQUEIDENTIFIER
        ,GenderDescription VARCHAR(50)
        ,RaceDescription VARCHAR(50)
        ,StudentGraduatedStatusCount INT
        ,ClockHourProgramCount INT
        );  
  
-- Get the list of FullTime, FirstTime, UnderGraduate Students  
-- Exclude students who are Transferred in to the institution 
    IF LOWER(@CohortPossible) = 'fall'
        BEGIN 
            SET @AcadInstFirstTimeStartDate = DATEADD(YEAR,-1,@EndDate);
            INSERT  INTO #StudentsListPELL
                    SELECT DISTINCT
                            t1.StudentId
                           ,t2.StuEnrollId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StuEnrollId = t2.StuEnrollId
                                        AND SQ3.SysStatusId = 19
                                        AND --SQ1.TransferDate<=@EndDate AND 
                                        SQ1.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                        StuENrollId
                                                                 FROM   arTrackTransfer )
                                        AND SQ1.DateDetermined <= @StatusDate
                            ) AS TransferredOut
                           ,(  
							 -- Check if the student was either dropped and if the drop reason is either  
							 -- deceased, active duty, foreign aid service, church mission  
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped  
                                                    AND SQ1.DateDetermined <= @StatusDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN (
                                               SELECT TOP 1
                                                        EthCodeId
                                               FROM     adEthCodes
                                               WHERE    EthCodeDescrip = 'Race/ethnicity unknown'
                                             )
                                 ELSE t1.Race
                            END AS Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN 'Race/ethnicity unknown'
                                 ELSE (
                                        SELECT DISTINCT
                                                AgencyDescrip
                                        FROM    syRptAgencyFldValues
                                        WHERE   RptAgencyFldValId = t4.IPEDSValue
                                      )
                            END AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StuEnrollId = t2.StuEnrollId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                                        AND ExpGradDate <= @StatusDate
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StuEnrollId = t2.StuEnrollId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students  
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                    INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                    INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61  -- Full Time 
                            AND t12.IPEDSValue = 11  -- First Time  
                            AND t9.IPEDSValue = 58   -- Under Graduate
                            AND AFS.AdvFundSourceId = 2 -- DL Sub
                            AND AT.AwardTypeId = 1 --Loan
                            AND
                        --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                            (
                              t2.StartDate > @AcadInstFirstTimeStartDate
                              AND t2.StartDate <= @EndDate
                            )
                            -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
                            AND t2.StuEnrollId NOT IN ( SELECT  t1.StuEnrollId
                                                        FROM    arStuEnrollments t1
                                                               ,syStatusCodes t2
                                                        WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                                AND StartDate <= @EndDate
                                                                AND -- Student started before the end date range
                                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                                AND (
                                                                      t1.DateDetermined < @EndDate
                                                                      OR ExpGradDate < @EndDate
                                                                      OR LDA < @EndDate
                                                                    ) )  
                        -- If Student is enrolled in only one program version and if that program version   
                       -- happens to be a continuing ed program exclude the student  
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )  
                        -- Exclude students who were Transferred in to your institution   
                        -- This was used in FALL Part B report and we can reuse it here  
                            AND t2.StuEnrollId NOT IN (
                            SELECT DISTINCT
                                    SQ1.StuEnrollId
                            FROM    arStuEnrollments SQ1
                                   ,adDegCertSeeking SQ2
                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                    AND   
                                    -- To be considered for TransferIn, Student should be a First-Time Student  
                               --SQ1.LeadId IS NOT NULL AND 
                                    SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                    AND SQ2.IPEDSValue = 11
                                    AND (
                                          SQ1.TransferHours > 0
                                          OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                    THEN (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    StuENrollId = SQ1.StuEnrollId
                                                                                    AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                            FROM    arGradeSystemDetails
                                                                                                            WHERE   IsTransferGrade = 1 )
                                                                         )
                                                                    ELSE (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    IsTransferred = 1
                                                                                    AND StuENrollId = SQ1.StuEnrollId
                                                                         )
                                                               END
                                        )
                                    AND SQ1.StartDate < @EndDate
                                    AND NOT EXISTS ( SELECT StuENrollId
                                                     FROM   arStuEnrollments
                                                     WHERE  StudentId = SQ1.StudentId
                                                            AND StartDate < SQ1.StartDate ) )
                            AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                  FROM      arStuEnrollments t2
                                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                  LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                  LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                            AND (
                                                                  @ProgId IS NULL
                                                                  OR t8.ProgId IN ( SELECT  Val
                                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                )
                                                            AND t10.IPEDSValue = 61
                                                            AND -- Full Time  
                                                            t12.IPEDSValue = 11
                                                            AND -- First Time  
                                                            t9.IPEDSValue = 58
                                                            AND -- Under Graduate  
                                                            t2.StudentId = t1.StudentId
                                                            AND
							 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                                                            (
                                                              t2.StartDate > @AcadInstFirstTimeStartDate
                                                              AND t2.StartDate <= @EndDate
                                                            ) )
							-- If two enrollments fall on same start date pick any one 
                            AND t2.StuEnrollId IN (
                            SELECT TOP 1
                                    StuENrollId
                            FROM    arStuEnrollments t2
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t10.IPEDSValue = 61
                                    AND -- Full Time  
                                    t12.IPEDSValue = 11
                                    AND -- First Time  
                                    t9.IPEDSValue = 58
                                    AND -- Under Graduate  
                                    t2.StudentId = t1.StudentId
                                    AND 
							 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                                    (
                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                      AND t2.StartDate <= @EndDate
                                    )
                                    AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                          FROM      arStuEnrollments t2
                                                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                          LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                          WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                    AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN ( SELECT  Val
                                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                        )
                                                                    AND t10.IPEDSValue = 61
                                                                    AND -- Full Time  
                                                                    t12.IPEDSValue = 11
                                                                    AND -- First Time  
                                                                    t9.IPEDSValue = 58
                                                                    AND -- Under Graduate  
                                                                    t2.StudentId = t1.StudentId
                                                                    AND
								 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                                                                    (
                                                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                                                      AND t2.StartDate <= @EndDate
                                                                    ) ) );
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t2.StuEnrollId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StuEnrollId = t2.StuEnrollId
                                        AND SQ3.SysStatusId = 19
                                        AND --SQ1.TransferDate<=@EndDate AND 
                                        SQ1.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                        StuENrollId
                                                                 FROM   arTrackTransfer )
                                        AND SQ1.DateDetermined <= @StatusDate
                            ) AS TransferredOut
                           ,(  
							 -- Check if the student was either dropped and if the drop reason is either  
							 -- deceased, active duty, foreign aid service, church mission  
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped  
                                                    AND SQ1.DateDetermined <= @StatusDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN (
                                               SELECT TOP 1
                                                        EthCodeId
                                               FROM     adEthCodes
                                               WHERE    EthCodeDescrip = 'Race/ethnicity unknown'
                                             )
                                 ELSE t1.Race
                            END AS Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN 'Race/ethnicity unknown'
                                 ELSE (
                                        SELECT DISTINCT
                                                AgencyDescrip
                                        FROM    syRptAgencyFldValues
                                        WHERE   RptAgencyFldValId = t4.IPEDSValue
                                      )
                            END AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StuEnrollId = t2.StuEnrollId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                                        AND ExpGradDate <= @StatusDate
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StuEnrollId = t2.StuEnrollId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students  
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                    INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                    INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND t2.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                StuENrollId
                                                        FROM    #StudentsListPELL )
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61  -- Full Time 
                            AND t12.IPEDSValue = 11  -- First Time  
                            AND t9.IPEDSValue = 58   -- Under Graduate
                            AND AFS.AdvFundSourceId = 7 -- DL Sub
                            AND AT.AwardTypeId = 2 --Loan
                            AND
                        --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                            (
                              t2.StartDate > @AcadInstFirstTimeStartDate
                              AND t2.StartDate <= @EndDate
                            )
                            -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
                            AND t2.StuEnrollId NOT IN ( SELECT  t1.StuEnrollId
                                                        FROM    arStuEnrollments t1
                                                               ,syStatusCodes t2
                                                        WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                                AND StartDate <= @EndDate
                                                                AND -- Student started before the end date range
                                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                                AND (
                                                                      t1.DateDetermined < @EndDate
                                                                      OR ExpGradDate < @EndDate
                                                                      OR LDA < @EndDate
                                                                    ) )  
                        -- If Student is enrolled in only one program version and if that program version   
                       -- happens to be a continuing ed program exclude the student  
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )  
                        -- Exclude students who were Transferred in to your institution   
                        -- This was used in FALL Part B report and we can reuse it here  
                            AND t2.StuEnrollId NOT IN (
                            SELECT DISTINCT
                                    SQ1.StuEnrollId
                            FROM    arStuEnrollments SQ1
                                   ,adDegCertSeeking SQ2
                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                    AND   
                                    -- To be considered for TransferIn, Student should be a First-Time Student  
                               --SQ1.LeadId IS NOT NULL AND 
                                    SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                    AND SQ2.IPEDSValue = 11
                                    AND (
                                          SQ1.TransferHours > 0
                                          OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                    THEN (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    StuENrollId = SQ1.StuEnrollId
                                                                                    AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                            FROM    arGradeSystemDetails
                                                                                                            WHERE   IsTransferGrade = 1 )
                                                                         )
                                                                    ELSE (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    IsTransferred = 1
                                                                                    AND StuENrollId = SQ1.StuEnrollId
                                                                         )
                                                               END
                                        )
                                    AND SQ1.StartDate < @EndDate
                                    AND NOT EXISTS ( SELECT StuENrollId
                                                     FROM   arStuEnrollments
                                                     WHERE  StudentId = SQ1.StudentId
                                                            AND StartDate < SQ1.StartDate ) )
                            AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                  FROM      arStuEnrollments t2
                                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                  LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                  LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                            AND (
                                                                  @ProgId IS NULL
                                                                  OR t8.ProgId IN ( SELECT  Val
                                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                )
                                                            AND t10.IPEDSValue = 61
                                                            AND -- Full Time  
                                                            t12.IPEDSValue = 11
                                                            AND -- First Time  
                                                            t9.IPEDSValue = 58
                                                            AND -- Under Graduate  
                                                            t2.StudentId = t1.StudentId
                                                            AND
							 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                                                            (
                                                              t2.StartDate > @AcadInstFirstTimeStartDate
                                                              AND t2.StartDate <= @EndDate
                                                            ) )
							-- If two enrollments fall on same start date pick any one 
                            AND t2.StuEnrollId IN (
                            SELECT TOP 1
                                    StuENrollId
                            FROM    arStuEnrollments t2
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t10.IPEDSValue = 61
                                    AND -- Full Time  
                                    t12.IPEDSValue = 11
                                    AND -- First Time  
                                    t9.IPEDSValue = 58
                                    AND -- Under Graduate  
                                    t2.StudentId = t1.StudentId
                                    AND 
							 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                                    (
                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                      AND t2.StartDate <= @EndDate
                                    )
                                    AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                          FROM      arStuEnrollments t2
                                                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                          LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                          WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                    AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN ( SELECT  Val
                                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                        )
                                                                    AND t10.IPEDSValue = 61
                                                                    AND -- Full Time  
                                                                    t12.IPEDSValue = 11
                                                                    AND -- First Time  
                                                                    t9.IPEDSValue = 58
                                                                    AND -- Under Graduate  
                                                                    t2.StudentId = t1.StudentId
                                                                    AND
								 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                                                                    (
                                                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                                                      AND t2.StartDate <= @EndDate
                                                                    ) ) );
        END;
    IF LOWER(@CohortPossible) = 'full'
        BEGIN
            INSERT  INTO #StudentsListPELL
                    SELECT DISTINCT
                            t1.StudentId
                           ,t2.StuEnrollId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StuEnrollId = t2.StuEnrollId
                                        AND SQ3.SysStatusId = 19
                                        AND --SQ1.TransferDate<=@EndDate AND 
                                        SQ1.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                        StuENrollId
                                                                 FROM   arTrackTransfer )
                                        AND SQ1.DateDetermined <= @StatusDate
                            ) AS TransferredOut
                           ,(  
     -- Check if the student was either dropped and if the drop reason is either  
     -- deceased, active duty, foreign aid service, church mission  
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped  
                                                    AND SQ1.DateDetermined <= @StatusDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN (
                                               SELECT TOP 1
                                                        EthCodeId
                                               FROM     adEthCodes
                                               WHERE    EthCodeDescrip = 'Race/ethnicity unknown'
                                             )
                                 ELSE t1.Race
                            END AS Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN 'Race/ethnicity unknown'
                                 ELSE (
                                        SELECT DISTINCT
                                                AgencyDescrip
                                        FROM    syRptAgencyFldValues
                                        WHERE   RptAgencyFldValId = t4.IPEDSValue
                                      )
                            END AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StuEnrollId = t2.StuEnrollId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                                        AND ExpGradDate <= @StatusDate
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StuEnrollId = t2.StuEnrollId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students  
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                    INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                    INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61 -- Full Time 
                            AND t12.IPEDSValue = 11 -- First Time
                            AND t9.IPEDSValue = 58 -- Under Graduate  
                            AND AFS.AdvFundSourceId = 2 -- PELL
                            AND AT.AwardTypeId = 1 --Grant
                            AND (
                                  t2.StartDate >= @StartDate
                                  AND t2.StartDate <= @EndDate
                                ) 
                            -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
                            AND t2.StuEnrollId NOT IN ( SELECT  t1.StuEnrollId
                                                        FROM    arStuEnrollments t1
                                                               ,syStatusCodes t2
                                                        WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                                AND StartDate <= @EndDate
                                                                AND -- Student started before the end date range
                                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                                AND (
                                                                      t1.DateDetermined < @StartDate
                                                                      OR ExpGradDate < @StartDate
                                                                      OR LDA < @StartDate
                                                                    ) ) 
                        -- Student Should Have Started Before the Report End Date  
                        -- If Student is enrolled in only one program version and 
                        --if that program version   
                       -- happens to be a continuing ed program exclude the student  
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )  
                        -- Exclude students who were Transferred in to your institution   
                        -- This was used in FALL Part B report and we can reuse it here  
                            AND t2.StuEnrollId NOT IN (
                            SELECT DISTINCT
                                    SQ1.StuEnrollId
                            FROM    arStuEnrollments SQ1
                                   ,adDegCertSeeking SQ2
                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                    AND   
                                    -- To be considered for TransferIn, Student should be a First-Time Student  
                                    --SQ1.LeadId IS NOT NULL AND 
                                    SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                    AND SQ2.IPEDSValue = 11
                                    AND (
                                          SQ1.TransferHours > 0
                                          OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                    THEN (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    StuENrollId = SQ1.StuEnrollId
                                                                                    AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                            FROM    arGradeSystemDetails
                                                                                                            WHERE   IsTransferGrade = 1 )
                                                                         )
                                                                    ELSE (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    IsTransferred = 1
                                                                                    AND StuENrollId = SQ1.StuEnrollId
                                                                         )
                                                               END
                                        )
                                    AND SQ1.StartDate < @EndDate
                                    AND NOT EXISTS ( SELECT StuENrollId
                                                     FROM   arStuEnrollments
                                                     WHERE  StudentId = SQ1.StudentId
                                                            AND StartDate < SQ1.StartDate ) )
                            AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                  FROM      arStuEnrollments t2
                                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                  LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                  LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                            AND (
                                                                  @ProgId IS NULL
                                                                  OR t8.ProgId IN ( SELECT  Val
                                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                )
                                                            AND t10.IPEDSValue = 61
                                                            AND -- Full Time  
                                                            t12.IPEDSValue = 11
                                                            AND -- First Time  
                                                            t9.IPEDSValue = 58
                                                            AND -- Under Graduate  
                                                            t2.StudentId = t1.StudentId
                                                            AND (
                                                                  t2.StartDate >= @StartDate
                                                                  AND t2.StartDate <= @EndDate
                                                                ) )
					 	-- If two enrollments fall on same start date pick any one 
                            AND t2.StuEnrollId IN (
                            SELECT TOP 1
                                    StuENrollId
                            FROM    arStuEnrollments t2
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t10.IPEDSValue = 61
                                    AND -- Full Time  
                                    t12.IPEDSValue = 11
                                    AND -- First Time  
                                    t9.IPEDSValue = 58
                                    AND -- Under Graduate  
                                    t2.StudentId = t1.StudentId
                                    AND (
                                          t2.StartDate >= @StartDate
                                          AND t2.StartDate <= @EndDate
                                        )
                                    AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                          FROM      arStuEnrollments t2
                                                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                          LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                          WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                    AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN ( SELECT  Val
                                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                        )
                                                                    AND t10.IPEDSValue = 61
                                                                    AND -- Full Time  
                                                                    t12.IPEDSValue = 11
                                                                    AND -- First Time  
                                                                    t9.IPEDSValue = 58
                                                                    AND -- Under Graduate  
                                                                    t2.StudentId = t1.StudentId
                                                                    AND (
                                                                          t2.StartDate >= @StartDate
                                                                          AND t2.StartDate <= @EndDate
                                                                        ) ) );
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t2.StuEnrollId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StuEnrollId = t2.StuEnrollId
                                        AND SQ3.SysStatusId = 19
                                        AND --SQ1.TransferDate<=@EndDate AND 
                                        SQ1.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                        StuENrollId
                                                                 FROM   arTrackTransfer )
                                        AND SQ1.DateDetermined <= @StatusDate
                            ) AS TransferredOut
                           ,(  
     -- Check if the student was either dropped and if the drop reason is either  
     -- deceased, active duty, foreign aid service, church mission  
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped  
                                                    AND SQ1.DateDetermined <= @StatusDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN (
                                               SELECT TOP 1
                                                        EthCodeId
                                               FROM     adEthCodes
                                               WHERE    EthCodeDescrip = 'Race/ethnicity unknown'
                                             )
                                 ELSE t1.Race
                            END AS Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN 'Race/ethnicity unknown'
                                 ELSE (
                                        SELECT DISTINCT
                                                AgencyDescrip
                                        FROM    syRptAgencyFldValues
                                        WHERE   RptAgencyFldValId = t4.IPEDSValue
                                      )
                            END AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StuEnrollId = t2.StuEnrollId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                                        AND ExpGradDate <= @StatusDate
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StuEnrollId = t2.StuEnrollId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students  
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                    INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                    INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND t2.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                StuENrollId
                                                        FROM    #StudentsListPELL )
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61 -- Full Time 
                            AND t12.IPEDSValue = 11 -- First Time
                            AND t9.IPEDSValue = 58 -- Under Graduate  
                            AND AFS.AdvFundSourceId = 7 -- DL SUB
                            AND AT.AwardTypeId = 2 --LOAN
                            AND (
                                  t2.StartDate >= @StartDate
                                  AND t2.StartDate <= @EndDate
                                ) 
                            -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
                            AND t2.StuEnrollId NOT IN ( SELECT  t1.StuEnrollId
                                                        FROM    arStuEnrollments t1
                                                               ,syStatusCodes t2
                                                        WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                                AND StartDate <= @EndDate
                                                                AND -- Student started before the end date range
                                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                                AND (
                                                                      t1.DateDetermined < @StartDate
                                                                      OR ExpGradDate < @StartDate
                                                                      OR LDA < @StartDate
                                                                    ) ) 
                        -- Student Should Have Started Before the Report End Date  
                        -- If Student is enrolled in only one program version and 
                        --if that program version   
                       -- happens to be a continuing ed program exclude the student  
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )  
                        -- Exclude students who were Transferred in to your institution   
                        -- This was used in FALL Part B report and we can reuse it here  
                            AND t2.StuEnrollId NOT IN (
                            SELECT DISTINCT
                                    SQ1.StuEnrollId
                            FROM    arStuEnrollments SQ1
                                   ,adDegCertSeeking SQ2
                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                    AND   
                                    -- To be considered for TransferIn, Student should be a First-Time Student  
                                    --SQ1.LeadId IS NOT NULL AND 
                                    SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                    AND SQ2.IPEDSValue = 11
                                    AND (
                                          SQ1.TransferHours > 0
                                          OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                    THEN (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    StuENrollId = SQ1.StuEnrollId
                                                                                    AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                            FROM    arGradeSystemDetails
                                                                                                            WHERE   IsTransferGrade = 1 )
                                                                         )
                                                                    ELSE (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    IsTransferred = 1
                                                                                    AND StuENrollId = SQ1.StuEnrollId
                                                                         )
                                                               END
                                        )
                                    AND SQ1.StartDate < @EndDate
                                    AND NOT EXISTS ( SELECT StuENrollId
                                                     FROM   arStuEnrollments
                                                     WHERE  StudentId = SQ1.StudentId
                                                            AND StartDate < SQ1.StartDate ) )
                            AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                  FROM      arStuEnrollments t2
                                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                  LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                  LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                            AND (
                                                                  @ProgId IS NULL
                                                                  OR t8.ProgId IN ( SELECT  Val
                                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                )
                                                            AND t10.IPEDSValue = 61
                                                            AND -- Full Time  
                                                            t12.IPEDSValue = 11
                                                            AND -- First Time  
                                                            t9.IPEDSValue = 58
                                                            AND -- Under Graduate  
                                                            t2.StudentId = t1.StudentId
                                                            AND (
                                                                  t2.StartDate >= @StartDate
                                                                  AND t2.StartDate <= @EndDate
                                                                ) )
					 	-- If two enrollments fall on same start date pick any one 
                            AND t2.StuEnrollId IN (
                            SELECT TOP 1
                                    StuENrollId
                            FROM    arStuEnrollments t2
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t10.IPEDSValue = 61
                                    AND -- Full Time  
                                    t12.IPEDSValue = 11
                                    AND -- First Time  
                                    t9.IPEDSValue = 58
                                    AND -- Under Graduate  
                                    t2.StudentId = t1.StudentId
                                    AND (
                                          t2.StartDate >= @StartDate
                                          AND t2.StartDate <= @EndDate
                                        )
                                    AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                          FROM      arStuEnrollments t2
                                                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                          LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                          WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                    AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN ( SELECT  Val
                                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                        )
                                                                    AND t10.IPEDSValue = 61
                                                                    AND -- Full Time  
                                                                    t12.IPEDSValue = 11
                                                                    AND -- First Time  
                                                                    t9.IPEDSValue = 58
                                                                    AND -- Under Graduate  
                                                                    t2.StudentId = t1.StudentId
                                                                    AND (
                                                                          t2.StartDate >= @StartDate
                                                                          AND t2.StartDate <= @EndDate
                                                                        ) ) );
        END;
/*********Get the list of students that will be shown in the report - Ends Here ******************/     



--SELECT * FROM #StudentsList  --WHERE lastname='Diognardi'

    INSERT  INTO #GraduationRate
            SELECT  NEWID() AS RowNumber
                   ,dbo.UDF_FormatSSN(SSN) AS SSN
                   ,StudentNumber
                   ,StudentName
                   ,GenderDescription
                   ,RaceDescription
                   ,CASE WHEN RevisedCohort >= 1 THEN 'X'
                         ELSE ''
                    END AS RevisedCohort
                   ,CASE WHEN Exclusions >= 1 THEN 'X'
                         ELSE ''
                    END AS Exclusions
                   ,CASE WHEN CopmPrg100Less2Yrs >= 1 THEN 'X'
                         ELSE ''
                    END AS CopmPrg100Less2Yrs
                   ,CASE WHEN CopmPrg150Less2Yrs >= 1 THEN 'X'
                         ELSE ''
                    END AS CopmPrg150Less2Yrs
                   ,CASE WHEN StillInProg150Percent >= 1 THEN 'X'
                         ELSE ''
                    END AS StillInProg150Percent
                   ,CASE WHEN TransferredOut >= 1 THEN 'X'
                         ELSE ''
                    END AS TransOut
                   ,CASE WHEN RevisedCohort >= 1 THEN 1
                         ELSE 0
                    END AS RevisedCohortCount
                   ,CASE WHEN Exclusions >= 1 THEN 1
                         ELSE 0
                    END AS ExclusionsCount
                   ,CASE WHEN CopmPrg100Less2Yrs >= 1 THEN 1
                         ELSE 0
                    END AS CopmPrg100Less2YrsCount
                   ,CASE WHEN CopmPrg150Less2Yrs >= 1 THEN 1
                         ELSE 0
                    END AS CopmPrg150Less2YrsCount
                   ,CASE WHEN TransferredOut >= 1 THEN 1
                         ELSE 0
                    END AS TransOutCount
                   ,CASE WHEN StillInProg150Percent >= 1 THEN 1
                         ELSE 0
                    END AS StillInProg150PercentCount
                   ,GenderSequence
                   ,RaceSequence
                   ,StudentId
                   ,StuEnrollId
                   ,(
                      SELECT TOP 1
                                StartDate
                      FROM      arStuEnrollments
                      WHERE     StuEnrollId = dt.StuEnrollId
                    ) AS StartDate
            FROM    (
                      -- If none of the students are mapped to non-citizen, then insert a row for 'Nonresident Alien'   
 -- Gender : Men and Women  
                      SELECT    NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t2.IPEDSValue
                                ) AS GenderDescription
                               ,'Nonresident Alien' AS RaceDescription
                               ,0 AS RevisedCohort
                               ,0 AS Exclusions
                               ,0 AS CopmPrg100Less2Yrs
                               ,0 AS CopmPrg150Less2Yrs
                               ,0 AS StillInProg150Percent
                               ,0 AS TransferredOut
                               ,t2.IPEDSSequence AS GenderSequence
                               ,1 AS RaceSequence
                               ,NULL AS StudentId
                               ,NULL AS StuEnrollId
                      FROM      (
                                  SELECT    *
                                  FROM      adGenders
                                  WHERE     IPEDSValue IN ( 30,31 )
                                ) t2
                      WHERE     (
                                  --Check if there are any men in the student list whose citizenship is set to non citizen  
                                  SELECT    COUNT(t1.FirstName)
                                  FROM      #StudentsList t1
                                  INNER JOIN adCitizenships t11 ON t1.CitizenId = t11.CitizenshipId
                                  WHERE     t11.IPEDSValue = 65
                                            AND t1.GenderId = t2.GenderId -- Non-Citizen  
                                ) = 0
                      UNION   
 -- Bring in Students whose Citizenship Type is set to non citizen   
 -- Gender : Men and Women  
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t4.IPEDSValue
                                ) AS GenderDescription
                               ,'Nonresident Alien' AS Race
                               ,1 AS RevisedCohort
                               ,t1.Exclusions
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount >= 1 THEN -- Is Student Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
                                                                CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
        -- Did the student graduate within 100% of the Program length in hours  
                                                                          CASE WHEN ( (
                                                                                        SELECT  ISNULL(SUM(SchedHours),0)
                                                                                        FROM    arStudentClockAttendance
                                                                                        WHERE   StuENrollId = t3.StuEnrollId
                                                                                      ) <= (
                                                                                             SELECT ( Hours )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                           ELSE CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
        -- Did the student graduate within 100% of the Weeks in Program in weeks 
        -- Convert to days for accurate results 
                                                                          CASE WHEN ( (
                                                                                        SELECT  DATEDIFF(DAY,t3.StartDate,t3.ExpGradDate)
                                                                                      ) <= (
                                                                                             SELECT ( Weeks * 7 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS CopmPrg100Less2Yrs
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount >= 1 THEN -- Is Student Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
                                                                CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
         --Did the student graduate within 150% (1.5) of the Program length in hours  
                                                                          CASE WHEN ( (
                                                                                        SELECT  ISNULL(SUM(SchedHours),0)
                                                                                        FROM    arStudentClockAttendance
                                                                                        WHERE   StuENrollId = t3.StuEnrollId
                                                                                      ) <= (
                                                                                             SELECT ( Hours * 1.5 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                           ELSE CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
        -- Did the student graduate within 150% (1.5) of the Weeks in Program in weeks  
                                                                          CASE WHEN ( (
                                                                                        SELECT  DATEDIFF(DAY,t3.StartDate,t3.ExpGradDate)
                                                                                      ) <= (
                                                                                             SELECT ( Weeks * 7 * 1.5 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS CopmPrg150Less2Yrs
                               ,  
   
   --We are no longer getting longer than 3 years. Now, we're getting enrollment greater than 150% time from 
   --start thru when program should have been completed. Revision 01/30/12
                                (
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount = 0
                                                      AND t3.StatusCodeId IN --Student should be in a InSchool Status
		( (
            SELECT DISTINCT
                    t1.StatusCodeId
            FROM    syStatusCodes t1
                   ,dbo.sySysStatus t2
            WHERE   t1.SysStatusId = t2.SysStatusId
                    AND t2.InSchool = 1
          )
                                                                          ) THEN -- Is Student Not Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
       -- Is the Program greater than 2 year and less than 4 years  
       --CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) >=3  
                                                                CASE WHEN (
                                                                            (
                                                                              SELECT    ISNULL(SUM(SchedHours),0)
                                                                              FROM      arStudentClockAttendance
                                                                              WHERE     StuENrollId = t3.StuEnrollId
                                                                            ) < (
                                                                                  SELECT    ( Hours * 1.5 )
                                                                                  FROM      arPrgVersions PVI
                                                                                  INNER JOIN arDegrees D ON PVI.DegreeId = D.DegreeId
                                                                                  WHERE     PVI.PrgVerId = t3.PrgVerId
                                                                                            AND t3.ExpGradDate <= @StatusDate
                                                                                )
                                                                            AND (
                                                                                  SELECT    COUNT(*)
                                                                                  FROM      --Check to see if last status is ACTIVE
                                                                                            (
                                                                                              SELECT TOP 1
                                                                                                        B.SysStatusId
                                                                                              FROM      dbo.syStudentStatusChanges A
                                                                                              LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
                                                                                              WHERE     A.StuEnrollId = t3.StuEnrollId
                                                                                                        AND A.ModDate <= @StatusDate
                                                                                              ORDER BY  A.ModDate DESC
                                                                                            ) B
                                                                                  WHERE     B.SysStatusId IN ( 7,9,20,11,10,22 )
                                                                                ) >= 1
                                                                          ) --Makes sure that they are still enrolled as of this date (@StatusDate)
                                                                          THEN 1
                                                                     ELSE 0
                                                                END
                                                           ELSE CASE WHEN (
                                                                            (
                                                                              SELECT    DATEDIFF(DAY,t3.StartDate,t3.ExpGradDate)
                                                                            ) < (
                                                                                  SELECT    ( Weeks * 7 * 1.5 )
                                                                                  FROM      arPrgVersions PVI
                                                                                  WHERE     PVI.PrgVerId = t3.PrgVerId
                                                                                            AND t3.ExpGradDate <= @StatusDate
                                                                                )
                                                                            AND (
                                                                                  SELECT    COUNT(*)
                                                                                  FROM      --Check to see if last status is ACTIVE
                                                                                            (
                                                                                              SELECT TOP 1
                                                                                                        B.SysStatusId
                                                                                              FROM      dbo.syStudentStatusChanges A
                                                                                              LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
                                                                                              WHERE     A.StuEnrollId = t3.StuEnrollId
                                                                                                        AND A.ModDate <= @StatusDate
                                                                                              ORDER BY  A.ModDate DESC
                                                                                            ) B
                                                                                  WHERE     B.SysStatusId IN ( 7,9,20,11,10,22 )
                                                                                ) >= 1
                                                                          ) --Makes sure that they are still enrolled as of this date (@StatusDate)
--((Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) >=3) AND
--((SELECT DATEDIFF(day,t3.StartDate,t3.ExpGradDate))<=(Select (Weeks*7) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId))  
                                                                          THEN 1
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS StillInProg150Percent
                               ,t1.TransferredOut
                               ,t4.IPEDSSequence AS GenderSequence
                               ,1 AS RaceSequence
                               ,t1.StudentId AS StudentId
                               ,t1.StuENrollId
                      FROM      (
                                  SELECT    *
                                  FROM      adGenders
                                  WHERE     IPEDSValue IN ( 30,31 )
                                ) t4
                      LEFT JOIN #StudentsList t1 ON t4.GenderId = t1.GenderId
                      INNER JOIN adCitizenships t11 ON t1.CitizenId = t11.CitizenshipId
                      INNER JOIN arStuEnrollments t3 ON t1.StuENrollId = t3.StuENrollId
                      WHERE     t11.IPEDSValue = 65 --Non-Citizen  
                      UNION  
 -- Get List of Races that is not tied to any students (Full Time, First Time, UnderGraduate Students)  
 -- Gender : Men  
                      SELECT    NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,'Men' AS GenderDescription
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = IPEDSValue
                                ) AS Race
                               ,0 AS RevisedCohort
                               ,0 AS Exclusions
                               ,0 AS CopmPrg100Less2Yrs
                               ,0 AS CopmPrg150Less2Yrs
                               ,0 AS StillInProg150Percent
                               ,0 AS TransOut
                               ,1 AS GenderSequence
                               ,IPEDSSequence AS RaceSequence
                               ,NULL AS StudentId
                               ,NULL AS StuEnrollId
                      FROM      adEthCodes
                      WHERE     EthCodeDescrip <> 'Nonresident Alien'
                                AND EthCodeId NOT IN ( SELECT DISTINCT
                                                                RaceId
                                                       FROM     #StudentsList t1
                                                       INNER JOIN adGenders t2 ON t1.GenderId = t2.GenderId
                                                                                  AND t2.IPEDSValue = 30 )
                      UNION  
 -- Get List of Races that is not tied to any students (Full Time, First Time, UnderGraduate Students)  
 -- Gender : Women  
                      SELECT    NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,'Women' AS GenderDescription
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = IPEDSValue
                                ) AS Race
                               ,0 AS RevisedCohort
                               ,0 AS Exclusions
                               ,0 AS CopmPrg100Less2Yrs
                               ,0 AS CopmPrg150Less2Yrs
                               ,0 AS StillInProg150Percent
                               ,0 AS TransOut
                               ,2 AS GenderSequence
                               ,IPEDSSequence AS RaceSequence
                               ,NULL AS StudentId
                               ,NULL AS StuEnrollId
                      FROM      adEthCodes
                      WHERE     EthCodeDescrip <> 'Nonresident Alien'
                                AND EthCodeId NOT IN ( SELECT DISTINCT
                                                                RaceId
                                                       FROM     #StudentsList t1
                                                       INNER JOIN adGenders t2 ON t1.GenderId = t2.GenderId
                                                                                  AND t2.IPEDSValue = 31 )
                      UNION  
  -- Get Students who belongs to a Race  
  -- Gender : Men and Women  
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,  
   --t1.GenderDescription AS GenderDescription,  
                                (
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t4.IPEDSValue
                                ) AS GenderDescription
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t2.IPEDSValue
                                ) AS RaceDescription
                               ,1 AS RevisedCohort
                               ,t1.Exclusions
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount >= 1 THEN -- Is Student Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
                                                                CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
        -- Did the student graduate within 100% of the Program length in hours  
                                                                          CASE WHEN ( (
                                                                                        SELECT  ISNULL(SUM(SchedHours),0)
                                                                                        FROM    arStudentClockAttendance
                                                                                        WHERE   StuENrollId = t3.StuEnrollId
                                                                                      ) <= (
                                                                                             SELECT ( Hours )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                           ELSE CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
        -- Did the student graduate within 100% of the Weeks in Program in weeks  
                                                                          CASE WHEN ( (
                                                                                        SELECT  DATEDIFF(DAY,t3.StartDate,t3.ExpGradDate)
                                                                                      ) <= (
                                                                                             SELECT ( Weeks * 7 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS CopmPrg100Less2Yrs
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount >= 1 THEN -- Is Student Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
                                                                CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
         --Did the student graduate within 150% (1.5) of the Program length in hours  
                                                                          CASE WHEN ( (
                                                                                        SELECT  ISNULL(SUM(SchedHours),0)
                                                                                        FROM    arStudentClockAttendance
                                                                                        WHERE   StuENrollId = t3.StuEnrollId
                                                                                      ) <= (
                                                                                             SELECT ( Hours * 1.5 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                           ELSE CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
        -- Did the student graduate within 150% (1.5) of the Weeks in Program in weeks  
                                                                          CASE WHEN ( (
                                                                                        SELECT  DATEDIFF(DAY,t3.StartDate,t3.ExpGradDate)
                                                                                      ) <= (
                                                                                             SELECT ( Weeks * 7 * 1.5 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS CopmPrg150Less2Yrs
                               , 
   
   --We are no longer getting longer than 3 years. Now, we're getting enrollment greater than 150% time from 
   --start thru when program should have been completed. Revision 01/30/12
                                (
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount = 0
                                                      AND t3.StatusCodeId IN --Student should be in a InSchool Status
		( (
            SELECT DISTINCT
                    t1.StatusCodeId
            FROM    syStatusCodes t1
                   ,dbo.sySysStatus t2
            WHERE   t1.SysStatusId = t2.SysStatusId
                    AND t2.InSchool = 1
          )
                                                                          ) THEN -- Is Student Not Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
       -- Is the Program greater than 2 year and less than 4 years  
       --CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) >=3  
                                                                CASE WHEN (
                                                                            (
                                                                              SELECT    ISNULL(SUM(SchedHours),0)
                                                                              FROM      arStudentClockAttendance
                                                                              WHERE     StuENrollId = t3.StuEnrollId
                                                                            ) < (
                                                                                  SELECT    ( Hours * 1.5 )
                                                                                  FROM      arPrgVersions PVI
                                                                                  INNER JOIN arDegrees D ON PVI.DegreeId = D.DegreeId
                                                                                  WHERE     PVI.PrgVerId = t3.PrgVerId
                                                                                            AND t3.ExpGradDate <= @StatusDate
                                                                                )
                                                                            AND (
                                                                                  SELECT    COUNT(*)
                                                                                  FROM      --Check to see if last status is ACTIVE
                                                                                            (
                                                                                              SELECT TOP 1
                                                                                                        B.SysStatusId
                                                                                              FROM      dbo.syStudentStatusChanges A
                                                                                              LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
                                                                                              WHERE     A.StuEnrollId = t3.StuEnrollId
                                                                                                        AND A.ModDate <= @StatusDate
                                                                                              ORDER BY  A.ModDate DESC
                                                                                            ) B
                                                                                  WHERE     B.SysStatusId IN ( 7,9,20,11,10,22 )
                                                                                ) >= 1
                                                                          ) --Makes sure that they are still enrolled as of this date (@StatusDate)
                                                                          THEN 1
                                                                     ELSE 0
                                                                END
                                                           ELSE CASE WHEN (
                                                                            (
                                                                              SELECT    DATEDIFF(DAY,t3.StartDate,t3.ExpGradDate)
                                                                            ) < (
                                                                                  SELECT    ( Weeks * 7 * 1.5 )
                                                                                  FROM      arPrgVersions PVI
                                                                                  WHERE     PVI.PrgVerId = t3.PrgVerId
                                                                                            AND t3.ExpGradDate <= @StatusDate
                                                                                )
                                                                            AND (
                                                                                  SELECT    COUNT(*)
                                                                                  FROM      --Check to see if last status is ACTIVE
                                                                                            (
                                                                                              SELECT TOP 1
                                                                                                        B.SysStatusId
                                                                                              FROM      dbo.syStudentStatusChanges A
                                                                                              LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
                                                                                              WHERE     A.StuEnrollId = t3.StuEnrollId
                                                                                                        AND A.ModDate <= @StatusDate
                                                                                              ORDER BY  A.ModDate DESC
                                                                                            ) B
                                                                                  WHERE     B.SysStatusId IN ( 7,9,20,11,10,22 )
                                                                                ) >= 1
                                                                          ) --Makes sure that they are still enrolled as of this date (@StatusDate)
--((Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) >=3) AND
--((SELECT DATEDIFF(day,t3.StartDate,t3.ExpGradDate))<=(Select (Weeks*7) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId))  
                                                                          THEN 1
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS StillInProg150Percent
                               ,t1.TransferredOut
                               ,t4.IPEDSSequence AS GenderSequence
                               ,t2.IPEDSSequence AS RaceSequence
                               ,t1.StudentId AS StudentId
                               ,t1.StuENrollId
                      FROM      (
                                  SELECT    *
                                  FROM      adGenders
                                  WHERE     IPEDSValue IN ( 30,31 )
                                ) t4
                      LEFT JOIN #StudentsList t1 ON t4.GenderId = t1.GenderId
                      LEFT JOIN adEthCodes t2 ON t2.EthCodeId = t1.RaceId
                      INNER JOIN arStuEnrollments t3 ON t1.StuENrollId = t3.StuENrollId 
   --t1.StudentId=t3.StudentId
                      WHERE     t2.EthCodeDescrip <> 'Nonresident Alien'
                                AND t1.CitizenShip_IPEDSValue <> 65
                    ) dt
            WHERE   StudentId IS NOT NULL 
--AND
--dt.StuEnrollId IN 
--(SELECT TOP 1 StuEnrollId FROM arStuEnrollments WHERE StudentId=dt.StudentId 
--AND StartDate=(SELECT MIN(StartDate) FROM arStuEnrollments WHERE StudentId=dt.StudentId))
ORDER BY            CASE WHEN @OrderBy = 'SSN' THEN dt.SSN
                    END
                   ,CASE WHEN @OrderBy = 'LastName' THEN dt.StudentName
                    END
                   ,CASE WHEN @OrderBy = 'StudentNumber' THEN CONVERT(INT,dt.StudentNumber)
                    END;  
  
 --SELECT * FROM #GraduationRate WHERE StudentId='BBC0A8FE-2642-409A-829A-77766AD4DCF2' 
--SELECT * FROM #GraduationRate WHERE StudentId='BBC0A8FE-2642-409A-829A-77766AD4DCF2' 
----(SELECT MIN(StartDate) FROM #GraduationRate WHERE StudentId='BBC0A8FE-2642-409A-829A-77766AD4DCF2')

--(SELECT TOP 1 t1.StuEnrollId FROM arStuEnrollments t1, #GraduationRate t2 
--WHERE t1.StudentId=t2.StudentId AND t2.StudentId='BBC0A8FE-2642-409A-829A-77766AD4DCF2' 
--AND t1.StartDate=(SELECT MIN(StartDate) FROM arStuEnrollments WHERE StudentId=t2.StudentId))
  
    CREATE TABLE #FinalStudentList
        (
         StudentId UNIQUEIDENTIFIER
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,Gender VARCHAR(50)
        ,Race VARCHAR(50)
        ,RevisedCohort VARCHAR(10)
        ,Exclusions VARCHAR(10)
        ,CopmPrg100Less2Yrs VARCHAR(10)
        ,CopmPrg150Less2Yrs VARCHAR(10)
        ,StillInProg150Percent VARCHAR(10)
        ,TransOut VARCHAR(10)
        ,RevisedCohortCount INT
        ,ExclusionsCount INT
        ,CopmPrg100Less2YrsCount INT
        ,CopmPrg150Less2YrsCount INT
        ,StillInProg150PercentCount INT
        ,TransOutCount INT
        );     

--INSERT INTO #FinalStudentList
--SELECT * FROM #GraduationRate 
--WHERE StuEnrollId IN 
--(SELECT TOP 1 StuEnrollId FROM arStuEnrollments WHERE StudentId=#GraduationRate.StudentId 
--AND StartDate=(SELECT MIN(StartDate) FROM arStuEnrollments WHERE StudentId=#GraduationRate.StudentId))

--INSERT INTO #FinalStudentList
--SELECT DISTINCT StudentId,SSN,StudentNumber,StudentName,Gender,Race,
--(CASE WHEN (SELECT COUNT(RevisedCohort) FROM #GraduationRate WHERE StudentId=t2.StudentId AND RevisedCohort='X')>=1 THEN 'X' else '' END) as RevisedCohort, 
--(CASE WHEN (SELECT COUNT(Exclusions) FROM #GraduationRate WHERE StudentId=t2.StudentId AND Exclusions='X')>=1 THEN 'X' else '' END) as Exclusions,
--(CASE WHEN (SELECT COUNT(CopmPrg100Less2Yrs) FROM #GraduationRate WHERE StudentId=t2.StudentId  AND CopmPrg100Less2Yrs='X')>=1 THEN 'X' else '' END) as CopmPrg100Less2Yrs,
--(CASE WHEN (SELECT COUNT(CopmPrg150Less2Yrs) FROM #GraduationRate WHERE StudentId=t2.StudentId AND CopmPrg150Less2Yrs='X')>=1 THEN 'X' else '' END) as CopmPrg150Less2Yrs,
--(CASE WHEN (SELECT COUNT(TransOut) FROM #GraduationRate WHERE StudentId=t2.StudentId AND TransOut='X')>=1 THEN 'X' else '' END) as TransOut,
--(CASE WHEN (SELECT COUNT(RevisedCohort) FROM #GraduationRate WHERE StudentId=t2.StudentId AND RevisedCohort='X')>=1 THEN 1 else 0 END) as RevisedCohortCount,
--(CASE WHEN (SELECT COUNT(Exclusions) FROM #GraduationRate WHERE StudentId=t2.StudentId AND Exclusions='X')>=1 THEN 1 else 0 END) as ExclusionsCount,
--(CASE WHEN (SELECT COUNT(CopmPrg100Less2Yrs) FROM #GraduationRate WHERE StudentId=t2.StudentId AND CopmPrg100Less2Yrs='X')>=1 THEN 1 else 0 END) as CopmPrg100Less2YrsCount, 
--(CASE WHEN (SELECT COUNT(CopmPrg150Less2Yrs) FROM #GraduationRate WHERE StudentId=t2.StudentId AND CopmPrg150Less2Yrs='X')>=1 THEN 1 else 0 END) as CopmPrg150Less2YrsCount, 
--(CASE WHEN (SELECT COUNT(TransOut) FROM #GraduationRate WHERE StudentId=t2.StudentId AND TransOut='X')>=1 THEN 1 else 0 END) as TransOutCount
--FROM #GraduationRate t2 Where StudentId is not NULL
    
    SELECT  StudentId
           ,SSN
           ,StudentNumber
           ,StudentName
           ,Gender
           ,Race
           ,RevisedCohort
           ,Exclusions
           ,CopmPrg100Less2Yrs
           ,CASE WHEN CopmPrg100Less2Yrs = 'X' THEN 'X'
                 ELSE CopmPrg150Less2Yrs
            END AS CopmPrg150Less2Yrs
           ,StillInProg150Percent
           ,TransOut
           ,RevisedCohortCount
           ,ExclusionsCount
           ,CopmPrg100Less2YrsCount
           ,CASE WHEN CopmPrg100Less2Yrs = 'X' THEN 1
                 ELSE CopmPrg150Less2YrsCount
            END AS CopmPrg150Less2YrsCount
           ,StillInProg150PercentCount
           ,TransOutCount
    FROM    #GraduationRate
--WHERE StuEnrollId IN 
--(SELECT TOP 1 StuEnrollId FROM arStuEnrollments WHERE StudentId=#GraduationRate.StudentId 
--AND StartDate=(SELECT MIN(StartDate) FROM arStuEnrollments WHERE StudentId=#GraduationRate.StudentId))
ORDER BY    CASE WHEN @OrderBy = 'SSN' THEN SSN
            END
           ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
            END
           ,CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
            END;  
  
    DROP TABLE #GraduationRate; 
    DROP TABLE  #FinalStudentList;
    DROP TABLE #StudentsList;  
GO
CREATE PROC dbo.usp_IPEDS_Spring_GradRatesPELLRecipients_Detail
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@CohortYear VARCHAR(10) = NULL
   ,@CohortPossible VARCHAR(20) = NULL
   ,@OrderBy VARCHAR(100)
   ,@StartDate DATETIME
   ,@EndDate DATETIME
AS --SET @CampusId='3F5E839A-589A-4B2A-B258-35A1A8B3B819'
--SET @ProgId=NULL
--SET @CohortYear='2013'
--SET @CohortPossible= 'full year'
--SET @OrderBy = 'SSN'
--SET @StartDate='09/01/2010'
--SET @EndDate='08/31/2011'


    DECLARE @AcadInstFirstTimeStartDate DATETIME;
    DECLARE @ReturnValue VARCHAR(100)
       ,@Value VARCHAR(100);  
    DECLARE @SSN VARCHAR(10)
       ,@FirstName VARCHAR(100)
       ,@LastName VARCHAR(100)
       ,@StudentNumber VARCHAR(50)
       ,@TransferredOut INT
       ,@Exclusions INT;  
    DECLARE @CitizenShip_IPEDSValue INT
       ,@ProgramType_IPEDSValue INT
       ,@Gender_IPEDSValue INT
       ,@FullTimePartTime_IPEDSValue INT
       ,@StudentId UNIQUEIDENTIFIER;   

    DECLARE @StatusDate DATETIME;

    SET @StatusDate = '08/31/' + CONVERT(CHAR(4),YEAR(GETDATE()) - 1);
  
-- Check if School tracks grades by letter or numeric   
--SET @Value = (SELECT TOP 1 Value FROM dbo.syConfigAppSetValues WHERE SettingId=47)  
-- 2/07/2013 - updated the @value 
    SET @Value = (
                   SELECT   dbo.GetAppSettingValue(47,@CampusId)
                 );  
 
    IF @ProgId IS NOT NULL
        BEGIN  
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );  
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);  
        END;  
    ELSE
        BEGIN  
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );  
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);  
        END;  
 
-- Create a temp table to hold the final output of this stored proc  
    CREATE TABLE #GraduationRate
        (
         RowNumber UNIQUEIDENTIFIER
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,Gender VARCHAR(50)
        ,Race VARCHAR(50)
        ,RevisedCohort VARCHAR(10)
        ,Exclusions VARCHAR(10)
        ,CopmPrg100Less2Yrs VARCHAR(10)
        ,CopmPrg150Less2Yrs VARCHAR(10)
        ,StillInProg150Percent VARCHAR(10)
        ,TransOut VARCHAR(10)
        ,RevisedCohortCount INT
        ,ExclusionsCount INT
        ,CopmPrg100Less2YrsCount INT
        ,CopmPrg150Less2YrsCount INT
        ,TransOutCount INT
        ,StillInProg150PercentCount INT
        ,GenderSequence INT
        ,RaceSequence INT
        ,StudentId UNIQUEIDENTIFIER
        ,StuEnrollId UNIQUEIDENTIFIER
        ,StartDate DATETIME
        );   
     
/*********Get the list of students that will be shown in the report - Starts Here ******************/     
    CREATE TABLE #StudentsList
        (
         StudentId UNIQUEIDENTIFIER
        ,StuENrollId UNIQUEIDENTIFIER
        ,SSN VARCHAR(10)
        ,FirstName VARCHAR(100)
        ,LastName VARCHAR(100)
        ,MiddleName VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,TransferredOut INT
        ,Exclusions INT
        ,CitizenShip_IPEDSValue INT
        ,ProgramType_IPEDSValue INT
        ,Gender_IPEDSValue INT
        ,FullTimePartTime_IPEDSValue INT
        ,GenderId UNIQUEIDENTIFIER
        ,RaceId UNIQUEIDENTIFIER
        ,CitizenId UNIQUEIDENTIFIER
        ,GenderDescription VARCHAR(50)
        ,RaceDescription VARCHAR(50)
        ,StudentGraduatedStatusCount INT
        ,ClockHourProgramCount INT
        );  
  
-- Get the list of FullTime, FirstTime, UnderGraduate Students  
-- Exclude students who are Transferred in to the institution 
    IF LOWER(@CohortPossible) = 'fall'
        BEGIN 
            SET @AcadInstFirstTimeStartDate = DATEADD(YEAR,-1,@EndDate);
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t2.StuEnrollId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StuEnrollId = t2.StuEnrollId
                                        AND SQ3.SysStatusId = 19
                                        AND --SQ1.TransferDate<=@EndDate AND 
                                        SQ1.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                        StuENrollId
                                                                 FROM   arTrackTransfer )
                                        AND SQ1.DateDetermined <= @StatusDate
                            ) AS TransferredOut
                           ,(  
							 -- Check if the student was either dropped and if the drop reason is either  
							 -- deceased, active duty, foreign aid service, church mission  
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped  
                                                    AND SQ1.DateDetermined <= @StatusDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN (
                                               SELECT TOP 1
                                                        EthCodeId
                                               FROM     adEthCodes
                                               WHERE    EthCodeDescrip = 'Race/ethnicity unknown'
                                             )
                                 ELSE t1.Race
                            END AS Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN 'Race/ethnicity unknown'
                                 ELSE (
                                        SELECT DISTINCT
                                                AgencyDescrip
                                        FROM    syRptAgencyFldValues
                                        WHERE   RptAgencyFldValId = t4.IPEDSValue
                                      )
                            END AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StuEnrollId = t2.StuEnrollId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                                        AND ExpGradDate <= @StatusDate
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StuEnrollId = t2.StuEnrollId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students  
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                    INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                    INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61  -- Full Time 
                            AND t12.IPEDSValue = 11  -- First Time  
                            AND t9.IPEDSValue = 58   -- Under Graduate
                            AND AFS.AdvFundSourceId = 2 -- PELL
                            AND AT.AwardTypeId = 1 --Grant
                            AND
                        --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                            (
                              t2.StartDate > @AcadInstFirstTimeStartDate
                              AND t2.StartDate <= @EndDate
                            )
                            -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
                            AND t2.StuEnrollId NOT IN ( SELECT  t1.StuEnrollId
                                                        FROM    arStuEnrollments t1
                                                               ,syStatusCodes t2
                                                        WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                                AND StartDate <= @EndDate
                                                                AND -- Student started before the end date range
                                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                                AND (
                                                                      t1.DateDetermined < @EndDate
                                                                      OR ExpGradDate < @EndDate
                                                                      OR LDA < @EndDate
                                                                    ) )  
                        -- If Student is enrolled in only one program version and if that program version   
                       -- happens to be a continuing ed program exclude the student  
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )  
                        -- Exclude students who were Transferred in to your institution   
                        -- This was used in FALL Part B report and we can reuse it here  
                            AND t2.StuEnrollId NOT IN (
                            SELECT DISTINCT
                                    SQ1.StuEnrollId
                            FROM    arStuEnrollments SQ1
                                   ,adDegCertSeeking SQ2
                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                    AND   
                                    -- To be considered for TransferIn, Student should be a First-Time Student  
                               --SQ1.LeadId IS NOT NULL AND 
                                    SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                    AND SQ2.IPEDSValue = 11
                                    AND (
                                          SQ1.TransferHours > 0
                                          OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                    THEN (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    StuENrollId = SQ1.StuEnrollId
                                                                                    AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                            FROM    arGradeSystemDetails
                                                                                                            WHERE   IsTransferGrade = 1 )
                                                                         )
                                                                    ELSE (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    IsTransferred = 1
                                                                                    AND StuENrollId = SQ1.StuEnrollId
                                                                         )
                                                               END
                                        )
                                    AND SQ1.StartDate < @EndDate
                                    AND NOT EXISTS ( SELECT StuENrollId
                                                     FROM   arStuEnrollments
                                                     WHERE  StudentId = SQ1.StudentId
                                                            AND StartDate < SQ1.StartDate ) )
                            AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                  FROM      arStuEnrollments t2
                                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                  LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                  LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                            AND (
                                                                  @ProgId IS NULL
                                                                  OR t8.ProgId IN ( SELECT  Val
                                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                )
                                                            AND t10.IPEDSValue = 61
                                                            AND -- Full Time  
                                                            t12.IPEDSValue = 11
                                                            AND -- First Time  
                                                            t9.IPEDSValue = 58
                                                            AND -- Under Graduate  
                                                            t2.StudentId = t1.StudentId
                                                            AND
							 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                                                            (
                                                              t2.StartDate > @AcadInstFirstTimeStartDate
                                                              AND t2.StartDate <= @EndDate
                                                            ) )
							-- If two enrollments fall on same start date pick any one 
                            AND t2.StuEnrollId IN (
                            SELECT TOP 1
                                    StuENrollId
                            FROM    arStuEnrollments t2
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t10.IPEDSValue = 61
                                    AND -- Full Time  
                                    t12.IPEDSValue = 11
                                    AND -- First Time  
                                    t9.IPEDSValue = 58
                                    AND -- Under Graduate  
                                    t2.StudentId = t1.StudentId
                                    AND 
							 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                                    (
                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                      AND t2.StartDate <= @EndDate
                                    )
                                    AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                          FROM      arStuEnrollments t2
                                                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                          LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                          WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                    AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN ( SELECT  Val
                                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                        )
                                                                    AND t10.IPEDSValue = 61
                                                                    AND -- Full Time  
                                                                    t12.IPEDSValue = 11
                                                                    AND -- First Time  
                                                                    t9.IPEDSValue = 58
                                                                    AND -- Under Graduate  
                                                                    t2.StudentId = t1.StudentId
                                                                    AND
								 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                                                                    (
                                                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                                                      AND t2.StartDate <= @EndDate
                                                                    ) ) );
        END;
    IF LOWER(@CohortPossible) = 'full'
        BEGIN
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t2.StuEnrollId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StuEnrollId = t2.StuEnrollId
                                        AND SQ3.SysStatusId = 19
                                        AND --SQ1.TransferDate<=@EndDate AND 
                                        SQ1.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                        StuENrollId
                                                                 FROM   arTrackTransfer )
                                        AND SQ1.DateDetermined <= @StatusDate
                            ) AS TransferredOut
                           ,(  
     -- Check if the student was either dropped and if the drop reason is either  
     -- deceased, active duty, foreign aid service, church mission  
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped  
                                                    AND SQ1.DateDetermined <= @StatusDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN (
                                               SELECT TOP 1
                                                        EthCodeId
                                               FROM     adEthCodes
                                               WHERE    EthCodeDescrip = 'Race/ethnicity unknown'
                                             )
                                 ELSE t1.Race
                            END AS Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN 'Race/ethnicity unknown'
                                 ELSE (
                                        SELECT DISTINCT
                                                AgencyDescrip
                                        FROM    syRptAgencyFldValues
                                        WHERE   RptAgencyFldValId = t4.IPEDSValue
                                      )
                            END AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StuEnrollId = t2.StuEnrollId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                                        AND ExpGradDate <= @StatusDate
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StuEnrollId = t2.StuEnrollId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students  
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                    INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                    INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61 -- Full Time 
                            AND t12.IPEDSValue = 11 -- First Time
                            AND t9.IPEDSValue = 58 -- Under Graduate  
                            AND AFS.AdvFundSourceId = 2 -- PELL
                            AND AT.AwardTypeId = 1 --Grant
                            AND (
                                  t2.StartDate >= @StartDate
                                  AND t2.StartDate <= @EndDate
                                ) 
                            -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
                            AND t2.StuEnrollId NOT IN ( SELECT  t1.StuEnrollId
                                                        FROM    arStuEnrollments t1
                                                               ,syStatusCodes t2
                                                        WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                                AND StartDate <= @EndDate
                                                                AND -- Student started before the end date range
                                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                                AND (
                                                                      t1.DateDetermined < @StartDate
                                                                      OR ExpGradDate < @StartDate
                                                                      OR LDA < @StartDate
                                                                    ) ) 
                        -- Student Should Have Started Before the Report End Date  
                        -- If Student is enrolled in only one program version and 
                        --if that program version   
                       -- happens to be a continuing ed program exclude the student  
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )  
                        -- Exclude students who were Transferred in to your institution   
                        -- This was used in FALL Part B report and we can reuse it here  
                            AND t2.StuEnrollId NOT IN (
                            SELECT DISTINCT
                                    SQ1.StuEnrollId
                            FROM    arStuEnrollments SQ1
                                   ,adDegCertSeeking SQ2
                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                    AND   
                                    -- To be considered for TransferIn, Student should be a First-Time Student  
                                    --SQ1.LeadId IS NOT NULL AND 
                                    SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                    AND SQ2.IPEDSValue = 11
                                    AND (
                                          SQ1.TransferHours > 0
                                          OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                    THEN (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    StuENrollId = SQ1.StuEnrollId
                                                                                    AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                            FROM    arGradeSystemDetails
                                                                                                            WHERE   IsTransferGrade = 1 )
                                                                         )
                                                                    ELSE (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    IsTransferred = 1
                                                                                    AND StuENrollId = SQ1.StuEnrollId
                                                                         )
                                                               END
                                        )
                                    AND SQ1.StartDate < @EndDate
                                    AND NOT EXISTS ( SELECT StuENrollId
                                                     FROM   arStuEnrollments
                                                     WHERE  StudentId = SQ1.StudentId
                                                            AND StartDate < SQ1.StartDate ) )
                            AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                  FROM      arStuEnrollments t2
                                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                  LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                  LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                            AND (
                                                                  @ProgId IS NULL
                                                                  OR t8.ProgId IN ( SELECT  Val
                                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                )
                                                            AND t10.IPEDSValue = 61
                                                            AND -- Full Time  
                                                            t12.IPEDSValue = 11
                                                            AND -- First Time  
                                                            t9.IPEDSValue = 58
                                                            AND -- Under Graduate  
                                                            t2.StudentId = t1.StudentId
                                                            AND (
                                                                  t2.StartDate >= @StartDate
                                                                  AND t2.StartDate <= @EndDate
                                                                ) )
					 	-- If two enrollments fall on same start date pick any one 
                            AND t2.StuEnrollId IN (
                            SELECT TOP 1
                                    StuENrollId
                            FROM    arStuEnrollments t2
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t10.IPEDSValue = 61
                                    AND -- Full Time  
                                    t12.IPEDSValue = 11
                                    AND -- First Time  
                                    t9.IPEDSValue = 58
                                    AND -- Under Graduate  
                                    t2.StudentId = t1.StudentId
                                    AND (
                                          t2.StartDate >= @StartDate
                                          AND t2.StartDate <= @EndDate
                                        )
                                    AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                          FROM      arStuEnrollments t2
                                                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                          LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                          WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                    AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN ( SELECT  Val
                                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                        )
                                                                    AND t10.IPEDSValue = 61
                                                                    AND -- Full Time  
                                                                    t12.IPEDSValue = 11
                                                                    AND -- First Time  
                                                                    t9.IPEDSValue = 58
                                                                    AND -- Under Graduate  
                                                                    t2.StudentId = t1.StudentId
                                                                    AND (
                                                                          t2.StartDate >= @StartDate
                                                                          AND t2.StartDate <= @EndDate
                                                                        ) ) );
        END;
/*********Get the list of students that will be shown in the report - Ends Here ******************/     



--SELECT * FROM #StudentsList  --WHERE lastname='Diognardi'

    INSERT  INTO #GraduationRate
            SELECT  NEWID() AS RowNumber
                   ,dbo.UDF_FormatSSN(SSN) AS SSN
                   ,StudentNumber
                   ,StudentName
                   ,GenderDescription
                   ,RaceDescription
                   ,CASE WHEN RevisedCohort >= 1 THEN 'X'
                         ELSE ''
                    END AS RevisedCohort
                   ,CASE WHEN Exclusions >= 1 THEN 'X'
                         ELSE ''
                    END AS Exclusions
                   ,CASE WHEN CopmPrg100Less2Yrs >= 1 THEN 'X'
                         ELSE ''
                    END AS CopmPrg100Less2Yrs
                   ,CASE WHEN CopmPrg150Less2Yrs >= 1 THEN 'X'
                         ELSE ''
                    END AS CopmPrg150Less2Yrs
                   ,CASE WHEN StillInProg150Percent >= 1 THEN 'X'
                         ELSE ''
                    END AS StillInProg150Percent
                   ,CASE WHEN TransferredOut >= 1 THEN 'X'
                         ELSE ''
                    END AS TransOut
                   ,CASE WHEN RevisedCohort >= 1 THEN 1
                         ELSE 0
                    END AS RevisedCohortCount
                   ,CASE WHEN Exclusions >= 1 THEN 1
                         ELSE 0
                    END AS ExclusionsCount
                   ,CASE WHEN CopmPrg100Less2Yrs >= 1 THEN 1
                         ELSE 0
                    END AS CopmPrg100Less2YrsCount
                   ,CASE WHEN CopmPrg150Less2Yrs >= 1 THEN 1
                         ELSE 0
                    END AS CopmPrg150Less2YrsCount
                   ,CASE WHEN TransferredOut >= 1 THEN 1
                         ELSE 0
                    END AS TransOutCount
                   ,CASE WHEN StillInProg150Percent >= 1 THEN 1
                         ELSE 0
                    END AS StillInProg150PercentCount
                   ,GenderSequence
                   ,RaceSequence
                   ,StudentId
                   ,StuEnrollId
                   ,(
                      SELECT TOP 1
                                StartDate
                      FROM      arStuEnrollments
                      WHERE     StuEnrollId = dt.StuEnrollId
                    ) AS StartDate
            FROM    (
                      -- If none of the students are mapped to non-citizen, then insert a row for 'Nonresident Alien'   
 -- Gender : Men and Women  
                      SELECT    NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t2.IPEDSValue
                                ) AS GenderDescription
                               ,'Nonresident Alien' AS RaceDescription
                               ,0 AS RevisedCohort
                               ,0 AS Exclusions
                               ,0 AS CopmPrg100Less2Yrs
                               ,0 AS CopmPrg150Less2Yrs
                               ,0 AS StillInProg150Percent
                               ,0 AS TransferredOut
                               ,t2.IPEDSSequence AS GenderSequence
                               ,1 AS RaceSequence
                               ,NULL AS StudentId
                               ,NULL AS StuEnrollId
                      FROM      (
                                  SELECT    *
                                  FROM      adGenders
                                  WHERE     IPEDSValue IN ( 30,31 )
                                ) t2
                      WHERE     (
                                  --Check if there are any men in the student list whose citizenship is set to non citizen  
                                  SELECT    COUNT(t1.FirstName)
                                  FROM      #StudentsList t1
                                  INNER JOIN adCitizenships t11 ON t1.CitizenId = t11.CitizenshipId
                                  WHERE     t11.IPEDSValue = 65
                                            AND t1.GenderId = t2.GenderId -- Non-Citizen  
                                ) = 0
                      UNION   
 -- Bring in Students whose Citizenship Type is set to non citizen   
 -- Gender : Men and Women  
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t4.IPEDSValue
                                ) AS GenderDescription
                               ,'Nonresident Alien' AS Race
                               ,1 AS RevisedCohort
                               ,t1.Exclusions
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount >= 1 THEN -- Is Student Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
                                                                CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
        -- Did the student graduate within 100% of the Program length in hours  
                                                                          CASE WHEN ( (
                                                                                        SELECT  ISNULL(SUM(SchedHours),0)
                                                                                        FROM    arStudentClockAttendance
                                                                                        WHERE   StuENrollId = t3.StuEnrollId
                                                                                      ) <= (
                                                                                             SELECT ( Hours )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                           ELSE CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
        -- Did the student graduate within 100% of the Weeks in Program in weeks 
        -- Convert to days for accurate results 
                                                                          CASE WHEN ( (
                                                                                        SELECT  DATEDIFF(DAY,t3.StartDate,t3.ExpGradDate)
                                                                                      ) <= (
                                                                                             SELECT ( Weeks * 7 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS CopmPrg100Less2Yrs
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount >= 1 THEN -- Is Student Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
                                                                CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
         --Did the student graduate within 150% (1.5) of the Program length in hours  
                                                                          CASE WHEN ( (
                                                                                        SELECT  ISNULL(SUM(SchedHours),0)
                                                                                        FROM    arStudentClockAttendance
                                                                                        WHERE   StuENrollId = t3.StuEnrollId
                                                                                      ) <= (
                                                                                             SELECT ( Hours * 1.5 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                           ELSE CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
        -- Did the student graduate within 150% (1.5) of the Weeks in Program in weeks  
                                                                          CASE WHEN ( (
                                                                                        SELECT  DATEDIFF(DAY,t3.StartDate,t3.ExpGradDate)
                                                                                      ) <= (
                                                                                             SELECT ( Weeks * 7 * 1.5 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS CopmPrg150Less2Yrs
                               ,  
   
   --We are no longer getting longer than 3 years. Now, we're getting enrollment greater than 150% time from 
   --start thru when program should have been completed. Revision 01/30/12
                                (
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount = 0
                                                      AND t3.StatusCodeId IN --Student should be in a InSchool Status
		( (
            SELECT DISTINCT
                    t1.StatusCodeId
            FROM    syStatusCodes t1
                   ,dbo.sySysStatus t2
            WHERE   t1.SysStatusId = t2.SysStatusId
                    AND t2.InSchool = 1
          )
                                                                          ) THEN -- Is Student Not Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
       -- Is the Program greater than 2 year and less than 4 years  
       --CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) >=3  
                                                                CASE WHEN (
                                                                            (
                                                                              SELECT    ISNULL(SUM(SchedHours),0)
                                                                              FROM      arStudentClockAttendance
                                                                              WHERE     StuENrollId = t3.StuEnrollId
                                                                            ) < (
                                                                                  SELECT    ( Hours * 1.5 )
                                                                                  FROM      arPrgVersions PVI
                                                                                  INNER JOIN arDegrees D ON PVI.DegreeId = D.DegreeId
                                                                                  WHERE     PVI.PrgVerId = t3.PrgVerId
                                                                                            AND t3.ExpGradDate <= @StatusDate
                                                                                )
                                                                            AND (
                                                                                  SELECT    COUNT(*)
                                                                                  FROM      --Check to see if last status is ACTIVE
                                                                                            (
                                                                                              SELECT TOP 1
                                                                                                        B.SysStatusId
                                                                                              FROM      dbo.syStudentStatusChanges A
                                                                                              LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
                                                                                              WHERE     A.StuEnrollId = t3.StuEnrollId
                                                                                                        AND A.ModDate <= @StatusDate
                                                                                              ORDER BY  A.ModDate DESC
                                                                                            ) B
                                                                                  WHERE     B.SysStatusId IN ( 7,9,20,11,10,22 )
                                                                                ) >= 1
                                                                          ) --Makes sure that they are still enrolled as of this date (@StatusDate)
                                                                          THEN 1
                                                                     ELSE 0
                                                                END
                                                           ELSE CASE WHEN (
                                                                            (
                                                                              SELECT    DATEDIFF(DAY,t3.StartDate,t3.ExpGradDate)
                                                                            ) < (
                                                                                  SELECT    ( Weeks * 7 * 1.5 )
                                                                                  FROM      arPrgVersions PVI
                                                                                  WHERE     PVI.PrgVerId = t3.PrgVerId
                                                                                            AND t3.ExpGradDate <= @StatusDate
                                                                                )
                                                                            AND (
                                                                                  SELECT    COUNT(*)
                                                                                  FROM      --Check to see if last status is ACTIVE
                                                                                            (
                                                                                              SELECT TOP 1
                                                                                                        B.SysStatusId
                                                                                              FROM      dbo.syStudentStatusChanges A
                                                                                              LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
                                                                                              WHERE     A.StuEnrollId = t3.StuEnrollId
                                                                                                        AND A.ModDate <= @StatusDate
                                                                                              ORDER BY  A.ModDate DESC
                                                                                            ) B
                                                                                  WHERE     B.SysStatusId IN ( 7,9,20,11,10,22 )
                                                                                ) >= 1
                                                                          ) --Makes sure that they are still enrolled as of this date (@StatusDate)
--((Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) >=3) AND
--((SELECT DATEDIFF(day,t3.StartDate,t3.ExpGradDate))<=(Select (Weeks*7) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId))  
                                                                          THEN 1
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS StillInProg150Percent
                               ,t1.TransferredOut
                               ,t4.IPEDSSequence AS GenderSequence
                               ,1 AS RaceSequence
                               ,t1.StudentId AS StudentId
                               ,t1.StuENrollId
                      FROM      (
                                  SELECT    *
                                  FROM      adGenders
                                  WHERE     IPEDSValue IN ( 30,31 )
                                ) t4
                      LEFT JOIN #StudentsList t1 ON t4.GenderId = t1.GenderId
                      INNER JOIN adCitizenships t11 ON t1.CitizenId = t11.CitizenshipId
                      INNER JOIN arStuEnrollments t3 ON t1.StuENrollId = t3.StuENrollId
                      WHERE     t11.IPEDSValue = 65 --Non-Citizen  
                      UNION  
 -- Get List of Races that is not tied to any students (Full Time, First Time, UnderGraduate Students)  
 -- Gender : Men  
                      SELECT    NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,'Men' AS GenderDescription
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = IPEDSValue
                                ) AS Race
                               ,0 AS RevisedCohort
                               ,0 AS Exclusions
                               ,0 AS CopmPrg100Less2Yrs
                               ,0 AS CopmPrg150Less2Yrs
                               ,0 AS StillInProg150Percent
                               ,0 AS TransOut
                               ,1 AS GenderSequence
                               ,IPEDSSequence AS RaceSequence
                               ,NULL AS StudentId
                               ,NULL AS StuEnrollId
                      FROM      adEthCodes
                      WHERE     EthCodeDescrip <> 'Nonresident Alien'
                                AND EthCodeId NOT IN ( SELECT DISTINCT
                                                                RaceId
                                                       FROM     #StudentsList t1
                                                       INNER JOIN adGenders t2 ON t1.GenderId = t2.GenderId
                                                                                  AND t2.IPEDSValue = 30 )
                      UNION  
 -- Get List of Races that is not tied to any students (Full Time, First Time, UnderGraduate Students)  
 -- Gender : Women  
                      SELECT    NULL AS SSN
                               ,NULL AS StudentNumber
                               ,NULL AS StudentName
                               ,'Women' AS GenderDescription
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = IPEDSValue
                                ) AS Race
                               ,0 AS RevisedCohort
                               ,0 AS Exclusions
                               ,0 AS CopmPrg100Less2Yrs
                               ,0 AS CopmPrg150Less2Yrs
                               ,0 AS StillInProg150Percent
                               ,0 AS TransOut
                               ,2 AS GenderSequence
                               ,IPEDSSequence AS RaceSequence
                               ,NULL AS StudentId
                               ,NULL AS StuEnrollId
                      FROM      adEthCodes
                      WHERE     EthCodeDescrip <> 'Nonresident Alien'
                                AND EthCodeId NOT IN ( SELECT DISTINCT
                                                                RaceId
                                                       FROM     #StudentsList t1
                                                       INNER JOIN adGenders t2 ON t1.GenderId = t2.GenderId
                                                                                  AND t2.IPEDSValue = 31 )
                      UNION  
  -- Get Students who belongs to a Race  
  -- Gender : Men and Women  
                      SELECT    t1.SSN
                               ,t1.StudentNumber
                               ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                               ,  
   --t1.GenderDescription AS GenderDescription,  
                                (
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t4.IPEDSValue
                                ) AS GenderDescription
                               ,(
                                  SELECT DISTINCT
                                            AgencyDescrip
                                  FROM      syRptAgencyFldValues
                                  WHERE     RptAgencyFldValId = t2.IPEDSValue
                                ) AS RaceDescription
                               ,1 AS RevisedCohort
                               ,t1.Exclusions
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount >= 1 THEN -- Is Student Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
                                                                CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
        -- Did the student graduate within 100% of the Program length in hours  
                                                                          CASE WHEN ( (
                                                                                        SELECT  ISNULL(SUM(SchedHours),0)
                                                                                        FROM    arStudentClockAttendance
                                                                                        WHERE   StuENrollId = t3.StuEnrollId
                                                                                      ) <= (
                                                                                             SELECT ( Hours )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                           ELSE CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
        -- Did the student graduate within 100% of the Weeks in Program in weeks  
                                                                          CASE WHEN ( (
                                                                                        SELECT  DATEDIFF(DAY,t3.StartDate,t3.ExpGradDate)
                                                                                      ) <= (
                                                                                             SELECT ( Weeks * 7 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS CopmPrg100Less2Yrs
                               ,(
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount >= 1 THEN -- Is Student Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
                                                                CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
         --Did the student graduate within 150% (1.5) of the Program length in hours  
                                                                          CASE WHEN ( (
                                                                                        SELECT  ISNULL(SUM(SchedHours),0)
                                                                                        FROM    arStudentClockAttendance
                                                                                        WHERE   StuENrollId = t3.StuEnrollId
                                                                                      ) <= (
                                                                                             SELECT ( Hours * 1.5 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                           ELSE CASE WHEN (
                                                                            SELECT  ( Weeks / 52 )
                                                                            FROM    arPrgVersions PVI
                                                                            WHERE   PVI.PrgVerId = t3.PrgVerId
                                                                          ) < 2 THEN -- Is the Program a 2 year program  
        -- Did the student graduate within 150% (1.5) of the Weeks in Program in weeks  
                                                                          CASE WHEN ( (
                                                                                        SELECT  DATEDIFF(DAY,t3.StartDate,t3.ExpGradDate)
                                                                                      ) <= (
                                                                                             SELECT ( Weeks * 7 * 1.5 )
                                                                                             FROM   arPrgVersions PVI
                                                                                             WHERE  PVI.PrgVerId = t3.PrgVerId
                                                                                           ) ) THEN 1
                                                                               ELSE 0
                                                                          END
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS CopmPrg150Less2Yrs
                               , 
   
   --We are no longer getting longer than 3 years. Now, we're getting enrollment greater than 150% time from 
   --start thru when program should have been completed. Revision 01/30/12
                                (
                                  SELECT    CASE WHEN t1.StudentGraduatedStatusCount = 0
                                                      AND t3.StatusCodeId IN --Student should be in a InSchool Status
		( (
            SELECT DISTINCT
                    t1.StatusCodeId
            FROM    syStatusCodes t1
                   ,dbo.sySysStatus t2
            WHERE   t1.SysStatusId = t2.SysStatusId
                    AND t2.InSchool = 1
          )
                                                                          ) THEN -- Is Student Not Graduated?  
                                                      CASE WHEN t1.ClockHourProgramCount >= 1 THEN -- Is the program a clock hour program?  
       -- Is the Program greater than 2 year and less than 4 years  
       --CASE WHEN (Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) >=3  
                                                                CASE WHEN (
                                                                            (
                                                                              SELECT    ISNULL(SUM(SchedHours),0)
                                                                              FROM      arStudentClockAttendance
                                                                              WHERE     StuENrollId = t3.StuEnrollId
                                                                            ) < (
                                                                                  SELECT    ( Hours * 1.5 )
                                                                                  FROM      arPrgVersions PVI
                                                                                  INNER JOIN arDegrees D ON PVI.DegreeId = D.DegreeId
                                                                                  WHERE     PVI.PrgVerId = t3.PrgVerId
                                                                                            AND t3.ExpGradDate <= @StatusDate
                                                                                )
                                                                            AND (
                                                                                  SELECT    COUNT(*)
                                                                                  FROM      --Check to see if last status is ACTIVE
                                                                                            (
                                                                                              SELECT TOP 1
                                                                                                        B.SysStatusId
                                                                                              FROM      dbo.syStudentStatusChanges A
                                                                                              LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
                                                                                              WHERE     A.StuEnrollId = t3.StuEnrollId
                                                                                                        AND A.ModDate <= @StatusDate
                                                                                              ORDER BY  A.ModDate DESC
                                                                                            ) B
                                                                                  WHERE     B.SysStatusId IN ( 7,9,20,11,10,22 )
                                                                                ) >= 1
                                                                          ) --Makes sure that they are still enrolled as of this date (@StatusDate)
                                                                          THEN 1
                                                                     ELSE 0
                                                                END
                                                           ELSE CASE WHEN (
                                                                            (
                                                                              SELECT    DATEDIFF(DAY,t3.StartDate,t3.ExpGradDate)
                                                                            ) < (
                                                                                  SELECT    ( Weeks * 7 * 1.5 )
                                                                                  FROM      arPrgVersions PVI
                                                                                  WHERE     PVI.PrgVerId = t3.PrgVerId
                                                                                            AND t3.ExpGradDate <= @StatusDate
                                                                                )
                                                                            AND (
                                                                                  SELECT    COUNT(*)
                                                                                  FROM      --Check to see if last status is ACTIVE
                                                                                            (
                                                                                              SELECT TOP 1
                                                                                                        B.SysStatusId
                                                                                              FROM      dbo.syStudentStatusChanges A
                                                                                              LEFT JOIN dbo.syStatusCodes B ON A.NewStatusId = B.StatusCodeId
                                                                                              WHERE     A.StuEnrollId = t3.StuEnrollId
                                                                                                        AND A.ModDate <= @StatusDate
                                                                                              ORDER BY  A.ModDate DESC
                                                                                            ) B
                                                                                  WHERE     B.SysStatusId IN ( 7,9,20,11,10,22 )
                                                                                ) >= 1
                                                                          ) --Makes sure that they are still enrolled as of this date (@StatusDate)
--((Select (Weeks/52) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId) >=3) AND
--((SELECT DATEDIFF(day,t3.StartDate,t3.ExpGradDate))<=(Select (Weeks*7) From arPrgVersions PVI Where PVI.PrgVerId=t3.PrgVerId))  
                                                                          THEN 1
                                                                     ELSE 0
                                                                END
                                                      END
                                                 ELSE 0
                                            END
                                ) AS StillInProg150Percent
                               ,t1.TransferredOut
                               ,t4.IPEDSSequence AS GenderSequence
                               ,t2.IPEDSSequence AS RaceSequence
                               ,t1.StudentId AS StudentId
                               ,t1.StuENrollId
                      FROM      (
                                  SELECT    *
                                  FROM      adGenders
                                  WHERE     IPEDSValue IN ( 30,31 )
                                ) t4
                      LEFT JOIN #StudentsList t1 ON t4.GenderId = t1.GenderId
                      LEFT JOIN adEthCodes t2 ON t2.EthCodeId = t1.RaceId
                      INNER JOIN arStuEnrollments t3 ON t1.StuENrollId = t3.StuENrollId 
   --t1.StudentId=t3.StudentId
                      WHERE     t2.EthCodeDescrip <> 'Nonresident Alien'
                                AND t1.CitizenShip_IPEDSValue <> 65
                    ) dt
            WHERE   StudentId IS NOT NULL 
--AND
--dt.StuEnrollId IN 
--(SELECT TOP 1 StuEnrollId FROM arStuEnrollments WHERE StudentId=dt.StudentId 
--AND StartDate=(SELECT MIN(StartDate) FROM arStuEnrollments WHERE StudentId=dt.StudentId))
ORDER BY            CASE WHEN @OrderBy = 'SSN' THEN dt.SSN
                    END
                   ,CASE WHEN @OrderBy = 'LastName' THEN dt.StudentName
                    END
                   ,CASE WHEN @OrderBy = 'StudentNumber' THEN CONVERT(INT,dt.StudentNumber)
                    END;  
  
 --SELECT * FROM #GraduationRate WHERE StudentId='BBC0A8FE-2642-409A-829A-77766AD4DCF2' 
--SELECT * FROM #GraduationRate WHERE StudentId='BBC0A8FE-2642-409A-829A-77766AD4DCF2' 
----(SELECT MIN(StartDate) FROM #GraduationRate WHERE StudentId='BBC0A8FE-2642-409A-829A-77766AD4DCF2')

--(SELECT TOP 1 t1.StuEnrollId FROM arStuEnrollments t1, #GraduationRate t2 
--WHERE t1.StudentId=t2.StudentId AND t2.StudentId='BBC0A8FE-2642-409A-829A-77766AD4DCF2' 
--AND t1.StartDate=(SELECT MIN(StartDate) FROM arStuEnrollments WHERE StudentId=t2.StudentId))
  
    CREATE TABLE #FinalStudentList
        (
         StudentId UNIQUEIDENTIFIER
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,Gender VARCHAR(50)
        ,Race VARCHAR(50)
        ,RevisedCohort VARCHAR(10)
        ,Exclusions VARCHAR(10)
        ,CopmPrg100Less2Yrs VARCHAR(10)
        ,CopmPrg150Less2Yrs VARCHAR(10)
        ,StillInProg150Percent VARCHAR(10)
        ,TransOut VARCHAR(10)
        ,RevisedCohortCount INT
        ,ExclusionsCount INT
        ,CopmPrg100Less2YrsCount INT
        ,CopmPrg150Less2YrsCount INT
        ,StillInProg150PercentCount INT
        ,TransOutCount INT
        );     

--INSERT INTO #FinalStudentList
--SELECT * FROM #GraduationRate 
--WHERE StuEnrollId IN 
--(SELECT TOP 1 StuEnrollId FROM arStuEnrollments WHERE StudentId=#GraduationRate.StudentId 
--AND StartDate=(SELECT MIN(StartDate) FROM arStuEnrollments WHERE StudentId=#GraduationRate.StudentId))

--INSERT INTO #FinalStudentList
--SELECT DISTINCT StudentId,SSN,StudentNumber,StudentName,Gender,Race,
--(CASE WHEN (SELECT COUNT(RevisedCohort) FROM #GraduationRate WHERE StudentId=t2.StudentId AND RevisedCohort='X')>=1 THEN 'X' else '' END) as RevisedCohort, 
--(CASE WHEN (SELECT COUNT(Exclusions) FROM #GraduationRate WHERE StudentId=t2.StudentId AND Exclusions='X')>=1 THEN 'X' else '' END) as Exclusions,
--(CASE WHEN (SELECT COUNT(CopmPrg100Less2Yrs) FROM #GraduationRate WHERE StudentId=t2.StudentId  AND CopmPrg100Less2Yrs='X')>=1 THEN 'X' else '' END) as CopmPrg100Less2Yrs,
--(CASE WHEN (SELECT COUNT(CopmPrg150Less2Yrs) FROM #GraduationRate WHERE StudentId=t2.StudentId AND CopmPrg150Less2Yrs='X')>=1 THEN 'X' else '' END) as CopmPrg150Less2Yrs,
--(CASE WHEN (SELECT COUNT(TransOut) FROM #GraduationRate WHERE StudentId=t2.StudentId AND TransOut='X')>=1 THEN 'X' else '' END) as TransOut,
--(CASE WHEN (SELECT COUNT(RevisedCohort) FROM #GraduationRate WHERE StudentId=t2.StudentId AND RevisedCohort='X')>=1 THEN 1 else 0 END) as RevisedCohortCount,
--(CASE WHEN (SELECT COUNT(Exclusions) FROM #GraduationRate WHERE StudentId=t2.StudentId AND Exclusions='X')>=1 THEN 1 else 0 END) as ExclusionsCount,
--(CASE WHEN (SELECT COUNT(CopmPrg100Less2Yrs) FROM #GraduationRate WHERE StudentId=t2.StudentId AND CopmPrg100Less2Yrs='X')>=1 THEN 1 else 0 END) as CopmPrg100Less2YrsCount, 
--(CASE WHEN (SELECT COUNT(CopmPrg150Less2Yrs) FROM #GraduationRate WHERE StudentId=t2.StudentId AND CopmPrg150Less2Yrs='X')>=1 THEN 1 else 0 END) as CopmPrg150Less2YrsCount, 
--(CASE WHEN (SELECT COUNT(TransOut) FROM #GraduationRate WHERE StudentId=t2.StudentId AND TransOut='X')>=1 THEN 1 else 0 END) as TransOutCount
--FROM #GraduationRate t2 Where StudentId is not NULL
    
    SELECT  StudentId
           ,SSN
           ,StudentNumber
           ,StudentName
           ,Gender
           ,Race
           ,RevisedCohort
           ,Exclusions
           ,CopmPrg100Less2Yrs
           ,CASE WHEN CopmPrg100Less2Yrs = 'X' THEN 'X'
                 ELSE CopmPrg150Less2Yrs
            END AS CopmPrg150Less2Yrs
           ,StillInProg150Percent
           ,TransOut
           ,RevisedCohortCount
           ,ExclusionsCount
           ,CopmPrg100Less2YrsCount
           ,CASE WHEN CopmPrg100Less2Yrs = 'X' THEN 1
                 ELSE CopmPrg150Less2YrsCount
            END AS CopmPrg150Less2YrsCount
           ,StillInProg150PercentCount
           ,TransOutCount
    FROM    #GraduationRate
--WHERE StuEnrollId IN 
--(SELECT TOP 1 StuEnrollId FROM arStuEnrollments WHERE StudentId=#GraduationRate.StudentId 
--AND StartDate=(SELECT MIN(StartDate) FROM arStuEnrollments WHERE StudentId=#GraduationRate.StudentId))
ORDER BY    CASE WHEN @OrderBy = 'SSN' THEN SSN
            END
           ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
            END
           ,CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
            END;  
  
    DROP TABLE #GraduationRate; 
    DROP TABLE  #FinalStudentList;
    DROP TABLE #StudentsList;  
GO






--------------------------------------------------------------------------------------------------------
--US6393 - END
--------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------
--Troy: DE13164:Unable to change term for a transfer credit on the transcript page
-------------------------------------------------------------------------------------------------------------------------------
/****** Object:  StoredProcedure [dbo].[UpdateTranscriptCompletedClass]    Script Date: 11/28/2016 6:34:44 AM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   type = 'P'
                    AND name = 'UpdateTranscriptCompletedClass' )
    DROP PROCEDURE UpdateTranscriptCompletedClass;
GO

/****** Object:  StoredProcedure [dbo].[UpdateTranscriptCompletedClass]    Script Date: 11/28/2016 6:34:44 AM ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO



-- =============================================
-- Author:		Ginzo, John
-- Create date: 06/16/2015
-- Description:	Change Grades and Terms for transdcript completed classes
-- =============================================
CREATE PROCEDURE dbo.UpdateTranscriptCompletedClass
    @ResultOrTransferId VARCHAR(50)
   ,@TermId VARCHAR(50)
   ,@ReqId VARCHAR(50)
   ,@TestId VARCHAR(50) = NULL
   ,@GrdSysDetailId VARCHAR(50) = NULL
   ,@GradBookWeightingLevel VARCHAR(50)
   ,@user VARCHAR(50)
   ,@StuEnrollId VARCHAR(50)
AS
    BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
        SET NOCOUNT ON;
		
        DECLARE @ReturnVal INT;
        SET @ReturnVal = 0;

        DECLARE @Enrollments TABLE
            (
             StuEnrollID UNIQUEIDENTIFIER
            );
        INSERT  INTO @Enrollments
                ( StuEnrollID )
        VALUES  ( @StuEnrollId );

        DECLARE @StuEnrollIdInput UNIQUEIDENTIFIER;

        IF @TestId IS NULL
            BEGIN
                                  

				-----------------------------------------------------------
				-- First get the req id of the transferred grade to update
				-----------------------------------------------------------
                DECLARE @Req2id UNIQUEIDENTIFIER;
                SET @Req2id = (
                                SELECT  ReqId
                                FROM    dbo.arTransferGrades
                                WHERE   TransferId = @ResultOrTransferId
                              );	
				
				------------------------------------------------------------------------------------
				--Get the other enrollments with transfer grade for the same course in the same term
				------------------------------------------------------------------------------------				
                INSERT  INTO @Enrollments
                        SELECT DISTINCT
                                r.StuEnrollId
                        FROM    dbo.arStuEnrollments se
                        INNER JOIN dbo.arTransferGrades r ON se.StuEnrollId = r.StuEnrollId
                        WHERE   se.StudentId = (
                                                 SELECT StudentId
                                                 FROM   dbo.arStuEnrollments
                                                 WHERE  StuEnrollID = @StuEnrollId
                                               )
                                AND se.StuEnrollId <> @StuEnrollId
                                AND r.ReqId = @Req2id
                                AND r.TermId = @TermId;
				-----------------------------------------------------------
				-- Do the update
				-----------------------------------------------------------	              

                DECLARE UpdateExistingGrade CURSOR
                FOR
                    SELECT  StuEnrollID
                    FROM    @Enrollments;
				

                OPEN UpdateExistingGrade;

                FETCH NEXT FROM UpdateExistingGrade INTO @StuEnrollIdInput;
		 
                BEGIN TRANSACTION;
                BEGIN TRY
                    WHILE @@FETCH_STATUS = 0
                        BEGIN       
							
                            UPDATE  arTransferGrades
                            SET     GrdSysDetailId = @GrdSysDetailId
                                   ,IsCourseCompleted = 1
                                   ,IsGradeOverridden = 1
                                   ,GradeOverriddenBy = @user
                                   ,GradeOverriddenDate = GETDATE()
                                   ,ModUser = @user
                                   ,ModDate = GETDATE()
                                   ,TermId = @TermId
                            WHERE   TransferId = @ResultOrTransferId;
									
						
                            FETCH NEXT FROM UpdateExistingGrade INTO @StuEnrollIdInput;  
                        END;
                END TRY
                BEGIN CATCH
                    ROLLBACK TRANSACTION;
                    SET @ReturnVal = -1;
                    PRINT ERROR_MESSAGE();
                END CATCH;

                IF @@TranCount > 0
                    COMMIT TRANSACTION;
				

				-----------------------------------------------------------
				-- close the cursor
				-----------------------------------------------------------
                CLOSE UpdateExistingGrade;   
                DEALLOCATE UpdateExistingGrade;
				-----------------------------------------------------------	
                --RETURN @ReturnVal	
            END;
        ELSE
            BEGIN					
											
				---------------------------------------------------------------
				--Get the other enrollments registered for the same class
				----------------------------------------------------------------				
                INSERT  INTO @Enrollments
                        SELECT DISTINCT
                                r.StuEnrollId
                        FROM    dbo.arStuEnrollments se
                        INNER JOIN dbo.arResults r ON se.StuEnrollId = r.StuEnrollId
                        WHERE   se.StudentId = (
                                                 SELECT StudentId
                                                 FROM   dbo.arStuEnrollments
                                                 WHERE  StuEnrollID = @StuEnrollId
                                               )
                                AND se.StuEnrollId <> @StuEnrollId
                                AND r.TestId = @TestId;
               			
				-----------------------------------------------------------
				-- Do the update
				-----------------------------------------------------------	              

                DECLARE UpdateExistingGrade CURSOR
                FOR
                    SELECT  StuEnrollID
                    FROM    @Enrollments;
				

                OPEN UpdateExistingGrade;

                FETCH NEXT FROM UpdateExistingGrade INTO @StuEnrollIdInput;
		 
                BEGIN TRANSACTION;
                BEGIN TRY
                    WHILE @@FETCH_STATUS = 0
                        BEGIN       
							
                            UPDATE  arResults
                            SET     GrdSysDetailId = @GrdSysDetailId
                                   ,IsCourseCompleted = 1
                                   ,IsGradeOverridden = 1
                                   ,GradeOverriddenBy = @user
                                   ,GradeOverriddenDate = GETDATE()
                                   ,ModUser = @user
                                   ,ModDate = GETDATE()
                            FROM    arResults r
                            WHERE   ResultId = @ResultOrTransferId;							 
                              
						
                            FETCH NEXT FROM UpdateExistingGrade INTO @StuEnrollIdInput;  
                        END;
                END TRY
                BEGIN CATCH
                    ROLLBACK TRANSACTION;
                    SET @ReturnVal = -1;
                    PRINT ERROR_MESSAGE();
                END CATCH;

                IF @@TranCount > 0
                    COMMIT TRANSACTION;
				

				-----------------------------------------------------------
				-- close the cursor
				-----------------------------------------------------------
                CLOSE UpdateExistingGrade;   
                DEALLOCATE UpdateExistingGrade;
				-----------------------------------------------------------
			   
                --RETURN @ReturnVal
            END;

    END;
-----------------------------------------------------------------------------------------
GO
-------------------------------------------------------------------------------------------------------------------------------
--End Troy: DE13164:Unable to change term for a transfer credit on the transcript page
-------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------
--Start Balaji: DE131216
-------------------------------------------------------------------------------------------------------------------------------
UPDATE  dbo.adLeadEntranceTest
SET     ActualScore = NULL
WHERE   ActualScore = 0.00
        AND LEN(TestTaken) = 0;
GO
-------------------------------------------------------------------------------------------------------------------------------
--End Balaji: DE13126
-------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------------
--Start Balaji: US9562 New Function to check if student completed program
-------------------------------------------------------------------------------------------------------------------------------
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'dbo.CompletedProgramin150PercentofProgramLength')
                    AND type IN ( N'FN',N'IF',N'TF',N'FS',N'FT' ) )
    DROP FUNCTION dbo.CompletedProgramin150PercentofProgramLength;
GO 
CREATE  FUNCTION dbo.CompletedProgramin150PercentofProgramLength ( @StuEnrollId CHAR(36) )
RETURNS BIT
--RETURNS DECIMAL(18,2)
AS
    BEGIN
        DECLARE @mtf DECIMAL(18,2);
        DECLARE @progid UNIQUEIDENTIFIER;
        DECLARE @undergradprogram BIT;
        DECLARE @credithourprogram BIT;
        DECLARE @CreditsEarned DECIMAL(18,2);
        DECLARE @HoursCompleted DECIMAL(18,2);
        DECLARE @IsProgramComplete BIT;
        DECLARE @CreditsAttempted DECIMAL(18,2);
        DECLARE @maxcredits DECIMAL(18,2);
        DECLARE @percentagecredits DECIMAL(18,2);
        DECLARE @percentageattempted DECIMAL(18,2);

        SET @percentagecredits = 0.00;
        SET @percentageattempted = 0.00;

        SET @progid = (
                        SELECT  ProgId
                        FROM    arPrgVersions pv
                               ,dbo.arStuEnrollments se
                        WHERE   pv.PrgVerId = se.PrgVerId
                                AND se.StuEnrollId = @StuEnrollId
                      );

        SET @undergradprogram = (
                                  SELECT    dbo.isProgramUndergraduate(@progid)
                                );
        SET @credithourprogram = (
                                   SELECT   dbo.isProgramCreditHour(@progid)
                                 );

        SET @IsProgramComplete = 0;

		--Undergrad and credit hour
        IF (
             @undergradprogram = 1
             AND @credithourprogram = 1
           )
            BEGIN
				
                SET @mtf = (
                             SELECT ISNULL(pv.Credits,0.00)
                             FROM   arPrgVersions pv
                                   ,dbo.arStuEnrollments se
                             WHERE  pv.PrgVerId = se.PrgVerId
                                    AND se.StuEnrollId = @StuEnrollId
                           );
				
                SET @maxcredits = ISNULL(@mtf,0) * 1.50;

                SET @percentagecredits = @mtf / @maxcredits;

				
                SET @CreditsEarned = (
                                       SELECT   ISNULL(SUM(CS.CreditsEarned),0.00)
                                       FROM     dbo.arStuEnrollments se
                                       INNER JOIN dbo.syCreditSummary CS ON CS.StuEnrollId = se.StuEnrollId
                                       INNER JOIN syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                       WHERE    se.StuEnrollId = @StuEnrollId
                                                AND sc.SysStatusId = 14
                                     );

                SET @CreditsAttempted = (
                                          SELECT    ISNULL(SUM(CS.CreditsAttempted),0.00)
                                          FROM      dbo.arStuEnrollments se
                                          INNER JOIN dbo.syCreditSummary CS ON CS.StuEnrollId = se.StuEnrollId
                                          INNER JOIN syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                          WHERE     se.StuEnrollId = @StuEnrollId
                                                    AND sc.SysStatusId = 14
                                        );



                IF ( @CreditsAttempted <> 0.00 )
                    BEGIN
                        SET @percentageattempted = @CreditsEarned / @CreditsAttempted;
                    END;
				
                IF (
                     @percentageattempted <> 0.00
                     AND @percentageattempted >= @percentagecredits
                   )
                    BEGIN
                        SET @IsProgramComplete = 1;
                    END;

		  	
            END;

		--undergrad and clock hour
        IF (
             @undergradprogram = 1
             AND @credithourprogram = 0
           )
            BEGIN
                SET @mtf = (
                             SELECT ISNULL(pv.Hours,0.00) * 1.50
                             FROM   arPrgVersions pv
                                   ,dbo.arStuEnrollments se
                             WHERE  pv.PrgVerId = se.PrgVerId
                                    AND se.StuEnrollId = @StuEnrollId
                           );

                SET @HoursCompleted = (
                                        SELECT TOP 1
                                                ISNULL(CS.AdjustedPresentDays,0.00)
                                        FROM    arPrgVersions pv
                                        INNER JOIN dbo.arStuEnrollments se ON se.PrgVerId = pv.PrgVerId
                                        INNER JOIN dbo.syStudentAttendanceSummary CS ON CS.StuEnrollId = se.StuEnrollId
                                        INNER JOIN syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                        WHERE   se.StuEnrollId = @StuEnrollId
                                                AND sc.SysStatusId = 14
                                        ORDER BY StudentAttendedDate DESC
                                      );
				
                IF (
                     @HoursCompleted <> 0.00
                     AND @HoursCompleted <= @mtf
                   )
                    BEGIN
                        SET @IsProgramComplete = 1;
                    END;	
            END;		

        
        RETURN @IsProgramComplete;
    END;
GO
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   type = 'P'
                    AND name = 'USP_PELLRecipients_GetList' )
    DROP PROCEDURE USP_PELLRecipients_GetList;
GO
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   type = 'P'
                    AND name = 'usp_GradRatesStafford_Detail_GetList' )
    DROP PROCEDURE usp_GradRatesStafford_Detail_GetList;
GO
CREATE PROCEDURE USP_PELLRecipients_GetList
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@CohortYear VARCHAR(10) = NULL
   ,@CohortPossible VARCHAR(20) = NULL
   ,@OrderBy VARCHAR(100)
   ,@StartDate DATETIME
   ,@EndDate DATETIME
AS --SET @CampusId='3F5E839A-589A-4B2A-B258-35A1A8B3B819'
--SET @ProgId=NULL
--SET @CohortYear='2015'
--SET @CohortPossible= 'full'
--SET @OrderBy = 'SSN'
--SET @StartDate='09/01/2015'
--SET @EndDate='08/31/2016'


    DECLARE @AcadInstFirstTimeStartDate DATETIME;
    DECLARE @ReturnValue VARCHAR(100)
       ,@Value VARCHAR(100);  
    DECLARE @SSN VARCHAR(10)
       ,@FirstName VARCHAR(100)
       ,@LastName VARCHAR(100)
       ,@StudentNumber VARCHAR(50)
       ,@TransferredOut INT
       ,@Exclusions INT;  
    DECLARE @CitizenShip_IPEDSValue INT
       ,@ProgramType_IPEDSValue INT
       ,@Gender_IPEDSValue INT
       ,@FullTimePartTime_IPEDSValue INT
       ,@StudentId UNIQUEIDENTIFIER;   

    DECLARE @StatusDate DATETIME;

    SET @StatusDate = '08/31/' + CONVERT(CHAR(4),YEAR(GETDATE()) - 1);
  
-- Check if School tracks grades by letter or numeric   
--SET @Value = (SELECT TOP 1 Value FROM dbo.syConfigAppSetValues WHERE SettingId=47)  
-- 2/07/2013 - updated the @value 
    SET @Value = (
                   SELECT   dbo.GetAppSettingValue(47,@CampusId)
                 );  
 
    IF @ProgId IS NOT NULL
        BEGIN  
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );  
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);  
        END;  
    ELSE
        BEGIN  
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );  
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);  
        END;  
 
-- Create a temp table to hold the final output of this stored proc  
    CREATE TABLE #GraduationRate
        (
         RowNumber UNIQUEIDENTIFIER
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,Gender VARCHAR(50)
        ,Race VARCHAR(50)
        ,RevisedCohort VARCHAR(10)
        ,Exclusions VARCHAR(10)
        ,CopmPrg100Less2Yrs VARCHAR(10)
        ,CopmPrg150Less2Yrs VARCHAR(10)
        ,StillInProg150Percent VARCHAR(10)
        ,TransOut VARCHAR(10)
        ,RevisedCohortCount INT
        ,ExclusionsCount INT
        ,CopmPrg100Less2YrsCount INT
        ,CopmPrg150Less2YrsCount INT
        ,TransOutCount INT
        ,StillInProg150PercentCount INT
        ,GenderSequence INT
        ,RaceSequence INT
        ,StudentId UNIQUEIDENTIFIER
        ,StuEnrollId UNIQUEIDENTIFIER
        ,StartDate DATETIME
        );   
     
     
    CREATE TABLE #StudentsList
        (
         StudentId UNIQUEIDENTIFIER
        ,StuENrollId UNIQUEIDENTIFIER
        ,SSN VARCHAR(10)
        ,FirstName VARCHAR(100)
        ,LastName VARCHAR(100)
        ,MiddleName VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,TransferredOut INT
        ,Exclusions INT
        ,CitizenShip_IPEDSValue INT
        ,ProgramType_IPEDSValue INT
        ,Gender_IPEDSValue INT
        ,FullTimePartTime_IPEDSValue INT
        ,GenderId UNIQUEIDENTIFIER
        ,RaceId UNIQUEIDENTIFIER
        ,CitizenId UNIQUEIDENTIFIER
        ,GenderDescription VARCHAR(50)
        ,RaceDescription VARCHAR(50)
        ,StudentGraduatedStatusCount INT
        ,ClockHourProgramCount INT
        );  
  
-- Get the list of FullTime, FirstTime, UnderGraduate Students  
-- Exclude students who are Transferred in to the institution 
    IF LOWER(@CohortPossible) = 'fall'
        BEGIN 
            SET @AcadInstFirstTimeStartDate = DATEADD(YEAR,-1,@EndDate);
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t2.StuEnrollId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StuEnrollId = t2.StuEnrollId
                                        AND SQ3.SysStatusId = 19
                                        AND --SQ1.TransferDate<=@EndDate AND 
                                        SQ1.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                        StuENrollId
                                                                 FROM   arTrackTransfer )
                                        AND SQ1.DateDetermined <= @StatusDate
                            ) AS TransferredOut
                           ,(  
							 -- Check if the student was either dropped and if the drop reason is either  
							 -- deceased, active duty, foreign aid service, church mission  
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped  
                                                    AND SQ1.DateDetermined <= @StatusDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN (
                                               SELECT TOP 1
                                                        EthCodeId
                                               FROM     adEthCodes
                                               WHERE    EthCodeDescrip = 'Race/ethnicity unknown'
                                             )
                                 ELSE t1.Race
                            END AS Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN 'Race/ethnicity unknown'
                                 ELSE (
                                        SELECT DISTINCT
                                                AgencyDescrip
                                        FROM    syRptAgencyFldValues
                                        WHERE   RptAgencyFldValId = t4.IPEDSValue
                                      )
                            END AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StuEnrollId = t2.StuEnrollId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                                        AND ExpGradDate <= @StatusDate
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StuEnrollId = t2.StuEnrollId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students  
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                    INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                    INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61  -- Full Time 
                            AND t12.IPEDSValue = 11  -- First Time  
                            AND t9.IPEDSValue = 58   -- Under Graduate
                            AND AFS.AdvFundSourceId = 2 -- PELL
                            AND AT.AwardTypeId = 1 --Grant
                            AND
                        --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                            (
                              t2.StartDate > @AcadInstFirstTimeStartDate
                              AND t2.StartDate <= @EndDate
                            )
                            -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
                            AND t2.StuEnrollId NOT IN ( SELECT  t1.StuEnrollId
                                                        FROM    arStuEnrollments t1
                                                               ,syStatusCodes t2
                                                        WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                                AND StartDate <= @EndDate
                                                                AND -- Student started before the end date range
                                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND t2.SysStatusId IN ( 12,19,8 ) -- Dropped out/Transferred/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                                AND (
                                                                      t1.DateDetermined < @EndDate
                                                                      OR ExpGradDate < @EndDate
                                                                      OR LDA < @EndDate
                                                                    ) )  
                        -- If Student is enrolled in only one program version and if that program version   
                       -- happens to be a continuing ed program exclude the student  
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )  
                        -- Exclude students who were Transferred in to your institution   
                        -- This was used in FALL Part B report and we can reuse it here  
                            AND t2.StuEnrollId NOT IN (
                            SELECT DISTINCT
                                    SQ1.StuEnrollId
                            FROM    arStuEnrollments SQ1
                                   ,adDegCertSeeking SQ2
                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                    AND   
                                    -- To be considered for TransferIn, Student should be a First-Time Student  
                               --SQ1.LeadId IS NOT NULL AND 
                                    SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                    AND SQ2.IPEDSValue = 11
                                    AND (
                                          SQ1.TransferHours > 0
                                          OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                    THEN (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    StuENrollId = SQ1.StuEnrollId
                                                                                    AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                            FROM    arGradeSystemDetails
                                                                                                            WHERE   IsTransferGrade = 1 )
                                                                         )
                                                                    ELSE (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    IsTransferred = 1
                                                                                    AND StuENrollId = SQ1.StuEnrollId
                                                                         )
                                                               END
                                        )
                                    AND SQ1.StartDate < @EndDate
                                    AND NOT EXISTS ( SELECT StuENrollId
                                                     FROM   arStuEnrollments
                                                     WHERE  StudentId = SQ1.StudentId
                                                            AND StartDate < SQ1.StartDate ) )
                            AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                  FROM      arStuEnrollments t2
                                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                  LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                  LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                            AND (
                                                                  @ProgId IS NULL
                                                                  OR t8.ProgId IN ( SELECT  Val
                                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                )
                                                            AND t10.IPEDSValue = 61
                                                            AND -- Full Time  
                                                            t12.IPEDSValue = 11
                                                            AND -- First Time  
                                                            t9.IPEDSValue = 58
                                                            AND -- Under Graduate  
                                                            t2.StudentId = t1.StudentId
                                                            AND
							 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                                                            (
                                                              t2.StartDate > @AcadInstFirstTimeStartDate
                                                              AND t2.StartDate <= @EndDate
                                                            ) )
							-- If two enrollments fall on same start date pick any one 
                            AND t2.StuEnrollId IN (
                            SELECT TOP 1
                                    StuENrollId
                            FROM    arStuEnrollments t2
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t10.IPEDSValue = 61
                                    AND -- Full Time  
                                    t12.IPEDSValue = 11
                                    AND -- First Time  
                                    t9.IPEDSValue = 58
                                    AND -- Under Graduate  
                                    t2.StudentId = t1.StudentId
                                    AND 
							 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                                    (
                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                      AND t2.StartDate <= @EndDate
                                    )
                                    AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                          FROM      arStuEnrollments t2
                                                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                          LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                          WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                    AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN ( SELECT  Val
                                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                        )
                                                                    AND t10.IPEDSValue = 61
                                                                    AND -- Full Time  
                                                                    t12.IPEDSValue = 11
                                                                    AND -- First Time  
                                                                    t9.IPEDSValue = 58
                                                                    AND -- Under Graduate  
                                                                    t2.StudentId = t1.StudentId
                                                                    AND
								 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                                                                    (
                                                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                                                      AND t2.StartDate <= @EndDate
                                                                    ) ) );
        END;
    IF LOWER(@CohortPossible) = 'full'
        BEGIN
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t2.StuEnrollId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StuEnrollId = t2.StuEnrollId
                                        AND SQ3.SysStatusId = 19
                                        AND --SQ1.TransferDate<=@EndDate AND 
                                        SQ1.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                        StuENrollId
                                                                 FROM   arTrackTransfer )
                                        AND SQ1.DateDetermined <= @StatusDate
                            ) AS TransferredOut
                           ,(  
     -- Check if the student was either dropped and if the drop reason is either  
     -- deceased, active duty, foreign aid service, church mission  
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped  
                                                    AND SQ1.DateDetermined <= @StatusDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN (
                                               SELECT TOP 1
                                                        EthCodeId
                                               FROM     adEthCodes
                                               WHERE    EthCodeDescrip = 'Race/ethnicity unknown'
                                             )
                                 ELSE t1.Race
                            END AS Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN 'Race/ethnicity unknown'
                                 ELSE (
                                        SELECT DISTINCT
                                                AgencyDescrip
                                        FROM    syRptAgencyFldValues
                                        WHERE   RptAgencyFldValId = t4.IPEDSValue
                                      )
                            END AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StuEnrollId = t2.StuEnrollId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                                        AND ExpGradDate <= @StatusDate
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StuEnrollId = t2.StuEnrollId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students  
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                    INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                    INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61 -- Full Time 
                            AND t12.IPEDSValue = 11 -- First Time
                            AND t9.IPEDSValue = 58 -- Under Graduate  
                            AND AFS.AdvFundSourceId = 2 -- PELL
                            AND AT.AwardTypeId = 1 --Grant
                            AND (
                                  t2.StartDate >= @StartDate
                                  AND t2.StartDate <= @EndDate
                                ) 
                            -- Exclude students who are Dropped out/Transferred/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
                            AND t2.StuEnrollId NOT IN ( SELECT  t1.StuEnrollId
                                                        FROM    arStuEnrollments t1
                                                               ,syStatusCodes t2
                                                        WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                                AND StartDate <= @EndDate
                                                                AND -- Student started before the end date range
                                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND t2.SysStatusId IN ( 12,19,8 ) -- Dropped out/Transferred/No Start
																-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                                AND (
                                                                      t1.DateDetermined < @StartDate
                                                                      OR LDA < @StartDate
                                                                    ) ) 
                        -- Student Should Have Started Before the Report End Date  
                        -- If Student is enrolled in only one program version and 
                        --if that program version   
                       -- happens to be a continuing ed program exclude the student  
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )  
                        -- Exclude students who were Transferred in to your institution   
                        -- This was used in FALL Part B report and we can reuse it here  
                            AND t2.StuEnrollId NOT IN (
                            SELECT DISTINCT
                                    SQ1.StuEnrollId
                            FROM    arStuEnrollments SQ1
                                   ,adDegCertSeeking SQ2
                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                    AND   
                                    -- To be considered for TransferIn, Student should be a First-Time Student  
                                    --SQ1.LeadId IS NOT NULL AND 
                                    SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                    AND SQ2.IPEDSValue = 11
                                    AND (
                                          SQ1.TransferHours > 0
                                          OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                    THEN (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    StuENrollId = SQ1.StuEnrollId
                                                                                    AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                            FROM    arGradeSystemDetails
                                                                                                            WHERE   IsTransferGrade = 1 )
                                                                         )
                                                                    ELSE (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    IsTransferred = 1
                                                                                    AND StuENrollId = SQ1.StuEnrollId
                                                                         )
                                                               END
                                        )
                                    AND SQ1.StartDate < @EndDate
                                    AND NOT EXISTS ( SELECT StuENrollId
                                                     FROM   arStuEnrollments
                                                     WHERE  StudentId = SQ1.StudentId
                                                            AND StartDate < SQ1.StartDate ) )
                            AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                  FROM      arStuEnrollments t2
                                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                  LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                  LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                            AND (
                                                                  @ProgId IS NULL
                                                                  OR t8.ProgId IN ( SELECT  Val
                                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                )
                                                            AND t10.IPEDSValue = 61
                                                            AND -- Full Time  
                                                            t12.IPEDSValue = 11
                                                            AND -- First Time  
                                                            t9.IPEDSValue = 58
                                                            AND -- Under Graduate  
                                                            t2.StudentId = t1.StudentId
                                                            AND (
                                                                  t2.StartDate >= @StartDate
                                                                  AND t2.StartDate <= @EndDate
                                                                ) )
					 	-- If two enrollments fall on same start date pick any one 
                            AND t2.StuEnrollId IN (
                            SELECT TOP 1
                                    StuENrollId
                            FROM    arStuEnrollments t2
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t10.IPEDSValue = 61
                                    AND -- Full Time  
                                    t12.IPEDSValue = 11
                                    AND -- First Time  
                                    t9.IPEDSValue = 58
                                    AND -- Under Graduate  
                                    t2.StudentId = t1.StudentId
                                    AND (
                                          t2.StartDate >= @StartDate
                                          AND t2.StartDate <= @EndDate
                                        )
                                    AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                          FROM      arStuEnrollments t2
                                                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                          LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                          WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                    AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN ( SELECT  Val
                                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                        )
                                                                    AND t10.IPEDSValue = 61
                                                                    AND -- Full Time  
                                                                    t12.IPEDSValue = 11
                                                                    AND -- First Time  
                                                                    t9.IPEDSValue = 58
                                                                    AND -- Under Graduate  
                                                                    t2.StudentId = t1.StudentId
                                                                    AND (
                                                                          t2.StartDate >= @StartDate
                                                                          AND t2.StartDate <= @EndDate
                                                                        ) ) );
        END;

  
    SELECT  NEWID() AS RowNumber
           ,dbo.UDF_FormatSSN(SSN) AS SSN
           ,StudentNumber
           ,LastName + ', ' + FirstName + ' ' + ISNULL(MiddleName,'') AS StudentName
           ,1 AS RevisedCohort
           ,Exclusions
           ,CASE WHEN Exclusions >= 1 THEN 1
                 ELSE 0
            END AS ExclusionsCount
           ,dbo.CompletedProgramin150PercentofProgramLength(StuENrollId) AS CompletedProgramin150PercentofProgramLength
    FROM    #StudentsList dt
    ORDER BY CASE WHEN @OrderBy = 'SSN' THEN dt.SSN
             END
           ,CASE WHEN @OrderBy = 'LastName' THEN dt.LastName
            END
           ,CASE WHEN @OrderBy = 'StudentNumber' THEN CONVERT(INT,dt.StudentNumber)
            END;
    DROP TABLE #GraduationRate;
    DROP TABLE #StudentsList;
GO
CREATE PROCEDURE dbo.usp_GradRatesStafford_Detail_GetList
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@CohortYear VARCHAR(10) = NULL
   ,@CohortPossible VARCHAR(20) = NULL
   ,@OrderBy VARCHAR(100)
   ,@StartDate DATETIME
   ,@EndDate DATETIME
AS --SET @CampusId='3F5E839A-589A-4B2A-B258-35A1A8B3B819'
--SET @ProgId=NULL
--SET @CohortYear='2013'
--SET @CohortPossible= 'full year'
--SET @OrderBy = 'SSN'
--SET @StartDate='09/01/2010'
--SET @EndDate='08/31/2011'


    DECLARE @AcadInstFirstTimeStartDate DATETIME;
    DECLARE @ReturnValue VARCHAR(100)
       ,@Value VARCHAR(100);  
    DECLARE @SSN VARCHAR(10)
       ,@FirstName VARCHAR(100)
       ,@LastName VARCHAR(100)
       ,@StudentNumber VARCHAR(50)
       ,@TransferredOut INT
       ,@Exclusions INT;  
    DECLARE @CitizenShip_IPEDSValue INT
       ,@ProgramType_IPEDSValue INT
       ,@Gender_IPEDSValue INT
       ,@FullTimePartTime_IPEDSValue INT
       ,@StudentId UNIQUEIDENTIFIER;   

    DECLARE @StatusDate DATETIME;

    SET @StatusDate = '08/31/' + CONVERT(CHAR(4),YEAR(GETDATE()) - 1);
  
-- Check if School tracks grades by letter or numeric   
--SET @Value = (SELECT TOP 1 Value FROM dbo.syConfigAppSetValues WHERE SettingId=47)  
-- 2/07/2013 - updated the @value 
    SET @Value = (
                   SELECT   dbo.GetAppSettingValue(47,@CampusId)
                 );  
 
    IF @ProgId IS NOT NULL
        BEGIN  
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.ProgId IN ( SELECT   Val
                                   FROM     MultipleValuesForReportParameters(@ProgId,',',1) );  
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);  
        END;  
    ELSE
        BEGIN  
            SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + ProgDescrip + ','
            FROM    arPrograms t1
            WHERE   t1.StatusId = 'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'
                    AND t1.CampGrpId IN ( SELECT    CampGrpId
                                          FROM      syCmpGrpCmps
                                          WHERE     CampusId = @CampusId );  
            SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);  
        END;  
 
-- Create a temp table to hold the final output of this stored proc  
    CREATE TABLE #GraduationRate
        (
         RowNumber UNIQUEIDENTIFIER
        ,SSN VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,StudentName VARCHAR(100)
        ,Gender VARCHAR(50)
        ,Race VARCHAR(50)
        ,RevisedCohort VARCHAR(10)
        ,Exclusions VARCHAR(10)
        ,CopmPrg100Less2Yrs VARCHAR(10)
        ,CopmPrg150Less2Yrs VARCHAR(10)
        ,StillInProg150Percent VARCHAR(10)
        ,TransOut VARCHAR(10)
        ,RevisedCohortCount INT
        ,ExclusionsCount INT
        ,CopmPrg100Less2YrsCount INT
        ,CopmPrg150Less2YrsCount INT
        ,TransOutCount INT
        ,StillInProg150PercentCount INT
        ,GenderSequence INT
        ,RaceSequence INT
        ,StudentId UNIQUEIDENTIFIER
        ,StuEnrollId UNIQUEIDENTIFIER
        ,StartDate DATETIME
        );   
     
     
    CREATE TABLE #StudentsList
        (
         StudentId UNIQUEIDENTIFIER
        ,StuENrollId UNIQUEIDENTIFIER
        ,SSN VARCHAR(10)
        ,FirstName VARCHAR(100)
        ,LastName VARCHAR(100)
        ,MiddleName VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,TransferredOut INT
        ,Exclusions INT
        ,CitizenShip_IPEDSValue INT
        ,ProgramType_IPEDSValue INT
        ,Gender_IPEDSValue INT
        ,FullTimePartTime_IPEDSValue INT
        ,GenderId UNIQUEIDENTIFIER
        ,RaceId UNIQUEIDENTIFIER
        ,CitizenId UNIQUEIDENTIFIER
        ,GenderDescription VARCHAR(50)
        ,RaceDescription VARCHAR(50)
        ,StudentGraduatedStatusCount INT
        ,ClockHourProgramCount INT
        );  

    CREATE TABLE #StudentsListPELL
        (
         StudentId UNIQUEIDENTIFIER
        ,StuENrollId UNIQUEIDENTIFIER
        ,SSN VARCHAR(10)
        ,FirstName VARCHAR(100)
        ,LastName VARCHAR(100)
        ,MiddleName VARCHAR(50)
        ,StudentNumber VARCHAR(50)
        ,TransferredOut INT
        ,Exclusions INT
        ,CitizenShip_IPEDSValue INT
        ,ProgramType_IPEDSValue INT
        ,Gender_IPEDSValue INT
        ,FullTimePartTime_IPEDSValue INT
        ,GenderId UNIQUEIDENTIFIER
        ,RaceId UNIQUEIDENTIFIER
        ,CitizenId UNIQUEIDENTIFIER
        ,GenderDescription VARCHAR(50)
        ,RaceDescription VARCHAR(50)
        ,StudentGraduatedStatusCount INT
        ,ClockHourProgramCount INT
        );  
  
-- Get the list of FullTime, FirstTime, UnderGraduate Students  
-- Exclude students who are Transferred in to the institution 
    IF LOWER(@CohortPossible) = 'fall'
        BEGIN 
            SET @AcadInstFirstTimeStartDate = DATEADD(YEAR,-1,@EndDate);
            INSERT  INTO #StudentsListPELL
                    SELECT DISTINCT
                            t1.StudentId
                           ,t2.StuEnrollId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StuEnrollId = t2.StuEnrollId
                                        AND SQ3.SysStatusId = 19
                                        AND --SQ1.TransferDate<=@EndDate AND 
                                        SQ1.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                        StuENrollId
                                                                 FROM   arTrackTransfer )
                                        AND SQ1.DateDetermined <= @StatusDate
                            ) AS TransferredOut
                           ,(  
							 -- Check if the student was either dropped and if the drop reason is either  
							 -- deceased, active duty, foreign aid service, church mission  
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped  
                                                    AND SQ1.DateDetermined <= @StatusDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN (
                                               SELECT TOP 1
                                                        EthCodeId
                                               FROM     adEthCodes
                                               WHERE    EthCodeDescrip = 'Race/ethnicity unknown'
                                             )
                                 ELSE t1.Race
                            END AS Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN 'Race/ethnicity unknown'
                                 ELSE (
                                        SELECT DISTINCT
                                                AgencyDescrip
                                        FROM    syRptAgencyFldValues
                                        WHERE   RptAgencyFldValId = t4.IPEDSValue
                                      )
                            END AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StuEnrollId = t2.StuEnrollId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                                        AND ExpGradDate <= @StatusDate
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StuEnrollId = t2.StuEnrollId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students  
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                    INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                    INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61  -- Full Time 
                            AND t12.IPEDSValue = 11  -- First Time  
                            AND t9.IPEDSValue = 58   -- Under Graduate
                            AND AFS.AdvFundSourceId = 2 -- DL Sub
                            AND AT.AwardTypeId = 1 --Loan
                            AND
                        --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                            (
                              t2.StartDate > @AcadInstFirstTimeStartDate
                              AND t2.StartDate <= @EndDate
                            )
                            -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
                            AND t2.StuEnrollId NOT IN ( SELECT  t1.StuEnrollId
                                                        FROM    arStuEnrollments t1
                                                               ,syStatusCodes t2
                                                        WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                                AND StartDate <= @EndDate
                                                                AND -- Student started before the end date range
                                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND t2.SysStatusId IN ( 12,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                                AND (
                                                                      t1.DateDetermined < @EndDate
                                                                      OR ExpGradDate < @EndDate
                                                                      OR LDA < @EndDate
                                                                    ) )  
                        -- If Student is enrolled in only one program version and if that program version   
                       -- happens to be a continuing ed program exclude the student  
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )  
                        -- Exclude students who were Transferred in to your institution   
                        -- This was used in FALL Part B report and we can reuse it here  
                            AND t2.StuEnrollId NOT IN (
                            SELECT DISTINCT
                                    SQ1.StuEnrollId
                            FROM    arStuEnrollments SQ1
                                   ,adDegCertSeeking SQ2
                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                    AND   
                                    -- To be considered for TransferIn, Student should be a First-Time Student  
                               --SQ1.LeadId IS NOT NULL AND 
                                    SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                    AND SQ2.IPEDSValue = 11
                                    AND (
                                          SQ1.TransferHours > 0
                                          OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                    THEN (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    StuENrollId = SQ1.StuEnrollId
                                                                                    AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                            FROM    arGradeSystemDetails
                                                                                                            WHERE   IsTransferGrade = 1 )
                                                                         )
                                                                    ELSE (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    IsTransferred = 1
                                                                                    AND StuENrollId = SQ1.StuEnrollId
                                                                         )
                                                               END
                                        )
                                    AND SQ1.StartDate < @EndDate
                                    AND NOT EXISTS ( SELECT StuENrollId
                                                     FROM   arStuEnrollments
                                                     WHERE  StudentId = SQ1.StudentId
                                                            AND StartDate < SQ1.StartDate ) )
                            AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                  FROM      arStuEnrollments t2
                                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                  LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                  LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                            AND (
                                                                  @ProgId IS NULL
                                                                  OR t8.ProgId IN ( SELECT  Val
                                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                )
                                                            AND t10.IPEDSValue = 61
                                                            AND -- Full Time  
                                                            t12.IPEDSValue = 11
                                                            AND -- First Time  
                                                            t9.IPEDSValue = 58
                                                            AND -- Under Graduate  
                                                            t2.StudentId = t1.StudentId
                                                            AND
							 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                                                            (
                                                              t2.StartDate > @AcadInstFirstTimeStartDate
                                                              AND t2.StartDate <= @EndDate
                                                            ) )
							-- If two enrollments fall on same start date pick any one 
                            AND t2.StuEnrollId IN (
                            SELECT TOP 1
                                    StuENrollId
                            FROM    arStuEnrollments t2
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t10.IPEDSValue = 61
                                    AND -- Full Time  
                                    t12.IPEDSValue = 11
                                    AND -- First Time  
                                    t9.IPEDSValue = 58
                                    AND -- Under Graduate  
                                    t2.StudentId = t1.StudentId
                                    AND 
							 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                                    (
                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                      AND t2.StartDate <= @EndDate
                                    )
                                    AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                          FROM      arStuEnrollments t2
                                                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                          LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                          WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                    AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN ( SELECT  Val
                                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                        )
                                                                    AND t10.IPEDSValue = 61
                                                                    AND -- Full Time  
                                                                    t12.IPEDSValue = 11
                                                                    AND -- First Time  
                                                                    t9.IPEDSValue = 58
                                                                    AND -- Under Graduate  
                                                                    t2.StudentId = t1.StudentId
                                                                    AND
								 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                                                                    (
                                                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                                                      AND t2.StartDate <= @EndDate
                                                                    ) ) );
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t2.StuEnrollId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StuEnrollId = t2.StuEnrollId
                                        AND SQ3.SysStatusId = 19
                                        AND --SQ1.TransferDate<=@EndDate AND 
                                        SQ1.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                        StuENrollId
                                                                 FROM   arTrackTransfer )
                                        AND SQ1.DateDetermined <= @StatusDate
                            ) AS TransferredOut
                           ,(  
							 -- Check if the student was either dropped and if the drop reason is either  
							 -- deceased, active duty, foreign aid service, church mission  
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped  
                                                    AND SQ1.DateDetermined <= @StatusDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN (
                                               SELECT TOP 1
                                                        EthCodeId
                                               FROM     adEthCodes
                                               WHERE    EthCodeDescrip = 'Race/ethnicity unknown'
                                             )
                                 ELSE t1.Race
                            END AS Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN 'Race/ethnicity unknown'
                                 ELSE (
                                        SELECT DISTINCT
                                                AgencyDescrip
                                        FROM    syRptAgencyFldValues
                                        WHERE   RptAgencyFldValId = t4.IPEDSValue
                                      )
                            END AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StuEnrollId = t2.StuEnrollId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                                        AND ExpGradDate <= @StatusDate
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StuEnrollId = t2.StuEnrollId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students  
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                    INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                    INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND t2.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                StuENrollId
                                                        FROM    #StudentsListPELL )
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61  -- Full Time 
                            AND t12.IPEDSValue = 11  -- First Time  
                            AND t9.IPEDSValue = 58   -- Under Graduate
                            AND AFS.AdvFundSourceId = 7 -- DL Sub
                            AND AT.AwardTypeId = 2 --Loan
                            AND
                        --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                            (
                              t2.StartDate > @AcadInstFirstTimeStartDate
                              AND t2.StartDate <= @EndDate
                            )
                            -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
                            AND t2.StuEnrollId NOT IN ( SELECT  t1.StuEnrollId
                                                        FROM    arStuEnrollments t1
                                                               ,syStatusCodes t2
                                                        WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                                AND StartDate <= @EndDate
                                                                AND -- Student started before the end date range
                                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND t2.SysStatusId IN ( 12,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                                AND (
                                                                      t1.DateDetermined < @EndDate
                                                                      OR ExpGradDate < @EndDate
                                                                      OR LDA < @EndDate
                                                                    ) )  
                        -- If Student is enrolled in only one program version and if that program version   
                       -- happens to be a continuing ed program exclude the student  
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )  
                        -- Exclude students who were Transferred in to your institution   
                        -- This was used in FALL Part B report and we can reuse it here  
                            AND t2.StuEnrollId NOT IN (
                            SELECT DISTINCT
                                    SQ1.StuEnrollId
                            FROM    arStuEnrollments SQ1
                                   ,adDegCertSeeking SQ2
                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                    AND   
                                    -- To be considered for TransferIn, Student should be a First-Time Student  
                               --SQ1.LeadId IS NOT NULL AND 
                                    SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                    AND SQ2.IPEDSValue = 11
                                    AND (
                                          SQ1.TransferHours > 0
                                          OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                    THEN (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    StuENrollId = SQ1.StuEnrollId
                                                                                    AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                            FROM    arGradeSystemDetails
                                                                                                            WHERE   IsTransferGrade = 1 )
                                                                         )
                                                                    ELSE (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    IsTransferred = 1
                                                                                    AND StuENrollId = SQ1.StuEnrollId
                                                                         )
                                                               END
                                        )
                                    AND SQ1.StartDate < @EndDate
                                    AND NOT EXISTS ( SELECT StuENrollId
                                                     FROM   arStuEnrollments
                                                     WHERE  StudentId = SQ1.StudentId
                                                            AND StartDate < SQ1.StartDate ) )
                            AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                  FROM      arStuEnrollments t2
                                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                  LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                  LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                            AND (
                                                                  @ProgId IS NULL
                                                                  OR t8.ProgId IN ( SELECT  Val
                                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                )
                                                            AND t10.IPEDSValue = 61
                                                            AND -- Full Time  
                                                            t12.IPEDSValue = 11
                                                            AND -- First Time  
                                                            t9.IPEDSValue = 58
                                                            AND -- Under Graduate  
                                                            t2.StudentId = t1.StudentId
                                                            AND
							 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                                                            (
                                                              t2.StartDate > @AcadInstFirstTimeStartDate
                                                              AND t2.StartDate <= @EndDate
                                                            ) )
							-- If two enrollments fall on same start date pick any one 
                            AND t2.StuEnrollId IN (
                            SELECT TOP 1
                                    StuENrollId
                            FROM    arStuEnrollments t2
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t10.IPEDSValue = 61
                                    AND -- Full Time  
                                    t12.IPEDSValue = 11
                                    AND -- First Time  
                                    t9.IPEDSValue = 58
                                    AND -- Under Graduate  
                                    t2.StudentId = t1.StudentId
                                    AND 
							 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                                    (
                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                      AND t2.StartDate <= @EndDate
                                    )
                                    AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                          FROM      arStuEnrollments t2
                                                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                          LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                          WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                    AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN ( SELECT  Val
                                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                        )
                                                                    AND t10.IPEDSValue = 61
                                                                    AND -- Full Time  
                                                                    t12.IPEDSValue = 11
                                                                    AND -- First Time  
                                                                    t9.IPEDSValue = 58
                                                                    AND -- Under Graduate  
                                                                    t2.StudentId = t1.StudentId
                                                                    AND
								 --2/07/2012 - DE9062 - Academic year options should have a cutoff start date For First Time 
                        --t2.StartDate<=@EndDate -- Student Should Have Started Before the Report End Date
                                                                    (
                                                                      t2.StartDate > @AcadInstFirstTimeStartDate
                                                                      AND t2.StartDate <= @EndDate
                                                                    ) ) );
        END;
    IF LOWER(@CohortPossible) = 'full'
        BEGIN
            INSERT  INTO #StudentsListPELL
                    SELECT DISTINCT
                            t1.StudentId
                           ,t2.StuEnrollId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StuEnrollId = t2.StuEnrollId
                                        AND SQ3.SysStatusId = 19
                                        AND --SQ1.TransferDate<=@EndDate AND 
                                        SQ1.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                        StuENrollId
                                                                 FROM   arTrackTransfer )
                                        AND SQ1.DateDetermined <= @StatusDate
                            ) AS TransferredOut
                           ,(  
     -- Check if the student was either dropped and if the drop reason is either  
     -- deceased, active duty, foreign aid service, church mission  
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped  
                                                    AND SQ1.DateDetermined <= @StatusDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN (
                                               SELECT TOP 1
                                                        EthCodeId
                                               FROM     adEthCodes
                                               WHERE    EthCodeDescrip = 'Race/ethnicity unknown'
                                             )
                                 ELSE t1.Race
                            END AS Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN 'Race/ethnicity unknown'
                                 ELSE (
                                        SELECT DISTINCT
                                                AgencyDescrip
                                        FROM    syRptAgencyFldValues
                                        WHERE   RptAgencyFldValId = t4.IPEDSValue
                                      )
                            END AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StuEnrollId = t2.StuEnrollId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                                        AND ExpGradDate <= @StatusDate
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StuEnrollId = t2.StuEnrollId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students  
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                    INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                    INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61 -- Full Time 
                            AND t12.IPEDSValue = 11 -- First Time
                            AND t9.IPEDSValue = 58 -- Under Graduate  
                            AND AFS.AdvFundSourceId = 2 -- PELL
                            AND AT.AwardTypeId = 1 --Grant
                            AND (
                                  t2.StartDate >= @StartDate
                                  AND t2.StartDate <= @EndDate
                                ) 
                            -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
                            AND t2.StuEnrollId NOT IN ( SELECT  t1.StuEnrollId
                                                        FROM    arStuEnrollments t1
                                                               ,syStatusCodes t2
                                                        WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                                AND StartDate <= @EndDate
                                                                AND -- Student started before the end date range
                                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND t2.SysStatusId IN ( 12,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                                AND (
                                                                      t1.DateDetermined < @StartDate
                                                                      OR ExpGradDate < @StartDate
                                                                      OR LDA < @StartDate
                                                                    ) ) 
                        -- Student Should Have Started Before the Report End Date  
                        -- If Student is enrolled in only one program version and 
                        --if that program version   
                       -- happens to be a continuing ed program exclude the student  
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )  
                        -- Exclude students who were Transferred in to your institution   
                        -- This was used in FALL Part B report and we can reuse it here  
                            AND t2.StuEnrollId NOT IN (
                            SELECT DISTINCT
                                    SQ1.StuEnrollId
                            FROM    arStuEnrollments SQ1
                                   ,adDegCertSeeking SQ2
                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                    AND   
                                    -- To be considered for TransferIn, Student should be a First-Time Student  
                                    --SQ1.LeadId IS NOT NULL AND 
                                    SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                    AND SQ2.IPEDSValue = 11
                                    AND (
                                          SQ1.TransferHours > 0
                                          OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                    THEN (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    StuENrollId = SQ1.StuEnrollId
                                                                                    AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                            FROM    arGradeSystemDetails
                                                                                                            WHERE   IsTransferGrade = 1 )
                                                                         )
                                                                    ELSE (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    IsTransferred = 1
                                                                                    AND StuENrollId = SQ1.StuEnrollId
                                                                         )
                                                               END
                                        )
                                    AND SQ1.StartDate < @EndDate
                                    AND NOT EXISTS ( SELECT StuENrollId
                                                     FROM   arStuEnrollments
                                                     WHERE  StudentId = SQ1.StudentId
                                                            AND StartDate < SQ1.StartDate ) )
                            AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                  FROM      arStuEnrollments t2
                                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                  LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                  LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                            AND (
                                                                  @ProgId IS NULL
                                                                  OR t8.ProgId IN ( SELECT  Val
                                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                )
                                                            AND t10.IPEDSValue = 61
                                                            AND -- Full Time  
                                                            t12.IPEDSValue = 11
                                                            AND -- First Time  
                                                            t9.IPEDSValue = 58
                                                            AND -- Under Graduate  
                                                            t2.StudentId = t1.StudentId
                                                            AND (
                                                                  t2.StartDate >= @StartDate
                                                                  AND t2.StartDate <= @EndDate
                                                                ) )
					 	-- If two enrollments fall on same start date pick any one 
                            AND t2.StuEnrollId IN (
                            SELECT TOP 1
                                    StuENrollId
                            FROM    arStuEnrollments t2
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t10.IPEDSValue = 61
                                    AND -- Full Time  
                                    t12.IPEDSValue = 11
                                    AND -- First Time  
                                    t9.IPEDSValue = 58
                                    AND -- Under Graduate  
                                    t2.StudentId = t1.StudentId
                                    AND (
                                          t2.StartDate >= @StartDate
                                          AND t2.StartDate <= @EndDate
                                        )
                                    AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                          FROM      arStuEnrollments t2
                                                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                          LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                          WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                    AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN ( SELECT  Val
                                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                        )
                                                                    AND t10.IPEDSValue = 61
                                                                    AND -- Full Time  
                                                                    t12.IPEDSValue = 11
                                                                    AND -- First Time  
                                                                    t9.IPEDSValue = 58
                                                                    AND -- Under Graduate  
                                                                    t2.StudentId = t1.StudentId
                                                                    AND (
                                                                          t2.StartDate >= @StartDate
                                                                          AND t2.StartDate <= @EndDate
                                                                        ) ) );
            INSERT  INTO #StudentsList
                    SELECT DISTINCT
                            t1.StudentId
                           ,t2.StuEnrollId
                           ,t1.SSN
                           ,t1.FirstName
                           ,t1.LastName
                           ,t1.MiddleName
                           ,t1.StudentNumber
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SQ1
                                       ,syStatusCodes SQ2
                                       ,dbo.sySysStatus SQ3
                              WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                        AND SQ2.SysStatusId = SQ3.SysStatusId
                                        AND SQ1.StuEnrollId = t2.StuEnrollId
                                        AND SQ3.SysStatusId = 19
                                        AND --SQ1.TransferDate<=@EndDate AND 
                                        SQ1.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                        StuENrollId
                                                                 FROM   arTrackTransfer )
                                        AND SQ1.DateDetermined <= @StatusDate
                            ) AS TransferredOut
                           ,(  
     -- Check if the student was either dropped and if the drop reason is either  
     -- deceased, active duty, foreign aid service, church mission  
                              CASE WHEN (
                                          SELECT    COUNT(*)
                                          FROM      arStuEnrollments SQ1
                                                   ,syStatusCodes SQ2
                                                   ,dbo.sySysStatus SQ3
                                                   ,arDropReasons SQ44
                                          WHERE     SQ1.StatusCodeId = SQ2.StatusCodeId
                                                    AND SQ2.SysStatusId = SQ3.SysStatusId
                                                    AND SQ1.DropReasonId = SQ44.DropReasonId
                                                    AND SQ1.StuEnrollId = t2.StuEnrollId
                                                    AND SQ3.SysStatusId IN ( 12 ) -- Dropped  
                                                    AND SQ1.DateDetermined <= @StatusDate
                                                    AND SQ44.IPEDSValue IN ( 15,16,17,18,19 )
                                        ) >= 1 THEN 1
                                   ELSE 0
                              END ) AS Exclusions
                           ,t11.IPEDSValue AS CitizenShip_IPEDSValue
                           ,t9.IPEDSValue AS ProgramType_IPEDSValue
                           ,t3.IPEDSValue AS Gender_IPEDSValue
                           ,t10.IPEDSValue AS FullTimePartTime_IPEDSValue
                           ,t1.Gender
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN (
                                               SELECT TOP 1
                                                        EthCodeId
                                               FROM     adEthCodes
                                               WHERE    EthCodeDescrip = 'Race/ethnicity unknown'
                                             )
                                 ELSE t1.Race
                            END AS Race
                           ,t1.Citizen
                           ,(
                              SELECT DISTINCT
                                        AgencyDescrip
                              FROM      syRptAgencyFldValues
                              WHERE     RptAgencyFldValId = t3.IPEDSValue
                            ) AS GenderDescription
                           ,CASE WHEN (
                                        t1.Race IS NULL
                                        AND t11.IPEDSValue <> 65
                                      ) THEN 'Race/ethnicity unknown'
                                 ELSE (
                                        SELECT DISTINCT
                                                AgencyDescrip
                                        FROM    syRptAgencyFldValues
                                        WHERE   RptAgencyFldValId = t4.IPEDSValue
                                      )
                            END AS RaceDescription
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,syStatusCodes SC
                              WHERE     SEC.StuEnrollId = t2.StuEnrollId
                                        AND SEC.StatusCodeId = SC.StatusCodeId
                                        AND SC.SysStatusId = 14
                                        AND ExpGradDate <= @StatusDate
                            ) AS StudentGraduatedStatusCount
                           ,(
                              SELECT    COUNT(*)
                              FROM      arStuEnrollments SEC
                                       ,arPrograms P
                                       ,arPrgVersions PV
                              WHERE     SEC.PrgVerId = PV.PrgVerId
                                        AND P.ProgId = PV.ProgId
                                        AND SEC.StuEnrollId = t2.StuEnrollId
                                        AND P.ACId = 5
                            ) AS ClockHourProgramCount
                    FROM    adGenders t3
                    LEFT JOIN arStudent t1 ON t3.GenderId = t1.Gender
                    LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                    INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                    INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                    INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                 AND t6.SysStatusId NOT IN ( 8 ) -- Ignore "No Start" Students  
                    INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                    INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                    INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                    LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                    LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                    INNER JOIN adCitizenships t11 ON t1.Citizen = t11.CitizenshipId
                    INNER JOIN faStudentAwards SA ON SA.StuEnrollId = t2.StuEnrollId
                    INNER JOIN saFundSources FS ON FS.FundSourceId = SA.AwardTypeId
                    INNER JOIN saAwardTypes AT ON AT.AwardTypeId = FS.AwardTypeId
                    INNER JOIN syAdvFundSources AFS ON AFS.AdvFundSourceId = FS.AdvFundSourceId
                    WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                            AND t2.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                StuENrollId
                                                        FROM    #StudentsListPELL )
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t10.IPEDSValue = 61 -- Full Time 
                            AND t12.IPEDSValue = 11 -- First Time
                            AND t9.IPEDSValue = 58 -- Under Graduate  
                            AND AFS.AdvFundSourceId = 7 -- DL SUB
                            AND AT.AwardTypeId = 2 --LOAN
                            AND (
                                  t2.StartDate >= @StartDate
                                  AND t2.StartDate <= @EndDate
                                ) 
                            -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
                            AND t2.StuEnrollId NOT IN ( SELECT  t1.StuEnrollId
                                                        FROM    arStuEnrollments t1
                                                               ,syStatusCodes t2
                                                        WHERE   t1.StatusCodeId = t2.StatusCodeId
                                                                AND StartDate <= @EndDate
                                                                AND -- Student started before the end date range
                                                                LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                AND (
                                                                      @ProgId IS NULL
                                                                      OR t8.ProgId IN ( SELECT  Val
                                                                                        FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                    )
                                                                AND t2.SysStatusId IN ( 12,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                                                AND (
                                                                      t1.DateDetermined < @StartDate
                                                                      OR ExpGradDate < @StartDate
                                                                      OR LDA < @StartDate
                                                                    ) ) 
                        -- Student Should Have Started Before the Report End Date  
                        -- If Student is enrolled in only one program version and 
                        --if that program version   
                       -- happens to be a continuing ed program exclude the student  
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )  
                        -- Exclude students who were Transferred in to your institution   
                        -- This was used in FALL Part B report and we can reuse it here  
                            AND t2.StuEnrollId NOT IN (
                            SELECT DISTINCT
                                    SQ1.StuEnrollId
                            FROM    arStuEnrollments SQ1
                                   ,adDegCertSeeking SQ2
                            WHERE   SQ1.StuEnrollId = t2.StuEnrollId
                                    AND   
                                    -- To be considered for TransferIn, Student should be a First-Time Student  
                                    --SQ1.LeadId IS NOT NULL AND 
                                    SQ1.degcertseekingid = SQ2.DegCertSeekingId
                                    AND SQ2.IPEDSValue = 11
                                    AND (
                                          SQ1.TransferHours > 0
                                          OR SQ1.StuEnrollId = CASE WHEN @Value = 'letter'
                                                                    THEN (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    StuENrollId = SQ1.StuEnrollId
                                                                                    AND GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                            FROM    arGradeSystemDetails
                                                                                                            WHERE   IsTransferGrade = 1 )
                                                                         )
                                                                    ELSE (
                                                                           SELECT TOP 1
                                                                                    StuENrollId
                                                                           FROM     arTransferGrades
                                                                           WHERE    IsTransferred = 1
                                                                                    AND StuENrollId = SQ1.StuEnrollId
                                                                         )
                                                               END
                                        )
                                    AND SQ1.StartDate < @EndDate
                                    AND NOT EXISTS ( SELECT StuENrollId
                                                     FROM   arStuEnrollments
                                                     WHERE  StudentId = SQ1.StudentId
                                                            AND StartDate < SQ1.StartDate ) )
                            AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                  FROM      arStuEnrollments t2
                                                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                  LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                  LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                  WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                            AND (
                                                                  @ProgId IS NULL
                                                                  OR t8.ProgId IN ( SELECT  Val
                                                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                )
                                                            AND t10.IPEDSValue = 61
                                                            AND -- Full Time  
                                                            t12.IPEDSValue = 11
                                                            AND -- First Time  
                                                            t9.IPEDSValue = 58
                                                            AND -- Under Graduate  
                                                            t2.StudentId = t1.StudentId
                                                            AND (
                                                                  t2.StartDate >= @StartDate
                                                                  AND t2.StartDate <= @EndDate
                                                                ) )
					 	-- If two enrollments fall on same start date pick any one 
                            AND t2.StuEnrollId IN (
                            SELECT TOP 1
                                    StuENrollId
                            FROM    arStuEnrollments t2
                            INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                            INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                            INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                            LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                            LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                            WHERE   LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t10.IPEDSValue = 61
                                    AND -- Full Time  
                                    t12.IPEDSValue = 11
                                    AND -- First Time  
                                    t9.IPEDSValue = 58
                                    AND -- Under Graduate  
                                    t2.StudentId = t1.StudentId
                                    AND (
                                          t2.StartDate >= @StartDate
                                          AND t2.StartDate <= @EndDate
                                        )
                                    AND t2.StartDate IN ( SELECT    MIN(StartDate)
                                                          FROM      arStuEnrollments t2
                                                          INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                                                          INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                                                          INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                                                          LEFT JOIN arAttendTypes t10 ON t2.attendtypeid = t10.AttendTypeId
                                                          LEFT JOIN dbo.adDegCertSeeking t12 ON t2.degcertseekingid = t12.DegCertSeekingId
                                                          WHERE     LTRIM(RTRIM(t2.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                                    AND (
                                                                          @ProgId IS NULL
                                                                          OR t8.ProgId IN ( SELECT  Val
                                                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                                                        )
                                                                    AND t10.IPEDSValue = 61
                                                                    AND -- Full Time  
                                                                    t12.IPEDSValue = 11
                                                                    AND -- First Time  
                                                                    t9.IPEDSValue = 58
                                                                    AND -- Under Graduate  
                                                                    t2.StudentId = t1.StudentId
                                                                    AND (
                                                                          t2.StartDate >= @StartDate
                                                                          AND t2.StartDate <= @EndDate
                                                                        ) ) );
        END;
     



--SELECT * FROM #StudentsList  --WHERE lastname='Diognardi'

    SELECT  NEWID() AS RowNumber
           ,dbo.UDF_FormatSSN(SSN) AS SSN
           ,StudentNumber
           ,LastName + ', ' + FirstName + ' ' + ISNULL(MiddleName,'') AS StudentName
           ,1 AS RevisedCohort
           ,Exclusions
           ,CASE WHEN Exclusions >= 1 THEN 1
                 ELSE 0
            END AS ExclusionsCount
           ,dbo.CompletedProgramin150PercentofProgramLength(StuENrollId) AS CompletedProgramin150PercentofProgramLength
    FROM    #StudentsList dt
    ORDER BY CASE WHEN @OrderBy = 'SSN' THEN dt.SSN
             END
           ,CASE WHEN @OrderBy = 'LastName' THEN dt.LastName
            END
           ,CASE WHEN @OrderBy = 'StudentNumber' THEN CONVERT(INT,dt.StudentNumber)
            END;

   

  
    DROP TABLE #GraduationRate; 
    DROP TABLE #StudentsList;
GO
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   type = 'P'
                    AND name = 'USP_FallPartC3_GetList_Enrollment_Detail' )
    DROP PROCEDURE USP_FallPartC3_GetList_Enrollment_Detail;
GO
CREATE PROCEDURE dbo.USP_FallPartC3_GetList_Enrollment_Detail
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@DateRangeText VARCHAR(100) = NULL
   ,@OrderBy VARCHAR(100)
   ,@AreasOfInterest VARCHAR(4000) = NULL
AS
    BEGIN -- sycampuses
/*------------------------------------------------------------------------------------------
Business Rules : 1. Exclude students with no start status
          	     2. Show Only Students who are "First-Time Degree/Certificate Seeking Undergraduates"
 				 3. Student should have started with in the date range
			
Objective: The following provides

 the number of first-time, degree/certificate-seeking undergraduate (7) students 
			Who Started in the date range and how many were full time and part time. The report needs to be grouped by Gender.
-------------------------------------------------------

------------------------------------*/
        DECLARE @ContinuingStartDate DATETIME;
        SET @ContinuingStartDate = @StartDate;
-- For Academic Year Reporter, the End Date should be 10/15/Cohort Year
-- and Start Date should be blank
        IF DAY(@EndDate) = 15
            AND MONTH(@EndDate) = 10
            BEGIN
                SET @EndDate = @StartDate; -- it will always be 10/15/cohort year example: for cohort year 2009, it is 10/15/2009
                SET @StartDate = @StartDate;
            END;

        SELECT  *
        FROM    (
                  SELECT    dbo.UDF_FormatSSN(t1.SSN) AS SSN
                           ,t1.StudentNumber
                           ,t1.LastName + ', ' + t1.FirstName + ' ' + ISNULL(t1.MiddleName,'') AS StudentName
                           ,t3.IPEDSValue
                           ,t2.StartDate
                           ,t3.IPEDSSequence AS GenderSequence
                           ,
				-- Men --
                            CASE WHEN ( t3.IPEDSValue = 30 ) THEN 'X'
                                 ELSE ''
                            END AS ApplicantMen
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 'X'
                                 ELSE ''
                            END AS AdmissionMen
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 1
                                 ELSE 0
                            END AS ApplicantMenCount
                           ,CASE WHEN ( t3.IPEDSValue = 30 ) THEN 1
                                 ELSE 0
                            END AS AdmissionMenCount
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 30
                                      ) THEN 'X'
                                 ELSE ''
                            END AS FullTimeMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 30
                                      ) THEN 'X'
                                 ELSE ''
                            END AS PartTimeMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 30
                                      ) THEN 1
                                 ELSE 0
                            END AS FullTimeCountMen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 30
                                      ) THEN 1
                                 ELSE 0
                            END AS PartTimeCountMen
                           ,
				-- Men --
				-- Women --
                            CASE WHEN ( t3.IPEDSValue = 31 ) THEN 'X'
                                 ELSE ''
                            END AS ApplicantWomen
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 'X'
                                 ELSE ''
                            END AS AdmissionWomen
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 1
                                 ELSE 0
                            END AS ApplicantWomenCount
                           ,CASE WHEN ( t3.IPEDSValue = 31 ) THEN 1
                                 ELSE 0
                            END AS AdmissionWomenCount
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 31
                                      ) THEN 'X'
                                 ELSE ''
                            END AS FullTimeWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 31
                                      ) THEN 'X'
                                 ELSE ''
                            END AS PartTimeWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 61
                                        AND t3.IPEDSValue = 31
                                      ) THEN 1
                                 ELSE 0
                            END AS FullTimeCountWomen
                           ,CASE WHEN (
                                        t10.IPEDSValue = 62
                                        AND t3.IPEDSValue = 31
                                      ) THEN 1
                                 ELSE 0
                            END AS PartTimeCountWomen
                           ,(
                              SELECT TOP 1
                                        EnrollmentId
                              FROM      arStuEnrollments C1
                                       ,arPrgVersions C2
                                       ,arProgTypes C3
                              WHERE     C1.PrgVerId = C2.PrgVerId
                                        AND C2.ProgTypId = C3.ProgTypId
                                        AND C3.IPEDSValue = 58
                                        AND C1.StudentId = t1.StudentId
                              ORDER BY  C1.StartDate
                                       ,C1.EnrollDate
                            ) AS EnrollmentId
				-- Women --
                  FROM      arStudent t1
                  LEFT JOIN adGenders t3 ON t1.Gender = t3.GenderId
                  LEFT JOIN adEthCodes t4 ON t4.EthCodeId = t1.Race
                  INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                  LEFT JOIN adDegCertSeeking t11 ON t11.DegCertSeekingId = t2.DegCertSeekingId ---- For Degree Seeking --
                  INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                  INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                               AND t6.SysStatusId NOT IN ( 8 )
                  INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                  INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId  
				--Areas Of Intrest --
                  INNER JOIN arPrgGrp t12 ON t12.PrgGrpId = t7.PrgGrpId 
				--Areas Of Intrest --
                  INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                  LEFT JOIN arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                  WHERE     t2.CampusId = @CampusId
                            AND (
                                  @ProgId IS NULL
                                  OR t8.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                )
                            AND t9.IPEDSValue = 58
                            AND -- Under Graduate
                            (
                              t10.IPEDSValue = 61
                              OR t10.IPEDSValue = 62
                            )
                            AND --Part Time or Full Time
                            (
                              t3.IPEDSValue = 30
                              OR t3.IPEDSValue = 31
                            )
                            AND -- Men or Women
                            t1.Race IS NOT NULL
                            AND t6.SysStatusId NOT IN ( 8 )  -- Exclude students who are No Start Students
				--and (@StartDate is null or t2.StartDate>=@StartDate) and (@EndDate is null or t2.StartDate<=@EndDate) -- Student should have started in the date range
                            AND t2.StartDate <= @EndDate
                            AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
						-- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
						( SELECT    t1.StuEnrollId
                          FROM      arStuEnrollments t1
                                   ,syStatusCodes t2
                          WHERE     t1.StatusCodeId = t2.StatusCodeId
                                    AND StartDate <= @EndDate
                                    AND -- Student started before the end date range
                                    LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                    AND (
                                          @ProgId IS NULL
                                          OR t8.ProgId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@ProgId,',',1) )
                                        )
                                    AND t2.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
							-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                                    AND (
                                          t1.DateDetermined < @StartDate
                                          OR ExpGradDate < @StartDate
                                          OR LDA < @StartDate
                                        ) )
					-- If Student is enrolled in only program version and if that program version 
						-- happens to be a continuing ed program exclude the student
                            AND t2.StudentId NOT IN ( SELECT DISTINCT
                                                                StudentId
                                                      FROM      (
                                                                  SELECT    StudentId
                                                                           ,COUNT(*) AS RowCounter
                                                                  FROM      arStuEnrollments
                                                                  WHERE     PrgVerId IN ( SELECT    PrgVerId
                                                                                          FROM      arPrgVersions
                                                                                          WHERE     IsContinuingEd = 1 )
                                                                  GROUP BY  StudentId
                                                                  HAVING    COUNT(*) = 1
                                                                ) dtStudent_ContinuingEd )
					--DE8492
					-- Transfer-In Students need to be excluded
                            AND t2.StuEnrollId NOT IN ( SELECT DISTINCT
                                                                StuEnrollId
                                                        FROM    arStuEnrollments
                                                        WHERE   CampusId = LTRIM(RTRIM(@CampusId))
                                                                AND LeadId IS NOT NULL
                                                                AND StuEnrollId IN ( SELECT StuEnrollId
                                                                                     FROM   arStuEnrollments
                                                                                     WHERE  TransferHours > 0
                                                                                            AND CampusId = LTRIM(RTRIM(@CampusId))
                                                                                     UNION
                                                                                     SELECT StuEnrollId
                                                                                     FROM   arTransferGrades
                                                                                     WHERE  GrdSysDetailId IN ( SELECT  GrdSysDetailId
                                                                                                                FROM    arGradeSystemDetails
                                                                                                                WHERE   IsTransferGrade = 1 ) ) )
                            AND ( t11.IPEDSValue = 11 ) -- Degree/Cert Seeking
                            AND (
                                  @AreasOfInterest IS NULL
                                  OR t12.PrgGrpId IN ( SELECT   Val
                                                       FROM     MultipleValuesForReportParameters(@AreasOfInterest,',',1) )
                                ) --Areas Of Intrest --
                ) dt
        ORDER BY CASE WHEN @OrderBy = 'SSN' THEN SSN
                 END
               ,CASE WHEN @OrderBy = 'LastName' THEN StudentName
                END
               , -- LastName end,
                CASE WHEN @OrderBy = 'Student Number' THEN StudentNumber
                END
               ,CASE WHEN @OrderBy = 'EnrollmentId' THEN EnrollmentId
                END;
    END;
GO
------------------------------------------------------------------------------------------------------------------------------
--END Balaji: US9562
-------------------------------------------------------------------------------------------------------------------------------

