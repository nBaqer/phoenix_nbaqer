-- ********************************************************************************************************
-- Consolidated Script Version 3.7SP5
-- Schema Changes Zone
-- Please deploy your schema changes in this area
-- (Format the code with TSQl Format before insert here please)
-- ********************************************************************************************************

-- =========================================================================================================
-- Usp_PR_Sub2_Enrollment_Summary
-- =========================================================================================================
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[Usp_PR_Sub2_Enrollment_Summary]')
                    AND type IN ( N'P',N'PC' ) )
    BEGIN
        DROP PROCEDURE Usp_PR_Sub2_Enrollment_Summary;
    END;
    
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =========================================================================================================
-- Usp_PR_Sub2_Enrollment_Summary
-- =========================================================================================================
CREATE PROCEDURE dbo.Usp_PR_Sub2_Enrollment_Summary
    @StuEnrollId VARCHAR(50)
   ,@TermId VARCHAR(4000) = NULL
   ,@TermStartDate DATETIME = NULL
   ,@TermStartDateModifier VARCHAR(10) = NULL
   ,@ShowFinanceCalculations BIT = 0
   -- by default hide cost and current balance
   ,@ShowWeeklySchedule BIT = 0
 -- , @TrackSapAttendance VARCHAR(10) = NULL
 -- , @displayhours VARCHAR(10) = NULL
AS
    DECLARE @TrackSapAttendance VARCHAR(1000);
    DECLARE @displayHours AS VARCHAR(1000);
    DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
    IF @TermStartDate IS NULL
        OR @TermStartDate = ''
        BEGIN
            SET @TermStartDate = CONVERT(VARCHAR(10),GETDATE(),120);
        END; 
    SET @StuEnrollCampusId = COALESCE((
                                        SELECT  ASE.CampusId
                                        FROM    arStuEnrollments AS ASE
                                        WHERE   ASE.StuEnrollId = @StuEnrollId
                                      ),NULL);
    SET @TrackSapAttendance = dbo.GetAppSettingValueByKeyName('TrackSapAttendance',@StuEnrollCampusId);
    IF ( @TrackSapAttendance IS NOT NULL )
        BEGIN
            SET @TrackSapAttendance = LOWER(LTRIM(RTRIM(@TrackSapAttendance)));
        END;
		--
    SET @displayHours = dbo.GetAppSettingValueByKeyName('DisplayAttendanceUnitForProgressReportByClass',@StuEnrollCampusId);
    IF ( @displayHours IS NOT NULL )
        BEGIN
            SET @displayHours = LOWER(LTRIM(RTRIM(@displayHours)));
        END;
		--

   
    IF ( @TermId IS NOT NULL )
        BEGIN
            SET @TermStartDate = (
                                   SELECT TOP 1
                                            EndDate
                                   FROM     arTerm
                                   WHERE    TermId = @TermId
                                 );
            IF @TermStartDate IS NULL
                BEGIN
                    SET @TermStartDate = (
                                           SELECT TOP 1
                                                    StartDate
                                           FROM     arTerm
                                           WHERE    TermId = @TermId
                                         );
                END;
            SET @TermStartDateModifier = '<=';
        END;



    DECLARE @ReturnValue VARCHAR(100);
    SELECT  @ReturnValue = COALESCE(@ReturnValue,'') + Descrip + ',  '
    FROM    adLeadByLeadGroups t1
    INNER JOIN adLeadGroups t2 ON t1.LeadGrpId = t2.LeadGrpId
    WHERE   t1.StuEnrollId = @StuEnrollId; 
    SET @ReturnValue = SUBSTRING(@ReturnValue,1,LEN(@ReturnValue) - 1);


/************  Logic to calculate GPA Starts Here *****************************/

    DECLARE @CoursesNotRepeated TABLE
        (
         StuEnrollId UNIQUEIDENTIFIER
        ,TermId UNIQUEIDENTIFIER
        ,TermDescrip VARCHAR(100)
        ,ReqId UNIQUEIDENTIFIER
        ,ReqDescrip VARCHAR(100)
        ,ClsSectionId VARCHAR(50)
        ,CreditsEarned DECIMAL(18,2)
        ,CreditsAttempted DECIMAL(18,2)
        ,CurrentScore DECIMAL(18,2)
        ,CurrentGrade VARCHAR(10)
        ,FinalScore DECIMAL(18,2)
        ,FinalGrade VARCHAR(10)
        ,Completed BIT
        ,FinalGPA DECIMAL(18,2)
        ,Product_WeightedAverage_Credits_GPA DECIMAL(18,2)
        ,Count_WeightedAverage_Credits DECIMAL(18,2)
        ,Product_SimpleAverage_Credits_GPA DECIMAL(18,2)
        ,Count_SimpleAverage_Credits DECIMAL(18,2)
        ,ModUser VARCHAR(50)
        ,ModDate DATETIME
        ,TermGPA_Simple DECIMAL(18,2)
        ,TermGPA_Weighted DECIMAL(18,2)
        ,coursecredits DECIMAL(18,2)
        ,CumulativeGPA DECIMAL(18,2)
        ,CumulativeGPA_Simple DECIMAL(18,2)
        ,FACreditsEarned DECIMAL(18,2)
        ,Average DECIMAL(18,2)
        ,CumAverage DECIMAL(18,2)
        ,TermStartDate DATETIME
        ,rownumber INT
        );

    INSERT  INTO @CoursesNotRepeated
            SELECT  StuEnrollId
                   ,TermId
                   ,TermDescrip
                   ,ReqId
                   ,ReqDescrip
                   ,ClsSectionId
                   ,CreditsEarned
                   ,CreditsAttempted
                   ,CurrentScore
                   ,CurrentGrade
                   ,FinalScore
                   ,FinalGrade
                   ,Completed
                   ,FinalGPA
                   ,Product_WeightedAverage_Credits_GPA
                   ,Count_WeightedAverage_Credits
                   ,Product_SimpleAverage_Credits_GPA
                   ,Count_SimpleAverage_Credits
                   ,ModUser
                   ,ModDate
                   ,TermGPA_Simple
                   ,TermGPA_Weighted
                   ,coursecredits
                   ,CumulativeGPA
                   ,CumulativeGPA_Simple
                   ,FACreditsEarned
                   ,Average
                   ,CumAverage
                   ,TermStartDate
                   ,NULL AS rownumber
            FROM    syCreditSummary
            WHERE   StuEnrollId = @StuEnrollId
                    AND ReqId IN ( SELECT   ReqId
                                   FROM     (
                                              SELECT    ReqId
                                                       ,ReqDescrip
                                                       ,COUNT(*) AS counter
                                              FROM      syCreditSummary
                                              WHERE     StuEnrollId = @StuEnrollId
                                              GROUP BY  ReqId
                                                       ,ReqDescrip
                                              HAVING    COUNT(*) = 1
                                            ) dt );

    DECLARE @GradeCourseRepetitionsMethod VARCHAR(50);
    SET @GradeCourseRepetitionsMethod = (
                                          SELECT    Value
                                          FROM      dbo.syConfigAppSetValues t1
                                          INNER JOIN dbo.syConfigAppSettings t2 ON t1.SettingId = t2.SettingId
                                                                                   AND t2.KeyName = 'GradeCourseRepetitionsMethod'
                                        );

    IF LOWER(@GradeCourseRepetitionsMethod) = 'latest'
        BEGIN
            INSERT  INTO @CoursesNotRepeated
                    SELECT  dt2.StuEnrollId
                           ,dt2.TermId
                           ,dt2.TermDescrip
                           ,dt2.ReqId
                           ,dt2.ReqDescrip
                           ,dt2.ClsSectionId
                           ,dt2.CreditsEarned
                           ,dt2.CreditsAttempted
                           ,dt2.CurrentScore
                           ,dt2.CurrentGrade
                           ,dt2.FinalScore
                           ,dt2.FinalGrade
                           ,dt2.Completed
                           ,dt2.FinalGPA
                           ,dt2.Product_WeightedAverage_Credits_GPA
                           ,dt2.Count_WeightedAverage_Credits
                           ,dt2.Product_SimpleAverage_Credits_GPA
                           ,dt2.Count_SimpleAverage_Credits
                           ,dt2.ModUser
                           ,dt2.ModDate
                           ,dt2.TermGPA_Simple
                           ,dt2.TermGPA_Weighted
                           ,dt2.coursecredits
                           ,dt2.CumulativeGPA
                           ,dt2.CumulativeGPA_Simple
                           ,dt2.FACreditsEarned
                           ,dt2.Average
                           ,dt2.CumAverage
                           ,dt2.TermStartDate
                           ,dt2.RowNumber
                    FROM    (
                              SELECT    StuEnrollId
                                       ,TermId
                                       ,TermDescrip
                                       ,ReqId
                                       ,ReqDescrip
                                       ,ClsSectionId
                                       ,CreditsEarned
                                       ,CreditsAttempted
                                       ,CurrentScore
                                       ,CurrentGrade
                                       ,FinalScore
                                       ,FinalGrade
                                       ,Completed
                                       ,FinalGPA
                                       ,Product_WeightedAverage_Credits_GPA
                                       ,Count_WeightedAverage_Credits
                                       ,Product_SimpleAverage_Credits_GPA
                                       ,Count_SimpleAverage_Credits
                                       ,ModUser
                                       ,ModDate
                                       ,TermGPA_Simple
                                       ,TermGPA_Weighted
                                       ,coursecredits
                                       ,CumulativeGPA
                                       ,CumulativeGPA_Simple
                                       ,FACreditsEarned
                                       ,Average
                                       ,CumAverage
                                       ,TermStartDate
                                       ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY TermStartDate DESC ) AS RowNumber
                              FROM      syCreditSummary
                              WHERE     StuEnrollId = @StuEnrollId
                                        AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                            )
                                        AND ReqId IN ( SELECT   ReqId
                                                       FROM     (
                                                                  SELECT    ReqId
                                                                           ,ReqDescrip
                                                                           ,COUNT(*) AS Counter
                                                                  FROM      syCreditSummary
                                                                  WHERE     StuEnrollId = @StuEnrollId
                                                                  GROUP BY  ReqId
                                                                           ,ReqDescrip
                                                                  HAVING    COUNT(*) > 1
                                                                ) dt )
                            ) dt2
                    WHERE   RowNumber = 1
                    ORDER BY TermStartDate DESC;
        END;

    IF LOWER(@GradeCourseRepetitionsMethod) = 'best'
        BEGIN
            INSERT  INTO @CoursesNotRepeated
                    SELECT  dt2.StuEnrollId
                           ,dt2.TermId
                           ,dt2.TermDescrip
                           ,dt2.ReqId
                           ,dt2.ReqDescrip
                           ,dt2.ClsSectionId
                           ,dt2.CreditsEarned
                           ,dt2.CreditsAttempted
                           ,dt2.CurrentScore
                           ,dt2.CurrentGrade
                           ,dt2.FinalScore
                           ,dt2.FinalGrade
                           ,dt2.Completed
                           ,dt2.FinalGPA
                           ,dt2.Product_WeightedAverage_Credits_GPA
                           ,dt2.Count_WeightedAverage_Credits
                           ,dt2.Product_SimpleAverage_Credits_GPA
                           ,dt2.Count_SimpleAverage_Credits
                           ,dt2.ModUser
                           ,dt2.ModDate
                           ,dt2.TermGPA_Simple
                           ,dt2.TermGPA_Weighted
                           ,dt2.coursecredits
                           ,dt2.CumulativeGPA
                           ,dt2.CumulativeGPA_Simple
                           ,dt2.FACreditsEarned
                           ,dt2.Average
                           ,dt2.CumAverage
                           ,dt2.TermStartDate
                           ,dt2.RowNumber
                    FROM    (
                              SELECT    StuEnrollId
                                       ,TermId
                                       ,TermDescrip
                                       ,ReqId
                                       ,ReqDescrip
                                       ,ClsSectionId
                                       ,CreditsEarned
                                       ,CreditsAttempted
                                       ,CurrentScore
                                       ,CurrentGrade
                                       ,FinalScore
                                       ,FinalGrade
                                       ,Completed
                                       ,FinalGPA
                                       ,Product_WeightedAverage_Credits_GPA
                                       ,Count_WeightedAverage_Credits
                                       ,Product_SimpleAverage_Credits_GPA
                                       ,Count_SimpleAverage_Credits
                                       ,ModUser
                                       ,ModDate
                                       ,TermGPA_Simple
                                       ,TermGPA_Weighted
                                       ,coursecredits
                                       ,CumulativeGPA
                                       ,CumulativeGPA_Simple
                                       ,FACreditsEarned
                                       ,Average
                                       ,CumAverage
                                       ,TermStartDate
                                       ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY FinalGPA DESC ) AS RowNumber
                              FROM      syCreditSummary
                              WHERE     StuEnrollId = @StuEnrollId
                                        AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                            )
                                        AND ReqId IN ( SELECT   ReqId
                                                       FROM     (
                                                                  SELECT    ReqId
                                                                           ,ReqDescrip
                                                                           ,COUNT(*) AS Counter
                                                                  FROM      syCreditSummary
                                                                  WHERE     StuEnrollId = @StuEnrollId
                                                                  GROUP BY  ReqId
                                                                           ,ReqDescrip
                                                                  HAVING    COUNT(*) > 1
                                                                ) dt )
                            ) dt2
                    WHERE   RowNumber = 1; 
        END;

    IF LOWER(@GradeCourseRepetitionsMethod) = 'average'
        BEGIN
            INSERT  INTO @CoursesNotRepeated
                    SELECT  dt2.StuEnrollId
                           ,dt2.TermId
                           ,dt2.TermDescrip
                           ,dt2.ReqId
                           ,dt2.ReqDescrip
                           ,dt2.ClsSectionId
                           ,dt2.CreditsEarned
                           ,dt2.CreditsAttempted
                           ,dt2.CurrentScore
                           ,dt2.CurrentGrade
                           ,dt2.FinalScore
                           ,dt2.FinalGrade
                           ,dt2.Completed
                           ,dt2.FinalGPA
                           ,dt2.Product_WeightedAverage_Credits_GPA
                           ,dt2.Count_WeightedAverage_Credits
                           ,dt2.Product_SimpleAverage_Credits_GPA
                           ,dt2.Count_SimpleAverage_Credits
                           ,dt2.ModUser
                           ,dt2.ModDate
                           ,dt2.TermGPA_Simple
                           ,dt2.TermGPA_Weighted
                           ,dt2.coursecredits
                           ,dt2.CumulativeGPA
                           ,dt2.CumulativeGPA_Simple
                           ,dt2.FACreditsEarned
                           ,dt2.Average
                           ,dt2.CumAverage
                           ,dt2.TermStartDate
                           ,dt2.RowNumber
                    FROM    (
                              SELECT    StuEnrollId
                                       ,TermId
                                       ,TermDescrip
                                       ,ReqId
                                       ,ReqDescrip
                                       ,ClsSectionId
                                       ,CreditsEarned
                                       ,CreditsAttempted
                                       ,CurrentScore
                                       ,CurrentGrade
                                       ,FinalScore
                                       ,FinalGrade
                                       ,Completed
                                       ,FinalGPA
                                       ,Product_WeightedAverage_Credits_GPA
                                       ,Count_WeightedAverage_Credits
                                       ,Product_SimpleAverage_Credits_GPA
                                       ,Count_SimpleAverage_Credits
                                       ,ModUser
                                       ,ModDate
                                       ,TermGPA_Simple
                                       ,TermGPA_Weighted
                                       ,coursecredits
                                       ,CumulativeGPA
                                       ,CumulativeGPA_Simple
                                       ,FACreditsEarned
                                       ,Average
                                       ,CumAverage
                                       ,TermStartDate
                                       ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY FinalGPA DESC ) AS RowNumber
                              FROM      syCreditSummary
                              WHERE     StuEnrollId = @StuEnrollId
                                        AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                            )
                                        AND ReqId IN ( SELECT   ReqId
                                                       FROM     (
                                                                  SELECT    ReqId
                                                                           ,ReqDescrip
                                                                           ,COUNT(*) AS Counter
                                                                  FROM      syCreditSummary
                                                                  WHERE     StuEnrollId = @StuEnrollId
                                                                  GROUP BY  ReqId
                                                                           ,ReqDescrip
                                                                  HAVING    COUNT(*) > 1
                                                                ) dt )
                            ) dt2; 
        END;

/*** Calculate Cumulative Simple GPA Starts Here ****/
    DECLARE @cumSimpleCourseCredits DECIMAL(18,2)
       ,@cumSimple_GPA_Credits DECIMAL(18,2)
       ,@cumSimpleGPA DECIMAL(18,2);
    SET @cumSimpleGPA = 0;
    SET @cumSimpleCourseCredits = (
                                    SELECT  COUNT(coursecredits)
                                    FROM    @CoursesNotRepeated
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND FinalGPA IS NOT NULL
                                  );
    SET @cumSimple_GPA_Credits = (
                                   SELECT   SUM(FinalGPA)
                                   FROM     @CoursesNotRepeated
                                   WHERE    StuEnrollId = @StuEnrollId
                                            AND FinalGPA IS NOT NULL
                                 );
	
	-- 3.7 SP2 Changes
			-- Include Equivalent Courses
    DECLARE @EquivCourse_SA_CC INT
       ,@EquivCourse_SA_GPA DECIMAL(18,2);
    SET @EquivCourse_SA_CC = (
                               SELECT   ISNULL(COUNT(Credits),0) AS Credits
                               FROM     arReqs
                               WHERE    ReqId IN ( SELECT   CE.EquivReqId
                                                   FROM     arCourseEquivalent CE
                                                   INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                   INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                   WHERE    StuEnrollId = @StuEnrollId
                                                            AND R.GrdSysDetailId IS NOT NULL )
                             );

    SET @EquivCourse_SA_GPA = (
                                SELECT  SUM(ISNULL(GSD.GPA,0.00))
                                FROM    arCourseEquivalent CE
                                INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                WHERE   StuEnrollId = @StuEnrollId
                                        AND R.GrdSysDetailId IS NOT NULL
                              );

	-- PRINT 'Equiv Course Simple '
	-- PRINT @EquivCourse_SA_CC
	-- PRINT 'Equiv Course GPA Simple'
	-- PRINT @EquivCourse_SA_GPA

	-- PRINT 'Before Course Credits Simple'
	-- PRINT @cumSimpleCourseCredits
	-- PRINT 'Before WE GPA Simple'
	-- PRINT @cumSimple_GPA_Credits
									 
    SET @cumSimpleCourseCredits = @cumSimpleCourseCredits;                   -- + ISNULL(@EquivCourse_SA_CC,0.00);
    SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits;                     -- + ISNULL(@EquivCourse_SA_GPA,0.00);
			
    IF @cumSimpleCourseCredits >= 1
        BEGIN
            SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
        END; 

	-- PRINT 'After Course Credits Simple'
	-- PRINT @cumSimpleCourseCredits
	-- PRINT 'After WE GPA Simple'
	-- PRINT @cumSimple_GPA_Credits
	-- PRINT @cumSimpleGPA

/*** Calculate Cumulative Simple GPA Ends Here ****/

/**** Calculate Cumulative GPA Weighted *********************/
    DECLARE @cumCourseCredits DECIMAL(18,2)
       ,@cumWeighted_GPA_Credits DECIMAL(18,2)
       ,@cumWeightedGPA DECIMAL(18,2);
    SET @cumWeightedGPA = 0;
    SET @cumCourseCredits = (
                              SELECT    SUM(coursecredits)
                              FROM      @CoursesNotRepeated
                              WHERE     StuEnrollId = @StuEnrollId
                                        AND FinalGPA IS NOT NULL
                            );
    SET @cumWeighted_GPA_Credits = (
                                     SELECT SUM(coursecredits * FinalGPA)
                                     FROM   @CoursesNotRepeated
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalGPA IS NOT NULL
                                   );


    DECLARE @doesThisCourseHaveAEquivalentCourse INT;

    DECLARE @EquivCourse_WGPA_CC1 INT
       ,@EquivCourse_WGPA_GPA1 DECIMAL(18,2);
    SET @EquivCourse_WGPA_CC1 = (
                                  SELECT    SUM(ISNULL(Credits,0)) AS Credits
                                  FROM      arReqs
                                  WHERE     ReqId IN ( SELECT   CE.EquivReqId
                                                       FROM     arCourseEquivalent CE
                                                       INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                       INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                       WHERE    StuEnrollId = @StuEnrollId
                                                                AND R.GrdSysDetailId IS NOT NULL )
                                );

    SET @EquivCourse_WGPA_GPA1 = (
                                   SELECT   SUM(GSD.GPA * R1.Credits)
                                   FROM     arCourseEquivalent CE
                                   INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                   INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                   INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                   INNER JOIN arReqs R1 ON R1.ReqId = CE.ReqId
                                   WHERE    StuEnrollId = @StuEnrollId
                                            AND R.GrdSysDetailId IS NOT NULL
                                 );

	-- PRINT 'Equiv Course '
	-- PRINT @EquivCourse_WGPA_CC1
	-- PRINT 'Equiv Course GPA'
	-- PRINT @EquivCourse_WGPA_GPA1

	-- PRINT 'Before Course Credits'
	-- PRINT @cumCourseCredits
	-- PRINT 'Before WE GPA'
	-- PRINT @cumWeighted_GPA_Credits


    SET @cumCourseCredits = @cumCourseCredits;                               -- + ISNULL(@EquivCourse_WGPA_CC1,0.00);
    SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits;                 -- + ISNULL(@EquivCourse_WGPA_GPA1,0.00);
					
    IF @cumCourseCredits >= 1
        BEGIN
            SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
        END; 

	-- PRINT 'After Course Credits'
	-- PRINT @cumCourseCredits
	-- PRINT 'After WE GPA'
	-- PRINT @cumWeighted_GPA_Credits

	-- PRINT 'After Cal GPA'
	-- PRINT @cumWeightedGPA

/**************** Cumulative GPA Ends Here ************/


/************  Logic to calculate GPA Ends Here *****************************/




    DECLARE @SUMCreditsEarned DECIMAL(18,2)
       ,@SUMFACreditsEarned DECIMAL(18,2);
    SET @SUMCreditsEarned = 0;
    SET @SUMFACreditsEarned = 0;

    DECLARE @EquivCourse_SA_CC2 DECIMAL(18,2);
    SET @EquivCourse_SA_CC2 = (
                                SELECT  ISNULL(SUM(Credits),0.00) AS Credits
                                FROM    arReqs
                                WHERE   ReqId IN ( SELECT   CE.EquivReqId
                                                   FROM     arCourseEquivalent CE
                                                   INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                   INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                   WHERE    StuEnrollId = @StuEnrollId
                                                            AND R.GrdSysDetailId IS NOT NULL )
                              );


    SET @SUMCreditsEarned = (
                              SELECT    SUM(CreditsEarned)
                              FROM      syCreditSummary
                              WHERE     StuEnrollId = @StuEnrollId
                            ); 

                                                                            --SET @SUMCreditsEarned = @SUMCreditsEarned + @EquivCourse_SA_CC2;  

    SET @SUMFACreditsEarned = (
                                SELECT  SUM(FACreditsEarned)
                                FROM    syCreditSummary
                                WHERE   StuEnrollId = @StuEnrollId
                              ); 





    DECLARE @Scheduledhours DECIMAL(18,2)
       ,@ActualPresentDays_ConvertTo_Hours DECIMAL(18,2);
    DECLARE @ActualAbsentDays_ConvertTo_Hours DECIMAL(18,2)
       ,@ActualTardyDays_ConvertTo_Hours DECIMAL(18,2);
    SET @Scheduledhours = 0;
    SET @Scheduledhours = (
                            SELECT  SUM(ScheduledHours)
                            FROM    syStudentAttendanceSummary
                            WHERE   StuEnrollId = @StuEnrollId
                          );

    SET @ActualPresentDays_ConvertTo_Hours = (
                                               SELECT   SUM(ActualPresentDays_ConvertTo_Hours)
                                               FROM     syStudentAttendanceSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                             );
						
    SET @ActualAbsentDays_ConvertTo_Hours = (
                                              SELECT    SUM(ActualAbsentDays_ConvertTo_Hours)
                                              FROM      syStudentAttendanceSummary
                                              WHERE     StuEnrollId = @StuEnrollId
                                            );


    SET @ActualTardyDays_ConvertTo_Hours = (
                                             SELECT SUM(ActualTardyDays_ConvertTo_Hours)
                                             FROM   syStudentAttendanceSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                           );


--Average calculation
    DECLARE @termAverageSum DECIMAL(18,2)
       ,@CumAverage DECIMAL(18,2)
       ,@cumAverageSum DECIMAL(18,2)
       ,@cumAveragecount INT;
    DECLARE @TermAverageCount INT
       ,@TermAverage DECIMAL(18,2);		


    DECLARE @TardiesMakingAbsence INT
       ,@UnitTypeDescrip VARCHAR(20)
       ,@OriginalTardiesMakingAbsence INT;
    SET @TardiesMakingAbsence = (
                                  SELECT TOP 1
                                            t1.TardiesMakingAbsence
                                  FROM      arPrgVersions t1
                                           ,arStuEnrollments t2
                                  WHERE     t1.PrgVerId = t2.PrgVerId
                                            AND t2.StuEnrollId = @StuEnrollId
                                );
							
    SET @UnitTypeDescrip = (
                             SELECT LTRIM(RTRIM(AAUT.UnitTypeDescrip))
                             FROM   arAttUnitType AS AAUT
                             INNER JOIN arPrgVersions AS APV ON APV.UnitTypeId = AAUT.UnitTypeId
                             INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                             WHERE  ASE.StuEnrollId = @StuEnrollId
                           );

    SET @OriginalTardiesMakingAbsence = (
                                          SELECT TOP 1
                                                    tardiesmakingabsence
                                          FROM      syStudentAttendanceSummary
                                          WHERE     StuEnrollId = @StuEnrollId
                                        );
    DECLARE @termstartdate1 DATETIME;

    DECLARE @MeetDate DATETIME
       ,@WeekDay VARCHAR(15)
       ,@StartDate DATETIME
       ,@EndDate DATETIME;
    DECLARE @PeriodDescrip VARCHAR(50)
       ,@Actual DECIMAL(18,2)
       ,@Excused DECIMAL(18,2)
       ,@ClsSectionId UNIQUEIDENTIFIER;
    DECLARE @Absent DECIMAL(18,2)
       ,@SchedHours DECIMAL(18,2)
       ,@TardyMinutes DECIMAL(18,2);
    DECLARE @tardy DECIMAL(18,2)
       ,@tracktardies INT
       ,@rownumber INT
       ,@IsTardy BIT
       ,@ActualRunningScheduledHours DECIMAL(18,2);
    DECLARE @ActualRunningPresentHours DECIMAL(18,2)
       ,@ActualRunningAbsentHours DECIMAL(18,2)
       ,@ActualRunningTardyHours DECIMAL(18,2)
       ,@ActualRunningMakeupHours DECIMAL(18,2);
    DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
       ,@intTardyBreakPoint INT
       ,@AdjustedRunningPresentHours DECIMAL(18,2)
       ,@AdjustedRunningAbsentHours DECIMAL(18,2)
       ,@ActualRunningScheduledDays DECIMAL(18,2);
		--declare @Scheduledhours decimal(18,2),@ActualPresentDays_ConvertTo_Hours decimal(18,2),@ActualAbsentDays_ConvertTo_Hours decimal(18,2),@AttendanceTrack varchar(50)
    DECLARE @TermDescrip VARCHAR(50)
       ,@PrgVerId UNIQUEIDENTIFIER;
    DECLARE @TardyHit VARCHAR(10)
       ,@ScheduledMinutes DECIMAL(18,2);
    DECLARE @Scheduledhours_noperiods DECIMAL(18,2)
       ,@ActualPresentDays_ConvertTo_Hours_NoPeriods DECIMAL(18,2)
       ,@ActualAbsentDays_ConvertTo_Hours_NoPeriods DECIMAL(18,2);
    DECLARE @ActualTardyDays_ConvertTo_Hours_NoPeriods DECIMAL(18,2);
		
--Select * from arAttUnitType
-- By Class and present absent, none
/*
	IF @UnitTypeDescrip IN ( 'none','present absent' )
		AND @TrackSapAttendance = 'byclass'
		BEGIN
			---- PRINT 'Step1!!!!'
			DELETE  FROM syStudentAttendanceSummary
			WHERE   StuEnrollId = @StuEnrollId;
			DECLARE @boolReset BIT
			   ,@MakeupHours DECIMAL(18,2)
			   ,@AdjustedPresentDaysComputed DECIMAL(18,2);
			DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD
			FOR
				SELECT  *
					   ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
				FROM    ( SELECT DISTINCT
									t1.StuEnrollId
								   ,t1.ClsSectionId
								   ,t1.MeetDate
								   ,DATENAME(dw,t1.MeetDate) AS WeekDay
								   ,t4.StartDate
								   ,t4.EndDate
								   ,t5.PeriodDescrip
								   ,t1.Actual
								   ,t1.Excused
								   ,CASE WHEN ( t1.Actual = 0
												AND t1.Excused = 0
											  ) THEN t1.Scheduled
										 ELSE CASE WHEN ( t1.Actual <> 9999.00
														  AND t1.Actual < t1.Scheduled
														  AND t1.Excused <> 1
														) THEN ( t1.Scheduled - t1.Actual )
												   ELSE 0
											  END
									END AS Absent
								   ,t1.Scheduled AS ScheduledMinutes
								   ,CASE WHEN ( t1.Actual > 0
												AND t1.Actual < t1.Scheduled
											  ) THEN ( t1.Scheduled - t1.Actual )
										 ELSE 0
									END AS TardyMinutes
								   ,t1.Tardy AS Tardy
								   ,t3.TrackTardies
								   ,t3.TardiesMakingAbsence
								   ,t3.PrgVerId
						  FROM      atClsSectAttendance t1
						  INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
						  INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
						  INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
						  INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
															 AND ( CONVERT(CHAR(10),t1.MeetDate,101) >= CONVERT(CHAR(10),t4.StartDate,101)
																   AND CONVERT(CHAR(10),t1.MeetDate,101) <= CONVERT(CHAR(10),t4.EndDate,101)
																 )
															 AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
						  INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
						  INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
						  INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
						  INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
						  INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
						  INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
						  WHERE     t2.StuEnrollId = @StuEnrollId
									AND ( AAUT1.UnitTypeDescrip IN ( 'None','Present Absent' )
										  OR AAUT2.UnitTypeDescrip IN ( 'None','Present Absent' )
										)
									AND t1.Actual <> 9999
						) dt
				ORDER BY StuEnrollId
					   ,MeetDate;
			OPEN GetAttendance_Cursor;
			FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,
				@PeriodDescrip,@Actual,@Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,
				@TardiesMakingAbsence,@PrgVerId,@rownumber;
			SET @ActualRunningPresentHours = 0;
			SET @ActualRunningPresentHours = 0;
			SET @ActualRunningAbsentHours = 0;
			SET @ActualRunningTardyHours = 0;
			SET @ActualRunningMakeupHours = 0;
			SET @intTardyBreakPoint = 0;
			SET @AdjustedRunningPresentHours = 0;
			SET @AdjustedRunningAbsentHours = 0;
			SET @ActualRunningScheduledDays = 0;
			SET @boolReset = 0;
			SET @MakeupHours = 0;
			WHILE @@FETCH_STATUS = 0
				BEGIN

					IF @PrevStuEnrollId <> @StuEnrollId
						BEGIN
							SET @ActualRunningPresentHours = 0;
							SET @ActualRunningAbsentHours = 0;
							SET @intTardyBreakPoint = 0;
							SET @ActualRunningTardyHours = 0;
							SET @AdjustedRunningPresentHours = 0;
							SET @AdjustedRunningAbsentHours = 0;
							SET @ActualRunningScheduledDays = 0;
							SET @MakeupHours = 0;
							SET @boolReset = 1;
						END;

					  
					-- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
					IF @Actual <> 9999.00
						BEGIN
							SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual + @Excused;
							SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual + @Excused;
							
							-- If there are make up hrs deduct that otherwise it will be added again in progress report
							IF ( @Actual > 0
								 AND @Actual > @ScheduledMinutes
								 AND @Actual <> 9999.00
							   )
								BEGIN
									SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual
																									- @ScheduledMinutes );
									SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual
																										- @ScheduledMinutes );
								END;
							  
							SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;                      
						END;
				   
					-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
					SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
					SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
			  
					-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
					IF ( @Actual > 0
						 AND @Actual < @ScheduledMinutes
						 AND @tardy = 1
					   )
						BEGIN
							SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
						END;
							
					-- Track how many days student has been tardy only when 
					-- program version requires to track tardy
					IF ( @tracktardies = 1
						 AND @tardy = 1
					   )
						BEGIN
							SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
						END;	    
		   
			
					-- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
					-- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
					-- when student is tardy the second time, that second occurance will be considered as
					-- absence
					-- Variable @intTardyBreakpoint tracks how many times the student was tardy
					-- Variable @tardiesMakingAbsence tracks the tardy rule
					IF ( @tracktardies = 1
						 AND @intTardyBreakPoint = @TardiesMakingAbsence
					   )
						BEGIN
							SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
							SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent )
								+ @ScheduledMinutes; --@TardyMinutes
							SET @intTardyBreakPoint = 0;
						END;
				   
				   -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
					IF ( @Actual > 0
						 AND @Actual > @ScheduledMinutes
						 AND @Actual <> 9999.00
					   )
						BEGIN
							SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
						END;
			  
					IF ( @tracktardies = 1 )
						BEGIN
							SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
						END;
					ELSE
						BEGIN
							SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
						END;
				
				---- PRINT @MeetDate
				---- PRINT @ActualRunningAbsentHours
				
					DELETE  FROM syStudentAttendanceSummary
					WHERE   StuEnrollId = @StuEnrollId
							AND ClsSectionId = @ClsSectionId
							AND StudentAttendedDate = @MeetDate;
					INSERT  INTO syStudentAttendanceSummary
							( StuEnrollId
							,ClsSectionId
							,StudentAttendedDate
							,ScheduledDays
							,ActualDays
							,ActualRunningScheduledDays
							,ActualRunningPresentDays
							,ActualRunningAbsentDays
							,ActualRunningMakeupDays
							,ActualRunningTardyDays
							,AdjustedPresentDays
							,AdjustedAbsentDays
							,AttendanceTrackType
							,ModUser
							,ModDate
							,tardiesmakingabsence
							)
					VALUES  ( @StuEnrollId
							,@ClsSectionId
							,@MeetDate
							,@ScheduledMinutes
							,@Actual
							,@ActualRunningScheduledDays
							,@ActualRunningPresentHours
							,@ActualRunningAbsentHours
							,ISNULL(@MakeupHours,0)
							,@ActualRunningTardyHours
							,@AdjustedPresentDaysComputed
							,@AdjustedRunningAbsentHours
							,'Post Attendance by Class Min'
							,'sa'
							,GETDATE()
							,@TardiesMakingAbsence
							);

				--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
					SET @PrevStuEnrollId = @StuEnrollId; 

					FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,
						@EndDate,@PeriodDescrip,@Actual,@Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,
						@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
				END;
			CLOSE GetAttendance_Cursor;
			DEALLOCATE GetAttendance_Cursor;
		END;			
*/
    IF @UnitTypeDescrip IN ( 'none','present absent' )
        AND @TrackSapAttendance = 'byclass'
        BEGIN
			---- PRINT 'Step1!!!!'
            DELETE  FROM syStudentAttendanceSummary
            WHERE   StuEnrollId = @StuEnrollId;
            DECLARE @boolReset BIT
               ,@MakeupHours DECIMAL(18,2)
               ,@AdjustedPresentDaysComputed DECIMAL(18,2);
            DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD
            FOR
                SELECT  dt.StuEnrollId
                       ,dt.ClsSectionId
                       ,dt.MeetDate
                       ,dt.WeekDay
                       ,dt.StartDate
                       ,dt.EndDate
                       ,dt.PeriodDescrip
                       ,dt.Actual
                       ,dt.Excused
                       ,dt.Absent
                       ,dt.ScheduledMinutes
                       ,dt.TardyMinutes
                       ,dt.Tardy
                       ,dt.TrackTardies
                       ,dt.TardiesMakingAbsence
                       ,dt.PrgVerId
                       ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                FROM    (
                          SELECT DISTINCT
                                    t1.StuEnrollId
                                   ,t1.ClsSectionId
                                   ,t1.MeetDate
                                   ,DATENAME(dw,t1.MeetDate) AS WeekDay
                                   ,t4.StartDate
                                   ,t4.EndDate
                                   ,t5.PeriodDescrip
                                   ,t1.Actual
                                   ,t1.Excused
                                   ,CASE WHEN (
                                                t1.Actual = 0
                                                AND t1.Excused = 0
                                              ) THEN t1.Scheduled
                                         ELSE CASE WHEN (
                                                          t1.Actual <> 9999.00
                                                          AND t1.Actual < t1.Scheduled
														  --AND t1.Excused <> 1
                                                        ) THEN ( t1.Scheduled - t1.Actual )
                                                   ELSE 0
                                              END
                                    END AS Absent
                                   ,t1.Scheduled AS ScheduledMinutes
                                   ,CASE WHEN (
                                                t1.Actual > 0
                                                AND t1.Actual < t1.Scheduled
                                              ) THEN ( t1.Scheduled - t1.Actual )
                                         ELSE 0
                                    END AS TardyMinutes
                                   ,t1.Tardy AS Tardy
                                   ,t3.TrackTardies
                                   ,t3.TardiesMakingAbsence
                                   ,t3.PrgVerId
                          FROM      atClsSectAttendance t1
                          INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                          INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                          INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                          INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                             AND (
                                                                   CONVERT(CHAR(10),t1.MeetDate,101) >= CONVERT(CHAR(10),t4.StartDate,101)
                                                                   AND CONVERT(CHAR(10),t1.MeetDate,101) <= CONVERT(CHAR(10),t4.EndDate,101)
                                                                 )
                                                             AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                          INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                          INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                          INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                          INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                          INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                          INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                          WHERE     t2.StuEnrollId = @StuEnrollId
                                    AND (
                                          AAUT1.UnitTypeDescrip IN ( 'None','Present Absent' )
                                          OR AAUT2.UnitTypeDescrip IN ( 'None','Present Absent' )
                                        )
                                    AND t1.Actual <> 9999
                        ) dt
                ORDER BY StuEnrollId
                       ,MeetDate;
            OPEN GetAttendance_Cursor;
            FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,@Excused,@Absent,
                @ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
            SET @ActualRunningPresentHours = 0;
            SET @ActualRunningPresentHours = 0;
            SET @ActualRunningAbsentHours = 0;
            SET @ActualRunningTardyHours = 0;
            SET @ActualRunningMakeupHours = 0;
            SET @intTardyBreakPoint = 0;
            SET @AdjustedRunningPresentHours = 0;
            SET @AdjustedRunningAbsentHours = 0;
            SET @ActualRunningScheduledDays = 0;
            SET @boolReset = 0;
            SET @MakeupHours = 0;
            WHILE @@FETCH_STATUS = 0
                BEGIN

                    IF @PrevStuEnrollId <> @StuEnrollId
                        BEGIN
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @MakeupHours = 0;
                            SET @boolReset = 1;
                        END;

					  
					  -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                    IF @Actual <> 9999.00
                        BEGIN
					-- Commented by Balaji on 2.6.2015 as Excused Absence is still considered an absence
					-- @Excused is not added to Present Hours
                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;  -- + @Excused
                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; -- + @Excused
					
					-- If there are make up hrs deduct that otherwise it will be added again in progress report
                            IF (
                                 @Actual > 0
                                 AND @Actual > @ScheduledMinutes
                                 AND @Actual <> 9999.00
                               )
                                BEGIN
                                    SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                END;
					  
                            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;                      
                        END;
				   
					-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                    SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
			  
					-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                    IF (
                         @Actual > 0
                         AND @Actual < @ScheduledMinutes
                         AND @tardy = 1
                       )
                        BEGIN
                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                        END;
                    ELSE
                        IF (
                             @Actual = 1
                             AND @Actual = @ScheduledMinutes
                             AND @tardy = 1
                           )
                            BEGIN
                                SET @ActualRunningTardyHours += 1;
                            END;
							
					-- Track how many days student has been tardy only when 
					-- program version requires to track tardy
                    IF (
                         @tracktardies = 1
                         AND @tardy = 1
                       )
                        BEGIN
                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                        END;	    
		   
			
					-- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
					-- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
					-- when student is tardy the second time, that second occurance will be considered as
					-- absence
					-- Variable @intTardyBreakpoint tracks how many times the student was tardy
					-- Variable @tardiesMakingAbsence tracks the tardy rule
                    IF (
                         @tracktardies = 1
                         AND @TardiesMakingAbsence > 0
                         AND @intTardyBreakPoint = @TardiesMakingAbsence
                       )
                        BEGIN
                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
                            SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                            SET @intTardyBreakPoint = 0;
                        END;
				   
				   -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                    IF (
                         @Actual > 0
                         AND @Actual > @ScheduledMinutes
                         AND @Actual <> 9999.00
                       )
                        BEGIN
                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                        END;
			  
                    IF ( @tracktardies = 1 )
                        BEGIN
                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                        END;
                    ELSE
                        BEGIN
                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                        END;
				
				---- PRINT @MeetDate
				---- PRINT @ActualRunningAbsentHours
				
                    DELETE  FROM syStudentAttendanceSummary
                    WHERE   StuEnrollId = @StuEnrollId
                            AND ClsSectionId = @ClsSectionId
                            AND StudentAttendedDate = @MeetDate;
                    INSERT  INTO syStudentAttendanceSummary
                            (
                             StuEnrollId
                            ,ClsSectionId
                            ,StudentAttendedDate
                            ,ScheduledDays
                            ,ActualDays
                            ,ActualRunningScheduledDays
                            ,ActualRunningPresentDays
                            ,ActualRunningAbsentDays
                            ,ActualRunningMakeupDays
                            ,ActualRunningTardyDays
                            ,AdjustedPresentDays
                            ,AdjustedAbsentDays
                            ,AttendanceTrackType
                            ,ModUser
                            ,ModDate
                            ,tardiesmakingabsence
							)
                    VALUES  (
                             @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@ScheduledMinutes
                            ,@Actual
                            ,@ActualRunningScheduledDays
                            ,@ActualRunningPresentHours
                            ,@ActualRunningAbsentHours
                            ,ISNULL(@MakeupHours,0)
                            ,@ActualRunningTardyHours
                            ,@AdjustedPresentDaysComputed
                            ,@AdjustedRunningAbsentHours
                            ,'Post Attendance by Class Min'
                            ,'sa'
                            ,GETDATE()
                            ,@TardiesMakingAbsence
							);

				--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                    SET @PrevStuEnrollId = @StuEnrollId; 

                    FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,@Excused,
                        @Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                END;
            CLOSE GetAttendance_Cursor;
            DEALLOCATE GetAttendance_Cursor;
			
            DECLARE @MyTardyTable TABLE
                (
                 ClsSectionId UNIQUEIDENTIFIER
                ,TardiesMakingAbsence INT
                ,AbsentHours DECIMAL(18,2)
                ); 

            INSERT  INTO @MyTardyTable
                    SELECT  ClsSectionId
                           ,PV.TardiesMakingAbsence
                           ,CASE WHEN ( COUNT(*) >= PV.TardiesMakingAbsence ) THEN ( COUNT(*) / PV.TardiesMakingAbsence )
                                 ELSE 0
                            END AS AbsentHours 
--Count(*) as NumberofTimesTardy 
                    FROM    syStudentAttendanceSummary SAS
                    INNER JOIN arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    WHERE   SE.StuEnrollId = @StuEnrollId
                            AND SAS.IsTardy = 1
                            AND PV.TrackTardies = 1
                    GROUP BY ClsSectionId
                           ,PV.TardiesMakingAbsence; 

--Drop table @MyTardyTable
            DECLARE @TotalTardyAbsentDays DECIMAL(18,2);
            SET @TotalTardyAbsentDays = (
                                          SELECT    ISNULL(SUM(AbsentHours),0)
                                          FROM      @MyTardyTable
                                        );

--Print @TotalTardyAbsentDays

            UPDATE  syStudentAttendanceSummary
            SET     AdjustedPresentDays = AdjustedPresentDays - @TotalTardyAbsentDays
                   ,AdjustedAbsentDays = AdjustedAbsentDays + @TotalTardyAbsentDays
            WHERE   StuEnrollId = @StuEnrollId
                    AND StudentAttendedDate = (
                                                SELECT TOP 1
                                                        StudentAttendedDate
                                                FROM    syStudentAttendanceSummary
                                                WHERE   StuEnrollId = @StuEnrollId
                                                ORDER BY StudentAttendedDate DESC
                                              );
			
        END;	
		
		-- By Class and Attendance Unit Type - Minutes and Clock Hour
		
    IF @UnitTypeDescrip IN ( 'minutes','clock hours' )
        AND @TrackSapAttendance = 'byclass'
        BEGIN
            DELETE  FROM syStudentAttendanceSummary
            WHERE   StuEnrollId = @StuEnrollId;
				
            DECLARE GetAttendance_Cursor CURSOR
            FOR
                SELECT  *
                       ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                FROM    (
                          SELECT DISTINCT
                                    t1.StuEnrollId
                                   ,t1.ClsSectionId
                                   ,t1.MeetDate
                                   ,DATENAME(dw,t1.MeetDate) AS WeekDay
                                   ,t4.StartDate
                                   ,t4.EndDate
                                   ,t5.PeriodDescrip
                                   ,t1.Actual
                                   ,t1.Excused
                                   ,CASE WHEN (
                                                t1.Actual = 0
                                                AND t1.Excused = 0
                                              ) THEN t1.Scheduled
                                         ELSE CASE WHEN (
                                                          t1.Actual <> 9999.00
                                                          AND t1.Actual < t1.Scheduled
                                                        ) THEN ( t1.Scheduled - t1.Actual )
                                                   ELSE 0
                                              END
                                    END AS Absent
                                   ,t1.Scheduled AS ScheduledMinutes
                                   ,CASE WHEN (
                                                t1.Actual > 0
                                                AND t1.Actual < t1.Scheduled
                                              ) THEN ( t1.Scheduled - t1.Actual )
                                         ELSE 0
                                    END AS TardyMinutes
                                   ,t1.Tardy AS Tardy
                                   ,t3.TrackTardies
                                   ,t3.TardiesMakingAbsence
                                   ,t3.PrgVerId
                          FROM      atClsSectAttendance t1
                          INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                          INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                          INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                          INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                             AND (
                                                                   CONVERT(CHAR(10),t1.MeetDate,101) >= CONVERT(CHAR(10),t4.StartDate,101)
                                                                   AND CONVERT(CHAR(10),t1.MeetDate,101) <= CONVERT(CHAR(10),t4.EndDate,101)
                                                                 )
                                                             AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                          INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                          INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                          INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                          INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                          INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                          INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                          WHERE     t2.StuEnrollId = @StuEnrollId
                                    AND (
                                          AAUT1.UnitTypeDescrip IN ( 'Minutes','Clock Hours' )
                                          OR AAUT2.UnitTypeDescrip IN ( 'Minutes','Clock Hours' )
                                        )
                                    AND t1.Actual <> 9999
                          UNION
                          SELECT DISTINCT
                                    t1.StuEnrollId
                                   ,NULL AS ClsSectionId
                                   ,t1.RecordDate AS MeetDate
                                   ,DATENAME(dw,t1.RecordDate) AS WeekDay
                                   ,NULL AS StartDate
                                   ,NULL AS EndDate
                                   ,NULL AS PeriodDescrip
                                   ,t1.ActualHours
                                   ,NULL AS Excused
                                   ,CASE WHEN ( t1.ActualHours = 0 ) THEN t1.SchedHours
                                         ELSE 0
								 --ELSE 
									--Case when (t1.ActualHours <> 9999.00 and t1.ActualHours < t1.SchedHours)
									--		THEN (t1.SchedHours - t1.ActualHours)
									--		ELSE 
									--			0
									--		End
                                    END AS Absent
                                   ,t1.SchedHours AS ScheduledMinutes
                                   ,CASE WHEN (
                                                t1.ActualHours <> 9999.00
                                                AND t1.ActualHours > 0
                                                AND t1.ActualHours < t1.SchedHours
                                              ) THEN ( t1.SchedHours - t1.ActualHours )
                                         ELSE 0
                                    END AS TardyMinutes
                                   ,NULL AS Tardy
                                   ,t3.TrackTardies
                                   ,t3.TardiesMakingAbsence
                                   ,t3.PrgVerId
                          FROM      arStudentClockAttendance t1
                          INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                          INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                          WHERE     t2.StuEnrollId = @StuEnrollId
                                    AND t1.Converted = 1
                                    AND t1.ActualHours <> 9999
                        ) dt
                ORDER BY StuEnrollId
                       ,MeetDate;
            OPEN GetAttendance_Cursor;
            FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,@Excused,@Absent,
                @ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
            SET @ActualRunningPresentHours = 0;
            SET @ActualRunningPresentHours = 0;
            SET @ActualRunningAbsentHours = 0;
            SET @ActualRunningTardyHours = 0;
            SET @ActualRunningMakeupHours = 0;
            SET @intTardyBreakPoint = 0;
            SET @AdjustedRunningPresentHours = 0;
            SET @AdjustedRunningAbsentHours = 0;
            SET @ActualRunningScheduledDays = 0;
            SET @boolReset = 0;
            SET @MakeupHours = 0;
            WHILE @@FETCH_STATUS = 0
                BEGIN

                    IF @PrevStuEnrollId <> @StuEnrollId
                        BEGIN
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @MakeupHours = 0;
                            SET @boolReset = 1;
                        END;

					  
			-- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                    IF @Actual <> 9999.00
                        BEGIN
                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; 
					-- If there are make up hrs deduct that otherwise it will be added again in progress report
                            IF (
                                 @Actual > 0
                                 AND @Actual > @ScheduledMinutes
                                 AND @Actual <> 9999.00
                               )
                                BEGIN
                                    SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                END; 
                            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;                      
                        END;
		   
			-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                    SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
	  
			-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                    IF (
                         @Actual > 0
                         AND @Actual < @ScheduledMinutes
                         AND @tardy = 1
                       )
                        BEGIN
                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                        END;
					
			-- Track how many days student has been tardy only when 
			-- program version requires to track tardy
                    IF (
                         @tracktardies = 1
                         AND @tardy = 1
                       )
                        BEGIN
                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                        END;	    
		   
			
			-- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
			-- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
			-- when student is tardy the second time, that second occurance will be considered as
			-- absence
			-- Variable @intTardyBreakpoint tracks how many times the student was tardy
			-- Variable @tardiesMakingAbsence tracks the tardy rule
                    IF (
                         @tracktardies = 1
                         AND @TardiesMakingAbsence > 0
                         AND @intTardyBreakPoint = @TardiesMakingAbsence
                       )
                        BEGIN
                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                            SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                            SET @intTardyBreakPoint = 0;
                        END;
		   
		   -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                    IF (
                         @Actual > 0
                         AND @Actual > @ScheduledMinutes
                         AND @Actual <> 9999.00
                       )
                        BEGIN
                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                        END;
	  
	 
                    IF ( @tracktardies = 1 )
                        BEGIN
                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                        END;
                    ELSE
                        BEGIN
                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                        END;
		
		
		
		
                    DELETE  FROM syStudentAttendanceSummary
                    WHERE   StuEnrollId = @StuEnrollId
                            AND ClsSectionId = @ClsSectionId
                            AND StudentAttendedDate = @MeetDate;
                    INSERT  INTO syStudentAttendanceSummary
                            (
                             StuEnrollId
                            ,ClsSectionId
                            ,StudentAttendedDate
                            ,ScheduledDays
                            ,ActualDays
                            ,ActualRunningScheduledDays
                            ,ActualRunningPresentDays
                            ,ActualRunningAbsentDays
                            ,ActualRunningMakeupDays
                            ,ActualRunningTardyDays
                            ,AdjustedPresentDays
                            ,AdjustedAbsentDays
                            ,AttendanceTrackType
                            ,ModUser
                            ,ModDate
							)
                    VALUES  (
                             @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@ScheduledMinutes
                            ,@Actual
                            ,@ActualRunningScheduledDays
                            ,@ActualRunningPresentHours
                            ,@ActualRunningAbsentHours
                            ,ISNULL(@MakeupHours,0)
                            ,@ActualRunningTardyHours
                            ,@AdjustedPresentDaysComputed
                            ,@AdjustedRunningAbsentHours
                            ,'Post Attendance by Class Min'
                            ,'sa'
                            ,GETDATE()
                            );

                    UPDATE  syStudentAttendanceSummary
                    SET     tardiesmakingabsence = @TardiesMakingAbsence
                    WHERE   StuEnrollId = @StuEnrollId;
                    SET @PrevStuEnrollId = @StuEnrollId; 

                    FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,@Excused,
                        @Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                END;
            CLOSE GetAttendance_Cursor;
            DEALLOCATE GetAttendance_Cursor;
        END;
--Select * from arAttUnitType

			
-- By Minutes/Day
-- remove clock hour from here
    IF @UnitTypeDescrip IN ( 'minutes' )
        AND @TrackSapAttendance = 'byday'
	-- -- PRINT GETDATE();	
	---- PRINT @UnitTypeId;
	---- PRINT @TrackSapAttendance;
        BEGIN
			--Delete from syStudentAttendanceSummary where StuEnrollId=@StuEnrollId

            DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD
            FOR
                SELECT  t1.StuEnrollId
                       ,NULL AS ClsSectionId
                       ,t1.RecordDate AS MeetDate
                       ,t1.ActualHours
                       ,t1.SchedHours AS ScheduledMinutes
                       ,CASE WHEN (
                                    (
                                      t1.SchedHours >= 1
                                      AND t1.SchedHours NOT IN ( 999,9999 )
                                    )
                                    AND t1.ActualHours = 0
                                  ) THEN t1.SchedHours
                             ELSE 0
                        END AS Absent
                       ,t1.isTardy
                       ,(
                          SELECT    ISNULL(SUM(SchedHours),0)
                          FROM      arStudentClockAttendance
                          WHERE     StuEnrollId = t1.StuEnrollId
                                    AND RecordDate <= t1.RecordDate
                                    AND (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999,9999 )
                                          AND t1.ActualHours NOT IN ( 999,9999 )
                                        )
                        ) AS ActualRunningScheduledHours
                       ,(
                          SELECT    SUM(ActualHours)
                          FROM      arStudentClockAttendance
                          WHERE     StuEnrollId = t1.StuEnrollId
                                    AND RecordDate <= t1.RecordDate
                                    AND (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999,9999 )
                                        )
                                    AND ActualHours >= 1
                                    AND ActualHours NOT IN ( 999,9999 )
                        ) AS ActualRunningPresentHours
                       ,(
                          SELECT    COUNT(ActualHours)
                          FROM      arStudentClockAttendance
                          WHERE     StuEnrollId = t1.StuEnrollId
                                    AND RecordDate <= t1.RecordDate
                                    AND (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999,9999 )
                                        )
                                    AND ActualHours = 0
                                    AND ActualHours NOT IN ( 999,9999 )
                        ) AS ActualRunningAbsentHours
                       ,(
                          SELECT    SUM(ActualHours)
                          FROM      arStudentClockAttendance
                          WHERE     StuEnrollId = t1.StuEnrollId
                                    AND RecordDate <= t1.RecordDate
                                    AND SchedHours = 0
                                    AND ActualHours >= 1
                                    AND ActualHours NOT IN ( 999,9999 )
                        ) AS ActualRunningMakeupHours
                       ,(
                          SELECT    SUM(SchedHours - ActualHours)
                          FROM      arStudentClockAttendance
                          WHERE     StuEnrollId = t1.StuEnrollId
                                    AND RecordDate <= t1.RecordDate
                                    AND (
                                          (
                                            t1.SchedHours >= 1
                                            AND t1.SchedHours NOT IN ( 999,9999 )
                                          )
                                          AND ActualHours >= 1
                                          AND ActualHours NOT IN ( 999,9999 )
                                        )
                                    AND isTardy = 1
                        ) AS ActualRunningTardyHours
                       ,t3.TrackTardies
                       ,t3.TardiesMakingAbsence
                       ,t3.PrgVerId
                       ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
                FROM    arStudentClockAttendance t1
                INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
			--inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                WHERE   AAUT1.UnitTypeDescrip IN ( 'Minutes' )
                        AND t2.StuEnrollId = @StuEnrollId
                        AND t1.ActualHours <> 9999.00
                ORDER BY t1.StuEnrollId
                       ,MeetDate;
            OPEN GetAttendance_Cursor;
--Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
            FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,
                @ActualRunningScheduledHours,@ActualRunningPresentHours,@ActualRunningAbsentHours,@ActualRunningMakeupHours,@ActualRunningTardyHours,
                @tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;

            SET @ActualRunningPresentHours = 0;
            SET @ActualRunningPresentHours = 0;
            SET @ActualRunningAbsentHours = 0;
            SET @ActualRunningTardyHours = 0;
            SET @ActualRunningMakeupHours = 0;
            SET @intTardyBreakPoint = 0;
            SET @AdjustedRunningPresentHours = 0;
            SET @AdjustedRunningAbsentHours = 0;
            SET @ActualRunningScheduledDays = 0;
            WHILE @@FETCH_STATUS = 0
                BEGIN

                    IF @PrevStuEnrollId <> @StuEnrollId
                        BEGIN
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                        END;

                    IF (
                         @ScheduledMinutes >= 1
                         AND (
                               @Actual <> 9999
                               AND @Actual <> 999
                             )
                         AND (
                               @ScheduledMinutes <> 9999
                               AND @Actual <> 999
                             )
                       )
                        BEGIN
                            SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays,0) + ISNULL(@ScheduledMinutes,0);
                        END;
                    ELSE
                        BEGIN
                            SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays,0); 
                        END;
	   
                    IF (
                         @Actual <> 9999
                         AND @Actual <> 999
                       )
                        BEGIN
                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                        END;
                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;

                    IF (
                         @Actual > 0
                         AND @Actual < @ScheduledMinutes
                       )
                        BEGIN
                            SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                        END;
			-- Make up hours
		--sched=5, Actual =7, makeup= 2,ab = 0
		--sched=0, Actual =7, makeup= 7,ab = 0
                    IF (
                         @Actual > 0
                         AND @ScheduledMinutes > 0
                         AND @Actual > @ScheduledMinutes
                         AND (
                               @Actual <> 9999
                               AND @Actual <> 999
                             )
                       )
                        BEGIN
                            SET @ActualRunningMakeupHours = @ActualRunningMakeupHours + ( @Actual - @ScheduledMinutes );
                        END;

		
                    IF (
                         @Actual <> 9999
                         AND @Actual <> 999
                       )
                        BEGIN
                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                        END;	
                    IF (
                         @Actual = 0
                         AND @ScheduledMinutes >= 1
                         AND @ScheduledMinutes NOT IN ( 999,9999 )
                       )
                        BEGIN
                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                        END;
		-- Absent hours
		--1. sched = 5, Actual = 2 then Ab = (5-3) = 2
                    IF (
                         @Absent = 0
                         AND @ActualRunningAbsentHours > 0
                         AND ( @Actual < @ScheduledMinutes )
                       )
                        BEGIN
                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + ( @ScheduledMinutes - @Actual );	
                        END;
                    IF (
                         @Actual > 0
                         AND @Actual < @ScheduledMinutes
                         AND (
                               @Actual <> 9999.00
                               AND @Actual <> 999.00
                             )
                       )
                        BEGIN
                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                        END;
	
                    IF @tracktardies = 1
                        AND (
                              @TardyMinutes > 0
                              OR @IsTardy = 1
                            )
                        BEGIN
                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                        END;	    
                    IF (
                         @tracktardies = 1
                         AND @TardiesMakingAbsence > 0
                         AND @intTardyBreakPoint = @TardiesMakingAbsence
                       )
                        BEGIN
                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Actual; --@TardyMinutes
                            SET @intTardyBreakPoint = 0;
                        END;
                    DELETE  FROM syStudentAttendanceSummary
                    WHERE   StuEnrollId = @StuEnrollId
                            AND StudentAttendedDate = @MeetDate;
                    INSERT  INTO syStudentAttendanceSummary
                            (
                             StuEnrollId
                            ,ClsSectionId
                            ,StudentAttendedDate
                            ,ScheduledDays
                            ,ActualDays
                            ,ActualRunningScheduledDays
                            ,ActualRunningPresentDays
                            ,ActualRunningAbsentDays
                            ,ActualRunningMakeupDays
                            ,ActualRunningTardyDays
                            ,AdjustedPresentDays
                            ,AdjustedAbsentDays
                            ,AttendanceTrackType
                            ,ModUser
                            ,ModDate
							)
                    VALUES  (
                             @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@ScheduledMinutes
                            ,@Actual
                            ,@ActualRunningScheduledDays
                            ,@ActualRunningPresentHours
                            ,@ActualRunningAbsentHours
                            ,@ActualRunningMakeupHours
                            ,@ActualRunningTardyHours
                            ,@AdjustedRunningPresentHours
                            ,@AdjustedRunningAbsentHours
                            ,'Post Attendance by Class'
                            ,'sa'
                            ,GETDATE()
                            );
				
                    UPDATE  syStudentAttendanceSummary
                    SET     tardiesmakingabsence = @TardiesMakingAbsence
                    WHERE   StuEnrollId = @StuEnrollId;
			--end
                    SET @PrevStuEnrollId = @StuEnrollId; 
                    FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,
                        @ActualRunningScheduledHours,@ActualRunningPresentHours,@ActualRunningAbsentHours,@ActualRunningMakeupHours,@ActualRunningTardyHours,
                        @tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                END;
            CLOSE GetAttendance_Cursor;
            DEALLOCATE GetAttendance_Cursor;
        END;

	---- PRINT 'Does it get to Clock Hour/PA';
	---- PRINT @UnitTypeId;
	---- PRINT @TrackSapAttendance;
	-- By Day and PA, Clock Hour
    IF @UnitTypeDescrip IN ( 'present absent','clock hours' )
        AND @TrackSapAttendance = 'byday'
        BEGIN
			---- PRINT 'By Day inside day';
            DECLARE GetAttendance_Cursor CURSOR
            FOR
                SELECT  t1.StuEnrollId
                       ,t1.RecordDate
                       ,t1.ActualHours
                       ,t1.SchedHours
                       ,CASE WHEN (
                                    (
                                      t1.SchedHours >= 1
                                      AND t1.SchedHours NOT IN ( 999,9999 )
                                    )
                                    AND t1.ActualHours = 0
                                  ) THEN t1.SchedHours
                             ELSE 0
                        END AS Absent
                       ,t1.isTardy
                       ,t3.TrackTardies
                       ,t3.TardiesMakingAbsence
                FROM    arStudentClockAttendance t1
                INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
			--inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                WHERE   -- Unit Types: Present Absent and Clock Hour
                        AAUT1.UnitTypeDescrip IN ( 'present absent','clock hours' )
                        AND ActualHours <> 9999.00
                        AND t2.StuEnrollId = @StuEnrollId
                ORDER BY t1.StuEnrollId
                       ,t1.RecordDate;
            OPEN GetAttendance_Cursor;
--Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
            FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,@tracktardies,@TardiesMakingAbsence;

            SET @ActualRunningPresentHours = 0;
            SET @ActualRunningPresentHours = 0;
            SET @ActualRunningAbsentHours = 0;
            SET @ActualRunningTardyHours = 0;
            SET @ActualRunningMakeupHours = 0;
            SET @intTardyBreakPoint = 0;
            SET @AdjustedRunningPresentHours = 0;
            SET @AdjustedRunningAbsentHours = 0;
            SET @ActualRunningScheduledDays = 0;
            SET @MakeupHours = 0;
            WHILE @@FETCH_STATUS = 0
                BEGIN

                    IF @PrevStuEnrollId <> @StuEnrollId
                        BEGIN
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @MakeupHours = 0;
                        END;
	   
                    IF (
                         @Actual <> 9999
                         AND @Actual <> 999
                       )
                        BEGIN
                            SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays,0) + @ScheduledMinutes;
                            SET @ActualRunningPresentHours = ISNULL(@ActualRunningPresentHours,0) + @Actual;
                            SET @AdjustedRunningPresentHours = ISNULL(@AdjustedRunningPresentHours,0) + @Actual;
                        END;
                    SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours,0) + @Absent;
                    IF (
                         @Actual = 0
                         AND @ScheduledMinutes >= 1
                         AND @ScheduledMinutes NOT IN ( 999,9999 )
                       )
                        BEGIN
                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                        END;
		
		-- NWH 
		-- If Scheduled = 3.0 hrs and Actual = 1.0 Then 2 hrs is considered absent
                    IF (
                         @ScheduledMinutes >= 1
                         AND @ScheduledMinutes NOT IN ( 999,9999 )
                         AND @Actual > 0
                         AND ( @Actual < @ScheduledMinutes )
                       )
                        BEGIN
                            SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours,0) + ( @ScheduledMinutes - @Actual );
                            SET @AdjustedRunningAbsentHours = ISNULL(@AdjustedRunningAbsentHours,0) + ( @ScheduledMinutes - @Actual );
                        END; 
		
                    IF (
                         @tracktardies = 1
                         AND @IsTardy = 1
                       )
                        BEGIN
                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + 1;
                        END;
		 --commented by balaji on 10/22/2012 as report (rdl) doesn't add days attended and make up days
		 ---- If there are make up hrs deduct that otherwise it will be added again in progress report
                    IF (
                         @Actual > 0
                         AND @Actual > @ScheduledMinutes
                         AND @Actual <> 9999.00
                       )
                        BEGIN
                            SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                        END;
			
                    IF @tracktardies = 1
                        AND (
                              @TardyMinutes > 0
                              OR @IsTardy = 1
                            )
                        BEGIN
                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                        END;	    
		
		
		
		-- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                    IF (
                         @Actual > 0
                         AND @Actual > @ScheduledMinutes
                         AND @Actual <> 9999.00
                       )
                        BEGIN
                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                        END;
		
                    IF (
                         @tracktardies = 1
                         AND @TardiesMakingAbsence > 0
                         AND @intTardyBreakPoint = @TardiesMakingAbsence
                       )
                        BEGIN
                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                            SET @intTardyBreakPoint = 0;
                        END;
				
                    DELETE  FROM syStudentAttendanceSummary
                    WHERE   StuEnrollId = @StuEnrollId
                            AND StudentAttendedDate = @MeetDate;
                    INSERT  INTO syStudentAttendanceSummary
                            (
                             StuEnrollId
                            ,ClsSectionId
                            ,StudentAttendedDate
                            ,ScheduledDays
                            ,ActualDays
                            ,ActualRunningScheduledDays
                            ,ActualRunningPresentDays
                            ,ActualRunningAbsentDays
                            ,ActualRunningMakeupDays
                            ,ActualRunningTardyDays
                            ,AdjustedPresentDays
                            ,AdjustedAbsentDays
                            ,AttendanceTrackType
                            ,ModUser
                            ,ModDate
                            ,tardiesmakingabsence
							)
                    VALUES  (
                             @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,ISNULL(@ScheduledMinutes,0)
                            ,@Actual
                            ,ISNULL(@ActualRunningScheduledDays,0)
                            ,ISNULL(@ActualRunningPresentHours,0)
                            ,ISNULL(@ActualRunningAbsentHours,0)
                            ,ISNULL(@MakeupHours,0)
                            ,ISNULL(@ActualRunningTardyHours,0)
                            ,ISNULL(@AdjustedRunningPresentHours,0)
                            ,ISNULL(@AdjustedRunningAbsentHours,0)
                            ,'Post Attendance by Class'
                            ,'sa'
                            ,GETDATE()
                            ,@TardiesMakingAbsence
							);

		--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
		--end
                    SET @PrevStuEnrollId = @StuEnrollId; 
                    FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,@tracktardies,
                        @TardiesMakingAbsence;
                END;
            CLOSE GetAttendance_Cursor;
            DEALLOCATE GetAttendance_Cursor;
        END;		
--end

-- Based on grade rounding round the final score and current score
--Declare @PrevTermId uniqueidentifier,@PrevReqId uniqueidentifier,@RowCount int,@FinalScore decimal(18,4),@CurrentScore decimal(18,4)
--declare @ReqId uniqueidentifier,@CourseCodeDescrip varchar(100),@FinalGrade varchar(10),@sysComponentTypeId int
--declare @CreditsAttempted decimal(18,4),@Grade varchar(10),@IsPass bit,@IsCreditsAttempted bit,@IsCreditsEarned bit
--declare @IsInGPA bit,@FinAidCredits decimal(18,4),@CurrentGrade varchar(10),@CourseCredits decimal(18,4)
    DECLARE @GPA DECIMAL(18,4);

    DECLARE @PrevReqId UNIQUEIDENTIFIER
       ,@PrevTermId UNIQUEIDENTIFIER
       ,@CreditsAttempted DECIMAL(18,2)
       ,@CreditsEarned DECIMAL(18,2);
    DECLARE @reqid UNIQUEIDENTIFIER
       ,@CourseCodeDescrip VARCHAR(50)
       ,@FinalGrade VARCHAR(50)
       ,@FinalScore DECIMAL(18,2)
       ,@Grade VARCHAR(50);
    DECLARE @IsPass BIT
       ,@IsCreditsAttempted BIT
       ,@IsCreditsEarned BIT
       ,@CurrentScore DECIMAL(18,2)
       ,@CurrentGrade VARCHAR(10)
       ,@FinalGPA DECIMAL(18,2);
    DECLARE @sysComponentTypeId INT
       ,@RowCount INT;
    DECLARE @IsInGPA BIT;
    DECLARE @CourseCredits DECIMAL(18,2);
    DECLARE @FinAidCreditsEarned DECIMAL(18,2)
       ,@FinAidCredits DECIMAL(18,2);




    DECLARE GetCreditsSummary_Cursor CURSOR
    FOR
        SELECT	DISTINCT
                SE.StuEnrollId
               ,T.TermId
               ,T.TermDescrip
               ,T.StartDate
               ,R.ReqId
               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
               ,RES.Score AS FinalScore
               ,RES.GrdSysDetailId AS FinalGrade
               ,GCT.SysComponentTypeId
               ,R.Credits AS CreditsAttempted
               ,CS.ClsSectionId
               ,GSD.Grade
               ,GSD.IsPass
               ,GSD.IsCreditsAttempted
               ,GSD.IsCreditsEarned
               ,SE.PrgVerId
               ,GSD.IsInGPA
               ,R.FinAidCredits AS FinAidCredits
        FROM    arStuEnrollments SE
        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
        INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
        INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
        INNER JOIN arTerm T ON CS.TermId = T.TermId
        INNER JOIN arReqs R ON CS.ReqId = R.ReqId
        LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
        LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
        LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
        LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
        WHERE   SE.StuEnrollId = @StuEnrollId
        ORDER BY T.StartDate
               ,T.TermDescrip
               ,R.ReqId; 

    DECLARE @varGradeRounding VARCHAR(3);
    DECLARE @roundfinalscore DECIMAL(18,4);
    SET @varGradeRounding = (
                              SELECT    Value
                              FROM      syConfigAppSetValues
                              WHERE     SettingId = 45
                            );

    OPEN GetCreditsSummary_Cursor;
    SET @PrevStuEnrollId = NULL;
    SET @PrevTermId = NULL;
    SET @PrevReqId = NULL;
    SET @RowCount = 0;
    FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,@FinalGrade,
        @sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,@FinAidCredits;
    WHILE @@FETCH_STATUS = 0
        BEGIN
			
			
            SET @CurrentScore = (
                                  SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                 ELSE NULL
                                            END AS CurrentScore
                                  FROM      (
                                              SELECT    InstrGrdBkWgtDetailId
                                                       ,Code
                                                       ,Descrip
                                                       ,Weight AS GradeBookWeight
                                                       ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                             ELSE 0
                                                        END AS ActualWeight
                                              FROM      (
                                                          SELECT    C.InstrGrdBkWgtDetailId
                                                                   ,D.Code
                                                                   ,D.Descrip
                                                                   ,ISNULL(C.Weight,0) AS Weight
                                                                   ,C.Number AS MinNumber
                                                                   ,C.GrdPolicyId
                                                                   ,C.Parameter AS Param
                                                                   ,X.GrdScaleId
                                                                   ,SUM(GR.Score) AS Score
                                                                   ,COUNT(D.Descrip) AS NumberOfComponents
                                                          FROM      (
                                                                      SELECT DISTINCT TOP 1
                                                                                A.InstrGrdBkWgtId
                                                                               ,A.EffectiveDate
                                                                               ,B.GrdScaleId
                                                                      FROM      arGrdBkWeights A
                                                                               ,arClassSections B
                                                                      WHERE     A.ReqId = B.ReqId
                                                                                AND A.EffectiveDate <= B.StartDate
                                                                                AND B.ClsSectionId = @ClsSectionId
                                                                      ORDER BY  A.EffectiveDate DESC
                                                                    ) X
                                                                   ,arGrdBkWgtDetails C
                                                                   ,arGrdComponentTypes D
                                                                   ,arGrdBkResults GR
                                                          WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                    AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                    AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                    AND D.SysComponentTypeId NOT IN ( 500,503 )
                                                                    AND GR.StuEnrollId = @StuEnrollId
                                                                    AND GR.ClsSectionId = @ClsSectionId
                                                                    AND GR.Score IS NOT NULL
                                                          GROUP BY  C.InstrGrdBkWgtDetailId
                                                                   ,D.Code
                                                                   ,D.Descrip
                                                                   ,C.Weight
                                                                   ,C.Number
                                                                   ,C.GrdPolicyId
                                                                   ,C.Parameter
                                                                   ,X.GrdScaleId
                                                        ) S
                                              GROUP BY  InstrGrdBkWgtDetailId
                                                       ,Code
                                                       ,Descrip
                                                       ,Weight
                                                       ,NumberOfComponents
                                            ) FinalTblToComputeCurrentScore
                                );
            IF ( @CurrentScore IS NULL )
                BEGIN
				-- instructor grade books
                    SET @CurrentScore = (
                                          SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                         ELSE NULL
                                                    END AS CurrentScore
                                          FROM      (
                                                      SELECT    InstrGrdBkWgtDetailId
                                                               ,Code
                                                               ,Descrip
                                                               ,Weight AS GradeBookWeight
                                                               ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                     ELSE 0
                                                                END AS ActualWeight
                                                      FROM      (
                                                                  SELECT    C.InstrGrdBkWgtDetailId
                                                                           ,D.Code
                                                                           ,D.Descrip
                                                                           ,ISNULL(C.Weight,0) AS Weight
                                                                           ,C.Number AS MinNumber
                                                                           ,C.GrdPolicyId
                                                                           ,C.Parameter AS Param
                                                                           ,X.GrdScaleId
                                                                           ,SUM(GR.Score) AS Score
                                                                           ,COUNT(D.Descrip) AS NumberOfComponents
                                                                  FROM      (
                                                                              --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
															--FROM          arGrdBkWeights A,arClassSections B        
															--WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
															--ORDER BY      A.EffectiveDate DESC
                                                                              SELECT DISTINCT TOP 1
                                                                                        t1.InstrGrdBkWgtId
                                                                                       ,t1.GrdScaleId
                                                                              FROM      arClassSections t1
                                                                                       ,arGrdBkWeights t2
                                                                              WHERE     t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                                        AND t1.ClsSectionId = @ClsSectionId
                                                                            ) X
                                                                           ,arGrdBkWgtDetails C
                                                                           ,arGrdComponentTypes D
                                                                           ,arGrdBkResults GR
                                                                  WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                            AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                            AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                            AND
													-- D.SysComponentTypeID not in (500,503) and 
                                                                            GR.StuEnrollId = @StuEnrollId
                                                                            AND GR.ClsSectionId = @ClsSectionId
                                                                            AND GR.Score IS NOT NULL
                                                                  GROUP BY  C.InstrGrdBkWgtDetailId
                                                                           ,D.Code
                                                                           ,D.Descrip
                                                                           ,C.Weight
                                                                           ,C.Number
                                                                           ,C.GrdPolicyId
                                                                           ,C.Parameter
                                                                           ,X.GrdScaleId
                                                                ) S
                                                      GROUP BY  InstrGrdBkWgtDetailId
                                                               ,Code
                                                               ,Descrip
                                                               ,Weight
                                                               ,NumberOfComponents
                                                    ) FinalTblToComputeCurrentScore
                                        );	
			
                END;
			
		
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
            IF ( LOWER(@varGradeRounding) = 'yes' )
                BEGIN
                    IF @FinalScore IS NOT NULL
                        BEGIN
                            SET @FinalScore = ROUND(@FinalScore,0);
                            SET @CurrentScore = @FinalScore;
                        END;
                    IF @FinalScore IS NULL
                        AND @CurrentScore IS NOT NULL
                        BEGIN
                            SET @CurrentScore = ROUND(@CurrentScore,0);
                        END;
                END;
            ELSE
                BEGIN
                    IF @FinalScore IS NOT NULL
                        BEGIN
                            SET @CurrentScore = @FinalScore;
                        END;	
                    IF @FinalScore IS NULL
                        AND @CurrentScore IS NOT NULL
                        BEGIN
                            SET @CurrentScore = ROUND(@CurrentScore,0);
                        END;
                END;
				
            UPDATE  syCreditSummary
            SET     FinalScore = @FinalScore
                   ,CurrentScore = @CurrentScore
            WHERE   StuEnrollId = @StuEnrollId
                    AND TermId = @TermId
                    AND ReqId = @reqid;

			--Average calculation
			
			-- Term Average
            SET @TermAverageCount = (
                                      SELECT    COUNT(*)
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = @StuEnrollId
                                                AND TermId = @TermId
                                                AND FinalScore IS NOT NULL
                                    );
            SET @termAverageSum = (
                                    SELECT  SUM(FinalScore)
                                    FROM    syCreditSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND TermId = @TermId
                                            AND FinalScore IS NOT NULL
                                  );
            SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
            SET @cumAveragecount = (
                                     SELECT COUNT(*)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                   );
            SET @cumAverageSum = (
                                   SELECT   SUM(FinalScore)
                                   FROM     syCreditSummary
                                   WHERE    StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                 );
            SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
            UPDATE  syCreditSummary
            SET     Average = @TermAverage
            WHERE   StuEnrollId = @StuEnrollId
                    AND TermId = @TermId; 
			
			--Update Cumulative GPA
            UPDATE  syCreditSummary
            SET     CumAverage = @CumAverage
            WHERE   StuEnrollId = @StuEnrollId;
			

            FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,@FinalGrade,
                @sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,@FinAidCredits;
        END;
    CLOSE GetCreditsSummary_Cursor;
    DEALLOCATE GetCreditsSummary_Cursor;


    DECLARE GetCreditsSummary_Cursor CURSOR
    FOR
        SELECT	DISTINCT
                SE.StuEnrollId
               ,T.TermId
               ,T.TermDescrip
               ,T.StartDate
               ,R.ReqId
               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
               ,RES.Score AS FinalScore
               ,RES.GrdSysDetailId AS FinalGrade
               ,GCT.SysComponentTypeId
               ,R.Credits AS CreditsAttempted
               ,CS.ClsSectionId
               ,GSD.Grade
               ,GSD.IsPass
               ,GSD.IsCreditsAttempted
               ,GSD.IsCreditsEarned
               ,SE.PrgVerId
               ,GSD.IsInGPA
               ,R.FinAidCredits AS FinAidCredits
        FROM    arStuEnrollments SE
        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
        INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
        INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                         AND RES.TermId = CS.TermId
        INNER JOIN arTerm T ON CS.TermId = T.TermId
        INNER JOIN arReqs R ON CS.ReqId = R.ReqId
        LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
        LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
        LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
        LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
        WHERE   SE.StuEnrollId = @StuEnrollId
        ORDER BY T.StartDate
               ,T.TermDescrip
               ,R.ReqId;


    SET @varGradeRounding = (
                              SELECT    Value
                              FROM      syConfigAppSetValues
                              WHERE     SettingId = 45
                            );

    OPEN GetCreditsSummary_Cursor;
    SET @PrevStuEnrollId = NULL;
    SET @PrevTermId = NULL;
    SET @PrevReqId = NULL;
    SET @RowCount = 0;
    FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,@FinalGrade,
        @sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,@FinAidCredits;
    WHILE @@FETCH_STATUS = 0
        BEGIN
			
            SET @CurrentScore = (
                                  SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                 ELSE NULL
                                            END AS CurrentScore
                                  FROM      (
                                              SELECT    InstrGrdBkWgtDetailId
                                                       ,Code
                                                       ,Descrip
                                                       ,Weight AS GradeBookWeight
                                                       ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                             ELSE 0
                                                        END AS ActualWeight
                                              FROM      (
                                                          SELECT    C.InstrGrdBkWgtDetailId
                                                                   ,D.Code
                                                                   ,D.Descrip
                                                                   ,ISNULL(C.Weight,0) AS Weight
                                                                   ,C.Number AS MinNumber
                                                                   ,C.GrdPolicyId
                                                                   ,C.Parameter AS Param
                                                                   ,X.GrdScaleId
                                                                   ,SUM(GR.Score) AS Score
                                                                   ,COUNT(D.Descrip) AS NumberOfComponents
                                                          FROM      (
                                                                      SELECT DISTINCT TOP 1
                                                                                A.InstrGrdBkWgtId
                                                                               ,A.EffectiveDate
                                                                               ,B.GrdScaleId
                                                                      FROM      arGrdBkWeights A
                                                                               ,arClassSections B
                                                                      WHERE     A.ReqId = B.ReqId
                                                                                AND A.EffectiveDate <= B.StartDate
                                                                                AND B.ClsSectionId = @ClsSectionId
                                                                      ORDER BY  A.EffectiveDate DESC
                                                                    ) X
                                                                   ,arGrdBkWgtDetails C
                                                                   ,arGrdComponentTypes D
                                                                   ,arGrdBkResults GR
                                                          WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                    AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                    AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                    AND D.SysComponentTypeId NOT IN ( 500,503 )
                                                                    AND GR.StuEnrollId = @StuEnrollId
                                                                    AND GR.ClsSectionId = @ClsSectionId
                                                                    AND GR.Score IS NOT NULL
                                                          GROUP BY  C.InstrGrdBkWgtDetailId
                                                                   ,D.Code
                                                                   ,D.Descrip
                                                                   ,C.Weight
                                                                   ,C.Number
                                                                   ,C.GrdPolicyId
                                                                   ,C.Parameter
                                                                   ,X.GrdScaleId
                                                        ) S
                                              GROUP BY  InstrGrdBkWgtDetailId
                                                       ,Code
                                                       ,Descrip
                                                       ,Weight
                                                       ,NumberOfComponents
                                            ) FinalTblToComputeCurrentScore
                                );
            IF ( @CurrentScore IS NULL )
                BEGIN
				-- instructor grade books
                    SET @CurrentScore = (
                                          SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                         ELSE NULL
                                                    END AS CurrentScore
                                          FROM      (
                                                      SELECT    InstrGrdBkWgtDetailId
                                                               ,Code
                                                               ,Descrip
                                                               ,Weight AS GradeBookWeight
                                                               ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                     ELSE 0
                                                                END AS ActualWeight
                                                      FROM      (
                                                                  SELECT    C.InstrGrdBkWgtDetailId
                                                                           ,D.Code
                                                                           ,D.Descrip
                                                                           ,ISNULL(C.Weight,0) AS Weight
                                                                           ,C.Number AS MinNumber
                                                                           ,C.GrdPolicyId
                                                                           ,C.Parameter AS Param
                                                                           ,X.GrdScaleId
                                                                           ,SUM(GR.Score) AS Score
                                                                           ,COUNT(D.Descrip) AS NumberOfComponents
                                                                  FROM      (
                                                                              --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
															--FROM          arGrdBkWeights A,arClassSections B        
															--WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
															--ORDER BY      A.EffectiveDate DESC
                                                                              SELECT DISTINCT TOP 1
                                                                                        t1.InstrGrdBkWgtId
                                                                                       ,t1.GrdScaleId
                                                                              FROM      arClassSections t1
                                                                                       ,arGrdBkWeights t2
                                                                              WHERE     t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                                        AND t1.ClsSectionId = @ClsSectionId
                                                                            ) X
                                                                           ,arGrdBkWgtDetails C
                                                                           ,arGrdComponentTypes D
                                                                           ,arGrdBkResults GR
                                                                  WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                            AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                            AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                            AND
													-- D.SysComponentTypeID not in (500,503) and 
                                                                            GR.StuEnrollId = @StuEnrollId
                                                                            AND GR.ClsSectionId = @ClsSectionId
                                                                            AND GR.Score IS NOT NULL
                                                                  GROUP BY  C.InstrGrdBkWgtDetailId
                                                                           ,D.Code
                                                                           ,D.Descrip
                                                                           ,C.Weight
                                                                           ,C.Number
                                                                           ,C.GrdPolicyId
                                                                           ,C.Parameter
                                                                           ,X.GrdScaleId
                                                                ) S
                                                      GROUP BY  InstrGrdBkWgtDetailId
                                                               ,Code
                                                               ,Descrip
                                                               ,Weight
                                                               ,NumberOfComponents
                                                    ) FinalTblToComputeCurrentScore
                                        );	
			
                END;
			
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
            IF ( LOWER(@varGradeRounding) = 'yes' )
                BEGIN
                    IF @FinalScore IS NOT NULL
                        BEGIN
                            SET @FinalScore = ROUND(@FinalScore,0);
                            SET @CurrentScore = @FinalScore;
                        END;
                    IF @FinalScore IS NULL
                        AND @CurrentScore IS NOT NULL
                        BEGIN
                            SET @CurrentScore = ROUND(@CurrentScore,0);
                        END;
                END;
            ELSE
                BEGIN
                    IF @FinalScore IS NOT NULL
                        BEGIN
                            SET @CurrentScore = @FinalScore;
                        END;	
                    IF @FinalScore IS NULL
                        AND @CurrentScore IS NOT NULL
                        BEGIN
                            SET @CurrentScore = ROUND(@CurrentScore,0);
                        END;
                END;
				
            UPDATE  syCreditSummary
            SET     FinalScore = @FinalScore
                   ,CurrentScore = @CurrentScore
            WHERE   StuEnrollId = @StuEnrollId
                    AND TermId = @TermId
                    AND ReqId = @reqid;

			--Average calculation
--			declare @CumAverage decimal(18,2),@cumAverageSum decimal(18,2),@cumAveragecount int
			
			-- Term Average
            SET @TermAverageCount = (
                                      SELECT    COUNT(*)
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = @StuEnrollId
                                                AND TermId = @TermId
                                                AND FinalScore IS NOT NULL
                                    );
            SET @termAverageSum = (
                                    SELECT  SUM(FinalScore)
                                    FROM    syCreditSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND TermId = @TermId
                                            AND FinalScore IS NOT NULL
                                  );
            SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
            SET @cumAveragecount = (
                                     SELECT COUNT(*)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                   );
            SET @cumAverageSum = (
                                   SELECT   SUM(FinalScore)
                                   FROM     syCreditSummary
                                   WHERE    StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                 );
            SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
            UPDATE  syCreditSummary
            SET     Average = @TermAverage
            WHERE   StuEnrollId = @StuEnrollId
                    AND TermId = @TermId; 
			
			--Update Cumulative GPA
            UPDATE  syCreditSummary
            SET     CumAverage = @CumAverage
            WHERE   StuEnrollId = @StuEnrollId;
			

            FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,@FinalGrade,
                @sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,@FinAidCredits;
        END;
    CLOSE GetCreditsSummary_Cursor;
    DEALLOCATE GetCreditsSummary_Cursor;

    DECLARE @GradeSystemDetailId UNIQUEIDENTIFIER
       ,@IsGPA BIT;
    DECLARE GetCreditsSummary_Cursor CURSOR
    FOR
        SELECT DISTINCT
                SE.StuEnrollId
               ,T.TermId
               ,T.TermDescrip
               ,T.StartDate
               ,R.ReqId
               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
               ,NULL AS FinalScore
               ,RES.GrdSysDetailId AS GradeSystemDetailId
               ,GSD.Grade AS FinalGrade
               ,GSD.Grade AS CurrentGrade
               ,R.Credits
               ,NULL AS ClsSectionId
               ,GSD.IsPass
               ,GSD.IsCreditsAttempted
               ,GSD.IsCreditsEarned
               ,GSD.GPA
               ,GSD.IsInGPA
               ,SE.PrgVerId
               ,GSD.IsInGPA
               ,R.FinAidCredits AS FinAidCredits
        FROM    arStuEnrollments SE
        INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
        INNER JOIN syCreditSummary CS ON CS.StuEnrollId = RES.StuEnrollId
        INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
        INNER JOIN arReqs R ON RES.ReqId = R.ReqId
        INNER JOIN arTerm T ON RES.TermId = T.TermId
        WHERE   RES.ReqId NOT IN ( SELECT DISTINCT
                                            ReqId
                                   FROM     syCreditSummary
                                   WHERE    StuEnrollId = SE.StuEnrollId
                                            AND TermId = T.TermId )
                AND SE.StuEnrollId = @StuEnrollId;

    SET @varGradeRounding = (
                              SELECT    Value
                              FROM      syConfigAppSetValues
                              WHERE     SettingId = 45
                            );

    OPEN GetCreditsSummary_Cursor;
    SET @PrevStuEnrollId = NULL;
    SET @PrevTermId = NULL;
    SET @PrevReqId = NULL;
    SET @RowCount = 0;
    FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,@GradeSystemDetailId,
        @FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,@PrgVerId,@IsInGPA,@FinAidCredits; 
    WHILE @@FETCH_STATUS = 0
        BEGIN
			
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
            IF ( LOWER(@varGradeRounding) = 'yes' )
                BEGIN
                    IF @FinalScore IS NOT NULL
                        BEGIN
                            SET @FinalScore = ROUND(@FinalScore,0);
                            SET @CurrentScore = @FinalScore;
                        END;
                    IF @FinalScore IS NULL
                        AND @CurrentScore IS NOT NULL
                        BEGIN
                            SET @CurrentScore = ROUND(@CurrentScore,0);
                        END;
                END;
            ELSE
                BEGIN
                    IF @FinalScore IS NOT NULL
                        BEGIN
                            SET @CurrentScore = @FinalScore;
                        END;	
                    IF @FinalScore IS NULL
                        AND @CurrentScore IS NOT NULL
                        BEGIN
                            SET @CurrentScore = ROUND(@CurrentScore,0);
                        END;
                END;
				
            UPDATE  syCreditSummary
            SET     FinalScore = @FinalScore
                   ,CurrentScore = @CurrentScore
            WHERE   StuEnrollId = @StuEnrollId
                    AND TermId = @TermId
                    AND ReqId = @reqid;

			--Average calculation
		
			-- Term Average
            SET @TermAverageCount = (
                                      SELECT    COUNT(*)
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = @StuEnrollId
                                                AND TermId = @TermId
                                                AND FinalScore IS NOT NULL
                                    );
            SET @termAverageSum = (
                                    SELECT  SUM(FinalScore)
                                    FROM    syCreditSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND TermId = @TermId
                                            AND FinalScore IS NOT NULL
                                  );
            SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
            SET @cumAveragecount = (
                                     SELECT COUNT(*)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                   );
            SET @cumAverageSum = (
                                   SELECT   SUM(FinalScore)
                                   FROM     syCreditSummary
                                   WHERE    StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                 );
            SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
            UPDATE  syCreditSummary
            SET     Average = @TermAverage
            WHERE   StuEnrollId = @StuEnrollId
                    AND TermId = @TermId; 
			
			--Update Cumulative GPA
            UPDATE  syCreditSummary
            SET     CumAverage = @CumAverage
            WHERE   StuEnrollId = @StuEnrollId;
			

            FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                @GradeSystemDetailId,@FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,@PrgVerId,
                @IsInGPA,@FinAidCredits; 
        END;
    CLOSE GetCreditsSummary_Cursor;
    DEALLOCATE GetCreditsSummary_Cursor;

    DECLARE GetCreditsSummary_Cursor CURSOR
    FOR
        SELECT DISTINCT
                SE.StuEnrollId
               ,T.TermId
               ,T.TermDescrip
               ,T.StartDate
               ,R.ReqId
               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
               ,NULL AS FinalScore
               ,RES.GrdSysDetailId AS GradeSystemDetailId
               ,GSD.Grade AS FinalGrade
               ,GSD.Grade AS CurrentGrade
               ,R.Credits
               ,NULL AS ClsSectionId
               ,GSD.IsPass
               ,GSD.IsCreditsAttempted
               ,GSD.IsCreditsEarned
               ,GSD.GPA
               ,GSD.IsInGPA
               ,SE.PrgVerId
               ,GSD.IsInGPA
               ,R.FinAidCredits AS FinAidCredits
        FROM    arStuEnrollments SE
        INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
        INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
        INNER JOIN arReqs R ON RES.ReqId = R.ReqId
        INNER JOIN arTerm T ON RES.TermId = T.TermId
        WHERE   SE.StuEnrollId NOT IN ( SELECT DISTINCT
                                                StuEnrollId
                                        FROM    syCreditSummary )
                AND SE.StuEnrollId = @StuEnrollId;

    SET @varGradeRounding = (
                              SELECT    Value
                              FROM      syConfigAppSetValues
                              WHERE     SettingId = 45
                            );

    OPEN GetCreditsSummary_Cursor;
    SET @PrevStuEnrollId = NULL;
    SET @PrevTermId = NULL;
    SET @PrevReqId = NULL;
    SET @RowCount = 0;
    FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,@GradeSystemDetailId,
        @FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,@PrgVerId,@IsInGPA,@FinAidCredits; 
    WHILE @@FETCH_STATUS = 0
        BEGIN
			
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
            IF ( LOWER(@varGradeRounding) = 'yes' )
                BEGIN
                    IF @FinalScore IS NOT NULL
                        BEGIN
                            SET @FinalScore = ROUND(@FinalScore,0);
                            SET @CurrentScore = @FinalScore;
                        END;
                    IF @FinalScore IS NULL
                        AND @CurrentScore IS NOT NULL
                        BEGIN
                            SET @CurrentScore = ROUND(@CurrentScore,0);
                        END;
                END;
            ELSE
                BEGIN
                    IF @FinalScore IS NOT NULL
                        BEGIN
                            SET @CurrentScore = @FinalScore;
                        END;	
                    IF @FinalScore IS NULL
                        AND @CurrentScore IS NOT NULL
                        BEGIN
                            SET @CurrentScore = ROUND(@CurrentScore,0);
                        END;
                END;
				
            UPDATE  syCreditSummary
            SET     FinalScore = @FinalScore
                   ,CurrentScore = @CurrentScore
            WHERE   StuEnrollId = @StuEnrollId
                    AND TermId = @TermId
                    AND ReqId = @reqid;

			--Average calculation
            SET @TermAverageCount = (
                                      SELECT    COUNT(*)
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = @StuEnrollId
                                                AND TermId = @TermId
                                                AND FinalScore IS NOT NULL
                                    );
            SET @termAverageSum = (
                                    SELECT  SUM(FinalScore)
                                    FROM    syCreditSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND TermId = @TermId
                                            AND FinalScore IS NOT NULL
                                  );
            SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
            SET @cumAveragecount = (
                                     SELECT COUNT(*)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                   );
            SET @cumAverageSum = (
                                   SELECT   SUM(FinalScore)
                                   FROM     syCreditSummary
                                   WHERE    StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                 );
            SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
            UPDATE  syCreditSummary
            SET     Average = @TermAverage
            WHERE   StuEnrollId = @StuEnrollId
                    AND TermId = @TermId; 
			
			--Update Cumulative GPA
            UPDATE  syCreditSummary
            SET     CumAverage = @CumAverage
            WHERE   StuEnrollId = @StuEnrollId;
			

            FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                @GradeSystemDetailId,@FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,@PrgVerId,
                @IsInGPA,@FinAidCredits;  
        END;
    CLOSE GetCreditsSummary_Cursor;
    DEALLOCATE GetCreditsSummary_Cursor;

-- Report Query Starts Here
--Declare @CurrentBal_Compute Decimal(18,2)
--set @CurrentBal_Compute = (select SUM(IsNULL(TransAmount,0)) as CurrentBalance from saTransactions where StuEnrollId=@StuEnrollId 
--							AND Voided = 0 AND 
--							(@TermStartDate is null or @TermStartDateModifier is null or 
--								(
--									((@TermStartDateModifier <> '=') OR (TransDate= ltrim(rtrim(@TermStartDate)))) And ((@TermStartDateModifier <> '>') OR (TransDate > ltrim(rtrim(@TermStartDate)))) 
--									And ((@TermStartDateModifier <> '<') OR (TransDate < ltrim(rtrim(@TermStartDate))))	
--									And ((@TermStartDateModifier <> '>=') OR (TransDate >= ltrim(rtrim(@TermStartDate)))) 
--									And ((@TermStartDateModifier <> '<=') OR (TransDate <= ltrim(rtrim(@TermStartDate))))
--								)
--							)
--						) 

---- PRINT '@CurrentBal_Compute'
---- PRINT @CurrentBal_Compute

    SET @TermAverageCount = (
                              SELECT    COUNT(*)
                              FROM      syCreditSummary
                              WHERE     StuEnrollId = @StuEnrollId
                                        AND FinalScore IS NOT NULL 
										--and Completed=1
										--and
										--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1)))
									--AND (
									--     @TermStartDate IS NULL
									--     OR @TermStartDateModifier IS NULL
									--     OR (
									--         (
									--          (@TermStartDateModifier <> '=')
									--          OR (TermStartDate = @TermStartDate)
									--         )
									--         AND (
									--              (@TermStartDateModifier <> '>')
									--              OR (TermStartDate > @TermStartDate)
									--             )
									--         AND (
									--              (@TermStartDateModifier <> '<')
									--              OR (TermStartDate < @TermStartDate)
									--             )
									--         AND (
									--              (@TermStartDateModifier <> '>=')
									--              OR (TermStartDate >= @TermStartDate)
									--             )
									--         AND (
									--              (@TermStartDateModifier <> '<=')
									--              OR (TermStartDate <= @TermStartDate)
									--             )
									--        )
									--    )
                            );
    SET @termAverageSum = (
                            SELECT  SUM(FinalScore)
                            FROM    syCreditSummary
                            WHERE   StuEnrollId = @StuEnrollId
                                    AND FinalScore IS NOT NULL 
										--and Completed=1
										--and
										--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1)))
									--AND (
									--     @TermStartDate IS NULL
									--     OR @TermStartDateModifier IS NULL
									--     OR (
									--         (
									--          (@TermStartDateModifier <> '=')
									--          OR (TermStartDate = @TermStartDate)
									--         )
									--         AND (
									--              (@TermStartDateModifier <> '>')
									--              OR (TermStartDate > @TermStartDate)
									--             )
									--         AND (
									--              (@TermStartDateModifier <> '<')
									--              OR (TermStartDate < @TermStartDate)
									--             )
									--         AND (
									--              (@TermStartDateModifier <> '>=')
									--              OR (TermStartDate >= @TermStartDate)
   --             )
									--         AND (
									--              (@TermStartDateModifier <> '<=')
									--              OR (TermStartDate <= @TermStartDate)
									--             )
									--        )
									--    )
                          );
    IF @TermAverageCount >= 1
        BEGIN
            SET @TermAverage = @termAverageSum / @TermAverageCount; 
        END;
			
-- Cumulative Average
    SET @cumAveragecount = (
                             SELECT COUNT(*)
                             FROM   syCreditSummary
                             WHERE  StuEnrollId = @StuEnrollId
                                    AND FinalScore IS NOT NULL 
										--and Completed=1
										--and
										--(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1)))
		   --                         AND (
		   --                              @TermStartDate IS NULL
		   --                              OR @TermStartDateModifier IS NULL
		   --                              OR 
											----(
											----	((@TermStartDateModifier <> '=') OR (TermStartDate= @TermStartDate)) And ((@TermStartDateModifier <> '>') OR (TermStartDate > @TermStartDate)) 
											----	And ((@TermStartDateModifier <> '<') OR (TermStartDate < @TermStartDate))	
											----	And ((@TermStartDateModifier <> '>=') OR (TermStartDate >= @TermStartDate)) 
											----	And ((@TermStartDateModifier <> '<=') OR (TermStartDate <= @TermStartDate))
											----)
		   --                              (
		   --                               (
		   --                                (@TermStartDateModifier <> '=')
		   --                                OR (TermStartDate = LTRIM(RTRIM(@TermStartDate)))
		   --                                OR (DATEDIFF(DAY, @TermStartDate,
		   --                                             TermStartDate)) = 0
		   --                               )
		   --                               AND (
		   --                                    (@TermStartDateModifier <> '>')
		   --                                    OR (TermStartDate > LTRIM(RTRIM(@TermStartDate)))
		   --                                    OR (DATEDIFF(DAY,
		   --                                                 @TermStartDate,
		   --                                                 TermStartDate)) > 0
		   --                                   )
		   --                               AND (
		   --                                    (@TermStartDateModifier <> '<')
		   --                                    OR (TermStartDate < LTRIM(RTRIM(@TermStartDate)))
		   --                                    OR (DATEDIFF(DAY,
		   --                                                 @TermStartDate,
		   --                                                 TermStartDate)) < 0
		   --                                   )
		   --                               AND (
		   --                                    (@TermStartDateModifier <> '>=')
		   --                                    OR (TermStartDate >= LTRIM(RTRIM(@TermStartDate)))
		   --                                    OR (DATEDIFF(DAY,
		   --                                                 @TermStartDate,
		   --                                                 TermStartDate)) >= 0
		   --                                   )
		   --                               AND (
		   --                                    (@TermStartDateModifier <> '<=')
		   --                                    OR (TermStartDate <= LTRIM(RTRIM(@TermStartDate)))
		   --                     OR (DATEDIFF(DAY,
		   --                                                 @TermStartDate,
		   --                                                 TermStartDate)) <= 0
		   --                                   )
		   --                              )
		   --                             )
                           );
---- PRINT @cumAveragecount

    SET @cumAverageSum = (
                           SELECT   SUM(FinalScore)
                           FROM     syCreditSummary
                           WHERE    StuEnrollId = @StuEnrollId
                                    AND FinalScore IS NOT NULL 
										----and Completed=1
										----and
										----(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1)))
		  --                          AND (
		  --                               @TermStartDate IS NULL
		  --                               OR @TermStartDateModifier IS NULL
		  --                               OR 
										--	--(
										--	--	((@TermStartDateModifier <> '=') OR (TermStartDate= @TermStartDate)) And ((@TermStartDateModifier <> '>') OR (TermStartDate > @TermStartDate)) 
										--	--	And ((@TermStartDateModifier <> '<') OR (TermStartDate < @TermStartDate))	
										--	--	And ((@TermStartDateModifier <> '>=') OR (TermStartDate >= @TermStartDate)) 
										--	--	And ((@TermStartDateModifier <> '<=') OR (TermStartDate <= @TermStartDate))
										--	--)
		  --                               (
		  --                                (
		  --                                 (@TermStartDateModifier <> '=')
		  --                                 OR (TermStartDate = LTRIM(RTRIM(@TermStartDate)))
		  --                                 OR (DATEDIFF(DAY, @TermStartDate,
		  --                                              TermStartDate)) = 0
		  --                                )
		  --                                AND (
		  --                                     (@TermStartDateModifier <> '>')
		  --                                     OR (TermStartDate > LTRIM(RTRIM(@TermStartDate)))
		  --                                     OR (DATEDIFF(DAY,
		  --                                                  @TermStartDate,
		  --                                                  TermStartDate)) > 0
		  --                                    )
		  --                                AND (
		  --                                     (@TermStartDateModifier <> '<')
		  --                                     OR (TermStartDate < LTRIM(RTRIM(@TermStartDate)))
		  --                                     OR (DATEDIFF(DAY,
		  --                                                  @TermStartDate,
		  --                                                  TermStartDate)) < 0
		  --                                    )
		  --                                AND (
		  --                                     (@TermStartDateModifier <> '>=')
		  --                                     OR (TermStartDate >= LTRIM(RTRIM(@TermStartDate)))
		  --                                     OR (DATEDIFF(DAY,
		  --                                                  @TermStartDate,
		  --                                                  TermStartDate)) >= 0
		  --                                    )
		  --                                AND (
		  --                                     (@TermStartDateModifier <> '<=')
		  --                                     OR (TermStartDate <= LTRIM(RTRIM(@TermStartDate)))
		  --                                     OR (DATEDIFF(DAY,
		  --                                                  @TermStartDate,
		  --        TermStartDate)) <= 0
		  --                                    )
		  --                               )
		  --                              )
                         );
	---- PRINT @CumAverageSum
								
    IF @cumAveragecount >= 1
        BEGIN
            SET @CumAverage = @cumAverageSum / @cumAveragecount;
        END;
	---- PRINT @CumAverage

	---- PRINT @UnitTypeId;

	---- PRINT @TrackSapAttendance;
	---- PRINT @displayHours;


-- Main query starts here 
    SELECT DISTINCT TOP 1
            S.SSN
           ,S.FirstName
           ,S.LastName
           ,SC.StatusCodeDescrip
           ,SE.StartDate AS StudentStartDate
           ,SE.ExpGradDate AS StudentExpectedGraduationDate
           ,PV.PrgVerId AS PrgVerId
           ,PV.PrgVerDescrip AS PrgVerDescrip
           ,PV.UnitTypeId AS UnitTypeId
           ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                 ELSE 0
            END AS PrgVersionTrackCredits
           ,AAUT1.UnitTypeDescrip AS UnitTypeDescripFrom_arAttUnitType
           ,CASE WHEN @TermStartDate IS NULL THEN (
                                                    SELECT  MAX(LDA)
                                                    FROM    (
                                                              SELECT    MAX(AttendedDate) AS LDA
                                                              FROM      arExternshipAttendance
                                                              WHERE     StuEnrollId = @StuEnrollId
                                                              UNION ALL
                                                              SELECT    MAX(MeetDate) AS LDA
                                                              FROM      atClsSectAttendance
                                                              WHERE     StuEnrollId = @StuEnrollId
                                                                        AND Actual > 0
                                                                        AND Actual <> 99.00
                                                                        AND Actual <> 999.00
                                                                        AND Actual <> 9999.00
                                                              UNION ALL
															  --SELECT    MAX(AttendanceDate) AS LDA
															  --FROM      atAttendance
															  --WHERE     EnrollId = @StuEnrollId
															  --          AND Actual > 0
															  --UNION ALL
                                                              SELECT    MAX(RecordDate) AS LDA
                                                              FROM      arStudentClockAttendance
                                                              WHERE     StuEnrollId = @StuEnrollId
                                                                        AND (
                                                                              ActualHours > 0
                                                                              AND ActualHours <> 99.00
                                                                              AND ActualHours <> 999.00
                                                                              AND ActualHours <> 9999.00
                                                                            )
                                                              UNION ALL
                                                              SELECT    MAX(MeetDate) AS LDA
                                                              FROM      atConversionAttendance
                                                              WHERE     StuEnrollId = @StuEnrollId
                                                                        AND (
                                                                              Actual > 0
                                                                              AND Actual <> 99.00
                                                                              AND Actual <> 999.00
                                                                              AND Actual <> 9999.00
                                                                            )
                                                              UNION ALL
                                                              SELECT TOP 1
                                                                        LDA
                                                              FROM      arStuEnrollments
                                                              WHERE     StuEnrollId = @StuEnrollId
                                                            ) TR
                                                  )
                 ELSE 
		-- Get the LDA Based on Term Start Date
                      (
                        SELECT  MAX(LDA)
                        FROM    (
                                  SELECT TOP 1
                                            LDA
                                  FROM      arStuEnrollments
                                  WHERE     StuEnrollId = @StuEnrollId
                                  UNION ALL
                                  SELECT    MAX(AttendedDate) AS LDA
                                  FROM      arExternshipAttendance
                                  WHERE     StuEnrollId = @StuEnrollId
	  --  AND
						----(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1))) and
	  --                                  (
	  --                                   @TermStartDate IS NULL
	  --                                   OR @TermStartDateModifier IS NULL
	  --                                   OR (
	  --                                       (
	  --                                        (@TermStartDateModifier <> '=')
	  --                                        OR (AttendedDate = @TermStartDate)
	  --                                       )
	  --                                       AND (
	  --                                            (@TermStartDateModifier <> '>')
	  --                                            OR (AttendedDate > @TermStartDate)
	  --                                           )
	  --                                       AND (
	  --                                            (@TermStartDateModifier <> '<')
	  --                                            OR (AttendedDate < @TermStartDate)
	  --                                           )
	  --                                       AND (
	  --                                            (@TermStartDateModifier <> '>=')
	  --                                            OR (AttendedDate >= @TermStartDate)
	  --                                           )
	  --                                       AND (
	  --                                            (@TermStartDateModifier <> '<=')
	  --                                            OR (AttendedDate <= @TermStartDate)
	  --                                           )
	  --                                      )
	  --                                  )
                                  UNION ALL
                                  SELECT    MAX(MeetDate) AS LDA
                                  FROM      atClsSectAttendance t1
                                           ,arClassSections t2
                                  WHERE     t1.ClsSectionId = t2.ClsSectionId
                                            AND StuEnrollId = @StuEnrollId
                                            AND Actual > 0
                                            AND Actual <> 99.00
                                            AND Actual <> 999.00
                                            AND Actual <> 9999.00
	  --                                  AND
						---- (@TermId is null or t2.TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1))) and
	  --                                  (
	  --                                   @TermStartDate IS NULL
	  --                                   OR @TermStartDateModifier IS NULL
	  --                                   OR (
	  --                                       (
	  --                                        (@TermStartDateModifier <> '=')
	  --                                        OR (MeetDate = @TermStartDate)
	  --                                       )
	  --                                       AND (
	  --                                            (@TermStartDateModifier <> '>')
	  --                                            OR (MeetDate > @TermStartDate)
	  --                                           )
	  --                                       AND (
	  --                                            (@TermStartDateModifier <> '<')
	  --                                            OR (MeetDate < @TermStartDate)
	  --                                           )
	  --                                       AND (
	  --                                            (@TermStartDateModifier <> '>=')
	  --                                            OR (MeetDate >= @TermStartDate)
	  --                                           )
	  --                                       AND (
	  --                                            (@TermStartDateModifier <> '<=')
	  --                                            OR (MeetDate <= @TermStartDate)
	  --                                           )
	  --                                      )
	  --                                  )
                                  UNION ALL
                                  SELECT    MAX(RecordDate) AS LDA
                                  FROM      arStudentClockAttendance
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND (
                                                  ActualHours > 0
                                                  AND ActualHours <> 99.00
                                                  AND ActualHours <> 999.00
                                                  AND ActualHours <> 9999.00
                                                )
	  --                                  AND
						----(@TermId is null or TermId in (Select Val from [MultipleValuesForReportParameters](@TermId,',',1))) and
	  --                                  (
	  --                                   @TermStartDate IS NULL
	  --                                   OR @TermStartDateModifier IS NULL
	  --                                   OR (
	  --                                       (
	  --                                        (@TermStartDateModifier <> '=')
	  --                                        OR (RecordDate = @TermStartDate)
	  --                                       )
	  --                                       AND (
	  --                                            (@TermStartDateModifier <> '>')
	  --                                            OR (RecordDate > @TermStartDate)
	  --                                           )
	  --                                       AND (
	  --                                            (@TermStartDateModifier <> '<')
	  --                                            OR (RecordDate < @TermStartDate)
	  --                                           )
	  --                                       AND (
	  --                                            (@TermStartDateModifier <> '>=')
	  --                                            OR (RecordDate >= @TermStartDate)
	  --                                           )
	  --                                       AND (
	  --                                            (@TermStartDateModifier <> '<=')
	  --                                            OR (RecordDate <= @TermStartDate)
	  --                                           )
	  --                                      )
	  --                                  )
                                  UNION ALL
                                  SELECT    MAX(MeetDate) AS LDA
                                  FROM      atConversionAttendance
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND (
                                                  Actual > 0
                                                  AND Actual <> 99.00
                                                  AND Actual <> 999.00
                                                  AND Actual <> 9999.00
                                                )
                                ) TR
                      )
            END AS StudentLastDateAttended
           ,( CASE WHEN ( AAUT1.UnitTypeId = '2600592A-9739-4A13-BDCE-7A25FE4A7478' ) -- NONE
                        THEN 0.00
                   ELSE (
                          SELECT    SUM(ISNULL(APSD.total,0))
                          FROM      arProgScheduleDetails AS APSD
                          INNER JOIN arProgSchedules AS APS ON APS.ScheduleId = APSD.ScheduleId
                          INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = APS.PrgVerId
                          INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                          INNER JOIN dbo.arStudentSchedules SS ON SS.StuEnrollId = ASE.StuEnrollId
                                                                  AND SS.ScheduleId = APSD.ScheduleId
                          WHERE     ASE.StuEnrollId = @StuEnrollId
                        )
              END ) AS WeeklySchedule
           ,CASE WHEN (
                        @TrackSapAttendance = 'byclass'
                        AND @displayHours = 'hours'
                        AND AAUT1.UnitTypeDescrip = 'Present Absent'
                      ) -- PA
                      THEN @ActualPresentDays_ConvertTo_Hours
                 ELSE CASE WHEN (
                                  @TrackSapAttendance = 'byclass'
                                  AND @displayHours = 'hours'
                                  AND AAUT1.UnitTypeDescrip = 'Minutes'
                                ) -- Minutes
                                THEN CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AttendedDays / 60
                                          ELSE SAS_ForTerm.AttendedDays / 60
                                     END
                           WHEN (
                                  @TrackSapAttendance = 'byclass'
                                  AND @displayHours = 'hours'
                                  AND AAUT1.UnitTypeDescrip = 'Clock Hours'
                                ) -- Clock Hour
                                THEN CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.AttendedDays / 60 )
                                          ELSE ( SAS_ForTerm.AttendedDays / 60 )
                                     END
                           WHEN (
                                  @TrackSapAttendance = 'byclass'
                                  AND @displayHours = 'presentabsent'
                                  AND AAUT1.UnitTypeDescrip = 'Clock Hours'
                                ) -- Clock Hour
                                THEN CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.AttendedDays / 60 )
                                          ELSE ( SAS_ForTerm.AttendedDays / 60 )
                                     END
                           WHEN (
                                  @TrackSapAttendance = 'byclass'
                                  AND @displayHours = 'presentabsent'
                                  AND AAUT1.UnitTypeDescrip = 'Minutes'
                                ) -- Minutes
                                THEN CASE WHEN @TermStartDate IS NULL THEN ( ( SAS_NoTerm.AttendedDays / 60 ) / 24 )
                                          ELSE ( ( SAS_ForTerm.AttendedDays / 60 ) / 24 )
                                     END
                           ELSE CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AttendedDays
                                     ELSE SAS_ForTerm.AttendedDays
                                END
                      END
            END AS TotalDaysAttended
           ,CASE WHEN (
                        @TrackSapAttendance = 'byclass'
                        AND @displayHours = 'hours'
                        AND AAUT1.UnitTypeDescrip = 'Present Absent'                     --AND LTRIM(RTRIM(PV.UnitTypeId)) = 'EF5535C2-142C-4223-AE3C-25A50A153CC6'      Present Absent
                      ) THEN @Scheduledhours
                 ELSE CASE WHEN (
                                  @TrackSapAttendance = 'byclass'
                                  AND @displayHours = 'hours'
                                  AND AAUT1.UnitTypeDescrip = 'Minutes'                  --AND LTRIM(RTRIM(PV.UnitTypeId)) = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'       Minutes
                                ) -- Minutes
                                THEN CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                                          ELSE SAS_ForTerm.ScheduledDays / 60
                                     END
                           WHEN (
                                  @TrackSapAttendance = 'byclass'
                                  AND @displayHours = 'hours'
                                  AND AAUT1.UnitTypeDescrip = 'Clock Hours'              --AND LTRIM(RTRIM(PV.UnitTypeId)) = 'B937C92E-FD7A-455E-A731-527A9918C734'   Clock Hours
                                ) -- Clock Hour
                                THEN CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                                          ELSE SAS_ForTerm.ScheduledDays / 60
                                     END
                           WHEN (
                                  @TrackSapAttendance = 'byclass'
                                  AND @displayHours = 'presentabsent'
                                  AND AAUT1.UnitTypeDescrip = 'Clock Hours'              --AND LTRIM(RTRIM(PV.UnitTypeId)) = 'B937C92E-FD7A-455E-A731-527A9918C734'   Clock Hours
                                ) -- Clock Hour
                                THEN CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                                          ELSE SAS_ForTerm.ScheduledDays / 60
                                     END
                           WHEN (
                                  @TrackSapAttendance = 'byclass'
                                  AND @displayHours = 'presentabsent'
                                  AND AAUT1.UnitTypeDescrip = 'Minutes'             --AND LTRIM(RTRIM(PV.UnitTypeId)) = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'     Minutes
                                ) -- Minutes
                                THEN CASE WHEN @TermStartDate IS NULL THEN ( ( SAS_NoTerm.ScheduledDays / 60 ) / 24 )
                                          ELSE ( ( SAS_ForTerm.ScheduledDays / 60 ) / 24 )
                                     END
                           ELSE CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.ScheduledDays )
                                     ELSE ( SAS_ForTerm.ScheduledDays )
                                END
                      END
            END AS ScheduledDays
           ,CASE WHEN (
                        @TrackSapAttendance = 'byclass'
                        AND @displayHours = 'hours'
                        AND AAUT1.UnitTypeDescrip = 'Present Absent'                 --AND LTRIM(RTRIM(PV.UnitTypeId)) = 'EF5535C2-142C-4223-AE3C-25A50A153CC6'     Present Absent
                      ) THEN @ActualAbsentDays_ConvertTo_Hours
                 ELSE CASE WHEN (
                                  @TrackSapAttendance = 'byclass'
                                  AND @displayHours = 'hours'
                                  AND AAUT1.UnitTypeDescrip = 'Minutes'              --AND LTRIM(RTRIM(PV.UnitTypeId)) = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'   Minutes
                                ) -- Minutes
                                THEN CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AbsentDays / 60
                                          ELSE SAS_ForTerm.AbsentDays / 60
                                     END
                           WHEN (
                                  @TrackSapAttendance = 'byclass'
                                  AND @displayHours = 'hours'
                                  AND AAUT1.UnitTypeDescrip = 'Clock Hours'         --AND LTRIM(RTRIM(PV.UnitTypeId)) = 'B937C92E-FD7A-455E-A731-527A9918C734'    Clock Hours
                                ) -- Clock Hour
                                THEN CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AbsentDays / 60
                                          ELSE SAS_ForTerm.AbsentDays / 60
                                     END
                           WHEN (
                                  @TrackSapAttendance = 'byclass'
                                  AND @displayHours = 'presentabsent'
                                  AND AAUT1.UnitTypeDescrip = 'Clock Hours'         --AND LTRIM(RTRIM(PV.UnitTypeId)) = 'B937C92E-FD7A-455E-A731-527A9918C734'    Clock Hours
                                ) -- Clock Hour
                                THEN CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AbsentDays / 60
                                          ELSE SAS_ForTerm.AbsentDays / 60
                                     END
                           WHEN (
                                  @TrackSapAttendance = 'byclass'
                                  AND @displayHours = 'presentabsent'
                                  AND AAUT1.UnitTypeDescrip = 'Minutes'              --AND LTRIM(RTRIM(PV.UnitTypeId)) = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'     Minutes
                                ) -- Minutes
                                THEN CASE WHEN @TermStartDate IS NULL THEN ( ( SAS_NoTerm.AbsentDays / 60 ) / 24 )
                                          ELSE ( ( SAS_ForTerm.AbsentDays / 60 ) / 24 )
                                     END
                           ELSE CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.AbsentDays )
                                     ELSE ( SAS_ForTerm.AbsentDays )
                                END
                      END
            END AS DaysAbsent
           ,CASE WHEN (
                        @TrackSapAttendance = 'byclass'
                        AND LOWER(RTRIM(LTRIM(@displayHours))) = 'hours'
                        AND AAUT1.UnitTypeDescrip = 'Present Absent'             --AND LTRIM(RTRIM(PV.UnitTypeId)) = 'EF5535C2-142C-4223-AE3C-25A50A153CC6'     Present Absent
                      ) THEN SAS_ForTerm.MakeupDays
                 ELSE CASE WHEN (
                                  @TrackSapAttendance = 'byclass'
                                  AND @displayHours = 'hours'
                                  AND AAUT1.UnitTypeDescrip = 'Minutes'         --AND LTRIM(RTRIM(PV.UnitTypeId)) = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'     Minutes
                                ) -- Minutes
                                THEN CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.MakeupDays / 60
                                          ELSE SAS_ForTerm.MakeupDays / 60
                                     END
                           WHEN (
                                  @TrackSapAttendance = 'byclass'
                                  AND @displayHours = 'hours'
                                  AND AAUT1.UnitTypeDescrip = 'Clock Hours'           --AND LTRIM(RTRIM(PV.UnitTypeId)) = 'B937C92E-FD7A-455E-A731-527A9918C734'     Clock Hours
                                ) -- Clock Hour
                                THEN CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.MakeupDays / 60
                                          ELSE SAS_ForTerm.MakeupDays / 60
                                     END
                           WHEN (
                                  @TrackSapAttendance = 'byclass'
                                  AND @displayHours = 'presentabsent'
                                  AND AAUT1.UnitTypeDescrip = 'Clock Hours'           --AND LTRIM(RTRIM(PV.UnitTypeId)) = 'B937C92E-FD7A-455E-A731-527A9918C734'     Clock Hours
                                ) -- Clock Hour
                                THEN CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.MakeupDays / 60
                                          ELSE SAS_ForTerm.MakeupDays / 60
                                     END
                           WHEN (
                                  @TrackSapAttendance = 'byclass'
                                  AND @displayHours = 'presentabsent'
                                  AND AAUT1.UnitTypeDescrip = 'Minutes'              --AND LTRIM(RTRIM(PV.UnitTypeId)) = 'A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'    Minutes
                                ) -- Minutes
                                THEN CASE WHEN @TermStartDate IS NULL THEN ( ( SAS_NoTerm.MakeupDays / 60 ) / 24 )
                                          ELSE ( ( SAS_ForTerm.MakeupDays / 60 ) / 24 )
                                     END
                           ELSE CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.MakeupDays )
                                     ELSE ( SAS_ForTerm.MakeupDays )
                                END
                      END
            END AS MakeupDays
           ,SE.StuEnrollId
           ,@SUMCreditsEarned AS CreditsEarned
           ,@SUMFACreditsEarned AS FACreditsEarned
           ,SE.DateDetermined AS DateDetermined
           ,SYS.InSchool
           ,P.ProgDescrip
           ,(
              SELECT    SUM(ISNULL(TransAmount,0))
              FROM      saTransactions t1
                       ,saTransCodes t2
              WHERE     t1.TransCodeId = t2.TransCodeId
                        AND t1.StuEnrollId = @StuEnrollId
                        AND t2.IsInstCharge = 1
                        AND t1.TransTypeId IN ( 0,1 )
                        AND t1.Voided = 0
            ) AS TotalCost
           ,(
              SELECT    SUM(ISNULL(TransAmount,0)) AS CurrentBalance
              FROM      saTransactions
              WHERE     StuEnrollId = @StuEnrollId
                        AND Voided = 0
            ) AS CurrentBalance
           ,@CumAverage AS OverallAverage
           ,@cumWeightedGPA AS WeightedAverage_CumGPA
           ,@cumSimpleGPA AS SimpleAverage_CumGPA
           ,@ReturnValue AS StudentGroups
           ,CASE WHEN P.ACId = 5 THEN 'True'
                 ELSE 'False'
            END AS ClockHourProgram
           ,CASE WHEN AAUT1.UnitTypeDescrip = 'Present Absent'     --LTRIM(RTRIM(PV.UnitTypeId)) = 'EF5535C2-142C-4223-AE3C-25A50A153CC6'    Present Absent
                      AND @displayHours = 'hours' THEN 'Hours'
                 WHEN AAUT1.UnitTypeDescrip = 'Present Absent'     --LTRIM(RTRIM(PV.UnitTypeId)) = 'EF5535C2-142C-4223-AE3C-25A50A153CC6'      Present Absent
                      AND @displayHours <> 'hours' THEN 'Days'
                 ELSE 'Hours'
            END AS UnitTypeDescrip
           ,SE.TransferHours AS TransferHours
           ,SE.ContractedGradDate AS ContractedGradDate
    FROM    arStudent S
    INNER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
    INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = PV.UnitTypeId
    INNER JOIN arPrograms P ON PV.ProgId = P.ProgId
    INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
    INNER JOIN sySysStatus SYS ON SC.SysStatusId = SYS.SysStatusId
    LEFT JOIN (
                SELECT  StuEnrollId
                       ,MAX(ActualRunningScheduledDays) AS ScheduledDays
                       ,MAX(AdjustedPresentDays) AS AttendedDays
                       ,MAX(AdjustedAbsentDays) AS AbsentDays
                       ,MAX(ActualRunningMakeupDays) AS MakeupDays
                       ,MAX(StudentAttendedDate) AS LDA
                FROM    syStudentAttendanceSummary
                GROUP BY StuEnrollId
              ) SAS_NoTerm ON SE.StuEnrollId = SAS_NoTerm.StuEnrollId
    LEFT JOIN (
                SELECT  StuEnrollId
                       ,MAX(ActualRunningScheduledDays) AS ScheduledDays
                       ,MAX(AdjustedPresentDays) AS AttendedDays
                       ,MAX(AdjustedAbsentDays) AS AbsentDays
                       ,MAX(ActualRunningMakeupDays) AS MakeupDays
                       ,MAX(StudentAttendedDate) AS LDA
                FROM    syStudentAttendanceSummary
                GROUP BY StuEnrollId
              ) SAS_ForTerm ON SE.StuEnrollId = SAS_ForTerm.StuEnrollId
    WHERE   ( SE.StuEnrollId = @StuEnrollId );

-- =========================================================================================================
-- END  --  Usp_PR_Sub2_Enrollment_Summary
-- =========================================================================================================
GO


--========================================================================================== 
-- Usp_Update_SyCreditsSummary
--==========================================================================================
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[Usp_Update_SyCreditsSummary]')
                    AND type IN ( N'P',N'PC' ) )
    BEGIN
        DROP PROCEDURE Usp_Update_SyCreditsSummary;
    END;
    
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =========================================================================================================
-- Usp_Update_SyCreditsSummary 
-- =========================================================================================================
CREATE PROCEDURE dbo.Usp_Update_SyCreditsSummary
    @pStuEnrollId UNIQUEIDENTIFIER  --VARCHAR(50)
AS
    DECLARE @PrgVerId UNIQUEIDENTIFIER
       ,@rownumber INT
       ,@StuEnrollId UNIQUEIDENTIFIER;
    DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
       ,@PrevReqId UNIQUEIDENTIFIER
       ,@PrevTermId UNIQUEIDENTIFIER
       ,@CreditsAttempted DECIMAL(18,2)
       ,@CreditsEarned DECIMAL(18,2)
       ,@TermId UNIQUEIDENTIFIER
       ,@TermDescrip VARCHAR(50);
    DECLARE @reqid UNIQUEIDENTIFIER
       ,@CourseCodeDescrip VARCHAR(50)
       ,@FinalGrade UNIQUEIDENTIFIER
       ,@FinalScore DECIMAL(18,2)
       ,@ClsSectionId UNIQUEIDENTIFIER
       ,@Grade VARCHAR(50)
       ,@IsGradeBookNotSatisified BIT
       ,@TermStartDate DATETIME;
    DECLARE @IsPass BIT
       ,@IsCreditsAttempted BIT
       ,@IsCreditsEarned BIT
       ,@Completed BIT
       ,@CurrentScore DECIMAL(18,2)
       ,@CurrentGrade VARCHAR(10)
       ,@FinalGradeDesc VARCHAR(50)
       ,@FinalGPA DECIMAL(18,2)
       ,@GrdBkResultId UNIQUEIDENTIFIER;
    DECLARE @Product_WeightedAverage_Credits_GPA DECIMAL(18,2)
       ,@Count_WeightedAverage_Credits DECIMAL(18,2)
       ,@Product_SimpleAverage_Credits_GPA DECIMAL(18,2)
       ,@Count_SimpleAverage_Credits DECIMAL(18,2);
    DECLARE @CreditsPerService DECIMAL(18,2)
       ,@NumberOfServicesAttempted INT
       ,@boolCourseHasLabWorkOrLabHours INT
       ,@sysComponentTypeId INT
       ,@RowCount INT;
    DECLARE @decGPALoop DECIMAL(18,2)
       ,@intCourseCount INT
       ,@decWeightedGPALoop DECIMAL(18,2)
       ,@IsInGPA BIT
       ,@isGradeEligibleForCreditsEarned BIT
       ,@isGradeEligibleForCreditsAttempted BIT;
    DECLARE @ComputedSimpleGPA DECIMAL(18,2)
       ,@ComputedWeightedGPA DECIMAL(18,2)
       ,@CourseCredits DECIMAL(18,2);
    DECLARE @FinAidCreditsEarned DECIMAL(18,2)
       ,@FinAidCredits DECIMAL(18,2)
       ,@TermAverage DECIMAL(18,2)
       ,@TermAverageCount INT
       ,@IsCourseCompleted INT;
    DECLARE @IsWeighted INT;
    SET @decGPALoop = 0;
    SET @intCourseCount = 0;
    SET @decWeightedGPALoop = 0;
    SET @ComputedSimpleGPA = 0;
    SET @ComputedWeightedGPA = 0;
    SET @CourseCredits = 0;
    DECLARE GetCreditsSummary_Cursor CURSOR
    FOR
        SELECT	DISTINCT
                SE.StuEnrollId
               ,T.TermId
               ,T.TermDescrip
               ,T.StartDate
               ,R.ReqId
               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
               ,RES.Score AS FinalScore
               ,RES.GrdSysDetailId AS FinalGrade
               ,GCT.SysComponentTypeId
               ,R.Credits AS CreditsAttempted
               ,CS.ClsSectionId
               ,GSD.Grade
               ,GSD.IsPass
               ,GSD.IsCreditsAttempted
               ,GSD.IsCreditsEarned
               ,SE.PrgVerId
               ,GSD.IsInGPA
               ,R.FinAidCredits AS FinAidCredits
               ,RES.IsCourseCompleted
        FROM    arStuEnrollments SE
        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
        INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
        INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
        INNER JOIN arTerm T ON CS.TermId = T.TermId
        INNER JOIN arReqs R ON CS.ReqId = R.ReqId
        LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
        LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
        LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
        LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
        WHERE   SE.StuEnrollId = @pStuEnrollId
        ORDER BY T.StartDate
               ,T.TermDescrip
               ,R.ReqId; 
    OPEN GetCreditsSummary_Cursor;
    SET @PrevStuEnrollId = NULL;
    SET @PrevTermId = NULL;
    SET @PrevReqId = NULL;
    SET @RowCount = 0;
    FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@TermStartDate,@reqid,@CourseCodeDescrip,@FinalScore,@FinalGrade,
        @sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,@FinAidCredits,
        @IsCourseCompleted;
--,@GrdBkResultId
    WHILE @@FETCH_STATUS = 0
        BEGIN
	
            SET @CourseCredits = @CreditsAttempted; 
            SET @RowCount = @RowCount + 1;
				
            DECLARE @DefaultCampusId UNIQUEIDENTIFIER;
            SET @DefaultCampusId = NULL;
            SET @DefaultCampusId = (
                                     SELECT TOP 1
                                            CampusId
                                     FROM   arStuEnrollments
                                     WHERE  StuEnrollId = @StuEnrollId
                                   );
				
				-- If the output is greater than or equal to 1 there are some grade books not satisfied
            SET @IsGradeBookNotSatisified = (
                                              SELECT    COUNT(*) AS UnsatisfiedWorkUnits
                                              FROM      (
                                                          SELECT    D.*
                                                                   ,CASE WHEN D.MinimumScore > D.Score THEN ( D.MinimumScore - D.Score )
                                                                         ELSE 0
                                                                    END AS Remaining
                                                                   ,CASE WHEN ( D.MinimumScore > D.Score )
                                                                              AND ( D.MustPass = 1 ) THEN 0
                                                                         WHEN D.Score IS NULL
                                                                              AND ( D.Required = 1 ) THEN 0
                                                                         ELSE 1
                                                                    END AS IsWorkUnitSatisfied
                                                          FROM      (
                                                                      SELECT    T.TermId
                                                                               ,T.TermDescrip
                                                                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                               ,R.ReqId
                                                                               ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                                               ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,544 ) THEN GBWD.Number
                                                                                       ELSE (
                                                                                              SELECT    MIN(MinVal)
                                                                                              FROM      arGradeScaleDetails GSD
                                                                                                       ,arGradeSystemDetails GSS
                                                                                              WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                        AND GSS.IsPass = 1
                                                                                            )
                                                                                  END ) AS MinimumScore
                                                                               ,GBR.Score AS Score
                                                                               ,GBWD.Weight AS Weight
                                                                               ,RES.Score AS FinalScore
                                                                               ,RES.GrdSysDetailId AS FinalGrade
                                                                               ,GBWD.Required
                                                                               ,GBWD.MustPass
                                                                               ,GBWD.GrdPolicyId
                                                                               ,( CASE GCT.SysComponentTypeId
                                                                                    WHEN 544 THEN (
                                                                                                    SELECT  SUM(HoursAttended)
                                                                                                    FROM    arExternshipAttendance
                                                                                                    WHERE   StuEnrollId = SE.StuEnrollId
                                                                                                  )
                                                                                    ELSE GBR.Score
                                                                                  END ) AS GradeBookResult
                                                                               ,GCT.SysComponentTypeId
                                                                               ,SE.StuEnrollId
                                                                               ,GBR.GrdBkResultId
                                                                               ,R.Credits AS CreditsAttempted
                                                                               ,CS.ClsSectionId
                                                                               ,GSD.Grade
                                                                               ,GSD.IsPass
                                                                               ,GSD.IsCreditsAttempted
                                                                               ,GSD.IsCreditsEarned
                                                                      FROM      arStuEnrollments SE
                                                                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                                      INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                                                                      INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                                                                      INNER JOIN arTerm T ON CS.TermId = T.TermId
                                                                      INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                                                      LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                                                                                      AND GBR.StuEnrollId = SE.StuEnrollId
                                                                      LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                      LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                      LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                                                      WHERE     SE.StuEnrollId = @StuEnrollId
                                                                                AND T.TermId = @TermId
                                                                                AND R.ReqId = @reqid
                                                                    ) D
                                                        ) E
                                              WHERE     IsWorkUnitSatisfied = 0
                                            ); 
				
            SET @IsWeighted = (
                                SELECT  COUNT(*) AS WeightsCount
                                FROM    (
                                          SELECT    T.TermId
                                                   ,T.TermDescrip
                                                   ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                   ,R.ReqId
                                                   ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                   ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,544 ) THEN GBWD.Number
                                                           ELSE (
                                                                  SELECT    MIN(MinVal)
                                                                  FROM      arGradeScaleDetails GSD
                                                                           ,arGradeSystemDetails GSS
                                                                  WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                            AND GSS.IsPass = 1
                                                                )
                                                      END ) AS MinimumScore
                                                   ,GBR.Score AS Score
                                                   ,GBWD.Weight AS Weight
                                                   ,RES.Score AS FinalScore
                                                   ,RES.GrdSysDetailId AS FinalGrade
                                                   ,GBWD.Required
                                                   ,GBWD.MustPass
                                                   ,GBWD.GrdPolicyId
                                                   ,( CASE GCT.SysComponentTypeId
                                                        WHEN 544 THEN (
                                                                        SELECT  SUM(HoursAttended)
                                                                        FROM    arExternshipAttendance
                                                                        WHERE   StuEnrollId = SE.StuEnrollId
                                                                      )
                                                        ELSE GBR.Score
                                                      END ) AS GradeBookResult
                                                   ,GCT.SysComponentTypeId
                                                   ,SE.StuEnrollId
                                                   ,GBR.GrdBkResultId
                                                   ,R.Credits AS CreditsAttempted
                                                   ,CS.ClsSectionId
                                                   ,GSD.Grade
                                                   ,GSD.IsPass
                                                   ,GSD.IsCreditsAttempted
                                                   ,GSD.IsCreditsEarned
                                          FROM      arStuEnrollments SE
                                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                          INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                                          INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                                          INNER JOIN arTerm T ON CS.TermId = T.TermId
                                          INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                          LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                                                          AND GBR.StuEnrollId = SE.StuEnrollId
                                          LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                          LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                          LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                          WHERE     SE.StuEnrollId = @StuEnrollId
                                                    AND T.TermId = @TermId
                                                    AND R.ReqId = @reqid
                                        ) D
                                WHERE   Weight >= 1
                              );
		
				
	--Check if IsCreditsAttempted is set to True
            IF (
                 @IsCreditsAttempted IS NULL
                 OR @IsCreditsAttempted = 0
               )
                BEGIN
                    SET @CreditsAttempted = 0;
                END;
            IF (
                 @IsCreditsEarned IS NULL
                 OR @IsCreditsEarned = 0
               )
                BEGIN
                    SET @CreditsEarned = 0;
                END;		
			
			
            IF ( @IsGradeBookNotSatisified >= 1 )
                BEGIN
                    IF ( @FinalScore IS NULL )
                        BEGIN
                            SET @CreditsEarned = 0;
                            SET @Completed = 0;
                        END;
                END;
            ELSE
                BEGIN
                    SET @GrdBkResultId = (
                                           SELECT TOP 1
                                                    GrdBkResultId
                                           FROM     arGrdBkResults
                                           WHERE    StuEnrollId = @StuEnrollId
                                                    AND ClsSectionId = @ClsSectionId
                                         ); 
                    IF @GrdBkResultId IS NOT NULL
                        BEGIN
                            IF ( @IsCreditsEarned = 1 )
                                BEGIN
                                    SET @CreditsEarned = @CreditsAttempted;
                                    SET @FinAidCreditsEarned = @FinAidCredits;
                                END; 
                            SET @Completed = 1;										
                        END;
						
                    IF (
                         @GrdBkResultId IS NULL
                         AND @Grade IS NOT NULL
                       )
                        BEGIN
                            IF ( @IsCreditsEarned = 1 )
                                BEGIN
                                    SET @CreditsEarned = @CreditsAttempted;
                                    SET @FinAidCreditsEarned = @FinAidCredits;
                                END; 
                            SET @Completed = 1;	
                        END;
                END;
				
				
				
            IF (
                 @FinalScore IS NOT NULL
                 AND @Grade IS NOT NULL
               )
                BEGIN
                    IF ( @IsCreditsEarned = 1 )
                        BEGIN
                            SET @CreditsEarned = @CreditsAttempted;
                            SET @FinAidCreditsEarned = @FinAidCredits;
                        END; 
                    SET @Completed = 1;
				
                END;
				
				-- If course is not part of the program version definition do not add credits earned and credits attempted
				-- set the credits earned and attempted to zero
            DECLARE @coursepartofdefinition INT;
            SET @coursepartofdefinition = 0;
				
				--Commented by Balaji on 1/16/2016 as it conflicts with Course Equivalency
				-- Due to following logic if a student takes a equivalent course, credits attempted considered as 0							

				--set @coursepartofdefinition = (select COUNT(*) as RowCountOfProgramDefinition from 
				--									(
				--										select * from arProgVerDef where PrgVerId=@PrgVerId and ReqId=@ReqId
				--											union
				--										select * from arProgVerDef where PrgVerId=@PrgVerId and ReqId in
				--											(select GrpId from arReqGrpDef where ReqId=@ReqId)
				--									) dt
				
				--			)
				--if (@coursepartofdefinition = 0)
				--	begin
				--		set @CreditsEarned = 0
				--		set @CreditsAttempted = 0
				--		set @FinAidCreditsEarned = 0
				--	end
		
				-- Check the grade scale associated with the class section and figure out of the final score was a passing score
            DECLARE @coursepassrowcount INT;
            SET @coursepassrowcount = 0;
            IF ( @FinalScore IS NOT NULL )
					
					-- If the student scores 56 and the score is a passing score then we consider this course as completed
                BEGIN
                    SET @coursepassrowcount = (
                                                SELECT  COUNT(t2.MinVal) AS IsCourseCompleted
                                                FROM    arClassSections t1
                                                INNER JOIN arGradeScaleDetails t2 ON t1.GrdScaleId = t2.GrdScaleId
                                                INNER JOIN arGradeSystemDetails t3 ON t2.GrdSysDetailId = t3.GrdSysDetailId
                                                WHERE   t1.ClsSectionId = @ClsSectionId
                                                        AND t3.IsPass = 1
                                                        AND @FinalScore >= t2.MinVal
                                              );
                    IF @coursepassrowcount >= 1
                        BEGIN
                            SET @Completed = 1;
                        END;
                    ELSE
                        BEGIN
                            SET @Completed = 0;
                        END;
                END;
				-- If Student Scored a Failing Grade (IsPass set to 0 in Grade System)
				-- then mark this course as Incomplete
            IF ( @FinalGrade IS NOT NULL )
                BEGIN
                    IF ( @IsPass = 0 )
                        BEGIN
                            SET @Completed = 0;
                            IF ( @IsCreditsEarned = 0 )
                                BEGIN
                                    SET @CreditsEarned = 0; 
                                    SET @FinAidCreditsEarned = 0;
                                END; 
                            IF ( @IsCreditsAttempted = 0 )
                                BEGIN
                                    SET @CreditsAttempted = 0;
                                END;
                        END;
                END;
			
            SET @CurrentScore = (
                                  SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                 ELSE NULL
                                            END AS CurrentScore
                                  FROM      (
                                              SELECT    InstrGrdBkWgtDetailId
                                                       ,Code
                                                       ,Descrip
                                                       ,Weight AS GradeBookWeight
                                                       ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                             ELSE 0
                                                        END AS ActualWeight
                                              FROM      (
                                                          SELECT    C.InstrGrdBkWgtDetailId
                                                                   ,D.Code
                                                                   ,D.Descrip
                                                                   ,ISNULL(C.Weight,0) AS Weight
                                                                   ,C.Number AS MinNumber
                                                                   ,C.GrdPolicyId
                                                                   ,C.Parameter AS Param
                                                                   ,X.GrdScaleId
                                                                   ,SUM(GR.Score) AS Score
                                                                   ,COUNT(D.Descrip) AS NumberOfComponents
                                                          FROM      (
                                                                      SELECT DISTINCT TOP 1
                                                                                A.InstrGrdBkWgtId
                                                                               ,A.EffectiveDate
                                                                               ,B.GrdScaleId
                                                                      FROM      arGrdBkWeights A
                                                                               ,arClassSections B
                                                                      WHERE     A.ReqId = B.ReqId
                                                                                AND A.EffectiveDate <= B.StartDate
                                                                                AND B.ClsSectionId = @ClsSectionId
                                                                      ORDER BY  A.EffectiveDate DESC
                                                                    ) X
                                                                   ,arGrdBkWgtDetails C
                                                                   ,arGrdComponentTypes D
                                                                   ,arGrdBkResults GR
                                                          WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                    AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                    AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                    AND D.SysComponentTypeId NOT IN ( 500,503 )
                                                                    AND GR.StuEnrollId = @StuEnrollId
                                                                    AND GR.ClsSectionId = @ClsSectionId
                                                                    AND GR.Score IS NOT NULL
                                                          GROUP BY  C.InstrGrdBkWgtDetailId
                                                                   ,D.Code
                                                                   ,D.Descrip
                                                                   ,C.Weight
                                                                   ,C.Number
                                                                   ,C.GrdPolicyId
                                                                   ,C.Parameter
                                                                   ,X.GrdScaleId
                                                        ) S
                                              GROUP BY  InstrGrdBkWgtDetailId
                                                       ,Code
                                                       ,Descrip
                                                       ,Weight
                                                       ,NumberOfComponents
                                            ) FinalTblToComputeCurrentScore
                                );
            IF ( @CurrentScore IS NULL )
                BEGIN
				-- instructor grade books
                    SET @CurrentScore = (
                                          SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                         ELSE NULL
                                                    END AS CurrentScore
                                          FROM      (
                                                      SELECT    InstrGrdBkWgtDetailId
                                                               ,Code
                                                               ,Descrip
                                                               ,Weight AS GradeBookWeight
                                                               ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                     ELSE 0
                                                                END AS ActualWeight
                                                      FROM      (
                                                                  SELECT    C.InstrGrdBkWgtDetailId
                                                                           ,D.Code
                                                                           ,D.Descrip
                                                                           ,ISNULL(C.Weight,0) AS Weight
                                                                           ,C.Number AS MinNumber
                                                                           ,C.GrdPolicyId
                                                                           ,C.Parameter AS Param
                                                                           ,X.GrdScaleId
                                                                           ,SUM(GR.Score) AS Score
                                                                           ,COUNT(D.Descrip) AS NumberOfComponents
                                                                  FROM      (
                                                                              --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
															--FROM          arGrdBkWeights A,arClassSections B        
															--WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
															--ORDER BY      A.EffectiveDate DESC
                                                                              SELECT DISTINCT TOP 1
                                                                                        t1.InstrGrdBkWgtId
                                                                                       ,t1.GrdScaleId
                                                                              FROM      arClassSections t1
                                                                                       ,arGrdBkWeights t2
                                                                              WHERE     t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                                        AND t1.ClsSectionId = @ClsSectionId
                                                                            ) X
                                                                           ,arGrdBkWgtDetails C
                                                                           ,arGrdComponentTypes D
                                                                           ,arGrdBkResults GR
                                                                  WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                            AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                            AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                            AND
													-- D.SysComponentTypeID not in (500,503) and 
                                                                            GR.StuEnrollId = @StuEnrollId
                                                                            AND GR.ClsSectionId = @ClsSectionId
                                                                            AND GR.Score IS NOT NULL
                                                                  GROUP BY  C.InstrGrdBkWgtDetailId
                                                                           ,D.Code
                                                                           ,D.Descrip
                                                                           ,C.Weight
                                                                           ,C.Number
                                                                           ,C.GrdPolicyId
                                                                           ,C.Parameter
                                                                           ,X.GrdScaleId
                                                                ) S
                                                      GROUP BY  InstrGrdBkWgtDetailId
                                                               ,Code
                                                               ,Descrip
                                                               ,Weight
                                                               ,NumberOfComponents
                                                    ) FinalTblToComputeCurrentScore
                                        );	
			
                END;

            IF ( @CurrentScore IS NOT NULL )
                BEGIN
                    SET @CurrentGrade = (
                                          SELECT    t2.Grade
                                          FROM      arGradeScaleDetails t1
                                                   ,arGradeSystemDetails t2
                                          WHERE     t1.GrdSysDetailId = t2.GrdSysDetailId
                                                    AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                           FROM     arClassSections
                                                                           WHERE    ClsSectionId = @ClsSectionId )
                                                    AND @CurrentScore >= t1.MinVal
                                                    AND @CurrentScore <= t1.MaxVal
                                        );
						
                END;	
            ELSE
                BEGIN
                    SET @CurrentGrade = NULL;
                END;
		
			
            IF (
                 @CurrentScore IS NULL
                 AND @CurrentGrade IS NULL
                 AND @FinalScore IS NULL
                 AND @FinalGrade IS NULL
               )
                BEGIN
                    SET @Completed = 0;
                    SET @CreditsAttempted = 0;
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
			
			
            IF (
                 @FinalScore IS NOT NULL
                 OR @FinalGrade IS NOT NULL
               )
                BEGIN

                    SET @FinalGradeDesc = (
                                            SELECT  Grade
                                            FROM    arGradeSystemDetails
                                            WHERE   GrdSysDetailId = @FinalGrade
                                          );
					

				
                    IF ( @FinalGradeDesc IS NULL )
                        BEGIN
                            SET @FinalGradeDesc = (
                                                    SELECT  t2.Grade
                                                    FROM    arGradeScaleDetails t1
                                                           ,arGradeSystemDetails t2
                                                    WHERE   t1.GrdSysDetailId = t2.GrdSysDetailId
                                                            AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                                   FROM     arClassSections
                                                                                   WHERE    ClsSectionId = @ClsSectionId )
                                                            AND @FinalScore >= t1.MinVal
                                                            AND @FinalScore <= t1.MaxVal
                                                  );
                        END;
                    SET @FinalGPA = (
                                      SELECT    GPA
                                      FROM      arGradeSystemDetails
                                      WHERE     GrdSysDetailId = @FinalGrade
                                    );
                    IF @FinalGPA IS NULL
                        BEGIN
                            SET @FinalGPA = (
                                              SELECT    t2.GPA
                                              FROM      arGradeScaleDetails t1
                                                       ,arGradeSystemDetails t2
                                              WHERE     t1.GrdSysDetailId = t2.GrdSysDetailId
                                                        AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                               FROM     arClassSections
                                                                               WHERE    ClsSectionId = @ClsSectionId )
                                                        AND @FinalScore >= t1.MinVal
                                                        AND @FinalScore <= t1.MaxVal
                                            );
                        END;
                END;
            ELSE
                BEGIN
                    SET @FinalGradeDesc = NULL;
                    SET @FinalGPA = NULL;
                END;
			

            SET @isGradeEligibleForCreditsEarned = (
                                                     SELECT t2.IsCreditsEarned
                                                     FROM   arGradeScaleDetails t1
                                                           ,arGradeSystemDetails t2
                                                     WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                            AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                                   FROM     arClassSections
                                                                                   WHERE    ClsSectionId = @ClsSectionId )
                                                            AND t2.Grade = @FinalGradeDesc
                                                   ); 
												
            SET @isGradeEligibleForCreditsAttempted = (
                                                        SELECT  t2.IsCreditsAttempted
                                                        FROM    arGradeScaleDetails t1
                                                               ,arGradeSystemDetails t2
                                                        WHERE   t1.GrdSysDetailId = t2.GrdSysDetailId
                                                                AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                                       FROM     arClassSections
                                                                                       WHERE    ClsSectionId = @ClsSectionId )
                                                                AND t2.Grade = @FinalGradeDesc
                                                      ); 
												
            IF ( @isGradeEligibleForCreditsEarned IS NULL )
                BEGIN
                    SET @isGradeEligibleForCreditsEarned = (
                                                             SELECT TOP 1
                                                                    t2.IsCreditsEarned
                                                             FROM   arGradeSystemDetails t2
                                                             WHERE  t2.Grade = @FinalGradeDesc
                                                           );
                END;
												
            IF ( @isGradeEligibleForCreditsAttempted IS NULL )
                BEGIN
                    SET @isGradeEligibleForCreditsAttempted = (
                                                                SELECT TOP 1
                                                                        t2.IsCreditsAttempted
                                                                FROM    arGradeSystemDetails t2
                                                                WHERE   t2.Grade = @FinalGradeDesc
                                                              );
                END;			
				
            IF @isGradeEligibleForCreditsEarned = 0
                BEGIN
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
            IF @isGradeEligibleForCreditsAttempted = 0
                BEGIN
                    SET @CreditsAttempted = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
												
            IF ( @IsPass = 0 )
                BEGIN
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
			--For Letter Grade Schools if the score is null but final grade was posted then the 
			--Final grade will be the current grade
            IF @CurrentGrade IS NULL
                AND @FinalGradeDesc IS NOT NULL
                BEGIN
                    SET @CurrentGrade = @FinalGradeDesc;
                END;

            IF (
                 @sysComponentTypeId = 503
                 OR @sysComponentTypeId = 500
               ) -- Lab work or Lab Hours
                BEGIN
				-- This course has lab work and lab hours
                    IF ( @Completed = 0 )
                        BEGIN
                            SET @CreditsPerService = (
                                                       SELECT TOP 1
                                                                GD.CreditsPerService
                                                       FROM     arGrdBkWeights GBW
                                                               ,arGrdComponentTypes GC
                                                               ,arGrdBkWgtDetails GD
                                                       WHERE    GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
                                                                AND GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                                                AND GBW.ReqId = @reqid
                                                                AND GC.SysComponentTypeId IN ( 500,503 )
                                                     );
                            SET @NumberOfServicesAttempted = (
                                                               SELECT TOP 1
                                                                        GBR.Score AS NumberOfServicesAttempted
                                                               FROM     arStuEnrollments SE
                                                               INNER JOIN arGrdBkResults GBR ON SE.StuEnrollId = GBR.StuEnrollId
                                                                                                AND GBR.ClsSectionId = @ClsSectionId
                                                             );

                            SET @CreditsEarned = ISNULL(@CreditsPerService,0) * ISNULL(@NumberOfServicesAttempted,0);
                        END;
                END;
			
            DECLARE @rowAlreadyInserted INT; 
            SET @rowAlreadyInserted = 0;
			
			-- Get the final Gpa only when IsCreditsAttempted is set to 1 and IsInGPA is set to 1
            IF @IsInGPA = 1
                BEGIN
                    IF ( @IsCreditsAttempted = 0 )
                        BEGIN
                            SET @FinalGPA = NULL; 
                        END;
                END;
            ELSE
                BEGIN
                    SET @FinalGPA = NULL; 
                END;

            IF @FinalScore IS NOT NULL --and @CurrentScore is null
                BEGIN
                    SET @CurrentScore = @FinalScore; 
                END;
			
			--Select * from syConfigAppSetValues
				
			
			-- Rally case DE 738 KeyBoarding Courses
            DECLARE @GradesFormat VARCHAR(50);
			---- PRINT @DefaultCampusId
			--(select Top 1 Value from syConfigAppSetValues where SettingId=47 and (CampusId=@DefaultCampusId or CampusId is NULL))
            SET @GradesFormat = (
                                  SELECT TOP 1
                                            Value
                                  FROM      syConfigAppSetValues
                                  WHERE     SettingId = 47
                                            AND (
                                                  CampusId = @DefaultCampusId
                                                  OR CampusId IS NULL
                                                )
                                ); -- 47 refers to grades format
			
			---- PRINT @GradesFormat
				-- This condition is met only for numeric grade schools
            IF (
                 @IsGradeBookNotSatisified = 0
                 AND @IsWeighted = 0
                 AND @FinalScore IS NULL
                 AND @IsCourseCompleted = 1
                 AND @FinalGradeDesc IS NULL
                 AND LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
               )
                BEGIN
                    SET @Completed = 1;	
                    SET @CreditsAttempted = (
                                              SELECT    Credits
                                              FROM      arReqs
                                              WHERE     ReqId = @reqid
                                            );
                    SET @FinAidCredits = (
                                           SELECT   FinAidCredits
                                           FROM     arReqs
                                           WHERE    ReqId = @reqid
                                         );
                    SET @CreditsAttempted = @CreditsAttempted; 
                    SET @CreditsEarned = @CreditsAttempted; 
                    SET @FinAidCreditsEarned = @FinAidCredits;							
                END;
			
			-- DE748 Name: ROSS: Completed field should also check for the Must Pass property of the work unit. 
            IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                AND @IsGradeBookNotSatisified >= 1
                BEGIN
                    SET @Completed = 0;
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
			
			--DE738 Name: ROSS: Progress Report not taking care of courses that are not weighted. 
            IF (
                 LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                 AND @Completed = 1
                 AND @FinalScore IS NULL
                 AND @FinalGradeDesc IS NULL
               )
                BEGIN
                    SET @CreditsAttempted = @CreditsAttempted; 
                    SET @CreditsEarned = @CreditsAttempted; 
                    SET @FinAidCreditsEarned = @FinAidCredits;
                END;

					-- In Ross Example : Externship, the student may not have completed the course but once he attempts a work unit
					-- we need to take the credits as attempted
            IF (
                 LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                 AND @Completed = 0
                 AND @FinalScore IS NULL
                 AND @FinalGradeDesc IS NULL
               )
                BEGIN
                    DECLARE @rowcount4 INT;
                    SET @rowcount4 = (
                                       SELECT   COUNT(*)
                                       FROM     arGrdBkResults
                                       WHERE    StuEnrollId = @StuEnrollId
                                                AND ClsSectionId = @ClsSectionId
                                                AND Score IS NOT NULL
                                     );
                    IF @rowcount4 >= 1
                        BEGIN
										-- -- PRINT 'Gets in to if'
                            SET @CreditsAttempted = (
                                                      SELECT    Credits
                                                      FROM      arReqs
                                                      WHERE     ReqId = @reqid
                                                    );
                            SET @CreditsEarned = 0;
                            SET @FinAidCreditsEarned = 0;
                        END;
                    ELSE
                        BEGIN
                            SET @rowcount4 = (
                                               SELECT   COUNT(*)
                                               FROM     arGrdBkConversionResults
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND ReqId = @reqid
                                                        AND TermId = @TermId
                                                        AND Score IS NOT NULL
                                             );
                            IF @rowcount4 >= 1
                                BEGIN
                                    SET @CreditsAttempted = (
                                                              SELECT    Credits
                                                              FROM      arReqs
                                                              WHERE     ReqId = @reqid
                                                            );
                                    SET @CreditsEarned = 0;
                                    SET @FinAidCreditsEarned = 0;
                                END;
                        END;

							--For Externship Attendance						
                    IF @sysComponentTypeId = 544
                        BEGIN
                            SET @rowcount4 = (
                                               SELECT   COUNT(*)
                                               FROM     arExternshipAttendance
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND HoursAttended >= 1
                                             );
                            IF @rowcount4 >= 1
                                BEGIN
                                    SET @CreditsAttempted = (
                                                              SELECT    Credits
                                                              FROM      arReqs
                                                              WHERE     ReqId = @reqid
                                                            );
                                    SET @CreditsEarned = 0;
                                    SET @FinAidCreditsEarned = 0;
                                END;

                        END;
				
                END;
			
			-- If the final grade is not null the final grade will over ride current grade 
            IF @FinalGradeDesc IS NOT NULL
                BEGIN
                    SET @CurrentGrade = @FinalGradeDesc; 
				
                END;

			
			-- For Letter Grade Schools, if any one of work unit is attempted and if no final grade is posted then 
			-- set credit attempted
            DECLARE @IsScorePostedForAnyWorkUnit INT; -- If value>=1 then score was posted
            SET @IsScorePostedForAnyWorkUnit = (
                                                 SELECT COUNT(*)
                                                 FROM   arGrdBkResults
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND ClsSectionId = @ClsSectionId
                                                        AND Score IS NOT NULL
                                               );
            IF (
                 LOWER(LTRIM(RTRIM(@GradesFormat))) = 'letter'
                 AND @FinalGradeDesc IS NULL
                 AND @IsScorePostedForAnyWorkUnit >= 1
               )
                BEGIN
                    SET @Completed = 0;
                    SET @CreditsAttempted = (
                                              SELECT    Credits
                                              FROM      arReqs
                                              WHERE     ReqId = @reqid
                                            );
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;				
                END;
			
			
			-- DE 996 Transfer Grades has completed set to no even when the credits were earned.
			-- If Credits was earned set completed to yes
            IF @IsCreditsEarned = 1
                BEGIN
                    SET @Completed = 1;
                END;

			-- Modified by Balaji to take IsCourseCompleted flag in to account
            IF @Completed = 0
                AND @IsCourseCompleted = 1
                BEGIN
                    SET @Completed = 1;
                END;

			

            IF @Completed = 0
                AND @IsCourseCompleted = 1
                BEGIN
                    SET @Completed = 1;
                END;

            DECLARE @IsExternship INT
               ,@intExternshipCount INT;
            SET @intExternshipCount = (
                                        SELECT  COUNT(*)
                                        FROM    arReqs
                                        WHERE   ReqId = @reqid
                                                AND IsExternship = 1
                                      );
			
		
			--Externship Courses
            IF @intExternshipCount = 1
                AND @Completed = 1
                AND @CreditsAttempted > 0
                BEGIN
                    SET @IsCreditsAttempted = 1;
                    SET @IsCreditsEarned = 1;
                    SET @CreditsEarned = @CreditsAttempted;
				
                END;

			--Lab Work and Lab Hours
            IF (
                 @sysComponentTypeId = 500
                 OR @sysComponentTypeId = 503
               )
                AND @Completed = 1
                AND @CreditsAttempted > 0
                BEGIN
                    SET @IsCreditsAttempted = 1;
                    SET @IsCreditsEarned = 1;
                    SET @CreditsEarned = @CreditsAttempted;
                END;

            IF @Completed = 1
                BEGIN
                    SET @FinAidCreditsEarned = @FinAidCredits;
                END;

			-- If course is externship and no attendance yet, set credits attempted to 0
            IF @intExternshipCount = 1
                BEGIN
                    DECLARE @ExternshipAttValue DECIMAL(18,2); 
                    SET @ExternshipAttValue = 0.00;
                    SET @ExternshipAttValue = (
                                                SELECT  SUM(ISNULL(HoursAttended,0.00))
                                                FROM    arExternshipAttendance
                                                WHERE   StuEnrollId = @StuEnrollId
                                              );
                    IF @ExternshipAttValue = 0.00
                        OR @ExternshipAttValue IS NULL
                        BEGIN
                            SET @CreditsAttempted = 0;
                            SET @CreditsEarned = 0;
                            SET @Completed = 0;
                        END;
                END;


			--declare @rowcount_CA int
			--set @rowcount_CA = (select Count(*) from arGrdBkResults where StuEnrollId=@StuEnrollId and ClsSectionId=@ClsSectionId and Score is not null)
			--if @rowcount_CA<=0 OR @rowcount IS NULL
			--	begin
			--				set @CreditsAttempted = 0
			--				set @CreditsEarned = 0
			--				set @FinAidCreditsEarned = 0
			--	end

			

            DELETE  FROM syCreditSummary
            WHERE   StuEnrollId = @StuEnrollId
                    AND TermId = @TermId
                    AND ReqId = @reqid
                    --AND ClsSectionId = @ClsSectionId;
                    AND (
                          ClsSectionId = @ClsSectionId
                          OR ClsSectionId IS NULL
                        );

            INSERT  INTO syCreditSummary
            VALUES  ( @StuEnrollId,@TermId,@TermDescrip,@reqid,@CourseCodeDescrip,@ClsSectionId,@CreditsEarned,@CreditsAttempted,@CurrentScore,@CurrentGrade,
                      @FinalScore,@FinalGradeDesc,@Completed,@FinalGPA,@Product_WeightedAverage_Credits_GPA,@Count_WeightedAverage_Credits,
                      @Product_SimpleAverage_Credits_GPA,@Count_SimpleAverage_Credits,'sa',GETDATE(),@ComputedSimpleGPA,@ComputedWeightedGPA,@CourseCredits,NULL,
                      NULL,@FinAidCreditsEarned,NULL,NULL,@TermStartDate );

            DECLARE @wCourseCredits DECIMAL(18,2)
               ,@wWeighted_GPA_Credits DECIMAL(18,2)
               ,@sCourseCredits DECIMAL(18,2)
               ,@sSimple_GPA_Credits DECIMAL(18,2);
			-- For weighted average
            SET @ComputedWeightedGPA = 0;
            SET @ComputedSimpleGPA = 0;
            SET @wCourseCredits = (
                                    SELECT  SUM(coursecredits)
                                    FROM    syCreditSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND TermId = @TermId
                                            AND FinalGPA IS NOT NULL
                                  );
            SET @wWeighted_GPA_Credits = (
                                           SELECT   SUM(coursecredits * FinalGPA)
                                           FROM     syCreditSummary
                                           WHERE    StuEnrollId = @StuEnrollId
                                                    AND TermId = @TermId
                                                    AND FinalGPA IS NOT NULL
                                         );
			
            IF @wCourseCredits >= 1
                BEGIN
                    SET @ComputedWeightedGPA = @wWeighted_GPA_Credits / @wCourseCredits;
                END;

			--For Simple Average
            SET @sCourseCredits = (
                                    SELECT  COUNT(*)
                                    FROM    syCreditSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND TermId = @TermId
                                            AND FinalGPA IS NOT NULL
                                  );
            SET @sSimple_GPA_Credits = (
                                         SELECT SUM(FinalGPA)
                                         FROM   syCreditSummary
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND TermId = @TermId
                                                AND FinalGPA IS NOT NULL
                                       );
			
			
			-- 3.7 SP2 Changes
			-- Include Equivalent Courses
            DECLARE @EquivCourse_SA_CC INT
               ,@EquivCourse_SA_GPA DECIMAL(18,2);
            SET @EquivCourse_SA_CC = (
                                       SELECT   ISNULL(COUNT(Credits),0) AS Credits
                                       FROM     arReqs
                                       WHERE    ReqId IN ( SELECT   CE.EquivReqId
                                                           FROM     arCourseEquivalent CE
                                                           INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                           INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                           WHERE    StuEnrollId = @StuEnrollId
                                                                    AND R.GrdSysDetailId IS NOT NULL )
                                     );

            SET @EquivCourse_SA_GPA = (
                                        SELECT  SUM(ISNULL(GSD.GPA,0.00))
                                        FROM    arCourseEquivalent CE
                                        INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                        INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                        INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                        WHERE   StuEnrollId = @StuEnrollId
                                                AND R.GrdSysDetailId IS NOT NULL
                                      );
									 
            SET @sCourseCredits = @sCourseCredits;                --+ ISNULL(@EquivCourse_SA_CC,0.00);
            SET @sSimple_GPA_Credits = @sSimple_GPA_Credits;      --+ ISNULL(@EquivCourse_SA_GPA,0.00);
			-- 3.7 SP2 Changes Ends here
			
            IF @sCourseCredits >= 1
                BEGIN
                    SET @ComputedSimpleGPA = @sSimple_GPA_Credits / @sCourseCredits;
                END; 

					
			--CumulativeGPA
            DECLARE @cumCourseCredits DECIMAL(18,2)
               ,@cumWeighted_GPA_Credits DECIMAL(18,2)
               ,@cumWeightedGPA DECIMAL(18,2);
            SET @cumWeightedGPA = 0;
            SET @cumCourseCredits = (
                                      SELECT    SUM(coursecredits)
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = @StuEnrollId
                                                AND FinalGPA IS NOT NULL
                                    );
            SET @cumWeighted_GPA_Credits = (
                                             SELECT SUM(coursecredits * FinalGPA)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                           );
			
            DECLARE @EquivCourse_WGPA_CC1 INT
               ,@EquivCourse_WGPA_GPA1 DECIMAL(18,2);
            SET @EquivCourse_WGPA_CC1 = (
                                          SELECT    SUM(ISNULL(Credits,0)) AS Credits
                                          FROM      arReqs
                                          WHERE     ReqId IN ( SELECT   CE.EquivReqId
                                                               FROM     arCourseEquivalent CE
                                                               INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                               INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                               WHERE    StuEnrollId = @StuEnrollId
                                                                        AND R.GrdSysDetailId IS NOT NULL )
                                        );

            SET @EquivCourse_WGPA_GPA1 = (
                                           SELECT   SUM(GSD.GPA * R1.Credits)
                                           FROM     arCourseEquivalent CE
                                           INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                           INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                           INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                           INNER JOIN arReqs R1 ON R1.ReqId = CE.ReqId
                                           WHERE    StuEnrollId = @StuEnrollId
                                                    AND R.GrdSysDetailId IS NOT NULL
                                         );

            SET @cumCourseCredits = @sCourseCredits;                         -- + ISNULL(@EquivCourse_WGPA_CC1,0.00);
            SET @cumWeighted_GPA_Credits = @sSimple_GPA_Credits;             -- + ISNULL(@EquivCourse_WGPA_GPA1,0.00);

            SET @cumCourseCredits = @cumCourseCredits;                       -- + ISNULL(@EquivCourse_WGPA_CC1,0.00);
            SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits;         -- + ISNULL(@EquivCourse_WGPA_GPA1,0.00);


            IF @cumCourseCredits >= 1
                BEGIN
                    SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                END; 
			
			--CumulativeSimpleGPA
            DECLARE @cumSimpleCourseCredits DECIMAL(18,2)
               ,@cumSimple_GPA_Credits DECIMAL(18,2)
               ,@cumSimpleGPA DECIMAL(18,2);
            SET @cumSimpleGPA = 0;
            SET @cumSimpleCourseCredits = (
                                            SELECT  COUNT(coursecredits)
                                            FROM    syCreditSummary
                                            WHERE   StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                          );
            SET @cumSimple_GPA_Credits = (
                                           SELECT   SUM(FinalGPA)
                                           FROM     syCreditSummary
                                           WHERE    StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                         );
			
            DECLARE @EquivCourse_SA_CC2 INT
               ,@EquivCourse_SA_GPA2 DECIMAL(18,2);
            SET @EquivCourse_SA_CC2 = (
                                        SELECT  ISNULL(COUNT(Credits),0) AS Credits
                                        FROM    arReqs
                                        WHERE   ReqId IN ( SELECT   CE.EquivReqId
                                                           FROM     arCourseEquivalent CE
                                                           INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                           INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                           WHERE    StuEnrollId = @StuEnrollId
                                                                    AND R.GrdSysDetailId IS NOT NULL )
                                      );

            SET @EquivCourse_SA_GPA2 = (
                                         SELECT SUM(ISNULL(GSD.GPA,0.00))
                                         FROM   arCourseEquivalent CE
                                         INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                         INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                         INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND R.GrdSysDetailId IS NOT NULL
                                       );

			
            SET @cumSimpleCourseCredits = @cumSimpleCourseCredits;           -- + ISNULL(@EquivCourse_SA_CC2,0.00);
            SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits;             -- + ISNULL(@EquivCourse_SA_GPA2,0.00);
			
            IF @cumSimpleCourseCredits >= 1
                BEGIN
                    SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                END; 
						
			--Average calculation
            DECLARE @termAverageSum DECIMAL(18,2)
               ,@CumAverage DECIMAL(18,2)
               ,@cumAverageSum DECIMAL(18,2)
               ,@cumAveragecount INT;
			
			-- Term Average
            SET @TermAverageCount = (
                                      SELECT    COUNT(*)
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = @StuEnrollId 
									--and Completed=1 
                                                AND TermId = @TermId
                                                AND FinalScore IS NOT NULL
                                    );
            SET @termAverageSum = (
                                    SELECT  SUM(FinalScore)
                                    FROM    syCreditSummary
                                    WHERE   StuEnrollId = @StuEnrollId 
									--and Completed=1 
                                            AND TermId = @TermId
                                            AND FinalScore IS NOT NULL
                                  );
            SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
            SET @cumAveragecount = (
                                     SELECT COUNT(*)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId 
									--and Completed=1 
                                            AND FinalScore IS NOT NULL
                                   );
            SET @cumAverageSum = (
                                   SELECT   SUM(FinalScore)
                                   FROM     syCreditSummary
                                   WHERE    StuEnrollId = @StuEnrollId
                                            AND 
									--Completed=1 and 
                                            FinalScore IS NOT NULL
                                 );
            SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
            UPDATE  syCreditSummary
            SET     TermGPA_Simple = @ComputedSimpleGPA
                   ,TermGPA_Weighted = @ComputedWeightedGPA
                   ,Average = @TermAverage
            WHERE   StuEnrollId = @StuEnrollId
                    AND TermId = @TermId; 
			
			--Update Cumulative GPA
            UPDATE  syCreditSummary
            SET     CumulativeGPA = @cumWeightedGPA
                   ,CumulativeGPA_Simple = @cumSimpleGPA
                   ,CumAverage = @CumAverage
            WHERE   StuEnrollId = @StuEnrollId;
			
														
            SET @PrevStuEnrollId = @StuEnrollId; 
            SET @PrevTermId = @TermId; 
            SET @PrevReqId = @reqid;

            FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@TermStartDate,@reqid,@CourseCodeDescrip,@FinalScore,@FinalGrade,
                @sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,@FinAidCredits,
                @IsCourseCompleted;
        END;
    CLOSE GetCreditsSummary_Cursor;
    DEALLOCATE GetCreditsSummary_Cursor;

-- =========================================================================================================
-- End Usp_Update_SyCreditsSummary 
-- =========================================================================================================
GO




-- =========================================================================================================
-- Usp_UpdateTransferCredits_SyCreditsSummary
-- =========================================================================================================
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[Usp_UpdateTransferCredits_SyCreditsSummary]')
                    AND type IN ( N'P',N'PC' ) )
    BEGIN
        DROP PROCEDURE Usp_UpdateTransferCredits_SyCreditsSummary;
    END;
    
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =========================================================================================================
-- Usp_UpdateTransferCredits_SyCreditsSummary
-- =========================================================================================================
CREATE PROCEDURE dbo.Usp_UpdateTransferCredits_SyCreditsSummary
    @pStuEnrollId UNIQUEIDENTIFIER  --VARCHAR(50)
AS
    DECLARE @PrgVerId UNIQUEIDENTIFIER
       ,@rownumber INT
       ,@StuEnrollId UNIQUEIDENTIFIER;
    DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
       ,@PrevReqId UNIQUEIDENTIFIER
       ,@PrevTermId UNIQUEIDENTIFIER
       ,@CreditsAttempted DECIMAL(18,2)
       ,@CreditsEarned DECIMAL(18,2)
       ,@TermId UNIQUEIDENTIFIER
       ,@TermDescrip VARCHAR(50);
    DECLARE @reqid UNIQUEIDENTIFIER
       ,@CourseCodeDescrip VARCHAR(50)
       ,@FinalGrade UNIQUEIDENTIFIER
       ,@FinalScore DECIMAL(18,2)
       ,@ClsSectionId UNIQUEIDENTIFIER
       ,@Grade VARCHAR(50)
       ,@IsGradeBookNotSatisified BIT
       ,@TermStartDate DATETIME;
    DECLARE @IsPass BIT
       ,@IsCreditsAttempted BIT
       ,@IsCreditsEarned BIT
       ,@Completed BIT
       ,@CurrentScore DECIMAL(18,2)
       ,@CurrentGrade VARCHAR(10)
       ,@FinalGradeDesc VARCHAR(50)
       ,@FinalGPA DECIMAL(18,2)
       ,@GrdBkResultId UNIQUEIDENTIFIER;
    DECLARE @Product_WeightedAverage_Credits_GPA DECIMAL(18,2)
       ,@Count_WeightedAverage_Credits DECIMAL(18,2)
       ,@Product_SimpleAverage_Credits_GPA DECIMAL(18,2)
       ,@Count_SimpleAverage_Credits DECIMAL(18,2);
    DECLARE @CreditsPerService DECIMAL(18,2)
       ,@NumberOfServicesAttempted INT
       ,@boolCourseHasLabWorkOrLabHours INT
       ,@sysComponentTypeId INT
       ,@RowCount INT;
    DECLARE @decGPALoop DECIMAL(18,2)
       ,@intCourseCount INT
       ,@decWeightedGPALoop DECIMAL(18,2)
       ,@IsInGPA BIT
       ,@isGradeEligibleForCreditsEarned BIT
       ,@isGradeEligibleForCreditsAttempted BIT;
    DECLARE @ComputedSimpleGPA DECIMAL(18,2)
       ,@ComputedWeightedGPA DECIMAL(18,2)
       ,@CourseCredits DECIMAL(18,2);
    DECLARE @FinAidCreditsEarned DECIMAL(18,2)
       ,@FinAidCredits DECIMAL(18,2)
       ,@TermAverage DECIMAL(18,2)
       ,@TermAverageCount INT;
    DECLARE @IsWeighted INT;
    SET @decGPALoop = 0;
    SET @intCourseCount = 0;
    SET @decWeightedGPALoop = 0;
    SET @ComputedSimpleGPA = 0;
    SET @ComputedWeightedGPA = 0;
    SET @CourseCredits = 0;
    DECLARE GetCreditsSummary_Cursor CURSOR
    FOR
        SELECT	DISTINCT
                SE.StuEnrollId
               ,T.TermId
               ,T.TermDescrip
               ,T.StartDate
               ,R.ReqId
               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
               ,RES.Score AS FinalScore
               ,RES.GrdSysDetailId AS FinalGrade
               ,NULL
               ,R.Credits AS CreditsAttempted
               ,NULL AS ClsSectionId
               ,GSD.Grade
               ,GSD.IsPass
               ,GSD.IsCreditsAttempted
               ,GSD.IsCreditsEarned
               ,SE.PrgVerId
               ,GSD.IsInGPA
               ,R.FinAidCredits AS FinAidCredits
        FROM    arStuEnrollments SE
        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
        INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId 
						--INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId and RES.TermId=CS.TermId 
        INNER JOIN arTerm T ON RES.TermId = T.TermId
        INNER JOIN arReqs R ON RES.ReqId = R.ReqId
						--LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId=GBR.ClsSectionId
						--LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
						--LEFT JOIN arGrdComponentTypes GCT on GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId 
        LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
        WHERE   SE.StuEnrollId = @pStuEnrollId
        ORDER BY T.StartDate
               ,T.TermDescrip
               ,R.ReqId; 
    OPEN GetCreditsSummary_Cursor;
    SET @PrevStuEnrollId = NULL;
    SET @PrevTermId = NULL;
    SET @PrevReqId = NULL;
    SET @RowCount = 0;
    FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@TermStartDate,@reqid,@CourseCodeDescrip,@FinalScore,@FinalGrade,
        @sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,@FinAidCredits; --,@GrdBkResultId
    WHILE @@FETCH_STATUS = 0
        BEGIN
            SET @CourseCredits = @CreditsAttempted; 
            SET @RowCount = @RowCount + 1;
            SET @IsGradeBookNotSatisified = (
                                              SELECT    COUNT(*) AS UnsatisfiedWorkUnits
                                              FROM      (
                                                          SELECT    D.*
                                                                   ,CASE WHEN D.MinimumScore > D.Score THEN ( D.MinimumScore - D.Score )
                                                                         ELSE 0
                                                                    END AS Remaining
                                                                   ,CASE WHEN ( D.MinimumScore > D.Score )
                                                                              AND ( D.MustPass = 1 ) THEN 0
                                                                         WHEN D.Score IS NULL
                                                                              AND ( D.Required = 1 ) THEN 0
                                                                         ELSE 1
                                                                    END AS IsWorkUnitSatisfied
                                                          FROM      (
                                                                      SELECT    T.TermId
                                                                               ,T.TermDescrip
                                                                               ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                                               ,R.ReqId
                                                                               ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                                               ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504 ) THEN GBWD.Number
                                                                                       ELSE (
                                                                                              SELECT    MIN(MinVal)
                                                                                              FROM      arGradeScaleDetails GSD
                                                                                                       ,arGradeSystemDetails GSS
                                                                                              WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                        AND GSS.IsPass = 1
                                                                                            )
                                                                                  END ) AS MinimumScore
                                                                               ,GBR.Score AS Score
                                                                               ,GBWD.Weight AS Weight
                                                                               ,RES.Score AS FinalScore
                                                                               ,RES.GrdSysDetailId AS FinalGrade
                                                                               ,GBWD.Required
                                                                               ,GBWD.MustPass
                                                                               ,GBWD.GrdPolicyId
                                                                               ,( CASE GCT.SysComponentTypeId
                                                                                    WHEN 544 THEN (
                                                                                                    SELECT  SUM(HoursAttended)
                                                                                                    FROM    arExternshipAttendance
                                                                                                    WHERE   StuEnrollId = SE.StuEnrollId
                                                                                                  )
                                                                                    ELSE GBR.Score
                                                                                  END ) AS GradeBookResult
                                                                               ,GCT.SysComponentTypeId
                                                                               ,SE.StuEnrollId
                                                                               ,GBR.GrdBkResultId
                                                                               ,R.Credits AS CreditsAttempted
                                                                               ,CS.ClsSectionId
                                                                               ,GSD.Grade
                                                                               ,GSD.IsPass
                                                                               ,GSD.IsCreditsAttempted
                                                                               ,GSD.IsCreditsEarned
                                                                      FROM      arStuEnrollments SE
                                                                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                                      INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                                                                      INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                                                                                       AND RES.TermId = CS.TermId
                                                                      INNER JOIN arTerm T ON CS.TermId = T.TermId
                                                                      INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                                                      LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                                                                                      AND GBR.StuEnrollId = SE.StuEnrollId
                                                                      LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                      LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                      LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                                                      WHERE     SE.StuEnrollId = @StuEnrollId
                                                                                AND T.TermId = @TermId
                                                                                AND R.ReqId = @reqid
                                                                    ) D
                                                        ) E
                                              WHERE     IsWorkUnitSatisfied = 0
                                            ); 
														
			
	--Check if IsCreditsAttempted is set to True
            IF (
                 @IsCreditsAttempted IS NULL
                 OR @IsCreditsAttempted = 0
               )
                BEGIN
                    SET @CreditsAttempted = 0;
                END;
            IF (
                 @IsCreditsEarned IS NULL
                 OR @IsCreditsEarned = 0
               )
                BEGIN
                    SET @CreditsEarned = 0;
                END;		

            IF ( @IsGradeBookNotSatisified >= 1 )
                BEGIN
                    SET @CreditsEarned = 0;
                    SET @Completed = 0;
                END;
            ELSE
                BEGIN
                    SET @GrdBkResultId = (
                                           SELECT TOP 1
                                                    GrdBkResultId
                                           FROM     arGrdBkResults
                                           WHERE    StuEnrollId = @StuEnrollId
                                                    AND ClsSectionId = @ClsSectionId
                                         ); 
                    IF @GrdBkResultId IS NOT NULL
                        BEGIN
                            IF ( @IsCreditsEarned = 1 )
                                BEGIN
                                    SET @CreditsEarned = @CreditsAttempted;
                                    SET @FinAidCreditsEarned = @FinAidCredits;
                                END; 
                            SET @Completed = 1;										
                        END;
                    IF (
                         @GrdBkResultId IS NULL
                         AND @Grade IS NOT NULL
                       )
                        BEGIN
                            IF ( @IsCreditsEarned = 1 )
                                BEGIN
                                    SET @CreditsEarned = @CreditsAttempted;
                                    SET @FinAidCreditsEarned = @FinAidCredits;
                                END; 
                            SET @Completed = 1;	
                        END;
                END;
            IF (
                 @FinalScore IS NOT NULL
                 AND @Grade IS NOT NULL
               )
                BEGIN
                    IF ( @IsCreditsEarned = 1 )
                        BEGIN
                            SET @CreditsEarned = @CreditsAttempted;
                            SET @FinAidCreditsEarned = @FinAidCredits;
                        END; 
                    SET @Completed = 1;
				
                END;

				-- If course is not part of the program version definition do not add credits earned and credits attempted
				-- set the credits earned and attempted to zero
            DECLARE @coursepartofdefinition INT;
            SET @coursepartofdefinition = 0;
				
				-- Commented by Balaji on 1/16/2016 as it conflicts with equivalent courses
				--AMC Miguel Carabello, course:Swedish & Medical Management
				--	set @coursepartofdefinition = (select COUNT(*) as RowCountOfProgramDefinition from 
				--									(
				--										select * from arProgVerDef where PrgVerId=@PrgVerId and ReqId=@ReqId
				--											union
				--										select * from arProgVerDef where PrgVerId=@PrgVerId and ReqId in
				--											(select GrpId from arReqGrpDef where ReqId=@ReqId)
				--									) dt
				--								)
				--if (@coursepartofdefinition = 0)
				--	begin
				--		set @CreditsEarned = 0
				--		set @CreditsAttempted = 0
				--		set @FinAidCreditsEarned = 0
				--	end
			
               
				-- Check the grade scale associated with the class section and figure out of the final score was a passing score
            DECLARE @coursepassrowcount INT;
            SET @coursepassrowcount = 0;
            IF ( @FinalScore IS NOT NULL )
					
					-- If the student scores 56 and the score is a passing score then we consider this course as completed
                BEGIN
                    SET @coursepassrowcount = (
                                                SELECT  COUNT(t2.MinVal) AS IsCourseCompleted
                                                FROM    arClassSections t1
                                                INNER JOIN arGradeScaleDetails t2 ON t1.GrdScaleId = t2.GrdScaleId
                                                INNER JOIN arGradeSystemDetails t3 ON t2.GrdSysDetailId = t3.GrdSysDetailId
                                                WHERE   t1.ClsSectionId = @ClsSectionId
                                                        AND t3.IsPass = 1
                                                        AND @FinalScore >= t2.MinVal
                                              );
                    IF @coursepassrowcount >= 1
                        BEGIN
                            SET @Completed = 1;
                        END;
                    ELSE
                        BEGIN
                            SET @Completed = 0;
                        END;
                END;
				
				-- If Student Scored a Failing Grade (IsPass set to 0 in Grade System)
				-- then mark this course as Incomplete
            IF ( @FinalGrade IS NOT NULL )
                BEGIN
                    IF ( @IsPass = 0 )
                        BEGIN
                            SET @Completed = 0;
                            IF ( @IsCreditsEarned = 0 )
                                BEGIN
                                    SET @CreditsEarned = 0; 
                                    SET @FinAidCreditsEarned = 0;
                                END; 
                            IF ( @IsCreditsAttempted = 0 )
                                BEGIN
                                    SET @CreditsAttempted = 0;
                                END;
                        END;
                END;
				
            SET @CurrentScore = (
                                  SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                 ELSE NULL
                                            END AS CurrentScore
                                  FROM      (
                                              SELECT    InstrGrdBkWgtDetailId
                                                       ,Code
                                                       ,Descrip
                                                       ,Weight AS GradeBookWeight
                                                       ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                             ELSE 0
                                                        END AS ActualWeight
                                              FROM      (
                                                          SELECT    C.InstrGrdBkWgtDetailId
                                                                   ,D.Code
                                                                   ,D.Descrip
                                                                   ,ISNULL(C.Weight,0) AS Weight
                                                                   ,C.Number AS MinNumber
                                                                   ,C.GrdPolicyId
                                                                   ,C.Parameter AS Param
                                                                   ,X.GrdScaleId
                                                                   ,SUM(GR.Score) AS Score
                                                                   ,COUNT(D.Descrip) AS NumberOfComponents
                                                          FROM      (
                                                                      SELECT DISTINCT TOP 1
                                                                                A.InstrGrdBkWgtId
                                                                               ,A.EffectiveDate
                                                                               ,B.GrdScaleId
                                                                      FROM      arGrdBkWeights A
                                                                               ,arClassSections B
                                                                      WHERE     A.ReqId = B.ReqId
                                                                                AND A.EffectiveDate <= B.StartDate
                                                                                AND B.ClsSectionId = @ClsSectionId
                                                                      ORDER BY  A.EffectiveDate DESC
                                                                    ) X
                                                                   ,arGrdBkWgtDetails C
                                                                   ,arGrdComponentTypes D
                                                                   ,arGrdBkResults GR
                                                          WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                    AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                    AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                    AND D.SysComponentTypeId NOT IN ( 500,503 )
                                                                    AND GR.StuEnrollId = @StuEnrollId
                                                                    AND GR.ClsSectionId = @ClsSectionId
                                                                    AND GR.Score IS NOT NULL
                                                          GROUP BY  C.InstrGrdBkWgtDetailId
                                                                   ,D.Code
                                                                   ,D.Descrip
                                                                   ,C.Weight
                                                                   ,C.Number
                                                                   ,C.GrdPolicyId
                                                                   ,C.Parameter
                                                                   ,X.GrdScaleId
                                                        ) S
                                              GROUP BY  InstrGrdBkWgtDetailId
                                                       ,Code
                                                       ,Descrip
                                                       ,Weight
                                                       ,NumberOfComponents
                                            ) FinalTblToComputeCurrentScore
                                );
            IF ( @CurrentScore IS NULL )
                BEGIN
				-- instructor grade books
                    SET @CurrentScore = (
                                          SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                         ELSE NULL
                                                    END AS CurrentScore
                                          FROM      (
                                                      SELECT    InstrGrdBkWgtDetailId
                                                               ,Code
                                                               ,Descrip
                                                               ,Weight AS GradeBookWeight
                                                               ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                     ELSE 0
                                                                END AS ActualWeight
                                                      FROM      (
                                                                  SELECT    C.InstrGrdBkWgtDetailId
                                                                           ,D.Code
                                                                           ,D.Descrip
                                                                           ,ISNULL(C.Weight,0) AS Weight
                                                                           ,C.Number AS MinNumber
                                                                           ,C.GrdPolicyId
                                                                           ,C.Parameter AS Param
                                                                           ,X.GrdScaleId
                                                                           ,SUM(GR.Score) AS Score
                                                                           ,COUNT(D.Descrip) AS NumberOfComponents
                                                                  FROM      (
                                                                              --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
															--FROM          arGrdBkWeights A,arClassSections B        
															--WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
															--ORDER BY      A.EffectiveDate DESC
                                                                              SELECT DISTINCT TOP 1
                                                                                        t1.InstrGrdBkWgtId
                                                                                       ,t1.GrdScaleId
                                                                              FROM      arClassSections t1
                                                                                       ,arGrdBkWeights t2
                                                                              WHERE     t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                                        AND t1.ClsSectionId = @ClsSectionId
                                                                            ) X
                                                                           ,arGrdBkWgtDetails C
                                                                           ,arGrdComponentTypes D
                                                                           ,arGrdBkResults GR
                                                                  WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                            AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                            AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                            AND
													-- D.SysComponentTypeID not in (500,503) and 
                                                                            GR.StuEnrollId = @StuEnrollId
                                                                            AND GR.ClsSectionId = @ClsSectionId
                                                                            AND GR.Score IS NOT NULL
                                                                  GROUP BY  C.InstrGrdBkWgtDetailId
                                                                           ,D.Code
                                                                           ,D.Descrip
                                                                           ,C.Weight
                                                                           ,C.Number
                                                                           ,C.GrdPolicyId
                                                                           ,C.Parameter
                                                                           ,X.GrdScaleId
                                                                ) S
                                                      GROUP BY  InstrGrdBkWgtDetailId
                                                               ,Code
                                                               ,Descrip
                                                               ,Weight
                                                               ,NumberOfComponents
                                                    ) FinalTblToComputeCurrentScore
                                        );	
			
                END;

            IF ( @CurrentScore IS NOT NULL )
                BEGIN
                    SET @CurrentGrade = (
                                          SELECT    t2.Grade
                                          FROM      arGradeScaleDetails t1
                                                   ,arGradeSystemDetails t2
                                          WHERE     t1.GrdSysDetailId = t2.GrdSysDetailId
                                                    AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                           FROM     arClassSections
                                                                           WHERE    ClsSectionId = @ClsSectionId )
                                                    AND @CurrentScore >= t1.MinVal
                                                    AND @CurrentScore <= t1.MaxVal
                                        );
						
                END;	
            ELSE
                BEGIN
                    SET @CurrentGrade = NULL;
                END;
			
            IF (
                 @CurrentScore IS NULL
                 AND @CurrentGrade IS NULL
                 AND @FinalScore IS NULL
                 AND @FinalGrade IS NULL
               )
                BEGIN
                    SET @Completed = 0;
                    SET @CreditsAttempted = 0;
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
			
            IF (
                 @FinalScore IS NOT NULL
                 OR @FinalGrade IS NOT NULL
               )
                BEGIN

                    SET @FinalGradeDesc = (
                                            SELECT  Grade
                                            FROM    arGradeSystemDetails
                                            WHERE   GrdSysDetailId = @FinalGrade
                                          );
					

				
                    IF ( @FinalGradeDesc IS NULL )
                        BEGIN
                            SET @FinalGradeDesc = (
                                                    SELECT  t2.Grade
                                                    FROM    arGradeScaleDetails t1
                                                           ,arGradeSystemDetails t2
                                                    WHERE   t1.GrdSysDetailId = t2.GrdSysDetailId
                                                            AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                                   FROM     arClassSections
                                                                                   WHERE    ClsSectionId = @ClsSectionId )
                                                            AND @FinalScore >= t1.MinVal
                                                            AND @FinalScore <= t1.MaxVal
                                                  );
                        END;
                    SET @FinalGPA = (
                                      SELECT    GPA
                                      FROM      arGradeSystemDetails
                                      WHERE     GrdSysDetailId = @FinalGrade
                                    );
                    IF @FinalGPA IS NULL
                        BEGIN
                            SET @FinalGPA = (
                                              SELECT    t2.GPA
                                              FROM      arGradeScaleDetails t1
                                                       ,arGradeSystemDetails t2
                                              WHERE     t1.GrdSysDetailId = t2.GrdSysDetailId
                                                        AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                               FROM     arClassSections
                                                                               WHERE    ClsSectionId = @ClsSectionId )
                                                        AND @FinalScore >= t1.MinVal
                                                        AND @FinalScore <= t1.MaxVal
                                            );
                        END;
                END;
            ELSE
                BEGIN
                    SET @FinalGradeDesc = NULL;
                    SET @FinalGPA = NULL;
                END;

		--set @IsInGPA = (SELECT t2.IsInGPA FROM arGradeScaleDetails t1,arGradeSystemDetails t2
		--										WHERE  t1.GrdSysDetailId=t2.GrdSysDetailId and 
		--										t1.GrdScaleId In (Select GrdScaleId from arClassSections where ClsSectionId =@ClsSectionId) 
		--										and t2.Grade=@FinalGradeDesc)
												
            SET @isGradeEligibleForCreditsEarned = (
                                                     SELECT t2.IsCreditsEarned
                                                     FROM   arGradeScaleDetails t1
                                                           ,arGradeSystemDetails t2
                                                     WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                            AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                                   FROM     arClassSections
                                                                                   WHERE    ClsSectionId = @ClsSectionId )
                                                            AND t2.Grade = @FinalGradeDesc
                                                   ); 
												
            SET @isGradeEligibleForCreditsAttempted = (
                                                        SELECT  t2.IsCreditsAttempted
                                                        FROM    arGradeScaleDetails t1
                                                               ,arGradeSystemDetails t2
                                                        WHERE   t1.GrdSysDetailId = t2.GrdSysDetailId
                                                                AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                                       FROM     arClassSections
                                                                                       WHERE    ClsSectionId = @ClsSectionId )
                                                                AND t2.Grade = @FinalGradeDesc
                                                      ); 
												
            IF ( @isGradeEligibleForCreditsEarned IS NULL )
                BEGIN
					--Print   'Credits Earned is NULL'
                    SET @isGradeEligibleForCreditsEarned = (
                                                             SELECT TOP 1
                                                                    t2.IsCreditsEarned
                                                             FROM   arGradeSystemDetails t2
                                                             WHERE  t2.Grade = @FinalGradeDesc
                                                           );
                END;
												
            IF ( @isGradeEligibleForCreditsAttempted IS NULL )
                BEGIN
					--Print   'Credits Attempted is NULL'
                    SET @isGradeEligibleForCreditsAttempted = (
                                                                SELECT TOP 1
                                                                        t2.IsCreditsAttempted
                                                                FROM    arGradeSystemDetails t2
                                                                WHERE   t2.Grade = @FinalGradeDesc
                                                              );
                END;			
				
            IF @isGradeEligibleForCreditsEarned = 0
                BEGIN
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
            IF @isGradeEligibleForCreditsAttempted = 0
                BEGIN
                    SET @CreditsAttempted = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
			
            IF ( @IsPass = 0 )
                BEGIN
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;
                END;									
			
			--For Letter Grade Schools if the score is null but final grade was posted then the 
			--Final grade will be the current grade
            IF @CurrentGrade IS NULL
                AND @FinalGradeDesc IS NOT NULL
                BEGIN
                    SET @CurrentGrade = @FinalGradeDesc;
                END;

--			if Trim(@PrevTermId) = Trim(@TermId)
--			begin
--				set @Sum_Product_WeightedAverage_Credits_GPA = @Sum_Product_WeightedAverage_Credits_GPA + (@Product_WeightedAverage_Credits_GPA)
--				set @Sum
--			end


			--Check if course has lab work or lab hours
--			set @boolCourseHasLabWorkOrLabHours = (select distinct Count(GC.Descrip) from arGrdBkWeights GBW,arGrdComponentTypes GC, arGrdBkWgtDetails GD  where 
--													GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId And GC.GrdComponentTypeId = GD.GrdComponentTypeId 
--													and     GBW.ReqId = @ReqId and GC.SysComponentTypeID in (500,503))
			
            IF (
                 @sysComponentTypeId = 503
                 OR @sysComponentTypeId = 500
               ) -- Lab work or Lab Hours
                BEGIN
				-- This course has lab work and lab hours
                    IF ( @Completed = 0 )
                        BEGIN
                            SET @CreditsPerService = (
                                                       SELECT TOP 1
                                                                GD.CreditsPerService
                                                       FROM     arGrdBkWeights GBW
                                                               ,arGrdComponentTypes GC
                                                               ,arGrdBkWgtDetails GD
                                                       WHERE    GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
                                                                AND GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                                                AND GBW.ReqId = @reqid
                                                                AND GC.SysComponentTypeId IN ( 500,503 )
                                                     );
                            SET @NumberOfServicesAttempted = (
                                                               SELECT TOP 1
                                                                        GBR.Score AS NumberOfServicesAttempted
                                                               FROM     arStuEnrollments SE
                                                               INNER JOIN arGrdBkResults GBR ON SE.StuEnrollId = GBR.StuEnrollId
                                                                                                AND GBR.ClsSectionId = @ClsSectionId
                                                             );

                            SET @CreditsEarned = ISNULL(@CreditsPerService,0) * ISNULL(@NumberOfServicesAttempted,0);
                        END;
                END;

            DECLARE @rowAlreadyInserted INT; 
            SET @rowAlreadyInserted = 0;
			
			-- Get the final Gpa only when IsCreditsAttempted is set to 1 and IsInGPA is set to 1
            IF @IsInGPA = 1
                BEGIN
                    IF ( @IsCreditsAttempted = 0 )
                        BEGIN
                            SET @FinalGPA = NULL; 
                        END;
                END;
            ELSE
                BEGIN
                    SET @FinalGPA = NULL; 
                END;
			
            IF @FinalScore IS NOT NULL
                AND @CurrentScore IS NULL
                BEGIN
                    SET @CurrentScore = @FinalScore; 
                END;
			
			-- Rally case DE 738 KeyBoarding Courses
            SET @IsWeighted = (
                                SELECT  COUNT(*) AS WeightsCount
                                FROM    (
                                          SELECT    T.TermId
                                                   ,T.TermDescrip
                                                   ,'(' + R.Code + ' ) ' + R.Descrip AS CourseCodeDescrip
                                                   ,R.ReqId
                                                   ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                   ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,544 ) THEN GBWD.Number
                                                           ELSE (
                                                                  SELECT    MIN(MinVal)
                                                                  FROM      arGradeScaleDetails GSD
                                                                           ,arGradeSystemDetails GSS
                                                                  WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                            AND GSS.IsPass = 1
                                                                )
                                                      END ) AS MinimumScore
                                                   ,GBR.Score AS Score
                                                   ,GBWD.Weight AS Weight
                                                   ,RES.Score AS FinalScore
                                                   ,RES.GrdSysDetailId AS FinalGrade
                                                   ,GBWD.Required
                                                   ,GBWD.MustPass
                                                   ,GBWD.GrdPolicyId
                                                   ,( CASE GCT.SysComponentTypeId
                                                        WHEN 544 THEN (
                                                                        SELECT  SUM(HoursAttended)
                                                                        FROM    arExternshipAttendance
                                                                        WHERE   StuEnrollId = SE.StuEnrollId
                                                                      )
                                                        ELSE GBR.Score
                                                      END ) AS GradeBookResult
                                                   ,GCT.SysComponentTypeId
                                                   ,SE.StuEnrollId
                                                   ,GBR.GrdBkResultId
                                                   ,R.Credits AS CreditsAttempted
                                                   ,CS.ClsSectionId
                                                   ,GSD.Grade
                                                   ,GSD.IsPass
                                                   ,GSD.IsCreditsAttempted
                                                   ,GSD.IsCreditsEarned
                                          FROM      arStuEnrollments SE
                                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                          INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId 
																		--inner join Inserted t4 on RES.TransferId = t4.TransferId
                                          INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                                                           AND RES.TermId = CS.TermId
                                          INNER JOIN arTerm T ON CS.TermId = T.TermId
                                          INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                          LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                          LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                          LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                          LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                          WHERE     SE.StuEnrollId = @StuEnrollId
                                                    AND T.TermId = @TermId
                                                    AND R.ReqId = @reqid
                                        ) D
                                WHERE   Weight >= 1
                              );
															
			/************************************************* Changes for Build 2816 *********************/
			-- Rally case DE 738 KeyBoarding Courses
            DECLARE @GradesFormat VARCHAR(50);
            SET @GradesFormat = (
                                  SELECT    Value
                                  FROM      syConfigAppSetValues
                                  WHERE     SettingId = 47
                                ); -- 47 refers to grades format
				-- This condition is met only for numeric grade schools
            IF (
                 @IsGradeBookNotSatisified = 0
                 AND @IsWeighted = 0
                 AND @FinalScore IS NULL
                 AND @FinalGradeDesc IS NULL
                 AND LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
               )
                BEGIN
                    SET @Completed = 1;	
                    SET @CreditsAttempted = (
                                              SELECT    Credits
                                              FROM      arReqs
                                              WHERE     ReqId = @reqid
                                            );
                    SET @FinAidCredits = (
                                           SELECT   FinAidCredits
                                           FROM     arReqs
                                           WHERE    ReqId = @reqid
                                         );
                    SET @CreditsAttempted = @CreditsAttempted; 
                    SET @CreditsEarned = @CreditsAttempted; 
                    SET @FinAidCreditsEarned = @FinAidCredits;							
                END;
			
			-- DE748 Name: ROSS: Completed field should also check for the Must Pass property of the work unit. 
            IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                AND @IsGradeBookNotSatisified >= 1
                BEGIN
                    SET @Completed = 0;
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
			
			--DE738 Name: ROSS: Progress Report not taking care of courses that are not weighted. 

			-- Print @TermDescrip
			-- Print @CourseCodeDescrip
			-- Print @Completed
--			-- Print LOWER(LTRIM(RTRIM(@GradesFormat)))
--			-- Print @FinalScore
--			-- Print @FinalGradedesc
--			-- Print '@IsWeighted='
--			-- Print @IsWeighted
			
            IF (
                 LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                 AND @Completed = 1
                 AND @FinalScore IS NULL
                 AND @FinalGradeDesc IS NULL
               )
                BEGIN
                    SET @CreditsAttempted = @CreditsAttempted; 
                    SET @CreditsEarned = @CreditsAttempted; 
                    SET @FinAidCreditsEarned = @FinAidCredits;
                END;

					-- In Ross Example : Externship, the student may not have completed the course but once he attempts a work unit
					-- we need to take the credits as attempted
            IF (
                 LOWER(LTRIM(RTRIM(@GradesFormat))) <> 'letter'
                 AND @Completed = 0
                 AND @FinalScore IS NULL
                 AND @FinalGradeDesc IS NULL
               )
                BEGIN
                    DECLARE @rowcount4 INT;
                    SET @rowcount4 = (
                                       SELECT   COUNT(*)
                                       FROM     arGrdBkResults
                                       WHERE    StuEnrollId = @StuEnrollId
                                                AND ClsSectionId = @ClsSectionId
                                                AND Score IS NOT NULL
                                     );
                    IF @rowcount4 >= 1
                        BEGIN
										-- Print 'Gets in to if'
                            SET @CreditsAttempted = (
                                                      SELECT    Credits
                                                      FROM      arReqs
                                                      WHERE     ReqId = @reqid
                                                    );
                            SET @CreditsEarned = 0;
                            SET @FinAidCreditsEarned = 0;
										-- Print @CreditsAttempted
                        END;
                    ELSE
                        BEGIN
                            SET @rowcount4 = (
                                               SELECT   COUNT(*)
                                               FROM     arGrdBkConversionResults
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND ReqId = @reqid
                                                        AND TermId = @TermId
                                                        AND Score IS NOT NULL
                                             );
                            IF @rowcount4 >= 1
                                BEGIN
                                    SET @CreditsAttempted = (
                                                              SELECT    Credits
                                                              FROM      arReqs
                                                              WHERE     ReqId = @reqid
                                                            );
                                    SET @CreditsEarned = 0;
                                    SET @FinAidCreditsEarned = 0;
                                END;
                        END;

							--For Externship Attendance						
                    IF @sysComponentTypeId = 544
                        BEGIN
                            SET @rowcount4 = (
                                               SELECT   COUNT(*)
                                               FROM     arExternshipAttendance
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND HoursAttended >= 1
                                             );
                            IF @rowcount4 >= 1
                                BEGIN
                                    SET @CreditsAttempted = (
                                                              SELECT    Credits
                                                              FROM      arReqs
                                                              WHERE     ReqId = @reqid
                                                            );
                                    SET @CreditsEarned = 0;
                                    SET @FinAidCreditsEarned = 0;
                                END;

                        END;
				
                END;
			/************************************************* Changes for Build 2816 *********************/
					
				-- If the final grade is not null the final grade will over ride current grade 
            IF @FinalGradeDesc IS NOT NULL
                BEGIN
                    SET @CurrentGrade = @FinalGradeDesc; 
				
                END;

            IF @IsCreditsEarned = 1
                BEGIN
                    SET @CreditsEarned = @CourseCredits;
                END;
				
            IF @IsCreditsAttempted = 0
                BEGIN
                    SET @CreditsAttempted = 0;
                END;

            DELETE  FROM syCreditSummary
            WHERE   StuEnrollId = @StuEnrollId
                    AND TermId = @TermId
                    AND ReqId = @reqid
                    --AND ClsSectionId = @ClsSectionId
                    AND (
                          ClsSectionId = @ClsSectionId
                          OR ClsSectionId IS NULL
                        );

            INSERT  INTO syCreditSummary
            VALUES  ( @StuEnrollId,@TermId,@TermDescrip,@reqid,@CourseCodeDescrip,@ClsSectionId,@CreditsEarned,@CreditsAttempted,@CurrentScore,@CurrentGrade,
                      @FinalScore,@FinalGradeDesc,@Completed,@FinalGPA,@Product_WeightedAverage_Credits_GPA,@Count_WeightedAverage_Credits,
                      @Product_SimpleAverage_Credits_GPA,@Count_SimpleAverage_Credits,'sa',GETDATE(),@ComputedSimpleGPA,@ComputedWeightedGPA,@CourseCredits,NULL,
                      NULL,@FinAidCreditsEarned,NULL,NULL,@TermStartDate );

            DECLARE @wCourseCredits DECIMAL(18,2)
               ,@wWeighted_GPA_Credits DECIMAL(18,2)
               ,@sCourseCredits DECIMAL(18,2)
               ,@sSimple_GPA_Credits DECIMAL(18,2);
			-- For weighted average
            SET @ComputedWeightedGPA = 0;
            SET @ComputedSimpleGPA = 0;
            SET @wCourseCredits = (
                                    SELECT  SUM(coursecredits)
                                    FROM    syCreditSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND TermId = @TermId
                                            AND FinalGPA IS NOT NULL
                                  );
            SET @wWeighted_GPA_Credits = (
                                           SELECT   SUM(coursecredits * FinalGPA)
                                           FROM     syCreditSummary
                                           WHERE    StuEnrollId = @StuEnrollId
                                                    AND TermId = @TermId
                                                    AND FinalGPA IS NOT NULL
                                         );
			
            IF @wCourseCredits >= 1
                BEGIN
                    SET @ComputedWeightedGPA = @wWeighted_GPA_Credits / @wCourseCredits;
                END;
			
			
			
			--For Simple Average
            SET @sCourseCredits = (
                                    SELECT  COUNT(*)
                                    FROM    syCreditSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND TermId = @TermId
                                            AND FinalGPA IS NOT NULL
                                  );
            SET @sSimple_GPA_Credits = (
                                         SELECT SUM(FinalGPA)
                                         FROM   syCreditSummary
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND TermId = @TermId
                                                AND FinalGPA IS NOT NULL
                                       );
            IF @sCourseCredits >= 1
                BEGIN
                    SET @ComputedSimpleGPA = @sSimple_GPA_Credits / @sCourseCredits;
                END; 
			
					
			--CumulativeGPA
            DECLARE @cumCourseCredits DECIMAL(18,2)
               ,@cumWeighted_GPA_Credits DECIMAL(18,2)
               ,@cumWeightedGPA DECIMAL(18,2);
            SET @cumWeightedGPA = 0;
            SET @cumCourseCredits = (
                                      SELECT    SUM(coursecredits)
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = @StuEnrollId
                                                AND FinalGPA IS NOT NULL
                                    );
            SET @cumWeighted_GPA_Credits = (
                                             SELECT SUM(coursecredits * FinalGPA)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                           );
			
            IF @cumCourseCredits >= 1
                BEGIN
                    SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                END; 
			
			--CumulativeSimpleGPA
            DECLARE @cumSimpleCourseCredits DECIMAL(18,2)
               ,@cumSimple_GPA_Credits DECIMAL(18,2)
               ,@cumSimpleGPA DECIMAL(18,2);
            SET @cumSimpleGPA = 0;
            SET @cumSimpleCourseCredits = (
                                            SELECT  COUNT(coursecredits)
                                            FROM    syCreditSummary
                                            WHERE   StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                          );
            SET @cumSimple_GPA_Credits = (
                                           SELECT   SUM(FinalGPA)
                                           FROM     syCreditSummary
                                           WHERE    StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                         );
			
            IF @cumSimpleCourseCredits >= 1
                BEGIN
                    SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                END; 
			
				--Average calculation
            DECLARE @termAverageSum DECIMAL(18,2)
               ,@CumAverage DECIMAL(18,2)
               ,@cumAverageSum DECIMAL(18,2)
               ,@cumAveragecount INT;
			
				-- Term Average
            SET @TermAverageCount = (
                                      SELECT    COUNT(*)
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = @StuEnrollId 
									--and Completed=1 
                                                AND TermId = @TermId
                                                AND FinalScore IS NOT NULL
                                    );
            SET @termAverageSum = (
                                    SELECT  SUM(FinalScore)
                                    FROM    syCreditSummary
                                    WHERE   StuEnrollId = @StuEnrollId 
									--and Completed=1 
                                            AND TermId = @TermId
                                            AND FinalScore IS NOT NULL
                                  );
            SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
            SET @cumAveragecount = (
                                     SELECT COUNT(*)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId 
									--and Completed=1 
                                            AND FinalScore IS NOT NULL
                                   );
            SET @cumAverageSum = (
                                   SELECT   SUM(FinalScore)
                                   FROM     syCreditSummary
                                   WHERE    StuEnrollId = @StuEnrollId
                                            AND 
									--Completed=1 and 
                                            FinalScore IS NOT NULL
                                 );
            SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
				 		
            UPDATE  syCreditSummary
            SET     TermGPA_Simple = @ComputedSimpleGPA
                   ,TermGPA_Weighted = @ComputedWeightedGPA
                   ,Average = @TermAverage
            WHERE   StuEnrollId = @StuEnrollId
                    AND TermId = @TermId; 
			
			--Update Cumulative GPA
            UPDATE  syCreditSummary
            SET     CumulativeGPA = @cumWeightedGPA
                   ,CumulativeGPA_Simple = @cumSimpleGPA
                   ,CumAverage = @CumAverage
            WHERE   StuEnrollId = @StuEnrollId;
		
			
														
            SET @PrevStuEnrollId = @StuEnrollId; 
            SET @PrevTermId = @TermId; 
            SET @PrevReqId = @reqid;

            FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@TermStartDate,@reqid,@CourseCodeDescrip,@FinalScore,@FinalGrade,
                @sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,@FinAidCredits;
        END;
    CLOSE GetCreditsSummary_Cursor;
    DEALLOCATE GetCreditsSummary_Cursor;
-- =========================================================================================================
-- END  --  Usp_UpdateTransferCredits_SyCreditsSummary 
-- =========================================================================================================

GO

-- ********************************************************************************************************
-- Usp_TR_GetAcademicType
-- ********************************************************************************************************
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[Usp_TR_GetAcademicType]')
                    AND type IN ( N'P',N'PC' ) )
    BEGIN
        DROP PROCEDURE Usp_TR_GetAcademicType;
    END;
	
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO

-- ********************************************************************************************************
-- Usp_TR_GetAcademicType
-- ********************************************************************************************************
CREATE PROCEDURE dbo.Usp_TR_GetAcademicType
    @StuEnrollIdList VARCHAR(MAX) = NULL
   ,@AcademicType NVARCHAR(50) OUTPUT
AS
    DECLARE @ReturnMessage AS NVARCHAR(1000);
    --DECLARE @Message01 AS NVARCHAR(1000)
    DECLARE @Message02 AS NVARCHAR(1000);
    --SET     @Message01 = 'Academic type for selected Enrollments must be same.'
    SET @Message02 = 'Program Version does not have credits or hours defined.';

    CREATE TABLE #Temp1
        (
         StuEnrollId UNIQUEIDENTIFIER
        ,AcademicType NVARCHAR(50)
        );
 
 
    BEGIN                                                     
        INSERT  INTO #Temp1
                SELECT  ASE.StuEnrollId AS StuEnrollId
                       ,( CASE WHEN (
                                      APV.Credits > 0.0
                                      AND APV.Hours > 0
                                    )
                                    OR ( APV.IsContinuingEd = 1 ) THEN 'Credits-ClockHours'
                               WHEN APV.Credits > 0.0 THEN 'Credits'
                               WHEN APV.Hours > 0.0 THEN 'ClockHours'
                               ELSE ''
                          END ) AS AcademicType
                FROM    arPrgVersions AS APV
                INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                WHERE   ASE.StuEnrollId IN ( SELECT Val
                                             FROM   MultipleValuesForReportParameters(@StuEnrollIdList,',',1) );

        SELECT TOP 1
                @AcademicType = T1.AcademicType
        FROM    #Temp1 AS T1
        ORDER BY T1.AcademicType;

        IF ( @AcademicType = '' )
            BEGIN
                SET @AcademicType = @Message02; --   'Program Version does not have credits or hours defined.'
            END;
        --ELSE
        --    BEGIN
        --        IF EXISTS ( SELECT 1
        --                    FROM #Temp1 AS T1 
        --                    WHERE T1.AcademicType <> @AcademicType
        --                  )
        --            BEGIN
        --                SET @AcademicType  = @Message01 --   'Academic type for selected Enrollments must be same.'
        --            END
        --END
    END;
-- ********************************************************************************************************
-- END  --  Usp_TR_GetAcademicType
-- ********************************************************************************************************   
GO



-- ********************************************************************************************************
-- END  --  Consolidated Script Version 3.7SP5
-- ********************************************************************************************************