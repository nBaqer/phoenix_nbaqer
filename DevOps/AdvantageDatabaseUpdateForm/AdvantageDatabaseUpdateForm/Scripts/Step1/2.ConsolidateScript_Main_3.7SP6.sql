-- ********************************************************************************************************
-- Consolidated Script Version 3.7SP6
-- Schema Changes Zone
-- Please deploy your schema changes in this area
-- (Format the code with TSQl Format before insert here please)
-- ********************************************************************************************************

-- To solve JTECH issue
 -- Update the MenuName  and DisplyName in syMenuItems table  
 -- USE  'Fall - Completions - All Institutions - Detail & Summary Reports' instead  'Completions - All Institution (Fall) Reports'
IF EXISTS ( SELECT  1
            FROM    syMenuItems AS SMI
            WHERE   SMI.MenuName = 'Completions - All Institution (Fall) Reports' )
    BEGIN
        UPDATE  SMI
        SET     SMI.MenuName = 'Fall - Completions - All Institutions - Detail & Summary Reports'
               ,SMI.DisplayName = 'Fall - Completions - All Institutions - Detail & Summary Reports'
               ,SMI.ModDate = GETDATE()
               ,SMI.ModUser = 'Support'
        FROM    syMenuItems AS SMI
        WHERE   SMI.MenuName = 'Completions - All Institution (Fall) Reports';
    END;

-- ===============================================================================================
-- US7834 IPEDS - Disability Service
-- JTorres 2016-09 -06
-- ===============================================================================================
GO
SET ANSI_NULLS ON; 
GO
SET QUOTED_IDENTIFIER ON; 
GO
SET ANSI_PADDING ON; 
GO

BEGIN
-- ===============================================================================================
-- Adding Resource ID 836 "IPEDS - Disability Service"  
-- ===============================================================================================

-- Declare variables
    BEGIN
    -- general fields
        DECLARE @Error AS INTEGER;  
        DECLARE @DisplayName AS NVARCHAR(250);  
        DECLARE @ModDate AS DATETIME; 
        DECLARE @ModUser AS NVARCHAR(50);  
    -- Resource Id
        DECLARE @ResourceId AS INT; 
        DECLARE @Resource AS NVARCHAR(200); 
        DECLARE @ResourceTypeID AS INTEGER; 
        DECLARE @ResourceURL AS NVARCHAR(100);
        DECLARE @ChildTypeId AS INTEGER; 
        DECLARE @AllowSchReqFlds AS BIT; 
        DECLARE @MRUTypeId AS SMALLINT; 
        DECLARE @UsedIn AS INT; 
        DECLARE @TblFldsId AS INT; 
    -- MenuItem      
        DECLARE @PPPMenuName AS NVARCHAR(250);
        DECLARE @PPMenuName AS NVARCHAR(250); 
        DECLARE @PMenuName AS NVARCHAR(250); 
        DECLARE @mMenuName AS NVARCHAR(250); 
        DECLARE @MenuParentId AS INT; 
        DECLARE @MenuItemId AS INT; 
        DECLARE @MenuName AS NVARCHAR(250); 
        DECLARE @Url AS NVARCHAR(250); 
        DECLARE @MenuItemTypeId AS SMALLINT; 
        DECLARE @DisplayOrder AS INT; 
        DECLARE @IsPopup AS BIT; 
        DECLARE @IsActive AS BIT; 
    --syNavigationNodes
        DECLARE @HierarchyId AS UNIQUEIDENTIFIER; 
        DECLARE @ParentId AS UNIQUEIDENTIFIER; 
        DECLARE @HierarchyIndex AS SMALLINT; 
        DECLARE @IsShipped AS SMALLINT; 
    -- syRlsResLvls
        DECLARE @AccessLevel AS INT; 
        DECLARE @RoleId AS UNIQUEIDENTIFIER; 
        DECLARE @Code_RRL AS NVARCHAR(12); 
    -- ParamSet
        DECLARE @SetId AS BIGINT;
        DECLARE @SetName AS NVARCHAR(50);
        DECLARE @SetDisplayName AS NVARCHAR(50);
        DECLARE @SetDescription AS NVARCHAR(1000);
        DECLARE @SetType AS NVARCHAR(50);
    -- ParamSection
        DECLARE @SectionId AS BIGINT;
        DECLARE @SectionName AS NVARCHAR(50);
        DECLARE @SectionCaption AS NVARCHAR(100);
        DECLARE @SectionSeq AS INTEGER;
        DECLARE @SectionDescription AS NVARCHAR(500);
        DECLARE @SectionType AS INTEGER;
    -- ParamItem
        DECLARE @ItemId AS BIGINT;
        DECLARE @ItemCaption AS NVARCHAR(50);
        DECLARE @ControllerClass AS NVARCHAR(50);
        DECLARE @ValueProp AS INTEGER;
        DECLARE @ReturnValueName AS NVARCHAR(50);
        DECLARE @IsItemOverriden AS BIT;
    -- ParamItemProp
        DECLARE @ItemPropertyId AS BIGINT;
       --DECLARE @ItemId              AS BIGINT
        DECLARE @ChildControl AS NVARCHAR(100);
        DECLARE @PropName AS NVARCHAR(100);
        DECLARE @Value AS NVARCHAR(MAX);
        DECLARE @ValueType AS NVARCHAR(25);

    -- ParamDetail
        DECLARE @DetailId AS BIGINT;
       --DECLARE @SectionId           AS BIGINT
       --DECLARE @ItemId              AS BIGINT
        DECLARE @CaptionOverride AS NVARCHAR(50);
        DECLARE @ItemSeq AS INTEGER;
   -- ParamDetailProp
        DECLARE @DetailPropertyId AS BIGINT;
       --DECLARE @DetailId            AS BIGINT
        DECLARE @DetailChildControl AS NVARCHAR(100);
        DECLARE @DetailPropName AS NVARCHAR(100);
        DECLARE @DetailValue AS NVARCHAR(MAX);
        DECLARE @DetailValueType AS NVARCHAR(25);

    -- syReports
        DECLARE @ReportId AS UNIQUEIDENTIFIER;
        DECLARE @ReportName AS NVARCHAR(50);
        DECLARE @ReportDescription AS NVARCHAR(500);
        DECLARE @RDLName AS NVARCHAR(200);
        DECLARE @AssebleyFilePath AS NVARCHAR(200);
        DECLARE @ReportClass AS NVARCHAR(200);
        DECLARE @CreationMethod AS NVARCHAR(200);
        DECLARE @WebServiceUrl AS NVARCHAR(200);
        DECLARE @RecordLimit AS BIGINT;
        DECLARE @AllowedExportTypes AS INTEGER;
        DECLARE @ReportTabLayout AS INTEGER;
        DECLARE @ShowPerformanceMsg AS BIT;
        DECLARE @ShowFilterMode AS BIT;
    -- syReportTabs
        DECLARE @PageViewId AS NVARCHAR(200);
        DECLARE @UserControlName AS NVARCHAR(200);
        DECLARE @ControlerClass AS NVARCHAR(200);
        DECLARE @TabDisplayName AS NVARCHAR(200);
        DECLARE @ParameterSetLookUp AS NVARCHAR(200);
        DECLARE @TabName AS NVARCHAR(200);
    END;
-- Set initials Values 
    BEGIN
    -- general fields
        SET @Error = 0; 
        SET @DisplayName = 'Disability Service';  
        SET @ModDate = GETDATE(); 
        SET @ModUser = 'support'; 
    -- Resource Id
        SET @ResourceId = 836;          -- Hard coded  --  standard for all DB's (Clients)
        SET @Resource = @DisplayName; 
        SET @ResourceTypeID = 5;        -- Maintenance --  SELECT * FROM syResourceTypes AS SRT
        SET @ResourceURL = '~/sy/ParamReport.aspx';
        SET @ChildTypeId = 5; 
        SET @AllowSchReqFlds = 0; 
        SET @MRUTypeId = 4;             -- Page        --  SELECT * FROM syMRUTypes AS SMT
        SET @UsedIn = 207;              -- page resource where is used (this is Report page)
        SET @TblFldsId = NULL;
    -- MenuItem 
        SET @PPPMenuName = 'Reports';
        SET @PPMenuName = 'IPEDS Reports'; 
        SET @PMenuName = 'Fall - Completions - All Institutions - Detail & Summary Reports'; 
        SET @MenuName = @DisplayName; 
        SET @Url = '/sy/ParamReport.aspx'; 
        SET @MenuItemTypeId = 4;        -- Page  --  SELECT * FROM syMenuItemType AS SMIT
        SET @DisplayOrder = 350;        -- After "Relationship Types"  SELECT * FROM syMenuItems AS SMI WHERE SMI.DisplayName = 'General Options'  ORDER BY SMI.ParentId, SMI.DisplayOrder
        SET @IsPopup = 0;               -- it is not popup menu
        SET @IsActive = 1;              --  Yes, it is active    
    -- syNavigationNodes
        SET @HierarchyIndex = 8;        -- Hierarchy Index???? 
        SET @IsShipped = 1; 
    -- syRlsResLvls
        SET @AccessLevel = 15;          -- sysadmins role with full access (15)
        SET @Code_RRL = 'SYSADMINS';    -- SELECT SR.* FROM syRoles AS SR 
        SET @RoleId = (
                        SELECT TOP 1
                                SR.RoleId
                        FROM    syRoles AS SR
                        WHERE   SR.Code = @Code_RRL
                      ); 
    -- syReports
        SET @ReportName = 'DisabilityServiceReport';
        SET @ReportDescription = 'Fall - Completions - ' + @DisplayName + ' Report';
        SET @RDLName = 'IPEDS/FALL/Completions/Fall04_Completions_' + @ReportName;
        SET @AssebleyFilePath = '~/Bin/Reporting.dll';
        SET @ReportClass = 'FAME.Advantage.Reporting.Logic.IPEDSStudentsMissingData';
        SET @CreationMethod = 'BuildReport';
        SET @WebServiceUrl = 'futurefield';
        SET @RecordLimit = '400';
        SET @AllowedExportTypes = 0;
        SET @ReportTabLayout = 1;
        SET @ShowPerformanceMsg = 0;
        SET @ShowFilterMode = 0;
    -- syReportTabs
        SET @PageViewId = 'RadRptPage_Options';
        SET @UserControlName = 'ParamPanelBarSetCustomOptions';
        SET @SetId = 0;  -- should be read from ParamSet table
        SET @ControlerClass = 'ParamSetPanelBarControl.ascx';
        SET @TabDisplayName = 'Custom Options Set for ' + @ReportName;
        SET @ParameterSetLookUp = @ReportName + 'CustomOptionsSet';
        SET @TabName = 'Options';
    -- ParamSet
    --   SET @SetId              
        SET @SetName = @ReportName + 'CustomOptionsSet';
        SET @SetDisplayName = @ReportName + 'Set';
        SET @SetDescription = 'Custom Options for IPEDS ' + @DisplayName + ' Report';
        SET @SetType = 'Custom';
    -- ParamSection
      --SET @SectionId         
        SET @SectionName = 'ParamsFor' + @ReportName; 
        SET @SectionCaption = @DisplayName + ' Report Parameters';
        SET @SectionSeq = 1;
        SET @SectionDescription = 'Choose ' + @DisplayName + ' Report Parameters';
        SET @SectionType = 0;
   -- ParamItem
      --SET @ItemId             
        SET @ItemCaption = @ReportName + 'Options';
        SET @ControllerClass = 'ParamIPEDS.ascx';
        SET @ValueProp = 0;
        SET @ReturnValueName = @ReportName + 'Options';
        SET @IsItemOverriden = NULL;
   -- ParamItemProp
      --SET @ItemPropertyId   
      --SET @ItemId            
        SET @ChildControl = NULL;
        SET @PropName = 'IPEDSControlType';
        SET @Value = 21;                --  Enum ControlType.[DisabilityService] = 21 'Disability Service
        SET @ValueType = 'Enum';
   -- ParamDetail
       --SET @DetailId           
       --SET @SectionId       
       --SET @ItemId              = 45
        SET @CaptionOverride = 'Report Options';
        SET @ItemSeq = 1;
   -- ParamDetailProp
      -- set inside If because several properties
   --
   -- 
    END;

    BEGIN TRANSACTION AddNewFields; 
    BEGIN TRY
        -- ===============================================================================================
        -- Adding Resource ID 836 "IPEDS - Disability Service"  
        -- ===============================================================================================
        IF ( @Error = 0 )  -- Insert ResourceID
            BEGIN
                IF NOT EXISTS ( SELECT  SR.ResourceID
                                FROM    syResources AS SR
                                WHERE   SR.ResourceID = @ResourceId )
                    BEGIN
                        INSERT  INTO dbo.syResources
                                (
                                 ResourceID
                                ,Resource
                                ,ResourceTypeID
                                ,ResourceURL
                                ,SummListId
                                ,ChildTypeId
                                ,ModDate
                                ,ModUser
                                ,AllowSchlReqFlds
                                ,MRUTypeId
                                ,UsedIn
                                ,TblFldsId
                                ,DisplayName
                                )
                        VALUES  (
                                 @ResourceId
                                ,@Resource
                                ,@ResourceTypeID
                                ,@ResourceURL
                                ,NULL
                                ,@ChildTypeId
                                ,@ModDate
                                ,@ModUser
                                ,@AllowSchReqFlds
                                ,@MRUTypeId
                                ,@UsedIn
                                ,@TblFldsId
                                ,@DisplayName
                                ); 
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SET @Error = 0;   
--                                PRINT 'Successful syResources ' + CONVERT(NVARCHAR(10), @ResourceId)
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed syResources ' + CONVERT(NVARCHAR(10), @ResourceId)
                            END; 
                    END;  
                ELSE
                    BEGIN
                        SET @Error = 0; 
--                        PRINT 'Exists  syResources ' + CONVERT(NVARCHAR(10), @ResourceId)
                    END; 
            END;  -- Adding Resource ID 836 "IPEDS - Disability Service"  -- Ends Here --
        -- ===============================================================================================
        --  Adding MenuItem  "Disability Service"  
        -- ===============================================================================================        
        IF ( @Error = 0 )  -- insert Menu item
            BEGIN
                IF NOT EXISTS ( SELECT TOP 1
                                        SMI.MenuItemId
                                FROM    syMenuItems AS SMI
                                INNER JOIN syMenuItems AS P ON P.MenuItemId = SMI.ParentId
                                INNER JOIN syMenuItems AS PP ON PP.MenuItemId = P.ParentId
                                INNER JOIN syMenuItems AS PPP ON PPP.MenuItemId = PP.ParentId
                                WHERE   SMI.MenuName = @MenuName
                                        AND P.MenuName = @PMenuName
                                        AND PP.MenuName = @PPMenuName
                                        AND PPP.MenuName = @PPPMenuName )
                    BEGIN
                        SELECT  @MenuParentId = P.MenuItemId
                        FROM    syMenuItems AS P
                        INNER JOIN syMenuItems AS PP ON PP.MenuItemId = P.ParentId
                        INNER JOIN syMenuItems AS PPP ON PPP.MenuItemId = PP.ParentId
                        WHERE   P.MenuName = @PMenuName
                                AND PP.MenuName = @PPMenuName
                                AND PPP.MenuName = @PPPMenuName;

                        IF @MenuParentId IS NOT NULL
                            BEGIN
                                INSERT  INTO dbo.syMenuItems
                                        (
                                         MenuName
                                        ,DisplayName
                                        ,Url
                                        ,MenuItemTypeId
                                        ,ParentId
                                        ,DisplayOrder
                                        ,IsPopup
                                        ,ModDate
                                        ,ModUser
                                        ,IsActive
                                        ,ResourceId
                                        ,HierarchyId
                                        ,ModuleCode
                                        ,MRUType
                                        ,HideStatusBar
                                        )
                                VALUES  (
                                         @MenuName
                                        ,@DisplayName
                                        ,@Url
                                        ,@MenuItemTypeId
                                        ,@MenuParentId
                                        ,@DisplayOrder
                                        ,@IsPopup
                                        ,@ModDate
                                        ,@ModUser
                                        ,1
                                        ,@ResourceId
                                        ,NULL
                                        ,NULL
                                        ,NULL
                                        ,NULL
                                        ); 
                                IF ( @@ERROR = 0 )
                                    BEGIN
                                        SELECT  @MenuItemId = SMI.MenuItemId
                                        FROM    syMenuItems AS SMI
                                        WHERE   SMI.MenuName = @MenuName
                                                AND SMI.Url = @Url
                                                AND SMI.ParentId = @MenuParentId; 
                                        SET @Error = 0;   
--                                        PRINT 'Successful syMenuItems ' + @MenuName
                                    END; 
                                ELSE
                                    BEGIN
                                        SET @Error = 1; 
--                                        PRINT 'Failed syMenuItems'  + @MenuName
                                    END; 
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 0; 
                                SELECT  @MenuParentId = P.MenuItemId
                                FROM    syMenuItems AS P
                                INNER JOIN syMenuItems AS PP ON PP.MenuItemId = P.ParentId
                                INNER JOIN syMenuItems AS PPP ON PPP.MenuItemId = PP.ParentId
                                WHERE   P.MenuName = @PMenuName
                                        AND PP.MenuName = @PPMenuName
                                        AND PPP.MenuName = @PPPMenuName;
--                              PRINT ' Exists syMenuItems'  + @MenuName
                            END;                      
                 
                    END;  -- Adding MenuItem     -- Ends Here --
                ELSE
                    BEGIN
                        SET @Error = 0; 
                        SELECT  @MenuParentId = P.MenuItemId
                        FROM    syMenuItems AS P
                        INNER JOIN syMenuItems AS PP ON PP.MenuItemId = P.ParentId
                        INNER JOIN syMenuItems AS PPP ON PPP.MenuItemId = PP.ParentId
                        WHERE   P.MenuName = @PMenuName
                                AND PP.MenuName = @PPMenuName
                                AND PPP.MenuName = @PPPMenuName;
--                              PRINT ' Exists syMenuItems'  + @MenuName
                    END;
            END;
        -- ==============================================================================================
        --  Fill syNavigationNodes     --Add navigation node
        -- ===============================================================================================        
        IF ( @Error = 0 )  -- syNavigationNodes
            BEGIN
                --Add navigation node
                SELECT  @ParentId = SNN.ParentId
                FROM    syNavigationNodes AS SNN
                WHERE   SNN.ResourceId = (
                                           SELECT   MIN(SMI.ResourceId)
                                           FROM     syMenuItems AS SMI
                                           WHERE    SMI.ParentId = @MenuParentId
                                         ); 
                IF NOT EXISTS ( SELECT  1
                                FROM    syNavigationNodes AS SNN
                                WHERE   SNN.ResourceId = @ResourceId
                                        AND SNN.ParentId = @ParentId )
                    BEGIN
                        INSERT  INTO syNavigationNodes
                                (
                                 HierarchyIndex
                                ,ResourceId
                                ,ParentId
                                ,ModDate
                                ,ModUser
                                ,IsPopupWindow
                                ,IsShipped
                                )
                        VALUES  (
                                 @HierarchyIndex
                                ,@ResourceId
                                ,@ParentId
                                ,@ModDate
                                ,@ModUser
                                ,@IsPopup
                                ,@IsShipped
                                ); 
                    
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SELECT  @HierarchyId = SNN.HierarchyId
                                FROM    syNavigationNodes AS SNN
                                WHERE   SNN.ResourceId = @ResourceId
                                        AND SNN.ParentId = @ParentId; 
                                SET @Error = 0;   
--                                PRINT 'Successful syNavigationNodes ' + CONVERT(NVARCHAR(40),  @HierarchyId)
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed syNavigationNodes ' + CONVERT(NVARCHAR(40),  @HierarchyId)
                            END; 
                    END; 
                ELSE
                    BEGIN
                        SET @Error = 0; 
--                        PRINT ' Exists syNavigationNodes ' + CONVERT(NVARCHAR(40),  @HierarchyId)
                    END; 
            END;   --Add navigation node  -- Ends Here --
        -- ===============================================================================================
        --  Update MenuItem   "Disability Service"    (with HierarchyId)
        -- ===============================================================================================
        IF ( @Error = 0 )  -- Update Table with Field (Id)
            BEGIN
                IF EXISTS ( SELECT  1
                            FROM    syMenuItems AS SMI
                            WHERE   SMI.MenuItemId = @MenuItemId )
                    BEGIN
                        UPDATE  SMI
                        SET     SMI.HierarchyId = @HierarchyId
                        FROM    syMenuItems AS SMI
                        WHERE   SMI.MenuItemId = @MenuItemId; 
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SET @Error = 0;    
--                                PRINT 'Successful Updates  syMenuItems with HierarchyId ' + CONVERT(NVARCHAR(10),  @MenuItemId) + ' '  + CONVERT(NVARCHAR(40),  @HierarchyId)
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed Updates  syMenuItems with HierarchyId ' + CONVERT(NVARCHAR(10),  @MenuItemId) + ' '  + CONVERT(NVARCHAR(40),  @HierarchyId)
                            END; 
                    END; 
                ELSE
                    BEGIN
                        SET @Error = 0; 
--                        PRINT ' Exists  syMenuItems with HierarchyId ' + CONVERT(NVARCHAR(10),  @MenuItemId) + ' '  + CONVERT(NVARCHAR(40),  @HierarchyId)
                    END; 
            END;   -- Update MenuItem    (with HierarchyId) -- Ends Here -- 
        -- ===============================================================================================
        --  Fill syRlsResLvls    --Link new resource to sysadmins role with full access (15)
        -- ===============================================================================================        
        IF ( @Error = 0 )  -- syRlsResLvls  Link new resource to sysadmins role 
            BEGIN
                ----Link new resource to sysadmins role with full access (15)
                IF NOT EXISTS ( SELECT  1
                                FROM    syRlsResLvls
                                WHERE   ResourceID = @ResourceId
                                        AND AccessLevel = @AccessLevel )
                    BEGIN
                        INSERT  INTO syRlsResLvls
                                (
                                 RRLId
                                ,RoleId
                                ,ResourceID
                                ,AccessLevel
                                ,ModDate
                                ,ModUser
                                )
                        VALUES  (
                                 NEWID()
                                ,@RoleId
                                ,@ResourceId
                                ,@AccessLevel
                                ,@ModDate
                                ,@ModUser
                                ); 	
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SET @Error = 0;   
--                                PRINT 'Successful syRlsResLvls ResourceId ' + CONVERT(NVARCHAR(10), @ResourceId) + ' AccessLevel ' + CONVERT(NVARCHAR(10), @AccessLevel) 
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed syRlsResLvls ResourceId ' + CONVERT(NVARCHAR(10), @ResourceId) + ' AccessLevel ' + CONVERT(NVARCHAR(10), @AccessLevel) 
                            END; 
                    END; 
                ELSE
                    BEGIN
                        SET @Error = 0; 
--                        PRINT ' Exists syRlsResLvls ResourceId ' + CONVERT(NVARCHAR(10), @ResourceId) + ' AccessLevel ' + CONVERT(NVARCHAR(10), @AccessLevel) 
                    END; 
            END;    -- syRlsResLvls  Link new resource to sysadmins role -- Ends Here --
        -- ===============================================================================================
        --  Fill Tab2 -- Options   -- Param SET 
        -- =============================================================================================== 
        IF ( @Error = 0 )  -- Update Param SET 
            BEGIN
                -- Tab2  Options
                IF NOT EXISTS ( SELECT  1
                                FROM    ParamSet AS PS
                                WHERE   PS.SetName = @SetName )
                    BEGIN
                        INSERT  INTO ParamSet
                                (
                                 SetName
                                ,SetDisplayName
                                ,SetDescription
                                ,SetType
				                )
                        VALUES  (
                                 @SetName           -- SetName - nvarchar(50)
                                ,@SetDisplayName    -- SetDisplayName - nvarchar(50)
                                ,@SetDescription    -- SetDescription - nvarchar(1000)
                                ,@SetType           -- SetType - nvarchar(50)
				                );
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SET @Error = 0;   
                                SELECT  @SetId = PS.SetId
                                FROM    ParamSet AS PS
                                WHERE   PS.SetName = @SetName;
--                                PRINT 'Successful ParamSET for ' + @SetName 
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed  ParamSET for ' + @SetName  
                            END; 
                    END; 
                ELSE
                    BEGIN
                        SELECT  @SetId = PS.SetId
                        FROM    ParamSet AS PS
                        WHERE   PS.SetName = @SetName;
                        SET @Error = 0;
--                        PRINT ' Exists  ParamSET for ' + @SetName  
                    END; 
            END;    -- Tab2 Options   -- Param SET  -- Ends Here --
        -- ===============================================================================================
        --  Tab2 Custom Option  Section Custom Option  -- ParamSection 
        -- =============================================================================================== 
        IF ( @Error = 0 )  -- Tab2 Custom Option  Section Custom Option 
            BEGIN
                -- Tab2 Custom Option  Section Custom Option
                IF NOT EXISTS ( SELECT  1
                                FROM    ParamSection AS PS1
                                INNER JOIN ParamSet AS PS ON PS.SetId = PS1.SetId
                                WHERE   PS1.SectionName = @SectionName
                                        AND PS.SetName = @SetName
                                        AND PS.SetId = @SetId )
                    BEGIN
                        INSERT  INTO ParamSection
                                (
                                 SectionName
                                ,SectionCaption
                                ,SetId
                                ,SectionSeq
                                ,SectionDescription
                                ,SectionType
                                )
                        VALUES  (
                                 @SectionName           -- SectionName - nvarchar(50)
                                ,@SectionCaption        -- SectionCaption - nvarchar(100)
                                ,@SetId                 -- SetId - bigint
                                ,@SectionSeq            -- SectionSeq - int
                                ,@SectionDescription    -- SectionDescription - nvarchar(500)
                                ,@SectionType           -- SectionType - int
                                );
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SET @Error = 0;   
                                SELECT  @SectionId = PS1.SectionId
                                FROM    ParamSection AS PS1
                                INNER JOIN ParamSet AS PS ON PS.SetId = PS1.SetId
                                WHERE   PS1.SectionName = @SectionName
                                        AND PS.SetName = @SetName
                                        AND PS.SetId = @SetId;
--                                PRINT 'Successful ParamSET for ' + @SetName 
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed  ParamSET for ' + @SetName  
                            END; 
                    END; 
                ELSE
                    BEGIN
                        SET @Error = 0; 
                        SELECT  @SectionId = PS1.SectionId
                        FROM    ParamSection AS PS1
                        INNER JOIN ParamSet AS PS ON PS.SetId = PS1.SetId
                        WHERE   PS1.SectionName = @SectionName
                                AND PS.SetName = @SetName
                                AND PS.SetId = @SetId;
--                        PRINT ' Exists  ParamSET for ' + @SetName  
                    END; 
            END;  -- Tab2 Custom Option  Section Custom Option  -- Ends Here --
        -- ===============================================================================================
        --   ParamItem   -- ParamItem 
        -- =============================================================================================== 
        IF ( @Error = 0 )  -- Update ParamItem
            BEGIN
                -- Tab2  Options ParamItem
                IF NOT EXISTS ( SELECT  1
                                FROM    ParamItem AS PI
                                WHERE   PI.Caption = @ItemCaption )
                    BEGIN
                        INSERT  INTO ParamItem
                                (
                                 Caption
                                ,ControllerClass
                                ,valueprop
                                ,ReturnValueName
                                ,IsItemOverriden
                                )
                        VALUES  (
                                 @ItemCaption           -- Caption - nvarchar(50)
                                ,@ControllerClass       -- ControllerClass - nvarchar(50)
                                ,@ValueProp             -- valueprop - int
                                ,@ReturnValueName       -- ReturnValueName - nvarchar(50)
                                ,@IsItemOverriden       -- IsItemOverriden - bit
                                );
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SET @Error = 0; 
                                SELECT  @ItemId = PI.ItemId
                                FROM    ParamItem AS PI
                                WHERE   PI.Caption = @ItemCaption;  
--                                PRINT 'Successful ParamItem for ' + @ItemCaption 
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed  ParamItem for ' + @ItemCaptione  
                            END; 
                    END; 
                ELSE
                    BEGIN
                        SET @Error = 0; 
                        SELECT  @ItemId = PI.ItemId
                        FROM    ParamItem AS PI
                        WHERE   PI.Caption = @ItemCaption;  
--                        PRINT ' Exists  ParamItem for ' + @ItemCaptione  
                    END; 
            END;    -- Tab2 Options   -- Param Item -- Ends Here --
        -- ===============================================================================================
        --   ParamItemProp   -- ParamItemProp 
        -- =============================================================================================== 
        IF ( @Error = 0 )  -- Update ParamItemPro
            BEGIN
                -- Tab2  Options ParamItemPro
                IF NOT EXISTS ( SELECT  1
                                FROM    ParamItemProp AS PIP
                                INNER JOIN ParamItem AS PI ON PI.ItemId = PIP.itemid
                                WHERE   PI.Caption = @ItemCaption
                                        AND PI.ItemId = @ItemId )
                    BEGIN
                        INSERT  INTO ParamItemProp
                                (
                                 itemid
                                ,childcontrol
                                ,propname
                                ,value
                                ,valuetype
                                )
                        VALUES  (
                                 @ItemId                -- itemid - bigint
                                ,@ChildControl          -- childcontrol - nvarchar(100)
                                ,@PropName              -- propname - nvarchar(100)
                                ,@Value                 -- value - nvarchar(max)
                                ,@ValueType             -- valuetype - nvarchar(25)
                                );
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SET @Error = 0; 
                                SELECT  @ItemPropertyId = PIP.itempropertyid
                                FROM    ParamItemProp AS PIP
                                INNER JOIN ParamItem AS PI ON PI.ItemId = PIP.itemid
                                WHERE   PI.Caption = @ItemCaption
                                        AND PI.ItemId = @ItemId;  
--                                PRINT 'Successful ParamItemProp for ' + @PropName 
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed ParamItemProp for ' + @PropName 
                            END; 
                    END; 
                ELSE
                    BEGIN
                        SET @Error = 0; 
                        SELECT  @ItemPropertyId = PIP.itempropertyid
                        FROM    ParamItemProp AS PIP
                        INNER JOIN ParamItem AS PI ON PI.ItemId = PIP.itemid
                        WHERE   PI.Caption = @ItemCaption
                                AND PI.ItemId = @ItemId;  
--                        PRINT ' Exists ParamItemProp for ' + @PropName  
                    END; 
            END;    -- ParamItemPro   -- ParamItemPro -- Ends Here --
        -- ===============================================================================================
        --  Tab2 Custom Option  Section Custom Option  -- ParamDetail 
        -- =============================================================================================== 
        IF ( @Error = 0 )  -- Tab2 Custom Option  ParamDetail
            BEGIN
                -- Tab2 Custom Option 1 Section Custom Option --> Detail
                IF NOT EXISTS ( SELECT  1
                                FROM    ParamDetail AS PD
                                INNER JOIN ParamSection AS PS1 ON PS1.SectionId = PD.SectionId
                                INNER JOIN ParamSet AS PS ON PS.SetId = PS1.SetId
                                INNER JOIN ParamItem AS PI ON PI.ItemId = PD.ItemId
                                WHERE   PD.CaptionOverride = @CaptionOverride
                                        AND PS1.SectionName = @SectionName
                                        AND PS.SetName = @SetName
                                        AND PI.Caption = @ItemCaption )
                    BEGIN
                        INSERT  INTO ParamDetail
                                (
                                 SectionId
                                ,ItemId
                                ,CaptionOverride
                                ,ItemSeq
                                )
                        VALUES  (
                                 @SectionId            -- SectionId - bigint
                                ,@ItemId               -- ItemId - bigint
                                ,@CaptionOverride      -- CaptionOverride - nvarchar(50)
                                ,@ItemSeq              -- ItemSeq - int
                                );

                        IF ( @@ERROR = 0 )
                            BEGIN
                                SET @Error = 0;   
                                SELECT  @DetailId = PD.DetailId
                                FROM    ParamDetail AS PD
                                INNER JOIN ParamSection AS PS1 ON PS1.SectionId = PD.SectionId
                                INNER JOIN ParamSet AS PS ON PS.SetId = PS1.SetId
                                INNER JOIN ParamItem AS PI ON PI.ItemId = PD.ItemId
                                WHERE   PD.CaptionOverride = @CaptionOverride
                                        AND PS1.SectionName = @SectionName
                                        AND PS.SetName = @SetName
                                        AND PI.Caption = @ItemCaption;  
--                                PRINT 'Successful ParamSET for ' + @SetName 
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed  ParamSET for ' + @SetName  
                            END; 
                    END; 
                ELSE
                    BEGIN
                        SET @Error = 0; 
                        SELECT  @DetailId = PD.DetailId
                        FROM    ParamDetail AS PD
                        INNER JOIN ParamSection AS PS1 ON PS1.SectionId = PD.SectionId
                        INNER JOIN ParamSet AS PS ON PS.SetId = PS1.SetId
                        INNER JOIN ParamItem AS PI ON PI.ItemId = PD.ItemId
                        WHERE   PD.CaptionOverride = @CaptionOverride
                                AND PS1.SectionName = @SectionName
                                AND PS.SetName = @SetName
                                AND PI.Caption = @ItemCaption;  
--                        PRINT ' Exists  ParamSET for ' + @SetName  
                    END; 
            END;  -- Tab2 Custom Option 1 Section Custom Option --> Detail  -- Ends Here --
        -- ===============================================================================================
        --  Tab2 Custom Option  Section Custom Option  -- ParamDetailProp 
        -- =============================================================================================== 
        IF ( @Error = 0 )  -- Update ParamItemPro
            BEGIN
                -- Tab2  Options ParamItemPro
                --SET @DetailPropertyId   
                --SET @DetailId         
                SET @DetailChildControl = NULL;
                SET @DetailPropName = 'IPEDSControlType';
                SET @DetailValue = 21;    --  Enum ControlType.[DisabilityService] = 21 'Disability Service
                SET @DetailValueType = 'Enum';

                IF NOT EXISTS ( SELECT  1
                                FROM    ParamDetailProp AS PDP
                                INNER JOIN ParamDetail AS PD ON PD.DetailId = PDP.detailid
                                WHERE   PDP.propname = @DetailPropName
                                        AND PD.DetailId = @DetailId )
                    BEGIN
                        INSERT  INTO ParamDetailProp
                                (
                                 detailid
                                ,childcontrol
                                ,propname
                                ,value
                                ,valuetype
                                )
                        VALUES  (
                                 @DetailId              -- detailid - bigint
                                ,@DetailChildControl    -- childcontrol - nvarchar(100)
                                ,@DetailPropName        -- propname - nvarchar(100)
                                ,@DetailValue           -- value - nvarchar(max)
                                ,@DetailValueType       -- valuetype - nvarchar(25)
                                );
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SET @Error = 0; 
                                SELECT  @DetailPropertyId = PDP.detailpropertyid
                                FROM    ParamDetailProp AS PDP
                                INNER JOIN ParamDetail AS PD ON PD.DetailId = PDP.detailid
                                WHERE   PDP.propname = @DetailPropName
                                        AND PD.DetailId = @DetailId; 
--                                PRINT 'Successful ParamDetailProp for ' + @DetailPropName 
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed ParamDetailProp for ' + @DetailPropName 
                            END; 
                    END; 
                ELSE
                    BEGIN
                        SET @Error = 0; 
                        SELECT  @DetailPropertyId = PDP.detailpropertyid
                        FROM    ParamDetailProp AS PDP
                        INNER JOIN ParamDetail AS PD ON PD.DetailId = PDP.detailid
                        WHERE   PDP.propname = @DetailPropName
                                AND PD.DetailId = @DetailId; 
--                        PRINT ' Exists ParamDetailProp for ' + @DetailPropName 
                    END; 
            END;    -- ParamItemPro   -- ParamItemPro -- Ends Here --
        -- ===============================================================================================
        --  Tab2 Custom Option  Section Custom Option  -- ParamDetailProp 
        -- =============================================================================================== 
        IF ( @Error = 0 )  -- Update ParamItemPro
            BEGIN
                -- Tab2  Options ParamItemPro
                --SET @DetailPropertyId   
                --SET @DetailId         
                SET @DetailChildControl = NULL;
                SET @DetailPropName = 'SelectedDatePartProgram';
                SET @DetailValue = '10/31/';
                SET @DetailValueType = 'String';

                IF NOT EXISTS ( SELECT  1
                                FROM    ParamDetailProp AS PDP
                                INNER JOIN ParamDetail AS PD ON PD.DetailId = PDP.detailid
                                WHERE   PDP.propname = @DetailPropName
                                        AND PD.DetailId = @DetailId )
                    BEGIN
                        INSERT  INTO ParamDetailProp
                                (
                                 detailid
                                ,childcontrol
                                ,propname
                                ,value
                                ,valuetype
                                )
                        VALUES  (
                                 @DetailId              -- detailid - bigint
                                ,@DetailChildControl    -- childcontrol - nvarchar(100)
                                ,@DetailPropName        -- propname - nvarchar(100)
                                ,@DetailValue           -- value - nvarchar(max)
                                ,@DetailValueType       -- valuetype - nvarchar(25)
                                );
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SET @Error = 0; 
                                SELECT  @DetailPropertyId = PDP.detailpropertyid
                                FROM    ParamDetailProp AS PDP
                                INNER JOIN ParamDetail AS PD ON PD.DetailId = PDP.detailid
                                WHERE   PDP.propname = @DetailPropName
                                        AND PD.DetailId = @DetailId; 
--                                PRINT 'Successful ParamDetailProp for ' + @DetailPropName 
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed ParamDetailProp for ' + @DetailPropName 
                            END; 
                    END; 
                ELSE
                    BEGIN
                        SET @Error = 0; 
                        SELECT  @DetailPropertyId = PDP.detailpropertyid
                        FROM    ParamDetailProp AS PDP
                        INNER JOIN ParamDetail AS PD ON PD.DetailId = PDP.detailid
                        WHERE   PDP.propname = @DetailPropName
                                AND PD.DetailId = @DetailId; 
--                        PRINT ' Exists ParamDetailProp for ' + @DetailPropName 
                    END; 
            END;    -- ParamItemPro   -- ParamItemPro -- Ends Here --
        -- ===============================================================================================
        --  Fill syReports    --  Record for a new Report "Disability Service"
        -- =============================================================================================== 
        IF ( @Error = 0 )  -- syReports  new Report "Disability Service"
            BEGIN       
                IF NOT EXISTS ( SELECT  1
                                FROM    syReports AS SR
                                WHERE   SR.ReportName = @ReportName )
                    BEGIN
                        INSERT  INTO syReports
                                (
                                 ReportId
                                ,ResourceId
                                ,ReportName
                                ,ReportDescription
                                ,RDLName
                                ,AssemblyFilePath
                                ,ReportClass
                                ,CreationMethod
                                ,WebServiceUrl
                                ,RecordLimit
                                ,AllowedExportTypes
                                ,ReportTabLayout
                                ,ShowPerformanceMsg
                                ,ShowFilterMode
                                )
                        VALUES  (
                                 NEWID()                -- ReportId - uniqueidentifier
                                ,@ResourceId            -- ResourceId - int
                                ,@ReportName            -- ReportName - varchar(50)
                                ,@ReportDescription     -- ReportDescription - varchar(500)
                                ,@RDLName               -- RDLName - varchar(200)
                                ,@AssebleyFilePath      -- AssemblyFilePath - varchar(200)
                                ,@ReportClass           -- ReportClass - varchar(200)
                                ,@CreationMethod        -- CreationMethod - varchar(200)
                                ,@WebServiceUrl         -- WebServiceUrl - varchar(200)
                                ,@RecordLimit           -- RecordLimit - bigint
                                ,@AllowedExportTypes    -- AllowedExportTypes - int
                                ,@ReportTabLayout       -- ReportTabLayout - int
                                ,@ShowPerformanceMsg    -- ShowPerformanceMsg - bit
                                ,@ShowFilterMode        -- ShowFilterMode - bit
                                );
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SET @Error = 0;   
--                                PRINT 'Successful ReportName ' + @DisplayName
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed  ReportName ' + @DisplayName
                            END;
                    END;        
                ELSE
                    BEGIN
                        SET @Error = 0; 
--                        PRINT ' Exists ReportName ' + @DisplayName
                    END; 
                SELECT TOP 1
                        @ReportId = SR.ReportId
                FROM    syReports AS SR
                WHERE   SR.ReportName = @ReportName;
            END;    --  syReports  new Report "Disability Service" -- Ends Here --        
        -- ===============================================================================================
        --  Fill syReportTabs    --  Record for "Option" tab in Report "Disability Service"
        -- =============================================================================================== 
        IF ( @Error = 0 )  --  Record for "Option" tab in Report "Disability Service"
            BEGIN       
                IF NOT EXISTS ( SELECT  1
                                FROM    syReportTabs AS SRT
                                WHERE   SRT.ReportId = @ReportId
                                        AND SRT.PageViewId = @PageViewId )
                    BEGIN
                        INSERT  INTO dbo.syReportTabs
                                (
                                 ReportId
                                ,PageViewId
                                ,UserControlName
                                ,SetId
                                ,ControllerClass
                                ,DisplayName
                                ,ParameterSetLookUp
                                ,TabName
                                )
                        VALUES  (
                                 @ReportId              -- ReportId - uniqueidentifier
                                ,@PageViewId            -- PageViewId - nvarchar(200)
                                ,@UserControlName       -- UserControlName - nvarchar(200)
                                ,@SetId                 -- SetId - bigint
                                ,@ControlerClass        -- ControllerClass - nvarchar(200)
                                ,@TabDisplayName        -- DisplayName - nvarchar(200)
                                ,@ParameterSetLookUp    -- ParameterSetLookUp - nvarchar(200)
                                ,@TabName               -- TabName - nvarchar(200)
                                );
                        IF ( @@ERROR = 0 )
                            BEGIN
                                SET @Error = 0;   
--                                PRINT 'Successful ReportTab ' + @DisplayName + ' - ' + @PageViewId
                            END; 
                        ELSE
                            BEGIN
                                SET @Error = 1; 
--                                PRINT 'Failed  ReportTab ' + @DisplayName + ' - ' + @PageViewId
                            END;
                    END;        
                ELSE
                    BEGIN
                        SET @Error = 0;
--                        PRINT ' Exists ReportTab ' + @DisplayName + ' - ' + @PageViewId
                    END; 
            END;    --  syReportTabs  "Disability Service" Options tab-- Ends Here --        

 -- To Test
--        SET @Error = 1;
 
        IF ( @Error = 0 )
            BEGIN
                COMMIT TRANSACTION  AddNewFields; 
--                PRINT 'Successful COMMIT TRANSACTION'
            END; 
        ELSE
            BEGIN
                ROLLBACK TRANSACTION AddNewFields; 
--                PRINT 'Failed ROLLBACK TRANSACTION '     
            END; 
    END TRY

    BEGIN CATCH
        ROLLBACK TRANSACTION  AddNewFields;   
--        PRINT 'Failed ROLLBACK TRANSACTION  Catching error'    
        SELECT  ERROR_NUMBER() AS ErrorNumber
               ,ERROR_SEVERITY() AS ErrorSeverity
               ,ERROR_STATE() AS ErrorState
               ,ERROR_PROCEDURE() AS ErrorProcedure
               ,ERROR_LINE() AS ErrorLine
               ,ERROR_MESSAGE() AS ErrorMessage; 
    END CATCH; 
END; 




-- ===============================================================================================
-- END  --  US 7834 IPEDS - Disability Service 
-- ===============================================================================================
GO

-- =========================================================================================================
-- USP_IPEDS_FALL_DisabilityService
-- =========================================================================================================
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[USP_IPEDS_FALL_DisabilityService]')
                    AND type IN ( N'P',N'PC' ) )
    BEGIN
        DROP PROCEDURE USP_IPEDS_FALL_DisabilityService;
    END;
    
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- =========================================================================================================
-- USP_IPEDS_FALL_DisabilityService
-- =========================================================================================================
CREATE PROCEDURE USP_IPEDS_FALL_DisabilityService
--EXEC USP_IPEDS_FALL_DISABILITY '3F5E839A-589A-4B2A-B258-35A1A8B3B819',NULL,NULL,'10/31/2016',NULL,'SSN',NULL 
    @CampusId VARCHAR(50)
   ,@ProgIdList VARCHAR(MAX) = NULL                 -- Program Id List
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@ShowSSN BIT
   ,@OrderBy VARCHAR(100)                           -- 'StudentIdentifier' OR 'StudentName'      
AS
    BEGIN
		
		/* Get a list of students who are out of school as of report end date. These students will be excluded from the report */
        DECLARE @StudentsInOutofSchoolStatusAsOfReportEndDate TABLE
            (
             StuEnrollId UNIQUEIDENTIFIER
            );
        INSERT  INTO @StudentsInOutofSchoolStatusAsOfReportEndDate
                SELECT  ASE1.StuEnrollId
                FROM    arStuEnrollments AS ASE1
                INNER JOIN syStatusCodes AS SSC1 ON SSC1.StatusCodeId = ASE1.StatusCodeId  -- t2
                INNER JOIN arPrgVersions AS APV1 ON APV1.PrgVerId = ASE1.PrgVerId
                INNER JOIN arPrograms AS AP1 ON AP1.ProgId = APV1.ProgId
                WHERE   ASE1.StartDate <= @EndDate -- Student enrolled as of report end date
                        AND LTRIM(RTRIM(ASE1.CampusId)) = LTRIM(RTRIM(@CampusId))
                        AND (
                              @ProgIdList IS NULL
                              OR AP1.ProgId IN ( SELECT Val
                                                 FROM   MultipleValuesForReportParameters(@ProgIdList,',',1) )
                            )
                        AND SSC1.SysStatusId IN ( 12,14,19,8 ) -- Dropped out/Transferred/Graduated/No Start
												-- Date Determined or ExpGradDate or LDA falls before 08/31/2010
                        AND (
                              ASE1.DateDetermined < @StartDate
                              OR ASE1.ExpGradDate < @StartDate
                              OR ASE1.LDA < @StartDate
                            );



	
		/* List of students who will show up in report */
        SELECT  List.StudentIdentifier
               ,List.StudentName
               ,List.DisabledStudent
               ,List.IPEDSSequence
        FROM    (
                  SELECT  DISTINCT
                            CASE WHEN ( @ShowSSN = 1 ) THEN AST.SSN
                                 ELSE AST.StudentNumber
                            END AS StudentIdentifier
                           ,( LTRIM(RTRIM(ISNULL(AST.LastName,''))) + CASE WHEN ( AST.FirstName IS NOT NULL ) THEN ', ' + LTRIM(RTRIM(ISNULL(AST.FirstName,'')))
                                                                           ELSE CASE WHEN ( AST.MiddleName IS NOT NULL ) THEN ','
                                                                                     ELSE ''
                                                                                END
                                                                      END + CASE WHEN ( AST.MiddleName IS NOT NULL )
                                                                                 THEN ' ' + LTRIM(RTRIM(SUBSTRING(ISNULL(AST.MiddleName,''),1,1)))
                                                                                 ELSE ''
                                                                            END ) AS StudentName
                           ,CASE WHEN ( ASE.IsDisabled = 1 ) THEN 'X'
                                 ELSE ''
                            END AS DisabledStudent
                           ,ISNULL(AG.IPEDSSequence,1) AS IPEDSSequence
                  FROM      arStudent AS AST
                  LEFT JOIN adGenders AS AG ON AST.Gender = AG.GenderId
                  INNER JOIN arStuEnrollments AS ASE ON AST.StudentId = ASE.StudentId
                  LEFT JOIN adDegCertSeeking AS ADCS ON ADCS.DegCertSeekingId = ASE.degcertseekingid ---- For Degree Seeking --
                  INNER JOIN syStatusCodes AS SSC ON ASE.StatusCodeId = SSC.StatusCodeId
                  INNER JOIN sySysStatus AS SSS ON SSC.SysStatusId = SSS.SysStatusId
                  INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                  INNER JOIN arPrograms AS AP ON AP.ProgId = APV.ProgId
							--Areas Of Intrest --
                  INNER JOIN arPrgGrp AS APG ON APG.PrgGrpId = APV.PrgGrpId 
						    --Areas Of Intrest --
                  INNER JOIN arProgTypes AS APT ON APV.ProgTypId = APT.ProgTypId
                  LEFT JOIN arAttendTypes AS AAT ON ASE.attendtypeid = AAT.AttendTypeId
                  WHERE     ASE.CampusId = @CampusId
                            AND (
                                  @ProgIdList IS NULL
                                  OR AP.ProgId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@ProgIdList,',',1) )
                                )
                            AND APT.IPEDSValue = 58							 -- Undergraduate
                            AND (
                                  AAT.IPEDSValue = 61
                                  OR AAT.IPEDSValue = 62
                                ) -- Part Time or Full Time
                            AND (
                                  AG.IPEDSValue = 30
                                  OR AG.IPEDSValue = 31
                                )   -- Men or Women
                            AND ASE.IsFirstTimeInSchool = 1					 -- First-time in school
                            AND SSS.SysStatusId NOT IN ( 8 )				 -- Exclude No Start Students
                            AND ASE.StartDate <= @EndDate					 -- Student who is enrolled before Report End Date
                            AND (
                                  ISNULL(ADCS.IPEDSValue,0) = 11
                                  OR ISNULL(ADCS.IPEDSValue,0) = 10
                                ) -- Degree/Cert Seeking and Non-Degree Certificate Seeking students     
                            AND StuEnrollId NOT IN ( SELECT StuEnrollId
                                                     FROM   @StudentsInOutofSchoolStatusAsOfReportEndDate )
                ) AS List
        ORDER BY CASE WHEN @OrderBy = 'StudentIdentifier' THEN List.StudentIdentifier
                      WHEN @OrderBy = 'StudentName' THEN List.StudentName
                 END; 
    END;
-- =========================================================================================================
-- END  --  USP_IPEDS_FALL_DisabilityService
-- =========================================================================================================

GO

-- ===============================================================================================
-- END US7834 IPEDS - Disability Service
-- ===============================================================================================



-- =========================================================================================================
-- DE12938 Normal - Advantage - Unilatina - Error when trying to print a transcript - 1022271
-- JTorres 2016-09 - 12
-- =========================================================================================================

-- =========================================================================================================
-- Usp_TR_GetAcademicType
-- =========================================================================================================
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[Usp_TR_GetAcademicType]')
                    AND type IN ( N'P',N'PC' ) )
    BEGIN
        DROP PROCEDURE Usp_TR_GetAcademicType;
    END;
    
GO

SET ANSI_NULLS ON;
GO
SET QUOTED_IDENTIFIER ON;
GO
-- ********************************************************************************************************
-- Usp_TR_GetAcademicType
-- ********************************************************************************************************
CREATE PROCEDURE dbo.Usp_TR_GetAcademicType
    @StuEnrollIdList VARCHAR(MAX) = NULL
   ,@AcademicType NVARCHAR(50) OUTPUT
AS
    DECLARE @ReturnMessage AS NVARCHAR(1000);
    --DECLARE @Message01 AS NVARCHAR(1000)
    DECLARE @Message02 AS NVARCHAR(1000);
    --SET     @Message01 = 'Academic type for selected Enrollments must be same.'
    SET @Message02 = 'Program Version does not have credits, hours defined or is not Continue Education set up.';

    CREATE TABLE #Temp1
        (
         StuEnrollId UNIQUEIDENTIFIER
        ,AcademicType NVARCHAR(50)
        );
 
    BEGIN                                                     
        INSERT  INTO #Temp1
                SELECT  ASE.StuEnrollId
                       ,( CASE WHEN (
                                      APV.Credits > 0.0
                                      AND APV.Hours > 0
                                    )
                                    OR ( APV.IsContinuingEd = 1 ) THEN 'Credits-ClockHours'
                               WHEN APV.Credits > 0.0 THEN 'Credits'
                               WHEN APV.Hours > 0.0 THEN 'ClockHours'
                               ELSE ''
                          END )
                FROM    arPrgVersions AS APV
                INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                WHERE   ASE.StuEnrollId IN ( SELECT Val
                                             FROM   MultipleValuesForReportParameters(@StuEnrollIdList,',',1) );

        SELECT TOP 1
                @AcademicType = T1.AcademicType
        FROM    #Temp1 AS T1
        ORDER BY T1.AcademicType;

        IF ( @AcademicType = '' )
            BEGIN
                SET @AcademicType = @Message02; --   'Program Version does not have credits or hours defined.'
            END;
        --ELSE
        --    BEGIN
        --        IF EXISTS ( SELECT 1
        --                    FROM #Temp1 AS T1 
        --                    WHERE T1.AcademicType <> @AcademicType
        --                  )
        --            BEGIN
        --                SET @AcademicType  = @Message01 --   'Academic type for selected Enrollments must be same.'
        --            END
        --END
    END;
-- ********************************************************************************************************
-- END  --  Usp_TR_GetAcademicType
-- ********************************************************************************************************   
GO

-- =========================================================================================================
-- Usp_TR_Sub3_StudentInfo
-- =========================================================================================================
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[Usp_TR_Sub3_StudentInfo]')
                    AND type IN ( N'P',N'PC' ) )
    BEGIN
        DROP PROCEDURE Usp_TR_Sub3_StudentInfo;
    END;
GO

SET ANSI_NULLS ON;
SET QUOTED_IDENTIFIER ON;
GO
-- =========================================================================================================
-- Usp_TR_Sub3_StudentInfo 
-- =========================================================================================================
CREATE PROCEDURE dbo.Usp_TR_Sub3_StudentInfo
    @StuEnrollIdList NVARCHAR(MAX) = NULL
   ,@ShowMultipleEnrollments BIT = 0
   ,@StatusList NVARCHAR(MAX) = NULL
   ,@OrderBy NVARCHAR(1000) = NULL
AS
    BEGIN
        CREATE TABLE #Temp1
            (
             StudentId NVARCHAR(50)
            ,EnrollAcademicType NVARCHAR(50)
            ,StuEnrollIdList NVARCHAR(MAX)
            );
        CREATE TABLE #Temp2
            (
             StudentId UNIQUEIDENTIFIER
            ,SSN NVARCHAR(50)
            ,StuFirstName NVARCHAR(50)
            ,StuLastName NVARCHAR(50)
            ,StuMiddleName NVARCHAR(50)
            ,StuDOB DATETIME
            ,StudentNumber NVARCHAR(50)
            ,StuEmail NVARCHAR(50)
            ,StuAddress1 NVARCHAR(50)
            ,StuAddress2 NVARCHAR(50)
            ,StuCity NVARCHAR(50)
            ,StuStateDescrip NVARCHAR(80)
            ,StuZIP NVARCHAR(50)
            ,StuCountryDescrip NVARCHAR(50)
            ,StuPhone NVARCHAR(50)
            ,EnrollAcademicType NVARCHAR(50)
            ,StuEnrollIdList NVARCHAR(MAX)
            );

        IF @ShowMultipleEnrollments = 0
            BEGIN
                SET @StatusList = NULL;
				
                INSERT  INTO #Temp1
                        SELECT  ASE.StudentId
                               ,CASE WHEN (
                                            APV.Credits > 0.0
                                            AND APV.Hours > 0
                                          )
                                          OR ( APV.IsContinuingEd = 1 ) THEN 'Credits-ClockHours'
                                     WHEN APV.Credits > 0.0 THEN 'Credits'
                                     WHEN APV.Hours > 0.0 THEN 'ClockHours'
                                     ELSE ''
                                END AcademicType
                               ,CONVERT(NVARCHAR(50),ASE.StuEnrollId) AS StuEnrollIdList
                        FROM    arStuEnrollments ASE
                        INNER JOIN syStatusCodes SC ON SC.StatusCodeId = ASE.StatusCodeId
                        INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId
					-- To get the Academic Type for each selected enrollment we go to Program version here
                        INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                        WHERE   (
                                  @StuEnrollIdList IS NULL
                                  OR ASE.StuEnrollId IN ( SELECT    Val
                                                          FROM      MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                                )
                        ORDER BY ASE.StudentId
                               ,ASE.StuEnrollId;                                          
            END;
        ELSE
            BEGIN
				-- if the parameter ShowMultipleEnrollmnets is True we will update the table #Temp1 with all enrollments for student selected
                INSERT  INTO #Temp1
                        SELECT  ASE.StudentId
                               ,CASE WHEN (
                                            APV.Credits > 0.0
                                            AND APV.Hours > 0
                                          )
                                          OR ( APV.IsContinuingEd = 1 ) THEN 'Credits-ClockHours'
                                     WHEN APV.Credits > 0.0 THEN 'Credits'
                                     WHEN APV.Hours > 0.0 THEN 'ClockHours'
                                     ELSE ''
                                END AcademicType
                               ,CONVERT(NVARCHAR(50),ASE.StuEnrollId) AS StuEnrollIdList
                        FROM    arStuEnrollments ASE
                        INNER JOIN syStatusCodes SC ON SC.StatusCodeId = ASE.StatusCodeId
                        INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId
					-- To get the Academic Type for each selected enrollment we go to Program version here
                        INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                        WHERE   ASE.StudentId IN ( SELECT DISTINCT
                                                            AST.StudentId
                                                   FROM     arStudent AS AST
                                                   INNER JOIN arStuEnrollments AS ASE1 ON ASE1.StudentId = AST.StudentId
                                                   WHERE    (
                                                              @StuEnrollIdList IS NULL
                                                              OR ASE1.StuEnrollId IN ( SELECT   Val
                                                                                       FROM     MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                                                            ) )
                        ORDER BY ASE.StudentId
                               ,ASE.StuEnrollId;
            END;

        IF ( @ShowMultipleEnrollments = 1 )
		-- if the parameter ShowMultipleEnrollmnets is True we will update the table #Temp1 with all enrollments for student selected
		-- but T1 table must be filtered for unique studentId
            BEGIN
				-- Create stuEnrollIdList for all student selected
                UPDATE  T4
                SET     T4.StuEnrollIdList = T2.List
                FROM    #Temp1 AS T4
                INNER JOIN (
                             SELECT T1.StudentId
                                   ,T1.EnrollAcademicType
                                   ,List = LEFT(T3.List,LEN(T3.List) - 1)
                             FROM   #Temp1 AS T1
                             CROSS APPLY (
                                           SELECT   CONVERT(VARCHAR(50),T2.StuEnrollIdList) + ','
                                           FROM     #Temp1 AS T2
                                           WHERE    T2.StudentId = T1.StudentId
                                                    AND T2.EnrollAcademicType = T1.EnrollAcademicType
                                         FOR
                                           XML PATH('')
                                         ) T3 ( List )
                           ) T2 ON T2.StudentId = T4.StudentId
                                   AND T2.EnrollAcademicType = T4.EnrollAcademicType; 
            END;


        INSERT  INTO #Temp2
                (
                 StudentId
                ,SSN
                ,StuFirstName
                ,StuLastName
                ,StuMiddleName
                ,StuDOB
                ,StudentNumber
                ,StuEmail
                ,StuAddress1
                ,StuAddress2
                ,StuCity
                ,StuStateDescrip
                ,StuZIP
                ,StuCountryDescrip
                ,StuPhone
                ,EnrollAcademicType
                ,StuEnrollIdList
				)
                SELECT  AST.StudentId AS StudentId
                       ,AST.SSN AS SSN
                       ,AST.FirstName AS StuFirstName
                       ,AST.LastName AS StuLastName
                       ,AST.MiddleName AS StuMiddleName
                       ,AST.DOB AS StuDOB
                       ,AST.StudentNumber AS StudentNumber
                       ,AST.HomeEmail AS StuEmail
                       ,ASA.Address1 AS StuAddress1
                       ,ASA.Address2 AS StuAddress2
                       ,ASA.City AS StuCity
                       ,ASA.StateDescrip AS StuStateDescrip
                       ,ASA.Zip AS StuZIP
                       ,ASA.CountryDescrip AS StuCountryDescrip
                       ,ASP.Phone AS StuPhone
                       ,T.EnrollAcademicType
                       ,T.StuEnrollIdList
                FROM    arStudent AS AST
                INNER JOIN (
                             SELECT DISTINCT
                                    T1.StudentId
                                   ,T1.EnrollAcademicType
                                   ,T1.StuEnrollIdList
                             FROM   #Temp1 AS T1
                           ) AS T ON T.StudentId = AST.StudentId
                LEFT OUTER JOIN (
                                  SELECT    ASA1.StudentId
                                           ,ASA1.Address1
                                           ,ASA1.Address2
                                           ,ASA1.City
                                           ,SS2.StateDescrip
                                           ,ASA1.Zip
                                           ,AC.CountryDescrip
                                           ,SS.Status
                                  FROM      arStudAddresses AS ASA1
                                  INNER JOIN syStatuses AS SS ON SS.StatusId = ASA1.StatusId
                                  LEFT OUTER JOIN adCountries AS AC ON AC.CountryId = ASA1.CountryId
                                  LEFT OUTER JOIN syStates AS SS2 ON SS2.StateId = ASA1.StateId
                                  WHERE     SS.Status = 'Active'
                                            AND ASA1.default1 = 1
                                ) AS ASA ON ASA.StudentId = AST.StudentId
                LEFT OUTER JOIN (
                                  SELECT    ASP1.StudentId
                                           ,ASP1.Phone
                                           ,ASP1.StatusId
                                           ,SS1.Status
                                  FROM      arStudentPhone AS ASP1
                                  INNER JOIN syStatuses AS SS1 ON SS1.StatusId = ASP1.StatusId
                                  WHERE     SS1.Status = 'Active'
                                            AND ASP1.default1 = 1
                                ) AS ASP ON ASP.StudentId = AST.StudentId;
  
		-- SELECT AND SORT     
        SELECT  StudentId
               ,SSN
               ,StuFirstName
               ,StuLastName
               ,StuMiddleName
               ,StuDOB
               ,StudentNumber
               ,StuEmail
               ,StuAddress1
               ,StuAddress2
               ,StuCity
               ,StuStateDescrip
               ,StuZIP
               ,StuCountryDescrip
               ,StuPhone
               ,EnrollAcademicType
               ,StuEnrollIdList
        FROM    #Temp2
        ORDER BY CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Asc,MiddleName Asc'
                      THEN ( RANK() OVER ( ORDER BY StuLastName, StuFirstName, StuMiddleName ) )
                 END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Asc,MiddleName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName, StuFirstName, StuMiddleName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Desc,MiddleName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName, StuFirstName DESC, StuMiddleName ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,FirstName Desc,MiddleName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName, StuFirstName DESC, StuMiddleName DESC ) )
                END
               ,
			 --LD
                CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Asc,MiddleName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuFirstName, StuMiddleName ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Asc,MiddleName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuFirstName, StuMiddleName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Desc,MiddleName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuFirstName DESC, StuMiddleName ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,FirstName Desc,MiddleName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuFirstName DESC, StuMiddleName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Asc,FirstName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName ASC, StuMiddleName ASC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Asc,FirstName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName ASC, StuMiddleName ASC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Desc,FirstName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName ASC, StuMiddleName DESC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Asc,MiddleName Desc,FirstName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName ASC, StuMiddleName DESC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Asc,FirstName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuMiddleName ASC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Asc,FirstName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuMiddleName ASC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Desc,FirstName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuMiddleName DESC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'LastName Desc,MiddleName Desc,FirstName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuMiddleName DESC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Asc,MiddleName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuLastName ASC, StuMiddleName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Asc,MiddleName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuLastName ASC, StuMiddleName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Desc,MiddleName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuLastName DESC, StuMiddleName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,LastName Desc,MiddleName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuLastName DESC, StuMiddleName DESC ) )
                END
               ,
			 --LD
                CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Asc,MiddleName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuLastName ASC, StuMiddleName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Asc,MiddleName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuLastName ASC, StuMiddleName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Desc,MiddleName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuLastName DESC, StuMiddleName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,LastName Desc,StuMiddleName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuLastName DESC, StuMiddleName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Asc,LastName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuMiddleName ASC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Asc,LastName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuMiddleName ASC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Desc,LastName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuMiddleName DESC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Asc,MiddleName Desc,LastName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuMiddleName DESC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Asc,LastName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuMiddleName ASC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Asc,LastName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuMiddleName ASC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Desc,LastName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuMiddleName DESC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'FirstName Desc,MiddleName Desc,LastName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuMiddleName DESC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Asc,FirstName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuLastName ASC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Asc,FirstName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuLastName ASC, StuFirstName DESC ) )
                END
               ,
			 
			 -- Start here
                CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Desc,FirstName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuLastName DESC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,LastName Desc,FirstName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuLastName DESC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Asc,FirstName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuLastName ASC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Asc,FirstName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuLastName ASC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Desc,FirstName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuLastName DESC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,LastName Desc,FirstName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuLastName DESC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Asc,LastName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuFirstName ASC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Asc,LastName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuFirstName ASC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Desc,LastName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuFirstName DESC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Asc,FirstName Desc,LastName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuFirstName DESC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Asc,LastName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuFirstName ASC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Asc,LastName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuFirstName ASC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Desc,LastName Asc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuFirstName DESC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = 'MiddleName Desc,FirstName Desc,LastName Desc'
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuFirstName DESC, StuLastName DESC ) )
                END
               ,StudentId;
    END;
-- =========================================================================================================
-- END  --  Usp_TR_Sub3_StudentInfo 
-- =========================================================================================================
GO

-- =========================================================================================================
-- Usp_TR_Sub6_Courses
-- =========================================================================================================
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[Usp_TR_Sub6_Courses]')
                    AND type IN ( N'P',N'PC' ) )
    BEGIN
        DROP PROCEDURE Usp_TR_Sub6_Courses;
    END;
GO

SET ANSI_NULLS ON;
SET QUOTED_IDENTIFIER ON;
GO
-- =========================================================================================================
-- Usp_TR_Sub6_Courses 
-- =========================================================================================================
CREATE PROCEDURE dbo.Usp_TR_Sub6_Courses
    @StuEnrollIdList VARCHAR(MAX)
   ,@TermId VARCHAR(50) = NULL
AS --''''''''''''''''''''''''''''' This section of the stored proc calculates the GPA '''''''''''''''''''''''''''''''
--''''''''''''''''''''''''''''' Term GPA and Program version GPA '''''''''''''''''''''''''''''''''''''''''''''''''
    DECLARE @CumulativeGPA_Range DECIMAL(18,2)
       ,@GradeCourseRepetitionsMethod VARCHAR(50);
    DECLARE @cumSimpleCourseCredits DECIMAL(18,2)
       ,@cumSimple_GPA_Credits DECIMAL(18,2)
       ,@cumSimpleGPA DECIMAL(18,2)
       ,@cumSimpleCourseCredits_OverAll DECIMAL(18,2)
       ,@cumSimple_GPA_Credits_OverAll DECIMAL(18,2)
       ,@cumSimpleGPA_OverAll DECIMAL(18,2);
    DECLARE @IsMakingSAP BIT;
    DECLARE @times INT
       ,@counter INT
       ,@StudentId VARCHAR(50)
       ,@StuEnrollId VARCHAR(50);
    DECLARE @cumCourseCredits DECIMAL(18,2)
       ,@cumWeighted_GPA_Credits DECIMAL(18,2)
       ,@cumWeightedGPA DECIMAL(18,2);
    DECLARE @EquivCourse_SA_CC INT
       ,@EquivCourse_SA_GPA DECIMAL(18,2)
       ,@EquivCourse_SA_CC_OverAll INT
       ,@EquivCourse_SA_GPA_OverAll DECIMAL(18,2);

	--DROP TABLE #CoursesNotRepeated
    CREATE TABLE #CoursesNotRepeated
        (
         StuEnrollId UNIQUEIDENTIFIER
        ,TermId UNIQUEIDENTIFIER
        ,TermDescrip VARCHAR(100)
        ,ReqId UNIQUEIDENTIFIER
        ,ReqDescrip VARCHAR(100)
        ,ClsSectionId VARCHAR(50)
        ,CreditsEarned DECIMAL(18,2)
        ,CreditsAttempted DECIMAL(18,2)
        ,CurrentScore DECIMAL(18,2)
        ,CurrentGrade VARCHAR(10)
        ,FinalScore DECIMAL(18,2)
        ,FinalGrade VARCHAR(10)
        ,Completed BIT
        ,FinalGPA DECIMAL(18,2)
        ,Product_WeightedAverage_Credits_GPA DECIMAL(18,2)
        ,Count_WeightedAverage_Credits DECIMAL(18,2)
        ,Product_SimpleAverage_Credits_GPA DECIMAL(18,2)
        ,Count_SimpleAverage_Credits DECIMAL(18,2)
        ,ModUser VARCHAR(50)
        ,ModDate DATETIME
        ,TermGPA_Simple DECIMAL(18,2)
        ,TermGPA_Weighted DECIMAL(18,2)
        ,coursecredits DECIMAL(18,2)
        ,CumulativeGPA DECIMAL(18,2)
        ,CumulativeGPA_Simple DECIMAL(18,2)
        ,FACreditsEarned DECIMAL(18,2)
        ,Average DECIMAL(18,2)
        ,CumAverage DECIMAL(18,2)
        ,TermStartDate DATETIME
        ,rownumber INT NULL
        ,StudentId UNIQUEIDENTIFIER
        );

    SET @GradeCourseRepetitionsMethod = (
                                          SELECT    Value
                                          FROM      dbo.syConfigAppSetValues t1
                                          INNER JOIN dbo.syConfigAppSettings t2 ON t1.SettingId = t2.SettingId
                                                                                   AND t2.KeyName = 'GradeCourseRepetitionsMethod'
                                        );

    SET @cumSimpleGPA = 0;


		  --SELECT  *
    --                   ,0 AS row-number
    --                   ,( SELECT TOP 1
    --                                StudentId
    --                      FROM      arStuEnrollments
    --                      WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId
    --                    ) AS StudentId
    --            FROM    syCreditSummary
    --            WHERE   StuEnrollId IN (Select Val from [MultipleValuesForReportParameters](@StuEnrollIdList,',',1))
				--		AND (FinalScore IS NOT NULL OR FinalGrade IS NOT NULL)
    --                    AND ReqId IN ( SELECT   ReqId
    --                                   FROM     ( SELECT    ReqId
--                                                       ,ReqDescrip
    --                                                       --,TermId
    --                                                       ,StuEnrollId
    --                                                       ,COUNT(*) AS counter
    --                                              FROM      syCreditSummary
    --                                              WHERE     StuEnrollId IN (Select Val from [MultipleValuesForReportParameters](@StuEnrollIdList,',',1))
    --                                              GROUP BY  ReqId
    --                                                       ,ReqDescrip
    --                                                       --,TermId
    --                                                       ,StuEnrollId
    --                                              HAVING    COUNT(*) = 1
    --                                            ) dt );


		

    CREATE TABLE #getStudentGPAbyTerms
        (
         StuEnrollId UNIQUEIDENTIFIER
        ,TermId UNIQUEIDENTIFIER
        ,TermDescrip VARCHAR(50)
        ,TermCredits DECIMAL(18,2) NULL
        ,TermSimpleGPA DECIMAL(18,2) NULL
        ,TermWeightedGPA DECIMAL(18,2) NULL
        ,TermStartDate DATETIME
        );

    INSERT  INTO #getStudentGPAbyTerms
            SELECT DISTINCT
                    StuEnrollId
                   ,T.TermId
                   ,T.TermDescrip
                   ,NULL
                   ,NULL
                   ,NULL
                   ,T.StartDate
            FROM    arResults R
            INNER JOIN arClassSections CS ON R.TestId = CS.ClsSectionId
            INNER JOIN arTerm T ON CS.TermId = T.TermId
            WHERE   R.StuEnrollId IN ( SELECT   Val
                                       FROM     MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
            UNION
            SELECT DISTINCT
                    StuEnrollId
                   ,T.TermId
                   ,T.TermDescrip
                   ,NULL
                   ,NULL
                   ,NULL
                   ,T.StartDate
            FROM    arTransferGrades TG
            INNER JOIN arTerm T ON TG.TermId = T.TermId
            WHERE   TG.StuEnrollId IN ( SELECT  Val
                                        FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) );

		--SELECT * FROM #getStudentGPAbyTerms WHERE StuEnrollID='95094539-B06E-4323-9F1C-AC1EF0B22F2F'

    CREATE TABLE #getStudentGPAByProgramVersion
        (
         LastName VARCHAR(50)
        ,FirstName VARCHAR(50)
        ,StudentId UNIQUEIDENTIFIER
        ,StudentNumber VARCHAR(50)
        ,StuEnrollID UNIQUEIDENTIFIER
        ,PrgVerId UNIQUEIDENTIFIER
        ,ProgramVersion VARCHAR(50)
        ,AcademicType NVARCHAR(50)
        ,ProgramVersion_SimpleGPA DECIMAL(18,2)
        ,ProgramVersion_WeightedGPA DECIMAL(18,2)
        ,OverAll_SimpleGPA DECIMAL(18,2)
        ,OverAll_WeightedGPA DECIMAL(18,2)
        ,IsMakingSAP BIT
        ,SSN VARCHAR(11)
        ,EnrollmentID VARCHAR(50)
        ,MiddleName VARCHAR(50)
        ,TermId UNIQUEIDENTIFIER
        ,TermDescription VARCHAR(50)
        ,rowNumber INT
        );

    INSERT  INTO #getStudentGPAByProgramVersion
            SELECT DISTINCT
                    S.LastName
                   ,S.FirstName
                   ,S.StudentId
                   ,S.StudentNumber
                   ,SE.StuEnrollId
                   ,APV.PrgVerId
                   ,APV.PrgVerDescrip
                   ,CASE WHEN (
                                APV.Credits > 0.0
                                AND APV.Hours > 0
                              )
                              OR ( APV.IsContinuingEd = 1 ) THEN 'Credits-ClockHours'
                         WHEN APV.Credits > 0.0 THEN 'Credits'
                         WHEN APV.Hours > 0.0 THEN 'ClockHours'
                         ELSE ''
                    END AcademicType
                   ,NULL AS ProgramVersion_SimpleGPA
                   ,NULL AS ProgramVersion_WeightedGPA
                   ,NULL AS OverAll_SimpleGPA
                   ,NULL AS OverAll_WeightedGPA
                   ,0 AS IsMakingSAP
                   ,S.SSN
                   ,SE.EnrollmentId
                   ,S.MiddleName
                   ,GST.TermId
                   ,GST.TermDescrip
                   ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId ORDER BY GST.TermStartDate DESC ) AS RowNumber
            FROM    arStudent S
            INNER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
            INNER JOIN dbo.arPrgVersions APV ON SE.PrgVerId = APV.PrgVerId
            INNER JOIN #getStudentGPAbyTerms GST ON GST.StuEnrollId = SE.StuEnrollId
            LEFT OUTER JOIN dbo.adLeadByLeadGroups LLG ON SE.StuEnrollId = LLG.StuEnrollId
            WHERE   SE.StuEnrollId IN ( SELECT  Val
                                        FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) );
						

    INSERT  INTO #CoursesNotRepeated
            SELECT  StuEnrollId
                   ,TermId
                   ,TermDescrip
                   ,ReqId
                   ,ReqDescrip
                   ,ClsSectionId
                   ,CreditsEarned
                   ,CreditsAttempted
                   ,CurrentScore
                   ,CurrentGrade
                   ,FinalScore
                   ,FinalGrade
                   ,Completed
                   ,FinalGPA
                   ,Product_WeightedAverage_Credits_GPA
                   ,Count_WeightedAverage_Credits
                   ,Product_SimpleAverage_Credits_GPA
                   ,Count_SimpleAverage_Credits
                   ,ModUser
                   ,ModDate
                   ,TermGPA_Simple
                   ,TermGPA_Weighted
                   ,coursecredits
                   ,CumulativeGPA
                   ,CumulativeGPA_Simple
                   ,FACreditsEarned
                   ,Average
                   ,CumAverage
                   ,TermStartDate
                   ,0 AS rownumber
                   ,(
                      SELECT TOP 1
                                StudentId
                      FROM      arStuEnrollments
                      WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId
                    ) AS StudentId
            FROM    syCreditSummary
            WHERE   StuEnrollId IN ( SELECT DISTINCT
                                            StuEnrollID
                                     FROM   #getStudentGPAByProgramVersion )
                    AND (
                          FinalScore IS NOT NULL
                          OR FinalGrade IS NOT NULL
                        )
                    AND ReqId IN ( SELECT   ReqId
                                   FROM     (
                                              SELECT    ReqId
                                                       ,ReqDescrip
                                                           --,TermId
                                                       ,StuEnrollId
                                                       ,COUNT(*) AS counter
                                              FROM      syCreditSummary
                                              WHERE     StuEnrollId IN ( SELECT DISTINCT
                                                                                StuEnrollID
                                                                         FROM   #getStudentGPAByProgramVersion )
                                              GROUP BY  ReqId
                                                       ,ReqDescrip
                                                           --,TermId
                                                       ,StuEnrollId
                                              HAVING    COUNT(*) = 1
                                            ) dt );

		--Select * from #CoursesNotRepeated where ReqId='485FBB54-202D-4A46-A353-3F9C76A275E4'
		--Select * from syCreditSummary

    IF LOWER(@GradeCourseRepetitionsMethod) = 'latest'
        BEGIN
            INSERT  INTO #CoursesNotRepeated
                    SELECT  dt2.StuEnrollId
                           ,dt2.TermId
                           ,dt2.TermDescrip
                           ,dt2.ReqId
                           ,dt2.ReqDescrip
                           ,dt2.ClsSectionId
                           ,dt2.CreditsEarned
                           ,dt2.CreditsAttempted
                           ,dt2.CurrentScore
                           ,dt2.CurrentGrade
                           ,dt2.FinalScore
                           ,dt2.FinalGrade
                           ,dt2.Completed
                           ,dt2.FinalGPA
                           ,dt2.Product_WeightedAverage_Credits_GPA
                           ,dt2.Count_WeightedAverage_Credits
                           ,dt2.Product_SimpleAverage_Credits_GPA
                           ,dt2.Count_SimpleAverage_Credits
                           ,dt2.ModUser
                           ,dt2.ModDate
                           ,dt2.TermGPA_Simple
                           ,dt2.TermGPA_Weighted
                           ,dt2.coursecredits
                           ,dt2.CumulativeGPA
                           ,dt2.CumulativeGPA_Simple
                           ,dt2.FACreditsEarned
                           ,dt2.Average
                           ,dt2.CumAverage
                           ,dt2.TermStartDate
                           ,dt2.RowNumber
                           ,dt2.StudentId
                    FROM    (
                              SELECT    StuEnrollId
                                       ,TermId
                                       ,TermDescrip
                                       ,ReqId
                                       ,ReqDescrip
                                       ,ClsSectionId
                                       ,CreditsEarned
                                       ,CreditsAttempted
                                       ,CurrentScore
                                       ,CurrentGrade
                                       ,FinalScore
                                       ,FinalGrade
                                       ,Completed
                                       ,FinalGPA
                                       ,Product_WeightedAverage_Credits_GPA
                                       ,Count_WeightedAverage_Credits
                                       ,Product_SimpleAverage_Credits_GPA
                                       ,Count_SimpleAverage_Credits
                                       ,ModUser
                                       ,ModDate
                                       ,TermGPA_Simple
                                       ,TermGPA_Weighted
                                       ,coursecredits
                                       ,CumulativeGPA
                                       ,CumulativeGPA_Simple
                                       ,FACreditsEarned
                                       ,Average
                                       ,CumAverage
                                       ,TermStartDate
                                       ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY TermStartDate DESC ) AS RowNumber
                                       ,(
                                          SELECT TOP 1
                                                    StudentId
                                          FROM      arStuEnrollments
                                          WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId
                                        ) AS StudentId
                              FROM      syCreditSummary
                              WHERE     StuEnrollId IN ( SELECT StuEnrollID
                                                         FROM   #getStudentGPAByProgramVersion )
                                        AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                            )
                                        AND ReqId IN ( SELECT   ReqId
                                                       FROM     (
                                                                  SELECT    ReqId
                                                                           ,ReqDescrip
                                                               --,TermId
                                                                           ,StuEnrollId
                                                                           ,COUNT(*) AS Counter
                                                                  FROM      syCreditSummary
                                                                  WHERE     StuEnrollId IN ( SELECT StuEnrollID
                                                                                             FROM   #getStudentGPAByProgramVersion )
                                                                  GROUP BY  ReqId
                                                                           ,ReqDescrip
                                                               --,TermId
                                                                           ,StuEnrollId
                                                                  HAVING    COUNT(*) > 1
                                                                ) dt )
                            ) dt2
                    WHERE   RowNumber = 1
                    ORDER BY TermStartDate DESC;
        END;

    IF LOWER(@GradeCourseRepetitionsMethod) = 'best'
        BEGIN
            INSERT  INTO #CoursesNotRepeated
                    SELECT  dt2.StuEnrollId
                           ,dt2.TermId
                           ,dt2.TermDescrip
                           ,dt2.ReqId
                           ,dt2.ReqDescrip
                           ,dt2.ClsSectionId
                           ,dt2.CreditsEarned
                           ,dt2.CreditsAttempted
                           ,dt2.CurrentScore
                           ,dt2.CurrentGrade
                           ,dt2.FinalScore
                           ,dt2.FinalGrade
                           ,dt2.Completed
                           ,dt2.FinalGPA
                           ,dt2.Product_WeightedAverage_Credits_GPA
                           ,dt2.Count_WeightedAverage_Credits
                           ,dt2.Product_SimpleAverage_Credits_GPA
                           ,dt2.Count_SimpleAverage_Credits
                           ,dt2.ModUser
                           ,dt2.ModDate
                           ,dt2.TermGPA_Simple
                           ,dt2.TermGPA_Weighted
                           ,dt2.coursecredits
                           ,dt2.CumulativeGPA
                           ,dt2.CumulativeGPA_Simple
                           ,dt2.FACreditsEarned
                           ,dt2.Average
                           ,dt2.CumAverage
                           ,dt2.TermStartDate
                           ,dt2.RowNumber
                           ,dt2.StudentId
                    FROM    (
                              SELECT    StuEnrollId
                                       ,TermId
                                       ,TermDescrip
                                       ,ReqId
                                       ,ReqDescrip
                                       ,ClsSectionId
                                       ,CreditsEarned
                                       ,CreditsAttempted
                                       ,CurrentScore
                                       ,CurrentGrade
                                       ,FinalScore
                                       ,FinalGrade
                                       ,Completed
                                       ,FinalGPA
                                       ,Product_WeightedAverage_Credits_GPA
                                       ,Count_WeightedAverage_Credits
                                       ,Product_SimpleAverage_Credits_GPA
                                       ,Count_SimpleAverage_Credits
                                       ,ModUser
                                       ,ModDate
                                       ,TermGPA_Simple
                                       ,TermGPA_Weighted
                                       ,coursecredits
                                       ,CumulativeGPA
                                       ,CumulativeGPA_Simple
                                       ,FACreditsEarned
                                       ,Average
                                       ,CumAverage
                                       ,TermStartDate
                                       ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY FinalGPA DESC ) AS RowNumber
                                       ,(
                                          SELECT TOP 1
                                                    StudentId
                                          FROM      arStuEnrollments
                                          WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId
                                        ) AS StudentId
                              FROM      syCreditSummary
                              WHERE     StuEnrollId IN ( SELECT StuEnrollID
                                                         FROM   #getStudentGPAByProgramVersion )
                                        AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                            )
                                        AND ReqId IN ( SELECT   ReqId
                                                       FROM     (
                                                                  SELECT    ReqId
                                                                           ,ReqDescrip
                                                               --,TermId
                                                                           ,StuEnrollId
                                                                           ,COUNT(*) AS Counter
                                                                  FROM      syCreditSummary
                                                                  WHERE     StuEnrollId IN ( SELECT StuEnrollID
                                                                                             FROM   #getStudentGPAByProgramVersion )
                                                                  GROUP BY  ReqId
                                                                           ,ReqDescrip
                                                               --,TermId
                                                                           ,StuEnrollId
                                                                  HAVING    COUNT(*) > 1
                                                                ) dt )
                            ) dt2
                    WHERE   RowNumber = 1; 
        END;

        --Select * from #CoursesNotRepeated where ReqId='485FBB54-202D-4A46-A353-3F9C76A275E4'

    IF LOWER(@GradeCourseRepetitionsMethod) = 'average'
        BEGIN
            INSERT  INTO #CoursesNotRepeated
                    SELECT  dt2.StuEnrollId
                           ,dt2.TermId
                           ,dt2.TermDescrip
                           ,dt2.ReqId
                           ,dt2.ReqDescrip
                           ,dt2.ClsSectionId
                           ,dt2.CreditsEarned
                           ,dt2.CreditsAttempted
                           ,dt2.CurrentScore
                           ,dt2.CurrentGrade
                           ,dt2.FinalScore
                           ,dt2.FinalGrade
                           ,dt2.Completed
                           ,dt2.FinalGPA
                           ,dt2.Product_WeightedAverage_Credits_GPA
                           ,dt2.Count_WeightedAverage_Credits
                           ,dt2.Product_SimpleAverage_Credits_GPA
                           ,dt2.Count_SimpleAverage_Credits
                           ,dt2.ModUser
                           ,dt2.ModDate
                           ,dt2.TermGPA_Simple
                           ,dt2.TermGPA_Weighted
                           ,dt2.coursecredits
                           ,dt2.CumulativeGPA
                           ,dt2.CumulativeGPA_Simple
                           ,dt2.FACreditsEarned
                           ,dt2.Average
                           ,dt2.CumAverage
                           ,dt2.TermStartDate
                           ,dt2.RowNumber
                           ,dt2.StudentId
                    FROM    (
                              SELECT    StuEnrollId
                                       ,TermId
                                       ,TermDescrip
                                       ,ReqId
                                       ,ReqDescrip
                                       ,ClsSectionId
                                       ,CreditsEarned
                                       ,CreditsAttempted
                                       ,CurrentScore
                                       ,CurrentGrade
                                       ,FinalScore
                                       ,FinalGrade
                                       ,Completed
                                       ,FinalGPA
                                       ,Product_WeightedAverage_Credits_GPA
                                       ,Count_WeightedAverage_Credits
                                       ,Product_SimpleAverage_Credits_GPA
                                       ,Count_SimpleAverage_Credits
                                       ,ModUser
                                       ,ModDate
                                       ,TermGPA_Simple
                                       ,TermGPA_Weighted
                                       ,coursecredits
                                       ,CumulativeGPA
                                       ,CumulativeGPA_Simple
                                       ,FACreditsEarned
                                       ,Average
                                       ,CumAverage
                                       ,TermStartDate
                                       ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY FinalGPA DESC ) AS RowNumber
                                       ,(
                                          SELECT TOP 1
                                                    StudentId
                                          FROM      arStuEnrollments
                                          WHERE     StuEnrollId = dbo.syCreditSummary.StuEnrollId
                                        ) AS StudentId
                              FROM      syCreditSummary
                              WHERE     StuEnrollId IN ( SELECT StuEnrollID
                                                         FROM   #getStudentGPAByProgramVersion )
                                        AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                            )
                                        AND ReqId IN ( SELECT   ReqId
                                                       FROM     (
                                                                  SELECT    ReqId
                                                                           ,ReqDescrip
                                                               --,TermId
                                                                           ,StuEnrollId
                                                                           ,COUNT(*) AS Counter
                                                                  FROM      syCreditSummary
                                                                  WHERE     StuEnrollId IN ( SELECT StuEnrollID
                                                                                             FROM   #getStudentGPAByProgramVersion )
                                                                  GROUP BY  ReqId
                                                                           ,ReqDescrip
                                                               --,TermId
                                                                           ,StuEnrollId
                                                                  HAVING    COUNT(*) > 1
                                                                ) dt )
                            ) dt2; 
        END;
--- ******************* Equivalent Courses in Term Temp Table Starts Here *************************	
-- Get Equivalent courses for all courses that student is currently registered in
    SELECT  dt.EquivReqId
           ,dt.StuEnrollId
    INTO    #tmpEquivalentCoursesTakenByStudentInTerm
    FROM    -- Get the list of Equivalent courses taken by student enrollment
            (
              SELECT DISTINCT
                        CE.EquivReqId
                       ,R.StuEnrollId
                       ,CS.TermId
              FROM      arResults R
              INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = R.TestId
              INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                AND CSS.ReqId = CS.ReqId
              INNER JOIN arReqs R1 ON R1.ReqId = CS.ReqId
              INNER JOIN arCourseEquivalent CE ON R1.ReqId = CE.ReqId
              WHERE     R.StuEnrollId IN ( SELECT   Val
                                           FROM     MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                        AND R.GrdSysDetailId IS NOT NULL
                        AND CSS.FinalGPA IS NOT NULL
              UNION
              SELECT DISTINCT
                        CE.EquivReqId
                       ,TR.StuEnrollId
                       ,TR.TermId
              FROM      dbo.arTransferGrades TR
              INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = TR.StuEnrollId
                                                AND CSS.ReqId = TR.ReqId
              INNER JOIN arReqs R1 ON R1.ReqId = TR.ReqId
              INNER JOIN arCourseEquivalent CE ON R1.ReqId = CE.ReqId
              WHERE     TR.StuEnrollId IN ( SELECT  Val
                                            FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                        AND TR.GrdSysDetailId IS NOT NULL
                        AND CSS.FinalGPA IS NOT NULL
            ) dt;

-- Check if student has got a grade on any of the Equivalent courses
    SELECT DISTINCT
            ECS.EquivReqId
           ,R.StuEnrollId
           ,CS.TermId
    INTO    #tmpStudentsWhoHasGradedEquivalentCourseInTerm
    FROM    arResults R
    INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = R.TestId
    INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                      AND CSS.ReqId = CS.ReqId
    INNER JOIN #tmpEquivalentCoursesTakenByStudentInTerm ECS ON R.StuEnrollId = ECS.StuEnrollId
                                                                AND ECS.EquivReqId = CS.ReqId
    WHERE   R.StuEnrollId IN ( SELECT   Val
                               FROM     MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
            AND R.GrdSysDetailId IS NOT NULL
            AND CSS.FinalGPA IS NOT NULL
    UNION
    SELECT DISTINCT
            ECS.EquivReqId
           ,TR.StuEnrollId
           ,TR.TermId
    FROM    dbo.arTransferGrades TR
    INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = TR.ReqId
    INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = TR.StuEnrollId
                                      AND CSS.ReqId = CS.ReqId
    INNER JOIN #tmpEquivalentCoursesTakenByStudentInTerm ECS ON TR.StuEnrollId = ECS.StuEnrollId
                                                                AND ECS.EquivReqId = CS.ReqId
    WHERE   TR.StuEnrollId IN ( SELECT  Val
                                FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
            AND TR.GrdSysDetailId IS NOT NULL
            AND CSS.FinalGPA IS NOT NULL;
--- ******************* Equivalent Courses Temp Table Ends Here *************************	

    DECLARE @curTermId UNIQUEIDENTIFIER;
    DECLARE getNodes_Cursor CURSOR FAST_FORWARD FORWARD_ONLY
    FOR
        SELECT  DISTINCT
                StuEnrollId
               ,TermId
        FROM    #CoursesNotRepeated;
	  

    OPEN getNodes_Cursor;
    FETCH NEXT FROM getNodes_Cursor
				INTO @StuEnrollId,@curTermId;
    WHILE @@FETCH_STATUS = 0
        BEGIN
				----- Program Version GPA Starts Here-----------------------------------------------------------------------
            SET @cumSimpleCourseCredits = (
                                            SELECT  COUNT(coursecredits)
                                            FROM    #CoursesNotRepeated
                                            WHERE   StuEnrollId IN ( @StuEnrollId )
                                                    AND TermId IN ( @curTermId )
                                                    AND FinalGPA IS NOT NULL
                                          );


            SET @cumSimple_GPA_Credits = (
                                           SELECT   SUM(FinalGPA)
                                           FROM     #CoursesNotRepeated
                                           WHERE    StuEnrollId IN ( @StuEnrollId )
                                                    AND TermId IN ( @curTermId )
                                                    AND FinalGPA IS NOT NULL
                                         );

            SET @EquivCourse_SA_CC = (
                                       --SELECT    ISNULL(COUNT(Credits), 0) AS Credits
                                      --FROM      arReqs
                                      --WHERE     ReqId IN (
                                      --          SELECT  CE.EquivReqId
                                      --          FROM    arCourseEquivalent CE
                                      --                  INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                      --                  INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                      --                  INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                      --                        AND CSS.ReqId = CS.ReqId
                                      --          WHERE   R.StuEnrollId IN (
                                      --                  @StuEnrollId)
                                      --                  AND CS.TermId IN (
                                      --                  @curTermId)
                                      --                  AND R.GrdSysDetailId IS NOT NULL
                                      --                  AND CSS.FinalGPA IS NOT NULL)
                                       SELECT   ISNULL(COUNT(Credits),0) AS Credits
                                       FROM     arReqs
                                       WHERE    ReqId IN ( SELECT   CE.EquivReqId
                                                           FROM     #tmpStudentsWhoHasGradedEquivalentCourseInTerm CE
                                                           WHERE    CE.StuEnrollId IN ( @StuEnrollId )
                                                                    AND CE.TermId IN ( @curTermId ) )
                                     );

            SET @EquivCourse_SA_GPA = (
                                        SELECT  SUM(ISNULL(GSD.GPA,0.00))
                                        FROM    #tmpStudentsWhoHasGradedEquivalentCourseInTerm GEC
                                        INNER JOIN arResults R ON GEC.StuEnrollId = R.StuEnrollId
                                        INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                        INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                          AND CSS.ReqId = GEC.EquivReqId
                                        WHERE   R.StuEnrollId IN ( @StuEnrollId )
                                                AND GEC.TermId IN ( @curTermId )
                                      );
            --                           SELECT   SUM(ISNULL(GSD.GPA, 0.00))
            --                           FROM     arCourseEquivalent CE
            --                                    INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
            --                                    INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
            --                                    INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
            --INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
            --                                                  AND CSS.ReqId = CS.ReqId
            --                           WHERE    R.StuEnrollId IN (@StuEnrollId)
            --                                    AND CS.TermId IN (@curTermId)
            --                                    AND R.GrdSysDetailId IS NOT NULL
            --                                    AND CSS.FinalGPA IS NOT NULL
                                    


            SET @cumSimpleCourseCredits = @cumSimpleCourseCredits;                                            --  + ISNULL(@EquivCourse_SA_CC, 0.00);
            SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits;                                              --  + ISNULL(@EquivCourse_SA_GPA, 0.00);

	
            IF @cumSimpleCourseCredits >= 1
                BEGIN
                    SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                END; 

            SET @cumWeightedGPA = 0;
            SET @cumCourseCredits = (
                                      SELECT    SUM(coursecredits)
                                      FROM      #CoursesNotRepeated
                                      WHERE     StuEnrollId IN ( @StuEnrollId )
                                                AND TermId IN ( @curTermId )
                                                AND FinalGPA IS NOT NULL
                                    );
            SET @cumWeighted_GPA_Credits = (
                                             SELECT SUM(coursecredits * FinalGPA)
                                             FROM   #CoursesNotRepeated
                                             WHERE  StuEnrollId IN ( @StuEnrollId )
                                                    AND TermId IN ( @curTermId )
                                                    AND FinalGPA IS NOT NULL
                                           );

            DECLARE @doesThisCourseHaveAEquivalentCourse INT
               ,@EquivCourse_WGPA_CC1 INT
               ,@EquivCourse_WGPA_GPA1 DECIMAL(18,2);
	
            SET @EquivCourse_WGPA_CC1 = (
                                          SELECT    SUM(ISNULL(Credits,0)) AS Credits
                                          FROM      arReqs
                                          WHERE     ReqId IN ( SELECT   CE.EquivReqId
                                                               FROM     #tmpStudentsWhoHasGradedEquivalentCourseInTerm CE
                                                               WHERE    CE.StuEnrollId IN ( @StuEnrollId )
                                                                        AND CE.TermId IN ( @TermId ) )
                                        );
                                         --SELECT SUM(ISNULL(Credits, 0)) AS Credits
                                         --FROM   arReqs
                                         --WHERE  ReqId IN (
                                         --       SELECT  CE.EquivReqId
                                         --       FROM    arCourseEquivalent CE
                                         --               INNER JOIN dbo.arClassSections CS ON CE.EquivReqId = CS.ReqId
                                         --               INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                         --               INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                         --                     AND CSS.ReqId = CE.EquivReqId
                                         --       WHERE   R.StuEnrollId IN (
                                         --               @StuEnrollId)
                                         --               AND R.GrdSysDetailId IS NOT NULL
                                         --               AND CS.TermId IN (
                                         --               @curTermId)
                                         --               AND CSS.FinalGPA IS NOT NULL)
                                        
		
            SET @EquivCourse_WGPA_GPA1 = (
                                           SELECT   SUM(GSD.GPA * R1.Credits)
                                           FROM     #tmpStudentsWhoHasGradedEquivalentCourseInTerm GEC
                                           INNER JOIN arResults R ON GEC.StuEnrollId = R.StuEnrollId
                                           INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                           INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                             AND CSS.ReqId = GEC.EquivReqId
                                           INNER JOIN arReqs R1 ON R1.ReqId = GEC.EquivReqId
                                           WHERE    R.StuEnrollId IN ( @StuEnrollId )
                                                    AND GEC.TermId IN ( @curTermId )


                           --               SELECT    SUM(GSD.GPA * R1.Credits)
                           --               FROM      arCourseEquivalent CE
                           --                         INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                           --                         INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                           --INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                           --                         INNER JOIN arReqs R1 ON R1.ReqId = CE.ReqId
                           --                         INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                           --                                   AND CSS.ReqId = CS.ReqId
                           --               WHERE     R.StuEnrollId IN (
                           --                         @StuEnrollId)
                           --                         AND R.GrdSysDetailId IS NOT NULL
                           --                         AND CS.TermId IN (
                           --                         @curTermId)
                           --                         AND CSS.FinalGPA IS NOT NULL
                                         );

		
            SET @cumCourseCredits = @cumCourseCredits;                                                        --  + ISNULL(@EquivCourse_WGPA_CC1, 0.00);
            SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits;                                          --  + ISNULL(@EquivCourse_WGPA_GPA1, 0.00);

            IF @cumCourseCredits >= 1
                BEGIN
                    SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                END; 	

            DECLARE @TermCredits DECIMAL(18,2);
            SET @TermCredits = (
                                 SELECT SUM(CreditsEarned)
                                 FROM   #CoursesNotRepeated
                                 WHERE  StuEnrollId IN ( @StuEnrollId )
                                        AND TermId IN ( @curTermId )
                                        AND FinalGPA IS NOT NULL
                               );

            UPDATE  #getStudentGPAbyTerms
            SET     TermCredits = @TermCredits
                   ,TermSimpleGPA = @cumSimpleGPA
                   ,TermWeightedGPA = @cumWeightedGPA
            WHERE   StuEnrollId = @StuEnrollId
                    AND TermId = @curTermId;

						 
            FETCH NEXT FROM getNodes_Cursor
				INTO @StuEnrollId,@curTermId;
        END;
    CLOSE getNodes_Cursor;
    DEALLOCATE getNodes_Cursor;


	-- Get Equivalent courses for all courses that student is currently registered in
    SELECT  dt.EquivReqId
           ,dt.StuEnrollId
    INTO    #tmpEquivalentCoursesTakenByStudent
    FROM    -- Get the list of Equivalent courses taken by student enrollment
            (
              SELECT DISTINCT
                        CE.EquivReqId
                       ,R.StuEnrollId
              FROM      arResults R
              INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = R.TestId
              INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                AND CSS.ReqId = CS.ReqId
              INNER JOIN arReqs R1 ON R1.ReqId = CS.ReqId
              INNER JOIN arCourseEquivalent CE ON R1.ReqId = CE.ReqId
              WHERE     R.StuEnrollId IN ( SELECT   Val
                                           FROM     MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                        AND R.GrdSysDetailId IS NOT NULL
                        AND CSS.FinalGPA IS NOT NULL
              UNION
              SELECT DISTINCT
                        CE.EquivReqId
                       ,TR.StuEnrollId
              FROM      dbo.arTransferGrades TR
              INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = TR.StuEnrollId
                                                AND CSS.ReqId = TR.ReqId
              INNER JOIN arReqs R1 ON R1.ReqId = TR.ReqId
              INNER JOIN arCourseEquivalent CE ON R1.ReqId = CE.ReqId
              WHERE     TR.StuEnrollId IN ( SELECT  Val
                                            FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                        AND TR.GrdSysDetailId IS NOT NULL
                        AND CSS.FinalGPA IS NOT NULL
            ) dt;

-- Check if student has got a grade on any of the Equivalent courses
    SELECT DISTINCT
            ECS.EquivReqId
           ,R.StuEnrollId
    INTO    #tmpStudentsWhoHasGradedEquivalentCourse
    FROM    arResults R
    INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = R.TestId
    INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                      AND CSS.ReqId = CS.ReqId
    INNER JOIN #tmpEquivalentCoursesTakenByStudent ECS ON R.StuEnrollId = ECS.StuEnrollId
                                                          AND ECS.EquivReqId = CS.ReqId
    WHERE   R.StuEnrollId IN ( SELECT   Val
                               FROM     MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
            AND R.GrdSysDetailId IS NOT NULL
            AND CSS.FinalGPA IS NOT NULL
    UNION
    SELECT DISTINCT
            ECS.EquivReqId
           ,TR.StuEnrollId
    FROM    dbo.arTransferGrades TR
    INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = TR.ReqId
    INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = TR.StuEnrollId
                                      AND CSS.ReqId = CS.ReqId
    INNER JOIN #tmpEquivalentCoursesTakenByStudent ECS ON TR.StuEnrollId = ECS.StuEnrollId
                                                          AND ECS.EquivReqId = CS.ReqId
    WHERE   TR.StuEnrollId IN ( SELECT  Val
                                FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
            AND TR.GrdSysDetailId IS NOT NULL
            AND CSS.FinalGPA IS NOT NULL;

    DECLARE getNodes_Cursor CURSOR FAST_FORWARD FORWARD_ONLY
    FOR
        SELECT  StuEnrollID
        FROM    #getStudentGPAByProgramVersion;
	  

    OPEN getNodes_Cursor;
    FETCH NEXT FROM getNodes_Cursor
				INTO @StuEnrollId;
    WHILE @@FETCH_STATUS = 0
        BEGIN
				----- Program Version GPA Starts Here-----------------------------------------------------------------------
            SET @cumSimpleCourseCredits = (
                                            SELECT  COUNT(coursecredits)
                                            FROM    #CoursesNotRepeated
                                            WHERE   StuEnrollId IN ( @StuEnrollId )
                                                    AND FinalGPA IS NOT NULL
                                          );

			--Print '@cumSimpleCourseCredits'
			--Print @cumSimpleCourseCredits

            SET @cumSimpleCourseCredits_OverAll = (
                                                    SELECT  COUNT(coursecredits)
                                                    FROM    #CoursesNotRepeated
                                                    WHERE   StudentId IN ( SELECT   StudentId
                                                                           FROM     arStuEnrollments
                                                                           WHERE    StuEnrollId = @StuEnrollId )
                                                            AND FinalGPA IS NOT NULL
                                                  );


            SET @cumSimple_GPA_Credits = (
                                           SELECT   SUM(FinalGPA)
                                           FROM     #CoursesNotRepeated
                                           WHERE    StuEnrollId IN ( @StuEnrollId )
                                                    AND FinalGPA IS NOT NULL
                                         );

            SET @cumSimple_GPA_Credits_OverAll = (
                                                   SELECT   SUM(FinalGPA)
                                                   FROM     #CoursesNotRepeated
                                                   WHERE    StudentId IN ( SELECT   StudentId
                                                                           FROM     arStuEnrollments
                                                                           WHERE    StuEnrollId = @StuEnrollId )
                                                            AND FinalGPA IS NOT NULL
                                                 );

			
	
            SET @EquivCourse_SA_CC = (
                                       SELECT   ISNULL(COUNT(Credits),0) AS Credits
                                       FROM     arReqs
                                       WHERE    ReqId IN ( SELECT   CE.EquivReqId
                                                           FROM     #tmpStudentsWhoHasGradedEquivalentCourse CE
                                                           WHERE    CE.StuEnrollId IN ( @StuEnrollId ) )
                                     );

			
														--SELECT  CE.EquivReqId
														--FROM    arCourseEquivalent CE
														--        INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
														--        INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
														--        INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
														--              AND CSS.ReqId = CS.ReqId
														--WHERE   R.StuEnrollId IN (
														--        @StuEnrollId)
														--        AND R.GrdSysDetailId IS NOT NULL
														--        AND CSS.FinalGPA IS NOT NULL
												
												
            SET @EquivCourse_SA_CC_OverAll = (
                                               SELECT   ISNULL(COUNT(Credits),0) AS Credits
                                               FROM     arReqs
                                               WHERE    ReqId IN ( SELECT DISTINCT
                                                                            CE.EquivReqId
                                                                   FROM     #tmpStudentsWhoHasGradedEquivalentCourse CE
                                                                   INNER JOIN arStuEnrollments SE ON CE.StuEnrollId = SE.StuEnrollId
                                                                   INNER JOIN arStudent S ON SE.StudentId = S.StudentId
                                                                   WHERE    S.StudentId IN ( SELECT DISTINCT
                                                                                                    StudentId
                                                                                             FROM   dbo.arStuEnrollments SE1
                                                                                             WHERE  SE1.StuEnrollId = @StuEnrollId ) )
                                             );

            PRINT 'Test Equiv Course=';
            PRINT @EquivCourse_SA_CC;
            PRINT @EquivCourse_SA_CC_OverAll;

                                      --                  SELECT
                                      --                        CE.EquivReqId
                                      --                  FROM  arCourseEquivalent CE
                                      --                        INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                      --                        INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                      --                        INNER JOIN dbo.arStuEnrollments SE ON R.StuEnrollId = SE.StuEnrollId
                                      --                        INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                      --                        AND CSS.ReqId = CS.ReqId
                                      --                  WHERE SE.StudentId IN (
                                      --                        SELECT
                                      --                        StudentId
                                      --FROM
                                      --                        arStuEnrollments
                                      --                        WHERE
                                      --                        StuEnrollId = @StuEnrollId)
                                      --                        AND R.GrdSysDetailId IS NOT NULL
                                      --                        AND CSS.FinalGPA IS NOT NULL)
                                           
		
            SET @EquivCourse_SA_GPA = (
                                        SELECT  SUM(ISNULL(GSD.GPA,0.00))
                                        FROM    #tmpStudentsWhoHasGradedEquivalentCourse GEC
                                        INNER JOIN arResults R ON GEC.StuEnrollId = R.StuEnrollId
                                        INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                        INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                          AND CSS.ReqId = GEC.EquivReqId
                                        WHERE   R.StuEnrollId IN ( @StuEnrollId )
                                      );

			
                                       --SELECT   SUM(ISNULL(GSD.GPA, 0.00))
                                       --FROM     arCourseEquivalent CE
                                       --         INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                       --         INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                       --         INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                       --         INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                       --                       AND CSS.ReqId = CS.ReqId
                                       --WHERE    R.StuEnrollId IN (@StuEnrollId)
                                       --         AND R.GrdSysDetailId IS NOT NULL
                                       --         AND CSS.FinalGPA IS NOT NULL
                                      

            SET @EquivCourse_SA_GPA_OverAll = (
                                                SELECT  SUM(ISNULL(GSD.GPA,0.00))
                                                FROM    #tmpStudentsWhoHasGradedEquivalentCourse GEC
                                                INNER JOIN arResults R ON GEC.StuEnrollId = R.StuEnrollId
                                                INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                                INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                                  AND CSS.ReqId = GEC.EquivReqId
                                                INNER JOIN dbo.arStuEnrollments SE2 ON SE2.StuEnrollId = R.StuEnrollId
                                                INNER JOIN arStudent S ON SE2.StudentId = S.StudentId
                                                WHERE   S.StudentId IN ( SELECT DISTINCT
                                                                                StudentId
                                                                         FROM   dbo.arStuEnrollments SE1
                                                                         WHERE  SE1.StuEnrollId = @StuEnrollId )
                                              );

            
            PRINT 'Test Equiv Course GPA=';
            PRINT @EquivCourse_SA_GPA;
            PRINT @EquivCourse_SA_GPA_OverAll;
			                                   --SELECT   SUM(ISNULL(GSD.GPA,
                                               --               0.00))
                                               --FROM     arCourseEquivalent CE
                                               --         INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                               --         INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                               --         INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                               --         INNER JOIN dbo.arStuEnrollments SE ON R.StuEnrollId = SE.StuEnrollId
                                               --         INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                               --               AND CSS.ReqId = CS.ReqId
                                               --WHERE    StudentId IN (
                                               --         SELECT
                                               --               StudentId
                                               --         FROM  arStuEnrollments
                                               --         WHERE StuEnrollId = @StuEnrollId)
                                               --         AND R.GrdSysDetailId IS NOT NULL
                                               --         AND CSS.FinalGPA IS NOT NULL
                                          
			

            SET @cumSimpleCourseCredits = @cumSimpleCourseCredits;                                            --  + ISNULL(@EquivCourse_SA_CC, 0.00);
            SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits;                                              --  + ISNULL(@EquivCourse_SA_GPA, 0.00);

            SET @cumSimpleCourseCredits_OverAll = @cumSimpleCourseCredits_OverAll;                            --  + ISNULL(@EquivCourse_SA_CC_OverAll, 0.00);
            SET @cumSimple_GPA_Credits_OverAll = @cumSimple_GPA_Credits_OverAll;                              --  + ISNULL(@EquivCourse_SA_GPA_OverAll, 0.00);

            PRINT 'Cum Values=';
            PRINT @cumSimple_GPA_Credits;
			
            IF @cumSimpleCourseCredits >= 1
                BEGIN
                    SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    SET @cumSimpleGPA_OverAll = @cumSimple_GPA_Credits_OverAll / @cumSimpleCourseCredits_OverAll;
                END; 
			
            PRINT 'NWH New';	
            PRINT @cumSimpleCourseCredits;
            PRINT @cumSimple_GPA_Credits;
            PRINT @EquivCourse_SA_CC;
            PRINT @EquivCourse_SA_GPA;
--			print 'beforePPPRP'	
--			PRINT        @cumSimpleCourseCredits
--Print @cumSimple_GPA_Credits
--			Print '@cumSimpleGPA='
--			Print @cumSimpleGPA

			
            SET @cumWeightedGPA = 0;
            SET @cumCourseCredits = (
                                      SELECT    SUM(coursecredits)
                                      FROM      #CoursesNotRepeated
                                      WHERE     StuEnrollId IN ( @StuEnrollId )
                                                AND FinalGPA IS NOT NULL
                                    );
            SET @cumWeighted_GPA_Credits = (
                                             SELECT SUM(coursecredits * FinalGPA)
                                             FROM   #CoursesNotRepeated
                                             WHERE  StuEnrollId IN ( @StuEnrollId )
                                                    AND FinalGPA IS NOT NULL
                                           );
			

            DECLARE @cumCourseCredits_OverAll DECIMAL(18,2)
               ,@cumWeighted_GPA_Credits_OverAll DECIMAL(18,2);

            SET @cumCourseCredits_OverAll = (
                                              SELECT    SUM(coursecredits)
                                              FROM      #CoursesNotRepeated
                                              WHERE     StudentId IN ( SELECT   StudentId
                                                                       FROM     arStuEnrollments
                                                                       WHERE    StuEnrollId = @StuEnrollId )
                                                        AND FinalGPA IS NOT NULL
                                            );
            PRINT 'New values=';
            PRINT @cumWeighted_GPA_Credits;
            PRINT @cumCourseCredits_OverAll;

            SET @cumWeighted_GPA_Credits_OverAll = (
                                                     SELECT SUM(coursecredits * FinalGPA)
                                                     FROM   #CoursesNotRepeated
                                                     WHERE  StudentId IN ( SELECT   StudentId
                                                                           FROM     arStuEnrollments
                                                                           WHERE    StuEnrollId = @StuEnrollId )
                                                            AND FinalGPA IS NOT NULL
                                                   );


            DECLARE @EquivCourse_WGPA_CC1_OverAll DECIMAL(18,2)
               ,@EquivCourse_WGPA_GPA1_OverAll DECIMAL(18,2)
               ,@cumWeightedGPA_OverAll DECIMAL(18,2);
		
            SET @EquivCourse_WGPA_CC1 = (
                                          --      SELECT SUM(ISNULL(Credits, 0)) AS Credits
                                   --      FROM   arReqs
                                   --      WHERE  ReqId IN (
                                   --             SELECT  CE.EquivReqId
                                   --             FROM    arCourseEquivalent CE
                                   --                     INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                   --                     INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                   --                     INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                   --AND CSS.ReqId = CS.ReqId
                                   --             WHERE   R.StuEnrollId IN (
                                   --                     @StuEnrollId)
                                   --                     AND R.GrdSysDetailId IS NOT NULL
                                   --                     AND CSS.FinalGPA IS NOT NULL)
                                          SELECT    SUM(ISNULL(Credits,0)) AS Credits
                                          FROM      arReqs
                                          WHERE     ReqId IN ( SELECT   CE.EquivReqId
                                                               FROM     #tmpStudentsWhoHasGradedEquivalentCourse CE
                                                               WHERE    CE.StuEnrollId IN ( @StuEnrollId ) )
                                        );
		

            SET @EquivCourse_WGPA_GPA1 = (
                                           --SELECT    SUM(GSD.GPA * R1.Credits)
                                          --FROM      arCourseEquivalent CE
                                          --          INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                          --          INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                          --          INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                          --          INNER JOIN arReqs R1 ON R1.ReqId = CE.ReqId
                                          --          INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                          --                    AND CSS.ReqId = CS.ReqId
                                          --WHERE     R.StuEnrollId IN (
                                          --          @StuEnrollId)
                                          --          AND R.GrdSysDetailId IS NOT NULL
                                          --          AND CSS.FinalGPA IS NOT NULL
                                           SELECT   SUM(GSD.GPA * R1.Credits)
                                           FROM     #tmpStudentsWhoHasGradedEquivalentCourse GEC
                                           INNER JOIN arResults R ON GEC.StuEnrollId = R.StuEnrollId
                                           INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                           INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                             AND CSS.ReqId = GEC.EquivReqId
                                           INNER JOIN arReqs R1 ON R1.ReqId = GEC.EquivReqId
                                           WHERE    R.StuEnrollId IN ( @StuEnrollId )
                                         );

            SET @EquivCourse_WGPA_CC1_OverAll = (
                                                  SELECT    SUM(ISNULL(Credits,0)) AS Credits
                                                  FROM      arReqs
                                                  WHERE     ReqId IN ( SELECT DISTINCT
                                                                                CE.EquivReqId
                                                                       FROM     #tmpStudentsWhoHasGradedEquivalentCourse CE
                                                                       INNER JOIN arStuEnrollments SE ON CE.StuEnrollId = SE.StuEnrollId
                                                                       INNER JOIN arStudent S ON SE.StudentId = S.StudentId
                                                                       WHERE    S.StudentId IN ( SELECT DISTINCT
                                                                                                        StudentId
                                                                                                 FROM   dbo.arStuEnrollments SE1
                                                                                                 WHERE  SE1.StuEnrollId = @StuEnrollId ) )

                                                 --SELECT SUM(ISNULL(Credits, 0)) AS Credits
                                                 --FROM   arReqs
                                                 --WHERE  ReqId IN (
                                                 --       SELECT
                                                 --             CE.EquivReqId
                                                 --       FROM  arCourseEquivalent CE
                                                 --             INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                 --             INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                 --             INNER JOIN dbo.arStuEnrollments SE ON R.StuEnrollId = SE.StuEnrollId
                                                 --             INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                 --             AND CSS.ReqId = CS.ReqId
                                                 --       WHERE StudentId IN (
                                                 --             SELECT
                                                 --             StudentId
                                                 --             FROM
                                                 --             arStuEnrollments
                                                 --             WHERE
                                                 --             StuEnrollId = @StuEnrollId)
                                                 --             AND R.GrdSysDetailId IS NOT NULL
                                                 --             AND CSS.FinalGPA IS NOT NULL)
                                                );

            SET @EquivCourse_WGPA_GPA1_OverAll = (
                                                   SELECT   SUM(GSD.GPA * R1.Credits)
                                                   FROM     #tmpStudentsWhoHasGradedEquivalentCourse GEC
                                                   INNER JOIN arResults R ON GEC.StuEnrollId = R.StuEnrollId
                                                   INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                                   INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                                                     AND CSS.ReqId = GEC.EquivReqId
                                                   INNER JOIN arReqs R1 ON R1.ReqId = GEC.EquivReqId
                                                   INNER JOIN dbo.arStuEnrollments SE2 ON SE2.StuEnrollId = R.StuEnrollId
                                                   INNER JOIN arStudent S ON SE2.StudentId = S.StudentId
                                                   WHERE    S.StudentId IN ( SELECT DISTINCT
                                                                                    StudentId
                                                                             FROM   dbo.arStuEnrollments SE1
                                                                             WHERE  SE1.StuEnrollId = @StuEnrollId )
                                                 ); 
                                                  --SELECT    SUM(GSD.GPA
                                                  --            * R1.Credits)
                                                  --FROM      arCourseEquivalent CE
                                                  --          INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                  --          INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                  --          INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                                  --          INNER JOIN arReqs R1 ON R1.ReqId = CE.ReqId
                                                  --          INNER JOIN dbo.arStuEnrollments SE ON R.StuEnrollId = SE.StuEnrollId
                                                  --          INNER JOIN syCreditSummary CSS ON CSS.StuEnrollId = R.StuEnrollId
                                                  --            AND CSS.ReqId = CS.ReqId
                                                  --WHERE     StudentId IN (
                                                  --          SELECT
                                                  --            StudentId
                                                  --          FROM
                                                  --            arStuEnrollments
                                                  --          WHERE
                                                  --            StuEnrollId = @StuEnrollId)
                                                  --          AND R.GrdSysDetailId IS NOT NULL
                                                  --          AND CSS.FinalGPA IS NOT NULL
                                                

            PRINT 'B';
            PRINT @cumCourseCredits;
			--PRINT ISNULL(@EquivCourse_WGPA_CC1, 0.00)
            PRINT @EquivCourse_WGPA_GPA1;

            SET @cumCourseCredits = @cumCourseCredits;                                                        --  + ISNULL(@EquivCourse_WGPA_CC1, 0.00);
            SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits;                                          --  + ISNULL(@EquivCourse_WGPA_GPA1, 0.00);

            SET @cumCourseCredits_OverAll = @cumCourseCredits_OverAll;                                        --  + ISNULL(@EquivCourse_WGPA_CC1_OverAll, 0.00);
            SET @cumWeighted_GPA_Credits_OverAll = @cumWeighted_GPA_Credits_OverAll;                          --  + ISNULL(@EquivCourse_WGPA_GPA1_OverAll, 0.00);



            PRINT 'Before calc';
            PRINT @cumWeighted_GPA_Credits;
            PRINT @cumCourseCredits;

            IF @cumCourseCredits >= 1
                BEGIN
                    SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    SET @cumWeightedGPA_OverAll = @cumWeighted_GPA_Credits_OverAll / @cumCourseCredits_OverAll;
                END; 	


            PRINT @cumWeighted_GPA_Credits;

            --********************* Making SAP *******************************************************
            SELECT  @IsMakingSAP = ASCR1.IsMakingSAP
            FROM    (
                      SELECT    ROW_NUMBER() OVER ( PARTITION BY ASCR.StuEnrollId ORDER BY ASCR.DatePerformed DESC ) AS N
                               ,ISNULL(ASCR.IsMakingSAP,0) AS IsMakingSAP
                      FROM      arSAPChkResults AS ASCR
                      WHERE     ASCR.StuEnrollId = @StuEnrollId
                    ) AS ASCR1
            WHERE   ASCR1.N = 1;

             --********************* Update #getStudentGPAByProgramVersion *******************************************************

            UPDATE  #getStudentGPAByProgramVersion
            SET     ProgramVersion_SimpleGPA = @cumSimpleGPA
                   ,ProgramVersion_WeightedGPA = @cumWeightedGPA
                   ,OverAll_SimpleGPA = @cumSimpleGPA_OverAll
                   ,OverAll_WeightedGPA = @cumWeightedGPA_OverAll
                   ,IsMakingSAP = ISNULL(@IsMakingSAP,0)
            WHERE   StuEnrollID = @StuEnrollId;

            FETCH NEXT FROM getNodes_Cursor
				INTO @StuEnrollId;
        END;
    CLOSE getNodes_Cursor;
    DEALLOCATE getNodes_Cursor;

	--SELECT * FROM #CoursesNotRepeated



    CREATE TABLE #GPASummary
        (
         StuEnrollId UNIQUEIDENTIFIER
        ,TermId UNIQUEIDENTIFIER
        ,TermDescrip VARCHAR(100)
        ,TermSimpleGPA DECIMAL(18,2)
        ,TermWeightedGPA DECIMAL(18,2)
        ,ProgramVersionSimpleGPA DECIMAL(18,2)
        ,ProgramVersionWeightedGPA DECIMAL(18,2)
        ,StudentSimpleGPA DECIMAL(18,2)
        ,StudentWeightedGPA DECIMAL(18,2)
        ,IsMakingSAP BIT
        ,AcademicType NVARCHAR(50)
        );

	
    INSERT  INTO #GPASummary
            SELECT DISTINCT
                    SGT.StuEnrollId
                   ,SGT.TermId
                   ,TermDescrip
                   ,TermSimpleGPA
                   ,TermWeightedGPA
                   ,ProgramVersion_SimpleGPA AS ProgramVersionSimpleGPA
                   ,ProgramVersion_WeightedGPA AS ProgramVersionWeightedGPA
                   ,OverAll_SimpleGPA AS StudentSimpleGPA
                   ,OverAll_WeightedGPA AS StudentWeightedGPA
                   ,SGPV.IsMakingSAP AS IsMakingSAP
                   ,SGPV.AcademicType AS AcademicType
            FROM    #getStudentGPAbyTerms SGT
            INNER JOIN #getStudentGPAByProgramVersion SGPV ON SGT.StuEnrollId = SGPV.StuEnrollID;

    DROP TABLE #CoursesNotRepeated;
    DROP TABLE #getStudentGPAbyTerms;
    DROP TABLE #getStudentGPAByProgramVersion;
    DROP TABLE #tmpEquivalentCoursesTakenByStudent;
    DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourse;
    DROP TABLE #tmpEquivalentCoursesTakenByStudentInTerm;
    DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourseInTerm;

--''''''''''''''''''''''''''''' End: This section of the stored proc calculates the GPA '''''''''''''''''''''''''''''''

    DECLARE @GradesFormat AS VARCHAR(50);
    DECLARE @GPAMethod AS VARCHAR(50);
    DECLARE @GradeBookAt AS VARCHAR(50);
    DECLARE @StuEnrollCampusId AS UNIQUEIDENTIFIER;
    DECLARE @TermStartDate1 AS DATETIME; 
    DECLARE @Score AS DECIMAL(18,2);
    DECLARE @GrdCompDescrip AS VARCHAR(50);

    DECLARE @curId AS UNIQUEIDENTIFIER;
    DECLARE @curReqId AS UNIQUEIDENTIFIER;
    DECLARE @curStuEnrollId AS UNIQUEIDENTIFIER;
    DECLARE @curDescrip AS VARCHAR(50);
    DECLARE @curNumber AS INT;
    DECLARE @curGrdComponentTypeId AS INT;
    DECLARE @curMinResult AS DECIMAL(18,2); 
    DECLARE @curGrdComponentDescription AS VARCHAR(50);   
    DECLARE @curClsSectionId AS UNIQUEIDENTIFIER;
    DECLARE @curRownumber AS INT;
			
    SET @StuEnrollCampusId = COALESCE((
                                        SELECT TOP 1
                                                ASE.CampusId
                                        FROM    arStuEnrollments AS ASE
                                        WHERE   ASE.StuEnrollId IN ( SELECT Val
                                                                     FROM   MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                                      ),NULL);
    SET @GradesFormat = dbo.GetAppSettingValueByKeyName('GradesFormat',@StuEnrollCampusId);
    IF ( @GradesFormat IS NOT NULL )
        BEGIN
            SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
        END;
    SET @GPAMethod = dbo.GetAppSettingValueByKeyName('GPAMethod',@StuEnrollCampusId);
    IF ( @GPAMethod IS NOT NULL )
        BEGIN
            SET @GPAMethod = LOWER(LTRIM(RTRIM(@GPAMethod)));
        END;
    SET @GradeBookAt = dbo.GetAppSettingValueByKeyName('GradeBookWeightingLevel',@StuEnrollCampusId);
    IF ( @GradeBookAt IS NOT NULL )
        BEGIN
            SET @GradeBookAt = LOWER(LTRIM(RTRIM(@GradeBookAt)));
        END;

 

    SELECT  dt1.PrgVerId
           ,dt1.PrgVerDescrip
           ,dt1.PrgVersionTrackCredits
           ,dt1.TermId
           ,dt1.TermDescription
           ,dt1.TermStartDate
           ,dt1.TermEndDate
           ,dt1.CourseId
           ,dt1.CouseStartDate
           ,dt1.CourseCode
           ,dt1.CourseDescription
           ,dt1.CourseCodeDescription
           ,dt1.CourseCredits
           ,dt1.CourseFinAidCredits
           ,dt1.MinVal
           ,dt1.CourseScore
           ,dt1.StuEnrollId
           ,dt1.CreditsAttempted
           ,dt1.CreditsEarned
           ,dt1.Completed
           ,dt1.CurrentScore
           ,dt1.CurrentGrade
           ,dt1.FinalScore
           ,dt1.FinalGrade
           ,dt1.FinalGPA
           ,dt1.ScheduleDays
           ,dt1.ActualDay
		--, dt1.CampusId
		-- dt1.CampDescrip
           ,dt1.GrdBkWgtDetailsCount
           ,dt1.ClockHourProgram
           ,dt1.GradesFormat
           ,dt1.GPAMethod
           ,GS.TermSimpleGPA
           ,GS.TermWeightedGPA
           ,GS.ProgramVersionSimpleGPA
           ,GS.ProgramVersionWeightedGPA
           ,GS.StudentSimpleGPA
           ,GS.StudentWeightedGPA
           ,GS.IsMakingSAP
           ,GS.AcademicType
           ,dt1.RowNumber
    FROM    (
              SELECT    dt.PrgVerId
                       ,dt.PrgVerDescrip
                       ,dt.PrgVersionTrackCredits
                       ,dt.TermId
                       ,dt.TermDescription
                       ,dt.TermStartDate
                       ,dt.TermEndDate
                       ,dt.CourseId
                       ,dt.CouseStartDate
                       ,dt.CourseCode
                       ,dt.CourseDescription
                       ,dt.CourseCodeDescription
                       ,dt.CourseCredits
                       ,dt.CourseFinAidCredits
                       ,dt.MinVal
                       ,dt.CourseScore
                       ,dt.StuEnrollId
                       ,dt.CreditsAttempted
                       ,dt.CreditsEarned
                       ,dt.Completed
                       ,dt.CurrentScore
                       ,dt.CurrentGrade
                       ,dt.FinalScore
                       ,dt.FinalGrade
                       ,dt.FinalGPA
                       ,dt.ScheduleDays
                       ,dt.ActualDay
                       ,dt.GrdBkWgtDetailsCount
                       ,dt.ClockHourProgram
                       ,@GradesFormat AS GradesFormat
                       ,@GPAMethod AS GPAMethod
                       ,ROW_NUMBER() OVER ( PARTITION BY TermId,CourseId ORDER BY TermStartDate, TermEndDate, TermDescription, CourseDescription ) AS RowNumber
              FROM      (
                          SELECT DISTINCT
                                    PV.PrgVerId AS PrgVerId
                                   ,PV.PrgVerDescrip AS PrgVerDescrip
                                   ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                         ELSE 0
                                    END AS PrgVersionTrackCredits
                                   ,T.TermId AS TermId
                                   ,T.TermDescrip AS TermDescription
                                   ,T.StartDate AS TermStartDate
                                   ,T.EndDate AS TermEndDate
                                   ,CS.ReqId AS CourseId
                                   ,CS.StartDate AS CouseStartDate
                                   ,R.Code AS CourseCode
                                   ,R.Descrip AS CourseDescription
                                   ,'(' + R.Code + ') ' + R.Descrip AS CourseCodeDescription
                                   ,R.Credits AS CourseCredits
                                   ,R.FinAidCredits AS CourseFinAidCredits
                                   ,(
                                      SELECT    MIN(MinVal)
                                      FROM      arGradeScaleDetails GCD
                                               ,arGradeSystemDetails GSD
                                      WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                                AND GSD.IsPass = 1
                                                AND GCD.GrdScaleId = CS.GrdScaleId
                                    ) AS MinVal
                                   ,RES.Score AS CourseScore
                                   ,SE.StuEnrollId AS StuEnrollId
                                   ,ISNULL(SCS.CreditsAttempted,0) AS CreditsAttempted
                                   ,ISNULL(SCS.CreditsEarned,0) AS CreditsEarned
                                   ,SCS.Completed AS Completed
                                   ,SCS.CurrentScore AS CurrentScore
                                   ,SCS.CurrentGrade AS CurrentGrade
                                   ,SCS.FinalScore AS FinalScore
                                   ,SCS.FinalGrade AS FinalGrade
                                   ,SCS.FinalGPA AS FinalGPA
						  --, SSAS.ScheduledDays AS ScheduleDays
						  --, SSAS.ActualDays AS ActualDay
                                   ,R.Hours AS ScheduleDays
                                   ,CASE WHEN (
                                                ISNULL(SCS.CreditsEarned,0) > 0
                                                OR (
                                                     SELECT IsCreditsEarned
                                                     FROM   dbo.arGradeSystemDetails GSD
                                                     INNER JOIN dbo.arGradeSystems GS ON GSD.GrdSystemId = GS.GrdSystemId
                                                     INNER JOIN arPrgVersions PV1 ON PV1.GrdSystemId = GS.GrdSystemId
                                                     WHERE  PV1.PrgVerId = PV.PrgVerId
                                                            AND GSD.Grade = SCS.FinalGrade
                                                   ) = 1
                                              ) THEN R.Hours
                                         ELSE 0
                                    END AS ActualDay
                                   ,(
                                      SELECT    COUNT(*) AS GrdBkWgtDetailsCount
                                      FROM      arGrdBkResults
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND ClsSectionId = RES.TestId
                                    ) AS GrdBkWgtDetailsCount
                                   ,CASE WHEN P.ACId = 5 THEN 'True'
                                         ELSE 'False'
                                    END AS ClockHourProgram
                          FROM      arClassSections CS
                          INNER JOIN arResults RES ON RES.TestId = CS.ClsSectionId
                          INNER JOIN arStuEnrollments SE ON RES.StuEnrollId = SE.StuEnrollId
                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                          INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                          INNER JOIN arTerm T ON CS.TermId = T.TermId
                          INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                          LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                           AND SCS.TermId = T.TermId
                                                           AND SCS.ReqId = R.ReqId
                                                           AND SCS.ClsSectionId = CS.ClsSectionId
                          LEFT JOIN syStudentAttendanceSummary AS SSAS ON SSAS.ClsSectionId = CS.ClsSectionId
                                                                          AND SSAS.StuEnrollId = SE.StuEnrollId
                          WHERE     SE.StuEnrollId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                                    AND (
                                          @TermId IS NULL
                                          OR T.TermId = @TermId
                                        )
							--AND R.IsAttendanceOnly = 0
                            -- Nota: 2016-01-05 JT: As per DE12275 Case 986512 Do not check for Completed but check for FinalsGPA is not null
                            --AND SCS.Completed = 1  
                            --AND SCS.FinalGPA IS NOT NULL
							-- Show the courses only if it has a grade   Letter or numenic always have GPAFinal not null 
							--AND (ISNULL(SCS.FinalScore, 0) > 0 OR ISNULL(SCS.FinalGrade, 0)
                                    AND (
                                          RES.GrdSysDetailId IS NOT NULL
                                          OR RES.Score IS NOT NULL
                                        )
                          UNION
                          SELECT DISTINCT
                                    PV.PrgVerId AS PrgVerId
                                   ,PV.PrgVerDescrip AS PrgVerDescrip
                                   ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                         ELSE 0
                                    END AS PrgVersionTrackCredits
                                   ,T.TermId AS TermId
                                   ,T.TermDescrip AS TermDescription
                                   ,T.StartDate AS TermStartDate
                                   ,T.EndDate AS TermEndDate
                                   ,GBCR.ReqId AS CourseId
                                   ,T.StartDate AS CouseStartDate   -- tranfered
                                   ,R.Code AS CourseCode
                                   ,R.Descrip AS CourseDescrip
                                   ,'(' + R.Code + ') ' + R.Descrip AS CourseCodeDescrip
                                   ,R.Credits AS CourseCredits
                                   ,R.FinAidCredits AS CourseFinAidCredits
                                   ,(
                                      SELECT    MIN(MinVal)
                                      FROM      arGradeScaleDetails GCD
                                               ,arGradeSystemDetails GSD
                                      WHERE     GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                                AND GSD.IsPass = 1
                                    ) AS MinVal
                                   ,ISNULL(GBCR.Score,0)
                                   ,SE.StuEnrollId
                                   ,ISNULL(SCS.CreditsAttempted,0) AS CreditsAttempted
                                   ,ISNULL(SCS.CreditsEarned,0) AS CreditsEarned
                                   ,SCS.Completed AS Completed
                                   ,SCS.CurrentScore AS CurrentScore
                                   ,SCS.CurrentGrade AS CurrentGrade
                                   ,SCS.FinalScore AS FinalScore
                                   ,SCS.FinalGrade AS FinalGrade
                                   ,SCS.FinalGPA AS FinalGPA
                                   ,R.Hours AS ScheduleDays
                                   ,CASE WHEN (
                                                ISNULL(SCS.CreditsEarned,0) > 0
                                                OR (
                                                     SELECT IsCreditsEarned
                                                     FROM   dbo.arGradeSystemDetails GSD
                                                     INNER JOIN dbo.arGradeSystems GS ON GSD.GrdSystemId = GS.GrdSystemId
                                                     INNER JOIN arPrgVersions PV1 ON PV1.GrdSystemId = GS.GrdSystemId
                                                     WHERE  PV1.PrgVerId = PV.PrgVerId
                                                            AND GSD.Grade = SCS.FinalGrade
                                                   ) = 1
                                              ) THEN R.Hours
                                         ELSE 0
                                    END AS ActualDay
						  --, NULL AS ScheduleDays -- SSAS.ScheduledDays AS ScheduleDays
						  --, NULL AS ActualDays   --SSAS.ActualDays    AS ActualDays
                                   ,(
                                      SELECT    COUNT(*) AS GrdBkWgtDetailsCount
                                      FROM      arGrdBkConversionResults
                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                AND TermId = GBCR.TermId
                                                AND ReqId = GBCR.ReqId
                                    ) AS GrdBkWgtDetailsCount
                                   ,CASE WHEN P.ACId = 5 THEN 'True'
                                         ELSE 'False'
                                    END AS ClockHourProgram
                          FROM      arTransferGrades GBCR
                          INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                          INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                          INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                          INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                          LEFT JOIN syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                           AND T.TermId = SCS.TermId
                                                           AND R.ReqId = SCS.ReqId
                          WHERE     SE.StuEnrollId IN ( SELECT  Val
                                                        FROM    MultipleValuesForReportParameters(@StuEnrollIdList,',',1) )
                                    AND (
                                          @TermId IS NULL
                                          OR T.TermId = @TermId
                                        )
							--AND R.IsAttendanceOnly = 0
                            -- Nota: 2016-01-05 JT: As per DE12275 Case 986512 Do not check for Completed but check for FinalsGPA is not null
                            --AND SCS.Completed = 1  
                            --AND SCS.FinalGPA IS NOT NULL
							--AND SCS.Completed = 1
							-- Show the courses only if it has a grade   Letter or numenic always have GPAFinal not null 
							--AND (ISNULL(SCS.FinalScore, 0) > 0 OR ISNULL(SCS.FinalGrade, 0)
                                    AND ( GBCR.GrdSysDetailId IS NOT NULL )
                        ) dt
            ) dt1
    INNER JOIN #GPASummary GS ON dt1.StuEnrollId = GS.StuEnrollId
                                 AND dt1.TermId = GS.TermId
	--WHERE   dt1.RowNumber = 1
    ORDER BY CASE WHEN @TermId IS NOT NULL THEN ( RANK() OVER ( ORDER BY dt1.TermStartDate, dt1.TermDescription, dt1.CourseDescription ) )
                  ELSE ( RANK() OVER ( ORDER BY dt1.CouseStartDate, dt1.CourseDescription ) )
             END;

--Drop table #CoursesNotRepeated
--	Drop table #getStudentGPAbyTerms
--	Drop table #getStudentGPAByProgramVersion
    DROP TABLE #GPASummary;

-- =========================================================================================================
-- END  --  Usp_TR_Sub6_Courses 
-- =========================================================================================================
GO

-- =========================================================================================================
-- DE12938 Normal - Advantage - Unilatina - Error when trying to print a transcript - 1022271
-- JTorres 2016-09 - 12
-- =========================================================================================================

------------------------------------------------------------------------------------------------------------------------
--DE13056:Normal - Advantage - MAU - Class roster report displaying students twice - 1030452
-------------------------------------------------------------------------------------------------------------------------
/****** Object:  UserDefinedFunction [dbo].[GetProgramVersions]    Script Date: 9/16/2016 3:05:47 PM ******/
IF EXISTS ( SELECT  *
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[dbo].[GetProgramVersions]')
                    AND type IN ( N'FN',N'IF',N'TF',N'FS',N'FT' ) )
    DROP FUNCTION dbo.GetProgramVersions;
GO

/****** Object:  UserDefinedFunction [dbo].[GetProgramVersions]    Script Date: 9/16/2016 3:05:47 PM ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO


CREATE FUNCTION dbo.GetProgramVersions
    (
     @studentid UNIQUEIDENTIFIER
    ,@clssectionid UNIQUEIDENTIFIER
	)
RETURNS VARCHAR(1000)
AS
    BEGIN
        DECLARE @ReturnValue VARCHAR(1000)= '';
        DECLARE @prgverdescrip VARCHAR(200);

        DECLARE mycursor CURSOR
        FOR
            SELECT  PrgVerDescrip
            FROM    arprgversions pv
                   ,dbo.arStuEnrollments se
                   ,dbo.arStudent st
                   ,arResults rs
            WHERE   pv.PrgVerId = se.PrgVerId
                    AND se.StudentId = st.StudentId
                    AND se.StuEnrollId = rs.StuEnrollId
                    AND st.StudentId = @studentid
                    AND rs.TestId = @clssectionid;    
		
        OPEN mycursor;
		
        FETCH NEXT FROM mycursor
		INTO @prgverdescrip;
		
        WHILE @@FETCH_STATUS = 0
            BEGIN
                SET @ReturnValue += @prgverdescrip + ', ';

                FETCH NEXT FROM mycursor
				INTO @prgverdescrip;
            END;			

        CLOSE mycursor;
        DEALLOCATE mycursor;

		--Remove the last comma
        SET @ReturnValue = LEFT(RTRIM(@ReturnValue),LEN(RTRIM(@ReturnValue)) - 1);

        RETURN @ReturnValue;
		
    END;



GO
------------------------------------------------------------------------------------------------------------------------
--END DE13056:Normal - Advantage - MAU - Class roster report displaying students twice - 1030452
-------------------------------------------------------------------------------------------------------------------------











