IF (
     NOT EXISTS ( SELECT    *
                  FROM      INFORMATION_SCHEMA.TABLES
                  WHERE     TABLE_SCHEMA = 'dbo'
                            AND TABLE_NAME = 'syVersionHistory' )
   )
    BEGIN
    --Do Stuff
        BEGIN TRANSACTION Version;

	
        BEGIN TRY
            CREATE TABLE syVersionHistory
                (
                 Id INT PRIMARY KEY IDENTITY
                ,Major INT
                ,Minor INT
                ,Build INT
				,Revision INT
                ,VersionBegin DATETIME
                ,VersionEnd DATETIME
                ,Description VARCHAR(100)
                ,ModUser VARCHAR(50)
                );

            INSERT  INTO dbo.syVersionHistory
                    (
                     Major
                    ,Minor
                    ,Build
					,Revision
                    ,VersionBegin
                    ,VersionEnd
                    ,Description
                    ,ModUser
	                )
            VALUES  (      
                     @Major  -- Major - int
                    ,@Minor -- Minor - int
                    ,@Build  -- Build - int
                    ,@Revision  -- Revision - int
                    ,GETDATE()  -- VersionBegin - datetime
                    ,NULL  -- VersionEnd - datetime
                    ,@Description  -- Description - varchar(100)
                    ,@User  -- ModUser - varchar(50)
	                );
            COMMIT TRAN Version;
        END TRY
        BEGIN CATCH
            ROLLBACK TRANSACTION Version;
			RAISERROR('Version Table can not be created',16,1)
        END CATCH; 
    END;