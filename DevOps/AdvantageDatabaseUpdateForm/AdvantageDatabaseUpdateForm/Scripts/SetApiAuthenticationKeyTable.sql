﻿IF NOT EXISTS ( SELECT  *
                FROM    dbo.ApiAuthenticationKey
                WHERE   TenantId = @TenantId )
    BEGIN

        INSERT  INTO dbo.ApiAuthenticationKey
                ( TenantId,[Key],CreatedDate )
        VALUES  ( @TenantId -- TenantId - int
                  ,@ApiKey  -- Key - varchar(80)
                  ,GETDATE()  -- CreatedDate - smalldatetime
                  );
    END;