--=================================================================================================
-- JOB_AttendanceJob.sql
--=================================================================================================
USE msdb;

-- @DatabaseName and @JobStringName are parameters supplied by Caller (Advantage DB Migration tool)
-- DECLARE @DatabaseName AS NVARCHAR(500);
-- DECLARE @JobStringName AS NVARCHAR(500);
DECLARE @JobName AS NVARCHAR(500);
--SET @DatabaseName = 'A1_Starter'

SET @JobName = @DatabaseName + '_' + @JobStringName;

BEGIN TRANSACTION;
DECLARE @ReturnCode INT;
SELECT @ReturnCode = 0;

PRINT N'If Exists Job ''' + @JobName + ''', Delete it ';
IF EXISTS (
          SELECT 1
          FROM   msdb.dbo.sysjobs_view
          WHERE  name = @JobName
          )
    BEGIN
        PRINT N'    Deleting Job ''' + @JobName + ''' ';
        EXEC msdb.dbo.sp_delete_job @job_name = @JobName
                                   ,@delete_unused_schedule = 1;
    END;
IF (
   @@ERROR <> 0
   OR @ReturnCode <> 0
   )
    BEGIN
        GOTO QuitWithRollback;
    END;

IF NOT EXISTS (
              SELECT name
              FROM   msdb.dbo.syscategories
              WHERE  name = N'[Uncategorized (Local)]'
                     AND category_class = 1
              )
    BEGIN
        EXEC @ReturnCode = msdb.dbo.sp_add_category @class = N'JOB'
                                                   ,@type = N'LOCAL'
                                                   ,@name = N'[Uncategorized (Local)]';
        IF (
           @@ERROR <> 0
           OR @ReturnCode <> 0
           )
            BEGIN
                GOTO QuitWithRollback;
            END;
    END;

DECLARE @jobId BINARY(16);
EXEC @ReturnCode = msdb.dbo.sp_add_job @job_name = @JobName
                                      ,@enabled = 1
                                      ,@notify_level_eventlog = 0
                                      ,@notify_level_email = 0
                                      ,@notify_level_netsend = 0
                                      ,@notify_level_page = 0
                                      ,@delete_level = 0
                                      ,@description = N'No description available.'
                                      ,@category_name = N'[Uncategorized (Local)]'
                                      ,@owner_login_name = N'sa'
                                      ,@job_id = @jobId OUTPUT;
IF (
   @@ERROR <> 0
   OR @ReturnCode <> 0
   )
    BEGIN
        GOTO QuitWithRollback;
    END;

EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id = @jobId
                                          ,@step_name = N'Step 01 - Create table or Trunk table syStudentAttendanceSummary'
                                          ,@step_id = 1
                                          ,@cmdexec_success_code = 0
                                          ,@on_success_action = 3
                                          ,@on_success_step_id = 0
                                          ,@on_fail_action = 2
                                          ,@on_fail_step_id = 0
                                          ,@retry_attempts = 0
                                          ,@retry_interval = 0
                                          ,@os_run_priority = 0
                                          ,@subsystem = N'TSQL'
                                          ,@command = N'
-- Step 01  --  Create table or Trunk table syStudentAttendanceSummary
--
EXECUTE [dbo].[USP_AT_Step01_CreateTableSyStudentAttendanceSummary]
GO
'
                                          ,@database_name = @DatabaseName
                                          ,@flags = 0;
IF (
   @@ERROR <> 0
   OR @ReturnCode <> 0
   )
    BEGIN
        GOTO QuitWithRollback;
    END;

EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id = @jobId
                                          ,@step_name = N'Step 02 - InsertAttendance_Day_PresentAbsent'
                                          ,@step_id = 2
                                          ,@cmdexec_success_code = 0
                                          ,@on_success_action = 3
                                          ,@on_success_step_id = 0
                                          ,@on_fail_action = 2
                                          ,@on_fail_step_id = 0
                                          ,@retry_attempts = 0
                                          ,@retry_interval = 0
                                          ,@os_run_priority = 0
                                          ,@subsystem = N'TSQL'
                                          ,@command = N'
-- Step 02 --  InsertAttendance_Day_PresentAbsent  -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'') 
--         --  TrackSapAttendance = ''byday''
--         --  @StuEnrollIdList = NULL   for ALL  enrollments
EXECUTE [dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]  @StuEnrollIdList = NULL
GO
'
                                          ,@database_name = @DatabaseName
                                          ,@flags = 0;
IF (
   @@ERROR <> 0
   OR @ReturnCode <> 0
   )
    BEGIN
        GOTO QuitWithRollback;
    END;

EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id = @jobId
                                          ,@step_name = N'Step 03 - InsertAttendance_Class_PresentAbsent'
                                          ,@step_id = 3
                                          ,@cmdexec_success_code = 0
                                          ,@on_success_action = 3
                                          ,@on_success_step_id = 0
                                          ,@on_fail_action = 2
                                          ,@on_fail_step_id = 0
                                          ,@retry_attempts = 0
                                          ,@retry_interval = 0
                                          ,@os_run_priority = 0
                                          ,@subsystem = N'TSQL'
                                          ,@command = N'
-- Step 03 --  InsertAttendance_Class_PresentAbsent   -- UnitTypeDescrip = (''None'', ''Present Absent'') 
--         --  TrackSapAttendance = ''byclass''
--         --  @StuEnrollIdList = NULL   for ALL  enrollments
EXECUTE [dbo].[USP_AT_Step03_InsertAttendance_Class_PresentAbsent]  @StuEnrollIdList = NULL
GO
'
                                          ,@database_name = @DatabaseName
                                          ,@flags = 0;
IF (
   @@ERROR <> 0
   OR @ReturnCode <> 0
   )
    BEGIN
        GOTO QuitWithRollback;
    END;

EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id = @jobId
                                          ,@step_name = N'Step 04 -InsertAttendance_Class_MinutesClockHours'
                                          ,@step_id = 4
                                          ,@cmdexec_success_code = 0
                                          ,@on_success_action = 3
                                          ,@on_success_step_id = 0
                                          ,@on_fail_action = 2
                                          ,@on_fail_step_id = 0
                                          ,@retry_attempts = 0
                                          ,@retry_interval = 0
                                          ,@os_run_priority = 0
                                          ,@subsystem = N'TSQL'
                                          ,@command = N'
-- Step 04 --  InsertAttendance_Class_MinutesClockHours  -- UnitTypeDescrip = ( ''Minutes'',''Clock Hours'' )
--         --  TrackSapAttendance = ''byclass''
--         --  @StuEnrollIdList = NULL   for ALL  enrollments
EXECUTE [dbo].[USP_AT_Step04_InsertAttendance_Class_MinutesClockHours]  @StuEnrollIdList = NULL
GO'
                                          ,@database_name = @DatabaseName
                                          ,@flags = 0;
IF (
   @@ERROR <> 0
   OR @ReturnCode <> 0
   )
    BEGIN
        GOTO QuitWithRollback;
    END;

EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id = @jobId
                                          ,@step_name = N'Step 05 - InsertAttendance_Day_Minutes'
                                          ,@step_id = 5
                                          ,@cmdexec_success_code = 0
                                          ,@on_success_action = 3
                                          ,@on_success_step_id = 0
                                          ,@on_fail_action = 2
                                          ,@on_fail_step_id = 0
                                          ,@retry_attempts = 0
                                          ,@retry_interval = 0
                                          ,@os_run_priority = 0
                                          ,@subsystem = N'TSQL'
                                          ,@command = N'
-- Step 05 --  InsertAttendance_Day_Minutes  -- UnitTypeDescrip = (''Minutes'') 
--         --  TrackSapAttendance = ''byday''
--         --  @StuEnrollIdList = NULL   for ALL  enrollments
EXECUTE [dbo].[USP_AT_Step05_InsertAttendance_Day_Minutes]  @StuEnrollIdList = NULL
GO'
                                          ,@database_name = @DatabaseName
                                          ,@flags = 0;
IF (
   @@ERROR <> 0
   OR @ReturnCode <> 0
   )
    BEGIN
        GOTO QuitWithRollback;
    END;

EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id = @jobId
                                          ,@step_name = N'Step 06 - InsertAttendance_Class_ClockHour'
                                          ,@step_id = 6
                                          ,@cmdexec_success_code = 0
                                          ,@on_success_action = 1
                                          ,@on_success_step_id = 0
                                          ,@on_fail_action = 2
                                          ,@on_fail_step_id = 0
                                          ,@retry_attempts = 0
                                          ,@retry_interval = 0
                                          ,@os_run_priority = 0
                                          ,@subsystem = N'TSQL'
                                          ,@command = N'
-- Step 06 --  InsertAttendance_Class_ClockHour  -- UnitTypeDescrip =  ''Clock Hours'' 
--         --  TrackSapAttendance = ''byclass''
--         --  @StuEnrollIdList = NULL   for ALL  enrollments
EXECUTE [dbo].[USP_AT_Step06_InsertAttendance_Class_ClockHour]  @StuEnrollIdList = NULL
GO'
                                          ,@database_name = @DatabaseName
                                          ,@flags = 0;
IF (
   @@ERROR <> 0
   OR @ReturnCode <> 0
   )
    BEGIN
        GOTO QuitWithRollback;
    END;

EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId
                                         ,@start_step_id = 1;
IF (
   @@ERROR <> 0
   OR @ReturnCode <> 0
   )
    BEGIN
        GOTO QuitWithRollback;
    END;

EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id = @jobId
                                              ,@name = @JobName
                                              ,@enabled = 1
                                              ,@freq_type = 4
                                              ,@freq_interval = 1
                                              ,@freq_subday_type = 1
                                              ,@freq_subday_interval = 0
                                              ,@freq_relative_interval = 0
                                              ,@freq_recurrence_factor = 0
                                              ,@active_start_date = 20150106
                                              ,@active_end_date = 99991231
                                              ,@active_start_time = 20000
                                              ,@active_end_time = 235959
                                              ,@schedule_uid = N'8bc1b4f3-a1bc-4345-88bb-f3383eea46f6';
IF (
   @@ERROR <> 0
   OR @ReturnCode <> 0
   )
    BEGIN
        GOTO QuitWithRollback;
    END;

EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId
                                            ,@server_name = N'(local)';
IF (
   @@ERROR <> 0
   OR @ReturnCode <> 0
   )
    BEGIN
        GOTO QuitWithRollback;
    END;

COMMIT TRANSACTION;
GOTO EndSave;

QuitWithRollback:
IF (@@TRANCOUNT > 0)
    BEGIN
        ROLLBACK TRANSACTION;
    END;

EndSave:
--=================================================================================================
-- JOB_AttendanceJob.sql
--=================================================================================================

