USE [TenantAuthDB];
GO

IF EXISTS
(
    SELECT NULL
    FROM sys.extended_properties
    WHERE [major_id] = OBJECT_ID('WapiLog')
          AND [name] = N'MS_Description'
          AND [minor_id] = 0
)
BEGIN
    EXEC sys.sp_dropextendedproperty @name = N'MS_Description',
                                     @level0type = N'SCHEMA',
                                     @level0name = N'dbo',
                                     @level1type = N'TABLE',
                                     @level1name = N'WapiLog';
END;
GO
IF EXISTS
(
    SELECT NULL
    FROM sys.extended_properties
    WHERE [major_id] = OBJECT_ID('WAPITenantCompanySecret')
          AND [name] = N'MS_Description'
          AND [minor_id] = 0
)
BEGIN
    EXEC sys.sp_dropextendedproperty @name = N'MS_Description',
                                     @level0type = N'SCHEMA',
                                     @level0name = N'dbo',
                                     @level1type = N'TABLE',
                                     @level1name = N'WAPITenantCompanySecret';
END;
GO

IF EXISTS
(
    SELECT *
    FROM dbo.sysobjects
    WHERE id = OBJECT_ID('[WapiLog]')
          AND OBJECTPROPERTY(id, 'IsUserTable') = 1
)
    ALTER TABLE [dbo].[WapiLog]
    DROP CONSTRAINT [DF__syWapiLog__IsOk__6E565CE8];
GO

IF EXISTS
(
    SELECT *
    FROM dbo.sysobjects
    WHERE id = OBJECT_ID('[WapiLog]')
          AND OBJECTPROPERTY(id, 'IsUserTable') = 1
)
    ALTER TABLE [dbo].[WapiLog]
    DROP CONSTRAINT [DF__syWapiLog__Numbe__6D6238AF];
GO

IF EXISTS
(
    SELECT *
    FROM dbo.sysobjects
    WHERE id = OBJECT_ID('[WAPITenantCompanySecret]')
          AND OBJECTPROPERTY(id, 'IsUserTable') = 1
)
    ALTER TABLE [dbo].[WAPITenantCompanySecret]
    DROP CONSTRAINT [FK_WAPITenantCompanySecret_Tenant];
GO

IF EXISTS
(
    SELECT *
    FROM dbo.sysobjects
    WHERE id = OBJECT_ID('[WAPITenantCompanySecret]')
          AND OBJECTPROPERTY(id, 'IsUserTable') = 1
)
    DROP TABLE [dbo].[WAPITenantCompanySecret];
GO

/****** Object:  Table [dbo].[WapiLog]    Script Date: 9/15/2014 3:33:28 PM ******/
IF EXISTS
(
    SELECT *
    FROM dbo.sysobjects
    WHERE id = OBJECT_ID('[WapiLog]')
          AND OBJECTPROPERTY(id, 'IsUserTable') = 1
)
    DROP TABLE [dbo].[WapiLog];
GO

/****** Object:  Table [dbo].[WapiLog]    Script Date: 9/15/2014 3:33:28 PM ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

SET ANSI_PADDING ON;
GO

CREATE TABLE [dbo].[WapiLog]
(
    [id] [BIGINT] IDENTITY(1, 1) NOT NULL,
    [TenantName] [VARCHAR](50) NULL,
    [DateMod] [DATETIME] NOT NULL,
    [NumberOfRecords] [INT] NOT NULL,
    [Company] [VARCHAR](50) NOT NULL,
    [ServiceInvoqued] [VARCHAR](30) NOT NULL,
    [OperationDescription] [VARCHAR](50) NULL,
    [IsOk] [BIT] NOT NULL,
    [Comment] [VARCHAR](100) NULL,
    CONSTRAINT [PK_syWebApiLogs]
        PRIMARY KEY CLUSTERED ([id] ASC)
        WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
              ALLOW_PAGE_LOCKS = ON
             ) ON [PRIMARY],
    CONSTRAINT [UQ_syWebApiLogs_id]
        UNIQUE NONCLUSTERED ([id] ASC)
        WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
              ALLOW_PAGE_LOCKS = ON
             ) ON [PRIMARY]
) ON [PRIMARY];

GO

SET ANSI_PADDING OFF;
GO

ALTER TABLE [dbo].[WapiLog]
ADD CONSTRAINT [DF__syWapiLog__Numbe__6D6238AF]
DEFAULT ((0)) FOR [NumberOfRecords];
GO

ALTER TABLE [dbo].[WapiLog]
ADD CONSTRAINT [DF__syWapiLog__IsOk__6E565CE8]
DEFAULT ((0)) FOR [IsOk];
GO

EXEC sys.sp_addextendedproperty @name = N'MS_Description',
                                @value = N'This table store the data log for Web API Operations
Fields Descriptions:
UserMod:                         Username
DatMod:                           Create data time
NumberOfRecords:        Number of record that were operated (if exists)
CompanyCode:              The code of the company that originate the log
ServiceCode:                   The service that originate the log (SETTING, LOGIN, GSTUDENTS)
OperationCode:              The type of operation involved (READ, PUSH, ETC)
IsOk:                                  If the operation was ERROR(0) or OK(1) 
		  ',
                                @level0type = N'SCHEMA',
                                @level0name = N'dbo',
                                @level1type = N'TABLE',
                                @level1name = N'WapiLog';
GO


/****** Object:  Table [dbo].[WAPITenantCompanySecret]    Script Date: 9/15/2014 3:37:31 PM ******/

CREATE TABLE [dbo].[WAPITenantCompanySecret]
(
    [Id] [INT] IDENTITY(1, 1) NOT NULL,
    [ExternalCompanyCode] [VARCHAR](50) NOT NULL,
    [TenantId] [INT] NOT NULL,
    CONSTRAINT [PK_WAPITenantCompanySecret]
        PRIMARY KEY CLUSTERED ([Id] ASC)
        WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
              ALLOW_PAGE_LOCKS = ON
             ) ON [PRIMARY],
    CONSTRAINT [UQ_WAPITenantCompanySecret_COMPANY_SECRET]
        UNIQUE NONCLUSTERED ([ExternalCompanyCode] ASC)
        WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
              ALLOW_PAGE_LOCKS = ON
             ) ON [PRIMARY],
    CONSTRAINT [UQ_WAPITenantCompanySecret_Id]
        UNIQUE NONCLUSTERED ([Id] ASC)
        WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
              ALLOW_PAGE_LOCKS = ON
             ) ON [PRIMARY]
) ON [PRIMARY];

GO

SET ANSI_PADDING OFF;
GO

ALTER TABLE [dbo].[WAPITenantCompanySecret] WITH CHECK
ADD CONSTRAINT [FK_WAPITenantCompanySecret_Tenant]
    FOREIGN KEY ([TenantId])
    REFERENCES [dbo].[Tenant] ([TenantId]);
GO

ALTER TABLE [dbo].[WAPITenantCompanySecret] CHECK CONSTRAINT [FK_WAPITenantCompanySecret_Tenant];
GO

EXEC sys.sp_addextendedproperty @name = N'MS_Description',
                                @value = N'This table hold the relation between the Tenant with the different company secrets associated with it.
One tenant can has many companySecret, but one company secret has only one Tenant.',
                                @level0type = N'SCHEMA',
                                @level0name = N'dbo',
                                @level1type = N'TABLE',
                                @level1name = N'WAPITenantCompanySecret';
GO

-- Insert Preload data to WAPOTenantCompanySecret

--SET IDENTITY_INSERT [dbo].[WAPITenantCompanySecret] ON 
--GO
--INSERT [dbo].[WAPITenantCompanySecret] ([Id], [ExternalCompanyCode], [TenantId]) VALUES (1, N'FAME_TEST_WZ4LA8', 60)
--GO
--INSERT [dbo].[WAPITenantCompanySecret] ([Id], [ExternalCompanyCode], [TenantId]) VALUES (2, N'VOYANT_XTLZ33KW', 60)
--GO
--SET IDENTITY_INSERT [dbo].[WAPITenantCompanySecret] OFF
--GO

--- Stored Procedure.

/****** Object:  StoredProcedure [dbo].[WapiTenantGetAdvantageOperationSettings]    Script Date: 9/15/2014 3:53:26 PM ******/
IF EXISTS
(
    SELECT *
    FROM sys.objects
    WHERE object_id = OBJECT_ID(N'[dbo].[WapiTenantGetAdvantageOperationSettings]')
          AND type IN ( N'P', N'PC' )
)
    DROP PROCEDURE [dbo].[WapiTenantGetAdvantageOperationSettings];
GO

/****** Object:  StoredProcedure [dbo].[WapiTenantGetAdvantageOperationSettings]    Script Date: 9/15/2014 3:53:26 PM ******/
SET ANSI_NULLS ON;
GO

SET QUOTED_IDENTIFIER ON;
GO

-- ================================================================================
-- Author:		Jose Alfredo
-- Create date: 4/15/2014
-- Description:	This get the internal setting 
--              for WAPI services. it use only 
--              for WAPI
-- Modification 10/28/2016: Environment and Tenant Access tables are not more used!
-- 
-- ================================================================================

CREATE PROCEDURE [dbo].[WapiTenantGetAdvantageOperationSettings]
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT 
	('server= ' + t.ServerName + ';Database=' +  t.DatabaseName 
 	+  ';Uid=' + t.UserName + ';password=' + t.Password) AS AdvantageConnection
	 ,ak.[Key]
	 , wapi.ExternalCompanyCode
	 , wapi.TenantId
	 , t.TenantName
	 FROM dbo.WAPITenantCompanySecret wapi
	 JOIN dbo.Tenant t ON t.TenantId = wapi.TenantId
	--JOIN dbo.TenantAccess ta ON ta.TenantId = t.TenantId
	--JOIN dbo.DataConnection dc ON dc.ConnectionStringId = ta.ConnectionStringId
	JOIN dbo.ApiAuthenticationKey ak ON ak.TenantId = t.TenantId
	--JOIN [dbo].[Environment] ev ON ev.EnvironmentId = t.EnvironmentID
 
END

GO


