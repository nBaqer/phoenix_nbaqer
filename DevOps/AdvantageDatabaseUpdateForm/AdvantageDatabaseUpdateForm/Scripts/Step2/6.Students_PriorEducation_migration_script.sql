-- *************************************************************************************************
-- BEGIN Migration Student Prior Education to Lead Education Table. 3.8
-- JAGG
-- *************************************************************************************************
SET NUMERIC_ROUNDABORT OFF;
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON;
GO
SET XACT_ABORT ON;
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
GO
BEGIN TRANSACTION MigratePriorEducation;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
IF EXISTS ( SELECT  1
            FROM    sys.objects AS O
            WHERE   O.name = 'plStudentEducation'
                    AND O.type IN ( 'V' ) )
    BEGIN
        SET NOEXEC ON;
        PRINT 'plStudentEducation View already exist.........';
    END;
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

-- ************************************************************************
-- BEGIN CURSOR OVER adLeads LeadId & StudentID:
-- MIGRATE:
-- Migrate Student Prior Education
-- ************************************************************************
PRINT 'Delete previous students migrations in table AdLeadEducation';
DELETE  adLeadEducation
WHERE   LeadId IN ( SELECT  LeadId
                    FROM    adLeads
                    WHERE   StudentId <> '00000000-0000-0000-0000-000000000000' );
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO

-- Create Values used in all operation
DECLARE @StatusActiveId AS UNIQUEIDENTIFIER  = (
                                                 SELECT StatusId
                                                 FROM   syStatuses
                                                 WHERE  StatusCode = 'A'
                                               );
-- Use Cursor to migrate the table
DECLARE @leadId UNIQUEIDENTIFIER;
DECLARE @studentId UNIQUEIDENTIFIER;
DECLARE lead_cursor CURSOR
FOR
    SELECT  LeadId
           ,StudentId
    FROM    adLeads
    WHERE   StudentId <> '00000000-0000-0000-0000-000000000000'
    ORDER BY ModDate;
OPEN lead_cursor;
FETCH NEXT FROM lead_cursor INTO @leadId,@studentId;
WHILE @@FETCH_STATUS = 0
    BEGIN
        INSERT  INTO dbo.adLeadEducation
                (
                 LeadEducationId
                ,LeadId
                ,EducationInstId
                ,EducationInstType
                ,GraduatedDate
                ,FinalGrade
                ,CertificateId
                ,Comments
                ,ModUser
                ,ModDate
                ,Major
                ,StatusId
                ,Graduate
                ,GPA
                ,Rank
                ,Percentile
                ,Certificate
                )
                SELECT  se.StuEducationId  -- StuEducationId - uniqueidentifier
                       ,@leadId -- LeadId - uniqueidentifier
                       ,se.EducationInstId  -- EducationInstId - uniqueidentifier
                       ,se.EducationInstType  -- EducationInstType - string
                       ,se.GraduatedDate  -- StartDate - datetime
                       ,se.FinalGrade  -- EndDate - datetime
                       ,se.CertificateId  -- Comments - varchar(300)
                       ,se.Comments
                       ,ISNULL(se.ModUser,'Support') AS ModUser  -- ModUser - varchar(50)
                       ,ISNULL(se.ModDate,GETDATE()) AS ModDate  -- ModDate - datetime
                       ,NULL -- Major - varchar(50)
                       ,@StatusActiveId -- StatusId - uniqueidentifier
                       ,( CASE WHEN se.GraduatedDate IS NOT NULL THEN 1
                               ELSE 0
                          END ) AS Graduate  -- Graduate - bit
                       ,NULL -- GPA - decimal
                       ,NULL  -- Rank - int
                       ,NULL -- Percentile - int
                       ,''  -- Certificate - varchar(50)
                FROM    dbo.plStudentEducation se
                WHERE   se.StudentId = @studentId;  
        IF @@ERROR <> 0
            SET NOEXEC ON;	
-- -------------------------------------------------------------------------
        FETCH NEXT FROM lead_cursor INTO @leadId,@studentId;		
    END;
CLOSE lead_cursor;
DEALLOCATE lead_cursor;
GO
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
-- ************************************************************************
-- END CURSOR OVER adLeads LeadId & StudentID:
-- MIGRATED:Migrate Prior Work
-- ************************************************************************
-- *************************************************************************************************
-- END TRANSACTION
PRINT 'End Transaction';
GO
COMMIT TRANSACTION MigratePriorEducation;
GO
-- *************************************************************************************************
IF @@ERROR <> 0
    SET NOEXEC ON;
GO
DECLARE @Success AS BIT; 
SET @Success = 1; 
SET NOEXEC OFF; 
IF ( @Success = 1 )
    BEGIN
        PRINT '';
        PRINT 'The database update SUCCEEDED'; 
        PRINT '';
    END;
ELSE
    BEGIN 
        IF @@TRANCOUNT > 0
            ROLLBACK TRANSACTION MigratePriorEducation;
        PRINT '';
        PRINT 'The database update FAILED ';
        PRINT '';
    END;
GO
-- *************************************************************************************************