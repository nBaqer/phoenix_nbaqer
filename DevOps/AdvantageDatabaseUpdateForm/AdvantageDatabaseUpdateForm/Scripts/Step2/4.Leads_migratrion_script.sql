-- Migration Script

/*
Use this migration script to make data changes only. You must commit any additive schema changes first.
Schema changes and migration scripts are deployed in the order they're committed.
*/

-- ********************************************************************************************************************
-- LEAD MIGRATION SCRIPT ADVANTAGE TO 3.8 (leads_migration_script.sql)
-- THIS MIGRATE LEAD information from old tables to new tables. 
-- WARNING Run before the information migration of Student To Lead !!!!
-- This script can be ran several time. but it is intended to be ran when the Client is updated to 3.8
-- WARNING !! Run the script AFTER the consolidated script
-- Changes Content:
-- US8986: Migrate all Address Fields in AdLeads To AdLeadAddresses
-- US8670: TECH - Convert Legacy Notes/comments to new Note Page
-- AdLeads Phone information fields are migrated to the new table AdLeadPhone.
-- *****************************************************************************
-- US8986 :   Lead Info Address and Data Migration
-- Script data migration
-- Migrate all Address Fields in AdLeads To AdLeadAddresses
-- ATTENTION!!!! All previous information in AdLeadAddresses must be deleted
-- JAGG
-- *****************************************************************************
-- MIGRATE Lead SKILLS to new Skill Table (adLeadSkill --> adSkill)
-- ALL Information in adSkill must be DELETED !!!!!!!
-- *****************************************************************************



-- ********************************************************************************************************************
-- Update Catalogos before migration
-- ********************************************************************************************************************
-- ********************************************************************************************************************
-- syEmailType
-- ********************************************************************************************************************
DECLARE @EMailTypeCode AS VARCHAR(12);

SET @EMailTypeCode = 'Home';

IF NOT EXISTS ( SELECT  1
                FROM    syEmailType AS SETY
                WHERE   SETY.EMailTypeCode = @EMailTypeCode --  EMailTypeCode = 'Home'
               )
    BEGIN
        INSERT  INTO syEmailType
                (
                 EMailTypeId
                ,EMailTypeCode
                ,EMailTypeDescription
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()
                ,@EMailTypeCode
                ,@EMailTypeCode
                ,'SUPPORT'
                ,GETDATE()
                );
    END;
GO	
-- ********************************************************************************************************************	
DECLARE @EMailTypeCode AS VARCHAR(12);
DECLARE @EmailTypeDesc AS VARCHAR(50);
SET @EMailTypeCode = 'Work';
SET @EmailTypeDesc = 'Personal';

IF NOT EXISTS ( SELECT  1
                FROM    syEmailType AS SETY
                WHERE   SETY.EMailTypeCode = @EMailTypeCode --  EMailTypeCode = 'Home'
               )
    BEGIN
        INSERT  INTO syEmailType
                (
                 EMailTypeId
                ,EMailTypeCode
                ,EMailTypeDescription
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()
                ,@EMailTypeCode
                ,@EmailTypeDesc
                ,'SUPPORT'
                ,GETDATE()
                );
    END;
GO	
-- ********************************************************************************************************************
-- ********************************************************************************************************************
-- syContactTypes
-- ********************************************************************************************************************
DECLARE @ContactTypeCode AS VARCHAR(12);
DECLARE @ContactTypeDesc AS VARCHAR(50);
DECLARE @StatusActive AS VARCHAR(15);

SET @ContactTypeCode = 'Other';
SET @StatusActive = 'Active';

IF NOT EXISTS ( SELECT  1
                FROM    syContactTypes AS SCT
                WHERE   SCT.ContactTypeCode = @ContactTypeCode --  ContactTypeCode = 'Other'
               )
    BEGIN
        INSERT  INTO syContactTypes
                (
                 ContactTypeId
                ,ContactTypeCode
                ,ContactTypeDescrip
                ,StatusId
                ,CampGrpId
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()  -- ContactTypeId - uniqueidentifier
                ,@ContactTypeCode  -- ContactTypeCode - varchar(12)
                ,@ContactTypeCode  -- ContactTypeDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
GO
-- ********************************************************************************************************************
DECLARE @ContactTypeCode AS VARCHAR(12);
DECLARE @ContactTypeDesc AS VARCHAR(50);
DECLARE @StatusActive AS VARCHAR(15);

SET @ContactTypeCode = 'Primary';
SET @StatusActive = 'Active';

IF NOT EXISTS ( SELECT  1
                FROM    syContactTypes AS SCT
                WHERE   SCT.ContactTypeCode = @ContactTypeCode --  ContactTypeCode = 'Other'
               )
    BEGIN
        INSERT  INTO syContactTypes
                (
                 ContactTypeId
                ,ContactTypeCode
                ,ContactTypeDescrip
                ,StatusId
                ,CampGrpId
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()  -- ContactTypeId - uniqueidentifier
                ,@ContactTypeCode  -- ContactTypeCode - varchar(12)
                ,@ContactTypeCode  -- ContactTypeDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
GO
-- ********************************************************************************************************************
-- ********************************************************************************************************************
DECLARE @ContactTypeCode AS VARCHAR(12);
DECLARE @ContactTypeDesc AS VARCHAR(50);
DECLARE @StatusActive AS VARCHAR(15);

SET @ContactTypeCode = 'Emergency';
SET @StatusActive = 'Active';

IF NOT EXISTS ( SELECT  1
                FROM    syContactTypes AS SCT
                WHERE   SCT.ContactTypeCode = @ContactTypeCode --  ContactTypeCode = 'Other'
               )
    BEGIN
        INSERT  INTO syContactTypes
                (
                 ContactTypeId
                ,ContactTypeCode
                ,ContactTypeDescrip
                ,StatusId
                ,CampGrpId
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()  -- ContactTypeId - uniqueidentifier
                ,@ContactTypeCode  -- ContactTypeCode - varchar(12)
                ,@ContactTypeCode  -- ContactTypeDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
GO
-- ********************************************************************************************************************

-- ********************************************************************************************************************
-- syPhoneType
-- ********************************************************************************************************************
DECLARE @PhoneTypeCode AS VARCHAR(12);
DECLARE @PhoneTypeDesc AS VARCHAR(50);
DECLARE @Sequence AS INTEGER;
DECLARE @StatusActive AS VARCHAR(15);

SET @PhoneTypeCode = 'Home';
SET @StatusActive = 'Active';
SET @Sequence = NULL;

IF NOT EXISTS ( SELECT  1
                FROM    syPhoneType AS SPT
                WHERE   SPT.PhoneTypeCode = @PhoneTypeCode --  PhoneTypeCode = 'Home'
               )
    BEGIN
        INSERT  INTO syPhoneType
                (
                 PhoneTypeId
                ,PhoneTypeCode
                ,PhoneTypeDescrip
                ,StatusId
                ,Sequence
                ,CampGrpId
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()			-- ContactTypeId - uniqueidentifier
                ,@PhoneTypeCode		-- ContactTypeCode - varchar(12)
                ,@PhoneTypeCode		-- ContactTypeDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,@Sequence			                    -- Sequence Integer	
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
GO
-- ********************************************************************************************************************
DECLARE @PhoneTypeCode AS VARCHAR(12);
DECLARE @PhoneTypeDesc AS VARCHAR(50);
DECLARE @Sequence AS INTEGER;
DECLARE @StatusActive AS VARCHAR(15);

SET @PhoneTypeCode = 'Other';
SET @StatusActive = 'Active';
SET @Sequence = NULL;

IF NOT EXISTS ( SELECT  1
                FROM    syPhoneType AS SPT
                WHERE   SPT.PhoneTypeCode = @PhoneTypeCode --  PhoneTypeCode = 'Other'
               )
    BEGIN
        INSERT  INTO syPhoneType
                (
                 PhoneTypeId
                ,PhoneTypeCode
                ,PhoneTypeDescrip
                ,StatusId
                ,Sequence
                ,CampGrpId
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()			-- ContactTypeId - uniqueidentifier
                ,@PhoneTypeCode		-- ContactTypeCode - varchar(12)
                ,@PhoneTypeCode		-- ContactTypeDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,@Sequence			                    -- Sequence Integer	
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
GO
-- ********************************************************************************************************************
DECLARE @PhoneTypeCode AS VARCHAR(12);
DECLARE @PhoneTypeDesc AS VARCHAR(50);
DECLARE @Sequence AS INTEGER;
DECLARE @StatusActive AS VARCHAR(15);

SET @PhoneTypeCode = 'Work';
SET @StatusActive = 'Active';
SET @Sequence = NULL;

IF NOT EXISTS ( SELECT  1
                FROM    syPhoneType AS SPT
                WHERE   SPT.PhoneTypeCode = @PhoneTypeCode --  PhoneTypeCode = 'Work'
               )
    BEGIN
        INSERT  INTO syPhoneType
                (
                 PhoneTypeId
                ,PhoneTypeCode
                ,PhoneTypeDescrip
                ,StatusId
                ,Sequence
                ,CampGrpId
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()			-- ContactTypeId - uniqueidentifier
                ,@PhoneTypeCode		-- ContactTypeCode - varchar(12)
                ,@PhoneTypeCode		-- ContactTypeDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,@Sequence			                    -- Sequence Integer	
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
GO
-- ********************************************************************************************************************
DECLARE @PhoneTypeCode AS VARCHAR(12);
DECLARE @PhoneTypeDesc AS VARCHAR(50);
DECLARE @Sequence AS INTEGER;
DECLARE @StatusActive AS VARCHAR(15);

SET @PhoneTypeCode = 'Emergency';
SET @StatusActive = 'Active';
SET @Sequence = NULL;

IF NOT EXISTS ( SELECT  1
                FROM    syPhoneType AS SPT
                WHERE   SPT.PhoneTypeCode = @PhoneTypeCode --  PhoneTypeCode = 'Emergency'
               )
    BEGIN
        INSERT  INTO syPhoneType
                (
                 PhoneTypeId
                ,PhoneTypeCode
                ,PhoneTypeDescrip
                ,StatusId
                ,Sequence
                ,CampGrpId
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()			-- ContactTypeId - uniqueidentifier
                ,@PhoneTypeCode		-- ContactTypeCode - varchar(12)
                ,@PhoneTypeCode		-- ContactTypeDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,@Sequence			                    -- Sequence Integer	
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
GO
-- ********************************************************************************************************************
DECLARE @PhoneTypeCode AS VARCHAR(12);
DECLARE @PhoneTypeDesc AS VARCHAR(50);
DECLARE @Sequence AS INTEGER;
DECLARE @StatusActive AS VARCHAR(15);

SET @PhoneTypeCode = 'Unknown';
SET @StatusActive = 'Active';
SET @Sequence = NULL;

IF NOT EXISTS ( SELECT  1
                FROM    syPhoneType AS SPT
                WHERE   SPT.PhoneTypeCode = @PhoneTypeCode --  PhoneTypeCode = 'Unknown'
               )
    BEGIN
        INSERT  INTO syPhoneType
                (
                 PhoneTypeId
                ,PhoneTypeCode
                ,PhoneTypeDescrip
                ,StatusId
                ,Sequence
                ,CampGrpId
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 NEWID()			-- ContactTypeId - uniqueidentifier
                ,@PhoneTypeCode		-- ContactTypeCode - varchar(12)
                ,@PhoneTypeCode		-- ContactTypeDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,@Sequence			                    -- Sequence Integer	
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
GO
-- ********************************************************************************************************************

-- ********************************************************************************************************************
--  syNotesPageFields
-- ********************************************************************************************************************

    --TRUNCATE TABLE syNotesPageFields;

IF NOT EXISTS ( SELECT  1
                FROM    syNotesPageFields AS SNPF
                WHERE   SNPF.PageFieldId = 1 )
    BEGIN
        INSERT  INTO dbo.syNotesPageFields
                (
                 PageFieldId
                ,PageName
                ,FieldCaption
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 1  -- PageFieldId - int
                ,'Notes'  -- PageName - varchar(50)
                ,'Note'  -- FieldCaption - varchar(50)
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
IF NOT EXISTS ( SELECT  1
                FROM    syNotesPageFields AS SNPF
                WHERE   SNPF.PageFieldId = 2 )
    BEGIN
        INSERT  INTO dbo.syNotesPageFields
                (
                 PageFieldId
                ,PageName
                ,FieldCaption
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 2  -- PageFieldId - int
                ,'Info'  -- PageName - varchar(50)
                ,'Comment'  -- FieldCaption - varchar(50)
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
IF NOT EXISTS ( SELECT  1
                FROM    syNotesPageFields AS SNPF
                WHERE   SNPF.PageFieldId = 3 )
    BEGIN
        INSERT  INTO dbo.syNotesPageFields
                (
                 PageFieldId
                ,PageName
                ,FieldCaption
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 3  -- PageFieldId - int
                ,'Info'  -- PageName - varchar(50)
                ,'Notes'  -- FieldCaption - varchar(50)
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
IF NOT EXISTS ( SELECT  1
                FROM    syNotesPageFields AS SNPF
                WHERE   SNPF.PageFieldId = 4 )
    BEGIN 
        INSERT  INTO dbo.syNotesPageFields
                (
                 PageFieldId
                ,PageName
                ,FieldCaption
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 4  -- PageFieldId - int
                ,'Requirements'  -- PageName - varchar(50)
                ,'Override Reason'  -- FieldCaption - varchar(50)
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
IF NOT EXISTS ( SELECT  1
                FROM    syNotesPageFields AS SNPF
                WHERE   SNPF.PageFieldId = 5 )
    BEGIN
        INSERT  INTO dbo.syNotesPageFields
                (
                 PageFieldId
                ,PageName
                ,FieldCaption
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 5  -- PageFieldId - int
                ,'Task'  -- PageName - varchar(50)
                ,'Message'  -- FieldCaption - varchar(50)
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
IF NOT EXISTS ( SELECT  1
                FROM    syNotesPageFields AS SNPF
                WHERE   SNPF.PageFieldId = 6 )
    BEGIN
        INSERT  INTO dbo.syNotesPageFields
                (
                 PageFieldId
                ,PageName
                ,FieldCaption
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 6  -- PageFieldId - int
                ,'Contact'  -- PageName - varchar(50)
                ,'Comment'  -- FieldCaption - varchar(50)
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
IF NOT EXISTS ( SELECT  1
                FROM    syNotesPageFields AS SNPF
                WHERE   SNPF.PageFieldId = 7 )
    BEGIN
        INSERT  INTO dbo.syNotesPageFields
                (
                 PageFieldId
                ,PageName
                ,FieldCaption
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 7  -- PageFieldId - int
                ,'Skill'  -- PageName - varchar(50)
                ,'Comment'  -- FieldCaption - varchar(50)
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
IF NOT EXISTS ( SELECT  1
                FROM    syNotesPageFields AS SNPF
                WHERE   SNPF.PageFieldId = 8 )
    BEGIN
        INSERT  INTO dbo.syNotesPageFields
                (
                 PageFieldId
                ,PageName
                ,FieldCaption
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 8  -- PageFieldId - int
                ,'Extra Curricular'  -- PageName - varchar(50)
                ,'Comment'  -- FieldCaption - varchar(50)
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
IF NOT EXISTS ( SELECT  1
                FROM    syNotesPageFields AS SNPF
                WHERE   SNPF.PageFieldId = 9 )
    BEGIN
        INSERT  INTO dbo.syNotesPageFields
                (
                 PageFieldId
                ,PageName
                ,FieldCaption
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 9  -- PageFieldId - int
                ,'Email'  -- PageName - varchar(50)
                ,'Template'  -- FieldCaption - varchar(50)
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
IF NOT EXISTS ( SELECT  1
                FROM    syNotesPageFields AS SNPF
                WHERE   SNPF.PageFieldId = 10 )
    BEGIN
        INSERT  INTO dbo.syNotesPageFields
                (
                 PageFieldId
                ,PageName
                ,FieldCaption
                ,ModUser
                ,ModDate
                )
        VALUES  (
                 10  -- PageFieldId - int
                ,'Prior Work'  -- PageName - varchar(50)
                ,'Comments'  -- FieldCaption - varchar(50)
                ,'support'  -- ModUser - varchar(50)
                ,GETDATE()  -- ModDate - datetime
                );
    END;
-- ********************************************************************************************************************

-- ********************************************************************************************************************
--  plAddressTypes
-- ********************************************************************************************************************
DECLARE @AddressCode AS VARCHAR(12);
DECLARE @StatusActive AS VARCHAR(15);

SET @AddressCode = 'Home';
SET @StatusActive = 'Active';

IF NOT EXISTS ( SELECT  1
                FROM    plAddressTypes AS PAT
                WHERE   PAT.AddressCode = @AddressCode --  AddressCode = 'Home'
               )
    BEGIN

        INSERT  INTO plAddressTypes
                (
                 AddressTypeId
                ,AddressDescrip
                ,StatusId
                ,CampGrpId
                ,AddressCode
                ,ModDate
                ,ModUser
                )
        VALUES  (
                 NEWID()						        -- AddressTypeId - uniqueidentifier
                ,@AddressCode							-- AddressDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,@AddressCode							-- AddressCode - varchar(12)
                ,GETDATE()								-- ModDate - datetime
                ,'support'								-- ModUser - varchar(50)
                );
    END;
GO
-- ********************************************************************************************************************

DECLARE @AddressCode AS VARCHAR(12);
DECLARE @StatusActive AS VARCHAR(15);


SET @AddressCode = 'Other';
SET @StatusActive = 'Active';

IF NOT EXISTS ( SELECT  1
                FROM    plAddressTypes AS PAT
                WHERE   PAT.AddressCode = @AddressCode --  AddressCode = 'Other'
               )
    BEGIN

        INSERT  INTO plAddressTypes
                (
                 AddressTypeId
                ,AddressDescrip
                ,StatusId
                ,CampGrpId
                ,AddressCode
                ,ModDate
                ,ModUser
                )
        VALUES  (
                 NEWID()						        -- AddressTypeId - uniqueidentifier
                ,@AddressCode							-- AddressDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,@AddressCode							-- AddressCode - varchar(12)
                ,GETDATE()								-- ModDate - datetime
                ,'support'								-- ModUser - varchar(50)
                );
    END;
GO
-- ********************************************************************************************************************
DECLARE @AddressCode AS VARCHAR(12);
DECLARE @StatusActive AS VARCHAR(15);

SET @AddressCode = 'Work';
SET @StatusActive = 'Active';

IF NOT EXISTS ( SELECT  1
                FROM    plAddressTypes AS PAT
                WHERE   PAT.AddressCode = @AddressCode --  AddressCode = 'Home'
               )
    BEGIN

        INSERT  INTO plAddressTypes
                (
                 AddressTypeId
                ,AddressDescrip
                ,StatusId
                ,CampGrpId
                ,AddressCode
                ,ModDate
                ,ModUser
                )
        VALUES  (
                 NEWID()						        -- AddressTypeId - uniqueidentifier
                ,@AddressCode							-- AddressDescrip - varchar(50)
                ,(
                   SELECT   SS.StatusId
                   FROM     syStatuses AS SS
                   WHERE    SS.Status = @StatusActive
                 )										-- StatusId - uniqueidentifier
                ,(
                   SELECT   SCG.CampGrpId
                   FROM     syCampGrps AS SCG
                   WHERE    SCG.IsAllCampusGrp = 1
                 )										-- CampGrpId - uniqueidentifier
                ,@AddressCode							-- AddressCode - varchar(12)
                ,GETDATE()								-- ModDate - datetime
                ,'support'								-- ModUser - varchar(50)
                );
    END;
GO
-- ********************************************************************************************************************

-- ********************************************************************************************************************
--  plStudentEducation
-- ********************************************************************************************************************
IF EXISTS ( SELECT  1
            FROM    sys.objects
            WHERE   object_id = OBJECT_ID(N'[plStudentEducation]')
                    AND type IN ( N'U' ) )
    BEGIN
        DELETE  plStudentEducation
        WHERE   EducationInstId IS NULL;
    END;
GO
-- ********************************************************************************************************************

-- ********************************************************************************************************************
-- Drop Defaults
-- ********************************************************************************************************************
IF OBJECT_ID('LOADefault','D') IS NOT NULL
    BEGIN
        IF EXISTS ( SELECT  1
                    FROM    sys.columns AS C
                    INNER JOIN sys.tables AS T ON C.object_id = T.object_id
                    WHERE   C.default_object_id = OBJECT_ID('LOADefault')
                            AND C.name = 'ReqGrpDefId'
                            AND T.name = 'adReqGrpDef' )
            BEGIN 
                PRINT 'unbin Default adReqGrpDef.ReqGrpDefId ';
                EXEC sp_unbindefault 'adReqGrpDef.ReqGrpDefId';
                IF ( OBJECT_ID('DF_adReqGrpDef_ReqGrpDefId') IS NULL )
                    BEGIN
                        PRINT 'ADD CONSTAINT DF_adReqGrpDef_ReqGrpDefId  ';
                        ALTER TABLE dbo.adReqGrpDef ADD CONSTRAINT DF_adReqGrpDef_ReqGrpDefId DEFAULT (NEWID()) FOR ReqGrpDefId;
                    END;
            END;

        IF EXISTS ( SELECT  1
                    FROM    sys.columns AS C
                    INNER JOIN sys.tables AS T ON C.object_id = T.object_id
                    WHERE   C.default_object_id = OBJECT_ID('LOADefault')
                            AND C.name = 'LeadGrpReqGrpId'
                            AND T.name = 'adLeadGrpReqGroups' )
            BEGIN 
                PRINT 'unbin Default adLeadGrpReqGroups.LeadGrpReqGrpId ';
                EXEC sp_unbindefault 'adLeadGrpReqGroups.LeadGrpReqGrpId';
                IF ( OBJECT_ID('DF_adLeadGrpReqGroups_LeadGrpReqGrpId') IS NULL )
                    BEGIN
                        PRINT 'ADD CONSTAINT DF_adLeadGrpReqGroups_LeadGrpReqGrpId  ';
                        ALTER TABLE dbo.adLeadGrpReqGroups ADD CONSTRAINT DF_adLeadGrpReqGroups_LeadGrpReqGrpId DEFAULT (NEWID()) FOR LeadGrpReqGrpId;
                    END;
            END;

        IF EXISTS ( SELECT  1
                    FROM    sys.columns AS C
                    INNER JOIN sys.tables AS T ON C.object_id = T.object_id
                    WHERE   C.default_object_id = OBJECT_ID('LOADefault')
                            AND C.name = 'ReqLeadGrpId'
                            AND T.name = 'adReqLeadGroups' )
            BEGIN 
                PRINT 'unbin Default adReqLeadGroups.ReqLeadGrpId ';
                EXEC sp_unbindefault 'adReqLeadGroups.ReqLeadGrpId ';
                IF ( OBJECT_ID('DF_adReqLeadGroups_ReqLeadGrpId') IS NULL )
                    BEGIN
                        PRINT 'ADD CONSTAINT DF_adReqLeadGroups_ReqLeadGrpId ';
                        ALTER TABLE dbo.adReqLeadGroups ADD CONSTRAINT DF_adReqLeadGroups_ReqLeadGrpId DEFAULT (NEWID()) FOR ReqLeadGrpId;
                    END;

            END;

        IF EXISTS ( SELECT  1
                    FROM    sys.columns AS C
                    INNER JOIN sys.tables AS T ON C.object_id = T.object_id
                    WHERE   C.default_object_id = OBJECT_ID('LOADefault')
                            AND C.name = 'LeadStatusChangeId'
                            AND T.name = 'syLeadStatusChanges' )
            BEGIN 
                PRINT 'unbin Default syLeadStatusChanges.LeadStatusChangeId ';
                EXEC sp_unbindefault 'syLeadStatusChanges.LeadStatusChangeId';
                IF ( OBJECT_ID('DF_syLeadStatusChanges_LeadStatusChangeId') IS NULL )
                    BEGIN
                        PRINT 'ADD CONSTAINT DF_syLeadStatusChanges_LeadStatusChangeId ';
                        ALTER TABLE dbo.syLeadStatusChanges ADD CONSTRAINT DF_syLeadStatusChanges_LeadStatusChangeId DEFAULT (NEWID()) FOR LeadStatusChangeId;
                    END;
            END;

        IF EXISTS ( SELECT  1
                    FROM    sys.columns AS C
                    INNER JOIN sys.tables AS T ON C.object_id = T.object_id
                    WHERE   C.default_object_id = OBJECT_ID('LOADefault')
                            AND C.name = 'LeadStatusChangePermissionID'
                            AND T.name = 'syLeadStatusChangePermissions' )
            BEGIN 
                PRINT 'unbin Default syLeadStatusChangePermissions.LeadStatusChangePermissionID';
                EXEC sp_unbindefault 'syLeadStatusChangePermissions.LeadStatusChangePermissionID';
                IF ( OBJECT_ID('DF_syLeadStatusChangePermissions_LeadStatusChangePermissionID') IS NULL )
                    BEGIN
                        PRINT 'ADD CONSTAINT DF_syLeadStatusChangePermissions_LeadStatusChangePermissionID ';
                        ALTER TABLE dbo.syLeadStatusChangePermissions ADD CONSTRAINT DF_syLeadStatusChangePermissions_LeadStatusChangePermissionID DEFAULT (NEWID()) FOR LeadStatusChangePermissionID;
                    END;
            END;

        IF EXISTS ( SELECT  1
                    FROM    sys.columns AS C
                    INNER JOIN sys.tables AS T ON C.object_id = T.object_id
                    WHERE   C.default_object_id = OBJECT_ID('LOADefault')
                            AND C.name = 'StatusChangeId'
                            AND T.name = 'syLeadStatusesChanges' )
            BEGIN 
                PRINT 'unbin Default syLeadStatusesChanges.StatusChangeId';
                EXEC sp_unbindefault 'syLeadStatusesChanges.StatusChangeId';
                IF ( OBJECT_ID('DF_syLeadStatusesChanges_StatusChangeId') IS NULL )
                    BEGIN
                        PRINT 'ADD CONSTAINT DF_syLeadStatusesChanges_StatusChangeId ';
                        ALTER TABLE dbo.syLeadStatusesChanges ADD CONSTRAINT DF_syLeadStatusesChanges_StatusChangeId DEFAULT (NEWID()) FOR StatusChangeId;
                    END;
            END;

        IF EXISTS ( SELECT  1
                    FROM    sys.columns AS C
                    INNER JOIN sys.tables AS T ON C.object_id = T.object_id
                    WHERE   C.default_object_id = OBJECT_ID('LOADefault')
                            AND C.name = 'ReqGrpId'
                            AND T.name = 'adReqGroups' )
            BEGIN 
                PRINT 'unbin Default adReqGroups.ReqGrpId';
                EXEC sp_unbindefault 'adReqGroups.ReqGrpId';
                IF ( OBJECT_ID('DF_adReqGroups_ReqGrpId') IS NULL )
                    BEGIN
                        PRINT 'ADD CONSTAINT DF_adReqGroups_ReqGrpId ';
                        ALTER TABLE dbo.adReqGroups ADD CONSTRAINT DF_adReqGroups_ReqGrpId DEFAULT (NEWID()) FOR ReqGrpId;
                    END;
            END;
        PRINT 'Drop default  LOADefault';
        DROP DEFAULT dbo.LOADefault;
    END; 
GO
-- ********************************************************************************************************************
IF OBJECT_ID('phonedflt') IS NOT NULL
    BEGIN
        PRINT 'Drop default  phonedflt';
        DROP DEFAULT dbo.phonedflt;
    END;
GO
-- ********************************************************************************************************************
IF OBJECT_ID('ForeignPhoneDefault','D') IS NOT NULL
    BEGIN
        IF EXISTS ( SELECT  1
                    FROM    sys.columns AS C
                    INNER JOIN sys.tables AS T ON C.object_id = T.object_id
                    WHERE   C.default_object_id = OBJECT_ID('ForeignPhoneDefault')
                            AND C.name = 'ForeignPhone'
                            AND T.name = 'adLeads' )
            BEGIN 
                PRINT 'unbin Default adLeads.ForeignPhone ';
                EXEC sp_unbindefault 'adLeads.ForeignPhone';
                IF ( OBJECT_ID('DF_adLeads_ForeignPhone') IS NULL )
                    BEGIN
                        PRINT 'ADD CONSTAINT DF_adLeads_ForeignPhone   ';
                        ALTER TABLE dbo.adLeads ADD CONSTRAINT DF_adLeads_ForeignPhone DEFAULT ((0)) FOR ForeignPhone;
                    END;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sys.columns AS C
                    INNER JOIN sys.tables AS T ON C.object_id = T.object_id
                    WHERE   C.default_object_id = OBJECT_ID('ForeignPhoneDefault')
                            AND C.name = 'ForeignPhone'
                            AND T.name = 'arStudentPhone' )
            BEGIN
                PRINT 'unbin Default arStudentPhone.ForeignPhone ';
                EXEC sp_unbindefault 'arStudentPhone.ForeignPhone';
                IF ( OBJECT_ID('DF_arStudentPhone_ForeignPhone') IS NOT NULL )
                    BEGIN
                        PRINT 'ADD CONSTAINT DF_arStudentPhone_ForeignPhone   ';
                        ALTER TABLE dbo.arStudentPhone ADD CONSTRAINT DF_arStudentPhone_ForeignPhone DEFAULT ((0)) FOR ForeignPhone;
                    END;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sys.columns AS C
                    INNER JOIN sys.tables AS T ON C.object_id = T.object_id
                    WHERE   C.default_object_id = OBJECT_ID('ForeignPhoneDefault')
                            AND C.name = 'ForeignPhone'
                            AND T.name = 'adColleges' )
            BEGIN
                PRINT 'unbin Default adColleges.ForeignPhone ';           
                EXEC sp_unbindefault 'adColleges.ForeignPhone';
                IF ( OBJECT_ID('DF_adColleges_ForeignPhone') IS NOT NULL )
                    BEGIN
                        PRINT 'ADD CONSTAINT DF_adColleges_ForeignPhone   ';
                        ALTER TABLE dbo.adColleges ADD CONSTRAINT DF_adColleges_ForeignPhone DEFAULT ((0)) FOR ForeignPhone;
                    END;
            END;

        PRINT 'Drop default  ForeignPhoneDefault';
        DROP DEFAULT dbo.ForeignPhoneDefault;
    END;
GO
-- ********************************************************************************************************************
IF OBJECT_ID('ForeignZipDefault','D') IS NOT NULL
    BEGIN
        IF EXISTS ( SELECT  1
                    FROM    sys.columns AS C
                    INNER JOIN sys.tables AS T ON C.object_id = T.object_id
                    WHERE   C.default_object_id = OBJECT_ID('ForeignZipDefault')
                            AND C.name = 'ForeignZip'
                            AND T.name = 'adLeads' )
            BEGIN 
                PRINT 'unbin Default adLeads.ForeignZip ';
                EXEC sp_unbindefault 'adLeads.ForeignZip';
                IF ( OBJECT_ID('DF_adLeads_ForeignZip') IS NULL )
                    BEGIN
                        PRINT 'ADD CONSTAINT DF_adLeads_ForeignZip   ';
                        ALTER TABLE dbo.adLeads ADD CONSTRAINT DF_adLeads_ForeignZip DEFAULT ((0)) FOR ForeignZip;
                    END;
            END;     
        IF EXISTS ( SELECT  1
                    FROM    sys.columns AS C
                    INNER JOIN sys.tables AS T ON C.object_id = T.object_id
                    WHERE   C.default_object_id = OBJECT_ID('ForeignZipDefault')
                            AND C.name = 'ForeignZip'
                            AND T.name = 'arStudAddresses' )
            BEGIN 
                PRINT 'unbin Default arStudAddresses.ForeignZip ';
                EXEC sp_unbindefault 'arStudAddresses.ForeignZip';
                IF ( OBJECT_ID('DF_arStudAddresses_ForeignZip') IS NULL )
                    BEGIN
                        PRINT 'ADD CONSTAINT DF_adLeads_ForeignPhone   ';
                        ALTER TABLE dbo.arStudAddresses ADD CONSTRAINT DF_arStudAddresses_ForeignZip DEFAULT ((0)) FOR ForeignZip;
                    END;
            END;
        IF EXISTS ( SELECT  1
                    FROM    sys.columns AS C
                    INNER JOIN sys.tables AS T ON C.object_id = T.object_id
                    WHERE   C.default_object_id = OBJECT_ID('ForeignZipDefault')
                            AND C.name = 'ForeignZip'
                            AND T.name = 'adColleges' )
            BEGIN 
                PRINT 'unbin Default adColleges.ForeignZip ';
                EXEC sp_unbindefault 'adColleges.ForeignZip';
                IF ( OBJECT_ID('DF_adColleges_ForeignZip') IS NULL )
                    BEGIN
                        PRINT 'ADD CONSTAINT DF_adColleges_ForeignZip   ';
                        ALTER TABLE dbo.adColleges ADD CONSTRAINT DF_adColleges_ForeignZip DEFAULT ((0)) FOR ForeignZip;
                    END;
            END;
        PRINT 'Drop default  ForeignPhoneDefault';
        DROP DEFAULT dbo.ForeignZipDefault;
    END;
GO
-- ********************************************************************************************************************
-- ********************************************************************************************************************
-- END  --  Update Catalogos 
-- ********************************************************************************************************************

DECLARE @Error AS INTEGER; 
DECLARE @CountTrans AS INTEGER;
DECLARE @Message AS NVARCHAR(MAX);
DECLARE @MigrationFlag AS VARCHAR(10);
DECLARE @StatusIdActive AS UNIQUEIDENTIFIER;
DECLARE @AddressTypeId AS UNIQUEIDENTIFIER; 
DECLARE @CountryIdUSA AS UNIQUEIDENTIFIER;
DECLARE @UserName AS VARCHAR(50);
DECLARE @ProcessDate AS DATETIME;

DECLARE @CurLeadId AS UNIQUEIDENTIFIER;
DECLARE @CurAddressTypeId AS UNIQUEIDENTIFIER;
DECLARE @CurAddress1 AS VARCHAR(250);
DECLARE @CurAddress2 AS VARCHAR(250);
DECLARE @curCity AS VARCHAR(250);
DECLARE @CurStateId AS UNIQUEIDENTIFIER;
DECLARE @CurZipCode AS VARCHAR(10); 
DECLARE @CurCountry AS UNIQUEIDENTIFIER;
DECLARE @CurStatusId AS UNIQUEIDENTIFIER;
DECLARE @CurModDate AS DATETIME;
DECLARE @CurModUser AS VARCHAR(50);
DECLARE @CurState AS VARCHAR(10);
DECLARE @CurIsInternational AS BIT;
DECLARE @CurCountyId AS UNIQUEIDENTIFIER;    
DECLARE @CurAddressApto AS VARCHAR(20);

SET @Error = 0;
SET @CountTrans = 0;
SET @MigrationFlag = 'MIG*';
SET @UserName = 'Support';
SET @ProcessDate = GETDATE();
SET @StatusIdActive = (
                        SELECT TOP 1
                                SS.StatusId
                        FROM    syStatuses AS SS
                        WHERE   SS.StatusCode = 'A'
                      );
SET @AddressTypeId = (
                       SELECT TOP 1
                                PAT.AddressTypeId
                       FROM     plAddressTypes AS PAT
                       WHERE    PAT.AddressCode = 'Home'
                     );
SET @CountryIdUSA = (
                      SELECT TOP 1
                                AC.CountryId
                      FROM      adCountries AS AC
                      WHERE     AC.CountryCode = 'USA'
                    );

DECLARE Lead_Cursor CURSOR FORWARD_ONLY STATIC
FOR
    SELECT  AL.LeadId
           ,ISNULL(AL.AddressType,@AddressTypeId) AS AddressTypeId
           ,ISNULL(AL.Address1,'') AS Address1
           ,ISNULL(AL.Address2,'') AS Address2
           ,AL.City AS City
           ,AL.StateId AS StateId
           ,LEFT(AL.Zip,10) AS ZipCode
           ,AL.Country AS CountryId
           ,@StatusIdActive
           ,ISNULL(AL.ModDate,@ProcessDate) AS ModeDate
           ,ISNULL(AL.ModUser,@UserName) AS ModUser
           ,AL.OtherState AS State
           ,CASE WHEN ISNULL(AL.Country,@CountryIdUSA) = @CountryIdUSA THEN 0
                 ELSE 1
            END AS IsInternantional
           ,AL.County AS CountyId
           ,AL.AddressApt AS AddressApto
    FROM    adLeads AS AL
    WHERE   SUBSTRING(ISNULL(AL.Address1,''),1,LEN(@MigrationFlag)) <> @MigrationFlag;



OPEN Lead_Cursor;
FETCH NEXT FROM Lead_Cursor INTO @CurLeadId,@CurAddressTypeId,@CurAddress1,@CurAddress2,@curCity,@CurStateId,@CurZipCode,@CurCountry,@CurStatusId,@CurModDate,
    @CurModUser,@CurState,@CurIsInternational,@CurCountyId,@CurAddressApto;
WHILE @@FETCH_STATUS = 0
--          AND @Error = 0    -- to test
    BEGIN
        SET @Message = '';
        SET @Error = 0;
        BEGIN TRANSACTION Migrate; 
        SET @CountTrans = @CountTrans + 1;
        BEGIN TRY 
            IF ( @Error = 0 )
                BEGIN
                    IF @CurAddress1 <> ''
                        BEGIN 
                                 -- Insert   adLeadAddresses
                            INSERT  adLeadAddresses
                                    (
                                     adLeadAddressId
                                    ,LeadId
                                    ,AddressTypeId
                                    ,Address1
                                    ,Address2
                                    ,City
                                    ,StateId
                                    ,ZipCode
                                    ,CountryId
                                    ,StatusId
                                    ,IsMailingAddress
                                    ,IsShowOnLeadPage
                                    ,ModDate
                                    ,ModUser
                                    ,State
                                    ,IsInternational
                                    ,CountyId
                                    ,ForeignCountyStr
                                    ,ForeignCountryStr
                                    ,AddressApto
                                    )
                            VALUES  (
                                     NEWID()  -- adLeadAddressId - uniqueidentifier
                                    ,@CurLeadId  -- LeadId - uniqueidentifier
                                    ,@CurAddressTypeId
                                    ,@CurAddress1
                                    ,@CurAddress2
                                    ,@curCity
                                    ,@CurStateId
                                    ,@CurZipCode
                                    ,@CurCountry
                                    ,@CurStatusId
                                    ,1              -- IsMailingAddress
                                    ,1              -- IsShowOnLeadPage
                                    ,@CurModDate
                                    ,@CurModUser
                                    ,@CurState
                                    ,@CurIsInternational
                                    ,@CurCountyId
                                    ,''             -- ForeignCountyStr
                                    ,''             -- ForeignCountryStr
                                    ,@CurAddressApto
                                    );
                            IF ( @@ERROR = 0 )
                                BEGIN
                                    SET @Error = 0;  
                                END;
                            ELSE
                                BEGIN
                                    SET @Error = 1;
                                    SET @Message = @Message + 'Failed insert adLeadAddresses ' + CONVERT(VARCHAR(50),@CountTrans) + '  '
                                        + CONVERT(VARCHAR(50),@CurLeadId) + CHAR(10);
                                END;
                        END;
                END; --
            IF ( @Error = 0 )
                BEGIN
                        -- UpdateLead Id
                    UPDATE  AL
                    SET     AL.Address1 = @MigrationFlag + LEFT(ISNULL(AL.Address1,''),46)
                    FROM    adLeads AS AL
                    WHERE   AL.LeadId = @CurLeadId;
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;  
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
                            SET @Message = @Message + 'Failed Update adLeads ' + CONVERT(VARCHAR(50),@CountTrans) + '  ' + CONVERT(VARCHAR(50),@CurLeadId)
                                + CHAR(10);
                        END;
                END;
-- To Test
--  SET @Error = 1
--  IF @CountTrans > 100   BEGIN  SET @Error = 1;  END;

            IF ( @Error = 0 )
                BEGIN
                    COMMIT TRANSACTION Migrate;
                    SET @Message = @Message + 'Successful COMMIT TRANSACTION ' + CONVERT(VARCHAR(50),@CountTrans) + '  ' + CONVERT(VARCHAR(50),@CurLeadId)
                        + CHAR(10);
                END;
            ELSE
                BEGIN
                    ROLLBACK TRANSACTION Migrate;
                    SET @Message = @Message + 'Failed ROLLBACK TRANSACTION ' + CONVERT(VARCHAR(50),@CountTrans) + '  ' + CONVERT(VARCHAR(50),@CurLeadId)
                        + CHAR(10);  
                    SELECT  @Message;   
                END;
        END TRY
        BEGIN CATCH
            ROLLBACK TRANSACTION Migrate;  
            SET @Message = @Message + 'Failed ROLLBACK TRANSACTION  Catching error' + CONVERT(VARCHAR(50),@CountTrans) + '  ' + CONVERT(VARCHAR(50),@CurLeadId)
                + CHAR(10);    
            SELECT  ERROR_NUMBER() AS ErrorNumber
                   ,ERROR_SEVERITY() AS ErrorSeverity
                   ,ERROR_STATE() AS ErrorState
                   ,ERROR_PROCEDURE() AS ErrorProcedure
                   ,ERROR_LINE() AS ErrorLine
                   ,ERROR_MESSAGE() AS ErrorMessage;   
            PRINT @Message;
        END CATCH;
        PRINT @Message;

        FETCH NEXT FROM Lead_Cursor INTO @CurLeadId,@CurAddressTypeId,@CurAddress1,@CurAddress2,@curCity,@CurStateId,@CurZipCode,@CurCountry,@CurStatusId,
            @CurModDate,@CurModUser,@CurState,@CurIsInternational,@CurCountyId,@CurAddressApto;
    END;
CLOSE Lead_Cursor;
DEALLOCATE Lead_Cursor;

GO
-- *****************************************************************************
--  END  --  US8986 :   Lead Info Address and Data Migration
-- *****************************************************************************


-- ***********************************************************************************
-- Migration Script to Move the phone fields from AdLeads to adLeadPhone
-- Authors: JAGG, 
-- Dependencies: RemoveNonNumericChar function
-- Minimal Version 3.8
-- ***********************************************************************************

DECLARE @Error AS INTEGER; 
DECLARE @CountTrans AS INTEGER;
DECLARE @Message AS NVARCHAR(MAX);
DECLARE @MigrationFlag AS VARCHAR(10);
DECLARE @StatusIdActive AS UNIQUEIDENTIFIER;
DECLARE @DefaultPhoneTypeId AS UNIQUEIDENTIFIER; 
DECLARE @UserName AS VARCHAR(50);
DECLARE @ProcessDate AS DATETIME;
DECLARE @DefaultIsShowOnLeadPage AS BIT;

DECLARE @CurLeadId AS UNIQUEIDENTIFIER;
DECLARE @CurPhoneTypeId AS UNIQUEIDENTIFIER;
DECLARE @CurPhoneTypeId2 AS UNIQUEIDENTIFIER;
DECLARE @CurPhone AS VARCHAR(50);
DECLARE @CurPhone2 AS VARCHAR(50);
DECLARE @CurModDate AS DATETIME;
DECLARE @CurModUser AS VARCHAR(50);
DECLARE @CurIsForeignPhone AS BIT;
DECLARE @CurIsForeignPhone2 AS BIT;
DECLARE @CurDefaultPhone AS INTEGER;


SET @Error = 0;
SET @CountTrans = 0;
SET @MigrationFlag = 'MIG*';
SET @UserName = 'Support';
SET @ProcessDate = GETDATE();
SET @DefaultIsShowOnLeadPage = 0;
SET @StatusIdActive = (
                        SELECT TOP 1
                                SS.StatusId
                        FROM    syStatuses AS SS
                        WHERE   SS.StatusCode = 'A'
                      );
SET @DefaultPhoneTypeId = (
                            SELECT TOP 1
                                    PhoneTypeId
                            FROM    dbo.syPhoneType
                            WHERE   PhoneTypeCode = 'Home'
                          );

DECLARE Lead_Cursor CURSOR FORWARD_ONLY STATIC
FOR
    SELECT  AL.LeadId AS LeadId
           ,ISNULL(AL.PhoneType,@DefaultPhoneTypeId) AS PhoneTypeId
           ,ISNULL(AL.PhoneType2,@DefaultPhoneTypeId) AS PhoneTypeId2
           ,dbo.RemoveNonNumericChar(ISNULL(AL.Phone,'')) AS Phone
           ,dbo.RemoveNonNumericChar(ISNULL(AL.Phone2,'')) AS Phone2
           ,ISNULL(AL.ModDate,@ProcessDate) AS ModeDate
           ,ISNULL(AL.ModUser,@UserName) AS ModUser
           ,AL.ForeignPhone AS IsForeignPhone
           ,AL.ForeignPhone2 AS IsForeignPhone2
           ,AL.DefaultPhone AS DefaultPhone
    FROM    dbo.adLeads AS AL
    WHERE   SUBSTRING(ISNULL(AL.Phone,''),1,LEN(@MigrationFlag)) <> @MigrationFlag
            AND SUBSTRING(ISNULL(AL.Phone2,''),1,LEN(@MigrationFlag)) <> @MigrationFlag;

OPEN Lead_Cursor;
FETCH NEXT FROM Lead_Cursor INTO @CurLeadId,@CurPhoneTypeId,@CurPhoneTypeId2,@CurPhone,@CurPhone2,@CurModDate,@CurModUser,@CurIsForeignPhone,@CurIsForeignPhone2,
    @CurDefaultPhone;
WHILE @@FETCH_STATUS = 0
--       AND @Error = 0    -- to test
    BEGIN
        SET @Message = '';
        SET @Error = 0;
        BEGIN TRANSACTION Migrate; 
        SET @CountTrans = @CountTrans + 1;
        BEGIN TRY 
            IF ( @Error = 0 )
                BEGIN
                    IF ( @CurPhone <> '' )
                        BEGIN
                            INSERT  INTO dbo.adLeadPhone
                                    (
                                     LeadPhoneId
                                    ,LeadId
                                    ,PhoneTypeId
                                    ,Phone
                                    ,ModDate
                                    ,ModUser
                                    ,IsForeignPhone
                                    ,Extension
                                    ,Position
                                    ,IsBest
                                    ,IsShowOnLeadPage
                                    ,StatusId
                                    )
                            VALUES  (
                                     NEWID()
                                    ,@CurLeadId
                                    ,@CurPhoneTypeId
                                    ,@CurPhone
                                    ,@CurModDate
                                    ,@CurModUser
                                    ,@CurIsForeignPhone
                                    ,''                    -- Extention
                                    ,CASE WHEN (
                                                 @CurDefaultPhone IS NULL
                                                 OR @CurDefaultPhone = 1
                                               ) THEN 1
                                          ELSE 2
                                     END
                                    ,CASE WHEN (
                                                 @CurDefaultPhone IS NULL
                                                 OR @CurDefaultPhone = 1
                                               ) THEN 1
                                          ELSE 0
                                     END                   -- IsBest
                                    ,@DefaultIsShowOnLeadPage
                                    ,@StatusIdActive
                                    );
                            IF ( @@ERROR = 0 )
                                BEGIN
                                    SET @Error = 0;  
                                END;
                            ELSE
                                BEGIN
                                    SET @Error = 1;
                                    SET @Message = @Message + 'Failed insert adLeadPhone Phone1 ' + CONVERT(VARCHAR(50),@CountTrans) + '  '
                                        + CONVERT(VARCHAR(50),@CurLeadId) + CHAR(10);
                                END;
                        END;
                END;
            IF ( @Error = 0 )
                BEGIN
                    IF ( @CurPhone2 <> '' )
                        BEGIN
                            INSERT  INTO dbo.adLeadPhone
                                    (
                                     LeadPhoneId
                                    ,LeadId
                                    ,PhoneTypeId
                                    ,Phone
                                    ,ModDate
                                    ,ModUser
                                    ,IsForeignPhone
                                    ,Extension
                                    ,Position
                                    ,IsBest
                                    ,IsShowOnLeadPage
                                    ,StatusId
                                    )
                            VALUES  (
                                     NEWID()
                                    ,@CurLeadId
                                    ,@CurPhoneTypeId2
                                    ,@CurPhone2
                                    ,@CurModDate
                                    ,@CurModUser
                                    ,@CurIsForeignPhone2
                                    ,''                    -- Extention
                                    ,CASE WHEN @CurPhone = ''
                                               AND (
                                                     @CurDefaultPhone IS NULL
                                                     OR @CurDefaultPhone = 2
                                                   ) THEN 1
                                          ELSE 2
                                     END
                                    ,CASE WHEN @CurPhone = ''
                                               AND (
                                                     @CurDefaultPhone IS NULL
                                                     OR @CurDefaultPhone = 2
                                                   ) THEN 1
                                          ELSE 0
                                     END                 -- IsBest
                                    ,@DefaultIsShowOnLeadPage
                                    ,@StatusIdActive
                                    );
                            IF ( @@ERROR = 0 )
                                BEGIN
                                    SET @Error = 0;  
                                END;
                            ELSE
                                BEGIN
                                    SET @Error = 1;
                                    SET @Message = @Message + 'Failed insert adLeadPhone Phone2 ' + CONVERT(VARCHAR(50),@CountTrans) + '  '
                                        + CONVERT(VARCHAR(50),@CurLeadId) + CHAR(10);
                                END;
                        END;
                END;
            IF ( @Error = 0 )
                BEGIN
                        -- UpdateLead Id
                    UPDATE  AL
                    SET     AL.Phone = @MigrationFlag + LEFT(ISNULL(AL.Phone,''),46)
                           ,AL.Phone2 = @MigrationFlag + LEFT(ISNULL(AL.Phone2,''),46)
                    FROM    adLeads AS AL
                    WHERE   AL.LeadId = @CurLeadId;
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;  
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
                            SET @Message = @Message + 'Failed Update adLeads ' + CONVERT(VARCHAR(50),@CountTrans) + '  ' + CONVERT(VARCHAR(50),@CurLeadId)
                                + CHAR(10);
                        END;
                END;
-- To Test
--  SET @Error = 1
--IF @CountTrans > 100 BEGIN SET @Error = 1 END

            IF ( @Error = 0 )
                BEGIN
                    COMMIT TRANSACTION Migrate;
                    SET @Message = @Message + 'Successful COMMIT TRANSACTION ' + CONVERT(VARCHAR(50),@CountTrans) + '  ' + CONVERT(VARCHAR(50),@CurLeadId)
                        + CHAR(10);
                END;
            ELSE
                BEGIN
--                        ROLLBACK TRANSACTION Migrate
                    SET @Message = @Message + 'Failed ROLLBACK TRANSACTION ' + CONVERT(VARCHAR(50),@CountTrans) + '  ' + CONVERT(VARCHAR(50),@CurLeadId)
                        + CHAR(10);  
                    SELECT  @Message;   
                END;
        END TRY
        BEGIN CATCH
--                ROLLBACK TRANSACTION Migrate  
            SET @Message = @Message + 'Failed ROLLBACK TRANSACTION  Catching error' + CONVERT(VARCHAR(50),@CountTrans) + '  ' + CONVERT(VARCHAR(50),@CurLeadId)
                + CHAR(10);    
            SELECT  ERROR_NUMBER() AS ErrorNumber
                   ,ERROR_SEVERITY() AS ErrorSeverity
                   ,ERROR_STATE() AS ErrorState
                   ,ERROR_PROCEDURE() AS ErrorProcedure
                   ,ERROR_LINE() AS ErrorLine
                   ,ERROR_MESSAGE() AS ErrorMessage;   
            PRINT @Message;
        END CATCH;
        PRINT @Message;

        FETCH NEXT FROM Lead_Cursor INTO @CurLeadId,@CurPhoneTypeId,@CurPhoneTypeId2,@CurPhone,@CurPhone2,@CurModDate,@CurModUser,@CurIsForeignPhone,
            @CurIsForeignPhone2,@CurDefaultPhone;
    END;
CLOSE Lead_Cursor;
DEALLOCATE Lead_Cursor;

GO
-----------------------------------------------------------------------
-- END -- Migration Script to Move the phone fields from AdLeads to adLeadPhone
--------------------------------------------------------------------


-- ***********************************************************************************
-- Migration Script to Move the Email fields from AdLeads to adLeadEmail
-- Authors: JAGG
-- Dependencies: None
-- Minimal Version 3.8
-- ***********************************************************************************

DECLARE @Error AS INTEGER; 
DECLARE @CountTrans AS INTEGER;
DECLARE @Message AS NVARCHAR(MAX);
DECLARE @MigrationFlag AS VARCHAR(10);
DECLARE @StatusIdActive AS UNIQUEIDENTIFIER;
DECLARE @HomeEmailTypeId UNIQUEIDENTIFIER;
DECLARE @WorkEmailTypeId UNIQUEIDENTIFIER;
DECLARE @UserName AS VARCHAR(50);
DECLARE @ProcessDate AS DATETIME;
DECLARE @DefaultIsShowOnLeadPage AS BIT;

DECLARE @CurLeadId AS UNIQUEIDENTIFIER;
DECLARE @CurHomeEmail AS VARCHAR(50);
DECLARE @CurWorkEmail AS VARCHAR(50);
DECLARE @CurModDate AS DATETIME;
DECLARE @CurModUser AS VARCHAR(50);



SET @Error = 0;
SET @CountTrans = 0;
SET @MigrationFlag = 'MIG*';
SET @UserName = 'Support';
SET @ProcessDate = GETDATE();
SET @DefaultIsShowOnLeadPage = 0;
SET @StatusIdActive = (
                        SELECT TOP 1
                                SS.StatusId
                        FROM    syStatuses AS SS
                        WHERE   SS.StatusCode = 'A'
                      );
SET @HomeEmailTypeId = (
                         SELECT EMailTypeId
                         FROM   dbo.syEmailType
                         WHERE  EMailTypeCode = 'Home'
                       );
SET @WorkEmailTypeId = (
                         SELECT EMailTypeId
                         FROM   dbo.syEmailType
                         WHERE  EMailTypeCode = 'Work'
                       );

DECLARE Lead_Cursor CURSOR FORWARD_ONLY STATIC
FOR
    SELECT  AL.LeadId
           ,ISNULL(AL.HomeEmail,'') AS HomeEmail
           ,ISNULL(AL.WorkEmail,'') AS WorkEmail
           ,ISNULL(AL.ModDate,@ProcessDate) AS ModeDate
           ,ISNULL(AL.ModUser,@UserName) AS ModUser
    FROM    adLeads AS AL
    WHERE   SUBSTRING(ISNULL(AL.HomeEmail,''),1,LEN(@MigrationFlag)) <> @MigrationFlag
            AND SUBSTRING(ISNULL(AL.WorkEmail,''),1,LEN(@MigrationFlag)) <> @MigrationFlag;

OPEN Lead_Cursor;
FETCH NEXT FROM Lead_Cursor INTO @CurLeadId,@CurHomeEmail,@CurWorkEmail,@CurModDate,@CurModUser;
WHILE @@FETCH_STATUS = 0
--    AND @Error = 0    -- to test
    BEGIN
        SET @Message = '';
        SET @Error = 0;
        BEGIN TRANSACTION Migrate; 
        SET @CountTrans = @CountTrans + 1;
        BEGIN TRY 
            IF ( @Error = 0 )
                BEGIN
                    IF ( @CurHomeEmail <> '' )
                        BEGIN
                            INSERT  INTO AdLeadEmail
                                    (
                                     LeadEMailId
                                    ,LeadId
                                    ,EMail
                                    ,EMailTypeId
                                    ,IsPreferred
                                    ,IsPortalUserName
                                    ,ModUser
                                    ,ModDate
                                    ,StatusId
                                    ,IsShowOnLeadPage
                                    )
                            VALUES  (
                                     NEWID()
                                    ,@CurLeadId
                                    ,@CurHomeEmail
                                    ,@HomeEmailTypeId
                                    ,1               --  IsPreferred
                                    ,0               --  IsPortalUserName
                                    ,@CurModUser
                                    ,@CurModDate
                                    ,@StatusIdActive
                                    ,@DefaultIsShowOnLeadPage
                                    );
                            IF ( @@ERROR = 0 )
                                BEGIN
                                    SET @Error = 0;  
                                END;
                            ELSE
                                BEGIN
                                    SET @Error = 1;
                                    SET @Message = @Message + 'Failed insert adLeadEmail Home ' + CONVERT(VARCHAR(50),@CountTrans) + '  '
                                        + CONVERT(VARCHAR(50),@CurLeadId) + CHAR(10);
                                END;
                        END;
                END;
            IF ( @Error = 0 )
                BEGIN
                    IF ( @CurWorkEmail <> '' )
                        BEGIN
                            INSERT  INTO AdLeadEmail
                                    (
                                     LeadEMailId
                                    ,LeadId
                                    ,EMail
                                    ,EMailTypeId
                                    ,IsPreferred
                                    ,IsPortalUserName
                                    ,ModUser
                                    ,ModDate
                                    ,StatusId
                                    ,IsShowOnLeadPage
                                    )
                            VALUES  (
                                     NEWID()
                                    ,@CurLeadId
                                    ,@CurWorkEmail
                                    ,@WorkEmailTypeId
                                    ,CASE WHEN @CurHomeEmail = '' THEN 1
                                          ELSE 0
                                     END             --  IsPreferred
                                    ,0               --  IsPortalUserName   
                                    ,@CurModUser
                                    ,@CurModDate
                                    ,@StatusIdActive
                                    ,@DefaultIsShowOnLeadPage
                                    );
                            IF ( @@ERROR = 0 )
                                BEGIN
                                    SET @Error = 0;  
                                END;
                            ELSE
                                BEGIN
                                    SET @Error = 1;
                                    SET @Message = @Message + 'Failed insert adLeadEmail Work ' + CONVERT(VARCHAR(50),@CountTrans) + '  '
                                        + CONVERT(VARCHAR(50),@CurLeadId) + CHAR(10);
                                END;
                        END;
                END;
            IF ( @Error = 0 )
                BEGIN
                     -- Update Lead 
                    UPDATE  AL
                    SET     AL.HomeEmail = @MigrationFlag + LEFT(ISNULL(AL.HomeEmail,''),46)
                           ,AL.WorkEmail = @MigrationFlag + LEFT(ISNULL(AL.WorkEmail,''),46)
                    FROM    adLeads AS AL
                    WHERE   AL.LeadId = @CurLeadId;
                    IF ( @@ERROR = 0 )
                        BEGIN
                            SET @Error = 0;  
                        END;
                    ELSE
                        BEGIN
                            SET @Error = 1;
                            SET @Message = @Message + 'Failed Update adLeads ' + CONVERT(VARCHAR(50),@CountTrans) + '  ' + CONVERT(VARCHAR(50),@CurLeadId)
                                + CHAR(10);
                        END;
                END;
-- To Test
--  SET @Error = 1
--            IF @CountTrans > 800 BEGIN  SET @Error = 1; END;

            IF ( @Error = 0 )
                BEGIN
                    COMMIT TRANSACTION Migrate;
                    SET @Message = @Message + 'Successful COMMIT TRANSACTION ' + CONVERT(VARCHAR(50),@CountTrans) + '  ' + CONVERT(VARCHAR(50),@CurLeadId)
                        + CHAR(10);
                END;
            ELSE
                BEGIN
--                        ROLLBACK TRANSACTION Migrate
                    SET @Message = @Message + 'Failed ROLLBACK TRANSACTION ' + CONVERT(VARCHAR(50),@CountTrans) + '  ' + CONVERT(VARCHAR(50),@CurLeadId)
                        + CHAR(10);  
                    SELECT  @Message;   
                END;
        END TRY
        BEGIN CATCH
--                ROLLBACK TRANSACTION Migrate  
            SET @Message = @Message + 'Failed ROLLBACK TRANSACTION  Catching error ' + CONVERT(VARCHAR(50),@CountTrans) + '  ' + CONVERT(VARCHAR(50),@CurLeadId)
                + CHAR(10);    
            SELECT  ERROR_NUMBER() AS ErrorNumber
                   ,ERROR_SEVERITY() AS ErrorSeverity
                   ,ERROR_STATE() AS ErrorState
                   ,ERROR_PROCEDURE() AS ErrorProcedure
                   ,ERROR_LINE() AS ErrorLine
                   ,ERROR_MESSAGE() AS ErrorMessage;   
            PRINT @Message;
        END CATCH;
        PRINT @Message;

        FETCH NEXT FROM Lead_Cursor INTO @CurLeadId,@CurHomeEmail,@CurWorkEmail,@CurModDate,@CurModUser;
    END;
CLOSE Lead_Cursor;
DEALLOCATE Lead_Cursor;

GO
-- ***********************************************************************************
--  END  --  Migration Script to Move the Email fields from AdLeads to adLeadEmail
-- ***********************************************************************************
-- ================================================================================
-- US8570: TECH - Convert Legacy Notes/comments to new Note Page
-- FIRST PART: Migration of Lead Info Comment and Notes From AdLeads to AllNotes
-- Requisites: Information in Catalogs: -- syResources, syNotesPageFields, syNotesTypes
-- JAGG
-- ================================================================================
DECLARE @Comment VARCHAR(2000);
DECLARE @Note VARCHAR(2000);
DECLARE @NoteId UNIQUEIDENTIFIER;
DECLARE @NewNoteId INT;
DECLARE @LeadId UNIQUEIDENTIFIER;
DECLARE @UserId UNIQUEIDENTIFIER;
DECLARE @ModUser VARCHAR(50);
DECLARE @NoteType VARCHAR(50) = 'Note';
DECLARE @CommentType VARCHAR(50) = 'Comment';
DECLARE @NotePageField INT = 3;
DECLARE @CommentPageField INT = 2; 

DECLARE MY_CURSOR CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY
FOR
    SELECT DISTINCT
            LeadId
    FROM    adLeads;

OPEN MY_CURSOR;
FETCH NEXT FROM MY_CURSOR INTO @LeadId;
WHILE @@FETCH_STATUS = 0
    BEGIN
        --PRINT @LeadId;
		-- Select the field to migrate	
        SET @Note = (
                      SELECT    AdvertisementNote
                      FROM      adLeads
                      WHERE     LeadId = @LeadId
                    ); 
        SET @Comment = (
                         SELECT Comments
                         FROM   adLeads
                         WHERE  LeadId = @LeadId
                       );

		-- TEST if the COMMENT EXISTS
        IF LEN(@Comment) > 0
            BEGIN		    

				-- Test if Comment was migrated
                IF NOT EXISTS ( SELECT  LeadId
                                FROM    dbo.adLead_Notes ln
                                JOIN    dbo.AllNotes an ON an.NotesId = ln.NotesId
                                WHERE   LeadId = @LeadId
                                        AND an.ModuleCode = 'AD'
                                        AND an.NoteType = @CommentType -- Comment
                                        AND an.PageFieldId = @CommentPageField -- Comment
					)
                    BEGIN

						-- Get the user ID
                        SET @UserId = (
                                        SELECT TOP 1
                                                UserId
                                        FROM    dbo.syUsers su
                                        JOIN    dbo.adLeads al ON al.ModUser = su.UserName
                                        WHERE   al.LeadId = @LeadId
                                      );
						-- Get mod user
                        SET @ModUser = (
                                         SELECT ModUser
                                         FROM   adLeads
                                         WHERE  LeadId = @LeadId
                                       );

						-- Enter the comment note
                        IF @UserId IS NOT NULL
                            BEGIN
                                BEGIN TRANSACTION COMMENT;
								-- Migrate Comment in AllNotes
                                INSERT  INTO dbo.AllNotes
                                        (
                                         ModuleCode
                                        ,NoteType
                                        ,UserId
                                        ,NoteText
                                        ,ModUser
                                        ,ModDate
                                        ,PageFieldId
	                                    )
                                VALUES  (
                                         'AD' -- ModuleCode - char(2)
                                        ,@CommentType  -- NoteType - varchar(50)
                                        ,@UserId -- UserId - uniqueidentifier
                                        ,@Comment-- NoteText - varchar(2000)
                                        ,@ModUser -- ModUser - varchar(50)
                                        ,GETDATE()  -- ModDate - datetime
                                        ,@CommentPageField  -- PageFieldId - int
	                                    );

								-- Get New Id
                                SET @NewNoteId = (
                                                   SELECT   IDENT_CURRENT('AllNotes') AS Current_Identity
                                                 );

                                INSERT  INTO dbo.adLead_Notes
                                        ( LeadId,NotesId )
                                VALUES  ( @LeadId  -- LeadId - uniqueidentifier
                                          ,@NewNoteId  -- NotesId - int
                                          );
                                --PRINT @NoteId;
                                COMMIT TRANSACTION COMMENT;

                            END;
                    END;
            END;

		-- TEST IF THE NOTE EXISTS
        IF LEN(@Note) > 0
            BEGIN	

		-- Test if Notes was migrated
                IF NOT EXISTS ( SELECT  LeadId
                                FROM    dbo.adLead_Notes ln
                                JOIN    dbo.AllNotes an ON an.NotesId = ln.NotesId
                                WHERE   LeadId = @LeadId
                                        AND an.ModuleCode = 'AD'
                                        AND an.NoteType = @NoteType  -- Note
                                        AND an.PageFieldId = @NotePageField -- Note
					)
                    BEGIN

						-- Get the user ID
                        SET @UserId = (
                                        SELECT TOP 1
                                                UserId
                                        FROM    dbo.syUsers su
                                        JOIN    dbo.adLeads al ON al.ModUser = su.UserName
                                        WHERE   al.LeadId = @LeadId
                                      );
						-- Get mod user
                        SET @ModUser = (
                                         SELECT ModUser
                                         FROM   adLeads
                                         WHERE  LeadId = @LeadId
                                       );

						-- Enter the comment note
                        IF @UserId IS NOT NULL
                            BEGIN
                                BEGIN TRANSACTION NOTES;
								-- Migrate Comment in AllNotes
                                INSERT  INTO dbo.AllNotes
                                        (
                                         ModuleCode
                                        ,NoteType
                                        ,UserId
                                        ,NoteText
                                        ,ModUser
                                        ,ModDate
                                        ,PageFieldId
	                                    )
                                VALUES  (
                                         'AD' -- ModuleCode - char(2)
                                        ,@NoteType  -- NoteType - varchar(50)
                                        ,@UserId -- UserId - uniqueidentifier
                                        ,@Note-- NoteText - varchar(2000)
                                        ,@ModUser -- ModUser - varchar(50)
                                        ,GETDATE()  -- ModDate - datetime
                                        ,@NotePageField  -- PageFieldId - int
	                                    );

								-- Get New Id
                                SET @NewNoteId = (
                                                   SELECT   IDENT_CURRENT('AllNotes') AS Current_Identity
                                                 );

                                INSERT  INTO dbo.adLead_Notes
                                        ( LeadId,NotesId )
                                VALUES  ( @LeadId  -- LeadId - uniqueidentifier
                                          ,@NewNoteId  -- NotesId - int
                                          );

                                --PRINT @NoteId;
                                COMMIT TRANSACTION NOTES;
                            END;

                    END;

            END;

        FETCH NEXT FROM MY_CURSOR INTO @LeadId;
    END;
CLOSE MY_CURSOR;
DEALLOCATE MY_CURSOR;
GO

-- ================================================================================
-- US8570: TECH - Convert Legacy Notes/comments to new Note Page
-- SECOND PART: Migration of Lead entries of AdLeadNotes to AllNotes
-- Requisites: Information in Catalogs: -- syResources, syNotesPageFields, syNotesTypes
-- NOTE: AdLeadNotes BECOME OBSOLETE!!
-- JAGG
-- ================================================================================ 

DECLARE @NoteId UNIQUEIDENTIFIER;
DECLARE @LeadId UNIQUEIDENTIFIER;
DECLARE @NewNoteId INT;
DECLARE @ModuleCode VARCHAR(2);

DECLARE @NoteType VARCHAR(50) = 'Note';
DECLARE @NotePageField INT = 1;
DECLARE @Note VARCHAR(2000);


BEGIN TRANSACTION ALLNOTES;

DECLARE MY_CURSOR CURSOR LOCAL STATIC READ_ONLY FORWARD_ONLY
FOR
    SELECT DISTINCT
            LeadId
           ,LeadNoteId
           ,ModuleCode
    FROM    adLeadNotes;

OPEN MY_CURSOR;
FETCH NEXT FROM MY_CURSOR INTO @LeadId,@NoteId,@ModuleCode;
WHILE @@FETCH_STATUS = 0
    BEGIN 
        -- Get note Text
        SET @Note = (
                      SELECT    LeadNoteDescrip
                      FROM      dbo.adLeadNotes
                      WHERE     LeadNoteId = @NoteId
                    );

		-- Get the lead attached to the notes
        --SET @LeadId = ( SELECT  LeadId
        --                FROM    dbo.adLeadNotes
        --                WHERE   LeadNoteId = @NoteId
        --              );

        IF @LeadId IS NOT NULL
            BEGIN			   
                -- TEST if the Lead is not orphan and exist in adLeads Table
                IF EXISTS ( SELECT  LeadId
                            FROM    adLeads
                            WHERE   LeadId = @LeadId )
                    BEGIN

						-- TEST IF THE NOTE EXISTS FOr Module AD
                        IF NOT EXISTS ( SELECT  LeadId
                                        FROM    dbo.adLead_Notes ln
                                        JOIN    dbo.AllNotes an ON an.NotesId = ln.NotesId
                                        WHERE   LeadId = @LeadId
                                                AND an.ModuleCode = @ModuleCode
                                                AND an.PageFieldId = @NotePageField -- Comment
                                                AND an.NoteText = @Note )
                            BEGIN
							-- Migrate to AllNotes table
                                INSERT  INTO dbo.AllNotes
                                        (
                                         ModuleCode
                                        ,NoteType
                                        ,UserId
                                        ,NoteText
                                        ,ModUser
                                        ,ModDate
                                        ,PageFieldId
	                                    )
                                VALUES  (
                                         (
                                           SELECT   ModuleCode
                                           FROM     adLeadNotes
                                           WHERE    LeadNoteId = @NoteId
                                         )-- ModuleCode - char(2)
                                        ,@NoteType  -- NoteType - VARCHAR(50) Note
                                        ,(
                                           SELECT   UserId
                                           FROM     adLeadNotes
                                           WHERE    LeadNoteId = @NoteId
                                         )  -- UserId - uniqueidentifier
                                        ,@Note   -- NoteText - varchar(2000)
                                        ,(
                                           SELECT   ModUser
                                           FROM     adLeadNotes
                                           WHERE    LeadNoteId = @NoteId
                                         )  -- ModUser - varchar(50)
                                        --,GETDATE()  -- ModDate - datetime
                                        ,(
                                           SELECT   ModDate
                                           FROM     adLeadNotes
                                           WHERE    LeadNoteId = @NoteId
                                         )  -- ModUser - varchar(50)
                                        ,@NotePageField  -- PageFieldId - int
	                                    );

                                SET @NewNoteId = (
                                                   SELECT   IDENT_CURRENT('AllNotes') AS Current_Identity
                                                 );

                                INSERT  INTO dbo.adLead_Notes
                                        ( LeadId,NotesId )
                                VALUES  ( @LeadId -- LeadId - uniqueidentifier
                                          ,@NewNoteId  -- NotesId - int
                                          );

                             --   PRINT @NoteId;
                            END;
                    END;
            END;
        FETCH NEXT FROM MY_CURSOR INTO @LeadId,@NoteId,@ModuleCode;
    END;
CLOSE MY_CURSOR;
DEALLOCATE MY_CURSOR;

COMMIT TRANSACTION ALLNOTES;

GO
-- ********************************************************************
-- MIGRATE Lead SKILLS: from adLeadSkills to adSkills
-- ALL INFORMATION MUST BE DELETED IN adSKILLS !!!
-- ********************************************************************

BEGIN TRANSACTION LeadSkills;
BEGIN TRY
		-- Delete all records in adSkills
    DELETE  adSkills;
    DECLARE lead_cursor CURSOR
    FOR
        SELECT  LeadId
        FROM    adLeads
        ORDER BY ModDate;
    DECLARE @LeadId UNIQUEIDENTIFIER;
    OPEN lead_cursor;
    FETCH NEXT FROM lead_cursor INTO @LeadId;
    WHILE @@FETCH_STATUS = 0
        BEGIN
            INSERT  INTO dbo.adSkills
                    (
                     LeadId
                    ,LevelId
                    ,SkillGrpId
                    ,Description
                    ,Comment
                    ,ModDate
                    ,ModUser
                    )
                    SELECT  @LeadId  -- LeadId - uniqueidentifier
                           ,1  -- LevelId - int
                           ,pl.SkillGrpId  -- SkillGrpId - uniqueidentifier
                           ,pl.SkillDescrip  -- Description - varchar(50)
                           ,g.SkillGrpName  -- Comment - varchar(100)
                           ,GETDATE()  -- ModDate - datetime
                           ,''  -- ModUser - varchar(50)
                    FROM    dbo.adLeadSkills sk
                    JOIN    dbo.plSkills pl ON pl.SkillId = sk.SkillId
                    JOIN    dbo.plSkillGroups g ON g.SkillGrpId = pl.SkillGrpId
                    WHERE   LeadId = @LeadId;

            FETCH NEXT FROM lead_cursor INTO @LeadId;		
        END;
    CLOSE lead_cursor;
    DEALLOCATE lead_cursor;
    COMMIT TRANSACTION LeadSkills;
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION LeadSkills;
END CATCH;





