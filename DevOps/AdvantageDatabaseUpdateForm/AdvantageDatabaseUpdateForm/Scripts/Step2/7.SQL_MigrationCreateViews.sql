--==========================================================================================
-- STEP 3: Create the necessary views and rename the old tables 
--==========================================================================================

--==========================================================================================
-- Start Control for #tmperrors and transaction
--==========================================================================================
SET NUMERIC_ROUNDABORT OFF; 
GO 
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON; 
GO 
IF EXISTS ( SELECT  *
            FROM    tempdb..sysobjects
            WHERE   id = OBJECT_ID('tempdb..#tmpErrors') )
    BEGIN 
        DROP TABLE #tmpErrors; 
    END; 
GO 
CREATE TABLE #tmpErrors ( Error INT ); 
GO 
SET XACT_ABORT ON; 
GO 
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE; 
GO 
BEGIN TRANSACTION; 
GO 
--==========================================================================================
--  arStudent TABLE
--==========================================================================================
-- Rename original Table arStudent to arStudentOld
PRINT 'If arStudentOld doesn''t exist renaming original Table arStudent to arStudentOld ';
GO
IF NOT EXISTS ( SELECT  *
                FROM    INFORMATION_SCHEMA.TABLES
                WHERE   TABLE_NAME = N'arStudentOld' )
    BEGIN
        EXEC sp_rename 'dbo.arStudent','arStudentOld';
    END; 
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 

-- Drop the view arStudent if exists
PRINT 'Dropping the view arStudent if exists';
GO
IF EXISTS ( SELECT  1
            FROM    sys.views
            WHERE   name = 'arStudent' )
    BEGIN
        DROP VIEW arStudent;       
    END;
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
-- Create the view arStudent
PRINT 'Creating the view arStudent';
GO
CREATE VIEW arStudent
AS
    SELECT  AL.StudentId
           ,AL.FirstName
           ,AL.MiddleName
           ,AL.LastName
           ,AL.SSN
           ,AL.StudentStatusId AS StudentStatus
           ,(
              SELECT TOP 1
                        ALE.EMail
              FROM      AdLeadEmail AS ALE
              INNER JOIN syStatuses AS SS ON SS.StatusId = ALE.StatusId
              WHERE     ALE.LeadId = AL.LeadId
                        AND SS.Status = 'Active'
                        AND ALE.EMailTypeId = (
                                                SELECT  EMailTypeId
                                                FROM    dbo.syEmailType
                                                WHERE   EMailTypeCode = 'Work'
                                              )
              ORDER BY  ALE.IsPreferred DESC
                       ,ALE.IsShowOnLeadPage DESC
                       ,ALE.ModDate DESC
            ) AS WorkEmail
           ,(
              SELECT TOP 1
                        EMail
              FROM      AdLeadEmail AS ALE
              INNER JOIN syStatuses AS SS ON SS.StatusId = ALE.StatusId
              WHERE     ALE.LeadId = AL.LeadId
                        AND SS.Status = 'Active'
                        AND ALE.EMailTypeId = (
                                                SELECT  EMailTypeId
                                                FROM    dbo.syEmailType
                                                WHERE   EMailTypeCode = 'Home'
                                              )
              ORDER BY  ALE.IsPreferred DESC
                       ,ALE.IsShowOnLeadPage DESC
                       ,ALE.ModDate DESC
            ) AS HomeEmail
           ,AL.BirthDate AS DOB
           ,AL.ModUser
           ,AL.ModDate
           ,AL.Prefix
           ,AL.Suffix
           ,AL.Sponsor
           ,AL.AssignedDate
           ,AL.Gender
           ,AL.Race
           ,AL.MaritalStatus
           ,AL.FamilyIncome
           ,CONVERT(INTEGER,AL.Children) AS Children
           ,AL.ExpectedStart
           ,AL.ShiftID AS ShiftId
           ,AL.Nationality
           ,AL.Citizen
           ,AL.DrivLicStateID AS DrivLicStateId
           ,AL.DrivLicNumber
           ,AL.AlienNumber
           ,(
              SELECT TOP 1
                        SUBSTRING(AN.NoteText,1,300)
              FROM      AllNotes AS AN
              JOIN      adLead_Notes AS ALN ON ALN.NotesId = AN.NotesId
              WHERE     AN.ModuleCode = 'AR'
                        AND AN.PageFieldId = 2
                        AND ALN.LeadId = AL.LeadId
            ) AS Comments                                       -- Varchar(300)
           ,(
              SELECT TOP 1
                        ALA.CountyId
              FROM      dbo.adLeadAddresses AS ALA
              INNER JOIN syStatuses AS SS ON SS.StatusId = ALA.StatusId
              INNER JOIN plAddressTypes AS PAT ON PAT.AddressTypeId = ALA.AddressTypeId
              WHERE     ALA.LeadId = AL.LeadId
                        AND SS.Status = 'Active'
              ORDER BY  ALA.IsShowOnLeadPage DESC
                       ,PAT.AddressDescrip ASC
                       ,ALA.ModDate DESC
            ) AS County                                          -- foreign key
           ,AL.SourceDate AS SourceDate                     -- SourceDate
           ,AL.PreviousEducation AS EdLvlId
           ,AL.StudentNumber
           ,'' AS Objective
           ,AL.DependencyTypeId
           ,AL.DegCertSeekingId
           ,AL.GeographicTypeId
           ,AL.HousingId
           ,AL.admincriteriaid
           ,AL.AttendTypeId
           ,NULL AS ApportionedCreditBalanceAY1
           ,NULL AS ApportionedCreditBalanceAY2
    FROM    adLeads AS AL
    WHERE   StudentId <> '00000000-0000-0000-0000-000000000000';
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
--==========================================================================================
--  PHONE
--==========================================================================================

-- Rename the table arStudentPhone
PRINT 'If arStudentPhoneOld doesn''t exist renaming original Table arStudentPhone to arStudentPhoneOld ';
GO
IF NOT EXISTS ( SELECT  *
                FROM    INFORMATION_SCHEMA.TABLES
                WHERE   TABLE_NAME = N'arStudentPhoneOld' )
    BEGIN
        EXEC sp_rename 'dbo.arStudentPhone','arStudentPhoneOld';
    END; 
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
-- Create the view arStudentPhone
PRINT 'Dropping the view arStudentPhone if exists';
GO
IF EXISTS ( SELECT  1
            FROM    sys.views
            WHERE   name = 'arStudentPhone' )
    BEGIN
        DROP VIEW arStudentPhone;       
    END;
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
-- Create the view arStudentPhone
PRINT 'Creating the view arStudentPhone';
GO
CREATE VIEW arStudentPhone
AS
    SELECT  LeadPhoneId AS StudentPhoneId
           ,AL.StudentId
           ,ALP.PhoneTypeId AS PhoneTypeId
           ,ALP.Phone
           ,NULL AS BestTime
           ,ALP.StatusId
           ,ALP.Extension AS Ext
           ,ALP.ModDate
           ,ALP.ModUser
           ,CASE WHEN ALP.Position = 1 THEN 1
                 ELSE 0
            END AS default1
           ,ALP.IsForeignPhone AS ForeignPhone
    FROM    adLeadPhone AS ALP
    JOIN    adLeads AS AL ON AL.LeadId = ALP.LeadId
    WHERE   AL.StudentId <> '00000000-0000-0000-0000-000000000000';
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
--==========================================================================================
--  ADDRESSES
--==========================================================================================

-- Rename the table arStudAddresses
PRINT 'If arStudAddressesOld doesn''t exist renaming original Table arStudAddresses to arStudAddressesOld ';
GO
IF NOT EXISTS ( SELECT  *
                FROM    INFORMATION_SCHEMA.TABLES
                WHERE   TABLE_NAME = N'arStudAddressesOld' )
    BEGIN
        EXEC sp_rename 'dbo.arStudAddresses','arStudAddressesOld';
    END; 
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
-- Drop the view arStudAddresses
PRINT 'Dropping the view arStudAddresses if exists';
GO
IF EXISTS ( SELECT  1
            FROM    sys.views
            WHERE   name = 'arStudAddresses' )
    BEGIN
        DROP VIEW arStudAddresses;       
    END;
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
-- Create the view arStudAddresses
PRINT 'Creating the view arStudAddresses';
GO
CREATE VIEW arStudAddresses
AS
    SELECT  ALA.adLeadAddressId AS StdAddressId
           ,AL.StudentId
           ,ALA.Address1
           ,ALA.Address2
           ,ALA.City
           ,ALA.StateId
           ,ALA.ZipCode AS Zip
           ,ALA.CountryId
           ,ALA.AddressTypeId
           ,ALA.ModUser
           ,ALA.ModDate
           ,ALA.StatusId
           ,ALA.IsShowOnLeadPage AS default1
           ,ALA.IsInternational AS ForeignZip
           ,ALA.State AS OtherState
    FROM    adLeadAddresses AS ALA
    JOIN    dbo.adLeads AS AL ON AL.LeadId = ALA.LeadId
    WHERE   AL.StudentId <> '00000000-0000-0000-0000-000000000000';
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
--==========================================================================================
--  NOTES
--==========================================================================================
-- Rename the table syStudentNotes
PRINT 'If syStudentNotesOld doesn''t exist renaming original Table syStudentNotes to syStudentNotesOld ';
GO
IF NOT EXISTS ( SELECT  *
                FROM    INFORMATION_SCHEMA.TABLES
                WHERE   TABLE_NAME = N'syStudentNotesOld' )
    BEGIN
        EXEC sp_rename 'dbo.syStudentNotes','syStudentNotesOld';
    END; 
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
-- Drop the view ArStudAddresses
PRINT 'Dropping the view syStudentNotes if exists';
GO
IF EXISTS ( SELECT  1
            FROM    sys.views
            WHERE   name = 'syStudentNotes' )
    BEGIN
        DROP VIEW syStudentNotes;       
    END;
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO
-- Create the view syStudentNotes
PRINT 'Creating the view syStudentNotes';
GO 
CREATE VIEW syStudentNotes
AS
    SELECT  AN.NotesId AS StudentNoteId
           ,(
              SELECT    SS.StatusId
              FROM      syStatuses AS SS
              WHERE     SS.Status = 'Active'
            ) AS StatusId
           ,AL.StudentId
           ,AN.NoteText AS StudentNoteDescrip
           ,AN.ModuleCode
           ,AN.UserId
           ,AN.ModDate AS CreatedDate
           ,AN.ModUser
           ,AN.ModDate
    FROM    AllNotes AS AN
    JOIN    dbo.adLead_Notes AS ALN ON ALN.NotesId = AN.NotesId
    JOIN    dbo.adLeads AS AL ON AL.LeadId = ALN.LeadId
    WHERE   AL.StudentId <> '00000000-0000-0000-0000-000000000000';
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
--==========================================================================================
--  OTHER CONTACT AND OTHER CONTACT EMAIL
--==========================================================================================
-- Rename the table syStudentNotes
PRINT 'If syStudentContactsOld doesn''t exist renaming original Table syStudentContacts to syStudentContactsOld ';
GO
IF NOT EXISTS ( SELECT  *
                FROM    INFORMATION_SCHEMA.TABLES
                WHERE   TABLE_NAME = N'syStudentContactsOld' )
    BEGIN
        EXEC sp_rename 'dbo.syStudentContacts','syStudentContactsOld';
    END; 
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
PRINT 'Dropping the view syStudentContacts if exists';
GO
IF EXISTS ( SELECT  1
            FROM    sys.views
            WHERE   name = 'syStudentContacts' )
    BEGIN
        DROP VIEW syStudentContacts;       
    END;
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
-- Create the view syStudentContacts
PRINT 'Creating the view syStudentContacts';
GO 
CREATE VIEW syStudentContacts
AS
    SELECT  ALOC.OtherContactId AS StudentContactId
           ,ALOC.StatusId
           ,AL.StudentId
           ,ALOC.FirstName
           ,ALOC.MiddleName AS MI
           ,ALOC.LastName
           ,ALOC.PrefixId
           ,ALOC.SufixId AS suffixId
           ,ALOC.Comments
           ,cmf.EMail
           ,ALOC.ModUser
           ,ALOC.ModDate
           ,ALOC.RelationshipId AS RelationId
    FROM    dbo.adLeadOtherContacts AS ALOC
    JOIN    dbo.adLeads AL ON AL.LeadId = ALOC.LeadId
    LEFT JOIN (
                SELECT TOP 1
                        ALOCE.EMail
                       ,ALOCE.OtherContactId
                FROM    dbo.adLeadOtherContactsEmail AS ALOCE
                JOIN    dbo.adLeadOtherContacts AS ALOC2 ON ALOC2.OtherContactId = ALOCE.OtherContactId
              ) cmf ON cmf.OtherContactId = ALOC.OtherContactId
    WHERE   AL.StudentId <> '00000000-0000-0000-0000-000000000000';
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
--==========================================================================================
--  OTHER CONTACT PHONE
--==========================================================================================
PRINT 'If syStudentContactPhonesOld doesn''t exist renaming original Table syStudentContactPhones to syStudentContactPhonesOld ';
GO
IF NOT EXISTS ( SELECT  *
                FROM    INFORMATION_SCHEMA.TABLES
                WHERE   TABLE_NAME = N'syStudentContactPhonesOld' )
    BEGIN
        EXEC sp_rename 'dbo.syStudentContactPhones','syStudentContactPhonesOld';
    END; 
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
PRINT 'Dropping the view syStudentContactPhones if exists';
GO
IF EXISTS ( SELECT  1
            FROM    sys.views
            WHERE   name = 'syStudentContactPhones' )
    BEGIN
        DROP VIEW syStudentContactPhones;       
    END;
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
-- Create the view syStudentContactPhones
PRINT 'Creating the view syStudentContactPhones';
GO 
CREATE VIEW syStudentContactPhones
AS
    SELECT  ALOCP.OtherContactsPhoneId AS StudentContactPhoneId
           ,ALOCP.StatusId
           ,ALOCP.OtherContactId AS StudentContactId
           ,ALOCP.PhoneTypeId
           ,CASE WHEN ALOCP.IsForeignPhone = 1 THEN ''
                 ELSE ALOCP.Phone
            END AS Phone
           ,NULL AS BestTime
           ,ALOCP.Extension AS Ext
           ,CASE WHEN ALOCP.IsForeignPhone = 1 THEN ALOCP.Phone
                 ELSE ''
            END AS ForeignPhone
           ,ALOCP.ModUser
           ,ALOCP.ModDate
    FROM    dbo.adLeadOtherContactsPhone AS ALOCP
    JOIN    dbo.adLeads AS AL ON AL.LeadId = ALOCP.LeadId
    WHERE   AL.StudentId <> '00000000-0000-0000-0000-000000000000';
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
--==========================================================================================
--  OTHER CONTACT ADDRESS
--==========================================================================================
PRINT 'If syStudentContactAddressesOld doesn''t exist renaming original Table syStudentContactAddresses to syStudentContactAddressesOld ';
GO
IF NOT EXISTS ( SELECT  *
                FROM    INFORMATION_SCHEMA.TABLES
                WHERE   TABLE_NAME = N'syStudentContactAddressesOld' )
    BEGIN
        EXEC sp_rename 'dbo.syStudentContactAddresses','syStudentContactAddressesOld';
    END; 
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
PRINT 'Dropping the view syStudentContactAddresses if exists';
GO
IF EXISTS ( SELECT  1
            FROM    sys.views
            WHERE   name = 'syStudentContactAddresses' )
    BEGIN
        DROP VIEW syStudentContactAddresses;       
    END;
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
-- Create the view syStudentContactAddresses
PRINT 'Creating the view syStudentContactAddresses';
GO 
CREATE VIEW syStudentContactAddresses
AS
    SELECT  ad.OtherContactsAddresesId AS StudentContactAddressId
           ,ad.StatusId
           ,ad.OtherContactId AS StudentContactId
           ,ad.AddressTypeId AS AddrTypeId
           ,ad.Address1
           ,ad.Address2
           ,ad.City
           ,ad.StateId
           ,ad.CountryId
           ,CASE WHEN ad.IsInternational = 1 THEN ''
                 ELSE ad.ZipCode
            END AS Zip
           ,CASE WHEN ad.IsInternational = 1 THEN ad.ZipCode
                 ELSE ''
            END AS ForeignZip
           ,ad.State AS OtherState
           ,ad.ModUser
           ,ad.ModDate
    FROM    dbo.adLeadOtherContactsAddreses ad
    JOIN    dbo.adLeads AS AL ON AL.LeadId = ad.LeadId
    WHERE   AL.StudentId <> '00000000-0000-0000-0000-000000000000';
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
--==========================================================================================
--  PRIOR WORK
--==========================================================================================
PRINT 'If plStudentEmploymentOld doesn''t exist renaming original Table plStudentEmployment to plStudentEmploymentOld ';
GO
IF NOT EXISTS ( SELECT  *
                FROM    INFORMATION_SCHEMA.TABLES
                WHERE   TABLE_NAME = N'plStudentEmploymentOld' )
    BEGIN
        EXEC sp_rename 'dbo.plStudentEmployment','plStudentEmploymentOld';
    END; 
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO
PRINT 'Dropping the view plStudentEmployment if exists';
GO 
IF EXISTS ( SELECT  1
            FROM    sys.views
            WHERE   name = 'plStudentEmployment' )
    BEGIN
        DROP VIEW plStudentEmployment;       
    END;
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
-- Create the view plStudentEmployment
PRINT 'Creating the view plStudentEmployment';
GO 
CREATE VIEW plStudentEmployment
AS
    SELECT  ALE.StEmploymentId
           ,AL.StudentId
           ,ALE.JobTitleId
           ,ALE.JobStatusId
           ,ALE.StartDate
           ,ALE.EndDate
           ,ALE.Comments
           ,ALE.ModUser
           ,ALE.ModDate
           ,ALE.JobResponsibilities
           ,ALE.EmployerName
           ,ALE.EmployerJobTitle
    FROM    adLeadEmployment AS ALE
    INNER JOIN adLeads AS AL ON AL.LeadId = ALE.LeadId
    WHERE   AL.StudentId <> '00000000-0000-0000-0000-000000000000';
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
--==========================================================================================
--  EXTRACURRICULAR
--==========================================================================================
PRINT 'If plStudentExtraCurricularsOld doesn''t exist renaming original Table plStudentExtraCurriculars to plStudentExtraCurricularsOld ';
GO
IF NOT EXISTS ( SELECT  *
                FROM    INFORMATION_SCHEMA.TABLES
                WHERE   TABLE_NAME = N'plStudentExtraCurricularsOld' )
    BEGIN
        EXEC sp_rename 'dbo.plStudentExtraCurriculars','plStudentExtraCurricularsOld';
    END; 
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
PRINT 'Dropping the view plStudentExtraCurriculars if exists';
GO 
IF EXISTS ( SELECT  1
            FROM    sys.views
            WHERE   name = 'plStudentExtraCurriculars' )
    BEGIN
        DROP VIEW plStudentExtraCurriculars;       
    END;
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
-- Create the view plStudentExtraCurriculars
PRINT 'Creating the view plStudentExtraCurriculars';
GO 
CREATE VIEW plStudentExtraCurriculars
AS
    SELECT  AEC.IdExtraCurricular AS StuExtraCurricularID
           ,NULL AS ExtraCurricularID  --adExtraCurr
           ,AL.StudentId AS StudentId
           ,AEC.ExtraCurrGrpId AS ExtraCurricularGrpID   -- adExtraCurrGrp
           ,AEC.ModDate AS ModDate
           ,AEC.ModUser AS ModUser
    FROM    adExtraCurricular AS AEC
    INNER JOIN adLeads AS AL ON AL.LeadId = AEC.LeadId
    WHERE   AL.StudentId <> '00000000-0000-0000-0000-000000000000';
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 

--==========================================================================================
--  PRIOR EDUCATION
--==========================================================================================
--==========================================================================================
--  SKILL
--==========================================================================================


--==========================================================================================
--  VIEW1 we need to modify this because we should delete plStudentSkills
--==========================================================================================
PRINT 'Dropping the view VIEW1 if exists';
GO 
IF EXISTS ( SELECT  1
            FROM    sys.views
            WHERE   name = 'VIEW1' )
    BEGIN
        DROP VIEW VIEW1;       
    END;
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 
-- Create the view VIEW1
PRINT 'Creating the view VIEW1';
GO 
CREATE VIEW VIEW1
AS
    SELECT  PSG.SkillGrpId
           ,PS.SkillDescrip
           ,PS.SkillId AS StudentSkillId
    FROM    plSkillGroups AS PSG
    INNER JOIN plSkills AS PS ON PS.SkillGrpId = PSG.SkillGrpId
    INNER JOIN adSkills AS ASK ON ASK.SkillGrpId = PSG.SkillGrpId;
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 

-- Rename the table plStudentSkills it is not more used
-- Note Sp_UnenrollLeads use plStudentSkills, but this stored 
-- procedure should be updated when the new enrollment is created

PRINT 'If plStudentSkillsOld doesn''t exist renaming original Table plStudentSkills to plStudentSkillsOld ';
GO
IF NOT EXISTS ( SELECT  *
                FROM    INFORMATION_SCHEMA.TABLES
                WHERE   TABLE_NAME = N'plStudentSkillsOld' )
    BEGIN
        EXEC sp_rename 'dbo.plStudentSkills','plStudentSkillsOld';
    END; 
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO
PRINT 'Dropping the view plStudentSkills if exists';
GO  
IF EXISTS ( SELECT  1
            FROM    sys.views
            WHERE   name = 'plStudentSkills' )
    BEGIN
        DROP VIEW plStudentSkills;       
    END;
GO
-- Create the view plStudentSkills
PRINT 'Creating the view plStudentSkills';
GO
CREATE VIEW plStudentSkills
AS
    SELECT  PS.SkillId AS StudentSkillId
           ,PS.SkillId AS SkillId
           ,PSG.SkillGrpId AS SkillGrpId
           ,AL.StudentId AS StudentId
           ,ASK.ModDate AS ModDate
           ,ASK.ModUser AS ModUser
    FROM    adSkills AS ASK
    INNER JOIN adLeads AS AL ON AL.LeadId = ASK.LeadId
    INNER JOIN plSkillGroups AS PSG ON PSG.SkillGrpId = ASK.SkillGrpId
    INNER JOIN plSkills AS PS ON PS.SkillGrpId = PSG.SkillGrpId
    WHERE   AL.StudentId <> '00000000-0000-0000-0000-000000000000';
GO
IF @@ERROR <> 0
    AND @@TRANCOUNT > 0
    BEGIN 
        ROLLBACK TRANSACTION; 
    END; 
GO 
IF @@TRANCOUNT = 0
    BEGIN 
        INSERT  INTO #tmpErrors
                ( Error )
                SELECT  1; 
        BEGIN TRANSACTION; 
    END; 
GO 

--==========================================================================================
-- Control for #tmperrors and Rollback or Commint transaction
--==========================================================================================
IF EXISTS ( SELECT  *
            FROM    #tmpErrors )
    BEGIN
        ROLLBACK TRANSACTION; 
    END;
GO
IF @@TRANCOUNT > 0
    BEGIN
        PRINT 'Thea database update succeeded'; 
        COMMIT TRANSACTION; 
    END; 
ELSE
    BEGIN 
        PRINT 'The database update failed'; 
    END; 
GO 
DROP TABLE #tmpErrors; 
GO 
--SELECT * FROM AdLeads WHERE LastName = 'Solorzano'

--SELECT * FROM arStudent WHERE StudentId ='992D2712-A821-40D9-9F7B-B6D235BD4DF4'
--SELECT * FROM arStudent WHERE StudentId ='193D70FD-F744-48AD-9A18-E8F75241C2CF'

--SELECT * FROM dbo.arStuEnrollments WHERE StudentId = '992D2712-A821-40D9-9F7B-B6D235BD4DF4'
--SELECT * FROM dbo.arStuEnrollments WHERE StudentId = '193D70FD-F744-48AD-9A18-E8F75241C2CF'


--==========================================================================================
-- END  --  STEP 3: Create the necessary views and rename the old tables
--==========================================================================================