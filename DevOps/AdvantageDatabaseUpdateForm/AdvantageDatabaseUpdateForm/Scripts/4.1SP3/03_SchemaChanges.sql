﻿/*
Run this script on:

        dev-com-db1\Adv.AvedaLive    -  This database will be modified

to synchronize it with a database with the schema represented by:

        Source

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.7.10021 from Red Gate Software Ltd at 10/15/2019 9:00:46 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
/*
* Use this Pre-Deployment script to perform tasks before the deployment of the project.
* Read more at https://www.red-gate.com/SOC7/pre-deployment-script-help
*/
UPDATE dbo.arClsSectMeetings
SET PeriodId = NULL
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)

DELETE FROM syPeriodsWorkDays
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[SyStudentStatusChanges_Audit_Delete] from [dbo].[syStudentStatusChanges]'
GO
IF OBJECT_ID(N'[dbo].[SyStudentStatusChanges_Audit_Delete]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[SyStudentStatusChanges_Audit_Delete]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[SyStudentStatusChanges_Audit_Insert] from [dbo].[syStudentStatusChanges]'
GO
IF OBJECT_ID(N'[dbo].[SyStudentStatusChanges_Audit_Insert]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[SyStudentStatusChanges_Audit_Insert]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[SyStudentStatusChanges_Audit_Update] from [dbo].[syStudentStatusChanges]'
GO
IF OBJECT_ID(N'[dbo].[SyStudentStatusChanges_Audit_Update]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[SyStudentStatusChanges_Audit_Update]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_Fall05_12MonthEnrollment_ClockHourActivity_GetList]'
GO
IF OBJECT_ID(N'[dbo].[USP_Fall05_12MonthEnrollment_ClockHourActivity_GetList]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_Fall05_12MonthEnrollment_ClockHourActivity_GetList]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_ProcessPaymentPeriodsHours]'
GO
IF OBJECT_ID(N'[dbo].[USP_ProcessPaymentPeriodsHours]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_ProcessPaymentPeriodsHours]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_PostClockAttendance_Insert]'
GO
IF OBJECT_ID(N'[dbo].[USP_PostClockAttendance_Insert]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_PostClockAttendance_Insert]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudent]'
GO
IF OBJECT_ID(N'[dbo].[arStudent]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[SyStudentStatusChanges_Audit_Delete] on [dbo].[syStudentStatusChanges]'
GO
IF OBJECT_ID(N'[dbo].[SyStudentStatusChanges_Audit_Delete]', 'TR') IS NULL
EXEC sp_executesql N' 
  
  
--==========================================================================================  
-- TRIGGER SyStudentStatusChanges_Audit_Delete  
-- INSERT  add Audit History when delete records in SyStudentStatusChanges  
--==========================================================================================  
CREATE TRIGGER [dbo].[SyStudentStatusChanges_Audit_Delete] ON [dbo].[syStudentStatusChanges]
    FOR DELETE
AS
    BEGIN  
        SET NOCOUNT ON;  
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;  
        DECLARE @EventRows AS INT;  
        DECLARE @EventDate AS DATETIME;  
        DECLARE @UserName AS VARCHAR(50);  
	 
        SET @AuditHistId = NEWID();  
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Deleted
                         );  
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Deleted
                         );  
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Deleted
                        );  
  
        IF @EventRows > 0
            BEGIN  
                EXEC fmAuditHistAdd @AuditHistId,''SyStudentStatusChanges'',''D'',@EventRows,@EventDate,@UserName;  
                -- StuEnrollId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,''StuEnrollId''
                               ,CONVERT(VARCHAR(MAX),Old.StuEnrollId)
                        FROM    Deleted AS Old;  
                -- OrigStatusId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,''OrigStatusId''
                               ,CONVERT(VARCHAR(MAX),Old.OrigStatusId)
                        FROM    Deleted AS Old;  
                -- NewStatusId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,''NewStatusId''
                               ,CONVERT(VARCHAR(MAX),Old.NewStatusId)
                        FROM    Deleted AS Old;  
                -- CampusId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,''CampusId''
                               ,CONVERT(VARCHAR(MAX),Old.CampusId)
                        FROM    Deleted AS Old;  
                -- ModDate  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,''ModDate''
                               ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                        FROM    Deleted AS Old;  
                -- ModUser  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,''ModUser''
                               ,CONVERT(VARCHAR(MAX),Old.ModUser)
                        FROM    Deleted AS Old;  
                -- IsReversal  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,''IsReversal''
                               ,CONVERT(VARCHAR(MAX),Old.IsReversal)
                        FROM    Deleted AS Old;  
                -- DropReasonId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,''DropReasonId''
                               ,CONVERT(VARCHAR(MAX),Old.DropReasonId)
                        FROM    Deleted AS Old;  
                -- DateOfChange  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,''DateOfChange''
                               ,CONVERT(VARCHAR(MAX),Old.DateOfChange,121)
                        FROM    Deleted AS Old;  
                -- Lda  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,''Lda''
                               ,CONVERT(VARCHAR(MAX),Old.Lda,121)
                        FROM    Deleted AS Old;  
                -- CaseNumber  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,''CaseNumber''
                               ,CONVERT(VARCHAR(MAX),Old.CaseNumber)
                        FROM    Deleted AS Old;  
                -- RequestedBy  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,''RequestedBy''
                               ,CONVERT(VARCHAR(MAX),Old.RequestedBy)
                        FROM    Deleted AS Old;  
                -- HaveBackup  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,''HaveBackup''
                               ,CONVERT(VARCHAR(MAX),Old.HaveBackup)
                        FROM    Deleted AS Old;  
                -- HaveClientConfirmation  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue 
                        )
                        SELECT  @AuditHistId
                               ,Old.StudentStatusChangeId
                               ,''HaveClientConfirmation''
                               ,CONVERT(VARCHAR(MAX),Old.HaveClientConfirmation)
                        FROM    Deleted AS Old;  
            END;  
        SET NOCOUNT OFF;  
    END;  
--==========================================================================================  
-- END TRIGGER SyStudentStatusChanges_Audit_Delete  
--==========================================================================================  
 
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[SyStudentStatusChanges_Audit_Insert] on [dbo].[syStudentStatusChanges]'
GO
IF OBJECT_ID(N'[dbo].[SyStudentStatusChanges_Audit_Insert]', 'TR') IS NULL
EXEC sp_executesql N' 
  
  
--==========================================================================================  
-- TRIGGER SyStudentStatusChanges_Audit_Insert  
-- INSERT  add Audit History when insert records in SyStudentStatusChanges  
--==========================================================================================  
CREATE TRIGGER [dbo].[SyStudentStatusChanges_Audit_Insert] ON [dbo].[syStudentStatusChanges]
    FOR INSERT
AS
    BEGIN  
        SET NOCOUNT ON;  
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;  
        DECLARE @EventRows AS INT;  
        DECLARE @EventDate AS DATETIME;  
        DECLARE @UserName AS VARCHAR(50);  
	 
        SET @AuditHistId = NEWID();  
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );  
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );  
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );  
  
        IF @EventRows > 0
            BEGIN  
                EXEC fmAuditHistAdd @AuditHistId,''SyStudentStatusChanges'',''I'',@EventRows,@EventDate,@UserName;  
                -- StuEnrollId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,''StuEnrollId''
                               ,CONVERT(VARCHAR(MAX),New.StuEnrollId)
                        FROM    Inserted AS New;  
                -- OrigStatusId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,''OrigStatusId''
                               ,CONVERT(VARCHAR(MAX),New.OrigStatusId)
                        FROM    Inserted AS New;  
                -- NewStatusId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,''NewStatusId''
                               ,CONVERT(VARCHAR(MAX),New.NewStatusId)
                        FROM    Inserted AS New;  
                -- CampusId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,''CampusId''
                               ,CONVERT(VARCHAR(MAX),New.CampusId)
                        FROM    Inserted AS New;  
                -- ModDate  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,''ModDate''
                               ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                        FROM    Inserted AS New;  
                -- ModUser  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,''ModUser''
                               ,CONVERT(VARCHAR(MAX),New.ModUser)
                        FROM    Inserted AS New;  
                -- IsReversal  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,''IsReversal''
                               ,CONVERT(VARCHAR(MAX),New.IsReversal)
                        FROM    Inserted AS New;  
                -- DropReasonId  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,''DropReasonId''
                               ,CONVERT(VARCHAR(MAX),New.DropReasonId)
                        FROM    Inserted AS New;  
                -- DateOfChange  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,''DateOfChange''
                               ,CONVERT(VARCHAR(MAX),New.DateOfChange,121)
                        FROM    Inserted AS New;  
                -- Lda  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,''Lda''
                               ,CONVERT(VARCHAR(MAX),New.Lda,121)
                        FROM    Inserted AS New;  
                -- CaseNumber  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,''CaseNumber''
                               ,CONVERT(VARCHAR(MAX),New.CaseNumber)
                        FROM    Inserted AS New;  
                -- RequestedBy  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,''RequestedBy''
                               ,CONVERT(VARCHAR(MAX),New.RequestedBy)
                        FROM    Inserted AS New;  
                -- HaveBackup  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,''HaveBackup''
                               ,CONVERT(VARCHAR(MAX),New.HaveBackup)
                        FROM    Inserted AS New;  
                -- HaveClientConfirmation  
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue 
                        )
                        SELECT  @AuditHistId
                               ,New.StudentStatusChangeId
                               ,''HaveClientConfirmation''
                               ,CONVERT(VARCHAR(MAX),New.HaveClientConfirmation)
                        FROM    Inserted AS New;  
            END;  
        SET NOCOUNT OFF;  
    END;  
--==========================================================================================  
-- END TRIGGER SyStudentStatusChanges_Audit_Insert  
--==========================================================================================  
 
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[SyStudentStatusChanges_Audit_Update] on [dbo].[syStudentStatusChanges]'
GO
IF OBJECT_ID(N'[dbo].[SyStudentStatusChanges_Audit_Update]', 'TR') IS NULL
EXEC sp_executesql N' 
  
--==========================================================================================  
-- TRIGGER SyStudentStatusChanges_Audit_Update  
-- UPDATE  add Audit History when update fields in SyStudentStatusChanges  
--==========================================================================================  
CREATE TRIGGER [dbo].[SyStudentStatusChanges_Audit_Update] ON [dbo].[syStudentStatusChanges]
    FOR UPDATE
AS
    BEGIN  
        SET NOCOUNT ON;  
        DECLARE @AuditHistId AS UNIQUEIDENTIFIER;  
        DECLARE @EventRows AS INT;  
        DECLARE @EventDate AS DATETIME;  
        DECLARE @UserName AS VARCHAR(50);  
	 
        SET @AuditHistId = NEWID();  
        SET @EventRows = (
                           SELECT   COUNT(*)
                           FROM     Inserted
                         );  
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );  
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );  
  
        IF @EventRows > 0
            BEGIN  
                EXEC fmAuditHistAdd @AuditHistId,''SyStudentStatusChanges'',''U'',@EventRows,@EventDate,@UserName;  
                -- StuEnrollId  
                IF UPDATE(StuEnrollId)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,''StuEnrollId''
                                       ,CONVERT(VARCHAR(MAX),Old.StuEnrollId)
                                       ,CONVERT(VARCHAR(MAX),New.StuEnrollId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.StuEnrollId <> New.StuEnrollId
                                        OR (
                                             Old.StuEnrollId IS NULL
                                             AND New.StuEnrollId IS NOT NULL
                                           )
                                        OR (
                                             New.StuEnrollId IS NULL
                                             AND Old.StuEnrollId IS NOT NULL
                                           );  
                    END;  
                -- OrigStatusId  
                IF UPDATE(OrigStatusId)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,''OrigStatusId''
                                       ,CONVERT(VARCHAR(MAX),Old.OrigStatusId)
                                       ,CONVERT(VARCHAR(MAX),New.OrigStatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.OrigStatusId <> New.OrigStatusId
                                        OR (
                                             Old.OrigStatusId IS NULL
                                             AND New.OrigStatusId IS NOT NULL
                                           )
                                        OR (
                                             New.OrigStatusId IS NULL
                                             AND Old.OrigStatusId IS NOT NULL
                                           );  
                    END;  
                -- NewStatusId  
                IF UPDATE(NewStatusId)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,''NewStatusId''
                                       ,CONVERT(VARCHAR(MAX),Old.NewStatusId)
                                       ,CONVERT(VARCHAR(MAX),New.NewStatusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.NewStatusId <> New.NewStatusId
                                        OR (
                                             Old.NewStatusId IS NULL
                                             AND New.NewStatusId IS NOT NULL
                                           )
                                        OR (
                                             New.NewStatusId IS NULL
                                             AND Old.NewStatusId IS NOT NULL
                                           );  
                    END;  
                -- CampusId  
                IF UPDATE(CampusId)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,''CampusId''
                                       ,CONVERT(VARCHAR(MAX),Old.CampusId)
                                       ,CONVERT(VARCHAR(MAX),New.CampusId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.CampusId <> New.CampusId
                                        OR (
                                             Old.CampusId IS NULL
                                             AND New.CampusId IS NOT NULL
                                           )
                                        OR (
                                             New.CampusId IS NULL
                                             AND Old.CampusId IS NOT NULL
                                           );  
                    END;  
                -- ModDate  
                IF UPDATE(ModDate)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,''ModDate''
                                       ,CONVERT(VARCHAR(MAX),Old.ModDate,121)
                                       ,CONVERT(VARCHAR(MAX),New.ModDate,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.ModDate <> New.ModDate
                                        OR (
                                             Old.ModDate IS NULL
                                             AND New.ModDate IS NOT NULL
                                           )
                                        OR (
                                             New.ModDate IS NULL
                                             AND Old.ModDate IS NOT NULL
                                           );  
                    END;  
                -- ModUser  
                IF UPDATE(ModUser)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,''ModUser''
                                       ,CONVERT(VARCHAR(MAX),Old.ModUser)
                                       ,CONVERT(VARCHAR(MAX),New.ModUser)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.ModUser <> New.ModUser
                                        OR (
                                             Old.ModUser IS NULL
                                             AND New.ModUser IS NOT NULL
                                           )
                                        OR (
                                             New.ModUser IS NULL
                                             AND Old.ModUser IS NOT NULL
                                           );  
                    END;  
                -- IsReversal  
                IF UPDATE(IsReversal)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,''IsReversal''
                                       ,CONVERT(VARCHAR(MAX),Old.IsReversal)
                                       ,CONVERT(VARCHAR(MAX),New.IsReversal)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.IsReversal <> New.IsReversal
                                        OR (
                                             Old.IsReversal IS NULL
                                             AND New.IsReversal IS NOT NULL
                                           )
                                        OR (
                                             New.IsReversal IS NULL
                                             AND Old.IsReversal IS NOT NULL
                                           );  
                    END;  
                -- DropReasonId  
                IF UPDATE(DropReasonId)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,''DropReasonId''
                                       ,CONVERT(VARCHAR(MAX),Old.DropReasonId)
                                       ,CONVERT(VARCHAR(MAX),New.DropReasonId)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.DropReasonId <> New.DropReasonId
                                        OR (
                                             Old.DropReasonId IS NULL
                                             AND New.DropReasonId IS NOT NULL
                                           )
                                        OR (
                                             New.DropReasonId IS NULL
                                             AND Old.DropReasonId IS NOT NULL
                                           );  
                    END;  
                -- DateOfChange  
                IF UPDATE(DateOfChange)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,''DateOfChange''
                                       ,CONVERT(VARCHAR(MAX),Old.DateOfChange,121)
                                       ,CONVERT(VARCHAR(MAX),New.DateOfChange,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.DateOfChange <> New.DateOfChange
                                        OR (
                                             Old.DateOfChange IS NULL
                                             AND New.DateOfChange IS NOT NULL
                                           )
                                        OR (
                                             New.DateOfChange IS NULL
                                             AND Old.DateOfChange IS NOT NULL
                                           );  
                    END;  
                -- Lda  
                IF UPDATE(Lda)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,''Lda''
                                       ,CONVERT(VARCHAR(MAX),Old.Lda,121)
                                       ,CONVERT(VARCHAR(MAX),New.Lda,121)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.Lda <> New.Lda
                                        OR (
                                             Old.Lda IS NULL
                                             AND New.Lda IS NOT NULL
                                           )
                                        OR (
                                             New.Lda IS NULL
                                             AND Old.Lda IS NOT NULL
                                           );  
                    END;  
                -- CaseNumber  
                IF UPDATE(CaseNumber)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,''CaseNumber''
                                       ,CONVERT(VARCHAR(MAX),Old.CaseNumber)
                                       ,CONVERT(VARCHAR(MAX),New.CaseNumber)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.CaseNumber <> New.CaseNumber
                                        OR (
                                             Old.CaseNumber IS NULL
                                             AND New.CaseNumber IS NOT NULL
                                           )
                                        OR (
                                             New.CaseNumber IS NULL
                                             AND Old.CaseNumber IS NOT NULL
                                           );  
                    END;  
                -- RequestedBy  
                IF UPDATE(RequestedBy)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,''RequestedBy''
                                       ,CONVERT(VARCHAR(MAX),Old.RequestedBy)
                                       ,CONVERT(VARCHAR(MAX),New.RequestedBy)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.RequestedBy <> New.RequestedBy
                                        OR (
                                             Old.RequestedBy IS NULL
                                             AND New.RequestedBy IS NOT NULL
                                           )
                                        OR (
                                             New.RequestedBy IS NULL
                                             AND Old.RequestedBy IS NOT NULL
                                           );  
                    END;  
                -- HaveBackup  
                IF UPDATE(HaveBackup)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,''HaveBackup''
                                       ,CONVERT(VARCHAR(MAX),Old.HaveBackup)
                                       ,CONVERT(VARCHAR(MAX),New.HaveBackup)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.HaveBackup <> New.HaveBackup
                                        OR (
                                             Old.HaveBackup IS NULL
                                             AND New.HaveBackup IS NOT NULL
                                           )
                                        OR (
                                             New.HaveBackup IS NULL
                                             AND Old.HaveBackup IS NOT NULL
                                           );  
                    END;  
                -- HaveClientConfirmation  
                IF UPDATE(HaveClientConfirmation)
                    BEGIN  
                        INSERT  INTO syAuditHistDetail
                                (
                                 AuditHistId
                                ,RowId
                                ,ColumnName
                                ,OldValue
                                ,NewValue 
                                )
                                SELECT  @AuditHistId
                                       ,New.StudentStatusChangeId
                                       ,''HaveClientConfirmation''
                                       ,CONVERT(VARCHAR(MAX),Old.HaveClientConfirmation)
                                       ,CONVERT(VARCHAR(MAX),New.HaveClientConfirmation)
                                FROM    Inserted AS New
                                FULL OUTER JOIN Deleted AS Old ON Old.StudentStatusChangeId = New.StudentStatusChangeId
                                WHERE   Old.HaveClientConfirmation <> New.HaveClientConfirmation
                                        OR (
                                             Old.HaveClientConfirmation IS NULL
                                             AND New.HaveClientConfirmation IS NOT NULL
                                           )
                                        OR (
                                             New.HaveClientConfirmation IS NULL
                                             AND Old.HaveClientConfirmation IS NOT NULL
                                           );  
                    END;  
            END;  
        SET NOCOUNT OFF;  
    END;  
--==========================================================================================  
-- END TRIGGER SyStudentStatusChanges_Audit_Update  
--==========================================================================================  
 
 
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_Fall05_12MonthEnrollment_ClockHourActivity_GetList]'
GO
IF OBJECT_ID(N'[dbo].[USP_Fall05_12MonthEnrollment_ClockHourActivity_GetList]', 'P') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[USP_Fall05_12MonthEnrollment_ClockHourActivity_GetList]
    @CampusId VARCHAR(50)
   ,@ProgId VARCHAR(4000) = NULL
   ,@StartDate DATETIME = NULL
   ,@EndDate DATETIME = NULL
   ,@OrderBy VARCHAR(100)
AS
    SELECT *
    INTO   #TempClock
    FROM   (
           SELECT
               --StuEnrollId,
                dbo.UDF_FormatSSN(SSN) AS SSN
               ,StudentNumber
               ,StudentName
               ,dt2.StuEnrollId
               ,(
                SELECT TOP 1 EnrollmentId
                FROM   arStuEnrollments A5
                WHERE  A5.StuEnrollId = dt2.StuEnrollId
                ) AS EnrollmentId
               ,ProgCode
               ,ProgramName
               ,PrgVerCode
               ,PrgVerDescrip
               ,PrgVerId
               ,ProgLength
               ,CONVERT(VARCHAR, StartDate, 101) AS StartDate
               ,HrsPerWeek
               -- Field 20
               ,NumberofWeeks
               -- Field 21
               ,CASE WHEN WeeksInReportingPeriod > 0 THEN CONVERT(DECIMAL(18, 2), ROUND(WeeksInReportingPeriod, 2))
                     ELSE CASE WHEN WeeksInReportingPeriod < 0 THEN 0
                               ELSE 0.00
                          END
                END AS WeeksInReportingPeriod
               -- Field 26
               -- Commented by Balaji on 9/23/2010
               --Case When WeeksInReportingPeriod>=0 Then Convert(decimal(18,2),Round(HrsPerWeek*WeeksInReportingPeriod,2)) Else Convert(decimal(18,2),Round(HrsPerWeek*NumberofWeeks,2)) End as ContactHours
               ,CASE WHEN WeeksInReportingPeriod > 0 THEN CAST(ROUND(HrsPerWeek * WeeksInReportingPeriod, 2) AS DECIMAL(18, 2))
                     ELSE 0.00
                END AS ContactHours
               ,(
                SELECT TOP 1 UnitTypeId
                FROM   arPrgVersions PV
                WHERE  PV.PrgVerId = PrgVerId
                ) AS UnitTypeId
           FROM (
                SELECT StuEnrollId
                      ,SSN
                      ,StudentNumber
                      ,StudentName
                      ,ProgCode
                      ,ProgramName
                      ,ProgLength
                      ,CONVERT(VARCHAR, StartDate, 101) AS StartDate
                      ,HrsPerWeek
                      ,ISNULL(CAST(ROUND(NumberOfWeeks, 2) AS DECIMAL(18, 2)), 0) AS NumberofWeeks
                                    --Cond1 - If Student started before 09/01/2009 (year may vary) and the number of weeks from Student Start Date exceeds the Report End Date
                                    --Then take the number of weeks from the StartDate to EndDate i.e get the number of weeks between September 1 2009 and Aug 31 2010.
                                    --	Case When (DATEDIFF(day,StartDate,@StartDate)>=1 and DateDiff(day,DATEADD(ww,NumberOfweeks,StartDate),@EndDate)<0) Then  DATEDIFF(WW,@StartDate,@EndDate) 
                                    --		 ELSE 
                                    --				--Condition 2: If condition 1 fails, check if the student started before 09/01/2009 and then get the difference between
                                    --				--The student start date and 09/01/2009 and subtract the weeks from WeeksInProgram.
                                    --				-- If the ouput is negative show zero
                                    --				Case When DATEDIFF(day,StartDate,@StartDate)>=1 Then (NumberOfweeks-DATEDIFF(WW,StartDate,@StartDate))
                                    --				ELSE
                                    --					Case When DATEADD(ww,NumberofWeeks,StartDate)>=@EndDate Then (NumberofWeeks-DATEDIFF(ww,@EndDate,DateAdd(ww,NumberofWeeks,StartDate))) Else NumberofWeeks End			
                                    --				End
                                    --		 End 
                                    --	as
                                    --	WeeksInReportingPeriod,
                      ,CASE
                            -- If Student started after the report start date and graduated in the date range
                            WHEN (
                                 StartDate >= @StartDate
                                 AND StartDate <= @EndDate
                                 AND EndOfProgramDate <= @EndDate
                                 ) THEN ISNULL(NumberOfWeeks, 0.00)
                            -- If Student started before the report start date and graduated after report end date
                            WHEN (
                                 StartDate < @StartDate
                                 AND EndOfProgramDate > @EndDate
                                 ) THEN ISNULL(DATEDIFF(ww, @StartDate, @EndDate), 0.00)
                            -- If Student started before the report start date
                            WHEN ( StartDate < @StartDate ) THEN ISNULL(NumberOfWeeks - DATEDIFF(ww, StartDate, @StartDate), 0.00)
                            -- If Student graduated after report end date
                            WHEN ( EndOfProgramDate > @EndDate ) THEN
                                ISNULL(NumberOfWeeks - ( DATEDIFF(WEEK, @EndDate, DATEADD(WEEK, NumberOfWeeks, StartDate))), 0.00)
                            ELSE 0.00
                       END AS WeeksInReportingPeriod
                      ,PrgVerCode
                      ,PrgVerDescrip
                      ,PrgVerId
                      ,ContactHours --,Case When WeeksInReportingPeriod>0 Then WeeksInReportingPeriod Else 0 End as WeeksInReportingPeriod1
                FROM   (
                       SELECT StuEnrollId
                             ,SSN
                             ,StudentNumber
                             ,StudentName
                             ,ProgCode
                             ,ProgramName
                             ,ProgLength
                             ,CONVERT(VARCHAR, StartDate, 101) AS StartDate
                             ,HrsPerWeek
                             ,CASE WHEN HrsPerWeek > 0 THEN ProgLength / HrsPerWeek
                                   ELSE NULL
                              END AS NumberofWeeks
                             ,DATEADD(   WEEK
                                        ,( CASE WHEN HrsPerWeek > 0 THEN ProgLength / HrsPerWeek
                                                ELSE NULL
                                           END
                                         )
                                        ,CONVERT(VARCHAR, StartDate, 101)
                                     ) AS EndOfProgramDate
                             ,PrgVerDescrip
                             ,PrgVerCode
                             ,PrgVerId
                             ,CONVERT(VARCHAR, ExpGradDate, 101) AS ExpGradDate
                             ,ContactHours
                       FROM   (
                              SELECT
                                  --t1.StudentId, 
                                         t1.SSN
                                        ,t1.StudentNumber
                                        ,t1.LastName + '', '' + t1.FirstName + '' '' + ISNULL(t1.MiddleName, '''') AS StudentName
                                        ,t8.ProgCode
                                        ,t8.ProgDescrip AS ProgramName
                                        ,t7.PrgVerDescrip
                                        ,t7.PrgVerCode
                                        ,t7.PrgVerId
                                        ,t7.Hours AS ProgLength
                                        ,t2.StartDate
                                        ,t2.ExpGradDate
                                        ,t2.StuEnrollId
                                        ,(
                                         SELECT SUM(PSD.total)
                                         FROM   arStudentSchedules SS
                                               ,arProgScheduleDetails PSD
                                         WHERE  StuEnrollId = t2.StuEnrollId
                                                AND SS.ScheduleId = PSD.ScheduleId
                                         ) AS HrsPerWeek
                                        --            (Select Top 1 AdjustedPresentDays from syStudentAttendanceSummary where StuEnrollId=t2.StuEnrollId
                                        --order by StudentAttendedDate Desc) as ContactHours
                                        ,(
                                         SELECT ISNULL(SUM(ActualDays), 0)
                                         FROM   dbo.syStudentAttendanceSummary sas
                                         WHERE  StuEnrollId = t2.StuEnrollId
                                                AND sas.StudentAttendedDate
                                                BETWEEN @StartDate AND @EndDate
                                         ) AS ContactHours
                              --ISNULL((Select SUM(PSD.total) as HrsPerWeek  from arProgSchedules PS, arProgScheduleDetails PSD, arStudentSchedules SS  Where t2.PrgVerId=PS.PrgVerId and t2.StuEnrollId =SS.StuEnrollId and 
                              --SS.ScheduleId =PS.ScheduleId And PS.ScheduleId=PSD.ScheduleId),0) as HrsPerWeek
                              FROM       dbo.adLeads t1
                              INNER JOIN arStuEnrollments t2 ON t1.StudentId = t2.StudentId
                              INNER JOIN syStatusCodes t5 ON t2.StatusCodeId = t5.StatusCodeId
                              INNER JOIN sySysStatus t6 ON t5.SysStatusId = t6.SysStatusId
                                                           AND t6.SysStatusId NOT IN ( 8 )
                              INNER JOIN arPrgVersions t7 ON t2.PrgVerId = t7.PrgVerId
                              INNER JOIN arPrograms t8 ON t7.ProgId = t8.ProgId
                              INNER JOIN arProgTypes t9 ON t7.ProgTypId = t9.ProgTypId
                              LEFT JOIN  arAttendTypes t10 ON t2.AttendTypeId = t10.AttendTypeId
                              WHERE      t2.CampusId = @CampusId
                                         AND (
                                             @ProgId IS NULL
                                             OR t8.ProgId IN (
                                                             SELECT Val
                                                             FROM   MultipleValuesForReportParameters(@ProgId, '','', 1)
                                                             )
                                             )
                                         AND t8.ACId = 5 -- Clock Hour Programs
                                         AND t2.StartDate <= @EndDate -- Check if Student started before the report end date
                                         AND StuEnrollId NOT IN -- Exclude students who are Dropped out/Transferred/Graduated/No Start 
                                             -- Who were Dropped or Graduated or LDA before 08/01/2009 StartDate
                                             (
                                             SELECT t1.StuEnrollId
                                             FROM   arStuEnrollments t1
                                                   ,syStatusCodes t2
                                             WHERE  t1.StatusCodeId = t2.StatusCodeId
                                                    AND StartDate <= @EndDate
                                                    AND -- Student started before the end date range
                                                 LTRIM(RTRIM(t1.CampusId)) = LTRIM(RTRIM(@CampusId))
                                                    AND (
                                                        @ProgId IS NULL
                                                        OR t8.ProgId IN (
                                                                        SELECT Val
                                                                        FROM   MultipleValuesForReportParameters(@ProgId, '','', 1)
                                                                        )
                                                        )
                                                    AND t2.SysStatusId IN ( 12, 14, 19, 8 ) -- Dropped out/Transferred/Graduated/No Start
                                                    -- Date Determined or ExpGradDate or LDA falls before Report Start Date
                                                    AND (
                                                        t1.DateDetermined < @StartDate
                                                        OR ExpGradDate < @StartDate
                                                        OR LDA < @StartDate
                                                        )
                                             )
                                         -- If Student is enrolled in only one program version and if that program version 
                                         -- happens to be a continuing ed program exclude the student
                                         AND t2.StudentId NOT IN (
                                                                 SELECT DISTINCT StudentId
                                                                 FROM   (
                                                                        SELECT   StudentId
                                                                                ,COUNT(*) AS RowCounter
                                                                        FROM     arStuEnrollments
                                                                        WHERE    PrgVerId IN (
                                                                                             SELECT PrgVerId
                                                                                             FROM   arPrgVersions
                                                                                             WHERE  IsContinuingEd = 1
                                                                                             )
                                                                        GROUP BY StudentId
                                                                        HAVING   COUNT(*) = 1
                                                                        ) dtStudent_ContinuingEd
                                                                 )
                                         -- If student is enrolled in program versions that belong to same program
                                         -- then ignore the second enrollment

                                         -- If student is only enrolled in one program show the program
                                         AND ( t2.StuEnrollId IN (
                                                                 SELECT     TOP 1 StuEnrollId
                                                                 FROM       arStuEnrollments se
                                                                 INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                                                 INNER JOIN dbo.sySysStatus ss ON ss.SysStatusId = sc.SysStatusId
                                                                 WHERE      StudentId IN (
                                                                                         SELECT DISTINCT StudentId
                                                                                         FROM   (
                                                                                                SELECT   StudentId
                                                                                                        ,A3.ProgId
                                                                                                        ,COUNT(*) AS RowCounter
                                                                                                FROM     arStuEnrollments A1
                                                                                                        ,arPrgVersions A2
                                                                                                        ,arPrograms A3
                                                                                                WHERE    A1.PrgVerId = A2.PrgVerId
                                                                                                         AND A2.ProgId = A3.ProgId
                                                                                                         AND A1.StudentId = t1.StudentId
                                                                                                GROUP BY StudentId
                                                                                                        ,A3.ProgId
                                                                                                HAVING   COUNT(*) > 1
                                                                                                ) dtStudentInMultiplePrograms
                                                                                         )
                                                                            AND 1 = ( CASE WHEN ss.SysStatusId IN ( 8 ) THEN 0
                                                                                           WHEN ss.SysStatusId IN ( 12, 14, 19 )
                                                                                                -- Dropped out/Transferred/Graduated/No Start
                                                                                                -- Date Determined or ExpGradDate or LDA falls before Report Start Date
                                                                                                AND (
                                                                                                    se.DateDetermined < @StartDate
                                                                                                    OR se.ExpGradDate < @StartDate
                                                                                                    OR se.LDA < @StartDate
                                                                                                    ) THEN 0
                                                                                           ELSE 1
                                                                                      END
                                                                                    )
                                                                 ORDER BY   StartDate
                                                                           ,EnrollDate ASC
                                                                 UNION
                                                                 SELECT StuEnrollId
                                                                 FROM   arStuEnrollments
                                                                 WHERE  StudentId IN (
                                                                                     SELECT DISTINCT StudentId
                                                                                     FROM   (
                                                                                            SELECT   StudentId
                                                                                                    ,A3.ProgId
                                                                                                    ,COUNT(*) AS RowCounter
                                                                                            FROM     arStuEnrollments A1
                                                                                                    ,arPrgVersions A2
                                                                                                    ,arPrograms A3
                                                                                            WHERE    A1.PrgVerId = A2.PrgVerId
                                                                                                     AND A2.ProgId = A3.ProgId
                                                                                                     AND A1.StudentId = t1.StudentId
                                                                                            GROUP BY StudentId
                                                                                                    ,A3.ProgId
                                                                                            HAVING   COUNT(*) = 1
                                                                                            ) dtStudentInOnePrograms
                                                                                     )
                                                                 )
                                             )
                              ) dt
                       ) dt1
                ) dt2
           ) dt3;


    --DECLARE @GrandTotal DECIMAL(18,2)
    --   ,@PrgVerTotal DECIMAL(18,2);
    --SET @GrandTotal = ( SELECT  SUM(ContactHours) AS TotalContactHours
    --                    FROM    #TempClock
    --                  );

    SELECT   ROW_NUMBER() OVER ( ORDER BY ProgCode
                                         ,ProgramName
                                         ,PrgVerCode
                                         ,PrgVerDescrip
                                         ,CASE WHEN @OrderBy = ''SSN'' THEN SSN
                                          END
                                         ,CASE WHEN @OrderBy = ''LastName'' THEN StudentName
                                          END
                                         ,CASE WHEN @OrderBy = ''student number'' THEN StudentNumber
                                          END
                                         ,CASE WHEN @OrderBy = ''enrollmentid'' THEN EnrollmentId
                                          END
                               ) AS RowID
            ,t1.SSN
            ,t1.StudentNumber
            ,t1.StudentName
            ,t1.StuEnrollId
            ,t1.EnrollmentId
            ,t1.ProgCode
            ,t1.ProgramName
            ,t1.PrgVerCode
            ,t1.PrgVerDescrip
            ,t1.PrgVerId
            ,t1.ProgLength
            ,t1.StartDate
            ,t1.HrsPerWeek
            ,t1.NumberofWeeks
            ,t1.WeeksInReportingPeriod
            ,CAST(CASE WHEN ( t1.UnitTypeId = ''A1389C74-0BB9-4BBF-A47F-68428BE7FA4D''
                            --OR t1.UnitTypeId = ''B937C92E-FD7A-455E-A731-527A9918C734''
                            ) THEN --Minutes 
                           CASE WHEN t1.ContactHours > 0 THEN t1.ContactHours / 60
                                ELSE t1.ContactHours
                           END
                       ELSE t1.ContactHours
                  END AS DECIMAL(18, 2)) AS ContactHours
            ,(
             SELECT CASE WHEN ( t1.UnitTypeId = ''A1389C74-0BB9-4BBF-A47F-68428BE7FA4D''
                              --OR t1.UnitTypeId = ''B937C92E-FD7A-455E-A731-527A9918C734''
                              ) THEN SUM(TC.ContactHours / 60)
                         ELSE SUM(TC.ContactHours)
                    END
             FROM   #TempClock TC
             WHERE  PrgVerId = t1.PrgVerId
             ) AS PrgVersion_SubTotal
            ,(
             SELECT COUNT(StudentName)
             FROM   #TempClock
             WHERE  PrgVerId = t1.PrgVerId
             ) AS StudentCount_SubTotal
            ,(
             SELECT CASE WHEN ( t1.UnitTypeId = ''A1389C74-0BB9-4BBF-A47F-68428BE7FA4D''
                              --OR t1.UnitTypeId = ''B937C92E-FD7A-455E-A731-527A9918C734''
                              ) THEN SUM(TC.ContactHours / 60)
                         ELSE SUM(TC.ContactHours)
                    END
             FROM   #TempClock TC
             ) AS GrandTotal
    FROM     #TempClock t1
    ORDER BY ProgCode
            ,ProgramName
            ,PrgVerCode
            ,PrgVerDescrip
            ,CASE WHEN @OrderBy = ''SSN'' THEN SSN
             END
            ,CASE WHEN @OrderBy = ''LastName'' THEN StudentName
             END
            ,CASE WHEN @OrderBy = ''student number'' THEN StudentNumber
             END
            ,CASE WHEN @OrderBy = ''enrollmentid'' THEN EnrollmentId
             END;
    DROP TABLE #TempClock;



'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_PostClockAttendance_Insert]'
GO
IF OBJECT_ID(N'[dbo].[USP_PostClockAttendance_Insert]', 'P') IS NULL
EXEC sp_executesql N'--=================================================================================================  
-- USP_PostClockAttendance_Insert  
--=================================================================================================  
CREATE PROCEDURE [dbo].[USP_PostClockAttendance_Insert]
    @AttValues NTEXT
AS
    BEGIN
        DECLARE @Error AS INTEGER;
        SET @Error = 0;

        BEGIN TRANSACTION AddMissingAttendanceRecords;
        BEGIN TRY

            IF @Error = 0
                BEGIN
                    DECLARE @hDoc INT;
                    DECLARE @modUser VARCHAR(50);

                    --Prepare input values as an XML document   
                    EXEC sp_xml_preparedocument @hDoc OUTPUT
                                               ,@AttValues;

                    -- temp table to find current default statuses per campus group  
                    DECLARE @CampusGroupStatuses TABLE
                        (
                            CampGrpId UNIQUEIDENTIFIER NOT NULL
                           ,StatusCodeId UNIQUEIDENTIFIER NOT NULL
                        );
                    INSERT INTO @CampusGroupStatuses
                                SELECT   pv.CampGrpId
                                        ,StatusCodeId = dbo.GetCurrentlyAttendingDefaultStatusIdForCampusGroup(pv.CampGrpId)
                                FROM     arStuEnrollments b
                                        ,dbo.arPrgVersions pv
                                WHERE    b.PrgVerId = pv.PrgVerId
                                         AND b.StuEnrollId IN (
                                                              SELECT StuEnrollId
                                                              FROM
                                                                     OPENXML(@hDoc, ''/NewDataSet/InsertPostClockAttendance'', 1)
                                                                         WITH (
                                                                              StuEnrollId VARCHAR(50)
                                                                              )
                                                              )
                                         AND b.StatusCodeId IN (
                                                               SELECT DISTINCT StatusCodeId
                                                               FROM   syStatusCodes
                                                               WHERE  SysStatusId = 7
                                                               )
                                GROUP BY pv.CampGrpId;

                    -- get Mod user from document  
                    SELECT @modUser = (
                                      SELECT TOP 1 ModUser
                                      FROM
                                             OPENXML(@hDoc, ''/NewDataSet/InsertPostClockAttendance'', 1)
                                                 WITH (
                                                      ModUser VARCHAR(50)
                                                      )
                                      );

                    -- Delete all records from arStudentClockAttendance table based on StuEnrollId,ScheduleId, RecordDate   
                    DELETE FROM arStudentClockAttendance
                    WHERE StuEnrollId IN (
                                         SELECT StuEnrollId
                                         FROM
                                                OPENXML(@hDoc, ''/NewDataSet/InsertPostClockAttendance'', 1)
                                                    WITH (
                                                         StuEnrollId VARCHAR(50)
                                                         )
                                         )
                          --AND ScheduleId IN (
                          --                  SELECT ScheduleId
                          --                  FROM
                          --                         OPENXML(@hDoc, ''/NewDataSet/InsertPostClockAttendance'', 1)
                          --                             WITH (
                          --                                  ScheduleId VARCHAR(50)
                          --                                  )
                          --                  )
                          AND RecordDate IN (
                                            SELECT RecordDate
                                            FROM
                                                   OPENXML(@hDoc, ''/NewDataSet/InsertPostClockAttendance'', 1)
                                                       WITH (
                                                            RecordDate VARCHAR(10)
                                                            )
                                            );


                    ---begin code by Balaji to insert into the summary table for progress reports   
                    DELETE FROM syStudentAttendanceSummary
                    WHERE StuEnrollId IN (
                                         SELECT StuEnrollId
                                         FROM
                                                OPENXML(@hDoc, ''/NewDataSet/InsertPostClockAttendance'', 1)
                                                    WITH (
                                                         StuEnrollId VARCHAR(50)
                                                         )
                                         )
                          AND StudentAttendedDate IN (
                                                     SELECT RecordDate
                                                     FROM
                                                            OPENXML(@hDoc, ''/NewDataSet/InsertPostClockAttendance'', 1)
                                                                WITH (
                                                                     RecordDate VARCHAR(10)
                                                                     )
                                                     );
                    IF EXISTS (
                              SELECT *
                              FROM   sysobjects
                              WHERE  type = ''TR''
                                     AND name = ''TR_InsertAttendanceSummary_ByDay_PA''
                              )
                        BEGIN
                            DISABLE TRIGGER TR_InsertAttendanceSummary_ByDay_PA ON arStudentClockAttendance;
                        END;
                    IF EXISTS (
                              SELECT *
                              FROM   sysobjects
                              WHERE  type = ''TR''
                                     AND name = ''TR_InsertAttendanceSummary_ByClockHour_Minutes''
                              )
                        BEGIN
                            DISABLE TRIGGER TR_InsertAttendanceSummary_ByClockHour_Minutes ON arStudentClockAttendance;
                        END;
                    ---end code by Balaji to insert into the summary table for progress reports  
                    -- Insert records into arStudentClockAttendance Table   

                    INSERT INTO arStudentClockAttendance (
                                                         StuEnrollId
                                                        ,ScheduleId
                                                        ,RecordDate
                                                        ,SchedHours
                                                        ,ActualHours
                                                        ,ModDate
                                                        ,ModUser
                                                        ,isTardy
                                                        ,PostByException
                                                        ,comments
                                                        ,TardyProcessed
                                                        ,Converted
                                                         )
                                SELECT StuEnrollId
                                      ,ScheduleId
                                      ,RecordDate
                                      ,SchedHours
                                      ,ActualHours
                                      ,ModDate
                                      ,ModUser
                                      ,IsTardy
                                      ,PostByException
                                      ,''''
                                      ,TardyProcessed
                                      ,0
                                FROM
                                       OPENXML(@hDoc, ''/NewDataSet/InsertPostClockAttendance'', 1)
                                           WITH (
                                                StuEnrollId UNIQUEIDENTIFIER
                                               ,ScheduleId UNIQUEIDENTIFIER
                                               ,RecordDate VARCHAR(10)
                                               ,SchedHours DECIMAL(18, 2)
                                               ,ActualHours DECIMAL(18, 2)
                                               ,ModDate VARCHAR(10)
                                               ,ModUser VARCHAR(50)
                                               ,IsTardy BIT
                                               ,PostByException VARCHAR(10)
                                               ,comments VARCHAR(240)
                                               ,TardyProcessed BIT
                                               ,Converted BIT
                                                );


                    -- Insert into history table 4/11/2018  
                    INSERT INTO dbo.syStudentStatusChanges (
                                                           StuEnrollId
                                                          ,OrigStatusId
                                                          ,NewStatusId
                                                          ,CampusId
                                                          ,ModDate
                                                          ,ModUser
                                                          ,IsReversal
                                                          ,DropReasonId
                                                          ,DateOfChange
                                                          ,Lda
                                                          ,CaseNumber
                                                          ,RequestedBy
                                                          ,HaveBackup
                                                          ,HaveClientConfirmation
                                                           )
                                SELECT StuEnrollId
                                      ,e.StatusCodeId OrigStatusId
                                      ,cgs.StatusCodeId NewStatusId
                                      ,e.CampusId CampusId
                                      ,GETDATE() ModDate
                                      ,@modUser ModUser
                                      ,0 IsReversal
                                      ,NULL DropReasonId
                                      ,GETDATE() DateOfChange
                                      ,NULL Lda
                                      ,NULL CaseNumber
                                      ,NULL RequestedBy
                                      ,NULL HaveBackup
                                      ,NULL HaveClientConfirmation
                                FROM   dbo.arStuEnrollments e
                                JOIN   dbo.adLeads l ON e.StudentId = l.StudentId
                                JOIN   dbo.arPrgVersions pv ON e.PrgVerId = pv.PrgVerId
                                JOIN   @CampusGroupStatuses cgs ON pv.CampGrpId = cgs.CampGrpId
                                WHERE  e.StuEnrollId IN (
                                                        SELECT StuEnrollId
                                                        FROM
                                                               OPENXML(@hDoc, ''/NewDataSet/InsertPostClockAttendance'', 1)
                                                                   WITH (
                                                                        StuEnrollId VARCHAR(50)
                                                                        )
                                                        )
                                       AND e.StatusCodeId IN (
                                                             SELECT DISTINCT StatusCodeId
                                                             FROM   syStatusCodes
                                                             WHERE  SysStatusId = 7
                                                             );

                    --update b set StatusCodeId=(select Distinct StatusCodeId from syStatusCodes where StatusCodeDescrip=''Currently Attending'')   
                    --from arStudentClockAttendance a,arStuEnrollments b where a.StuEnrollId=b.StuEnrollId and a.ActualHours<>9999.00 and a.ActualHours<>999.00   
                    --and a.StuEnrollId in (select StuEnrollId from OPENXML(@hDoc,''/NewDataSet/InsertPostClockAttendance'',1)   
                    --       with (StuEnrollId varchar(50)))   
                    --and b.StatusCodeId=(select Distinct StatusCodeId from syStatusCodes where StatusCodeDescrip=''Future Start'')   
                    --DE8240  Aug 15  
                    -- DE8192 OCt 05 2012  
                    --UPDATE  b  
                    --SET     StatusCodeId = (  
                    --                         SELECT TOP 1  
                    --                                StatusCodeId  
                    --                         FROM   syStatusCodes  
                    --                         WHERE  SysStatusId = 9  
                    --                                AND StatusId = ''F23DE1E2-D90A-4720-B4C7-0F6FB09C9965''  
                    --                       )  
                    --FROM    arStudentClockAttendance a  
                    --       ,arStuEnrollments b  
                    --WHERE   a.StuEnrollId = b.StuEnrollId  
                    --        AND a.ActualHours <> 9999.00  
                    --        AND a.ActualHours <> 999.00  
                    --        AND a.StuEnrollId IN ( SELECT   StuEnrollId  
                    --                               FROM     OPENXML(@hDoc,''/NewDataSet/InsertPostClockAttendance'',1)   
                    --WITH (StuEnrollId VARCHAR(50)) )  
                    --        AND b.StatusCodeId IN ( SELECT DISTINCT  
                    --                                        StatusCodeId  
                    --                                FROM    syStatusCodes  
                    --                                WHERE   SysStatusId = 7 );   
                    UPDATE b
                    SET    StatusCodeId = cgs.StatusCodeId
                    FROM   arStudentClockAttendance a
                    JOIN   arStuEnrollments b ON a.StuEnrollId = b.StuEnrollId
                    JOIN   arPrgVersions pv ON pv.PrgVerId = b.PrgVerId
                    JOIN   @CampusGroupStatuses cgs ON cgs.CampGrpId = pv.CampGrpId
                    WHERE  a.ActualHours <> 9999.00
                           AND a.ActualHours <> 999.00
                           AND a.StuEnrollId IN (
                                                SELECT StuEnrollId
                                                FROM
                                                       OPENXML(@hDoc, ''/NewDataSet/InsertPostClockAttendance'', 1)
                                                           WITH (
                                                                StuEnrollId VARCHAR(50)
                                                                )
                                                )
                           AND b.StatusCodeId IN (
                                                 SELECT DISTINCT StatusCodeId
                                                 FROM   syStatusCodes
                                                 WHERE  SysStatusId = 7
                                                 );




                    --update arStuEnrollments start date with expected start date  
                    UPDATE c
                    SET    StartDate = ExpStartDate
                    FROM   arStudentClockAttendance a
                          ,arStuEnrollments c
                    WHERE  a.StuEnrollId = c.StuEnrollId
                           AND a.StuEnrollId IN (
                                                SELECT StuEnrollId
                                                FROM
                                                       OPENXML(@hDoc, ''/NewDataSet/InsertPostClockAttendance'', 1)
                                                           WITH (
                                                                StuEnrollId VARCHAR(50)
                                                                )
                                                )
                           AND c.StartDate IS NULL;



                    --Select * from arAttUnitType  

                    DECLARE @StuEnrollId UNIQUEIDENTIFIER
                           ,@MeetDate DATETIME
                           ,@WeekDay VARCHAR(15)
                           ,@StartDate DATETIME
                           ,@EndDate DATETIME;
                    DECLARE @PeriodDescrip VARCHAR(50)
                           ,@Actual DECIMAL(18, 2)
                           ,@Excused DECIMAL(18, 2)
                           ,@ClsSectionId UNIQUEIDENTIFIER;
                    DECLARE @Absent DECIMAL(18, 2)
                           ,@ScheduledMinutes DECIMAL(18, 2)
                           ,@TardyMinutes DECIMAL(18, 2);
                    DECLARE @tardy DECIMAL(18, 2)
                           ,@tracktardies INT
                           ,@tardiesMakingAbsence INT
                           ,@PrgVerId UNIQUEIDENTIFIER
                           ,@rownumber INT
                           ,@IsTardy BIT
                           ,@ActualRunningScheduledHours DECIMAL(18, 2);
                    DECLARE @ActualRunningPresentHours DECIMAL(18, 2)
                           ,@ActualRunningAbsentHours DECIMAL(18, 2)
                           ,@ActualRunningTardyHours DECIMAL(18, 2)
                           ,@ActualRunningMakeupHours DECIMAL(18, 2);
                    DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
                           ,@intTardyBreakPoint INT
                           ,@AdjustedRunningPresentHours DECIMAL(18, 2)
                           ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
                           ,@ActualRunningScheduledDays DECIMAL(18, 2)
                           ,@MakeUpHours DECIMAL(18, 2);


                    DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER
                           ,@trackattendanceby VARCHAR(50)
                           ,@UnitTypeId UNIQUEIDENTIFIER;
                    SET @StuEnrollCampusId = (
                                             SELECT TOP 1 CampusId
                                             FROM   arStuEnrollments
                                             WHERE  StuEnrollId IN (
                                                                   SELECT StuEnrollId
                                                                   FROM
                                                                          OPENXML(@hDoc, ''/NewDataSet/InsertPostClockAttendance'', 1)
                                                                              WITH (
                                                                                   StuEnrollId VARCHAR(50)
                                                                                   )
                                                                   )
                                             );

                    SET @trackattendanceby = (
                                             SELECT dbo.GetAppSettingValue(72, @StuEnrollCampusId)
                                             );

                    SET @UnitTypeId = (
                                      SELECT TOP 1 t1.UnitTypeId
                                      FROM   arPrgVersions t1
                                            ,arStuEnrollments t2
                                      WHERE  t1.PrgVerId = t2.PrgVerId
                                             AND t2.StuEnrollId IN (
                                                                   SELECT StuEnrollId
                                                                   FROM
                                                                          OPENXML(@hDoc, ''/NewDataSet/InsertPostClockAttendance'', 1)
                                                                              WITH (
                                                                                   StuEnrollId VARCHAR(50)
                                                                                   )
                                                                   )
                                      );

                    -- by Class and P/A  
                    IF ( @UnitTypeId = ''EF5535C2-142C-4223-AE3C-25A50A153CC6'' )
                       AND LOWER(@trackattendanceby) = ''byclass''
                        BEGIN
                            PRINT ''Step 2'';

                            DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
                                SELECT   *
                                        ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                FROM     (
                                         SELECT DISTINCT t1.StuEnrollId
                                               ,t1.ClsSectionId
                                               ,t1.MeetDate
                                               ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                               ,t4.StartDate
                                               ,t4.EndDate
                                               ,t5.PeriodDescrip
                                               ,t1.Actual
                                               ,t1.Excused
                                               ,CASE WHEN (
                                                          t1.Actual = 0
                                                          AND t1.Excused = 0
                                                          ) THEN t1.Scheduled
                                                     ELSE CASE WHEN (
                                                                    t1.Actual <> 9999.00
                                                                    AND t1.Actual < t1.Scheduled
                                                                    AND t1.Excused <> 1
                                                                    ) THEN ( t1.Scheduled - t1.Actual )
                                                               ELSE 0
                                                          END
                                                END AS Absent
                                               ,t1.Scheduled AS ScheduledMinutes
                                               ,CASE WHEN (
                                                          t1.Actual > 0
                                                          AND t1.Actual < t1.Scheduled
                                                          ) THEN ( t1.Scheduled - t1.Actual )
                                                     ELSE 0
                                                END AS TardyMinutes
                                               ,t1.Tardy AS Tardy
                                               ,t3.TrackTardies
                                               ,t3.TardiesMakingAbsence
                                               ,t3.PrgVerId
                                         FROM   atClsSectAttendance t1
                                         INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                         INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                         INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                            AND (
                                                                                CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                                AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                                )
                                                                            AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added  
                                         INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                         INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                         INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                         INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                         INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                         WHERE  t1.StuEnrollId IN (
                                                                  SELECT StuEnrollId
                                                                  FROM
                                                                         OPENXML(@hDoc, ''/NewDataSet/InsertPostClockAttendance'', 1)
                                                                             WITH (
                                                                                  StuEnrollId VARCHAR(50)
                                                                                  )
                                                                  )
                                                AND (
                                                    t10.UnitTypeId IN ( ''2600592A-9739-4A13-BDCE-7A25FE4A7478'', ''EF5535C2-142C-4223-AE3C-25A50A153CC6'' )
                                                    OR t3.UnitTypeId IN ( ''2600592A-9739-4A13-BDCE-7A25FE4A7478'', ''EF5535C2-142C-4223-AE3C-25A50A153CC6'' )
                                                    ) -- Minutes  
                                                AND t1.Actual <> 9999
                                         ) dt
                                ORDER BY StuEnrollId
                                        ,MeetDate;
                            DECLARE @boolReset BIT
                                   ,@AdjustedPresentDaysComputed DECIMAL(18, 2);
                            OPEN GetAttendance_Cursor;
                            FETCH NEXT FROM GetAttendance_Cursor
                            INTO @StuEnrollId
                                ,@ClsSectionId
                                ,@MeetDate
                                ,@WeekDay
                                ,@StartDate
                                ,@EndDate
                                ,@PeriodDescrip
                                ,@Actual
                                ,@Excused
                                ,@Absent
                                ,@ScheduledMinutes
                                ,@TardyMinutes
                                ,@tardy
                                ,@tracktardies
                                ,@tardiesMakingAbsence
                                ,@PrgVerId
                                ,@rownumber;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @boolReset = 0;
                            SET @MakeUpHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeUpHours = 0;
                                            SET @boolReset = 1;
                                        END;


                                    -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day  
                                    IF @Actual <> 9999.00
                                        BEGIN
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual + @Excused;
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual + @Excused;

                                            -- If there are make up hrs deduct that otherwise it will be added again in progress report  
                                            IF (
                                               @Actual > 0
                                               AND @Actual > @ScheduledMinutes
                                               AND @Actual <> 9999.00
                                               )
                                                BEGIN
                                                    SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                END;

                                            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                                        END;

                                    -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day  
                                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                                    SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                                    -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day     
                                    IF (
                                       @Actual > 0
                                       AND @Actual < @ScheduledMinutes
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                        END;

                                    -- Track how many days student has been tardy only when   
                                    -- program version requires to track tardy  
                                    IF (
                                       @tracktardies = 1
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;


                                    -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches  
                                    -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that  
                                    -- when student is tardy the second time, that second occurance will be considered as  
                                    -- absence  
                                    -- Variable @intTardyBreakpoint tracks how many times the student was tardy  
                                    -- Variable @tardiesMakingAbsence tracks the tardy rule  
                                    IF (
                                       @tracktardies = 1
                                       AND @intTardyBreakPoint = @tardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
                                            SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes  
                                            SET @intTardyBreakPoint = 0;
                                        END;

                                    -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours                    
                                    IF (
                                       @Actual > 0
                                       AND @Actual > @ScheduledMinutes
                                       AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeUpHours = @MakeUpHours + ( @Actual - @ScheduledMinutes );
                                        END;

                                    IF ( @tracktardies = 1 )
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)  
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                                        END;

                                    DELETE FROM syStudentAttendanceSummary
                                    WHERE StuEnrollId = @StuEnrollId
                                          AND ClsSectionId = @ClsSectionId
                                          AND StudentAttendedDate = @MeetDate;
                                    INSERT INTO syStudentAttendanceSummary (
                                                                           StuEnrollId
                                                                          ,ClsSectionId
                                                                          ,StudentAttendedDate
                                                                          ,ScheduledDays
                                                                          ,ActualDays
                                                                          ,ActualRunningScheduledDays
                                                                          ,ActualRunningPresentDays
                                                                          ,ActualRunningAbsentDays
                                                                          ,ActualRunningMakeupDays
                                                                          ,ActualRunningTardyDays
                                                                          ,AdjustedPresentDays
                                                                          ,AdjustedAbsentDays
                                                                          ,AttendanceTrackType
                                                                          ,ModUser
                                                                          ,ModDate
                                                                          ,tardiesmakingabsence
                                                                           )
                                    VALUES ( @StuEnrollId
                                            ,@ClsSectionId
                                            ,@MeetDate
                                            ,@ScheduledMinutes
                                            ,@Actual
                                            ,@ActualRunningScheduledDays
                                            ,@ActualRunningPresentHours
                                            ,@ActualRunningAbsentHours
                                            ,ISNULL(@MakeUpHours, 0)
                                            ,@ActualRunningTardyHours
                                            ,@AdjustedPresentDaysComputed
                                            ,@AdjustedRunningAbsentHours
                                            ,''Post Attendance by Class Min''
                                            ,''sa''
                                            ,GETDATE()
                                            ,@tardiesMakingAbsence
                                           );

                                    --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId  
                                    SET @PrevStuEnrollId = @StuEnrollId;

                                    FETCH NEXT FROM GetAttendance_Cursor
                                    INTO @StuEnrollId
                                        ,@ClsSectionId
                                        ,@MeetDate
                                        ,@WeekDay
                                        ,@StartDate
                                        ,@EndDate
                                        ,@PeriodDescrip
                                        ,@Actual
                                        ,@Excused
                                        ,@Absent
                                        ,@ScheduledMinutes
                                        ,@TardyMinutes
                                        ,@tardy
                                        ,@tracktardies
                                        ,@tardiesMakingAbsence
                                        ,@PrgVerId
                                        ,@rownumber;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
                        END;
                    --select * from arAttUnitType  
                    --else if (@UnitTypeId=''EF5535C2-142C-4223-AE3C-25A50A153CC6'') and lower(@trackattendanceby)<>''byclass''  
                    -- begin  
                    --  DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR  
                    --  Select t1.StuEnrollId,t1.RecordDate,t1.ActualHours,t1.SchedHours,  
                    --    Case when ((t1.SchedHours >= 1 and t1.SchedHours not in (999,9999)) and t1.ActualHours=0)  Then t1.SchedHours else 0 end as Absent,  
                    --    t1.IsTardy,  
                    --      t3.TrackTardies,  
                    --    t3.tardiesMakingAbsence  
                    --  from  
                    --   arStudentClockAttendance t1 inner join arStuEnrollments t2 on t1.StuEnrollId=t2.StuEnrollId   
                    --   inner join arPrgVersions t3 on t2.PrgVerId=t3.PrgVerId   
                    --  where  
                    --    t1.StuEnrollId IN (  
                    --                                SELECT  StuEnrollId  
                    --                                FROM    OPENXML(@hDoc,''/NewDataSet/InsertPostClockAttendance'',1)   
                    --         WITH (StuEnrollId VARCHAR(50))) and  
                    --   t3.UnitTypeId in (''ef5535c2-142c-4223-ae3c-25a50a153cc6'') and t1.ActualHours <> 9999.00  
                    --  order by t1.StuEnrollId, t1.RecordDate  
                    --OPEN GetAttendance_Cursor  
                    --Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)  
                    --FETCH NEXT FROM GetAttendance_Cursor INTO    
                    --@StuEnrollId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,  
                    --@IsTardy,  
                    --@TrackTardies,@tardiesMakingAbsence  
                    ----select * from arAttUnitType  
                    --set @ActualRunningPresentHours=0  
                    --set @ActualRunningPresentHours=0  
                    --set @ActualRunningAbsentHours=0  
                    --set @ActualRunningTardyHours=0  
                    --set @ActualRunningMakeupHours=0  
                    --set @intTardyBreakPoint=0  
                    --set @AdjustedRunningPresentHours=0  
                    --set @AdjustedRunningAbsentHours=0  
                    --set @ActualRunningScheduledDays=0  
                    --set @MakeupHours=0  
                    --WHILE @@FETCH_STATUS = 0  
                    --BEGIN  

                    --  if @PrevStuEnrollId <> @StuEnrollId   
                    --  begin  
                    --    set @ActualRunningPresentHours = 0  
                    --    set @ActualRunningAbsentHours = 0  
                    --    set @intTardyBreakPoint = 0  
                    --    set @ActualRunningTardyHours = 0  
                    --    set @AdjustedRunningPresentHours = 0  
                    --    set @AdjustedRunningAbsentHours = 0  
                    --    set @ActualRunningScheduledDays = 0  
                    --    set @MakeupHours=0  
                    --  end  

                    --  if (@Actual <> 9999 and @Actual <> 999)  
                    --  begin  
                    --   set @ActualRunningScheduledDays = IsNULL(@ActualRunningScheduledDays,0) +  @ScheduledMinutes  
                    --   set @ActualRunningPresentHours = isNULL(@ActualRunningPresentHours,0) + @Actual  
                    --   set @AdjustedRunningPresentHours = IsNULL(@AdjustedRunningPresentHours,0) + @Actual  
                    --  end  
                    --  set @ActualRunningAbsentHours = IsNULL(@ActualRunningAbsentHours,0) + @Absent  
                    --      if (@Actual=0 and @ScheduledMinutes >= 1 and @ScheduledMinutes not in (999,9999))   
                    --  begin  
                    --   set @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent  
                    --  end  
                    --  if (@tracktardies=1 and @IsTardy=1)  
                    --  begin  
                    --   set @ActualRunningTardyHours = @ActualRunningTardyHours + 1  
                    --  end  
                    --  --commented by balaji on 10/22/2012 as report (rdl) doesn''t add days attended and make up days  
                    --   -- If there are make up hrs deduct that otherwise it will be added again in progress report  
                    --          IF (@Actual > 0 AND @Actual > @ScheduledMinutes and @Actual<>9999.00)   
                    --     BEGIN  
                    --      SET @ActualRunningPresentHours = @ActualRunningPresentHours - (@Actual - @ScheduledMinutes)  
                    --      SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - (@Actual - @ScheduledMinutes)  
                    --     END  
                    --  if @tracktardies=1 and (@TardyMinutes > 0 or @IsTardy=1)  
                    --  begin  
                    --   set @intTardyBreakPoint = @intTardyBreakPoint + 1  
                    --  end       

                    --  -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours                    
                    --         IF (@Actual > 0 AND @Actual > @ScheduledMinutes and @Actual<>9999.00)   
                    --                BEGIN  
                    --                    SET @MakeupHours = @MakeupHours + (@Actual - @ScheduledMinutes)  
                    --                END  

                    --  if (@tracktardies=1 and @intTardyBreakPoint=@tardiesMakingAbsence)  
                    --  begin  
                    --   set @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual  
                    --   set @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes --@TardyMinutes  
                    --   set @intTardyBreakPoint=0  
                    --  end  


                    --  Delete from syStudentAttendanceSummary where StuEnrollId=@StuEnrollId and StudentAttendedDate=@MeetDate  
                    --  insert into syStudentAttendanceSummary  
                    --     (StuEnrollId,ClsSectionId,StudentAttendedDate,ScheduledDays,ActualDays,  
                    --     ActualRunningScheduledDays,  
                    --     ActualRunningPresentDays,  
                    --     ActualRunningAbsentDays,  
                    --     ActualRunningMakeupDays,  
                    --     ActualRunningTardyDays,  
                    --     AdjustedPresentDays,  
                    --     AdjustedAbsentDays,AttendanceTrackType,  
                    --     ModUser,ModDate,TardiesMakingAbsence)  
                    --    values  
                    --     (@StuEnrollId,@ClsSectionId,@MeetDate,IsNULL(@ScheduledMinutes,0),@Actual,IsNULL(@ActualRunningScheduledDays,0),IsNULL(@ActualRunningPresentHours,0),  
                    --     IsNULL(@ActualRunningAbsentHours,0),IsNULL(@MakeupHours,0),IsNULL(@ActualRunningTardyHours,0),isNULL(@AdjustedRunningPresentHours,0),  
                    --     isNULL(@AdjustedRunningAbsentHours,0),''Post Attendance by Class'',''sa'',getdate(),@TardiesMakingAbsence)  

                    --  --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId  
                    --   --end  
                    --    set @PrevStuEnrollId=@StuEnrollId   
                    --FETCH NEXT FROM GetAttendance_Cursor INTO    
                    --@StuEnrollId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,  
                    --@IsTardy,  
                    --@TrackTardies,@tardiesMakingAbsence  
                    --END  
                    --CLOSE GetAttendance_Cursor  
                    --DEALLOCATE GetAttendance_Cursor  

                    -- end  



                    -- by (minutes or clock hour) and P/A  
                    BEGIN
                        DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
                            SELECT   *
                                    ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                            FROM     (
                                     SELECT DISTINCT t1.StuEnrollId
                                           ,t1.ClsSectionId
                                           ,t1.MeetDate
                                           ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                           ,t4.StartDate
                                           ,t4.EndDate
                                           ,t5.PeriodDescrip
                                           ,t1.Actual
                                           ,t1.Excused
                                           ,CASE WHEN (
                                                      t1.Actual = 0
                                                      AND t1.Excused = 0
                                                      ) THEN t1.Scheduled
                                                 ELSE CASE WHEN (
                                                                t1.Actual <> 9999.00
                                                                AND t1.Actual < t1.Scheduled
                                                                ) THEN ( t1.Scheduled - t1.Actual )
                                                           ELSE 0
                                                      END
                                            END AS Absent
                                           ,t1.Scheduled AS ScheduledMinutes
                                           ,CASE WHEN (
                                                      t1.Actual > 0
                                                      AND t1.Actual < t1.Scheduled
                                                      ) THEN ( t1.Scheduled - t1.Actual )
                                                 ELSE 0
                                            END AS TardyMinutes
                                           ,t1.Tardy AS Tardy
                                           ,t3.TrackTardies
                                           ,t3.TardiesMakingAbsence
                                           ,t3.PrgVerId
                                     FROM   atClsSectAttendance t1
                                     INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                     INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                     INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                        AND (
                                                                            CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                            AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                            )
                                                                        AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added  
                                     INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                     INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                     INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                     INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                     INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                     WHERE  t1.StuEnrollId IN (
                                                              SELECT StuEnrollId
                                                              FROM
                                                                     OPENXML(@hDoc, ''/NewDataSet/InsertPostClockAttendance'', 1)
                                                                         WITH (
                                                                              StuEnrollId VARCHAR(50)
                                                                              )
                                                              )
                                            AND (
                                                t10.UnitTypeId IN ( ''A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'', ''B937C92E-FD7A-455E-A731-527A9918C734'' )
                                                OR t3.UnitTypeId IN ( ''A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'', ''B937C92E-FD7A-455E-A731-527A9918C734'' )
                                                ) -- Minutes  
                                            AND t1.Actual <> 9999
                                     ) dt
                            ORDER BY StuEnrollId
                                    ,MeetDate;
                        OPEN GetAttendance_Cursor;
                        FETCH NEXT FROM GetAttendance_Cursor
                        INTO @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@WeekDay
                            ,@StartDate
                            ,@EndDate
                            ,@PeriodDescrip
                            ,@Actual
                            ,@Excused
                            ,@Absent
                            ,@ScheduledMinutes
                            ,@TardyMinutes
                            ,@tardy
                            ,@tracktardies
                            ,@tardiesMakingAbsence
                            ,@PrgVerId
                            ,@rownumber;
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @ActualRunningMakeupHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        SET @boolReset = 0;
                        SET @MakeUpHours = 0;
                        WHILE @@FETCH_STATUS = 0
                            BEGIN

                                IF @PrevStuEnrollId <> @StuEnrollId
                                    BEGIN
                                        SET @ActualRunningPresentHours = 0;
                                        SET @ActualRunningAbsentHours = 0;
                                        SET @intTardyBreakPoint = 0;
                                        SET @ActualRunningTardyHours = 0;
                                        SET @AdjustedRunningPresentHours = 0;
                                        SET @AdjustedRunningAbsentHours = 0;
                                        SET @ActualRunningScheduledDays = 0;
                                        SET @MakeUpHours = 0;
                                        SET @boolReset = 1;
                                    END;


                                -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day  
                                IF @Actual <> 9999.00
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;

                                        -- If there are make up hrs deduct that otherwise it will be added again in progress report  
                                        IF (
                                           @Actual > 0
                                           AND @Actual > @ScheduledMinutes
                                           AND @Actual <> 9999.00
                                           )
                                            BEGIN
                                                SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                            END;

                                        SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                                    END;

                                -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day  
                                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                                -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day     
                                IF (
                                   @Actual > 0
                                   AND @Actual < @ScheduledMinutes
                                   AND @tardy = 1
                                   )
                                    BEGIN
                                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                    END;

                                -- Track how many days student has been tardy only when   
                                -- program version requires to track tardy  
                                IF (
                                   @tracktardies = 1
                                   AND @tardy = 1
                                   )
                                    BEGIN
                                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                    END;


                                -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches  
                                -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that  
                                -- when student is tardy the second time, that second occurance will be considered as  
                                -- absence  
                                -- Variable @intTardyBreakpoint tracks how many times the student was tardy  
                                -- Variable @tardiesMakingAbsence tracks the tardy rule  
                                IF (
                                   @tracktardies = 1
                                   AND @intTardyBreakPoint = @tardiesMakingAbsence
                                   )
                                    BEGIN
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                        SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes  
                                        SET @intTardyBreakPoint = 0;
                                    END;

                                -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours                    
                                IF (
                                   @Actual > 0
                                   AND @Actual > @ScheduledMinutes
                                   AND @Actual <> 9999.00
                                   )
                                    BEGIN
                                        SET @MakeUpHours = @MakeUpHours + ( @Actual - @ScheduledMinutes );
                                    END;

                                IF ( @tracktardies = 1 )
                                    BEGIN
                                        SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)  
                                    END;
                                ELSE
                                    BEGIN
                                        SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                                    END;

                                DELETE FROM syStudentAttendanceSummary
                                WHERE StuEnrollId = @StuEnrollId
                                      AND ClsSectionId = @ClsSectionId
                                      AND StudentAttendedDate = @MeetDate;
                                INSERT INTO syStudentAttendanceSummary (
                                                                       StuEnrollId
                                                                      ,ClsSectionId
                                                                      ,StudentAttendedDate
                                                                      ,ScheduledDays
                                                                      ,ActualDays
                                                                      ,ActualRunningScheduledDays
                                                                      ,ActualRunningPresentDays
                                                                      ,ActualRunningAbsentDays
                                                                      ,ActualRunningMakeupDays
                                                                      ,ActualRunningTardyDays
                                                                      ,AdjustedPresentDays
                                                                      ,AdjustedAbsentDays
                                                                      ,AttendanceTrackType
                                                                      ,ModUser
                                                                      ,ModDate
                                                                      ,tardiesmakingabsence
                                                                       )
                                VALUES ( @StuEnrollId
                                        ,@ClsSectionId
                                        ,@MeetDate
                                        ,@ScheduledMinutes
                                        ,@Actual
                                        ,@ActualRunningScheduledDays
                                        ,@ActualRunningPresentHours
                                        ,@ActualRunningAbsentHours
                                        ,ISNULL(@MakeUpHours, 0)
                                        ,@ActualRunningTardyHours
                                        ,@AdjustedPresentDaysComputed
                                        ,@AdjustedRunningAbsentHours
                                        ,''Post Attendance by Class Min''
                                        ,''sa''
                                        ,GETDATE()
                                        ,@tardiesMakingAbsence
                                       );

                                --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId  
                                SET @PrevStuEnrollId = @StuEnrollId;

                                FETCH NEXT FROM GetAttendance_Cursor
                                INTO @StuEnrollId
                                    ,@ClsSectionId
                                    ,@MeetDate
                                    ,@WeekDay
                                    ,@StartDate
                                    ,@EndDate
                                    ,@PeriodDescrip
                                    ,@Actual
                                    ,@Excused
                                    ,@Absent
                                    ,@ScheduledMinutes
                                    ,@TardyMinutes
                                    ,@tardy
                                    ,@tracktardies
                                    ,@tardiesMakingAbsence
                                    ,@PrgVerId
                                    ,@rownumber;
                            END;
                        CLOSE GetAttendance_Cursor;
                        DEALLOCATE GetAttendance_Cursor;
                    END;

                    -- DECLARE GetAttendance_Cursor CURSOR  
                    -- FOR  
                    --     SELECT  t1.StuEnrollId ,  
                    --             NULL AS ClsSectionId ,  
                    --             t1.RecordDate AS MeetDate ,  
                    --             t1.ActualHours ,  
                    --             t1.SchedHours AS ScheduledMinutes ,  
                    --             CASE WHEN ( ( t1.SchedHours > 1  
                    --                           AND t1.SchedHours NOT IN ( 999, 9999 )  
                    --                         )  
                    --                         AND t1.ActualHours = 0  
                    --                       ) THEN t1.SchedHours  
                    --                  ELSE 0  
                    --             END AS Absent ,  
                    --             t1.IsTardy ,  
                    --             ( SELECT    SUM(SchedHours)  
                    --               FROM      arStudentClockAttendance  
                    --               WHERE     StuEnrollId = t1.StuEnrollId  
                    --                         AND RecordDate <= t1.RecordDate  
                    --                         AND ( t1.SchedHours > 1  
                    --                               AND t1.SchedHours NOT IN ( 999, 9999 )  
                    --                             )  
                    --             ) AS ActualRunningScheduledHours ,  
                    --             ( SELECT    SUM(ActualHours)  
                    --               FROM      arStudentClockAttendance  
                    --               WHERE     StuEnrollId = t1.StuEnrollId  
                    --                         AND RecordDate <= t1.RecordDate  
                    --                         AND ( t1.SchedHours > 1  
                    --                               AND t1.SchedHours NOT IN ( 999, 9999 )  
                    --                             )  
                    --                         AND ActualHours > 1  
                    --             ) AS ActualRunningPresentHours ,  
                    --             ( SELECT    COUNT(ActualHours)  
                    --               FROM      arStudentClockAttendance  
                    --               WHERE     StuEnrollId = t1.StuEnrollId  
                    --                         AND RecordDate <= t1.RecordDate  
                    --                         AND ( t1.SchedHours > 1  
                    --                               AND t1.SchedHours NOT IN ( 999, 9999 )  
                    --                             )  
                    --                         AND ActualHours = 0  
                    --             ) AS ActualRunningAbsentHours ,  
                    --             ( SELECT    SUM(ActualHours)  
                    --               FROM      arStudentClockAttendance  
                    --               WHERE     StuEnrollId = t1.StuEnrollId  
                    --                         AND RecordDate <= t1.RecordDate  
                    --                         AND SchedHours = 0  
                    --                         AND ActualHours > 1  
                    --             ) AS ActualRunningMakeupHours ,  
                    --             ( SELECT    SUM(SchedHours - ActualHours)  
                    --               FROM      arStudentClockAttendance  
                    --               WHERE     StuEnrollId = t1.StuEnrollId  
                    --                         AND RecordDate <= t1.RecordDate  
                    --                         AND ( ( t1.SchedHours > 1  
                    --                                 AND t1.SchedHours NOT IN ( 999, 9999 )  
                    --                               )  
                    --                               AND ActualHours > 1  
                    --                             )  
                    --             ) AS ActualRunningTardyHours ,  
                    --             t3.TrackTardies ,  
                    --             t3.tardiesMakingAbsence ,  
                    --             t3.PrgVerId ,  
                    --             ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber  
                    --     FROM    arStudentClockAttendance t1  
                    --             INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId  
                    --             INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId  
                    --     WHERE   t1.StuEnrollId IN (  
                    --             SELECT  StuEnrollId  
                    --             FROM    OPENXML(@hDoc,''/NewDataSet/InsertPostClockAttendance'',1)   
                    --      WITH (StuEnrollId VARCHAR(50)) )  
                    --             AND -- StuEnrollId goes in here  
                    --             t3.UnitTypeId IN ( ''A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'')  
                    --             AND t1.ActualHours <> 9999.00  
                    -- OPEN GetAttendance_Cursor  

                    -- FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId, @ClsSectionId,  
                    --     @MeetDate, @Actual, @ScheduledMinutes, @Absent, @IsTardy,  
                    --     @ActualRunningScheduledHours, @ActualRunningPresentHours,  
                    --     @ActualRunningAbsentHours, @ActualRunningMakeupHours,  
                    --     @ActualRunningTardyHours, @TrackTardies, @tardiesMakingAbsence,  
                    --     @PrgVerId, @RowNumber  

                    -- SET @ActualRunningPresentHours = 0  
                    -- SET @ActualRunningPresentHours = 0  
                    -- SET @ActualRunningAbsentHours = 0  
                    -- SET @ActualRunningTardyHours = 0  
                    -- SET @ActualRunningMakeupHours = 0  
                    -- SET @intTardyBreakPoint = 0  
                    -- SET @AdjustedRunningPresentHours = 0  
                    -- SET @AdjustedRunningAbsentHours = 0  
                    -- SET @ActualRunningScheduledDays = 0  
                    -- WHILE @@FETCH_STATUS = 0   
                    --     BEGIN  

                    --         IF @PrevStuEnrollId <> @StuEnrollId   
                    --             BEGIN  
                    --                 SET @ActualRunningPresentHours = 0  
                    --            SET @ActualRunningAbsentHours = 0  
                    --                 SET @intTardyBreakPoint = 0  
                    --                 SET @ActualRunningTardyHours = 0  
                    --                 SET @AdjustedRunningPresentHours = 0  
                    --                 SET @AdjustedRunningAbsentHours = 0  
                    --                 SET @ActualRunningScheduledDays = 0  
                    --             END  

                    --         SET @ActualRunningScheduledDays = @ActualRunningScheduledDays  
                    --             + @ScheduledMinutes  
                    --         SET @ActualRunningPresentHours = @ActualRunningPresentHours  
                    --             + @Actual  
                    --         SET @ActualRunningAbsentHours = @ActualRunningAbsentHours  
                    --             + @Absent  
                    --         SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours  
                    --             + @Actual  
                    --         SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours  
                    --             + @Absent   
                    --         IF ( @Actual > 0  
                    --              AND @Actual < @ScheduledMinutes  
                    --            )   
                    --             BEGIN  
                    --                 SET @ActualRunningTardyHours = @ActualRunningTardyHours  
                    --                     + ( @ScheduledMinutes - @Actual )  
                    --             END  

                    --         IF @tracktardies = 1  
                    --             AND @TardyMinutes > 0   
                    --             BEGIN  
                    --                 SET @intTardyBreakPoint = @intTardyBreakPoint + 1  
                    --             END   
                    --         IF ( @tracktardies = 1  
                    --              AND @intTardyBreakPoint = @tardiesMakingAbsence  
                    --            )   
                    --             BEGIN  
                    --                 SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours  
                    --                     - @Actual  
                    --                 SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours  
                    --                     + @ScheduledMinutes --@TardyMinutes  
                    --                 SET @intTardyBreakPoint = 0  
                    --             END  

                    --         INSERT  INTO syStudentAttendanceSummary  
                    --                 ( StuEnrollId ,  
                    --                   ClsSectionId ,  
                    --                   StudentAttendedDate ,  
                    --                   ScheduledDays ,  
                    --                   ActualDays ,  
                    --                   ActualRunningScheduledDays ,  
                    --                   ActualRunningPresentDays ,  
                    --                   ActualRunningAbsentDays ,  
                    --                   ActualRunningMakeupDays ,  
                    --                   ActualRunningTardyDays ,  
                    --                   AdjustedPresentDays ,  
                    --                   AdjustedAbsentDays ,  
                    --                   AttendanceTrackType ,  
                    --                   ModUser ,  
                    --                   ModDate  
                    --                 )  
                    --         VALUES  ( @StuEnrollId ,  
                    --                   @ClsSectionId ,  
                    --                   @MeetDate ,  
                    --                   @ScheduledMinutes ,  
                    --                   @Actual ,  
                    --                   @ActualRunningScheduledDays ,  
                    --                   @ActualRunningPresentHours ,  
                    --                   @ActualRunningAbsentHours ,  
                    --                   0 ,  
                    --                   @ActualRunningTardyHours ,  
                    --                   @AdjustedRunningPresentHours ,  
                    --                   @AdjustedRunningAbsentHours ,  
                    --                   ''Post Attendance by Day'' ,  
                    --                   ''sa'' ,  
                    --                   GETDATE()  
                    --                 )  
                    ----end  
                    --         SET @PrevStuEnrollId = @StuEnrollId   

                    --         FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,  
                    --             @ClsSectionId, @MeetDate, @Actual, @ScheduledMinutes, @Absent,  
                    --             @IsTardy, @ActualRunningScheduledHours,  
                    --             @ActualRunningPresentHours, @ActualRunningAbsentHours,  
                    --             @ActualRunningMakeupHours, @ActualRunningTardyHours,  
                    --             @TrackTardies, @tardiesMakingAbsence, @PrgVerId, @RowNumber  
                    --     END  
                    -- CLOSE GetAttendance_Cursor  
                    -- DEALLOCATE GetAttendance_Cursor  

                    IF EXISTS (
                              SELECT *
                              FROM   sysobjects
                              WHERE  type = ''TR''
                                     AND name = ''TR_InsertAttendanceSummary_ByDay_PA''
                              )
                        BEGIN
                            ENABLE TRIGGER TR_InsertAttendanceSummary_ByDay_PA ON arStudentClockAttendance;
                        END;
                    IF EXISTS (
                              SELECT *
                              FROM   sysobjects
                              WHERE  type = ''TR''
                                     AND name = ''TR_InsertAttendanceSummary_ByClockHour_Minutes''
                              )
                        BEGIN
                            ENABLE TRIGGER TR_InsertAttendanceSummary_ByClockHour_Minutes ON arStudentClockAttendance;
                        END;
                    ---begin code by Balaji to insert into the summary table for progress reports  
                    EXEC sp_xml_removedocument @hDoc;
                    IF ( @@ERROR > 0 )
                        BEGIN
                            SET @Error = @@ERROR;
                        END;
                END;

        END TRY
        BEGIN CATCH
            DECLARE @msg NVARCHAR(MAX);
            DECLARE @severity INT;
            DECLARE @state INT;
            SELECT @msg = ERROR_MESSAGE()
                  ,@severity = ERROR_SEVERITY()
                  ,@state = ERROR_STATE();
            RAISERROR(@msg, @severity, @state);
            SET @Error = 1;
        END CATCH;

        IF ( @Error = 0 )
            BEGIN
                COMMIT TRANSACTION AddMissingAttendanceRecords;
            END;
        ELSE
            BEGIN
                ROLLBACK TRANSACTION AddMissingAttendanceRecords;
            END;
    END;
--=================================================================================================  
-- END  --  USP_PostClockAttendance_Insert  
--=================================================================================================  
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_ProcessPaymentPeriodsHours]'
GO
IF OBJECT_ID(N'[dbo].[USP_ProcessPaymentPeriodsHours]', 'P') IS NULL
EXEC sp_executesql N'--================================================================================================= 
-- USP_ProcessPaymentPeriodsHours 
--================================================================================================= 
-- ============================================= 
-- Author:		Ginzo, John 
-- Create date: 11/17/2014 
-- ============================================= 
CREATE PROCEDURE [dbo].[USP_ProcessPaymentPeriodsHours]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from 
        -- interfering with SELECT statements. 
        SET NOCOUNT ON;


        BEGIN TRAN PAYMENTPERIODTRANSACTION;

        ----------------------------------------------------------------- 
        --Table to store Student population 
        ----------------------------------------------------------------- 
        DECLARE @MasterStudentPopulation TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ClsSectionId VARCHAR(250)
               ,MaxDate DATETIME
               ,ScheduledHours DECIMAL
               ,MakeUpHours DECIMAL
               ,ActualHours DECIMAL
               ,TermId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,PrgVerCode VARCHAR(250)
               ,StartDate DATETIME
               ,EndDate DATETIME
            );

        DECLARE @MasterStudentPopulationRunningTotal TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ClsSectionId VARCHAR(250)
               ,PrgVerId UNIQUEIDENTIFIER
               ,MeetingDate DATETIME
               ,TermId UNIQUEIDENTIFIER
               ,ScheduledHoursRunningTotal DECIMAL(18, 2)
               ,ActualHoursRunningTotal DECIMAL(18, 2)
               ,IsExcused BIT
               ,ExcusedHours DECIMAL(18, 2)
               ,ScheduledDays DECIMAL(18, 2)
               ,ActualDays DECIMAL(18, 2)
            );


        ----------------------------------------------------------------- 
        -- TABLE FOR HOURS DATA 
        ----------------------------------------------------------------- 
        DECLARE @HoursScheduledActual TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,ScheduledHours DECIMAL
               ,ActualHours DECIMAL
               ,MakeUpHours DECIMAL
            );


        DECLARE @HoursExcused TABLE
            (
                PmtPeriodId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,ExcusedHours DECIMAL(18, 2)
            );

        DECLARE @TransactionDates TABLE
            (
                PmtPeriodId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,TransactionDate DATETIME
            );
        ----------------------------------------------------------------- 
        -- Insert student population into table 
        ----------------------------------------------------------------- 
        INSERT INTO @MasterStudentPopulation
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,CS.ClsSectionId
                                       ,MD.MaxDate
                                       ,SH.ScheduledHours
                                       ,MUH.MakeUpHours
                                       ,AH.ActualHours
                                       ,TT.TermId
                                       ,PV.PrgVerId
                                       ,PV.PrgVerCode
                                       ,TT.StartDate
                                       ,TT.EndDate
                    FROM       dbo.arStuEnrollments SE
                    INNER JOIN dbo.syStudentAttendanceSummary SA ON SE.StuEnrollId = SA.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningScheduledDays) AS ScheduledHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) SH ON SA.StuEnrollId = SH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningPresentDays) AS ActualHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) AH ON SE.StuEnrollId = AH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningMakeupDays) AS MakeUpHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) MUH ON SE.StuEnrollId = MUH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(StudentAttendedDate) AS MaxDate
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) MD ON SE.StuEnrollId = MD.StuEnrollId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arClassSections CS ON SA.ClsSectionId = CS.ClsSectionId
                    INNER JOIN dbo.arTerm TT ON CS.TermId = TT.TermId
                    INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                    INNER JOIN dbo.syStatuses ST ON PV.StatusId = ST.StatusId
                    WHERE      SC.SysStatusId IN ( 9, 20 )
								--removed due to this condition being required for by term or course charges and auto post is for payment period charging
                               --AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(SE.StuEnrollId, CS.ClsSectionId) --Added by Troy as part of fix for US8005 
                               AND SE.DisableAutoCharge <> 1 --Exclude enrollments with auto charge disabled  
                               AND PV.ProgramRegistrationType = 0
                    --AND pv.PrgVerId=''5E8BFC2C-FE62-4220-BDC6-51C7941F5AE0'' 
                    --AND se.StuEnrollId=''71A812F7-972D-4AB6-971C-3CFD02A3086B'' 

                    UNION
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,CS.ClsSectionId
                                       ,MD.MaxDate
                                       ,SH.ScheduledHours
                                       ,MUH.MakeUpHours
                                       ,AH.ActualHours
                                       ,TT.TermId
                                       ,PV.PrgVerId
                                       ,PV.PrgVerCode
                                       ,TT.StartDate
                                       ,TT.EndDate
                    FROM       dbo.arStuEnrollments SE
                    INNER JOIN dbo.syStudentAttendanceSummary SA ON SE.StuEnrollId = SA.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningScheduledDays) AS ScheduledHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) SH ON SA.StuEnrollId = SH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningPresentDays) AS ActualHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) AH ON SE.StuEnrollId = AH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningMakeupDays) AS MakeUpHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) MUH ON SE.StuEnrollId = MUH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(StudentAttendedDate) AS MaxDate
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) MD ON SE.StuEnrollId = MD.StuEnrollId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arTerm TT ON TT.ProgramVersionId = PV.PrgVerId
                    INNER JOIN dbo.arProgVerDef pvd ON pvd.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arClassSections CS ON CS.ProgramVersionDefinitionId = pvd.ProgVerDefId
                    INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                    INNER JOIN dbo.syStatuses ST ON PV.StatusId = ST.StatusId
                    WHERE      SC.SysStatusId IN ( 9, 20 )
							   --removed due to this condition being required for by term or course charges and auto post is for payment period charging
                               --AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(SE.StuEnrollId, CS.ClsSectionId) --Added by Troy as part of fix for US8005 
                               AND SE.DisableAutoCharge <> 1 --Exclude enrollments with auto charge disabled  
                               AND PV.ProgramRegistrationType = 1;
        --AND pv.PrgVerId=''5E8BFC2C-FE62-4220-BDC6-51C7941F5AE0'' 
        --AND se.StuEnrollId=''71A812F7-972D-4AB6-971C-3CFD02A3086B'' 


        -- Get Running Totals for Current Term 
        INSERT INTO @MasterStudentPopulationRunningTotal
                    SELECT     DISTINCT SAS.StuEnrollId
                                       ,SAS.ClsSectionId
                                       ,SE.PrgVerId
                                       ,SAS.StudentAttendedDate
                                       ,(
                                        SELECT     TOP 1 TermId
                                        FROM       arResults R
                                        INNER JOIN arClassSections CS ON R.TestId = CS.ClsSectionId
                                        WHERE      R.StuEnrollId = SAS.StuEnrollId
                                                   AND CS.ClsSectionId = ClsSectionId
                                        ) AS termId
                                       ,SAS.ActualRunningScheduledDays
                                       ,SAS.AdjustedPresentDays
                                       ,SAS.IsExcused
                                       ,( CASE WHEN IsExcused = 1 THEN ( SAS.ScheduledDays - SAS.ActualDays )
                                               ELSE 0
                                          END
                                        ) AS ExcusedHours
                                       ,SAS.ScheduledDays
                                       ,SAS.ActualDays
                    FROM       syStudentAttendanceSummary SAS
                    INNER JOIN dbo.arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                    WHERE      SAS.StuEnrollId IN (
                                                  SELECT DISTINCT StuEnrollId
                                                  FROM   @MasterStudentPopulation
                                                  )
                    ORDER BY   SAS.StuEnrollId
                              ,StudentAttendedDate;


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error creating Student Population.'', 16, 1);
                RETURN;
            END;






        ----------------------------------------------------------------- 
        -- insert hours data into table 
        ----------------------------------------------------------------- 
        INSERT INTO @HoursScheduledActual
                    SELECT   StuEnrollId
                            ,PrgVerId
                            ,ScheduledHours
                            ,ActualHours
                            ,MakeUpHours
                    FROM     @MasterStudentPopulation
                    GROUP BY StuEnrollId
                            ,PrgVerId
                            ,ScheduledHours
                            ,ActualHours
                            ,MakeUpHours;


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error creating hours data.'', 16, 1);
                RETURN;
            END;




        ----------------------------------------------------------------------------------- 
        -- Cursor to handle Excused Hours 
        ----------------------------------------------------------------------------------- 
        DECLARE @pmtperiodid UNIQUEIDENTIFIER;
        DECLARE @CumulativeValue DECIMAL;
        DECLARE @StuEnrollId UNIQUEIDENTIFIER;
        DECLARE @NextCumulativeValue DECIMAL;




        DECLARE db_cursor CURSOR FOR
            SELECT     p.PmtPeriodId
                      ,p.CumulativeValue
                      ,e.StuEnrollId
                      ,(
                       SELECT   TOP 1 child.CumulativeValue
                       FROM     saPmtPeriods child
                       WHERE    child.IncrementId = p.IncrementId
                                AND child.PeriodNumber = p.PeriodNumber + 1
                       ORDER BY p.PeriodNumber ASC
                               ,p.CumulativeValue ASC
                       ) AS NextCumulativeValue
            FROM       dbo.saPmtPeriods p
            INNER JOIN dbo.saIncrements I ON p.IncrementId = I.IncrementId
            INNER JOIN dbo.saBillingMethods B ON I.BillingMethodId = B.BillingMethodId
            INNER JOIN dbo.arPrgVersions prg ON B.BillingMethodId = prg.BillingMethodId
            INNER JOIN dbo.arStuEnrollments e ON prg.PrgVerId = e.PrgVerId
            WHERE      I.IncrementType = 0
            ORDER BY   e.StuEnrollId
                      ,e.PrgVerId
                      ,I.IncrementId
                      ,p.PeriodNumber;

        DECLARE @FloorValue DECIMAL;
        SET @FloorValue = 0;

        DECLARE @currentEnrollId UNIQUEIDENTIFIER;
        SET @currentEnrollId = NEWID();

        DECLARE @curCumValue DECIMAL;
        SET @curCumValue = 0;

        OPEN db_cursor;

        FETCH NEXT FROM db_cursor
        INTO @pmtperiodid
            ,@CumulativeValue
            ,@StuEnrollId
            ,@NextCumulativeValue;


        WHILE @@FETCH_STATUS = 0
            BEGIN

                IF @StuEnrollId <> @currentEnrollId
                    BEGIN
                        SET @FloorValue = 0;
                        SET @curCumValue = @CumulativeValue;
                        SET @currentEnrollId = @StuEnrollId;
                    END;
                ELSE
                    BEGIN
                        SET @FloorValue = @curCumValue;
                    END;


                --SELECT @FloorValue,@CumulativeValue,@currentEnrollId,@StuEnrollId 

                INSERT INTO @HoursExcused
                            SELECT   @pmtperiodid AS pmtperiodid
                                    ,StuEnrollId AS StuEnrollId
                                    ,PrgVerId
                                    ,SUM(ExcusedHours) AS ExcusedHours
                            FROM     @MasterStudentPopulationRunningTotal
                            WHERE    StuEnrollId = @StuEnrollId
                                     AND ActualHoursRunningTotal > @FloorValue
                                     AND (
                                         ( ActualHoursRunningTotal <= @CumulativeValue )
                                         OR ( ActualHoursRunningTotal
                                     BETWEEN @CumulativeValue AND @NextCumulativeValue
                                            )
                                         OR ( ActualHoursRunningTotal >= @CumulativeValue )
                                         )
                            GROUP BY StuEnrollId
                                    ,PrgVerId;



                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error creating excused hours data.'', 16, 1);
                        RETURN;
                    END;



                FETCH NEXT FROM db_cursor
                INTO @pmtperiodid
                    ,@CumulativeValue
                    ,@StuEnrollId
                    ,@NextCumulativeValue;

            END;

        CLOSE db_cursor;
        DEALLOCATE db_cursor;
        ----------------------------------------------------------------------------------- 

        --for testing 
        --SELECT * FROM @MasterStudentPopulation 
        --SELECT * FROM @MasterStudentPopulationRunningTotal 
        --SELECT * FROM @HoursScheduledActual	 
        --SELECT * FROM @HoursExcused 




        DECLARE @BatchNumber INT;
        SET @BatchNumber = (
                           SELECT ISNULL(MAX(PmtPeriodBatchHeaderId), 0) + 1
                           FROM   saPmtPeriodBatchHeaders
                           );

        INSERT INTO saPmtPeriodBatchHeaders (
                                            BatchName
                                            )
                    SELECT ''Batch '' + CAST(@BatchNumber AS VARCHAR(20)) + '' (Hours) - '' + LEFT(CONVERT(VARCHAR, GETDATE(), 101), 10) + '' ''
                           + LEFT(CONVERT(VARCHAR, GETDATE(), 108), 10);


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error creating batch header.'', 16, 1);
                RETURN;
            END;

        DECLARE @InsertedHeaderId INT;
        SET @InsertedHeaderId = SCOPE_IDENTITY();





        INSERT INTO saPmtPeriodBatchItems (
                                          PmtPeriodId
                                         ,PmtPeriodBatchHeaderId
                                         ,StuEnrollId
                                         ,ChargeAmount
                                         ,CreditsHoursValue
                                         ,TransactionReference
                                         ,TransactionDescription
                                         ,ModUser
                                         ,ModDate
                                         ,TermId
                                          )
                    SELECT     DISTINCT ( p.PmtPeriodId )
                                       ,@InsertedHeaderId
                                       ,e.StuEnrollId
                                       ,p.ChargeAmount
                                       ,( CASE WHEN i.IncrementType = 0 THEN CAE.ActualHours + ( ISNULL(HE.ExcusedHours,0) * i.ExcAbscenesPercent )
                                               WHEN i.IncrementType = 1 THEN CAE.ScheduledHours
                                               ELSE 0
                                          END
                                        ) AS CreditsHoursValue
                                       ,''Payment Period '' + CAST(p.PeriodNumber AS VARCHAR(5)) AS TransactionReference
                                       ,tc.TransCodeDescrip + '': '' + CAST(p.CumulativeValue AS VARCHAR(20)) + '' '' + i.IncrementName AS TransactionDescription
                                       ,''AutoPost'' AS ModUser
                                       ,GETDATE() AS ModDate
                                       ,( CASE WHEN i.IncrementType = 0 THEN
                                               (
                                               SELECT TOP 1 TermId
                                               FROM   @MasterStudentPopulationRunningTotal
                                               -- INNER JOIN dbo.arPrgVersions pv ON pv.PrgVerId = [@MasterStudentPopulationRunningTotal].PrgVerId
                                               WHERE  StuEnrollId = e.StuEnrollId
                                                      -- AND ( pv.ProgramRegistrationType = 1 )
                                                      AND ActualHoursRunningTotal >= p.CumulativeValue
                                               )
                                               WHEN i.IncrementType = 1 THEN
                                               (
                                               SELECT TOP 1 TermId
                                               FROM   @MasterStudentPopulationRunningTotal
                                               --INNER JOIN dbo.arPrgVersions pv ON pv.PrgVerId = [@MasterStudentPopulationRunningTotal].PrgVerId
                                               WHERE  StuEnrollId = e.StuEnrollId
                                                      --AND ( pv.ProgramRegistrationType = 1 )
                                                      AND ScheduledHoursRunningTotal >= p.CumulativeValue
                                               )
                                               ELSE NULL
                                          END
                                        ) AS TermId
                    FROM       saPmtPeriods p
                    INNER JOIN dbo.saIncrements i ON p.IncrementId = i.IncrementId
                    INNER JOIN dbo.saBillingMethods B ON i.BillingMethodId = B.BillingMethodId
                    INNER JOIN dbo.arPrgVersions prg ON B.BillingMethodId = prg.BillingMethodId
                    INNER JOIN dbo.arStuEnrollments e ON prg.PrgVerId = e.PrgVerId
                    --INNER JOIN dbo.arStudent st ON e.StudentId = st.StudentId
                    INNER JOIN @HoursScheduledActual CAE ON e.StuEnrollId = CAE.StuEnrollId
                    LEFT JOIN  @HoursExcused HE ON p.PmtPeriodId = HE.PmtPeriodId
                                                   AND e.StuEnrollId = HE.StuEnrollId
                                                   AND e.PrgVerId = HE.PrgVerId
                    LEFT JOIN  dbo.saTransCodes tc ON tc.TransCodeId = p.TransactionCodeId
                    WHERE      B.BillingMethod = 3
                               AND e.StartDate >= i.EffectiveDate
                               AND ( CASE WHEN i.IncrementType = 0 THEN CAE.ActualHours + ( ISNULL(HE.ExcusedHours,0) * i.ExcAbscenesPercent ) + CAE.MakeUpHours
                                          WHEN i.IncrementType = 1 THEN CAE.ScheduledHours
                                          ELSE 0
                                     END
                                   ) >= p.CumulativeValue
                               AND e.StuEnrollId NOT IN (
                                                        SELECT StuEnrollId
                                                        FROM   dbo.saTransactions
                                                        WHERE  StuEnrollId = e.StuEnrollId
                                                               AND PmtPeriodId = p.PmtPeriodId
                                                               OR p.PeriodNumber = PaymentPeriodNumber
                                                        )
                               AND e.StuEnrollId NOT IN (
                                                        SELECT     i.StuEnrollId
                                                        FROM       dbo.saPmtPeriodBatchItems i
                                                        INNER JOIN saPmtPeriodBatchHeaders h ON i.PmtPeriodBatchHeaderId = h.PmtPeriodBatchHeaderId
                                                        WHERE      StuEnrollId = e.StuEnrollId
                                                                   AND PmtPeriodId = p.PmtPeriodId
                                                                   AND h.IsPosted = 0
                                                        );


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error creating batch items.'', 16, 1);
                RETURN;
            END;



        ----------------------------------------------------------------- 
        -- UPDATE Batch Item count in header table 
        ----------------------------------------------------------------- 
        UPDATE saPmtPeriodBatchHeaders
        SET    BatchItemCount = (
                                SELECT     COUNT(*)
                                FROM       saPmtPeriodBatchHeaders h
                                INNER JOIN saPmtPeriodBatchItems i ON h.PmtPeriodBatchHeaderId = i.PmtPeriodBatchHeaderId
                                WHERE      h.PmtPeriodBatchHeaderId = @InsertedHeaderId
                                )
        WHERE  PmtPeriodBatchHeaderId = @InsertedHeaderId;

        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error updating batch count.'', 16, 1);
                RETURN;
            END;
        ----------------------------------------------------------------- 

        ----------------------------------------------------------------- 
        -- GET AutoPost config setting 
        ----------------------------------------------------------------- 
        DECLARE @IsAutoPost VARCHAR(50);
        SET @IsAutoPost = (
                          SELECT     TOP 1 v.Value
                          FROM       dbo.syConfigAppSettings c
                          INNER JOIN dbo.syConfigAppSetValues v ON c.SettingId = v.SettingId
                          WHERE      c.KeyName = ''PaymentPeriodAutoPost''
                          );
        ----------------------------------------------------------------- 



        ------------------------------------------------------------------- 
        ---- INSERT Transactions from Batch 
        ------------------------------------------------------------------- 
        IF @IsAutoPost = ''yes''
            BEGIN

                IF OBJECTPROPERTY(OBJECT_ID(''Trigger_Insert_Check_saTransactions''), ''IsTrigger'') = 1
                    BEGIN
                        ALTER TABLE dbo.saTransactions DISABLE TRIGGER Trigger_Insert_Check_saTransactions;
                    END;



                INSERT INTO @TransactionDates (
                                              PmtPeriodId
                                             ,StuEnrollId
                                             ,TransactionDate
                                              )
                            SELECT      ppCumulative.PmtPeriodId
                                       ,ppCumulative.StuEnrollId
                                       ,transDate.StudentAttendedDate
                            FROM        (
                                        SELECT     DISTINCT pp.PmtPeriodId
                                                           ,StuEnrollId
                                                           ,pp.CumulativeValue
                                        FROM       dbo.saPmtPeriodBatchItems ppbi
                                        INNER JOIN dbo.saPmtPeriods pp ON pp.PmtPeriodId = ppbi.PmtPeriodId
                                        WHERE      ppbi.PmtPeriodBatchHeaderId = @InsertedHeaderId
                                        ) ppCumulative
                            CROSS APPLY (
                                        SELECT   TOP 1 sas.StudentAttendedDate
                                        FROM     dbo.syStudentAttendanceSummary sas
                                        WHERE    sas.StuEnrollId = ppCumulative.StuEnrollId
                                                 AND (( sas.ActualRunningPresentDays + sas.ActualRunningMakeupDays ) >= ppCumulative.CumulativeValue )
                                        ORDER BY sas.StudentAttendedDate
                                        ) AS transDate;



                INSERT INTO dbo.saTransactions (
                                               StuEnrollId
                                              ,TermId
                                              ,CampusId
                                              ,TransDate
                                              ,TransCodeId
                                              ,TransReference
                                              ,AcademicYearId
                                              ,TransDescrip
                                              ,TransAmount
                                              ,TransTypeId
                                              ,IsPosted
                                              ,CreateDate
                                              ,BatchPaymentId
                                              ,ViewOrder
                                              ,IsAutomatic
                                              ,ModUser
                                              ,ModDate
                                              ,Voided
                                              ,FeeLevelId
                                              ,FeeId
                                              ,PaymentCodeId
                                              ,FundSourceId
                                              ,StuEnrollPayPeriodId
                                              ,DisplaySequence
                                              ,SecondDisplaySequence
                                              ,PmtPeriodId
                                              ,PaymentPeriodNumber
                                               )
                            SELECT     BI.StuEnrollId AS StuEnrollId
                                      ,BI.TermId AS TermId
                                      ,SE.CampusId
                                      ,COALESCE((
                                                SELECT TransactionDate
                                                FROM   @TransactionDates
                                                WHERE  PmtPeriodId = BI.PmtPeriodId
                                                       AND StuEnrollId = BI.StuEnrollId
                                                )
                                               ,GETDATE()
                                               ,SE.StartDate
                                               ,SE.ExpStartDate
                                               ) --transaction date
                                      ,PP.TransactionCodeId AS TranscodeId
                                      ,BI.TransactionReference
                                      ,NULL AS AcademicYearId
                                                 --??? HOW TO GET THIS?, 
                                      ,BI.TransactionDescription
                                      ,BI.ChargeAmount
                                      ,(
                                       SELECT TOP 1 TransTypeId
                                       FROM   dbo.saTransTypes
                                       WHERE  Description LIKE ''Charge%''
                                       ) AS TransTypeId
                                      ,1 AS IsPosted
                                      ,GETDATE() AS CreatedDate
                                      ,NULL AS BatchPaymentId
                                      ,NULL AS ViewOrder
                                      ,0 AS IsAutomatic
                                      ,''AutoPost'' AS ModUser
                                      ,GETDATE() AS ModDate
                                      ,0 AS Voided
                                      ,NULL AS FeeLevelId
                                      ,NULL AS FeeId
                                      ,NULL AS PaymentCodeId
                                      ,NULL AS FundSourceId
                                      ,NULL AS StuEnrollPayPeriodId
                                      ,NULL AS DisplaySequence
                                      ,NULL AS SecondDisplaySequence
                                      ,BI.PmtPeriodId AS PmtPeriodId
                                      ,PP.PeriodNumber
                            FROM       saPmtPeriodBatchItems BI
                            INNER JOIN saPmtPeriodBatchHeaders BH ON BI.PmtPeriodBatchHeaderId = BH.PmtPeriodBatchHeaderId
                            INNER JOIN dbo.arStuEnrollments SE ON BI.StuEnrollId = SE.StuEnrollId
                            INNER JOIN dbo.saPmtPeriods PP ON PP.PmtPeriodId = BI.PmtPeriodId
                            LEFT JOIN  dbo.saTransCodes tc ON tc.TransCodeId = PP.TransactionCodeId
                            WHERE      BH.PmtPeriodBatchHeaderId = @InsertedHeaderId
                                       AND BI.IncludedInPost = 1
                                       AND SE.StuEnrollId NOT IN (
                                                                 SELECT StuEnrollId
                                                                 FROM   dbo.saTransactions
                                                                 WHERE  StuEnrollId = SE.StuEnrollId
                                                                        AND PmtPeriodId = BI.PmtPeriodId
                                                                        OR PaymentPeriodNumber = PP.PeriodNumber
                                                                 )
                            ORDER BY   SE.StuEnrollId
                                      ,BI.TransactionReference;


                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error inserting transactions.'', 16, 1);
                        RETURN;
                    END;
                IF OBJECTPROPERTY(OBJECT_ID(''Trigger_Insert_Check_saTransactions''), ''IsTrigger'') = 1
                    BEGIN
                        ALTER TABLE dbo.saTransactions ENABLE TRIGGER Trigger_Insert_Check_saTransactions;
                    END;


                UPDATE     saPmtPeriodBatchItems
                SET        TransactionId = T.TransactionId
                FROM       dbo.saTransactions T
                INNER JOIN dbo.saPmtPeriodBatchItems BI ON T.PmtPeriodId = BI.PmtPeriodId
                                                           AND T.StuEnrollId = BI.StuEnrollId
                INNER JOIN dbo.saPmtPeriodBatchHeaders BH ON BI.PmtPeriodBatchHeaderId = BH.PmtPeriodBatchHeaderId
                WHERE      BH.PmtPeriodBatchHeaderId = @InsertedHeaderId;

                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error updating transaction ids in batch.'', 16, 1);
                        RETURN;
                    END;
                ----------------------------------------------------------------- 

                ----------------------------------------------------------------- 
                -- Set Batch to IsPosted 
                ----------------------------------------------------------------- 
                UPDATE dbo.saPmtPeriodBatchHeaders
                SET    IsPosted = 1
                      ,PostedDate = GETDATE()
                WHERE  PmtPeriodBatchHeaderId = @InsertedHeaderId;

                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error updating batch is posted flag.'', 16, 1);
                        RETURN;
                    END;
                ----------------------------------------------------------------- 


                ----------------------------------------------------------------- 
                -- Set IsCharged flag for payment periods from this batch 
                ----------------------------------------------------------------- 
                UPDATE     dbo.saPmtPeriods
                SET        IsCharged = 1
                FROM       saPmtPeriodBatchItems I
                INNER JOIN dbo.saPmtPeriods P ON I.PmtPeriodId = P.PmtPeriodId
                WHERE      I.PmtPeriodBatchHeaderId = @InsertedHeaderId;

                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error updating batch is posted flag.'', 16, 1);
                        RETURN;
                    END;
            END;


        ----------------------------------------------------------------- 
        -- Set IsBillingMethodCharged flag for Program Version from this batch 
        ----------------------------------------------------------------- 
        UPDATE     P
        SET        P.IsBillingMethodCharged = 1
        FROM       dbo.arPrgVersions P
        INNER JOIN dbo.arStuEnrollments E ON P.PrgVerId = E.PrgVerId
        INNER JOIN saPmtPeriodBatchItems I ON E.StuEnrollId = I.StuEnrollId
        INNER JOIN dbo.saPmtPeriodBatchHeaders H ON I.PmtPeriodBatchHeaderId = H.PmtPeriodBatchHeaderId
        WHERE      H.PmtPeriodBatchHeaderId = @InsertedHeaderId
                   AND I.TransactionId IS NOT NULL
                   AND I.IncludedInPost = 1;


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error updating program version ischarged flag.'', 16, 1);
                RETURN;
            END;
        -----------------------------------------------------------------	 


        COMMIT TRAN PAYMENTPERIODTRANSACTION;
    -----------------------------------------------------------------	 



    END;
--================================================================================================= 
-- END  --  USP_ProcessPaymentPeriodsHours 
--================================================================================================= 

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
