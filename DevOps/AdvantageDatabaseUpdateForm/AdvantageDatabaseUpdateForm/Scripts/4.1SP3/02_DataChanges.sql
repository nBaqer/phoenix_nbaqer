﻿-- ===============================================================================================
-- START Consolidated Script Version 4.1SP3
-- Data Changes Zone
-- Please do not deploy your schema changes in this area
-- Please use SQL Prompt format before insert here please
-- Please run after Schema changes....
-- ===============================================================================================
--=================================================================================================
-- START  AD-10019 : Charging Method by Payment Period - Allow more than one charge per period and choose transaction code for each charge.
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION TransCodeId;
BEGIN TRY
    DECLARE @transCodeId UNIQUEIDENTIFIER;
    SET @transCodeId = (
                       SELECT     TOP 1 t.TransCodeId
                       FROM       dbo.saTransactions t
                       INNER JOIN dbo.saPmtPeriods p ON p.PmtPeriodId = t.PmtPeriodId
                       WHERE      t.TransCodeId IS NOT NULL
                       );

    UPDATE dbo.saPmtPeriods
    SET    TransactionCodeId = @transCodeId
    WHERE  TransactionCodeId IS NULL;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION TransCodeId;
        PRINT 'Failed to update TransCodeId for saPmtPeriods';
    END;
ELSE
    BEGIN
        COMMIT TRANSACTION TransCodeId;
        PRINT 'updated TransCodeId for saPmtPeriods';
    END;
GO

--=================================================================================================
-- END  Add MakeupHours field to Adhoc AD-15849
--=================================================================================================
-- ===============================================================================================
-- END Consolidated Script Version 4.1SP3
-- ===============================================================================================