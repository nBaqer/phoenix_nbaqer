﻿/*
Run this script on:

        dev-com-db1\Adv.Aveda1029    -  This database will be modified

to synchronize it with a database with the schema represented by:

        Source

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.7.10021 from Red Gate Software Ltd at 10/29/2019 5:30:55 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
/*
* Use this Pre-Deployment script to perform tasks before the deployment of the project.
* Read more at https://www.red-gate.com/SOC7/pre-deployment-script-help
*/
UPDATE dbo.arClsSectMeetings
SET PeriodId = NULL
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)

DELETE FROM syPeriodsWorkDays
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_REGISTER_STUDENT_AUTOMATICALLY]'
GO
IF OBJECT_ID(N'[dbo].[USP_REGISTER_STUDENT_AUTOMATICALLY]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_REGISTER_STUDENT_AUTOMATICALLY]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudent]'
GO
IF OBJECT_ID(N'[dbo].[arStudent]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_REGISTER_STUDENT_AUTOMATICALLY]'
GO
IF OBJECT_ID(N'[dbo].[USP_REGISTER_STUDENT_AUTOMATICALLY]', 'P') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[USP_REGISTER_STUDENT_AUTOMATICALLY]
    (
        @ProgramVersionId UNIQUEIDENTIFIER
       ,@StudentEnrollmentId UNIQUEIDENTIFIER
       ,@ModifiedUser VARCHAR(50)
    )
AS
    BEGIN
        DECLARE @RegistrationType INT;
        SET @RegistrationType = (
                                SELECT   TOP ( 1 ) ProgramRegistrationType
                                FROM     dbo.arPrgVersions
                                WHERE    PrgVerId = @ProgramVersionId
                                ORDER BY ModDate DESC
                                );

        DECLARE @ShowROSSOnlyTabsForStudent VARCHAR(50);
        SET @ShowROSSOnlyTabsForStudent = (
                                          SELECT     TOP ( 1 ) Value
                                          FROM       dbo.syConfigAppSetValues AS configValues
                                          INNER JOIN dbo.syConfigAppSettings settings ON settings.SettingId = configValues.SettingId
                                          WHERE      settings.KeyName = ''ShowROSSOnlyTabsForStudent''
                                          ORDER BY   configValues.ModDate DESC
                                          );


        IF ( @RegistrationType = 1 )
            BEGIN
                INSERT INTO dbo.arResults (
                                          ResultId
                                         ,TestId
                                         ,Score
                                         ,GrdSysDetailId
                                         ,Cnt
                                         ,Hours
                                         ,StuEnrollId
                                         ,IsInComplete
                                         ,DroppedInAddDrop
                                         ,ModUser
                                         ,ModDate
                                         ,IsTransfered
                                         ,isClinicsSatisfied
                                         ,DateDetermined
                                         ,IsCourseCompleted
                                         ,IsGradeOverridden
                                         ,GradeOverriddenBy
                                         ,GradeOverriddenDate
                                         ,DateCompleted
                                          )
                            SELECT     NEWID()              -- ResultId - uniqueidentifier
                                      ,classes.ClsSectionId -- TestId - uniqueidentifier
                                      ,NULL                 -- Score - decimal(18, 2)
                                      ,NULL                 -- GrdSysDetailId - uniqueidentifier
                                      ,0                    -- Cnt - int
                                      ,0                    -- Hours - int
                                      ,@StudentEnrollmentId -- StuEnrollId - uniqueidentifier
                                      ,NULL                 -- IsInComplete - bit
                                      ,NULL                 -- DroppedInAddDrop - bit
                                      ,@ModifiedUser        -- ModUser - varchar(50)
                                      ,GETDATE()            -- ModDate - datetime
                                      ,NULL                 -- IsTransfered - bit
                                      ,NULL                 -- isClinicsSatisfied - bit
                                      ,GETDATE()            -- DateDetermined - datetime
                                      ,0                    -- IsCourseCompleted - bit
                                      ,0                    -- IsGradeOverridden - bit
                                      ,''''                   -- GradeOverriddenBy - varchar(50)
                                      ,GETDATE()            -- GradeOverriddenDate - datetime
                                      ,GETDATE()            -- DateCompleted - datetime
                            FROM       dbo.arPrgVersions programVersion
                            INNER JOIN dbo.arProgVerDef programVersionDef ON programVersionDef.PrgVerId = programVersion.PrgVerId
                            INNER JOIN dbo.arClassSections classes ON classes.ProgramVersionDefinitionId = programVersionDef.ProgVerDefId
                            INNER JOIN dbo.arStuEnrollments se ON se.PrgVerId = programVersion.PrgVerId
                            WHERE      programVersion.PrgVerId = @ProgramVersionId
                                       AND se.StuEnrollId = @StudentEnrollmentId
                                       AND se.CampusId = classes.CampusId;

                IF (
                   @ShowROSSOnlyTabsForStudent = ''true''
                   OR @ShowROSSOnlyTabsForStudent = ''yes''
                   )
                    BEGIN
                        INSERT INTO dbo.arGrdBkResults (
                                                       GrdBkResultId
                                                      ,ClsSectionId
                                                      ,InstrGrdBkWgtDetailId
                                                      ,Comments
                                                      ,StuEnrollId
                                                      ,ModUser
                                                      ,ModDate
                                                       )
                                    SELECT     NEWID()                                      -- GrdBkResultId - uniqueidentifier
                                              ,classes.ClsSectionId                         -- ClsSectionId - uniqueidentifier
                                              ,gradeBookWeightDetails.InstrGrdBkWgtDetailId -- InstrGrdBkWgtDetailId - uniqueidentifier
                                              ,gradeComponentTypes.Descrip                  -- Comments - varchar(50)
                                              ,@StudentEnrollmentId                         -- StuEnrollId - uniqueidentifier
                                              ,@ModifiedUser                                -- ModUser - varchar(50)
                                              ,GETDATE()                                    -- ModDate - datetime
                                    FROM       dbo.arStuEnrollments enrollments
                                    INNER JOIN dbo.arPrgVersions programVersion ON programVersion.PrgVerId = enrollments.PrgVerId
                                    INNER JOIN dbo.arProgVerDef programVersionDef ON programVersionDef.PrgVerId = programVersion.PrgVerId
                                    INNER JOIN dbo.arClassSections classes ON classes.ProgramVersionDefinitionId = programVersionDef.ProgVerDefId
                                    INNER JOIN dbo.arReqs courses ON courses.ReqId = classes.ReqId
                                    INNER JOIN dbo.arGrdBkWeights gradeBookWeight ON gradeBookWeight.ReqId = classes.ReqId
                                    INNER JOIN dbo.arGrdBkWgtDetails gradeBookWeightDetails ON gradeBookWeightDetails.InstrGrdBkWgtId = gradeBookWeight.InstrGrdBkWgtId
                                    INNER JOIN dbo.arGrdComponentTypes gradeComponentTypes ON gradeComponentTypes.GrdComponentTypeId = gradeBookWeightDetails.GrdComponentTypeId
                                    WHERE      enrollments.StuEnrollId = @StudentEnrollmentId
                                               AND programVersion.PrgVerId = @ProgramVersionId
                                               AND enrollments.CampusId = classes.CampusId
                                               AND NOT EXISTS (
                                                              SELECT *
                                                              FROM   dbo.arGrdBkResults
                                                              WHERE  ClsSectionId = classes.ClsSectionId
                                                                     AND InstrGrdBkWgtDetailId = gradeBookWeightDetails.InstrGrdBkWgtDetailId
                                                                     AND StuEnrollId = @StudentEnrollmentId
                                                              );
                    END;
                ELSE
                    BEGIN

                        INSERT INTO dbo.arGrdBkResults (
                                                       GrdBkResultId
                                                      ,ClsSectionId
                                                      ,InstrGrdBkWgtDetailId
                                                      ,Comments
                                                      ,StuEnrollId
                                                      ,ModUser
                                                      ,ModDate
                                                       )
                                    SELECT     NEWID()                                      -- GrdBkResultId - uniqueidentifier
                                              ,classes.ClsSectionId                         -- ClsSectionId - uniqueidentifier
                                              ,gradeBookWeightDetails.InstrGrdBkWgtDetailId -- InstrGrdBkWgtDetailId - uniqueidentifier
                                              ,gradeComponentTypes.Descrip                  -- Comments - varchar(50)
                                              ,@StudentEnrollmentId                         -- StuEnrollId - uniqueidentifier
                                              ,@ModifiedUser                                -- ModUser - varchar(50)
                                              ,GETDATE()                                    -- ModDate - datetime
                                    FROM       dbo.arStuEnrollments enrollments
                                    INNER JOIN dbo.arPrgVersions programVersion ON programVersion.PrgVerId = enrollments.PrgVerId
                                    INNER JOIN dbo.arTerm terms ON terms.ProgramVersionId = programVersion.PrgVerId
                                    INNER JOIN dbo.arClassSections classes ON classes.TermId = terms.TermId
                                    INNER JOIN dbo.arReqs courses ON courses.ReqId = classes.ReqId
                                    INNER JOIN dbo.arGrdBkWeights gradeBookWeight ON gradeBookWeight.ReqId = classes.ReqId
                                    INNER JOIN dbo.arGrdBkWgtDetails gradeBookWeightDetails ON gradeBookWeightDetails.InstrGrdBkWgtId = gradeBookWeight.InstrGrdBkWgtId
                                    INNER JOIN dbo.arGrdComponentTypes gradeComponentTypes ON gradeComponentTypes.GrdComponentTypeId = gradeBookWeightDetails.GrdComponentTypeId
                                    WHERE      enrollments.StuEnrollId = @StudentEnrollmentId
                                               AND enrollments.CampusId = classes.CampusId
                                               AND programVersion.PrgVerId = @ProgramVersionId
                                               AND courses.IsExternship = 1
                                               AND gradeComponentTypes.SysComponentTypeId = 544
                                               AND NOT EXISTS (
                                                              SELECT *
                                                              FROM   dbo.arGrdBkResults
                                                              WHERE  ClsSectionId = classes.ClsSectionId
                                                                     AND InstrGrdBkWgtDetailId = gradeBookWeightDetails.InstrGrdBkWgtDetailId
                                                                     AND StuEnrollId = @StudentEnrollmentId
                                                              );

                    END;


            END;

    END;

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
