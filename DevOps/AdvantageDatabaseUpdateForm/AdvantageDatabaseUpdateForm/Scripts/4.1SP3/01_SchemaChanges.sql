﻿/*
Run this script on:

        dev-com-db1\Adv.AvedaLive    -  This database will be modified

to synchronize it with a database with the schema represented by:

        Source

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.7.10021 from Red Gate Software Ltd at 10/8/2019 6:23:44 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
/*
* Use this Pre-Deployment script to perform tasks before the deployment of the project.
* Read more at https://www.red-gate.com/SOC7/pre-deployment-script-help
*/
UPDATE dbo.arClsSectMeetings
SET PeriodId = NULL
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)

DELETE FROM syPeriodsWorkDays
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[saPmtPeriods]'
GO
IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_saPmtPeriods_saIncrements_IncrementId_IncrementId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[saPmtPeriods]', 'U'))
ALTER TABLE [dbo].[saPmtPeriods] DROP CONSTRAINT [FK_saPmtPeriods_saIncrements_IncrementId_IncrementId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UIX_saPmtPeriods_IncrementId_PeriodNumber] from [dbo].[saPmtPeriods]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'UIX_saPmtPeriods_IncrementId_PeriodNumber' AND object_id = OBJECT_ID(N'[dbo].[saPmtPeriods]'))
DROP INDEX [UIX_saPmtPeriods_IncrementId_PeriodNumber] ON [dbo].[saPmtPeriods]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[TR_InsertAttendanceSummary_ByClockHour_Minutes] from [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[TR_InsertAttendanceSummary_ByClockHour_Minutes]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[TR_InsertAttendanceSummary_ByClockHour_Minutes]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[tr_arStudentClockAttendance_Insert] from [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[tr_arStudentClockAttendance_Insert]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[tr_arStudentClockAttendance_Insert]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[tr_arStudentClockAttendance_Update] from [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[tr_arStudentClockAttendance_Update]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[tr_arStudentClockAttendance_Update]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[TR_InsertAttendanceSummary_ByDay_PA] from [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[TR_InsertAttendanceSummary_ByDay_PA]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[TR_InsertAttendanceSummary_ByDay_PA]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[SyStudentStatusChanges_RemoveScheduledForTerminatedOrGrad] from [dbo].[syStudentStatusChanges]'
GO
IF OBJECT_ID(N'[dbo].[SyStudentStatusChanges_RemoveScheduledForTerminatedOrGrad]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[SyStudentStatusChanges_RemoveScheduledForTerminatedOrGrad]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_GetAbsentToday]'
GO
IF OBJECT_ID(N'[dbo].[USP_GetAbsentToday]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_GetAbsentToday]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_ProcessPaymentPeriodsHours]'
GO
IF OBJECT_ID(N'[dbo].[USP_ProcessPaymentPeriodsHours]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_ProcessPaymentPeriodsHours]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]'
GO
IF OBJECT_ID(N'[dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[arStudentClockAttendance]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[arStudentClockAttendance]', N'SchedHoursOnTermination') IS NULL
ALTER TABLE [dbo].[arStudentClockAttendance] ADD[SchedHoursOnTermination] [decimal] (18, 2) NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[saPmtPeriods]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[saPmtPeriods]', N'TransactionCodeId') IS NULL
ALTER TABLE [dbo].[saPmtPeriods] ADD[TransactionCodeId] [uniqueidentifier] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudent]'
GO
IF OBJECT_ID(N'[dbo].[arStudent]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[tr_arStudentClockAttendance_Insert] on [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[tr_arStudentClockAttendance_Insert]', 'TR') IS NULL
EXEC sp_executesql N'

-- =============================================
-- Author:		FAME Inc.
-- Create date: 5/17/2019
-- Description:	When inserting record into arStudentClockAttendance, if time clock punches exist and hours not 0 or 999.0, calculate rounding hours
-- =============================================
CREATE   TRIGGER [dbo].[tr_arStudentClockAttendance_Insert]
ON [dbo].[arStudentClockAttendance]
INSTEAD OF INSERT
AS
    BEGIN
        INSERT INTO dbo.arStudentClockAttendance
                    SELECT Inserted.StuEnrollId
                          ,Inserted.ScheduleId
                          ,Inserted.RecordDate
                          ,Inserted.SchedHours
                          ,CASE WHEN (
                                     inserted.ActualHours = 0
                                     OR inserted.ActualHours = 9999.0
                                     OR inserted.ActualHours = 999.0
                                     OR inserted.ActualHours = 99.0
                                     ) THEN Inserted.ActualHours 
                                ELSE dbo.CalculateHoursForStudentOnDate(inserted.StuEnrollId, inserted.RecordDate, inserted.ActualHours)
                           END AS ActualHours
                          ,Inserted.ModDate
                          ,Inserted.ModUser
                          ,Inserted.isTardy
                          ,Inserted.PostByException
                          ,Inserted.comments
                          ,Inserted.TardyProcessed
                          ,Inserted.Converted,
						  Inserted.SchedHoursOnTermination
                    FROM   inserted;


    END;

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[tr_arStudentClockAttendance_Update] on [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[tr_arStudentClockAttendance_Update]', 'TR') IS NULL
EXEC sp_executesql N'
-- =============================================
-- Author:		FAME Inc.
-- Create date: 5/17/2019
-- Description:	When updating record into arStudentClockAttendance, if time clock punches is updated hours not 0 or 999.0, calculate rounding hours
-- =============================================
CREATE     TRIGGER [dbo].[tr_arStudentClockAttendance_Update]
ON [dbo].[arStudentClockAttendance]
INSTEAD OF UPDATE
AS
    BEGIN
					UPDATE a
					SET a.StuEnrollId = Inserted.StuEnrollId,
					a.ScheduleId = Inserted.ScheduleId,
					a.RecordDate = Inserted.RecordDate,
					a.SchedHours = inserted.SchedHours,
					a.ActualHours =  (CASE WHEN (
                                     inserted.ActualHours = 0
                                     OR inserted.ActualHours = 9999.0
                                     OR inserted.ActualHours = 999.0
                                     OR inserted.ActualHours = 99.0
                                     ) THEN Inserted.ActualHours
                                ELSE dbo.CalculateHoursForStudentOnDate(inserted.StuEnrollId, inserted.RecordDate, inserted.ActualHours)
                           END),
					a.ModDate = Inserted.ModDate,
					a.ModUser = Inserted.ModUser,
					a.isTardy = Inserted.isTardy,
					a.PostByException = Inserted.PostByException,
					a.comments = Inserted.comments,
					a.TardyProcessed = Inserted.TardyProcessed,
					a.Converted =Inserted.Converted,
					a.SchedHoursOnTermination = Inserted.SchedHoursOnTermination
					FROM Inserted JOIN dbo.arStudentClockAttendance a ON a.StuEnrollId = Inserted.StuEnrollId AND a.RecordDate = Inserted.RecordDate;
    END;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[TR_InsertAttendanceSummary_ByDay_PA] on [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[TR_InsertAttendanceSummary_ByDay_PA]', 'TR') IS NULL
EXEC sp_executesql N'

CREATE TRIGGER [dbo].[TR_InsertAttendanceSummary_ByDay_PA]
ON [dbo].[arStudentClockAttendance]
AFTER INSERT, UPDATE
AS
SET NOCOUNT ON;

DECLARE @MyEnrollments TABLE
    (
        StuEnrollId UNIQUEIDENTIFIER
    );

DECLARE @HourTotals2 TABLE
    (
        StuEnrollId UNIQUEIDENTIFIER
       ,RecordDate DATETIME
       ,ScheduledHours DECIMAL(18, 2)
       ,ActualHours DECIMAL(18, 2)
       ,ActualRunningScheduledDays DECIMAL(18, 2)
       ,ActualRunningPresentHours DECIMAL(18, 2)
       ,ActualRunningAbsentHours DECIMAL(18, 2)
       ,ActualRunningMakeupHours DECIMAL(18, 2)
       ,ActualRunningTardyHours DECIMAL(18, 2)
       ,ActualRunningExcusedDays INT
       ,Tardy DECIMAL(18, 2)
       ,AdjustedRunningPresentHours DECIMAL(18, 2)
       ,AdjustedRunningAbsentHours DECIMAL(18, 2)
       ,tracktardies INT
       ,tardiesMakingAbsence INT
       ,intTardyBreakPoint INT
       ,rownumber INT
    );

INSERT INTO @MyEnrollments
            SELECT     enrollments.StuEnrollId
            FROM       dbo.arStuEnrollments enrollments
            INNER JOIN Inserted i ON i.StuEnrollId = enrollments.StuEnrollId;


INSERT INTO @HourTotals2 (
                         StuEnrollId
                        ,RecordDate
                        ,ScheduledHours
                        ,ActualHours
                        ,Tardy
                        ,ActualRunningScheduledDays
                        ,ActualRunningPresentHours
                        ,ActualRunningAbsentHours
                        ,ActualRunningMakeupHours
                        ,ActualRunningTardyHours
                        ,tracktardies
                        ,tardiesMakingAbsence
                        ,intTardyBreakPoint
                        ,rownumber
                         )
            SELECT   hoursBucketAll.StuEnrollId
                    ,hoursBucketAll.RecordDate
                    ,hoursBucketAll.SchedHours
                    ,hoursBucketAll.ActualHours
                    ,hoursBucketAll.isTardy
                    ,hoursBucketAll.ActualRunningScheduledHours
                    ,hoursBucketAll.ActualRunningPresentHours
                    ,hoursBucketAll.ActualRunningAbsentHours
                    ,hoursBucketAll.ActualRunningMakeupHours
                    ,hoursBucketAll.ActualRunningTardyHours
                    ,hoursBucketAll.TrackTardies
                    ,hoursBucketAll.TardiesMakingAbsence
                    ,hoursBucketAll.intTardyBreakPoint
                    ,hoursBucketAll.RowNumber
            FROM     (
                     SELECT *
                           ,CASE WHEN hoursBucketWithTardy.TrackTardies = 1
                                      AND hoursBucketWithTardy.intTardyBreakPoint % hoursBucketWithTardy.TardiesMakingAbsence = 0 THEN
                           ( -hoursBucketWithTardy.ActualRunningPresentHours )
                                 ELSE hoursBucketWithTardy.ActualRunningPresentHours
                            END AS AdjustedRunningPresentHours
                           ,CASE WHEN hoursBucketWithTardy.TrackTardies = 1
                                      AND hoursBucketWithTardy.intTardyBreakPoint = hoursBucketWithTardy.TardiesMakingAbsence THEN
                           ( -hoursBucketWithTardy.ActualRunningAbsentHours + hoursBucketWithTardy.SchedHours )
                                 ELSE hoursBucketWithTardy.ActualRunningAbsentHours
                            END AS AdjustedRunningAbsentHours
                     FROM   (
                            SELECT hoursBucket.StuEnrollId
                                  ,hoursBucket.RecordDate
                                  ,hoursBucket.SchedHours
                                  ,hoursBucket.ActualHours
                                  ,hoursBucket.isTardy
                                  ,hoursBucket.Absent
                                  ,SUM(   CASE WHEN hoursBucket.ActualHours <> 9999.00
                                                    AND hoursBucket.ActualHours <> 999 THEN hoursBucket.SchedHours
                                               ELSE 0
                                          END
                                      ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                               ORDER BY hoursBucket.RecordDate
                                             ) AS ActualRunningScheduledHours
                                  ,SUM(   CASE WHEN (
                                                    hoursBucket.ActualHours > 0
                                                    AND hoursBucket.ActualHours > hoursBucket.SchedHours
                                                    AND hoursBucket.ActualHours <> 9999.00
                                                    ) THEN ( hoursBucket.ActualHours - ( hoursBucket.ActualHours - hoursBucket.SchedHours ))
                                               WHEN hoursBucket.ActualHours <> 9999.00 THEN hoursBucket.ActualHours
                                               ELSE 0
                                          END
                                      ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                               ORDER BY hoursBucket.RecordDate
                                             ) AS ActualRunningPresentHours
                                  ,SUM(   hoursBucket.Absent
                                          + CASE WHEN hoursBucket.ActualHours > 0
                                                      AND hoursBucket.ActualHours < hoursBucket.SchedHours THEN hoursBucket.SchedHours - hoursBucket.ActualHours
                                                 ELSE 0
                                            END
                                      ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                               ORDER BY hoursBucket.RecordDate
                                             ) AS ActualRunningAbsentHours
                                  ,SUM(   CASE WHEN hoursBucket.ActualHours > 0
                                                    AND hoursBucket.ActualHours > hoursBucket.SchedHours
                                                    AND hoursBucket.ActualHours <> 9999.00 THEN hoursBucket.ActualHours - hoursBucket.SchedHours
                                               ELSE 0
                                          END
                                      ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                               ORDER BY hoursBucket.RecordDate
                                             ) AS ActualRunningMakeupHours
                                  ,SUM(   CASE WHEN hoursBucket.ActualHours > 0
                                                    AND hoursBucket.ActualHours < hoursBucket.SchedHours
                                                    AND hoursBucket.isTardy = 1 THEN hoursBucket.SchedHours - hoursBucket.ActualHours
                                               ELSE 0
                                          END
                                      ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                               ORDER BY hoursBucket.RecordDate
                                             ) AS ActualRunningTardyHours
                                  ,SUM(   CASE WHEN hoursBucket.TrackTardies = 1
                                                    AND hoursBucket.isTardy = 1 THEN 1
                                               ELSE 0
                                          END
                                      ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                               ORDER BY hoursBucket.RecordDate
                                             ) AS intTardyBreakPoint
                                  ,hoursBucket.TrackTardies
                                  ,hoursBucket.TardiesMakingAbsence
                                  ,hoursBucket.RowNumber
                            FROM   (
                                   SELECT *
                                         ,ROW_NUMBER() OVER ( ORDER BY dt.RecordDate ) AS RowNumber
                                   FROM   (
                                          SELECT     t1.StuEnrollId
                                                    ,t1.RecordDate
                                                    ,t1.SchedHours
                                                    ,t1.ActualHours
                                                    ,CASE WHEN (
                                                               (
                                                               (
                                                               (
                                                               AAUT.UnitTypeDescrip = ''Present Absent''
                                                               OR AAUT.UnitTypeDescrip = ''Minutes''
                                                                  AND t1.SchedHours >= 1
                                                               )
                                                               OR (
                                                                  AAUT.UnitTypeDescrip = ''Clock Hours''
                                                                  AND t1.SchedHours > 0
                                                                  )
                                                               )
                                                               AND t1.SchedHours NOT IN ( 999, 9999 )
                                                               )
                                                               AND t1.ActualHours = 0
                                                               ) THEN t1.SchedHours
                                                          ELSE 0
                                                     END AS Absent
                                                    ,t1.isTardy
                                                    ,t3.TrackTardies
                                                    ,t3.TardiesMakingAbsence
                                          FROM       arStudentClockAttendance t1
                                          INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                          INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                          INNER JOIN arAttUnitType AS AAUT ON AAUT.UnitTypeId = t3.UnitTypeId
                                          INNER JOIN @MyEnrollments ON t1.StuEnrollId = [@MyEnrollments].StuEnrollId
                                          WHERE --t3.UnitTypeId IN ( ''ef5535c2-142c-4223-ae3c-25a50a153cc6'',''B937C92E-FD7A-455E-A731-527A9918C734'' ) -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'') -
                                                     AAUT.UnitTypeDescrip IN ( ''Present Absent'', ''Clock Hours'', ''Minutes'' )
                                                     AND t1.ActualHours <> 9999.00
                                          ) dt
                                   --ORDER BY StuEnrollId
                                   --        ,MeetDate
                                   ) hoursBucket
                            ) hoursBucketWithTardy
                     ) hoursBucketAll
            ORDER BY hoursBucketAll.StuEnrollId
                    ,hoursBucketAll.RecordDate;


DELETE     dbo.syStudentAttendanceSummary
FROM       dbo.syStudentAttendanceSummary
INNER JOIN @MyEnrollments ON syStudentAttendanceSummary.StuEnrollId = [@MyEnrollments].StuEnrollId;

INSERT INTO syStudentAttendanceSummary (
                                       StuEnrollId
                                      ,ClsSectionId
                                      ,StudentAttendedDate
                                      ,ScheduledDays
                                      ,ActualDays
                                      ,ActualRunningScheduledDays
                                      ,ActualRunningPresentDays
                                      ,ActualRunningAbsentDays
                                      ,ActualRunningMakeupDays
                                      ,ActualRunningTardyDays
                                      ,AdjustedPresentDays
                                      ,AdjustedAbsentDays
                                      ,AttendanceTrackType
                                      ,ModUser
                                      ,ModDate
                                      ,IsExcused
                                      ,ActualRunningExcusedDays
                                      ,IsTardy
                                       )
            SELECT StuEnrollId
                  ,NULL
                  ,RecordDate
                  ,ScheduledHours
                  ,ActualHours
                  ,ActualRunningScheduledDays
                  ,ActualRunningPresentHours
                  ,ActualRunningAbsentHours
                  ,ISNULL(ActualRunningMakeupHours, 0)
                  ,ActualRunningTardyHours
                  ,AdjustedRunningPresentHours
                  ,AdjustedRunningAbsentHours
                  ,''Post Attendance by Class''
                  ,''sa''
                  ,GETDATE()
                  ,0
                  ,NULL
                  ,Tardy
            FROM   @HourTotals2;




SET NOCOUNT OFF;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[saPmtPeriods_PeriodNumber] on [dbo].[saPmtPeriods]'
GO
IF OBJECT_ID(N'[dbo].[saPmtPeriods_PeriodNumber]', 'TR') IS NULL
EXEC sp_executesql N'

-- =============================================
-- Author:		<Author,Sweta>
-- Create date: <Create Date,10/3/2019>
-- Description:	<Description,Updates period number depending on the increment value>
-- =============================================
CREATE TRIGGER [dbo].[saPmtPeriods_PeriodNumber]
ON [dbo].[saPmtPeriods]
AFTER UPDATE, INSERT, DELETE
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @incrementId UNIQUEIDENTIFIER;
		IF EXISTS (SELECT * FROM inserted) AND NOT EXISTS(SELECT * FROM deleted)
		BEGIN
		    SET @incrementId = (
                           SELECT TOP 1 Inserted.IncrementId
                           FROM   Inserted
                           );
		END
        ELSE	
		BEGIN
		    SET @incrementId = (
                           SELECT TOP 1 Deleted.IncrementId
                           FROM   Deleted
                           );
		END
        DECLARE @pmtTbl AS TABLE
            (
                NewPeriod INT
               ,PmtPeriodId UNIQUEIDENTIFIER
               ,PeriodNumber INT
               ,IncrementValue DECIMAL(10, 2)
            );
        INSERT INTO @pmtTbl (
                            NewPeriod
                           ,PmtPeriodId
                           ,PeriodNumber
                           ,IncrementValue
                            )
                    SELECT DENSE_RANK() OVER ( ORDER BY IncrementValue ) AS newPeriod
                          ,PmtPeriodId
                          ,PeriodNumber
                          ,IncrementValue
                    FROM   dbo.saPmtPeriods
                    WHERE  IncrementId = @incrementId;

        UPDATE     dbo.saPmtPeriods
        SET        PeriodNumber = [@pmtTbl].NewPeriod
        FROM       dbo.saPmtPeriods
        INNER JOIN @pmtTbl ON [@pmtTbl].PmtPeriodId = saPmtPeriods.PmtPeriodId
        WHERE      IncrementId = @incrementId;

    END;
	SET NOCOUNT OFF;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[SyStudentStatusChanges_RemoveScheduledForTerminatedOrGrad] on [dbo].[syStudentStatusChanges]'
GO
IF OBJECT_ID(N'[dbo].[SyStudentStatusChanges_RemoveScheduledForTerminatedOrGrad]', 'TR') IS NULL
EXEC sp_executesql N'

--==========================================================================================  
-- TRIGGER [SyStudentStatusChanges_RemoveScheduledForTerminatedOrGrad]e  
-- UPDATE,INSERT  Remove Scheduled and Absent Hours After LDA 
--==========================================================================================  
CREATE   TRIGGER [dbo].[SyStudentStatusChanges_RemoveScheduledForTerminatedOrGrad]
ON [dbo].[syStudentStatusChanges]
FOR UPDATE, INSERT
AS
BEGIN

    SET NOCOUNT ON;
    --Remove Scheduled and Absent Hours After LDA

    BEGIN
		
        UPDATE asca
        SET SchedHoursOnTermination = SchedHours
        FROM arStudentClockAttendance asca
            JOIN syStudentStatusChanges ssc ON ssc.StuEnrollId = asca.StuEnrollId
            JOIN Inserted ins ON ins.StuEnrollId = asca.StuEnrollId
			JOIN dbo.syStatusCodes sco ON sco.StatusCodeId = ins.OrigStatusId
			JOIN dbo.syStatusCodes scn ON scn.StatusCodeId = ins.NewStatusId
			JOIN dbo.sySysStatus sst ON sst.SysStatusId = sco.SysStatusId
        WHERE scn.SysStatusId IN ( 14, 12 )
			  AND sst.InSchool = 1 
              AND RecordDate >
              (
                  SELECT dbo.GetLDA(ssc.StuEnrollId)
              );


        UPDATE asca
        SET SchedHours = 0
        FROM arStudentClockAttendance asca
            JOIN syStudentStatusChanges ssc ON ssc.StuEnrollId = asca.StuEnrollId
            JOIN Inserted ins ON ins.StuEnrollId = asca.StuEnrollId
            JOIN syStatusCodes sc ON sc.StatusCodeId = ins.NewStatusId
        WHERE sc.SysStatusId IN ( 14, 12 )
              AND RecordDate >
              (
                  SELECT dbo.GetLDA(ssc.StuEnrollId)
              );

    END;


    SET NOCOUNT OFF;
END;

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[SyStudentStatusChanges_RestoreSchedHoursAfterUndoTermination] on [dbo].[syStudentStatusChanges]'
GO
IF OBJECT_ID(N'[dbo].[SyStudentStatusChanges_RestoreSchedHoursAfterUndoTermination]', 'TR') IS NULL
EXEC sp_executesql N'--==========================================================================================  
-- TRIGGER [SyStudentStatusChanges_RestoreSchedHoursAfterUndoTermination]  
-- DELETE  Restore Scheduled Hours After Undoing Termination
--========================================================================================== 
CREATE TRIGGER [dbo].[SyStudentStatusChanges_RestoreSchedHoursAfterUndoTermination]
ON [dbo].[syStudentStatusChanges]
FOR DELETE
AS 
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    BEGIN
	
        UPDATE asca
        SET SchedHours = 
			CASE  
                WHEN SchedHoursOnTermination > 0.0 THEN SchedHoursOnTermination 
                ELSE SchedHours
            END
			,asca.SchedHoursOnTermination = 0
        FROM arStudentClockAttendance asca
            JOIN syStudentStatusChanges ssc ON ssc.StuEnrollId = asca.StuEnrollId
            JOIN Deleted del ON del.StuEnrollId = asca.StuEnrollId
			JOIN dbo.syStatusCodes sco ON sco.StatusCodeId = del.OrigStatusId
			JOIN dbo.syStatusCodes scn ON scn.StatusCodeId = del.NewStatusId
			JOIN dbo.sySysStatus sst ON sst.SysStatusId = sco.SysStatusId
        WHERE scn.SysStatusId IN ( 14, 12 )
			  AND sst.InSchool = 1 
              AND RecordDate >
              (
                  SELECT dbo.GetLDA(ssc.StuEnrollId)
              );
	END
	
    SET NOCOUNT OFF;
END
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]'
GO
IF OBJECT_ID(N'[dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]', 'P') IS NULL
EXEC sp_executesql N'
--=================================================================================================
-- USP_AT_Step02_InsertAttendance_Day_PresentAbsent
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]
    (
        @StuEnrollIdList AS VARCHAR(MAX) = NULL
    )
AS
    BEGIN

        DECLARE @MyEnrollments TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
            );

        DECLARE @HourTotals2 TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,RecordDate DATETIME
               ,ScheduledHours DECIMAL(18, 2)
               ,ActualHours DECIMAL(18, 2)
               ,ActualRunningScheduledDays DECIMAL(18, 2)
               ,ActualRunningPresentHours DECIMAL(18, 2)
               ,ActualRunningAbsentHours DECIMAL(18, 2)
               ,ActualRunningMakeupHours DECIMAL(18, 2)
               ,ActualRunningTardyHours DECIMAL(18, 2)
               ,ActualRunningExcusedDays INT
               ,Tardy DECIMAL(18, 2)
               ,AdjustedRunningPresentHours DECIMAL(18, 2)
               ,AdjustedRunningAbsentHours DECIMAL(18, 2)
               ,tracktardies INT
               ,tardiesMakingAbsence INT
               ,intTardyBreakPoint INT
               ,rownumber INT
            );

        INSERT INTO @MyEnrollments
                    SELECT enrollments.StuEnrollId
                    FROM   dbo.arStuEnrollments enrollments
                    WHERE  (
                           @StuEnrollIdList IS NULL
                           OR ( enrollments.StuEnrollId IN (
                                                           SELECT Val
                                                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                           )
                              )
                           );


        INSERT INTO @HourTotals2 (
                                 StuEnrollId
                                ,RecordDate
                                ,ScheduledHours
                                ,ActualHours
                                ,Tardy
                                ,ActualRunningScheduledDays
                                ,ActualRunningPresentHours
                                ,ActualRunningAbsentHours
                                ,ActualRunningMakeupHours
                                ,ActualRunningTardyHours
                                ,tracktardies
                                ,tardiesMakingAbsence
                                ,intTardyBreakPoint
                                ,rownumber
                                 )
                    SELECT   hoursBucketAll.StuEnrollId
                            ,hoursBucketAll.RecordDate
                            ,hoursBucketAll.SchedHours
                            ,hoursBucketAll.ActualHours
                            ,hoursBucketAll.isTardy
                            ,hoursBucketAll.ActualRunningScheduledHours
                            ,hoursBucketAll.ActualRunningPresentHours
                            ,hoursBucketAll.ActualRunningAbsentHours
                            ,hoursBucketAll.ActualRunningMakeupHours
                            ,hoursBucketAll.ActualRunningTardyHours
                            ,hoursBucketAll.TrackTardies
                            ,hoursBucketAll.TardiesMakingAbsence
                            ,hoursBucketAll.intTardyBreakPoint
                            ,hoursBucketAll.RowNumber
                    FROM     (
                             SELECT *
                                   ,CASE WHEN hoursBucketWithTardy.TrackTardies = 1
                                              AND hoursBucketWithTardy.intTardyBreakPoint % hoursBucketWithTardy.TardiesMakingAbsence = 0 THEN
                                   ( -hoursBucketWithTardy.ActualRunningPresentHours )
                                         ELSE hoursBucketWithTardy.ActualRunningPresentHours
                                    END AS AdjustedRunningPresentHours
                                   ,CASE WHEN hoursBucketWithTardy.TrackTardies = 1
                                              AND hoursBucketWithTardy.intTardyBreakPoint = hoursBucketWithTardy.TardiesMakingAbsence THEN
                                   ( -hoursBucketWithTardy.ActualRunningAbsentHours + hoursBucketWithTardy.SchedHours )
                                         ELSE hoursBucketWithTardy.ActualRunningAbsentHours
                                    END AS AdjustedRunningAbsentHours
                             FROM   (
                                    SELECT hoursBucket.StuEnrollId
                                          ,hoursBucket.RecordDate
                                          ,hoursBucket.SchedHours
                                          ,hoursBucket.ActualHours
                                          ,hoursBucket.isTardy
                                          ,hoursBucket.Absent
                                          ,SUM(   CASE WHEN hoursBucket.ActualHours <> 9999.00
                                                            AND hoursBucket.ActualHours <> 999 THEN hoursBucket.SchedHours
                                                       ELSE 0
                                                  END
                                              ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                                       ORDER BY hoursBucket.RecordDate
                                                     ) AS ActualRunningScheduledHours
                                          ,SUM(   CASE WHEN (
                                                            hoursBucket.ActualHours > 0
                                                            AND hoursBucket.ActualHours > hoursBucket.SchedHours
                                                            AND hoursBucket.ActualHours <> 9999.00
                                                            ) THEN ( hoursBucket.ActualHours - ( hoursBucket.ActualHours - hoursBucket.SchedHours ))
                                                       WHEN hoursBucket.ActualHours <> 9999.00 THEN hoursBucket.ActualHours
                                                       ELSE 0
                                                  END
                                              ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                                       ORDER BY hoursBucket.RecordDate
                                                     ) AS ActualRunningPresentHours
                                          ,SUM(   hoursBucket.Absent
                                                  + CASE WHEN hoursBucket.ActualHours > 0
                                                              AND hoursBucket.ActualHours < hoursBucket.SchedHours THEN
                                                             hoursBucket.SchedHours - hoursBucket.ActualHours
                                                         ELSE 0
                                                    END
                                              ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                                       ORDER BY hoursBucket.RecordDate
                                                     ) AS ActualRunningAbsentHours
                                          ,SUM(   CASE WHEN hoursBucket.ActualHours > 0
                                                            AND hoursBucket.ActualHours > hoursBucket.SchedHours
                                                            AND hoursBucket.ActualHours <> 9999.00 THEN hoursBucket.ActualHours - hoursBucket.SchedHours
                                                       ELSE 0
                                                  END
                                              ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                                       ORDER BY hoursBucket.RecordDate
                                                     ) AS ActualRunningMakeupHours
                                          ,SUM(   CASE WHEN hoursBucket.ActualHours > 0
                                                            AND hoursBucket.ActualHours < hoursBucket.SchedHours
                                                            AND hoursBucket.isTardy = 1 THEN hoursBucket.SchedHours - hoursBucket.ActualHours
                                                       ELSE 0
                                                  END
                                              ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                                       ORDER BY hoursBucket.RecordDate
                                                     ) AS ActualRunningTardyHours
                                          ,SUM(   CASE WHEN hoursBucket.TrackTardies = 1
                                                            AND hoursBucket.isTardy = 1 THEN 1
                                                       ELSE 0
                                                  END
                                              ) OVER ( PARTITION BY hoursBucket.StuEnrollId
                                                       ORDER BY hoursBucket.RecordDate
                                                     ) AS intTardyBreakPoint
                                          ,hoursBucket.TrackTardies
                                          ,hoursBucket.TardiesMakingAbsence
                                          ,hoursBucket.RowNumber
                                    FROM   (
                                           SELECT *
                                                 ,ROW_NUMBER() OVER ( ORDER BY dt.RecordDate ) AS RowNumber
                                           FROM   (
                                                  SELECT     t1.StuEnrollId
                                                            ,t1.RecordDate
                                                            ,t1.SchedHours
                                                            ,t1.ActualHours
                                                            ,CASE WHEN (
                                                                       (
                                                                       (
                                                                       (
                                                                       AAUT.UnitTypeDescrip = ''Present Absent''
                                                                       AND t1.SchedHours >= 1
                                                                       )
                                                                       OR (
                                                                          AAUT.UnitTypeDescrip = ''Clock Hours''
                                                                          AND t1.SchedHours > 0
                                                                          )
                                                                       )
                                                                       AND t1.SchedHours NOT IN ( 999, 9999 )
                                                                       )
                                                                       AND t1.ActualHours = 0
                                                                       ) THEN t1.SchedHours
                                                                  ELSE 0
                                                             END AS Absent
                                                            ,t1.isTardy
                                                            ,t3.TrackTardies
                                                            ,t3.TardiesMakingAbsence
                                                  FROM       arStudentClockAttendance t1
                                                  INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                  INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                                  INNER JOIN arAttUnitType AS AAUT ON AAUT.UnitTypeId = t3.UnitTypeId
                                                  INNER JOIN @MyEnrollments ON t1.StuEnrollId = [@MyEnrollments].StuEnrollId
                                                  WHERE --t3.UnitTypeId IN ( ''ef5535c2-142c-4223-ae3c-25a50a153cc6'',''B937C92E-FD7A-455E-A731-527A9918C734'' ) -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'') -
                                                             AAUT.UnitTypeDescrip IN ( ''Present Absent'', ''Clock Hours'' )
                                                             AND t1.ActualHours <> 9999.00
                                                  ) dt
                                           --ORDER BY StuEnrollId
                                           --        ,MeetDate
                                           ) hoursBucket
                                    ) hoursBucketWithTardy
                             ) hoursBucketAll
                    ORDER BY hoursBucketAll.StuEnrollId
                            ,hoursBucketAll.RecordDate;


        DELETE     dbo.syStudentAttendanceSummary
        FROM       dbo.syStudentAttendanceSummary
        INNER JOIN @MyEnrollments ON syStudentAttendanceSummary.StuEnrollId = [@MyEnrollments].StuEnrollId;

        INSERT INTO syStudentAttendanceSummary (
                                               StuEnrollId
                                              ,ClsSectionId
                                              ,StudentAttendedDate
                                              ,ScheduledDays
                                              ,ActualDays
                                              ,ActualRunningScheduledDays
                                              ,ActualRunningPresentDays
                                              ,ActualRunningAbsentDays
                                              ,ActualRunningMakeupDays
                                              ,ActualRunningTardyDays
                                              ,AdjustedPresentDays
                                              ,AdjustedAbsentDays
                                              ,AttendanceTrackType
                                              ,ModUser
                                              ,ModDate
                                              ,IsExcused
                                              ,ActualRunningExcusedDays
                                              ,IsTardy
                                               )
                    SELECT StuEnrollId
                          ,NULL
                          ,RecordDate
                          ,ScheduledHours
                          ,ActualHours
                          ,ActualRunningScheduledDays
                          ,ActualRunningPresentHours
                          ,ActualRunningAbsentHours
                          ,ISNULL(ActualRunningMakeupHours, 0)
                          ,ActualRunningTardyHours
                          ,AdjustedRunningPresentHours
                          ,AdjustedRunningAbsentHours
                          ,''Post Attendance by Class''
                          ,''sa''
                          ,GETDATE()
                          ,0
                          ,NULL
                          ,Tardy
                    FROM   @HourTotals2;


    END;


--=================================================================================================
-- END  --  USP_AT_Step02_InsertAttendance_Day_PresentAbsent
--=================================================================================================
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_GetAbsentToday]'
GO
IF OBJECT_ID(N'[dbo].[USP_GetAbsentToday]', 'P') IS NULL
EXEC sp_executesql N'

--=================================================================================================
-- USP_GetAbsentToday
--=================================================================================================
-- =============================================
-- Author:		Kimberly 
-- Create date: 05/13/2019
-- Description: Get List of students that are Absent Today
--             
--                 
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAbsentToday]
    @DateRun DATE,
    @CampusId UNIQUEIDENTIFIER
AS
BEGIN

    DECLARE @isTimeClock BIT;
    SET @isTimeClock = 1;
    IF EXISTS
    (
        SELECT *
        FROM dbo.arPrgVersions prg
            JOIN dbo.arStuEnrollments ste
                ON ste.PrgVerId = prg.PrgVerId
        WHERE ste.CampusId = @CampusId
              AND UseTimeClock = 0
    )
    BEGIN
        SET @isTimeClock = 0;
    END;


    IF (@isTimeClock = 0)
    BEGIN
        
        SELECT DISTINCT
               LTRIM(RTRIM(cp.CampDescrip)) AS CampDescrip,
               l.StudentNumber,
               (FirstName + '' '' + LastName) AS FullName,
               FirstName,
               LastName,
               (
                   SELECT STUFF(STUFF(STUFF(
                                      (
                                          SELECT TOP (1)
                                                 Phone
                                          FROM adLeadPhone
                                          WHERE LeadId = l.LeadId
                                          ORDER BY IsBest DESC,
                                                   IsShowOnLeadPage DESC,
                                                   Position DESC
                                      ),
                                      1,
                                      0,
                                      ''(''
                                           ),
                                      5,
                                      0,
                                      '') ''
                                     ),
                                10,
                                0,
                                ''-''
                               )
               ) AS Phone,
               sc.StatusCodeDescrip,
               (
                   SELECT TOP (1)
                          CONVERT(DATE, RecordDate)
                   FROM arStudentClockAttendance
                   WHERE SchedHours > 0
                         AND ActualHours > 0
                         AND StuEnrollId = e.StuEnrollId
						 AND ActualHours NOT IN ( 999,9999,99999 )
                   ORDER BY RecordDate DESC
               ) AS LDA
        FROM dbo.arStuEnrollments e
            JOIN syStatusCodes sc
                ON sc.StatusCodeId = e.StatusCodeId
            JOIN dbo.arStudentSchedules ss
                ON ss.StuEnrollId = e.StuEnrollId
            JOIN dbo.arProgSchedules ps
                ON ps.ScheduleId = ss.ScheduleId
            JOIN dbo.arProgScheduleDetails psd
                ON psd.ScheduleId = ss.ScheduleId
            JOIN arStudentClockAttendance sca
                ON sca.StuEnrollId = e.StuEnrollId
            JOIN adLeads l
                ON l.LeadId = e.LeadId
            JOIN dbo.adLeadPhone lp
                ON lp.LeadId = l.LeadId
            JOIN dbo.syCampuses cp
                ON cp.CampusId = e.CampusId
        WHERE e.ExpStartDate <= @DateRun
              AND e.ExpGradDate >= @DateRun
              AND cp.CampusId = @CampusId
              AND e.StuEnrollId IN
                  (
                      SELECT DISTINCT
                             sc.StuEnrollId
                      FROM arStudentClockAttendance sc
                          JOIN dbo.arStuEnrollments e
                              ON e.StuEnrollId = sc.StuEnrollId
                      WHERE CONVERT(DATE, sc.RecordDate) = @DateRun
                            AND sc.ActualHours = 0
                            AND sc.SchedHours > 0
							AND sc.ActualHours NOT IN ( 999,9999,99999 )
                           
                  );
        RETURN;

    END;

    IF (@isTimeClock = 1)
    BEGIN

        DECLARE @dayOfWeek INT;
        SET @dayOfWeek =
        (
            SELECT CASE DATENAME(WEEKDAY, @DateRun)
                       WHEN ''Monday'' THEN
                           1
                       WHEN ''Tuesday'' THEN
                           2
                       WHEN ''Wednesday'' THEN
                           3
                       WHEN ''Thursday'' THEN
                           4
                       WHEN ''Friday'' THEN
                           5
                       WHEN ''Saturday'' THEN
                           6
                       WHEN ''Sunday'' THEN
                           7
                   END AS wDay
        );

        SELECT DISTINCT
               LTRIM(RTRIM(cp.CampDescrip)) AS CampDescrip,
               l.StudentNumber,
               (FirstName + '' '' + LastName) AS FullName,
               FirstName,
               LastName,
               (
                   SELECT STUFF(STUFF(STUFF(
                                      (
                                          SELECT TOP (1)
                                                 Phone
                                          FROM adLeadPhone
                                          WHERE LeadId = l.LeadId
                                          ORDER BY IsBest DESC,
                                                   IsShowOnLeadPage DESC,
                                                   Position DESC
                                      ),
                                      1,
                                      0,
                                      ''(''
                                           ),
                                      5,
                                      0,
                                      '') ''
                                     ),
                                10,
                                0,
                                ''-''
                               )
               ) AS Phone,
               sc.StatusCodeDescrip,
               (
                   SELECT TOP (1)
                          CONVERT(DATE, RecordDate)
                   FROM arStudentClockAttendance
                   WHERE SchedHours > 0
                         AND ActualHours > 0
                         AND StuEnrollId = e.StuEnrollId
						 AND ActualHours NOT IN ( 999,9999,99999 )
                   ORDER BY RecordDate DESC
               ) AS LDA
        FROM dbo.arStuEnrollments e
            JOIN syStatusCodes sc
                ON sc.StatusCodeId = e.StatusCodeId
            JOIN dbo.arStudentSchedules ss
                ON ss.StuEnrollId = e.StuEnrollId
            JOIN dbo.arProgSchedules ps
                ON ps.ScheduleId = ss.ScheduleId
            JOIN dbo.arProgScheduleDetails psd
                ON psd.ScheduleId = ss.ScheduleId
            JOIN arStudentTimeClockPunches p
                ON p.StuEnrollId = e.StuEnrollId
            JOIN adLeads l
                ON l.LeadId = e.LeadId
            JOIN dbo.adLeadPhone lp
                ON lp.LeadId = l.LeadId
            JOIN dbo.syCampuses cp
                ON cp.CampusId = e.CampusId
        WHERE e.ExpStartDate <= @DateRun
              AND e.ExpGradDate >= @DateRun
              AND psd.dw = @dayOfWeek
              AND psd.total > 0
              AND p.Status = 1
              AND e.CampusId = @CampusId
              AND e.StuEnrollId NOT IN
                  (
                      SELECT DISTINCT
                             sp.StuEnrollId
                      FROM arStudentTimeClockPunches sp
                          JOIN dbo.arStuEnrollments e
                              ON e.StuEnrollId = sp.StuEnrollId
                      WHERE CONVERT(DATE, PunchTime) = @DateRun
                  )
              AND SysStatusId IN ( 9, 20, 6 )
        ORDER BY l.LastName,
                 l.FirstName;
        RETURN;
    END;


END;
--=================================================================================================
-- END  --  USP_GetAbsentToday
--=================================================================================================


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_ProcessPaymentPeriodsHours]'
GO
IF OBJECT_ID(N'[dbo].[USP_ProcessPaymentPeriodsHours]', 'P') IS NULL
EXEC sp_executesql N'--================================================================================================= 
-- USP_ProcessPaymentPeriodsHours 
--================================================================================================= 
-- ============================================= 
-- Author:		Ginzo, John 
-- Create date: 11/17/2014 
-- ============================================= 
CREATE PROCEDURE [dbo].[USP_ProcessPaymentPeriodsHours]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from 
        -- interfering with SELECT statements. 
        SET NOCOUNT ON;


        BEGIN TRAN PAYMENTPERIODTRANSACTION;

        ----------------------------------------------------------------- 
        --Table to store Student population 
        ----------------------------------------------------------------- 
        DECLARE @MasterStudentPopulation TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ClsSectionId VARCHAR(250)
               ,MaxDate DATETIME
               ,ScheduledHours DECIMAL
               ,MakeUpHours DECIMAL
               ,ActualHours DECIMAL
               ,TermId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,PrgVerCode VARCHAR(250)
               ,StartDate DATETIME
               ,EndDate DATETIME
            );

        DECLARE @MasterStudentPopulationRunningTotal TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ClsSectionId VARCHAR(250)
               ,PrgVerId UNIQUEIDENTIFIER
               ,MeetingDate DATETIME
               ,TermId UNIQUEIDENTIFIER
               ,ScheduledHoursRunningTotal DECIMAL(18, 2)
               ,ActualHoursRunningTotal DECIMAL(18, 2)
               ,IsExcused BIT
               ,ExcusedHours DECIMAL(18, 2)
               ,ScheduledDays DECIMAL(18, 2)
               ,ActualDays DECIMAL(18, 2)
            );


        ----------------------------------------------------------------- 
        -- TABLE FOR HOURS DATA 
        ----------------------------------------------------------------- 
        DECLARE @HoursScheduledActual TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,ScheduledHours DECIMAL
               ,ActualHours DECIMAL
               ,MakeUpHours DECIMAL
            );


        DECLARE @HoursExcused TABLE
            (
                PmtPeriodId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,ExcusedHours DECIMAL(18, 2)
            );

        DECLARE @TransactionDates TABLE
            (
                PmtPeriodId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,TransactionDate DATETIME
            );
        ----------------------------------------------------------------- 
        -- Insert student population into table 
        ----------------------------------------------------------------- 
        INSERT INTO @MasterStudentPopulation
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,CS.ClsSectionId
                                       ,MD.MaxDate
                                       ,SH.ScheduledHours
                                       ,MUH.MakeUpHours
                                       ,AH.ActualHours
                                       ,TT.TermId
                                       ,PV.PrgVerId
                                       ,PV.PrgVerCode
                                       ,TT.StartDate
                                       ,TT.EndDate
                    FROM       dbo.arStuEnrollments SE
                    INNER JOIN dbo.syStudentAttendanceSummary SA ON SE.StuEnrollId = SA.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningScheduledDays) AS ScheduledHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) SH ON SA.StuEnrollId = SH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningPresentDays) AS ActualHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) AH ON SE.StuEnrollId = AH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningMakeupDays) AS MakeUpHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) MUH ON SE.StuEnrollId = MUH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(StudentAttendedDate) AS MaxDate
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) MD ON SE.StuEnrollId = MD.StuEnrollId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arClassSections CS ON SA.ClsSectionId = CS.ClsSectionId
                    INNER JOIN dbo.arTerm TT ON CS.TermId = TT.TermId
                    INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                    INNER JOIN dbo.syStatuses ST ON PV.StatusId = ST.StatusId
                    WHERE      SC.SysStatusId IN ( 9, 20 )
								--removed due to this condition being required for by term or course charges and auto post is for payment period charging
                               --AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(SE.StuEnrollId, CS.ClsSectionId) --Added by Troy as part of fix for US8005 
                               AND SE.DisableAutoCharge <> 1 --Exclude enrollments with auto charge disabled  
                               AND PV.ProgramRegistrationType = 0
                    --AND pv.PrgVerId=''5E8BFC2C-FE62-4220-BDC6-51C7941F5AE0'' 
                    --AND se.StuEnrollId=''71A812F7-972D-4AB6-971C-3CFD02A3086B'' 

                    UNION
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,CS.ClsSectionId
                                       ,MD.MaxDate
                                       ,SH.ScheduledHours
                                       ,MUH.MakeUpHours
                                       ,AH.ActualHours
                                       ,TT.TermId
                                       ,PV.PrgVerId
                                       ,PV.PrgVerCode
                                       ,TT.StartDate
                                       ,TT.EndDate
                    FROM       dbo.arStuEnrollments SE
                    INNER JOIN dbo.syStudentAttendanceSummary SA ON SE.StuEnrollId = SA.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningScheduledDays) AS ScheduledHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) SH ON SA.StuEnrollId = SH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningPresentDays) AS ActualHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) AH ON SE.StuEnrollId = AH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningMakeupDays) AS MakeUpHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) MUH ON SE.StuEnrollId = MUH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(StudentAttendedDate) AS MaxDate
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) MD ON SE.StuEnrollId = MD.StuEnrollId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arTerm TT ON TT.ProgramVersionId = PV.PrgVerId
                    INNER JOIN dbo.arProgVerDef pvd ON pvd.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arClassSections CS ON CS.ProgramVersionDefinitionId = pvd.ProgVerDefId
                    INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                    INNER JOIN dbo.syStatuses ST ON PV.StatusId = ST.StatusId
                    WHERE      SC.SysStatusId IN ( 9, 20 )
							   --removed due to this condition being required for by term or course charges and auto post is for payment period charging
                               --AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(SE.StuEnrollId, CS.ClsSectionId) --Added by Troy as part of fix for US8005 
                               AND SE.DisableAutoCharge <> 1 --Exclude enrollments with auto charge disabled  
                               AND PV.ProgramRegistrationType = 1;
        --AND pv.PrgVerId=''5E8BFC2C-FE62-4220-BDC6-51C7941F5AE0'' 
        --AND se.StuEnrollId=''71A812F7-972D-4AB6-971C-3CFD02A3086B'' 


        -- Get Running Totals for Current Term 
        INSERT INTO @MasterStudentPopulationRunningTotal
                    SELECT     DISTINCT SAS.StuEnrollId
                                       ,SAS.ClsSectionId
                                       ,SE.PrgVerId
                                       ,SAS.StudentAttendedDate
                                       ,(
                                        SELECT     TOP 1 TermId
                                        FROM       arResults R
                                        INNER JOIN arClassSections CS ON R.TestId = CS.ClsSectionId
                                        WHERE      R.StuEnrollId = SAS.StuEnrollId
                                                   AND CS.ClsSectionId = ClsSectionId
                                        ) AS termId
                                       ,SAS.ActualRunningScheduledDays
                                       ,SAS.AdjustedPresentDays
                                       ,SAS.IsExcused
                                       ,( CASE WHEN IsExcused = 1 THEN ( SAS.ScheduledDays - SAS.ActualDays )
                                               ELSE 0
                                          END
                                        ) AS ExcusedHours
                                       ,SAS.ScheduledDays
                                       ,SAS.ActualDays
                    FROM       syStudentAttendanceSummary SAS
                    INNER JOIN dbo.arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                    WHERE      SAS.StuEnrollId IN (
                                                  SELECT DISTINCT StuEnrollId
                                                  FROM   @MasterStudentPopulation
                                                  )
                    ORDER BY   SAS.StuEnrollId
                              ,StudentAttendedDate;


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error creating Student Population.'', 16, 1);
                RETURN;
            END;






        ----------------------------------------------------------------- 
        -- insert hours data into table 
        ----------------------------------------------------------------- 
        INSERT INTO @HoursScheduledActual
                    SELECT   StuEnrollId
                            ,PrgVerId
                            ,ScheduledHours
                            ,ActualHours
                            ,MakeUpHours
                    FROM     @MasterStudentPopulation
                    GROUP BY StuEnrollId
                            ,PrgVerId
                            ,ScheduledHours
                            ,ActualHours
                            ,MakeUpHours;


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error creating hours data.'', 16, 1);
                RETURN;
            END;




        ----------------------------------------------------------------------------------- 
        -- Cursor to handle Excused Hours 
        ----------------------------------------------------------------------------------- 
        DECLARE @pmtperiodid UNIQUEIDENTIFIER;
        DECLARE @CumulativeValue DECIMAL;
        DECLARE @StuEnrollId UNIQUEIDENTIFIER;
        DECLARE @NextCumulativeValue DECIMAL;




        DECLARE db_cursor CURSOR FOR
            SELECT     p.PmtPeriodId
                      ,p.CumulativeValue
                      ,e.StuEnrollId
                      ,(
                       SELECT   TOP 1 child.CumulativeValue
                       FROM     saPmtPeriods child
                       WHERE    child.IncrementId = p.IncrementId
                                AND child.PeriodNumber = p.PeriodNumber + 1
                       ORDER BY p.PeriodNumber ASC
                               ,p.CumulativeValue ASC
                       ) AS NextCumulativeValue
            FROM       dbo.saPmtPeriods p
            INNER JOIN dbo.saIncrements I ON p.IncrementId = I.IncrementId
            INNER JOIN dbo.saBillingMethods B ON I.BillingMethodId = B.BillingMethodId
            INNER JOIN dbo.arPrgVersions prg ON B.BillingMethodId = prg.BillingMethodId
            INNER JOIN dbo.arStuEnrollments e ON prg.PrgVerId = e.PrgVerId
            WHERE      I.IncrementType = 0
            ORDER BY   e.StuEnrollId
                      ,e.PrgVerId
                      ,I.IncrementId
                      ,p.PeriodNumber;

        DECLARE @FloorValue DECIMAL;
        SET @FloorValue = 0;

        DECLARE @currentEnrollId UNIQUEIDENTIFIER;
        SET @currentEnrollId = NEWID();

        DECLARE @curCumValue DECIMAL;
        SET @curCumValue = 0;

        OPEN db_cursor;

        FETCH NEXT FROM db_cursor
        INTO @pmtperiodid
            ,@CumulativeValue
            ,@StuEnrollId
            ,@NextCumulativeValue;


        WHILE @@FETCH_STATUS = 0
            BEGIN

                IF @StuEnrollId <> @currentEnrollId
                    BEGIN
                        SET @FloorValue = 0;
                        SET @curCumValue = @CumulativeValue;
                        SET @currentEnrollId = @StuEnrollId;
                    END;
                ELSE
                    BEGIN
                        SET @FloorValue = @curCumValue;
                    END;


                --SELECT @FloorValue,@CumulativeValue,@currentEnrollId,@StuEnrollId 

                INSERT INTO @HoursExcused
                            SELECT   @pmtperiodid AS pmtperiodid
                                    ,StuEnrollId AS StuEnrollId
                                    ,PrgVerId
                                    ,SUM(ExcusedHours) AS ExcusedHours
                            FROM     @MasterStudentPopulationRunningTotal
                            WHERE    StuEnrollId = @StuEnrollId
                                     AND ActualHoursRunningTotal > @FloorValue
                                     AND (
                                         ( ActualHoursRunningTotal <= @CumulativeValue )
                                         OR ( ActualHoursRunningTotal
                                     BETWEEN @CumulativeValue AND @NextCumulativeValue
                                            )
                                         OR ( ActualHoursRunningTotal >= @CumulativeValue )
                                         )
                            GROUP BY StuEnrollId
                                    ,PrgVerId;



                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error creating excused hours data.'', 16, 1);
                        RETURN;
                    END;



                FETCH NEXT FROM db_cursor
                INTO @pmtperiodid
                    ,@CumulativeValue
                    ,@StuEnrollId
                    ,@NextCumulativeValue;

            END;

        CLOSE db_cursor;
        DEALLOCATE db_cursor;
        ----------------------------------------------------------------------------------- 

        --for testing 
        --SELECT * FROM @MasterStudentPopulation 
        --SELECT * FROM @MasterStudentPopulationRunningTotal 
        --SELECT * FROM @HoursScheduledActual	 
        --SELECT * FROM @HoursExcused 




        DECLARE @BatchNumber INT;
        SET @BatchNumber = (
                           SELECT ISNULL(MAX(PmtPeriodBatchHeaderId), 0) + 1
                           FROM   saPmtPeriodBatchHeaders
                           );

        INSERT INTO saPmtPeriodBatchHeaders (
                                            BatchName
                                            )
                    SELECT ''Batch '' + CAST(@BatchNumber AS VARCHAR(20)) + '' (Hours) - '' + LEFT(CONVERT(VARCHAR, GETDATE(), 101), 10) + '' ''
                           + LEFT(CONVERT(VARCHAR, GETDATE(), 108), 10);


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error creating batch header.'', 16, 1);
                RETURN;
            END;

        DECLARE @InsertedHeaderId INT;
        SET @InsertedHeaderId = SCOPE_IDENTITY();





        INSERT INTO saPmtPeriodBatchItems (
                                          PmtPeriodId
                                         ,PmtPeriodBatchHeaderId
                                         ,StuEnrollId
                                         ,ChargeAmount
                                         ,CreditsHoursValue
                                         ,TransactionReference
                                         ,TransactionDescription
                                         ,ModUser
                                         ,ModDate
                                         ,TermId
                                          )
                    SELECT     DISTINCT ( p.PmtPeriodId )
                                       ,@InsertedHeaderId
                                       ,e.StuEnrollId
                                       ,p.ChargeAmount
                                       ,( CASE WHEN i.IncrementType = 0 THEN CAE.ActualHours + ( HE.ExcusedHours * i.ExcAbscenesPercent )
                                               WHEN i.IncrementType = 1 THEN CAE.ScheduledHours
                                               ELSE 0
                                          END
                                        ) AS CreditsHoursValue
                                       ,''Payment Period '' + CAST(p.PeriodNumber AS VARCHAR(5)) AS TransactionReference
                                       ,tc.TransCodeDescrip + '': '' + CAST(p.CumulativeValue AS VARCHAR(20)) + '' '' + i.IncrementName AS TransactionDescription
                                       ,''AutoPost'' AS ModUser
                                       ,GETDATE() AS ModDate
                                       ,( CASE WHEN i.IncrementType = 0 THEN
                                               (
                                               SELECT TOP 1 TermId
                                               FROM   @MasterStudentPopulationRunningTotal
                                               -- INNER JOIN dbo.arPrgVersions pv ON pv.PrgVerId = [@MasterStudentPopulationRunningTotal].PrgVerId
                                               WHERE  StuEnrollId = e.StuEnrollId
                                                      -- AND ( pv.ProgramRegistrationType = 1 )
                                                      AND ActualHoursRunningTotal >= p.CumulativeValue
                                               )
                                               WHEN i.IncrementType = 1 THEN
                                               (
                                               SELECT TOP 1 TermId
                                               FROM   @MasterStudentPopulationRunningTotal
                                               --INNER JOIN dbo.arPrgVersions pv ON pv.PrgVerId = [@MasterStudentPopulationRunningTotal].PrgVerId
                                               WHERE  StuEnrollId = e.StuEnrollId
                                                      --AND ( pv.ProgramRegistrationType = 1 )
                                                      AND ScheduledHoursRunningTotal >= p.CumulativeValue
                                               )
                                               ELSE NULL
                                          END
                                        ) AS TermId
                    FROM       saPmtPeriods p
                    INNER JOIN dbo.saIncrements i ON p.IncrementId = i.IncrementId
                    INNER JOIN dbo.saBillingMethods B ON i.BillingMethodId = B.BillingMethodId
                    INNER JOIN dbo.arPrgVersions prg ON B.BillingMethodId = prg.BillingMethodId
                    INNER JOIN dbo.arStuEnrollments e ON prg.PrgVerId = e.PrgVerId
                    --INNER JOIN dbo.arStudent st ON e.StudentId = st.StudentId
                    INNER JOIN @HoursScheduledActual CAE ON e.StuEnrollId = CAE.StuEnrollId
                    LEFT JOIN  @HoursExcused HE ON p.PmtPeriodId = HE.PmtPeriodId
                                                   AND e.StuEnrollId = HE.StuEnrollId
                                                   AND e.PrgVerId = HE.PrgVerId
                    LEFT JOIN  dbo.saTransCodes tc ON tc.TransCodeId = p.TransactionCodeId
                    WHERE      B.BillingMethod = 3
                               AND e.StartDate >= i.EffectiveDate
                               AND ( CASE WHEN i.IncrementType = 0 THEN CAE.ActualHours + ( HE.ExcusedHours * i.ExcAbscenesPercent ) + CAE.MakeUpHours
                                          WHEN i.IncrementType = 1 THEN CAE.ScheduledHours
                                          ELSE 0
                                     END
                                   ) >= p.CumulativeValue
                               AND e.StuEnrollId NOT IN (
                                                        SELECT StuEnrollId
                                                        FROM   dbo.saTransactions
                                                        WHERE  StuEnrollId = e.StuEnrollId
                                                               AND PmtPeriodId = p.PmtPeriodId
                                                               OR p.PeriodNumber = PaymentPeriodNumber
                                                        )
                               AND e.StuEnrollId NOT IN (
                                                        SELECT     i.StuEnrollId
                                                        FROM       dbo.saPmtPeriodBatchItems i
                                                        INNER JOIN saPmtPeriodBatchHeaders h ON i.PmtPeriodBatchHeaderId = h.PmtPeriodBatchHeaderId
                                                        WHERE      StuEnrollId = e.StuEnrollId
                                                                   AND PmtPeriodId = p.PmtPeriodId
                                                                   AND h.IsPosted = 0
                                                        );


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error creating batch items.'', 16, 1);
                RETURN;
            END;



        ----------------------------------------------------------------- 
        -- UPDATE Batch Item count in header table 
        ----------------------------------------------------------------- 
        UPDATE saPmtPeriodBatchHeaders
        SET    BatchItemCount = (
                                SELECT     COUNT(*)
                                FROM       saPmtPeriodBatchHeaders h
                                INNER JOIN saPmtPeriodBatchItems i ON h.PmtPeriodBatchHeaderId = i.PmtPeriodBatchHeaderId
                                WHERE      h.PmtPeriodBatchHeaderId = @InsertedHeaderId
                                )
        WHERE  PmtPeriodBatchHeaderId = @InsertedHeaderId;

        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error updating batch count.'', 16, 1);
                RETURN;
            END;
        ----------------------------------------------------------------- 

        ----------------------------------------------------------------- 
        -- GET AutoPost config setting 
        ----------------------------------------------------------------- 
        DECLARE @IsAutoPost VARCHAR(50);
        SET @IsAutoPost = (
                          SELECT     TOP 1 v.Value
                          FROM       dbo.syConfigAppSettings c
                          INNER JOIN dbo.syConfigAppSetValues v ON c.SettingId = v.SettingId
                          WHERE      c.KeyName = ''PaymentPeriodAutoPost''
                          );
        ----------------------------------------------------------------- 



        ------------------------------------------------------------------- 
        ---- INSERT Transactions from Batch 
        ------------------------------------------------------------------- 
        IF @IsAutoPost = ''yes''
            BEGIN

                IF OBJECTPROPERTY(OBJECT_ID(''Trigger_Insert_Check_saTransactions''), ''IsTrigger'') = 1
                    BEGIN
                        ALTER TABLE dbo.saTransactions DISABLE TRIGGER Trigger_Insert_Check_saTransactions;
                    END;



                INSERT INTO @TransactionDates (
                                              PmtPeriodId
                                             ,StuEnrollId
                                             ,TransactionDate
                                              )
                            SELECT      ppCumulative.PmtPeriodId
                                       ,ppCumulative.StuEnrollId
                                       ,transDate.StudentAttendedDate
                            FROM        (
                                        SELECT     DISTINCT pp.PmtPeriodId
                                                           ,StuEnrollId
                                                           ,pp.CumulativeValue
                                        FROM       dbo.saPmtPeriodBatchItems ppbi
                                        INNER JOIN dbo.saPmtPeriods pp ON pp.PmtPeriodId = ppbi.PmtPeriodId
                                        WHERE      ppbi.PmtPeriodBatchHeaderId = @InsertedHeaderId
                                        ) ppCumulative
                            CROSS APPLY (
                                        SELECT   TOP 1 sas.StudentAttendedDate
                                        FROM     dbo.syStudentAttendanceSummary sas
                                        WHERE    sas.StuEnrollId = ppCumulative.StuEnrollId
                                                 AND (( sas.ActualRunningPresentDays + sas.ActualRunningMakeupDays ) >= ppCumulative.CumulativeValue )
                                        ORDER BY sas.StudentAttendedDate
                                        ) AS transDate;



                INSERT INTO dbo.saTransactions (
                                               StuEnrollId
                                              ,TermId
                                              ,CampusId
                                              ,TransDate
                                              ,TransCodeId
                                              ,TransReference
                                              ,AcademicYearId
                                              ,TransDescrip
                                              ,TransAmount
                                              ,TransTypeId
                                              ,IsPosted
                                              ,CreateDate
                                              ,BatchPaymentId
                                              ,ViewOrder
                                              ,IsAutomatic
                                              ,ModUser
                                              ,ModDate
                                              ,Voided
                                              ,FeeLevelId
                                              ,FeeId
                                              ,PaymentCodeId
                                              ,FundSourceId
                                              ,StuEnrollPayPeriodId
                                              ,DisplaySequence
                                              ,SecondDisplaySequence
                                              ,PmtPeriodId
                                              ,PaymentPeriodNumber
                                               )
                            SELECT     BI.StuEnrollId AS StuEnrollId
                                      ,BI.TermId AS TermId
                                      ,SE.CampusId
                                      ,COALESCE((
                                                SELECT TransactionDate
                                                FROM   @TransactionDates
                                                WHERE  PmtPeriodId = BI.PmtPeriodId
                                                       AND StuEnrollId = BI.StuEnrollId
                                                )
                                               ,GETDATE()
                                               ,SE.StartDate
                                               ,SE.ExpStartDate
                                               ) --transaction date
                                      ,PP.TransactionCodeId AS TranscodeId
                                      ,BI.TransactionReference
                                      ,NULL AS AcademicYearId
                                                 --??? HOW TO GET THIS?, 
                                      ,BI.TransactionDescription
                                      ,BI.ChargeAmount
                                      ,(
                                       SELECT TOP 1 TransTypeId
                                       FROM   dbo.saTransTypes
                                       WHERE  Description LIKE ''Charge%''
                                       ) AS TransTypeId
                                      ,1 AS IsPosted
                                      ,GETDATE() AS CreatedDate
                                      ,NULL AS BatchPaymentId
                                      ,NULL AS ViewOrder
                                      ,0 AS IsAutomatic
                                      ,''AutoPost'' AS ModUser
                                      ,GETDATE() AS ModDate
                                      ,0 AS Voided
                                      ,NULL AS FeeLevelId
                                      ,NULL AS FeeId
                                      ,NULL AS PaymentCodeId
                                      ,NULL AS FundSourceId
                                      ,NULL AS StuEnrollPayPeriodId
                                      ,NULL AS DisplaySequence
                                      ,NULL AS SecondDisplaySequence
                                      ,BI.PmtPeriodId AS PmtPeriodId
                                      ,PP.PeriodNumber
                            FROM       saPmtPeriodBatchItems BI
                            INNER JOIN saPmtPeriodBatchHeaders BH ON BI.PmtPeriodBatchHeaderId = BH.PmtPeriodBatchHeaderId
                            INNER JOIN dbo.arStuEnrollments SE ON BI.StuEnrollId = SE.StuEnrollId
                            INNER JOIN dbo.saPmtPeriods PP ON PP.PmtPeriodId = BI.PmtPeriodId
                            LEFT JOIN  dbo.saTransCodes tc ON tc.TransCodeId = PP.TransactionCodeId
                            WHERE      BH.PmtPeriodBatchHeaderId = @InsertedHeaderId
                                       AND BI.IncludedInPost = 1
                                       AND SE.StuEnrollId NOT IN (
                                                                 SELECT StuEnrollId
                                                                 FROM   dbo.saTransactions
                                                                 WHERE  StuEnrollId = SE.StuEnrollId
                                                                        AND PmtPeriodId = BI.PmtPeriodId
                                                                        OR PaymentPeriodNumber = PP.PeriodNumber
                                                                 )
                            ORDER BY   SE.StuEnrollId
                                      ,BI.TransactionReference;


                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error inserting transactions.'', 16, 1);
                        RETURN;
                    END;
                IF OBJECTPROPERTY(OBJECT_ID(''Trigger_Insert_Check_saTransactions''), ''IsTrigger'') = 1
                    BEGIN
                        ALTER TABLE dbo.saTransactions ENABLE TRIGGER Trigger_Insert_Check_saTransactions;
                    END;


                UPDATE     saPmtPeriodBatchItems
                SET        TransactionId = T.TransactionId
                FROM       dbo.saTransactions T
                INNER JOIN dbo.saPmtPeriodBatchItems BI ON T.PmtPeriodId = BI.PmtPeriodId
                                                           AND T.StuEnrollId = BI.StuEnrollId
                INNER JOIN dbo.saPmtPeriodBatchHeaders BH ON BI.PmtPeriodBatchHeaderId = BH.PmtPeriodBatchHeaderId
                WHERE      BH.PmtPeriodBatchHeaderId = @InsertedHeaderId;

                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error updating transaction ids in batch.'', 16, 1);
                        RETURN;
                    END;
                ----------------------------------------------------------------- 

                ----------------------------------------------------------------- 
                -- Set Batch to IsPosted 
                ----------------------------------------------------------------- 
                UPDATE dbo.saPmtPeriodBatchHeaders
                SET    IsPosted = 1
                      ,PostedDate = GETDATE()
                WHERE  PmtPeriodBatchHeaderId = @InsertedHeaderId;

                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error updating batch is posted flag.'', 16, 1);
                        RETURN;
                    END;
                ----------------------------------------------------------------- 


                ----------------------------------------------------------------- 
                -- Set IsCharged flag for payment periods from this batch 
                ----------------------------------------------------------------- 
                UPDATE     dbo.saPmtPeriods
                SET        IsCharged = 1
                FROM       saPmtPeriodBatchItems I
                INNER JOIN dbo.saPmtPeriods P ON I.PmtPeriodId = P.PmtPeriodId
                WHERE      I.PmtPeriodBatchHeaderId = @InsertedHeaderId;

                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error updating batch is posted flag.'', 16, 1);
                        RETURN;
                    END;
            END;


        ----------------------------------------------------------------- 
        -- Set IsBillingMethodCharged flag for Program Version from this batch 
        ----------------------------------------------------------------- 
        UPDATE     P
        SET        P.IsBillingMethodCharged = 1
        FROM       dbo.arPrgVersions P
        INNER JOIN dbo.arStuEnrollments E ON P.PrgVerId = E.PrgVerId
        INNER JOIN saPmtPeriodBatchItems I ON E.StuEnrollId = I.StuEnrollId
        INNER JOIN dbo.saPmtPeriodBatchHeaders H ON I.PmtPeriodBatchHeaderId = H.PmtPeriodBatchHeaderId
        WHERE      H.PmtPeriodBatchHeaderId = @InsertedHeaderId
                   AND I.TransactionId IS NOT NULL
                   AND I.IncludedInPost = 1;


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error updating program version ischarged flag.'', 16, 1);
                RETURN;
            END;
        -----------------------------------------------------------------	 


        COMMIT TRAN PAYMENTPERIODTRANSACTION;
    -----------------------------------------------------------------	 



    END;
--================================================================================================= 
-- END  --  USP_ProcessPaymentPeriodsHours 
--================================================================================================= 
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[GlTransView1]'
GO
IF OBJECT_ID(N'[dbo].[GlTransView1]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[GlTransView1]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[GlTransView2]'
GO
IF OBJECT_ID(N'[dbo].[GlTransView2]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[GlTransView2]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[saPmtPeriods]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_saPmtPeriods_saTransCodes_TransactionCodeId_TransCodeId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[saPmtPeriods]', 'U'))
ALTER TABLE [dbo].[saPmtPeriods] ADD CONSTRAINT [FK_saPmtPeriods_saTransCodes_TransactionCodeId_TransCodeId] FOREIGN KEY ([TransactionCodeId]) REFERENCES [dbo].[saTransCodes] ([TransCodeId])
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_saPmtPeriods_saIncrements_IncrementId_IncrementId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[saPmtPeriods]', 'U'))
ALTER TABLE [dbo].[saPmtPeriods] ADD CONSTRAINT [FK_saPmtPeriods_saIncrements_IncrementId_IncrementId] FOREIGN KEY ([IncrementId]) REFERENCES [dbo].[saIncrements] ([IncrementId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
