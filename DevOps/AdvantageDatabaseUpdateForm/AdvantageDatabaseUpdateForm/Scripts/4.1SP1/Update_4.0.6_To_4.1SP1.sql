﻿/*
Run this script on:

        dev2.internal.fameinc.com\a1.Prod_Aveda_400    -  This database will be modified

to synchronize it with a database with the schema represented by:

        Source

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.7.10021 from Red Gate Software Ltd at 6/6/2019 6:13:16 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
/*
* Use this Pre-Deployment script to perform tasks before the deployment of the project.
* Read more at https://www.red-gate.com/SOC7/pre-deployment-script-help
*/
UPDATE dbo.arClsSectMeetings
SET PeriodId = NULL
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)

DELETE FROM syPeriodsWorkDays
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[plEmployerJobs]'
GO
IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_plEmployerJobs_adTitles_JobTitleId_TitleId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[plEmployerJobs]', 'U'))
ALTER TABLE [dbo].[plEmployerJobs] DROP CONSTRAINT [FK_plEmployerJobs_adTitles_JobTitleId_TitleId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_plEmployerJobs_plEmployers_EmployerId_EmployerId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[plEmployerJobs]', 'U'))
ALTER TABLE [dbo].[plEmployerJobs] DROP CONSTRAINT [FK_plEmployerJobs_plEmployers_EmployerId_EmployerId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UIX_adReqs_Code_Descrip] from [dbo].[adReqs]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'UIX_adReqs_Code_Descrip' AND object_id = OBJECT_ID(N'[dbo].[adReqs]'))
DROP INDEX [UIX_adReqs_Code_Descrip] ON [dbo].[adReqs]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UIX_adSourceCatagory_SourceCatagoryCode_SourceCatagoryDescrip] from [dbo].[adSourceCatagory]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'UIX_adSourceCatagory_SourceCatagoryCode_SourceCatagoryDescrip' AND object_id = OBJECT_ID(N'[dbo].[adSourceCatagory]'))
DROP INDEX [UIX_adSourceCatagory_SourceCatagoryCode_SourceCatagoryDescrip] ON [dbo].[adSourceCatagory]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UIX_arPrgGrp_PrgGrpCode_PrgGrpDescrip] from [dbo].[arPrgGrp]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'UIX_arPrgGrp_PrgGrpCode_PrgGrpDescrip' AND object_id = OBJECT_ID(N'[dbo].[arPrgGrp]'))
DROP INDEX [UIX_arPrgGrp_PrgGrpCode_PrgGrpDescrip] ON [dbo].[arPrgGrp]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UIX_arPrograms_ProgCode_ProgDescrip] from [dbo].[arPrograms]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'UIX_arPrograms_ProgCode_ProgDescrip' AND object_id = OBJECT_ID(N'[dbo].[arPrograms]'))
DROP INDEX [UIX_arPrograms_ProgCode_ProgDescrip] ON [dbo].[arPrograms]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UIX_faLenders_Code_LenderDescrip_PrimaryContact] from [dbo].[faLenders]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'UIX_faLenders_Code_LenderDescrip_PrimaryContact' AND object_id = OBJECT_ID(N'[dbo].[faLenders]'))
DROP INDEX [UIX_faLenders_Code_LenderDescrip_PrimaryContact] ON [dbo].[faLenders]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UIX_plEmployerJobs_Code_EmployerJobTitle_JobTitleId_EmployerId] from [dbo].[plEmployerJobs]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'UIX_plEmployerJobs_Code_EmployerJobTitle_JobTitleId_EmployerId' AND object_id = OBJECT_ID(N'[dbo].[plEmployerJobs]'))
DROP INDEX [UIX_plEmployerJobs_Code_EmployerJobTitle_JobTitleId_EmployerId] ON [dbo].[plEmployerJobs]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UIX_plEmployers_Code_EmployerDescrip] from [dbo].[plEmployers]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'UIX_plEmployers_Code_EmployerDescrip' AND object_id = OBJECT_ID(N'[dbo].[plEmployers]'))
DROP INDEX [UIX_plEmployers_Code_EmployerDescrip] ON [dbo].[plEmployers]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[tr_HourRounding_Insert] from [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[tr_HourRounding_Insert]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[tr_HourRounding_Insert]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[tr_HourRounding_Update] from [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[tr_HourRounding_Update]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[tr_HourRounding_Update]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[TR_InsertAttendanceSummary_ByClockHour_Minutes] from [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[TR_InsertAttendanceSummary_ByClockHour_Minutes]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[TR_InsertAttendanceSummary_ByClockHour_Minutes]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[TR_InsertAttendanceSummary_ByDay_PA] from [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[TR_InsertAttendanceSummary_ByDay_PA]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[TR_InsertAttendanceSummary_ByDay_PA]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[syCampuses_Audit_Delete] from [dbo].[syCampuses]'
GO
IF OBJECT_ID(N'[dbo].[syCampuses_Audit_Delete]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[syCampuses_Audit_Delete]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[syCampuses_Audit_Insert] from [dbo].[syCampuses]'
GO
IF OBJECT_ID(N'[dbo].[syCampuses_Audit_Insert]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[syCampuses_Audit_Insert]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[syCampuses_Audit_Update] from [dbo].[syCampuses]'
GO
IF OBJECT_ID(N'[dbo].[syCampuses_Audit_Update]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[syCampuses_Audit_Update]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_Report_CreateReportTab]'
GO
IF OBJECT_ID(N'[dbo].[USP_Report_CreateReportTab]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_Report_CreateReportTab]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_Report_CreateReportRecord]'
GO
IF OBJECT_ID(N'[dbo].[USP_Report_CreateReportRecord]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_Report_CreateReportRecord]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_Report_CreateParamSet]'
GO
IF OBJECT_ID(N'[dbo].[USP_Report_CreateParamSet]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_Report_CreateParamSet]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_Report_CreateParamSection]'
GO
IF OBJECT_ID(N'[dbo].[USP_Report_CreateParamSection]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_Report_CreateParamSection]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_Report_CreateParamItem]'
GO
IF OBJECT_ID(N'[dbo].[USP_Report_CreateParamItem]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_Report_CreateParamItem]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_Report_CreateParamDetail]'
GO
IF OBJECT_ID(N'[dbo].[USP_Report_CreateParamDetail]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_Report_CreateParamDetail]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_NACCAS_Summary]'
GO
IF OBJECT_ID(N'[dbo].[USP_NACCAS_Summary]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_NACCAS_Summary]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_NACCAS_ExemptionList]'
GO
IF OBJECT_ID(N'[dbo].[USP_NACCAS_ExemptionList]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_NACCAS_ExemptionList]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_NACCAS_EnrollmentCount]'
GO
IF OBJECT_ID(N'[dbo].[USP_NACCAS_EnrollmentCount]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_NACCAS_EnrollmentCount]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_NACCAS_CohortGrid]'
GO
IF OBJECT_ID(N'[dbo].[USP_NACCAS_CohortGrid]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_NACCAS_CohortGrid]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_FA_PostZerosForStudents]'
GO
IF OBJECT_ID(N'[dbo].[USP_FA_PostZerosForStudents]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_FA_PostZerosForStudents]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[UDF_GetEnrollmentStatusIdAtGivenDate]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UDF_GetEnrollmentStatusIdAtGivenDate]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[UDF_GetEnrollmentStatusIdAtGivenDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[StudentScheduledHours]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StudentScheduledHours]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[StudentScheduledHours]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[GetStuEnrollment]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetStuEnrollment]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[GetStuEnrollment]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[GetGraduatedDate]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetGraduatedDate]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[GetGraduatedDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[DoesEnrollmentHavePendingBalance]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DoesEnrollmentHavePendingBalance]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[DoesEnrollmentHavePendingBalance]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[CompletedRequiredCourses]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CompletedRequiredCourses]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[CompletedRequiredCourses]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_AR_GetSystemTransCodesRefunds_GetList]'
GO
IF OBJECT_ID(N'[dbo].[USP_AR_GetSystemTransCodesRefunds_GetList]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_AR_GetSystemTransCodesRefunds_GetList]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[EnrollmentHasAttendance]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EnrollmentHasAttendance]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[EnrollmentHasAttendance]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_TitleIV_QualitativeAndQuantitative]'
GO
IF OBJECT_ID(N'[dbo].[USP_TitleIV_QualitativeAndQuantitative]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_TitleIV_QualitativeAndQuantitative]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_EnrollLead]'
GO
IF OBJECT_ID(N'[dbo].[USP_EnrollLead]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_EnrollLead]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_ConsecutiveDaysAbsentByClass_MainReport]'
GO
IF OBJECT_ID(N'[dbo].[USP_ConsecutiveDaysAbsentByClass_MainReport]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_ConsecutiveDaysAbsentByClass_MainReport]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_Invoice_GetStudentInfo]'
GO
IF OBJECT_ID(N'[dbo].[USP_Invoice_GetStudentInfo]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_Invoice_GetStudentInfo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_TR_Sub06_Courses]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub06_Courses]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_TR_Sub06_Courses]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_FASAPGenerateNotice]'
GO
IF OBJECT_ID(N'[dbo].[USP_FASAPGenerateNotice]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_FASAPGenerateNotice]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_TR_Sub03_StudentInfo]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub03_StudentInfo]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_TR_Sub03_StudentInfo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_TR_Sub03_PrepareGPA]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub03_PrepareGPA]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_TR_Sub03_PrepareGPA]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[usp_AR_GetCompletedComponents]'
GO
IF OBJECT_ID(N'[dbo].[usp_AR_GetCompletedComponents]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[usp_AR_GetCompletedComponents]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_ManageSecurity_Reports_Permissions]'
GO
IF OBJECT_ID(N'[dbo].[USP_ManageSecurity_Reports_Permissions]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_ManageSecurity_Reports_Permissions]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_TR_Sub3_StudentInfo]'
GO
IF OBJECT_ID(N'[dbo].[Usp_TR_Sub3_StudentInfo]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_TR_Sub3_StudentInfo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[CalculateHoursForStudentOnDate]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateHoursForStudentOnDate]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[CalculateHoursForStudentOnDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[arStuEnrollments]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[arStuEnrollments]', N'ThirdPartyContract') IS NULL
ALTER TABLE [dbo].[arStuEnrollments] ADD[ThirdPartyContract] [bit] NOT NULL CONSTRAINT [DF_arStuEnrollments_ThirdPartyContract] DEFAULT ((0))
IF COL_LENGTH(N'[dbo].[arStuEnrollments]', N'TransferHoursFromThisSchoolEnrollmentId') IS NULL
ALTER TABLE [dbo].[arStuEnrollments] ADD[TransferHoursFromThisSchoolEnrollmentId] [uniqueidentifier] NULL
IF COL_LENGTH(N'[dbo].[arStuEnrollments]', N'TotalTransferHoursFromThisSchool') IS NULL
ALTER TABLE [dbo].[arStuEnrollments] ADD[TotalTransferHoursFromThisSchool] [decimal] (18, 2) NOT NULL CONSTRAINT [DF_arStuEnrollments_TotalTransferHoursFromThisSchool] DEFAULT ((0))
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[plExitInterview]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[plExitInterview]', N'InelReasonId') IS NULL
ALTER TABLE [dbo].[plExitInterview] ADD[InelReasonId] [uniqueidentifier] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[syCampuses]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[syCampuses]', N'IsBranch') IS NULL
ALTER TABLE [dbo].[syCampuses] ADD[IsBranch] [bit] NOT NULL CONSTRAINT [DF_SyCampuses_IsBranch] DEFAULT ((0))
IF COL_LENGTH(N'[dbo].[syCampuses]', N'ParentCampusId') IS NULL
ALTER TABLE [dbo].[syCampuses] ADD[ParentCampusId] [uniqueidentifier] NULL
IF COL_LENGTH(N'[dbo].[syCampuses]', N'AllowGraduateAndStillOweMoney') IS NULL
ALTER TABLE [dbo].[syCampuses] ADD[AllowGraduateAndStillOweMoney] [bit] NOT NULL CONSTRAINT [DF_syCampuses_AllowGraduateAndStillOweMoney] DEFAULT ((0))
IF COL_LENGTH(N'[dbo].[syCampuses]', N'AllowGraduateWithoutFullCompletion') IS NULL
ALTER TABLE [dbo].[syCampuses] ADD[AllowGraduateWithoutFullCompletion] [bit] NOT NULL CONSTRAINT [DF_syCampuses_AllowGraduateWithoutFullCompletion] DEFAULT ((0))
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[syNACCASDropReasonsMapping]'
GO
IF OBJECT_ID(N'[dbo].[syNACCASDropReasonsMapping]', 'U') IS NULL
CREATE TABLE [dbo].[syNACCASDropReasonsMapping]
(
[NACCASDropReasonId] [uniqueidentifier] NULL,
[ADVDropReasonId] [uniqueidentifier] NULL,
[NaccasSettingId] [int] NOT NULL,
[DropReasonMappingId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_syNACCASDropReasonsMapping_DropReasonMappingId] DEFAULT (newsequentialid())
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_syNACCASDropReasonsMapping_DropReasonMappingId] on [dbo].[syNACCASDropReasonsMapping]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK_syNACCASDropReasonsMapping_DropReasonMappingId]', 'PK') AND parent_object_id = OBJECT_ID(N'[dbo].[syNACCASDropReasonsMapping]', 'U'))
ALTER TABLE [dbo].[syNACCASDropReasonsMapping] ADD CONSTRAINT [PK_syNACCASDropReasonsMapping_DropReasonMappingId] PRIMARY KEY CLUSTERED  ([DropReasonMappingId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudent]'
GO
IF OBJECT_ID(N'[dbo].[arStudent]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[arGrdBkResultsHistory]'
GO
IF OBJECT_ID(N'[dbo].[arGrdBkResultsHistory]', 'U') IS NULL
CREATE TABLE [dbo].[arGrdBkResultsHistory]
(
[GrdBkResultHistoryId] [uniqueidentifier] NOT NULL,
[ModUserHistory] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDateHistory] [datetime] NOT NULL,
[GrdBkResultId] [uniqueidentifier] NOT NULL,
[Score] [decimal] (6, 2) NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[ModDate] [datetime] NOT NULL,
[ResNum] [int] NOT NULL,
[PostDate] [smalldatetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__arGrdBkResultsHistory__GrdBkResultHistoryId] on [dbo].[arGrdBkResultsHistory]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK__arGrdBkResultsHistory__GrdBkResultHistoryId]', 'PK') AND parent_object_id = OBJECT_ID(N'[dbo].[arGrdBkResultsHistory]', 'U'))
ALTER TABLE [dbo].[arGrdBkResultsHistory] ADD CONSTRAINT [PK__arGrdBkResultsHistory__GrdBkResultHistoryId] PRIMARY KEY CLUSTERED  ([GrdBkResultHistoryId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[arGrdBkResults_Update] on [dbo].[arGrdBkResults]'
GO
IF OBJECT_ID(N'[dbo].[arGrdBkResults_Update]', 'TR') IS NULL
EXEC sp_executesql N' 
  
--==========================================================================================  
-- TRIGGER [dbo].[arGrdBkResults_Update] 
-- UPDATE  add GrdBkResultsHistory when update fields in arGrdBkResults 
--==========================================================================================  
CREATE TRIGGER [dbo].[arGrdBkResults_Update] ON [dbo].[arGrdBkResults]
    FOR UPDATE
AS
    SET NOCOUNT ON;  
 
    BEGIN  
        SET NOCOUNT ON;  
        DECLARE @EventDate AS DATETIME;  
        DECLARE @UserName AS VARCHAR(50);  

       
        SET @EventDate = (
                           SELECT TOP 1
                                    ModDate
                           FROM     Inserted
                         );  
        SET @UserName = (
                          SELECT TOP 1
                                    ModUser
                          FROM      Inserted
                        );  

                IF UPDATE(Score)
                    BEGIN  
                        INSERT  INTO arGrdBkResultsHistory
                                
                                SELECT  NEWID() 
								       ,@UserName AS ModUserHistory
                                       ,@EventDate AS ModDateHistory
									   ,old.GrdBkResultId
									   ,old.Score
									   ,old.ModUser
									   ,old.ModDate
									   ,old.ResNum
									   ,old.PostDate
                                FROM    Deleted AS old
                                 
                    END; 


        SET NOCOUNT OFF;  
    END;  
--==========================================================================================  
-- END TRIGGER arGrdBkResults_Update 
--==========================================================================================  
  
 
    SET NOCOUNT OFF;  
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CalculateHoursForStudentOnDate]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateHoursForStudentOnDate]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[CalculateHoursForStudentOnDate]
    (
        @StuEnrollId UNIQUEIDENTIFIER
       ,@Date DATETIME
       ,@AttemptedInsertedHours DECIMAL(16, 2)
    )
RETURNS DECIMAL(16, 2)
AS
    BEGIN
        DECLARE @CampusId UNIQUEIDENTIFIER = (
                                             SELECT TOP 1 CampusId
                                             FROM   dbo.arStuEnrollments
                                             WHERE  StuEnrollId = @StuEnrollId
                                             );

        DECLARE @IsTimeClock BIT = (
                                   SELECT TOP 1 dbo.arPrgVersions.UseTimeClock
                                   FROM   dbo.arStuEnrollments
                                   JOIN   dbo.arPrgVersions ON arPrgVersions.PrgVerId = arStuEnrollments.PrgVerId
                                   WHERE  StuEnrollId = @StuEnrollId
                                   );

        DECLARE @TrackSapAttendance VARCHAR(50) = LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', @CampusId))));

        --no calculation needed, inserting these numbers as place holder or no attendance
        IF (
           @AttemptedInsertedHours = 0
           OR @AttemptedInsertedHours = 9999.0
           OR @AttemptedInsertedHours = 999.0
           OR @AttemptedInsertedHours = 99.0
           )
            BEGIN
                RETURN @AttemptedInsertedHours;
            END;

        --return attempted inserted hours if not by day or not time clock 
        IF (
           @TrackSapAttendance != ''byday''
           OR @IsTimeClock = 0
           )
            RETURN @AttemptedInsertedHours;

        --if by day and time clock, calculate hours based on punches, round based on campus settings
        DECLARE @MinimumHoursToBeConsideredPresent DECIMAL(16, 2) = (
                                                                    SELECT MinimumHoursToBePresent
                                                                    FROM   dbo.arStuEnrollments
                                                                    JOIN   dbo.arStudentSchedules ON arStudentSchedules.StuEnrollId = arStuEnrollments.StuEnrollId
                                                                    JOIN   dbo.arProgSchedules ON arProgSchedules.ScheduleId = arStudentSchedules.ScheduleId
                                                                    JOIN   dbo.arProgScheduleDetails ON arProgScheduleDetails.ScheduleId = arProgSchedules.ScheduleId
                                                                                                        AND dw = ( DATEPART(dw, @Date) - 1 )
                                                                                                        AND arStuEnrollments.StuEnrollId = @StuEnrollId
                                                                    );

        DECLARE @RoundingMethod VARCHAR(50) = dbo.GetAppSettingValueByKeyName(''HoursRoundingMethod'', @CampusId);

        IF @TrackSapAttendance = ''byday''
           AND @IsTimeClock = 1
            BEGIN
                DECLARE @CalculatedHoursForDayRounded DECIMAL(16, 2) = (
                                                                       SELECT   SUM(HoursForSingleInAndOut) AS TotalHoursForSingleDay
                                                                       FROM     (
                                                                                SELECT   dta.StuEnrollId
                                                                                        ,dta.PunchDate
                                                                                        ,CASE WHEN ( dta.rn % 2 ) = 0 THEN dta.rn - 1
                                                                                              ELSE dta.rn
                                                                                         END AS RelatedPunches
                                                                                        ,dbo.CalculateRoundingOnMinutes(
                                                                                                                           DATEDIFF(
                                                                                                                                       SECOND
                                                                                                                                      ,MIN(dta.PunchTime)
                                                                                                                                      ,MAX(dta.PunchTime)
                                                                                                                                   ) / 60.0
                                                                                                                          ,@RoundingMethod
                                                                                                                       ) AS HoursForSingleInAndOut
                                                                                FROM     (
                                                                                         SELECT BadgeId
                                                                                               ,StuEnrollId
                                                                                               ,PunchTime
                                                                                               ,CAST(PunchTime AS DATE) AS PunchDate
                                                                                               ,ROW_NUMBER() OVER ( ORDER BY StuEnrollId
                                                                                                                            ,PunchTime
                                                                                                                  ) rn
                                                                                         FROM   dbo.arStudentTimeClockPunches
                                                                                         WHERE  dbo.arStudentTimeClockPunches.Status = 1
                                                                                                AND StuEnrollId = @StuEnrollId
                                                                                                AND CAST(PunchTime AS DATE) = @Date
                                                                                         ) AS dta
                                                                                GROUP BY dta.StuEnrollId
                                                                                        ,dta.PunchDate
                                                                                        ,CASE WHEN ( dta.rn % 2 ) = 0 THEN dta.rn - 1
                                                                                              ELSE dta.rn
                                                                                         END
                                                                                HAVING   COUNT(dta.StuEnrollId) % 2 = 0
                                                                                ) AS Dta
                                                                       GROUP BY Dta.StuEnrollId
                                                                               ,Dta.PunchDate
                                                                       ) / 60.0;

                RETURN CASE WHEN @CalculatedHoursForDayRounded IS NULL THEN @AttemptedInsertedHours
                            WHEN ( @CalculatedHoursForDayRounded >= ISNULL(@MinimumHoursToBeConsideredPresent, 0)) THEN @CalculatedHoursForDayRounded
                            ELSE 0 --if student did not meet minimum hours to be considered present, return 0
                       END;
            END;

        RETURN @AttemptedInsertedHours;
    END;






'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[tr_arStudentClockAttendance_Insert] on [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[tr_arStudentClockAttendance_Insert]', 'TR') IS NULL
EXEC sp_executesql N'

-- =============================================
-- Author:		FAME Inc.
-- Create date: 5/17/2019
-- Description:	When inserting record into arStudentClockAttendance, if time clock punches exist and hours not 0 or 999.0, calculate rounding hours
-- =============================================
CREATE   TRIGGER [dbo].[tr_arStudentClockAttendance_Insert]
ON [dbo].[arStudentClockAttendance]
INSTEAD OF INSERT
AS
    BEGIN
        INSERT INTO dbo.arStudentClockAttendance
                    SELECT Inserted.StuEnrollId
                          ,Inserted.ScheduleId
                          ,Inserted.RecordDate
                          ,Inserted.SchedHours
                          ,CASE WHEN (
                                     inserted.ActualHours = 0
                                     OR inserted.ActualHours = 9999.0
                                     OR inserted.ActualHours = 999.0
                                     OR inserted.ActualHours = 99.0
                                     ) THEN Inserted.ActualHours 
                                ELSE dbo.CalculateHoursForStudentOnDate(inserted.StuEnrollId, inserted.RecordDate, inserted.ActualHours)
                           END AS ActualHours
                          ,Inserted.ModDate
                          ,Inserted.ModUser
                          ,Inserted.isTardy
                          ,Inserted.PostByException
                          ,Inserted.comments
                          ,Inserted.TardyProcessed
                          ,Inserted.Converted
                    FROM   inserted;


    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[tr_arStudentClockAttendance_Update] on [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[tr_arStudentClockAttendance_Update]', 'TR') IS NULL
EXEC sp_executesql N'
-- =============================================
-- Author:		FAME Inc.
-- Create date: 5/17/2019
-- Description:	When updating record into arStudentClockAttendance, if time clock punches is updated hours not 0 or 999.0, calculate rounding hours
-- =============================================
CREATE     TRIGGER [dbo].[tr_arStudentClockAttendance_Update]
ON [dbo].[arStudentClockAttendance]
INSTEAD OF UPDATE
AS
    BEGIN
					UPDATE a
					SET a.StuEnrollId = Inserted.StuEnrollId,
					a.ScheduleId = Inserted.ScheduleId,
					a.RecordDate = Inserted.RecordDate,
					a.SchedHours = inserted.SchedHours,
					a.ActualHours =  (CASE WHEN (
                                     inserted.ActualHours = 0
                                     OR inserted.ActualHours = 9999.0
                                     OR inserted.ActualHours = 999.0
                                     OR inserted.ActualHours = 99.0
                                     ) THEN Inserted.ActualHours
                                ELSE dbo.CalculateHoursForStudentOnDate(inserted.StuEnrollId, inserted.RecordDate, inserted.ActualHours)
                           END),
					a.ModDate = Inserted.ModDate,
					a.ModUser = Inserted.ModUser,
					a.isTardy = Inserted.isTardy,
					a.PostByException = Inserted.PostByException,
					a.comments = Inserted.comments,
					a.TardyProcessed = Inserted.TardyProcessed,
					a.Converted =Inserted.Converted
					FROM Inserted JOIN dbo.arStudentClockAttendance a ON a.StuEnrollId = Inserted.StuEnrollId AND a.RecordDate = Inserted.RecordDate;
    END;

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[TR_InsertAttendanceSummary_ByClockHour_Minutes] on [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[TR_InsertAttendanceSummary_ByClockHour_Minutes]', 'TR') IS NULL
EXEC sp_executesql N'


CREATE TRIGGER [dbo].[TR_InsertAttendanceSummary_ByClockHour_Minutes]
ON [dbo].[arStudentClockAttendance]
AFTER INSERT, UPDATE
AS
SET NOCOUNT ON;

DECLARE @StuEnrollId UNIQUEIDENTIFIER
       ,@MeetDate DATETIME
       ,@WeekDay VARCHAR(15)
       ,@StartDate DATETIME
       ,@EndDate DATETIME;
DECLARE @PeriodDescrip VARCHAR(50)
       ,@Actual DECIMAL(18, 2)
       ,@Excused DECIMAL(18, 2)
       ,@ClsSectionId UNIQUEIDENTIFIER;
DECLARE @Absent DECIMAL(18, 2)
       ,@ScheduledMinutes DECIMAL(18, 2)
       ,@TardyMinutes DECIMAL(18, 2);
DECLARE @tardy DECIMAL(18, 2)
       ,@tracktardies INT
       ,@tardiesMakingAbsence INT
       ,@PrgVerId UNIQUEIDENTIFIER
       ,@rownumber INT
       ,@IsTardy BIT
       ,@ActualRunningScheduledHours DECIMAL(18, 2);
DECLARE @ActualRunningPresentHours DECIMAL(18, 2)
       ,@ActualRunningAbsentHours DECIMAL(18, 2)
       ,@ActualRunningTardyHours DECIMAL(18, 2)
       ,@ActualRunningMakeupHours DECIMAL(18, 2);
DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
       ,@intTardyBreakPoint INT
       ,@AdjustedRunningPresentHours DECIMAL(18, 2)
       ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
       ,@ActualRunningScheduledDays DECIMAL(18, 2);
DECLARE GetAttendance_Cursor CURSOR FOR
    SELECT     t1.StuEnrollId
              ,NULL AS ClsSectionId
              ,t1.RecordDate AS MeetDate
              ,t1.ActualHours
              ,t1.SchedHours AS ScheduledMinutes
              ,CASE WHEN (
                         (
                         t1.SchedHours >= 1
                         AND t1.SchedHours NOT IN ( 999, 9999 )
                         )
                         AND t1.ActualHours = 0
                         ) THEN t1.SchedHours
                    ELSE 0
               END AS Absent
              ,t1.isTardy
              ,(
               SELECT SUM(SchedHours)
               FROM   arStudentClockAttendance
               WHERE  StuEnrollId = t1.StuEnrollId
                      AND RecordDate <= t1.RecordDate
                      AND (
                          t1.SchedHours >= 1
                          AND t1.SchedHours NOT IN ( 999, 9999 )
                          AND t1.ActualHours NOT IN ( 999, 9999 )
                          )
               ) AS ActualRunningScheduledHours
              ,(
               SELECT SUM(ActualHours)
               FROM   arStudentClockAttendance
               WHERE  StuEnrollId = t1.StuEnrollId
                      AND RecordDate <= t1.RecordDate
                      AND (
                          t1.SchedHours >= 1
                          AND t1.SchedHours NOT IN ( 999, 9999 )
                          )
                      AND ActualHours >= 1
                      AND ActualHours NOT IN ( 999, 9999 )
               ) AS ActualRunningPresentHours
              ,(
               SELECT COUNT(ActualHours)
               FROM   arStudentClockAttendance
               WHERE  StuEnrollId = t1.StuEnrollId
                      AND RecordDate <= t1.RecordDate
                      AND (
                          t1.SchedHours >= 1
                          AND t1.SchedHours NOT IN ( 999, 9999 )
                          )
                      AND ActualHours = 0
                      AND ActualHours NOT IN ( 999, 9999 )
               ) AS ActualRunningAbsentHours
              ,(
               SELECT SUM(ActualHours)
               FROM   arStudentClockAttendance
               WHERE  StuEnrollId = t1.StuEnrollId
                      AND RecordDate <= t1.RecordDate
                      AND SchedHours = 0
                      AND ActualHours >= 1
                      AND ActualHours NOT IN ( 999, 9999 )
               ) AS ActualRunningMakeupHours
              ,(
               SELECT SUM(SchedHours - ActualHours)
               FROM   arStudentClockAttendance
               WHERE  StuEnrollId = t1.StuEnrollId
                      AND RecordDate <= t1.RecordDate
                      AND (
                          (
                          t1.SchedHours >= 1
                          AND t1.SchedHours NOT IN ( 999, 9999 )
                          )
                          AND ActualHours >= 1
                          AND ActualHours NOT IN ( 999, 9999 )
                          )
                      AND isTardy = 1
               ) AS ActualRunningTardyHours
              ,t3.TrackTardies
              ,t3.TardiesMakingAbsence
              ,t3.PrgVerId
              ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
    FROM       arStudentClockAttendance t1
    INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
    INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
    INNER JOIN arAttUnitType AS t5 ON t5.UnitTypeId = t3.UnitTypeId
    --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
    WHERE      t1.StuEnrollId IN (
                                 SELECT StuEnrollId
                                 FROM   inserted
                                 )
               AND t5.UnitTypeDescrip IN ( ''Clock Hours'', ''Minutes'' );

DELETE FROM syStudentAttendanceSummary
WHERE StuEnrollId IN (
                    SELECT INSERTED.StuEnrollId
                    FROM   INSERTED
                    );
OPEN GetAttendance_Cursor;
DECLARE @ActualHours DECIMAL(18, 2)
       ,@SchedHours DECIMAL(18, 2);
FETCH NEXT FROM GetAttendance_Cursor
INTO @StuEnrollId
    ,@ClsSectionId
    ,@MeetDate
    ,@Actual
    ,@ScheduledMinutes
    ,@Absent
    ,@IsTardy
    ,@ActualRunningScheduledHours
    ,@ActualRunningPresentHours
    ,@ActualRunningAbsentHours
    ,@ActualRunningMakeupHours
    ,@ActualRunningTardyHours
    ,@tracktardies
    ,@tardiesMakingAbsence
    ,@PrgVerId
    ,@rownumber;

SET @ActualRunningPresentHours = 0;
SET @ActualRunningPresentHours = 0;
SET @ActualRunningAbsentHours = 0;
SET @ActualRunningTardyHours = 0;
SET @ActualRunningMakeupHours = 0;
SET @intTardyBreakPoint = 0;
SET @AdjustedRunningPresentHours = 0;
SET @AdjustedRunningAbsentHours = 0;
SET @ActualRunningScheduledDays = 0;
WHILE @@FETCH_STATUS = 0
    BEGIN

        IF @PrevStuEnrollId <> @StuEnrollId
            BEGIN
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @ActualRunningTardyHours = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
            END;

        -- Calculate Actual Running Scheduled Days
        IF (
           @ScheduledMinutes >= 1
           AND (
               @Actual <> 9999
               AND @Actual <> 999
               )
           AND (
               @ScheduledMinutes <> 9999
               AND @Actual <> 999
               )
           )
            BEGIN
                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + ISNULL(@ScheduledMinutes, 0);
            END;
        ELSE
            BEGIN
                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0);
            END;

        -- Calculate Actual Running Present Hours
        IF (
           @Actual <> 9999
           AND @Actual <> 999
           )
            BEGIN
                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
            END;

        -- Calculate: Actual Running Absent Hours
        --1. sched = 5, Actual = 2 then Ab = (5-3) = 2
        IF (
           @Actual > 0
           AND @Actual < @ScheduledMinutes
           )
            BEGIN
                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + ( @ScheduledMinutes - @Actual );
            END;
        ELSE
            BEGIN
                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
            END;

        -- Calculate: Actual Running Make up hours
        --sched=5, Actual =7, makeup= 2,ab = 0
        --sched=0, Actual =7, makeup= 7,ab = 0
        IF (
           @Actual > 0
           AND @ScheduledMinutes > 0
           AND @Actual > @ScheduledMinutes
           AND (
               @Actual <> 9999
               AND @Actual <> 999
               )
           )
            BEGIN
                SET @ActualRunningMakeupHours = @ActualRunningMakeupHours + ( @Actual - @ScheduledMinutes );
            END;

        -- Calculate: Adjusted Running Present Hours
        IF (
           @Actual <> 9999
           AND @Actual <> 999
           )
            BEGIN
                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
            END;

        -- Calculate: Adjusted Running Absent Hours
        -- Absent hours
        --1. sched = 5, Actual = 2 then Ab = (5-3) = 2
        IF (
           @Actual = 0
           AND @ScheduledMinutes >= 1
           AND @ScheduledMinutes NOT IN ( 999, 9999 )
           )
            BEGIN
                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
            END;
        ELSE IF (
                @Absent = 0
                AND @ActualRunningAbsentHours > 0
                AND ( @Actual < @ScheduledMinutes )
                )
                 BEGIN
                     SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                 END;

        -- Calculate : Actual Running Tardy Hours
        IF (
           @Actual > 0
           AND @Actual < @ScheduledMinutes
           AND (
               @Actual <> 9999.00
               AND @Actual <> 999.00
               )
           AND @IsTardy = 1
           )
            BEGIN
                SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
            END;

        -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
        -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
        -- when student is tardy the second time, that second occurance will be considered as
        -- absence
        -- Variable @intTardyBreakpoint tracks how many times the student was tardy
        -- Variable @tardiesMakingAbsence tracks the tardy rule
        IF @tracktardies = 1
           AND (
               @TardyMinutes > 0
               OR @IsTardy = 1
               )
            BEGIN
                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
            END;

        IF (
           @tracktardies = 1
           AND @intTardyBreakPoint = @tardiesMakingAbsence
           )
            BEGIN
                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --old code : @AdjustedRunningAbsentHours + @Actual 
                SET @intTardyBreakPoint = 0;
            END;



        INSERT INTO syStudentAttendanceSummary (
                                               StuEnrollId
                                              ,ClsSectionId
                                              ,StudentAttendedDate
                                              ,ScheduledDays
                                              ,ActualDays
                                              ,ActualRunningScheduledDays
                                              ,ActualRunningPresentDays
                                              ,ActualRunningAbsentDays
                                              ,ActualRunningMakeupDays
                                              ,ActualRunningTardyDays
                                              ,AdjustedPresentDays
                                              ,AdjustedAbsentDays
                                              ,AttendanceTrackType
                                              ,ModUser
                                              ,ModDate
                                               )
        VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                ,@ActualRunningAbsentHours, @ActualRunningMakeupHours, @ActualRunningTardyHours, @AdjustedRunningPresentHours, @AdjustedRunningAbsentHours
                ,''Post Attendance by Class'', ''sa'', GETDATE());
        --end
        SET @PrevStuEnrollId = @StuEnrollId;

        FETCH NEXT FROM GetAttendance_Cursor
        INTO @StuEnrollId
            ,@ClsSectionId
            ,@MeetDate
            ,@Actual
            ,@ScheduledMinutes
            ,@Absent
            ,@IsTardy
            ,@ActualRunningScheduledHours
            ,@ActualRunningPresentHours
            ,@ActualRunningAbsentHours
            ,@ActualRunningMakeupHours
            ,@ActualRunningTardyHours
            ,@tracktardies
            ,@tardiesMakingAbsence
            ,@PrgVerId
            ,@rownumber;
    END;
CLOSE GetAttendance_Cursor;
DEALLOCATE GetAttendance_Cursor;



SET NOCOUNT OFF;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[TR_InsertAttendanceSummary_ByDay_PA] on [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[TR_InsertAttendanceSummary_ByDay_PA]', 'TR') IS NULL
EXEC sp_executesql N'

CREATE TRIGGER [dbo].[TR_InsertAttendanceSummary_ByDay_PA]
ON [dbo].[arStudentClockAttendance]
AFTER INSERT, UPDATE
AS
SET NOCOUNT ON;

DECLARE @StuEnrollId UNIQUEIDENTIFIER
       ,@MeetDate DATETIME
       ,@WeekDay VARCHAR(15)
       ,@StartDate DATETIME
       ,@EndDate DATETIME;
DECLARE @PeriodDescrip VARCHAR(50)
       ,@Actual DECIMAL(18, 2)
       ,@Excused DECIMAL(18, 2)
       ,@ClsSectionId UNIQUEIDENTIFIER;
DECLARE @Absent DECIMAL(18, 2)
       ,@ScheduledMinutes DECIMAL(18, 2)
       ,@TardyMinutes DECIMAL(18, 2)
       ,@MakeupHours DECIMAL(18, 2);
DECLARE @tardy DECIMAL(18, 2)
       ,@tracktardies INT
       ,@tardiesMakingAbsence INT
       ,@PrgVerId UNIQUEIDENTIFIER
       ,@rownumber INT
       ,@IsTardy BIT
       ,@ActualRunningScheduledHours DECIMAL(18, 2);
DECLARE @ActualRunningPresentHours DECIMAL(18, 2)
       ,@ActualRunningAbsentHours DECIMAL(18, 2)
       ,@ActualRunningTardyHours DECIMAL(18, 2)
       ,@ActualRunningMakeupHours DECIMAL(18, 2);
DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
       ,@intTardyBreakPoint INT
       ,@AdjustedRunningPresentHours DECIMAL(18, 2)
       ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
       ,@ActualRunningScheduledDays DECIMAL(18, 2);
/***************************************************************************************
		Calculation for Attendance by Day and Present Absent : 
		
			Total Days Attended = Adjusted Present Days + Makeup days
			Absent Days = Days Absent + Tardy Days

*******************************************************************************************/
DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
    SELECT     t1.StuEnrollId
              ,t1.RecordDate
              ,t1.ActualHours
              ,t1.SchedHours
              ,CASE WHEN (
                         (
                         t1.SchedHours >= 1
                         AND t1.SchedHours NOT IN ( 999, 9999 )
                         )
                         AND t1.ActualHours = 0
                         ) THEN t1.SchedHours
                    ELSE 0
               END AS Absent
              ,t1.isTardy
              ,t3.TrackTardies
              ,t3.TardiesMakingAbsence
    FROM       arStudentClockAttendance t1
    INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
    INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
    INNER JOIN Inserted t4 ON t1.StuEnrollId = t4.StuEnrollId
    INNER JOIN arAttUnitType AS t5 ON t5.UnitTypeId = t3.UnitTypeId
    WHERE -- both Present Absent and Clock Hour Unit Type
               t5.UnitTypeDescrip IN ( ''Present Absent'', ''Clock Hours'' )
               AND t1.ActualHours <> 9999.00
    ORDER BY   t1.StuEnrollId
              ,t1.RecordDate;

DELETE FROM dbo.syStudentAttendanceSummary
WHERE StuEnrollId IN (
                    SELECT INSERTED.StuEnrollId
                    FROM   INSERTED
                    );
OPEN GetAttendance_Cursor;
DECLARE @ActualHours DECIMAL(18, 2)
       ,@SchedHours DECIMAL(18, 2);
FETCH NEXT FROM GetAttendance_Cursor
INTO @StuEnrollId
    ,@MeetDate
    ,@Actual
    ,@ScheduledMinutes
    ,@Absent
    ,@IsTardy
    ,@tracktardies
    ,@tardiesMakingAbsence;

SET @ActualRunningPresentHours = 0;
SET @ActualRunningPresentHours = 0;
SET @ActualRunningAbsentHours = 0;
SET @ActualRunningTardyHours = 0;
SET @ActualRunningMakeupHours = 0;
SET @intTardyBreakPoint = 0;
SET @AdjustedRunningPresentHours = 0;
SET @AdjustedRunningAbsentHours = 0;
SET @ActualRunningScheduledDays = 0;
SET @MakeupHours = 0;
WHILE @@FETCH_STATUS = 0
    BEGIN

        IF @PrevStuEnrollId <> @StuEnrollId
            BEGIN
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @ActualRunningTardyHours = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                SET @MakeupHours = 0;
            END;

        IF (
           @Actual <> 9999
           AND @Actual <> 999
           )
            BEGIN
                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + @ScheduledMinutes;
                SET @ActualRunningPresentHours = ISNULL(@ActualRunningPresentHours, 0) + @Actual;
                SET @AdjustedRunningPresentHours = ISNULL(@AdjustedRunningPresentHours, 0) + @Actual;
            END;
        SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + @Absent;
        IF (
           @Actual = 0
           AND @ScheduledMinutes >= 1
           AND @ScheduledMinutes NOT IN ( 999, 9999 )
           )
            BEGIN
                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
            END;

        -- The following condition applies only NWH 
        -- If Scheduled = 3.0 hrs and Actual = 1.0 Then 2 hrs is considered absent
        IF (
           @ScheduledMinutes >= 1
           AND @ScheduledMinutes NOT IN ( 999, 9999 )
           AND @Actual > 0
           AND ( @Actual < @ScheduledMinutes )
           )
            BEGIN
                SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                SET @AdjustedRunningAbsentHours = ISNULL(@AdjustedRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
            END;

        IF (
           @tracktardies = 1
           AND @IsTardy = 1
           )
            BEGIN
                SET @ActualRunningTardyHours = @ActualRunningTardyHours + 1;
            END;

        -- If there are make up hrs deduct that otherwise it will be added again in progress report
        IF (
           @Actual > 0
           AND @Actual > @ScheduledMinutes
           AND @Actual <> 9999.00
           )
            BEGIN
                SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
            END;
        IF @tracktardies = 1
           AND (
               @TardyMinutes > 0
               OR @IsTardy = 1
               )
            BEGIN
                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
            END;

        -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
        IF (
           @Actual > 0
           AND @Actual > @ScheduledMinutes
           AND @Actual <> 9999.00
           )
            BEGIN
                SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
            END;

        IF (
           @tracktardies = 1
           AND @intTardyBreakPoint = @tardiesMakingAbsence
           )
            BEGIN
                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                SET @intTardyBreakPoint = 0;
            END;


        INSERT INTO syStudentAttendanceSummary (
                                               StuEnrollId
                                              ,ClsSectionId
                                              ,StudentAttendedDate
                                              ,ScheduledDays
                                              ,ActualDays
                                              ,ActualRunningScheduledDays
                                              ,ActualRunningPresentDays
                                              ,ActualRunningAbsentDays
                                              ,ActualRunningMakeupDays
                                              ,ActualRunningTardyDays
                                              ,AdjustedPresentDays
                                              ,AdjustedAbsentDays
                                              ,AttendanceTrackType
                                              ,ModUser
                                              ,ModDate
                                              ,tardiesmakingabsence
                                               )
        VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, ISNULL(@ScheduledMinutes, 0), @Actual, ISNULL(@ActualRunningScheduledDays, 0)
                ,ISNULL(@ActualRunningPresentHours, 0), ISNULL(@ActualRunningAbsentHours, 0), ISNULL(@MakeupHours, 0), ISNULL(@ActualRunningTardyHours, 0)
                ,ISNULL(@AdjustedRunningPresentHours, 0), ISNULL(@AdjustedRunningAbsentHours, 0), ''Post Attendance by Class'', ''sa'', GETDATE()
                ,@tardiesMakingAbsence );

        --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
        --end
        SET @PrevStuEnrollId = @StuEnrollId;
        FETCH NEXT FROM GetAttendance_Cursor
        INTO @StuEnrollId
            ,@MeetDate
            ,@Actual
            ,@ScheduledMinutes
            ,@Absent
            ,@IsTardy
            ,@tracktardies
            ,@tardiesMakingAbsence;
    END;
CLOSE GetAttendance_Cursor;
DEALLOCATE GetAttendance_Cursor;



SET NOCOUNT OFF;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[syCampuses_Audit_Delete] on [dbo].[syCampuses]'
GO
IF OBJECT_ID(N'[dbo].[syCampuses_Audit_Delete]', 'TR') IS NULL
EXEC sp_executesql N'

CREATE TRIGGER [dbo].[syCampuses_Audit_Delete]
ON [dbo].[syCampuses]
FOR DELETE
AS
SET NOCOUNT ON;

DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
DECLARE @EventRows AS INT;
DECLARE @EventDate AS DATETIME;
DECLARE @UserName AS VARCHAR(50);
SET @AuditHistId = NEWID();
SET @EventRows = (
                 SELECT COUNT(*)
                 FROM   Deleted
                 );
SET @EventDate = (
                 SELECT TOP 1 ModDate
                 FROM   Deleted
                 );
SET @UserName = (
                SELECT TOP 1 ModUser
                FROM   Deleted
                );
IF @EventRows > 0
    BEGIN
        EXEC fmAuditHistAdd @AuditHistId
                           ,''syCampuses''
                           ,''D''
                           ,@EventRows
                           ,@EventDate
                           ,@UserName;
        BEGIN
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''CampCode''
                              ,CONVERT(VARCHAR(8000), Old.CampCode, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''CampDescrip''
                              ,CONVERT(VARCHAR(8000), Old.CampDescrip, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''Address1''
                              ,CONVERT(VARCHAR(8000), Old.Address1, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''Address2''
                              ,CONVERT(VARCHAR(8000), Old.Address2, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''City''
                              ,CONVERT(VARCHAR(8000), Old.City, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''StateId''
                              ,CONVERT(VARCHAR(8000), Old.StateId, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''Zip''
                              ,CONVERT(VARCHAR(8000), Old.Zip, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''CountryId''
                              ,CONVERT(VARCHAR(8000), Old.CountryId, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''StatusId''
                              ,CONVERT(VARCHAR(8000), Old.StatusId, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''Phone1''
                              ,CONVERT(VARCHAR(8000), Old.Phone1, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''Phone2''
                              ,CONVERT(VARCHAR(8000), Old.Phone2, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''Phone3''
                              ,CONVERT(VARCHAR(8000), Old.Phone3, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''Fax''
                              ,CONVERT(VARCHAR(8000), Old.Fax, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''Email''
                              ,CONVERT(VARCHAR(8000), Old.Email, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''Website''
                              ,CONVERT(VARCHAR(8000), Old.Website, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''IsCorporate''
                              ,CONVERT(VARCHAR(8000), Old.IsCorporate, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''IsCorporate''
                              ,CONVERT(VARCHAR(8000), Old.AllowGraduateAndStillOweMoney, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''IsCorporate''
                              ,CONVERT(VARCHAR(8000), Old.AllowGraduateWithoutFullCompletion, 121)
                        FROM   Deleted Old;
        END;
    END;



SET NOCOUNT OFF;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[syCampuses_Audit_Insert] on [dbo].[syCampuses]'
GO
IF OBJECT_ID(N'[dbo].[syCampuses_Audit_Insert]', 'TR') IS NULL
EXEC sp_executesql N'


CREATE TRIGGER [dbo].[syCampuses_Audit_Insert]
ON [dbo].[syCampuses]
FOR INSERT
AS
SET NOCOUNT ON;

DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
DECLARE @EventRows AS INT;
DECLARE @EventDate AS DATETIME;
DECLARE @UserName AS VARCHAR(50);
SET @AuditHistId = NEWID();
SET @EventRows = (
                 SELECT COUNT(*)
                 FROM   Inserted
                 );
SET @EventDate = (
                 SELECT TOP 1 ModDate
                 FROM   Inserted
                 );
SET @UserName = (
                SELECT TOP 1 ModUser
                FROM   Inserted
                );
IF @EventRows > 0
    BEGIN
        EXEC fmAuditHistAdd @AuditHistId
                           ,''syCampuses''
                           ,''I''
                           ,@EventRows
                           ,@EventDate
                           ,@UserName;
        BEGIN
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''CampCode''
                              ,CONVERT(VARCHAR(8000), New.CampCode, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''CampDescrip''
                              ,CONVERT(VARCHAR(8000), New.CampDescrip, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''Address1''
                              ,CONVERT(VARCHAR(8000), New.Address1, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''Address2''
                              ,CONVERT(VARCHAR(8000), New.Address2, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''City''
                              ,CONVERT(VARCHAR(8000), New.City, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''StateId''
                              ,CONVERT(VARCHAR(8000), New.StateId, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''Zip''
                              ,CONVERT(VARCHAR(8000), New.Zip, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''CountryId''
                              ,CONVERT(VARCHAR(8000), New.CountryId, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''StatusId''
                              ,CONVERT(VARCHAR(8000), New.StatusId, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''Phone1''
                              ,CONVERT(VARCHAR(8000), New.Phone1, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''Phone2''
                              ,CONVERT(VARCHAR(8000), New.Phone2, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''Phone3''
                              ,CONVERT(VARCHAR(8000), New.Phone3, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''Fax''
                              ,CONVERT(VARCHAR(8000), New.Fax, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''Email''
                              ,CONVERT(VARCHAR(8000), New.Email, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''Website''
                              ,CONVERT(VARCHAR(8000), New.Website, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''IsCorporate''
                              ,CONVERT(VARCHAR(8000), New.IsCorporate, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''IsCorporate''
                              ,CONVERT(VARCHAR(8000), New.AllowGraduateAndStillOweMoney, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''IsCorporate''
                              ,CONVERT(VARCHAR(8000), New.AllowGraduateWithoutFullCompletion, 121)
                        FROM   Inserted New;
        END;
    END;



SET NOCOUNT OFF;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[syCampuses_Audit_Update] on [dbo].[syCampuses]'
GO
IF OBJECT_ID(N'[dbo].[syCampuses_Audit_Update]', 'TR') IS NULL
EXEC sp_executesql N'

CREATE TRIGGER [dbo].[syCampuses_Audit_Update]
ON [dbo].[syCampuses]
FOR UPDATE
AS
SET NOCOUNT ON;

DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
DECLARE @EventRows AS INT;
DECLARE @EventDate AS DATETIME;
DECLARE @UserName AS VARCHAR(50);
SET @AuditHistId = NEWID();
SET @EventRows = (
                 SELECT COUNT(*)
                 FROM   Inserted
                 );
SET @EventDate = (
                 SELECT TOP 1 ModDate
                 FROM   Inserted
                 );
SET @UserName = (
                SELECT TOP 1 ModUser
                FROM   Inserted
                );
IF @EventRows > 0
    BEGIN
        EXEC fmAuditHistAdd @AuditHistId
                           ,''syCampuses''
                           ,''U''
                           ,@EventRows
                           ,@EventDate
                           ,@UserName;
        BEGIN
            IF UPDATE(CampCode)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''CampCode''
                                           ,CONVERT(VARCHAR(8000), Old.CampCode, 121)
                                           ,CONVERT(VARCHAR(8000), New.CampCode, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.CampCode <> New.CampCode
                                            OR (
                                               Old.CampCode IS NULL
                                               AND New.CampCode IS NOT NULL
                                               )
                                            OR (
                                               New.CampCode IS NULL
                                               AND Old.CampCode IS NOT NULL
                                               );
            IF UPDATE(CampDescrip)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''CampDescrip''
                                           ,CONVERT(VARCHAR(8000), Old.CampDescrip, 121)
                                           ,CONVERT(VARCHAR(8000), New.CampDescrip, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.CampDescrip <> New.CampDescrip
                                            OR (
                                               Old.CampDescrip IS NULL
                                               AND New.CampDescrip IS NOT NULL
                                               )
                                            OR (
                                               New.CampDescrip IS NULL
                                               AND Old.CampDescrip IS NOT NULL
                                               );
            IF UPDATE(Address1)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''Address1''
                                           ,CONVERT(VARCHAR(8000), Old.Address1, 121)
                                           ,CONVERT(VARCHAR(8000), New.Address1, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Address1 <> New.Address1
                                            OR (
                                               Old.Address1 IS NULL
                                               AND New.Address1 IS NOT NULL
                                               )
                                            OR (
                                               New.Address1 IS NULL
                                               AND Old.Address1 IS NOT NULL
                                               );
            IF UPDATE(Address2)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''Address2''
                                           ,CONVERT(VARCHAR(8000), Old.Address2, 121)
                                           ,CONVERT(VARCHAR(8000), New.Address2, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Address2 <> New.Address2
                                            OR (
                                               Old.Address2 IS NULL
                                               AND New.Address2 IS NOT NULL
                                               )
                                            OR (
                                               New.Address2 IS NULL
                                               AND Old.Address2 IS NOT NULL
                                               );
            IF UPDATE(City)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''City''
                                           ,CONVERT(VARCHAR(8000), Old.City, 121)
                                           ,CONVERT(VARCHAR(8000), New.City, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.City <> New.City
                                            OR (
                                               Old.City IS NULL
                                               AND New.City IS NOT NULL
                                               )
                                            OR (
                                               New.City IS NULL
                                               AND Old.City IS NOT NULL
                                               );
            IF UPDATE(StateId)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''StateId''
                                           ,CONVERT(VARCHAR(8000), Old.StateId, 121)
                                           ,CONVERT(VARCHAR(8000), New.StateId, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.StateId <> New.StateId
                                            OR (
                                               Old.StateId IS NULL
                                               AND New.StateId IS NOT NULL
                                               )
                                            OR (
                                               New.StateId IS NULL
                                               AND Old.StateId IS NOT NULL
                                               );
            IF UPDATE(Zip)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''Zip''
                                           ,CONVERT(VARCHAR(8000), Old.Zip, 121)
                                           ,CONVERT(VARCHAR(8000), New.Zip, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Zip <> New.Zip
                                            OR (
                                               Old.Zip IS NULL
                                               AND New.Zip IS NOT NULL
                                               )
                                            OR (
                                               New.Zip IS NULL
                                               AND Old.Zip IS NOT NULL
                                               );
            IF UPDATE(CountryId)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''CountryId''
                                           ,CONVERT(VARCHAR(8000), Old.CountryId, 121)
                                           ,CONVERT(VARCHAR(8000), New.CountryId, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.CountryId <> New.CountryId
                                            OR (
                                               Old.CountryId IS NULL
                                               AND New.CountryId IS NOT NULL
                                               )
                                            OR (
                                               New.CountryId IS NULL
                                               AND Old.CountryId IS NOT NULL
                                               );
            IF UPDATE(StatusId)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''StatusId''
                                           ,CONVERT(VARCHAR(8000), Old.StatusId, 121)
                                           ,CONVERT(VARCHAR(8000), New.StatusId, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.StatusId <> New.StatusId
                                            OR (
                                               Old.StatusId IS NULL
                                               AND New.StatusId IS NOT NULL
                                               )
                                            OR (
                                               New.StatusId IS NULL
                                               AND Old.StatusId IS NOT NULL
                                               );
            IF UPDATE(Phone1)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''Phone1''
                                           ,CONVERT(VARCHAR(8000), Old.Phone1, 121)
                                           ,CONVERT(VARCHAR(8000), New.Phone1, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Phone1 <> New.Phone1
                                            OR (
                                               Old.Phone1 IS NULL
                                               AND New.Phone1 IS NOT NULL
                                               )
                                            OR (
                                               New.Phone1 IS NULL
                                               AND Old.Phone1 IS NOT NULL
                                               );
            IF UPDATE(Phone2)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''Phone2''
                                           ,CONVERT(VARCHAR(8000), Old.Phone2, 121)
                                           ,CONVERT(VARCHAR(8000), New.Phone2, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Phone2 <> New.Phone2
                                            OR (
                                               Old.Phone2 IS NULL
                                               AND New.Phone2 IS NOT NULL
                                               )
                                            OR (
                                               New.Phone2 IS NULL
                                               AND Old.Phone2 IS NOT NULL
                                               );
            IF UPDATE(Phone3)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''Phone3''
                                           ,CONVERT(VARCHAR(8000), Old.Phone3, 121)
                                           ,CONVERT(VARCHAR(8000), New.Phone3, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Phone3 <> New.Phone3
                                            OR (
                                               Old.Phone3 IS NULL
                                               AND New.Phone3 IS NOT NULL
                                               )
                                            OR (
                                               New.Phone3 IS NULL
                                               AND Old.Phone3 IS NOT NULL
                                               );
            IF UPDATE(Fax)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''Fax''
                                           ,CONVERT(VARCHAR(8000), Old.Fax, 121)
                                           ,CONVERT(VARCHAR(8000), New.Fax, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Fax <> New.Fax
                                            OR (
                                               Old.Fax IS NULL
                                               AND New.Fax IS NOT NULL
                                               )
                                            OR (
                                               New.Fax IS NULL
                                               AND Old.Fax IS NOT NULL
                                               );
            IF UPDATE(Email)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''Email''
                                           ,CONVERT(VARCHAR(8000), Old.Email, 121)
                                           ,CONVERT(VARCHAR(8000), New.Email, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Email <> New.Email
                                            OR (
                                               Old.Email IS NULL
                                               AND New.Email IS NOT NULL
                                               )
                                            OR (
                                               New.Email IS NULL
                                               AND Old.Email IS NOT NULL
                                               );
            IF UPDATE(Website)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''Website''
                                           ,CONVERT(VARCHAR(8000), Old.Website, 121)
                                           ,CONVERT(VARCHAR(8000), New.Website, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Website <> New.Website
                                            OR (
                                               Old.Website IS NULL
                                               AND New.Website IS NOT NULL
                                               )
                                            OR (
                                               New.Website IS NULL
                                               AND Old.Website IS NOT NULL
                                               );
            IF UPDATE(IsCorporate)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''IsCorporate''
                                           ,CONVERT(VARCHAR(8000), Old.IsCorporate, 121)
                                           ,CONVERT(VARCHAR(8000), New.IsCorporate, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.IsCorporate <> New.IsCorporate
                                            OR (
                                               Old.IsCorporate IS NULL
                                               AND New.IsCorporate IS NOT NULL
                                               )
                                            OR (
                                               New.IsCorporate IS NULL
                                               AND Old.IsCorporate IS NOT NULL
                                               );

            IF UPDATE(AllowGraduateWithoutFullCompletion)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''IsCorporate''
                                           ,CONVERT(VARCHAR(8000), Old.AllowGraduateWithoutFullCompletion, 121)
                                           ,CONVERT(VARCHAR(8000), New.AllowGraduateWithoutFullCompletion, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.AllowGraduateWithoutFullCompletion <> New.AllowGraduateWithoutFullCompletion
                                            OR (
                                               Old.AllowGraduateWithoutFullCompletion IS NULL
                                               AND New.AllowGraduateWithoutFullCompletion IS NOT NULL
                                               )
                                            OR (
                                               New.AllowGraduateWithoutFullCompletion IS NULL
                                               AND Old.AllowGraduateWithoutFullCompletion IS NOT NULL
                                               );

            IF UPDATE(AllowGraduateAndStillOweMoney)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''IsCorporate''
                                           ,CONVERT(VARCHAR(8000), Old.AllowGraduateAndStillOweMoney, 121)
                                           ,CONVERT(VARCHAR(8000), New.AllowGraduateAndStillOweMoney, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.AllowGraduateAndStillOweMoney <> New.AllowGraduateAndStillOweMoney
                                            OR (
                                               Old.AllowGraduateAndStillOweMoney IS NULL
                                               AND New.AllowGraduateAndStillOweMoney IS NOT NULL
                                               )
                                            OR (
                                               New.AllowGraduateAndStillOweMoney IS NULL
                                               AND Old.AllowGraduateAndStillOweMoney IS NOT NULL
                                               );
        END;
    END;



SET NOCOUNT OFF;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[arPrgChargePeriodSeq]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[arPrgChargePeriodSeq] ALTER COLUMN [Sequence] [smallint] NOT NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[syIneligibilityReasons]'
GO
IF OBJECT_ID(N'[dbo].[syIneligibilityReasons]', 'U') IS NULL
CREATE TABLE [dbo].[syIneligibilityReasons]
(
[InelReasonId] [uniqueidentifier] NOT NULL,
[InelReasonName] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[InelReasonCode] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__syInelig__0CB92C135B492739] on [dbo].[syIneligibilityReasons]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK__syInelig__0CB92C135B492739]', 'PK') AND parent_object_id = OBJECT_ID(N'[dbo].[syIneligibilityReasons]', 'U'))
ALTER TABLE [dbo].[syIneligibilityReasons] ADD CONSTRAINT [PK__syInelig__0CB92C135B492739] PRIMARY KEY CLUSTERED  ([InelReasonId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[syAccreditingAgencies]'
GO
IF OBJECT_ID(N'[dbo].[syAccreditingAgencies]', 'U') IS NULL
CREATE TABLE [dbo].[syAccreditingAgencies]
(
[AccreditingAgencyId] [int] NOT NULL,
[Code] [varchar] (80) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[Description] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModUser] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NULL,
[StatusId] [uniqueidentifier] NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__syAccred__3D7480ACEC715508] on [dbo].[syAccreditingAgencies]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK__syAccred__3D7480ACEC715508]', 'PK') AND parent_object_id = OBJECT_ID(N'[dbo].[syAccreditingAgencies]', 'U'))
ALTER TABLE [dbo].[syAccreditingAgencies] ADD CONSTRAINT [PK__syAccred__3D7480ACEC715508] PRIMARY KEY CLUSTERED  ([AccreditingAgencyId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [dbo].[syAccreditingAgencies]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UQ__syAccred__A25C5AA7771F9A19]', 'UQ') AND parent_object_id = OBJECT_ID(N'[dbo].[syAccreditingAgencies]', 'U'))
ALTER TABLE [dbo].[syAccreditingAgencies] ADD CONSTRAINT [UQ__syAccred__A25C5AA7771F9A19] UNIQUE NONCLUSTERED  ([Code])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[syApprovedNACCASProgramVersion]'
GO
IF OBJECT_ID(N'[dbo].[syApprovedNACCASProgramVersion]', 'U') IS NULL
CREATE TABLE [dbo].[syApprovedNACCASProgramVersion]
(
[IsApproved] [bit] NOT NULL,
[ProgramVersionId] [uniqueidentifier] NULL,
[CreatedById] [uniqueidentifier] NULL,
[CreateDate] [datetime2] NULL,
[NaccasSettingId] [int] NOT NULL,
[ApprovedNACCASProgramVersionId] [int] NOT NULL IDENTITY(1, 1)
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_syApprovedNACCASProgramVersion_ApprovedNACCASProgramVersionId] on [dbo].[syApprovedNACCASProgramVersion]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK_syApprovedNACCASProgramVersion_ApprovedNACCASProgramVersionId]', 'PK') AND parent_object_id = OBJECT_ID(N'[dbo].[syApprovedNACCASProgramVersion]', 'U'))
ALTER TABLE [dbo].[syApprovedNACCASProgramVersion] ADD CONSTRAINT [PK_syApprovedNACCASProgramVersion_ApprovedNACCASProgramVersionId] PRIMARY KEY CLUSTERED  ([ApprovedNACCASProgramVersionId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[syNaccasSettings]'
GO
IF OBJECT_ID(N'[dbo].[syNaccasSettings]', 'U') IS NULL
CREATE TABLE [dbo].[syNaccasSettings]
(
[CampusId] [uniqueidentifier] NOT NULL,
[CreatedById] [uniqueidentifier] NULL,
[CreateDate] [datetime2] NOT NULL,
[NaccasSettingId] [int] NOT NULL IDENTITY(1, 1),
[ModUserId] [uniqueidentifier] NULL,
[ModDate] [datetime] NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_syNaccasSettings_NaccasSettingId] on [dbo].[syNaccasSettings]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK_syNaccasSettings_NaccasSettingId]', 'PK') AND parent_object_id = OBJECT_ID(N'[dbo].[syNaccasSettings]', 'U'))
ALTER TABLE [dbo].[syNaccasSettings] ADD CONSTRAINT [PK_syNaccasSettings_NaccasSettingId] PRIMARY KEY CLUSTERED  ([NaccasSettingId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[syNACCASDropReasons]'
GO
IF OBJECT_ID(N'[dbo].[syNACCASDropReasons]', 'U') IS NULL
CREATE TABLE [dbo].[syNACCASDropReasons]
(
[NACCASDropReasonId] [uniqueidentifier] NOT NULL,
[Name] [varchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Code] [varchar] (3) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK__syNACCAS__57EF0CA511FAE5F4] on [dbo].[syNACCASDropReasons]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK__syNACCAS__57EF0CA511FAE5F4]', 'PK') AND parent_object_id = OBJECT_ID(N'[dbo].[syNACCASDropReasons]', 'U'))
ALTER TABLE [dbo].[syNACCASDropReasons] ADD CONSTRAINT [PK__syNACCAS__57EF0CA511FAE5F4] PRIMARY KEY CLUSTERED  ([NACCASDropReasonId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[syRptFilterOtherPrefs]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[syRptFilterOtherPrefs] ALTER COLUMN [RptParamId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CompletedRequiredCourses]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CompletedRequiredCourses]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'

CREATE FUNCTION [dbo].[CompletedRequiredCourses]
    (
        @StuEnrollId UNIQUEIDENTIFIER
    )
RETURNS BIT
AS
    BEGIN
        DECLARE @ReturnValue BIT = 0;
        SET @ReturnValue = (
                           SELECT CASE WHEN EXISTS (
                                                   SELECT     *
                                                   FROM       dbo.arResults results
                                                   INNER JOIN dbo.arGradeSystemDetails gsd ON gsd.GrdSysDetailId = results.GrdSysDetailId
                                                   WHERE      results.StuEnrollId = @StuEnrollId
                                                              AND (
                                                                  gsd.IsPass = 0
                                                                  AND gsd.IsCreditsAttempted = 1
                                                                  )
                                                   ) THEN 0
                                       ELSE 1
                                  END
                           );
        RETURN @ReturnValue;
    END;



'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[DoesEnrollmentHavePendingBalance]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[DoesEnrollmentHavePendingBalance]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[DoesEnrollmentHavePendingBalance]
    (
        @StuEnrollId UNIQUEIDENTIFIER
    )
RETURNS INT
AS
    BEGIN
        DECLARE @ReturnValue INT = 0;


        DECLARE @AmountOwe DECIMAL(18, 2) = (
                                            SELECT SUM(TransAmount)
                                            FROM   dbo.saTransactions
                                            WHERE  StuEnrollId = @StuEnrollId
                                                   AND Voided = 0
                                            );
        IF ( @AmountOwe > 0 )
            BEGIN
                SET @ReturnValue = 1;
            END;
        RETURN @ReturnValue;
    END;

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[EnrollmentHasAttendance]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[EnrollmentHasAttendance]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'
-- =============================================
-- Author:		Sweta, Thakkar
-- Create date: 05/21/2019
-- Description:	Determine if enrollment has attendance
-- =============================================
CREATE FUNCTION [dbo].[EnrollmentHasAttendance]
    (
        @StuEnrollId UNIQUEIDENTIFIER
    )
RETURNS VARCHAR(3)
AS
    BEGIN

        DECLARE @retVal VARCHAR(3);
        IF (
           SELECT SUM(ActualDays)
           FROM   dbo.syStudentAttendanceSummary
           WHERE  StuEnrollId = @StuEnrollId
                  AND ActualDays != 9999.0
                  AND ActualDays != 999.0
           ) > 0
            SET @retVal = ''Yes'';
        ELSE
            SET @retVal = ''No'';

        RETURN @retVal;

    END;
	
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[GetGraduatedDate]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetGraduatedDate]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'


CREATE FUNCTION [dbo].[GetGraduatedDate]
    (
        @StuEnrollId UNIQUEIDENTIFIER
    )
RETURNS DATETIME
AS
    BEGIN
        DECLARE @Graduated INT = 14;
        DECLARE @ReturnValue DATETIME = NULL;
        SET @ReturnValue = (
                           SELECT     TOP 1 DateOfChange
                           FROM       dbo.syStudentStatusChanges
                           INNER JOIN dbo.syStatusCodes ON syStatusCodes.StatusCodeId = syStudentStatusChanges.NewStatusId
                           WHERE      SysStatusId = @Graduated AND StuEnrollId = @StuEnrollId
                           ORDER BY   DateOfChange DESC
                           );
        RETURN @ReturnValue;
    END;



'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[GetStuEnrollment]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetStuEnrollment]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[GetStuEnrollment](
@FirstName varchar(MAX),
@LastName varchar(MAX))
RETURNs TABLE
as
RETURN (
	SELECT e.StuEnrollId FROM adleads L LEFT JOIN dbo.arStuEnrollments E ON E.LeadId = L.LeadId
	WHERE L.FirstName = @FirstName AND L.LastName = @LastName
	
)
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[StudentScheduledHours]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StudentScheduledHours]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'-- =============================================
-- Author:		FAME Inc.
-- Create date: 5/23/2019
-- Description:	Student scheduled table valued function. Returns table student enrollment id , scheduled hours for day of the week , students must all be from same single campus
CREATE   FUNCTION [dbo].[StudentScheduledHours]
    (
        @StuEnrollIdList VARCHAR(MAX) = NULL
       ,@Date DATETIME
    )
RETURNS TABLE
AS
RETURN (
       SELECT ScheduledData.StuEnrollId
             ,CASE WHEN CAST(( ScheduledData.ScheduledMinutes - ScheduledData.LunchMinutes - ScheduledData.HolidayMinutes + LunchHolidayOverlapMinutes ) / 60.0 AS DECIMAL(16, 2)) < 0 THEN
                       0
                   ELSE
                       CAST(( ScheduledData.ScheduledMinutes - ScheduledData.LunchMinutes - ScheduledData.HolidayMinutes + LunchHolidayOverlapMinutes ) / 60.0 AS DECIMAL(16, 2))
              END AS CalculatedScheduledHours
             ,ScheduledData.ScheduleId
       FROM   (
              SELECT      arStudentSchedules.StuEnrollId
                         ,CampusId
                         ,total
                         ,timein
                         ,timeout
                         ,lunchin
                         ,lunchout
                         ,arProgSchedules.ScheduleId
                         ,ISNULL(DATEDIFF(MINUTE, timein, timeout), 0) AS ScheduledMinutes
                         ,ISNULL(DATEDIFF(MINUTE, lunchout, lunchin), 0) AS LunchMinutes
                         ,ISNULL(
                                    CASE WHEN Holiday.AllDay = 1 THEN DATEDIFF(MINUTE, timein, timeout)
                                         WHEN (
                                              CAST(Holiday.BeginTimeInteral AS TIME) <= CAST(timeout AS TIME)
                                              AND CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME) >= CAST(timein AS TIME)
                                              ) -- holiday falls entirely inside students schedule

                                    THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME))
                                         WHEN (
                                              CAST(Holiday.BeginTimeInteral AS TIME) >= CAST(timein AS TIME)
                                              AND CAST(Holiday.BeginTimeInteral AS TIME) <= CAST(timeout AS TIME)
                                              ) -- holiday start time falls betweem time in and time out 
                                    THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(timeout AS TIME))
                                         WHEN (
                                              CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME) >= CAST(timein AS TIME)
                                              AND CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME) <= CAST(timeout AS TIME)
                                              ) -- holiday end time falls betweem time in and time out 
                                    THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME))
                                         ELSE 0
                                    END
                                   ,0
                                ) AS HolidayMinutes
                         ,ISNULL(
                                    CASE WHEN Holiday.AllDay = 1 THEN DATEDIFF(MINUTE, lunchout, lunchin)
                                         WHEN (
                                              CAST(Holiday.BeginTimeInteral AS TIME) <= CAST(lunchin AS TIME)
                                              AND CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME) >= CAST(lunchout AS TIME)
                                              ) -- holiday falls entirely inside holiday schedule
                                    THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME))
                                         WHEN (
                                              CAST(Holiday.BeginTimeInteral AS TIME) >= CAST(lunchout AS TIME)
                                              AND CAST(Holiday.BeginTimeInteral AS TIME) <= CAST(lunchin AS TIME)
                                              ) -- holiday start time falls between lunch
                                    THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(lunchin AS TIME))
                                         WHEN (
                                              CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME) >= CAST(lunchout AS TIME)
                                              AND CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME) <= CAST(lunchin AS TIME)
                                              ) -- holiday end time falls between lunch 
                                    THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME))
                                         ELSE 0
                                    END
                                   ,0
                                ) AS LunchHolidayOverlapMinutes -- lunch and holiday may overlap, calculate this time and add it back to student scheduled hours bucket
                         ,Holiday.AllDay
                         ,Holiday.BeginTimeInteral
                         ,Holiday.EndTimeInterval
                         ,Holiday.HolidayDescrip
              FROM        dbo.arStuEnrollments
              JOIN        dbo.arStudentSchedules ON arStudentSchedules.StuEnrollId = arStuEnrollments.StuEnrollId
              JOIN        dbo.arProgSchedules ON arProgSchedules.ScheduleId = arStudentSchedules.ScheduleId
              JOIN        dbo.arProgScheduleDetails ON arProgScheduleDetails.ScheduleId = arStudentSchedules.ScheduleId
              OUTER APPLY (
                          SELECT    TOP 1 HolidayDescrip
                                   ,HolidayStartDate
                                   ,HolidayEndDate
                                   ,AllDay
                                   ,a.TimeIntervalDescrip AS BeginTimeInteral
                                   ,b.TimeIntervalDescrip AS EndTimeInterval
                          FROM      dbo.syHolidays
                          LEFT JOIN dbo.cmTimeInterval a ON a.TimeIntervalId = syHolidays.StartTimeId
                          LEFT JOIN dbo.cmTimeInterval b ON b.TimeIntervalId = syHolidays.EndTimeId
                          WHERE     (
                                    CAST(@Date AS DATE) >= CAST(HolidayStartDate AS DATE)
                                    AND CAST(@Date AS DATE) <= CAST(HolidayEndDate AS DATE)
                                    )
                                    AND syHolidays.CampGrpId IN (
                                                                SELECT CampGrpId
                                                                FROM   dbo.syCmpGrpCmps
                                                                WHERE  dbo.syCmpGrpCmps.CampusId = (
                                                                                                   SELECT CampusId
                                                                                                   FROM   dbo.arStuEnrollments
                                                                                                   WHERE  StuEnrollId = (
                                                                                                                        SELECT TOP 1 Val
                                                                                                                        FROM   MultipleValuesForReportParameters(
                                                                                                                                                                    @StuEnrollIdList
                                                                                                                                                                   ,'',''
                                                                                                                                                                   ,1
                                                                                                                                                                )
                                                                                                                        )
                                                                                                   )
                                                                )
                                    AND syHolidays.StatusId = (
                                                              SELECT TOP 1 StatusId
                                                              FROM   dbo.syStatuses
                                                              WHERE  StatusCode = ''A''
                                                              )
                          ) Holiday
              WHERE       dw = ( DATEPART(dw, @Date) - 1 )
                          AND arStuEnrollments.StuEnrollId IN (
                                                              SELECT Val
                                                              FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                              )
              --AND (
              --    @Date >= arStuEnrollments.StartDate
              --    AND @Date <= dbo.arStuEnrollments.ExpGradDate
              --    )
              ) ScheduledData
       );
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[UDF_GetEnrollmentStatusIdAtGivenDate]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UDF_GetEnrollmentStatusIdAtGivenDate]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'--------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Function to return the status of an enrollment at a specified date
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[UDF_GetEnrollmentStatusIdAtGivenDate]
    (
        @StuEnrollId UNIQUEIDENTIFIER
       ,@ReportDate DATETIME
    )
RETURNS UNIQUEIDENTIFIER
    BEGIN
        DECLARE @ReturnValue UNIQUEIDENTIFIER;
        DECLARE @CurrentStatus UNIQUEIDENTIFIER;
        DECLARE @StartDate DATETIME;
		DECLARE @LastDateOfChange DATETIME;
		DECLARE @LastNewStatus VARCHAR(50);


        SELECT     @CurrentStatus = sc.StatusCodeId
                  ,@StartDate = se.StartDate
        FROM       dbo.arStuEnrollments se
        INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
        WHERE      se.StuEnrollId = @StuEnrollId;

		SET @LastDateOfChange = (SELECT MAX(DateOfChange)
										 FROM dbo.syStudentStatusChanges
										 WHERE StuEnrollId=@StuEnrollId);

		SET @LastNewStatus=(SELECT TOP 1 sc.StatusCodeId
							FROM dbo.syStudentStatusChanges ssc
							INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = ssc.NewStatusId
							WHERE ssc.StuEnrollId=@StuEnrollId
							ORDER BY ssc.DateOfChange DESC, ssc.ModDate DESC);


        --If there are no status changes for the enrollment and the given date is >= enrollment start date we can simply return the current status. 
        IF NOT EXISTS (
                      SELECT 1
                      FROM   dbo.syStudentStatusChanges
                      WHERE  StuEnrollId = @StuEnrollId
                      )           
            BEGIN
				IF @ReportDate >= @StartDate
					BEGIN
						SET @ReturnValue = @CurrentStatus;
					END                
            END;
		ELSE			
			BEGIN				
				--If the report date > last date of change for the enrollment then we can simply return the last new status (should be same as current status)
				IF @ReportDate > @LastDateOfChange AND @ReportDate >= @StartDate
					BEGIN
						SET @ReturnValue=@LastNewStatus;
					END
				ELSE
					BEGIN
						--If the report date is the same as a date of change then we can simply return the new status for that date of change.
						--Use the FORMAT function just in case the DateOfChange field in the database has a time component on it such as 2018-10-19 05:00:00.000
					
								--At this point we can get the last change record with DateOfChange that is less than the report date
								SET @ReturnValue=(SELECT TOP 1 sc.StatusCodeId
														FROM dbo.syStudentStatusChanges ssc
														INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = ssc.NewStatusId
														WHERE ssc.StuEnrollId=@StuEnrollId
														AND  DateOfChange <= @ReportDate
														ORDER BY ssc.DateOfChange DESC, ssc.ModDate DESC);
							

					END	

			END	







        RETURN @ReturnValue;
    END;



'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudAddresses]'
GO
IF OBJECT_ID(N'[dbo].[arStudAddresses]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudAddresses]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[usp_AR_GetCompletedComponents]'
GO
IF OBJECT_ID(N'[dbo].[usp_AR_GetCompletedComponents]', 'P') IS NULL
EXEC sp_executesql N'/* 
US4110 Transfer Partially Completed Components 

CREATED: 
6/27/2013 WMP

PURPOSE: 
Select completed components for a course to be displayed in transfer components grid
	A record in arGrdBkResults table (regardless of score) means a grade has been entered (and component is completed)

MODIFIED:
7/10/2013 WMP	DE9829	Remove score > 0 from where
7/10/2013 WMP	DE9832	Show lab work (500), lab hours (503) and externship hours (544) as cumulative amounts
7/18/2013 WMP			Update sort order
7/22/2013 WMP	DE9875	Exclude externship hours (544) since they are only empty links
9/3/2013  WMP	US4366	renamed to remove ByCourse (formerly usp_AR_GetCompletedComponentsByCourse)

*/

CREATE PROCEDURE [dbo].[usp_AR_GetCompletedComponents] @ResultId UNIQUEIDENTIFIER
AS
BEGIN


    CREATE TABLE #ComponentScoreList
    --create list of results to include cumulative and individual scores/hours
    (
        InstrGrdBkWgtId UNIQUEIDENTIFIER,
        GrdComponentTypeId UNIQUEIDENTIFIER,
        ClsSectionId UNIQUEIDENTIFIER,
        StuEnrollId UNIQUEIDENTIFIER,
        Score DECIMAL(10, 2)
    );
    INSERT INTO #ComponentScoreList
    (
        InstrGrdBkWgtId,
        GrdComponentTypeId,
        ClsSectionId,
        StuEnrollId,
        Score
    )
    SELECT gbwd.InstrGrdBkWgtId,
           gbwd.GrdComponentTypeId,
           gbr.ClsSectionId,
           StuEnrollId,
           gbr.Score AS Score
    FROM arGrdBkResults gbr
        INNER JOIN arGrdBkWgtDetails gbwd
            ON gbwd.InstrGrdBkWgtDetailId = gbr.InstrGrdBkWgtDetailId
        INNER JOIN arGrdComponentTypes gct
            ON gct.GrdComponentTypeId = gbwd.GrdComponentTypeId
    WHERE gct.SysComponentTypeId NOT IN ( 500, 503, 544 ); --individual scores

    INSERT INTO #ComponentScoreList
    (
        InstrGrdBkWgtId,
        GrdComponentTypeId,
        ClsSectionId,
        StuEnrollId,
        Score
    )
    SELECT gbwd.InstrGrdBkWgtId,
           gbwd.GrdComponentTypeId,
           gbr.ClsSectionId,
           StuEnrollId,
           SUM(ISNULL(gbr.Score, 0)) AS Score
    FROM arGrdBkResults gbr
        INNER JOIN arGrdBkWgtDetails gbwd
            ON gbwd.InstrGrdBkWgtDetailId = gbr.InstrGrdBkWgtDetailId
        INNER JOIN arGrdComponentTypes gct
            ON gct.GrdComponentTypeId = gbwd.GrdComponentTypeId
    WHERE gct.SysComponentTypeId IN ( 500, 503 ) --cumulative scores
    GROUP BY gbwd.InstrGrdBkWgtId,
             gbwd.GrdComponentTypeId,
             gbr.ClsSectionId,
             StuEnrollId;



    SELECT csl.InstrGrdBkWgtId,
           gct.Descrip AS Description,
           r.Resource AS ComponentType,
           csl.Score,
           rs.ResultId,
           rq.ReqId
    FROM #ComponentScoreList csl
        INNER JOIN arGrdComponentTypes gct
            ON gct.GrdComponentTypeId = csl.GrdComponentTypeId
        INNER JOIN syResources r
            ON r.ResourceID = gct.SysComponentTypeId
        INNER JOIN arClassSections cs
            ON cs.ClsSectionId = csl.ClsSectionId
        INNER JOIN arReqs rq
            ON rq.ReqId = cs.ReqId
        INNER JOIN arResults rs
            ON rs.TestId = cs.ClsSectionId
               AND rs.StuEnrollId = csl.StuEnrollId
    WHERE rs.ResultId = @ResultId
    ORDER BY r.Resource,
             gct.Descrip;

    DROP TABLE #ComponentScoreList;


END;




'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_AR_GetSystemTransCodesRefunds_GetList]'
GO
IF OBJECT_ID(N'[dbo].[USP_AR_GetSystemTransCodesRefunds_GetList]', 'P') IS NULL
EXEC sp_executesql N'

CREATE PROCEDURE [dbo].[USP_AR_GetSystemTransCodesRefunds_GetList]
    (
        @ShowActive NVARCHAR(50)
       ,@CampusId NVARCHAR(50) = NULL
    )
AS /*----------------------------------------------------------------------------------------------------
    Author : Saraswathi Lakshmanan
    
    Create date : 05/05/2010
    
    Procedure Name : [USP_AR_GetSystemTransCodesRefunds_GetList]

    Objective : Get the system TransactionCodes of type charges
    
    Parameters : Name Type Data Type Required? 
                        ===== ==== ========= ========= 
                        @ShowActive In nvarchar Required
    
    Output : Returns the TransCodes of type whose systransCodeID 16 
                        
*/
    -----------------------------------------------------------------------------------------------------

    BEGIN

        DECLARE @ShowActiveValue NVARCHAR(20);
        IF LOWER(@ShowActive) = ''true''
            BEGIN
                SET @ShowActiveValue = ''Active'';
            END;
        ELSE
            BEGIN
                SET @ShowActiveValue = ''InActive'';
            END;


        SELECT   TC.TransCodeId
                ,TC.TransCodeCode
                ,TC.TransCodeDescrip
                ,TC.StatusId
                ,TC.CampGrpId
                ,ST.StatusId
                ,ST.Status
        FROM     saTransCodes TC
                ,syStatuses ST
        WHERE    TC.StatusId = ST.StatusId
                 AND SysTransCodeId = 16
                 AND ST.Status = @ShowActiveValue
                 AND (
                     @CampusId IS NULL
                     OR CampGrpId IN (
                                     SELECT CampGrpId
                                     FROM   dbo.syCmpGrpCmps
                                     WHERE  CampusId = @CampusId
                                     )
                     )
        ORDER BY TC.TransCodeDescrip;

    END;








'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_ConsecutiveDaysAbsentByClass_MainReport]'
GO
IF OBJECT_ID(N'[dbo].[USP_ConsecutiveDaysAbsentByClass_MainReport]', 'P') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[USP_ConsecutiveDaysAbsentByClass_MainReport]
    @TermId VARCHAR(MAX) = NULL
   ,@ReqId VARCHAR(MAX) = NULL
   ,@ConsDays INT = 1
   ,@CutoffDate DATETIME
   ,@CampusId VARCHAR(MAX)
   ,@ShowCalendarDays BIT = 0
AS
    BEGIN
        SELECT   TermId
                ,TermDescrip
                ,ReqId
                ,Course
                ,ClsSectionId
                ,ClsSection
                ,[Term Start Date]
                ,COUNT(*) AS ID
        FROM     (
                 SELECT     st.StudentNumber
                           ,st.FirstName + '' '' + ISNULL(st.MiddleName, '''') + '' '' + st.LastName AS Student
                           ,se.StartDate
                           ,se.LDA
                           ,SC.StatusCodeDescrip AS status
                           ,tm.TermId
                           ,tm.TermDescrip
                           ,tm.StartDate AS [Term Start Date]
                           ,rq.ReqId
                           ,rq.Descrip AS Course
                           ,cs.ClsSectionId
                           ,cs.ClsSection
                           ,(
                            SELECT dbo.MaxConsecutiveDaysAbsentForProgram(se.StuEnrollId, @CutoffDate, @ShowCalendarDays, @ConsDays, cs.ClsSectionId)
                            ) AS ''Days Missed''
                 FROM       adLeads AS st
                 INNER JOIN dbo.arStuEnrollments AS se ON st.StudentId = se.StudentId
                 INNER JOIN arPrgVersions AS pv ON se.PrgVerId = pv.PrgVerId
                 INNER JOIN dbo.arResults AS rs ON se.StuEnrollId = rs.StuEnrollId
                 INNER JOIN dbo.arClassSections AS cs ON rs.TestId = cs.ClsSectionId
                 INNER JOIN dbo.arTerm AS tm ON cs.TermId = tm.TermId
                 INNER JOIN arReqs AS rq ON cs.ReqId = rq.ReqId
                 INNER JOIN dbo.syStatusCodes AS SC ON se.StatusCodeId = SC.StatusCodeId
                 INNER JOIN dbo.sySysStatus AS SS ON SC.SysStatusId = SS.SysStatusId
                 WHERE      (
                            @TermId IS NULL
                            OR tm.TermId IN (
                                            SELECT Val
                                            FROM   MultipleValuesForReportParameters(@TermId, '','', 1)
                                            )
                            )
                            AND (
                                @ReqId IS NULL
                                OR rq.ReqId IN (
                                               SELECT Val
                                               FROM   MultipleValuesForReportParameters(@ReqId, '','', 1)
                                               )
                                )
                            AND SS.InSchool = 1
                            AND (
                                @CampusId IS NULL
                                OR ( se.CampusId IN (
                                                    SELECT Val
                                                    FROM   MultipleValuesForReportParameters(@CampusId, '','', 1)
                                                    )
                                   )
                                )
                 ) AS R
        WHERE    ( R.[Days Missed] >= @ConsDays )
        GROUP BY TermId
                ,TermDescrip
                ,ReqId
                ,Course
                ,ClsSectionId
                ,ClsSection
                ,[Term Start Date]
        ORDER BY [Term Start Date]
                ,R.Course;

    END;



'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudentPhone]'
GO
IF OBJECT_ID(N'[dbo].[arStudentPhone]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudentPhone]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_EnrollLead]'
GO
IF OBJECT_ID(N'[dbo].[USP_EnrollLead]', 'P') IS NULL
EXEC sp_executesql N'-- =========================================================================================================          
-- USP_EnrollLead          
-- =========================================================================================================          
CREATE PROCEDURE [dbo].[USP_EnrollLead]
    (
        @leadId AS UNIQUEIDENTIFIER
       ,@campus UNIQUEIDENTIFIER
       ,@gender AS UNIQUEIDENTIFIER
       ,@familyIncome UNIQUEIDENTIFIER = NULL
       ,@housingType UNIQUEIDENTIFIER = NULL
       ,@educationLevel UNIQUEIDENTIFIER = NULL
       ,@adminCriteria UNIQUEIDENTIFIER = NULL
       ,@degSeekCert UNIQUEIDENTIFIER = NULL
       ,@attendType UNIQUEIDENTIFIER = NULL
       ,@race UNIQUEIDENTIFIER = NULL
       ,@citizenship UNIQUEIDENTIFIER = NULL
       ,@prgVersion UNIQUEIDENTIFIER = NULL
       ,@programId UNIQUEIDENTIFIER = NULL
       ,@areaId UNIQUEIDENTIFIER = NULL
       ,@prgVerType INT = 0
       ,@startDt DATETIME
       --,@studentId UNIQUEIDENTIFIER          
       ,@expectedStartDt DATETIME = NULL
       ,@contractGrdDt DATETIME
       ,@tuitionCategory UNIQUEIDENTIFIER
       ,@enrollDt DATETIME
       ,@transferhr DECIMAL(18, 2) = NULL
       ,@dob AS DATETIME = NULL
       ,@ssn AS VARCHAR(50) = NULL
       ,@depencytype UNIQUEIDENTIFIER = NULL
       ,@maritalstatus UNIQUEIDENTIFIER = NULL
       ,@nationality UNIQUEIDENTIFIER = NULL
       ,@geographicType UNIQUEIDENTIFIER = NULL
       ,@state UNIQUEIDENTIFIER = NULL
       ,@disabled BIT = NULL
       ,@disableAutoCharge BIT = NULL
	   ,@thirdPartyContract BIT = 0
       ,@firsttimeSchool BIT = NULL
       ,@firsttimePostSec BIT = NULL
       ,@schedule UNIQUEIDENTIFIER = NULL
       ,@shift UNIQUEIDENTIFIER = NULL
       ,@midptDate DATETIME = NULL
       ,@transferDt DATETIME = NULL
       ,@entranceInterviewDt DATETIME = NULL
       ,@chargingMethod UNIQUEIDENTIFIER = NULL
       ,@fAAvisor UNIQUEIDENTIFIER = NULL
       ,@acedemicAdvisor UNIQUEIDENTIFIER = NULL
       ,@badgeNum VARCHAR(50) = NULL
       ,@studentNumber VARCHAR(50) = ''''
       --,@studentStatusId UNIQUEIDENTIFIER = NULL -- should be active as it is in adleads          
       --,@stuEnrollId UNIQUEIDENTIFIER = NULL              
       ,@modUser UNIQUEIDENTIFIER = NULL
       ,@enrollId VARCHAR(50) = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @transDate DATETIME;
        DECLARE @MyError AS INTEGER;
        DECLARE @Message AS NVARCHAR(MAX);
        DECLARE @modUserN VARCHAR(50)
               ,@studentStatusId UNIQUEIDENTIFIER
               ,@studentEnrolStatus UNIQUEIDENTIFIER
               ,@studentId UNIQUEIDENTIFIER
               ,@stuEnrollId UNIQUEIDENTIFIER
               ,@admissionRepId UNIQUEIDENTIFIER;
        SET @transDate = GETDATE();
        SET @studentStatusId = (
                               SELECT StatusId
                               FROM   syStatuses
                               WHERE  StatusCode = ''A''
                               );
        --,@originalStatus UNIQUEIDENTIFIER;           
        SET @studentId = NEWID();
        --set @studentEnrolStatus to system future start          
        SET @studentEnrolStatus = (
                                  SELECT DISTINCT TOP 1 A.StatusCodeId
                                  FROM   syStatusCodes A
                                        ,sySysStatus B
                                        ,syStatuses C
                                        ,syStatusLevels D
                                  WHERE  A.SysStatusId = B.SysStatusId
                                         AND A.StatusId = C.StatusId
                                         AND B.StatusLevelId = D.StatusLevelId
                                         AND C.Status = ''Active''
                                         AND D.StatusLevelId = 2
                                         AND B.SysStatusId = 7
                                  --ORDER BY A.StatusCodeDescrip          
                                  );
        DECLARE @acCalendarID INT;
        SELECT @modUserN = UserName
        FROM   syUsers
        WHERE  UserId = @modUser;
        SET @MyError = 0; -- all good          
        SET @Message = '''';
        BEGIN TRANSACTION;
        BEGIN TRY
            --step 1: temperary insert into arstudent          
            DECLARE @lName VARCHAR(50)
                   ,@fName VARCHAR(50)
                   ,@mName VARCHAR(50);

            SET @lName = (
                         SELECT LastName
                         FROM   adLeads
                         WHERE  LeadId = @leadId
                         );
            SET @fName = (
                         SELECT FirstName
                         FROM   adLeads
                         WHERE  LeadId = @leadId
                         );
            SET @mName = (
                         SELECT MiddleName
                         FROM   adLeads
                         WHERE  LeadId = @leadId
                         );
            --IF ( @MyError = 0 )          
            --    BEGIN          
            --        INSERT  INTO arStudent          
            --                (          
            --                 StudentId          
            --                ,StudentStatus          
            --                ,LastName          
            --                ,SSN          
            --                ,MiddleName          
            --                ,FirstName          
            --                )          
            --        VALUES  (          
            --                 @studentId          
            --                ,@studentStatusId          
            --                ,@lName  -- StudentStatus - uniqueidentifier                     
            --                ,@ssn          
            --                ,@mName          
            --                ,@fName          
            --                );           
            --        SET @MyError = @@ERROR;              
            --        IF ( @MyError <> 0 )          
            --            BEGIN          
            --                SET @MyError = 1;          
            --                SET @Message = @Message + ''Failed to insert in arStudent'';          
            --            END;          
            --    END;          
            IF ( @MyError = 0 )
                BEGIN
                    --update the adleads table          
                    DECLARE @enrolLeadStatus UNIQUEIDENTIFIER;
                    SET @enrolLeadStatus = (
                                           SELECT   TOP 1 StatusCodeId
                                           FROM     syStatusCodes
                                           WHERE    SysStatusId = 6
                                                    AND StatusId = ''F23DE1E2-D90A-4720-B4C7-0F6FB09C9965''
                                           ORDER BY ModDate
                                           );
                    DECLARE @stuNum VARCHAR(50);
                    SET @stuNum = (
                                  SELECT StudentNumber
                                  FROM   adLeads
                                  WHERE  LeadId = @leadId
                                  );
                    IF ( @stuNum = '''' )
                        BEGIN
                            -- generate student number          
                            DECLARE @formatType INT
                                   ,@yrNo INT
                                   ,@monthNo INT
                                   ,@dateNo INT
                                   ,@lNameNo INT
                                   ,@fNameNo INT;
                            DECLARE @lNameStu VARCHAR(50)
                                   ,@fNameStu VARCHAR(50)
                                   ,@yr VARCHAR(10)
                                   ,@mon VARCHAR(10)
                                   ,@dt VARCHAR(10)
                                   ,@seqStartNo INT;
                            SELECT @formatType = CAST(FormatType AS INT)
                                  ,@yrNo = ISNULL(YearNumber, 0)
                                  ,@monthNo = ISNULL(MonthNumber, 0)
                                  ,@dateNo = ISNULL(DateNumber, 0)
                                  ,@lNameNo = ISNULL(LNameNumber, 0)
                                  ,@fNameNo = ISNULL(FNameNumber, 0)
                                  ,@seqStartNo = SeqStartingNumber
                            FROM   syStudentFormat;
                            IF ( @formatType = 1 )
                                BEGIN
                                    SET @stuNum = '''';
                                END;
                            ELSE IF ( @formatType = 3 )
                                     BEGIN
                                         SET @lNameStu = SUBSTRING(@lName, 1, ISNULL(@lNameNo, 0));
                                         SET @fNameStu = SUBSTRING(@fName, 1, ISNULL(@fNameNo, 0));
                                         SET @yr = '''';
                                         SET @yr = CASE @yrNo
                                                        WHEN 0 THEN ''''
                                                        WHEN 4 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 1, 4)
                                                        WHEN 3 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 2, 3)
                                                        WHEN 2 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 3, 2)
                                                        WHEN 1 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 4, 1)
                                                   END;
                                         SET @mon = SUBSTRING(CAST(MONTH(GETDATE()) AS VARCHAR(10)), 1, @monthNo);
                                         SET @dt = SUBSTRING(CAST(DAY(GETDATE()) AS VARCHAR(10)), 1, @dateNo);
                                         IF (
                                            LEN(@mon) = 1
                                            AND @monthNo = 2
                                            )
                                             BEGIN
                                                 SET @mon = ''0'' + @mon;
                                             END;
                                         IF (
                                            LEN(@dt) = 1
                                            AND @dateNo = 2
                                            )
                                             BEGIN
                                                 SET @dt = ''0'' + @dt;
                                             END;
                                         SET NOCOUNT ON;
                                         DECLARE @stuId INT;
                                         SET @stuId = (
                                                      SELECT MAX(Student_SeqId) AS Student_SeqID
                                                      FROM   syGenerateStudentFormatID
                                                      ) + 1;
                                         INSERT INTO syGenerateStudentFormatID (
                                                                               Student_SeqId
                                                                              ,ModDate
                                                                               )
                                         VALUES ( @stuId, GETDATE());

                                         IF ( @yr = '''' )
                                             BEGIN
                                                 SET @stuNum = @mon + @dt + @lNameStu + @fNameStu + CAST(@stuId AS VARCHAR(5));
                                             END;
                                         ELSE
                                             BEGIN
                                                 SET @stuNum = @yr + @mon + @dt + @lNameStu + @fNameStu + CAST(@stuId AS VARCHAR(5));
                                             END;
                                     --SELECT @stuNum          
                                     END;
                            ELSE IF ( @formatType = 4 )
                                     BEGIN
                                         SET @stuNum = @badgeNum;
                                     END;
                            ELSE
                                     BEGIN
                                         SET NOCOUNT ON;

                                         IF EXISTS (
                                                   SELECT *
                                                   FROM   syGenerateStudentSeq
                                                   )
                                             BEGIN
                                                 SET @stuId = (
                                                              SELECT MAX(Student_SeqId) AS Student_SeqID
                                                              FROM   syGenerateStudentSeq
                                                              ) + 1;
                                             END;
                                         ELSE
                                             BEGIN
                                                 SET @stuId = @seqStartNo;
                                             END;
                                         INSERT INTO syGenerateStudentSeq (
                                                                          Student_SeqId
                                                                         ,ModDate
                                                                          )
                                         VALUES ( @stuId, GETDATE());
                                         SET @stuNum = CAST(@stuId AS VARCHAR(10));
                                     END;

                        END;
                    UPDATE adLeads
                    SET    Gender = @gender
                          ,BirthDate = @dob                             --ISNULL(@dob,BirthDate)          
                          ,SSN = @ssn                                   --ISNULL(@ssn,SSN)          
                          ,DependencyTypeId = @depencytype              --ISNULL(@depencytype,DependencyTypeId)          
                          ,MaritalStatus = @maritalstatus               --ISNULL(@maritalstatus,MaritalStatus)          
                          ,FamilyIncome = @familyIncome
                          ,HousingId = @housingType
                          ,admincriteriaid = @adminCriteria
                          ,DegCertSeekingId = @degSeekCert
                          ,AttendTypeId = @attendType
                          ,Race = @race
                          ,Nationality = @nationality                   --ISNULL(@nationality,Nationality)          
                          ,Citizen = @citizenship
                          ,GeographicTypeId = @geographicType           --ISNULL(@geographicType,GeographicTypeId)          
                          ,IsDisabled = @disabled                       --ISNULL(@disabled,IsDisabled)          
                          ,IsFirstTimeInSchool = @firsttimeSchool       --ISNULL(@firsttimeSchool,IsFirstTimeInSchool)          
                          ,CampusId = @campus
                          ,IsFirstTimePostSecSchool = @firsttimePostSec --ISNULL(@firsttimePostSec,IsFirstTimePostSecSchool)          
                          ,PrgVerId = @prgVersion
                          ,ProgramID = @programId
                          ,AreaID = @areaId
                          ,ProgramScheduleId = @schedule                --ISNULL(@schedule,ProgramScheduleId)          
                          ,ShiftID = @shift                             --ISNULL(@shift,ShiftID)          
                          ,ExpectedStart = @expectedStartDt
                          ,EntranceInterviewDate = @entranceInterviewDt --ISNULL(@entranceInterviewDt,EntranceInterviewDate)          
                          ,StudentNumber = ISNULL(@stuNum, '''')          --ISNULL(@studentNumber,StudentNumber)          
                          ,StudentStatusId = ISNULL(@studentStatusId, StudentStatusId)
                          ,StudentId = ISNULL(@studentId, StudentId)
                          ,ModUser = ISNULL(@modUserN, ModUser)
                          ,ModDate = @transDate
                          ,LeadStatus = @enrolLeadStatus                --update to enrolled          
                          ,PreviousEducation = @educationLevel
                          ,EnrollStateId = @state
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + ''Failed to update in adleads'';
                        END;
                    SET @admissionRepId = (
                                          SELECT AdmissionsRep
                                          FROM   adLeads
                                          WHERE  LeadId = @leadId
                                          );
                END;

            IF ( @MyError = 0 )
                BEGIN
                    --insert into arStuEnrollments          
                    SET @stuEnrollId = NEWID();
                    INSERT INTO arStuEnrollments (
                                                 StuEnrollId
                                                ,StudentId
                                                ,EnrollDate
                                                ,PrgVerId
                                                ,StartDate
                                                ,ExpStartDate
                                                ,MidPtDate
                                                ,TransferDate
                                                ,ShiftId
                                                ,BillingMethodId
                                                ,CampusId
                                                ,StatusCodeId
                                                ,ModDate
                                                ,ModUser
                                                ,EnrollmentId
                                                ,AcademicAdvisor
                                                ,LeadId
                                                ,TuitionCategoryId
                                                ,EdLvlId
                                                ,FAAdvisorId
                                                ,attendtypeid
                                                ,degcertseekingid
                                                ,BadgeNumber
                                                ,ContractedGradDate
                                                ,TransferHours
                                                ,PrgVersionTypeId
                                                ,IsDisabled
                                                ,DisableAutoCharge
												,ThirdPartyContract
                                                ,IsFirstTimeInSchool
                                                ,IsFirstTimePostSecSchool
                                                ,EntranceInterviewDate
                                                ,AdmissionsRep
                                                ,ExpGradDate
                                                ,CohortStartDate
                                                 )
                    VALUES ( @stuEnrollId, @studentId             -- StudentId - uniqueidentifier          
                            ,@enrollDt                            -- EnrollDate - datetime          
                            ,@prgVersion                          -- PrgVerId - uniqueidentifier          
                            ,CONVERT(DATE, @startDt, 101)         -- StartDate - datetime          
                            ,CONVERT(DATE, @expectedStartDt, 101) -- ExpStartDate - datetime          
                            ,@midptDate                           -- MidPtDate - datetime                   
                            ,@transferDt                          -- TransferDate - datetime          
                            ,@shift                               -- ShiftId - uniqueidentifier          
                            ,@chargingMethod                      -- BillingMethodId - uniqueidentifier          
                            ,@campus                              -- CampusId - uniqueidentifier          
                            ,@studentEnrolStatus                  -- StatusCodeId - uniqueidentifier                   
                            ,@transDate                           -- ModDate - datetime          
                            ,@modUserN                            -- ModUser - varchar(50)          
                            ,@enrollId                            -- EnrollmentId - varchar(50)                   
                            ,@acedemicAdvisor                     -- AcademicAdvisor - uniqueidentifier          
                            ,@leadId                              -- LeadId - uniqueidentifier          
                            ,@tuitionCategory                     -- TuitionCategoryId - uniqueidentifier          
                            ,@educationLevel                      -- EdLvlId - uniqueidentifier          
                            ,@fAAvisor                            -- FAAdvisorId - uniqueidentifier          
                            ,@attendType                          -- attendtypeid - uniqueidentifier          
                            ,@degSeekCert                         -- degcertseekingid - uniqueidentifier          
                            ,@badgeNum                            -- BadgeNumber - varchar(50)          
                            ,@contractGrdDt                       -- ContractedGradDate - datetime          
                            ,@transferhr                          -- TransferHours - decimal          
                            ,@prgVerType                          -- PrgVersionTypeId - int          
                            ,@disabled                            -- IsDisabled - bit    
                            ,@disableAutoCharge                   -- DisableAutoCharge - bit    
							,@thirdPartyContract				  -- ThirdPartyContract - bit      
                            ,@firsttimeSchool                     -- IsFirstTimeInSchool - bit          
                            ,@firsttimePostSec                    -- IsFirstTimePostSecSchool - bit          
                            ,@entranceInterviewDt                 -- EntranceInterviewDate - datetime          
                            ,@admissionRepId                      -- AdmissionsRep uniqueidentifier          
                            ,@contractGrdDt                       -- ExpGradDate DateTime          
                            ,CONVERT(DATE, @expectedStartDt, 101) -- cohort start date                    
                        );


                    IF ((
                        SELECT TOP 1 RTRIM(LTRIM(FormatType))
                        FROM   dbo.syStudentFormat
                        ) = ''4''
                       )
                        BEGIN
                            DECLARE @badgeId VARCHAR(50) = (
                                                           SELECT TOP 1 BadgeNumber
                                                           FROM   dbo.arStuEnrollments
                                                           WHERE  StuEnrollId = @stuEnrollId
                                                           );

                            UPDATE adLeads
                            SET    StudentNumber = @badgeId
                            WHERE  LeadId = @leadId;
                        END;


                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + ''Failed to insert in arStuEnrollments'';
                        END;
                END;
            -- insert into arStudentSchedules if schedule is not null
            IF ( @MyError = 0 )
                BEGIN
                    IF @schedule IS NOT NULL
                        BEGIN
                            INSERT INTO arStudentSchedules (
                                                           StuScheduleId
                                                          ,StuEnrollId
                                                          ,ScheduleId
                                                          ,StartDate
                                                          ,EndDate
                                                          ,Active
                                                          ,ModUser
                                                          ,ModDate
                                                          ,Source
                                                           )
                            VALUES ( NEWID()      -- StuScheduleId - uniqueidentifier
                                    ,@stuEnrollId -- StuEnrollId - uniqueidentifier
                                    ,@schedule    -- ScheduleId - uniqueidentifier
                                    ,NULL         -- StartDate - smalldatetime
                                    ,NULL         -- EndDate - smalldatetime
                                    ,1            -- Active - bit
                                    ,@modUserN    -- ModUser - varchar(50)
                                    ,GETDATE()    -- ModDate - smalldatetime
                                    ,''attendance'' -- Source - varchar(50)
                                );
                        END;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + ''Failed to insert in arStuEnrollments'';
                        END;
                END;
            IF ( @MyError = 0 )
                BEGIN
                    IF @stuEnrollId IS NOT NULL
                        BEGIN
                            IF ( @MyError = 0 )
                                BEGIN
                                    --insert into syStudentStatusChanges          
                                    DECLARE @OriginalleadStatus UNIQUEIDENTIFIER;
                                    SET @OriginalleadStatus = (
                                                              SELECT LeadStatus
                                                              FROM   adLeads
                                                              WHERE  LeadId = @leadId
                                                              );
                                    INSERT INTO syStudentStatusChanges (
                                                                       StudentStatusChangeId
                                                                      ,StuEnrollId
                                                                      ,OrigStatusId
                                                                      ,NewStatusId
                                                                      ,CampusId
                                                                      ,ModDate
                                                                      ,ModUser
                                                                      ,IsReversal
                                                                      ,DropReasonId
                                                                      ,DateOfChange
                                                                      ,Lda
                                                                       )
                                    VALUES ( NEWID(), @stuEnrollId, @OriginalleadStatus, @studentEnrolStatus, @campus, @transDate, @modUserN, 0, NULL
                                            ,@enrollDt, NULL );
                                    SET @MyError = @@ERROR;
                                    IF ( @MyError <> 0 )
                                        BEGIN
                                            SET @MyError = 1;
                                            SET @Message = @Message + ''Failed to insert in syStudentStatusChanges'';
                                        END;
                                END;
                            --insert into plStudentDocs          
                            INSERT INTO plStudentDocs (
                                                      StudentId
                                                     ,DocumentId
                                                     ,DocStatusId
                                                     ,ReceiveDate
                                                     ,ScannedId
                                                     ,ModDate
                                                     ,ModUser
                                                     ,ModuleID
                                                      )
                                        SELECT @studentId
                                              ,DocumentId
                                              ,DocStatusId
                                              ,ReceiveDate
                                              ,1
                                              ,@transDate
                                              ,@modUserN
                                              ,(
                                               SELECT DISTINCT ModuleId
                                               FROM   adReqs
                                               WHERE  adReqId = adLeadDocsReceived.DocumentId
                                               )
                                        FROM   adLeadDocsReceived
                                        WHERE  LeadId = @leadId;
                        END;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + ''Failed to insert in plStudentDocs'';
                        END;
                END;
            IF ( @MyError = 0 )
                BEGIN
                    --update syDocumentHistory          
                    UPDATE syDocumentHistory
                    SET    StudentId = @studentId
                          ,ModUser = @modUserN
                          ,ModDate = @transDate
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + ''Failed to update in syDocumentHistory'';
                        END;
                END;
            --update adLeadByLeadGroups          
            IF ( @MyError = 0 )
                BEGIN
                    UPDATE adLeadByLeadGroups
                    SET    StuEnrollId = @stuEnrollId
                          ,ModUser = @modUserN
                          ,ModDate = @transDate
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + ''Failed to update in adLeadByLeadGroups'';
                        END;

                END;
            --insert in adLeadEntranceTest          
            IF ( @MyError = 0 )
                BEGIN

                    UPDATE adLeadEntranceTest
                    SET    StudentId = @studentId
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + ''Failed to update in adLeadEntranceTest'';

                        END;
                END;
            --copy all the lead requirements adEntrTestOverRide          
            IF ( @MyError = 0 )
                BEGIN
                    --documents          
                    --                   INSERT  INTO adEntrTestOverRide        
                    --                           (        
                    --                            EntrTestOverRideId        
                    --                           ,LeadId        
                    --                           ,EntrTestId        
                    --                           ,ModUser        
                    --                           ,ModDate        
                    --                           ,OverRide        
                    --                           ,StudentId          
                    --                           )        
                    --                           SELECT  NEWID()        
                    --                                  ,@leadId        
                    --                                  ,DocumentId        
                    --,ModUser        
                    --                                  ,@transDate        
                    --                                  ,Override        
                    --                                  ,@studentId        
                    --                           FROM    adLeadDocsReceived        
                    --                           WHERE   LeadId = @leadId;          
                    --    --test          
                    --                   INSERT  INTO adEntrTestOverRide        
                    --                           (        
                    --                            EntrTestOverRideId        
                    --                           ,LeadId        
                    --                           ,EntrTestId        
                    --                           ,ModUser        
                    --                           ,ModDate        
                    --                           ,OverRide        
                    --                           ,StudentId          
                    --                           )        
                    --                           SELECT  NEWID()        
                    --                                  ,@leadId        
                    --                                  ,EntrTestId        
                    --                                  ,ModUser        
                    --                                  ,@transDate        
                    --                                  ,OverRide        
                    --                                  ,@studentId        
                    --                           FROM    adLeadEntranceTest        
                    --                           WHERE   LeadId = @leadId;          
                    --    --tours          
                    --                   INSERT  INTO adEntrTestOverRide        
                    --                           (        
                    --                            EntrTestOverRideId        
                    --                           ,LeadId        
                    --                           ,EntrTestId        
                    --                           ,ModUser        
                    --                           ,ModDate        
                    --                           ,OverRide        
                    --                           ,StudentId          
                    --                           )        
                    --                           SELECT  NEWID()        
                    --                                  ,@leadId        
                    --                                  ,DocumentId        
                    --                                  ,ModUser        
                    --                                  ,@transDate        
                    --                                  ,Override        
                    --                                  ,@studentId        
                    --                           FROM    AdLeadReqsReceived        
                    --                           WHERE   LeadId = @leadId;          
                    --    --fee          
                    --                   INSERT  INTO adEntrTestOverRide        
                    --                           (        
                    --                            EntrTestOverRideId        
                    --                           ,LeadId        
                    --                           ,EntrTestId        
                    --                           ,ModUser        
                    --                           ,ModDate        
                    --                           ,OverRide        
                    --                           ,StudentId          
                    --                           )        
                    --                           SELECT  NEWID()        
                    --                                  ,@leadId        
                    --                                  ,DocumentId        
                    --                                  ,ModUser        
                    --                                  ,@transDate        
                    --                                  ,Override        
                    --                                  ,@studentId        
                    --                           FROM    adLeadTranReceived        
                    --                           WHERE   LeadId = @leadId;          
                    UPDATE adEntrTestOverRide
                    SET    StudentId = @studentId
                    WHERE  LeadId = @leadId;

                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + ''Failed to update in adEntrTestOverRide'';
                        END;
                END;
            --exec USP_AD_CopyLeadTransactions          
            IF ( @MyError = 0 )
                BEGIN
                    DECLARE @rc INT;
                    EXECUTE @rc = dbo.USP_AD_CopyLeadTransactions @leadId
                                                                 ,@stuEnrollId
                                                                 ,@campus;
                    SET @MyError = @@ERROR;
                    IF (
                       @MyError <> 0
                       AND @rc = 0
                       )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + ''Failed to execute USP_AD_CopyLeadTransactions'';
                        END;
                END;
            --exec fees related SP          
            SET @acCalendarID = (
                                SELECT COUNT(P.ACId)
                                FROM   arPrograms P
                                      ,arPrgVersions PV
                                      ,arStuEnrollments SE
                                WHERE  PV.PrgVerId = SE.PrgVerId
                                       AND SE.StuEnrollId = @stuEnrollId
                                       AND P.ProgId = PV.ProgId
                                       AND ACId = 5
                                );
            --IF @acCalendarID >= 1          
            --    BEGIN          
            --apply fees by progver for clock hr programs          
            INSERT INTO saTransactions (
                                       TransactionId
                                      ,StuEnrollId
                                      ,TermId
                                      ,CampusId
                                      ,TransDate
                                      ,TransCodeId
                                      ,TransReference
                                      ,TransDescrip
                                      ,TransAmount
                                      ,FeeLevelId
                                      ,AcademicYearId
                                      ,TransTypeId
                                      ,IsPosted
                                      ,FeeId
                                      ,CreateDate
                                      ,IsAutomatic
                                      ,ModUser
                                      ,ModDate
                                       )
                        SELECT     NEWID()
                                  ,@stuEnrollId
                                  ,NULL
                                  ,@campus
                                  ,SE.ExpStartDate
                                  ,PVF.TransCodeId
                                  ,PV.PrgVerDescrip AS TransDescrip
                                  ,ST.TransCodeDescrip + ''-'' + PV.PrgVerDescrip AS TransCodeDescrip
                                  ,CASE PVF.UnitId
                                        WHEN 0 THEN PVF.Amount * PV.Credits
                                        WHEN 1 THEN PVF.Amount * PV.Hours
                                        WHEN 2 THEN PVF.Amount
                                   END AS TransAmount
                                  ,2 -- program version fees          
                                  ,NULL
                                  ,0
                                  ,1
                                  ,PVF.PrgVerFeeId
                                  ,@transDate
                                  ,0
                                  ,@modUserN
                                  ,@transDate
                        FROM       saProgramVersionFees AS PVF
                        INNER JOIN arPrgVersions PV ON PV.PrgVerId = PVF.PrgVerId
                        INNER JOIN arStuEnrollments SE ON SE.PrgVerId = PVF.PrgVerId
                        INNER JOIN saTransCodes AS ST ON ST.TransCodeId = PVF.TransCodeId
                        WHERE      SE.StuEnrollId = @stuEnrollId
                                   AND PVF.TuitionCategoryId = SE.TuitionCategoryId
                                   AND RateScheduleId IS NULL;

            --charge Program Fees if applicable          
            IF ( @MyError = 0 )
                BEGIN
                    DECLARE @Result INT;
                    DECLARE @currdt DATETIME;
                    SET @currdt = @transDate;
                    EXEC USP_ApplyFeesByProgramVersion @stuEnrollId    -- uniqueidentifier          
                                                      ,@modUserN       -- varchar(100)          
                                                      ,@campus         -- uniqueidentifier          
                                                      ,@currdt         -- datetime          
                                                      ,@Result OUTPUT; -- int          
                    SET @MyError = @@ERROR;
                END;
            IF ( @MyError = 0 )
                BEGIN
                    IF ( @prgVersion IS NOT NULL )
                        BEGIN
                            EXEC dbo.USP_REGISTER_STUDENT_AUTOMATICALLY @prgVersion
                                                                       ,@stuEnrollId
                                                                       ,@modUserN;
                        END;
                    SET @MyError = @@ERROR;
                END;

            IF (
               @MyError = 0
               AND @Result = 1
               )
                BEGIN
                    COMMIT TRANSACTION;
                    SET @Message = ''Successful COMMIT TRANSACTION''; -- do not change this message. It is used for verification in C# code.          
                    SELECT @Message;
                END;
            ELSE
                BEGIN
                    ROLLBACK TRANSACTION;
                    SET @Message = @Message + ''rollback failed transaction'';
                    SELECT @Message;
                END;
        END TRY
        BEGIN CATCH
            ROLLBACK TRANSACTION;
            SET @Message = @Message + ''Failed ROLLBACK TRANSACTION  Catching error'';
            --SELECT  ERROR_NUMBER() AS ErrorNumber          
            --       ,ERROR_SEVERITY() AS ErrorSeverity          
            --       ,ERROR_STATE() AS ErrorState          
            --       ,ERROR_PROCEDURE() AS ErrorProcedure          
            --       ,ERROR_LINE() AS ErrorLine          
            --       ,ERROR_MESSAGE() AS ErrorMessage;             

            SET @Message = ERROR_MESSAGE() + @Message + ''try failed transaction'';
            SELECT @Message;
        END CATCH;
    END;
-- =========================================================================================================          
-- END  --  USP_EnrollLead          
-- =========================================================================================================          

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_FA_PostZerosForStudents]'
GO
IF OBJECT_ID(N'[dbo].[USP_FA_PostZerosForStudents]', 'P') IS NULL
EXEC sp_executesql N'-- =============================================
-- Author:		FAME Inc.
-- Create date: 5/23/2019
-- Description:	For arStudentClockAttendanceSchools posting zeros. Given enroll id list and record date, posts zeros for students on this date. Will post zero''s for all students in @StuEnrollIdList
--				wihout checking students in school status. Filtering of students should be down prior to the execution of this SP.	   				
-- =============================================
CREATE   PROCEDURE [dbo].[USP_FA_PostZerosForStudents]
    @StuEnrollIdList VARCHAR(MAX) = NULL
   ,@Date DATETIME
   ,@ModUser VARCHAR(50)
AS
    BEGIN
        DECLARE @ModDate DATETIME = GETDATE();

        --update placeholder records if they exist for given student enrollments on given day
        UPDATE dbo.arStudentClockAttendance
              SET	ActualHours = 0
                  ,SchedHours = a.CalculatedScheduledHours
                  ,ModDate = @ModDate
                  ,ModUser = @ModUser
                  ,isTardy = 0
                  ,PostByException = ''no''
                  ,comments = NULL
                  ,TardyProcessed = 0
                  ,Converted = 0
              FROM   dbo.arStudentClockAttendance
              JOIN   dbo.StudentScheduledHours(@StuEnrollIdList, @Date) a ON a.StuEnrollId = arStudentClockAttendance.StuEnrollId
              WHERE  CAST(RecordDate AS DATE) = CAST(@Date AS DATE)
                     AND ActualHours IN(99.0, 999.0, 9999.0) AND arStudentClockAttendance.StuEnrollId IN (SELECT Val
                                   FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)); 

        --create records if they do not exist for given student enrollments on given day 
        INSERT INTO dbo.arStudentClockAttendance
                    SELECT StuEnrollList.StuEnrollId  -- StuEnrollId - uniqueidentifier
                          ,a.ScheduleId               -- ScheduleId - uniqueidentifier
                          ,CAST(@Date AS DATE)		  -- RecordDate - smalldatetime
                          ,a.CalculatedScheduledHours -- SchedHours - decimal(18, 2)
                          ,0                          -- ActualHours - decimal(18, 2)
                          ,@ModDate                   -- ModDate - smalldatetime
                          ,@ModUser                   -- ModUser - varchar(50)
                          ,0                          -- isTardy - bit
                          ,''no''                       -- PostByException - varchar(10)
                          ,NULL                       -- comments - varchar(240)
                          ,0                          -- TardyProcessed - bit
                          ,0                          -- Converted - bit
                    FROM   (
                           SELECT CAST(Val AS UNIQUEIDENTIFIER) AS StuEnrollId
                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                           ) StuEnrollList
                    JOIN   dbo.StudentScheduledHours(@StuEnrollIdList, @Date) a ON a.StuEnrollId = StuEnrollList.StuEnrollId
                    WHERE  NOT EXISTS (
                                      SELECT *
                                      FROM   dbo.arStudentClockAttendance
                                      WHERE  StuEnrollId = StuEnrollList.StuEnrollId
                                             AND CAST(RecordDate AS DATE) = CAST(@Date AS DATE)
                                      );
    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_FASAPGenerateNotice]'
GO
IF OBJECT_ID(N'[dbo].[USP_FASAPGenerateNotice]', 'P') IS NULL
EXEC sp_executesql N'

CREATE PROCEDURE [dbo].[USP_FASAPGenerateNotice]
    (
        @EnrollmentId VARCHAR(MAX) = NULL
    )
AS
    BEGIN
        DECLARE @ActiveStatusId UNIQUEIDENTIFIER;
        SET @ActiveStatusId = (
                              SELECT TOP 1 StatusId
                              FROM   dbo.syStatuses
                              WHERE  UPPER(StatusCode) = ''A''
                              );
        DECLARE @SchoolType VARCHAR(20);
        SET @SchoolType = LOWER((
                                SELECT TOP 1 Value
                                FROM   dbo.syConfigAppSetValues
                                WHERE  SettingId = 47
                                )
                               );
        DECLARE @enrollment TABLE
            (
                rNum INT IDENTITY(1, 1)
               ,stuEnrollId UNIQUEIDENTIFIER
               ,completedHours DECIMAL(18, 2) NULL
               ,ScheduleHrs DECIMAL(18, 2) NULL
            );
        INSERT INTO @enrollment (
                                stuEnrollId
                                )
                    SELECT Val
                    FROM   MultipleValuesForReportParameters(@EnrollmentId, '','', 1);

        DECLARE @total INT;
        DECLARE @attedType VARCHAR(50);
        DECLARE @enrollId UNIQUEIDENTIFIER;
        SET @total = (
                     SELECT COUNT(*)
                     FROM   @enrollment
                     );
        DECLARE @i INT = 1;
        DECLARE @attenUnitTypeId UNIQUEIDENTIFIER;
        DECLARE @SchedulecompletedHrs TABLE
            (
                schRId INT IDENTITY(1, 1)
               ,TotalHoursAbsent DECIMAL(18, 2)
               ,TotalPresentHours DECIMAL(18, 2)
               ,SchedHours DECIMAL(18, 2)
               ,TotalMakeupHours DECIMAL(18, 2)
               ,ScheduleHoursByWeek DECIMAL(18, 2)
               ,LastDateAttended DATETIME
               ,TardyCount INT
            );
        --WHILE ( @i <= @total )
        --    BEGIN
        --        SET @attenUnitTypeId = (
        --                               SELECT     arAttUnitType.UnitTypeId
        --                               FROM       dbo.arStuEnrollments
        --                               INNER JOIN dbo.arPrgVersions ON arPrgVersions.PrgVerId = arStuEnrollments.PrgVerId
        --                               INNER JOIN dbo.arAttUnitType ON arAttUnitType.UnitTypeId = arPrgVersions.UnitTypeId
        --                               INNER JOIN @enrollment ON [@enrollment].stuEnrollId = arStuEnrollments.StuEnrollId
        --                               WHERE      rNum = @i
        --                               );
        --        SET @attedType = CASE @attenUnitTypeId
        --                              WHEN ''EF5535C2-142C-4223-AE3C-25A50A153CC6'' THEN ''pa''
        --                              WHEN ''A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'' THEN ''minutes''
        --                              WHEN ''B937C92E-FD7A-455E-A731-527A9918C734'' THEN ''hrs''
        --                              ELSE ''''
        --                         END;
        --        SET @enrollId = (
        --                        SELECT stuEnrollId
        --                        FROM   @enrollment
        --                        WHERE  rNum = @i
        --                        );
        --        INSERT INTO @SchedulecompletedHrs
        --        EXEC USP_SchedAndCompletedHours_ByDay @enrollId
        --                                             ,@attedType;
        --        UPDATE @enrollment
        --        SET    completedHours = (
        --                                SELECT TotalPresentHours
        --                                FROM   @SchedulecompletedHrs
        --                                WHERE  schRId = @i
        --                                )
        --              ,ScheduleHrs = (
        --                             SELECT SchedHours
        --                             FROM   @SchedulecompletedHrs
        --                             WHERE  schRId = @i
        --                             )
        --        WHERE  rNum = @i;
        --        SET @i = @i + 1;
        --    END;



        SELECT     TOP 1 R.StdRecKey AS FASAPKey
                        ,E.StuEnrollId AS EnrollmentId
                        ,S.FirstName
                        ,S.MiddleName
                        ,S.LastName
                        ,S.StudentNumber
                        ,P.ProgDescrip AS ProgramDescription
                        ,PV.PrgVerDescrip AS ProgramVersionDescription
                        ,E.StartDate
                        ,R.Period AS Increment
                        ,TUT.TrigUnitTypDescrip AS UnitType
                        ,TOST.TrigOffTypDescrip AS TriggerOffsetType
                        ,R.CheckPointDate AS SAPCheckDate
                        ,CASE WHEN @SchoolType = ''letter'' THEN R.GPA
                              WHEN @SchoolType = ''numeric'' THEN R.Average
                              ELSE ''School is not numeric or letter graded''
                         END AS QualitativeResults
                        ,QMT.QualMinTypDesc AS MinimumQualitativeLabel
                        ,SD.QualMinValue AS MinimumGrade
                        ,R.PercentCompleted AS QuantitativeResult
                        ,QMUT.QuantMinTypDesc AS MinimumQuantityUnitLabel
                        ,SD.QuantMinValue AS MinimumAttendance
                        ,REPLACE(REPLACE(   CASE WHEN CV.Message IS NOT NULL THEN CV.Message
                                                 ELSE TIVSS.DefaultMessage
                                            END
                                           ,''<<Minimum quantity>>''
                                           ,SD.QuantMinValue
                                        )
                                ,''<<MinimumGPA>>''
                                ,SD.QualMinValue
                                ) AS FASAPMessage
                        ,C.CampDescrip CampusDescription
                        ,CAST(CAST(TrigValue AS DECIMAL) / 100 AS DECIMAL(8, 2)) AS TriggerValue
                        ,TIVSS.Code AS TitleIVStatus
                        ,r.HrsEarned AS CompletedHours
                        ,r.HrsAttended AS ScheduledHours
        FROM       dbo.arFASAPChkResults R
        JOIN       dbo.arStuEnrollments E ON E.StuEnrollId = R.StuEnrollId
        JOIN       dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
        JOIN       dbo.arPrograms P ON P.ProgId = PV.ProgId
        JOIN       dbo.arStudent S ON E.StudentId = S.StudentId
        JOIN       dbo.arSAPDetails SD ON SD.SAPDetailId = R.SAPDetailId
        JOIN       dbo.arQuantMinUnitTyps QMUT ON QMUT.QuantMinUnitTypId = SD.QuantMinUnitTypId
        JOIN       dbo.arQualMinTyps QMT ON QMT.QualMinTypId = SD.QualMinTypId
        JOIN       dbo.arTrigUnitTyps TUT ON TUT.TrigUnitTypId = SD.TrigUnitTypId
        JOIN       dbo.arTrigOffsetTyps TOST ON TOST.TrigOffsetTypId = SD.TrigOffsetTypId
        JOIN       dbo.syTitleIVSapStatus TIVSS ON TIVSS.Id = R.TitleIVStatusId
        JOIN       dbo.syCampuses C ON C.CampusId = E.CampusId
        LEFT JOIN  dbo.syTitleIVSapCustomVerbiage CV ON CV.SapId = SD.SAPId
                                                        AND CV.TitleIVSapStatusId = R.TitleIVStatusId
                                                        AND CV.StatusId = @ActiveStatusId
        INNER JOIN @enrollment ET ON ET.stuEnrollId = E.StuEnrollId
        WHERE      R.StatusId = @ActiveStatusId
                   AND @EnrollmentId IS NULL
                   OR ( E.StuEnrollId IN (
                                         SELECT Val
                                         FROM   MultipleValuesForReportParameters(@EnrollmentId, '','', 1)
                                         )
                      )
        ORDER BY   Period DESC
                  ,SAPCheckDate DESC;

    END;

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_Invoice_GetStudentInfo]'
GO
IF OBJECT_ID(N'[dbo].[USP_Invoice_GetStudentInfo]', 'P') IS NULL
EXEC sp_executesql N'-- =============================================
-- Author:		Edwin Sosa
-- Create date: 11/19/2018
-- Description:	Procedure for getting invoice info per student.
-- =============================================
CREATE PROCEDURE [dbo].[USP_Invoice_GetStudentInfo]
    -- Add the parameters for the stored procedure here
    @campusId UNIQUEIDENTIFIER
   ,@stuEnrollIdList VARCHAR(MAX)
   ,@refDate DATETIME
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        SELECT   payPlansAmounts.Address
                ,payPlansAmounts.City
                ,payPlansAmounts.Zip
                ,payPlansAmounts.StateDescrip
                ,payPlansAmounts.CountryDescrip
                ,SUM(PlanAmount) - SUM(payPlansAmounts.AmountPaid) AS OutstandingBalance
                ,SUM(payPlansAmounts.PlanAmount) AS OriginalTotal
                ,payPlansAmounts.StuEnrollId
                ,payPlansAmounts.SSN
                ,payPlansAmounts.StudentNumber
                ,payPlansAmounts.LastName
                ,payPlansAmounts.FirstName
                ,payPlansAmounts.MiddleName
                ,payPlansAmounts.Description
                ,payPlansAmounts.PaymentPlanId
				,payPlansAmounts.CampusName
				,payPlansAmounts.StartDate
        FROM     (
                 SELECT     ASA2.Address
                           ,ASA2.City
                           ,ASA2.Zip
                           ,ASA2.StateDescrip
                           ,ASA2.CountryDescrip
                           ,ISNULL(FSPPS.Amount, 0) AS PlanAmount
                           ,ISNULL(SPDR2.AmountPaid, 0) AS AmountPaid
                           ,ASE.StuEnrollId
                           ,AST.SSN
                           ,AST.StudentNumber
                           ,AST.LastName
                           ,AST.FirstName
                           ,AST.MiddleName
                           ,FSPP.PayPlanDescrip AS Description
                           ,FSPP.PaymentPlanId
						   ,camp.CampDescrip AS CampusName
						   ,ASE.StartDate
                 FROM       faStudentPaymentPlans AS FSPP
                 INNER JOIN faStuPaymentPlanSchedule AS FSPPS ON FSPPS.PaymentPlanId = FSPP.PaymentPlanId
                 INNER JOIN arStuEnrollments AS ASE ON ASE.StuEnrollId = FSPP.StuEnrollId
				 JOIN dbo.syCampuses camp ON camp.CampusId = ASE.CampusId
                 INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId
                 INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                 INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                 LEFT JOIN  (
                            SELECT CASE WHEN amountsPaid.AmountPaid < 0 THEN amountsPaid.AmountPaid * -1
                                        ELSE amountsPaid.AmountPaid
                                   END AS AmountPaid
                                  ,amountsPaid.PayPlanScheduleId
                            FROM   (
                                   SELECT     SUM(ISNULL(SPDR.Amount, 0)) AS AmountPaid
                                             ,SPDR.PayPlanScheduleId
                                   FROM       saPmtDisbRel AS SPDR
                                   INNER JOIN saTransactions AS ST ON ST.TransactionId = SPDR.TransactionId
                                   INNER JOIN faStuPaymentPlanSchedule AS FSPPS2 ON FSPPS2.PayPlanScheduleId = SPDR.PayPlanScheduleId
                                   WHERE      ST.Voided = 0
                                   GROUP BY   SPDR.PayPlanScheduleId
                                   ) AS amountsPaid
                            ) AS SPDR2 ON SPDR2.PayPlanScheduleId = FSPPS.PayPlanScheduleId
                 LEFT JOIN  (
                            SELECT T.StudentId
                                  ,T.StdAddressId
                                  ,T.Address
                                  ,T.City
                                  ,T.Zip
                                  ,T.StateDescrip
                                  ,T.CountryDescrip
                            FROM   (
                                   SELECT     ASA.StudentId
                                             ,ASA.StdAddressId
                                             ,( Address1 + '' '' + ISNULL(Address2, '''')) AS Address
                                             ,ASA.City
                                             ,ASA.Zip
                                             ,ISNULL(adc.CountryDescrip, '''') AS CountryDescrip
                                             ,ISNULL(SS2.StateDescrip, '''') AS StateDescrip
                                             ,ROW_NUMBER() OVER ( PARTITION BY ASA.StudentId
                                                                  ORDER BY ASA.default1 DESC
                                                                ) AS RowNumber
                                   FROM       arStudAddresses AS ASA
                                   INNER JOIN syStatuses AS SS ON SS.StatusId = ASA.StatusId
                                   LEFT JOIN  syStates AS SS2 ON SS2.StateId = ASA.StateId
                                   LEFT JOIN  dbo.adCountries adc ON adc.CountryId = SS2.CountryId
                                   WHERE      SS.StatusCode = ''A''
                                   ) AS T
                            WHERE  T.RowNumber = 1
                            ) AS ASA2 ON ASA2.StudentId = AST.StudentId
                 WHERE      FSPPS.ExpectedDate <= @refDate -- RefDate  
                            AND ASE.CampusId = @campusId -- CampusId 
                            AND @stuEnrollIdList IS NULL
                            OR ( ASE.StuEnrollId IN (
                                                    SELECT Val
                                                    FROM   MultipleValuesForReportParameters(@stuEnrollIdList, '','', 1)
                                                    )
                               )
                 ) payPlansAmounts
        GROUP BY payPlansAmounts.Address
                ,payPlansAmounts.City
                ,payPlansAmounts.Zip
                ,payPlansAmounts.StateDescrip
                ,payPlansAmounts.CountryDescrip
                ,payPlansAmounts.StuEnrollId
                ,payPlansAmounts.SSN
                ,payPlansAmounts.StudentNumber
                ,payPlansAmounts.LastName
                ,payPlansAmounts.FirstName
                ,payPlansAmounts.MiddleName
                ,payPlansAmounts.Description
                ,payPlansAmounts.PaymentPlanId
				,payPlansAmounts.CampusName
				,payPlansAmounts.StartDate
        ORDER BY payPlansAmounts.StuEnrollId;

    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_ManageSecurity_Reports_Permissions]'
GO
IF OBJECT_ID(N'[dbo].[USP_ManageSecurity_Reports_Permissions]', 'P') IS NULL
EXEC sp_executesql N'
-- Add Page to export pdf/excel         
CREATE PROCEDURE [dbo].[USP_ManageSecurity_Reports_Permissions]  
    @RoleId VARCHAR(50)  
   ,@ModuleResourceId INT  
   ,@SchoolEnumerator INT  
   ,@CampusId UNIQUEIDENTIFIER  
AS --SET @RoleId=''BB0E90D6-4D77-4362-95FA-B2D34260A4A1''         
    --SET @SchoolEnumerator=348         
    --SET @CampusId=''5ebf6c1f-1fb1-492d-ad53-348f3986a178''         
  
    -- declare local variables           
    DECLARE @ShowRossOnlyTabs BIT  
           ,@SchedulingMethod VARCHAR(50)  
           ,@TrackSAPAttendance VARCHAR(50);  
    DECLARE @ShowCollegeOfCourtReporting VARCHAR(5)  
           ,@FameESP VARCHAR(5)  
           ,@EdExpress VARCHAR(5);  
    DECLARE @GradeBookWeightingLevel VARCHAR(20)  
           ,@ShowExternshipTabs VARCHAR(5);  
    DECLARE @showNaccasReports VARCHAR(5);  
    DECLARE @ShowStateBoardReports VARCHAR(5);  
 DECLARE @ShouldIncludeNaccas BIT;  
 DECLARE @ShouldIncludeState BIT;  
 --Declaring a variable table that will be used to store data about what header pages should be shown.   
 --ResID will hold ResourceID’s   
    --Header will be set to 1 if the ResourceID is a header  
    --ShouldInclude will be set to 1 if it should be shown   
  
    DECLARE @InTbl TABLE  
        (  
            ResId INT  
           ,Header BIT  
           ,ShouldInclude BIT  
        );  
  
    -- Get Values        
  
    IF  (  
        SELECT COUNT(*)  
        FROM   syConfigAppSetValues cv  
        INNER JOIN syConfigAppSettings cs ON cs.SettingId = cv.SettingId  
        WHERE  KeyName = ''ShowNACCASReports''  
               AND CampusId = @CampusId  
               AND Active = 1  
        ) >= 1  
        BEGIN  
            SET @showNaccasReports = (  
                                     SELECT Value  
                                     FROM   syConfigAppSetValues cv  
                                     INNER JOIN syConfigAppSettings cs ON cs.SettingId = cv.SettingId  
                                     WHERE  KeyName = ''ShowNACCASReports''  
                                            AND CampusId = @CampusId  
                                            AND Active = 1  
                                     );  
        END;  
    ELSE  
        BEGIN  
            SET @showNaccasReports = (  
                                     SELECT Value  
                                     FROM   syConfigAppSetValues cv  
                                     INNER JOIN syConfigAppSettings cs ON cs.SettingId = cv.SettingId  
                                     WHERE  KeyName = ''ShowNACCASReports''  
                                            AND CampusId IS NULL  
                                            AND Active = 1  
                                     );  
        END;  
    IF EXISTS (  
              SELECT 1  
              FROM   syConfigAppSetValues cv  
              INNER JOIN syConfigAppSettings cs ON cs.SettingId = cv.SettingId  
              WHERE  KeyName = ''ShowStateBoardAccreditationAgencyReports''  
                     AND Active = 1  
              )  
        BEGIN  
            SET @ShowStateBoardReports = (  
                                         SELECT Value  
                                         FROM   syConfigAppSetValues cv  
                                         INNER JOIN syConfigAppSettings cs ON cs.SettingId = cv.SettingId  
                                         WHERE  KeyName = ''ShowStateBoardAccreditationAgencyReports''  
                                                AND Active = 1  
                                         );  
        END;  
    ELSE  
        BEGIN  
            SET @ShowStateBoardReports = ''no'';  
        END;  
  
  
  
    --Inserts for headers and pages that should always be shown   
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 691 -- ResId - int  
            ,0   -- Header - bit  
            ,1   --ShouldInclude  -bit  
           );  
    INSERT INTO @InTbl (  
                       ResId  
      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 690 -- ResId - int  
            ,0   -- Header - bit  
            ,1   --ShouldInclude  -bit  
           );  
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 692 -- ResId - int  
            ,0   -- Header - bit  
            ,1   --ShouldInclude  -bit  
           );  
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 727 -- ResId - int  
            ,0   -- Header - bit  
            ,1   --ShouldInclude  -bit  
           );  
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 729 -- ResId - int  
            ,0   -- Header - bit  
            ,1   --ShouldInclude  -bit  
           );  
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 736 -- ResId - int  
            ,0   -- Header - bit  
            ,1   --ShouldInclude  -bit  
           );  
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 730 -- ResId - int  
            ,0   -- Header - bit  
            ,1   --ShouldInclude  -bit  
           );  
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 678 -- ResId - int  
            ,0   -- Header - bit  
            ,1   --ShouldInclude  -bit  
           );  
  
      
    IF ( @showNaccasReports = ''yes'' )  
        BEGIN  
            SET @ShouldIncludeNaccas = 1;  
        END;  
    ELSE  
        BEGIN  
            SET @ShouldIncludeNaccas = 0;  
        END;  
		
    --Inserts with value of ShouldInclude based on showNaccasReports  
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 845                  -- ResId - int  
            ,0                    -- Header - bit  
            ,@ShouldIncludeNaccas --ShouldInclude  -bit  
           );  
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 846                  -- ResId - int  
            ,0                    -- Header - bit  
            ,@ShouldIncludeNaccas --ShouldInclude  -bit  
           );      
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 844                  -- ResId - int  
            ,1                    -- Header - bit  
            ,@ShouldIncludeNaccas --ShouldInclude  -bit  
           );  
  
     
    IF ( @ShowStateBoardReports = ''yes'' )  
        BEGIN  
            SET @ShouldIncludeState = 1;  
        END;  
    ELSE  
        BEGIN  
            SET @ShouldIncludeState = 0;  
        END;  
 --Inserts with value of ShouldInclude based on ShowStateBoardReports  
    INSERT INTO @InTbl (  
                       ResId  
                      ,Header  
                      ,ShouldInclude  
                       )  
    VALUES ( 864                 -- ResId - int  
            ,1                   -- Header - bit  
            ,@ShouldIncludeState --ShouldInclude  -bit  
           );  
  
     
    IF  (  
        SELECT COUNT(*)  
        FROM   syConfigAppSetValues  
        WHERE  SettingId = 68  
               AND CampusId = @CampusId  
        ) >= 1  
        BEGIN  
            SET @ShowRossOnlyTabs = (  
                                    SELECT Value  
                                    FROM   dbo.syConfigAppSetValues  
                                    WHERE  SettingId = 68  
                                           AND CampusId = @CampusId  
                                    );  
  
        END;  
    ELSE  
        BEGIN  
            SET @ShowRossOnlyTabs = (  
                                    SELECT Value  
                                    FROM   dbo.syConfigAppSetValues  
                                    WHERE  SettingId = 68  
                                           AND CampusId IS NULL  
                                    );  
        END;  
  
  
  
    IF  (  
        SELECT COUNT(*)  
        FROM   syConfigAppSetValues  
        WHERE  SettingId = 65  
               AND CampusId = @CampusId  
        ) >= 1  
        BEGIN  
            SET @SchedulingMethod = (  
                                    SELECT Value  
                                    FROM   dbo.syConfigAppSetValues  
                                    WHERE  SettingId = 65  
                                           AND CampusId = @CampusId  
                                    );  
  
        END;  
    ELSE  
        BEGIN  
            SET @SchedulingMethod = (  
                                    SELECT Value  
                                    FROM   dbo.syConfigAppSetValues  
                                    WHERE  SettingId = 65  
                                           AND CampusId IS NULL  
                                    );  
        END;  
  
  
  
    IF  (  
        SELECT COUNT(*)  
        FROM   syConfigAppSetValues  
        WHERE  SettingId = 72  
               AND CampusId = @CampusId  
        ) >= 1  
        BEGIN  
            SET @TrackSAPAttendance = (  
                                      SELECT Value  
                                      FROM   dbo.syConfigAppSetValues  
                                      WHERE  SettingId = 72  
                                             AND CampusId = @CampusId  
                                      );  
  
        END;  
    ELSE  
        BEGIN  
            SET @TrackSAPAttendance = (  
                                      SELECT Value  
                                      FROM   dbo.syConfigAppSetValues  
                                      WHERE  SettingId = 72  
                                             AND CampusId IS NULL  
                                      );  
        END;  
  
  
    IF  (  
        SELECT COUNT(*)  
        FROM   syConfigAppSetValues  
        WHERE  SettingId = 118  
               AND CampusId = @CampusId  
        ) >= 1  
        BEGIN  
            SET @ShowCollegeOfCourtReporting = (  
                                               SELECT Value  
                                               FROM   dbo.syConfigAppSetValues  
                                               WHERE  SettingId = 118  
                                                      AND CampusId = @CampusId  
                                               );  
  
        END;  
    ELSE  
        BEGIN  
            SET @ShowCollegeOfCourtReporting = (  
                                               SELECT Value  
                                               FROM   dbo.syConfigAppSetValues  
                                               WHERE  SettingId = 118  
                                                      AND CampusId IS NULL  
    );  
        END;  
  
  
    IF  (  
        SELECT COUNT(*)  
        FROM   syConfigAppSetValues  
        WHERE  SettingId = 37  
               AND CampusId = @CampusId  
        ) >= 1  
        BEGIN  
            SET @FameESP = (  
                           SELECT Value  
                           FROM   dbo.syConfigAppSetValues  
                           WHERE  SettingId = 37  
                                  AND CampusId = @CampusId  
                           );  
  
        END;  
    ELSE  
        BEGIN  
            SET @FameESP = (  
                           SELECT Value  
                           FROM   dbo.syConfigAppSetValues  
                           WHERE  SettingId = 37  
                                  AND CampusId IS NULL  
                           );  
        END;  
  
  
    IF  (  
        SELECT COUNT(*)  
        FROM   syConfigAppSetValues  
        WHERE  SettingId = 91  
               AND CampusId = @CampusId  
        ) >= 1  
        BEGIN  
            SET @EdExpress = (  
                             SELECT Value  
                             FROM   dbo.syConfigAppSetValues  
                             WHERE  SettingId = 91  
                                    AND CampusId = @CampusId  
                             );  
  
        END;  
    ELSE  
        BEGIN  
            SET @EdExpress = (  
                             SELECT Value  
                             FROM   dbo.syConfigAppSetValues  
                             WHERE  SettingId = 91  
                                    AND CampusId IS NULL  
                             );  
        END;  
  
  
  
    IF  (  
        SELECT COUNT(*)  
        FROM   syConfigAppSetValues  
        WHERE  SettingId = 43  
               AND CampusId = @CampusId  
        ) >= 1  
        BEGIN  
            SET @GradeBookWeightingLevel = (  
                                           SELECT Value  
                                           FROM   dbo.syConfigAppSetValues  
                                           WHERE  SettingId = 43  
                                                  AND CampusId = @CampusId  
                                           );  
  
        END;  
    ELSE  
        BEGIN  
            SET @GradeBookWeightingLevel = (  
                                           SELECT Value  
                                           FROM   dbo.syConfigAppSetValues  
                                           WHERE  SettingId = 43  
                                                  AND CampusId IS NULL  
                                           );  
        END;  
  
  
  
    IF  (  
        SELECT COUNT(*)  
        FROM   syConfigAppSetValues  
        WHERE  SettingId = 71  
               AND CampusId = @CampusId  
        ) >= 1  
        BEGIN  
            SET @ShowExternshipTabs = (  
                                      SELECT Value  
                                      FROM   dbo.syConfigAppSetValues  
                                      WHERE  SettingId = 71  
                                             AND CampusId = @CampusId  
                                      );  
  
        END;  
    ELSE  
        BEGIN  
            SET @ShowExternshipTabs = (  
                                      SELECT Value  
                                      FROM   dbo.syConfigAppSetValues  
                                      WHERE  SettingId = 71  
                                             AND CampusId IS NULL  
                                      );  
        END;  
  
    SELECT   *  
            ,CASE WHEN AccessLevel = 15 THEN 1  
                  ELSE 0  
             END AS FullPermission  
            ,CASE WHEN AccessLevel IN ( 14, 13, 12, 11, 10, 9, 8 ) THEN 1  
                  ELSE 0  
             END AS EditPermission  
            ,CASE WHEN AccessLevel IN ( 14, 13, 12, 7, 6, 5, 4 ) THEN 1  
                  ELSE 0  
             END AS AddPermission  
            ,CASE WHEN AccessLevel IN ( 14, 11, 10, 7, 6, 2 ) THEN 1  
                  ELSE 0  
             END AS DeletePermission  
            ,CASE WHEN AccessLevel IN ( 13, 11, 9, 7, 5, 3, 1 ) THEN 1  
                  ELSE 0  
             END AS ReadPermission  
            ,NULL AS TabId  
    FROM     (  
             SELECT 189 AS ModuleResourceId  
                   ,ResourceID AS ChildResourceId  
                   ,Resource AS ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = syResources.ResourceID  
                    ) AS AccessLevel  
                   ,ResourceURL AS ChildResourceURL  
                   ,NULL AS ParentResourceId  
                   ,NULL AS ParentResource  
                   ,1 AS GroupSortOrder  
                   ,NULL AS FirstSortOrder  
                   ,NULL AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   syResources  
             WHERE  ResourceID = 189  
             UNION  
             SELECT 189 AS ModuleResourceId  
                   ,NNChild.ResourceId AS ChildResourceId  
                   ,CASE WHEN NNChild.ResourceId = 395 THEN ''Lead Tabs''  
                         ELSE CASE WHEN NNChild.ResourceId = 398 THEN ''Add/View Leads''  
                                   ELSE RChild.Resource  
                              END  
                    END AS ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = NNChild.ResourceId  
                    ) AS AccessLevel  
                   ,RChild.ResourceURL AS ChildResourceURL  
                   ,CASE WHEN (  
                              NNParent.ResourceId IN ( 689 )  
                              OR NNChild.ResourceId IN ( 710, 402, 617, 734, 794 )  
                              ) THEN NULL  
                         ELSE NNParent.ResourceId  
                    END AS ParentResourceId  
                   ,RParent.Resource AS ParentResource  
                   ,CASE WHEN (  
                              NNChild.ResourceId IN ( 710 )  
                              OR NNParent.ResourceId IN ( 710 )  
                              ) THEN 2  
                         ELSE CASE WHEN (  
                                        NNChild.ResourceId IN ( 734 )  
                                        OR NNParent.ResourceId IN ( 734 )  
                                        ) THEN 3  
                                   ELSE CASE WHEN (  
                                                  NNChild.ResourceId IN ( 617 )  
                                                  OR NNParent.ResourceId IN ( 617 )  
                                                  ) THEN 4  
                                             ELSE CASE WHEN (  
                                                            NNChild.ResourceId IN ( 794 )  
                                                            OR NNParent.ResourceId IN ( 794 )  
                                                            ) THEN 5  
                                                       ELSE 6  
                                                  END  
                                        END  
                              END  
                    END AS GroupSortOrder  
                   ,CASE WHEN NNChild.ResourceId = 398 THEN 1  
                         ELSE CASE WHEN NNChild.ResourceId = 395 THEN 2  
                                   ELSE 3  
                              END  
                    END AS FirstSortOrder  
                   ,CASE WHEN (  
                              NNChild.ResourceId IN ( 710, 734 )  
                              OR NNParent.ResourceId IN ( 710, 734 )  
                              ) THEN 1  
                         ELSE 2  
                    END AS DisplaySequence  
                   ,RChild.ResourceTypeID  
             FROM   syResources RChild  
             INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
             INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
             INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
             LEFT OUTER JOIN (  
                             SELECT *  
                             FROM   syResources  
                             WHERE  ResourceTypeID = 1  
                             ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
             WHERE  RChild.ResourceTypeID IN ( 2, 5, 8 )  
                    AND (  
                        RChild.ChildTypeId IS NULL  
                        OR RChild.ChildTypeId = 5  
                        )  
                    AND ( RChild.ResourceID NOT IN ( 395 ))  
                    AND ( NNParent.ResourceId NOT IN ( 395 ))  
                    AND (  
                        NNParent.ParentId IN (  
                                             SELECT HierarchyId  
                                             FROM   syNavigationNodes  
                                             WHERE  ResourceId = 189  
                                             )  
                        OR NNChild.ParentId IN (  
                                               SELECT HierarchyId  
                                               FROM   syNavigationNodes  
                                               WHERE  ResourceId IN ( 710, 402, 617, 734, 794 )  
                                               )  
                        )  
                    AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
             UNION  
             -- ACADEMICS RECORDS         
             SELECT 26 AS ModuleResourceId  
                   ,ResourceID AS ChildResourceId  
                   ,Resource AS ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = syResources.ResourceID  
                    ) AS AccessLevel  
                   ,ResourceURL AS ChildResourceURL  
                   ,NULL AS ParentResourceId  
                   ,NULL AS ParentResource  
                   ,1 AS GroupSortOrder  
                   ,NULL AS FirstSortOrder  
                   ,NULL AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   syResources  
             WHERE  ResourceID = 26  
             UNION  
             SELECT 26 AS ModuleResourceId  
                   ,ChildResourceId  
                   ,ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = t1.ChildResourceId  
                    ) AS AccessLevel  
                   ,ChildResourceURL  
                   ,ParentResourceId  
                   ,ParentResource  
                   ,GroupSortOrder  
                   ,(  
                    SELECT TOP 1 HierarchyIndex  
                    FROM   dbo.syNavigationNodes  
                    WHERE  ResourceId = ChildResourceId  
                    ) AS FirstSortOrder  
                   ,CASE WHEN (  
                              ChildResourceId IN ( 690, 691, 729, 730 )  
                              OR ParentResourceId IN ( 690, 691, 729, 730 )  
                              ) THEN 1  
                         ELSE CASE WHEN (  
                                        ChildResourceId IN ( 727, 692, 736, 678 )  
                                        OR ParentResourceId IN ( 727, 736, 692, 678 )  
                                        ) THEN 2  
                                   ELSE 3  
                              END  
                    END AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   (  
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId  
                          ,CASE WHEN NNChild.ResourceId = 409 THEN ''IPEDS - General Reports''  
                                ELSE RChild.Resource  
                           END AS ChildResource  
                          ,RChild.ResourceURL AS ChildResourceURL  
                          ,CASE WHEN (  
                                     NNParent.ResourceId IN ( 689 )  
                                     OR NNChild.ResourceId IN ( 472, 474, 712, 736, 678 ) --678         
                                     ) THEN NULL  
                                ELSE NNParent.ResourceId  
                           END AS ParentResourceId  
                          ,RParent.Resource AS ParentResource  
                          ,RParentModule.Resource AS MODULE  
                          ,CASE WHEN (  
                                     NNChild.ResourceId IN ( 691 )  
                                     OR NNParent.ResourceId IN ( 691 )  
                                     ) THEN 2  
                                WHEN (  
                                     NNChild.ResourceId IN ( 690 )  
                                     OR NNParent.ResourceId IN ( 690 )  
                                     ) THEN 3  
                                WHEN (  
                                     NNChild.ResourceId IN ( 729 )  
                                     OR NNParent.ResourceId IN ( 729 )  
                                     ) THEN 4  
                                WHEN (  
                                     NNChild.ResourceId IN ( 730 )  
                                     OR NNParent.ResourceId IN ( 730 )  
                                     ) THEN 5  
                                WHEN (  
                                     NNChild.ResourceId IN ( 736 )  
                                     OR NNParent.ResourceId IN ( 736 )  
                                     ) THEN 6  
                                WHEN (  
                                     NNChild.ResourceId IN ( 678 )  
                                     OR NNParent.ResourceId IN ( 678 )  
                                     ) THEN 7  
                                WHEN (  
                                     NNChild.ResourceId IN ( 692 )  
                                     OR NNParent.ResourceId IN ( 692 )  
                                     ) THEN 8  
                                WHEN (  
                                     NNChild.ResourceId IN ( 727 )  
                                     OR NNParent.ResourceId IN ( 727 )  
                                     ) THEN 9  
                                WHEN (  
                                     NNChild.ResourceId IN ( 864 )  
                                     OR NNParent.ResourceId IN ( 864 )  
                                     ) THEN 10  
									  WHEN (  
                                     NNChild.ResourceId IN ( 839 )  
                                     OR NNParent.ResourceId IN ( 839 )  
                                     ) THEN 11
									 WHEN (  
                                     NNChild.ResourceId IN ( 844 )  
                                     OR NNParent.ResourceId IN ( 844 )  
                                     ) THEN 12
                                ELSE 13 
                           END AS GroupSortOrder  
                          ,RChild.ResourceTypeID  
                    FROM   syResources RChild  
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
                    LEFT OUTER JOIN (  
                                    SELECT *  
                                    FROM   syResources  
                                    WHERE  ResourceTypeID = 1  
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
                    WHERE  RChild.ResourceTypeID IN ( 2, 5, 8 )  
                           AND (  
                               RChild.ChildTypeId IS NULL  
                               OR RChild.ChildTypeId = 5  
                               )  
                           AND ( RChild.ResourceID NOT IN (  
                                                          SELECT ResId  
                                                          FROM   @InTbl  
                                                          WHERE  Header = 1  
                                                                 AND ShouldInclude = 0  
                                                          )  
                               )  
                           AND ( RChild.ResourceID NOT IN ( 394, 472, 474, 711, 409, 719, 720, 721, 722, 723, 724, 725, 789 ))  
                           AND (  
                               NNParent.ParentId IN (  
                                                    SELECT HierarchyId  
                                                    FROM   syNavigationNodes  
                                                    WHERE  ResourceId = 26  
                                                    )  
                               OR NNChild.ParentId IN (  
                                                      SELECT HierarchyId  
                                                      FROM   syNavigationNodes  
                                                      WHERE ResourceId IN ( SELECT ResId FROM   @InTbl WHERE ShouldInclude = 1)   
                                                      )  
                               )  
                           AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
                    ) t1  
             UNION  
             SELECT 194 AS ModuleResourceId  
                   ,ResourceID AS ChildResourceId  
                   ,Resource AS ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = syResources.ResourceID  
                    ) AS AccessLevel  
                   ,ResourceURL AS ChildResourceURL  
                   ,NULL AS ParentResourceId  
                   ,NULL AS ParentResource  
                   ,1 AS GroupSortOrder  
                   ,NULL AS FirstSortOrder  
                   ,NULL AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   syResources  
             WHERE  ResourceID = 194  
             UNION  
             SELECT 194 AS ModuleResourceId  
                   ,ChildResourceId  
                   ,ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = t1.ChildResourceId  
                    ) AS AccessLevel  
                   ,ChildResourceURL  
                   ,ParentResourceId  
                   ,ParentResource  
                   ,GroupSortOrder  
                   ,GroupSortOrder AS FirstSortOrder  
                   ,CASE WHEN (  
                              ChildResourceId IN ( 731, 732 )  
                              OR ParentResourceId IN ( 731, 732 )  
                              ) THEN 1  
                         ELSE 2  
                    END AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   (  
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId  
                          ,CASE WHEN NNChild.ResourceId = 409 THEN ''IPEDS - General Reports''  
                                ELSE RChild.Resource  
                           END AS ChildResource  
                          ,RChild.ResourceURL AS ChildResourceURL  
                          ,CASE WHEN NNParent.ResourceId IN ( 194 )  
                                     OR NNChild.ResourceId IN ( 731, 732, 733 ) THEN NULL  
                                ELSE NNParent.ResourceId  
                           END AS ParentResourceId  
                          ,RParent.Resource AS ParentResource  
                          ,RParentModule.Resource AS MODULE  
                          -- DE7659 updated for the Payment Report (4.2)         
                          ,CASE WHEN (  
                                     NNChild.ResourceId IN ( 732 )  
                                     OR NNParent.ResourceId IN ( 732 )  
                              ) THEN 2  
                                ELSE CASE WHEN (  
                                               NNChild.ResourceId IN ( 731 )  
                                               OR NNParent.ResourceId IN ( 731 )  
                                               ) THEN 3  
                                          ELSE CASE WHEN (  
                                                         NNChild.ResourceId IN ( 733 )  
                                                         OR NNParent.ResourceId IN ( 733 )  
                                                         ) THEN 4  
                                                    ELSE 5  
                                               END  
                                     END  
                           END AS GroupSortOrder  
                          ,RChild.ResourceTypeID  
                    FROM   syResources RChild  
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
                    LEFT OUTER JOIN (  
                                    SELECT *  
                                    FROM   syResources  
                                    WHERE  ResourceTypeID = 1  
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
                    WHERE  RChild.ResourceTypeID IN ( 2, 5, 8 )  
                           AND (  
                               RChild.ChildTypeId IS NULL  
                               OR RChild.ChildTypeId = 5  
                               )  
                           --AND (NNParent.ResourceId IN (689,409,711,472,474,712))         
                           AND ( RChild.ResourceID NOT IN ( 394 ))  
                           AND (  
                               NNParent.ParentId IN (  
                                                    SELECT HierarchyId  
                                                    FROM   syNavigationNodes  
                                                    WHERE  ResourceId = 194  
                                                    )  
                               OR NNChild.ParentId IN (  
                                                      SELECT HierarchyId  
                                                      FROM   syNavigationNodes  
                                                      WHERE  ResourceId IN ( 731, 732, 733, 711 )  
                                                      )  
                               )  
                           AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
                    ) t1  
             UNION  
             SELECT 300 AS ModuleResourceId  
                   ,ResourceID AS ChildResourceId  
                   ,Resource AS ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = syResources.ResourceID  
                    ) AS AccessLevel  
                   ,ResourceURL AS ChildResourceURL  
                   ,NULL AS ParentResourceId  
                   ,NULL AS ParentResource  
                   ,1 AS GroupSortOrder  
                   ,NULL AS FirstSortOrder  
                   ,NULL AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   syResources  
             WHERE  ResourceID = 300  
             UNION  
             SELECT 300 AS ModuleResourceId  
                   ,ChildResourceId  
                   ,ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = t1.ChildResourceId  
                    ) AS AccessLevel  
      ,ChildResourceURL  
                   ,ParentResourceId  
                   ,ParentResource  
                   ,GroupSortOrder  
                   ,GroupSortOrder AS FirstSortOrder  
                   ,1 AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   (  
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId  
                          ,CASE WHEN NNChild.ResourceId = 394 THEN ''Student Tabs''  
                                ELSE RChild.Resource  
                           END AS ChildResource  
                          ,RChild.ResourceURL AS ChildResourceURL  
                          ,CASE WHEN NNParent.ResourceId = 300  
                                     OR NNChild.ResourceId = 715 THEN NULL  
                                --ELSE NNParent.ResourceId         
                                -- Had to hard code the parentresourceid to 715 for DE7532 on 7/25/2012 B. Shanblatt         
                                ELSE 715  
                           END AS ParentResourceId  
                          --RParent.Resource AS ParentResource ,         
                          -- Had to hard code the parentresource to General Reports for DE7532 on 7/25/2012 B. Shanblatt         
                          ,''General Reports'' AS ParentResource  
                          ,RParentModule.Resource AS MODULE  
                          ,CASE WHEN (  
                                     NNChild.ResourceId IN ( 715 )  
                                     -- Added 691 to fix the Weekly Attendance report getting duplicated         
                                     OR NNParent.ResourceId IN ( 715, 691 )  
                                     ) THEN 2  
                                ELSE 3  
                           END AS GroupSortOrder  
                          ,RChild.ResourceTypeID  
                    FROM   syResources RChild  
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
                    LEFT OUTER JOIN (  
                                    SELECT *  
                                    FROM   syResources  
                                    WHERE  ResourceTypeID = 1  
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
                    WHERE  RChild.ResourceTypeID IN ( 2, 5, 8 )  
                           AND (  
                               RChild.ChildTypeId IS NULL  
                               OR RChild.ChildTypeId = 5  
                               )  
                           --AND (NNParent.ResourceId IN (689,409,711,472,474,712))         
                           -- Added resource 325, 329, 366, 492, 554, 588, 633 for DE7532 on 7/25/2012 B. Shanblatt         
                           AND ( RChild.ResourceID NOT IN ( 394, 325, 329, 366, 492, 554, 633 ))  
                           AND (  
                               NNParent.ParentId IN (  
                                                    SELECT HierarchyId  
                                                    FROM   syNavigationNodes  
                                                    WHERE  ResourceId = 300  
                                                    )  
                               OR NNChild.ParentId IN (  
                                                      SELECT HierarchyId  
                                                      FROM   syNavigationNodes  
                                                      -- Added resource 691 for DE7532 on 7/25/2012 B. Shanblatt          
                                                      WHERE  ResourceId IN ( 715, 691 )  
                                                      )  
                               )  
                           AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
                    ) t1  
             UNION  
             SELECT 191 AS ModuleResourceId  
                   ,ResourceID AS ChildResourceId  
                   ,Resource AS ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = syResources.ResourceID  
                    ) AS AccessLevel  
                   ,ResourceURL AS ChildResourceURL  
                   ,NULL AS ParentResourceId  
                   ,NULL AS ParentResource  
                   ,1 AS GroupSortOrder  
                   ,NULL AS FirstSortOrder  
                   ,NULL AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   syResources  
             WHERE  ResourceID = 191  
             UNION  
             SELECT 191 AS ModuleResourceId  
                   ,ChildResourceId  
                   ,ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = t1.ChildResourceId  
                    ) AS AccessLevel  
                   ,ChildResourceURL  
                   ,ParentResourceId  
                   ,ParentResource  
                   ,GroupSortOrder  
                   ,GroupSortOrder AS FirstSortOrder  
                   ,CASE WHEN (  
                              ChildResourceId IN ( 716 )  
                              OR ParentResourceId IN ( 716 )  
                              ) THEN 1  
                         ELSE 2  
                    END AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   (  
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId  
                          ,CASE WHEN NNChild.ResourceId = 394 THEN ''Student Tabs''  
                                ELSE RChild.Resource  
                           END AS ChildResource  
                          ,RChild.ResourceURL AS ChildResourceURL  
                          ,CASE WHEN (  
                                     NNParent.ResourceId = 191  
                                     OR NNChild.ResourceId = 716  
                                     ) THEN NULL  
                                ELSE NNParent.ResourceId  
                           END AS ParentResourceId  
                          ,RParent.Resource AS ParentResource  
                          ,RParentModule.Resource AS MODULE  
                          ,CASE WHEN (  
                                     NNChild.ResourceId IN ( 716 )  
                                     OR NNParent.ResourceId IN ( 716 )  
                                     ) THEN 2  
                                ELSE 3  
                           END AS GroupSortOrder  
                          ,RChild.ResourceTypeID  
                    FROM   syResources RChild  
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
                    LEFT OUTER JOIN (  
                                    SELECT *  
                                    FROM   syResources  
                                    WHERE  ResourceTypeID = 1  
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
                    WHERE  RChild.ResourceTypeID IN ( 2, 5, 8 )  
                           AND (  
                               RChild.ChildTypeId IS NULL  
                               OR RChild.ChildTypeId = 5  
                               )  
                           --AND (NNParent.ResourceId IN (689,409,711,472,474,712))         
                           AND ( RChild.ResourceID NOT IN ( 394 ))  
         AND (  
                               NNParent.ParentId IN (  
                                                    SELECT HierarchyId  
                                                    FROM   syNavigationNodes  
                                                    WHERE  ResourceId = 191  
                                                    )  
                               OR NNChild.ParentId IN (  
                                                      SELECT HierarchyId  
                                                      FROM   syNavigationNodes  
                                                      WHERE  ResourceId IN ( 716 )  
                                                      )  
                               )  
                           AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
                    ) t1  
             UNION  
             SELECT 193 AS ModuleResourceId  
                   ,ResourceID AS ChildResourceId  
                   ,Resource AS ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = syResources.ResourceID  
                    ) AS AccessLevel  
                   ,ResourceURL AS ChildResourceURL  
                   ,NULL AS ParentResourceId  
                   ,NULL AS ParentResource  
                   ,1 AS GroupSortOrder  
                   ,NULL AS FirstSortOrder  
                   ,NULL AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   syResources  
             WHERE  ResourceID = 193  
             UNION  
             SELECT 193 AS ModuleResourceId  
                   ,ChildResourceId  
                   ,ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = t1.ChildResourceId  
                    ) AS AccessLevel  
                   ,ChildResourceURL  
                   ,ParentResourceId  
                   ,ParentResource  
                   ,GroupSortOrder  
                   ,GroupSortOrder AS FirstSortOrder  
                   ,CASE WHEN (  
                              ChildResourceId IN ( 713 )  
                              OR ParentResourceId IN ( 713 )  
                              ) THEN 1  
                         ELSE 2  
                    END AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   (  
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId  
                          ,CASE WHEN NNChild.ResourceId = 394 THEN ''Student Tabs''  
                                ELSE RChild.Resource  
                           END AS ChildResource  
                          ,RChild.ResourceURL AS ChildResourceURL  
                          ,CASE WHEN (  
                                     NNParent.ResourceId = 193  
                                     OR NNChild.ResourceId IN ( 713, 735 )  
                                     ) THEN NULL  
                                ELSE NNParent.ResourceId  
                           END AS ParentResourceId  
                          ,RParent.Resource AS ParentResource  
                          ,RParentModule.Resource AS MODULE  
                          ,CASE WHEN (  
                                     NNChild.ResourceId IN ( 713 )  
                                     OR NNParent.ResourceId IN ( 713 )  
                                     ) THEN 2  
                                ELSE 3  
                           END AS GroupSortOrder  
                          ,RChild.ResourceTypeID  
                    FROM   syResources RChild  
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
                    LEFT OUTER JOIN (  
                                    SELECT *  
                                    FROM   syResources  
                                    WHERE  ResourceTypeID = 1  
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
                    WHERE  RChild.ResourceTypeID IN ( 2, 5, 8 )  
                           AND (  
                               RChild.ChildTypeId IS NULL  
                               OR RChild.ChildTypeId = 5  
                               )  
                           --AND (NNParent.ResourceId IN (689,409,711,472,474,712))         
                           AND ( RChild.ResourceID NOT IN ( 394 ))  
                           AND (  
                               NNParent.ParentId IN (  
                                                    SELECT HierarchyId  
                                                    FROM   syNavigationNodes  
                                                    WHERE  ResourceId = 193  
                                                    )  
                               OR NNChild.ParentId IN (  
                                                      SELECT HierarchyId  
                                                      FROM   syNavigationNodes  
                                                      WHERE  ResourceId IN ( 713, 735 )  
                                                      )  
                               )  
                           AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
                    ) t1  
             UNION  
             SELECT 195 AS ModuleResourceId  
                   ,ResourceID AS ChildResourceId  
                   ,Resource AS ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = syResources.ResourceID  
                    ) AS AccessLevel  
                   ,ResourceURL AS ChildResourceURL  
                   ,NULL AS ParentResourceId  
                   ,NULL AS ParentResource  
                   ,1 AS GroupSortOrder  
                   ,NULL AS FirstSortOrder  
                   ,NULL AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   syResources  
             WHERE  ResourceID = 195  
             UNION  
             SELECT 195 AS ModuleResourceId  
                   ,ChildResourceId  
                   ,ChildResource  
                   ,(  
                    SELECT AccessLevel  
                    FROM   syRlsResLvls  
                    WHERE  RoleId = @RoleId  
                           AND ResourceID = t1.ChildResourceId  
                    ) AS AccessLevel  
                   ,ChildResourceURL  
                   ,ParentResourceId  
                   ,ParentResource  
                   ,GroupSortOrder  
                   ,GroupSortOrder AS FirstSortOrder  
                   ,CASE WHEN (  
                              ChildResourceId IN ( 717 )  
                              OR ParentResourceId IN ( 717 )  
                              ) THEN 1  
                         ELSE 2  
                    END AS DisplaySequence  
                   ,ResourceTypeID  
             FROM   (  
                    SELECT DISTINCT NNChild.ResourceId AS ChildResourceId  
                          ,CASE WHEN NNChild.ResourceId = 394 THEN ''Student Tabs''  
                                ELSE RChild.Resource  
                           END AS ChildResource  
                          ,RChild.ResourceURL AS ChildResourceURL  
                          ,CASE WHEN (  
                                     NNParent.ResourceId = 195  
                                     OR NNChild.ResourceId IN ( 717 )  
                                     ) THEN NULL  
                                ELSE NNParent.ResourceId  
                           END AS ParentResourceId  
                          ,RParent.Resource AS ParentResource  
                          ,RParentModule.Resource AS MODULE  
                          ,CASE WHEN (  
                                     NNChild.ResourceId IN ( 717 )  
                                     OR NNParent.ResourceId IN ( 717 )  
                                     ) THEN 2  
                                ELSE 3  
                           END AS GroupSortOrder  
                          ,RChild.ResourceTypeID  
                    FROM   syResources RChild  
                    INNER JOIN syNavigationNodes NNChild ON RChild.ResourceID = NNChild.ResourceId  
                    INNER JOIN syNavigationNodes NNParent ON NNChild.ParentId = NNParent.HierarchyId  
                    INNER JOIN syResources RParent ON NNParent.ResourceId = RParent.ResourceID  
                    LEFT OUTER JOIN (  
                                    SELECT *  
                                    FROM   syResources  
                                    WHERE  ResourceTypeID = 1  
                                    ) RParentModule ON RParent.ResourceID = RParentModule.ResourceID  
                    WHERE  RChild.ResourceTypeID IN ( 2, 5, 8 )  
                           AND (  
                               RChild.ChildTypeId IS NULL  
                               OR RChild.ChildTypeId = 5  
                               )  
                           --AND (NNParent.ResourceId IN (689,409,711,472,474,712))         
                           AND ( RChild.ResourceID NOT IN ( 394 ))  
                           AND (  
                               NNParent.ParentId IN (  
                                                    SELECT HierarchyId  
                                                    FROM   syNavigationNodes  
                                                    WHERE  ResourceId = 195  
                                                    )  
                               OR NNChild.ParentId IN (  
                                                      SELECT HierarchyId  
                                                      FROM   syNavigationNodes  
                                                      WHERE  ResourceId IN ( 717 )  
                                                      )  
                               )  
                           AND ( RChild.UsedIn & @SchoolEnumerator > 0 )  
                    ) t1  
             ) t2  
    WHERE    t2.ModuleResourceId = @ModuleResourceId  
  
             -- Hide resources based on Configuration Settings           
             AND (  
                 -- The following expression means if showross... is set to false, hide            
                 -- ResourceIds 541,542,532,534,535,538,543,539           
                 -- (Not False) OR (Condition)           
                 (  
                 ( @ShowRossOnlyTabs <> 0 )  
                 OR ( ChildResourceId NOT IN ( 541, 542, 532, 534, 535, 538, 543, 539 ))  
                 )  
                 AND  
                 -- The following expression means if showross... is set to true, hide            
                 -- ResourceIds 142,375,330,476,508,102,107,237           
                 -- (Not True) OR (Condition)           
                 (  
                 ( @ShowRossOnlyTabs <> 1 )  
                 OR ( ChildResourceId NOT IN ( 142, 375, 330, 476, 508, 102, 107, 237 ))  
                 )  
                 AND  
                 ---- The following expression means if SchedulingMethod=regulartraditional, hide            
                 ---- ResourceIds 91 and 497           
                 ---- (Not True) OR (Condition)           
                 (  
                 ( NOT LOWER(LTRIM(RTRIM(@SchedulingMethod))) = ''regulartraditional'' )  
                 OR ( ChildResourceId NOT IN ( 91, 497 ))  
                 )  
                 AND  
                 -- The following expression means if TrackSAPAttendance=byday, hide            
                 -- ResourceIds 585,586,589,590,677,678,770,670           
                 -- (Not True) OR (Condition)           
                 -- Removed 667 and 770 and 585 on 7/25/2012 B. Shanblatt          
                 -- DE8640         
                 -- ( ( NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = ''byday''         
                 -- )         
                 --OR ( ChildResourceId NOT IN ( 589, 590,         
                 --         670 ) )         
                 --  )               
                 --  AND           
                 (  
                 ( NOT LOWER(LTRIM(RTRIM(@TrackSAPAttendance))) = ''byclass'' )  
                 OR ( ChildResourceId NOT IN ( 633 ))  
                 )  
                 AND  
                 ---- The following expression means if @ShowCollegeOfCourtReporting is set to false, hide            
                 ---- ResourceIds 614,615           
                 ---- (Not False) OR (Condition)           
                 (  
                 ( NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = ''no'' )  
                 OR ( ChildResourceId NOT IN ( 614, 615, 729 ))  
                 )  
                 AND  
                 -- The following expression means if @ShowCollegeOfCourtReporting is set to true, hide            
                 -- ResourceIds 497           
                 -- (Not True) OR (Condition)           
                 (  
                 ( NOT LOWER(LTRIM(RTRIM(@ShowCollegeOfCourtReporting))) = ''yes'' )  
                 OR ( ChildResourceId NOT IN ( 497 ))  
                 )  
                 AND  
                 -- The following expression means if FAMEESP is set to false, hide            
                 -- ResourceIds 517,523, 525           
                 -- (Not False) OR (Condition)           
                 (  
                 ( NOT LOWER(LTRIM(RTRIM(@FameESP))) = ''no'' )  
                 OR ( ChildResourceId NOT IN ( 517, 523, 525 ))  
                 )  
                 AND  
                 ---- The following expression means if EDExpress is set to false, hide            
                 ---- ResourceIds 603,604,606,619           
                 ---- (Not False) OR (Condition)           
                 (  
                 ( NOT LOWER(LTRIM(RTRIM(@EdExpress))) = ''no'' )  
                 OR ( ChildResourceId NOT IN ( 603, 604, 605, 619 ))  
                 )  
                 AND  
                 ---- The following expression means if @GradeBookWeightingLevel is set to courselevel, hide            
                 ---- ResourceIds 107,96,222           
                 ---- (Not False) OR (Condition)           
                 (  
                 ( NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = ''courselevel'' )  
                 OR ( ChildResourceId NOT IN ( 107, 96, 222 ))  
                 )  
                 AND (  
                     ( NOT LOWER(LTRIM(RTRIM(@GradeBookWeightingLevel))) = ''instructorlevel'' )  
                     OR ( ChildResourceId NOT IN ( 476 ))  
                     )  
                 AND (  
                     ( NOT LOWER(LTRIM(RTRIM(@ShowExternshipTabs))) = ''no'' )  
                     OR ( ChildResourceId NOT IN ( 543, 538 ))  
                     )  
                 )  
    ORDER BY GroupSortOrder  
            ,ParentResourceId  
            ,ChildResource;  
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_NACCAS_CohortGrid]'
GO
IF OBJECT_ID(N'[dbo].[USP_NACCAS_CohortGrid]', 'P') IS NULL
EXEC sp_executesql N'

CREATE PROCEDURE [dbo].[USP_NACCAS_CohortGrid]
    @PrgIdList VARCHAR(MAX) = NULL
   ,@CampusId VARCHAR(50)
   ,@ReportYear INT
   ,@ReportType VARCHAR(50)
AS
    DECLARE @StartMonth INT = 1
           ,@StartDay INT = 1
           ,@EndMonth INT = 12
           ,@EndDay INT = 31
           ,@FutureStart INT = 7
           ,@CurrentlyAttending INT = 9
           ,@LOA INT = 10
           ,@Suspension INT = 11
           ,@Dropped INT = 12
           ,@Graduated INT = 14
           ,@Transfer INT = 19
           ,@Preliminary VARCHAR(50) = ''Preliminary''
           ,@FallAnnual VARCHAR(50) = ''FallAnnual''
           ,@MilitaryTransfer UNIQUEIDENTIFIER
           ,@CallToActiveDuty UNIQUEIDENTIFIER
           ,@Deceased UNIQUEIDENTIFIER
           ,@TemporarilyDisabled UNIQUEIDENTIFIER
           ,@PermanentlyDisabled UNIQUEIDENTIFIER
           ,@TransferredToEquivalent UNIQUEIDENTIFIER;

    BEGIN

        SET @MilitaryTransfer = (
                                SELECT TOP 1 NACCASDropReasonId
                                FROM   dbo.syNACCASDropReasons
                                WHERE  Name = ''Military Transfer''
                                );
        SET @CallToActiveDuty = (
                                SELECT TOP 1 NACCASDropReasonId
                                FROM   dbo.syNACCASDropReasons
                                WHERE  Name = ''Call to Active Duty''
                                );
        SET @Deceased = (
                        SELECT TOP 1 NACCASDropReasonId
                        FROM   dbo.syNACCASDropReasons
                        WHERE  Name = ''Deceased''
                        );
        SET @TemporarilyDisabled = (
                                   SELECT TOP 1 NACCASDropReasonId
                                   FROM   dbo.syNACCASDropReasons
                                   WHERE  Name = ''Temporarily disabled''
                                   );
        SET @PermanentlyDisabled = (
                                   SELECT TOP 1 NACCASDropReasonId
                                   FROM   dbo.syNACCASDropReasons
                                   WHERE  Name = ''Permanently disabled''
                                   );
        SET @TransferredToEquivalent = (
                                       SELECT TOP 1 NACCASDropReasonId
                                       FROM   dbo.syNACCASDropReasons
                                       WHERE  Name = ''Transferred to an equivalent program at another school with same accreditation''
                                       );

        DECLARE @Enrollments TABLE
            (
                PrgVerID UNIQUEIDENTIFIER
               ,PrgVerDescrip VARCHAR(50)
               ,ProgramHours DECIMAL(9, 2)
               ,ProgramCredits DECIMAL(18, 2)
               ,TotalTransferHours DECIMAL(18, 2)
               ,TransferredProgram UNIQUEIDENTIFIER
               ,TransferHoursFromProgram DECIMAL(18, 2)
               ,SSN VARCHAR(50)
               ,PhoneNumber VARCHAR(50)
               ,Email VARCHAR(100)
               ,Student VARCHAR(50)
               ,LeadId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,EnrollmentID NVARCHAR(50)
               ,Status VARCHAR(80)
               ,SysStatusId INT
               ,DropReasonId UNIQUEIDENTIFIER
               ,StartDate DATETIME
               ,ContractedGradDate DATETIME
               ,ExpectedGradDate DATETIME
               ,LDA DATETIME
               ,LicensureWrittenAllParts BIT
               ,LicensureLastPartWrittenOn DATETIME
               ,LicensurePassedAllParts BIT
               ,AllowsMoneyOwed BIT
               ,AllowsIncompleteReq BIT
            );

        DECLARE @EnrollmentsWithLastStatus TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,SysStatusId INT
               ,DropReasonId UNIQUEIDENTIFIER
               ,StatusCodeDescription VARCHAR(80)
            );
        INSERT INTO @EnrollmentsWithLastStatus
                    SELECT lastStatusOfYear.StuEnrollId
                          ,lastStatusOfYear.SysStatusId
                          ,lastStatusOfYear.DropReasonId
                          ,lastStatusOfYear.StatusCodeDescrip
                    FROM   (
                           SELECT     StuEnrollId
                                     ,syStatusCodes.SysStatusId
                                     ,StatusCodeDescrip
                                     ,DateOfChange
                                     ,DropReasonId
                                     ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId
                                                          ORDER BY DateOfChange DESC
                                                        ) rn
                           FROM       dbo.syStudentStatusChanges
                           INNER JOIN dbo.syStatusCodes ON syStatusCodes.StatusCodeId = syStudentStatusChanges.NewStatusId
                           WHERE      DATEPART(YEAR, DateOfChange) <= ( @ReportYear - 1 )
                           ) lastStatusOfYear
                    WHERE  rn = 1;





        INSERT INTO @Enrollments
                    SELECT     Prog.ProgId AS PrgVerID
                              ,Prog.ProgDescrip AS PrgVerDescrip
                              ,PV.Hours AS ProgramHours
                              ,PV.Credits AS ProgramCredits
                              ,SE.TransferHours
                              ,SE.TransferHoursFromThisSchoolEnrollmentId
                              ,SE.TotalTransferHoursFromThisSchool
                              ,LD.SSN
                              ,ISNULL((
                                      SELECT   TOP 1 Phone
                                      FROM     dbo.adLeadPhone
                                      WHERE    LeadId = LD.LeadId
                                               AND (
                                                   IsBest = 1
                                                   OR IsShowOnLeadPage = 1
                                                   )
                                      ORDER BY Position
                                      )
                                     ,''''
                                     ) AS PhoneNumber
                              ,ISNULL((
                                      SELECT   TOP 1 EMail
                                      FROM     dbo.AdLeadEmail
                                      WHERE    LeadId = LD.LeadId
                                               AND (
                                                   IsPreferred = 1
                                                   OR IsShowOnLeadPage = 1
                                                   )
                                      ORDER BY IsPreferred DESC
                                              ,IsShowOnLeadPage DESC
                                      )
                                     ,''''
                                     ) AS Email
                              ,LD.LastName + '', '' + LD.FirstName + '' '' + ISNULL(LD.MiddleName, '''') AS Student
                              ,LD.LeadId
                              ,SE.StuEnrollId
                              ,LD.StudentNumber AS EnrollmentID
                              ,[@EnrollmentsWithLastStatus].StatusCodeDescription AS Status
                              ,[@EnrollmentsWithLastStatus].SysStatusId AS SysStatusId
                              ,[@EnrollmentsWithLastStatus].DropReasonId AS DropReasonId
                              ,SE.StartDate AS StartDate
                              ,SE.ContractedGradDate AS ContractedGradDate
                              ,dbo.GetGraduatedDate(SE.StuEnrollId) AS ExpectedGradDate
                              ,dbo.GetLDA(SE.StuEnrollId) AS LDA
                              ,SE.LicensureWrittenAllParts AS LicensureWrittenAllParts
                              ,SE.LicensureLastPartWrittenOn AS LicensureLastPartWrittenOn
                              ,SE.LicensurePassedAllParts AS LicensurePassedAllParts
                              ,camp.AllowGraduateAndStillOweMoney AS AllowsMoneyOwed
                              ,camp.AllowGraduateWithoutFullCompletion AS AllowsIncompleteReq
                    FROM       dbo.arStuEnrollments AS SE
                    INNER JOIN dbo.adLeads AS LD ON LD.LeadId = SE.LeadId
                    INNER JOIN dbo.arPrgVersions AS PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arPrograms AS Prog ON Prog.ProgId = PV.ProgId
                    INNER JOIN dbo.syApprovedNACCASProgramVersion NaccasApproved ON NaccasApproved.ProgramVersionId = SE.PrgVerId
                    INNER JOIN dbo.syNaccasSettings NaccasSettings ON NaccasSettings.NaccasSettingId = NaccasApproved.NaccasSettingId
                    INNER JOIN dbo.syCampuses AS camp ON camp.CampusId = SE.CampusId
                    INNER JOIN @EnrollmentsWithLastStatus ON [@EnrollmentsWithLastStatus].StuEnrollId = SE.StuEnrollId
                    WHERE      SE.CampusId = @CampusId
                               AND NaccasSettings.CampusId = @CampusId
                               --that have a contracted graduation date between January 1 of reporting year minus 1 year and December 31st of reporting year minus 1 year
                               -- Not a third party contract
                               AND NOT SE.ThirdPartyContract = 1
                               AND NaccasApproved.IsApproved = 1
                               AND SE.ContractedGradDate
                               BETWEEN CONVERT(DATETIME, CONVERT(VARCHAR(50), (( @ReportYear - 1 ) * 10000 + @StartMonth * 100 + @StartDay )), 112) AND CONVERT(
                                                                                                                                                                   DATETIME
                                                                                                                                                                  ,CONVERT(
                                                                                                                                                                              VARCHAR(50)
                                                                                                                                                                             ,(( @ReportYear
                                                                                                                                                                                 - 1
                                                                                                                                                                               )
                                                                                                                                                                               * 10000
                                                                                                                                                                               + @EndMonth
                                                                                                                                                                               * 100
                                                                                                                                                                               + @EndDay
                                                                                                                                                                              )
                                                                                                                                                                          )
                                                                                                                                                                  ,112
                                                                                                                                                               );
        --Base Case 
        WITH A
        AS ( SELECT se.PrgVerID
                   ,se.PrgVerDescrip
                   ,se.ProgramHours
                   ,se.ProgramCredits
                   ,se.SSN
                   ,se.PhoneNumber
                   ,se.Email
                   ,se.Student
                   ,se.LeadId
                   ,se.StuEnrollId
                   ,se.EnrollmentID
                   ,se.Status
                   ,se.SysStatusId
                   ,se.DropReasonId
                   ,se.StartDate
                   ,se.ContractedGradDate
                   ,se.ExpectedGradDate
                   ,se.LDA
                   ,se.LicensureWrittenAllParts
                   ,se.LicensureLastPartWrittenOn
                   ,se.LicensurePassedAllParts
                   ,se.AllowsMoneyOwed
                   ,se.AllowsIncompleteReq
             FROM   (
                    SELECT *
                          ,ROW_NUMBER() OVER ( PARTITION BY LeadId
                                                           ,PrgVerID
                                                           ,SysStatusId
                                               ORDER BY StartDate DESC
                                             ) RN
                    FROM   @Enrollments
                    ) se
             --Is currently attending the school or on LOA or Suspended or is enrolled but have not yet started
             WHERE  se.SysStatusId IN ( @FutureStart, @CurrentlyAttending, @Suspension, @LOA )
                    AND se.RN = 1
                    AND se.StuEnrollId NOT IN ((
                                               SELECT     se.TransferredProgram
                                               FROM       @Enrollments se
                                               INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
                                               WHERE      (
                                                          (
                                                          se.TotalTransferHours IS NOT NULL
                                                          AND se.TotalTransferHours <> 0
                                                          )
                                                          AND (( se.TransferHoursFromProgram / pe.ProgramHours ) = 1 )
                                                          )
                                               UNION
                                               SELECT     pe.TransferredProgram
                                               FROM       @Enrollments se
                                               INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
                                               WHERE      (
                                                          (
                                                          se.TotalTransferHours IS NOT NULL
                                                          AND se.TotalTransferHours <> 0
                                                          )
                                                          AND (( se.TransferHoursFromProgram / pe.ProgramHours ) <> 1 )
                                                          )
                                               )
                                              ))

            --Was enrolled in two different NACCAS approved programs in the same school and graduated/licensed/placed from both the programs in the same report period
            ,B
        AS ( SELECT se.PrgVerID
                   ,se.PrgVerDescrip
                   ,se.ProgramHours
                   ,se.ProgramCredits
                   ,se.SSN
                   ,se.PhoneNumber
                   ,se.Email
                   ,se.Student
                   ,se.LeadId
                   ,se.StuEnrollId
                   ,se.EnrollmentID
                   ,se.Status
                   ,se.SysStatusId
                   ,se.DropReasonId
                   ,se.StartDate
                   ,se.ContractedGradDate
                   ,se.ExpectedGradDate
                   ,se.LDA
                   ,se.LicensureWrittenAllParts
                   ,se.LicensureLastPartWrittenOn
                   ,se.LicensurePassedAllParts
                   ,se.AllowsMoneyOwed
                   ,se.AllowsIncompleteReq
             FROM   (
                    SELECT *
                          ,COUNT(1) OVER ( PARTITION BY LeadId
                                                       ,SysStatusId
                                         ) AS cnt
                    FROM   @Enrollments
                    ) se
             WHERE  se.SysStatusId IN ( @Graduated ))
            ,C
        AS (
           --Has dropped from a program whose length is less than one academic year of 900 hours after being enrolled for more than 15 calendar days
           --Has dropped from a program whose length is equal to or more than one academic year of 900 hours after being enrolled for more than 30 calendar DAYS
           SELECT se.PrgVerID
                 ,se.PrgVerDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND (
                      (
                      se.ProgramHours < 900
                      AND DATEDIFF(DAY, se.StartDate, se.LDA) > 15
                      AND NOT EXISTS (
                                     SELECT     *
                                     FROM       dbo.syNACCASDropReasonsMapping nm
                                     INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                                     WHERE      ns.CampusId = @CampusId
                                                AND se.DropReasonId = nm.ADVDropReasonId
                                                AND nm.NACCASDropReasonId IN ( @CallToActiveDuty, @MilitaryTransfer, @Deceased, @TemporarilyDisabled
                                                                              ,@PermanentlyDisabled, @TransferredToEquivalent
                                                                             )
                                     )
                      )
                      OR (
                         se.ProgramHours >= 900
                         AND DATEDIFF(DAY, se.StartDate, se.LDA) > 30
                         AND NOT EXISTS (
                                        SELECT     *
                                        FROM       dbo.syNACCASDropReasonsMapping nm
                                        INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                                        WHERE      ns.CampusId = @CampusId
                                                   AND se.DropReasonId = nm.ADVDropReasonId
                                                   AND nm.NACCASDropReasonId IN ( @CallToActiveDuty, @MilitaryTransfer, @Deceased, @TemporarilyDisabled
                                                                                 ,@PermanentlyDisabled, @TransferredToEquivalent
                                                                                )
                                        )
                         )
                      )
                  AND se.StuEnrollId NOT IN ((
                                             SELECT     se.TransferredProgram
                                             FROM       @Enrollments se
                                             INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
                                             WHERE      (
                                                        (
                                                        se.TotalTransferHours IS NOT NULL
                                                        AND se.TotalTransferHours <> 0
                                                        )
                                                        AND (( se.TransferHoursFromProgram / pe.ProgramHours ) = 1 )
                                                        )
                                             )
                                            ))
            --Was transferred from the school for which the report is generated to another school and both the schools are a branch of the same parent.
            -- Such a student would be included in the report generated for the original school as did not complete
            ,D
        AS ( SELECT     se.PrgVerID
                       ,se.PrgVerDescrip
                       ,se.ProgramHours
                       ,se.ProgramCredits
                       ,se.SSN
                       ,se.PhoneNumber
                       ,se.Email
                       ,se.Student
                       ,se.LeadId
                       ,se.StuEnrollId
                       ,se.EnrollmentID
                       ,se.Status
                       ,se.SysStatusId
                       ,se.DropReasonId
                       ,se.StartDate
                       ,se.ContractedGradDate
                       ,se.ExpectedGradDate
                       ,se.LDA
                       ,se.LicensureWrittenAllParts
                       ,se.LicensureLastPartWrittenOn
                       ,se.LicensurePassedAllParts
                       ,se.AllowsMoneyOwed
                       ,se.AllowsIncompleteReq
             FROM       @Enrollments se
             INNER JOIN dbo.arTrackTransfer tt ON se.StuEnrollId = tt.StuEnrollId
             INNER JOIN dbo.syCampuses srcCmp ON srcCmp.CampusId = tt.SourceCampusId
             INNER JOIN dbo.syCampuses targetCmp ON targetCmp.CampusId = tt.TargetCampusId
             INNER JOIN dbo.arPrgVersions targetPV ON targetPV.PrgVerId = tt.TargetPrgVerId
             WHERE      se.SysStatusId IN ( @Transfer )
                        AND ((
                             srcCmp.IsBranch = 1
                             AND targetCmp.IsBranch = 1
                             AND srcCmp.ParentCampusId <> targetCmp.ParentCampusId
                             )
                            ))
            ,E
        AS (
           -- select from PE id not all hours are transffered else select from SE
           SELECT     se.PrgVerID
                     ,se.PrgVerDescrip
                     ,se.ProgramHours
                     ,se.ProgramCredits
                     ,se.SSN
                     ,se.PhoneNumber
                     ,se.Email
                     ,se.Student
                     ,se.LeadId
                     ,se.StuEnrollId
                     ,se.EnrollmentID
                     ,se.Status
                     ,se.SysStatusId
                     ,se.DropReasonId
                     ,se.StartDate
                     ,se.ContractedGradDate
                     ,se.ExpectedGradDate
                     ,se.LDA
                     ,se.LicensureWrittenAllParts
                     ,se.LicensureLastPartWrittenOn
                     ,se.LicensurePassedAllParts
                     ,se.AllowsMoneyOwed
                     ,se.AllowsIncompleteReq
           FROM       @Enrollments se
           INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
           WHERE      (
                      (
                      se.TotalTransferHours IS NOT NULL
                      AND se.TotalTransferHours <> 0
                      )
                      AND (( se.TransferHoursFromProgram / pe.ProgramHours ) = 1 )
                      )
           UNION
           SELECT     pe.PrgVerID
                     ,pe.PrgVerDescrip
                     ,pe.ProgramHours
                     ,pe.ProgramCredits
                     ,pe.SSN
                     ,pe.PhoneNumber
                     ,pe.Email
                     ,pe.Student
                     ,pe.LeadId
                     ,pe.StuEnrollId
                     ,pe.EnrollmentID
                     ,pe.Status
                     ,pe.SysStatusId
                     ,pe.DropReasonId
                     ,pe.StartDate
                     ,pe.ContractedGradDate
                     ,pe.ExpectedGradDate
                     ,pe.LDA
                     ,pe.LicensureWrittenAllParts
                     ,pe.LicensureLastPartWrittenOn
                     ,pe.LicensurePassedAllParts
                     ,pe.AllowsMoneyOwed
                     ,pe.AllowsIncompleteReq
           FROM       @Enrollments se
           INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
           WHERE      (
                      (
                      se.TotalTransferHours IS NOT NULL
                      AND se.TotalTransferHours <> 0
                      )
                      AND (( se.TransferHoursFromProgram / pe.ProgramHours ) <> 1 )
                      ))
        SELECT    allData.PrgVerID
                 ,allData.PrgVerDescrip
                 ,allData.ProgramHours
                 ,allData.ProgramCredits
                 ,allData.SSN
                 ,(
                  SELECT STUFF(STUFF(STUFF(allData.PhoneNumber, 1, 0, ''(''), 5, 0, '')''), 9, 0, ''-'')
                  ) AS PhoneNumber
                 ,allData.Email
                 ,allData.Student
                 ,allData.LeadId
                 ,allData.StuEnrollId
                 ,allData.EnrollmentID
                 ,allData.Status
                 ,allData.SysStatusId
                 ,allData.DropReasonId
                 ,allData.StartDate
                 ,allData.ContractedGradDate
                 ,allData.ExpectedGradDate
                 ,allData.LDA
                 ,allData.LicensureWrittenAllParts
                 ,allData.LicensureLastPartWrittenOn
                 ,allData.LicensurePassedAllParts
                 ,allData.AllowsMoneyOwed
                 ,allData.AllowsIncompleteReq
                 ,allData.Graduated
                 ,allData.OwesMoney
                 ,allData.MissingRequired
                 ,allData.GraduatedProgram
                 ,allData.DateGraduated
                 ,allData.PlacementStatus
                 ,allData.IneligibilityReason
                 ,allData.Placed
                 ,allData.SatForAllExamParts
                 ,allData.PassedAllParts
                 ,ISNULL(pemp.EmployerDescrip, '''') AS EmployerName
                 ,ISNULL(pemp.Address1 + '' '' + pemp.Address2, '''') AS EmployerAddress
                 ,ISNULL(pemp.City, '''') AS EmployerCity
                 ,ISNULL(states.StateDescrip, '''') AS EmployerState
                 ,ISNULL(pemp.Zip, '''') AS EmployerZip
                 ,ISNULL(pemp.Phone, '''') AS EmployerPhone
        FROM      (
                  SELECT *
                        ,(
                         SELECT CASE WHEN satForExam.SatForAllExamParts = ''Y'' THEN CASE WHEN satForExam.LicensurePassedAllParts = 1 THEN ''Y''
                                                                                        ELSE ''N''
                                                                                   END
                                     ELSE ''N/A''
                                END
                         ) PassedAllParts
                  FROM   (
                         SELECT *
                               ,(
                                SELECT CASE WHEN placed.GraduatedProgram = ''Y'' THEN
                                                CASE WHEN placed.LicensureWrittenAllParts = 0 THEN ''N''
                                                     ELSE
                                                         CASE WHEN @ReportType = @Preliminary THEN
                                                                  CASE WHEN placed.LicensureLastPartWrittenOn < CONVERT(
                                                                                                                           DATETIME
                                                                                                                          ,CONVERT(
                                                                                                                                      VARCHAR(50)
                                                                                                                                     ,(( @ReportYear ) * 10000 + 3
                                                                                                                                       * 100 + 30
                                                                                                                                      )
                                                                                                                                  )
                                                                                                                          ,112
                                                                                                                       ) THEN ''Y''
                                                                       ELSE ''N''
                                                                  END
                                                              ELSE
                                                                  CASE WHEN placed.LicensureLastPartWrittenOn < CONVERT(
                                                                                                                           DATETIME
                                                                                                                          ,CONVERT(
                                                                                                                                      VARCHAR(50)
                                                                                                                                     ,(( @ReportYear ) * 10000 + 11
                                                                                                                                       * 100 + 30
                                                                                                                                      )
                                                                                                                                  )
                                                                                                                          ,112
                                                                                                                       ) THEN ''Y''
                                                                       ELSE ''N''
                                                                  END
                                                         END
                                                END
                                            ELSE ''N/A''
                                       END
                                ) AS SatForAllExamParts
                         FROM   (
                                SELECT *
                                      ,(
                                       SELECT CASE WHEN placementStat.PlacementStatus = ''E'' THEN
                                                       CASE WHEN EXISTS (
                                                                        SELECT     1
                                                                        FROM       dbo.PlStudentsPlaced
                                                                        INNER JOIN dbo.plFldStudy ON plFldStudy.FldStudyId = PlStudentsPlaced.FldStudyId
                                                                        WHERE      dbo.PlStudentsPlaced.StuEnrollId = placementStat.StuEnrollId
                                                                                   AND (
                                                                                       dbo.plFldStudy.FldStudyCode = ''Yes''
                                                                                       OR dbo.plFldStudy.FldStudyCode = ''Related''
                                                                                       )
                                                                        ) THEN ''Y''
                                                            ELSE ''N''
                                                       END
                                                   ELSE ''N/A''
                                              END
                                       ) AS Placed
                                FROM   (
                                       SELECT *
                                             ,(
                                              SELECT CASE WHEN graduated.GraduatedProgram = ''Y'' THEN CONVERT(VARCHAR, graduated.ExpectedGradDate, 101)
                                                          ELSE ''N/A''
                                                     END
                                              ) AS DateGraduated
                                             ,(
                                              SELECT CASE WHEN graduated.GraduatedProgram = ''Y'' THEN
                                                              CASE WHEN (
                                                                        SELECT   TOP 1 Eligible
                                                                        FROM     dbo.plExitInterview
                                                                        WHERE    EnrollmentId = graduated.StuEnrollId
                                                                        ORDER BY COALESCE(ExitInterviewDate, graduated.ExpectedGradDate) DESC
                                                                        ) = ''Yes'' THEN ''E''
                                                                   ELSE ''I''
                                                              END
                                                          ELSE ''N/A''
                                                     END
                                              ) AS PlacementStatus
                                             ,(
                                              SELECT   TOP 1 COALESCE(Reason, '''')
                                              FROM     dbo.plExitInterview
                                              WHERE    EnrollmentId = graduated.StuEnrollId
                                              ORDER BY COALESCE(ExitInterviewDate, graduated.ExpectedGradDate) DESC
                                              ) AS IneligibilityReason
                                       FROM   (
                                              SELECT *
                                                    ,(
                                                     SELECT CASE WHEN didGraduate.Graduated = 0 THEN ''N''
                                                                 ELSE CASE WHEN didGraduate.OwesMoney = 1
                                                                                AND didGraduate.AllowsMoneyOwed = 0 THEN ''N''
                                                                           WHEN didGraduate.MissingRequired = 1
                                                                                AND didGraduate.AllowsIncompleteReq = 0 THEN ''N''
                                                                           ELSE ''Y''
                                                                      END
                                                            END
                                                     ) AS GraduatedProgram
                                              FROM   (
                                                     SELECT *
                                                           ,(
                                                            SELECT CASE WHEN @ReportType = @Preliminary THEN
                                                                            CASE WHEN result.ExpectedGradDate < CONVERT(
                                                                                                                           DATETIME
                                                                                                                          ,CONVERT(
                                                                                                                                      VARCHAR(50)
                                                                                                                                     ,(( @ReportYear ) * 10000 + 3 * 100 + 30 )
                                                                                                                                  )
                                                                                                                          ,112
                                                                                                                       ) THEN 1
                                                                                 ELSE 0
                                                                            END
                                                                        ELSE
                                                                            CASE WHEN result.ExpectedGradDate < CONVERT(
                                                                                                                           DATETIME
                                                                                                                          ,CONVERT(
                                                                                                                                      VARCHAR(50)
                                                                                                                                     ,(( @ReportYear ) * 10000 + 11 * 100 + 30 )
                                                                                                                                  )
                                                                                                                          ,112
                                                                                                                       ) THEN 1
                                                                                 ELSE 0
                                                                            END
                                                                   END
                                                            ) AS Graduated
                                                           ,dbo.DoesEnrollmentHavePendingBalance(result.StuEnrollId) AS OwesMoney
                                                           ,~ ( dbo.CompletedRequiredCourses(result.StuEnrollId)) AS MissingRequired
                                                     FROM   (
                                                            SELECT *
                                                            FROM   A
                                                            UNION
                                                            SELECT *
                                                            FROM   B
                                                            UNION
                                                            SELECT *
                                                            FROM   C
                                                            UNION
                                                            SELECT *
                                                            FROM   D
                                                            UNION
                                                            SELECT *
                                                            FROM   E
                                                            ) AS result
                                                     WHERE  (
                                                            @PrgIdList IS NULL
                                                            OR result.PrgVerID IN (
                                                                                  SELECT Val
                                                                                  FROM   MultipleValuesForReportParameters(@PrgIdList, '','', 1)
                                                                                  )
                                                            )
                                                     ) didGraduate
                                              ) graduated
                                       ) placementStat
                                ) placed
                         ) satForExam
                  ) allData
        LEFT JOIN dbo.PlStudentsPlaced psp ON psp.StuEnrollId = allData.StuEnrollId
        LEFT JOIN dbo.plEmployerJobs pej ON pej.EmployerJobId = psp.EmployerJobId
        LEFT JOIN dbo.plEmployers pemp ON pemp.EmployerId = pej.EmployerId
        LEFT JOIN dbo.syStates states ON states.StateId = pemp.StateId
        ORDER BY  allData.PrgVerDescrip
                 ,allData.Student;



    END;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_NACCAS_EnrollmentCount]'
GO
IF OBJECT_ID(N'[dbo].[USP_NACCAS_EnrollmentCount]', 'P') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[USP_NACCAS_EnrollmentCount]
    @PrgIdList VARCHAR(MAX) = NULL
   ,@CampusId VARCHAR(50)
   ,@ReportYear INT
AS
    DECLARE @StartMonth INT = 1
           ,@StartDay INT = 1
           ,@EndMonth INT = 12
           ,@EndDay INT = 31
           ,@FutureStart INT = 7
           ,@CurrentlyAttending INT = 9
           ,@LOA INT = 10
           ,@Suspension INT = 11
           ,@Dropped INT = 12
           ,@Graduated INT = 14
           ,@Transfer INT = 19;
    BEGIN


        DECLARE @Enrollments TABLE
            (
                PrgVerID UNIQUEIDENTIFIER
               ,PrgVerDescrip VARCHAR(50)
               ,ProgramHours DECIMAL(9, 2)
               ,ProgramCredits DECIMAL(18, 2)
               ,SSN VARCHAR(50)
               ,Student VARCHAR(50)
               ,LeadId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,EnrollmentID NVARCHAR(50)
               ,Status VARCHAR(80)
               ,SysStatusId INT
               ,StartDate DATETIME
               ,LDA DATETIME
               ,StudentEnrolledAsOf BIT
               ,StudentStartedTrainingIn BIT
               ,StudentStartedTrainingBetween BIT
               ,RN INT
               ,cnt INT
            );

        DECLARE @EnrollmentsWithLastStatus TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,SysStatusId INT
               ,StatusCodeDescription VARCHAR(80)
            );
        INSERT INTO @EnrollmentsWithLastStatus
                    SELECT lastStatusOfYear.StuEnrollId
                          ,lastStatusOfYear.SysStatusId
                          ,lastStatusOfYear.StatusCodeDescrip
                    FROM   (
                           SELECT     StuEnrollId
                                     ,syStatusCodes.SysStatusId
                                     ,StatusCodeDescrip
                                     ,DateOfChange
                                     ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId
                                                          ORDER BY DateOfChange DESC
                                                        ) rn
                           FROM       dbo.syStudentStatusChanges
                           INNER JOIN dbo.syStatusCodes ON syStatusCodes.StatusCodeId = syStudentStatusChanges.NewStatusId
                           WHERE      DateOfChange <= CONVERT(DATETIME, CONVERT(VARCHAR(50), (( @ReportYear ) * 10000 + 8 * 100 + 31 )), 112)
                           ) lastStatusOfYear
                    WHERE  rn = 1;

        INSERT INTO @Enrollments
                    SELECT *
                          ,ROW_NUMBER() OVER ( PARTITION BY enr.LeadId
                                                           ,enr.SysStatusId
                                               ORDER BY enr.StartDate DESC
                                             ) RN
                          ,COUNT(1) OVER ( PARTITION BY enr.LeadId
                                                       ,enr.SysStatusId
                                         ) AS cnt
                    FROM   (
                           SELECT     Prog.ProgId AS PrgVerID
                                     ,Prog.ProgDescrip AS PrgVerDescrip
                                     ,PV.Hours AS ProgramHours
                                     ,PV.Credits AS ProgramCredits
                                     ,LD.SSN
                                     ,LD.LastName + '', '' + LD.FirstName + '' '' + ISNULL(LD.MiddleName, '''') AS Student
                                     ,LD.LeadId
                                     ,SE.StuEnrollId
                                     ,LD.StudentNumber AS EnrollmentID
                                     ,[@EnrollmentsWithLastStatus].StatusCodeDescription AS Status
                                     ,[@EnrollmentsWithLastStatus].SysStatusId AS SysStatusId
                                     ,SE.StartDate AS StartDate
                                     ,dbo.GetLDA(SE.StuEnrollId) AS LDA
                                     ,CASE WHEN SE.StartDate <= CONVERT(
                                                                           DATETIME
                                                                          ,CONVERT(VARCHAR(50), (( @ReportYear - 2 ) * 10000 + @EndMonth * 100 + @EndDay ))
                                                                          ,112
                                                                       )
                                                AND 1 IN (
                                                         SELECT     dbo.sySysStatus.InSchool
                                                         FROM       dbo.syStatusCodes
                                                         INNER JOIN dbo.sySysStatus ON sySysStatus.SysStatusId = syStatusCodes.SysStatusId
                                                         WHERE      dbo.syStatusCodes.StatusCodeId = dbo.UDF_GetEnrollmentStatusIdAtGivenDate(
                                                                                                                                                 SE.StuEnrollId
                                                                                                                                                ,CONVERT(
                                                                                                                                                            DATETIME
                                                                                                                                                           ,CONVERT(
                                                                                                                                                                       VARCHAR(50)
                                                                                                                                                                      ,(( @ReportYear
                                                                                                                                                                          - 1
                                                                                                                                                                        )
                                                                                                                                                                        * 10000
                                                                                                                                                                        + @StartMonth
                                                                                                                                                                        * 100
                                                                                                                                                                        + @StartDay
                                                                                                                                                                       )
                                                                                                                                                                   )
                                                                                                                                                           ,112
                                                                                                                                                        )
                                                                                                                                             )
                                                         ) THEN 1
                                           ELSE 0
                                      END AS StudentEnrolledAsOf
                                     ,CASE WHEN DATEPART(YEAR, SE.StartDate) = @ReportYear - 1 THEN 1
                                           ELSE 0
                                      END AS StudentStartedTrainingIn
                                     ,CASE WHEN SE.StartDate >= CONVERT(DATETIME, CONVERT(VARCHAR(50), (( @ReportYear - 1 ) * 10000 + 9 * 100 + 1 )), 112)
                                                AND SE.StartDate <= CONVERT(DATETIME, CONVERT(VARCHAR(50), (( @ReportYear ) * 10000 + 8 * 100 + 31 )), 112) THEN 1
                                           ELSE 0
                                      END AS StudentStartedTrainingBetween
                           FROM       dbo.arStuEnrollments AS SE
                           INNER JOIN dbo.adLeads AS LD ON LD.LeadId = SE.LeadId
                           INNER JOIN dbo.arPrgVersions AS PV ON SE.PrgVerId = PV.PrgVerId
                           INNER JOIN dbo.arPrograms AS Prog ON Prog.ProgId = PV.ProgId
                           INNER JOIN dbo.syApprovedNACCASProgramVersion NaccasApproved ON NaccasApproved.ProgramVersionId = SE.PrgVerId
                           INNER JOIN dbo.syNaccasSettings NaccasSettings ON NaccasSettings.NaccasSettingId = NaccasApproved.NaccasSettingId
                           INNER JOIN @EnrollmentsWithLastStatus ON [@EnrollmentsWithLastStatus].StuEnrollId = SE.StuEnrollId
                           WHERE      SE.CampusId = @CampusId
                                      AND NaccasSettings.CampusId = @CampusId
                                      --that have a contracted graduation date between January 1 of reporting year minus 1 year and December 31st of reporting year minus 1 year
                                      -- Not a third party contract
                                      AND NOT SE.ThirdPartyContract = 1
                                      AND NaccasApproved.IsApproved = 1
                           ) enr
                    WHERE  enr.StudentEnrolledAsOf = 1
                           OR enr.StudentStartedTrainingIn = 1
                           OR enr.StudentStartedTrainingBetween = 1; --Base Case 

        WITH A
        AS ( SELECT *
             FROM   @Enrollments
             --Is currently attending the school or on LOA or Suspended or is enrolled but have not yet started
             -- Will handle multi NACCAS approved programs by selecting rn = 1
             WHERE  [@Enrollments].SysStatusId IN ( @FutureStart, @CurrentlyAttending, @Suspension, @LOA )
                    AND RN = 1 )
            --Was enrolled in two different NACCAS approved programs in the same school and graduated/licensed/placed from both the programs in the same report period
            ,B
        AS ( SELECT PrgVerID
                   ,PrgVerDescrip
                   ,ProgramHours
                   ,ProgramCredits
                   ,SSN
                   ,Student
                   ,LeadId
                   ,StuEnrollId
                   ,EnrollmentID
                   ,Status
                   ,SysStatusId
                   ,StartDate
                   ,LDA
                   ,StudentEnrolledAsOf
                   ,StudentStartedTrainingIn
                   ,StudentStartedTrainingBetween
                   ,RN
                   ,cnt
             FROM   (
                    SELECT *
                          ,ROW_NUMBER() OVER ( PARTITION BY [@Enrollments].LeadId
                                                           ,[@Enrollments].PrgVerID
                                               ORDER BY [@Enrollments].StartDate DESC
                                             ) AS RN1
                    FROM   @Enrollments
                    WHERE  [@Enrollments].SysStatusId IN ( @Graduated )
                    ) tbl
             WHERE  tbl.SysStatusId IN ( @Graduated )
                    AND tbl.RN1 = 1 )
            ,C
        AS (
           --Has dropped from a program whose length is less than one academic year of 900 hours after being enrolled for more than 15 calendar days
           --Has dropped from a program whose length is equal to or more than one academic year of 900 hours after being enrolled for more than 30 calendar DAYS
           SELECT *
           FROM   @Enrollments
           WHERE  [@Enrollments].SysStatusId IN ( @Dropped )
                  AND (
                      (
                      [@Enrollments].ProgramHours < 900
                      AND DATEDIFF(DAY, [@Enrollments].StartDate, [@Enrollments].LDA) > 15
                      )
                      OR (
                         [@Enrollments].ProgramHours >= 900
                         AND DATEDIFF(DAY, [@Enrollments].StartDate, [@Enrollments].LDA) > 30
                         )
                      ))
            --Was transferred from the school for which the report is generated to another school and both the schools are a branch of the same parent.
            -- Such a student would be included in the report generated for the original school as did not complete
            ,D
        AS ( SELECT     se.PrgVerID
                       ,se.PrgVerDescrip
                       ,se.ProgramHours
                       ,se.ProgramCredits
                       ,se.SSN
                       ,se.Student
                       ,se.LeadId
                       ,se.StuEnrollId
                       ,se.EnrollmentID
                       ,se.Status
                       ,se.SysStatusId
                       ,se.StartDate
                       ,se.LDA
                       ,se.StudentEnrolledAsOf
                       ,se.StudentStartedTrainingIn
                       ,se.StudentStartedTrainingBetween
                       ,se.RN
                       ,se.cnt
             FROM       @Enrollments se
             INNER JOIN dbo.arTrackTransfer tt ON se.StuEnrollId = tt.StuEnrollId
             INNER JOIN dbo.syCampuses srcCmp ON srcCmp.CampusId = tt.SourceCampusId
             INNER JOIN dbo.syCampuses targetCmp ON targetCmp.CampusId = tt.TargetCampusId
             WHERE      se.SysStatusId IN ( @Transfer )
                        AND (
                            srcCmp.IsBranch = 1
                            AND targetCmp.IsBranch = 1
                            )
                        AND ( srcCmp.ParentCampusId = targetCmp.ParentCampusId ))
        --Below the ''Students enrolled as of 12/31/<Reporting Year - 2 Years> & who remained enrolled as of January 1, <Re Year - 1 Year>'' column the total number of cells with a value "X" is displayed
        --Below the ''<Reporting Year - 1 Year> Year Starts Students who started training in <Reporting Year - 1 Year>'' column the total number of cells with a value "X" is displayed
        --Below the ''Students who started training between 9-01-<Reporting Year - 1 Year> & 8-31-<Reporting Year>'' column the total number of cells with a value "X" is displayed
        SELECT   result.PrgVerID
                ,result.PrgVerDescrip
                ,result.ProgramHours
                ,result.ProgramCredits
                ,result.SSN
                ,result.Student
                ,result.LeadId
                ,result.StuEnrollId
                ,result.EnrollmentID
                ,result.Status
                ,result.SysStatusId
                ,result.StartDate
                ,result.LDA
                ,CASE WHEN result.StudentEnrolledAsOf = 1 THEN ''true''
                      ELSE ''false''
                 END AS StudentEnrolledAsOf
                ,CASE WHEN result.StudentStartedTrainingIn = 1
                           AND result.StudentStartedTrainingBetween = 0 THEN ''true''
                      ELSE ''false''
                 END AS StudentStartedTrainingIn
                ,CASE WHEN result.StudentStartedTrainingBetween = 1 THEN ''true''
                      ELSE ''false''
                 END AS StudentStartedTrainingBetween
                ,result.RN
                ,result.cnt
        FROM     (
                 SELECT *
                 FROM   A
                 UNION
                 SELECT *
                 FROM   B
                 UNION
                 SELECT *
                 FROM   C
                 UNION
                 SELECT *
                 FROM   D
                 ) AS result
        WHERE    (
                 @PrgIdList IS NULL
                 OR result.PrgVerID IN (
                                       SELECT Val
                                       FROM   MultipleValuesForReportParameters(@PrgIdList, '','', 1)
                                       )
                 )
        ORDER BY result.PrgVerDescrip
                ,result.Student
                ,result.SSN
                ,result.EnrollmentID;
    END;






'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_NACCAS_ExemptionList]'
GO
IF OBJECT_ID(N'[dbo].[USP_NACCAS_ExemptionList]', 'P') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[USP_NACCAS_ExemptionList]
    @PrgIdList VARCHAR(MAX) = NULL
   ,@CampusId VARCHAR(50)
   ,@ReportYear INT
AS
    DECLARE @StartMonth INT = 1
           ,@StartDay INT = 1
           ,@EndMonth INT = 12
           ,@EndDay INT = 31
           ,@FutureStart INT = 7
           ,@CurrentlyAttending INT = 9
           ,@LOA INT = 10
           ,@Suspension INT = 11
           ,@Dropped INT = 12
           ,@Graduated INT = 14
           ,@Transfer INT = 19
           ,@DeceasedReason INT = 1
           ,@TempOrPermDisabledReason INT = 2
           ,@DeployedReason INT = 3
           ,@Transferred100Reason INT = 4
           ,@TransferredAccreditedReason INT = 5
           ,@EarlyWithdrawalReason INT = 6
           ,@MilitaryTransfer UNIQUEIDENTIFIER
           ,@CallToActiveDuty UNIQUEIDENTIFIER
           ,@Deceased UNIQUEIDENTIFIER
           ,@TemporarilyDisabled UNIQUEIDENTIFIER
           ,@PermanentlyDisabled UNIQUEIDENTIFIER
           ,@TransferredToEquivalent UNIQUEIDENTIFIER;
    BEGIN


        SET @MilitaryTransfer = (
                                SELECT TOP 1 NACCASDropReasonId
                                FROM   dbo.syNACCASDropReasons
                                WHERE  Name = ''Military Transfer''
                                );
        SET @CallToActiveDuty = (
                                SELECT TOP 1 NACCASDropReasonId
                                FROM   dbo.syNACCASDropReasons
                                WHERE  Name = ''Call to Active Duty''
                                );
        SET @Deceased = (
                        SELECT TOP 1 NACCASDropReasonId
                        FROM   dbo.syNACCASDropReasons
                        WHERE  Name = ''Deceased''
                        );
        SET @TemporarilyDisabled = (
                                   SELECT TOP 1 NACCASDropReasonId
                                   FROM   dbo.syNACCASDropReasons
                                   WHERE  Name = ''Temporarily disabled''
                                   );
        SET @PermanentlyDisabled = (
                                   SELECT TOP 1 NACCASDropReasonId
                                   FROM   dbo.syNACCASDropReasons
                                   WHERE  Name = ''Permanently disabled''
                                   );
        SET @TransferredToEquivalent = (
                                       SELECT TOP 1 NACCASDropReasonId
                                       FROM   dbo.syNACCASDropReasons
                                       WHERE  Name = ''Transferred to an equivalent program at another school with same accreditation''
                                       );

        DECLARE @Enrollments TABLE
            (
                PrgID UNIQUEIDENTIFIER
               ,PrgDescrip VARCHAR(50)
               ,ProgramHours DECIMAL(9, 2)
               ,ProgramCredits DECIMAL(18, 2)
               ,TotalTransferHours DECIMAL(18, 2)
               ,TransferredProgram UNIQUEIDENTIFIER
               ,TransferHoursFromProgram DECIMAL(18, 2)
               ,PrgVerId UNIQUEIDENTIFIER
               ,SSN VARCHAR(50)
               ,PhoneNumber VARCHAR(50)
               ,Email VARCHAR(100)
               ,Student VARCHAR(50)
               ,LeadId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,EnrollmentID NVARCHAR(50)
               ,Status VARCHAR(80)
               ,SysStatusId INT
               ,DropReasonId UNIQUEIDENTIFIER
               ,StartDate DATETIME
               ,ContractedGradDate DATETIME
               ,ExpectedGradDate DATETIME
               ,ReEnrollmentDate DATETIME
               ,LDA DATETIME
               ,LicensureWrittenAllParts BIT
               ,LicensureLastPartWrittenOn DATETIME
               ,LicensurePassedAllParts BIT
               ,AllowsMoneyOwed BIT
               ,AllowsIncompleteReq BIT
               ,ThirdPartyContract BIT
               ,RN INT
               ,cnt INT
            );

        DECLARE @EnrollmentsWithLastStatus TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,SysStatusId INT
               ,DropReasonId UNIQUEIDENTIFIER
               ,StatusCodeDescription VARCHAR(80)
            );
        INSERT INTO @EnrollmentsWithLastStatus
                    SELECT lastStatusOfYear.StuEnrollId
                          ,lastStatusOfYear.SysStatusId
                          ,lastStatusOfYear.DropReasonId
                          ,lastStatusOfYear.StatusCodeDescrip
                    FROM   (
                           SELECT     StuEnrollId
                                     ,syStatusCodes.SysStatusId
                                     ,StatusCodeDescrip
                                     ,DateOfChange
                                     ,DropReasonId
                                     ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId
                                                          ORDER BY DateOfChange DESC
                                                        ) rn
                           FROM       dbo.syStudentStatusChanges
                           INNER JOIN dbo.syStatusCodes ON syStatusCodes.StatusCodeId = syStudentStatusChanges.NewStatusId
                           WHERE      DATEPART(YEAR, DateOfChange) <= ( @ReportYear - 1 )
                           ) lastStatusOfYear
                    WHERE  rn = 1;


        INSERT INTO @Enrollments
                    SELECT *
                          ,ROW_NUMBER() OVER ( PARTITION BY enr.LeadId
                                                           ,enr.PrgID
                                                           ,enr.SysStatusId
                                               ORDER BY enr.StartDate DESC
                                             ) RN
                          ,COUNT(1) OVER ( PARTITION BY enr.LeadId
                                                       ,enr.PrgVerId
                                         ) AS ReenrollmentCount
                    FROM   (
                           SELECT     Prog.ProgId AS PrgID
                                     ,Prog.ProgDescrip AS PrgDescrip
                                     ,PV.Hours AS ProgramHours
                                     ,PV.Credits AS ProgramCredits
                                     ,SE.TransferHours
                                     ,SE.TransferHoursFromThisSchoolEnrollmentId
                                     ,SE.TotalTransferHoursFromThisSchool
                                     ,PV.PrgVerId AS PrgVerId
                                     ,LD.SSN
                                     ,ISNULL((
                                             SELECT   TOP 1 Phone
                                             FROM     dbo.adLeadPhone
                                             WHERE    LeadId = LD.LeadId
                                                      AND (
                                                          IsBest = 1
                                                          OR IsShowOnLeadPage = 1
                                                          )
                                             ORDER BY Position
                                             )
                                            ,''''
                                            ) AS PhoneNumber
                                     ,ISNULL((
                                             SELECT   TOP 1 EMail
                                             FROM     dbo.AdLeadEmail
                                             WHERE    LeadId = LD.LeadId
                                                      AND (
                                                          IsPreferred = 1
                                                          OR IsShowOnLeadPage = 1
                                                          )
                                             ORDER BY IsPreferred DESC
                                                     ,IsShowOnLeadPage DESC
                                             )
                                            ,''''
                                            ) AS Email
                                     ,LD.LastName + '', '' + LD.FirstName + '' '' + ISNULL(LD.MiddleName, '''') AS Student
                                     ,LD.LeadId
                                     ,SE.StuEnrollId
                                     ,LD.StudentNumber AS EnrollmentID
                                     ,[@EnrollmentsWithLastStatus].StatusCodeDescription AS Status
                                     ,[@EnrollmentsWithLastStatus].SysStatusId AS SysStatusId
                                     ,[@EnrollmentsWithLastStatus].DropReasonId AS DropReasonId
                                     ,SE.StartDate AS StartDate
                                     ,SE.ContractedGradDate AS ContractedGradDate
                                     ,dbo.GetGraduatedDate(SE.StuEnrollId) AS ExpectedGradDate
                                     ,SE.ReEnrollmentDate AS ReEnrollmentDate
                                     ,dbo.GetLDA(SE.StuEnrollId) AS LDA
                                     ,SE.LicensureWrittenAllParts AS LicensureWrittenAllParts
                                     ,SE.LicensureLastPartWrittenOn AS LicensureLastPartWrittenOn
                                     ,SE.LicensurePassedAllParts AS LicensurePassedAllParts
                                     ,camp.AllowGraduateAndStillOweMoney AS AllowsMoneyOwed
                                     ,camp.AllowGraduateWithoutFullCompletion AS AllowsIncompleteReq
                                     ,SE.ThirdPartyContract AS ThirdPartyContract
                           FROM       dbo.arStuEnrollments AS SE
                           INNER JOIN dbo.adLeads AS LD ON LD.LeadId = SE.LeadId
                           INNER JOIN dbo.arPrgVersions AS PV ON SE.PrgVerId = PV.PrgVerId
                           INNER JOIN dbo.arPrograms AS Prog ON Prog.ProgId = PV.ProgId
                           INNER JOIN dbo.syApprovedNACCASProgramVersion NaccasApproved ON NaccasApproved.ProgramVersionId = SE.PrgVerId
                           INNER JOIN dbo.syNaccasSettings NaccasSettings ON NaccasSettings.NaccasSettingId = NaccasApproved.NaccasSettingId
                           INNER JOIN dbo.syCampuses AS camp ON camp.CampusId = SE.CampusId
                           INNER JOIN @EnrollmentsWithLastStatus ON [@EnrollmentsWithLastStatus].StuEnrollId = SE.StuEnrollId
                           WHERE      SE.CampusId = @CampusId
                                      AND NaccasSettings.CampusId = @CampusId
                                      --that have a revised graduation date between January 1 of reporting year minus 1 year and December 31st of reporting year minus 1 year

                                      AND NaccasApproved.IsApproved = 1
                                      AND SE.ContractedGradDate
                                      BETWEEN CONVERT(DATETIME, CONVERT(VARCHAR(50), (( @ReportYear - 1 ) * 10000 + @StartMonth * 100 + @StartDay )), 112) AND CONVERT(
                                                                                                                                                                          DATETIME
                                                                                                                                                                         ,CONVERT(
                                                                                                                                                                                     VARCHAR(50)
                                                                                                                                                                                    ,(( @ReportYear
                                                                                                                                                                                        - 1
                                                                                                                                                                                      )
                                                                                                                                                                                      * 10000
                                                                                                                                                                                      + @EndMonth
                                                                                                                                                                                      * 100
                                                                                                                                                                                      + @EndDay
                                                                                                                                                                                     )
                                                                                                                                                                                 )
                                                                                                                                                                         ,112
                                                                                                                                                                      )
                           ) enr;



        WITH A
        AS (
           --Has dropped from a program whose length is less than one academic year of 900 hours after being enrolled for less than or equal to 15 calendar days
           --Has dropped from a program whose length is equal to or more than one academic year of 900 hours after being enrolled for less than or equal to 30 calendar DAYS
           SELECT se.PrgID
                 ,se.PrgDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
                 ,@EarlyWithdrawalReason AS ReasonNumber
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND (
                      (
                      se.ProgramHours < 900
                      AND DATEDIFF(DAY, se.StartDate, se.LDA) <= 15
                      )
                      OR (
                         se.ProgramHours >= 900
                         AND DATEDIFF(DAY, se.StartDate, se.LDA) <= 30
                         )
                      ))
            ,B
        AS (
           --exists in naccas drop reason
           SELECT se.PrgID
                 ,se.PrgDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
                 ,@DeployedReason AS ReasonNumber
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND ( EXISTS (
                               SELECT     *
                               FROM       dbo.syNACCASDropReasonsMapping nm
                               INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                               WHERE      ns.CampusId = @CampusId
                                          AND se.DropReasonId = nm.ADVDropReasonId
                                          AND nm.NACCASDropReasonId IN ( @MilitaryTransfer, @CallToActiveDuty )
                               )
                      ))
            ,C
        AS (
           --exists in naccas drop reason
           SELECT se.PrgID
                 ,se.PrgDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
                 ,@DeceasedReason AS ReasonNumber
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND ( EXISTS (
                               SELECT     *
                               FROM       dbo.syNACCASDropReasonsMapping nm
                               INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                               WHERE      ns.CampusId = @CampusId
                                          AND se.DropReasonId = nm.ADVDropReasonId
                                          AND nm.NACCASDropReasonId IN ( @Deceased )
                               )
                      ))
            ,D
        AS (
           --exists in naccas drop reason
           SELECT se.PrgID
                 ,se.PrgDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
                 ,@TempOrPermDisabledReason AS ReasonNumber
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND ( EXISTS (
                               SELECT     *
                               FROM       dbo.syNACCASDropReasonsMapping nm
                               INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                               WHERE      ns.CampusId = @CampusId
                                          AND se.DropReasonId = nm.ADVDropReasonId
                                          AND nm.NACCASDropReasonId IN ( @TemporarilyDisabled, @PermanentlyDisabled )
                               )
                      ))
            ,E
        AS (
           --exists in naccas drop reason
           SELECT se.PrgID
                 ,se.PrgDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
                 ,@TransferredAccreditedReason AS ReasonNumber
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND ( EXISTS (
                               SELECT     *
                               FROM       dbo.syNACCASDropReasonsMapping nm
                               INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                               WHERE      ns.CampusId = @CampusId
                                          AND se.DropReasonId = nm.ADVDropReasonId
                                          AND nm.NACCASDropReasonId IN ( @TransferredToEquivalent )
                               )
                      ))
            ,F
        AS ( SELECT     pe.PrgID
                       ,pe.PrgDescrip
                       ,pe.ProgramHours
                       ,pe.ProgramCredits
                       ,pe.SSN
                       ,pe.PhoneNumber
                       ,pe.Email
                       ,pe.Student
                       ,pe.LeadId
                       ,pe.StuEnrollId
                       ,pe.EnrollmentID
                       ,pe.Status
                       ,pe.SysStatusId
                       ,pe.DropReasonId
                       ,pe.StartDate
                       ,pe.ContractedGradDate
                       ,pe.ExpectedGradDate
                       ,pe.LDA
                       ,pe.LicensureWrittenAllParts
                       ,pe.LicensureLastPartWrittenOn
                       ,pe.LicensurePassedAllParts
                       ,pe.AllowsMoneyOwed
                       ,pe.AllowsIncompleteReq
                       ,@Transferred100Reason AS ReasonNumber
             FROM       @Enrollments se
             INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
             WHERE      (
                        (
                        se.TotalTransferHours IS NOT NULL
                        AND se.TotalTransferHours <> 0
                        )
                        AND (( se.TransferHoursFromProgram / pe.ProgramHours ) = 1 )
                        ))
        SELECT   *
        FROM     (
                 SELECT *
                       ,ROW_NUMBER() OVER ( PARTITION BY resultByProg.LeadId
                                            ORDER BY resultByProg.LeadId
                                          ) numOfReasons
                 FROM   (
                        SELECT result.PrgID
                              ,result.PrgDescrip
                              ,result.ProgramHours
                              ,result.ProgramCredits
                              ,result.SSN
                              ,result.Student
                              ,result.LeadId
                              ,result.StuEnrollId
                              ,result.EnrollmentID
                              ,result.Status
                              ,result.SysStatusId
                              ,result.StartDate
                              ,result.LDA
                              ,result.ReasonNumber
                        FROM   (
                               SELECT *
                               FROM   A
                               UNION
                               SELECT *
                               FROM   B
                               UNION
                               SELECT *
                               FROM   C
                               UNION
                               SELECT *
                               FROM   D
                               UNION
                               SELECT *
                               FROM   E
                               UNION
                               SELECT *
                               FROM   F
                               ) AS result
                        WHERE  (
                               @PrgIdList IS NULL
                               OR result.PrgID IN (
                                                  SELECT Val
                                                  FROM   MultipleValuesForReportParameters(@PrgIdList, '','', 1)
                                                  )
                               )
                        ) resultByProg
                 ) distinctResults
        WHERE    distinctResults.numOfReasons = 1
        ORDER BY distinctResults.Student;


    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_NACCAS_Summary]'
GO
IF OBJECT_ID(N'[dbo].[USP_NACCAS_Summary]', 'P') IS NULL
EXEC sp_executesql N'

CREATE PROCEDURE [dbo].[USP_NACCAS_Summary]
    @PrgIdList VARCHAR(MAX) = NULL
   ,@CampusId VARCHAR(50)
   ,@ReportYear INT
   ,@ReportType VARCHAR(50)
AS
    BEGIN

           DECLARE @Summary TABLE
            (
                ProgramId UNIQUEIDENTIFIER
               ,ProgramDescrip VARCHAR(50)
               ,ProgramHours DECIMAL(9, 2)
               ,ProgramCredits DECIMAL(18, 2)
               ,IsClockHour BIT
               ,TotalStudentsEnrolledAsOf INT
               ,TotalStudentsStartedTrainingIn INT
               ,TotalStudentsStartedTrainingBetween INT
               ,Item1 INT
               ,Item2 INT
               ,Item3 INT
               ,Item4 INT
               ,Item5 INT
               ,Item6 INT
            );
        DECLARE @EnrollmentCounts TABLE
            (
                PrgID UNIQUEIDENTIFIER
               ,PrgDescrip VARCHAR(50)
               ,ProgramHours DECIMAL(9, 2)
               ,ProgramCredits DECIMAL(18, 2)
               ,SSN VARCHAR(50)
               ,Student VARCHAR(50)
               ,LeadId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,EnrollmentID NVARCHAR(50)
               ,Status VARCHAR(80)
               ,SysStatusId INT
               ,StartDate DATETIME
               ,LDA DATETIME
               ,StudentEnrolledAsOf BIT
               ,StudentStartedTrainingIn BIT
               ,StudentStartedTrainingBetween BIT
               ,RN INT
               ,cnt INT
            );

        DECLARE @CohortGrid TABLE
            (
                PrgID UNIQUEIDENTIFIER
               ,PrgDescrip VARCHAR(50)
               ,ProgramHours DECIMAL(9, 2)
               ,ProgramCredits DECIMAL(18, 2)
               ,SSN VARCHAR(50)
               ,PhoneNumber VARCHAR(50)
               ,Email VARCHAR(100)
               ,Student VARCHAR(50)
               ,LeadId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,EnrollmentID NVARCHAR(50)
               ,Status VARCHAR(80)
               ,SysStatusId INT
               ,DropReasonId UNIQUEIDENTIFIER
               ,StartDate DATETIME
               ,ContractedGradDate DATETIME
               ,ExpectedGradDate DATETIME
               ,LDA DATETIME
               ,LicensureWrittenAllParts BIT
               ,LicensureLastPartWrittenOn DATETIME
               ,LicensurePassedAllParts BIT
               ,AllowsMoneyOwed BIT
               ,AllowsIncompleteReq BIT
               ,Graduated BIT
               ,OwesMoney BIT
               ,MissingRequired BIT
               ,GraduatedProgram VARCHAR(5)
               ,DateGraduated VARCHAR(50)
               ,PlacementStatus VARCHAR(5)
               ,IneligibilityReason VARCHAR(100)
               ,Placed VARCHAR(5)
               ,SatForAllExamParts VARCHAR(5)
               ,PassedAllParts VARCHAR(5)
               ,EmployerName VARCHAR(150)
               ,EmployerAddress VARCHAR(150)
               ,EmployerCity VARCHAR(100)
               ,EmployerState VARCHAR(100)
               ,EmployerZip VARCHAR(25)
               ,EmployerPhone VARCHAR(25)
            );

        INSERT INTO @EnrollmentCounts (
                                      PrgID
                                     ,PrgDescrip
                                     ,ProgramHours
                                     ,ProgramCredits
                                     ,SSN
                                     ,Student
                                     ,LeadId
                                     ,StuEnrollId
                                     ,EnrollmentID
                                     ,Status
                                     ,SysStatusId
                                     ,StartDate
                                     ,LDA
                                     ,StudentEnrolledAsOf
                                     ,StudentStartedTrainingIn
                                     ,StudentStartedTrainingBetween
                                     ,RN
                                     ,cnt
                                      )
        EXEC dbo.USP_NACCAS_EnrollmentCount @PrgIdList = @PrgIdList -- varchar(max)
                                           ,@CampusId = @CampusId      -- varchar(50)
                                           ,@ReportYear = @ReportYear; -- int


        INSERT INTO @CohortGrid (
                                PrgID
                               ,PrgDescrip
                               ,ProgramHours
                               ,ProgramCredits
                               ,SSN
                               ,PhoneNumber
                               ,Email
                               ,Student
                               ,LeadId
                               ,StuEnrollId
                               ,EnrollmentID
                               ,Status
                               ,SysStatusId
                               ,DropReasonId
                               ,StartDate
                               ,ContractedGradDate
                               ,ExpectedGradDate
                               ,LDA
                               ,LicensureWrittenAllParts
                               ,LicensureLastPartWrittenOn
                               ,LicensurePassedAllParts
                               ,AllowsMoneyOwed
                               ,AllowsIncompleteReq
                               ,Graduated
                               ,OwesMoney
                               ,MissingRequired
                               ,GraduatedProgram
                               ,DateGraduated
                               ,PlacementStatus
                               ,IneligibilityReason
                               ,Placed
                               ,SatForAllExamParts
                               ,PassedAllParts
                               ,EmployerName
                               ,EmployerAddress
                               ,EmployerCity
                               ,EmployerState
                               ,EmployerZip
                               ,EmployerPhone
                                )
        EXEC dbo.USP_NACCAS_CohortGrid @PrgIdList = @PrgIdList    -- varchar(max)
                                      ,@CampusId = @CampusId      -- varchar(50)
                                      ,@ReportYear = @ReportYear  -- int
                                      ,@ReportType = @ReportType; -- varchar(50)

									  --SELECT * FROM @CohortGrid

        DECLARE @numOfEnrollments TABLE
            (
                prgId UNIQUEIDENTIFIER
               ,numOfEnrollments INT
            );
        INSERT INTO @numOfEnrollments (
                                      prgId
                                     ,numOfEnrollments
                                      )
                    (SELECT   cg.PrgID
                             ,COUNT(*)
                     FROM     @CohortGrid cg
                     GROUP BY cg.PrgID);


       
                                       
                                            WITH summ AS ( SELECT     *
                                                                ,CASE WHEN (
                                                                           SELECT AC.ACId
                                                                           FROM   dbo.syAcademicCalendars AC
                                                                           WHERE  AC.ACId = prog.ACId
                                                                           ) = 5 THEN 1
                                                                      ELSE 0
                                                                 END AS IsClockHour
                                                                ,ROW_NUMBER() OVER ( PARTITION BY ecCG.PrgID
                                                                                                 ,ecCG.PrgDescrip
                                                                                     ORDER BY ecCG.PrgID
                                                                                   ) AS RN
                                                      FROM       (
                                                                 SELECT ec.PrgID
                                                                       ,ec.PrgDescrip
                                                                       ,ec.ProgramHours
                                                                       ,ec.ProgramCredits
                                                                       ,ec.StudentEnrolledAsOf
                                                                       ,ec.StudentStartedTrainingIn
                                                                       ,ec.StudentStartedTrainingBetween
                                                                       ,NULL AS LicensureWrittenAllParts
                                                                       ,NULL AS LicensureLastPartWrittenOn
                                                                       ,NULL AS LicensurePassedAllParts
                                                                       ,NULL AS Graduated
                                                                       ,NULL AS GraduatedProgram
                                                                       ,NULL AS DateGraduated
                                                                       ,NULL AS PlacementStatus
                                                                       ,NULL AS IneligibilityReason
                                                                       ,NULL AS Placed
                                                                       ,NULL AS SatForAllExamParts
                                                                       ,NULL AS PassedAllParts
                                                                 FROM   @EnrollmentCounts ec
                                                                 UNION ALL
                                                                 SELECT CG.PrgID
                                                                       ,CG.PrgDescrip
                                                                       ,CG.ProgramHours
                                                                       ,CG.ProgramCredits
                                                                       ,NULL AS StudentEnrolledAsOf
                                                                       ,NULL AS StudentStartedTrainingIn
                                                                       ,NULL AS StudentStartedTrainingBetween
                                                                       ,CG.LicensureWrittenAllParts
                                                                       ,CG.LicensureLastPartWrittenOn
                                                                       ,CG.LicensurePassedAllParts
                                                                       ,CG.Graduated
                                                                       ,CG.GraduatedProgram
                                                                       ,CG.DateGraduated
                                                                       ,CG.PlacementStatus
                                                                       ,CG.IneligibilityReason
                                                                       ,CG.Placed
                                                                       ,CG.SatForAllExamParts
                                                                       ,CG.PassedAllParts
                                                                 FROM   @CohortGrid CG
                                                                 ) AS ecCG
                                                      INNER JOIN dbo.arPrograms prog ON ecCG.PrgID = prog.ProgId
													  ), programLength as
													  (SELECT summ.ProgId, summ.ProgramHours AS ProgramLengthHours, summ.ProgramCredits AS ProgramLengthCredits FROM summ WHERE rn = 1)


													   INSERT INTO @Summary (
                             ProgramId
                            ,ProgramDescrip
                            ,ProgramHours
                            ,ProgramCredits
                            ,IsClockHour
                            ,TotalStudentsEnrolledAsOf
                            ,TotalStudentsStartedTrainingIn
                            ,TotalStudentsStartedTrainingBetween
                            ,Item1
                            ,Item2
                            ,Item3
                            ,Item4
                            ,Item5
                            ,Item6
                             )
                    SELECT     summaryResult.ProgId
                              ,summaryResult.PrgDescrip
                              ,summaryResult.ProgramLengthHours
                              ,summaryResult.ProgramLengthCredits
                              ,summaryResult.IsClockHour
                              ,summaryResult.TotalStudentsEnrolledAsOf
                              ,summaryResult.TotalStudentsStartedTrainingIn
                              ,summaryResult.TotalStudentsStartedTrainingBetween
                              ,[@numOfEnrollments].numOfEnrollments
                              ,summaryResult.numOfStudentsGraduated
                              ,summaryResult.numOfStudentsEligible
                              ,summaryResult.numOfStudentsPlaced
                              ,summaryResult.numOfStudentsSatForAllExams
                              ,summaryResult.numOfStudentsPassedAllParts
                    FROM       (
                               SELECT   summed.ProgId
                                       ,summed.PrgDescrip
                                     ,summed.ProgramLengthHours
									 ,summed.ProgramLengthCredits
									 ,summed.IsClockHour
                                       ,SUM(   CASE WHEN summed.StudentEnrolledAsOf = 1 THEN 1
                                                    ELSE 0
                                               END
                                           ) TotalStudentsEnrolledAsOf
                                       ,SUM(   CASE WHEN summed.StudentStartedTrainingIn = 1 THEN 1
                                                    ELSE 0
                                               END
                                           ) TotalStudentsStartedTrainingIn
                                       ,SUM(   CASE WHEN summed.StudentStartedTrainingBetween = 1 THEN 1
                                                    ELSE 0
                                               END
                                           ) TotalStudentsStartedTrainingBetween
                                       ,SUM(   CASE WHEN summed.GraduatedProgram = ''Y'' THEN 1
                                                    ELSE 0
                                               END
                                           ) AS numOfStudentsGraduated
                                       ,SUM(   CASE WHEN summed.PlacementStatus = ''E'' THEN 1
                                                    ELSE 0
                                               END
                                           ) AS numOfStudentsEligible
                                       ,SUM(   CASE WHEN summed.Placed = ''Y'' THEN 1
                                                    ELSE 0
                                               END
                                           ) AS numOfStudentsPlaced
                                       ,SUM(   CASE WHEN summed.SatForAllExamParts = ''Y'' THEN 1
                                                    ELSE 0
                                               END
                                           ) AS numOfStudentsSatForAllExams
                                       ,SUM(   CASE WHEN summed.PassedAllParts = ''Y'' THEN 1
                                                    ELSE 0
                                               END
                                           ) AS numOfStudentsPassedAllParts
                               FROM     (


										  SELECT programLength.ProgId
                                              ,summ.PrgDescrip
                                              ,programLength.ProgramLengthHours
                                              ,programLength.ProgramLengthCredits
                                              ,summ.IsClockHour
                                              ,summ.StudentEnrolledAsOf
                                              ,summ.StudentStartedTrainingIn
                                              ,summ.StudentStartedTrainingBetween
                                              ,summ.GraduatedProgram
                                              ,summ.PlacementStatus
                                              ,summ.Placed
                                              ,summ.SatForAllExamParts
                                              ,summ.PassedAllParts
											  FROM programLength
											  INNER JOIN summ ON summ.ProgId = programLength.ProgId                                                    
									
                                        ) AS summed
                               GROUP BY summed.ProgId
                                       ,summed.PrgDescrip
									,summed.ProgramLengthHours
									 ,summed.ProgramLengthCredits
									 ,summed.IsClockHour
                               ) AS summaryResult
                    INNER JOIN @numOfEnrollments ON [@numOfEnrollments].prgId = summaryResult.ProgId;

        SELECT *
        FROM   @Summary;
    END;



'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]', 'P') IS NULL
EXEC sp_executesql N'-- =========================================================================================================
-- Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId
-- =========================================================================================================
CREATE PROCEDURE [dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]
    @StuEnrollIdList VARCHAR(MAX),
    @TermId VARCHAR(50) = NULL,
    @SysComponentTypeIdList VARCHAR(50) = NULL,
    @ReqId VARCHAR(50),
    @SysComponentTypeId VARCHAR(10) = NULL,
    @ShowIncomplete BIT = 1
AS
BEGIN
    DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
    DECLARE @SetGradeBookAt VARCHAR(50);
    DECLARE @CharInt INT;
    DECLARE @Counter AS INT;
    DECLARE @times AS INT;
    DECLARE @TermStartDate1 AS DATETIME;
    DECLARE @Score AS DECIMAL(18, 2);
    DECLARE @GrdCompDescrip AS VARCHAR(50);


    DECLARE @curId AS UNIQUEIDENTIFIER;
    DECLARE @curReqId AS UNIQUEIDENTIFIER;
    DECLARE @curStuEnrollId AS UNIQUEIDENTIFIER;
    DECLARE @curDescrip AS VARCHAR(50);
    DECLARE @curNumber AS INT;
    DECLARE @curGrdComponentTypeId AS INT;
    DECLARE @curMinResult AS DECIMAL(18, 2);
    DECLARE @curGrdComponentDescription AS VARCHAR(50);
    DECLARE @curClsSectionId AS UNIQUEIDENTIFIER;
    DECLARE @curRownumber AS INT;

    DECLARE @ExecutedSproc BIT = 0;

    CREATE TABLE #Temp21
    (
        Id UNIQUEIDENTIFIER,
        TermId UNIQUEIDENTIFIER,
        ReqId UNIQUEIDENTIFIER,
        GradeBookDescription VARCHAR(50),
        Number INT,
        GradeBookSysComponentTypeId INT,
        GradeBookScore DECIMAL(18, 2),
        MinResult DECIMAL(18, 2),
        GradeComponentDescription VARCHAR(50),
        ClsSectionId UNIQUEIDENTIFIER,
        StuEnrollId UNIQUEIDENTIFIER,
        RowNumber INT
    );
    CREATE TABLE #Temp22
    (
        ReqId UNIQUEIDENTIFIER,
        EffectiveDate DATETIME
    );

    IF @SysComponentTypeIdList IS NULL
    BEGIN
        SET @SysComponentTypeIdList = ''501,544,502,499,503,500,533'';
    END;

    SET @StuEnrollCampusId = COALESCE(
                             (
                                 SELECT TOP 1
                                        ASE.CampusId
                                 FROM dbo.arStuEnrollments AS ASE
                                 WHERE EXISTS
                                 (
                                     SELECT Val
                                     FROM dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                     WHERE Val = ASE.StuEnrollId
                                 )
                             ),
                             NULL
                                     );
    SET @SetGradeBookAt = (dbo.GetAppSettingValueByKeyName(''GradeBookWeightingLevel'', @StuEnrollCampusId));
    IF (@SetGradeBookAt IS NOT NULL)
    BEGIN
        SET @SetGradeBookAt = LOWER(LTRIM(RTRIM(@SetGradeBookAt)));
    END;


    SET @CharInt =
    (
        SELECT CHARINDEX(@SysComponentTypeId, @SysComponentTypeIdList)
    );

    IF @SetGradeBookAt = ''instructorlevel''
    BEGIN
        SELECT CASE
                   WHEN GradeBookDescription IS NULL THEN
                       GradeComponentDescription
                   ELSE
                       GradeBookDescription
               END AS GradeBookDescription,
               MinResult,
               GradeBookScore,
               GradeComponentDescription,
               GradeBookSysComponentTypeId,
               StuEnrollId,
               rownumber
        FROM
        (
            SELECT AR2.ReqId,
                   AT.TermId,
                   CASE
                       WHEN AGBWD.Descrip IS NULL THEN
                           AGCT.Descrip
                       ELSE
                           AGBWD.Descrip
                   END AS GradeBookDescription,
                   (CASE
                        WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN
                            AGBWD.Number
                        ELSE
                    (
                        SELECT MIN(MinVal)
                        FROM arGradeScaleDetails GSD,
                             arGradeSystemDetails GSS
                        WHERE GSD.GrdSysDetailId = GSS.GrdSysDetailId
                              AND GSS.IsPass = 1
                              AND GSD.GrdScaleId = ACS.GrdScaleId
                    )
                    END
                   ) AS MinResult,
                   AGBWD.Required,
                   AGBWD.MustPass,
                   ISNULL(AGCT.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId,
                   AGBWD.Number,
                   SR.Resource AS GradeComponentDescription,
                   AGBWD.InstrGrdBkWgtDetailId,
                   ASE.StuEnrollId,
                   0 AS IsExternShip,
                   (CASE AGCT.SysComponentTypeId
                        WHEN 544 THEN
                        (
                            SELECT SUM(HoursAttended)
                            FROM arExternshipAttendance
                            WHERE StuEnrollId = ASE.StuEnrollId
                        )
                        ELSE
                    (
                        SELECT SUM(ISNULL(Score, 0))
                        FROM arGrdBkResults
                        WHERE StuEnrollId = ASE.StuEnrollId
                              AND InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                              AND ClsSectionId = ACS.ClsSectionId
                    )
                    END
                   ) AS GradeBookScore,
                   ROW_NUMBER() OVER (PARTITION BY ASE.StuEnrollId,
                                                   AT.TermId,
                                                   AR2.ReqId
                                      ORDER BY AGCT.SysComponentTypeId,
                                               AGBWD.Descrip
                                     ) AS rownumber
            FROM arGrdBkWgtDetails AS AGBWD
                INNER JOIN arGrdBkWeights AS AGBW
                    ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                INNER JOIN arClassSections AS ACS
                    ON ACS.InstrGrdBkWgtId = AGBW.InstrGrdBkWgtId
                INNER JOIN arResults AS AR
                    ON AR.TestId = ACS.ClsSectionId
                INNER JOIN arGrdComponentTypes AS AGCT
                    ON AGCT.GrdComponentTypeId = AGBWD.GrdComponentTypeId
                INNER JOIN arStuEnrollments AS ASE
                    ON ASE.StuEnrollId = AR.StuEnrollId
                INNER JOIN arTerm AS AT
                    ON AT.TermId = ACS.TermId
                INNER JOIN arReqs AS AR2
                    ON AR2.ReqId = ACS.ReqId
                INNER JOIN syResources AS SR
                    ON SR.ResourceID = AGCT.SysComponentTypeId
                LEFT JOIN arGrdBkResults AS AGBR
                    ON AGBR.StuEnrollId = ASE.StuEnrollId
                       AND AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                       AND AGBR.ClsSectionId = ACS.ClsSectionId
            WHERE AR.StuEnrollId IN
                  (
                      SELECT Val
                      FROM MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                  )
                  AND AT.TermId = @TermId
                  AND AR2.ReqId = @ReqId
                  AND
                  (
                      @SysComponentTypeIdList IS NULL
                      OR AGCT.SysComponentTypeId IN
                         (
                             SELECT Val
                             FROM MultipleValuesForReportParameters(@SysComponentTypeIdList, '','', 1)
                         )
                  )
            UNION
            SELECT AR2.ReqId,
                   AT.TermId,
                   CASE
                       WHEN AGBW.Descrip IS NULL THEN
                       (
                           SELECT Resource
                           FROM syResources
                           WHERE ResourceID = AGCT.SysComponentTypeId
                       )
                       ELSE
                           AGBW.Descrip
                   END AS GradeBookDescription,
                   (CASE
                        WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN
                            AGBWD.Number
                        ELSE
                    (
                        SELECT MIN(MinVal)
                        FROM arGradeScaleDetails GSD,
                             arGradeSystemDetails GSS
                        WHERE GSD.GrdSysDetailId = GSS.GrdSysDetailId
                              AND GSS.IsPass = 1
                              AND GSD.GrdScaleId = ACS.GrdScaleId
                    )
                    END
                   ) AS MinResult,
                   AGBWD.Required,
                   AGBWD.MustPass,
                   ISNULL(AGCT.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId,
                   AGBWD.Number,
                   (
                       SELECT Resource
                       FROM syResources
                       WHERE ResourceID = AGCT.SysComponentTypeId
                   ) AS GradeComponentDescription,
                   AGBWD.InstrGrdBkWgtDetailId,
                   ASE.StuEnrollId,
                   0 AS IsExternShip,
                   (CASE AGCT.SysComponentTypeId
                        WHEN 544 THEN
                        (
                            SELECT SUM(HoursAttended)
                            FROM arExternshipAttendance
                            WHERE StuEnrollId = ASE.StuEnrollId
                        )
                        ELSE
                    (
                        SELECT SUM(ISNULL(Score, 0))
                        FROM arGrdBkResults
                        WHERE StuEnrollId = ASE.StuEnrollId
                              AND InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                              AND ClsSectionId = ACS.ClsSectionId
                    )
                    END
                   ) AS GradeBookScore,
                   ROW_NUMBER() OVER (PARTITION BY ASE.StuEnrollId,
                                                   AT.TermId,
                                                   AR2.ReqId
                                      ORDER BY AGCT.SysComponentTypeId,
                                               AGCT.Descrip
                                     ) AS rownumber
            FROM arGrdBkConversionResults GBCR
                INNER JOIN arStuEnrollments AS ASE
                    ON ASE.StuEnrollId = GBCR.StuEnrollId
                INNER JOIN adLeads AS AST
                    ON AST.StudentId = ASE.StudentId
                INNER JOIN arPrgVersions AS APV
                    ON APV.PrgVerId = ASE.PrgVerId
                INNER JOIN arTerm AS AT
                    ON AT.TermId = GBCR.TermId
                INNER JOIN arReqs AS AR2
                    ON AR2.ReqId = GBCR.ReqId
                INNER JOIN arGrdComponentTypes AS AGCT
                    ON AGCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWgtDetails AS AGBWD
                    ON AGBWD.GrdComponentTypeId = AGCT.GrdComponentTypeId
                       AND AGBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWeights AS AGBW
                    ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                       AND AGBW.ReqId = GBCR.ReqId
                INNER JOIN
                (
                    SELECT ReqId,
                           MAX(EffectiveDate) AS EffectiveDate
                    FROM arGrdBkWeights AS AGBW
                    GROUP BY ReqId
                ) AS MaxEffectiveDatesByCourse
                    ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                INNER JOIN
                (
                    SELECT Resource,
                           ResourceID
                    FROM syResources
                    WHERE ResourceTypeID = 10
                ) SYRES
                    ON SYRES.ResourceID = AGCT.SysComponentTypeId
                INNER JOIN syCampuses AS SC
                    ON SC.CampusId = ASE.CampusId
                INNER JOIN arClassSections AS ACS
                    ON ACS.TermId = AT.TermId
                       AND ACS.ReqId = AR2.ReqId
                LEFT JOIN arGrdBkResults AS AGBR
                    ON AGBR.StuEnrollId = ASE.StuEnrollId
                       AND AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                       AND AGBR.ClsSectionId = ACS.ClsSectionId
            WHERE MaxEffectiveDatesByCourse.EffectiveDate <= AT.StartDate
                  AND ASE.StuEnrollId IN
                      (
                          SELECT Val
                          FROM MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                      )
                  AND AT.TermId = @TermId
                  AND AR2.ReqId = @ReqId
                  AND
                  (
                      @SysComponentTypeIdList IS NULL
                      OR AGCT.SysComponentTypeId IN
                         (
                             SELECT Val
                             FROM MultipleValuesForReportParameters(@SysComponentTypeIdList, '','', 1)
                         )
                  )
        ) dt
        WHERE (
                  @SysComponentTypeId IS NULL
                  OR dt.GradeBookSysComponentTypeId = @SysComponentTypeId
              )
              AND
              (
                  @ShowIncomplete = 1
                  OR
                  (
                      @ShowIncomplete = 0
                      AND dt.GradeBookScore IS NOT NULL
                  )
              )
        ORDER BY GradeComponentDescription,
                 rownumber,
                 GradeBookDescription;
    END;
    ELSE
    BEGIN

        SET @TermStartDate1 =
        (
            SELECT AT.StartDate FROM dbo.arTerm AS AT WHERE AT.TermId = @TermId
        );
        INSERT INTO #Temp22
        SELECT AGBW.ReqId,
               MAX(AGBW.EffectiveDate) AS EffectiveDate
        FROM dbo.arGrdBkWeights AS AGBW
            INNER JOIN dbo.syCreditSummary AS SCS
                ON SCS.ReqId = AGBW.ReqId
        WHERE SCS.TermId = @TermId
              AND SCS.StuEnrollId IN
                  (
                      SELECT Val
                      FROM dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                  )
              AND AGBW.EffectiveDate <= @TermStartDate1
        GROUP BY AGBW.ReqId;


        DECLARE getUsers_Cursor CURSOR FOR
        SELECT dt.ID,
               dt.ReqId,
               dt.Descrip,
               dt.Number,
               dt.SysComponentTypeId,
               dt.MinResult,
               dt.GradeComponentDescription,
               dt.ClsSectionId,
               dt.StuEnrollId,
               ROW_NUMBER() OVER (PARTITION BY dt.StuEnrollId,
                                               @TermId,
                                               dt.SysComponentTypeId
                                  ORDER BY dt.SysComponentTypeId,
                                           dt.Descrip
                                 ) AS rownumber
        FROM
        (
            SELECT ISNULL(AGBWD.InstrGrdBkWgtDetailId, NEWID()) AS ID,
                   AR.ReqId,
                   AGBWD.Descrip,
                   AGBWD.Number,
                   AGCT.SysComponentTypeId,
                   (CASE
                        WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN
                            AGBWD.Number
                        ELSE
                    (
                        SELECT MIN(AGSD.MinVal) AS MinVal
                        FROM dbo.arGradeScaleDetails AS AGSD
                            INNER JOIN dbo.arGradeSystemDetails AS AGSDetails
                                ON AGSDetails.GrdSysDetailId = AGSD.GrdSysDetailId
                        WHERE AGSDetails.IsPass = 1
                              AND AGSD.GrdScaleId = ACS.GrdScaleId
                    )
                    END
                   ) AS MinResult,
                   SR.Resource AS GradeComponentDescription,
                   ACS.ClsSectionId,
                   AGBR.StuEnrollId
            FROM dbo.arGrdBkResults AS AGBR
                INNER JOIN dbo.arGrdBkWgtDetails AS AGBWD
                    ON AGBWD.InstrGrdBkWgtDetailId = AGBR.InstrGrdBkWgtDetailId
                INNER JOIN dbo.arGrdBkWeights AS AGBW
                    ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                INNER JOIN dbo.arGrdComponentTypes AS AGCT
                    ON AGCT.GrdComponentTypeId = AGBWD.GrdComponentTypeId
                INNER JOIN dbo.syResources AS SR
                    ON SR.ResourceID = AGCT.SysComponentTypeId
                --                             INNER JOIN #Temp22 AS T2 ON T2.ReqId = AGBW.ReqId
                INNER JOIN dbo.arReqs AS AR
                    ON AR.ReqId = AGBW.ReqId
                INNER JOIN dbo.arClassSections AS ACS
                    ON ACS.ReqId = AR.ReqId
                       AND ACS.ClsSectionId = AGBR.ClsSectionId
                INNER JOIN dbo.arResults AS AR2
                    ON AR2.TestId = ACS.ClsSectionId
                       AND AR2.StuEnrollId = AGBR.StuEnrollId --AND AR2.TestId = AGBR.ClsSectionId

            WHERE (
                      @StuEnrollIdList IS NULL
                      OR AGBR.StuEnrollId IN
                         (
                             SELECT Val
                             FROM dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                         )
                  )
                  --                                        AND T2.EffectiveDate = AGBW.EffectiveDate
                  AND AGBWD.Number > 0
                  AND AR.ReqId = @ReqId
        ) dt
        ORDER BY SysComponentTypeId,
                 rownumber;

        DECLARE @schoolTypeSetting VARCHAR(50);
        SET @schoolTypeSetting = dbo.GetAppSettingValueByKeyName(''gradesformat'', NULL);


        OPEN getUsers_Cursor;
        FETCH NEXT FROM getUsers_Cursor
        INTO @curId,
             @curReqId,
             @curDescrip,
             @curNumber,
             @curGrdComponentTypeId,
             @curMinResult,
             @curGrdComponentDescription,
             @curClsSectionId,
             @curStuEnrollId,
             @curRownumber;
        SET @Counter = 0;
        WHILE @@FETCH_STATUS = 0
        BEGIN
            --PRINT @Number; 

            IF @schoolTypeSetting = ''Numeric''
            BEGIN
                SET @times = 1;

                SET @GrdCompDescrip = @curDescrip;
                IF (@curGrdComponentTypeId <> 500)
                BEGIN
                    SET @Score =
                    (
                        SELECT TOP 1
                               Score
                        FROM arGrdBkResults
                        WHERE StuEnrollId = @curStuEnrollId
                              AND InstrGrdBkWgtDetailId = @curId
                              AND ResNum = @times
                    );
                    IF @Score IS NULL
                    BEGIN
                        SET @Score =
                        (
                            SELECT TOP 1
                                   Score
                            FROM arGrdBkResults
                            WHERE StuEnrollId = @curStuEnrollId
                                  AND InstrGrdBkWgtDetailId = @curId
                                  AND ResNum = (@times - 1)
                        );
                    END;

                    IF @curGrdComponentTypeId = 544
                    BEGIN
                        SET @Score =
                        (
                            SELECT SUM(HoursAttended)
                            FROM arExternshipAttendance
                            WHERE StuEnrollId = @curStuEnrollId
                        );
                    END;
                    INSERT INTO #Temp21
                    VALUES
                    (@curId, @TermId, @curReqId, @GrdCompDescrip, @curNumber, @curGrdComponentTypeId, @Score,
                     @curMinResult, @curGrdComponentDescription, @curClsSectionId, @curStuEnrollId, @curRownumber);
                END;
                ELSE
                BEGIN
                    IF @curGrdComponentTypeId = 500
                       AND @ExecutedSproc = 0
                    BEGIN


                        DECLARE @Services TABLE
                        (
                            Descrip VARCHAR(50),
                            Required DECIMAL(18, 2) NULL,
                            Completed INT NULL,
                            Remaining DECIMAL(18, 2) NULL,
                            Average DECIMAL(18, 2) NULL,
                            EnrollDate DATETIME NULL,
                            InstrGrdBkWgtDetailId UNIQUEIDENTIFIER,
                            ClsSectionId UNIQUEIDENTIFIER,
							Seq INT NULL
                        );


                        INSERT INTO @Services
                        (
                            Descrip,
                            Required,
                            Completed,
                            Remaining,
                            Average,
                            EnrollDate,
                            InstrGrdBkWgtDetailId,
                            ClsSectionId,
							Seq
                        )
                        EXEC dbo.usp_GetClassroomWorkServices @StuEnrollId = @curStuEnrollId,   -- uniqueidentifier
                                                              @ClsSectionId = @curClsSectionId, -- uniqueidentifier
                                                              @GradeBookComponentType = 500;    -- int



                        INSERT INTO #Temp21
                        SELECT @curId,
                               @TermId,
                               @curReqId,
                               LTRIM(RTRIM(Descrip)) + '' ('' + CAST(Completed AS VARCHAR(10)) + ''/''
                               + CAST(Required AS VARCHAR(10)) + '')'',
                               @curNumber,
                               @curGrdComponentTypeId,
                               Average,
                               [Required],
                               @curGrdComponentDescription,
                               @curClsSectionId,
                               @curStuEnrollId,
                               @curRownumber
                        FROM @Services;


                        SET @ExecutedSproc = 1;
                    END;
                END;



            END;
            ELSE
            BEGIN

                SET @times = 1;
                WHILE @times <= @curNumber
                BEGIN
                    --PRINT @times; 
                    IF @curNumber > 1
                    BEGIN
                        SET @GrdCompDescrip = @curDescrip + CAST(@times AS CHAR);

                        IF @curGrdComponentTypeId = 544
                        BEGIN
                            SET @Score =
                            (
                                SELECT SUM(HoursAttended)
                                FROM arExternshipAttendance
                                WHERE StuEnrollId = @curStuEnrollId
                            );
                        END;
                        ELSE
                        BEGIN
                            SET @Score =
                            (
                                SELECT Score
                                FROM arGrdBkResults
                                WHERE StuEnrollId = @curStuEnrollId
                                      AND InstrGrdBkWgtDetailId = @curId
                                      AND ResNum = @times
                                      AND ClsSectionId = @curClsSectionId
                            );
                        END;


                        SET @curRownumber = @times;
                    END;
                    ELSE
                    BEGIN
                        SET @GrdCompDescrip = @curDescrip;
                        SET @Score =
                        (
                            SELECT TOP 1
                                   Score
                            FROM arGrdBkResults
                            WHERE StuEnrollId = @curStuEnrollId
                                  AND InstrGrdBkWgtDetailId = @curId
                                  AND ResNum = @times
                        );
                        IF @Score IS NULL
                        BEGIN
                            SET @Score =
                            (
                                SELECT TOP 1
                                       Score
                                FROM arGrdBkResults
                                WHERE StuEnrollId = @curStuEnrollId
                                      AND InstrGrdBkWgtDetailId = @curId
                                      AND ResNum = (@times - 1)
                            );
                        END;

                        IF @curGrdComponentTypeId = 544
                        BEGIN
                            SET @Score =
                            (
                                SELECT SUM(HoursAttended)
                                FROM arExternshipAttendance
                                WHERE StuEnrollId = @curStuEnrollId
                            );
                        END;

                    END;
                    --PRINT @Score; 
                    INSERT INTO #Temp21
                    VALUES
                    (@curId, @TermId, @curReqId, @GrdCompDescrip, @curNumber, @curGrdComponentTypeId, @Score,
                     @curMinResult, @curGrdComponentDescription, @curClsSectionId, @curStuEnrollId, @curRownumber);

                    SET @times = @times + 1;
                END;

            END;
            FETCH NEXT FROM getUsers_Cursor
            INTO @curId,
                 @curReqId,
                 @curDescrip,
                 @curNumber,
                 @curGrdComponentTypeId,
                 @curMinResult,
                 @curGrdComponentDescription,
                 @curClsSectionId,
                 @curStuEnrollId,
                 @curRownumber;
        END;
        CLOSE getUsers_Cursor;
        DEALLOCATE getUsers_Cursor;

        SET @CharInt =
        (
            SELECT CHARINDEX(@SysComponentTypeId, @SysComponentTypeIdList)
        );

        IF (@SysComponentTypeIdList IS NULL)
           OR (@CharInt >= 1)
        BEGIN
            SELECT Id,
                   --, StuEnrollId 
                   TermId,
                   GradeBookDescription,
                   Number,
                   GradeBookSysComponentTypeId,
                   GradeBookScore,
                   MinResult,
                   GradeComponentDescription,
                   RowNumber,
                   ClsSectionId
            FROM #Temp21
            WHERE GradeBookSysComponentTypeId = @SysComponentTypeId
			AND
              (
                  @ShowIncomplete = 1
                  OR
                  (
                      @ShowIncomplete = 0
                      AND GradeBookScore IS NOT NULL
                  )
              )
            UNION
            SELECT GBWD.InstrGrdBkWgtDetailId,
                   --, SE.StuEnrollId 
                   T.TermId,
                   GCT.Descrip AS GradeBookDescription,
                   GBWD.Number,
                   GCT.SysComponentTypeId,
                   GBCR.Score,
                   GBCR.MinResult,
                   SYRES.Resource,
                   ROW_NUMBER() OVER (PARTITION BY --SE.StuEnrollId,  
                                          T.TermId,
                                          GCT.SysComponentTypeId
                                      ORDER BY GCT.SysComponentTypeId,
                                               GCT.Descrip
                                     ) AS rownumber,
                   (
                       SELECT TOP 1
                              ClsSectionId
                       FROM arClassSections
                       WHERE TermId = T.TermId
                             AND ReqId = R.ReqId
                   ) AS ClsSectionId
            FROM arGrdBkConversionResults GBCR
                INNER JOIN arStuEnrollments SE
                    ON GBCR.StuEnrollId = SE.StuEnrollId
                INNER JOIN
                (SELECT StudentId, FirstName, LastName, MiddleName FROM adLeads) S
                    ON S.StudentId = SE.StudentId
                INNER JOIN arPrgVersions PV
                    ON SE.PrgVerId = PV.PrgVerId
                INNER JOIN arTerm T
                    ON GBCR.TermId = T.TermId
                INNER JOIN arReqs R
                    ON GBCR.ReqId = R.ReqId
                INNER JOIN arGrdComponentTypes GCT
                    ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWgtDetails GBWD
                    ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                       AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWeights GBW
                    ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                       AND GBCR.ReqId = GBW.ReqId
                INNER JOIN
                (
                    SELECT ReqId,
                           MAX(EffectiveDate) AS EffectiveDate
                    FROM arGrdBkWeights
                    GROUP BY ReqId
                ) AS MaxEffectiveDatesByCourse
                    ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                INNER JOIN
                (
                    SELECT Resource,
                           ResourceID
                    FROM syResources
                    WHERE ResourceTypeID = 10
                ) SYRES
                    ON SYRES.ResourceID = GCT.SysComponentTypeId
                INNER JOIN syCampuses C
                    ON SE.CampusId = C.CampusId
            WHERE MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                  AND SE.StuEnrollId IN
                      (
                          SELECT Val
                          FROM MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                      )
                  AND T.TermId = @TermId
                  AND R.ReqId = @ReqId
                  AND
                  (
                      @SysComponentTypeIdList IS NULL
                      OR GCT.SysComponentTypeId IN
                         (
                             SELECT Val
                             FROM MultipleValuesForReportParameters(@SysComponentTypeIdList, '','', 1)
                         )
                  )
                  AND
                  (
                      @SysComponentTypeId IS NULL
                      OR GCT.SysComponentTypeId = @SysComponentTypeId
                  )
				  AND
              (
                  @ShowIncomplete = 1
                  OR
                  (
                      @ShowIncomplete = 0
                      AND GBCR.Score IS NOT NULL
                  )
              )
            ORDER BY GradeBookSysComponentTypeId,
                     GradeBookDescription,
                     RowNumber;
        END;
    END;
    DROP TABLE #Temp22;
    DROP TABLE #Temp21;
END;
-- =========================================================================================================
-- END  --  Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId
-- =========================================================================================================


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_Report_CreateParamDetail]'
GO
IF OBJECT_ID(N'[dbo].[USP_Report_CreateParamDetail]', 'P') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[USP_Report_CreateParamDetail]
    @ItemId AS INT
   ,@SectionId AS INT
   ,@Sequence AS INT
-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| ''user_name''
AS
    IF NOT EXISTS (
                  SELECT TOP 1 DetailId
                  FROM   dbo.ParamDetail
                  WHERE  SectionId = @SectionId
                         AND ItemId = @ItemId
                  )
        BEGIN
            INSERT INTO dbo.ParamDetail (
                                        SectionId
                                       ,ItemId
                                       ,CaptionOverride
                                       ,ItemSeq
                                        )
            VALUES ( @SectionId        -- SectionId - bigint
                    ,@ItemId           -- ItemId - bigint
                    ,N''Report Options'' -- CaptionOverride - nvarchar(50)
                    ,@Sequence         -- ItemSeq - int
                );
        END;

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_Report_CreateParamItem]'
GO
IF OBJECT_ID(N'[dbo].[USP_Report_CreateParamItem]', 'P') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[USP_Report_CreateParamItem]
    @Caption AS VARCHAR(MAX)
   ,@ViewNameNoExtension AS VARCHAR(MAX)

-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| ''user_name''
AS
    DECLARE @Return_ItemId INT = 0;
    IF NOT EXISTS (
                  SELECT TOP 1 ItemId
                  FROM   dbo.ParamItem
                  WHERE  Caption = @Caption
                  )
        BEGIN

            DECLARE @ControllerClass VARCHAR(MAX) = @ViewNameNoExtension + ''.ascx'';
            DECLARE @ReturnValue VARCHAR(MAX) = @ViewNameNoExtension + ''Option'';
            INSERT INTO dbo.ParamItem (
                                      Caption
                                     ,ControllerClass
                                     ,valueprop
                                     ,ReturnValueName
                                     ,IsItemOverriden
                                      )
            VALUES ( @Caption         -- Caption - nvarchar(50)
                    ,@ControllerClass -- ControllerClass - nvarchar(50)
                    ,0                -- valueprop - int
                    ,@ReturnValue     -- ReturnValueName - nvarchar(50)
                    ,NULL             -- IsItemOverriden - bit
                );
        END;

    SET @Return_ItemId = (
                         SELECT TOP 1 ItemId
                         FROM   dbo.ParamItem
                         WHERE  Caption = @Caption
                         );

    RETURN @Return_ItemId;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_Report_CreateParamSection]'
GO
IF OBJECT_ID(N'[dbo].[USP_Report_CreateParamSection]', 'P') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[USP_Report_CreateParamSection]
    @SectionName AS VARCHAR(MAX)
   ,@Caption AS VARCHAR(MAX)
   ,@SetId AS INT,@Sequence AS INT,
   @Description AS VARCHAR(Max)
-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| ''user_name''
AS
    DECLARE @Return_Section INT = 0;
     IF NOT EXISTS (
                  SELECT TOP 1 SectionId
                  FROM   dbo.ParamSection
                  WHERE  SectionName = @SectionName
                  )
        BEGIN
            INSERT INTO dbo.ParamSection (
                                         SectionName
                                        ,SectionCaption
                                        ,SetId
                                        ,SectionSeq
                                        ,SectionDescription
                                        ,SectionType
                                         )
            VALUES ( @SectionName                              -- SectionName - nvarchar(50)
                    ,@Caption                             -- SectionCaption - nvarchar(100)
                    ,@SetId                                -- SetId - bigint
                    ,@Sequence                                                      -- SectionSeq - int
                    ,@Description -- SectionDescription - nvarchar(500)
                    ,0                                                      -- SectionType - int
                );
        END;

    SET @Return_Section = (
                                      SELECT TOP 1 SectionId
                                      FROM   dbo.ParamSection
                                      WHERE  SectionName = @SectionName
                                      );



    RETURN @Return_Section;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_Report_CreateParamSet]'
GO
IF OBJECT_ID(N'[dbo].[USP_Report_CreateParamSet]', 'P') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[USP_Report_CreateParamSet]
    @SetName AS VARCHAR(MAX)
   ,@ReportName AS VARCHAR(MAX)
   ,@Type AS VARCHAR(MAX)
-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| ''user_name''
AS
    DECLARE @Return_Set INT = 0;
    IF NOT EXISTS (
                  SELECT TOP 1 SetId
                  FROM   dbo.ParamSet
                  WHERE  SetName = @SetName
                  )
        BEGIN
            DECLARE @FilterSet VARCHAR(MAX) = @ReportName + ''Filter Set'';
            INSERT INTO dbo.ParamSet (
                                     SetName
                                    ,SetDisplayName
                                    ,SetDescription
                                    ,SetType
                                     )
            VALUES ( @SetName    -- SetName - nvarchar(50)
                    ,@FilterSet  -- SetDisplayName - nvarchar(50)
                    ,@ReportName -- SetDescription - nvarchar(1000)
                    ,@Type       -- SetType - nvarchar(50)
                );
        END;

    SET @Return_Set = (
                      SELECT TOP 1 SetId
                      FROM   dbo.ParamSet
                      WHERE  SetName = @SetName
                      );



    RETURN @Return_Set;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_Report_CreateReportRecord]'
GO
IF OBJECT_ID(N'[dbo].[USP_Report_CreateReportRecord]', 'P') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[USP_Report_CreateReportRecord]
    @ResourceId INT
   ,@ReportName VARCHAR(MAX)
   ,@ReportTableLayout BIT
   ,@Return_ReportId UNIQUEIDENTIFIER OUTPUT
   ,@RDLDir VARCHAR(MAX) = NULL
AS
    IF @ResourceId > 0
       AND LTRIM(RTRIM(@ReportName)) <> ''''
        BEGIN
            DECLARE @ReportNameNoSpaces VARCHAR(MAX) = (
                                                       SELECT REPLACE(@ReportName, '' '', '''')
                                                       );
            DECLARE @RDL VARCHAR(MAX);
            IF @RDLDir IS NULL
                BEGIN
                    SET @RDL = @ReportNameNoSpaces + ''/'' + @ReportNameNoSpaces;
                END;
            ELSE
                BEGIN
                    SET @RDL = @RDLDir + ''/'' + @ReportNameNoSpaces;
                END;

            DECLARE @ReportCLass VARCHAR(MAX) = ''FAME.Advantage.Reporting.Logic.'' + @ReportNameNoSpaces;


            IF NOT EXISTS (
                          SELECT TOP 1 ReportId
                          FROM   dbo.syReports
                          WHERE  ResourceId = @ResourceId
                          )
                BEGIN
                    INSERT INTO dbo.syReports (
                                              ReportId
                                             ,ResourceId
                                             ,ReportName
                                             ,ReportDescription
                                             ,RDLName
                                             ,AssemblyFilePath
                                             ,ReportClass
                                             ,CreationMethod
                                             ,WebServiceUrl
                                             ,RecordLimit
                                             ,AllowedExportTypes
                                             ,ReportTabLayout
                                             ,ShowPerformanceMsg
                                             ,ShowFilterMode
                                              )
                    VALUES ( NEWID()               -- ReportId - uniqueidentifier
                            ,@ResourceId           -- ResourceId - int
                            ,@ReportName           -- ReportName - varchar(50)
                            ,@ReportName           -- ReportDescription - varchar(500)
                            ,@RDL                  -- RDLName - varchar(200)
                            ,''~/Bin/Reporting.dll'' -- AssemblyFilePath - varchar(200)
                            ,@ReportCLass          -- ReportClass - varchar(200)
                            ,''BuildReport''         -- CreationMethod - varchar(200)
                            ,''futurefield''         -- WebServiceUrl - varchar(200)
                            ,400                   -- RecordLimit - bigint
                            ,0                     -- AllowedExportTypes - int
                            ,@ReportTableLayout    -- ReportTabLayout - int
                            ,0                     -- ShowPerformanceMsg - bit
                            ,0                     -- ShowFilterMode - bit
                        );
                END;



        END;

    SET @Return_ReportId = (
                           SELECT TOP 1 ReportId
                           FROM   syReports
                           WHERE  ResourceId = @ResourceId
                           );


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_Report_CreateReportTab]'
GO
IF OBJECT_ID(N'[dbo].[USP_Report_CreateReportTab]', 'P') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[USP_Report_CreateReportTab]
    @ReportId AS UNIQUEIDENTIFIER
   ,@FilterSetId AS INT
   ,@OptionsSetId AS INT
   ,@SortSetId AS INT
   ,@SetName AS VARCHAR(MAX)
   ,@ReportName AS VARCHAR(MAX)
-- WITH ENCRYPTION, RECOMPILE, EXECUTE AS CALLER|SELF|OWNER| ''user_name''
AS
    IF @FilterSetId > 0
        BEGIN
            IF NOT EXISTS (
                          SELECT TOP 1 ReportId
                          FROM   dbo.syReportTabs
                          WHERE  ReportId = @ReportId
                                 AND PageViewId = ''RadRptPage_Filtering''
                          )
                BEGIN
                    INSERT INTO dbo.syReportTabs (
                                                 ReportId
                                                ,PageViewId
                                                ,UserControlName
                                                ,SetId
                                                ,ControllerClass
                                                ,DisplayName
                                                ,ParameterSetLookUp
                                                ,TabName
                                                 )
                    VALUES ( @ReportId                             -- ReportId - uniqueidentifier
                            ,N''RadRptPage_Filtering''               -- PageViewId - nvarchar(200)
                            ,N''ParamSetPanelBarFilterControl''      -- UserControlName - nvarchar(200)
                            ,@FilterSetId                          -- SetId - bigint
                            ,N''ParamSetPanelBarFilterControl.ascx'' -- ControllerClass - nvarchar(200)
                            ,''Filter for '' + @ReportName           -- DisplayName - nvarchar(200)
                            ,@SetName                              -- ParameterSetLookUp - nvarchar(200)
                            ,N''Filtering''                          -- TabName - nvarchar(200)
                        );
                END;
        END;

    IF @OptionsSetId > 0
        BEGIN
            IF NOT EXISTS (
                          SELECT TOP 1 ReportId
                          FROM   dbo.syReportTabs
                          WHERE  ReportId = @ReportId
                                 AND PageViewId = ''RadRptPage_Options''
                          )
                BEGIN
                    INSERT INTO dbo.syReportTabs (
                                                 ReportId
                                                ,PageViewId
                                                ,UserControlName
                                                ,SetId
                                                ,ControllerClass
                                                ,DisplayName
                                                ,ParameterSetLookUp
                                                ,TabName
                                                 )
                    VALUES ( @ReportId                        -- ReportId - uniqueidentifier
                            ,N''RadRptPage_Options''            -- PageViewId - nvarchar(200)
                            ,N''ParamPanelBarSetCustomOptions'' -- UserControlName - nvarchar(200)
                            ,@OptionsSetId                    -- SetId - bigint
                            ,N''ParamSetPanelBarControl.ascx''  -- ControllerClass - nvarchar(200)
                            ,N''Options for '' + @ReportName    -- DisplayName - nvarchar(200)
                            ,@SetName                         -- ParameterSetLookUp - nvarchar(200)
                            ,N''Options''                       -- TabName - nvarchar(200)
                        );
                END;
        END;


    IF @SortSetId > 0
        BEGIN
            IF NOT EXISTS (
                          SELECT TOP 1 ReportId
                          FROM   dbo.syReportTabs
                          WHERE  ReportId = @ReportId
                                 AND PageViewId = ''RadRptPage_Sorting''
                          )
                BEGIN
                    INSERT INTO dbo.syReportTabs (
                                                 ReportId
                                                ,PageViewId
                                                ,UserControlName
                                                ,SetId
                                                ,ControllerClass
                                                ,DisplayName
                                                ,ParameterSetLookUp
                                                ,TabName
                                                 )
                    VALUES ( @ReportId                       -- ReportId - uniqueidentifier
                            ,N''RadRptPage_Sorting''           -- PageViewId - nvarchar(200)
                            ,N''ParamPanelBarSetSorting''      -- UserControlName - nvarchar(200)
                            ,@SortSetId                      -- SetId - bigint
                            ,N''ParamSetPanelBarControl.ascx'' -- ControllerClass - nvarchar(200)
                            ,N''Sort for '' + @ReportName      -- DisplayName - nvarchar(200)
                            ,@SetName                        -- ParameterSetLookUp - nvarchar(200)
                            ,N''Sorting''                      -- TabName - nvarchar(200)
                        );
                END;
        END;

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_TitleIV_QualitativeAndQuantitative]'
GO
IF OBJECT_ID(N'[dbo].[USP_TitleIV_QualitativeAndQuantitative]', 'P') IS NULL
EXEC sp_executesql N'-- =============================================
-- Author:		<Author,,Edwin Sosa>
-- Create date: <Create Date,,>
-- Description:	<This procedure calculates the qualitative and quantitative values for Title IV by using an offset date. The attendance portion of this procedure
-- removes the makeup hours from the actual hours before storing it into the sy attendance summary table. This differs slightly from the sprocs called in the attendance job to acheive the correct results.
-- When calculating the quantitative value, the makeup hours are added back into the actuals to get a consisten result. This sproc should be refactored to use the same procedures the attendance job uses internally.>
-- =============================================
CREATE PROCEDURE [dbo].[USP_TitleIV_QualitativeAndQuantitative]
    -- Add the parameters for the stored procedure here
    @StuEnrollId VARCHAR(MAX) = NULL
   ,@QuantMinUnitTypeId INTEGER
   ,@OffsetDate DATETIME
   ,@CumAverage DECIMAL(18, 2) OUTPUT
   ,@cumWeightedGPA DECIMAL(18, 2) OUTPUT
   ,@cumSimpleGPA DECIMAL(18, 2) OUTPUT
   ,@Qualitative DECIMAL(18, 2) OUTPUT
   ,@Quantitative DECIMAL(18, 2) OUTPUT
   ,@ActualHours DECIMAL(18, 2) OUTPUT
   ,@ScheduledHours DECIMAL(18, 2) OUTPUT
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @attendanceSummary TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ClsSectionId UNIQUEIDENTIFIER
               ,StudentAttendedDate DATETIME
               ,ScheduledDays DECIMAL(18, 2)
               ,ActualDays DECIMAL(18, 2)
               ,ActualRunningScheduledDays DECIMAL(18, 2)
               ,ActualRunningPresentDays DECIMAL(18, 2)
               ,ActualRunningAbsentDays DECIMAL(18, 2)
               ,ActualRunningMakeupDays DECIMAL(18, 2)
               ,ActualRunningTardyDays DECIMAL(18, 2)
               ,AdjustedPresentDays DECIMAL(18, 2)
               ,AdjustedAbsentDays DECIMAL(18, 2)
               ,AttendanceTrackType VARCHAR(50)
               ,ModUser VARCHAR(50)
               ,ModDate DATETIME
               ,TardiesMakingAbsence INT
            );
        -- Insert statements for procedure here
        DECLARE @ClsSectionIntervalMinutes AS TABLE
            (
                ClsSectionId UNIQUEIDENTIFIER NOT NULL
               ,IntervalInMinutes DECIMAL(18, 2)
            );

        DECLARE @TrackSapAttendance VARCHAR(1000);
        DECLARE @displayHours AS VARCHAR(1000);
        DECLARE @GradeCourseRepetitionsMethod VARCHAR(1000);
        DECLARE @GradesFormat AS VARCHAR(1000);
        DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;

        SET @StuEnrollCampusId = COALESCE((
                                          SELECT ASE.CampusId
                                          FROM   arStuEnrollments AS ASE
                                          WHERE  ASE.StuEnrollId = @StuEnrollId
                                          )
                                         ,NULL
                                         );
        SET @TrackSapAttendance = dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', @StuEnrollCampusId);
        IF ( @TrackSapAttendance IS NOT NULL )
            BEGIN
                SET @TrackSapAttendance = LOWER(LTRIM(RTRIM(@TrackSapAttendance)));
            END;

        SET @GradeCourseRepetitionsMethod = dbo.GetAppSettingValueByKeyName(''GradeCourseRepetitionsMethod'', @StuEnrollCampusId);
        SET @GradesFormat = dbo.GetAppSettingValueByKeyName(''GradesFormat'', @StuEnrollCampusId);
        IF ( @GradesFormat IS NOT NULL )
            BEGIN
                SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
            END;



        DECLARE @CoursesNotRepeated TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,TermId UNIQUEIDENTIFIER
               ,TermDescrip VARCHAR(100)
               ,ReqId UNIQUEIDENTIFIER
               ,ReqDescrip VARCHAR(100)
               ,ClsSectionId VARCHAR(50)
               ,CreditsEarned DECIMAL(18, 2)
               ,CreditsAttempted DECIMAL(18, 2)
               ,CurrentScore DECIMAL(18, 2)
               ,CurrentGrade VARCHAR(10)
               ,FinalScore DECIMAL(18, 2)
               ,FinalGrade VARCHAR(10)
               ,Completed BIT
               ,FinalGPA DECIMAL(18, 2)
               ,Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
               ,Count_WeightedAverage_Credits DECIMAL(18, 2)
               ,Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
               ,Count_SimpleAverage_Credits DECIMAL(18, 2)
               ,ModUser VARCHAR(50)
               ,ModDate DATETIME
               ,TermGPA_Simple DECIMAL(18, 2)
               ,TermGPA_Weighted DECIMAL(18, 2)
               ,coursecredits DECIMAL(18, 2)
               ,CumulativeGPA DECIMAL(18, 2)
               ,CumulativeGPA_Simple DECIMAL(18, 2)
               ,FACreditsEarned DECIMAL(18, 2)
               ,Average DECIMAL(18, 2)
               ,CumAverage DECIMAL(18, 2)
               ,TermStartDate DATETIME
               ,rownumber INT
            );

        INSERT INTO @CoursesNotRepeated
                    SELECT StuEnrollId
                          ,TermId
                          ,TermDescrip
                          ,ReqId
                          ,ReqDescrip
                          ,ClsSectionId
                          ,CreditsEarned
                          ,CreditsAttempted
                          ,CurrentScore
                          ,CurrentGrade
                          ,FinalScore
                          ,FinalGrade
                          ,Completed
                          ,FinalGPA
                          ,Product_WeightedAverage_Credits_GPA
                          ,Count_WeightedAverage_Credits
                          ,Product_SimpleAverage_Credits_GPA
                          ,Count_SimpleAverage_Credits
                          ,ModUser
                          ,ModDate
                          ,TermGPA_Simple
                          ,TermGPA_Weighted
                          ,coursecredits
                          ,CumulativeGPA
                          ,CumulativeGPA_Simple
                          ,FACreditsEarned
                          ,Average
                          ,CumAverage
                          ,TermStartDate
                          ,NULL AS rownumber
                    FROM   (
                           SELECT     sCS.StuEnrollId
                                     ,sCS.TermId
                                     ,sCS.TermDescrip
                                     ,sCS.ReqId
                                     ,sCS.ReqDescrip
                                     ,sCS.ClsSectionId
                                     ,sCS.CreditsEarned
                                     ,sCS.CreditsAttempted
                                     ,sCS.CurrentScore
                                     ,sCS.CurrentGrade
                                     ,sCS.FinalScore
                                     ,sCS.FinalGrade
                                     ,sCS.Completed
                                     ,sCS.FinalGPA
                                     ,sCS.Product_WeightedAverage_Credits_GPA
                                     ,sCS.Count_WeightedAverage_Credits
                                     ,sCS.Product_SimpleAverage_Credits_GPA
                                     ,sCS.Count_SimpleAverage_Credits
                                     ,sCS.ModUser
                                     ,sCS.ModDate
                                     ,sCS.TermGPA_Simple
                                     ,sCS.TermGPA_Weighted
                                     ,sCS.coursecredits
                                     ,sCS.CumulativeGPA
                                     ,sCS.CumulativeGPA_Simple
                                     ,sCS.FACreditsEarned
                                     ,sCS.Average
                                     ,sCS.CumAverage
                                     ,sCS.TermStartDate
                           FROM       dbo.syCreditSummary sCS
                           INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                           INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                           WHERE      sCS.StuEnrollId = @StuEnrollId
                                      AND CS.ReqId = sCS.ReqId
                                      AND (
                                          R.DateCompleted IS NOT NULL
                                          AND R.DateCompleted <= @OffsetDate
                                          )
                           UNION ALL
                           (SELECT     sCS.StuEnrollId
                                      ,sCS.TermId
                                      ,sCS.TermDescrip
                                      ,sCS.ReqId
                                      ,sCS.ReqDescrip
                                      ,sCS.ClsSectionId
                                      ,sCS.CreditsEarned
                                      ,sCS.CreditsAttempted
                                      ,sCS.CurrentScore
                                      ,sCS.CurrentGrade
                                      ,sCS.FinalScore
                                      ,sCS.FinalGrade
                                      ,sCS.Completed
                                      ,sCS.FinalGPA
                                      ,sCS.Product_WeightedAverage_Credits_GPA
                                      ,sCS.Count_WeightedAverage_Credits
                                      ,sCS.Product_SimpleAverage_Credits_GPA
                                      ,sCS.Count_SimpleAverage_Credits
                                      ,sCS.ModUser
                                      ,sCS.ModDate
                                      ,sCS.TermGPA_Simple
                                      ,sCS.TermGPA_Weighted
                                      ,sCS.coursecredits
                                      ,sCS.CumulativeGPA
                                      ,sCS.CumulativeGPA_Simple
                                      ,sCS.FACreditsEarned
                                      ,sCS.Average
                                      ,sCS.CumAverage
                                      ,sCS.TermStartDate
                            FROM       dbo.syCreditSummary sCS
                            INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                            WHERE      sCS.StuEnrollId = @StuEnrollId
                                       AND tG.ReqId = sCS.ReqId
                                       AND (
                                           tG.CompletedDate IS NOT NULL
                                           AND tG.CompletedDate <= @OffsetDate
                                           ))
                           ) sCSEA
                    WHERE  StuEnrollId = @StuEnrollId
                           AND ReqId IN (
                                        SELECT ReqId
                                        FROM   (
                                               SELECT   ReqId
                                                       ,ReqDescrip
                                                       ,COUNT(*) AS counter
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                               GROUP BY ReqId
                                                       ,ReqDescrip
                                               HAVING   COUNT(*) = 1
                                               ) dt
                                        );



        IF LOWER(@GradeCourseRepetitionsMethod) = ''latest''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT   dt2.StuEnrollId
                                    ,dt2.TermId
                                    ,dt2.TermDescrip
                                    ,dt2.ReqId
                                    ,dt2.ReqDescrip
                                    ,dt2.ClsSectionId
                                    ,dt2.CreditsEarned
                                    ,dt2.CreditsAttempted
                                    ,dt2.CurrentScore
                                    ,dt2.CurrentGrade
                                    ,dt2.FinalScore
                                    ,dt2.FinalGrade
                                    ,dt2.Completed
                                    ,dt2.FinalGPA
                                    ,dt2.Product_WeightedAverage_Credits_GPA
                                    ,dt2.Count_WeightedAverage_Credits
                                    ,dt2.Product_SimpleAverage_Credits_GPA
                                    ,dt2.Count_SimpleAverage_Credits
                                    ,dt2.ModUser
                                    ,dt2.ModDate
                                    ,dt2.TermGPA_Simple
                                    ,dt2.TermGPA_Weighted
                                    ,dt2.coursecredits
                                    ,dt2.CumulativeGPA
                                    ,dt2.CumulativeGPA_Simple
                                    ,dt2.FACreditsEarned
                                    ,dt2.Average
                                    ,dt2.CumAverage
                                    ,dt2.TermStartDate
                                    ,dt2.RowNumber
                            FROM     (
                                     SELECT StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                                ORDER BY TermStartDate DESC
                                                              ) AS RowNumber
                                     FROM   (
                                            SELECT     sCS.StuEnrollId
                                                      ,sCS.TermId
                                                      ,sCS.TermDescrip
                                                      ,sCS.ReqId
                                                      ,sCS.ReqDescrip
                                                      ,sCS.ClsSectionId
                                                      ,sCS.CreditsEarned
                                                      ,sCS.CreditsAttempted
                                                      ,sCS.CurrentScore
                                                      ,sCS.CurrentGrade
                                                      ,sCS.FinalScore
                                                      ,sCS.FinalGrade
                                                      ,sCS.Completed
                                                      ,sCS.FinalGPA
                                                      ,sCS.Product_WeightedAverage_Credits_GPA
                                                      ,sCS.Count_WeightedAverage_Credits
                                                      ,sCS.Product_SimpleAverage_Credits_GPA
                                                      ,sCS.Count_SimpleAverage_Credits
                                                      ,sCS.ModUser
                                                      ,sCS.ModDate
                                                      ,sCS.TermGPA_Simple
                                                      ,sCS.TermGPA_Weighted
                                                      ,sCS.coursecredits
                                                      ,sCS.CumulativeGPA
                                                      ,sCS.CumulativeGPA_Simple
                                                      ,sCS.FACreditsEarned
                                                      ,sCS.Average
                                                      ,sCS.CumAverage
                                                      ,sCS.TermStartDate
                                            FROM       dbo.syCreditSummary sCS
                                            INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                            INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                            WHERE      sCS.StuEnrollId = @StuEnrollId
                                                       AND CS.ReqId = sCS.ReqId
                                                       AND (
                                                           R.DateCompleted IS NOT NULL
                                                           AND R.DateCompleted <= @OffsetDate
                                                           )
                                            UNION ALL
                                            (SELECT     sCS.StuEnrollId
                                                       ,sCS.TermId
                                                       ,sCS.TermDescrip
                                                       ,sCS.ReqId
                                                       ,sCS.ReqDescrip
                                                       ,sCS.ClsSectionId
                                                       ,sCS.CreditsEarned
                                                       ,sCS.CreditsAttempted
                                                       ,sCS.CurrentScore
                                                       ,sCS.CurrentGrade
                                                       ,sCS.FinalScore
                                                       ,sCS.FinalGrade
                                                       ,sCS.Completed
                                                       ,sCS.FinalGPA
                                                       ,sCS.Product_WeightedAverage_Credits_GPA
                                                       ,sCS.Count_WeightedAverage_Credits
                                                       ,sCS.Product_SimpleAverage_Credits_GPA
                                                       ,sCS.Count_SimpleAverage_Credits
                                                       ,sCS.ModUser
                                                       ,sCS.ModDate
                                                       ,sCS.TermGPA_Simple
                                                       ,sCS.TermGPA_Weighted
                                                       ,sCS.coursecredits
                                                       ,sCS.CumulativeGPA
                                                       ,sCS.CumulativeGPA_Simple
                                                       ,sCS.FACreditsEarned
                                                       ,sCS.Average
                                                       ,sCS.CumAverage
                                                       ,sCS.TermStartDate
                                             FROM       dbo.syCreditSummary sCS
                                             INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                             WHERE      sCS.StuEnrollId = @StuEnrollId
                                                        AND tG.ReqId = sCS.ReqId
                                                        AND (
                                                            tG.CompletedDate IS NOT NULL
                                                            AND tG.CompletedDate <= @OffsetDate
                                                            ))
                                            ) sCSEA
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND (
                                                FinalScore IS NOT NULL
                                                OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN (
                                                         SELECT ReqId
                                                         FROM   (
                                                                SELECT   ReqId
                                                                        ,ReqDescrip
                                                                        ,COUNT(*) AS Counter
                                                                FROM     syCreditSummary
                                                                WHERE    StuEnrollId = @StuEnrollId
                                                                GROUP BY ReqId
                                                                        ,ReqDescrip
                                                                HAVING   COUNT(*) > 1
                                                                ) dt
                                                         )
                                     ) dt2
                            WHERE    RowNumber = 1
                            ORDER BY TermStartDate DESC;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''best''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                            FROM   (
                                   SELECT StuEnrollId
                                         ,TermId
                                         ,TermDescrip
                                         ,ReqId
                                         ,ReqDescrip
                                         ,ClsSectionId
                                         ,CreditsEarned
                                         ,CreditsAttempted
                                         ,CurrentScore
                                         ,CurrentGrade
                                         ,FinalScore
                                         ,FinalGrade
                                         ,Completed
                                         ,FinalGPA
                                         ,Product_WeightedAverage_Credits_GPA
                                         ,Count_WeightedAverage_Credits
                                         ,Product_SimpleAverage_Credits_GPA
                                         ,Count_SimpleAverage_Credits
                                         ,ModUser
                                         ,ModDate
                                         ,TermGPA_Simple
                                         ,TermGPA_Weighted
                                         ,coursecredits
                                         ,CumulativeGPA
                                         ,CumulativeGPA_Simple
                                         ,FACreditsEarned
                                         ,Average
                                         ,CumAverage
                                         ,TermStartDate
                                         ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                              ORDER BY Completed DESC
                                                                      ,CreditsEarned DESC
                                                                      ,FinalGPA DESC
                                                            ) AS RowNumber
                                   FROM   (
                                          SELECT     sCS.StuEnrollId
                                                    ,sCS.TermId
                                                    ,sCS.TermDescrip
                                                    ,sCS.ReqId
                                                    ,sCS.ReqDescrip
                                                    ,sCS.ClsSectionId
                                                    ,sCS.CreditsEarned
                                                    ,sCS.CreditsAttempted
                                                    ,sCS.CurrentScore
                                                    ,sCS.CurrentGrade
                                                    ,sCS.FinalScore
                                                    ,sCS.FinalGrade
                                                    ,sCS.Completed
                                                    ,sCS.FinalGPA
                                                    ,sCS.Product_WeightedAverage_Credits_GPA
                                                    ,sCS.Count_WeightedAverage_Credits
                                                    ,sCS.Product_SimpleAverage_Credits_GPA
                                                    ,sCS.Count_SimpleAverage_Credits
                                                    ,sCS.ModUser
                                                    ,sCS.ModDate
                                                    ,sCS.TermGPA_Simple
                                                    ,sCS.TermGPA_Weighted
                                                    ,sCS.coursecredits
                                                    ,sCS.CumulativeGPA
                                                    ,sCS.CumulativeGPA_Simple
                                                    ,sCS.FACreditsEarned
                                                    ,sCS.Average
                                                    ,sCS.CumAverage
                                                    ,sCS.TermStartDate
                                          FROM       dbo.syCreditSummary sCS
                                          INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                          INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                          WHERE      sCS.StuEnrollId = @StuEnrollId
                                                     AND CS.ReqId = sCS.ReqId
                                                     AND (
                                                         R.DateCompleted IS NOT NULL
                                                         AND R.DateCompleted <= @OffsetDate
                                                         )
                                          UNION ALL
                                          (SELECT     sCS.StuEnrollId
                                                     ,sCS.TermId
                                                     ,sCS.TermDescrip
                                                     ,sCS.ReqId
                                                     ,sCS.ReqDescrip
                                                     ,sCS.ClsSectionId
                                                     ,sCS.CreditsEarned
                                                     ,sCS.CreditsAttempted
                                                     ,sCS.CurrentScore
                                                     ,sCS.CurrentGrade
                                                     ,sCS.FinalScore
                                                     ,sCS.FinalGrade
                                                     ,sCS.Completed
                                                     ,sCS.FinalGPA
                                                     ,sCS.Product_WeightedAverage_Credits_GPA
                                                     ,sCS.Count_WeightedAverage_Credits
                                                     ,sCS.Product_SimpleAverage_Credits_GPA
                                                     ,sCS.Count_SimpleAverage_Credits
                                                     ,sCS.ModUser
                                                     ,sCS.ModDate
                                                     ,sCS.TermGPA_Simple
                                                     ,sCS.TermGPA_Weighted
                                                     ,sCS.coursecredits
                                                     ,sCS.CumulativeGPA
                                                     ,sCS.CumulativeGPA_Simple
                                                     ,sCS.FACreditsEarned
                                                     ,sCS.Average
                                                     ,sCS.CumAverage
                                                     ,sCS.TermStartDate
                                           FROM       dbo.syCreditSummary sCS
                                           INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                           WHERE      sCS.StuEnrollId = @StuEnrollId
                                                      AND tG.ReqId = sCS.ReqId
                                                      AND (
                                                          tG.CompletedDate IS NOT NULL
                                                          AND tG.CompletedDate <= @OffsetDate
                                                          ))
                                          ) sCSEA
                                   WHERE  StuEnrollId = @StuEnrollId
                                          AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                              )
                                          AND ReqId IN (
                                                       SELECT ReqId
                                                       FROM   (
                                                              SELECT   ReqId
                                                                      ,ReqDescrip
                                                                      ,COUNT(*) AS Counter
                                                              FROM     syCreditSummary
                                                              WHERE    StuEnrollId = @StuEnrollId
                                                              GROUP BY ReqId
                                                                      ,ReqDescrip
                                                              HAVING   COUNT(*) > 1
                                                              ) dt
                                                       )
                                   ) dt2
                            WHERE  RowNumber = 1;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''average''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                            FROM   (
                                   SELECT StuEnrollId
                                         ,TermId
                                         ,TermDescrip
                                         ,ReqId
                                         ,ReqDescrip
                                         ,ClsSectionId
                                         ,CreditsEarned
                                         ,CreditsAttempted
                                         ,CurrentScore
                                         ,CurrentGrade
                                         ,FinalScore
                                         ,FinalGrade
                                         ,Completed
                                         ,FinalGPA
                                         ,Product_WeightedAverage_Credits_GPA
                                         ,Count_WeightedAverage_Credits
                                         ,Product_SimpleAverage_Credits_GPA
                                         ,Count_SimpleAverage_Credits
                                         ,ModUser
                                         ,ModDate
                                         ,TermGPA_Simple
                                         ,TermGPA_Weighted
                                         ,coursecredits
                                         ,CumulativeGPA
                                         ,CumulativeGPA_Simple
                                         ,FACreditsEarned
                                         ,Average
                                         ,CumAverage
                                         ,TermStartDate
                                         ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                              ORDER BY FinalGPA DESC
                                                            ) AS RowNumber
                                   FROM   (
                                          SELECT     sCS.StuEnrollId
                                                    ,sCS.TermId
                                                    ,sCS.TermDescrip
                                                    ,sCS.ReqId
                                                    ,sCS.ReqDescrip
                                                    ,sCS.ClsSectionId
                                                    ,sCS.CreditsEarned
                                                    ,sCS.CreditsAttempted
                                                    ,sCS.CurrentScore
                                                    ,sCS.CurrentGrade
                                                    ,sCS.FinalScore
                                                    ,sCS.FinalGrade
                                                    ,sCS.Completed
                                                    ,sCS.FinalGPA
                                                    ,sCS.Product_WeightedAverage_Credits_GPA
                                                    ,sCS.Count_WeightedAverage_Credits
                                                    ,sCS.Product_SimpleAverage_Credits_GPA
                                                    ,sCS.Count_SimpleAverage_Credits
                                                    ,sCS.ModUser
                                                    ,sCS.ModDate
                                                    ,sCS.TermGPA_Simple
                                                    ,sCS.TermGPA_Weighted
                                                    ,sCS.coursecredits
                                                    ,sCS.CumulativeGPA
                                                    ,sCS.CumulativeGPA_Simple
                                                    ,sCS.FACreditsEarned
                                                    ,sCS.Average
                                                    ,sCS.CumAverage
                                                    ,sCS.TermStartDate
                                          FROM       dbo.syCreditSummary sCS
                                          INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                          INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                          WHERE      sCS.StuEnrollId = @StuEnrollId
                                                     AND CS.ReqId = sCS.ReqId
                                                     AND (
                                                         R.DateCompleted IS NOT NULL
                                                         AND R.DateCompleted <= @OffsetDate
                                                         )
                                          UNION ALL
                                          (SELECT     sCS.StuEnrollId
                                                     ,sCS.TermId
                                                     ,sCS.TermDescrip
                                                     ,sCS.ReqId
                                                     ,sCS.ReqDescrip
                                                     ,sCS.ClsSectionId
                                                     ,sCS.CreditsEarned
                                                     ,sCS.CreditsAttempted
                                                     ,sCS.CurrentScore
                                                     ,sCS.CurrentGrade
                                                     ,sCS.FinalScore
                                                     ,sCS.FinalGrade
                                                     ,sCS.Completed
                                                     ,sCS.FinalGPA
                                                     ,sCS.Product_WeightedAverage_Credits_GPA
                                                     ,sCS.Count_WeightedAverage_Credits
                                                     ,sCS.Product_SimpleAverage_Credits_GPA
                                                     ,sCS.Count_SimpleAverage_Credits
                                                     ,sCS.ModUser
                                                     ,sCS.ModDate
                                                     ,sCS.TermGPA_Simple
                                                     ,sCS.TermGPA_Weighted
                                                     ,sCS.coursecredits
                                                     ,sCS.CumulativeGPA
                                                     ,sCS.CumulativeGPA_Simple
                                                     ,sCS.FACreditsEarned
                                                     ,sCS.Average
                                                     ,sCS.CumAverage
                                                     ,sCS.TermStartDate
                                           FROM       dbo.syCreditSummary sCS
                                           INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                           WHERE      sCS.StuEnrollId = @StuEnrollId
                                                      AND tG.ReqId = sCS.ReqId
                                                      AND (
                                                          tG.CompletedDate IS NOT NULL
                                                          AND tG.CompletedDate <= @OffsetDate
                                                          ))
                                          ) sCSEA
                                   WHERE  StuEnrollId = @StuEnrollId
                                          AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                              )
                                          AND ReqId IN (
                                                       SELECT ReqId
                                                       FROM   (
                                                              SELECT   ReqId
                                                                      ,ReqDescrip
                                                                      ,COUNT(*) AS Counter
                                                              FROM     syCreditSummary
                                                              WHERE    StuEnrollId = @StuEnrollId
                                                              GROUP BY ReqId
                                                                      ,ReqDescrip
                                                              HAVING   COUNT(*) > 1
                                                              ) dt
                                                       )
                                   ) dt2;
            END;


        DECLARE @cumSimpleCourseCredits DECIMAL(18, 2);
        DECLARE @cumSimple_GPA_Credits DECIMAL(18, 2);
        -- (OUTPUT parameter)  DECLARE @cumSimpleGPA DECIMAL(18, 2)



        DECLARE @cumCourseCredits DECIMAL(18, 2)
               ,@cumWeighted_GPA_Credits DECIMAL(18, 2);
        DECLARE @cumCourseCredits_repeated DECIMAL(18, 2)
               ,@cumWeighted_GPA_Credits_repeated DECIMAL(18, 2);

        PRINT @GradeCourseRepetitionsMethod;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''average''
            BEGIN
                SET @cumWeightedGPA = 0;
                SET @cumSimpleGPA = 0;

                SET @cumSimpleCourseCredits = (
                                              SELECT COUNT(*)
                                              FROM   @CoursesNotRepeated
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND FinalGPA IS NOT NULL
                                                     AND (
                                                         rownumber = 0
                                                         OR rownumber IS NULL
                                                         )
                                              );

                DECLARE @cumSimpleCourseCredits_repeated DECIMAL(18, 2);
                SET @cumSimpleCourseCredits_repeated = (
                                                       SELECT SUM(CourseCreditCount)
                                                       FROM   (
                                                              SELECT   ReqId
                                                                      ,COUNT(coursecredits) AS CourseCreditCount
                                                              FROM     @CoursesNotRepeated
                                                              WHERE    StuEnrollId IN ( @StuEnrollId )
                                                                       AND FinalGPA IS NOT NULL
                                                                       AND rownumber = 1
                                                              GROUP BY ReqId
                                                              ) dt
                                                       );
                SET @cumSimpleCourseCredits = @cumSimpleCourseCredits + ISNULL(@cumSimpleCourseCredits_repeated, 0);

                DECLARE @cumSimple_GPA_Credits_repeated DECIMAL(18, 2);

                SET @cumSimple_GPA_Credits = (
                                             SELECT SUM(FinalGPA)
                                             FROM   @CoursesNotRepeated
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                                    AND (
                                                        rownumber = 0
                                                        OR rownumber IS NULL
                                                        )
                                             );

                SET @cumSimple_GPA_Credits_repeated = (
                                                      SELECT SUM(AverageGPA)
                                                      FROM   (
                                                             SELECT   ReqId
                                                                     ,SUM(FinalGPA) / MAX(rownumber) AS AverageGPA
                                                             FROM     @CoursesNotRepeated
                                                             WHERE    StuEnrollId IN ( @StuEnrollId )
                                                                      AND FinalGPA IS NOT NULL
                                                                      AND rownumber >= 1
                                                             GROUP BY ReqId
                                                             ) dt
                                                      );

                SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits + ISNULL(@cumSimple_GPA_Credits_repeated, 0.00);

                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END;

                SET @cumCourseCredits = (
                                        SELECT SUM(coursecredits)
                                        FROM   @CoursesNotRepeated
                                        WHERE  StuEnrollId IN ( @StuEnrollId )
                                               AND FinalGPA IS NOT NULL
                                               AND (
                                                   rownumber = 0
                                                   OR rownumber IS NULL
                                                   )
                                        );

                SET @cumCourseCredits_repeated = (
                                                 SELECT SUM(CourseCreditCount)
                                                 FROM   (
                                                        SELECT   ReqId
                                                                ,MAX(coursecredits) AS CourseCreditCount
                                                        FROM     @CoursesNotRepeated
                                                        WHERE    StuEnrollId IN ( @StuEnrollId )
                                                                 AND FinalGPA IS NOT NULL
                                                                 AND rownumber >= 1
                                                        GROUP BY ReqId
                                                        ) dt
                                                 );
                SET @cumCourseCredits = @cumCourseCredits + ISNULL(@cumCourseCredits_repeated, 0.00);

                PRINT @cumCourseCredits;

                SET @cumWeighted_GPA_Credits = (
                                               SELECT SUM(coursecredits * FinalGPA)
                                               FROM   @CoursesNotRepeated
                                               WHERE  StuEnrollId IN ( @StuEnrollId )
                                                      AND FinalGPA IS NOT NULL
                                                      AND (
                                                          rownumber = 0
                                                          OR rownumber IS NULL
                                                          )
                                               );
                SET @cumWeighted_GPA_Credits_repeated = (
                                                        SELECT SUM(CourseCreditCount)
                                                        FROM   (
                                                               SELECT   ReqId
                                                                       ,SUM(coursecredits * FinalGPA) / MAX(rownumber) AS CourseCreditCount
                                                               FROM     @CoursesNotRepeated
                                                               WHERE    StuEnrollId IN ( @StuEnrollId )
                                                                        AND FinalGPA IS NOT NULL
                                                                        AND rownumber >= 1
                                                               GROUP BY ReqId
                                                               ) dt
                                                        );



                SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits + ISNULL(@cumWeighted_GPA_Credits_repeated, 0);
                PRINT @cumWeighted_GPA_Credits;

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END;
            END;
        ELSE
            BEGIN
                SET @cumSimpleGPA = 0;

                SET @cumSimpleCourseCredits = (
                                              SELECT COUNT(*)
                                              FROM   @CoursesNotRepeated
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND FinalGPA IS NOT NULL
                                              );
                SET @cumSimple_GPA_Credits = (
                                             SELECT SUM(FinalGPA)
                                             FROM   @CoursesNotRepeated
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                             );
                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END;
                SET @cumWeightedGPA = 0;
                SET @cumCourseCredits = (
                                        SELECT SUM(coursecredits)
                                        FROM   @CoursesNotRepeated
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND FinalGPA IS NOT NULL
                                        );
                SET @cumWeighted_GPA_Credits = (
                                               SELECT SUM(coursecredits * FinalGPA)
                                               FROM   @CoursesNotRepeated
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalGPA IS NOT NULL
                                               );

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END;

            END;





        IF @cumCourseCredits >= 1
            BEGIN
                SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
            END;


        DECLARE @TermAverageCount INT;
        DECLARE @TermAverage DECIMAL(18, 2);
        -- (output parameter)  DECLARE @CumAverage DECIMAL(18, 2)
        IF @GradesFormat <> ''letter''
           OR @QuantMinUnitTypeId = 3
            BEGIN

                DECLARE @termAverageSum DECIMAL(18, 2)
                       ,@cumAverageSum DECIMAL(18, 2)
                       ,@cumAveragecount INT;
                DECLARE @TardiesMakingAbsence INT
                       ,@UnitTypeDescrip VARCHAR(20)
                       ,@OriginalTardiesMakingAbsence INT;
                SET @TardiesMakingAbsence = (
                                            SELECT TOP 1 t1.TardiesMakingAbsence
                                            FROM   arPrgVersions t1
                                                  ,arStuEnrollments t2
                                            WHERE  t1.PrgVerId = t2.PrgVerId
                                                   AND t2.StuEnrollId = @StuEnrollId
                                            );

                SET @UnitTypeDescrip = (
                                       SELECT     LTRIM(RTRIM(AAUT.UnitTypeDescrip))
                                       FROM       arAttUnitType AS AAUT
                                       INNER JOIN arPrgVersions AS APV ON APV.UnitTypeId = AAUT.UnitTypeId
                                       INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                                       WHERE      ASE.StuEnrollId = @StuEnrollId
                                       );

                SET @OriginalTardiesMakingAbsence = (
                                                    SELECT TOP 1 tardiesmakingabsence
                                                    FROM   syStudentAttendanceSummary
                                                    WHERE  StuEnrollId = @StuEnrollId
                                                    );
                DECLARE @termstartdate1 DATETIME;

                -- Declare Variables
                BEGIN -- Declare Variables
                    DECLARE @MeetDate DATETIME
                           ,@WeekDay VARCHAR(15)
                           ,@StartDate DATETIME
                           ,@EndDate DATETIME;
                    DECLARE @PeriodDescrip VARCHAR(50)
                           ,@Actual DECIMAL(18, 2)
                           ,@Excused DECIMAL(18, 2)
                           ,@ClsSectionId UNIQUEIDENTIFIER;
                    DECLARE @Absent DECIMAL(18, 2)
                           ,@SchedHours DECIMAL(18, 2)
                           ,@TardyMinutes DECIMAL(18, 2);
                    DECLARE @tardy DECIMAL(18, 2)
                           ,@tracktardies INT
                           ,@rownumber INT
                           ,@IsTardy BIT
                           ,@ActualRunningScheduledHours DECIMAL(18, 2);
                    DECLARE @ActualRunningPresentHours DECIMAL(18, 2)
                           ,@ActualRunningAbsentHours DECIMAL(18, 2)
                           ,@ActualRunningTardyHours DECIMAL(18, 2)
                           ,@ActualRunningMakeupHours DECIMAL(18, 2);
                    DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
                           ,@intTardyBreakPoint INT
                           ,@AdjustedRunningPresentHours DECIMAL(18, 2)
                           ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
                           ,@ActualRunningScheduledDays DECIMAL(18, 2);
                    --declare @Scheduledhours decimal(18,2),@ActualPresentDays_ConvertTo_Hours decimal(18,2),@ActualAbsentDays_ConvertTo_Hours decimal(18,2),@AttendanceTrack varchar(50)
                    DECLARE @TermId VARCHAR(50)
                           ,@TermDescrip VARCHAR(50)
                           ,@PrgVerId UNIQUEIDENTIFIER;
                    DECLARE @TardyHit VARCHAR(10)
                           ,@ScheduledMinutes DECIMAL(18, 2);
                    DECLARE @Scheduledhours_noperiods DECIMAL(18, 2)
                           ,@ActualPresentDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2)
                           ,@ActualAbsentDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2);
                    DECLARE @ActualTardyDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2);
                    DECLARE @boolReset BIT
                           ,@MakeupHours DECIMAL(18, 2)
                           ,@AdjustedPresentDaysComputed DECIMAL(18, 2);
                END;

                -- Step 3  --  InsertAttendance_Class_PresentAbsent   -- UnitTypeDescrip = (''None'', ''Present Absent'') 
                --         --  TrackSapAttendance = ''byclass''
                BEGIN -- Step 3  --  InsertAttendance_Class_PresentAbsent   -- UnitTypeDescrip = (''None'', ''Present Absent'') 
                    --         --  TrackSapAttendance = ''byclass''
                    IF @UnitTypeDescrip IN ( ''none'', ''present absent'' )
                       AND @TrackSapAttendance = ''byclass''
                        BEGIN
                            PRINT ''Present absent, by class'';
                            ---- PRINT ''Step1!!!!''
                            --BEGIN
                            --    --DELETE FROM syStudentAttendanceSummary
                            --    --WHERE StuEnrollId = @StuEnrollId
                            --    --      AND StudentAttendedDate <= @OffsetDate;
                            --END;
                            INSERT INTO @ClsSectionIntervalMinutes (
                                                                   ClsSectionId
                                                                  ,IntervalInMinutes
                                                                   )
                                        SELECT     DISTINCT t1.ClsSectionId
                                                           ,DATEDIFF(MI, t6.TimeIntervalDescrip, t7.TimeIntervalDescrip)
                                        FROM       atClsSectAttendance t1
                                        INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                        INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                        INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                        INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                           AND (
                                                                               CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                               AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                               )
                                                                           AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                        INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                        INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                        INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                        INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                        INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                        INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                        WHERE      t2.StuEnrollId = @StuEnrollId
                                                   AND (
                                                       AAUT1.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                                       OR AAUT2.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                                       );

                            DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
                                SELECT   *
                                        ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                FROM     (
                                         SELECT     DISTINCT t1.StuEnrollId
                                                            ,t1.ClsSectionId
                                                            ,t1.MeetDate
                                                            ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                            ,t4.StartDate
                                                            ,t4.EndDate
                                                            ,t5.PeriodDescrip
                                                            ,t1.Actual
                                                            ,t1.Excused
                                                            ,CASE WHEN (
                                                                       t1.Actual = 0
                                                                       AND t1.Excused = 0
                                                                       ) THEN t1.Scheduled
                                                                  ELSE CASE WHEN (
                                                                                 t1.Actual <> 9999.00
                                                                                 AND t1.Actual < t1.Scheduled
                                                                                 --AND t1.Excused <> 1
                                                                                 ) THEN ( t1.Scheduled - t1.Actual )
                                                                            ELSE 0
                                                                       END
                                                             END AS Absent
                                                            ,t1.Scheduled AS ScheduledMinutes
                                                            ,CASE WHEN (
                                                                       t1.Actual > 0
                                                                       AND t1.Actual < t1.Scheduled
                                                                       ) THEN ( t1.Scheduled - t1.Actual )
                                                                  ELSE 0
                                                             END AS TardyMinutes
                                                            ,t1.Tardy AS Tardy
                                                            ,t3.TrackTardies
                                                            ,t3.TardiesMakingAbsence
                                                            ,t3.PrgVerId
                                         FROM       atClsSectAttendance t1
                                         INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                         INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                         INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                         INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                            AND (
                                                                                CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                                AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                                )
                                                                            AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                         INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                         INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                         INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                         INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                         INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                         INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                         WHERE      t2.StuEnrollId = @StuEnrollId
                                                    AND (
                                                        AAUT1.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                                        OR AAUT2.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                                        )
                                                    AND t1.Actual <> 9999
                                                    AND t1.MeetDate <= @OffsetDate
                                         ) dt
                                ORDER BY StuEnrollId
                                        ,MeetDate;
                            OPEN GetAttendance_Cursor;
                            FETCH NEXT FROM GetAttendance_Cursor
                            INTO @StuEnrollId
                                ,@ClsSectionId
                                ,@MeetDate
                                ,@WeekDay
                                ,@StartDate
                                ,@EndDate
                                ,@PeriodDescrip
                                ,@Actual
                                ,@Excused
                                ,@Absent
                                ,@ScheduledMinutes
                                ,@TardyMinutes
                                ,@tardy
                                ,@tracktardies
                                ,@TardiesMakingAbsence
                                ,@PrgVerId
                                ,@rownumber;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @boolReset = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                            SET @boolReset = 1;
                                        END;


                                    -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                                    IF @Actual <> 9999.00
                                        BEGIN
                                            -- Commented by Balaji on 2.6.2015 as Excused Absence is still considered an absence
                                            -- @Excused is not added to Present Hours
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual; -- + @Excused
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; -- + @Excused

                                            -- If there are make up hrs deduct that otherwise it will be added again in progress report
                                            IF (
                                               @Actual > 0
                                               AND @Actual > @ScheduledMinutes
                                               AND @Actual <> 9999.00
                                               )
                                                BEGIN
                                                    SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                END;

                                            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                                        END;

                                    -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                                    SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                                    -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                                    IF (
                                       @Actual > 0
                                       AND @Actual < @ScheduledMinutes
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                        END;
                                    ELSE IF (
                                            @Actual = 1
                                            AND @Actual = @ScheduledMinutes
                                            AND @tardy = 1
                                            )
                                             BEGIN
                                                 SET @ActualRunningTardyHours += 1;
                                             END;

                                    -- Track how many days student has been tardy only when 
                                    -- program version requires to track tardy
                                    IF (
                                       @tracktardies = 1
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;


                                    -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
                                    -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
                                    -- when student is tardy the second time, that second occurance will be considered as
                                    -- absence
                                    -- Variable @intTardyBreakpoint tracks how many times the student was tardy
                                    -- Variable @tardiesMakingAbsence tracks the tardy rule
                                    IF (
                                       @tracktardies = 1
                                       AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
                                            SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;

                                    -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                       @Actual > 0
                                       AND @Actual > @ScheduledMinutes
                                       AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;

                                    IF ( @tracktardies = 1 )
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                                        END;

                                    ---- PRINT @MeetDate
                                    ---- PRINT @ActualRunningAbsentHours



                                    INSERT INTO @attendanceSummary (
                                                                   StuEnrollId
                                                                  ,ClsSectionId
                                                                  ,StudentAttendedDate
                                                                  ,ScheduledDays
                                                                  ,ActualDays
                                                                  ,ActualRunningScheduledDays
                                                                  ,ActualRunningPresentDays
                                                                  ,ActualRunningAbsentDays
                                                                  ,ActualRunningMakeupDays
                                                                  ,ActualRunningTardyDays
                                                                  ,AdjustedPresentDays
                                                                  ,AdjustedAbsentDays
                                                                  ,AttendanceTrackType
                                                                  ,ModUser
                                                                  ,ModDate
                                                                  ,TardiesMakingAbsence
                                                                   )
                                    VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays
                                            ,@ActualRunningPresentHours, @ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours
                                            ,@AdjustedPresentDaysComputed, @AdjustedRunningAbsentHours, ''Post Attendance by Class Min'', ''sa'', GETDATE()
                                            ,@TardiesMakingAbsence );

                                    --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                                    SET @PrevStuEnrollId = @StuEnrollId;

                                    FETCH NEXT FROM GetAttendance_Cursor
                                    INTO @StuEnrollId
                                        ,@ClsSectionId
                                        ,@MeetDate
                                        ,@WeekDay
                                        ,@StartDate
                                        ,@EndDate
                                        ,@PeriodDescrip
                                        ,@Actual
                                        ,@Excused
                                        ,@Absent
                                        ,@ScheduledMinutes
                                        ,@TardyMinutes
                                        ,@tardy
                                        ,@tracktardies
                                        ,@TardiesMakingAbsence
                                        ,@PrgVerId
                                        ,@rownumber;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;

                            DECLARE @MyTardyTable TABLE
                                (
                                    ClsSectionId UNIQUEIDENTIFIER
                                   ,TardiesMakingAbsence INT
                                   ,AbsentHours DECIMAL(18, 2)
                                );

                            INSERT INTO @MyTardyTable
                                        SELECT     ClsSectionId
                                                  ,PV.TardiesMakingAbsence
                                                  ,CASE WHEN ( COUNT(*) >= PV.TardiesMakingAbsence ) THEN ( COUNT(*) / PV.TardiesMakingAbsence )
                                                        ELSE 0
                                                   END AS AbsentHours
                                        --Count(*) as NumberofTimesTardy 
                                        FROM       dbo.syStudentAttendanceSummary SAS
                                        INNER JOIN arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                                        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                        WHERE      SE.StuEnrollId = @StuEnrollId
                                                   AND SAS.IsTardy = 1
                                                   AND PV.TrackTardies = 1
                                        GROUP BY   ClsSectionId
                                                  ,PV.TardiesMakingAbsence;

                            --Drop table @MyTardyTable
                            DECLARE @TotalTardyAbsentDays DECIMAL(18, 2);
                            SET @TotalTardyAbsentDays = (
                                                        SELECT ISNULL(SUM(AbsentHours), 0)
                                                        FROM   @MyTardyTable
                                                        );

                            --Print @TotalTardyAbsentDays

                            UPDATE @attendanceSummary
                            SET    AdjustedPresentDays = AdjustedPresentDays - @TotalTardyAbsentDays
                                  ,AdjustedAbsentDays = AdjustedAbsentDays + @TotalTardyAbsentDays
                            WHERE  StuEnrollId = @StuEnrollId
                                   AND StudentAttendedDate = (
                                                             SELECT   TOP 1 StudentAttendedDate
                                                             FROM     @attendanceSummary
                                                             WHERE    StuEnrollId = @StuEnrollId
                                                             ORDER BY StudentAttendedDate DESC
                                                             );

                        END;
                END; -- Step 3 
                -- END -- Step 3 

                -- Step 4  --  InsertAttendance_Class_Minutes
                --         --  TrackSapAttendance = ''byclass''
                BEGIN -- Step 4  --  InsertAttendance_Class_Minutes
                    -- By Class and Attendance Unit Type - Minutes and Clock Hour

                    IF @UnitTypeDescrip IN ( ''minutes'', ''clock hours'' )
                       AND @TrackSapAttendance = ''byclass''
                        BEGIN

                            --BEGIN
                            --    --DELETE FROM syStudentAttendanceSummary
                            --    --WHERE StuEnrollId = @StuEnrollId
                            --    --      AND StudentAttendedDate <= @OffsetDate;
                            --END;
                            DECLARE GetAttendance_Cursor CURSOR FOR
                                SELECT   *
                                        ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                FROM     (
                                         SELECT     DISTINCT t1.StuEnrollId
                                                            ,t1.ClsSectionId
                                                            ,t1.MeetDate
                                                            ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                            ,t4.StartDate
                                                            ,t4.EndDate
                                                            ,t5.PeriodDescrip
                                                            ,t1.Actual
                                                            ,t1.Excused
                                                            ,CASE WHEN (
                                                                       t1.Actual = 0
                                                                       AND t1.Excused = 0
                                                                       ) THEN t1.Scheduled
                                                                  ELSE CASE WHEN (
                                                                                 t1.Actual <> 9999.00
                                                                                 AND t1.Actual < t1.Scheduled
                                                                                 ) THEN ( t1.Scheduled - t1.Actual )
                                                                            ELSE 0
                                                                       END
                                                             END AS Absent
                                                            ,t1.Scheduled AS ScheduledMinutes
                                                            ,CASE WHEN (
                                                                       t1.Actual > 0
                                                                       AND t1.Actual < t1.Scheduled
                                                                       ) THEN ( t1.Scheduled - t1.Actual )
                                                                  ELSE 0
                                                             END AS TardyMinutes
                                                            ,t1.Tardy AS Tardy
                                                            ,t3.TrackTardies
                                                            ,t3.TardiesMakingAbsence
                                                            ,t3.PrgVerId
                                         FROM       atClsSectAttendance t1
                                         INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                         INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                         INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                         INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                            AND (
                                                                                CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                                AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                                )
                                                                            AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                         INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                         INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                         INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                         INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                         INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                         INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                         WHERE      t2.StuEnrollId = @StuEnrollId
                                                    AND (
                                                        AAUT1.UnitTypeDescrip IN ( ''Minutes'', ''Clock Hours'' )
                                                        OR AAUT2.UnitTypeDescrip IN ( ''Minutes'', ''Clock Hours'' )
                                                        )
                                                    AND t1.Actual <> 9999
                                                    AND t1.MeetDate <= @OffsetDate
                                         UNION
                                         SELECT     DISTINCT t1.StuEnrollId
                                                            ,NULL AS ClsSectionId
                                                            ,t1.RecordDate AS MeetDate
                                                            ,DATENAME(dw, t1.RecordDate) AS WeekDay
                                                            ,NULL AS StartDate
                                                            ,NULL AS EndDate
                                                            ,NULL AS PeriodDescrip
                                                            ,t1.ActualHours
                                                            ,NULL AS Excused
                                                            ,CASE WHEN ( t1.ActualHours = 0 ) THEN t1.SchedHours
                                                                  ELSE 0
                                                             --ELSE 
                                                             --Case when (t1.ActualHours <> 9999.00 and t1.ActualHours < t1.SchedHours)
                                                             --		THEN (t1.SchedHours - t1.ActualHours)
                                                             --		ELSE 
                                                             --			0
                                                             --		End
                                                             END AS Absent
                                                            ,t1.SchedHours AS ScheduledMinutes
                                                            ,CASE WHEN (
                                                                       t1.ActualHours <> 9999.00
                                                                       AND t1.ActualHours > 0
                                                                       AND t1.ActualHours < t1.SchedHours
                                                                       ) THEN ( t1.SchedHours - t1.ActualHours )
                                                                  ELSE 0
                                                             END AS TardyMinutes
                                                            ,NULL AS Tardy
                                                            ,t3.TrackTardies
                                                            ,t3.TardiesMakingAbsence
                                                            ,t3.PrgVerId
                                         FROM       arStudentClockAttendance t1
                                         INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                         INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                         WHERE      t2.StuEnrollId = @StuEnrollId
                                                    AND t1.Converted = 1
                                                    AND t1.ActualHours <> 9999
                                                    AND t1.RecordDate <= @OffsetDate
                                         ) dt
                                ORDER BY StuEnrollId
                                        ,MeetDate;
                            OPEN GetAttendance_Cursor;
                            FETCH NEXT FROM GetAttendance_Cursor
                            INTO @StuEnrollId
                                ,@ClsSectionId
                                ,@MeetDate
                                ,@WeekDay
                                ,@StartDate
                                ,@EndDate
                                ,@PeriodDescrip
                                ,@Actual
                                ,@Excused
                                ,@Absent
                                ,@ScheduledMinutes
                                ,@TardyMinutes
                                ,@tardy
                                ,@tracktardies
                                ,@TardiesMakingAbsence
                                ,@PrgVerId
                                ,@rownumber;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @boolReset = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                            SET @boolReset = 1;
                                        END;


                                    -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                                    IF @Actual <> 9999.00
                                        BEGIN
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                                            -- If there are make up hrs deduct that otherwise it will be added again in progress report
                                            IF (
                                               @Actual > 0
                                               AND @Actual > @ScheduledMinutes
                                               AND @Actual <> 9999.00
                                               )
                                                BEGIN
                                                    SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                END;
                                            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                                        END;

                                    -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                                    SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                                    -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                                    IF (
                                       @Actual > 0
                                       AND @Actual < @ScheduledMinutes
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                        END;

                                    -- Track how many days student has been tardy only when 
                                    -- program version requires to track tardy
                                    IF (
                                       @tracktardies = 1
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;


                                    -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
                                    -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
                                    -- when student is tardy the second time, that second occurance will be considered as
                                    -- absence
                                    -- Variable @intTardyBreakpoint tracks how many times the student was tardy
                                    -- Variable @tardiesMakingAbsence tracks the tardy rule
                                    IF (
                                       @tracktardies = 1
                                       AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                            SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;

                                    -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                       @Actual > 0
                                       AND @Actual > @ScheduledMinutes
                                       AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;


                                    IF ( @tracktardies = 1 )
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                                        END;



                                    INSERT INTO @attendanceSummary (
                                                                   StuEnrollId
                                                                  ,ClsSectionId
                                                                  ,StudentAttendedDate
                                                                  ,ScheduledDays
                                                                  ,ActualDays
                                                                  ,ActualRunningScheduledDays
                                                                  ,ActualRunningPresentDays
                                                                  ,ActualRunningAbsentDays
                                                                  ,ActualRunningMakeupDays
                                                                  ,ActualRunningTardyDays
                                                                  ,AdjustedPresentDays
                                                                  ,AdjustedAbsentDays
                                                                  ,AttendanceTrackType
                                                                  ,ModUser
                                                                  ,ModDate
                                                                   )
                                    VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays
                                            ,@ActualRunningPresentHours, @ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours
                                            ,@AdjustedPresentDaysComputed, @AdjustedRunningAbsentHours, ''Post Attendance by Class Min'', ''sa'', GETDATE());

                                    UPDATE @attendanceSummary
                                    SET    TardiesMakingAbsence = @TardiesMakingAbsence
                                    WHERE  StuEnrollId = @StuEnrollId;
                                    SET @PrevStuEnrollId = @StuEnrollId;

                                    FETCH NEXT FROM GetAttendance_Cursor
                                    INTO @StuEnrollId
                                        ,@ClsSectionId
                                        ,@MeetDate
                                        ,@WeekDay
                                        ,@StartDate
                                        ,@EndDate
                                        ,@PeriodDescrip
                                        ,@Actual
                                        ,@Excused
                                        ,@Absent
                                        ,@ScheduledMinutes
                                        ,@TardyMinutes
                                        ,@tardy
                                        ,@tracktardies
                                        ,@TardiesMakingAbsence
                                        ,@PrgVerId
                                        ,@rownumber;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
                        END;
                END; --  Step 3  --   InsertAttendance_Class_Minutes
                --END --  Step 3  --   InsertAttendance_Class_Minutes


                --Select * from arAttUnitType


                -- By Minutes/Day
                -- remove clock hour from here
                IF @UnitTypeDescrip IN ( ''minutes'' )
                   AND @TrackSapAttendance = ''byday''
                    -- -- PRINT GETDATE();	
                    ---- PRINT @UnitTypeId;
                    ---- PRINT @TrackSapAttendance;
                    BEGIN
                        --BEGIN
                        --    --DELETE FROM syStudentAttendanceSummary
                        --    --WHERE StuEnrollId = @StuEnrollId
                        --    --      AND StudentAttendedDate <= @OffsetDate;
                        --END;
                        DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
                            SELECT     t1.StuEnrollId
                                      ,NULL AS ClsSectionId
                                      ,t1.RecordDate AS MeetDate
                                      ,t1.ActualHours
                                      ,t1.SchedHours AS ScheduledMinutes
                                      ,CASE WHEN (
                                                 (
                                                 t1.SchedHours >= 1
                                                 AND t1.SchedHours NOT IN ( 999, 9999 )
                                                 )
                                                 AND t1.ActualHours = 0
                                                 ) THEN t1.SchedHours
                                            ELSE 0
                                       END AS Absent
                                      ,t1.isTardy
                                      ,(
                                       SELECT ISNULL(SUM(SchedHours), 0)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999, 9999 )
                                                  AND t1.ActualHours NOT IN ( 999, 9999 )
                                                  )
                                       ) AS ActualRunningScheduledHours
                                      ,(
                                       SELECT SUM(ActualHours)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999, 9999 )
                                                  )
                                              AND ActualHours >= 1
                                              AND ActualHours NOT IN ( 999, 9999 )
                                       ) AS ActualRunningPresentHours
                                      ,(
                                       SELECT COUNT(ActualHours)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999, 9999 )
                                                  )
                                              AND ActualHours = 0
                                              AND ActualHours NOT IN ( 999, 9999 )
                                       ) AS ActualRunningAbsentHours
                                      ,(
                                       SELECT SUM(ActualHours)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND SchedHours = 0
                                              AND ActualHours >= 1
                                              AND ActualHours NOT IN ( 999, 9999 )
                                       ) AS ActualRunningMakeupHours
                                      ,(
                                       SELECT SUM(SchedHours - ActualHours)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND (
                                                  (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999, 9999 )
                                                  )
                                                  AND ActualHours >= 1
                                                  AND ActualHours NOT IN ( 999, 9999 )
                                                  )
                                              AND isTardy = 1
                                       ) AS ActualRunningTardyHours
                                      ,t3.TrackTardies
                                      ,t3.TardiesMakingAbsence
                                      ,t3.PrgVerId
                                      ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
                            FROM       arStudentClockAttendance t1
                            INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                            INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                            INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                            --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                            WHERE      AAUT1.UnitTypeDescrip IN ( ''Minutes'' )
                                       AND t2.StuEnrollId = @StuEnrollId
                                       AND t1.ActualHours <> 9999.00
                                       AND t1.RecordDate <= @OffsetDate
                            ORDER BY   t1.StuEnrollId
                                      ,MeetDate;
                        OPEN GetAttendance_Cursor;
                        --Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                        FETCH NEXT FROM GetAttendance_Cursor
                        INTO @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@Actual
                            ,@ScheduledMinutes
                            ,@Absent
                            ,@IsTardy
                            ,@ActualRunningScheduledHours
                            ,@ActualRunningPresentHours
                            ,@ActualRunningAbsentHours
                            ,@ActualRunningMakeupHours
                            ,@ActualRunningTardyHours
                            ,@tracktardies
                            ,@TardiesMakingAbsence
                            ,@PrgVerId
                            ,@rownumber;

                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @ActualRunningMakeupHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        WHILE @@FETCH_STATUS = 0
                            BEGIN

                                IF @PrevStuEnrollId <> @StuEnrollId
                                    BEGIN
                                        SET @ActualRunningPresentHours = 0;
                                        SET @ActualRunningAbsentHours = 0;
                                        SET @intTardyBreakPoint = 0;
                                        SET @ActualRunningTardyHours = 0;
                                        SET @AdjustedRunningPresentHours = 0;
                                        SET @AdjustedRunningAbsentHours = 0;
                                        SET @ActualRunningScheduledDays = 0;
                                    END;

                                IF (
                                   @ScheduledMinutes >= 1
                                   AND (
                                       @Actual <> 9999
                                       AND @Actual <> 999
                                       )
                                   AND (
                                       @ScheduledMinutes <> 9999
                                       AND @Actual <> 999
                                       )
                                   )
                                    BEGIN
                                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + ISNULL(@ScheduledMinutes, 0);
                                    END;
                                ELSE
                                    BEGIN
                                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0);
                                    END;

                                IF (
                                   @Actual <> 9999
                                   AND @Actual <> 999
                                   )
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual - ( @Actual - @ScheduledMinutes );
                                    END;
                                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;

                                IF (
                                   @Actual > 0
                                   AND @Actual < @ScheduledMinutes
                                   )
                                    BEGIN
                                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                                    END;
                                -- Make up hours
                                --sched=5, Actual =7, makeup= 2,ab = 0
                                --sched=0, Actual =7, makeup= 7,ab = 0
                                IF (
                                   @Actual > 0
                                   AND @ScheduledMinutes > 0
                                   AND @Actual > @ScheduledMinutes
                                   AND (
                                       @Actual <> 9999
                                       AND @Actual <> 999
                                       )
                                   )
                                    BEGIN
                                        SET @ActualRunningMakeupHours = @ActualRunningMakeupHours + ( @Actual - @ScheduledMinutes );
                                    END;


                                IF (
                                   @Actual <> 9999
                                   AND @Actual <> 999
                                   )
                                    BEGIN
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                                    END;
                                IF (
                                   @Actual = 0
                                   AND @ScheduledMinutes >= 1
                                   AND @ScheduledMinutes NOT IN ( 999, 9999 )
                                   )
                                    BEGIN
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                                    END;
                                -- Absent hours
                                --1. sched = 5, Actual = 2 then Ab = (5-3) = 2
                                IF (
                                   @Absent = 0
                                   AND @ActualRunningAbsentHours > 0
                                   AND ( @Actual < @ScheduledMinutes )
                                   )
                                    BEGIN
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                                    END;
                                IF (
                                   @Actual > 0
                                   AND @Actual < @ScheduledMinutes
                                   AND (
                                       @Actual <> 9999.00
                                       AND @Actual <> 999.00
                                       )
                                   )
                                    BEGIN
                                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                    END;

                                IF @tracktardies = 1
                                   AND (
                                       @TardyMinutes > 0
                                       OR @IsTardy = 1
                                       )
                                    BEGIN
                                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                    END;
                                IF (
                                   @tracktardies = 1
                                   AND @intTardyBreakPoint = @TardiesMakingAbsence
                                   )
                                    BEGIN
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Actual; --@TardyMinutes
                                        SET @intTardyBreakPoint = 0;
                                    END;

                                INSERT INTO @attendanceSummary (
                                                               StuEnrollId
                                                              ,ClsSectionId
                                                              ,StudentAttendedDate
                                                              ,ScheduledDays
                                                              ,ActualDays
                                                              ,ActualRunningScheduledDays
                                                              ,ActualRunningPresentDays
                                                              ,ActualRunningAbsentDays
                                                              ,ActualRunningMakeupDays
                                                              ,ActualRunningTardyDays
                                                              ,AdjustedPresentDays
                                                              ,AdjustedAbsentDays
                                                              ,AttendanceTrackType
                                                              ,ModUser
                                                              ,ModDate
                                                               )
                                VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays
                                        ,@ActualRunningPresentHours, @ActualRunningAbsentHours, @ActualRunningMakeupHours, @ActualRunningTardyHours
                                        ,@AdjustedRunningPresentHours, @AdjustedRunningAbsentHours, ''Post Attendance by Class'', ''sa'', GETDATE());

                                UPDATE @attendanceSummary
                                SET    TardiesMakingAbsence = @TardiesMakingAbsence
                                WHERE  StuEnrollId = @StuEnrollId;
                                --end
                                SET @PrevStuEnrollId = @StuEnrollId;
                                FETCH NEXT FROM GetAttendance_Cursor
                                INTO @StuEnrollId
                                    ,@ClsSectionId
                                    ,@MeetDate
                                    ,@Actual
                                    ,@ScheduledMinutes
                                    ,@Absent
                                    ,@IsTardy
                                    ,@ActualRunningScheduledHours
                                    ,@ActualRunningPresentHours
                                    ,@ActualRunningAbsentHours
                                    ,@ActualRunningMakeupHours
                                    ,@ActualRunningTardyHours
                                    ,@tracktardies
                                    ,@TardiesMakingAbsence
                                    ,@PrgVerId
                                    ,@rownumber;
                            END;
                        CLOSE GetAttendance_Cursor;
                        DEALLOCATE GetAttendance_Cursor;
                    END;

                ---- PRINT ''Does it get to Clock Hour/PA'';
                ---- PRINT @UnitTypeId;
                ---- PRINT @TrackSapAttendance;
                -- By Day and PA, Clock Hour
                -- -- Step 2  --  InsertAttendance_Day_PresentAbsent  -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'')  and TrackSapAttendance = ''byday''
                BEGIN -- Step 2  --  InsertAttendance_Day_PresentAbsent  -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'') and TrackSapAttendance = ''byday''
                    IF @UnitTypeDescrip IN ( ''present absent'', ''clock hours'' )
                       AND @TrackSapAttendance = ''byday''
                        BEGIN
                            --BEGIN
                            --    --DELETE FROM syStudentAttendanceSummary
                            --    --WHERE StuEnrollId = @StuEnrollId
                            --    --      AND StudentAttendedDate <= @OffsetDate;
                            --END;
                            ---- PRINT ''By Day inside day'';
                            DECLARE GetAttendance_Cursor CURSOR FOR
                                SELECT     t1.StuEnrollId
                                          ,t1.RecordDate
                                          ,t1.ActualHours
                                          ,t1.SchedHours
                                          ,CASE WHEN (
                                                     (
                                                     t1.SchedHours >= 1
                                                     AND t1.SchedHours NOT IN ( 999, 9999 )
                                                     )
                                                     AND t1.ActualHours = 0
                                                     ) THEN t1.SchedHours
                                                ELSE 0
                                           END AS Absent
                                          ,t1.isTardy
                                          ,t3.TrackTardies
                                          ,t3.TardiesMakingAbsence
                                FROM       arStudentClockAttendance t1
                                INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                                WHERE -- Unit Types: Present Absent and Clock Hour
                                           AAUT1.UnitTypeDescrip IN ( ''present absent'', ''clock hours'' )
                                           AND ActualHours <> 9999.00
                                           AND t2.StuEnrollId = @StuEnrollId
                                           AND t1.RecordDate <= @OffsetDate
                                ORDER BY   t1.StuEnrollId
                                          ,t1.RecordDate;
                            OPEN GetAttendance_Cursor;
                            --Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                            FETCH NEXT FROM GetAttendance_Cursor
                            INTO @StuEnrollId
                                ,@MeetDate
                                ,@Actual
                                ,@ScheduledMinutes
                                ,@Absent
                                ,@IsTardy
                                ,@tracktardies
                                ,@TardiesMakingAbsence;

                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                        END;

                                    IF (
                                       @Actual <> 9999
                                       AND @Actual <> 999
                                       )
                                        BEGIN
                                            SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + @ScheduledMinutes;
                                            SET @ActualRunningPresentHours = ISNULL(@ActualRunningPresentHours, 0) + @Actual;
                                            SET @AdjustedRunningPresentHours = ISNULL(@AdjustedRunningPresentHours, 0) + @Actual;
                                        END;
                                    SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + @Absent;
                                    IF (
                                       @Actual = 0
                                       AND @ScheduledMinutes >= 1
                                       AND @ScheduledMinutes NOT IN ( 999, 9999 )
                                       )
                                        BEGIN
                                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                                        END;

                                    -- NWH 
                                    -- If Scheduled = 3.0 hrs and Actual = 1.0 Then 2 hrs is considered absent
                                    IF (
                                       @ScheduledMinutes >= 1
                                       AND @ScheduledMinutes NOT IN ( 999, 9999 )
                                       AND @Actual > 0
                                       AND ( @Actual < @ScheduledMinutes )
                                       )
                                        BEGIN
                                            SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                                            SET @AdjustedRunningAbsentHours = ISNULL(@AdjustedRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                                        END;

                                    IF (
                                       @tracktardies = 1
                                       AND @IsTardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + 1;
                                        END;
                                    --commented by balaji on 10/22/2012 as report (rdl) doesn''t add days attended and make up days
                                    ---- If there are make up hrs deduct that otherwise it will be added again in progress report
                                    IF (
                                       @Actual > 0
                                       AND @Actual > @ScheduledMinutes
                                       AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                        END;

                                    IF @tracktardies = 1
                                       AND (
                                           @TardyMinutes > 0
                                           OR @IsTardy = 1
                                           )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;



                                    -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                       @Actual > 0
                                       AND @Actual > @ScheduledMinutes
                                       AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;

                                    IF (
                                       @tracktardies = 1
                                       AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;


                                    INSERT INTO @attendanceSummary (
                                                                   StuEnrollId
                                                                  ,ClsSectionId
                                                                  ,StudentAttendedDate
                                                                  ,ScheduledDays
                                                                  ,ActualDays
                                                                  ,ActualRunningScheduledDays
                                                                  ,ActualRunningPresentDays
                                                                  ,ActualRunningAbsentDays
                                                                  ,ActualRunningMakeupDays
                                                                  ,ActualRunningTardyDays
                                                                  ,AdjustedPresentDays
                                                                  ,AdjustedAbsentDays
                                                                  ,AttendanceTrackType
                                                                  ,ModUser
                                                                  ,ModDate
                                                                  ,TardiesMakingAbsence
                                                                   )
                                    VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, ISNULL(@ScheduledMinutes, 0), @Actual
                                            ,ISNULL(@ActualRunningScheduledDays, 0), ISNULL(@ActualRunningPresentHours, 0)
                                            ,ISNULL(@ActualRunningAbsentHours, 0), ISNULL(@MakeupHours, 0), ISNULL(@ActualRunningTardyHours, 0)
                                            ,ISNULL(@AdjustedRunningPresentHours, 0), ISNULL(@AdjustedRunningAbsentHours, 0), ''Post Attendance by Class'', ''sa''
                                            ,GETDATE(), @TardiesMakingAbsence );

                                    --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                                    --end
                                    SET @PrevStuEnrollId = @StuEnrollId;
                                    FETCH NEXT FROM GetAttendance_Cursor
                                    INTO @StuEnrollId
                                        ,@MeetDate
                                        ,@Actual
                                        ,@ScheduledMinutes
                                        ,@Absent
                                        ,@IsTardy
                                        ,@tracktardies
                                        ,@TardiesMakingAbsence;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
                        END;
                END;

                --end

                -- Based on grade rounding round the final score and current score
                --Declare @PrevTermId uniqueidentifier,@PrevReqId uniqueidentifier,@RowCount int,@FinalScore decimal(18,4),@CurrentScore decimal(18,4)
                --declare @ReqId uniqueidentifier,@CourseCodeDescrip varchar(100),@FinalGrade varchar(10),@sysComponentTypeId int
                --declare @CreditsAttempted decimal(18,4),@Grade varchar(10),@IsPass bit,@IsCreditsAttempted bit,@IsCreditsEarned bit
                --declare @IsInGPA bit,@FinAidCredits decimal(18,4),@CurrentGrade varchar(10),@CourseCredits decimal(18,4)
                DECLARE @GPA DECIMAL(18, 4);

                DECLARE @PrevReqId UNIQUEIDENTIFIER
                       ,@PrevTermId UNIQUEIDENTIFIER
                       ,@CreditsAttempted DECIMAL(18, 2)
                       ,@CreditsEarned DECIMAL(18, 2);
                DECLARE @reqid UNIQUEIDENTIFIER
                       ,@CourseCodeDescrip VARCHAR(50)
                       ,@FinalGrade VARCHAR(50)
                       ,@FinalScore DECIMAL(18, 2)
                       ,@Grade VARCHAR(50);
                DECLARE @IsPass BIT
                       ,@IsCreditsAttempted BIT
                       ,@IsCreditsEarned BIT
                       ,@CurrentScore DECIMAL(18, 2)
                       ,@CurrentGrade VARCHAR(10)
                       ,@FinalGPA DECIMAL(18, 2);
                DECLARE @sysComponentTypeId INT
                       ,@RowCount INT;
                DECLARE @IsInGPA BIT;
                DECLARE @CourseCredits DECIMAL(18, 2);
                DECLARE @FinAidCreditsEarned DECIMAL(18, 2)
                       ,@FinAidCredits DECIMAL(18, 2);

                DECLARE GetCreditsSummary_Cursor CURSOR FOR
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,T.TermId
                                       ,T.TermDescrip
                                       ,T.StartDate
                                       ,R.ReqId
                                       ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                       ,RES.Score AS FinalScore
                                       ,RES.GrdSysDetailId AS FinalGrade
                                       ,GCT.SysComponentTypeId
                                       ,R.Credits AS CreditsAttempted
                                       ,CS.ClsSectionId
                                       ,GSD.Grade
                                       ,GSD.IsPass
                                       ,GSD.IsCreditsAttempted
                                       ,GSD.IsCreditsEarned
                                       ,SE.PrgVerId
                                       ,GSD.IsInGPA
                                       ,R.FinAidCredits AS FinAidCredits
                    FROM       arStuEnrollments SE
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                    INNER JOIN arTerm T ON CS.TermId = T.TermId
                    INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                    LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                    LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                    LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                    LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    WHERE      SE.StuEnrollId = @StuEnrollId
                               AND (
                                   RES.DateCompleted IS NOT NULL
                                   AND RES.DateCompleted <= @OffsetDate
                                   )
                    ORDER BY   T.StartDate
                              ,T.TermDescrip
                              ,R.ReqId;

                DECLARE @varGradeRounding VARCHAR(3);
                DECLARE @roundfinalscore DECIMAL(18, 4);
                SET @varGradeRounding = (
                                        SELECT Value
                                        FROM   syConfigAppSetValues
                                        WHERE  SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@FinalGrade
                    ,@sysComponentTypeId
                    ,@CreditsAttempted
                    ,@ClsSectionId
                    ,@Grade
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN


                        SET @CurrentScore = (
                                            SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                        ELSE NULL
                                                   END AS CurrentScore
                                            FROM   (
                                                   SELECT   InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                   FROM     (
                                                            SELECT   C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,ISNULL(C.Weight, 0) AS Weight
                                                                    ,C.Number AS MinNumber
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter AS Param
                                                                    ,X.GrdScaleId
                                                                    ,SUM(GR.Score) AS Score
                                                                    ,COUNT(D.Descrip) AS NumberOfComponents
                                                            FROM     (
                                                                     SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                            ,A.EffectiveDate
                                                                                            ,B.GrdScaleId
                                                                     FROM     arGrdBkWeights A
                                                                             ,arClassSections B
                                                                     WHERE    A.ReqId = B.ReqId
                                                                              AND A.EffectiveDate <= B.StartDate
                                                                              AND B.ClsSectionId = @ClsSectionId
                                                                     ORDER BY A.EffectiveDate DESC
                                                                     ) X
                                                                    ,arGrdBkWgtDetails C
                                                                    ,arGrdComponentTypes D
                                                                    ,arGrdBkResults GR
                                                            WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                     AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                     AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                     AND D.SysComponentTypeId NOT IN ( 500, 503 )
                                                                     AND GR.StuEnrollId = @StuEnrollId
                                                                     AND GR.ClsSectionId = @ClsSectionId
                                                                     AND GR.Score IS NOT NULL
                                                            GROUP BY C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,C.Weight
                                                                    ,C.Number
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter
                                                                    ,X.GrdScaleId
                                                            ) S
                                                   GROUP BY InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                   ) FinalTblToComputeCurrentScore
                                            );
                        IF ( @CurrentScore IS NULL )
                            BEGIN
                                -- instructor grade books
                                SET @CurrentScore = (
                                                    SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                                ELSE NULL
                                                           END AS CurrentScore
                                                    FROM   (
                                                           SELECT   InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight AS GradeBookWeight
                                                                   ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                         ELSE 0
                                                                    END AS ActualWeight
                                                           FROM     (
                                                                    SELECT   C.InstrGrdBkWgtDetailId
                                                                            ,D.Code
                                                                            ,D.Descrip
                                                                            ,ISNULL(C.Weight, 0) AS Weight
                                                                            ,C.Number AS MinNumber
                                                                            ,C.GrdPolicyId
                                                                            ,C.Parameter AS Param
                                                                            ,X.GrdScaleId
                                                                            ,SUM(GR.Score) AS Score
                                                                            ,COUNT(D.Descrip) AS NumberOfComponents
                                                                    FROM     (
                                                                             --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
                                                                             --FROM          arGrdBkWeights A,arClassSections B        
                                                                             --WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
                                                                             --ORDER BY      A.EffectiveDate DESC
                                                                             SELECT DISTINCT TOP 1 t1.InstrGrdBkWgtId
                                                                                                  ,t1.GrdScaleId
                                                                             FROM   arClassSections t1
                                                                                   ,arGrdBkWeights t2
                                                                             WHERE  t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                                    AND t1.ClsSectionId = @ClsSectionId
                                                                             ) X
                                                                            ,arGrdBkWgtDetails C
                                                                            ,arGrdComponentTypes D
                                                                            ,arGrdBkResults GR
                                                                    WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                             AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                             AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                             AND
                                                                        -- D.SysComponentTypeID not in (500,503) and 
                                                                        GR.StuEnrollId = @StuEnrollId
                                                                             AND GR.ClsSectionId = @ClsSectionId
                                                                             AND GR.Score IS NOT NULL
                                                                    GROUP BY C.InstrGrdBkWgtDetailId
                                                                            ,D.Code
                                                                            ,D.Descrip
                                                                            ,C.Weight
                                                                            ,C.Number
                                                                            ,C.GrdPolicyId
                                                                            ,C.Parameter
                                                                            ,X.GrdScaleId
                                                                    ) S
                                                           GROUP BY InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight
                                                                   ,NumberOfComponents
                                                           ) FinalTblToComputeCurrentScore
                                                    );

                            END;


                        -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore, 0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;

                        UPDATE syCreditSummary
                        SET    FinalScore = @FinalScore
                              ,CurrentScore = @CurrentScore
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId
                               AND ReqId = @reqid;

                        --Average calculation

                        -- Term Average
                        SET @TermAverageCount = (
                                                SELECT COUNT(*)
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                       AND TermId = @TermId
                                                       AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                              SELECT SUM(FinalScore)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND TermId = @TermId
                                                     AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount;

                        -- Cumulative Average
                        SET @cumAveragecount = (
                                               SELECT COUNT(*)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                             SELECT SUM(FinalScore)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount;

                        UPDATE syCreditSummary
                        SET    Average = @TermAverage
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId;

                        --Update Cumulative GPA
                        UPDATE syCreditSummary
                        SET    CumAverage = @CumAverage
                        WHERE  StuEnrollId = @StuEnrollId;


                        FETCH NEXT FROM GetCreditsSummary_Cursor
                        INTO @StuEnrollId
                            ,@TermId
                            ,@TermDescrip
                            ,@termstartdate1
                            ,@reqid
                            ,@CourseCodeDescrip
                            ,@FinalScore
                            ,@FinalGrade
                            ,@sysComponentTypeId
                            ,@CreditsAttempted
                            ,@ClsSectionId
                            ,@Grade
                            ,@IsPass
                            ,@IsCreditsAttempted
                            ,@IsCreditsEarned
                            ,@PrgVerId
                            ,@IsInGPA
                            ,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;


                DECLARE GetCreditsSummary_Cursor CURSOR FOR
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,T.TermId
                                       ,T.TermDescrip
                                       ,T.StartDate
                                       ,R.ReqId
                                       ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                       ,RES.Score AS FinalScore
                                       ,RES.GrdSysDetailId AS FinalGrade
                                       ,GCT.SysComponentTypeId
                                       ,R.Credits AS CreditsAttempted
                                       ,CS.ClsSectionId
                                       ,GSD.Grade
                                       ,GSD.IsPass
                                       ,GSD.IsCreditsAttempted
                                       ,GSD.IsCreditsEarned
                                       ,SE.PrgVerId
                                       ,GSD.IsInGPA
                                       ,R.FinAidCredits AS FinAidCredits
                    FROM       arStuEnrollments SE
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                                     AND RES.TermId = CS.TermId
                    INNER JOIN arTerm T ON CS.TermId = T.TermId
                    INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                    LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                    LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                    LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                    LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    WHERE      SE.StuEnrollId = @StuEnrollId
                               AND (
                                   RES.CompletedDate IS NOT NULL
                                   AND RES.CompletedDate <= @OffsetDate
                                   )
                    ORDER BY   T.StartDate
                              ,T.TermDescrip
                              ,R.ReqId;


                SET @varGradeRounding = (
                                        SELECT Value
                                        FROM   syConfigAppSetValues
                                        WHERE  SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@FinalGrade
                    ,@sysComponentTypeId
                    ,@CreditsAttempted
                    ,@ClsSectionId
                    ,@Grade
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        SET @CurrentScore = (
                                            SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                        ELSE NULL
                                                   END AS CurrentScore
                                            FROM   (
                                                   SELECT   InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                   FROM     (
                                                            SELECT   C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,ISNULL(C.Weight, 0) AS Weight
                                                                    ,C.Number AS MinNumber
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter AS Param
                                                                    ,X.GrdScaleId
                                                                    ,SUM(GR.Score) AS Score
                                                                    ,COUNT(D.Descrip) AS NumberOfComponents
                                                            FROM     (
                                                                     SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                            ,A.EffectiveDate
                                                                                            ,B.GrdScaleId
                                                                     FROM     arGrdBkWeights A
                                                                             ,arClassSections B
                                                                     WHERE    A.ReqId = B.ReqId
                                                                              AND A.EffectiveDate <= B.StartDate
                                                                              AND B.ClsSectionId = @ClsSectionId
                                                                     ORDER BY A.EffectiveDate DESC
                                                                     ) X
                                                                    ,arGrdBkWgtDetails C
                                                                    ,arGrdComponentTypes D
                                                                    ,arGrdBkResults GR
                                                            WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                     AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                     AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                     AND D.SysComponentTypeId NOT IN ( 500, 503 )
                                                                     AND GR.StuEnrollId = @StuEnrollId
                                                                     AND GR.ClsSectionId = @ClsSectionId
                                                                     AND GR.Score IS NOT NULL
                                                            GROUP BY C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,C.Weight
                                                                    ,C.Number
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter
                                                                    ,X.GrdScaleId
                                                            ) S
                                                   GROUP BY InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                   ) FinalTblToComputeCurrentScore
                                            );
                        IF ( @CurrentScore IS NULL )
                            BEGIN
                                -- instructor grade books
                                SET @CurrentScore = (
                                                    SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                                ELSE NULL
                                                           END AS CurrentScore
                                                    FROM   (
                                                           SELECT   InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight AS GradeBookWeight
                                                                   ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                         ELSE 0
                                                                    END AS ActualWeight
                                                           FROM     (
                                                                    SELECT   C.InstrGrdBkWgtDetailId
                                                                            ,D.Code
                                                                            ,D.Descrip
                                                                            ,ISNULL(C.Weight, 0) AS Weight
                                                                            ,C.Number AS MinNumber
                                                                            ,C.GrdPolicyId
                                                                            ,C.Parameter AS Param
                                                                            ,X.GrdScaleId
                                                                            ,SUM(GR.Score) AS Score
                                                                            ,COUNT(D.Descrip) AS NumberOfComponents
                                                                    FROM     (
                                                                             --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
                                                                             --FROM          arGrdBkWeights A,arClassSections B        
                                                                             --WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
                                                                             --ORDER BY      A.EffectiveDate DESC
                                                                             SELECT DISTINCT TOP 1 t1.InstrGrdBkWgtId
                                                                                                  ,t1.GrdScaleId
                                                                             FROM   arClassSections t1
                                                                                   ,arGrdBkWeights t2
                                                                             WHERE  t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                                    AND t1.ClsSectionId = @ClsSectionId
                                                                             ) X
                                                                            ,arGrdBkWgtDetails C
                                                                            ,arGrdComponentTypes D
                                                                            ,arGrdBkResults GR
                                                                    WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                             AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                             AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                             AND
                                                                        -- D.SysComponentTypeID not in (500,503) and 
                                                                        GR.StuEnrollId = @StuEnrollId
                                                                             AND GR.ClsSectionId = @ClsSectionId
                                                                             AND GR.Score IS NOT NULL
                                                                    GROUP BY C.InstrGrdBkWgtDetailId
                                                                            ,D.Code
                                                                            ,D.Descrip
                                                                            ,C.Weight
                                                                            ,C.Number
                                                                            ,C.GrdPolicyId
                                                                            ,C.Parameter
                                                                            ,X.GrdScaleId
                                                                    ) S
                                                           GROUP BY InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight
                                                                   ,NumberOfComponents
                                                           ) FinalTblToComputeCurrentScore
                                                    );

                            END;

                        -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore, 0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;

                        UPDATE syCreditSummary
                        SET    FinalScore = @FinalScore
                              ,CurrentScore = @CurrentScore
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId
                               AND ReqId = @reqid;

                        --Average calculation
                        --			declare @CumAverage decimal(18,2),@cumAverageSum decimal(18,2),@cumAveragecount int

                        -- Term Average
                        SET @TermAverageCount = (
                                                SELECT COUNT(*)
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                       AND TermId = @TermId
                                                       AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                              SELECT SUM(FinalScore)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND TermId = @TermId
                                                     AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount;

                        -- Cumulative Average
                        SET @cumAveragecount = (
                                               SELECT COUNT(*)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                             SELECT SUM(FinalScore)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount;

                        UPDATE syCreditSummary
                        SET    Average = @TermAverage
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId;

                        --Update Cumulative GPA
                        UPDATE syCreditSummary
                        SET    CumAverage = @CumAverage
                        WHERE  StuEnrollId = @StuEnrollId;


                        FETCH NEXT FROM GetCreditsSummary_Cursor
                        INTO @StuEnrollId
                            ,@TermId
                            ,@TermDescrip
                            ,@termstartdate1
                            ,@reqid
                            ,@CourseCodeDescrip
                            ,@FinalScore
                            ,@FinalGrade
                            ,@sysComponentTypeId
                            ,@CreditsAttempted
                            ,@ClsSectionId
                            ,@Grade
                            ,@IsPass
                            ,@IsCreditsAttempted
                            ,@IsCreditsEarned
                            ,@PrgVerId
                            ,@IsInGPA
                            ,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

                DECLARE @GradeSystemDetailId UNIQUEIDENTIFIER
                       ,@IsGPA BIT;
                DECLARE GetCreditsSummary_Cursor CURSOR FOR
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,T.TermId
                                       ,T.TermDescrip
                                       ,T.StartDate
                                       ,R.ReqId
                                       ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                       ,NULL AS FinalScore
                                       ,RES.GrdSysDetailId AS GradeSystemDetailId
                                       ,GSD.Grade AS FinalGrade
                                       ,GSD.Grade AS CurrentGrade
                                       ,R.Credits
                                       ,NULL AS ClsSectionId
                                       ,GSD.IsPass
                                       ,GSD.IsCreditsAttempted
                                       ,GSD.IsCreditsEarned
                                       ,GSD.GPA
                                       ,GSD.IsInGPA
                                       ,SE.PrgVerId
                                       ,GSD.IsInGPA
                                       ,R.FinAidCredits AS FinAidCredits
                    FROM       arStuEnrollments SE
                    INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
                    INNER JOIN syCreditSummary CS ON CS.StuEnrollId = RES.StuEnrollId
                    INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                    INNER JOIN arTerm T ON RES.TermId = T.TermId
                    WHERE      RES.ReqId NOT IN (
                                                SELECT DISTINCT ReqId
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = SE.StuEnrollId
                                                       AND TermId = T.TermId
                                                )
                               AND SE.StuEnrollId = @StuEnrollId
                               AND (
                                   RES.CompletedDate IS NOT NULL
                                   AND RES.CompletedDate <= @OffsetDate
                                   );

                SET @varGradeRounding = (
                                        SELECT Value
                                        FROM   syConfigAppSetValues
                                        WHERE  SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@GradeSystemDetailId
                    ,@FinalGrade
                    ,@CurrentGrade
                    ,@CourseCredits
                    ,@ClsSectionId
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@GPA
                    ,@IsGPA
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore, 0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;

                        UPDATE syCreditSummary
                        SET    FinalScore = @FinalScore
                              ,CurrentScore = @CurrentScore
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId
                               AND ReqId = @reqid;

                        --Average calculation

                        -- Term Average
                        SET @TermAverageCount = (
                                                SELECT COUNT(*)
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                       AND TermId = @TermId
                                                       AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                              SELECT SUM(FinalScore)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND TermId = @TermId
                                                     AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount;

                        -- Cumulative Average
                        SET @cumAveragecount = (
                                               SELECT COUNT(*)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                               --AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                             SELECT SUM(FinalScore)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount;

                        UPDATE syCreditSummary
                        SET    Average = @TermAverage
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId;

                        --Update Cumulative GPA
                        UPDATE syCreditSummary
                        SET    CumAverage = @CumAverage
                        WHERE  StuEnrollId = @StuEnrollId;


                        FETCH NEXT FROM GetCreditsSummary_Cursor
                        INTO @StuEnrollId
                            ,@TermId
                            ,@TermDescrip
                            ,@termstartdate1
                            ,@reqid
                            ,@CourseCodeDescrip
                            ,@FinalScore
                            ,@GradeSystemDetailId
                            ,@FinalGrade
                            ,@CurrentGrade
                            ,@CourseCredits
                            ,@ClsSectionId
                            ,@IsPass
                            ,@IsCreditsAttempted
                            ,@IsCreditsEarned
                            ,@GPA
                            ,@IsGPA
                            ,@PrgVerId
                            ,@IsInGPA
                            ,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

                DECLARE GetCreditsSummary_Cursor CURSOR FOR
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,T.TermId
                                       ,T.TermDescrip
                                       ,T.StartDate
                                       ,R.ReqId
                                       ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                       ,NULL AS FinalScore
                                       ,RES.GrdSysDetailId AS GradeSystemDetailId
                                       ,GSD.Grade AS FinalGrade
                                       ,GSD.Grade AS CurrentGrade
                                       ,R.Credits
                                       ,NULL AS ClsSectionId
                                       ,GSD.IsPass
                                       ,GSD.IsCreditsAttempted
                                       ,GSD.IsCreditsEarned
                                       ,GSD.GPA
                                       ,GSD.IsInGPA
                                       ,SE.PrgVerId
                                       ,GSD.IsInGPA
                                       ,R.FinAidCredits AS FinAidCredits
                    FROM       arStuEnrollments SE
                    INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
                    INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                    INNER JOIN arTerm T ON RES.TermId = T.TermId
                    WHERE      SE.StuEnrollId NOT IN (
                                                     SELECT DISTINCT StuEnrollId
                                                     FROM   syCreditSummary
                                                     )
                               AND SE.StuEnrollId = @StuEnrollId
                               AND (
                                   RES.CompletedDate IS NOT NULL
                                   AND RES.CompletedDate <= @OffsetDate
                                   );

                SET @varGradeRounding = (
                                        SELECT Value
                                        FROM   syConfigAppSetValues
                                        WHERE  SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@GradeSystemDetailId
                    ,@FinalGrade
                    ,@CurrentGrade
                    ,@CourseCredits
                    ,@ClsSectionId
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@GPA
                    ,@IsGPA
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore, 0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;

                        UPDATE syCreditSummary
                        SET    FinalScore = @FinalScore
                              ,CurrentScore = @CurrentScore
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId
                               AND ReqId = @reqid;

                        --Average calculation
                        SET @TermAverageCount = (
                                                SELECT COUNT(*)
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                       AND TermId = @TermId
                                                       AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                              SELECT SUM(FinalScore)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND TermId = @TermId
                                                     AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount;

                        -- Cumulative Average
                        SET @cumAveragecount = (
                                               SELECT COUNT(*)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                             SELECT SUM(FinalScore)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount;

                        UPDATE syCreditSummary
                        SET    Average = @TermAverage
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId;

                        --Update Cumulative GPA
                        UPDATE syCreditSummary
                        SET    CumAverage = @CumAverage
                        WHERE  StuEnrollId = @StuEnrollId;


                        FETCH NEXT FROM GetCreditsSummary_Cursor
                        INTO @StuEnrollId
                            ,@TermId
                            ,@TermDescrip
                            ,@termstartdate1
                            ,@reqid
                            ,@CourseCodeDescrip
                            ,@FinalScore
                            ,@GradeSystemDetailId
                            ,@FinalGrade
                            ,@CurrentGrade
                            ,@CourseCredits
                            ,@ClsSectionId
                            ,@IsPass
                            ,@IsCreditsAttempted
                            ,@IsCreditsEarned
                            ,@GPA
                            ,@IsGPA
                            ,@PrgVerId
                            ,@IsInGPA
                            ,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

            ---- PRINT ''@CurrentBal_Compute'', 
            ---- PRINT @CurrentBal_Compute


            END;

        SET @TermAverageCount = (
                                SELECT COUNT(*)
                                FROM   syCreditSummary
                                WHERE  StuEnrollId = @StuEnrollId
                                       AND FinalScore IS NOT NULL
                                );
        SET @termAverageSum = (
                              SELECT SUM(FinalScore)
                              FROM   syCreditSummary
                              WHERE  StuEnrollId = @StuEnrollId
                                     AND FinalScore IS NOT NULL
                              );
        IF @TermAverageCount >= 1
            BEGIN
                SET @TermAverage = @termAverageSum / @TermAverageCount;
            END;

        -- Cumulative Average
        SET @cumAveragecount = (
                               SELECT COUNT(*)
                               FROM   (
                                      SELECT     sCS.StuEnrollId
                                                ,sCS.TermId
                                                ,sCS.TermDescrip
                                                ,sCS.ReqId
                                                ,sCS.ReqDescrip
                                                ,sCS.ClsSectionId
                                                ,sCS.CreditsEarned
                                                ,sCS.CreditsAttempted
                                                ,sCS.CurrentScore
                                                ,sCS.CurrentGrade
                                                ,sCS.FinalScore
                                                ,sCS.FinalGrade
                                                ,sCS.Completed
                                                ,sCS.FinalGPA
                                                ,sCS.Product_WeightedAverage_Credits_GPA
                                                ,sCS.Count_WeightedAverage_Credits
                                                ,sCS.Product_SimpleAverage_Credits_GPA
                                                ,sCS.Count_SimpleAverage_Credits
                                                ,sCS.ModUser
                                                ,sCS.ModDate
                                                ,sCS.TermGPA_Simple
                                                ,sCS.TermGPA_Weighted
                                                ,sCS.coursecredits
                                                ,sCS.CumulativeGPA
                                                ,sCS.CumulativeGPA_Simple
                                                ,sCS.FACreditsEarned
                                                ,sCS.Average
                                                ,sCS.CumAverage
                                                ,sCS.TermStartDate
                                      FROM       dbo.syCreditSummary sCS
                                      INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                      INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                      WHERE      sCS.StuEnrollId = @StuEnrollId
                                                 AND CS.ReqId = sCS.ReqId
                                                 AND (
                                                     R.DateCompleted IS NOT NULL
                                                     AND R.DateCompleted <= @OffsetDate
                                                     )
                                      UNION ALL
                                      (SELECT     sCS.StuEnrollId
                                                 ,sCS.TermId
                                                 ,sCS.TermDescrip
                                                 ,sCS.ReqId
                                                 ,sCS.ReqDescrip
                                                 ,sCS.ClsSectionId
                                                 ,sCS.CreditsEarned
                                                 ,sCS.CreditsAttempted
                                                 ,sCS.CurrentScore
                                                 ,sCS.CurrentGrade
                                                 ,sCS.FinalScore
                                                 ,sCS.FinalGrade
                                                 ,sCS.Completed
                                                 ,sCS.FinalGPA
                                                 ,sCS.Product_WeightedAverage_Credits_GPA
                                                 ,sCS.Count_WeightedAverage_Credits
                                                 ,sCS.Product_SimpleAverage_Credits_GPA
                                                 ,sCS.Count_SimpleAverage_Credits
                                                 ,sCS.ModUser
                                                 ,sCS.ModDate
                                                 ,sCS.TermGPA_Simple
                                                 ,sCS.TermGPA_Weighted
                                                 ,sCS.coursecredits
                                                 ,sCS.CumulativeGPA
                                                 ,sCS.CumulativeGPA_Simple
                                                 ,sCS.FACreditsEarned
                                                 ,sCS.Average
                                                 ,sCS.CumAverage
                                                 ,sCS.TermStartDate
                                       FROM       dbo.syCreditSummary sCS
                                       INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                       WHERE      sCS.StuEnrollId = @StuEnrollId
                                                  AND tG.ReqId = sCS.ReqId
                                                  AND (
                                                      tG.CompletedDate IS NOT NULL
                                                      AND tG.CompletedDate <= @OffsetDate
                                                      ))
                                      ) sCSEA
                               WHERE  StuEnrollId = @StuEnrollId
                                      AND FinalScore IS NOT NULL
                               );
        SET @cumAverageSum = (
                             SELECT SUM(FinalScore)
                             FROM   (
                                    SELECT     sCS.StuEnrollId
                                              ,sCS.TermId
                                              ,sCS.TermDescrip
                                              ,sCS.ReqId
                                              ,sCS.ReqDescrip
                                              ,sCS.ClsSectionId
                                              ,sCS.CreditsEarned
                                              ,sCS.CreditsAttempted
                                              ,sCS.CurrentScore
                                              ,sCS.CurrentGrade
                                              ,sCS.FinalScore
                                              ,sCS.FinalGrade
                                              ,sCS.Completed
                                              ,sCS.FinalGPA
                                              ,sCS.Product_WeightedAverage_Credits_GPA
                                              ,sCS.Count_WeightedAverage_Credits
                                              ,sCS.Product_SimpleAverage_Credits_GPA
                                              ,sCS.Count_SimpleAverage_Credits
                                              ,sCS.ModUser
                                              ,sCS.ModDate
                                              ,sCS.TermGPA_Simple
                                              ,sCS.TermGPA_Weighted
                                              ,sCS.coursecredits
                                              ,sCS.CumulativeGPA
                                              ,sCS.CumulativeGPA_Simple
                                              ,sCS.FACreditsEarned
                                              ,sCS.Average
                                              ,sCS.CumAverage
                                              ,sCS.TermStartDate
                                    FROM       dbo.syCreditSummary sCS
                                    INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                    INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                    WHERE      sCS.StuEnrollId = @StuEnrollId
                                               AND CS.ReqId = sCS.ReqId
                                               AND (
                                                   R.DateCompleted IS NOT NULL
                                                   AND R.DateCompleted <= @OffsetDate
                                                   )
                                    UNION ALL
                                    (SELECT     sCS.StuEnrollId
                                               ,sCS.TermId
                                               ,sCS.TermDescrip
                                               ,sCS.ReqId
                                               ,sCS.ReqDescrip
                                               ,sCS.ClsSectionId
                                               ,sCS.CreditsEarned
                                               ,sCS.CreditsAttempted
                                               ,sCS.CurrentScore
                                               ,sCS.CurrentGrade
                                               ,sCS.FinalScore
                                               ,sCS.FinalGrade
                                               ,sCS.Completed
                                               ,sCS.FinalGPA
                                               ,sCS.Product_WeightedAverage_Credits_GPA
                                               ,sCS.Count_WeightedAverage_Credits
                                               ,sCS.Product_SimpleAverage_Credits_GPA
                                               ,sCS.Count_SimpleAverage_Credits
                                               ,sCS.ModUser
                                               ,sCS.ModDate
                                               ,sCS.TermGPA_Simple
                                               ,sCS.TermGPA_Weighted
                                               ,sCS.coursecredits
                                               ,sCS.CumulativeGPA
                                               ,sCS.CumulativeGPA_Simple
                                               ,sCS.FACreditsEarned
                                               ,sCS.Average
                                               ,sCS.CumAverage
                                               ,sCS.TermStartDate
                                     FROM       dbo.syCreditSummary sCS
                                     INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                     WHERE      sCS.StuEnrollId = @StuEnrollId
                                                AND tG.ReqId = sCS.ReqId
                                                AND (
                                                    tG.CompletedDate IS NOT NULL
                                                    AND tG.CompletedDate <= @OffsetDate
                                                    ))
                                    ) sCSEA
                             WHERE  StuEnrollId = @StuEnrollId
                                    AND FinalScore IS NOT NULL
                             );

        IF @cumAveragecount >= 1
            BEGIN
                SET @CumAverage = @cumAverageSum / @cumAveragecount;
            END;

		--If Clock Hour / Numeric - use New Average Calculation
        IF (
           SELECT TOP 1 P.ACId
           FROM   dbo.arStuEnrollments E
           JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
           JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
           WHERE  E.StuEnrollId = @StuEnrollId
           ) = 5
           AND @GradesFormat = ''numeric''
            BEGIN
                EXEC dbo.USP_GPACalculator @EnrollmentId = @StuEnrollId
                                          ,@EndDate = @OffsetDate
                                          ,@StudentGPA = @CumAverage OUTPUT;
        END;

        IF @GradesFormat <> ''letter''
            BEGIN
                SET @Qualitative = @CumAverage;
            END;
        ELSE
            SET @Qualitative = @cumWeightedGPA;

        IF ( @QuantMinUnitTypeId = 3 )
            BEGIN
                IF @UnitTypeDescrip IN ( ''none'', ''present absent'' )
                   AND @TrackSapAttendance = ''byclass''
                    BEGIN
                        SELECT @Quantitative = CAST(( sas.Present / sas.Scheduled ) * 100 AS DECIMAL)
                              ,@ActualHours = sas.Present
                              ,@ScheduledHours = sas.Scheduled
                        FROM   (
                               SELECT   MAX(convertedSAS.ConvertedActualRunningScheduledDays) AS Scheduled
                                       ,MAX(convertedSAS.ConvertedActualRunningPresentDays) + MAX(convertedSAS.ConvertedActualRunningMakeupDays) AS Present -- add makeup to present since procedure splits it before added into syattendance summary
                               FROM     (
                                        SELECT    sySas.StuEnrollId
                                                 ,sySas.StudentAttendedDate
                                                 ,sySas.ClsSectionId
                                                 ,sySas.ActualRunningScheduledDays * ISNULL(cim.IntervalInMinutes, 1) AS ConvertedActualRunningScheduledDays
                                                 ,sySas.ActualRunningPresentDays * ISNULL(cim.IntervalInMinutes, 1) AS ConvertedActualRunningPresentDays
                                                 ,sySas.ActualRunningMakeupDays * ISNULL(cim.IntervalInMinutes, 1) AS ConvertedActualRunningMakeupDays
                                        FROM      @attendanceSummary sySas
                                        LEFT JOIN @ClsSectionIntervalMinutes cim ON cim.ClsSectionId = sySas.ClsSectionId
                                        ) AS convertedSAS
                               WHERE    convertedSAS.StuEnrollId = @StuEnrollId
                                        AND convertedSAS.StudentAttendedDate <= @OffsetDate
                               GROUP BY StuEnrollId
                               ) AS sas;



                    END;
                ELSE
                    BEGIN

                        SELECT @Quantitative = CAST(( sas.Present / sas.Scheduled ) * 100 AS DECIMAL)
                              ,@ActualHours = sas.Present
                              ,@ScheduledHours = sas.Scheduled
                        FROM   (
                               SELECT   MAX(ActualRunningScheduledDays) AS Scheduled
                                       ,MAX(ActualRunningPresentDays) + MAX(ActualRunningMakeupDays) AS Present -- add makeup to present since procedure splits it before added into syattendance summary
                               FROM     @attendanceSummary
                               WHERE    StuEnrollId = @StuEnrollId
                                        AND StudentAttendedDate <= @OffsetDate
                               GROUP BY StuEnrollId
                               ) sas;
                    END;

            END;
        ELSE
            BEGIN
                SET @Quantitative = (
                                    SELECT CAST(( sCSEA.Earned / sCSEA.Attempted ) * 100 AS DECIMAL)
                                    FROM   (
                                           SELECT     SUM(sCS.CreditsEarned) Earned
                                                     ,SUM(sCS.CreditsAttempted) Attempted
                                           FROM       dbo.syCreditSummary sCS
                                           INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                           INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                           WHERE      sCS.StuEnrollId = @StuEnrollId
                                                      AND CS.ReqId = sCS.ReqId
                                                      AND (
                                                          R.DateCompleted IS NOT NULL
                                                          AND R.DateCompleted <= @OffsetDate
                                                          )
                                           GROUP BY   sCS.StuEnrollId
                                           UNION ALL
                                           (SELECT     SUM(sCS.CreditsEarned)
                                                      ,SUM(sCS.CreditsAttempted)
                                            FROM       dbo.syCreditSummary sCS
                                            INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                            WHERE      sCS.StuEnrollId = @StuEnrollId
                                                       AND tG.ReqId = sCS.ReqId
                                                       AND (
                                                           tG.CompletedDate IS NOT NULL
                                                           AND tG.CompletedDate <= @OffsetDate
                                                           )
                                            GROUP BY   sCS.StuEnrollId)
                                           ) sCSEA
                                    );
            END;

    --SELECT * FROM dbo.syCreditSummary WHERE StuEnrollId  = ''F3616299-DCF2-4F59-BC8F-2F3034ED01A3''

    ---- PRINT @cumAveragecount 
    ---- PRINT @CumAverageSum
    ---- PRINT @CumAverage
    ---- PRINT @UnitTypeId;
    ---- PRINT @TrackSapAttendance;
    ---- PRINT @displayHours;
    END;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_TR_Sub03_PrepareGPA]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub03_PrepareGPA]', 'P') IS NULL
EXEC sp_executesql N'-- =========================================================================================================
-- USP_TR_Sub03_PrepareGPA
-- =========================================================================================================
CREATE PROCEDURE [dbo].[USP_TR_Sub03_PrepareGPA]
    @StuEnrollIdList VARCHAR(MAX)
   ,@ShowMultipleEnrollments BIT = 0
   ,@CoursesTakenTableName NVARCHAR(200) OUTPUT
   ,@GPASummaryTableName NVARCHAR(200) OUTPUT
AS --
    BEGIN
        -- 00) Variable Definition, Create Temp Table and Set Initial Values
        BEGIN
            DECLARE @MinimunDate AS DATETIME;
            DECLARE @MaximunDate AS DATETIME;
            DECLARE @StuEnrollCampusId AS UNIQUEIDENTIFIER;

            DECLARE @GradesFormat AS VARCHAR(50);
            DECLARE @GPAMethod AS VARCHAR(50);
            DECLARE @GradeBookAt AS VARCHAR(50);
            DECLARE @RetakenPolicy AS VARCHAR(50);

            DECLARE @curStudentId AS VARCHAR(50);
            DECLARE @curStuEnrollId AS VARCHAR(50);
            DECLARE @curTermId AS UNIQUEIDENTIFIER;

            DECLARE @cumTermAverage AS DECIMAL(18, 2);

            DECLARE @cumEnroTermSimple_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumEnroTermSimple_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumEnroTermSimple_GPA AS DECIMAL(18, 2);

            DECLARE @cumEnroTermWeight_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumEnroTermWeight_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumEnroTermWeight_GPA AS DECIMAL(18, 2);

            DECLARE @cumStudTermSimple_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumStudTermSimple_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumStudTermSimple_GPA AS DECIMAL(18, 2);

            DECLARE @cumStudTermWeight_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumStudTermWeight_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumStudTermWeight_GPA AS DECIMAL(18, 2);

            DECLARE @cumEnroSimple_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumEnroSimple_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumEnroSimple_GPA AS DECIMAL(18, 2);

            DECLARE @cumEnroWeight_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumEnroWeight_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumEnroWeight_GPA AS DECIMAL(18, 2);

            DECLARE @cumStudSimple_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumStudSimple_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumStudSimple_GPA AS DECIMAL(18, 2);

            DECLARE @cumStudWeight_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumStudWeight_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumStudWeight_GPA AS DECIMAL(18, 2);

            DECLARE @IsMakingSAP AS BIT;
            DECLARE @NewID AS NVARCHAR(50);
            DECLARE @SQL01 AS NVARCHAR(MAX);
            DECLARE @SQL02 AS NVARCHAR(MAX);

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#CoursesTaken'')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            CREATE TABLE #CoursesTaken
                (
                    CoursesTakenId INTEGER IDENTITY(1, 1) NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,ReqId UNIQUEIDENTIFIER
                   ,ReqCode VARCHAR(100)
                   ,ReqDescription VARCHAR(100)
                   ,ReqCodeDescrip VARCHAR(100)
                   ,ReqCredits DECIMAL(18, 2)
                   ,MinVal DECIMAL(18, 2)
                   ,ClsSectionId VARCHAR(50)
                   ,CourseStarDate DATETIME
                   ,CourseEndDate DATETIME
                   ,SCS_CreditsAttempted DECIMAL(18, 2)
                   ,SCS_CreditsEarned DECIMAL(18, 2)
                   ,SCS_CurrentScore DECIMAL(18, 2)
                   ,SCS_CurrentGrade VARCHAR(10)
                   ,SCS_FinalScore DECIMAL(18, 2)
                   ,SCS_FinalGrade VARCHAR(10)
                   ,SCS_Completed BIT
                   ,SCS_FinalGPA DECIMAL(18, 2)
                   ,SCS_Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_WeightedAverage_Credits DECIMAL(18, 2)
                   ,SCS_Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_SimpleAverage_Credits DECIMAL(18, 2)
                   ,SCS_ModUser VARCHAR(50)
                   ,SCS_ModDate DATETIME
                   ,SCS_TermGPA_Simple DECIMAL(18, 2)
                   ,SCS_TermGPA_Weighted DECIMAL(18, 2)
                   ,SCS_coursecredits DECIMAL(18, 2)
                   ,SCS_CumulativeGPA DECIMAL(18, 2)
                   ,SCS_CumulativeGPA_Simple DECIMAL(18, 2)
                   ,SCS_FACreditsEarned DECIMAL(18, 2)
                   ,SCS_Average DECIMAL(18, 2)
                   ,SCS_CumAverage DECIMAL(18, 2)
                   ,ScheduleDays DECIMAL(18, 2)
                   ,ActualDay DECIMAL(18, 2)
                   ,FinalGPA_Calculations DECIMAL(18, 2)
                   ,FinalGPA_TermCalculations DECIMAL(18, 2)
                   ,CreditsEarned_Calculations DECIMAL(18, 2)
                   ,ActualDay_Calculations DECIMAL(18, 2)
                   ,GrdBkWgtDetailsCount INTEGER
                   ,CountMultipleEnrollment INTEGER
                   ,RowNumberMultipleEnrollment INTEGER
                   ,CountRepeated INTEGER
                   ,RowNumberRetaked INTEGER
                   ,SameTermCountRetaken INTEGER
                   ,RowNumberSameTermRetaked INTEGER
                   ,RowNumberMultEnrollNoRetaken INTEGER
                   ,RowNumberOverAllByClassSection INTEGER
                   ,IsCreditsEarned BIT
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#getStudentGPAbyTerms'')
                      )
                BEGIN
                    DROP TABLE #getStudentGPAbyTerms;
                END;
            CREATE TABLE #getStudentGPAbyTerms
                (
                    StudentGPAbyTermsId INTEGER IDENTITY(1, 1) NOT NULL PRIMARY KEY
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(50)
                   ,TermStartDate DATETIME
                   ,TermEndDate DATETIME
                   ,DescripXTranscript VARCHAR(250)
                   ,TermAverage DECIMAL(18, 2)
                   ,EnroTermSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPACredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPA DECIMAL(18, 2)
                   ,EnroTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPACredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPA DECIMAL(18, 2)
                   ,StudTermSimple_CourseCredits DECIMAL(18, 2)
                   ,StudTermSimple_GPACredits DECIMAL(18, 2)
                   ,StudTermSimple_GPA DECIMAL(18, 2)
                   ,StudTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudTermWeight_GPACredits DECIMAL(18, 2)
                   ,StudTermWeight_GPA DECIMAL(18, 2)
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#getStudentGPAByEnrollment'')
                      )
                BEGIN
                    DROP TABLE #getStudentGPAByEnrollment;
                END;
            CREATE TABLE #getStudentGPAByEnrollment
                (
                    StudentGPAByEnrollmentId INTEGER IDENTITY(1, 1) NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,LastName VARCHAR(50)
                   ,FirstName VARCHAR(50)
                   ,MiddleName VARCHAR(50)
                   ,SSN VARCHAR(11)
                   ,StudentNumber VARCHAR(50)
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,EnrollmentID VARCHAR(50)
                   ,PrgVerId UNIQUEIDENTIFIER
                   ,PrgVerDescrip VARCHAR(50)
                   ,PrgVersionTrackCredits BIT
                   ,GrdSystemId UNIQUEIDENTIFIER
                   ,AcademicType VARCHAR(50)
                   ,ClockHourProgram VARCHAR(10)
                   ,IsMakingSAP BIT
                   ,TermId UNIQUEIDENTIFIER
                   --,TermDescription VARCHAR(50)
                   ,TermAverage DECIMAL(18, 2)
                   ,EnroSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroSimple_GPACredits DECIMAL(18, 2)
                   ,EnroSimple_GPA DECIMAL(18, 2)
                   ,EnroWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroWeight_GPACredits DECIMAL(18, 2)
                   ,EnroWeight_GPA DECIMAL(18, 2)
                   ,StudSimple_CourseCredits DECIMAL(18, 2)
                   ,StudSimple_GPACredits DECIMAL(18, 2)
                   ,StudSimple_GPA DECIMAL(18, 2)
                   ,StudWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudWeight_GPA_Credits DECIMAL(18, 2)
                   ,StudWeight_GPA DECIMAL(18, 2)
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#GPASummary'')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
            CREATE TABLE #GPASummary
                (
                    GPASummaryId INTEGER IDENTITY(1, 1) NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,LastName VARCHAR(50)
                   ,FirstName VARCHAR(50)
                   ,MiddleName VARCHAR(50)
                   ,SSN VARCHAR(11)
                   ,StudentNumber VARCHAR(50)
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,EnrollmentID VARCHAR(50)
                   ,PrgVerId UNIQUEIDENTIFIER
                   ,PrgVerDescrip VARCHAR(50)
                   ,PrgVersionTrackCredits BIT
                   ,GrdSystemId UNIQUEIDENTIFIER
                   ,AcademicType VARCHAR(50)
                   ,ClockHourProgram VARCHAR(10)
                   ,IsMakingSAP BIT
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,TermEndDate DATETIME
                   ,DescripXTranscript VARCHAR(250)
                   ,TermAverage DECIMAL(18, 2)
                   ,EnroTermSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPACredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPA DECIMAL(18, 2)
                   ,EnroTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPACredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPA DECIMAL(18, 2)
                   ,StudTermSimple_CourseCredits DECIMAL(18, 2)
                   ,StudTermSimple_GPACredits DECIMAL(18, 2)
                   ,StudTermSimple_GPA DECIMAL(18, 2)
                   ,StudTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudTermWeight_GPACredits DECIMAL(18, 2)
                   ,StudTermWeight_GPA DECIMAL(18, 2)
                   ,EnroSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroSimple_GPACredits DECIMAL(18, 2)
                   ,EnroSimple_GPA DECIMAL(18, 2)
                   ,EnroWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroWeight_GPACredits DECIMAL(18, 2)
                   ,EnroWeight_GPA DECIMAL(18, 2)
                   ,StudSimple_CourseCredits DECIMAL(18, 2)
                   ,StudSimple_GPACredits DECIMAL(18, 2)
                   ,StudSimple_GPA DECIMAL(18, 2)
                   ,StudWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudWeight_GPACredits DECIMAL(18, 2)
                   ,StudWeight_GPA DECIMAL(18, 2)
                   ,GradesFormat VARCHAR(50)
                   ,GPAMethod VARCHAR(50)
                   ,GradeBookAt VARCHAR(50)
                   ,RetakenPolicy VARCHAR(50)
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpEquivalentCoursesTakenByStudent'')
                      )
                BEGIN
                    DROP TABLE #tmpEquivalentCoursesTakenByStudent;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpStudentsWhoHasGradedEquivalentCourse'')
                      )
                BEGIN
                    DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourse;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpEquivalentCoursesTakenByStudentInTerm'')
                      )
                BEGIN
                    DROP TABLE #tmpEquivalentCoursesTakenByStudentInTerm;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpStudentsWhoHasGradedEquivalentCourseInTerm'')
                      )
                BEGIN
                    DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourseInTerm;
                END;

            SET @MinimunDate = ''1900-01-01'';
            SET @MaximunDate = ''2199-01-01'';
            SET @NewID = REPLACE(CONVERT(NVARCHAR(50), NEWID()), ''-'', ''_'');
            SET @CoursesTakenTableName = ''CoursesTaked_'' + @NewID;
            SET @GPASummaryTableName = ''GPASummary_'' + @NewID;

            SET @StuEnrollCampusId = COALESCE(
                                     (
                                     SELECT     TOP 1 ASE.CampusId
                                     FROM       dbo.arStuEnrollments AS ASE
                                     INNER JOIN (
                                                SELECT Val
                                                FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                ) AS LIST ON LIST.Val = ASE.EnrollmentId
                                     )
                                    ,NULL
                                             );

            SET @GradesFormat = dbo.GetAppSettingValueByKeyName(''GradesFormat'', @StuEnrollCampusId);
            IF ( @GradesFormat IS NOT NULL )
                BEGIN
                    SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
                END;
            SET @GPAMethod = dbo.GetAppSettingValueByKeyName(''GPAMethod'', @StuEnrollCampusId);
            IF ( @GPAMethod IS NOT NULL )
                BEGIN
                    SET @GPAMethod = LOWER(LTRIM(RTRIM(@GPAMethod)));
                END;
            SET @GradeBookAt = dbo.GetAppSettingValueByKeyName(''GradeBookWeightingLevel'', @StuEnrollCampusId);
            IF ( @GradeBookAt IS NOT NULL )
                BEGIN
                    SET @GradeBookAt = LOWER(LTRIM(RTRIM(@GradeBookAt)));
                END;
            SET @RetakenPolicy = (
                                 SELECT     TOP 1 SCASV.Value
                                 FROM       dbo.syConfigAppSetValues AS SCASV
                                 INNER JOIN dbo.syConfigAppSettings AS SCAS ON SCAS.SettingId = SCASV.SettingId
                                                                               AND SCAS.KeyName = ''GradeCourseRepetitionsMethod''
                                 );
        END;
        -- END  --  00) Variable Definition and Set Initial Values

        -- 01) Create Temp Table 
        BEGIN
            IF NOT EXISTS (
                          SELECT 1
                          FROM   tempdb.sys.objects AS O
                          WHERE  O.type IN ( ''U'' )
                                 AND O.name = @CoursesTakenTableName
                          )
                BEGIN
                    SET @SQL01 = '''';
                    SET @SQL01 = @SQL01 + ''CREATE TABLE tempdb.dbo.'' + @CoursesTakenTableName + '' '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''    ( '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''        CoursesTakenId INTEGER NOT NULL PRIMARY KEY '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,StudentId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,StuEnrollId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,TermId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,TermDescrip VARCHAR(100) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,TermStartDate DATETIME '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ReqId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ReqCode VARCHAR(100) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ReqDescription VARCHAR(100) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ReqCodeDescrip VARCHAR(100) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ReqCredits DECIMAL(18,2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,MinVal DECIMAL(18,2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ClsSectionId VARCHAR(50) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,CourseStarDate DATETIME '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,CourseEndDate DATETIME '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CreditsAttempted DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CreditsEarned DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CurrentScore DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CurrentGrade VARCHAR(10) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_FinalScore DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_FinalGrade VARCHAR(10) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_Completed BIT '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_FinalGPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_Product_WeightedAverage_Credits_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_Count_WeightedAverage_Credits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_Product_SimpleAverage_Credits_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_Count_SimpleAverage_Credits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_ModUser VARCHAR(50) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_ModDate DATETIME '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_TermGPA_Simple DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_TermGPA_Weighted DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_coursecredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CumulativeGPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CumulativeGPA_Simple DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_FACreditsEarned DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_Average DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CumAverage DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ScheduleDays DECIMAL(18,2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ActualDay DECIMAL(18,2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,FinalGPA_Calculations DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,FinalGPA_TermCalculations DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,CreditsEarned_Calculations DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ActualDay_Calculations DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,GrdBkWgtDetailsCount INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,CountMultipleEnrollment INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,RowNumberMultipleEnrollment INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,CountRepeated INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,RowNumberRetaked INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SameTermCountRetaken INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,RowNumberSameTermRetaked INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,RowNumberMultEnrollNoRetaken INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,RowNumberOverAllByClassSection INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,IsCreditsEarned BIT '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''    ) '' + CHAR(10);
                    --PRINT @SQL01;
                    EXECUTE ( @SQL01 );
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   tempdb.sys.objects AS O
                          WHERE  O.type IN ( ''U'' )
                                 AND O.name = @GPASummaryTableName
                          )
                BEGIN
                    SET @SQL02 = '''';
                    SET @SQL02 = @SQL02 + ''CREATE TABLE tempdb.dbo.'' + @GPASummaryTableName + '' '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''    ( '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''         GPASummaryId INTEGER NOT NULL PRIMARY KEY '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudentId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,LastName VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,FirstName VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,MiddleName VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,SSN VARCHAR(11) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudentNumber VARCHAR(50)'' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StuEnrollId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnrollmentID VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,PrgVerId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,PrgVerDescrip VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,PrgVersionTrackCredits BIT '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,GrdSystemId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,AcademicType NVARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,ClockHourProgram VARCHAR(10) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,IsMakingSAP BIT '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,TermId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,TermDescrip VARCHAR(100) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,TermStartDate DATETIME '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,TermEndDate DATETIME '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,DescripXTranscript VARCHAR(250) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,TermAverage DECIMAL(18,2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroTermSimple_CourseCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroTermSimple_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroTermSimple_GPA DECIMAL(18, 2)				    '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroTermWeight_CoursesCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroTermWeight_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroTermWeight_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudTermSimple_CourseCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudTermSimple_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudTermSimple_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudTermWeight_CoursesCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudTermWeight_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudTermWeight_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroSimple_CourseCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroSimple_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroSimple_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroWeight_CoursesCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroWeight_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''  	   ,EnroWeight_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudSimple_CourseCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudSimple_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudSimple_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudWeight_CoursesCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudWeight_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudWeight_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,GradesFormat VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,GPAMethod VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,GradeBookAt VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,RetakenPolicy VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''    ) '' + CHAR(10);
                    --PRINT @SQL02;
                    EXECUTE ( @SQL02 );
                END;
        END;
        -- END --  01) Create Temp Table

        -- 02) -- Refresh the Credit Summary Table Just in case any modifications were made
        BEGIN
            EXECUTE dbo.Usp_Update_SyCreditsSummary @StuEnrollIdList = @StuEnrollIdList;
            EXECUTE dbo.Usp_UpdateTransferCredits_SyCreditsSummary @StuEnrollIdList = @StuEnrollIdList;
        END;
        --  END  --  02) -- Refresh the Credit Summary Table Just in case any modifications were made

        -- 03) Get courses taked to informe and courses used for GPA Calculations (not multiple enrollment -same term-  and not retaked)	
        BEGIN
            INSERT INTO #getStudentGPAbyTerms
                        SELECT    DISTINCT T1.StuEnrollId
                                 ,T1.TermId
                                 ,T1.TermDescrip
                                 ,T1.StartDate
                                 ,T1.EndDate
                                 ,ISNULL(ATES.DescripXTranscript, '''') AS DescripXTranscript
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                        FROM      (
                                  SELECT     DISTINCT AR.StuEnrollId AS StuEnrollId
                                            ,T.TermId AS TermId
                                            ,T.TermDescrip AS TermDescrip
                                            ,T.StartDate AS StartDate
                                            ,T.EndDate AS EndDate
                                  FROM       dbo.arClassSections AS ACS
                                  INNER JOIN dbo.arResults AS AR ON AR.TestId = ACS.ClsSectionId
                                  INNER JOIN dbo.arTerm AS T ON T.TermId = ACS.TermId
                                  INNER JOIN (
                                             SELECT Val
                                             FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                             ) AS List ON List.Val = AR.StuEnrollId
                                  WHERE      AR.IsCourseCompleted = 1
                                  UNION
                                  SELECT     DISTINCT ATG.StuEnrollId
                                            ,T.TermId
                                            ,T.TermDescrip
                                            ,T.StartDate
                                            ,T.EndDate
                                  FROM       dbo.arTransferGrades AS ATG
                                  INNER JOIN dbo.arTerm AS T ON T.TermId = ATG.TermId
                                  INNER JOIN (
                                             SELECT Val
                                             FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                             ) AS List ON List.Val = ATG.StuEnrollId
                                  ) AS T1
                        LEFT JOIN dbo.arTermEnrollSummary AS ATES ON ATES.TermId = T1.TermId
                                                                     AND ATES.StuEnrollId = T1.StuEnrollId
                        ORDER BY  T1.StartDate
                                 ,T1.TermDescrip;

            INSERT INTO #getStudentGPAByEnrollment
                        SELECT     DISTINCT AL.StudentId
                                  ,AL.LastName
                                  ,AL.FirstName
                                  ,AL.MiddleName
                                  ,AL.SSN
                                  ,AL.StudentNumber
                                  ,ASE.StuEnrollId
                                  ,ASE.EnrollmentId
                                  ,APV.PrgVerId
                                  ,APV.PrgVerDescrip
                                  ,CASE WHEN ( APV.Credits > 0.0 ) THEN 1
                                        ELSE 0
                                   END AS PrgVersionTrackCredits
                                  ,APV.GrdSystemId
                                  ,CASE WHEN (
                                             APV.Credits > 0.0
                                             AND APV.Hours > 0
                                             )
                                             OR ( APV.IsContinuingEd = 1 ) THEN ''Credits-ClockHours''
                                        WHEN APV.Credits > 0.0 THEN ''Credits''
                                        WHEN APV.Hours > 0.0 THEN ''ClockHours''
                                        ELSE ''''
                                   END AcademicType
                                  ,CASE WHEN AP.ACId = 5 THEN ''True''
                                        ELSE ''False''
                                   END AS ClockHourProgram
                                  ,0 AS IsMakingSAP
                                  ,GSGAT.TermId
                                  --,NULL  --,GSGAT.TermDescrip
                                  ,0.0 AS TermAverage
                                  ,NULL AS EnroSimple_CourseCredits
                                  ,NULL AS EnroSimple_Credits
                                  ,NULL AS EnroSimple_GPA
                                  ,NULL AS EnroWeight_CoursesCredits
                                  ,NULL AS EnroWeight_GPA_Credits
                                  ,NULL AS EnroWeight_GPA
                                  ,NULL AS StudSimple_CourseCredits
                                  ,NULL AS StudSimple_GPA_Credits
                                  ,NULL AS StudSimple_GPA
                                  ,NULL AS StudWeight_CoursesCredits
                                  ,NULL AS StudWeight_GPA_Credits
                                  ,NULL AS StudWeight_GPA
                        FROM       adLeads AS AL --arStudent 
                        INNER JOIN arStuEnrollments ASE ON ASE.StudentId = AL.StudentId
                        INNER JOIN arPrgVersions APV ON ASE.PrgVerId = APV.PrgVerId
                        INNER JOIN arPrograms AS AP ON AP.ProgId = APV.ProgId
                        INNER JOIN #getStudentGPAbyTerms AS GSGAT ON GSGAT.StuEnrollId = ASE.StuEnrollId
                        INNER JOIN (
                                   SELECT DISTINCT CONVERT(UNIQUEIDENTIFIER, Val) AS StuEnrollId
                                   FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                   ) AS List ON List.StuEnrollId = ASE.StuEnrollId;

            INSERT INTO #CoursesTaken
                        SELECT     ASE.StudentId
                                  ,SCS.StuEnrollId AS StuEnrollId
                                  ,SCS.TermId AS TermId
                                  ,SCS.TermDescrip AS TermDescrip
                                  ,SCS.TermStartDate AS TermStartDate
                                  ,SCS.ReqId AS ReqId
                                  ,NULL AS ReqCode
                                  ,NULL AS ReqDescription
                                  ,NULL AS ReqCodeDescrip
                                  ,NULL AS ReqCredits
                                  ,NULL AS MinVal
                                  ,SCS.ClsSectionId AS ClsSectionId
                                  ,ISNULL(ACS.StartDate, @MaximunDate) AS CourseStarDate
                                  ,ISNULL(ACS.EndDate, @MaximunDate) AS CourseEndDate
                                  ,ISNULL(SCS.CreditsAttempted, 0) AS SCS_CreditsAttempted
                                  ,ISNULL(SCS.CreditsEarned, 0) AS SCS_CreditsEarned
                                  ,SCS.CurrentScore AS SCS_CurrentScore
                                  ,SCS.CurrentGrade AS SCS_CurrentGrade
                                  ,SCS.FinalScore AS SCS_FinalScore
                                  ,SCS.FinalGrade AS SCS_FinalGrade
                                  ,SCS.Completed AS SCS_Completed
                                  ,SCS.FinalGPA AS SCS_FinalGPA
                                  ,SCS.Product_WeightedAverage_Credits_GPA
                                  ,SCS.Count_WeightedAverage_Credits
                                  ,SCS.Product_SimpleAverage_Credits_GPA
                                  ,SCS.Count_SimpleAverage_Credits
                                  ,SCS.ModUser
                                  ,SCS.ModDate
                                  ,SCS.TermGPA_Simple
                                  ,SCS.TermGPA_Weighted
                                  ,SCS.coursecredits
                                  ,SCS.CumulativeGPA
                                  ,SCS.CumulativeGPA_Simple
                                  ,SCS.FACreditsEarned
                                  ,SCS.Average
                                  ,SCS.CumAverage
                                  ,0.0 AS ScheduleDays
                                  ,0.0 AS ActualDay
                                                                                      -- For Transfer Courses  (TR) SCS.FinalGPA should be Null so it will filter for GPA Calculations
                                  ,CASE WHEN ( LOWER(@RetakenPolicy) = ''average'' ) THEN AVG(SCS.FinalGPA) OVER ( PARTITION BY SCS.StuEnrollId
                                                                                                                             ,SCS.ReqId
                                                                                                               )
                                        ELSE SCS.FinalGPA
                                   END AS FinalGPA_Calculations
                                  ,CASE WHEN ( LOWER(@RetakenPolicy) = ''average'' ) THEN AVG(SCS.FinalGPA) OVER ( PARTITION BY SCS.StuEnrollId
                                                                                                                             ,SCS.TermId
                                                                                                                             ,SCS.ReqId
                                                                                                               )
                                        ELSE SCS.FinalGPA
                                   END AS FinalGPA_TermCalculations
                                  ,0.0 AS CreditsEarned_Calculations
                                  ,0.0 AS ActualDay_Calculations
                                  ,0 AS GrdBkWgtDetailsCount
                                  ,COUNT(*) OVER ( PARTITION BY ASE.StudentId --DISTINCT SCS.StuEnrollId
                                                               ,SCS.TermId
                                                               ,SCS.ReqId
                                                 ) AS CountMultipleEnrollment
                                  ,ROW_NUMBER() OVER ( PARTITION BY ASE.StudentId
                                                                   ,SCS.TermId
                                                                   ,SCS.ReqId
                                                       ORDER BY ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                               ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                               ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                               ,SCS.StuEnrollId
                                                     ) AS RowNumberMultipleEnrollment --  same course -same termRowNumberMultipleEnrollment INTEGER
                                  ,COUNT(*) OVER ( PARTITION BY SCS.StuEnrollId
                                                               ,SCS.ReqId
                                                 ) AS CountRepeated
                                  ,CASE WHEN ( LOWER(@RetakenPolicy) = ''latest'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.TermStartDate DESC
                                                                        ,ISNULL(ACS.EndDate, @MinimunDate) DESC   -- DESC Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MinimunDate) DESC -- DESC Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC                    -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                        WHEN ( LOWER(@RetakenPolicy) = ''average'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.TermStartDate
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                        WHEN ( LOWER(@RetakenPolicy) = ''best'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.FinalGPA DESC
																		,GSD.IsPass DESC
                                                                        ,SCS.TermStartDate DESC
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                        ELSE
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId -- ''best''
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.FinalGPA DESC
                                                                        ,SCS.TermStartDate
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                   END AS RowNumberRetaked
                                  ,COUNT(*) OVER ( PARTITION BY SCS.StuEnrollId
                                                               ,SCS.TermId
                                                               ,SCS.ReqId
                                                 ) AS SameTermCountRetaken
                                  ,CASE WHEN ( LOWER(@RetakenPolicy) = ''latest'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                            ,SCS.TermId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.TermStartDate DESC
                                                                        ,ISNULL(ACS.EndDate, @MinimunDate) DESC   -- DESC Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MinimunDate) DESC -- DESC Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC                    -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                        WHEN ( LOWER(@RetakenPolicy) = ''average'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                            ,SCS.TermId
                                                                            ,SCS.ReqId
                                                                ORDER BY ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                        WHEN ( LOWER(@RetakenPolicy) = ''best'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                            ,SCS.TermId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.FinalGPA DESC
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                        ELSE
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId -- ''best''
                                                                            ,SCS.TermId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.FinalGPA DESC
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate)
                                                                        ,SCS.ClsSectionId DESC -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                   END AS RowNumberSameTermRetaked
                                  ,ROW_NUMBER() OVER ( PARTITION BY ASE.StudentId
                                                                   ,SCS.TermId
                                                                   ,SCS.ReqId
                                                                   ,SCS.ClsSectionId
                                                       ORDER BY ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                               ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                               ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                               ,SCS.StuEnrollId
                                                     ) AS RowNumberMultEnrollNoRetaken
                                  ,CASE WHEN ( LOWER(@RetakenPolicy) = ''latest'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY ASE.StudentId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.TermStartDate DESC
                                                                        ,ISNULL(ACS.EndDate, @MinimunDate) DESC   -- DESC Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MinimunDate) DESC -- DESC Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC                    -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                                        ,SCS.StuEnrollId
                                                              )
                                        WHEN ( LOWER(@RetakenPolicy) = ''average'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY ASE.StudentId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.TermStartDate
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                                        ,SCS.StuEnrollId
                                                              )
                                        WHEN ( LOWER(@RetakenPolicy) = ''best'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY ASE.StudentId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.FinalGPA DESC
                                                                        ,SCS.TermStartDate
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                                        ,SCS.StuEnrollId
                                                              )
                                        ELSE
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId -- ''best''
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.FinalGPA DESC
                                                                        ,SCS.TermStartDate
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                                        ,SCS.StuEnrollId
                                                              )
                                   END AS RowNumberOverAllByClassSection
                                  ,GSD.IsCreditsEarned
                        FROM       dbo.syCreditSummary AS SCS
                        INNER JOIN dbo.arStuEnrollments AS ASE ON ASE.StuEnrollId = SCS.StuEnrollId
                        INNER JOIN (
                                   SELECT DISTINCT GSGBE.StuEnrollId AS StuEnrollId
                                   FROM   #getStudentGPAByEnrollment AS GSGBE
                                   ) AS List ON List.StuEnrollId = SCS.StuEnrollId
                        LEFT JOIN  dbo.arClassSections AS ACS ON ACS.ClsSectionId = SCS.ClsSectionId
                        INNER JOIN dbo.arPrgVersions AS PV ON PV.PrgVerId = ASE.PrgVerId
                        INNER JOIN dbo.arGradeSystems AS GS ON GS.GrdSystemId = PV.GrdSystemId
                        INNER JOIN dbo.arGradeSystemDetails AS GSD ON GSD.GrdSystemId = GS.GrdSystemId
                        WHERE      (
                                   SCS.FinalScore IS NOT NULL
                                   OR SCS.FinalGrade IS NOT NULL
                                   )
                                   AND SCS.FinalGrade = GSD.Grade
                        ORDER BY   SCS.TermStartDate
                                  ,SCS.TermDescrip
                                  ,SCS.ReqDescrip
                                  ,SCS.StuEnrollId;

        -- Update  RowNumberMultipleEnrollment for those that are retaken and multiple enrollment
        --UPDATE     CT3
        --SET        CT3.RowNumberMultEnrollNoRetaken = CT2.RowNumberMultEnrollNoRetaken
        --FROM       #CoursesTaken AS CT3
        --INNER JOIN (
        --           SELECT CT1.CoursesTakenId
        --                 ,ROW_NUMBER () OVER (PARTITION BY CT1.StudentId
        --                                                  ,CT1.TermId
        --                                                  ,CT1.ReqId
        --                                                  ,CT1.ClsSectionId
        --                                      ORDER BY CT1.StuEnrollId
        --                                              ,CT1.TermStartDate
        --                                              ,CT1.CourseEndDate
        --                                              ,CT1.CourseStarDate
        --                                     ) AS RowNumberMultEnrollNoRetaken
        --           FROM   #CoursesTaken AS CT1
        --           ) AS CT2 ON CT2.CoursesTakenId = CT3.CoursesTakenId;
        -- Update RowNumberOverAllByClassSection  TO 0 for those that are ClsSectionId IS NULL
        --UPDATE CT
        --SET    CT.RowNumberOverAllByClassSection = 0
        --FROM   #CoursesTaken AS CT
        --WHERE  CT.ClsSectionId IS NULL;
        END;
        -- END  --  03) Get courses taked to informe and courses used for GPA Calculations (not multiple enrollment -same term-  and not retaked)

        -- 04) Get GPA for StuEnrollId, TermId
        BEGIN
            DECLARE getNodes_Cursor CURSOR FAST_FORWARD FORWARD_ONLY FOR
                SELECT DISTINCT GSGBE.StudentId
                      ,GSGBE.StuEnrollId
                      ,GSGBE.TermId
                FROM   #getStudentGPAByEnrollment AS GSGBE;
            OPEN getNodes_Cursor;
            FETCH NEXT FROM getNodes_Cursor
            INTO @curStudentId
                ,@curStuEnrollId
                ,@curTermId;
            WHILE @@FETCH_STATUS = 0
                BEGIN
                    SET @cumTermAverage = (
                                          SELECT TOP 1 CT.SCS_Average
                                          FROM   #CoursesTaken AS CT
                                          WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                 AND CT.TermId IN ( @curTermId )
                                                 AND (
                                                     CT.RowNumberMultEnrollNoRetaken = 1
                                                     OR CT.RowNumberSameTermRetaked = 1
                                                     )
                                          );
                    SET @cumEnroTermSimple_CourseCredits = (
                                                           SELECT COUNT(CT.SCS_coursecredits)
                                                           FROM   #CoursesTaken AS CT
                                                           WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                                  AND CT.TermId IN ( @curTermId )
                                                                  AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                                  AND ( CT.RowNumberSameTermRetaked = 1
                                                                      --OR CT.RowNumberMultipleEnrollment = 1
                                                                      )
                                                           );
                    SET @cumEnroTermSimple_GPACredits = (
                                                        SELECT SUM(CT.FinalGPA_TermCalculations)
                                                        FROM   #CoursesTaken AS CT
                                                        WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                               AND CT.TermId IN ( @curTermId )
                                                               AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                               AND ( CT.RowNumberSameTermRetaked = 1
                                                                   --OR CT.RowNumberMultipleEnrollment = 1
                                                                   )
                                                        );
                    SET @cumEnroTermWeight_CourseCredits = (
                                                           SELECT SUM(CT.SCS_coursecredits)
                                                           FROM   #CoursesTaken AS CT
                                                           WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                                  AND CT.TermId IN ( @curTermId )
                                                                  AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                                  AND ( CT.RowNumberSameTermRetaked = 1
                                                                      --OR CT.RowNumberMultipleEnrollment = 1
                                                                      )
                                                           );
                    SET @cumEnroTermWeight_GPACredits = (
                                                        SELECT SUM(CT.SCS_coursecredits * CT.FinalGPA_TermCalculations)
                                                        FROM   #CoursesTaken AS CT
                                                        WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                               AND CT.TermId IN ( @curTermId )
                                                               AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                               AND ( CT.RowNumberSameTermRetaked = 1
                                                                   --OR CT.RowNumberMultipleEnrollment = 1
                                                                   )
                                                        );
                    SET @cumStudTermSimple_CourseCredits = (
                                                           SELECT COUNT(CT.SCS_coursecredits)
                                                           FROM   #CoursesTaken AS CT
                                                           WHERE  CT.StudentId IN ( @curStudentId )
                                                                  AND CT.TermId IN ( @curTermId )
                                                                  AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                                  AND ( CT.RowNumberSameTermRetaked = 1
                                                                      --OR CT.RowNumberMultipleEnrollment = 1
                                                                      )
                                                           );
                    SET @cumStudTermSimple_GPACredits = (
                                                        SELECT SUM(CT.FinalGPA_TermCalculations)
                                                        FROM   #CoursesTaken AS CT
                                                        WHERE  CT.StudentId IN ( @curStudentId )
                                                               AND CT.TermId IN ( @curTermId )
                                                               AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                               AND ( CT.RowNumberSameTermRetaked = 1
                                                                   --OR CT.RowNumberMultipleEnrollment = 1
                                                                   )
                                                        );
                    SET @cumStudTermWeight_CourseCredits = (
                                                           SELECT SUM(CT.SCS_coursecredits)
                                                           FROM   #CoursesTaken AS CT
                                                           WHERE  CT.StudentId IN ( @curStudentId )
                                                                  AND CT.TermId IN ( @curTermId )
                                                                  AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                                  AND ( CT.RowNumberSameTermRetaked = 1
                                                                      --OR CT.RowNumberMultipleEnrollment = 1
                                                                      )
                                                           );
                    SET @cumStudTermWeight_GPACredits = (
                                                        SELECT SUM(CT.SCS_coursecredits * CT.FinalGPA_TermCalculations)
                                                        FROM   #CoursesTaken AS CT
                                                        WHERE  CT.StudentId IN ( @curStudentId )
                                                               AND CT.TermId IN ( @curTermId )
                                                               AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                               AND ( CT.RowNumberSameTermRetaked = 1
                                                                   --OR CT.RowNumberMultipleEnrollment = 1
                                                                   )
                                                        );

                    IF @cumEnroTermSimple_CourseCredits > 0
                        BEGIN
                            SET @cumEnroTermSimple_GPA = @cumEnroTermSimple_GPACredits / @cumEnroTermSimple_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumEnroTermSimple_GPA = 0.0;
                        END;
                    IF @cumEnroTermWeight_CourseCredits > 0.0
                        BEGIN
                            SET @cumEnroTermWeight_GPA = @cumEnroTermWeight_GPACredits / @cumEnroTermWeight_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumEnroTermWeight_GPA = 0.0;
                        END;
                    IF @cumStudTermSimple_CourseCredits > 0.0
                        BEGIN
                            SET @cumStudTermSimple_GPA = @cumStudTermSimple_GPACredits / @cumStudTermSimple_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumStudTermSimple_GPA = 0.0;
                        END;
                    IF @cumStudTermWeight_CourseCredits > 0.0
                        BEGIN
                            SET @cumStudTermWeight_GPA = @cumStudTermWeight_GPACredits / @cumStudTermWeight_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumStudTermWeight_GPA = 0.0;
                        END;

                    UPDATE GSGAT
                    SET    GSGAT.TermAverage = @cumTermAverage
                          ,GSGAT.EnroTermSimple_CourseCredits = @cumEnroTermSimple_CourseCredits
                          ,GSGAT.EnroTermSimple_GPACredits = @cumEnroTermSimple_GPACredits
                          ,GSGAT.EnroTermSimple_GPA = @cumEnroTermSimple_GPA
                          ,GSGAT.EnroTermWeight_CoursesCredits = @cumEnroTermWeight_CourseCredits
                          ,GSGAT.EnroTermWeight_GPACredits = @cumEnroTermWeight_GPACredits
                          ,GSGAT.EnroTermWeight_GPA = @cumEnroTermWeight_GPA
                          ,GSGAT.StudTermSimple_CourseCredits = @cumStudTermSimple_CourseCredits
                          ,GSGAT.StudTermSimple_GPACredits = @cumStudTermSimple_GPACredits
                          ,GSGAT.StudTermSimple_GPA = @cumStudTermSimple_GPA
                          ,GSGAT.StudTermWeight_CoursesCredits = @cumStudTermWeight_CourseCredits
                          ,GSGAT.StudTermWeight_GPACredits = @cumStudTermWeight_GPACredits
                          ,GSGAT.StudTermWeight_GPA = @cumStudTermWeight_GPA
                    FROM   #getStudentGPAbyTerms AS GSGAT
                    WHERE  GSGAT.StuEnrollId = @curStuEnrollId
                           AND GSGAT.TermId = @curTermId;

                    FETCH NEXT FROM getNodes_Cursor
                    INTO @curStudentId
                        ,@curStuEnrollId
                        ,@curTermId;
                END;
            CLOSE getNodes_Cursor;
            DEALLOCATE getNodes_Cursor;
        END;
        -- END  --  04)Get GPA for StuEnrollId, TermId

        -- 05) Get GPA for StudentId and StuEnrollId
        BEGIN
            DECLARE getNodes_Cursor CURSOR FAST_FORWARD FORWARD_ONLY FOR
                SELECT DISTINCT GSGBE.StudentId
                      ,GSGBE.StuEnrollId
                FROM   #getStudentGPAByEnrollment AS GSGBE;
            OPEN getNodes_Cursor;
            FETCH NEXT FROM getNodes_Cursor
            INTO @curStudentId
                ,@curStuEnrollId;
            WHILE @@FETCH_STATUS = 0
                BEGIN

                    SET @cumEnroSimple_CourseCredits = (
                                                       SELECT COUNT(CT.SCS_coursecredits)
                                                       FROM   #CoursesTaken AS CT
                                                       WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                              AND CT.FinalGPA_Calculations IS NOT NULL
                                                              AND CT.RowNumberRetaked = 1
                                                       );
                    SET @cumEnroSimple_GPACredits = (
                                                    SELECT SUM(CT.FinalGPA_Calculations)
                                                    FROM   #CoursesTaken AS CT
                                                    WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                           AND CT.FinalGPA_Calculations IS NOT NULL
                                                           AND CT.RowNumberRetaked = 1
                                                    );
                    SET @cumEnroWeight_CourseCredits = (
                                                       SELECT SUM(CT.SCS_coursecredits)
                                                       FROM   #CoursesTaken AS CT
                                                       WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                              AND CT.FinalGPA_Calculations IS NOT NULL
                                                              AND CT.RowNumberRetaked = 1
                                                       );
                    SET @cumEnroWeight_GPACredits = (
                                                    SELECT SUM(CT.SCS_coursecredits * CT.FinalGPA_Calculations)
                                                    FROM   #CoursesTaken AS CT
                                                    WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                           AND CT.FinalGPA_Calculations IS NOT NULL
                                                           AND CT.RowNumberRetaked = 1
                                                    );
                    SET @cumStudSimple_CourseCredits = (
                                                       SELECT COUNT(CT.SCS_coursecredits)
                                                       FROM   #CoursesTaken AS CT
                                                       WHERE  CT.StudentId IN ( @curStudentId )
                                                              AND CT.FinalGPA_Calculations IS NOT NULL
                                                              AND CT.RowNumberRetaked = 1
                                                              AND RowNumberOverAllByClassSection = 1
                                                       );
                    SET @cumStudSimple_GPACredits = (
                                                    SELECT SUM(CT.FinalGPA_Calculations)
                                                    FROM   #CoursesTaken AS CT
                                                    WHERE  CT.StudentId IN ( @curStudentId )
                                                           AND CT.FinalGPA_Calculations IS NOT NULL
                                                           AND CT.RowNumberRetaked = 1
                                                           AND RowNumberOverAllByClassSection = 1
                                                    );
                    SET @cumStudWeight_CourseCredits = (
                                                       SELECT SUM(CT.SCS_coursecredits)
                                                       FROM   #CoursesTaken AS CT
                                                       WHERE  CT.StudentId IN ( @curStudentId )
                                                              AND CT.FinalGPA_Calculations IS NOT NULL
                                                              AND CT.RowNumberRetaked = 1
                                                              AND RowNumberOverAllByClassSection = 1
                                                       );
                    SET @cumStudWeight_GPACredits = (
                                                    SELECT SUM(CT.SCS_coursecredits * CT.FinalGPA_Calculations)
                                                    FROM   #CoursesTaken AS CT
                                                    WHERE  CT.StudentId IN ( @curStudentId )
                                                           AND CT.FinalGPA_Calculations IS NOT NULL
                                                           AND RowNumberOverAllByClassSection = 1
                                                           AND CT.RowNumberRetaked = 1
                                                    );

                    IF @cumEnroSimple_CourseCredits > 0.0
                        BEGIN
                            SET @cumEnroSimple_GPA = @cumEnroSimple_GPACredits / @cumEnroSimple_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumEnroSimple_GPA = 0.0;
                        END;
                    IF @cumStudSimple_CourseCredits > 0.0
                        BEGIN
                            SET @cumStudSimple_GPA = @cumStudSimple_GPACredits / @cumStudSimple_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumStudSimple_GPA = 0.0;
                        END;
                    IF @cumEnroWeight_CourseCredits > 0.0
                        BEGIN
                            SET @cumEnroWeight_GPA = @cumEnroWeight_GPACredits / @cumEnroWeight_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumEnroWeight_GPA = 0.0;
                        END;
                    IF @cumStudWeight_CourseCredits > 0.0
                        BEGIN
                            SET @cumStudWeight_GPA = @cumStudWeight_GPACredits / @cumStudWeight_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumStudWeight_CourseCredits = 0.0;
                        END;
                    --********************* Making SAP *******************************************************
                    SELECT @IsMakingSAP = ASCR1.IsMakingSAP
                    FROM   (
                           SELECT ROW_NUMBER() OVER ( PARTITION BY ASCR.StuEnrollId
                                                      ORDER BY ASCR.DatePerformed DESC
                                                    ) AS N
                                 ,ISNULL(ASCR.IsMakingSAP, 0) AS IsMakingSAP
                           FROM   arSAPChkResults AS ASCR
                           WHERE  ASCR.StuEnrollId = @curStuEnrollId
                           ) AS ASCR1
                    WHERE  ASCR1.N = 1;
                    --********************* Update #getStudentGPAByEnrollment *******************************************************
                    UPDATE GSGBE
                    SET    GSGBE.IsMakingSAP = ISNULL(@IsMakingSAP, 0)
                          ,GSGBE.EnroSimple_CourseCredits = @cumEnroSimple_CourseCredits
                          ,GSGBE.EnroSimple_GPACredits = @cumEnroSimple_GPACredits
                          ,GSGBE.EnroSimple_GPA = @cumEnroSimple_GPA
                          ,GSGBE.EnroWeight_CoursesCredits = @cumEnroWeight_CourseCredits
                          ,GSGBE.EnroWeight_GPACredits = @cumEnroWeight_GPACredits
                          ,GSGBE.EnroWeight_GPA = @cumEnroWeight_GPA
                          ,GSGBE.StudSimple_CourseCredits = @cumStudSimple_CourseCredits
                          ,GSGBE.StudSimple_GPACredits = @cumStudSimple_GPACredits
                          ,GSGBE.StudSimple_GPA = @cumStudSimple_GPA
                          ,GSGBE.StudWeight_CoursesCredits = @cumStudWeight_CourseCredits
                          ,GSGBE.StudWeight_GPA_Credits = @cumStudWeight_GPACredits
                          ,GSGBE.StudWeight_GPA = @cumStudWeight_GPA
                    FROM   #getStudentGPAByEnrollment AS GSGBE
                    WHERE  GSGBE.StuEnrollId = @curStuEnrollId;

                    FETCH NEXT FROM getNodes_Cursor
                    INTO @curStudentId
                        ,@curStuEnrollId;
                END;
            CLOSE getNodes_Cursor;
            DEALLOCATE getNodes_Cursor;
        END;
        -- END  -- 05) Get GPA for StudentId and StuEnrollId

        -- 06) Get ScheduleDays and ActualDay for each course taked (coursed and tranferred)
        BEGIN
            UPDATE     CT
            SET        CT.ReqCode = AR.Code
                      ,CT.ReqDescription = AR.Descrip
                      ,CT.ReqCodeDescrip = ''('' + AR.Code + '' ) '' + AR.Descrip
                      ,CT.ReqCredits = AR.Credits
                      ,CT.CreditsEarned_Calculations = ( CASE WHEN (
                                                                   CT.SCS_CreditsEarned > 0
                                                                   AND CT.RowNumberRetaked <> 1
                                                                   ) THEN 0.0
                                                              ELSE CT.SCS_CreditsEarned
                                                         END
                                                       )
                      ,CT.ScheduleDays = AR.Hours
                      ,CT.ActualDay = ( CASE WHEN ( CT.IsCreditsEarned = 0 ) THEN 0.0
                                             ELSE AR.Hours
                                        END
                                      )
                      ,CT.ActualDay_Calculations = ( CASE WHEN (
                                                               CT.IsCreditsEarned = 1
                                                               AND CT.RowNumberRetaked <> 1
                                                               ) THEN 0.0
                                                          WHEN ( CT.IsCreditsEarned = 0 ) THEN 0.0
                                                          ELSE AR.Hours
                                                     END
                                                   )
                      ,CT.MinVal = (
                                   SELECT     MIN(GCD.MinVal)
                                   FROM       dbo.arGradeScaleDetails GCD
                                   INNER JOIN dbo.arGradeSystemDetails GSD ON GSD.GrdSysDetailId = GCD.GrdSysDetailId
                                   WHERE      GSD.IsPass = 1
                                              AND GCD.GrdScaleId = ACS.GrdScaleId
                                   )
                      ,CT.GrdBkWgtDetailsCount = (
                                                 SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                                 FROM   dbo.arGrdBkResults AS AGBR
                                                 WHERE  AGBR.StuEnrollId = CT.StuEnrollId
                                                        AND AGBR.ClsSectionId = CT.ClsSectionId
                                                 )
            FROM       #CoursesTaken AS CT
            INNER JOIN #getStudentGPAByEnrollment GSGBE ON GSGBE.StuEnrollId = CT.StuEnrollId
                                                           AND GSGBE.TermId = CT.TermId
            INNER JOIN dbo.arClassSections AS ACS ON ACS.ClsSectionId = CT.ClsSectionId
                                                     AND ACS.ReqId = CT.ReqId
            INNER JOIN dbo.arReqs AS AR ON AR.ReqId = CT.ReqId;

            UPDATE     CT
            SET        CT.ReqCode = AR.Code
                      ,CT.ReqDescription = AR.Descrip
                      ,CT.ReqCodeDescrip = ''('' + AR.Code + '' ) '' + AR.Descrip
                      ,CT.ReqCredits = AR.Credits
                      ,CT.CreditsEarned_Calculations = ( CASE WHEN (
                                                                   CT.SCS_CreditsEarned > 0
                                                                   AND CT.RowNumberRetaked <> 1
                                                                   ) THEN 0.0
                                                              ELSE CT.SCS_CreditsEarned
                                                         END
                                                       )
                      ,CT.ScheduleDays = AR.Hours
                      ,CT.ActualDay = ( CASE WHEN ( CT.IsCreditsEarned = 0 ) THEN 0.0
                                             ELSE AR.Hours
                                        END
                                      )
                      ,CT.ActualDay_Calculations = ( CASE WHEN (
                                                               CT.IsCreditsEarned = 1
                                                               AND CT.RowNumberRetaked <> 1
                                                               ) THEN 0.0
                                                          WHEN ( CT.IsCreditsEarned = 0 ) THEN 0.0
                                                          ELSE AR.Hours
                                                     END
                                                   )
                      ,CT.MinVal = (
                                   SELECT     MIN(GCD.MinVal)
                                   FROM       dbo.arGradeScaleDetails GCD
                                   INNER JOIN dbo.arGradeSystemDetails GSD ON GSD.GrdSysDetailId = GCD.GrdSysDetailId
                                   WHERE      GSD.IsPass = 1
                                   )
                      ,CT.GrdBkWgtDetailsCount = (
                                                 SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                                 FROM   dbo.arGrdBkResults AS AGBR
                                                 WHERE  AGBR.StuEnrollId = CT.StuEnrollId
                                                        AND AGBR.ClsSectionId = CT.ClsSectionId
                                                 )
            FROM       #CoursesTaken AS CT
            INNER JOIN #getStudentGPAByEnrollment GSGBE ON GSGBE.StuEnrollId = CT.StuEnrollId
                                                           AND GSGBE.TermId = CT.TermId
            INNER JOIN dbo.arTransferGrades AS ATG ON ATG.StuEnrollId = CT.StuEnrollId
                                                      AND ATG.TermId = CT.TermId
                                                      AND ATG.ReqId = CT.ReqId
            INNER JOIN dbo.arReqs AS AR ON AR.ReqId = CT.ReqId;

        END;
        -- END  --  06) Get ScheduleDays and ActualDay for each course taked (coursed and tranferred)

        -- 07) Summary update
        BEGIN
            INSERT INTO #GPASummary
                        SELECT     GSGBE.StudentId
                                  ,GSGBE.LastName
                                  ,GSGBE.FirstName
                                  ,GSGBE.MiddleName
                                  ,GSGBE.SSN
                                  ,GSGBE.StudentNumber
                                  ,GSGBE.StuEnrollId
                                  ,GSGBE.EnrollmentID
                                  ,GSGBE.PrgVerId
                                  ,GSGBE.PrgVerDescrip
                                  ,GSGBE.PrgVersionTrackCredits
                                  ,GSGBE.GrdSystemId
                                  ,GSGBE.AcademicType AS AcademicType
                                  ,GSGBE.ClockHourProgram AS ClockHourProgram
                                  ,GSGBE.IsMakingSAP AS IsMakingSAP
                                  ,GSGAT.TermId
                                  ,GSGAT.TermDescrip
                                  ,GSGAT.TermStartDate
                                  ,GSGAT.TermEndDate
                                  ,GSGAT.DescripXTranscript
                                  ,GSGAT.TermAverage
                                  ,GSGAT.EnroTermSimple_CourseCredits
                                  ,GSGAT.EnroTermSimple_GPACredits
                                  ,GSGAT.EnroTermSimple_GPA
                                  ,GSGAT.EnroTermWeight_CoursesCredits
                                  ,GSGAT.EnroTermWeight_GPACredits
                                  ,GSGAT.EnroTermWeight_GPA
                                  ,GSGAT.StudTermSimple_CourseCredits
                                  ,GSGAT.StudTermSimple_GPACredits
                                  ,GSGAT.StudTermSimple_GPA
                                  ,GSGAT.StudTermWeight_CoursesCredits
                                  ,GSGAT.StudTermWeight_GPACredits
                                  ,GSGAT.StudTermWeight_GPA
                                  ,GSGBE.EnroSimple_CourseCredits
                                  ,GSGBE.EnroSimple_GPACredits
                                  ,GSGBE.EnroSimple_GPA
                                  ,GSGBE.EnroWeight_CoursesCredits
                                  ,GSGBE.EnroWeight_GPACredits
                                  ,GSGBE.EnroWeight_GPA
                                  ,GSGBE.StudSimple_CourseCredits
                                  ,GSGBE.StudSimple_GPACredits
                                  ,GSGBE.StudSimple_GPA
                                  ,GSGBE.StudWeight_CoursesCredits
                                  ,GSGBE.StudWeight_GPA_Credits
                                  ,GSGBE.StudWeight_GPA
                                  ,@GradesFormat AS GradesFormat
                                  ,@GPAMethod AS GPAMethod
                                  ,@GradeBookAt AS GradeBookAt
                                  ,@RetakenPolicy AS RetakenPolicy
                        FROM       #getStudentGPAbyTerms AS GSGAT
                        INNER JOIN #getStudentGPAByEnrollment GSGBE ON GSGBE.StuEnrollId = GSGAT.StuEnrollId
                                                                       AND GSGAT.TermId = GSGBE.TermId
                        ORDER BY   GSGBE.StudentId
                                  ,GSGBE.StuEnrollId
                                  ,GSGAT.TermStartDate
                                  ,GSGAT.TermDescrip
                                  ,GSGAT.TermId;

        END;
        -- END  --  07) Summary update and 

        -- to Test
        --SELECT * FROM #getStudentGPAbyTerms AS GSGAT ORDER BY  GSGAT.StuEnrollId, GSGAT.TermId
        --SELECT * FROM #getStudentGPAByEnrollment AS GSGBE
        --SELECT * FROM #CoursesTaken AS CT ORDER BY CT.StudentId, CT.StuEnrollId, CT.TermId
        --SELECT * FROM #GPASummary AS GS ORDER BY GS.StudentId, GS.StuEnrollId, GS.TermId

        -- 08) Save saving prepared GPA Information
        BEGIN
            SET @SQL01 = '''';
            SET @SQL01 = @SQL01 + ''INSERT INTO tempdb.dbo.'' + @CoursesTakenTableName + '' '' + CHAR(10);
            SET @SQL01 = @SQL01 + ''SELECT CT.* '' + CHAR(10);
            SET @SQL01 = @SQL01 + ''FROM  #CoursesTaken AS CT '' + CHAR(10);
            --PRINT @SQL01;
            EXECUTE ( @SQL01 );

            SET @SQL02 = '''';
            SET @SQL02 = @SQL02 + ''INSERT INTO tempdb.dbo.'' + @GPASummaryTableName + '' '' + CHAR(10);
            SET @SQL02 = @SQL02 + ''       SELECT GS.*     '' + CHAR(10);
            SET @SQL02 = @SQL02 + ''       FROM  #GPASummary AS GS '' + CHAR(10);
            --PRINT @SQL02;
            EXECUTE ( @SQL02 );
        END;
        -- END  --08) Save saving prepared GPA Information

        --09) DropTempTables
        BEGIN
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#CoursesTaken'')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#getStudentGPAbyTerms'')
                      )
                BEGIN
                    DROP TABLE #getStudentGPAbyTerms;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#getStudentGPAByEnrollment'')
                      )
                BEGIN
                    DROP TABLE #getStudentGPAByEnrollment;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#GPASummary'')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpEquivalentCoursesTakenByStudent'')
                      )
                BEGIN
                    DROP TABLE #tmpEquivalentCoursesTakenByStudent;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpStudentsWhoHasGradedEquivalentCourse'')
                      )
                BEGIN
                    DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourse;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpEquivalentCoursesTakenByStudentInTerm'')
                      )
                BEGIN
                    DROP TABLE #tmpEquivalentCoursesTakenByStudentInTerm;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpStudentsWhoHasGradedEquivalentCourseInTerm'')
                      )
                BEGIN
                    DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourseInTerm;
                END;
        END;
    -- END  --  09) DropTempTables

    -- 10) show tables names
    --BEGIN
    --    SELECT @CoursesTakenTableName AS CoursesTakenTableName
    --          ,@GPASummaryTableName AS GPASummaryTableName;
    --END;
    -- END  --  10) show tables names
    END;
-- =========================================================================================================
-- END  --  USP_TR_Sub03_PrepareGPA
-- =========================================================================================================
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_TR_Sub03_StudentInfo]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub03_StudentInfo]', 'P') IS NULL
EXEC sp_executesql N'
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- =========================================================================================================
-- USP_TR_Sub03_StudentInfo 
-- =========================================================================================================
CREATE PROCEDURE [dbo].[USP_TR_Sub03_StudentInfo]
    @StuEnrollIdList NVARCHAR(MAX) = NULL
   ,@ShowMultipleEnrollments BIT = 0
   ,@StatusList NVARCHAR(MAX) = NULL
   ,@OrderBy NVARCHAR(1000) = NULL
AS
    BEGIN
        DECLARE @CoursesTakenTableName NVARCHAR(200);
        DECLARE @GPASummaryTableName NVARCHAR(200);
        -- StuEnrollIdList plus double Enrollments
        DECLARE @StuEnrollIdListWithDoubleEnrollments NVARCHAR(MAX);

        CREATE TABLE #Temp1
            (
                StudentId NVARCHAR(50)
               ,EnrollAcademicType NVARCHAR(50)
               ,StuEnrollIdList NVARCHAR(MAX)
            );
        CREATE TABLE #Temp2
            (
                StudentId UNIQUEIDENTIFIER
               ,SSN NVARCHAR(50)
               ,StuFirstName NVARCHAR(50)
               ,StuLastName NVARCHAR(50)
               ,StuMiddleName NVARCHAR(50)
               ,StuDOB DATETIME
               ,StudentNumber NVARCHAR(50)
               ,StuEmail NVARCHAR(50)
               ,StuAddress1 NVARCHAR(250)
               ,StuAddress2 NVARCHAR(250)
               ,StuCity NVARCHAR(250)
               ,StuStateDescrip NVARCHAR(100)
               ,StuZIP NVARCHAR(50)
               ,StuCountryDescrip NVARCHAR(50)
               ,StuPhone NVARCHAR(50)
               ,EnrollAcademicType NVARCHAR(50)
               ,StuEnrollIdList NVARCHAR(MAX)
            );

        IF @ShowMultipleEnrollments = 0
            BEGIN
                SET @StatusList = NULL;

                INSERT INTO #Temp1
                            SELECT     ASE.StudentId
                                      ,CASE WHEN (
                                                 APV.Credits > 0.0
                                                 AND APV.Hours > 0
                                                 )
                                                 OR (APV.IsContinuingEd = 1) THEN ''Credits-ClockHours''
                                            WHEN APV.Credits > 0.0 THEN ''Credits''
                                            WHEN APV.Hours > 0.0 THEN ''ClockHours''
                                            ELSE ''''
                                       END AcademicType
                                      ,CONVERT(NVARCHAR(50), ASE.StuEnrollId) AS StuEnrollIdList
                            FROM       arStuEnrollments ASE
                            INNER JOIN syStatusCodes SC ON SC.StatusCodeId = ASE.StatusCodeId
                            INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId
                            -- To get the Academic Type for each selected enrollment we go to Program version here
                            INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                            WHERE      (
                                       @StuEnrollIdList IS NULL
                                       OR ASE.StuEnrollId IN (
                                                             SELECT Val
                                                             FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                             )
                                       )
                            ORDER BY   ASE.StudentId
                                      ,ASE.StuEnrollId;
            END;
        ELSE
            BEGIN
                -- if the parameter ShowMultipleEnrollmnets is True we will update the table #Temp1 with all enrollments for student selected
                INSERT INTO #Temp1
                            SELECT     ASE.StudentId
                                      ,CASE WHEN (
             APV.Credits > 0.0
                                                 AND APV.Hours > 0
                                                 )
                                                 OR (APV.IsContinuingEd = 1) THEN ''Credits-ClockHours''
                                            WHEN APV.Credits > 0.0 THEN ''Credits''
                                            WHEN APV.Hours > 0.0 THEN ''ClockHours''
                                            ELSE ''''
                                       END AcademicType
                                      ,CONVERT(NVARCHAR(50), ASE.StuEnrollId) AS StuEnrollIdList
                            FROM       arStuEnrollments ASE
                            INNER JOIN syStatusCodes SC ON SC.StatusCodeId = ASE.StatusCodeId
                            INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId
                            -- To get the Academic Type for each selected enrollment we go to Program version here
                            INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                            WHERE      ASE.StudentId IN (
                                                        SELECT     DISTINCT AST.StudentId
                                                        FROM       arStudent AS AST
                                                        INNER JOIN arStuEnrollments AS ASE1 ON ASE1.StudentId = AST.StudentId
                                                        WHERE      (
                                                                   @StuEnrollIdList IS NULL
                                                                   OR ASE1.StuEnrollId IN (
                                                                                          SELECT Val
                                                                                          FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                                          )
                                                                   )
                                                        )
                            ORDER BY   ASE.StudentId
                                      ,ASE.StuEnrollId;
            END;

        IF (@ShowMultipleEnrollments = 1)
            -- if the parameter ShowMultipleEnrollmnets is True we will update the table #Temp1 with all enrollments for student selected
            -- but T1 table must be filtered for unique studentId
            BEGIN
                -- Create stuEnrollIdList for all student selected
                UPDATE     T4
                SET        T4.StuEnrollIdList = T2.List
                FROM       #Temp1 AS T4
                INNER JOIN (
                           SELECT      T1.StudentId
                                      ,T1.EnrollAcademicType
                                      ,LEFT(T3.List, LEN(T3.List) - 1) AS List
                           FROM        #Temp1 AS T1
                           CROSS APPLY (
                                       SELECT CONVERT(VARCHAR(50), T2.StuEnrollIdList) + '',''
                                       FROM   #Temp1 AS T2
                                       WHERE  T2.StudentId = T1.StudentId
                                              AND T2.EnrollAcademicType = T1.EnrollAcademicType
                                       FOR XML PATH('''')
                                       ) T3(List)
                           ) T2 ON T2.StudentId = T4.StudentId
                                   AND T2.EnrollAcademicType = T4.EnrollAcademicType;

            END;

        SELECT @StuEnrollIdListWithDoubleEnrollments = COALESCE(@StuEnrollIdListWithDoubleEnrollments + '', '', '''') + T1.StuEnrollIdList
        FROM   #Temp1 AS T1;

        EXECUTE dbo.USP_TR_Sub03_PrepareGPA @StuEnrollIdList = @StuEnrollIdListWithDoubleEnrollments
                     ,@ShowMultipleEnrollments = @ShowMultipleEnrollments
                                           ,@CoursesTakenTableName = @CoursesTakenTableName OUTPUT
                                           ,@GPASummaryTableName = @GPASummaryTableName OUTPUT;


        INSERT INTO #Temp2 (
                           StudentId
                          ,SSN
                          ,StuFirstName
                          ,StuLastName
                          ,StuMiddleName
                          ,StuDOB
                          ,StudentNumber
                          ,StuEmail
                          ,StuAddress1
                          ,StuAddress2
                          ,StuCity
                          ,StuStateDescrip
                          ,StuZIP
                          ,StuCountryDescrip
                          ,StuPhone
                          ,EnrollAcademicType
                          ,StuEnrollIdList
                           )
                    SELECT          AST.StudentId AS StudentId
                                   ,AST.SSN AS SSN
                                   ,AST.FirstName AS StuFirstName
                                   ,AST.LastName AS StuLastName
                                   ,AST.MiddleName AS StuMiddleName
                                   ,AST.DOB AS StuDOB
                                   ,AST.StudentNumber AS StudentNumber
                                   ,AST.HomeEmail AS StuEmail
                                   ,ASA.Address1 AS StuAddress1
                                   ,ASA.Address2 AS StuAddress2
                                   ,ASA.City AS StuCity
                                   ,ASA.StateDescrip AS StuStateDescrip
                                   ,ASA.Zip AS StuZIP
                                   ,ASA.CountryDescrip AS StuCountryDescrip
                                   ,ASP.Phone AS StuPhone
                                   ,T.EnrollAcademicType
                                   ,T.StuEnrollIdList
                    FROM            arStudent AS AST
                    INNER JOIN      (
                                    SELECT DISTINCT T1.StudentId
                                          ,T1.EnrollAcademicType
                                          ,T1.StuEnrollIdList
                                    FROM   #Temp1 AS T1
                                    ) AS T ON T.StudentId = AST.StudentId
                    LEFT OUTER JOIN (
                                    SELECT          ASA1.StudentId
                                                   ,ASA1.Address1
                                                   ,ASA1.Address2
                                                   ,ASA1.City
                                                   ,SS2.StateDescrip
                                                   ,ASA1.Zip
                                                   ,AC.CountryDescrip
                                                   ,SS.Status
                                    FROM            arStudAddresses AS ASA1
                                    INNER JOIN      syStatuses AS SS ON SS.StatusId = ASA1.StatusId
                                    LEFT OUTER JOIN adCountries AS AC ON AC.CountryId = ASA1.CountryId
                                    LEFT OUTER JOIN syStates AS SS2 ON SS2.StateId = ASA1.StateId
                                    WHERE           SS.Status = ''Active''
                                                    AND ASA1.default1 = 1
                                    ) AS ASA ON ASA.StudentId = AST.StudentId
                    LEFT OUTER JOIN (
                                    SELECT     ASP1.StudentId
                                              ,ASP1.Phone
                                              ,ASP1.StatusId
                                              ,SS1.Status
FROM       arStudentPhone AS ASP1
                                    INNER JOIN syStatuses AS SS1 ON SS1.StatusId = ASP1.StatusId
                                    WHERE      SS1.Status = ''Active''
                                               AND ASP1.default1 = 1
                                    ) AS ASP ON ASP.StudentId = AST.StudentId;

        -- SELECT AND SORT     
        SELECT   StudentId
                ,SSN
                ,StuFirstName
                ,StuLastName
                ,StuMiddleName
                ,StuDOB
                ,StudentNumber
                ,StuEmail
                ,StuAddress1
                ,StuAddress2
                ,StuCity
                ,StuStateDescrip
                ,StuZIP
                ,StuCountryDescrip
                ,StuPhone
                ,EnrollAcademicType
                ,StuEnrollIdList
                ,@CoursesTakenTableName AS CoursesTakenTableName
                ,@GPASummaryTableName AS GPASummaryTableName
        FROM     #Temp2
        ORDER BY CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,FirstName Asc,MiddleName Asc'' THEN (RANK() OVER (ORDER BY StuLastName
                                                                                                                           ,StuFirstName
                                                                                                                           ,StuMiddleName
                                                                                                                  )
                                                                                                     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,FirstName Asc,MiddleName Desc'' THEN (RANK() OVER (ORDER BY StuLastName
                                                                                                                            ,StuFirstName
                                                                                                                            ,StuMiddleName DESC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,FirstName Desc,MiddleName Asc'' THEN (RANK() OVER (ORDER BY StuLastName
                                                                                                                            ,StuFirstName DESC
                                                                                                                            ,StuMiddleName
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,FirstName Desc,MiddleName Desc'' THEN (RANK() OVER (ORDER BY StuLastName
                                                                                                                             ,StuFirstName DESC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                    )
                                                                                                       )
                 END
                --LD
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,FirstName Asc,MiddleName Asc'' THEN (RANK() OVER (ORDER BY StuLastName DESC
    ,StuFirstName
                                                                                                                            ,StuMiddleName
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,FirstName Asc,MiddleName Desc'' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                             ,StuFirstName
                                                                                                                             ,StuMiddleName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,FirstName Desc,MiddleName Asc'' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                             ,StuFirstName DESC
                                                                                                                             ,StuMiddleName
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,FirstName Desc,MiddleName Desc'' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                              ,StuFirstName DESC
                                                                                                                              ,StuMiddleName DESC
                                                                                                                     )
                                                                                                        )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,MiddleName Asc,FirstName Asc'' THEN (RANK() OVER (ORDER BY StuLastName ASC
                                                                                                                           ,StuMiddleName ASC
                                                                                                                           ,StuFirstName ASC
                                                                                                                  )
                                                                                                     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,MiddleName Asc,FirstName Desc'' THEN (RANK() OVER (ORDER BY StuLastName ASC
                                                                                                                            ,StuMiddleName ASC
                                                                                                                            ,StuFirstName DESC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,MiddleName Desc,FirstName Asc'' THEN (RANK() OVER (ORDER BY StuLastName ASC
                                                       ,StuMiddleName DESC
                                                                                                                            ,StuFirstName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,MiddleName Desc,FirstName Desc'' THEN (RANK() OVER (ORDER BY StuLastName ASC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                             ,StuFirstName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,MiddleName Asc,FirstName Asc'' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                            ,StuMiddleName ASC
                                                                                                                            ,StuFirstName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,MiddleName Asc,FirstName Desc'' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                             ,StuMiddleName ASC
                                                                                                                             ,StuFirstName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,MiddleName Desc,FirstName Asc'' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                             ,StuFirstName ASC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,MiddleName Desc,FirstName Desc'' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                              ,StuMiddleName DESC
                                                                                                                              ,StuFirstName DESC
                                                                                                                     )
                                                                                                        )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,LastName Asc,MiddleName Asc'' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                           ,StuLastName ASC
                                                                                                                           ,StuMiddleName ASC
                                                                                                                  )
                                                                                                     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,LastName Asc,MiddleName Desc'' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                            ,StuLastName ASC
                                                                                                                            ,StuMiddleName DESC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,LastName Desc,MiddleName Asc'' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                            ,StuLastName DESC
                                                                                                                            ,StuMiddleName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,LastName Desc,MiddleName Desc'' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                             ,StuLastName DESC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                    )
                                                                                                       )
                 END
                --LD
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,LastName Asc,MiddleName Asc'' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                            ,StuLastName ASC
                                                                                                                            ,StuMiddleName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,LastName Asc,MiddleName Desc'' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                             ,StuLastName ASC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,LastName Desc,MiddleName Asc'' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                             ,StuLastName DESC
                                                                                                                             ,StuMiddleName ASC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,LastName Desc,StuMiddleName Desc'' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                                 ,StuLastName DESC
                                                                                                                                 ,StuMiddleName DESC
                                                                                                                        )
                                                                                                           )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,MiddleName Asc,LastName Asc'' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                           ,StuMiddleName ASC
                                                                                                                           ,StuLastName ASC
                                                                                                                  )
                                                                                                     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,MiddleName Asc,LastName Desc'' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                            ,StuMiddleName ASC
                                                                                                                            ,StuLastName DESC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,MiddleName Desc,LastName Asc'' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                            ,StuMiddleName DESC
                                                                                                                            ,StuLastName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,MiddleName Desc,LastName Desc'' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                             ,StuLastName DESC
                                                                                                                    )
     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,MiddleName Asc,LastName Asc'' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                            ,StuMiddleName ASC
                                                                                                                            ,StuLastName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,MiddleName Asc,LastName Desc'' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                             ,StuMiddleName ASC
                                                                                                                             ,StuLastName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,MiddleName Desc,LastName Asc'' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                             ,StuLastName ASC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,MiddleName Desc,LastName Desc'' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                              ,StuMiddleName DESC
                                                                                                                              ,StuLastName DESC
                                                                                                                     )
                                                                                                        )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,LastName Asc,FirstName Asc'' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                           ,StuLastName ASC
                                                                                                                           ,StuFirstName ASC
                                                                                                                  )
                                                                                                     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,LastName Asc,FirstName Desc'' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                            ,StuLastName ASC
                                                                                                                            ,StuFirstName DESC
                                                                                                                   )
                                                                         )
                 END

                -- Start here
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,LastName Desc,FirstName Asc'' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                            ,StuLastName DESC
                                                                                                                            ,StuFirstName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,LastName Desc,FirstName Desc'' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                             ,StuLastName DESC
                                                                                                                             ,StuFirstName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,LastName Asc,FirstName Asc'' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                            ,StuLastName ASC
                                                                                                                            ,StuFirstName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,LastName Asc,FirstName Desc'' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                             ,StuLastName ASC
                                                                                                                             ,StuFirstName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,LastName Desc,FirstName Asc'' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                             ,StuLastName DESC
                                                                                                                             ,StuFirstName ASC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,LastName Desc,FirstName Desc'' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                              ,StuLastName DESC
                                                                                                                              ,StuFirstName DESC
                                                                               )
                                                                                                        )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,FirstName Asc,LastName Asc'' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                           ,StuFirstName ASC
                                                                                                                           ,StuLastName ASC
                                                                                                                  )
                                                                                                     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,FirstName Asc,LastName Desc'' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                            ,StuFirstName ASC
                                                                                                                            ,StuLastName DESC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,FirstName Desc,LastName Asc'' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                            ,StuFirstName DESC
                                                                                                                            ,StuLastName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,FirstName Desc,LastName Desc'' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                             ,StuFirstName DESC
                                                                                                                             ,StuLastName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,FirstName Asc,LastName Asc'' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                            ,StuFirstName ASC
                                                                                                                            ,StuLastName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,FirstName Asc,LastName Desc'' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                             ,StuFirstName ASC
 ,StuLastName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,FirstName Desc,LastName Asc'' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                             ,StuFirstName DESC
                                                                                                                             ,StuLastName ASC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,FirstName Desc,LastName Desc'' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                              ,StuFirstName DESC
                                                                                                                              ,StuLastName DESC
                                                                                                                     )
                                                                                                        )
                 END
                ,StudentId;
    END;
-- =========================================================================================================
-- END  --  USP_TR_Sub03_StudentInfo 
-- =========================================================================================================

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_TR_Sub06_Courses]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub06_Courses]', 'P') IS NULL
EXEC sp_executesql N'
-- =========================================================================================================
-- USP_TR_Sub06_Courses
-- =========================================================================================================
CREATE PROCEDURE [dbo].[USP_TR_Sub06_Courses]
    @CoursesTakenTableName NVARCHAR(200)
   ,@GPASummaryTableName NVARCHAR(200)
   ,@StuEnrollIdList NVARCHAR(MAX)
   ,@TermId UNIQUEIDENTIFIER = NULL
   ,@ShowMultipleEnrollments BIT = 0
AS --
    BEGIN
        -- 0) Declare varialbles and Set initial values
        BEGIN

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#CoursesTaken'')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            CREATE TABLE #CoursesTaken
                (
                    CoursesTakenId INTEGER NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,ReqId UNIQUEIDENTIFIER
                   ,ReqCode VARCHAR(100)
                   ,ReqDescription VARCHAR(100)
                   ,ReqCodeDescrip VARCHAR(100)
                   ,ReqCredits DECIMAL(18, 2)
                   ,MinVal DECIMAL(18, 2)
                   ,ClsSectionId VARCHAR(50)
                   ,CourseStarDate DATETIME
                   ,CourseEndDate DATETIME
                   ,SCS_CreditsAttempted DECIMAL(18, 2)
                   ,SCS_CreditsEarned DECIMAL(18, 2)
                   ,SCS_CurrentScore DECIMAL(18, 2)
                   ,SCS_CurrentGrade VARCHAR(10)
                   ,SCS_FinalScore DECIMAL(18, 2)
                   ,SCS_FinalGrade VARCHAR(10)
                   ,SCS_Completed BIT
                   ,SCS_FinalGPA DECIMAL(18, 2)
                   ,SCS_Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_WeightedAverage_Credits DECIMAL(18, 2)
                   ,SCS_Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_SimpleAverage_Credits DECIMAL(18, 2)
                   ,SCS_ModUser VARCHAR(50)
                   ,SCS_ModDate DATETIME
                   ,SCS_TermGPA_Simple DECIMAL(18, 2)
                   ,SCS_TermGPA_Weighted DECIMAL(18, 2)
                   ,SCS_coursecredits DECIMAL(18, 2)
                   ,SCS_CumulativeGPA DECIMAL(18, 2)
                   ,SCS_CumulativeGPA_Simple DECIMAL(18, 2)
                   ,SCS_FACreditsEarned DECIMAL(18, 2)
                   ,SCS_Average DECIMAL(18, 2)
                   ,SCS_CumAverage DECIMAL(18, 2)
                   ,ScheduleDays DECIMAL(18, 2)
                   ,ActualDay DECIMAL(18, 2)
                   ,FinalGPA_Calculations DECIMAL(18, 2)
                   ,FinalGPA_TermCalculations DECIMAL(18, 2)
                   ,CreditsEarned_Calculations DECIMAL(18, 2)
                   ,ActualDay_Calculations DECIMAL(18, 2)
                   ,GrdBkWgtDetailsCount INTEGER
                   ,CountMultipleEnrollment INTEGER
                   ,RowNumberMultipleEnrollment INTEGER
                   ,CountRepeated INTEGER
                   ,RowNumberRetaked INTEGER
                   ,SameTermCountRetaken INTEGER
                   ,RowNumberSameTermRetaked INTEGER
                   ,RowNumberMultEnrollNoRetaken INTEGER
                   ,RowNumberOverAllByClassSection INTEGER
                   ,IsCreditsEarned BIT
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#GPASummary'')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
            CREATE TABLE #GPASummary
                (
                    GPASummaryId INTEGER NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,LastName VARCHAR(50)
                   ,FirstName VARCHAR(50)
                   ,MiddleName VARCHAR(50)
                   ,SSN VARCHAR(11)
                   ,StudentNumber VARCHAR(50)
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,EnrollmentID VARCHAR(50)
                   ,PrgVerId UNIQUEIDENTIFIER
                   ,PrgVerDescrip VARCHAR(50)
                   ,PrgVersionTrackCredits BIT
                   ,GrdSystemId UNIQUEIDENTIFIER
                   ,AcademicType VARCHAR(50)
                   ,ClockHourProgram VARCHAR(10)
                   ,IsMakingSAP BIT
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,TermEndDate DATETIME
                   ,DescripXTranscript VARCHAR(250)
                   ,TermAverage DECIMAL(18, 2)
                   ,EnroTermSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPACredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPA DECIMAL(18, 2)
                   ,EnroTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPACredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPA DECIMAL(18, 2)
                   ,StudTermSimple_CourseCredits DECIMAL(18, 2)
                   ,StudTermSimple_GPACredits DECIMAL(18, 2)
                   ,StudTermSimple_GPA DECIMAL(18, 2)
                   ,StudTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudTermWeight_GPACredits DECIMAL(18, 2)
                   ,StudTermWeight_GPA DECIMAL(18, 2)
                   ,EnroSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroSimple_GPACredits DECIMAL(18, 2)
                   ,EnroSimple_GPA DECIMAL(18, 2)
                   ,EnroWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroWeight_GPACredits DECIMAL(18, 2)
                   ,EnroWeight_GPA DECIMAL(18, 2)
                   ,StudSimple_CourseCredits DECIMAL(18, 2)
                   ,StudSimple_GPACredits DECIMAL(18, 2)
                   ,StudSimple_GPA DECIMAL(18, 2)
                   ,StudWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudWeight_GPACredits DECIMAL(18, 2)
                   ,StudWeight_GPA DECIMAL(18, 2)
                   ,GradesFormat VARCHAR(50)
                   ,GPAMethod VARCHAR(50)
                   ,GradeBookAt VARCHAR(50)
                   ,RetakenPolicy VARCHAR(50)
                );
        END;
        -- END --  0) Declare varialbles and Set initial values

        --  1)Fill temp tables
        BEGIN
            INSERT INTO #CoursesTaken
            EXEC dbo.USP_TR_Sub03_GetPreparedGPATable @TableName = @CoursesTakenTableName;


            INSERT INTO #GPASummary
            EXEC dbo.USP_TR_Sub03_GetPreparedGPATable @TableName = @GPASummaryTableName;

        END;
        -- END  --  1)Fill temp tables

        --  3) select info for courses
        BEGIN
            SELECT   DT.PrgVerId
                    ,DT.TermId
                    ,DT.TermDescription AS TermDescription
                    ,DT.TermStartDate
                    ,DT.AcademicType
                    ,DT.ReqId
                    ,DT.Course
                    ,DT.CourseStartDate
                    ,DT.ReqCode
                    ,DT.ReqDescription
                    ,DT.ReqCodeDescrip
                    ,DT.ReqCredits
                    ,DT.MinVal
                    ,DT.SCS_ReqDescrip
                    ,DT.SCS_CreditsAttempted
                    ,DT.SCS_CreditsEarned
                    ,DT.SCS_CurrentScore
                    ,DT.SCS_CurrentGrade
                    ,DT.FinalScore
                    ,DT.FinalGrade
                    ,DT.FinalGPA
                    ,DT.ScheduleDays
                    ,DT.ActualDay
                    ,DT.GradesFormat
            FROM     (
                     SELECT    distinct  GS.PrgVerId AS PrgVerId
                               ,GS.TermId AS TermId
                               ,GS.TermDescrip AS TermDescription
                               ,GS.TermStartDate AS TermStartDate
                               ,GS.AcademicType AS AcademicType
                               ,CT.ReqId AS ReqId
                               ,CT.ReqCode AS Course
                               ,CT.CourseStarDate AS CourseStartDate
                               ,CT.ReqCode AS ReqCode
                               ,CT.ReqDescription AS ReqDescription
                               ,CT.ReqCodeDescrip AS ReqCodeDescrip
                               ,CT.ReqCredits AS ReqCredits
                               ,CT.MinVal AS MinVal
                               ,CT.ReqDescription AS SCS_ReqDescrip
                               ,CT.SCS_CreditsAttempted AS SCS_CreditsAttempted
                               ,CT.SCS_CreditsEarned AS SCS_CreditsEarned
                               ,CT.SCS_CurrentScore AS SCS_CurrentScore
                               ,CT.SCS_CurrentGrade AS SCS_CurrentGrade
                               ,CT.SCS_FinalScore AS FinalScore
                               ,CT.SCS_FinalGrade AS FinalGrade
                               ,CT.SCS_FinalGPA AS FinalGPA
                               ,CT.ScheduleDays AS ScheduleDays
                               ,CT.ActualDay AS ActualDay
                               ,GS.GradesFormat AS GradesFormat
							   ,D.Seq
                     FROM       #GPASummary AS GS
                     INNER JOIN #CoursesTaken AS CT ON CT.StuEnrollId = GS.StuEnrollId
                                                       AND CT.TermId = GS.TermId
                     INNER JOIN (
                                SELECT T.Val
                                FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1) AS T
                                ) AS List ON List.Val = GS.StuEnrollId
                     LEFT JOIN  dbo.arClassSections CS ON CS.ReqId = CT.ReqId
                     LEFT JOIN  dbo.arGrdBkWgtDetails D ON D.InstrGrdBkWgtId = CS.InstrGrdBkWgtId
                     WHERE      (
                                @TermId IS NULL
                                OR GS.TermId = @TermId
                                )
                                AND (
                                    (
                                    @ShowMultipleEnrollments = 1
                                    AND CT.RowNumberMultEnrollNoRetaken = 1
                                    )
                                    OR @ShowMultipleEnrollments = 0
                                    )
                     ) AS DT
            ORDER BY CASE WHEN @TermId IS NOT NULL THEN ( RANK() OVER ( ORDER BY DT.TermStartDate
                                                                                ,DT.TermDescription
																				,DT.Seq
                                                                                ,DT.ReqDescription
                                                                      )
                                                        )
                          ELSE ( RANK() OVER ( ORDER BY DT.CourseStartDate
                                                       ,DT.ReqDescription
                                             )
                               )
                     END;
        END;
        -- END  --  3) select info for courses

        -- 4) drop temp tables
        BEGIN
             --Drop temp tables
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#CoursesTaken'')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#GPASummary'')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
        END;
    -- END  --  4) drop temp tables
    END;
-- =========================================================================================================
-- END  --  USP_TR_Sub06_Courses
-- =========================================================================================================

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_TR_Sub3_StudentInfo]'
GO
IF OBJECT_ID(N'[dbo].[Usp_TR_Sub3_StudentInfo]', 'P') IS NULL
EXEC sp_executesql N'-- =========================================================================================================
-- Usp_TR_Sub3_StudentInfo 
-- =========================================================================================================
CREATE PROCEDURE [dbo].[Usp_TR_Sub3_StudentInfo]
    @StuEnrollIdList NVARCHAR(MAX) = NULL
   ,@ShowMultipleEnrollments BIT = 0
   ,@StatusList NVARCHAR(MAX) = NULL
   ,@OrderBy NVARCHAR(1000) = NULL
AS
    BEGIN
        CREATE TABLE #Temp1
            (
             StudentId NVARCHAR(50)
            ,EnrollAcademicType NVARCHAR(50)
            ,StuEnrollIdList NVARCHAR(MAX)
            );
        CREATE TABLE #Temp2
            (
             StudentId UNIQUEIDENTIFIER
            ,SSN NVARCHAR(50)
            ,StuFirstName NVARCHAR(50)
            ,StuLastName NVARCHAR(50)
            ,StuMiddleName NVARCHAR(50)
            ,StuDOB DATETIME
            ,StudentNumber NVARCHAR(50)
            ,StuEmail NVARCHAR(50)
            ,StuAddress1 NVARCHAR(250)
            ,StuAddress2 NVARCHAR(50)
            ,StuCity NVARCHAR(50)
            ,StuStateDescrip NVARCHAR(80)
            ,StuZIP NVARCHAR(50)
            ,StuCountryDescrip NVARCHAR(50)
            ,StuPhone NVARCHAR(50)
            ,EnrollAcademicType NVARCHAR(50)
            ,StuEnrollIdList NVARCHAR(MAX)
            );

        IF @ShowMultipleEnrollments = 0
            BEGIN
                SET @StatusList = NULL;
				
                INSERT  INTO #Temp1
                        SELECT  ASE.StudentId
                               ,CASE WHEN (
                                            APV.Credits > 0.0
                                            AND APV.Hours > 0
                                          )
                                          OR ( APV.IsContinuingEd = 1 ) THEN ''Credits-ClockHours''
                                     WHEN APV.Credits > 0.0 THEN ''Credits''
                                     WHEN APV.Hours > 0.0 THEN ''ClockHours''
                                     ELSE ''''
                                END AcademicType
                               ,CONVERT(NVARCHAR(50),ASE.StuEnrollId) AS StuEnrollIdList
                        FROM    arStuEnrollments ASE
                        INNER JOIN syStatusCodes SC ON SC.StatusCodeId = ASE.StatusCodeId
                        INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId
					-- To get the Academic Type for each selected enrollment we go to Program version here
                        INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                        WHERE   (
                                  @StuEnrollIdList IS NULL
                                  OR ASE.StuEnrollId IN ( SELECT    Val
                                                          FROM      MultipleValuesForReportParameters(@StuEnrollIdList,'','',1) )
                                )
                        ORDER BY ASE.StudentId
                               ,ASE.StuEnrollId;                                          
            END;
        ELSE
            BEGIN
				-- if the parameter ShowMultipleEnrollmnets is True we will update the table #Temp1 with all enrollments for student selected
                INSERT  INTO #Temp1
                        SELECT  ASE.StudentId
                               ,CASE WHEN (
                                            APV.Credits > 0.0
                                            AND APV.Hours > 0
                                          )
                                          OR ( APV.IsContinuingEd = 1 ) THEN ''Credits-ClockHours''
                                     WHEN APV.Credits > 0.0 THEN ''Credits''
                                     WHEN APV.Hours > 0.0 THEN ''ClockHours''
                                     ELSE ''''
                                END AcademicType
                               ,CONVERT(NVARCHAR(50),ASE.StuEnrollId) AS StuEnrollIdList
                        FROM    arStuEnrollments ASE
                        INNER JOIN syStatusCodes SC ON SC.StatusCodeId = ASE.StatusCodeId
                        INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId
					-- To get the Academic Type for each selected enrollment we go to Program version here
                        INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                        WHERE   ASE.StudentId IN ( SELECT DISTINCT
                                                            AST.StudentId
                                                   FROM     arStudent AS AST
                                                   INNER JOIN arStuEnrollments AS ASE1 ON ASE1.StudentId = AST.StudentId
                                                   WHERE    (
                                                              @StuEnrollIdList IS NULL
                                                              OR ASE1.StuEnrollId IN ( SELECT   Val
                                                                                       FROM     MultipleValuesForReportParameters(@StuEnrollIdList,'','',1) )
                                                            ) )
                        ORDER BY ASE.StudentId
                               ,ASE.StuEnrollId;
            END;

        IF ( @ShowMultipleEnrollments = 1 )
		-- if the parameter ShowMultipleEnrollmnets is True we will update the table #Temp1 with all enrollments for student selected
		-- but T1 table must be filtered for unique studentId
            BEGIN
				-- Create stuEnrollIdList for all student selected
                UPDATE  T4
                SET     T4.StuEnrollIdList = T2.List
                FROM    #Temp1 AS T4
                INNER JOIN (
                             SELECT T1.StudentId
                                   ,T1.EnrollAcademicType
                                   ,List = LEFT(T3.List,LEN(T3.List) - 1)
                             FROM   #Temp1 AS T1
                             CROSS APPLY (
                                           SELECT   CONVERT(VARCHAR(50),T2.StuEnrollIdList) + '',''
                                           FROM     #Temp1 AS T2
                                           WHERE    T2.StudentId = T1.StudentId
                                                    AND T2.EnrollAcademicType = T1.EnrollAcademicType
                                         FOR
                                           XML PATH('''')
                                         ) T3 ( List )
                           ) T2 ON T2.StudentId = T4.StudentId
                                   AND T2.EnrollAcademicType = T4.EnrollAcademicType; 
            END;


        INSERT  INTO #Temp2
                (
                 StudentId
                ,SSN
                ,StuFirstName
                ,StuLastName
                ,StuMiddleName
                ,StuDOB
                ,StudentNumber
                ,StuEmail
                ,StuAddress1
                ,StuAddress2
                ,StuCity
                ,StuStateDescrip
                ,StuZIP
                ,StuCountryDescrip
                ,StuPhone
                ,EnrollAcademicType
                ,StuEnrollIdList
				)
                SELECT  AST.StudentId AS StudentId
                       ,AST.SSN AS SSN
                       ,AST.FirstName AS StuFirstName
                       ,AST.LastName AS StuLastName
                       ,AST.MiddleName AS StuMiddleName
                       ,AST.DOB AS StuDOB
                       ,AST.StudentNumber AS StudentNumber
                       ,AST.HomeEmail AS StuEmail
                       ,ASA.Address1 AS StuAddress1
                       ,ASA.Address2 AS StuAddress2
                       ,ASA.City AS StuCity
                       ,ASA.StateDescrip AS StuStateDescrip
                       ,ASA.Zip AS StuZIP
                       ,ASA.CountryDescrip AS StuCountryDescrip
                       ,ASP.Phone AS StuPhone
                       ,T.EnrollAcademicType
                       ,T.StuEnrollIdList
                FROM    arStudent AS AST
                INNER JOIN (
                             SELECT DISTINCT
                                    T1.StudentId
                                   ,T1.EnrollAcademicType
                                   ,T1.StuEnrollIdList
                             FROM   #Temp1 AS T1
                           ) AS T ON T.StudentId = AST.StudentId
                LEFT OUTER JOIN (
                                  SELECT    ASA1.StudentId
                                           ,ASA1.Address1
                                           ,ASA1.Address2
                                           ,ASA1.City
                                           ,SS2.StateDescrip
                                           ,ASA1.Zip
                                           ,AC.CountryDescrip
                                           ,SS.Status
                                  FROM      arStudAddresses AS ASA1
                                  INNER JOIN syStatuses AS SS ON SS.StatusId = ASA1.StatusId
                                  LEFT OUTER JOIN adCountries AS AC ON AC.CountryId = ASA1.CountryId
                                  LEFT OUTER JOIN syStates AS SS2 ON SS2.StateId = ASA1.StateId
                                  WHERE     SS.Status = ''Active''
                                            AND ASA1.default1 = 1
                                ) AS ASA ON ASA.StudentId = AST.StudentId
                LEFT OUTER JOIN (
                                  SELECT    ASP1.StudentId
                                           ,ASP1.Phone
                                           ,ASP1.StatusId
                                           ,SS1.Status
                                  FROM      arStudentPhone AS ASP1
                                  INNER JOIN syStatuses AS SS1 ON SS1.StatusId = ASP1.StatusId
                                  WHERE     SS1.Status = ''Active''
                                            AND ASP1.default1 = 1
                                ) AS ASP ON ASP.StudentId = AST.StudentId;
  
		-- SELECT AND SORT     
        SELECT  StudentId
               ,SSN
               ,StuFirstName
               ,StuLastName
               ,StuMiddleName
               ,StuDOB
               ,StudentNumber
               ,StuEmail
               ,StuAddress1
               ,StuAddress2
               ,StuCity
               ,StuStateDescrip
               ,StuZIP
               ,StuCountryDescrip
               ,StuPhone
               ,EnrollAcademicType
               ,StuEnrollIdList
        FROM    #Temp2
        ORDER BY CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,FirstName Asc,MiddleName Asc''
                      THEN ( RANK() OVER ( ORDER BY StuLastName, StuFirstName, StuMiddleName ) )
                 END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,FirstName Asc,MiddleName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuLastName, StuFirstName, StuMiddleName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,FirstName Desc,MiddleName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuLastName, StuFirstName DESC, StuMiddleName ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,FirstName Desc,MiddleName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuLastName, StuFirstName DESC, StuMiddleName DESC ) )
                END
               ,
			 --LD
                CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,FirstName Asc,MiddleName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuFirstName, StuMiddleName ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,FirstName Asc,MiddleName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuFirstName, StuMiddleName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,FirstName Desc,MiddleName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuFirstName DESC, StuMiddleName ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,FirstName Desc,MiddleName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuFirstName DESC, StuMiddleName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,MiddleName Asc,FirstName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuLastName ASC, StuMiddleName ASC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,MiddleName Asc,FirstName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuLastName ASC, StuMiddleName ASC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,MiddleName Desc,FirstName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuLastName ASC, StuMiddleName DESC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,MiddleName Desc,FirstName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuLastName ASC, StuMiddleName DESC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,MiddleName Asc,FirstName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuMiddleName ASC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,MiddleName Asc,FirstName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuMiddleName ASC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,MiddleName Desc,FirstName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuMiddleName DESC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,MiddleName Desc,FirstName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuLastName DESC, StuMiddleName DESC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,LastName Asc,MiddleName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuLastName ASC, StuMiddleName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,LastName Asc,MiddleName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuLastName ASC, StuMiddleName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,LastName Desc,MiddleName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuLastName DESC, StuMiddleName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,LastName Desc,MiddleName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuLastName DESC, StuMiddleName DESC ) )
                END
               ,
			 --LD
                CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,LastName Asc,MiddleName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuLastName ASC, StuMiddleName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,LastName Asc,MiddleName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuLastName ASC, StuMiddleName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,LastName Desc,MiddleName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuLastName DESC, StuMiddleName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,LastName Desc,StuMiddleName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuLastName DESC, StuMiddleName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,MiddleName Asc,LastName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuMiddleName ASC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,MiddleName Asc,LastName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuMiddleName ASC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,MiddleName Desc,LastName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuMiddleName DESC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,MiddleName Desc,LastName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuFirstName ASC, StuMiddleName DESC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,MiddleName Asc,LastName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuMiddleName ASC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,MiddleName Asc,LastName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuMiddleName ASC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,MiddleName Desc,LastName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuMiddleName DESC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,MiddleName Desc,LastName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuFirstName DESC, StuMiddleName DESC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,LastName Asc,FirstName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuLastName ASC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,LastName Asc,FirstName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuLastName ASC, StuFirstName DESC ) )
                END
               ,
			 
			 -- Start here
                CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,LastName Desc,FirstName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuLastName DESC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,LastName Desc,FirstName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuLastName DESC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,LastName Asc,FirstName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuLastName ASC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,LastName Asc,FirstName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuLastName ASC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,LastName Desc,FirstName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuLastName DESC, StuFirstName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,LastName Desc,FirstName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuLastName DESC, StuFirstName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,FirstName Asc,LastName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuFirstName ASC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,FirstName Asc,LastName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuFirstName ASC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,FirstName Desc,LastName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuFirstName DESC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,FirstName Desc,LastName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName ASC, StuFirstName DESC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,FirstName Asc,LastName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuFirstName ASC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,FirstName Asc,LastName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuFirstName ASC, StuLastName DESC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,FirstName Desc,LastName Asc''
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuFirstName DESC, StuLastName ASC ) )
                END
               ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,FirstName Desc,LastName Desc''
                     THEN ( RANK() OVER ( ORDER BY StuMiddleName DESC, StuFirstName DESC, StuLastName DESC ) )
                END
               ,StudentId;
    END;
-- =========================================================================================================
-- END  --  Usp_TR_Sub3_StudentInfo 
-- =========================================================================================================
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[adLeadsView]'
GO
IF OBJECT_ID(N'[dbo].[adLeadsView]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[adLeadsView]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[GlTransView1]'
GO
IF OBJECT_ID(N'[dbo].[GlTransView1]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[GlTransView1]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[GlTransView2]'
GO
IF OBJECT_ID(N'[dbo].[GlTransView2]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[GlTransView2]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[NewStudentSearch]'
GO
IF OBJECT_ID(N'[dbo].[NewStudentSearch]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[NewStudentSearch]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[plStudentEducation]'
GO
IF OBJECT_ID(N'[dbo].[plStudentEducation]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[plStudentEducation]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[plStudentEmployment]'
GO
IF OBJECT_ID(N'[dbo].[plStudentEmployment]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[plStudentEmployment]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[plStudentExtraCurriculars]'
GO
IF OBJECT_ID(N'[dbo].[plStudentExtraCurriculars]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[plStudentExtraCurriculars]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[plStudentSkills]'
GO
IF OBJECT_ID(N'[dbo].[plStudentSkills]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[plStudentSkills]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[SettingStdScoreLimit]'
GO
IF OBJECT_ID(N'[dbo].[SettingStdScoreLimit]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[SettingStdScoreLimit]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[StudentEnrollmentSearch]'
GO
IF OBJECT_ID(N'[dbo].[StudentEnrollmentSearch]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[StudentEnrollmentSearch]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[syStudentContactAddresses]'
GO
IF OBJECT_ID(N'[dbo].[syStudentContactAddresses]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[syStudentContactAddresses]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[syStudentContactPhones]'
GO
IF OBJECT_ID(N'[dbo].[syStudentContactPhones]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[syStudentContactPhones]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[syStudentContacts]'
GO
IF OBJECT_ID(N'[dbo].[syStudentContacts]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[syStudentContacts]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[syStudentNotes]'
GO
IF OBJECT_ID(N'[dbo].[syStudentNotes]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[syStudentNotes]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[VIEW_GetHistory]'
GO
IF OBJECT_ID(N'[dbo].[VIEW_GetHistory]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[VIEW_GetHistory]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[VIEW_LeadUserTask]'
GO
IF OBJECT_ID(N'[dbo].[VIEW_LeadUserTask]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[VIEW_LeadUserTask]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[View_Messages]'
GO
IF OBJECT_ID(N'[dbo].[View_Messages]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[View_Messages]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[VIEW_SySDFModuleValue_Lead]'
GO
IF OBJECT_ID(N'[dbo].[VIEW_SySDFModuleValue_Lead]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[VIEW_SySDFModuleValue_Lead]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[View_TaskNotes]'
GO
IF OBJECT_ID(N'[dbo].[View_TaskNotes]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[View_TaskNotes]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[VIEW1]'
GO
IF OBJECT_ID(N'[dbo].[VIEW1]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[VIEW1]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UIX_adReqs_Code_Descrip_CampGrpId] on [dbo].[adReqs]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'UIX_adReqs_Code_Descrip_CampGrpId' AND object_id = OBJECT_ID(N'[dbo].[adReqs]'))
CREATE UNIQUE NONCLUSTERED INDEX [UIX_adReqs_Code_Descrip_CampGrpId] ON [dbo].[adReqs] ([Code], [Descrip], [CampGrpId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UIX_adSourceCatagory_SourceCatagoryCode_SourceCatagoryDescrip_CampGrpId] on [dbo].[adSourceCatagory]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'UIX_adSourceCatagory_SourceCatagoryCode_SourceCatagoryDescrip_CampGrpId' AND object_id = OBJECT_ID(N'[dbo].[adSourceCatagory]'))
CREATE UNIQUE NONCLUSTERED INDEX [UIX_adSourceCatagory_SourceCatagoryCode_SourceCatagoryDescrip_CampGrpId] ON [dbo].[adSourceCatagory] ([SourceCatagoryCode], [SourceCatagoryDescrip], [CampGrpId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UIX_arPrgGrp_PrgGrpCode_PrgGrpDescrip_CampGrpId] on [dbo].[arPrgGrp]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'UIX_arPrgGrp_PrgGrpCode_PrgGrpDescrip_CampGrpId' AND object_id = OBJECT_ID(N'[dbo].[arPrgGrp]'))
CREATE UNIQUE NONCLUSTERED INDEX [UIX_arPrgGrp_PrgGrpCode_PrgGrpDescrip_CampGrpId] ON [dbo].[arPrgGrp] ([PrgGrpCode], [PrgGrpDescrip], [CampGrpId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UIX_arPrograms_ProgCode_ProgDescrip_CampGrpId] on [dbo].[arPrograms]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'UIX_arPrograms_ProgCode_ProgDescrip_CampGrpId' AND object_id = OBJECT_ID(N'[dbo].[arPrograms]'))
CREATE UNIQUE NONCLUSTERED INDEX [UIX_arPrograms_ProgCode_ProgDescrip_CampGrpId] ON [dbo].[arPrograms] ([ProgCode], [ProgDescrip], [CampGrpId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UIX_faLenders_Code_LenderDescrip_PrimaryContact_CampGrpId] on [dbo].[faLenders]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'UIX_faLenders_Code_LenderDescrip_PrimaryContact_CampGrpId' AND object_id = OBJECT_ID(N'[dbo].[faLenders]'))
CREATE UNIQUE NONCLUSTERED INDEX [UIX_faLenders_Code_LenderDescrip_PrimaryContact_CampGrpId] ON [dbo].[faLenders] ([Code], [LenderDescrip], [PrimaryContact], [CampGrpId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UIX_plEmployerJobs_Code_EmployerJobTitle_JobTitleId_EmployerId_CampGrpId] on [dbo].[plEmployerJobs]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'UIX_plEmployerJobs_Code_EmployerJobTitle_JobTitleId_EmployerId_CampGrpId' AND object_id = OBJECT_ID(N'[dbo].[plEmployerJobs]'))
CREATE UNIQUE NONCLUSTERED INDEX [UIX_plEmployerJobs_Code_EmployerJobTitle_JobTitleId_EmployerId_CampGrpId] ON [dbo].[plEmployerJobs] ([Code], [EmployerJobTitle], [JobTitleId], [EmployerId], [CampGrpId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [UIX_plEmployers_Code_EmployerDescrip_CampGrpId] on [dbo].[plEmployers]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'UIX_plEmployers_Code_EmployerDescrip_CampGrpId' AND object_id = OBJECT_ID(N'[dbo].[plEmployers]'))
CREATE UNIQUE NONCLUSTERED INDEX [UIX_plEmployers_Code_EmployerDescrip_CampGrpId] ON [dbo].[plEmployers] ([Code], [EmployerDescrip], [CampGrpId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[arClsSectMeetings]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_arClsSectMeetings_syPeriods_PeriodId_PeriodId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[arClsSectMeetings]', 'U'))
ALTER TABLE [dbo].[arClsSectMeetings] ADD CONSTRAINT [FK_arClsSectMeetings_syPeriods_PeriodId_PeriodId] FOREIGN KEY ([PeriodId]) REFERENCES [dbo].[syPeriods] ([PeriodId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[arStuEnrollments]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_arStuEnrollments_arPrograms_TransferHoursFromThisSchoolEnrollmentId_stuEnrollmentId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[arStuEnrollments]', 'U'))
ALTER TABLE [dbo].[arStuEnrollments] ADD CONSTRAINT [FK_arStuEnrollments_arPrograms_TransferHoursFromThisSchoolEnrollmentId_stuEnrollmentId] FOREIGN KEY ([TransferHoursFromThisSchoolEnrollmentId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[plExitInterview]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__plExitInt__InelR__7C65823E]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[plExitInterview]', 'U'))
ALTER TABLE [dbo].[plExitInterview] ADD CONSTRAINT [FK__plExitInt__InelR__7C65823E] FOREIGN KEY ([InelReasonId]) REFERENCES [dbo].[syIneligibilityReasons] ([InelReasonId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[syAccreditingAgencies]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_syAccreditingAgencies_syStatuses_StatusId_StatusId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[syAccreditingAgencies]', 'U'))
ALTER TABLE [dbo].[syAccreditingAgencies] ADD CONSTRAINT [FK_syAccreditingAgencies_syStatuses_StatusId_StatusId] FOREIGN KEY ([StatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[syApprovedNACCASProgramVersion]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_syApprovedNACCASProgramVersion_arPrgVersions_PrgVerId_PrgVerId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[syApprovedNACCASProgramVersion]', 'U'))
ALTER TABLE [dbo].[syApprovedNACCASProgramVersion] ADD CONSTRAINT [FK_syApprovedNACCASProgramVersion_arPrgVersions_PrgVerId_PrgVerId] FOREIGN KEY ([ProgramVersionId]) REFERENCES [dbo].[arPrgVersions] ([PrgVerId])
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_syApprovedNACCASProgramVersion_syUsers_CreatedById_UserId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[syApprovedNACCASProgramVersion]', 'U'))
ALTER TABLE [dbo].[syApprovedNACCASProgramVersion] ADD CONSTRAINT [FK_syApprovedNACCASProgramVersion_syUsers_CreatedById_UserId] FOREIGN KEY ([CreatedById]) REFERENCES [dbo].[syUsers] ([UserId])
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_syApprovedNACCASProgramVersion_syNaccasSettings_NaccasSettingId_NaccasSettingId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[syApprovedNACCASProgramVersion]', 'U'))
ALTER TABLE [dbo].[syApprovedNACCASProgramVersion] ADD CONSTRAINT [FK_syApprovedNACCASProgramVersion_syNaccasSettings_NaccasSettingId_NaccasSettingId] FOREIGN KEY ([NaccasSettingId]) REFERENCES [dbo].[syNaccasSettings] ([NaccasSettingId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[syCampuses]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_syCampuses_syCampuses_CampusId_ParentCampusId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[syCampuses]', 'U'))
ALTER TABLE [dbo].[syCampuses] ADD CONSTRAINT [FK_syCampuses_syCampuses_CampusId_ParentCampusId] FOREIGN KEY ([ParentCampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[syNACCASDropReasonsMapping]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__syNACCASD__NACCA__1348E796]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[syNACCASDropReasonsMapping]', 'U'))
ALTER TABLE [dbo].[syNACCASDropReasonsMapping] ADD CONSTRAINT [FK__syNACCASD__NACCA__1348E796] FOREIGN KEY ([NACCASDropReasonId]) REFERENCES [dbo].[syNACCASDropReasons] ([NACCASDropReasonId])
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__syNACCASD__ADVDr__1254C35D]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[syNACCASDropReasonsMapping]', 'U'))
ALTER TABLE [dbo].[syNACCASDropReasonsMapping] ADD CONSTRAINT [FK__syNACCASD__ADVDr__1254C35D] FOREIGN KEY ([ADVDropReasonId]) REFERENCES [dbo].[arDropReasons] ([DropReasonId])
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_syNACCASDropReasonsMapping_syNaccasSettings_NaccasSettingId_NaccasSettingId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[syNACCASDropReasonsMapping]', 'U'))
ALTER TABLE [dbo].[syNACCASDropReasonsMapping] ADD CONSTRAINT [FK_syNACCASDropReasonsMapping_syNaccasSettings_NaccasSettingId_NaccasSettingId] FOREIGN KEY ([NaccasSettingId]) REFERENCES [dbo].[syNaccasSettings] ([NaccasSettingId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[syNaccasSettings]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_syNaccasSettings_syCampuses_CampusId_CampusId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[syNaccasSettings]', 'U'))
ALTER TABLE [dbo].[syNaccasSettings] ADD CONSTRAINT [FK_syNaccasSettings_syCampuses_CampusId_CampusId] FOREIGN KEY ([CampusId]) REFERENCES [dbo].[syCampuses] ([CampusId])
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK__syNaccasS__ModUs__0E4F284F]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[syNaccasSettings]', 'U'))
ALTER TABLE [dbo].[syNaccasSettings] ADD CONSTRAINT [FK__syNaccasS__ModUs__0E4F284F] FOREIGN KEY ([ModUserId]) REFERENCES [dbo].[syUsers] ([UserId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[syPeriodsWorkDays]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_syPeriodsWorkDays_plWorkDays_WorkDayId_WorkDaysId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[syPeriodsWorkDays]', 'U'))
ALTER TABLE [dbo].[syPeriodsWorkDays] ADD CONSTRAINT [FK_syPeriodsWorkDays_plWorkDays_WorkDayId_WorkDaysId] FOREIGN KEY ([WorkDayId]) REFERENCES [dbo].[plWorkDays] ([WorkDaysId])
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_syPeriodsWorkDays_syPeriods_PeriodId_PeriodId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[syPeriodsWorkDays]', 'U'))
ALTER TABLE [dbo].[syPeriodsWorkDays] ADD CONSTRAINT [FK_syPeriodsWorkDays_syPeriods_PeriodId_PeriodId] FOREIGN KEY ([PeriodId]) REFERENCES [dbo].[syPeriods] ([PeriodId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[plEmployerJobs]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_plEmployerJobs_adTitles_JobTitleId_TitleId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[plEmployerJobs]', 'U'))
ALTER TABLE [dbo].[plEmployerJobs] ADD CONSTRAINT [FK_plEmployerJobs_adTitles_JobTitleId_TitleId] FOREIGN KEY ([JobTitleId]) REFERENCES [dbo].[adTitles] ([TitleId])
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_plEmployerJobs_plEmployers_EmployerId_EmployerId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[plEmployerJobs]', 'U'))
ALTER TABLE [dbo].[plEmployerJobs] ADD CONSTRAINT [FK_plEmployerJobs_plEmployers_EmployerId_EmployerId] FOREIGN KEY ([EmployerId]) REFERENCES [dbo].[plEmployers] ([EmployerId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END