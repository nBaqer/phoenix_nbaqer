BEGIN
    DECLARE @Error AS INTEGER;

    SET @Error = 0;

    BEGIN TRANSACTION IdentityFix;
    BEGIN TRY


        IF NOT EXISTS
        (
            SELECT OBJECT_NAME(object_id) AS TABLENAME,
                   name AS COLUMNNAME,
                   seed_value,
                   increment_value,
                   last_value,
                   is_not_for_replication
            FROM sys.identity_columns
            WHERE OBJECT_NAME(object_id) = 'syConfigAppSettings'
        )
        BEGIN

            SELECT *
            INTO #syConfigAppSettings
            FROM syConfigAppSettings;

            ALTER TABLE syConfigAppSettings
            DROP CONSTRAINT PK_syConfigAppSettings_SettingId;

            ALTER TABLE syConfigAppSettings DROP COLUMN SettingId;

            ALTER TABLE syConfigAppSettings
            ADD SettingId INT IDENTITY(1, 1) NOT NULL;

            SET IDENTITY_INSERT dbo.syConfigAppSettings ON;

            DELETE FROM syConfigAppSettings;

            INSERT INTO syConfigAppSettings
            (
                KeyName,
                Description,
                ModUser,
                ModDate,
                CampusSpecific,
                ExtraConfirmation,
                SettingId
            )
            SELECT KeyName,
                   Description,
                   ModUser,
                   ModDate,
                   CampusSpecific,
                   ExtraConfirmation,
                   SettingId
            FROM #syConfigAppSettings;


            SET IDENTITY_INSERT dbo.syConfigAppSettings OFF;

            DROP TABLE #syConfigAppSettings;


            ALTER TABLE [dbo].[syConfigAppSettings]
            ADD CONSTRAINT [PK_syConfigAppSettings_SettingId]
                PRIMARY KEY CLUSTERED ([SettingId] ASC)
                WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF,
                      ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON
                     ) ON [PRIMARY];


					  PRINT 'identity added to syConfigAppSettings';
        END;

		
		IF NOT EXISTS
		(
			SELECT *
			FROM sys.default_constraints
			WHERE name = 'DF_syConfigAppSetValues_ValueId'
		)
		BEGIN
			ALTER TABLE [dbo].[syConfigAppSetValues]
			ADD CONSTRAINT [DF_syConfigAppSetValues_ValueId]
				DEFAULT (NEWSEQUENTIALID()) FOR [ValueId];
		END;

        IF (@@ERROR > 0)
        BEGIN
            SET @Error = @@ERROR;
        END;

    END TRY
    BEGIN CATCH
        DECLARE @msg NVARCHAR(MAX);
        DECLARE @severity INT;
        DECLARE @state INT;
        SELECT @msg = ERROR_MESSAGE(),
               @severity = ERROR_SEVERITY(),
               @state = ERROR_STATE();
        RAISERROR(@msg, @severity, @state);
        SET @Error = 1;
    END CATCH;

    IF (@Error = 0)
    BEGIN
        COMMIT TRANSACTION IdentityFix;

        PRINT 'IdentityFix script for syConfigAppSettings was executed';
    END;
    ELSE
    BEGIN
        ROLLBACK TRANSACTION IdentityFix;
        PRINT 'Unable to add identity to syConfigAppSettings';
    END;
END;