/*
Run this script on:

        dev-com-db1\Adv.Tricoci    -  This database will be modified

to synchronize it with:

        dev2.internal.fameinc.com\A1.Prod_Aveda_41SP1

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.7.10021 from Red Gate Software Ltd at 7/23/2019 4:14:59 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[arStuEnrollments_Integration_Tracking] from [dbo].[arStuEnrollments]'
GO
IF OBJECT_ID(N'[dbo].[arStuEnrollments_Integration_Tracking]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[arStuEnrollments_Integration_Tracking]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[saDeferredRevenues_Audit_Delete] from [dbo].[saDeferredRevenues]'
GO
IF OBJECT_ID(N'[dbo].[saDeferredRevenues_Audit_Delete]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[saDeferredRevenues_Audit_Delete]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[saDeferredRevenues_Audit_Insert] from [dbo].[saDeferredRevenues]'
GO
IF OBJECT_ID(N'[dbo].[saDeferredRevenues_Audit_Insert]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[saDeferredRevenues_Audit_Insert]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[saDeferredRevenues_Audit_Update] from [dbo].[saDeferredRevenues]'
GO
IF OBJECT_ID(N'[dbo].[saDeferredRevenues_Audit_Update]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[saDeferredRevenues_Audit_Update]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[saDeferredRevenues_Audit_Insert] on [dbo].[saDeferredRevenues]'
GO
IF OBJECT_ID(N'[dbo].[saDeferredRevenues_Audit_Insert]', 'TR') IS NULL
EXEC sp_executesql N'
CREATE TRIGGER [dbo].[saDeferredRevenues_Audit_Insert] ON [dbo].[saDeferredRevenues]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,''saDeferredRevenues'',''I'',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,''Amount''
                               ,CONVERT(VARCHAR(8000),New.Amount,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,''TransactionId''
                               ,CONVERT(VARCHAR(8000),New.TransactionId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,''DefRevenueDate''
                               ,CONVERT(VARCHAR(8000),New.DefRevenueDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,''Source''
                               ,CONVERT(VARCHAR(8000),New.Source,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,''Type''
                               ,CONVERT(VARCHAR(8000),New.Type,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,''IsPosted''
                               ,CONVERT(VARCHAR(8000),New.IsPosted,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DISABLE TRIGGER [dbo].[saDeferredRevenues_Audit_Insert] ON [dbo].[saDeferredRevenues]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[saDeferredRevenues_Audit_Update] on [dbo].[saDeferredRevenues]'
GO
IF OBJECT_ID(N'[dbo].[saDeferredRevenues_Audit_Update]', 'TR') IS NULL
EXEC sp_executesql N'
CREATE TRIGGER [dbo].[saDeferredRevenues_Audit_Update] ON [dbo].[saDeferredRevenues]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,''saDeferredRevenues'',''U'',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(Amount)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,''Amount''
                                   ,CONVERT(VARCHAR(8000),Old.Amount,121)
                                   ,CONVERT(VARCHAR(8000),New.Amount,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.Amount <> New.Amount
                                    OR (
                                         Old.Amount IS NULL
                                         AND New.Amount IS NOT NULL
                                       )
                                    OR (
                                         New.Amount IS NULL
                                         AND Old.Amount IS NOT NULL
                                       ); 
                IF UPDATE(TransactionId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,''TransactionId''
                                   ,CONVERT(VARCHAR(8000),Old.TransactionId,121)
                                   ,CONVERT(VARCHAR(8000),New.TransactionId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.TransactionId <> New.TransactionId
                                    OR (
                                         Old.TransactionId IS NULL
                                         AND New.TransactionId IS NOT NULL
                                       )
                                    OR (
                                         New.TransactionId IS NULL
                                         AND Old.TransactionId IS NOT NULL
                                       ); 
                IF UPDATE(DefRevenueDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,''DefRevenueDate''
                                   ,CONVERT(VARCHAR(8000),Old.DefRevenueDate,121)
                                   ,CONVERT(VARCHAR(8000),New.DefRevenueDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.DefRevenueDate <> New.DefRevenueDate
                                    OR (
                                         Old.DefRevenueDate IS NULL
                                         AND New.DefRevenueDate IS NOT NULL
                                       )
                                    OR (
                                         New.DefRevenueDate IS NULL
                                         AND Old.DefRevenueDate IS NOT NULL
                                       ); 
                IF UPDATE(Source)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,''Source''
                                   ,CONVERT(VARCHAR(8000),Old.Source,121)
                                   ,CONVERT(VARCHAR(8000),New.Source,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.Source <> New.Source
                                    OR (
                                         Old.Source IS NULL
                                         AND New.Source IS NOT NULL
                                       )
                                    OR (
                                         New.Source IS NULL
                                         AND Old.Source IS NOT NULL
                                       ); 
                IF UPDATE(Type)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,''Type''
                                   ,CONVERT(VARCHAR(8000),Old.Type,121)
                                   ,CONVERT(VARCHAR(8000),New.Type,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.Type <> New.Type
                                    OR (
                                         Old.Type IS NULL
                                         AND New.Type IS NOT NULL
                                       )
                                    OR (
                                         New.Type IS NULL
                                         AND Old.Type IS NOT NULL
                                       ); 
                IF UPDATE(IsPosted)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,''IsPosted''
                                   ,CONVERT(VARCHAR(8000),Old.IsPosted,121)
                                   ,CONVERT(VARCHAR(8000),New.IsPosted,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.IsPosted <> New.IsPosted
                                    OR (
                                         Old.IsPosted IS NULL
                                         AND New.IsPosted IS NOT NULL
                                       )
                                    OR (
                                         New.IsPosted IS NULL
                                         AND Old.IsPosted IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DISABLE TRIGGER [dbo].[saDeferredRevenues_Audit_Update] ON [dbo].[saDeferredRevenues]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[arStuEnrollments_Integration_Tracking] on [dbo].[arStuEnrollments]'
GO
IF OBJECT_ID(N'[dbo].[arStuEnrollments_Integration_Tracking]', 'TR') IS NULL
EXEC sp_executesql N'--BEGIN	
--CREATE TABLE dbo.IntegrationEnrollmentTracking
--(
-- EnrollmentTrackingId UNIQUEIDENTIFIER CONSTRAINT [PK_IntegrationEnrollmentTracking_EnrollmentTrackingId] PRIMARY KEY CLUSTERED 
-- CONSTRAINT [DF_IntegrationEnrollmentTracking_EnrollmentTrackingId] DEFAULT NEWID(),
-- StuEnrollId UNIQUEIDENTIFIER CONSTRAINT[FK_IntegrationEnrollmentTracking_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY REFERENCES dbo.arStuEnrollments NOT NULL,
--  EnrollmentStatus VARCHAR(100),
-- ModDate DATETIME NOT NULL ,
-- ModUser NVARCHAR(100),
-- Processed BIT CONSTRAINT [DF_IntegrationEnrollmentTracking_Processed] DEFAULT 0 NOT NULL 	

--)
--END
--DROP TABLE dbo.IntegrationEnrollmentTracking
CREATE TRIGGER [dbo].[arStuEnrollments_Integration_Tracking]
ON [dbo].[arStuEnrollments]
AFTER UPDATE
AS
DECLARE @INS INT
       ,@DEL INT;

SELECT @INS = COUNT(*)
FROM   INSERTED;
SELECT @DEL = COUNT(*)
FROM   DELETED;

IF @INS > 0
   AND @DEL > 0
    BEGIN
	SET NOCOUNT ON; 

        IF UPDATE(StatusCodeId)
           OR UPDATE(LDA)
            BEGIN
                IF EXISTS (
                          SELECT     1
                          FROM       INSERTED i
                          INNER JOIN DELETED d ON d.StuEnrollId = i.StuEnrollId
                          WHERE      ( i.StatusCodeId <> d.StatusCodeId )
                                     OR ( i.LDA <> d.LDA )
                          )
                    BEGIN
                        IF EXISTS (
                                  SELECT     1
                                  FROM       dbo.IntegrationEnrollmentTracking IET
                                  INNER JOIN inserted i ON i.StuEnrollId = IET.StuEnrollId
                                  )
                            BEGIN


                                UPDATE     IET
                                SET        EnrollmentStatus = ss.SysStatusDescrip
                                          ,ModDate = i.ModDate
                                          ,ModUser = i.ModUser
                                          ,Processed = 0
                                FROM       dbo.IntegrationEnrollmentTracking IET
                                INNER JOIN INSERTED i ON i.StuEnrollId = IET.StuEnrollId
                                INNER JOIN dbo.arStuEnrollments se ON i.StuEnrollId = se.StuEnrollId
                                INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                INNER JOIN dbo.sySysStatus ss ON ss.SysStatusId = sc.SysStatusId
                                INNER JOIN dbo.adLeads l ON l.LeadId = se.LeadId
                                WHERE IET.StuEnrollId = i.StuEnrollId AND l.AfaStudentId IS NOT NULL
                            END;

                        ELSE
                            BEGIN

                                INSERT INTO dbo.IntegrationEnrollmentTracking (
                                                                              EnrollmentTrackingId
                                                                             ,StuEnrollId
                                                                             ,EnrollmentStatus
                                                                             ,ModDate
                                                                             ,ModUser
                                                                             ,Processed
                                                                              )
                                            SELECT     NEWID()
                                                      ,i.StuEnrollId
                                                      ,ss.SysStatusDescrip
                                                      ,i.ModDate
                                                      ,i.ModUser
                                                      ,0
                                            FROM       INSERTED i
                                            INNER JOIN dbo.arStuEnrollments se ON i.StuEnrollId = se.StuEnrollId
                                            INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                            INNER JOIN dbo.sySysStatus ss ON ss.SysStatusId = sc.SysStatusId
											INNER JOIN dbo.adLeads l ON l.LeadId = se.LeadId
											WHERE l.AfaStudentId IS NOT NULL

                            END;


                    END;

            END;

SET NOCOUNT OFF; 

    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[saDeferredRevenues_Audit_Delete] on [dbo].[saDeferredRevenues]'
GO
IF OBJECT_ID(N'[dbo].[saDeferredRevenues_Audit_Delete]', 'TR') IS NULL
EXEC sp_executesql N'
CREATE TRIGGER [dbo].[saDeferredRevenues_Audit_Delete] ON [dbo].[saDeferredRevenues]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,''saDeferredRevenues'',''D'',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,''Amount''
                               ,CONVERT(VARCHAR(8000),Old.Amount,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,''TransactionId''
                               ,CONVERT(VARCHAR(8000),Old.TransactionId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,''DefRevenueDate''
                               ,CONVERT(VARCHAR(8000),Old.DefRevenueDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,''Source''
                               ,CONVERT(VARCHAR(8000),Old.Source,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,''Type''
                               ,CONVERT(VARCHAR(8000),Old.Type,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,''IsPosted''
                               ,CONVERT(VARCHAR(8000),Old.IsPosted,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DISABLE TRIGGER [dbo].[saDeferredRevenues_Audit_Delete] ON [dbo].[saDeferredRevenues]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[NewStudentSearch]'
GO
IF OBJECT_ID(N'[dbo].[NewStudentSearch]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[NewStudentSearch]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[GlTransView1]'
GO
IF OBJECT_ID(N'[dbo].[GlTransView1]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[GlTransView1]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[GlTransView2]'
GO
IF OBJECT_ID(N'[dbo].[GlTransView2]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[GlTransView2]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[SettingStdScoreLimit]'
GO
IF OBJECT_ID(N'[dbo].[SettingStdScoreLimit]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[SettingStdScoreLimit]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[StudentEnrollmentSearch]'
GO
IF OBJECT_ID(N'[dbo].[StudentEnrollmentSearch]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[StudentEnrollmentSearch]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Enabling constraints on [dbo].[saDeferredRevenues]'
GO
IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_saDeferredRevenues_saTransactions_TransactionId_TransactionId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[saDeferredRevenues]', 'U'))
ALTER TABLE [dbo].[saDeferredRevenues] NOCHECK CONSTRAINT [FK_saDeferredRevenues_saTransactions_TransactionId_TransactionId]
GO
ALTER TABLE [dbo].[saDeferredRevenues] CHECK CONSTRAINT [FK_saDeferredRevenues_saTransactions_TransactionId_TransactionId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END