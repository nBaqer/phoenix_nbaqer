﻿--=================================================================================================
-- START AbsentHours field to Adhoc AD-15849
--=================================================================================================

DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION AbsentHours;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Decimal'
    );
    IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'AbsentHours')
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,        -- FldId - int
            'AbsentHours', -- FldName - varchar(200)
            @fldTypeId,    -- FldTypeId - int
            10,            -- FldLen - int
            NULL,          -- DDLId - int
            NULL,          -- DerivedFld - bit
            NULL,          -- SchlReq - bit
            NULL,          -- LogChanges - bit
            NULL           -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Enrollment'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,                                  -- FldCapId - int
            @fldId,                                     -- FldId - int
            1,                                          -- LangId - tinyint
            'Absent Hours',                            -- Caption - varchar(100)
            'Total Absent Hours' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            NULL         -- FKColDescrip - varchar(50)
            );
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @fldId,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           -- FldId - int
            '     (SELECT ISNULL((SUM(SchedHours) - sum(ActualHours)),0) AS AbsentHours FROM dbo.arStudentClockAttendance WHERE SchedHours NOT IN (9999.00) AND ActualHours NOT IN (9999.00) AND (ActualHours < SchedHours OR ActualHours = SchedHours ) AND  StuEnrollId = arStuEnrollments.StuEnrollId) AS AbsentHours  ' -- CalculationSql - varchar(8000)
            );
    END;
    ELSE
    BEGIN
        DECLARE @tdFldId INT;
        SET @tdFldId =
        (
            SELECT FldId FROM syFields WHERE FldName = 'AbsentHours'
        );
        UPDATE syFieldCalculation
        SET CalculationSql = '     (SELECT ISNULL((SUM(SchedHours) - sum(ActualHours)),0) AS AbsentHours FROM dbo.arStudentClockAttendance WHERE SchedHours NOT IN (9999.00) AND ActualHours NOT IN (9999.00) AND (ActualHours < SchedHours OR ActualHours = SchedHours ) AND  StuEnrollId = arStuEnrollments.StuEnrollId) AS AbsentHours   '
        WHERE FldId = @tdFldId;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION AbsentHours;
    PRINT 'Failed to Add AbsentHours to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION AbsentHours;
    PRINT 'Added AbsentHours to Adhoc Report';
END;
GO
--=================================================================================================
-- END  Add AbsentHours field to Adhoc AD-15849
--=================================================================================================


--=================================================================================================
-- START MakeupHours field to Adhoc AD-15849
--=================================================================================================

DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION AbsentHours;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Decimal'
    );
    IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'MakeupHours')
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,        -- FldId - int
            'MakeupHours', -- FldName - varchar(200)
            @fldTypeId,    -- FldTypeId - int
            10,            -- FldLen - int
            NULL,          -- DDLId - int
            NULL,          -- DerivedFld - bit
            NULL,          -- SchlReq - bit
            NULL,          -- LogChanges - bit
            NULL           -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Enrollment'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,                                  -- FldCapId - int
            @fldId,                                     -- FldId - int
            1,                                          -- LangId - tinyint
            'Make-up Hours',                            -- Caption - varchar(100)
            'Total Make-up Hours' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            NULL         -- FKColDescrip - varchar(50)
            );
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @fldId,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           -- FldId - int
            '     (SELECT ISNULL((SUM(ActualHours) - sum(SchedHours)),0) AS Makeup FROM dbo.arStudentClockAttendance WHERE SchedHours NOT IN (9999.00) AND ActualHours NOT IN (9999.00) AND ActualHours > SchedHours AND StuEnrollId =  arStuEnrollments.StuEnrollId) AS MakeupHours  ' -- CalculationSql - varchar(8000)
            );
    END;
    ELSE
    BEGIN
        DECLARE @tdFldId INT;
        SET @tdFldId =
        (
            SELECT FldId FROM syFields WHERE FldName = 'MakeupHours'
        );
        UPDATE syFieldCalculation
        SET CalculationSql = '     (SELECT ISNULL((SUM(ActualHours) - sum(SchedHours)),0) AS Makeup FROM dbo.arStudentClockAttendance WHERE SchedHours NOT IN (9999.00) AND ActualHours NOT IN (9999.00) AND ActualHours > SchedHours AND StuEnrollId =  arStuEnrollments.StuEnrollId) AS MakeupHours   '
        WHERE FldId = @tdFldId;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION MakeupHours;
    PRINT 'Failed to Add MakeupHours to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION AbsentHours;
    PRINT 'Added MakeupHours to Adhoc Report';
END;
--=================================================================================================
-- END  Add MakeupHours field to Adhoc AD-15849
--=================================================================================================