﻿--=================================================================================================
-- START AD-12633 Enable export to excel for Attendance Summary report.
--=================================================================================================
DECLARE @ErrorAllowExportTypeExcelForAttSummReport AS INT;
SET @ErrorAllowExportTypeExcelForAttSummReport = 0;
BEGIN TRANSACTION AllowExportExcelForAttSummRep;
BEGIN TRY
    UPDATE syReports
    SET    AllowedExportTypes = 4
    WHERE  ResourceId = 877;
END TRY
BEGIN CATCH
    SET @ErrorAllowExportTypeExcelForAttSummReport = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @ErrorAllowExportTypeExcelForAttSummReport > 0
    BEGIN
        ROLLBACK TRANSACTION AllowExportExcelForAttSummRep;
        PRINT 'Failed transcation AllowExportTypeExcelForAttSummReport.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION AllowExportExcelForAttSummRep;
        PRINT 'successful transaction AllowExportTypeExcelForAttSummReport.';
    END;
GO
--=================================================================================================
-- END AD-12633 Enable export to excel for Attendance Summary report.
--=================================================================================================
--=================================================================================================
-- START AD-13246 : Beta 4.0: Back Fill Script for Date Completed
--=================================================================================================
DECLARE @ErrorUpdate AS INT;
SET @ErrorUpdate = 0;
BEGIN TRANSACTION DateCompleteUpdate;
BEGIN TRY
    IF EXISTS (
              SELECT 1
              FROM   dbo.arResults
              WHERE  GrdSysDetailId IS NOT NULL
                     AND (DateCompleted IS NULL or CAST(DateCompleted AS DATE) = '1/1/1900')
              )
        BEGIN
			ALTER TABLE dbo.arResults DISABLE TRIGGER ALL;
 
            UPDATE     r
            SET        r.DateCompleted = CASE WHEN r.ModDate IS NULL THEN e.ContractedGradDate
                                              ELSE CASE WHEN r.ModDate
                                                             BETWEEN e.StartDate AND e.ExpGradDate THEN r.ModDate
                                                        ELSE e.ExpGradDate
                                                   END
                                         END
            FROM       dbo.arResults r
            INNER JOIN dbo.arStuEnrollments e ON e.StuEnrollId = r.StuEnrollId
            WHERE      GrdSysDetailId IS NOT NULL
                       AND (DateCompleted IS NULL or CAST(DateCompleted AS DATE) = '1/1/1900')
					   
			ALTER TABLE dbo.arResults ENABLE TRIGGER ALL;
        END;

END TRY
BEGIN CATCH
    SET @ErrorUpdate = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @ErrorUpdate > 0
    BEGIN
        ROLLBACK TRANSACTION DateCompleteUpdate;
        PRINT 'Failed transcation DateCompleteUpdate.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION DateCompleteUpdate;
        PRINT 'successful transaction DateCompleteUpdate.';
    END;
GO
--=================================================================================================
-- END AD-13246 : Beta 4.0: Back Fill Script for Date Completed
--=================================================================================================
--=================================================================================================
-- START AD-13507 Backfill Script for Historical Component Dates
--=================================================================================================
DECLARE @Error AS INT;
SET @Error = 0;
BEGIN TRANSACTION GrdBkResultsDateComp;
BEGIN TRY
	DECLARE @exists BIGINT = (SELECT COUNT(*) FROM   dbo.arGrdBkResults r	WHERE  r.DateCompleted IS NULL AND r.score IS NOT NULL)
	IF (@exists > 0)
	BEGIN
		UPDATE dbo.arGrdBkResults 
		SET DateCompleted =  CASE WHEN arGrdBkResults.ModDate < EndDate
										AND arGrdBkResults.ModDate < ExpGradDate THEN dbo.arGrdBkResults.ModDate
									WHEN arGrdBkResults.ModDate > EndDate
										AND EndDate < ExpGradDate THEN EndDate
									WHEN arGrdBkResults.ModDate > EndDate
										AND EndDate > ExpGradDate THEN ExpGradDate
									ELSE ExpGradDate
								END
		FROM   dbo.arGrdBkResults
		JOIN   dbo.arClassSections ON arClassSections.ClsSectionId = arGrdBkResults.ClsSectionId
		JOIN   dbo.arStuEnrollments ON arStuEnrollments.StuEnrollId = arGrdBkResults.StuEnrollId
		WHERE  DateCompleted IS NULL AND score IS NOT NULL;
	END
 END TRY
BEGIN CATCH
    SET @Error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN		
        ROLLBACK TRANSACTION GrdBkResultsDateComp;
        PRINT 'Failed transaction GrdBkResultsDateComp.';
    END;
ELSE
    BEGIN
        COMMIT TRANSACTION GrdBkResultsDateComp;
        PRINT 'successful transaction GrdBkResultsDateComp.';
    END;
GO
--=================================================================================================
-- END AD-13507 Backfill Script for Historical Component Dates
--=================================================================================================
--========================================================================================================================
-- AD-14097-Time Clock Punch Rounding
--========================================================================================================================

DECLARE @KeyName VARCHAR(50) = 'HoursRoundingMethod';
DECLARE @KeyDescription VARCHAR(300) = 'Rounding method for time clock punches.';

IF NOT EXISTS (
              SELECT *
              FROM   syConfigAppSettings
              WHERE  KeyName = @KeyName
              )
    BEGIN TRY
        BEGIN
            BEGIN TRANSACTION TCRounding;

            DECLARE @MaxId INT;

            INSERT INTO dbo.syConfigAppSettings (
                                                KeyName
                                               ,Description
                                               ,ModUser
                                               ,ModDate
                                               ,CampusSpecific
                                               ,ExtraConfirmation
                                                )
            VALUES ( @KeyName        -- KeyName - varchar(200)
                    ,@KeyDescription -- Description - varchar(1000)
                    ,'Support'       -- ModUser - varchar(50)
                    ,GETDATE()       -- ModDate - datetime
                    ,1               -- CampusSpecific - bit
                    ,0               -- ExtraConfirmation - bit
                );

            SET @MaxId = (
                         SELECT   TOP ( 1 ) SettingId
                         FROM     syConfigAppSettings
                         WHERE    KeyName = @KeyName
                         ORDER BY KeyName
                         );



            INSERT INTO dbo.syConfigAppSetValues (
                                                 ValueId
                                                ,SettingId
                                                ,CampusId
                                                ,Value
                                                ,ModUser
                                                ,ModDate
                                                ,Active
                                                 )
            VALUES ( NEWID()   -- ValueId - uniqueidentifier
                    ,@MaxId    -- SettingId - int
                    ,NULL      -- CampusId - uniqueidentifier
                    ,'None'    -- Value - varchar(1000)
                    ,'Support' -- ModUser - varchar(50)
                    ,GETDATE() -- ModDate - datetime
                    ,1         -- Active - bit
                );

            INSERT INTO dbo.syConfigAppSet_Lookup (
                                                  LookUpId
                                                 ,SettingId
                                                 ,ValueOptions
                                                 ,ModUser
                                                 ,ModDate
                                                  )
            VALUES ( NEWID()   -- LookUpId - uniqueidentifier
                    ,@MaxId    -- SettingId - int
                    ,'None'    -- ValueOptions - varchar(50)
                    ,'Support' -- ModUser - varchar(50)
                    ,GETDATE() -- ModDate - datetime
                );

            INSERT INTO dbo.syConfigAppSet_Lookup (
                                                  LookUpId
                                                 ,SettingId
                                                 ,ValueOptions
                                                 ,ModUser
                                                 ,ModDate
                                                  )
            VALUES ( NEWID()   -- LookUpId - uniqueidentifier
                    ,@MaxId    -- SettingId - int
                    ,'Hour'    -- ValueOptions - varchar(50)
                    ,'Support' -- ModUser - varchar(50)
                    ,GETDATE() -- ModDate - datetime
                );

            INSERT INTO dbo.syConfigAppSet_Lookup (
                                                  LookUpId
                                                 ,SettingId
                                                 ,ValueOptions
                                                 ,ModUser
                                                 ,ModDate
                                                  )
            VALUES ( NEWID()    -- LookUpId - uniqueidentifier
                    ,@MaxId     -- SettingId - int
                    ,'HalfHour' -- ValueOptions - varchar(50)
                    ,'Support'  -- ModUser - varchar(50)
                    ,GETDATE()  -- ModDate - datetime
                );

            INSERT INTO dbo.syConfigAppSet_Lookup (
                                                  LookUpId
                                                 ,SettingId
                                                 ,ValueOptions
                                                 ,ModUser
                                                 ,ModDate
                                                  )
            VALUES ( NEWID()       -- LookUpId - uniqueidentifier
                    ,@MaxId        -- SettingId - int
                    ,'QuarterHour' -- ValueOptions - varchar(50)
                    ,'Support'     -- ModUser - varchar(50)
                    ,GETDATE()     -- ModDate - datetime
                );

            INSERT INTO dbo.syConfigAppSet_Lookup (
                                                  LookUpId
                                                 ,SettingId
                                                 ,ValueOptions
                                                 ,ModUser
                                                 ,ModDate
                                                  )
            VALUES ( NEWID()      -- LookUpId - uniqueidentifier
                    ,@MaxId       -- SettingId - int
                    ,'TenMinutes' -- ValueOptions - varchar(50)
                    ,'Support'    -- ModUser - varchar(50)
                    ,GETDATE()    -- ModDate - datetime
                );

            INSERT INTO dbo.syConfigAppSet_Lookup (
                                                  LookUpId
                                                 ,SettingId
                                                 ,ValueOptions
                                                 ,ModUser
                                                 ,ModDate
                                                  )
            VALUES ( NEWID()       -- LookUpId - uniqueidentifier
                    ,@MaxId        -- SettingId - int
                    ,'FiveMinutes' -- ValueOptions - varchar(50)
                    ,'Support'     -- ModUser - varchar(50)
                    ,GETDATE()     -- ModDate - datetime
                );

            INSERT INTO dbo.syConfigAppSet_Lookup (
                                                  LookUpId
                                                 ,SettingId
                                                 ,ValueOptions
                                                 ,ModUser
                                                 ,ModDate
                                                  )
            VALUES ( NEWID()   -- LookUpId - uniqueidentifier
                    ,@MaxId    -- SettingId - int
                    ,'Minute'  -- ValueOptions - varchar(50)
                    ,'Support' -- ModUser - varchar(50)
                    ,GETDATE() -- ModDate - datetime
                );

        END;
    END TRY
    BEGIN CATCH
        SELECT ERROR_NUMBER() AS ErrorNumber;
        SELECT ERROR_SEVERITY() AS ErrorSeverity;
        SELECT ERROR_STATE() AS ErrorState;
        SELECT ERROR_PROCEDURE() AS ErrorProcedure;
        SELECT ERROR_LINE() AS ErrorLine;
        SELECT ERROR_MESSAGE() AS ErrorMessage;

        IF @@TRANCOUNT > 0
            BEGIN
                ROLLBACK TRANSACTION TCRounding;
                PRINT 'Exception Ocurred!';
            END;
    END CATCH;

IF @@TRANCOUNT > 0
    BEGIN
        COMMIT TRANSACTION TCRounding;
    END;
--===============================================================================================================================
-- END --  AD-14097-Time Clock Punch Rounding
--===============================================================================================================================