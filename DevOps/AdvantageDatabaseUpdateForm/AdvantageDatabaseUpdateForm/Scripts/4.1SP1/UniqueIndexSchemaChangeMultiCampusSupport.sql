IF EXISTS
(
    SELECT *
    FROM sys.indexes
    WHERE name = 'UIX_plEmployers_Code_EmployerDescrip'
          AND object_id = OBJECT_ID('[dbo].[plEmployers]')
)
BEGIN
    DROP INDEX [UIX_plEmployers_Code_EmployerDescrip] ON [dbo].[plEmployers];
END;
GO

SET ANSI_PADDING ON;
GO

IF NOT EXISTS
(
    SELECT *
    FROM sys.indexes
    WHERE name = 'UIX_plEmployers_Code_EmployerDescrip_CampGrpId'
          AND object_id = OBJECT_ID('[dbo].[plEmployers]')
)
BEGIN
    CREATE UNIQUE NONCLUSTERED INDEX [UIX_plEmployers_Code_EmployerDescrip_CampGrpId]
    ON [dbo].[plEmployers] (
                               [Code] ASC,
                               [EmployerDescrip] ASC,
                               [CampGrpId] ASC
                           )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF,
          DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON
         )
    ON [PRIMARY];
END;
GO


IF EXISTS
(
    SELECT *
    FROM sys.indexes
    WHERE name = 'UIX_plEmployerJobs_Code_EmployerJobTitle_JobTitleId_EmployerId'
          AND object_id = OBJECT_ID('[dbo].[plEmployerJobs]')
)
BEGIN
    DROP INDEX [UIX_plEmployerJobs_Code_EmployerJobTitle_JobTitleId_EmployerId]
    ON [dbo].[plEmployerJobs];
END;
GO

SET ANSI_PADDING ON;
GO
IF NOT EXISTS
(
    SELECT *
    FROM sys.indexes
    WHERE name = 'UIX_plEmployerJobs_Code_EmployerJobTitle_JobTitleId_EmployerId_CampGrpId'
          AND object_id = OBJECT_ID('[dbo].[plEmployerJobs]')
)
BEGIN
    CREATE UNIQUE NONCLUSTERED INDEX [UIX_plEmployerJobs_Code_EmployerJobTitle_JobTitleId_EmployerId_CampGrpId]
    ON [dbo].[plEmployerJobs] (
                                  [Code] ASC,
                                  [EmployerJobTitle] ASC,
                                  [JobTitleId] ASC,
                                  [EmployerId] ASC,
                                  [CampGrpId] ASC
                              )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF,
          DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON
         )
    ON [PRIMARY];
END;

GO
IF EXISTS
(
    SELECT *
    FROM sys.indexes
    WHERE name = 'UIX_faLenders_Code_LenderDescrip_PrimaryContact'
          AND object_id = OBJECT_ID('[dbo].[faLenders]')
)
BEGIN
    DROP INDEX [UIX_faLenders_Code_LenderDescrip_PrimaryContact]
    ON [dbo].[faLenders];
END;
GO

SET ANSI_PADDING ON;
GO
IF NOT EXISTS
(
    SELECT *
    FROM sys.indexes
    WHERE name = 'UIX_faLenders_Code_LenderDescrip_PrimaryContact_CampGrpId'
          AND object_id = OBJECT_ID('[dbo].[faLenders]')
)
BEGIN
    CREATE UNIQUE NONCLUSTERED INDEX [UIX_faLenders_Code_LenderDescrip_PrimaryContact_CampGrpId]
    ON [dbo].[faLenders] (
                             [Code] ASC,
                             [LenderDescrip] ASC,
                             [PrimaryContact] ASC,
                             [CampGrpId] ASC
                         )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF,
          DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON
         )
    ON [PRIMARY];
END;
GO
IF EXISTS
(
    SELECT *
    FROM sys.indexes
    WHERE name = 'UIX_arPrograms_ProgCode_ProgDescrip'
          AND object_id = OBJECT_ID('[dbo].[arPrograms]')
)
BEGIN
    DROP INDEX [UIX_arPrograms_ProgCode_ProgDescrip] ON [dbo].[arPrograms];
END;
GO

SET ANSI_PADDING ON;
GO
IF NOT EXISTS
(
    SELECT *
    FROM sys.indexes
    WHERE name = 'UIX_arPrograms_ProgCode_ProgDescrip_CampGrpId'
          AND object_id = OBJECT_ID('[dbo].[arPrograms]')
)
BEGIN
    CREATE UNIQUE NONCLUSTERED INDEX [UIX_arPrograms_ProgCode_ProgDescrip_CampGrpId]
    ON [dbo].[arPrograms] (
                              [ProgCode] ASC,
                              [ProgDescrip] ASC,
                              [CampGrpId] ASC
                          )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF,
          DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON
         )
    ON [PRIMARY];
END;
GO
IF EXISTS
(
    SELECT *
    FROM sys.indexes
    WHERE name = 'UIX_arPrgGrp_PrgGrpCode_PrgGrpDescrip'
          AND object_id = OBJECT_ID('[dbo].[arPrgGrp]')
)
BEGIN
    DROP INDEX [UIX_arPrgGrp_PrgGrpCode_PrgGrpDescrip] ON [dbo].[arPrgGrp];
END;
GO

SET ANSI_PADDING ON;
GO
IF NOT EXISTS
(
    SELECT *
    FROM sys.indexes
    WHERE name = 'UIX_arPrograms_ProgCode_ProgDescrip_CampGrpId'
          AND object_id = OBJECT_ID('[dbo].[arPrgGrp]')
)
BEGIN
    CREATE UNIQUE NONCLUSTERED INDEX [UIX_arPrograms_ProgCode_ProgDescrip_CampGrpId]
    ON [dbo].[arPrgGrp] (
                            [PrgGrpCode] ASC,
                            [PrgGrpDescrip] ASC,
                            [CampGrpId] ASC
                        )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF,
          DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON
         )
    ON [PRIMARY];
END;
GO

IF EXISTS
(
    SELECT *
    FROM sys.indexes
    WHERE name = 'UIX_adSourceCatagory_SourceCatagoryCode_SourceCatagoryDescrip'
          AND object_id = OBJECT_ID('[dbo].[adSourceCatagory]')
)
BEGIN
    DROP INDEX [UIX_adSourceCatagory_SourceCatagoryCode_SourceCatagoryDescrip]
    ON [dbo].[adSourceCatagory];
END;
GO

SET ANSI_PADDING ON;
GO
IF NOT EXISTS
(
    SELECT *
    FROM sys.indexes
    WHERE name = 'UIX_adSourceCatagory_SourceCatagoryCode_SourceCatagoryDescrip_CampGrpId'
          AND object_id = OBJECT_ID('[dbo].[adSourceCatagory]')
)
BEGIN
    CREATE UNIQUE NONCLUSTERED INDEX [UIX_adSourceCatagory_SourceCatagoryCode_SourceCatagoryDescrip_CampGrpId]
    ON [dbo].[adSourceCatagory] (
                                    [SourceCatagoryCode] ASC,
                                    [SourceCatagoryDescrip] ASC,
                                    [CampGrpId] ASC
                                )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF,
          DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON
         )
    ON [PRIMARY];
END;
GO

IF EXISTS
(
    SELECT *
    FROM sys.indexes
    WHERE name = 'UIX_adReqs_Code_Descrip'
          AND object_id = OBJECT_ID('[dbo].[adReqs]')
)
BEGIN
    DROP INDEX [UIX_adReqs_Code_Descrip] ON [dbo].[adReqs];
END;
GO

SET ANSI_PADDING ON;
GO

IF NOT EXISTS
(
    SELECT *
    FROM sys.indexes
    WHERE name = 'UIX_adReqs_Code_Descrip_CampGrpId'
          AND object_id = OBJECT_ID('[dbo].[adReqs]')
)
BEGIN
    CREATE UNIQUE NONCLUSTERED INDEX [UIX_adReqs_Code_Descrip_CampGrpId]
    ON [dbo].[adReqs] (
                          [Code] ASC,
                          [Descrip] ASC,
                          [CampGrpId] ASC
                      )
    WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF,
          DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON
         )
    ON [PRIMARY];
END;
GO

