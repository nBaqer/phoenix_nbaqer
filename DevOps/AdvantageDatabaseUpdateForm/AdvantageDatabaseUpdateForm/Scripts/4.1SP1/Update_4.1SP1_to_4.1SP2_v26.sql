﻿/*
Run this script on:

        dev-com-db1.dev.fameinc.com\adv.AvedaLive    -  This database will be modified

to synchronize it with a database with the schema represented by:

        Source

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.7.10021 from Red Gate Software Ltd at 6/10/2019 1:58:22 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
/*
* Use this Pre-Deployment script to perform tasks before the deployment of the project.
* Read more at https://www.red-gate.com/SOC7/pre-deployment-script-help
*/
UPDATE dbo.arClsSectMeetings
SET PeriodId = NULL
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)

DELETE FROM syPeriodsWorkDays
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[TR_InsertAttendanceSummary_ByClockHour_Minutes] from [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[TR_InsertAttendanceSummary_ByClockHour_Minutes]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[TR_InsertAttendanceSummary_ByClockHour_Minutes]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[TR_InsertAttendanceSummary_ByDay_PA] from [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[TR_InsertAttendanceSummary_ByDay_PA]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[TR_InsertAttendanceSummary_ByDay_PA]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[syCampuses_Audit_Delete] from [dbo].[syCampuses]'
GO
IF OBJECT_ID(N'[dbo].[syCampuses_Audit_Delete]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[syCampuses_Audit_Delete]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[syCampuses_Audit_Insert] from [dbo].[syCampuses]'
GO
IF OBJECT_ID(N'[dbo].[syCampuses_Audit_Insert]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[syCampuses_Audit_Insert]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[syCampuses_Audit_Update] from [dbo].[syCampuses]'
GO
IF OBJECT_ID(N'[dbo].[syCampuses_Audit_Update]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[syCampuses_Audit_Update]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[syCampuses_KlassApps_Delete] from [dbo].[syCampuses]'
GO
IF OBJECT_ID(N'[dbo].[syCampuses_KlassApps_Delete]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[syCampuses_KlassApps_Delete]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[syCampuses_KlassApps_Insert] from [dbo].[syCampuses]'
GO
IF OBJECT_ID(N'[dbo].[syCampuses_KlassApps_Insert]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[syCampuses_KlassApps_Insert]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[syCampuses_KlassApps_Update] from [dbo].[syCampuses]'
GO
IF OBJECT_ID(N'[dbo].[syCampuses_KlassApps_Update]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[syCampuses_KlassApps_Update]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_FA_PostZerosForStudents]'
GO
IF OBJECT_ID(N'[dbo].[USP_FA_PostZerosForStudents]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_FA_PostZerosForStudents]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[StudentScheduledHours]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StudentScheduledHours]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[StudentScheduledHours]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_AR_GetSystemTransCodesRefunds_GetList]'
GO
IF OBJECT_ID(N'[dbo].[USP_AR_GetSystemTransCodesRefunds_GetList]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_AR_GetSystemTransCodesRefunds_GetList]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_TR_Sub11_ComponentsByCourse_GivenComponentTypeId]'
GO
IF OBJECT_ID(N'[dbo].[Usp_TR_Sub11_ComponentsByCourse_GivenComponentTypeId]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_TR_Sub11_ComponentsByCourse_GivenComponentTypeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_TR_Sub03_StudentInfo]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub03_StudentInfo]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_TR_Sub03_StudentInfo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_NACCAS_ExemptionList]'
GO
IF OBJECT_ID(N'[dbo].[USP_NACCAS_ExemptionList]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_NACCAS_ExemptionList]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_NACCAS_CohortGrid]'
GO
IF OBJECT_ID(N'[dbo].[USP_NACCAS_CohortGrid]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_NACCAS_CohortGrid]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[CalculateHoursForStudentOnDate]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateHoursForStudentOnDate]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[CalculateHoursForStudentOnDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudent]'
GO
IF OBJECT_ID(N'[dbo].[arStudent]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CalculateHoursForStudentOnDate]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateHoursForStudentOnDate]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'CREATE FUNCTION [dbo].[CalculateHoursForStudentOnDate]
    (
        @StuEnrollId UNIQUEIDENTIFIER
       ,@Date DATETIME
       ,@AttemptedInsertedHours DECIMAL(16, 2)
    )
RETURNS DECIMAL(16, 2)
AS
    BEGIN
        DECLARE @CampusId UNIQUEIDENTIFIER = (
                                             SELECT TOP 1 CampusId
                                             FROM   dbo.arStuEnrollments
                                             WHERE  StuEnrollId = @StuEnrollId
                                             );

        DECLARE @IsTimeClock BIT = (
                                   SELECT TOP 1 dbo.arPrgVersions.UseTimeClock
                                   FROM   dbo.arStuEnrollments
                                   JOIN   dbo.arPrgVersions ON arPrgVersions.PrgVerId = arStuEnrollments.PrgVerId
                                   WHERE  StuEnrollId = @StuEnrollId
                                   );

        DECLARE @TrackSapAttendance VARCHAR(50) = LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', @CampusId))));

        --no calculation needed, inserting these numbers as place holder or no attendance
        IF (
           @AttemptedInsertedHours = 0
           OR @AttemptedInsertedHours = 9999.0
           OR @AttemptedInsertedHours = 999.0
           OR @AttemptedInsertedHours = 99.0
           )
            BEGIN
                RETURN @AttemptedInsertedHours;
            END;

        --return attempted inserted hours if not by day or not time clock 
        IF (
           @TrackSapAttendance != ''byday''
           OR @IsTimeClock = 0
           )
            RETURN @AttemptedInsertedHours;

        --if by day and time clock, calculate hours based on punches, round based on campus settings
        DECLARE @MinimumHoursToBeConsideredPresent DECIMAL(16, 2) = (
                                                                    SELECT MinimumHoursToBePresent
                                                                    FROM   dbo.arStuEnrollments
                                                                    JOIN   dbo.arStudentSchedules ON arStudentSchedules.StuEnrollId = arStuEnrollments.StuEnrollId
                                                                    JOIN   dbo.arProgSchedules ON arProgSchedules.ScheduleId = arStudentSchedules.ScheduleId
                                                                    JOIN   dbo.arProgScheduleDetails ON arProgScheduleDetails.ScheduleId = arProgSchedules.ScheduleId
                                                                                                        AND dw = ( DATEPART(dw, @Date) - 1 )
                                                                                                        AND arStuEnrollments.StuEnrollId = @StuEnrollId
                                                                    );

        DECLARE @RoundingMethod VARCHAR(50) = dbo.GetAppSettingValueByKeyName(''HoursRoundingMethod'', @CampusId);

        IF @TrackSapAttendance = ''byday''
           AND @IsTimeClock = 1
            BEGIN
                DECLARE @CalculatedHoursForDayRounded DECIMAL(16, 2) = (
                                                                       SELECT   SUM(HoursForSingleInAndOut) AS TotalHoursForSingleDay
                                                                       FROM     (
                                                                                SELECT   dta.StuEnrollId
                                                                                        ,dta.PunchDate
                                                                                        ,CASE WHEN ( dta.rn % 2 ) = 0 THEN dta.rn - 1
                                                                                              ELSE dta.rn
                                                                                         END AS RelatedPunches
                                                                                        ,dbo.CalculateRoundingOnMinutes(
                                                                                                                           DATEDIFF(
                                                                                                                                       SECOND
                                                                                                                                      ,MIN(dta.PunchTime)
                                                                                                                                      ,MAX(dta.PunchTime)
                                                                                                                                   ) / 60.0
                                                                                                                          ,@RoundingMethod
                                                                                                                       ) AS HoursForSingleInAndOut
                                                                                FROM     (
                                                                                         SELECT BadgeId
                                                                                               ,StuEnrollId
                                                                                               ,PunchTime
                                                                                               ,CAST(PunchTime AS DATE) AS PunchDate
                                                                                               ,ROW_NUMBER() OVER ( ORDER BY StuEnrollId
                                                                                                                            ,PunchTime
                                                                                                                  ) rn
                                                                                         FROM   dbo.arStudentTimeClockPunches
                                                                                         WHERE  dbo.arStudentTimeClockPunches.Status = 1
                                                                                                AND StuEnrollId = @StuEnrollId
                                                                                                AND CAST(PunchTime AS DATE) = @Date
                                                                                         ) AS dta
                                                                                GROUP BY dta.StuEnrollId
                                                                                        ,dta.PunchDate
                                                                                        ,CASE WHEN ( dta.rn % 2 ) = 0 THEN dta.rn - 1
                                                                                              ELSE dta.rn
                                                                                         END
                                                                                HAVING   COUNT(dta.StuEnrollId) % 2 = 0
                                                                                ) AS Dta
                                                                       GROUP BY Dta.StuEnrollId
                                                                               ,Dta.PunchDate
                                                                       ) / 60.0;

                RETURN CASE WHEN @CalculatedHoursForDayRounded IS NULL THEN @AttemptedInsertedHours
                            WHEN ( @CalculatedHoursForDayRounded >= ISNULL(@MinimumHoursToBeConsideredPresent, 0)) THEN @CalculatedHoursForDayRounded
                            ELSE 0 --if student did not meet minimum hours to be considered present, return 0
                       END;
            END;

        RETURN @AttemptedInsertedHours;
    END;






'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[TR_InsertAttendanceSummary_ByClockHour_Minutes] on [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[TR_InsertAttendanceSummary_ByClockHour_Minutes]', 'TR') IS NULL
EXEC sp_executesql N'


CREATE TRIGGER [dbo].[TR_InsertAttendanceSummary_ByClockHour_Minutes]
ON [dbo].[arStudentClockAttendance]
AFTER INSERT, UPDATE
AS
SET NOCOUNT ON;

DECLARE @StuEnrollId UNIQUEIDENTIFIER
       ,@MeetDate DATETIME
       ,@WeekDay VARCHAR(15)
       ,@StartDate DATETIME
       ,@EndDate DATETIME;
DECLARE @PeriodDescrip VARCHAR(50)
       ,@Actual DECIMAL(18, 2)
       ,@Excused DECIMAL(18, 2)
       ,@ClsSectionId UNIQUEIDENTIFIER;
DECLARE @Absent DECIMAL(18, 2)
       ,@ScheduledMinutes DECIMAL(18, 2)
       ,@TardyMinutes DECIMAL(18, 2);
DECLARE @tardy DECIMAL(18, 2)
       ,@tracktardies INT
       ,@tardiesMakingAbsence INT
       ,@PrgVerId UNIQUEIDENTIFIER
       ,@rownumber INT
       ,@IsTardy BIT
       ,@ActualRunningScheduledHours DECIMAL(18, 2);
DECLARE @ActualRunningPresentHours DECIMAL(18, 2)
       ,@ActualRunningAbsentHours DECIMAL(18, 2)
       ,@ActualRunningTardyHours DECIMAL(18, 2)
       ,@ActualRunningMakeupHours DECIMAL(18, 2);
DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
       ,@intTardyBreakPoint INT
       ,@AdjustedRunningPresentHours DECIMAL(18, 2)
       ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
       ,@ActualRunningScheduledDays DECIMAL(18, 2);
DECLARE GetAttendance_Cursor CURSOR FOR
    SELECT     t1.StuEnrollId
              ,NULL AS ClsSectionId
              ,t1.RecordDate AS MeetDate
              ,t1.ActualHours
              ,t1.SchedHours AS ScheduledMinutes
              ,CASE WHEN (
                         (
                         t1.SchedHours >= 1
                         AND t1.SchedHours NOT IN ( 999, 9999 )
                         )
                         AND t1.ActualHours = 0
                         ) THEN t1.SchedHours
                    ELSE 0
               END AS Absent
              ,t1.isTardy
              ,(
               SELECT SUM(SchedHours)
               FROM   arStudentClockAttendance
               WHERE  StuEnrollId = t1.StuEnrollId
                      AND RecordDate <= t1.RecordDate
                      AND (
                          t1.SchedHours >= 1
                          AND t1.SchedHours NOT IN ( 999, 9999 )
                          AND t1.ActualHours NOT IN ( 999, 9999 )
                          )
               ) AS ActualRunningScheduledHours
              ,(
               SELECT SUM(ActualHours)
               FROM   arStudentClockAttendance
               WHERE  StuEnrollId = t1.StuEnrollId
                      AND RecordDate <= t1.RecordDate
                      AND (
                          t1.SchedHours >= 1
                          AND t1.SchedHours NOT IN ( 999, 9999 )
                          )
                      AND ActualHours >= 1
                      AND ActualHours NOT IN ( 999, 9999 )
               ) AS ActualRunningPresentHours
              ,(
               SELECT COUNT(ActualHours)
               FROM   arStudentClockAttendance
               WHERE  StuEnrollId = t1.StuEnrollId
                      AND RecordDate <= t1.RecordDate
                      AND (
                          t1.SchedHours >= 1
                          AND t1.SchedHours NOT IN ( 999, 9999 )
                          )
                      AND ActualHours = 0
                      AND ActualHours NOT IN ( 999, 9999 )
               ) AS ActualRunningAbsentHours
              ,(
               SELECT SUM(ActualHours)
               FROM   arStudentClockAttendance
               WHERE  StuEnrollId = t1.StuEnrollId
                      AND RecordDate <= t1.RecordDate
                      AND SchedHours = 0
                      AND ActualHours >= 1
                      AND ActualHours NOT IN ( 999, 9999 )
               ) AS ActualRunningMakeupHours
              ,(
               SELECT SUM(SchedHours - ActualHours)
               FROM   arStudentClockAttendance
               WHERE  StuEnrollId = t1.StuEnrollId
                      AND RecordDate <= t1.RecordDate
                      AND (
                          (
                          t1.SchedHours >= 1
                          AND t1.SchedHours NOT IN ( 999, 9999 )
                          )
                          AND ActualHours >= 1
                          AND ActualHours NOT IN ( 999, 9999 )
                          )
                      AND isTardy = 1
               ) AS ActualRunningTardyHours
              ,t3.TrackTardies
              ,t3.TardiesMakingAbsence
              ,t3.PrgVerId
              ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
    FROM       arStudentClockAttendance t1
    INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
    INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
    INNER JOIN arAttUnitType AS t5 ON t5.UnitTypeId = t3.UnitTypeId
    --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
    WHERE      t1.StuEnrollId IN (
                                 SELECT StuEnrollId
                                 FROM   inserted
                                 )
               AND t5.UnitTypeDescrip IN ( ''Clock Hours'', ''Minutes'' );

DELETE FROM syStudentAttendanceSummary
WHERE StuEnrollId IN (
                    SELECT INSERTED.StuEnrollId
                    FROM   INSERTED
                    );
OPEN GetAttendance_Cursor;
DECLARE @ActualHours DECIMAL(18, 2)
       ,@SchedHours DECIMAL(18, 2);
FETCH NEXT FROM GetAttendance_Cursor
INTO @StuEnrollId
    ,@ClsSectionId
    ,@MeetDate
    ,@Actual
    ,@ScheduledMinutes
    ,@Absent
    ,@IsTardy
    ,@ActualRunningScheduledHours
    ,@ActualRunningPresentHours
    ,@ActualRunningAbsentHours
    ,@ActualRunningMakeupHours
    ,@ActualRunningTardyHours
    ,@tracktardies
    ,@tardiesMakingAbsence
    ,@PrgVerId
    ,@rownumber;

SET @ActualRunningPresentHours = 0;
SET @ActualRunningPresentHours = 0;
SET @ActualRunningAbsentHours = 0;
SET @ActualRunningTardyHours = 0;
SET @ActualRunningMakeupHours = 0;
SET @intTardyBreakPoint = 0;
SET @AdjustedRunningPresentHours = 0;
SET @AdjustedRunningAbsentHours = 0;
SET @ActualRunningScheduledDays = 0;
WHILE @@FETCH_STATUS = 0
    BEGIN

        IF @PrevStuEnrollId <> @StuEnrollId
            BEGIN
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @ActualRunningTardyHours = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
            END;

        -- Calculate Actual Running Scheduled Days
        IF (
           @ScheduledMinutes >= 1
           AND (
               @Actual <> 9999
               AND @Actual <> 999
               )
           AND (
               @ScheduledMinutes <> 9999
               AND @Actual <> 999
               )
           )
            BEGIN
                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + ISNULL(@ScheduledMinutes, 0);
            END;
        ELSE
            BEGIN
                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0);
            END;

        -- Calculate Actual Running Present Hours
        IF (
           @Actual <> 9999
           AND @Actual <> 999
           )
            BEGIN
                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
            END;

        -- Calculate: Actual Running Absent Hours
        --1. sched = 5, Actual = 2 then Ab = (5-3) = 2
        IF (
           @Actual > 0
           AND @Actual < @ScheduledMinutes
           )
            BEGIN
                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + ( @ScheduledMinutes - @Actual );
            END;
        ELSE
            BEGIN
                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
            END;

        -- Calculate: Actual Running Make up hours
        --sched=5, Actual =7, makeup= 2,ab = 0
        --sched=0, Actual =7, makeup= 7,ab = 0
        IF (
           @Actual > 0
           AND @ScheduledMinutes > 0
           AND @Actual > @ScheduledMinutes
           AND (
               @Actual <> 9999
               AND @Actual <> 999
               )
           )
            BEGIN
                SET @ActualRunningMakeupHours = @ActualRunningMakeupHours + ( @Actual - @ScheduledMinutes );
            END;

        -- Calculate: Adjusted Running Present Hours
        IF (
           @Actual <> 9999
           AND @Actual <> 999
           )
            BEGIN
                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
            END;

        -- Calculate: Adjusted Running Absent Hours
        -- Absent hours
        --1. sched = 5, Actual = 2 then Ab = (5-3) = 2
        IF (
           @Actual = 0
           AND @ScheduledMinutes >= 1
           AND @ScheduledMinutes NOT IN ( 999, 9999 )
           )
            BEGIN
                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
            END;
        ELSE IF (
                @Absent = 0
                AND @ActualRunningAbsentHours > 0
                AND ( @Actual < @ScheduledMinutes )
                )
                 BEGIN
                     SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                 END;

        -- Calculate : Actual Running Tardy Hours
        IF (
           @Actual > 0
           AND @Actual < @ScheduledMinutes
           AND (
               @Actual <> 9999.00
               AND @Actual <> 999.00
               )
           AND @IsTardy = 1
           )
            BEGIN
                SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
            END;

        -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
        -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
        -- when student is tardy the second time, that second occurance will be considered as
        -- absence
        -- Variable @intTardyBreakpoint tracks how many times the student was tardy
        -- Variable @tardiesMakingAbsence tracks the tardy rule
        IF @tracktardies = 1
           AND (
               @TardyMinutes > 0
               OR @IsTardy = 1
               )
            BEGIN
                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
            END;

        IF (
           @tracktardies = 1
           AND @intTardyBreakPoint = @tardiesMakingAbsence
           )
            BEGIN
                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --old code : @AdjustedRunningAbsentHours + @Actual 
                SET @intTardyBreakPoint = 0;
            END;



        INSERT INTO syStudentAttendanceSummary (
                                               StuEnrollId
                                              ,ClsSectionId
                                              ,StudentAttendedDate
                                              ,ScheduledDays
                                              ,ActualDays
                                              ,ActualRunningScheduledDays
                                              ,ActualRunningPresentDays
                                              ,ActualRunningAbsentDays
                                              ,ActualRunningMakeupDays
                                              ,ActualRunningTardyDays
                                              ,AdjustedPresentDays
                                              ,AdjustedAbsentDays
                                              ,AttendanceTrackType
                                              ,ModUser
                                              ,ModDate
                                               )
        VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                ,@ActualRunningAbsentHours, @ActualRunningMakeupHours, @ActualRunningTardyHours, @AdjustedRunningPresentHours, @AdjustedRunningAbsentHours
                ,''Post Attendance by Class'', ''sa'', GETDATE());
        --end
        SET @PrevStuEnrollId = @StuEnrollId;

        FETCH NEXT FROM GetAttendance_Cursor
        INTO @StuEnrollId
            ,@ClsSectionId
            ,@MeetDate
            ,@Actual
            ,@ScheduledMinutes
            ,@Absent
            ,@IsTardy
            ,@ActualRunningScheduledHours
            ,@ActualRunningPresentHours
            ,@ActualRunningAbsentHours
            ,@ActualRunningMakeupHours
            ,@ActualRunningTardyHours
            ,@tracktardies
            ,@tardiesMakingAbsence
            ,@PrgVerId
            ,@rownumber;
    END;
CLOSE GetAttendance_Cursor;
DEALLOCATE GetAttendance_Cursor;



SET NOCOUNT OFF;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[TR_InsertAttendanceSummary_ByDay_PA] on [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[TR_InsertAttendanceSummary_ByDay_PA]', 'TR') IS NULL
EXEC sp_executesql N'

CREATE TRIGGER [dbo].[TR_InsertAttendanceSummary_ByDay_PA]
ON [dbo].[arStudentClockAttendance]
AFTER INSERT, UPDATE
AS
SET NOCOUNT ON;

DECLARE @StuEnrollId UNIQUEIDENTIFIER
       ,@MeetDate DATETIME
       ,@WeekDay VARCHAR(15)
       ,@StartDate DATETIME
       ,@EndDate DATETIME;
DECLARE @PeriodDescrip VARCHAR(50)
       ,@Actual DECIMAL(18, 2)
       ,@Excused DECIMAL(18, 2)
       ,@ClsSectionId UNIQUEIDENTIFIER;
DECLARE @Absent DECIMAL(18, 2)
       ,@ScheduledMinutes DECIMAL(18, 2)
       ,@TardyMinutes DECIMAL(18, 2)
       ,@MakeupHours DECIMAL(18, 2);
DECLARE @tardy DECIMAL(18, 2)
       ,@tracktardies INT
       ,@tardiesMakingAbsence INT
       ,@PrgVerId UNIQUEIDENTIFIER
       ,@rownumber INT
       ,@IsTardy BIT
       ,@ActualRunningScheduledHours DECIMAL(18, 2);
DECLARE @ActualRunningPresentHours DECIMAL(18, 2)
       ,@ActualRunningAbsentHours DECIMAL(18, 2)
       ,@ActualRunningTardyHours DECIMAL(18, 2)
       ,@ActualRunningMakeupHours DECIMAL(18, 2);
DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
       ,@intTardyBreakPoint INT
       ,@AdjustedRunningPresentHours DECIMAL(18, 2)
       ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
       ,@ActualRunningScheduledDays DECIMAL(18, 2);
/***************************************************************************************
		Calculation for Attendance by Day and Present Absent : 
		
			Total Days Attended = Adjusted Present Days + Makeup days
			Absent Days = Days Absent + Tardy Days

*******************************************************************************************/
DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
    SELECT     t1.StuEnrollId
              ,t1.RecordDate
              ,t1.ActualHours
              ,t1.SchedHours
              ,CASE WHEN (
                         (
                         t1.SchedHours >= 1
                         AND t1.SchedHours NOT IN ( 999, 9999 )
                         )
                         AND t1.ActualHours = 0
                         ) THEN t1.SchedHours
                    ELSE 0
               END AS Absent
              ,t1.isTardy
              ,t3.TrackTardies
              ,t3.TardiesMakingAbsence
    FROM       arStudentClockAttendance t1
    INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
    INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
    INNER JOIN Inserted t4 ON t1.StuEnrollId = t4.StuEnrollId
    INNER JOIN arAttUnitType AS t5 ON t5.UnitTypeId = t3.UnitTypeId
    WHERE -- both Present Absent and Clock Hour Unit Type
               t5.UnitTypeDescrip IN ( ''Present Absent'', ''Clock Hours'' )
               AND t1.ActualHours <> 9999.00
    ORDER BY   t1.StuEnrollId
              ,t1.RecordDate;

DELETE FROM dbo.syStudentAttendanceSummary
WHERE StuEnrollId IN (
                    SELECT INSERTED.StuEnrollId
                    FROM   INSERTED
                    );
OPEN GetAttendance_Cursor;
DECLARE @ActualHours DECIMAL(18, 2)
       ,@SchedHours DECIMAL(18, 2);
FETCH NEXT FROM GetAttendance_Cursor
INTO @StuEnrollId
    ,@MeetDate
    ,@Actual
    ,@ScheduledMinutes
    ,@Absent
    ,@IsTardy
    ,@tracktardies
    ,@tardiesMakingAbsence;

SET @ActualRunningPresentHours = 0;
SET @ActualRunningPresentHours = 0;
SET @ActualRunningAbsentHours = 0;
SET @ActualRunningTardyHours = 0;
SET @ActualRunningMakeupHours = 0;
SET @intTardyBreakPoint = 0;
SET @AdjustedRunningPresentHours = 0;
SET @AdjustedRunningAbsentHours = 0;
SET @ActualRunningScheduledDays = 0;
SET @MakeupHours = 0;
WHILE @@FETCH_STATUS = 0
    BEGIN

        IF @PrevStuEnrollId <> @StuEnrollId
            BEGIN
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @ActualRunningTardyHours = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                SET @MakeupHours = 0;
            END;

        IF (
           @Actual <> 9999
           AND @Actual <> 999
           )
            BEGIN
                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + @ScheduledMinutes;
                SET @ActualRunningPresentHours = ISNULL(@ActualRunningPresentHours, 0) + @Actual;
                SET @AdjustedRunningPresentHours = ISNULL(@AdjustedRunningPresentHours, 0) + @Actual;
            END;
        SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + @Absent;
        IF (
           @Actual = 0
           AND @ScheduledMinutes >= 1
           AND @ScheduledMinutes NOT IN ( 999, 9999 )
           )
            BEGIN
                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
            END;

        -- The following condition applies only NWH 
        -- If Scheduled = 3.0 hrs and Actual = 1.0 Then 2 hrs is considered absent
        IF (
           @ScheduledMinutes >= 1
           AND @ScheduledMinutes NOT IN ( 999, 9999 )
           AND @Actual > 0
           AND ( @Actual < @ScheduledMinutes )
           )
            BEGIN
                SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                SET @AdjustedRunningAbsentHours = ISNULL(@AdjustedRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
            END;

        IF (
           @tracktardies = 1
           AND @IsTardy = 1
           )
            BEGIN
                SET @ActualRunningTardyHours = @ActualRunningTardyHours + 1;
            END;

        -- If there are make up hrs deduct that otherwise it will be added again in progress report
        IF (
           @Actual > 0
           AND @Actual > @ScheduledMinutes
           AND @Actual <> 9999.00
           )
            BEGIN
                SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
            END;
        IF @tracktardies = 1
           AND (
               @TardyMinutes > 0
               OR @IsTardy = 1
               )
            BEGIN
                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
            END;

        -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
        IF (
           @Actual > 0
           AND @Actual > @ScheduledMinutes
           AND @Actual <> 9999.00
           )
            BEGIN
                SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
            END;

        IF (
           @tracktardies = 1
           AND @intTardyBreakPoint = @tardiesMakingAbsence
           )
            BEGIN
                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                SET @intTardyBreakPoint = 0;
            END;


        INSERT INTO syStudentAttendanceSummary (
                                               StuEnrollId
                                              ,ClsSectionId
                                              ,StudentAttendedDate
                                              ,ScheduledDays
                                              ,ActualDays
                                              ,ActualRunningScheduledDays
                                              ,ActualRunningPresentDays
                                              ,ActualRunningAbsentDays
                                              ,ActualRunningMakeupDays
                                              ,ActualRunningTardyDays
                                              ,AdjustedPresentDays
                                              ,AdjustedAbsentDays
                                              ,AttendanceTrackType
                                              ,ModUser
                                              ,ModDate
                                              ,tardiesmakingabsence
                                               )
        VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, ISNULL(@ScheduledMinutes, 0), @Actual, ISNULL(@ActualRunningScheduledDays, 0)
                ,ISNULL(@ActualRunningPresentHours, 0), ISNULL(@ActualRunningAbsentHours, 0), ISNULL(@MakeupHours, 0), ISNULL(@ActualRunningTardyHours, 0)
                ,ISNULL(@AdjustedRunningPresentHours, 0), ISNULL(@AdjustedRunningAbsentHours, 0), ''Post Attendance by Class'', ''sa'', GETDATE()
                ,@tardiesMakingAbsence );

        --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
        --end
        SET @PrevStuEnrollId = @StuEnrollId;
        FETCH NEXT FROM GetAttendance_Cursor
        INTO @StuEnrollId
            ,@MeetDate
            ,@Actual
            ,@ScheduledMinutes
            ,@Absent
            ,@IsTardy
            ,@tracktardies
            ,@tardiesMakingAbsence;
    END;
CLOSE GetAttendance_Cursor;
DEALLOCATE GetAttendance_Cursor;



SET NOCOUNT OFF;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[syCampuses_Audit_Delete] on [dbo].[syCampuses]'
GO
IF OBJECT_ID(N'[dbo].[syCampuses_Audit_Delete]', 'TR') IS NULL
EXEC sp_executesql N'

CREATE TRIGGER [dbo].[syCampuses_Audit_Delete]
ON [dbo].[syCampuses]
FOR DELETE
AS
SET NOCOUNT ON;

DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
DECLARE @EventRows AS INT;
DECLARE @EventDate AS DATETIME;
DECLARE @UserName AS VARCHAR(50);
SET @AuditHistId = NEWID();
SET @EventRows = (
                 SELECT COUNT(*)
                 FROM   Deleted
                 );
SET @EventDate = (
                 SELECT TOP 1 ModDate
                 FROM   Deleted
                 );
SET @UserName = (
                SELECT TOP 1 ModUser
                FROM   Deleted
                );
IF @EventRows > 0
    BEGIN
        EXEC fmAuditHistAdd @AuditHistId
                           ,''syCampuses''
                           ,''D''
                           ,@EventRows
                           ,@EventDate
                           ,@UserName;
        BEGIN
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''CampCode''
                              ,CONVERT(VARCHAR(8000), Old.CampCode, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''CampDescrip''
                              ,CONVERT(VARCHAR(8000), Old.CampDescrip, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''Address1''
                              ,CONVERT(VARCHAR(8000), Old.Address1, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''Address2''
                              ,CONVERT(VARCHAR(8000), Old.Address2, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''City''
                              ,CONVERT(VARCHAR(8000), Old.City, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''StateId''
                              ,CONVERT(VARCHAR(8000), Old.StateId, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''Zip''
                              ,CONVERT(VARCHAR(8000), Old.Zip, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''CountryId''
                              ,CONVERT(VARCHAR(8000), Old.CountryId, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''StatusId''
                              ,CONVERT(VARCHAR(8000), Old.StatusId, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''Phone1''
                              ,CONVERT(VARCHAR(8000), Old.Phone1, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''Phone2''
                              ,CONVERT(VARCHAR(8000), Old.Phone2, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''Phone3''
                              ,CONVERT(VARCHAR(8000), Old.Phone3, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''Fax''
                              ,CONVERT(VARCHAR(8000), Old.Fax, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''Email''
                              ,CONVERT(VARCHAR(8000), Old.Email, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''Website''
                              ,CONVERT(VARCHAR(8000), Old.Website, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''IsCorporate''
                              ,CONVERT(VARCHAR(8000), Old.IsCorporate, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''IsCorporate''
                              ,CONVERT(VARCHAR(8000), Old.AllowGraduateAndStillOweMoney, 121)
                        FROM   Deleted Old;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,OldValue
                                          )
                        SELECT @AuditHistId
                              ,Old.CampusId
                              ,''IsCorporate''
                              ,CONVERT(VARCHAR(8000), Old.AllowGraduateWithoutFullCompletion, 121)
                        FROM   Deleted Old;
        END;
    END;



SET NOCOUNT OFF;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[syCampuses_Audit_Insert] on [dbo].[syCampuses]'
GO
IF OBJECT_ID(N'[dbo].[syCampuses_Audit_Insert]', 'TR') IS NULL
EXEC sp_executesql N'


CREATE TRIGGER [dbo].[syCampuses_Audit_Insert]
ON [dbo].[syCampuses]
FOR INSERT
AS
SET NOCOUNT ON;

DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
DECLARE @EventRows AS INT;
DECLARE @EventDate AS DATETIME;
DECLARE @UserName AS VARCHAR(50);
SET @AuditHistId = NEWID();
SET @EventRows = (
                 SELECT COUNT(*)
                 FROM   Inserted
                 );
SET @EventDate = (
                 SELECT TOP 1 ModDate
                 FROM   Inserted
                 );
SET @UserName = (
                SELECT TOP 1 ModUser
                FROM   Inserted
                );
IF @EventRows > 0
    BEGIN
        EXEC fmAuditHistAdd @AuditHistId
                           ,''syCampuses''
                           ,''I''
                           ,@EventRows
                           ,@EventDate
                           ,@UserName;
        BEGIN
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''CampCode''
                              ,CONVERT(VARCHAR(8000), New.CampCode, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''CampDescrip''
                              ,CONVERT(VARCHAR(8000), New.CampDescrip, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''Address1''
                              ,CONVERT(VARCHAR(8000), New.Address1, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''Address2''
                              ,CONVERT(VARCHAR(8000), New.Address2, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''City''
                              ,CONVERT(VARCHAR(8000), New.City, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''StateId''
                              ,CONVERT(VARCHAR(8000), New.StateId, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''Zip''
                              ,CONVERT(VARCHAR(8000), New.Zip, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''CountryId''
                              ,CONVERT(VARCHAR(8000), New.CountryId, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''StatusId''
                              ,CONVERT(VARCHAR(8000), New.StatusId, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''Phone1''
                              ,CONVERT(VARCHAR(8000), New.Phone1, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''Phone2''
                              ,CONVERT(VARCHAR(8000), New.Phone2, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''Phone3''
                              ,CONVERT(VARCHAR(8000), New.Phone3, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''Fax''
                              ,CONVERT(VARCHAR(8000), New.Fax, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''Email''
                              ,CONVERT(VARCHAR(8000), New.Email, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''Website''
                              ,CONVERT(VARCHAR(8000), New.Website, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''IsCorporate''
                              ,CONVERT(VARCHAR(8000), New.IsCorporate, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''IsCorporate''
                              ,CONVERT(VARCHAR(8000), New.AllowGraduateAndStillOweMoney, 121)
                        FROM   Inserted New;
            INSERT INTO syAuditHistDetail (
                                          AuditHistId
                                         ,RowId
                                         ,ColumnName
                                         ,NewValue
                                          )
                        SELECT @AuditHistId
                              ,New.CampusId
                              ,''IsCorporate''
                              ,CONVERT(VARCHAR(8000), New.AllowGraduateWithoutFullCompletion, 121)
                        FROM   Inserted New;
        END;
    END;



SET NOCOUNT OFF;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[syCampuses_Audit_Update] on [dbo].[syCampuses]'
GO
IF OBJECT_ID(N'[dbo].[syCampuses_Audit_Update]', 'TR') IS NULL
EXEC sp_executesql N'

CREATE TRIGGER [dbo].[syCampuses_Audit_Update]
ON [dbo].[syCampuses]
FOR UPDATE
AS
SET NOCOUNT ON;

DECLARE @AuditHistId AS UNIQUEIDENTIFIER;
DECLARE @EventRows AS INT;
DECLARE @EventDate AS DATETIME;
DECLARE @UserName AS VARCHAR(50);
SET @AuditHistId = NEWID();
SET @EventRows = (
                 SELECT COUNT(*)
                 FROM   Inserted
                 );
SET @EventDate = (
                 SELECT TOP 1 ModDate
                 FROM   Inserted
                 );
SET @UserName = (
                SELECT TOP 1 ModUser
                FROM   Inserted
                );
IF @EventRows > 0
    BEGIN
        EXEC fmAuditHistAdd @AuditHistId
                           ,''syCampuses''
                           ,''U''
                           ,@EventRows
                           ,@EventDate
                           ,@UserName;
        BEGIN
            IF UPDATE(CampCode)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''CampCode''
                                           ,CONVERT(VARCHAR(8000), Old.CampCode, 121)
                                           ,CONVERT(VARCHAR(8000), New.CampCode, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.CampCode <> New.CampCode
                                            OR (
                                               Old.CampCode IS NULL
                                               AND New.CampCode IS NOT NULL
                                               )
                                            OR (
                                               New.CampCode IS NULL
                                               AND Old.CampCode IS NOT NULL
                                               );
            IF UPDATE(CampDescrip)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''CampDescrip''
                                           ,CONVERT(VARCHAR(8000), Old.CampDescrip, 121)
                                           ,CONVERT(VARCHAR(8000), New.CampDescrip, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.CampDescrip <> New.CampDescrip
                                            OR (
                                               Old.CampDescrip IS NULL
                                               AND New.CampDescrip IS NOT NULL
                                               )
                                            OR (
                                               New.CampDescrip IS NULL
                                               AND Old.CampDescrip IS NOT NULL
                                               );
            IF UPDATE(Address1)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''Address1''
                                           ,CONVERT(VARCHAR(8000), Old.Address1, 121)
                                           ,CONVERT(VARCHAR(8000), New.Address1, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Address1 <> New.Address1
                                            OR (
                                               Old.Address1 IS NULL
                                               AND New.Address1 IS NOT NULL
                                               )
                                            OR (
                                               New.Address1 IS NULL
                                               AND Old.Address1 IS NOT NULL
                                               );
            IF UPDATE(Address2)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''Address2''
                                           ,CONVERT(VARCHAR(8000), Old.Address2, 121)
                                           ,CONVERT(VARCHAR(8000), New.Address2, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Address2 <> New.Address2
                                            OR (
                                               Old.Address2 IS NULL
                                               AND New.Address2 IS NOT NULL
                                               )
                                            OR (
                                               New.Address2 IS NULL
                                               AND Old.Address2 IS NOT NULL
                                               );
            IF UPDATE(City)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''City''
                                           ,CONVERT(VARCHAR(8000), Old.City, 121)
                                           ,CONVERT(VARCHAR(8000), New.City, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.City <> New.City
                                            OR (
                                               Old.City IS NULL
                                               AND New.City IS NOT NULL
                                               )
                                            OR (
                                               New.City IS NULL
                                               AND Old.City IS NOT NULL
                                               );
            IF UPDATE(StateId)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''StateId''
                                           ,CONVERT(VARCHAR(8000), Old.StateId, 121)
                                           ,CONVERT(VARCHAR(8000), New.StateId, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.StateId <> New.StateId
                                            OR (
                                               Old.StateId IS NULL
                                               AND New.StateId IS NOT NULL
                                               )
                                            OR (
                                               New.StateId IS NULL
                                               AND Old.StateId IS NOT NULL
                                               );
            IF UPDATE(Zip)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''Zip''
                                           ,CONVERT(VARCHAR(8000), Old.Zip, 121)
                                           ,CONVERT(VARCHAR(8000), New.Zip, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Zip <> New.Zip
                                            OR (
                                               Old.Zip IS NULL
                                               AND New.Zip IS NOT NULL
                                               )
                                            OR (
                                               New.Zip IS NULL
                                               AND Old.Zip IS NOT NULL
                                               );
            IF UPDATE(CountryId)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''CountryId''
                                           ,CONVERT(VARCHAR(8000), Old.CountryId, 121)
                                           ,CONVERT(VARCHAR(8000), New.CountryId, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.CountryId <> New.CountryId
                                            OR (
                                               Old.CountryId IS NULL
                                               AND New.CountryId IS NOT NULL
                                               )
                                            OR (
                                               New.CountryId IS NULL
                                               AND Old.CountryId IS NOT NULL
                                               );
            IF UPDATE(StatusId)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''StatusId''
                                           ,CONVERT(VARCHAR(8000), Old.StatusId, 121)
                                           ,CONVERT(VARCHAR(8000), New.StatusId, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.StatusId <> New.StatusId
                                            OR (
                                               Old.StatusId IS NULL
                                               AND New.StatusId IS NOT NULL
                                               )
                                            OR (
                                               New.StatusId IS NULL
                                               AND Old.StatusId IS NOT NULL
                                               );
            IF UPDATE(Phone1)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''Phone1''
                                           ,CONVERT(VARCHAR(8000), Old.Phone1, 121)
                                           ,CONVERT(VARCHAR(8000), New.Phone1, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Phone1 <> New.Phone1
                                            OR (
                                               Old.Phone1 IS NULL
                                               AND New.Phone1 IS NOT NULL
                                               )
                                            OR (
                                               New.Phone1 IS NULL
                                               AND Old.Phone1 IS NOT NULL
                                               );
            IF UPDATE(Phone2)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''Phone2''
                                           ,CONVERT(VARCHAR(8000), Old.Phone2, 121)
                                           ,CONVERT(VARCHAR(8000), New.Phone2, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Phone2 <> New.Phone2
                                            OR (
                                               Old.Phone2 IS NULL
                                               AND New.Phone2 IS NOT NULL
                                               )
                                            OR (
                                               New.Phone2 IS NULL
                                               AND Old.Phone2 IS NOT NULL
                                               );
            IF UPDATE(Phone3)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''Phone3''
                                           ,CONVERT(VARCHAR(8000), Old.Phone3, 121)
                                           ,CONVERT(VARCHAR(8000), New.Phone3, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Phone3 <> New.Phone3
                                            OR (
                                               Old.Phone3 IS NULL
                                               AND New.Phone3 IS NOT NULL
                                               )
                                            OR (
                                               New.Phone3 IS NULL
                                               AND Old.Phone3 IS NOT NULL
                                               );
            IF UPDATE(Fax)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''Fax''
                                           ,CONVERT(VARCHAR(8000), Old.Fax, 121)
                                           ,CONVERT(VARCHAR(8000), New.Fax, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Fax <> New.Fax
                                            OR (
                                               Old.Fax IS NULL
                                               AND New.Fax IS NOT NULL
                                               )
                                            OR (
                                               New.Fax IS NULL
                                               AND Old.Fax IS NOT NULL
                                               );
            IF UPDATE(Email)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''Email''
                                           ,CONVERT(VARCHAR(8000), Old.Email, 121)
                                           ,CONVERT(VARCHAR(8000), New.Email, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Email <> New.Email
                                            OR (
                                               Old.Email IS NULL
                                               AND New.Email IS NOT NULL
                                               )
                                            OR (
                                               New.Email IS NULL
                                               AND Old.Email IS NOT NULL
                                               );
            IF UPDATE(Website)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''Website''
                                           ,CONVERT(VARCHAR(8000), Old.Website, 121)
                                           ,CONVERT(VARCHAR(8000), New.Website, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.Website <> New.Website
                                            OR (
                                               Old.Website IS NULL
                                               AND New.Website IS NOT NULL
                                               )
                                            OR (
                                               New.Website IS NULL
                                               AND Old.Website IS NOT NULL
                                               );
            IF UPDATE(IsCorporate)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''IsCorporate''
                                           ,CONVERT(VARCHAR(8000), Old.IsCorporate, 121)
                                           ,CONVERT(VARCHAR(8000), New.IsCorporate, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.IsCorporate <> New.IsCorporate
                                            OR (
                                               Old.IsCorporate IS NULL
                                               AND New.IsCorporate IS NOT NULL
                                               )
                                            OR (
                                               New.IsCorporate IS NULL
                                               AND Old.IsCorporate IS NOT NULL
                                               );

            IF UPDATE(AllowGraduateWithoutFullCompletion)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''IsCorporate''
                                           ,CONVERT(VARCHAR(8000), Old.AllowGraduateWithoutFullCompletion, 121)
                                           ,CONVERT(VARCHAR(8000), New.AllowGraduateWithoutFullCompletion, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.AllowGraduateWithoutFullCompletion <> New.AllowGraduateWithoutFullCompletion
                                            OR (
                                               Old.AllowGraduateWithoutFullCompletion IS NULL
                                               AND New.AllowGraduateWithoutFullCompletion IS NOT NULL
                                               )
                                            OR (
                                               New.AllowGraduateWithoutFullCompletion IS NULL
                                               AND Old.AllowGraduateWithoutFullCompletion IS NOT NULL
                                               );

            IF UPDATE(AllowGraduateAndStillOweMoney)
                INSERT INTO syAuditHistDetail (
                                              AuditHistId
                                             ,RowId
                                             ,ColumnName
                                             ,OldValue
                                             ,NewValue
                                              )
                            SELECT          @AuditHistId
                                           ,New.CampusId
                                           ,''IsCorporate''
                                           ,CONVERT(VARCHAR(8000), Old.AllowGraduateAndStillOweMoney, 121)
                                           ,CONVERT(VARCHAR(8000), New.AllowGraduateAndStillOweMoney, 121)
                            FROM            Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.CampusId = New.CampusId
                            WHERE           Old.AllowGraduateAndStillOweMoney <> New.AllowGraduateAndStillOweMoney
                                            OR (
                                               Old.AllowGraduateAndStillOweMoney IS NULL
                                               AND New.AllowGraduateAndStillOweMoney IS NOT NULL
                                               )
                                            OR (
                                               New.AllowGraduateAndStillOweMoney IS NULL
                                               AND Old.AllowGraduateAndStillOweMoney IS NOT NULL
                                               );
        END;
    END;



SET NOCOUNT OFF;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[syCampuses_KlassApps_Delete] on [dbo].[syCampuses]'
GO
IF OBJECT_ID(N'[dbo].[syCampuses_KlassApps_Delete]', 'TR') IS NULL
EXEC sp_executesql N'
--==========================================================================================
-- TRIGGER syCampuses_KlassApps_Delete
-- AFTER DELETE syCampuses, marks syKlassAppConfigurationSetting record as deleted for Klass Apps
--==========================================================================================
CREATE TRIGGER [dbo].[syCampuses_KlassApps_Delete] ON [dbo].[syCampuses]
    AFTER DELETE
	-- KlassApps
	-- locations  is kte   --> Campus is label 
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @EventRows AS INT; 

        UPDATE  SKACS
        SET     SKACS.ItemStatus = ''D''
               ,SKACS.ModDate = GETDATE() -- D.ModDate
               ,SKACS.ModUser = ''Support'' -- D.ModUser
        FROM    syKlassAppConfigurationSetting AS SKACS
        INNER JOIN syKlassOperationType AS SKOT ON SKOT.KlassOperationTypeId = SKACS.KlassOperationTypeId
        INNER JOIN Deleted AS D ON D.CampusId = SKACS.AdvantageId
        WHERE   SKOT.Code = ''locations'';

        SET NOCOUNT OFF; 
    END;
--==========================================================================================
-- END  --  TRIGGER syCampuses_KlassApsp_Delete
--==========================================================================================
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[syCampuses_KlassApps_Insert] on [dbo].[syCampuses]'
GO
IF OBJECT_ID(N'[dbo].[syCampuses_KlassApps_Insert]', 'TR') IS NULL
EXEC sp_executesql N'

--==========================================================================================
-- TRIGGER syCampuses_KlassApps_Insert
-- AFTER INSERT syCampuses, insert syKlassAppConfigurationSetting record for Klass Apps
--==========================================================================================
CREATE TRIGGER [dbo].[syCampuses_KlassApps_Insert] ON [dbo].[syCampuses]
    AFTER INSERT
	-- KlassApps
	-- locations  is kte   --> Campus is label 
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @EventRows AS INT; 
        DECLARE @KlassOperationTypeId AS INTEGER;
        SELECT  @KlassOperationTypeId = SKOT.KlassOperationTypeId
        FROM    syKlassOperationType AS SKOT
        WHERE   SKOT.Code = ''locations'';

        INSERT  INTO syKlassAppConfigurationSetting
                (
                 AdvantageId
                ,KlassAppId
                ,KlassEntityId
                ,KlassOperationTypeId
                ,IsCustomField
                ,IsActive
                ,ItemStatus
                ,ItemValue
                ,ItemLabel
                ,ItemValueType
                ,CreationDate
                ,ModDate
                ,ModUser
                ,LastOperationLog
		        )
                SELECT  I.CampusId -- AdvantageId - varchar(38)
                       ,0    -- KlassAppId - int
                       ,1    -- KlassEntityId - int
                       ,@KlassOperationTypeId  -- KlassOperationTypeId - int
                       ,0    -- IsCustomField - bit
                       ,1    -- IsActive - bit
                       ,''I''  -- ItemStatus - char(1)
                       ,I.CampDescrip  -- ItemValue - varchar(200)
                       ,''Campus''  -- ItemLabel - varchar(50)
                       ,''String''  -- ItemValueType - varchar(50)
                       ,I.ModDate -- CreationDate - datetime
                       ,I.ModDate -- ModDate - datetime
                       ,I.ModUser -- ModUser - varchar(50)
                       ,''''  -- LastOperationLog - varchar(1000)
                FROM    Inserted AS I;




        SET NOCOUNT OFF; 
    END;
--==========================================================================================
-- END  --  TRIGGER syCampuses_KlassApps_Insert
--==========================================================================================

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[syCampuses_KlassApps_Update] on [dbo].[syCampuses]'
GO
IF OBJECT_ID(N'[dbo].[syCampuses_KlassApps_Update]', 'TR') IS NULL
EXEC sp_executesql N'
--==========================================================================================
-- TRIGGER syCampuses_KlassApps_Update
-- AFTER UPDATE  syCampuses, syKlassAppConfigurationSetting when change fields for Klass Apps
--==========================================================================================
CREATE TRIGGER [dbo].[syCampuses_KlassApps_Update] ON [dbo].[syCampuses]
    AFTER UPDATE
	--   DB syCampuses  --   MAP        --  Klass Apps               locations   
	-- CampusId                                                     AdvantageId
    -- CampDescrip      -- Description  --> name	
	-- Address1			-- Address1		--> address
	-- Phone1			-- Phone1		--> phone
	--                  --              --> location_type
	--					-- Email		--> contact_email
	--                  --              --> contact_person
	-- Phone1			-- Phone1		--> contact_phone
	--                  --              --> contact_position
	--                  --              --> deleted
	--                  --              --> referrals_email_address
	--                  --              --> created_at
	--                  -- ModDate      --> updated_at
	--                  --              --> payments_on
	--                  --              --> 

	-- KlassApps
	-- locations  is kte   --> Campus is label 
AS
    BEGIN
        SET NOCOUNT ON;

        DECLARE @EventRows AS INT; 

        UPDATE  SKACS
        SET     SKACS.ItemStatus = ''U''
               ,SKACS.ModDate = I.ModDate
               ,SKACS.ModUser = I.ModUser
        FROM    syKlassAppConfigurationSetting AS SKACS
        INNER JOIN syKlassOperationType AS SKOT ON SKOT.KlassOperationTypeId = SKACS.KlassOperationTypeId
        INNER JOIN Inserted AS I ON I.CampusId = SKACS.AdvantageId
        WHERE   SKOT.Code = ''locations''
                AND SKACS.ItemStatus <> ''I''
                AND (
                      UPDATE(Address1)
                      OR UPDATE(Email)
                      OR UPDATE(CampDescrip)
                      OR UPDATE(Phone1)
                    );

        SET NOCOUNT OFF; 
    END;
--==========================================================================================
-- END  --  TRIGGER syCampuses_KlassApps_Update
--==========================================================================================
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[syRptFilterOtherPrefs]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[syRptFilterOtherPrefs] ALTER COLUMN [RptParamId] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[StudentScheduledHours]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StudentScheduledHours]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'-- =============================================
-- Author:		FAME Inc.
-- Create date: 5/23/2019
-- Description:	Student scheduled table valued function. Returns table student enrollment id , scheduled hours for day of the week , students must all be from same single campus
CREATE   FUNCTION [dbo].[StudentScheduledHours]
    (
        @StuEnrollIdList VARCHAR(MAX) = NULL
       ,@Date DATETIME
    )
RETURNS TABLE
AS
RETURN (
       SELECT ScheduledData.StuEnrollId
             ,CASE WHEN CAST(( ScheduledData.ScheduledMinutes - ScheduledData.LunchMinutes - ScheduledData.HolidayMinutes + LunchHolidayOverlapMinutes ) / 60.0 AS DECIMAL(16, 2)) < 0 THEN
                       0
                   ELSE
                       CAST(( ScheduledData.ScheduledMinutes - ScheduledData.LunchMinutes - ScheduledData.HolidayMinutes + LunchHolidayOverlapMinutes ) / 60.0 AS DECIMAL(16, 2))
              END AS CalculatedScheduledHours
             ,ScheduledData.ScheduleId
       FROM   (
              SELECT      arStudentSchedules.StuEnrollId
                         ,CampusId
                         ,total
                         ,timein
                         ,timeout
                         ,lunchin
                         ,lunchout
                         ,arProgSchedules.ScheduleId
                         ,ISNULL(DATEDIFF(MINUTE, timein, timeout), 0) AS ScheduledMinutes
                         ,ISNULL(DATEDIFF(MINUTE, lunchout, lunchin), 0) AS LunchMinutes
                         ,ISNULL(
                                    CASE WHEN Holiday.AllDay = 1 THEN DATEDIFF(MINUTE, timein, timeout)
                                         WHEN (
                                              CAST(Holiday.BeginTimeInteral AS TIME) <= CAST(timeout AS TIME)
                                              AND CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME) >= CAST(timein AS TIME)
                                              ) -- holiday falls entirely inside students schedule

                                    THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME))
                                         WHEN (
                                              CAST(Holiday.BeginTimeInteral AS TIME) >= CAST(timein AS TIME)
                                              AND CAST(Holiday.BeginTimeInteral AS TIME) <= CAST(timeout AS TIME)
                                              ) -- holiday start time falls betweem time in and time out 
                                    THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(timeout AS TIME))
                                         WHEN (
                                              CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME) >= CAST(timein AS TIME)
                                              AND CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME) <= CAST(timeout AS TIME)
                                              ) -- holiday end time falls betweem time in and time out 
                                    THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME))
                                         ELSE 0
                                    END
                                   ,0
                                ) AS HolidayMinutes
                         ,ISNULL(
                                    CASE WHEN Holiday.AllDay = 1 THEN DATEDIFF(MINUTE, lunchout, lunchin)
                                         WHEN (
                                              CAST(Holiday.BeginTimeInteral AS TIME) <= CAST(lunchin AS TIME)
                                              AND CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME) >= CAST(lunchout AS TIME)
                                              ) -- holiday falls entirely inside holiday schedule
                                    THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME))
                                         WHEN (
                                              CAST(Holiday.BeginTimeInteral AS TIME) >= CAST(lunchout AS TIME)
                                              AND CAST(Holiday.BeginTimeInteral AS TIME) <= CAST(lunchin AS TIME)
                                              ) -- holiday start time falls between lunch
                                    THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(lunchin AS TIME))
                                         WHEN (
                                              CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME) >= CAST(lunchout AS TIME)
                                              AND CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME) <= CAST(lunchin AS TIME)
                                              ) -- holiday end time falls between lunch 
                                    THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME))
                                         ELSE 0
                                    END
                                   ,0
                                ) AS LunchHolidayOverlapMinutes -- lunch and holiday may overlap, calculate this time and add it back to student scheduled hours bucket
                         ,Holiday.AllDay
                         ,Holiday.BeginTimeInteral
                         ,Holiday.EndTimeInterval
                         ,Holiday.HolidayDescrip
              FROM        dbo.arStuEnrollments
              JOIN        dbo.arStudentSchedules ON arStudentSchedules.StuEnrollId = arStuEnrollments.StuEnrollId
              JOIN        dbo.arProgSchedules ON arProgSchedules.ScheduleId = arStudentSchedules.ScheduleId
              JOIN        dbo.arProgScheduleDetails ON arProgScheduleDetails.ScheduleId = arStudentSchedules.ScheduleId
              OUTER APPLY (
                          SELECT    TOP 1 HolidayDescrip
                                   ,HolidayStartDate
                                   ,HolidayEndDate
                                   ,AllDay
                                   ,a.TimeIntervalDescrip AS BeginTimeInteral
                                   ,b.TimeIntervalDescrip AS EndTimeInterval
                          FROM      dbo.syHolidays
                          LEFT JOIN dbo.cmTimeInterval a ON a.TimeIntervalId = syHolidays.StartTimeId
                          LEFT JOIN dbo.cmTimeInterval b ON b.TimeIntervalId = syHolidays.EndTimeId
                          WHERE     (
                                    CAST(@Date AS DATE) >= CAST(HolidayStartDate AS DATE)
                                    AND CAST(@Date AS DATE) <= CAST(HolidayEndDate AS DATE)
                                    )
                                    AND syHolidays.CampGrpId IN (
                                                                SELECT CampGrpId
                                                                FROM   dbo.syCmpGrpCmps
                                                                WHERE  dbo.syCmpGrpCmps.CampusId = (
                                                                                                   SELECT CampusId
                                                                                                   FROM   dbo.arStuEnrollments
                                                                                                   WHERE  StuEnrollId = (
                                                                                                                        SELECT TOP 1 Val
                                                                                                                        FROM   MultipleValuesForReportParameters(
                                                                                                                                                                    @StuEnrollIdList
                                                                                                                                                                   ,'',''
                                                                                                                                                                   ,1
                                                                                                                                                                )
                                                                                                                        )
                                                                                                   )
                                                                )
                                    AND syHolidays.StatusId = (
                                                              SELECT TOP 1 StatusId
                                                              FROM   dbo.syStatuses
                                                              WHERE  StatusCode = ''A''
                                                              )
                          ) Holiday
              WHERE       dw = ( DATEPART(dw, @Date) - 1 )
                          AND arStuEnrollments.StuEnrollId IN (
                                                              SELECT Val
                                                              FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                              )
              --AND (
              --    @Date >= arStuEnrollments.StartDate
              --    AND @Date <= dbo.arStuEnrollments.ExpGradDate
              --    )
              ) ScheduledData
       );
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudAddresses]'
GO
IF OBJECT_ID(N'[dbo].[arStudAddresses]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudAddresses]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_AR_GetSystemTransCodesRefunds_GetList]'
GO
IF OBJECT_ID(N'[dbo].[USP_AR_GetSystemTransCodesRefunds_GetList]', 'P') IS NULL
EXEC sp_executesql N'

CREATE PROCEDURE [dbo].[USP_AR_GetSystemTransCodesRefunds_GetList]
    (
        @ShowActive NVARCHAR(50)
       ,@CampusId NVARCHAR(50) = NULL
    )
AS /*----------------------------------------------------------------------------------------------------
    Author : Saraswathi Lakshmanan
    
    Create date : 05/05/2010
    
    Procedure Name : [USP_AR_GetSystemTransCodesRefunds_GetList]

    Objective : Get the system TransactionCodes of type charges
    
    Parameters : Name Type Data Type Required? 
                        ===== ==== ========= ========= 
                        @ShowActive In nvarchar Required
    
    Output : Returns the TransCodes of type whose systransCodeID 16 
                        
*/
    -----------------------------------------------------------------------------------------------------

    BEGIN

        DECLARE @ShowActiveValue NVARCHAR(20);
        IF LOWER(@ShowActive) = ''true''
            BEGIN
                SET @ShowActiveValue = ''Active'';
            END;
        ELSE
            BEGIN
                SET @ShowActiveValue = ''InActive'';
            END;


        SELECT   TC.TransCodeId
                ,TC.TransCodeCode
                ,TC.TransCodeDescrip
                ,TC.StatusId
                ,TC.CampGrpId
                ,ST.StatusId
                ,ST.Status
        FROM     saTransCodes TC
                ,syStatuses ST
        WHERE    TC.StatusId = ST.StatusId
                 AND SysTransCodeId = 16
                 AND ST.Status = @ShowActiveValue
                 AND (
                     @CampusId IS NULL
                     OR CampGrpId IN (
                                     SELECT CampGrpId
                                     FROM   dbo.syCmpGrpCmps
                                     WHERE  CampusId = @CampusId
                                     )
                     )
        ORDER BY TC.TransCodeDescrip;

    END;








'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudentPhone]'
GO
IF OBJECT_ID(N'[dbo].[arStudentPhone]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudentPhone]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_FA_PostZerosForStudents]'
GO
IF OBJECT_ID(N'[dbo].[USP_FA_PostZerosForStudents]', 'P') IS NULL
EXEC sp_executesql N'-- =============================================
-- Author:		FAME Inc.
-- Create date: 5/23/2019
-- Description:	For arStudentClockAttendanceSchools posting zeros. Given enroll id list and record date, posts zeros for students on this date. Will post zero''s for all students in @StuEnrollIdList
--				wihout checking students in school status. Filtering of students should be down prior to the execution of this SP.	   				
-- =============================================
CREATE   PROCEDURE [dbo].[USP_FA_PostZerosForStudents]
    @StuEnrollIdList VARCHAR(MAX) = NULL
   ,@Date DATETIME
   ,@ModUser VARCHAR(50)
AS
    BEGIN
        DECLARE @ModDate DATETIME = GETDATE();

        --update placeholder records if they exist for given student enrollments on given day
        UPDATE dbo.arStudentClockAttendance
              SET	ActualHours = 0
                  ,SchedHours = a.CalculatedScheduledHours
                  ,ModDate = @ModDate
                  ,ModUser = @ModUser
                  ,isTardy = 0
                  ,PostByException = ''no''
                  ,comments = NULL
                  ,TardyProcessed = 0
                  ,Converted = 0
              FROM   dbo.arStudentClockAttendance
              JOIN   dbo.StudentScheduledHours(@StuEnrollIdList, @Date) a ON a.StuEnrollId = arStudentClockAttendance.StuEnrollId
              WHERE  CAST(RecordDate AS DATE) = CAST(@Date AS DATE)
                     AND ActualHours IN(99.0, 999.0, 9999.0) AND arStudentClockAttendance.StuEnrollId IN (SELECT Val
                                   FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)); 

        --create records if they do not exist for given student enrollments on given day 
        INSERT INTO dbo.arStudentClockAttendance
                    SELECT StuEnrollList.StuEnrollId  -- StuEnrollId - uniqueidentifier
                          ,a.ScheduleId               -- ScheduleId - uniqueidentifier
                          ,CAST(@Date AS DATE)		  -- RecordDate - smalldatetime
                          ,a.CalculatedScheduledHours -- SchedHours - decimal(18, 2)
                          ,0                          -- ActualHours - decimal(18, 2)
                          ,@ModDate                   -- ModDate - smalldatetime
                          ,@ModUser                   -- ModUser - varchar(50)
                          ,0                          -- isTardy - bit
                          ,''no''                       -- PostByException - varchar(10)
                          ,NULL                       -- comments - varchar(240)
                          ,0                          -- TardyProcessed - bit
                          ,0                          -- Converted - bit
                    FROM   (
                           SELECT CAST(Val AS UNIQUEIDENTIFIER) AS StuEnrollId
                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                           ) StuEnrollList
                    JOIN   dbo.StudentScheduledHours(@StuEnrollIdList, @Date) a ON a.StuEnrollId = StuEnrollList.StuEnrollId
                    WHERE  NOT EXISTS (
                                      SELECT *
                                      FROM   dbo.arStudentClockAttendance
                                      WHERE  StuEnrollId = StuEnrollList.StuEnrollId
                                             AND CAST(RecordDate AS DATE) = CAST(@Date AS DATE)
                                      );
    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_NACCAS_CohortGrid]'
GO
IF OBJECT_ID(N'[dbo].[USP_NACCAS_CohortGrid]', 'P') IS NULL
EXEC sp_executesql N'

CREATE PROCEDURE [dbo].[USP_NACCAS_CohortGrid]
    @PrgIdList VARCHAR(MAX) = NULL
   ,@CampusId VARCHAR(50)
   ,@ReportYear INT
   ,@ReportType VARCHAR(50)
AS
    DECLARE @StartMonth INT = 1
           ,@StartDay INT = 1
           ,@EndMonth INT = 12
           ,@EndDay INT = 31
           ,@FutureStart INT = 7
           ,@CurrentlyAttending INT = 9
           ,@LOA INT = 10
           ,@Suspension INT = 11
           ,@Dropped INT = 12
           ,@Graduated INT = 14
           ,@Transfer INT = 19
           ,@Preliminary VARCHAR(50) = ''Preliminary''
           ,@FallAnnual VARCHAR(50) = ''FallAnnual''
           ,@MilitaryTransfer UNIQUEIDENTIFIER
           ,@CallToActiveDuty UNIQUEIDENTIFIER
           ,@Deceased UNIQUEIDENTIFIER
           ,@TemporarilyDisabled UNIQUEIDENTIFIER
           ,@PermanentlyDisabled UNIQUEIDENTIFIER
           ,@TransferredToEquivalent UNIQUEIDENTIFIER;

    BEGIN

        SET @MilitaryTransfer = (
                                SELECT TOP 1 NACCASDropReasonId
                                FROM   dbo.syNACCASDropReasons
                                WHERE  Name = ''Military Transfer''
                                );
        SET @CallToActiveDuty = (
                                SELECT TOP 1 NACCASDropReasonId
                                FROM   dbo.syNACCASDropReasons
                                WHERE  Name = ''Call to Active Duty''
                                );
        SET @Deceased = (
                        SELECT TOP 1 NACCASDropReasonId
                        FROM   dbo.syNACCASDropReasons
                        WHERE  Name = ''Deceased''
                        );
        SET @TemporarilyDisabled = (
                                   SELECT TOP 1 NACCASDropReasonId
                                   FROM   dbo.syNACCASDropReasons
                                   WHERE  Name = ''Temporarily disabled''
                                   );
        SET @PermanentlyDisabled = (
                                   SELECT TOP 1 NACCASDropReasonId
                                   FROM   dbo.syNACCASDropReasons
                                   WHERE  Name = ''Permanently disabled''
                                   );
        SET @TransferredToEquivalent = (
                                       SELECT TOP 1 NACCASDropReasonId
                                       FROM   dbo.syNACCASDropReasons
                                       WHERE  Name = ''Transferred to an equivalent program at another school with same accreditation''
                                       );

        DECLARE @Enrollments TABLE
            (
                PrgVerID UNIQUEIDENTIFIER
               ,PrgVerDescrip VARCHAR(50)
               ,ProgramHours DECIMAL(9, 2)
               ,ProgramCredits DECIMAL(18, 2)
               ,TotalTransferHours DECIMAL(18, 2)
               ,TransferredProgram UNIQUEIDENTIFIER
               ,TransferHoursFromProgram DECIMAL(18, 2)
               ,SSN VARCHAR(50)
               ,PhoneNumber VARCHAR(50)
               ,Email VARCHAR(100)
               ,Student VARCHAR(50)
               ,LeadId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,EnrollmentID NVARCHAR(50)
               ,Status VARCHAR(80)
               ,SysStatusId INT
               ,DropReasonId UNIQUEIDENTIFIER
               ,StartDate DATETIME
               ,ContractedGradDate DATETIME
               ,ExpectedGradDate DATETIME
               ,LDA DATETIME
               ,LicensureWrittenAllParts BIT
               ,LicensureLastPartWrittenOn DATETIME
               ,LicensurePassedAllParts BIT
               ,AllowsMoneyOwed BIT
               ,AllowsIncompleteReq BIT
            );

        DECLARE @EnrollmentsWithLastStatus TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,SysStatusId INT
               ,DropReasonId UNIQUEIDENTIFIER
               ,StatusCodeDescription VARCHAR(80)
            );
        INSERT INTO @EnrollmentsWithLastStatus
                    SELECT lastStatusOfYear.StuEnrollId
                          ,lastStatusOfYear.SysStatusId
                          ,lastStatusOfYear.DropReasonId
                          ,lastStatusOfYear.StatusCodeDescrip
                    FROM   (
                           SELECT     StuEnrollId
                                     ,syStatusCodes.SysStatusId
                                     ,StatusCodeDescrip
                                     ,DateOfChange
                                     ,DropReasonId
                                     ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId
                                                          ORDER BY DateOfChange DESC
                                                        ) rn
                           FROM       dbo.syStudentStatusChanges
                           INNER JOIN dbo.syStatusCodes ON syStatusCodes.StatusCodeId = syStudentStatusChanges.NewStatusId
                           WHERE      DATEPART(YEAR, DateOfChange) <= ( @ReportYear - 1 )
                           ) lastStatusOfYear
                    WHERE  rn = 1;





        INSERT INTO @Enrollments
                    SELECT     Prog.ProgId AS PrgVerID
                              ,Prog.ProgDescrip AS PrgVerDescrip
                              ,PV.Hours AS ProgramHours
                              ,PV.Credits AS ProgramCredits
                              ,SE.TransferHours
                              ,SE.TransferHoursFromThisSchoolEnrollmentId
                              ,SE.TotalTransferHoursFromThisSchool
                              ,LD.SSN
                              ,ISNULL((
                                      SELECT   TOP 1 Phone
                                      FROM     dbo.adLeadPhone
                                      WHERE    LeadId = LD.LeadId
                                               AND (
                                                   IsBest = 1
                                                   OR IsShowOnLeadPage = 1
                                                   )
                                      ORDER BY Position
                                      )
                                     ,''''
                                     ) AS PhoneNumber
                              ,ISNULL((
                                      SELECT   TOP 1 EMail
                                      FROM     dbo.AdLeadEmail
                                      WHERE    LeadId = LD.LeadId
                                               AND (
                                                   IsPreferred = 1
                                                   OR IsShowOnLeadPage = 1
                                                   )
                                      ORDER BY IsPreferred DESC
                                              ,IsShowOnLeadPage DESC
                                      )
                                     ,''''
                                     ) AS Email
                              ,LD.LastName + '', '' + LD.FirstName + '' '' + ISNULL(LD.MiddleName, '''') AS Student
                              ,LD.LeadId
                              ,SE.StuEnrollId
                              ,LD.StudentNumber AS EnrollmentID
                              ,[@EnrollmentsWithLastStatus].StatusCodeDescription AS Status
                              ,[@EnrollmentsWithLastStatus].SysStatusId AS SysStatusId
                              ,[@EnrollmentsWithLastStatus].DropReasonId AS DropReasonId
                              ,SE.StartDate AS StartDate
                              ,SE.ContractedGradDate AS ContractedGradDate
                              ,dbo.GetGraduatedDate(SE.StuEnrollId) AS ExpectedGradDate
                              ,dbo.GetLDA(SE.StuEnrollId) AS LDA
                              ,SE.LicensureWrittenAllParts AS LicensureWrittenAllParts
                              ,SE.LicensureLastPartWrittenOn AS LicensureLastPartWrittenOn
                              ,SE.LicensurePassedAllParts AS LicensurePassedAllParts
                              ,camp.AllowGraduateAndStillOweMoney AS AllowsMoneyOwed
                              ,camp.AllowGraduateWithoutFullCompletion AS AllowsIncompleteReq
                    FROM       dbo.arStuEnrollments AS SE
                    INNER JOIN dbo.adLeads AS LD ON LD.LeadId = SE.LeadId
                    INNER JOIN dbo.arPrgVersions AS PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arPrograms AS Prog ON Prog.ProgId = PV.ProgId
                    INNER JOIN dbo.syApprovedNACCASProgramVersion NaccasApproved ON NaccasApproved.ProgramVersionId = SE.PrgVerId
                    INNER JOIN dbo.syNaccasSettings NaccasSettings ON NaccasSettings.NaccasSettingId = NaccasApproved.NaccasSettingId
                    INNER JOIN dbo.syCampuses AS camp ON camp.CampusId = SE.CampusId
                    INNER JOIN @EnrollmentsWithLastStatus ON [@EnrollmentsWithLastStatus].StuEnrollId = SE.StuEnrollId
                    WHERE      SE.CampusId = @CampusId
                               AND NaccasSettings.CampusId = @CampusId
                               --that have a contracted graduation date between January 1 of reporting year minus 1 year and December 31st of reporting year minus 1 year
                               -- Not a third party contract
                               AND NOT SE.ThirdPartyContract = 1
                               AND NaccasApproved.IsApproved = 1
                               AND SE.ContractedGradDate
                               BETWEEN CONVERT(DATETIME, CONVERT(VARCHAR(50), (( @ReportYear - 1 ) * 10000 + @StartMonth * 100 + @StartDay )), 112) AND CONVERT(
                                                                                                                                                                   DATETIME
                                                                                                                                                                  ,CONVERT(
                                                                                                                                                                              VARCHAR(50)
                                                                                                                                                                             ,(( @ReportYear
                                                                                                                                                                                 - 1
                                                                                                                                                                               )
                                                                                                                                                                               * 10000
                                                                                                                                                                               + @EndMonth
                                                                                                                                                                               * 100
                                                                                                                                                                               + @EndDay
                                                                                                                                                                              )
                                                                                                                                                                          )
                                                                                                                                                                  ,112
                                                                                                                                                               );
        --Base Case 
        WITH A
        AS ( SELECT se.PrgVerID
                   ,se.PrgVerDescrip
                   ,se.ProgramHours
                   ,se.ProgramCredits
                   ,se.SSN
                   ,se.PhoneNumber
                   ,se.Email
                   ,se.Student
                   ,se.LeadId
                   ,se.StuEnrollId
                   ,se.EnrollmentID
                   ,se.Status
                   ,se.SysStatusId
                   ,se.DropReasonId
                   ,se.StartDate
                   ,se.ContractedGradDate
                   ,se.ExpectedGradDate
                   ,se.LDA
                   ,se.LicensureWrittenAllParts
                   ,se.LicensureLastPartWrittenOn
                   ,se.LicensurePassedAllParts
                   ,se.AllowsMoneyOwed
                   ,se.AllowsIncompleteReq
             FROM   (
                    SELECT *
                          ,ROW_NUMBER() OVER ( PARTITION BY LeadId
                                                           ,PrgVerID
                                                           ,SysStatusId
                                               ORDER BY StartDate DESC
                                             ) RN
                    FROM   @Enrollments
                    ) se
             --Is currently attending the school or on LOA or Suspended or is enrolled but have not yet started
             WHERE  se.SysStatusId IN ( @FutureStart, @CurrentlyAttending, @Suspension, @LOA )
                    AND se.RN = 1
                    AND se.StuEnrollId NOT IN ((
                                               SELECT     se.TransferredProgram
                                               FROM       @Enrollments se
                                               INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
                                               WHERE      (
                                                          (
                                                          se.TotalTransferHours IS NOT NULL
                                                          AND se.TotalTransferHours <> 0
                                                          )
                                                          AND (( se.TransferHoursFromProgram / pe.ProgramHours ) = 1 )
                                                          )
                                               UNION
                                               SELECT     pe.TransferredProgram
                                               FROM       @Enrollments se
                                               INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
                                               WHERE      (
                                                          (
                                                          se.TotalTransferHours IS NOT NULL
                                                          AND se.TotalTransferHours <> 0
                                                          )
                                                          AND (( se.TransferHoursFromProgram / pe.ProgramHours ) <> 1 )
                                                          )
                                               )
                                              ))

            --Was enrolled in two different NACCAS approved programs in the same school and graduated/licensed/placed from both the programs in the same report period
            ,B
        AS ( SELECT se.PrgVerID
                   ,se.PrgVerDescrip
                   ,se.ProgramHours
                   ,se.ProgramCredits
                   ,se.SSN
                   ,se.PhoneNumber
                   ,se.Email
                   ,se.Student
                   ,se.LeadId
                   ,se.StuEnrollId
                   ,se.EnrollmentID
                   ,se.Status
                   ,se.SysStatusId
                   ,se.DropReasonId
                   ,se.StartDate
                   ,se.ContractedGradDate
                   ,se.ExpectedGradDate
                   ,se.LDA
                   ,se.LicensureWrittenAllParts
                   ,se.LicensureLastPartWrittenOn
                   ,se.LicensurePassedAllParts
                   ,se.AllowsMoneyOwed
                   ,se.AllowsIncompleteReq
             FROM   (
                    SELECT *
                          ,COUNT(1) OVER ( PARTITION BY LeadId
                                                       ,SysStatusId
                                         ) AS cnt
                    FROM   @Enrollments
                    ) se
             WHERE  se.SysStatusId IN ( @Graduated ))
            ,C
        AS (
           --Has dropped from a program whose length is less than one academic year of 900 hours after being enrolled for more than 15 calendar days
           --Has dropped from a program whose length is equal to or more than one academic year of 900 hours after being enrolled for more than 30 calendar DAYS
           SELECT se.PrgVerID
                 ,se.PrgVerDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND (
                      (
                      se.ProgramHours < 900
                      AND DATEDIFF(DAY, se.StartDate, se.LDA) > 15
                      AND NOT EXISTS (
                                     SELECT     *
                                     FROM       dbo.syNACCASDropReasonsMapping nm
                                     INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                                     WHERE      ns.CampusId = @CampusId
                                                AND se.DropReasonId = nm.ADVDropReasonId
                                                AND nm.NACCASDropReasonId IN ( @CallToActiveDuty, @MilitaryTransfer, @Deceased, @TemporarilyDisabled
                                                                              ,@PermanentlyDisabled, @TransferredToEquivalent
                                                                             )
                                     )
                      )
                      OR (
                         se.ProgramHours >= 900
                         AND DATEDIFF(DAY, se.StartDate, se.LDA) > 30
                         AND NOT EXISTS (
                                        SELECT     *
                                        FROM       dbo.syNACCASDropReasonsMapping nm
                                        INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                                        WHERE      ns.CampusId = @CampusId
                                                   AND se.DropReasonId = nm.ADVDropReasonId
                                                   AND nm.NACCASDropReasonId IN ( @CallToActiveDuty, @MilitaryTransfer, @Deceased, @TemporarilyDisabled
                                                                                 ,@PermanentlyDisabled, @TransferredToEquivalent
                                                                                )
                                        )
                         )
                      )
                  AND se.StuEnrollId NOT IN ((
                                             SELECT     se.TransferredProgram
                                             FROM       @Enrollments se
                                             INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
                                             WHERE      (
                                                        (
                                                        se.TotalTransferHours IS NOT NULL
                                                        AND se.TotalTransferHours <> 0
                                                        )
                                                        AND (( se.TransferHoursFromProgram / pe.ProgramHours ) = 1 )
                                                        )
                                             )
                                            ))
            --Was transferred from the school for which the report is generated to another school and both the schools are a branch of the same parent.
            -- Such a student would be included in the report generated for the original school as did not complete
            ,D
        AS ( SELECT     se.PrgVerID
                       ,se.PrgVerDescrip
                       ,se.ProgramHours
                       ,se.ProgramCredits
                       ,se.SSN
                       ,se.PhoneNumber
                       ,se.Email
                       ,se.Student
                       ,se.LeadId
                       ,se.StuEnrollId
                       ,se.EnrollmentID
                       ,se.Status
                       ,se.SysStatusId
                       ,se.DropReasonId
                       ,se.StartDate
                       ,se.ContractedGradDate
                       ,se.ExpectedGradDate
                       ,se.LDA
                       ,se.LicensureWrittenAllParts
                       ,se.LicensureLastPartWrittenOn
                       ,se.LicensurePassedAllParts
                       ,se.AllowsMoneyOwed
                       ,se.AllowsIncompleteReq
             FROM       @Enrollments se
             INNER JOIN dbo.arTrackTransfer tt ON se.StuEnrollId = tt.StuEnrollId
             INNER JOIN dbo.syCampuses srcCmp ON srcCmp.CampusId = tt.SourceCampusId
             INNER JOIN dbo.syCampuses targetCmp ON targetCmp.CampusId = tt.TargetCampusId
             INNER JOIN dbo.arPrgVersions targetPV ON targetPV.PrgVerId = tt.TargetPrgVerId
             WHERE      se.SysStatusId IN ( @Transfer )
                        AND ((
                             srcCmp.IsBranch = 1
                             AND targetCmp.IsBranch = 1
                             AND srcCmp.ParentCampusId <> targetCmp.ParentCampusId
                             )
                            ))
            ,E
        AS (
           -- select from PE id not all hours are transffered else select from SE
           SELECT     se.PrgVerID
                     ,se.PrgVerDescrip
                     ,se.ProgramHours
                     ,se.ProgramCredits
                     ,se.SSN
                     ,se.PhoneNumber
                     ,se.Email
                     ,se.Student
                     ,se.LeadId
                     ,se.StuEnrollId
                     ,se.EnrollmentID
                     ,se.Status
                     ,se.SysStatusId
                     ,se.DropReasonId
                     ,se.StartDate
                     ,se.ContractedGradDate
                     ,se.ExpectedGradDate
                     ,se.LDA
                     ,se.LicensureWrittenAllParts
                     ,se.LicensureLastPartWrittenOn
                     ,se.LicensurePassedAllParts
                     ,se.AllowsMoneyOwed
                     ,se.AllowsIncompleteReq
           FROM       @Enrollments se
           INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
           WHERE      (
                      (
                      se.TotalTransferHours IS NOT NULL
                      AND se.TotalTransferHours <> 0
                      )
                      AND (( se.TransferHoursFromProgram / pe.ProgramHours ) = 1 )
                      )
           UNION
           SELECT     pe.PrgVerID
                     ,pe.PrgVerDescrip
                     ,pe.ProgramHours
                     ,pe.ProgramCredits
                     ,pe.SSN
                     ,pe.PhoneNumber
                     ,pe.Email
                     ,pe.Student
                     ,pe.LeadId
                     ,pe.StuEnrollId
                     ,pe.EnrollmentID
                     ,pe.Status
                     ,pe.SysStatusId
                     ,pe.DropReasonId
                     ,pe.StartDate
                     ,pe.ContractedGradDate
                     ,pe.ExpectedGradDate
                     ,pe.LDA
                     ,pe.LicensureWrittenAllParts
                     ,pe.LicensureLastPartWrittenOn
                     ,pe.LicensurePassedAllParts
                     ,pe.AllowsMoneyOwed
                     ,pe.AllowsIncompleteReq
           FROM       @Enrollments se
           INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
           WHERE      (
                      (
                      se.TotalTransferHours IS NOT NULL
                      AND se.TotalTransferHours <> 0
                      )
                      AND (( se.TransferHoursFromProgram / pe.ProgramHours ) <> 1 )
                      ))
        SELECT    allData.PrgVerID
                 ,allData.PrgVerDescrip
                 ,allData.ProgramHours
                 ,allData.ProgramCredits
                 ,allData.SSN
                 ,(
                  SELECT STUFF(STUFF(STUFF(allData.PhoneNumber, 1, 0, ''(''), 5, 0, '')''), 9, 0, ''-'')
                  ) AS PhoneNumber
                 ,allData.Email
                 ,allData.Student
                 ,allData.LeadId
                 ,allData.StuEnrollId
                 ,allData.EnrollmentID
                 ,allData.Status
                 ,allData.SysStatusId
                 ,allData.DropReasonId
                 ,allData.StartDate
                 ,allData.ContractedGradDate
                 ,allData.ExpectedGradDate
                 ,allData.LDA
                 ,allData.LicensureWrittenAllParts
                 ,allData.LicensureLastPartWrittenOn
                 ,allData.LicensurePassedAllParts
                 ,allData.AllowsMoneyOwed
                 ,allData.AllowsIncompleteReq
                 ,allData.Graduated
                 ,allData.OwesMoney
                 ,allData.MissingRequired
                 ,allData.GraduatedProgram
                 ,allData.DateGraduated
                 ,allData.PlacementStatus
                 ,allData.IneligibilityReason
                 ,allData.Placed
                 ,allData.SatForAllExamParts
                 ,allData.PassedAllParts
                 ,ISNULL(pemp.EmployerDescrip, '''') AS EmployerName
                 ,ISNULL(pemp.Address1 + '' '' + pemp.Address2, '''') AS EmployerAddress
                 ,ISNULL(pemp.City, '''') AS EmployerCity
                 ,ISNULL(states.StateDescrip, '''') AS EmployerState
                 ,ISNULL(pemp.Zip, '''') AS EmployerZip
                 ,ISNULL(pemp.Phone, '''') AS EmployerPhone
        FROM      (
                  SELECT *
                        ,(
                         SELECT CASE WHEN satForExam.SatForAllExamParts = ''Y'' THEN CASE WHEN satForExam.LicensurePassedAllParts = 1 THEN ''Y''
                                                                                        ELSE ''N''
                                                                                   END
                                     ELSE ''N/A''
                                END
                         ) PassedAllParts
                  FROM   (
                         SELECT *
                               ,(
                                SELECT CASE WHEN placed.GraduatedProgram = ''Y'' THEN
                                                CASE WHEN placed.LicensureWrittenAllParts = 0 THEN ''N''
                                                     ELSE
                                                         CASE WHEN @ReportType = @Preliminary THEN
                                                                  CASE WHEN placed.LicensureLastPartWrittenOn < CONVERT(
                                                                                                                           DATETIME
                                                                                                                          ,CONVERT(
                                                                                                                                      VARCHAR(50)
                                                                                                                                     ,(( @ReportYear ) * 10000 + 3
                                                                                                                                       * 100 + 30
                                                                                                                                      )
                                                                                                                                  )
                                                                                                                          ,112
                                                                                                                       ) THEN ''Y''
                                                                       ELSE ''N''
                                                                  END
                                                              ELSE
                                                                  CASE WHEN placed.LicensureLastPartWrittenOn < CONVERT(
                                                                                                                           DATETIME
                                                                                                                          ,CONVERT(
                                                                                                                                      VARCHAR(50)
                                                                                                                                     ,(( @ReportYear ) * 10000 + 11
                                                                                                                                       * 100 + 30
                                                                                                                                      )
                                                                                                                                  )
                                                                                                                          ,112
                                                                                                                       ) THEN ''Y''
                                                                       ELSE ''N''
                                                                  END
                                                         END
                                                END
                                            ELSE ''N/A''
                                       END
                                ) AS SatForAllExamParts
                         FROM   (
                                SELECT *
                                      ,(
                                       SELECT CASE WHEN placementStat.PlacementStatus = ''E'' THEN
                                                       CASE WHEN EXISTS (
                                                                        SELECT     1
                                                                        FROM       dbo.PlStudentsPlaced
                                                                        INNER JOIN dbo.plFldStudy ON plFldStudy.FldStudyId = PlStudentsPlaced.FldStudyId
                                                                        WHERE      dbo.PlStudentsPlaced.StuEnrollId = placementStat.StuEnrollId
                                                                                   AND (
                                                                                       dbo.plFldStudy.FldStudyCode = ''Yes''
                                                                                       OR dbo.plFldStudy.FldStudyCode = ''Related''
                                                                                       )
                                                                        ) THEN ''Y''
                                                            ELSE ''N''
                                                       END
                                                   ELSE ''N/A''
                                              END
                                       ) AS Placed
                                FROM   (
                                       SELECT *
                                             ,(
                                              SELECT CASE WHEN graduated.GraduatedProgram = ''Y'' THEN CONVERT(VARCHAR, graduated.ExpectedGradDate, 101)
                                                          ELSE ''N/A''
                                                     END
                                              ) AS DateGraduated
                                             ,(
                                              SELECT CASE WHEN graduated.GraduatedProgram = ''Y'' THEN
                                                              CASE WHEN (
                                                                        SELECT   TOP 1 Eligible
                                                                        FROM     dbo.plExitInterview
                                                                        WHERE    EnrollmentId = graduated.StuEnrollId
                                                                        ORDER BY COALESCE(ExitInterviewDate, graduated.ExpectedGradDate) DESC
                                                                        ) = ''Yes'' THEN ''E''
                                                                   ELSE ''I''
                                                              END
                                                          ELSE ''N/A''
                                                     END
                                              ) AS PlacementStatus
                                             ,(
                                              SELECT   TOP 1 COALESCE(Reason, '''')
                                              FROM     dbo.plExitInterview
                                              WHERE    EnrollmentId = graduated.StuEnrollId
                                              ORDER BY COALESCE(ExitInterviewDate, graduated.ExpectedGradDate) DESC
                                              ) AS IneligibilityReason
                                       FROM   (
                                              SELECT *
                                                    ,(
                                                     SELECT CASE WHEN didGraduate.Graduated = 0 THEN ''N''
                                                                 ELSE CASE WHEN didGraduate.OwesMoney = 1
                                                                                AND didGraduate.AllowsMoneyOwed = 0 THEN ''N''
                                                                           WHEN didGraduate.MissingRequired = 1
                                                                                AND didGraduate.AllowsIncompleteReq = 0 THEN ''N''
                                                                           ELSE ''Y''
                                                                      END
                                                            END
                                                     ) AS GraduatedProgram
                                              FROM   (
                                                     SELECT *
                                                           ,(
                                                            SELECT CASE WHEN @ReportType = @Preliminary THEN
                                                                            CASE WHEN result.ExpectedGradDate < CONVERT(
                                                                                                                           DATETIME
                                                                                                                          ,CONVERT(
                                                                                                                                      VARCHAR(50)
                                                                                                                                     ,(( @ReportYear ) * 10000 + 3 * 100 + 30 )
                                                                                                                                  )
                                                                                                                          ,112
                                                                                                                       ) THEN 1
                                                                                 ELSE 0
                                                                            END
                                                                        ELSE
                                                                            CASE WHEN result.ExpectedGradDate < CONVERT(
                                                                                                                           DATETIME
                                                                                                                          ,CONVERT(
                                                                                                                                      VARCHAR(50)
                                                                                                                                     ,(( @ReportYear ) * 10000 + 11 * 100 + 30 )
                                                                                                                                  )
                                                                                                                          ,112
                                                                                                                       ) THEN 1
                                                                                 ELSE 0
                                                                            END
                                                                   END
                                                            ) AS Graduated
                                                           ,dbo.DoesEnrollmentHavePendingBalance(result.StuEnrollId) AS OwesMoney
                                                           ,~ ( dbo.CompletedRequiredCourses(result.StuEnrollId)) AS MissingRequired
                                                     FROM   (
                                                            SELECT *
                                                            FROM   A
                                                            UNION
                                                            SELECT *
                                                            FROM   B
                                                            UNION
                                                            SELECT *
                                                            FROM   C
                                                            UNION
                                                            SELECT *
                                                            FROM   D
                                                            UNION
                                                            SELECT *
                                                            FROM   E
                                                            ) AS result
                                                     WHERE  (
                                                            @PrgIdList IS NULL
                                                            OR result.PrgVerID IN (
                                                                                  SELECT Val
                                                                                  FROM   MultipleValuesForReportParameters(@PrgIdList, '','', 1)
                                                                                  )
                                                            )
                                                     ) didGraduate
                                              ) graduated
                                       ) placementStat
                                ) placed
                         ) satForExam
                  ) allData
        LEFT JOIN dbo.PlStudentsPlaced psp ON psp.StuEnrollId = allData.StuEnrollId
        LEFT JOIN dbo.plEmployerJobs pej ON pej.EmployerJobId = psp.EmployerJobId
        LEFT JOIN dbo.plEmployers pemp ON pemp.EmployerId = pej.EmployerId
        LEFT JOIN dbo.syStates states ON states.StateId = pemp.StateId
        ORDER BY  allData.PrgVerDescrip
                 ,allData.Student;



    END;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_NACCAS_ExemptionList]'
GO
IF OBJECT_ID(N'[dbo].[USP_NACCAS_ExemptionList]', 'P') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[USP_NACCAS_ExemptionList]
    @PrgIdList VARCHAR(MAX) = NULL
   ,@CampusId VARCHAR(50)
   ,@ReportYear INT
AS
    DECLARE @StartMonth INT = 1
           ,@StartDay INT = 1
           ,@EndMonth INT = 12
           ,@EndDay INT = 31
           ,@FutureStart INT = 7
           ,@CurrentlyAttending INT = 9
           ,@LOA INT = 10
           ,@Suspension INT = 11
           ,@Dropped INT = 12
           ,@Graduated INT = 14
           ,@Transfer INT = 19
           ,@DeceasedReason INT = 1
           ,@TempOrPermDisabledReason INT = 2
           ,@DeployedReason INT = 3
           ,@Transferred100Reason INT = 4
           ,@TransferredAccreditedReason INT = 5
           ,@EarlyWithdrawalReason INT = 6
           ,@MilitaryTransfer UNIQUEIDENTIFIER
           ,@CallToActiveDuty UNIQUEIDENTIFIER
           ,@Deceased UNIQUEIDENTIFIER
           ,@TemporarilyDisabled UNIQUEIDENTIFIER
           ,@PermanentlyDisabled UNIQUEIDENTIFIER
           ,@TransferredToEquivalent UNIQUEIDENTIFIER;
    BEGIN


        SET @MilitaryTransfer = (
                                SELECT TOP 1 NACCASDropReasonId
                                FROM   dbo.syNACCASDropReasons
                                WHERE  Name = ''Military Transfer''
                                );
        SET @CallToActiveDuty = (
                                SELECT TOP 1 NACCASDropReasonId
                                FROM   dbo.syNACCASDropReasons
                                WHERE  Name = ''Call to Active Duty''
                                );
        SET @Deceased = (
                        SELECT TOP 1 NACCASDropReasonId
                        FROM   dbo.syNACCASDropReasons
                        WHERE  Name = ''Deceased''
                        );
        SET @TemporarilyDisabled = (
                                   SELECT TOP 1 NACCASDropReasonId
                                   FROM   dbo.syNACCASDropReasons
                                   WHERE  Name = ''Temporarily disabled''
                                   );
        SET @PermanentlyDisabled = (
                                   SELECT TOP 1 NACCASDropReasonId
                                   FROM   dbo.syNACCASDropReasons
                                   WHERE  Name = ''Permanently disabled''
                                   );
        SET @TransferredToEquivalent = (
                                       SELECT TOP 1 NACCASDropReasonId
                                       FROM   dbo.syNACCASDropReasons
                                       WHERE  Name = ''Transferred to an equivalent program at another school with same accreditation''
                                       );

        DECLARE @Enrollments TABLE
            (
                PrgID UNIQUEIDENTIFIER
               ,PrgDescrip VARCHAR(50)
               ,ProgramHours DECIMAL(9, 2)
               ,ProgramCredits DECIMAL(18, 2)
               ,TotalTransferHours DECIMAL(18, 2)
               ,TransferredProgram UNIQUEIDENTIFIER
               ,TransferHoursFromProgram DECIMAL(18, 2)
               ,PrgVerId UNIQUEIDENTIFIER
               ,SSN VARCHAR(50)
               ,PhoneNumber VARCHAR(50)
               ,Email VARCHAR(100)
               ,Student VARCHAR(50)
               ,LeadId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,EnrollmentID NVARCHAR(50)
               ,Status VARCHAR(80)
               ,SysStatusId INT
               ,DropReasonId UNIQUEIDENTIFIER
               ,StartDate DATETIME
               ,ContractedGradDate DATETIME
               ,ExpectedGradDate DATETIME
               ,ReEnrollmentDate DATETIME
               ,LDA DATETIME
               ,LicensureWrittenAllParts BIT
               ,LicensureLastPartWrittenOn DATETIME
               ,LicensurePassedAllParts BIT
               ,AllowsMoneyOwed BIT
               ,AllowsIncompleteReq BIT
               ,ThirdPartyContract BIT
               ,RN INT
               ,cnt INT
            );

        DECLARE @EnrollmentsWithLastStatus TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,SysStatusId INT
               ,DropReasonId UNIQUEIDENTIFIER
               ,StatusCodeDescription VARCHAR(80)
            );
        INSERT INTO @EnrollmentsWithLastStatus
                    SELECT lastStatusOfYear.StuEnrollId
                          ,lastStatusOfYear.SysStatusId
                          ,lastStatusOfYear.DropReasonId
                          ,lastStatusOfYear.StatusCodeDescrip
                    FROM   (
                           SELECT     StuEnrollId
                                     ,syStatusCodes.SysStatusId
                                     ,StatusCodeDescrip
                                     ,DateOfChange
                                     ,DropReasonId
                                     ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId
                                                          ORDER BY DateOfChange DESC
                                                        ) rn
                           FROM       dbo.syStudentStatusChanges
                           INNER JOIN dbo.syStatusCodes ON syStatusCodes.StatusCodeId = syStudentStatusChanges.NewStatusId
                           WHERE      DATEPART(YEAR, DateOfChange) <= ( @ReportYear - 1 )
                           ) lastStatusOfYear
                    WHERE  rn = 1;


        INSERT INTO @Enrollments
                    SELECT *
                          ,ROW_NUMBER() OVER ( PARTITION BY enr.LeadId
                                                           ,enr.PrgID
                                                           ,enr.SysStatusId
                                               ORDER BY enr.StartDate DESC
                                             ) RN
                          ,COUNT(1) OVER ( PARTITION BY enr.LeadId
                                                       ,enr.PrgVerId
                                         ) AS ReenrollmentCount
                    FROM   (
                           SELECT     Prog.ProgId AS PrgID
                                     ,Prog.ProgDescrip AS PrgDescrip
                                     ,PV.Hours AS ProgramHours
                                     ,PV.Credits AS ProgramCredits
                                     ,SE.TransferHours
                                     ,SE.TransferHoursFromThisSchoolEnrollmentId
                                     ,SE.TotalTransferHoursFromThisSchool
                                     ,PV.PrgVerId AS PrgVerId
                                     ,LD.SSN
                                     ,ISNULL((
                                             SELECT   TOP 1 Phone
                                             FROM     dbo.adLeadPhone
                                             WHERE    LeadId = LD.LeadId
                                                      AND (
                                                          IsBest = 1
                                                          OR IsShowOnLeadPage = 1
                                                          )
                                             ORDER BY Position
                                             )
                                            ,''''
                                            ) AS PhoneNumber
                                     ,ISNULL((
                                             SELECT   TOP 1 EMail
                                             FROM     dbo.AdLeadEmail
                                             WHERE    LeadId = LD.LeadId
                                                      AND (
                                                          IsPreferred = 1
                                                          OR IsShowOnLeadPage = 1
                                                          )
                                             ORDER BY IsPreferred DESC
                                                     ,IsShowOnLeadPage DESC
                                             )
                                            ,''''
                                            ) AS Email
                                     ,LD.LastName + '', '' + LD.FirstName + '' '' + ISNULL(LD.MiddleName, '''') AS Student
                                     ,LD.LeadId
                                     ,SE.StuEnrollId
                                     ,LD.StudentNumber AS EnrollmentID
                                     ,[@EnrollmentsWithLastStatus].StatusCodeDescription AS Status
                                     ,[@EnrollmentsWithLastStatus].SysStatusId AS SysStatusId
                                     ,[@EnrollmentsWithLastStatus].DropReasonId AS DropReasonId
                                     ,SE.StartDate AS StartDate
                                     ,SE.ContractedGradDate AS ContractedGradDate
                                     ,dbo.GetGraduatedDate(SE.StuEnrollId) AS ExpectedGradDate
                                     ,SE.ReEnrollmentDate AS ReEnrollmentDate
                                     ,dbo.GetLDA(SE.StuEnrollId) AS LDA
                                     ,SE.LicensureWrittenAllParts AS LicensureWrittenAllParts
                                     ,SE.LicensureLastPartWrittenOn AS LicensureLastPartWrittenOn
                                     ,SE.LicensurePassedAllParts AS LicensurePassedAllParts
                                     ,camp.AllowGraduateAndStillOweMoney AS AllowsMoneyOwed
                                     ,camp.AllowGraduateWithoutFullCompletion AS AllowsIncompleteReq
                                     ,SE.ThirdPartyContract AS ThirdPartyContract
                           FROM       dbo.arStuEnrollments AS SE
                           INNER JOIN dbo.adLeads AS LD ON LD.LeadId = SE.LeadId
                           INNER JOIN dbo.arPrgVersions AS PV ON SE.PrgVerId = PV.PrgVerId
                           INNER JOIN dbo.arPrograms AS Prog ON Prog.ProgId = PV.ProgId
                           INNER JOIN dbo.syApprovedNACCASProgramVersion NaccasApproved ON NaccasApproved.ProgramVersionId = SE.PrgVerId
                           INNER JOIN dbo.syNaccasSettings NaccasSettings ON NaccasSettings.NaccasSettingId = NaccasApproved.NaccasSettingId
                           INNER JOIN dbo.syCampuses AS camp ON camp.CampusId = SE.CampusId
                           INNER JOIN @EnrollmentsWithLastStatus ON [@EnrollmentsWithLastStatus].StuEnrollId = SE.StuEnrollId
                           WHERE      SE.CampusId = @CampusId
                                      AND NaccasSettings.CampusId = @CampusId
                                      --that have a revised graduation date between January 1 of reporting year minus 1 year and December 31st of reporting year minus 1 year

                                      AND NaccasApproved.IsApproved = 1
                                      AND SE.ContractedGradDate
                                      BETWEEN CONVERT(DATETIME, CONVERT(VARCHAR(50), (( @ReportYear - 1 ) * 10000 + @StartMonth * 100 + @StartDay )), 112) AND CONVERT(
                                                                                                                                                                          DATETIME
                                                                                                                                                                         ,CONVERT(
                                                                                                                                                                                     VARCHAR(50)
                                                                                                                                                                                    ,(( @ReportYear
                                                                                                                                                                                        - 1
                                                                                                                                                                                      )
                                                                                                                                                                                      * 10000
                                                                                                                                                                                      + @EndMonth
                                                                                                                                                                                      * 100
                                                                                                                                                                                      + @EndDay
                                                                                                                                                                                     )
                                                                                                                                                                                 )
                                                                                                                                                                         ,112
                                                                                                                                                                      )
                           ) enr;



        WITH A
        AS (
           --Has dropped from a program whose length is less than one academic year of 900 hours after being enrolled for less than or equal to 15 calendar days
           --Has dropped from a program whose length is equal to or more than one academic year of 900 hours after being enrolled for less than or equal to 30 calendar DAYS
           SELECT se.PrgID
                 ,se.PrgDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
                 ,@EarlyWithdrawalReason AS ReasonNumber
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND (
                      (
                      se.ProgramHours < 900
                      AND DATEDIFF(DAY, se.StartDate, se.LDA) <= 15
                      )
                      OR (
                         se.ProgramHours >= 900
                         AND DATEDIFF(DAY, se.StartDate, se.LDA) <= 30
                         )
                      ))
            ,B
        AS (
           --exists in naccas drop reason
           SELECT se.PrgID
                 ,se.PrgDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
                 ,@DeployedReason AS ReasonNumber
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND ( EXISTS (
                               SELECT     *
                               FROM       dbo.syNACCASDropReasonsMapping nm
                               INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                               WHERE      ns.CampusId = @CampusId
                                          AND se.DropReasonId = nm.ADVDropReasonId
                                          AND nm.NACCASDropReasonId IN ( @MilitaryTransfer, @CallToActiveDuty )
                               )
                      ))
            ,C
        AS (
           --exists in naccas drop reason
           SELECT se.PrgID
                 ,se.PrgDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
                 ,@DeceasedReason AS ReasonNumber
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND ( EXISTS (
                               SELECT     *
                               FROM       dbo.syNACCASDropReasonsMapping nm
                               INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                               WHERE      ns.CampusId = @CampusId
                                          AND se.DropReasonId = nm.ADVDropReasonId
                                          AND nm.NACCASDropReasonId IN ( @Deceased )
                               )
                      ))
            ,D
        AS (
           --exists in naccas drop reason
           SELECT se.PrgID
                 ,se.PrgDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
                 ,@TempOrPermDisabledReason AS ReasonNumber
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND ( EXISTS (
                               SELECT     *
                               FROM       dbo.syNACCASDropReasonsMapping nm
                               INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                               WHERE      ns.CampusId = @CampusId
                                          AND se.DropReasonId = nm.ADVDropReasonId
                                          AND nm.NACCASDropReasonId IN ( @TemporarilyDisabled, @PermanentlyDisabled )
                               )
                      ))
            ,E
        AS (
           --exists in naccas drop reason
           SELECT se.PrgID
                 ,se.PrgDescrip
                 ,se.ProgramHours
                 ,se.ProgramCredits
                 ,se.SSN
                 ,se.PhoneNumber
                 ,se.Email
                 ,se.Student
                 ,se.LeadId
                 ,se.StuEnrollId
                 ,se.EnrollmentID
                 ,se.Status
                 ,se.SysStatusId
                 ,se.DropReasonId
                 ,se.StartDate
                 ,se.ContractedGradDate
                 ,se.ExpectedGradDate
                 ,se.LDA
                 ,se.LicensureWrittenAllParts
                 ,se.LicensureLastPartWrittenOn
                 ,se.LicensurePassedAllParts
                 ,se.AllowsMoneyOwed
                 ,se.AllowsIncompleteReq
                 ,@TransferredAccreditedReason AS ReasonNumber
           FROM   @Enrollments se
           WHERE  se.SysStatusId IN ( @Dropped )
                  AND ( EXISTS (
                               SELECT     *
                               FROM       dbo.syNACCASDropReasonsMapping nm
                               INNER JOIN dbo.syNaccasSettings ns ON ns.NaccasSettingId = nm.NaccasSettingId
                               WHERE      ns.CampusId = @CampusId
                                          AND se.DropReasonId = nm.ADVDropReasonId
                                          AND nm.NACCASDropReasonId IN ( @TransferredToEquivalent )
                               )
                      ))
            ,F
        AS ( SELECT     pe.PrgID
                       ,pe.PrgDescrip
                       ,pe.ProgramHours
                       ,pe.ProgramCredits
                       ,pe.SSN
                       ,pe.PhoneNumber
                       ,pe.Email
                       ,pe.Student
                       ,pe.LeadId
                       ,pe.StuEnrollId
                       ,pe.EnrollmentID
                       ,pe.Status
                       ,pe.SysStatusId
                       ,pe.DropReasonId
                       ,pe.StartDate
                       ,pe.ContractedGradDate
                       ,pe.ExpectedGradDate
                       ,pe.LDA
                       ,pe.LicensureWrittenAllParts
                       ,pe.LicensureLastPartWrittenOn
                       ,pe.LicensurePassedAllParts
                       ,pe.AllowsMoneyOwed
                       ,pe.AllowsIncompleteReq
                       ,@Transferred100Reason AS ReasonNumber
             FROM       @Enrollments se
             INNER JOIN @Enrollments pe ON pe.StuEnrollId = se.TransferredProgram
             WHERE      (
                        (
                        se.TotalTransferHours IS NOT NULL
                        AND se.TotalTransferHours <> 0
                        )
                        AND (( se.TransferHoursFromProgram / pe.ProgramHours ) = 1 )
                        ))
        SELECT   *
        FROM     (
                 SELECT *
                       ,ROW_NUMBER() OVER ( PARTITION BY resultByProg.LeadId
                                            ORDER BY resultByProg.LeadId
                                          ) numOfReasons
                 FROM   (
                        SELECT result.PrgID
                              ,result.PrgDescrip
                              ,result.ProgramHours
                              ,result.ProgramCredits
                              ,result.SSN
                              ,result.Student
                              ,result.LeadId
                              ,result.StuEnrollId
                              ,result.EnrollmentID
                              ,result.Status
                              ,result.SysStatusId
                              ,result.StartDate
                              ,result.LDA
                              ,result.ReasonNumber
                        FROM   (
                               SELECT *
                               FROM   A
                               UNION
                               SELECT *
                               FROM   B
                               UNION
                               SELECT *
                               FROM   C
                               UNION
                               SELECT *
                               FROM   D
                               UNION
                               SELECT *
                               FROM   E
                               UNION
                               SELECT *
                               FROM   F
                               ) AS result
                        WHERE  (
                               @PrgIdList IS NULL
                               OR result.PrgID IN (
                                                  SELECT Val
                                                  FROM   MultipleValuesForReportParameters(@PrgIdList, '','', 1)
                                                  )
                               )
                        ) resultByProg
                 ) distinctResults
        WHERE    distinctResults.numOfReasons = 1
        ORDER BY distinctResults.Student;


    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]', 'P') IS NULL
EXEC sp_executesql N'-- =========================================================================================================
-- Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId
-- =========================================================================================================
CREATE PROCEDURE [dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]
    @StuEnrollIdList VARCHAR(MAX),
    @TermId VARCHAR(50) = NULL,
    @SysComponentTypeIdList VARCHAR(50) = NULL,
    @ReqId VARCHAR(50),
    @SysComponentTypeId VARCHAR(10) = NULL,
    @ShowIncomplete BIT = 1
AS
BEGIN
    DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
    DECLARE @SetGradeBookAt VARCHAR(50);
    DECLARE @CharInt INT;
    DECLARE @Counter AS INT;
    DECLARE @times AS INT;
    DECLARE @TermStartDate1 AS DATETIME;
    DECLARE @Score AS DECIMAL(18, 2);
    DECLARE @GrdCompDescrip AS VARCHAR(50);


    DECLARE @curId AS UNIQUEIDENTIFIER;
    DECLARE @curReqId AS UNIQUEIDENTIFIER;
    DECLARE @curStuEnrollId AS UNIQUEIDENTIFIER;
    DECLARE @curDescrip AS VARCHAR(50);
    DECLARE @curNumber AS INT;
    DECLARE @curGrdComponentTypeId AS INT;
    DECLARE @curMinResult AS DECIMAL(18, 2);
    DECLARE @curGrdComponentDescription AS VARCHAR(50);
    DECLARE @curClsSectionId AS UNIQUEIDENTIFIER;
    DECLARE @curRownumber AS INT;
		 DECLARE @curRowSeq AS INT;
    DECLARE @ExecutedSproc BIT = 0;

    CREATE TABLE #Temp21
    (
        Id UNIQUEIDENTIFIER,
        TermId UNIQUEIDENTIFIER,
        ReqId UNIQUEIDENTIFIER,
        GradeBookDescription VARCHAR(50),
        Number INT,
        GradeBookSysComponentTypeId INT,
        GradeBookScore DECIMAL(18, 2),
        MinResult DECIMAL(18, 2),
        GradeComponentDescription VARCHAR(50),
        ClsSectionId UNIQUEIDENTIFIER,
        StuEnrollId UNIQUEIDENTIFIER,
        RowNumber INT
			,Seq int
    );
    CREATE TABLE #Temp22
    (
        ReqId UNIQUEIDENTIFIER,
        EffectiveDate DATETIME
    );

    IF @SysComponentTypeIdList IS NULL
    BEGIN
        SET @SysComponentTypeIdList = ''501,544,502,499,503,500,533'';
    END;

    SET @StuEnrollCampusId = COALESCE(
                             (
                                 SELECT TOP 1
                                        ASE.CampusId
                                 FROM dbo.arStuEnrollments AS ASE
                                 WHERE EXISTS
                                 (
                                     SELECT Val
                                     FROM dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                     WHERE Val = ASE.StuEnrollId
                                 )
                             ),
                             NULL
                                     );
    SET @SetGradeBookAt = (dbo.GetAppSettingValueByKeyName(''GradeBookWeightingLevel'', @StuEnrollCampusId));
    IF (@SetGradeBookAt IS NOT NULL)
    BEGIN
        SET @SetGradeBookAt = LOWER(LTRIM(RTRIM(@SetGradeBookAt)));
    END;


    SET @CharInt =
    (
        SELECT CHARINDEX(@SysComponentTypeId, @SysComponentTypeIdList)
    );

    IF @SetGradeBookAt = ''instructorlevel''
    BEGIN
        SELECT CASE
                   WHEN GradeBookDescription IS NULL THEN
                       GradeComponentDescription
                   ELSE
                       GradeBookDescription
               END AS GradeBookDescription,
               MinResult,
               GradeBookScore,
               GradeComponentDescription,
               GradeBookSysComponentTypeId,
               StuEnrollId,
               rownumber
        FROM
        (
            SELECT AR2.ReqId,
                   AT.TermId,
                   CASE
                       WHEN AGBWD.Descrip IS NULL THEN
                           AGCT.Descrip
                       ELSE
                           AGBWD.Descrip
                   END AS GradeBookDescription,
                   (CASE
                        WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN
                            AGBWD.Number
                        ELSE
                    (
                        SELECT MIN(MinVal)
                        FROM arGradeScaleDetails GSD,
                             arGradeSystemDetails GSS
                        WHERE GSD.GrdSysDetailId = GSS.GrdSysDetailId
                              AND GSS.IsPass = 1
                              AND GSD.GrdScaleId = ACS.GrdScaleId
                    )
                    END
                   ) AS MinResult,
                   AGBWD.Required,
                   AGBWD.MustPass,
                   ISNULL(AGCT.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId,
                   AGBWD.Number,
                   SR.Resource AS GradeComponentDescription,
                   AGBWD.InstrGrdBkWgtDetailId,
                   ASE.StuEnrollId,
                   0 AS IsExternShip,
                   (CASE AGCT.SysComponentTypeId
                        WHEN 544 THEN
                        (
                            SELECT SUM(HoursAttended)
                            FROM arExternshipAttendance
                            WHERE StuEnrollId = ASE.StuEnrollId
                        )
                        ELSE
                    (
                        SELECT SUM(ISNULL(Score, 0))
                        FROM arGrdBkResults
                        WHERE StuEnrollId = ASE.StuEnrollId
                              AND InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                              AND ClsSectionId = ACS.ClsSectionId
                    )
                    END
                   ) AS GradeBookScore,
				   AGBWD.Seq,
                   ROW_NUMBER() OVER (PARTITION BY ASE.StuEnrollId,
                                                   AT.TermId,
                                                   AR2.ReqId
                                      ORDER BY AGCT.SysComponentTypeId,
                                               AGBWD.Descrip
                                     ) AS rownumber
            FROM arGrdBkWgtDetails AS AGBWD
                INNER JOIN arGrdBkWeights AS AGBW
                    ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                INNER JOIN arClassSections AS ACS
                    ON ACS.InstrGrdBkWgtId = AGBW.InstrGrdBkWgtId
                INNER JOIN arResults AS AR
                    ON AR.TestId = ACS.ClsSectionId
                INNER JOIN arGrdComponentTypes AS AGCT
                    ON AGCT.GrdComponentTypeId = AGBWD.GrdComponentTypeId
                INNER JOIN arStuEnrollments AS ASE
                    ON ASE.StuEnrollId = AR.StuEnrollId
                INNER JOIN arTerm AS AT
                    ON AT.TermId = ACS.TermId
                INNER JOIN arReqs AS AR2
                    ON AR2.ReqId = ACS.ReqId
                INNER JOIN syResources AS SR
                    ON SR.ResourceID = AGCT.SysComponentTypeId
                LEFT JOIN arGrdBkResults AS AGBR
                    ON AGBR.StuEnrollId = ASE.StuEnrollId
                       AND AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                       AND AGBR.ClsSectionId = ACS.ClsSectionId
            WHERE AR.StuEnrollId IN
                  (
                      SELECT Val
                      FROM MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                  )
                  AND AT.TermId = @TermId
                  AND AR2.ReqId = @ReqId
                  AND
                  (
                      @SysComponentTypeIdList IS NULL
                      OR AGCT.SysComponentTypeId IN
                         (
                             SELECT Val
                             FROM MultipleValuesForReportParameters(@SysComponentTypeIdList, '','', 1)
                         )
                  )
            UNION
            SELECT AR2.ReqId,
                   AT.TermId,
                   CASE
                       WHEN AGBW.Descrip IS NULL THEN
                       (
                           SELECT Resource
                           FROM syResources
                           WHERE ResourceID = AGCT.SysComponentTypeId
                       )
                       ELSE
                           AGBW.Descrip
                   END AS GradeBookDescription,
                   (CASE
                        WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN
                            AGBWD.Number
                        ELSE
                    (
                        SELECT MIN(MinVal)
                        FROM arGradeScaleDetails GSD,
                             arGradeSystemDetails GSS
                        WHERE GSD.GrdSysDetailId = GSS.GrdSysDetailId
                              AND GSS.IsPass = 1
                              AND GSD.GrdScaleId = ACS.GrdScaleId
                    )
                    END
                   ) AS MinResult,
                   AGBWD.Required,
                   AGBWD.MustPass,
                   ISNULL(AGCT.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId,
                   AGBWD.Number,
                   (
                       SELECT Resource
                       FROM syResources
                       WHERE ResourceID = AGCT.SysComponentTypeId
                   ) AS GradeComponentDescription,
                   AGBWD.InstrGrdBkWgtDetailId,
                   ASE.StuEnrollId,
                   0 AS IsExternShip,
                   (CASE AGCT.SysComponentTypeId
                        WHEN 544 THEN
                        (
                            SELECT SUM(HoursAttended)
                            FROM arExternshipAttendance
                            WHERE StuEnrollId = ASE.StuEnrollId
                        )
                        ELSE
                    (
                        SELECT SUM(ISNULL(Score, 0))
                        FROM arGrdBkResults
                        WHERE StuEnrollId = ASE.StuEnrollId
                              AND InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                              AND ClsSectionId = ACS.ClsSectionId
                    )
                    END
                   ) AS GradeBookScore,
				   AGBWD.seq,
                   ROW_NUMBER() OVER (PARTITION BY ASE.StuEnrollId,
                                                   AT.TermId,
                                                   AR2.ReqId
                                      ORDER BY AGCT.SysComponentTypeId,
                                               AGCT.Descrip
                                     ) AS rownumber
            FROM arGrdBkConversionResults GBCR
                INNER JOIN arStuEnrollments AS ASE
                    ON ASE.StuEnrollId = GBCR.StuEnrollId
                INNER JOIN adLeads AS AST
                    ON AST.StudentId = ASE.StudentId
                INNER JOIN arPrgVersions AS APV
                    ON APV.PrgVerId = ASE.PrgVerId
                INNER JOIN arTerm AS AT
                    ON AT.TermId = GBCR.TermId
                INNER JOIN arReqs AS AR2
                    ON AR2.ReqId = GBCR.ReqId
                INNER JOIN arGrdComponentTypes AS AGCT
                    ON AGCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWgtDetails AS AGBWD
                    ON AGBWD.GrdComponentTypeId = AGCT.GrdComponentTypeId
                       AND AGBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWeights AS AGBW
                    ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                       AND AGBW.ReqId = GBCR.ReqId
                INNER JOIN
                (
                    SELECT ReqId,
                           MAX(EffectiveDate) AS EffectiveDate
                    FROM arGrdBkWeights AS AGBW
                    GROUP BY ReqId
                ) AS MaxEffectiveDatesByCourse
                    ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                INNER JOIN
                (
                    SELECT Resource,
                           ResourceID
                    FROM syResources
                    WHERE ResourceTypeID = 10
                ) SYRES
                    ON SYRES.ResourceID = AGCT.SysComponentTypeId
                INNER JOIN syCampuses AS SC
                    ON SC.CampusId = ASE.CampusId
                INNER JOIN arClassSections AS ACS
                    ON ACS.TermId = AT.TermId
                       AND ACS.ReqId = AR2.ReqId
                LEFT JOIN arGrdBkResults AS AGBR
                    ON AGBR.StuEnrollId = ASE.StuEnrollId
                       AND AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                       AND AGBR.ClsSectionId = ACS.ClsSectionId
            WHERE MaxEffectiveDatesByCourse.EffectiveDate <= AT.StartDate
                  AND ASE.StuEnrollId IN
                      (
                          SELECT Val
                          FROM MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                      )
                  AND AT.TermId = @TermId
                  AND AR2.ReqId = @ReqId
                  AND
                  (
                      @SysComponentTypeIdList IS NULL
                      OR AGCT.SysComponentTypeId IN
                         (
                             SELECT Val
                             FROM MultipleValuesForReportParameters(@SysComponentTypeIdList, '','', 1)
                         )
                  )
        ) dt
        WHERE (
                  @SysComponentTypeId IS NULL
                  OR dt.GradeBookSysComponentTypeId = @SysComponentTypeId
              )
              AND
              (
                  @ShowIncomplete = 1
                  OR
                  (
                      @ShowIncomplete = 0
                      AND dt.GradeBookScore IS NOT NULL
                  )
              )
        ORDER BY seq,GradeComponentDescription,
                 rownumber,
                 GradeBookDescription;
    END;
    ELSE
    BEGIN

        SET @TermStartDate1 =
        (
            SELECT AT.StartDate FROM dbo.arTerm AS AT WHERE AT.TermId = @TermId
        );
        INSERT INTO #Temp22
        SELECT AGBW.ReqId,
               MAX(AGBW.EffectiveDate) AS EffectiveDate
        FROM dbo.arGrdBkWeights AS AGBW
            INNER JOIN dbo.syCreditSummary AS SCS
                ON SCS.ReqId = AGBW.ReqId
        WHERE SCS.TermId = @TermId
              AND SCS.StuEnrollId IN
                  (
                      SELECT Val
                      FROM dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                  )
              AND AGBW.EffectiveDate <= @TermStartDate1
        GROUP BY AGBW.ReqId;


        DECLARE getUsers_Cursor CURSOR FOR
        SELECT dt.ID,
               dt.ReqId,
               dt.Descrip,
               dt.Number,
               dt.SysComponentTypeId,
               dt.MinResult,
               dt.GradeComponentDescription,
               dt.ClsSectionId,
               dt.StuEnrollId,
               ROW_NUMBER() OVER (PARTITION BY dt.StuEnrollId,
                                               @TermId,
                                               dt.SysComponentTypeId
                                  ORDER BY dt.SysComponentTypeId,
                                           dt.Descrip
                                 ) AS rownumber,
								 dt.Seq
        FROM
        (
            SELECT ISNULL(AGBWD.InstrGrdBkWgtDetailId, NEWID()) AS ID,
                   AR.ReqId,
                   AGBWD.Descrip,
                   AGBWD.Number,
                   AGCT.SysComponentTypeId,
                   (CASE
                        WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN
                            AGBWD.Number
                        ELSE
                    (
                        SELECT MIN(AGSD.MinVal) AS MinVal
                        FROM dbo.arGradeScaleDetails AS AGSD
                            INNER JOIN dbo.arGradeSystemDetails AS AGSDetails
                                ON AGSDetails.GrdSysDetailId = AGSD.GrdSysDetailId
                        WHERE AGSDetails.IsPass = 1
                              AND AGSD.GrdScaleId = ACS.GrdScaleId
                    )
                    END
                   ) AS MinResult,
                   SR.Resource AS GradeComponentDescription,
                   ACS.ClsSectionId,
                   AGBR.StuEnrollId,
				   AGBWD.Seq
            FROM dbo.arGrdBkResults AS AGBR
                INNER JOIN dbo.arGrdBkWgtDetails AS AGBWD
                    ON AGBWD.InstrGrdBkWgtDetailId = AGBR.InstrGrdBkWgtDetailId
                INNER JOIN dbo.arGrdBkWeights AS AGBW
                    ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                INNER JOIN dbo.arGrdComponentTypes AS AGCT
                    ON AGCT.GrdComponentTypeId = AGBWD.GrdComponentTypeId
                INNER JOIN dbo.syResources AS SR
                    ON SR.ResourceID = AGCT.SysComponentTypeId
                --                             INNER JOIN #Temp22 AS T2 ON T2.ReqId = AGBW.ReqId
                INNER JOIN dbo.arReqs AS AR
                    ON AR.ReqId = AGBW.ReqId
                INNER JOIN dbo.arClassSections AS ACS
                    ON ACS.ReqId = AR.ReqId
                       AND ACS.ClsSectionId = AGBR.ClsSectionId
                INNER JOIN dbo.arResults AS AR2
                    ON AR2.TestId = ACS.ClsSectionId
                       AND AR2.StuEnrollId = AGBR.StuEnrollId --AND AR2.TestId = AGBR.ClsSectionId

            WHERE (
                      @StuEnrollIdList IS NULL
                      OR AGBR.StuEnrollId IN
                         (
                             SELECT Val
                             FROM dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                         )
                  )
                  --                                        AND T2.EffectiveDate = AGBW.EffectiveDate
                  AND AGBWD.Number > 0
                  AND AR.ReqId = @ReqId
        ) dt
        ORDER BY SysComponentTypeId,
                 rownumber;

        DECLARE @schoolTypeSetting VARCHAR(50);
        SET @schoolTypeSetting = dbo.GetAppSettingValueByKeyName(''gradesformat'', NULL);


        OPEN getUsers_Cursor;
        FETCH NEXT FROM getUsers_Cursor
        INTO @curId,
             @curReqId,
             @curDescrip,
             @curNumber,
             @curGrdComponentTypeId,
             @curMinResult,
             @curGrdComponentDescription,
             @curClsSectionId,
             @curStuEnrollId,
             @curRownumber,
			 @curRowSeq
        SET @Counter = 0;
        WHILE @@FETCH_STATUS = 0
        BEGIN
            --PRINT @Number; 

            IF @schoolTypeSetting = ''Numeric''
            BEGIN
                SET @times = 1;

                SET @GrdCompDescrip = @curDescrip;
                IF (@curGrdComponentTypeId <> 500)
                BEGIN
                    SET @Score =
                    (
                        SELECT TOP 1
                               Score
                        FROM arGrdBkResults
                        WHERE StuEnrollId = @curStuEnrollId
                              AND InstrGrdBkWgtDetailId = @curId
                              AND ResNum = @times
                    );
                    IF @Score IS NULL
                    BEGIN
                        SET @Score =
                        (
                            SELECT TOP 1
                                   Score
                            FROM arGrdBkResults
                            WHERE StuEnrollId = @curStuEnrollId
                                  AND InstrGrdBkWgtDetailId = @curId
                                  AND ResNum = (@times - 1)
                        );
                    END;

                    IF @curGrdComponentTypeId = 544
                    BEGIN
                        SET @Score =
                        (
                            SELECT SUM(HoursAttended)
                            FROM arExternshipAttendance
                            WHERE StuEnrollId = @curStuEnrollId
                        );
                    END;
                    INSERT INTO #Temp21
                    VALUES
                    (@curId, @TermId, @curReqId, @GrdCompDescrip, @curNumber, @curGrdComponentTypeId, @Score,
                     @curMinResult, @curGrdComponentDescription, @curClsSectionId, @curStuEnrollId, @curRownumber, @curRowSeq);
                END;
                ELSE
                BEGIN
                    IF @curGrdComponentTypeId = 500
                       AND @ExecutedSproc = 0
                    BEGIN


                        DECLARE @Services TABLE
                        (
                            Descrip VARCHAR(50),
                            Required DECIMAL(18, 2) NULL,
                            Completed INT NULL,
                            Remaining DECIMAL(18, 2) NULL,
                            Average DECIMAL(18, 2) NULL,
                            EnrollDate DATETIME NULL,
                            InstrGrdBkWgtDetailId UNIQUEIDENTIFIER,
                            ClsSectionId UNIQUEIDENTIFIER,
							Seq INT NULL
                        );


                        INSERT INTO @Services
                        (
                            Descrip,
                            Required,
                            Completed,
                            Remaining,
                            Average,
                            EnrollDate,
                            InstrGrdBkWgtDetailId,
                            ClsSectionId,
							Seq
                        )
                        EXEC dbo.usp_GetClassroomWorkServices @StuEnrollId = @curStuEnrollId,   -- uniqueidentifier
                                                              @ClsSectionId = @curClsSectionId, -- uniqueidentifier
                                                              @GradeBookComponentType = 500;    -- int



                        INSERT INTO #Temp21
                        SELECT @curId,
                               @TermId,
                               @curReqId,
                               LTRIM(RTRIM(Descrip)) + '' ('' + CAST(Completed AS VARCHAR(10)) + ''/''
                               + CAST(Required AS VARCHAR(10)) + '')'',
                               @curNumber,
                               @curGrdComponentTypeId,
                               Average,
                               [Required],
                               @curGrdComponentDescription,
                               @curClsSectionId,
                               @curStuEnrollId,
                               @curRownumber,
							   @curRowSeq
                        FROM @Services;


                        SET @ExecutedSproc = 1;
                    END;
                END;



            END;
            ELSE
            BEGIN

                SET @times = 1;
                WHILE @times <= @curNumber
                BEGIN
                    --PRINT @times; 
                    IF @curNumber > 1
                    BEGIN
                        SET @GrdCompDescrip = @curDescrip + CAST(@times AS CHAR);

                        IF @curGrdComponentTypeId = 544
                        BEGIN
                            SET @Score =
                            (
                                SELECT SUM(HoursAttended)
                                FROM arExternshipAttendance
                                WHERE StuEnrollId = @curStuEnrollId
                            );
                        END;
                        ELSE
                        BEGIN
                            SET @Score =
                            (
                                SELECT Score
                                FROM arGrdBkResults
                                WHERE StuEnrollId = @curStuEnrollId
                                      AND InstrGrdBkWgtDetailId = @curId
                                      AND ResNum = @times
                                      AND ClsSectionId = @curClsSectionId
                            );
                        END;


                        SET @curRownumber = @times;
                    END;
                    ELSE
                    BEGIN
                        SET @GrdCompDescrip = @curDescrip;
                        SET @Score =
                        (
                            SELECT TOP 1
                                   Score
                            FROM arGrdBkResults
                            WHERE StuEnrollId = @curStuEnrollId
                                  AND InstrGrdBkWgtDetailId = @curId
                                  AND ResNum = @times
                        );
                        IF @Score IS NULL
                        BEGIN
                            SET @Score =
                            (
                                SELECT TOP 1
                                       Score
                                FROM arGrdBkResults
                                WHERE StuEnrollId = @curStuEnrollId
                                      AND InstrGrdBkWgtDetailId = @curId
                                      AND ResNum = (@times - 1)
                            );
                        END;

                        IF @curGrdComponentTypeId = 544
                        BEGIN
                            SET @Score =
                            (
                                SELECT SUM(HoursAttended)
                                FROM arExternshipAttendance
                                WHERE StuEnrollId = @curStuEnrollId
                            );
                        END;

                    END;
                    --PRINT @Score; 
                    INSERT INTO #Temp21
                    VALUES
                    (@curId, @TermId, @curReqId, @GrdCompDescrip, @curNumber, @curGrdComponentTypeId, @Score,
                     @curMinResult, @curGrdComponentDescription, @curClsSectionId, @curStuEnrollId, @curRownumber);

                    SET @times = @times + 1;
                END;

            END;
            FETCH NEXT FROM getUsers_Cursor
            INTO @curId,
                 @curReqId,
                 @curDescrip,
                 @curNumber,
                 @curGrdComponentTypeId,
                 @curMinResult,
                 @curGrdComponentDescription,
                 @curClsSectionId,
                 @curStuEnrollId,
                 @curRownumber,
				 @curRowSeq
        END;
        CLOSE getUsers_Cursor;
        DEALLOCATE getUsers_Cursor;

        SET @CharInt =
        (
            SELECT CHARINDEX(@SysComponentTypeId, @SysComponentTypeIdList)
        );

        IF (@SysComponentTypeIdList IS NULL)
           OR (@CharInt >= 1)
        BEGIN
            SELECT Id,
                   --, StuEnrollId 
                   TermId,
                   GradeBookDescription,
                   Number,
                   GradeBookSysComponentTypeId,
                   GradeBookScore,
                   MinResult,
                   GradeComponentDescription,
                   RowNumber,
                   ClsSectionId
				      ,Seq
            FROM #Temp21
            WHERE GradeBookSysComponentTypeId = @SysComponentTypeId
			AND
              (
                  @ShowIncomplete = 1
                  OR
                  (
                      @ShowIncomplete = 0
                      AND GradeBookScore IS NOT NULL
                  )
              )
            UNION
            SELECT GBWD.InstrGrdBkWgtDetailId,
                   --, SE.StuEnrollId 
                   T.TermId,
                   GCT.Descrip AS GradeBookDescription,
                   GBWD.Number,
                   GCT.SysComponentTypeId,
                   GBCR.Score,
                   GBCR.MinResult,
                   SYRES.Resource,
                   ROW_NUMBER() OVER (PARTITION BY --SE.StuEnrollId,  
                                          T.TermId,
                                          GCT.SysComponentTypeId
                                      ORDER BY GCT.SysComponentTypeId,
                                               GCT.Descrip
                                     ) AS rownumber,
                   (
                       SELECT TOP 1
                              ClsSectionId
                       FROM arClassSections
                       WHERE TermId = T.TermId
                             AND ReqId = R.ReqId
                   ) AS ClsSectionId
				      ,GBWD.Seq
            FROM arGrdBkConversionResults GBCR
                INNER JOIN arStuEnrollments SE
                    ON GBCR.StuEnrollId = SE.StuEnrollId
                INNER JOIN
                (SELECT StudentId, FirstName, LastName, MiddleName FROM adLeads) S
                    ON S.StudentId = SE.StudentId
                INNER JOIN arPrgVersions PV
                    ON SE.PrgVerId = PV.PrgVerId
                INNER JOIN arTerm T
                    ON GBCR.TermId = T.TermId
                INNER JOIN arReqs R
                    ON GBCR.ReqId = R.ReqId
                INNER JOIN arGrdComponentTypes GCT
                    ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWgtDetails GBWD
                    ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                       AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWeights GBW
                    ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                       AND GBCR.ReqId = GBW.ReqId
                INNER JOIN
                (
                    SELECT ReqId,
                           MAX(EffectiveDate) AS EffectiveDate
                    FROM arGrdBkWeights
                    GROUP BY ReqId
                ) AS MaxEffectiveDatesByCourse
                    ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                INNER JOIN
                (
                    SELECT Resource,
                           ResourceID
                    FROM syResources
                    WHERE ResourceTypeID = 10
                ) SYRES
                    ON SYRES.ResourceID = GCT.SysComponentTypeId
                INNER JOIN syCampuses C
                    ON SE.CampusId = C.CampusId
            WHERE MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                  AND SE.StuEnrollId IN
                      (
                          SELECT Val
                          FROM MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                      )
                  AND T.TermId = @TermId
                  AND R.ReqId = @ReqId
                  AND
                  (
                      @SysComponentTypeIdList IS NULL
                      OR GCT.SysComponentTypeId IN
                         (
                             SELECT Val
                             FROM MultipleValuesForReportParameters(@SysComponentTypeIdList, '','', 1)
                         )
                  )
                  AND
                  (
                      @SysComponentTypeId IS NULL
                      OR GCT.SysComponentTypeId = @SysComponentTypeId
                  )
				  AND
              (
                  @ShowIncomplete = 1
                  OR
                  (
                      @ShowIncomplete = 0
                      AND GBCR.Score IS NOT NULL
                  )
              )
            ORDER BY GradeBookSysComponentTypeId,
			seq,
                     GradeBookDescription,
                     RowNumber;
        END;
    END;
    DROP TABLE #Temp22;
    DROP TABLE #Temp21;
END;
-- =========================================================================================================
-- END  --  Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId
-- =========================================================================================================



'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_TR_Sub03_StudentInfo]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub03_StudentInfo]', 'P') IS NULL
EXEC sp_executesql N'
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- =========================================================================================================
-- USP_TR_Sub03_StudentInfo 
-- =========================================================================================================
CREATE PROCEDURE [dbo].[USP_TR_Sub03_StudentInfo]
    @StuEnrollIdList NVARCHAR(MAX) = NULL
   ,@ShowMultipleEnrollments BIT = 0
   ,@StatusList NVARCHAR(MAX) = NULL
   ,@OrderBy NVARCHAR(1000) = NULL
AS
    BEGIN
        DECLARE @CoursesTakenTableName NVARCHAR(200);
        DECLARE @GPASummaryTableName NVARCHAR(200);
        -- StuEnrollIdList plus double Enrollments
        DECLARE @StuEnrollIdListWithDoubleEnrollments NVARCHAR(MAX);

        CREATE TABLE #Temp1
            (
                StudentId NVARCHAR(50)
               ,EnrollAcademicType NVARCHAR(50)
               ,StuEnrollIdList NVARCHAR(MAX)
            );
        CREATE TABLE #Temp2
            (
                StudentId UNIQUEIDENTIFIER
               ,SSN NVARCHAR(50)
               ,StuFirstName NVARCHAR(50)
               ,StuLastName NVARCHAR(50)
               ,StuMiddleName NVARCHAR(50)
               ,StuDOB DATETIME
               ,StudentNumber NVARCHAR(50)
               ,StuEmail NVARCHAR(50)
               ,StuAddress1 NVARCHAR(250)
               ,StuAddress2 NVARCHAR(250)
               ,StuCity NVARCHAR(250)
               ,StuStateDescrip NVARCHAR(100)
               ,StuZIP NVARCHAR(50)
               ,StuCountryDescrip NVARCHAR(50)
               ,StuPhone NVARCHAR(50)
               ,EnrollAcademicType NVARCHAR(50)
               ,StuEnrollIdList NVARCHAR(MAX)
            );

        IF @ShowMultipleEnrollments = 0
            BEGIN
                SET @StatusList = NULL;

                INSERT INTO #Temp1
                            SELECT     ASE.StudentId
                                      ,CASE WHEN (
                                                 APV.Credits > 0.0
                                                 AND APV.Hours > 0
                                                 )
                                                 OR (APV.IsContinuingEd = 1) THEN ''Credits-ClockHours''
                                            WHEN APV.Credits > 0.0 THEN ''Credits''
                                            WHEN APV.Hours > 0.0 THEN ''ClockHours''
                                            ELSE ''''
                                       END AcademicType
                                      ,CONVERT(NVARCHAR(50), ASE.StuEnrollId) AS StuEnrollIdList
                            FROM       arStuEnrollments ASE
                            INNER JOIN syStatusCodes SC ON SC.StatusCodeId = ASE.StatusCodeId
                            INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId
                            -- To get the Academic Type for each selected enrollment we go to Program version here
                            INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                            WHERE      (
                                       @StuEnrollIdList IS NULL
                                       OR ASE.StuEnrollId IN (
                                                             SELECT Val
                                                             FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                             )
                                       )
                            ORDER BY   ASE.StudentId
                                      ,ASE.StuEnrollId;
            END;
        ELSE
            BEGIN
                -- if the parameter ShowMultipleEnrollmnets is True we will update the table #Temp1 with all enrollments for student selected
                INSERT INTO #Temp1
                            SELECT     ASE.StudentId
                                      ,CASE WHEN (
             APV.Credits > 0.0
                                                 AND APV.Hours > 0
                                                 )
                                                 OR (APV.IsContinuingEd = 1) THEN ''Credits-ClockHours''
                                            WHEN APV.Credits > 0.0 THEN ''Credits''
                                            WHEN APV.Hours > 0.0 THEN ''ClockHours''
                                            ELSE ''''
                                       END AcademicType
                                      ,CONVERT(NVARCHAR(50), ASE.StuEnrollId) AS StuEnrollIdList
                            FROM       arStuEnrollments ASE
                            INNER JOIN syStatusCodes SC ON SC.StatusCodeId = ASE.StatusCodeId
                            INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId
                            -- To get the Academic Type for each selected enrollment we go to Program version here
                            INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                            WHERE      ASE.StudentId IN (
                                                        SELECT     DISTINCT AST.StudentId
                                                        FROM       arStudent AS AST
                                                        INNER JOIN arStuEnrollments AS ASE1 ON ASE1.StudentId = AST.StudentId
                                                        WHERE      (
                                                                   @StuEnrollIdList IS NULL
                                                                   OR ASE1.StuEnrollId IN (
                                                                                          SELECT Val
                                                                                          FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                                          )
                                                                   )
                                                        )
                            ORDER BY   ASE.StudentId
                                      ,ASE.StuEnrollId;
            END;

        IF (@ShowMultipleEnrollments = 1)
            -- if the parameter ShowMultipleEnrollmnets is True we will update the table #Temp1 with all enrollments for student selected
            -- but T1 table must be filtered for unique studentId
            BEGIN
                -- Create stuEnrollIdList for all student selected
                UPDATE     T4
                SET        T4.StuEnrollIdList = T2.List
                FROM       #Temp1 AS T4
                INNER JOIN (
                           SELECT      T1.StudentId
                                      ,T1.EnrollAcademicType
                                      ,LEFT(T3.List, LEN(T3.List) - 1) AS List
                           FROM        #Temp1 AS T1
                           CROSS APPLY (
                                       SELECT CONVERT(VARCHAR(50), T2.StuEnrollIdList) + '',''
                                       FROM   #Temp1 AS T2
                                       WHERE  T2.StudentId = T1.StudentId
                                              AND T2.EnrollAcademicType = T1.EnrollAcademicType
                                       FOR XML PATH('''')
                                       ) T3(List)
                           ) T2 ON T2.StudentId = T4.StudentId
                                   AND T2.EnrollAcademicType = T4.EnrollAcademicType;

            END;

        SELECT @StuEnrollIdListWithDoubleEnrollments = COALESCE(@StuEnrollIdListWithDoubleEnrollments + '', '', '''') + T1.StuEnrollIdList
        FROM   #Temp1 AS T1;

        EXECUTE dbo.USP_TR_Sub03_PrepareGPA @StuEnrollIdList = @StuEnrollIdListWithDoubleEnrollments
                     ,@ShowMultipleEnrollments = @ShowMultipleEnrollments
                                           ,@CoursesTakenTableName = @CoursesTakenTableName OUTPUT
                                           ,@GPASummaryTableName = @GPASummaryTableName OUTPUT;


        INSERT INTO #Temp2 (
                           StudentId
                          ,SSN
                          ,StuFirstName
                          ,StuLastName
                          ,StuMiddleName
                          ,StuDOB
                          ,StudentNumber
                          ,StuEmail
                          ,StuAddress1
                          ,StuAddress2
                          ,StuCity
                          ,StuStateDescrip
                          ,StuZIP
                          ,StuCountryDescrip
                          ,StuPhone
                          ,EnrollAcademicType
                          ,StuEnrollIdList
                           )
                    SELECT          AST.StudentId AS StudentId
                                   ,AST.SSN AS SSN
                                   ,AST.FirstName AS StuFirstName
                                   ,AST.LastName AS StuLastName
                                   ,AST.MiddleName AS StuMiddleName
                                   ,AST.DOB AS StuDOB
                                   ,AST.StudentNumber AS StudentNumber
                                   ,AST.HomeEmail AS StuEmail
                                   ,ASA.Address1 AS StuAddress1
                                   ,ASA.Address2 AS StuAddress2
                                   ,ASA.City AS StuCity
                                   ,ASA.StateDescrip AS StuStateDescrip
                                   ,ASA.Zip AS StuZIP
                                   ,ASA.CountryDescrip AS StuCountryDescrip
                                   ,ASP.Phone AS StuPhone
                                   ,T.EnrollAcademicType
                                   ,T.StuEnrollIdList
                    FROM            arStudent AS AST
                    INNER JOIN      (
                                    SELECT DISTINCT T1.StudentId
                                          ,T1.EnrollAcademicType
                                          ,T1.StuEnrollIdList
                                    FROM   #Temp1 AS T1
                                    ) AS T ON T.StudentId = AST.StudentId
                    LEFT OUTER JOIN (
                                    SELECT          ASA1.StudentId
                                                   ,ASA1.Address1
                                                   ,ASA1.Address2
                                                   ,ASA1.City
                                                   ,SS2.StateDescrip
                                                   ,ASA1.Zip
                                                   ,AC.CountryDescrip
                                                   ,SS.Status
                                    FROM            arStudAddresses AS ASA1
                                    INNER JOIN      syStatuses AS SS ON SS.StatusId = ASA1.StatusId
                                    LEFT OUTER JOIN adCountries AS AC ON AC.CountryId = ASA1.CountryId
                                    LEFT OUTER JOIN syStates AS SS2 ON SS2.StateId = ASA1.StateId
                                    WHERE           SS.Status = ''Active''
                                                    AND ASA1.default1 = 1
                                    ) AS ASA ON ASA.StudentId = AST.StudentId
                    LEFT OUTER JOIN (
                                    SELECT     ASP1.StudentId
                                              ,ASP1.Phone
                                              ,ASP1.StatusId
                                              ,SS1.Status
FROM       arStudentPhone AS ASP1
                                    INNER JOIN syStatuses AS SS1 ON SS1.StatusId = ASP1.StatusId
                                    WHERE      SS1.Status = ''Active''
                                               AND ASP1.default1 = 1
                                    ) AS ASP ON ASP.StudentId = AST.StudentId;

        -- SELECT AND SORT     
        SELECT   StudentId
                ,SSN
                ,StuFirstName
                ,StuLastName
                ,StuMiddleName
                ,StuDOB
                ,StudentNumber
                ,StuEmail
                ,StuAddress1
                ,StuAddress2
                ,StuCity
                ,StuStateDescrip
                ,StuZIP
                ,StuCountryDescrip
                ,StuPhone
                ,EnrollAcademicType
                ,StuEnrollIdList
                ,@CoursesTakenTableName AS CoursesTakenTableName
                ,@GPASummaryTableName AS GPASummaryTableName
        FROM     #Temp2
        ORDER BY CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,FirstName Asc,MiddleName Asc'' THEN (RANK() OVER (ORDER BY StuLastName
                                                                                                                           ,StuFirstName
                                                                                                                           ,StuMiddleName
                                                                                                                  )
                                                                                                     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,FirstName Asc,MiddleName Desc'' THEN (RANK() OVER (ORDER BY StuLastName
                                                                                                                            ,StuFirstName
                                                                                                                            ,StuMiddleName DESC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,FirstName Desc,MiddleName Asc'' THEN (RANK() OVER (ORDER BY StuLastName
                                                                                                                            ,StuFirstName DESC
                                                                                                                            ,StuMiddleName
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,FirstName Desc,MiddleName Desc'' THEN (RANK() OVER (ORDER BY StuLastName
                                                                                                                             ,StuFirstName DESC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                    )
                                                                                                       )
                 END
                --LD
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,FirstName Asc,MiddleName Asc'' THEN (RANK() OVER (ORDER BY StuLastName DESC
    ,StuFirstName
                                                                                                                            ,StuMiddleName
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,FirstName Asc,MiddleName Desc'' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                             ,StuFirstName
                                                                                                                             ,StuMiddleName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,FirstName Desc,MiddleName Asc'' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                             ,StuFirstName DESC
                                                                                                                             ,StuMiddleName
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,FirstName Desc,MiddleName Desc'' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                              ,StuFirstName DESC
                                                                                                                              ,StuMiddleName DESC
                                                                                                                     )
                                                                                                        )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,MiddleName Asc,FirstName Asc'' THEN (RANK() OVER (ORDER BY StuLastName ASC
                                                                                                                           ,StuMiddleName ASC
                                                                                                                           ,StuFirstName ASC
                                                                                                                  )
                                                                                                     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,MiddleName Asc,FirstName Desc'' THEN (RANK() OVER (ORDER BY StuLastName ASC
                                                                                                                            ,StuMiddleName ASC
                                                                                                                            ,StuFirstName DESC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,MiddleName Desc,FirstName Asc'' THEN (RANK() OVER (ORDER BY StuLastName ASC
                                                       ,StuMiddleName DESC
                                                                                                                            ,StuFirstName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Asc,MiddleName Desc,FirstName Desc'' THEN (RANK() OVER (ORDER BY StuLastName ASC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                             ,StuFirstName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,MiddleName Asc,FirstName Asc'' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                            ,StuMiddleName ASC
                                                                                                                            ,StuFirstName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,MiddleName Asc,FirstName Desc'' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                             ,StuMiddleName ASC
                                                                                                                             ,StuFirstName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,MiddleName Desc,FirstName Asc'' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                             ,StuFirstName ASC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''LastName Desc,MiddleName Desc,FirstName Desc'' THEN (RANK() OVER (ORDER BY StuLastName DESC
                                                                                                                              ,StuMiddleName DESC
                                                                                                                              ,StuFirstName DESC
                                                                                                                     )
                                                                                                        )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,LastName Asc,MiddleName Asc'' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                           ,StuLastName ASC
                                                                                                                           ,StuMiddleName ASC
                                                                                                                  )
                                                                                                     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,LastName Asc,MiddleName Desc'' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                            ,StuLastName ASC
                                                                                                                            ,StuMiddleName DESC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,LastName Desc,MiddleName Asc'' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                            ,StuLastName DESC
                                                                                                                            ,StuMiddleName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,LastName Desc,MiddleName Desc'' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                             ,StuLastName DESC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                    )
                                                                                                       )
                 END
                --LD
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,LastName Asc,MiddleName Asc'' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                            ,StuLastName ASC
                                                                                                                            ,StuMiddleName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,LastName Asc,MiddleName Desc'' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                             ,StuLastName ASC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,LastName Desc,MiddleName Asc'' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                             ,StuLastName DESC
                                                                                                                             ,StuMiddleName ASC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,LastName Desc,StuMiddleName Desc'' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                                 ,StuLastName DESC
                                                                                                                                 ,StuMiddleName DESC
                                                                                                                        )
                                                                                                           )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,MiddleName Asc,LastName Asc'' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                           ,StuMiddleName ASC
                                                                                                                           ,StuLastName ASC
                                                                                                                  )
                                                                                                     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,MiddleName Asc,LastName Desc'' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                            ,StuMiddleName ASC
                                                                                                                            ,StuLastName DESC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,MiddleName Desc,LastName Asc'' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                            ,StuMiddleName DESC
                                                                                                                            ,StuLastName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Asc,MiddleName Desc,LastName Desc'' THEN (RANK() OVER (ORDER BY StuFirstName ASC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                             ,StuLastName DESC
                                                                                                                    )
     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,MiddleName Asc,LastName Asc'' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                            ,StuMiddleName ASC
                                                                                                                            ,StuLastName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,MiddleName Asc,LastName Desc'' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                             ,StuMiddleName ASC
                                                                                                                             ,StuLastName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,MiddleName Desc,LastName Asc'' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                             ,StuMiddleName DESC
                                                                                                                             ,StuLastName ASC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''FirstName Desc,MiddleName Desc,LastName Desc'' THEN (RANK() OVER (ORDER BY StuFirstName DESC
                                                                                                                              ,StuMiddleName DESC
                                                                                                                              ,StuLastName DESC
                                                                                                                     )
                                                                                                        )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,LastName Asc,FirstName Asc'' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                           ,StuLastName ASC
                                                                                                                           ,StuFirstName ASC
                                                                                                                  )
                                                                                                     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,LastName Asc,FirstName Desc'' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                            ,StuLastName ASC
                                                                                                                            ,StuFirstName DESC
                                                                                                                   )
                                                                         )
                 END

                -- Start here
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,LastName Desc,FirstName Asc'' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                            ,StuLastName DESC
                                                                                                                            ,StuFirstName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,LastName Desc,FirstName Desc'' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                             ,StuLastName DESC
                                                                                                                             ,StuFirstName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,LastName Asc,FirstName Asc'' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                            ,StuLastName ASC
                                                                                                                            ,StuFirstName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,LastName Asc,FirstName Desc'' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                             ,StuLastName ASC
                                                                                                                             ,StuFirstName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,LastName Desc,FirstName Asc'' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                             ,StuLastName DESC
                                                                                                                             ,StuFirstName ASC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,LastName Desc,FirstName Desc'' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                              ,StuLastName DESC
                                                                                                                              ,StuFirstName DESC
                                                                               )
                                                                                                        )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,FirstName Asc,LastName Asc'' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                           ,StuFirstName ASC
                                                                                                                           ,StuLastName ASC
                                                                                                                  )
                                                                                                     )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,FirstName Asc,LastName Desc'' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                            ,StuFirstName ASC
                                                                                                                            ,StuLastName DESC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,FirstName Desc,LastName Asc'' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                            ,StuFirstName DESC
                                                                                                                            ,StuLastName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Asc,FirstName Desc,LastName Desc'' THEN (RANK() OVER (ORDER BY StuMiddleName ASC
                                                                                                                             ,StuFirstName DESC
                                                                                                                             ,StuLastName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,FirstName Asc,LastName Asc'' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                            ,StuFirstName ASC
                                                                                                                            ,StuLastName ASC
                                                                                                                   )
                                                                                                      )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,FirstName Asc,LastName Desc'' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                             ,StuFirstName ASC
 ,StuLastName DESC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,FirstName Desc,LastName Asc'' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                             ,StuFirstName DESC
                                                                                                                             ,StuLastName ASC
                                                                                                                    )
                                                                                                       )
                 END
                ,CASE WHEN LTRIM(RTRIM(@OrderBy)) = ''MiddleName Desc,FirstName Desc,LastName Desc'' THEN (RANK() OVER (ORDER BY StuMiddleName DESC
                                                                                                                              ,StuFirstName DESC
                                                                                                                              ,StuLastName DESC
                                                                                                                     )
                                                                                                        )
                 END
                ,StudentId;
    END;
-- =========================================================================================================
-- END  --  USP_TR_Sub03_StudentInfo 
-- =========================================================================================================

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_TR_Sub11_ComponentsByCourse_GivenComponentTypeId]'
GO
IF OBJECT_ID(N'[dbo].[Usp_TR_Sub11_ComponentsByCourse_GivenComponentTypeId]', 'P') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[Usp_TR_Sub11_ComponentsByCourse_GivenComponentTypeId]
--EXEC Usp_TR_Sub11_ComponentsByCourse_GivenComponentTypeId ''2825E1F6-ADD1-4365-9E88-0028F7E9C567'',NULL,NULL,''5C188D11-7C81-45E5-8280-7E1BE8D2CC12'',NULL
    @StuEnrollIdList VARCHAR(MAX)
   ,@TermId VARCHAR(50) = NULL
   ,@SysComponentTypeIdList VARCHAR(50) = NULL
   ,@ReqId VARCHAR(50)
   ,@SysComponentTypeId VARCHAR(10) = NULL
AS
    DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
    DECLARE @GradeBookAt VARCHAR(50);
    DECLARE @CharInt INT;
    DECLARE @Counter AS INT;  
    DECLARE @times AS INT;
	--DECLARE @TermStartDate1 AS DATETIME;     
    DECLARE @Score AS DECIMAL(18,2);
    DECLARE @GrdCompDescrip AS VARCHAR(50);


    DECLARE @curId AS UNIQUEIDENTIFIER;
    DECLARE @curReqId AS UNIQUEIDENTIFIER;
    DECLARE @curStuEnrollId AS UNIQUEIDENTIFIER;
    DECLARE @curDescrip AS VARCHAR(50);
    DECLARE @curNumber AS INT;
    DECLARE @curGrdComponentTypeId AS INT;
    DECLARE @curMinResult AS DECIMAL(18,2); 
    DECLARE @curGrdComponentDescription AS VARCHAR(50);   
    DECLARE @curClsSectionId AS UNIQUEIDENTIFIER;
    DECLARE @curTermId AS UNIQUEIDENTIFIER;
    DECLARE @curRownumber AS INT;
	 DECLARE @curRowSeq AS INT;
    DECLARE @DefaulSysComponentTypeIdList VARCHAR(MAX);
	
    SET @DefaulSysComponentTypeIdList = ''501,544,502,499,503,500,533'';
 
    CREATE TABLE #Temp1
        (
         Id UNIQUEIDENTIFIER
        ,TermId UNIQUEIDENTIFIER
        ,ReqId UNIQUEIDENTIFIER
        ,GradeBookDescription VARCHAR(50)
        ,Number INT
        ,GradeBookSysComponentTypeId INT
        ,GradeBookScore DECIMAL(18,2)
        ,MinResult DECIMAL(18,2)
        ,GradeComponentDescription VARCHAR(50)
        ,ClsSectionId UNIQUEIDENTIFIER
        ,StuEnrollId UNIQUEIDENTIFIER
        ,RowNumber INT
		,Seq int
        );
    CREATE TABLE #Temp2
        (
         ReqId UNIQUEIDENTIFIER
        ,EffectiveDate DATETIME
        );

    IF @SysComponentTypeIdList IS NULL
        BEGIN
            SET @SysComponentTypeIdList = @DefaulSysComponentTypeIdList;
        END;

    SET @StuEnrollCampusId = COALESCE((
                                        SELECT TOP 1
                                                ASE.CampusId
                                        FROM    arStuEnrollments AS ASE
                                        WHERE   ASE.StuEnrollId IN ( SELECT Val
                                                                     FROM   MultipleValuesForReportParameters(@StuEnrollIdList,'','',1) )
                                      ),NULL);
    SET @GradeBookAt = ( dbo.GetAppSettingValueByKeyName(''GradeBookWeightingLevel'',@StuEnrollCampusId) );     

    IF ( @GradeBookAt IS NOT NULL )
        BEGIN
            SET @GradeBookAt = LOWER(LTRIM(RTRIM(@GradeBookAt)));
        END;

    SET @CharInt = (
                     SELECT CHARINDEX(@SysComponentTypeId,@SysComponentTypeIdList)
                   );

    
    IF @GradeBookAt = ''instructorlevel''
        BEGIN
            SELECT  CASE WHEN GradeBookDescription IS NULL THEN GradeComponentDescription
                         ELSE GradeBookDescription
                    END AS GradeBookDescription
                   ,MinResult
                   ,GradeBookScore
                   ,GradeComponentDescription
                   ,GradeBookSysComponentTypeId
                   ,StuEnrollId
                   ,rownumber
                   ,TermId
                   ,ReqId
            FROM    (
                      SELECT    AR2.ReqId
                               ,AT.TermId
                               ,CASE WHEN AGBWD.Descrip IS NULL THEN AGCT.Descrip
                                     ELSE AGBWD.Descrip
                                END AS GradeBookDescription
                               ,( CASE WHEN AGCT.SysComponentTypeId IN ( 500,503,544 ) THEN AGBWD.Number
                                       ELSE (
                                              SELECT    MIN(MinVal)
                                              FROM      arGradeScaleDetails GSD
                                                       ,arGradeSystemDetails GSS
                                              WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                        AND GSS.IsPass = 1
                                                        AND GSD.GrdScaleId = ACS.GrdScaleId
                                            )
                                  END ) AS MinResult
                               ,AGBWD.Required
                               ,AGBWD.MustPass
                               ,ISNULL(AGCT.SysComponentTypeId,0) AS GradeBookSysComponentTypeId
                               ,AGBWD.Number
                               ,SR.Resource AS GradeComponentDescription
                               ,AGBWD.InstrGrdBkWgtDetailId
                               ,ASE.StuEnrollId
                               ,0 AS IsExternShip
                               ,CASE AGCT.SysComponentTypeId
                                  WHEN 544 THEN (
                                                  SELECT    SUM(ISNULL(AEA.HoursAttended,0.0))
                                                  FROM      arExternshipAttendance AS AEA
                                                  WHERE     AEA.StuEnrollId = ASE.StuEnrollId
                                                )
                                  ELSE (
                                         SELECT SUM(ISNULL(AGBR1.Score,0.0))
                                         FROM   arGrdBkResults AS AGBR1
                                         WHERE  AGBR1.StuEnrollId = ASE.StuEnrollId
                                                AND AGBR1.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                                                AND AGBR1.ClsSectionId = ACS.ClsSectionId
                                       )
                                END AS GradeBookScore,
								AGBWD.Seq
                               ,ROW_NUMBER() OVER ( PARTITION BY ASE.StuEnrollId,AT.TermId,AR2.ReqId ORDER BY AGCT.SysComponentTypeId, AGBWD.Descrip ) AS rownumber
                      FROM      arGrdBkWgtDetails AS AGBWD
                      INNER JOIN arGrdBkResults AS AGBR ON AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId 
							--INNER JOIN arGrdBkWeights AS AGBW ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                      INNER JOIN arClassSections AS ACS ON ACS.ClsSectionId = AGBR.ClsSectionId
                      INNER JOIN arResults AS AR ON AR.TestId = ACS.ClsSectionId
                      INNER JOIN arGrdComponentTypes AS AGCT ON AGCT.GrdComponentTypeId = AGBWD.GrdComponentTypeId
                      INNER JOIN arStuEnrollments AS ASE ON ASE.StuEnrollId = AR.StuEnrollId
                      INNER JOIN arTerm AS AT ON AT.TermId = ACS.TermId
                      INNER JOIN arReqs AS AR2 ON AR2.ReqId = ACS.ReqId
                      INNER JOIN syResources AS SR ON SR.ResourceID = AGCT.SysComponentTypeId
                      WHERE     AR.StuEnrollId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@StuEnrollIdList,'','',1) )
                                AND AT.TermId = @TermId
                                AND AR2.ReqId = @ReqId
                                AND AGCT.SysComponentTypeId IN ( SELECT Val
                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeIdList,'','',1) )
                      UNION
                      SELECT    AR2.ReqId
                               ,AT.TermId
                               ,CASE WHEN AGBW.Descrip IS NULL THEN (
                                                                      SELECT    Resource
                                                                      FROM      syResources
                                                                      WHERE     ResourceID = AGCT.SysComponentTypeId
                                                                    )
                                     ELSE AGBW.Descrip
                                END AS GradeBookDescription
                               ,CASE WHEN AGCT.SysComponentTypeId IN ( 500,503,544 ) THEN AGBWD.Number
                                     ELSE (
                                            SELECT  MIN(MinVal)
                                            FROM    arGradeScaleDetails GSD
                                                   ,arGradeSystemDetails GSS
                                            WHERE   GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                    AND GSS.IsPass = 1
                                                    AND GSD.GrdScaleId = ACS.GrdScaleId
                                          )
                                END AS MinResult
                               ,AGBWD.Required
                               ,AGBWD.MustPass
                               ,ISNULL(AGCT.SysComponentTypeId,0) AS GradeBookSysComponentTypeId
                               ,AGBWD.Number
                               ,(
                                  SELECT    Resource
                                  FROM      syResources
                                  WHERE     ResourceID = AGCT.SysComponentTypeId
                                ) AS GradeComponentDescription
                               ,AGBWD.InstrGrdBkWgtDetailId
                               ,ASE.StuEnrollId
                               ,0 AS IsExternShip
                               ,CASE AGCT.SysComponentTypeId
                                  WHEN 544 THEN (
                                                  SELECT    SUM(ISNULL(AEA.HoursAttended,0.0))
                                                  FROM      arExternshipAttendance AS AEA
                                                  WHERE     AEA.StuEnrollId = ASE.StuEnrollId
                                                )
                                  ELSE (
                                         SELECT SUM(ISNULL(AGBR.Score,0.0))
                                         FROM   arGrdBkResults AS AGBR
                                         WHERE  AGBR.StuEnrollId = ASE.StuEnrollId
                                                AND AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                                                AND AGBR.ClsSectionId = ACS.ClsSectionId
                                       )
                                END AS GradeBookScore
								, AGBWD.Seq
                               ,ROW_NUMBER() OVER ( PARTITION BY ASE.StuEnrollId,AT.TermId,AR2.ReqId ORDER BY AGCT.SysComponentTypeId, AGCT.Descrip ) AS rownumber
                      FROM      arGrdBkConversionResults GBCR
                      INNER JOIN arStuEnrollments AS ASE ON ASE.StuEnrollId = GBCR.StuEnrollId
                      INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId
                      INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                      INNER JOIN arTerm AS AT ON AT.TermId = GBCR.TermId
                      INNER JOIN arReqs AS AR2 ON AR2.ReqId = GBCR.ReqId
                      INNER JOIN arGrdComponentTypes AS AGCT ON AGCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                      INNER JOIN arGrdBkWgtDetails AS AGBWD ON AGBWD.GrdComponentTypeId = AGCT.GrdComponentTypeId
                                                               AND AGBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                      INNER JOIN arGrdBkWeights AS AGBW ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                                                           AND AGBW.ReqId = GBCR.ReqId
                      INNER JOIN (
                                   SELECT   AGBW.ReqId
                                           ,MAX(AGBW.EffectiveDate) AS EffectiveDate
                                   FROM     arGrdBkWeights AS AGBW
                                   GROUP BY AGBW.ReqId
                                 ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                      INNER JOIN (
                                   SELECT   SR.Resource
                                           ,SR.ResourceID
                                   FROM     syResources AS SR
                                   WHERE    SR.ResourceTypeID = 10
                                 ) SYRES ON SYRES.ResourceID = AGCT.SysComponentTypeId
                      INNER JOIN syCampuses AS SC ON SC.CampusId = ASE.CampusId
                      INNER JOIN arClassSections AS ACS ON ACS.TermId = AT.TermId
                                                           AND ACS.ReqId = AR2.ReqId
                      LEFT JOIN arGrdBkResults AS AGBR ON AGBR.StuEnrollId = ASE.StuEnrollId
                                                          AND AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                                                          AND AGBR.ClsSectionId = ACS.ClsSectionId
                      WHERE     MaxEffectiveDatesByCourse.EffectiveDate <= AT.StartDate
                                AND ASE.StuEnrollId IN ( SELECT Val
                                                         FROM   MultipleValuesForReportParameters(@StuEnrollIdList,'','',1) )
                                AND AT.TermId = @TermId
                                AND AR2.ReqId = @ReqId
                                AND AGCT.SysComponentTypeId IN ( SELECT Val
                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeIdList,'','',1) )
                    ) dt
            WHERE   (
                      @SysComponentTypeId IS NULL
                      OR dt.GradeBookSysComponentTypeId = @SysComponentTypeId
                    )
            ORDER BY dt.Seq,
			 GradeComponentDescription
                   ,rownumber
                   ,GradeBookDescription; 
        END;
    ELSE
        BEGIN


            SELECT  AGBWD.InstrGrdBkWgtDetailId
                   ,AGBWD.InstrGrdBkWgtId
                   ,AGBWD.Code
                   ,AGBWD.Descrip
                   ,AGBWD.Weight
                   ,AGBWD.Seq
                   ,AGBWD.ModUser
                   ,AGBWD.ModDate
                   ,AGBWD.GrdComponentTypeId
                   ,AGBWD.Parameter
                   ,AGBWD.Number
                   ,AGBWD.GrdPolicyId
                   ,AGBWD.Required
                   ,AGBWD.MustPass
                   ,AGBWD.CreditsPerService
            INTO    #tmpInnerQueryTable
            FROM    arGrdBkWgtDetails AS AGBWD
            INNER JOIN arGrdBkResults AS AGBR ON AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
            INNER JOIN arClassSections AS ACS ON ACS.ClsSectionId = AGBR.ClsSectionId
            WHERE   AGBR.StuEnrollId IN ( SELECT    Val
                                          FROM      MultipleValuesForReportParameters(@StuEnrollIdList,'','',1) )
                    AND ACS.ReqId = @ReqId;

            SELECT  ReqId
                   ,MAX(EffectiveDate) AS EffectiveDate
            INTO    #tmpGrdBkWeightsWithMaxEffectiveDates
            FROM    arGrdBkWeights
            GROUP BY ReqId;
                                        --HAVING ReqId=@ReqId

			--SET @TermStartDate1 = (
			--                       SELECT   AT.StartDate
			--                       FROM     arTerm AS AT
			--                       WHERE    AT.TermId = @TermId
			--                      );
            INSERT  INTO #Temp2
                    SELECT  AGBW.ReqId
                           ,MAX(AGBW.EffectiveDate) AS EffectiveDate
                    FROM    arGrdBkWeights AS AGBW
                    INNER JOIN syCreditSummary AS SCS ON SCS.ReqId = AGBW.ReqId
                    WHERE   SCS.StuEnrollId IN ( SELECT Val
                                                 FROM   MultipleValuesForReportParameters(@StuEnrollIdList,'','',1) ) 
				   -- AND SCS.TermId = @TermId
				   -- AND AGBW.EffectiveDate <= @TermStartDate1
                            AND SCS.ReqId = @ReqId
                    GROUP BY AGBW.ReqId;

            DECLARE getUsers_Cursor CURSOR FAST_FORWARD FORWARD_ONLY
            FOR
                SELECT  dt.ID
                       ,dt.ReqId
                       ,dt.Descrip
                       ,dt.Number
                       ,dt.SysComponentTypeId
                       ,dt.MinResult
                       ,dt.GradeComponentDescription
                       ,dt.ClsSectionId
                       ,dt.StuEnrollId
                       ,dt.TermId
                       ,ROW_NUMBER() OVER ( PARTITION BY dt.StuEnrollId,dt.TermId,dt.SysComponentTypeId ORDER BY dt.SysComponentTypeId, dt.Descrip ) AS rownumber
					   ,dt.seq
                FROM    (
                          SELECT DISTINCT
                                    ISNULL(GD.InstrGrdBkWgtDetailId,NEWID()) AS ID
                                   ,AR.ReqId
                                   ,GC.Descrip
                                   ,GD.Number
                                   ,GC.SysComponentTypeId
                                   ,( CASE WHEN GC.SysComponentTypeId IN ( 500,503,544 ) THEN GD.Number
                                           ELSE (
                                                  SELECT    MIN(MinVal)
                                                  FROM      arGradeScaleDetails GSD
                                                           ,arGradeSystemDetails GSS
                                                  WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                            AND GSS.IsPass = 1
                                                            AND GSD.GrdScaleId = CS.GrdScaleId
                                                )
                                      END ) AS MinResult
                                   ,S.Resource AS GradeComponentDescription
                                   ,CS.ClsSectionId
                                   ,RES.StuEnrollId
                                   ,T.TermId
								   ,GD.Seq
                          FROM      arGrdComponentTypes AS GC
                          INNER JOIN #tmpInnerQueryTable AS GD ON GD.GrdComponentTypeId = GC.GrdComponentTypeId
                          INNER JOIN arGrdBkWeights AS AGBW1 ON AGBW1.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
                          INNER JOIN arReqs AS AR ON AR.ReqId = AGBW1.ReqId
                          INNER JOIN arClassSections AS CS ON CS.ReqId = AR.ReqId
                          INNER JOIN syResources AS S ON S.ResourceID = GC.SysComponentTypeId
                          INNER JOIN arResults AS RES ON RES.TestId = CS.ClsSectionId
                          INNER JOIN arTerm AS T ON T.TermId = CS.TermId
                          WHERE     RES.StuEnrollId IN ( SELECT Val
                                                         FROM   MultipleValuesForReportParameters(@StuEnrollIdList,'','',1) )
                                    AND GD.Number > 0
                                    AND AR.ReqId = @ReqId
                        ) dt
                ORDER BY SysComponentTypeId
                       ,rownumber;
            OPEN getUsers_Cursor;
            FETCH NEXT FROM getUsers_Cursor
					INTO @curId,@curReqId,@curDescrip,@curNumber,@curGrdComponentTypeId,@curMinResult,@curGrdComponentDescription,@curClsSectionId,
                @curStuEnrollId,@curTermId,@curRownumber, @curRowSeq;
            SET @Counter = 0;
            WHILE @@FETCH_STATUS = 0
                BEGIN
					--PRINT @Number;
                    SET @times = 1;
                    WHILE @times <= @curNumber
                        BEGIN
							--PRINT @times;
                            IF @curNumber > 1
                                BEGIN
                                    SET @GrdCompDescrip = @curDescrip + CAST(@times AS CHAR);
                                    IF @curGrdComponentTypeId = 544
                                        BEGIN
                                            SET @Score = (
                                                           SELECT   SUM(ISNULL(AEA.HoursAttended,0.0))
                                                           FROM     arExternshipAttendance AS AEA
                                                           WHERE    AEA.StuEnrollId = @curStuEnrollId
                                                         );
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @Score = (
                                                           SELECT   SUM(ISNULL(AGBR.Score,0.0))
                                                           FROM     arGrdBkResults AS AGBR
                                                           WHERE    AGBR.StuEnrollId = @curStuEnrollId
                                                                    AND AGBR.InstrGrdBkWgtDetailId = @curId
                                                                    AND AGBR.ResNum = @times
                                                                    AND AGBR.ClsSectionId = @curClsSectionId
                                                         );
                                        END;
                                    SET @curRownumber = @times;
                                END;
                            ELSE
                                BEGIN
                                    SET @GrdCompDescrip = @curDescrip;
                                    SET @Score = (
                                                   SELECT TOP 1
                                                            Score
                                                   FROM     arGrdBkResults
                                                   WHERE    StuEnrollId = @curStuEnrollId
                                                            AND InstrGrdBkWgtDetailId = @curId
                                                            AND ResNum = @times
                                                 );
                                    IF @Score IS NULL
                                        BEGIN
                                            SET @Score = (
                                                           SELECT TOP 1
                                                                    Score
                                                           FROM     arGrdBkResults
                                                           WHERE    StuEnrollId = @curStuEnrollId
                                                                    AND InstrGrdBkWgtDetailId = @curId
                                                                    AND ResNum = ( @times - 1 )
                                                         ); 
                                        END;
                                    IF @curGrdComponentTypeId = 544
                                        BEGIN
                                            SET @Score = (
                                                           SELECT   SUM(ISNULL(AEA.HoursAttended,0.0))
                                                           FROM     arExternshipAttendance AS AEA
                                                           WHERE    AEA.StuEnrollId = @curStuEnrollId
                                                         );
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @Score = (
                                                           SELECT   SUM(ISNULL(AGBR.Score,0.0))
                                                           FROM     arGrdBkResults AS AGBR
                                                           WHERE    AGBR.StuEnrollId = @curStuEnrollId
                                                                    AND AGBR.InstrGrdBkWgtDetailId = @curId
                                                                    AND AGBR.ResNum = @times
                                                         );
                                        END;
                                END;
							--PRINT @Score;
                            INSERT  INTO #Temp1
                            VALUES  ( @curId,@curTermId,@curReqId,@GrdCompDescrip,@curNumber,@curGrdComponentTypeId,@Score,@curMinResult,
                                      @curGrdComponentDescription,@curClsSectionId,@curStuEnrollId,@curRownumber, @curRowSeq );
							
                            SET @times = @times + 1;
                        END;

                    FETCH NEXT FROM getUsers_Cursor
					INTO @curId,@curReqId,@curDescrip,@curNumber,@curGrdComponentTypeId,@curMinResult,@curGrdComponentDescription,@curClsSectionId,
                        @curStuEnrollId,@curTermId,@curRownumber, @curRowSeq;
                END;
            CLOSE getUsers_Cursor; 
            DEALLOCATE getUsers_Cursor; 

            SET @CharInt = (
                             SELECT CHARINDEX(@SysComponentTypeId,@SysComponentTypeIdList)
                           );
		
            IF ( @CharInt >= 1 )
                BEGIN
                    SELECT  GradeBookDescription
                           ,MinResult
                           ,GradeBookScore
                           ,GradeComponentDescription
                           ,GradeBookSysComponentTypeId
                           ,StuEnrollId
                           ,RowNumber
                           ,TermId
                           ,ReqId
						   ,seq
                          --   Id  , Number, ClsSectionId
                    FROM    #Temp1
                    WHERE   GradeBookSysComponentTypeId = @SysComponentTypeId
                    UNION
                    SELECT  CASE WHEN GBWD.Descrip IS NULL THEN GCT.Descrip
                                 ELSE GBWD.Descrip
                            END AS GradeBookDescription
                           ,GBCR.MinResult AS MinResult
                           ,GBCR.Score AS GradeBookScore
                           ,SYRES.Resource AS GradeComponentDescription
                           ,ISNULL(GCT.SysComponentTypeId,0) AS GradeBookSysComponentTypeId
                           ,SE.StuEnrollId
                           ,ROW_NUMBER() OVER ( PARTITION BY T.TermId,GCT.SysComponentTypeId ORDER BY GCT.SysComponentTypeId, GCT.Descrip ) AS rownumber
                           ,T.TermId
                           ,R.ReqId
						   ,GBWD.Seq
                         --, GBWD.InstrGrdBkWgtDetailId, GBWD.Number, SYRES.Resource
                          --, (
                          --   SELECT TOP 1
                          --          ClsSectionId
                          --   FROM   arClassSections
                          --   WHERE  TermId = T.TermId
                          --          AND ReqId = R.ReqId
                          --  ) AS ClsSectionId
                    FROM    arGrdBkConversionResults GBCR
                    INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arStudent S ON S.StudentId = SE.StudentId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                    INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                    INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                    INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                         AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
							--INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
							--                                 AND GBCR.ReqId = GBW.ReqId
                    INNER JOIN #tmpGrdBkWeightsWithMaxEffectiveDates AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                    INNER JOIN (
                                 SELECT Resource
                                       ,ResourceID
                                 FROM   syResources
                                 WHERE  ResourceTypeID = 10
                               ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                    INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                    WHERE   MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                            AND SE.StuEnrollId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@StuEnrollIdList,'','',1) )
                            AND T.TermId = @TermId
                            AND R.ReqId = @ReqId
                            AND GCT.SysComponentTypeId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@SysComponentTypeIdList,'','',1) )
                            AND (
                                  @SysComponentTypeId IS NULL
                                  OR GCT.SysComponentTypeId = @SysComponentTypeId
                                )
                    ORDER BY GradeBookSysComponentTypeId,
					seq
                           ,GradeBookDescription
                           ,rownumber; 
                END; 
            DROP TABLE #tmpInnerQueryTable;
        END;
    DROP TABLE #Temp2;
    DROP TABLE #Temp1;
	


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
