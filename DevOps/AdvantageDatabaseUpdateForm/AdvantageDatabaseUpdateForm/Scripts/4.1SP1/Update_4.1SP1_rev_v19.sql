﻿--=================================================================================================
-- START  AD-16533 : Save the changes when you are navigating off the leadInfo page
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION leadUrl;
BEGIN TRY
UPDATE dbo.syMenuItems SET Url = '/AD/AleadInfoPage.aspx' WHERE url = '/AD/aLeadInfoPage.aspx'
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION leadUrl;
    PRINT 'Failed to update leadUrl for LeadInfoPage';
END;
ELSE
BEGIN
    COMMIT TRANSACTION leadUrl;
    PRINT 'updated leadUrl for LeadInfoPage';
END;
GO
--=================================================================================================
-- END  AD-16533 : Save the changes when you are navigating off the leadInfo page
--=================================================================================================