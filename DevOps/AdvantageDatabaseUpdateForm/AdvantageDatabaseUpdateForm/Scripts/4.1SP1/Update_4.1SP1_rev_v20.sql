﻿--=================================================================================================
-- START  AD-17208- Error when viewing student info
--=================================================================================================
BEGIN TRANSACTION ScriptTransaction;
BEGIN TRY

    DELETE FROM adLead_Notes
    WHERE NotesId IN
          (
              SELECT NotesId FROM dbo.AllNotes WHERE LEN(NoteText) = 0
          );

    DELETE FROM dbo.AllNotes
    WHERE LEN(NoteText) = 0;
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber,
           ERROR_SEVERITY() AS ErrorSeverity,
           ERROR_STATE() AS ErrorState,
           ERROR_PROCEDURE() AS ErrorProcedure,
           ERROR_LINE() AS ErrorLine,
           ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
    BEGIN
        ROLLBACK TRANSACTION ScriptTransaction;
    END;
END CATCH;

IF @@TRANCOUNT > 0
BEGIN
    COMMIT TRANSACTION ScriptTransaction;
END;

GO


--=================================================================================================
-- END  AD-17208- Error when viewing student info
--=================================================================================================