﻿SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
PRINT N'Creating [RedGateLocal].[DeploymentMetadata] for migration script history'
GO
IF SCHEMA_ID(N'RedGateLocal') IS NULL
    EXEC sp_executesql N'CREATE SCHEMA [RedGateLocal] AUTHORIZATION [dbo]'
GO
IF OBJECT_ID(N'[RedGateLocal].[DeploymentMetadata]') IS NULL
BEGIN
	CREATE TABLE [RedGateLocal].[DeploymentMetadata] (
	  [Id] INT NOT NULL PRIMARY KEY IDENTITY(1,1), 
	  [Name] NVARCHAR(max) NOT NULL, 
	  [Type] VARCHAR(50) NOT NULL, 
	  [Action] VARCHAR(50) NOT NULL, 
	  [By] NVARCHAR(128) NOT NULL DEFAULT ORIGINAL_LOGIN(), 
	  [As] NVARCHAR(128) NOT NULL DEFAULT SUSER_SNAME(), 
	  [CompletedDate] DATETIME NOT NULL DEFAULT GETDATE(), 
	  [With] NVARCHAR(128) NOT NULL DEFAULT APP_NAME(), 
	  [BlockId] VARCHAR(50) NOT NULL, 
	  [InsertedSerial] BINARY(8) NOT NULL DEFAULT @@DBTS + 1, 
	  [UpdatedSerial] TIMESTAMP NOT NULL, 
	  [MetadataVersion] VARCHAR(50) NOT NULL, 
	  [Hash] NVARCHAR(max) NULL
	)
	EXEC sp_addextendedproperty N'MS_Description', N'This table records deployments with migration scripts. Learn more: http://rd.gt/230GBP3', 'SCHEMA', N'RedGateLocal', 'TABLE', N'DeploymentMetadata', NULL, NULL
END
GO
PRINT N'Executing: Changes before migration script ''Covers changes to: TenantQuestions. Script created at 2018-01-23 10:11.'''
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Finished executing: Changes before migration script ''Covers changes to: TenantQuestions. Script created at 2018-01-23 10:11.'''
GO
INSERT INTO [RedGateLocal].[DeploymentMetadata] ([Name], [Type], [Action], [BlockId], [MetadataVersion]) 
VALUES (N'Changes before migration script ''Covers changes to: TenantQuestions. Script created at 2018-01-23 10:11.''', 'Compare', 'Deployed', '2018-01-23-104108 mx auto', '5.66.0.250')
GO
PRINT N'Executing: Migration script ''Covers changes to: TenantQuestions. Script created at 2018-01-23 10:11.'''
GO
/*
This migration script replaces uncommitted changes made to these objects:
TenantQuestions

Use this script to make necessary schema and data changes for these objects only. Schema changes to any other objects won't be deployed.

Schema changes and migration scripts are deployed in the order they're committed.

Migration scripts must not reference static data. When you deploy migration scripts alongside static data 
changes, the migration scripts will run first. This can cause the deployment to fail. 
Read more at https://documentation.red-gate.com/display/SOC6/Static+data+and+migrations.
*/

--================================================================

-- START --  AD-2688 Add Primary Keys to the Tenant tables missing the PK so that EF Core can scafold correctly

-- Name Initial and the date

--================================================================


IF OBJECT_ID('tempdb..#TenantQuestionsTemp') IS NOT NULL
    DROP TABLE #TenantQuestionsTemp;
GO
--select into temp table values where id is null
SELECT [Description]
INTO   #TenantQuestionsTemp
FROM   dbo.TenantQuestions
WHERE  Id IS NULL;

--delete from tenant questions table rows where id is null
DELETE FROM dbo.TenantQuestions
WHERE Id IS NULL;

--delete orphan tenant users
DELETE FROM dbo.TenantUsers
WHERE UserId NOT IN  (SELECT UserId FROM dbo.aspnet_Users)

SET NUMERIC_ROUNDABORT OFF;
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON;
PRINT N'Altering [dbo].[TenantQuestions]';


--alter table so that id is not null
ALTER TABLE [dbo].[TenantQuestions]
ALTER COLUMN [Id] [INT] NOT NULL;
GO
--if pk does not exist
IF (
   SELECT COUNT(*)
   FROM   sys.objects
   WHERE  type_desc LIKE '%CONSTRAINT'
          AND OBJECT_NAME(object_id) = 'PK_TenantQuestions_TenantQuestionId'
   ) = 0
    BEGIN
        PRINT N'Creating primary key [PK_TenantQuestions_TenantQuestionId] on [dbo].[TenantQuestions]';

        --create PK contraint for Id
        ALTER TABLE [dbo].[TenantQuestions]
        ADD CONSTRAINT [PK_TenantQuestions_TenantQuestionId]
            PRIMARY KEY CLUSTERED ( [Id] ) ON [PRIMARY];

    END;

--insert values into table that were copied into temp table
INSERT INTO dbo.TenantQuestions (
                                Description
                                )
            SELECT [Description]
            FROM   #TenantQuestionsTemp;

--drop temp table
DROP TABLE #TenantQuestionsTemp;

GO
--================================================================

-- END  --  AD-2688 Add Primary Keys to the Tenant tables missing the PK so that EF Core can scafold correctly

--================================================================



GO
PRINT N'Finished executing: Migration script ''Covers changes to: TenantQuestions. Script created at 2018-01-23 10:11.'''
GO
INSERT INTO [RedGateLocal].[DeploymentMetadata] ([Name], [Type], [Action], [BlockId], [MetadataVersion]) 
VALUES (N'Migration script ''Covers changes to: TenantQuestions. Script created at 2018-01-23 10:11.''', 'Migration', 'Deployed', '2018-01-23-104108 my user', '5.66.0.250')
GO
PRINT N'Executing: Changes before migration script ''Covers changes to: UserMigrationErrors. Script created at 2018-02-13 15:07.'''
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[UserMigrationErrors]'
GO
PRINT N'Finished executing: Changes before migration script ''Covers changes to: UserMigrationErrors. Script created at 2018-02-13 15:07.'''
GO
INSERT INTO [RedGateLocal].[DeploymentMetadata] ([Name], [Type], [Action], [BlockId], [MetadataVersion]) 
VALUES (N'Changes before migration script ''Covers changes to: UserMigrationErrors. Script created at 2018-02-13 15:07.''', 'Compare', 'Deployed', '2018-02-13-150734 sj auto', '5.66.0.250')
GO
PRINT N'Executing: Migration script ''Covers changes to: UserMigrationErrors. Script created at 2018-02-13 15:07.'''
GO
/*
This migration script replaces uncommitted changes made to these objects:
UserMigrationErrors

Use this script to make necessary schema and data changes for these objects only. Schema changes to any other objects won't be deployed.

Schema changes and migration scripts are deployed in the order they're committed.

Migration scripts must not reference static data. When you deploy migration scripts alongside static data 
changes, the migration scripts will run first. This can cause the deployment to fail. 
Read more at https://documentation.red-gate.com/display/SOC6/Static+data+and+migrations.
*/

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
PRINT N'Dropping constraints from [dbo].[UserMigrationErrors]'
GO

PRINT N'Creating primary key [PK_UserMigrationErrors_MigrationId] on [dbo].[UserMigrationErrors]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK_UserMigrationErrors_MigrationId]', 'PK') AND parent_object_id = OBJECT_ID(N'[dbo].[UserMigrationErrors]', 'U'))
BEGIN
	ALTER TABLE [dbo].[UserMigrationErrors] ADD CONSTRAINT [PK_UserMigrationErrors_MigrationId] PRIMARY KEY CLUSTERED  ([MigrationId]) ON [PRIMARY]
END 
GO
PRINT N'Finished executing: Migration script ''Covers changes to: UserMigrationErrors. Script created at 2018-02-13 15:07.'''
GO
INSERT INTO [RedGateLocal].[DeploymentMetadata] ([Name], [Type], [Action], [BlockId], [MetadataVersion]) 
VALUES (N'Migration script ''Covers changes to: UserMigrationErrors. Script created at 2018-02-13 15:07.''', 'Migration', 'Deployed', '2018-02-13-150734 sk user', '5.66.0.250')
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[UpdateAdvantageSupportUser]'
GO
IF OBJECT_ID(N'[dbo].[UpdateAdvantageSupportUser]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[UpdateAdvantageSupportUser]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_DEVELOPMENT_Setup_AdvantageDB]'
GO
IF OBJECT_ID(N'[dbo].[USP_DEVELOPMENT_Setup_AdvantageDB]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_DEVELOPMENT_Setup_AdvantageDB]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[RemoveTenantbyName]'
GO
IF OBJECT_ID(N'[dbo].[RemoveTenantbyName]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[RemoveTenantbyName]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_MigrateUsers]'
GO
IF OBJECT_ID(N'[dbo].[USP_MigrateUsers]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_MigrateUsers]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[RemoveTenantbyName]'
GO
IF OBJECT_ID(N'[dbo].[RemoveTenantbyName]', 'P') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[RemoveTenantbyName]
    @TenantName VARCHAR(50)
AS
    BEGIN
        BEGIN TRY
            BEGIN TRANSACTION DeleteTenant;

            DECLARE @tenantIdValue INT = 0;
            SELECT @tenantIdValue = TenantId
            FROM   dbo.Tenant
            WHERE  DatabaseName = @TenantName;

            DECLARE @return_status INT;
            EXEC @return_status = dbo.RemoveTenant @tenantId = @tenantIdValue; -- int

            COMMIT TRANSACTION DeleteTenant;
        END TRY
        BEGIN CATCH
            ROLLBACK TRANSACTION DeleteTenant;
        END CATCH;
        SET NOCOUNT OFF;
    END;

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_MigrateUsers]'
GO
IF OBJECT_ID(N'[dbo].[USP_MigrateUsers]', 'P') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[USP_MigrateUsers]
    @DatabaseName VARCHAR(50)
   ,@TenantId INT
AS
    BEGIN

        DECLARE @SecurityQuestion VARCHAR(100)
               ,@SecurityAnswer VARCHAR(50);
        DECLARE @ApplicationId UNIQUEIDENTIFIER
               ,@LowerEmail VARCHAR(100);
        DECLARE @UserId UNIQUEIDENTIFIER
               ,@Email VARCHAR(100)
               ,@AccountActive BIT;
        SET @ApplicationId = ''C75D06DD-25F6-45B5-93A3-B71806320191'';
        DECLARE @SQL VARCHAR(500)
               ,@SearchString VARCHAR(50)
               ,@saSearchString VARCHAR(5);


        --Set @SecurityQuestion = ''What is your favoutite color?''  
        --Set @SecurityAnswer = ''Blue''  

        SET @SearchString = ''support'';
        SET @saSearchString = ''sa'';
        SET @SQL = ''insert into tblUsers(DatabaseName,UserId,Email,AccountActive) Select Distinct '''''' + @DatabaseName + '''''',UserId,Email,AccountActive from ''
                   + @DatabaseName + ''.dbo.syusers where Lower(username) <> '''''' + @SearchString + '''''''' + '' and Lower(username) <> '''''' + @saSearchString + ''''''''
                   + '' order by email'';
        PRINT @SQL;
        EXECUTE ( @SQL );

        DECLARE MigrateUsers_Cursor CURSOR LOCAL FORWARD_ONLY FAST_FORWARD READ_ONLY FOR
            SELECT DISTINCT UserId
                           ,Email
                           ,AccountActive
            FROM   tblUsers;

        OPEN MigrateUsers_Cursor;
        FETCH NEXT FROM MigrateUsers_Cursor
        INTO @UserId
            ,@Email
            ,@AccountActive;
        WHILE @@FETCH_STATUS = 0
            BEGIN
                --insert user''s email address  
                --DBName.OWNER.SOURCETABLENAME : Syntax is mandatory in insert statement  
                BEGIN TRY
                    SET @LowerEmail = LOWER(@Email);

                    IF NOT EXISTS (
                                  SELECT *
                                  FROM   TenantAuthDB.dbo.aspnet_Users
                                  WHERE  UserId = @UserId
                                  )
                        BEGIN
                            INSERT INTO TenantAuthDB.dbo.aspnet_Users (
                                                                      ApplicationId
                                                                     ,UserId
                                                                     ,UserName
                                                                     ,LoweredUserName
                                                                     ,MobileAlias
                                                                     ,IsAnonymous
                                                                     ,LastActivityDate
                                                                      )
                            VALUES ( @ApplicationId, @UserId, @LowerEmail, @LowerEmail, NULL, 0, GETDATE());

                            -- Default password being used is : advantage3$  
                            -- Default security anwer: security answer  
                            -- User will be forced to change password at first login  
                            INSERT INTO dbo.aspnet_Membership (
                                                              ApplicationId
                                                             ,UserId
                                                             ,Password
                                                             ,PasswordFormat
                                                             ,PasswordSalt
                                                             ,MobilePIN
                                                             ,Email
                                                             ,LoweredEmail
                                                             ,PasswordQuestion
                                                             ,PasswordAnswer
                                                             ,IsApproved
                                                             ,IsLockedOut
                                                             ,CreateDate
                                                             ,LastLoginDate
                                                             ,LastPasswordChangedDate
                                                             ,LastLockoutDate
                                                             ,FailedPasswordAttemptCount
                                                             ,FailedPasswordAttemptWindowStart
                                                             ,FailedPasswordAnswerAttemptCount
                                                             ,FailedPasswordAnswerAttemptWindowStart
                                                             ,Comment
                                                              )
                            VALUES ( @ApplicationId, @UserId, ''CcnbzYVEUDPVOH1ne2m/X2ttvQ4='', 1, ''ghMdIEJSDwFgiWkA/a0JTw=='', NULL, @LowerEmail, @LowerEmail
                                    ,''What is your favourite color'', ''BtJLiybtDny54+q4nmhXtzlXovE='', @AccountActive, 0, GETDATE(), GETDATE(), GETDATE()
                                    ,GETDATE(), 0, GETDATE(), 0, GETDATE(), NULL );
                        END;

                    IF NOT EXISTS (
                                  SELECT *
                                  FROM   TenantAuthDB.dbo.TenantUsers
                                  WHERE  TenantId = @TenantId
                                         AND UserId = @UserId
                                  )
                        BEGIN
                            INSERT INTO TenantAuthDB.dbo.TenantUsers (
                                                                     TenantId
                                                                    ,UserId
                                                                    ,IsDefaultTenant
                                                                    ,IsSupportUser
                                                                     )
                            VALUES ( @TenantId, @UserId, 0, 0 );
                        END;

                END TRY
                BEGIN CATCH
                    INSERT INTO UserMigrationErrors (
                                                    DatabaseName
                                                   ,UserId
                                                   ,Email
                                                   ,ErrorNumber
                                                   ,ErrorSeverity
                                                   ,ErrorState
                                                   ,ErrorProcedure
                                                   ,ErrorLine
                                                   ,ErrorMessage
                                                   ,ModUser
                                                   ,ModDate
                                                    )
                                SELECT @DatabaseName
                                      ,@UserId
                                      ,@Email
                                      ,ERROR_NUMBER() AS ErrorNumber
                                      ,ERROR_SEVERITY() AS ErrorSeverity
                                      ,ERROR_STATE() AS ErrorState
                                      ,ERROR_PROCEDURE() AS ErrorProcedure
                                      ,ERROR_LINE() AS ErrorLine
                                      ,ERROR_MESSAGE() AS ErrorMessage
                                      ,''support''
                                      ,GETDATE();
                END CATCH;
                FETCH NEXT FROM MigrateUsers_Cursor
                INTO @UserId
                    ,@Email
                    ,@AccountActive;
            END;
        --Truncate table tblUsers -- Clear contents   
        CLOSE MigrateUsers_Cursor;
        DEALLOCATE MigrateUsers_Cursor;

    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[RebusQueue]'
GO
IF OBJECT_ID(N'[dbo].[RebusQueue]', 'U') IS NULL
CREATE TABLE [dbo].[RebusQueue]
(
[id] [bigint] NOT NULL IDENTITY(1, 1),
[recipient] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[priority] [int] NOT NULL,
[expiration] [datetime2] NOT NULL,
[visible] [datetime2] NOT NULL,
[headers] [varbinary] (max) NOT NULL,
[body] [varbinary] (max) NOT NULL
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_RebusQueue] on [dbo].[RebusQueue]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK_RebusQueue]', 'PK') AND parent_object_id = OBJECT_ID(N'[dbo].[RebusQueue]', 'U'))
ALTER TABLE [dbo].[RebusQueue] ADD CONSTRAINT [PK_RebusQueue] PRIMARY KEY CLUSTERED  ([recipient], [priority], [id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_EXPIRATION_RebusQueue] on [dbo].[RebusQueue]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IDX_EXPIRATION_RebusQueue' AND object_id = OBJECT_ID(N'[dbo].[RebusQueue]'))
CREATE NONCLUSTERED INDEX [IDX_EXPIRATION_RebusQueue] ON [dbo].[RebusQueue] ([expiration])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating index [IDX_RECEIVE_RebusQueue] on [dbo].[RebusQueue]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'IDX_RECEIVE_RebusQueue' AND object_id = OBJECT_ID(N'[dbo].[RebusQueue]'))
CREATE NONCLUSTERED INDEX [IDX_RECEIVE_RebusQueue] ON [dbo].[RebusQueue] ([recipient], [priority], [visible], [expiration], [id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[UpdateAdvantageSupportUser]'
GO
IF OBJECT_ID(N'[dbo].[UpdateAdvantageSupportUser]', 'P') IS NULL
EXEC sp_executesql N'-- =============================================
-- Author:		Ginzo, John
-- Create date: 06/26/2015
-- Description:	Sync up the support user from tenant
--				to Advantage
-- =============================================
CREATE PROCEDURE [dbo].[UpdateAdvantageSupportUser]
    @DatabaseName VARCHAR(50)
   ,@CampusCode VARCHAR(5) = NULL
   ,@Username VARCHAR(50) = NULL
AS
    BEGIN



        BEGIN TRAN UpdateUsers;

        BEGIN TRY
            --DECLARE @DatabaseName VARCHAR(50)
            --SET @DatabaseName=''[IDTI_3.8_MIG]''
            DECLARE @oldUserId VARCHAR(50);
            DECLARE @newUserId VARCHAR(50);
            DECLARE @Sql VARCHAR(1000);

            -- Clean up rptAdmissionsRep Table from support user.
            --SET @Sql = ''DELETE  '' + @DatabaseName + ''.dbo.rptAdmissionsRep
            --WHERE   AdmissionsRepId IN ( SELECT AdmissionsRepId
            --                             FROM    '' + @DatabaseName + ''.dbo.rptAdmissionsRep
            --                             WHERE  AdmissionsRepDescrip LIKE ''''%Support%'''' );''
            PRINT @Sql;
            EXECUTE ( @Sql );

            --Support User id from tenant auth db
            SET @newUserId = (
                             SELECT TOP 1 UserId
                             FROM   TenantAuthDB.dbo.aspnet_Users
                             WHERE  (
                                    @Username IS NULL
                                    AND UserName IN ( ''support@fameinc.com'', ''advantagedev@fameinc.com'' )
                                    )
                                    OR (
                                       @Username IS NOT NULL
                                       AND UserName = @Username
                                       )
                             );

            IF ( @CampusCode IS NULL )
                BEGIN



                    DECLARE @SQL2 NVARCHAR(MAX) = ''SELECT TOP 1 @oldUserId = UserId FROM '' + @DatabaseName + ''.dbo.syUsers where username='' + ''''''support'''''';
                    EXEC sp_executesql @SQL2
                                      ,N''@oldUserId uniqueidentifier out''
                                      ,@oldUserId OUT;
                    SET @oldUserId = CAST(@oldUserId AS VARCHAR(50));




                    SET @Sql = ( ''INSERT INTO '' + @DatabaseName
                                 + ''.dbo.syUsers
			( UserId
			,FullName
			,Email
			,UserName
			,Password
			,ConfirmPassword
			,Salt
			,AccountActive
			,ModDate
			,ModUser
			,CampusId
			,ShowDefaultCampus
			,ModuleId
			,IsAdvantageSuperUser
			,IsDefaultAdminRep
			,IsLoggedIn
			)
	SELECT  top 1 ''''''            + @newUserId + ''''''
			,''''support''''
			,''                   + ''''''support@yahoo.com'''''' + ''
			,''''support''''
			,''                   + ''''''no password'''''' + ''
			,''                   + ''''''no password''''''
                                 + ''
			,Salt
			,AccountActive
			,ModDate
			,ModUser
			,null
			,ShowDefaultCampus
			,ModuleId
			,IsAdvantageSuperUser
			,IsDefaultAdminRep
			,IsLoggedIn
	FROM ''                       + @DatabaseName + ''.dbo.syUsers
	WHERE IsAdvantageSuperUser=1''
                               );

                    --PRINT @sql
                    EXECUTE ( @Sql );

                    SET @Sql = ''insert into '' + @DatabaseName
                               + ''..syUsersRolesCampGrps (
                                     UserRoleCampGrpId
                                    ,UserId
                                    ,RoleId
                                    ,CampGrpId
                                    ,ModDate
                                    ,ModUser
                                     )  
									 SELECT NEWID(),'''''' + @newUserId + '''''', RoleId, ''''2AC97FC1-BB9B-450A-AA84-7C503C90A1EB'''', GETDATE(), ''''SA'''' FROM ''
                               + @DatabaseName + ''..syroles WHERE Code  IN (''''SYSADMINS'''', ''''AD-Api'''', ''''Reports-Api'''', ''''DIRADM'''')'';

                    EXECUTE ( @Sql );

                    PRINT @Sql;

                    DECLARE @UserUpdateFKSQL VARCHAR(MAX);

                    DECLARE @newUserIdString VARCHAR(50) = CONVERT(NVARCHAR(50), @newUserId);
                    DECLARE @oldUserIdString VARCHAR(50) = CONVERT(NVARCHAR(50), @oldUserId);



                    CREATE TABLE #Updates
                        (
                            UpdateStatement NVARCHAR(MAX)
                        );


                    SET @UserUpdateFKSQL = ''insert into #Updates SELECT     ''''UPDATE ''''+ '''''' + @DatabaseName
                                           + ''''''+''''..''''+ + detail.name + '''' SET '''' + dcolumn.name + '''' = '''''''''' + @newUserIdString
                                           + ''''''''''   WHERE '''' + dcolumn.name + '''' = '''''''''' + @oldUserIdString + '''''''''''''' 

FROM       ''                               + @DatabaseName + ''.sys.columns AS mcolumn
INNER JOIN ''                               + @DatabaseName
                                           + ''.sys.foreign_key_columns fkColumn ON mcolumn.object_id = fkColumn.referenced_object_id
                                      AND mcolumn.column_id = fkColumn.referenced_column_id
INNER JOIN ''                               + @DatabaseName + ''.sys.tables AS master ON mcolumn.object_id = master.object_id
INNER JOIN ''                               + @DatabaseName
                                           + ''.sys.columns AS dcolumn ON fkColumn.parent_object_id = dcolumn.object_id
                                     AND fkColumn.parent_column_id = dcolumn.column_id
INNER JOIN ''                               + @DatabaseName
                                           + ''.sys.tables AS detail ON dcolumn.object_id = detail.object_id
                                   AND detail.name <> ''''syUsers''''
WHERE      ( master.name = N''''syUsers'''' )'';


                    EXEC ( @UserUpdateFKSQL );

                    DECLARE @currentUpdateStatement NVARCHAR(MAX);

                    DECLARE update_cursor CURSOR FOR
                        SELECT UpdateStatement
                        FROM   #Updates;

                    OPEN update_cursor;

                    FETCH NEXT FROM update_cursor
                    INTO @currentUpdateStatement;

                    WHILE @@FETCH_STATUS = 0
                        BEGIN
                            EXEC ( @currentUpdateStatement );
                            IF @@ERROR <> 0
                                BEGIN
                                    ROLLBACK TRAN UpdateUsers;
                                    PRINT ''ERROR='' + CAST(@@ERROR AS VARCHAR(100));
                                    PRINT @currentUpdateStatement;
                                    RETURN @@ERROR;
                                END;

                            FETCH NEXT FROM update_cursor
                            INTO @currentUpdateStatement;

                        END;

                    CLOSE update_cursor;
                    DEALLOCATE update_cursor;


                --make sure username or email are not repeated
                    SET @Sql = ''update '' + @DatabaseName + ''..syUsers set username = ''''support_'' + REPLACE(@oldUserId, ''-'', ''_'') + '''''', email = ''''support_''
                               + REPLACE(@oldUserId, ''-'', ''_'') + ''@famein.com'''' where userid = '''''' + @oldUserId + '''''''';

                    EXECUTE ( @Sql );






                    PRINT ''Update is compete'';
                END;
            ELSE
                BEGIN

                    SET @Sql = ( ''INSERT INTO '' + @DatabaseName
                                 + ''.dbo.syUsers
			( UserId
			,FullName
			,Email
			,UserName
			,Password
			,ConfirmPassword
			,Salt
			,AccountActive
			,ModDate
			,ModUser
			,CampusId
			,ShowDefaultCampus
			,ModuleId
			,IsAdvantageSuperUser
			,IsDefaultAdminRep
			,IsLoggedIn
			)
	SELECT  top 1 ''''''            + @newUserId + ''''''
			,''''support_''         + @CampusCode + ''''''
			,''''support_''         + @CampusCode + ''@yahoo.com'''''' + ''
			,''''support_''         + @CampusCode + ''''''
			,''                   + ''''''no password'''''' + ''
			,''                   + ''''''no password''''''
                                 + ''
			,Salt
			,AccountActive
			,ModDate
			,ModUser
			,(SELECT TOP 1 Campusid FROM FreedomAdvantage..syCampuses ORDER BY ModDate desc)
			,ShowDefaultCampus
			,ModuleId
			,IsAdvantageSuperUser
			,IsDefaultAdminRep
			,IsLoggedIn
	FROM ''                       + @DatabaseName + ''.dbo.syUsers
	WHERE IsAdvantageSuperUser=1''
                               );

                    PRINT @Sql;

                    --PRINT @sql
                    EXECUTE ( @Sql );

                    SET @Sql = ''insert into '' + @DatabaseName
                               + ''..syUsersRolesCampGrps (
                                     UserRoleCampGrpId
                                    ,UserId
                                    ,RoleId
                                    ,CampGrpId
                                    ,ModDate
                                    ,ModUser
                                     )  
									 SELECT NEWID(),'''''' + @newUserId + '''''', RoleId, ''''2AC97FC1-BB9B-450A-AA84-7C503C90A1EB'''', GETDATE(), ''''SA'''' FROM ''
                               + @DatabaseName + ''..syroles WHERE Code  IN (''''SYSADMINS'''', ''''AD-Api'''', ''''Reports-Api'''', ''''DIRADM'''')'';

                    EXECUTE ( @Sql );
                END;

            IF OBJECT_ID(''tempdb..#Updates'', ''U'') IS NOT NULL
                BEGIN
                    DROP TABLE #Updates;

                END;


            COMMIT TRAN UpdateUsers;
        END TRY
        BEGIN CATCH
            IF OBJECT_ID(''tempdb..#Updates'', ''U'') IS NOT NULL
                BEGIN
                    DROP TABLE #Updates;

                END;

            ROLLBACK TRANSACTION;
        END CATCH;
        RETURN 0;



    END;




'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_UserCategory_UserCategoryId] on [dbo].[UserCatagory]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK_UserCategory_UserCategoryId]', 'PK') AND parent_object_id = OBJECT_ID(N'[dbo].[UserCatagory]', 'U'))
ALTER TABLE [dbo].[UserCatagory] ADD CONSTRAINT [PK_UserCategory_UserCategoryId] PRIMARY KEY CLUSTERED  ([UserCategoryId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_tblUsers_Id] on [dbo].[tblUsers]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[PK_tblUsers_Id]', 'PK') AND parent_object_id = OBJECT_ID(N'[dbo].[tblUsers]', 'U'))
ALTER TABLE [dbo].[tblUsers] ADD CONSTRAINT [PK_tblUsers_Id] PRIMARY KEY CLUSTERED  ([Id])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding constraints to [dbo].[Tenant]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UC_TenantName]', 'UQ') AND parent_object_id = OBJECT_ID(N'[dbo].[Tenant]', 'U'))
ALTER TABLE [dbo].[Tenant] ADD CONSTRAINT [UC_TenantName] UNIQUE NONCLUSTERED  ([TenantName])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[TenantUsers]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_TenantUsers_aspnet_Users_UserId_UserId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[TenantUsers]', 'U'))
ALTER TABLE [dbo].[TenantUsers] ADD CONSTRAINT [FK_TenantUsers_aspnet_Users_UserId_UserId] FOREIGN KEY ([UserId]) REFERENCES [dbo].[aspnet_Users] ([UserId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END