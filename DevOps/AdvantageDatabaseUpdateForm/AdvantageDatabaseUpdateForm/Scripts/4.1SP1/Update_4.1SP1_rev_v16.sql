﻿--=================================================================================================
-- START Add Missing Permissions Custom Reports and Attendance Summary to Academic Director Role
--=================================================================================================
BEGIN TRANSACTION ScriptTransaction;
BEGIN TRY

    IF NOT EXISTS
    (
        SELECT *
        FROM syRlsResLvls permissions
            INNER JOIN dbo.syRoles roles
                ON roles.RoleId = permissions.RoleId
        WHERE ResourceID = 877
              AND roles.SysRoleId = 13
    )
    BEGIN
        INSERT INTO dbo.syRlsResLvls
        (
            RRLId,
            RoleId,
            ResourceID,
            AccessLevel,
            ModDate,
            ModUser,
            ParentId
        )
        VALUES
        (   NEWID(),                                -- RRLId - uniqueidentifier
            'DCFBF417-9B6F-41ED-A4C8-0FA84F9681CE', -- RoleId - uniqueidentifier
            877,                                    -- ResourceID - smallint
            15,                                     -- AccessLevel - smallint
            GETDATE(),                              -- ModDate - datetime
            'sa',                                   -- ModUser - varchar(50)
            0                                       -- ParentId - int
            );
    END;

    IF NOT EXISTS
    (
        SELECT *
        FROM syRlsResLvls permissions
            INNER JOIN dbo.syRoles roles
                ON roles.RoleId = permissions.RoleId
        WHERE ResourceID = 842
              AND roles.SysRoleId = 13
    )
    BEGIN
        INSERT INTO dbo.syRlsResLvls
        (
            RRLId,
            RoleId,
            ResourceID,
            AccessLevel,
            ModDate,
            ModUser,
            ParentId
        )
        VALUES
        (   NEWID(),                                -- RRLId - uniqueidentifier
            'DCFBF417-9B6F-41ED-A4C8-0FA84F9681CE', -- RoleId - uniqueidentifier
            842,                                    -- ResourceID - smallint
            15,                                     -- AccessLevel - smallint
            GETDATE(),                              -- ModDate - datetime
            'sa',                                   -- ModUser - varchar(50)
            0                                       -- ParentId - int
            );

    END;
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber,
           ERROR_SEVERITY() AS ErrorSeverity,
           ERROR_STATE() AS ErrorState,
           ERROR_PROCEDURE() AS ErrorProcedure,
           ERROR_LINE() AS ErrorLine,
           ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
    BEGIN
        ROLLBACK TRANSACTION ScriptTransaction;
    END;
END CATCH;

IF @@TRANCOUNT > 0
BEGIN
    COMMIT TRANSACTION ScriptTransaction;
END;
--=================================================================================================
-- END Add Missing Permissions Custom Reports and Attendance Summary to Academic Director Role
--=================================================================================================