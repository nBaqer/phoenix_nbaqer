﻿/*
Run this script on:

        dev-com-db1\Adv.Aveda    -  This database will be modified

to synchronize it with a database with the schema represented by:

        Source

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.7.10021 from Red Gate Software Ltd at 7/31/2019 3:31:30 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
/*
* Use this Pre-Deployment script to perform tasks before the deployment of the project.
* Read more at https://www.red-gate.com/SOC7/pre-deployment-script-help
*/
UPDATE dbo.arClsSectMeetings
SET PeriodId = NULL
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)

DELETE FROM syPeriodsWorkDays
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_TR_Sub03_PrepareGPA]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub03_PrepareGPA]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_TR_Sub03_PrepareGPA]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_TR_Sub03_PrepareGPA]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub03_PrepareGPA]', 'P') IS NULL
EXEC sp_executesql N'-- =========================================================================================================
-- USP_TR_Sub03_PrepareGPA
-- =========================================================================================================
CREATE PROCEDURE [dbo].[USP_TR_Sub03_PrepareGPA]
    @StuEnrollIdList VARCHAR(MAX)
   ,@ShowMultipleEnrollments BIT = 0
   ,@CoursesTakenTableName NVARCHAR(200) OUTPUT
   ,@GPASummaryTableName NVARCHAR(200) OUTPUT
AS --
    BEGIN
        -- 00) Variable Definition, Create Temp Table and Set Initial Values
        BEGIN
            DECLARE @MinimunDate AS DATETIME;
            DECLARE @MaximunDate AS DATETIME;
            DECLARE @StuEnrollCampusId AS UNIQUEIDENTIFIER;

            DECLARE @GradesFormat AS VARCHAR(50);
            DECLARE @GPAMethod AS VARCHAR(50);
            DECLARE @GradeBookAt AS VARCHAR(50);
            DECLARE @RetakenPolicy AS VARCHAR(50);

            DECLARE @curStudentId AS VARCHAR(50);
            DECLARE @curStuEnrollId AS VARCHAR(50);
            DECLARE @curTermId AS UNIQUEIDENTIFIER;

            DECLARE @cumTermAverage AS DECIMAL(18, 2);

            DECLARE @cumEnroTermSimple_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumEnroTermSimple_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumEnroTermSimple_GPA AS DECIMAL(18, 2);

            DECLARE @cumEnroTermWeight_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumEnroTermWeight_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumEnroTermWeight_GPA AS DECIMAL(18, 2);

            DECLARE @cumStudTermSimple_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumStudTermSimple_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumStudTermSimple_GPA AS DECIMAL(18, 2);

            DECLARE @cumStudTermWeight_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumStudTermWeight_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumStudTermWeight_GPA AS DECIMAL(18, 2);

            DECLARE @cumEnroSimple_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumEnroSimple_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumEnroSimple_GPA AS DECIMAL(18, 2);

            DECLARE @cumEnroWeight_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumEnroWeight_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumEnroWeight_GPA AS DECIMAL(18, 2);

            DECLARE @cumStudSimple_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumStudSimple_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumStudSimple_GPA AS DECIMAL(18, 2);

            DECLARE @cumStudWeight_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumStudWeight_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumStudWeight_GPA AS DECIMAL(18, 2);

            DECLARE @IsMakingSAP AS BIT;
            DECLARE @NewID AS NVARCHAR(50);
            DECLARE @SQL01 AS NVARCHAR(MAX);
            DECLARE @SQL02 AS NVARCHAR(MAX);

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#CoursesTaken'')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            CREATE TABLE #CoursesTaken
                (
                    CoursesTakenId INTEGER IDENTITY(1, 1) NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,ReqId UNIQUEIDENTIFIER
                   ,ReqCode VARCHAR(100)
                   ,ReqDescription VARCHAR(100)
                   ,ReqCodeDescrip VARCHAR(100)
                   ,ReqCredits DECIMAL(18, 2)
                   ,MinVal DECIMAL(18, 2)
                   ,ClsSectionId VARCHAR(50)
                   ,CourseStarDate DATETIME
                   ,CourseEndDate DATETIME
                   ,SCS_CreditsAttempted DECIMAL(18, 2)
                   ,SCS_CreditsEarned DECIMAL(18, 2)
                   ,SCS_CurrentScore DECIMAL(18, 2)
                   ,SCS_CurrentGrade VARCHAR(10)
                   ,SCS_FinalScore DECIMAL(18, 2)
                   ,SCS_FinalGrade VARCHAR(10)
                   ,SCS_Completed BIT
                   ,SCS_FinalGPA DECIMAL(18, 2)
                   ,SCS_Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_WeightedAverage_Credits DECIMAL(18, 2)
                   ,SCS_Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_SimpleAverage_Credits DECIMAL(18, 2)
                   ,SCS_ModUser VARCHAR(50)
                   ,SCS_ModDate DATETIME
                   ,SCS_TermGPA_Simple DECIMAL(18, 2)
                   ,SCS_TermGPA_Weighted DECIMAL(18, 2)
                   ,SCS_coursecredits DECIMAL(18, 2)
                   ,SCS_CumulativeGPA DECIMAL(18, 2)
                   ,SCS_CumulativeGPA_Simple DECIMAL(18, 2)
                   ,SCS_FACreditsEarned DECIMAL(18, 2)
                   ,SCS_Average DECIMAL(18, 2)
                   ,SCS_CumAverage DECIMAL(18, 2)
                   ,ScheduleDays DECIMAL(18, 2)
                   ,ActualDay DECIMAL(18, 2)
                   ,FinalGPA_Calculations DECIMAL(18, 2)
                   ,FinalGPA_TermCalculations DECIMAL(18, 2)
                   ,CreditsEarned_Calculations DECIMAL(18, 2)
                   ,ActualDay_Calculations DECIMAL(18, 2)
                   ,GrdBkWgtDetailsCount INTEGER
                   ,CountMultipleEnrollment INTEGER
                   ,RowNumberMultipleEnrollment INTEGER
                   ,CountRepeated INTEGER
                   ,RowNumberRetaked INTEGER
                   ,SameTermCountRetaken INTEGER
                   ,RowNumberSameTermRetaked INTEGER
                   ,RowNumberMultEnrollNoRetaken INTEGER
                   ,RowNumberOverAllByClassSection INTEGER
                   ,IsCreditsEarned BIT
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#getStudentGPAbyTerms'')
                      )
                BEGIN
                    DROP TABLE #getStudentGPAbyTerms;
                END;
            CREATE TABLE #getStudentGPAbyTerms
                (
                    StudentGPAbyTermsId INTEGER IDENTITY(1, 1) NOT NULL PRIMARY KEY
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(50)
                   ,TermStartDate DATETIME
                   ,TermEndDate DATETIME
                   ,DescripXTranscript VARCHAR(250)
                   ,TermAverage DECIMAL(18, 2)
                   ,EnroTermSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPACredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPA DECIMAL(18, 2)
                   ,EnroTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPACredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPA DECIMAL(18, 2)
                   ,StudTermSimple_CourseCredits DECIMAL(18, 2)
                   ,StudTermSimple_GPACredits DECIMAL(18, 2)
                   ,StudTermSimple_GPA DECIMAL(18, 2)
                   ,StudTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudTermWeight_GPACredits DECIMAL(18, 2)
                   ,StudTermWeight_GPA DECIMAL(18, 2)
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#getStudentGPAByEnrollment'')
                      )
                BEGIN
                    DROP TABLE #getStudentGPAByEnrollment;
                END;
            CREATE TABLE #getStudentGPAByEnrollment
                (
                    StudentGPAByEnrollmentId INTEGER IDENTITY(1, 1) NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,LastName VARCHAR(50)
                   ,FirstName VARCHAR(50)
                   ,MiddleName VARCHAR(50)
                   ,SSN VARCHAR(11)
                   ,StudentNumber VARCHAR(50)
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,EnrollmentID VARCHAR(50)
                   ,PrgVerId UNIQUEIDENTIFIER
                   ,PrgVerDescrip VARCHAR(50)
                   ,PrgVersionTrackCredits BIT
                   ,GrdSystemId UNIQUEIDENTIFIER
                   ,AcademicType VARCHAR(50)
                   ,ClockHourProgram VARCHAR(10)
                   ,IsMakingSAP BIT
                   ,TermId UNIQUEIDENTIFIER
                   --,TermDescription VARCHAR(50)
                   ,TermAverage DECIMAL(18, 2)
                   ,EnroSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroSimple_GPACredits DECIMAL(18, 2)
                   ,EnroSimple_GPA DECIMAL(18, 2)
                   ,EnroWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroWeight_GPACredits DECIMAL(18, 2)
                   ,EnroWeight_GPA DECIMAL(18, 2)
                   ,StudSimple_CourseCredits DECIMAL(18, 2)
                   ,StudSimple_GPACredits DECIMAL(18, 2)
                   ,StudSimple_GPA DECIMAL(18, 2)
                   ,StudWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudWeight_GPA_Credits DECIMAL(18, 2)
                   ,StudWeight_GPA DECIMAL(18, 2)
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#GPASummary'')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
            CREATE TABLE #GPASummary
                (
                    GPASummaryId INTEGER IDENTITY(1, 1) NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,LastName VARCHAR(50)
                   ,FirstName VARCHAR(50)
                   ,MiddleName VARCHAR(50)
                   ,SSN VARCHAR(11)
                   ,StudentNumber VARCHAR(50)
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,EnrollmentID VARCHAR(50)
                   ,PrgVerId UNIQUEIDENTIFIER
                   ,PrgVerDescrip VARCHAR(50)
                   ,PrgVersionTrackCredits BIT
                   ,GrdSystemId UNIQUEIDENTIFIER
                   ,AcademicType VARCHAR(50)
                   ,ClockHourProgram VARCHAR(10)
                   ,IsMakingSAP BIT
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,TermEndDate DATETIME
                   ,DescripXTranscript VARCHAR(250)
                   ,TermAverage DECIMAL(18, 2)
                   ,EnroTermSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPACredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPA DECIMAL(18, 2)
                   ,EnroTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPACredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPA DECIMAL(18, 2)
                   ,StudTermSimple_CourseCredits DECIMAL(18, 2)
                   ,StudTermSimple_GPACredits DECIMAL(18, 2)
                   ,StudTermSimple_GPA DECIMAL(18, 2)
                   ,StudTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudTermWeight_GPACredits DECIMAL(18, 2)
                   ,StudTermWeight_GPA DECIMAL(18, 2)
                   ,EnroSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroSimple_GPACredits DECIMAL(18, 2)
                   ,EnroSimple_GPA DECIMAL(18, 2)
                   ,EnroWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroWeight_GPACredits DECIMAL(18, 2)
                   ,EnroWeight_GPA DECIMAL(18, 2)
                   ,StudSimple_CourseCredits DECIMAL(18, 2)
                   ,StudSimple_GPACredits DECIMAL(18, 2)
                   ,StudSimple_GPA DECIMAL(18, 2)
                   ,StudWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudWeight_GPACredits DECIMAL(18, 2)
                   ,StudWeight_GPA DECIMAL(18, 2)
                   ,GradesFormat VARCHAR(50)
                   ,GPAMethod VARCHAR(50)
                   ,GradeBookAt VARCHAR(50)
                   ,RetakenPolicy VARCHAR(50)
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpEquivalentCoursesTakenByStudent'')
                      )
                BEGIN
                    DROP TABLE #tmpEquivalentCoursesTakenByStudent;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpStudentsWhoHasGradedEquivalentCourse'')
                      )
                BEGIN
                    DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourse;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpEquivalentCoursesTakenByStudentInTerm'')
                      )
                BEGIN
                    DROP TABLE #tmpEquivalentCoursesTakenByStudentInTerm;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpStudentsWhoHasGradedEquivalentCourseInTerm'')
                      )
                BEGIN
                    DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourseInTerm;
                END;

            SET @MinimunDate = ''1900-01-01'';
            SET @MaximunDate = ''2199-01-01'';
            SET @NewID = REPLACE(CONVERT(NVARCHAR(50), NEWID()), ''-'', ''_'');
            SET @CoursesTakenTableName = ''CoursesTaked_'' + @NewID;
            SET @GPASummaryTableName = ''GPASummary_'' + @NewID;

            SET @StuEnrollCampusId = COALESCE(
                                     (
                                     SELECT     TOP 1 ASE.CampusId
                                     FROM       dbo.arStuEnrollments AS ASE
                                     INNER JOIN (
                                                SELECT Val
                                                FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                ) AS LIST ON LIST.Val = ASE.EnrollmentId
                                     )
                                    ,NULL
                                             );

            SET @GradesFormat = dbo.GetAppSettingValueByKeyName(''GradesFormat'', @StuEnrollCampusId);
            IF ( @GradesFormat IS NOT NULL )
                BEGIN
                    SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
                END;
            SET @GPAMethod = dbo.GetAppSettingValueByKeyName(''GPAMethod'', @StuEnrollCampusId);
            IF ( @GPAMethod IS NOT NULL )
                BEGIN
                    SET @GPAMethod = LOWER(LTRIM(RTRIM(@GPAMethod)));
                END;
            SET @GradeBookAt = dbo.GetAppSettingValueByKeyName(''GradeBookWeightingLevel'', @StuEnrollCampusId);
            IF ( @GradeBookAt IS NOT NULL )
                BEGIN
                    SET @GradeBookAt = LOWER(LTRIM(RTRIM(@GradeBookAt)));
                END;
            SET @RetakenPolicy = (
                                 SELECT     TOP 1 SCASV.Value
                                 FROM       dbo.syConfigAppSetValues AS SCASV
                                 INNER JOIN dbo.syConfigAppSettings AS SCAS ON SCAS.SettingId = SCASV.SettingId
                                                                               AND SCAS.KeyName = ''GradeCourseRepetitionsMethod''
                                 );
        END;
        -- END  --  00) Variable Definition and Set Initial Values

        -- 01) Create Temp Table 
        BEGIN
            IF NOT EXISTS (
                          SELECT 1
                          FROM   tempdb.sys.objects AS O
                          WHERE  O.type IN ( ''U'' )
                                 AND O.name = @CoursesTakenTableName
                          )
                BEGIN
                    SET @SQL01 = '''';
                    SET @SQL01 = @SQL01 + ''CREATE TABLE tempdb.dbo.'' + @CoursesTakenTableName + '' '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''    ( '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''        CoursesTakenId INTEGER NOT NULL PRIMARY KEY '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,StudentId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,StuEnrollId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,TermId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,TermDescrip VARCHAR(100) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,TermStartDate DATETIME '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ReqId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ReqCode VARCHAR(100) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ReqDescription VARCHAR(100) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ReqCodeDescrip VARCHAR(100) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ReqCredits DECIMAL(18,2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,MinVal DECIMAL(18,2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ClsSectionId VARCHAR(50) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,CourseStarDate DATETIME '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,CourseEndDate DATETIME '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CreditsAttempted DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CreditsEarned DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CurrentScore DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CurrentGrade VARCHAR(10) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_FinalScore DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_FinalGrade VARCHAR(10) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_Completed BIT '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_FinalGPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_Product_WeightedAverage_Credits_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_Count_WeightedAverage_Credits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_Product_SimpleAverage_Credits_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_Count_SimpleAverage_Credits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_ModUser VARCHAR(50) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_ModDate DATETIME '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_TermGPA_Simple DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_TermGPA_Weighted DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_coursecredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CumulativeGPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CumulativeGPA_Simple DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_FACreditsEarned DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_Average DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CumAverage DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ScheduleDays DECIMAL(18,2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ActualDay DECIMAL(18,2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,FinalGPA_Calculations DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,FinalGPA_TermCalculations DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,CreditsEarned_Calculations DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ActualDay_Calculations DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,GrdBkWgtDetailsCount INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,CountMultipleEnrollment INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,RowNumberMultipleEnrollment INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,CountRepeated INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,RowNumberRetaked INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SameTermCountRetaken INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,RowNumberSameTermRetaked INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,RowNumberMultEnrollNoRetaken INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,RowNumberOverAllByClassSection INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,IsCreditsEarned BIT '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''    ) '' + CHAR(10);
                    --PRINT @SQL01;
                    EXECUTE ( @SQL01 );
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   tempdb.sys.objects AS O
                          WHERE  O.type IN ( ''U'' )
                                 AND O.name = @GPASummaryTableName
                          )
                BEGIN
                    SET @SQL02 = '''';
                    SET @SQL02 = @SQL02 + ''CREATE TABLE tempdb.dbo.'' + @GPASummaryTableName + '' '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''    ( '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''         GPASummaryId INTEGER NOT NULL PRIMARY KEY '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudentId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,LastName VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,FirstName VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,MiddleName VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,SSN VARCHAR(11) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudentNumber VARCHAR(50)'' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StuEnrollId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnrollmentID VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,PrgVerId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,PrgVerDescrip VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,PrgVersionTrackCredits BIT '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,GrdSystemId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,AcademicType NVARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,ClockHourProgram VARCHAR(10) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,IsMakingSAP BIT '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,TermId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,TermDescrip VARCHAR(100) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,TermStartDate DATETIME '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,TermEndDate DATETIME '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,DescripXTranscript VARCHAR(250) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,TermAverage DECIMAL(18,2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroTermSimple_CourseCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroTermSimple_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroTermSimple_GPA DECIMAL(18, 2)				    '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroTermWeight_CoursesCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroTermWeight_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroTermWeight_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudTermSimple_CourseCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudTermSimple_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudTermSimple_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudTermWeight_CoursesCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudTermWeight_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudTermWeight_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroSimple_CourseCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroSimple_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroSimple_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroWeight_CoursesCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroWeight_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''  	   ,EnroWeight_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudSimple_CourseCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudSimple_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudSimple_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudWeight_CoursesCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudWeight_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudWeight_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,GradesFormat VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,GPAMethod VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,GradeBookAt VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,RetakenPolicy VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''    ) '' + CHAR(10);
                    --PRINT @SQL02;
                    EXECUTE ( @SQL02 );
                END;
        END;
        -- END --  01) Create Temp Table

        -- 02) -- Refresh the Credit Summary Table Just in case any modifications were made
        BEGIN
            EXECUTE dbo.Usp_Update_SyCreditsSummary @StuEnrollIdList = @StuEnrollIdList;
            EXECUTE dbo.Usp_UpdateTransferCredits_SyCreditsSummary @StuEnrollIdList = @StuEnrollIdList;
        END;
        --  END  --  02) -- Refresh the Credit Summary Table Just in case any modifications were made

        -- 03) Get courses taked to informe and courses used for GPA Calculations (not multiple enrollment -same term-  and not retaked)	
        BEGIN
            INSERT INTO #getStudentGPAbyTerms
                        SELECT    DISTINCT T1.StuEnrollId
                                 ,T1.TermId
                                 ,T1.TermDescrip
                                 ,T1.StartDate
                                 ,T1.EndDate
                                 ,ISNULL(ATES.DescripXTranscript, '''') AS DescripXTranscript
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                                 ,NULL
                        FROM      (
                                  SELECT     DISTINCT AR.StuEnrollId AS StuEnrollId
                                            ,T.TermId AS TermId
                                            ,T.TermDescrip AS TermDescrip
                                            ,T.StartDate AS StartDate
                                            ,T.EndDate AS EndDate
                                  FROM       dbo.arClassSections AS ACS
                                  INNER JOIN dbo.arResults AS AR ON AR.TestId = ACS.ClsSectionId
                                  INNER JOIN dbo.arTerm AS T ON T.TermId = ACS.TermId
								  INNER JOIN dbo.arStuEnrollments E ON AR.StuEnrollId = E.StuEnrollId
								  INNER JOIN dbo.arPrgVersions P ON P.PrgVerId = E.PrgVerId
                                  INNER JOIN (
                                             SELECT Val
                                             FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                             ) AS List ON List.Val = AR.StuEnrollId
                                  WHERE      AR.IsCourseCompleted = 1 OR P.ProgramRegistrationType = 1
                                  UNION
                                  SELECT     DISTINCT ATG.StuEnrollId
                                            ,T.TermId
                                            ,T.TermDescrip
                                            ,T.StartDate
                                            ,T.EndDate
                                  FROM       dbo.arTransferGrades AS ATG
                                  INNER JOIN dbo.arTerm AS T ON T.TermId = ATG.TermId
                                  INNER JOIN (
                                             SELECT Val
                                             FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                             ) AS List ON List.Val = ATG.StuEnrollId
                                  ) AS T1
                        LEFT JOIN dbo.arTermEnrollSummary AS ATES ON ATES.TermId = T1.TermId
                                                                     AND ATES.StuEnrollId = T1.StuEnrollId
                        ORDER BY  T1.StartDate
                                 ,T1.TermDescrip;

            INSERT INTO #getStudentGPAByEnrollment
                        SELECT     DISTINCT AL.StudentId
                                  ,AL.LastName
                                  ,AL.FirstName
                                  ,AL.MiddleName
                                  ,AL.SSN
                                  ,AL.StudentNumber
                                  ,ASE.StuEnrollId
                                  ,ASE.EnrollmentId
                                  ,APV.PrgVerId
                                  ,APV.PrgVerDescrip
                                  ,CASE WHEN ( APV.Credits > 0.0 ) THEN 1
                                        ELSE 0
                                   END AS PrgVersionTrackCredits
                                  ,APV.GrdSystemId
                                  ,CASE WHEN (
                                             APV.Credits > 0.0
                                             AND APV.Hours > 0
                                             )
                                             OR ( APV.IsContinuingEd = 1 ) THEN ''Credits-ClockHours''
                                        WHEN APV.Credits > 0.0 THEN ''Credits''
                                        WHEN APV.Hours > 0.0 THEN ''ClockHours''
                                        ELSE ''''
                                   END AcademicType
                                  ,CASE WHEN AP.ACId = 5 THEN ''True''
                                        ELSE ''False''
                                   END AS ClockHourProgram
                                  ,0 AS IsMakingSAP
                                  ,GSGAT.TermId
                                  --,NULL  --,GSGAT.TermDescrip
                                  ,0.0 AS TermAverage
                                  ,NULL AS EnroSimple_CourseCredits
                                  ,NULL AS EnroSimple_Credits
                                  ,NULL AS EnroSimple_GPA
                                  ,NULL AS EnroWeight_CoursesCredits
                                  ,NULL AS EnroWeight_GPA_Credits
                                  ,NULL AS EnroWeight_GPA
                                  ,NULL AS StudSimple_CourseCredits
                                  ,NULL AS StudSimple_GPA_Credits
                                  ,NULL AS StudSimple_GPA
                                  ,NULL AS StudWeight_CoursesCredits
                                  ,NULL AS StudWeight_GPA_Credits
                                  ,NULL AS StudWeight_GPA
                        FROM       adLeads AS AL --arStudent 
                        INNER JOIN arStuEnrollments ASE ON ASE.StudentId = AL.StudentId
                        INNER JOIN arPrgVersions APV ON ASE.PrgVerId = APV.PrgVerId
                        INNER JOIN arPrograms AS AP ON AP.ProgId = APV.ProgId
                        INNER JOIN #getStudentGPAbyTerms AS GSGAT ON GSGAT.StuEnrollId = ASE.StuEnrollId
                        INNER JOIN (
                                   SELECT DISTINCT CONVERT(UNIQUEIDENTIFIER, Val) AS StuEnrollId
                                   FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                   ) AS List ON List.StuEnrollId = ASE.StuEnrollId;

            INSERT INTO #CoursesTaken
                        SELECT     ASE.StudentId
                                  ,SCS.StuEnrollId AS StuEnrollId
                                  ,SCS.TermId AS TermId
                                  ,SCS.TermDescrip AS TermDescrip
                                  ,SCS.TermStartDate AS TermStartDate
                                  ,SCS.ReqId AS ReqId
                                  ,NULL AS ReqCode
                                  ,NULL AS ReqDescription
                                  ,NULL AS ReqCodeDescrip
                                  ,NULL AS ReqCredits
                                  ,NULL AS MinVal
                                  ,SCS.ClsSectionId AS ClsSectionId
                                  ,ISNULL(ACS.StartDate, @MaximunDate) AS CourseStarDate
                                  ,ISNULL(ACS.EndDate, @MaximunDate) AS CourseEndDate
                                  ,ISNULL(SCS.CreditsAttempted, 0) AS SCS_CreditsAttempted
                                  ,ISNULL(SCS.CreditsEarned, 0) AS SCS_CreditsEarned
                                  ,SCS.CurrentScore AS SCS_CurrentScore
                                  ,SCS.CurrentGrade AS SCS_CurrentGrade
                                  ,SCS.FinalScore AS SCS_FinalScore
                                  ,SCS.FinalGrade AS SCS_FinalGrade
                                  ,SCS.Completed AS SCS_Completed
                                  ,SCS.FinalGPA AS SCS_FinalGPA
                                  ,SCS.Product_WeightedAverage_Credits_GPA
                                  ,SCS.Count_WeightedAverage_Credits
                                  ,SCS.Product_SimpleAverage_Credits_GPA
                                  ,SCS.Count_SimpleAverage_Credits
                                  ,SCS.ModUser
                                  ,SCS.ModDate
                                  ,SCS.TermGPA_Simple
                                  ,SCS.TermGPA_Weighted
                                  ,SCS.coursecredits
                                  ,SCS.CumulativeGPA
                                  ,SCS.CumulativeGPA_Simple
                                  ,SCS.FACreditsEarned
                                  ,SCS.Average
                                  ,SCS.CumAverage
                                  ,0.0 AS ScheduleDays
                                  ,0.0 AS ActualDay
                                                                                      -- For Transfer Courses  (TR) SCS.FinalGPA should be Null so it will filter for GPA Calculations
                                  ,CASE WHEN ( LOWER(@RetakenPolicy) = ''average'' ) THEN AVG(SCS.FinalGPA) OVER ( PARTITION BY SCS.StuEnrollId
                                                                                                                             ,SCS.ReqId
                                                                                                               )
                                        ELSE SCS.FinalGPA
                                   END AS FinalGPA_Calculations
                                  ,CASE WHEN ( LOWER(@RetakenPolicy) = ''average'' ) THEN AVG(SCS.FinalGPA) OVER ( PARTITION BY SCS.StuEnrollId
                                                                                                                             ,SCS.TermId
                                                                                                                             ,SCS.ReqId
                                                                                                               )
                                        ELSE SCS.FinalGPA
                                   END AS FinalGPA_TermCalculations
                                  ,0.0 AS CreditsEarned_Calculations
                                  ,0.0 AS ActualDay_Calculations
                                  ,0 AS GrdBkWgtDetailsCount
                                  ,COUNT(*) OVER ( PARTITION BY ASE.StudentId --DISTINCT SCS.StuEnrollId
                                                               ,SCS.TermId
                                                               ,SCS.ReqId
                                                 ) AS CountMultipleEnrollment
                                  ,ROW_NUMBER() OVER ( PARTITION BY ASE.StudentId
                                                                   ,SCS.TermId
                                                                   ,SCS.ReqId
                                                       ORDER BY ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                               ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                               ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                               ,SCS.StuEnrollId
                                                     ) AS RowNumberMultipleEnrollment --  same course -same termRowNumberMultipleEnrollment INTEGER
                                  ,COUNT(*) OVER ( PARTITION BY SCS.StuEnrollId
                                                               ,SCS.ReqId
                                                 ) AS CountRepeated
                                  ,CASE WHEN ( LOWER(@RetakenPolicy) = ''latest'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.TermStartDate DESC
                                                                        ,ISNULL(ACS.EndDate, @MinimunDate) DESC   -- DESC Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MinimunDate) DESC -- DESC Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC                    -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                        WHEN ( LOWER(@RetakenPolicy) = ''average'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.TermStartDate
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                        WHEN ( LOWER(@RetakenPolicy) = ''best'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.FinalGPA DESC
																		,GSD.IsPass DESC
                                                                        ,SCS.TermStartDate DESC
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                        ELSE
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId -- ''best''
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.FinalGPA DESC
                                                                        ,SCS.TermStartDate
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                   END AS RowNumberRetaked
                                  ,COUNT(*) OVER ( PARTITION BY SCS.StuEnrollId
                                                               ,SCS.TermId
                                                               ,SCS.ReqId
                                                 ) AS SameTermCountRetaken
                                  ,CASE WHEN ( LOWER(@RetakenPolicy) = ''latest'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                            ,SCS.TermId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.TermStartDate DESC
                                                                        ,ISNULL(ACS.EndDate, @MinimunDate) DESC   -- DESC Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MinimunDate) DESC -- DESC Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC                    -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                        WHEN ( LOWER(@RetakenPolicy) = ''average'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                            ,SCS.TermId
                                                                            ,SCS.ReqId
                                                                ORDER BY ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                        WHEN ( LOWER(@RetakenPolicy) = ''best'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                            ,SCS.TermId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.FinalGPA DESC
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                        ELSE
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId -- ''best''
                                                                            ,SCS.TermId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.FinalGPA DESC
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate)
                                                                        ,SCS.ClsSectionId DESC -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                   END AS RowNumberSameTermRetaked
                                  ,ROW_NUMBER() OVER ( PARTITION BY ASE.StudentId
                                                                   ,SCS.TermId
                                                                   ,SCS.ReqId
                                                                   ,SCS.ClsSectionId
                                                       ORDER BY ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                               ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                               ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                               ,SCS.StuEnrollId
                                                     ) AS RowNumberMultEnrollNoRetaken
                                  ,CASE WHEN ( LOWER(@RetakenPolicy) = ''latest'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY ASE.StudentId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.TermStartDate DESC
                                                                        ,ISNULL(ACS.EndDate, @MinimunDate) DESC   -- DESC Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MinimunDate) DESC -- DESC Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC                    -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                                        ,SCS.StuEnrollId
                                                              )
                                        WHEN ( LOWER(@RetakenPolicy) = ''average'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY ASE.StudentId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.TermStartDate
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                                        ,SCS.StuEnrollId
                                                              )
                                        WHEN ( LOWER(@RetakenPolicy) = ''best'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY ASE.StudentId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.FinalGPA DESC
                                                                        ,SCS.TermStartDate
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                                        ,SCS.StuEnrollId
                                                              )
                                        ELSE
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId -- ''best''
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.FinalGPA DESC
                                                                        ,SCS.TermStartDate
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                                        ,SCS.StuEnrollId
                                                              )
                                   END AS RowNumberOverAllByClassSection
                                  ,GSD.IsCreditsEarned
                        FROM       dbo.syCreditSummary AS SCS
                        INNER JOIN dbo.arStuEnrollments AS ASE ON ASE.StuEnrollId = SCS.StuEnrollId
                        INNER JOIN (
                                   SELECT DISTINCT GSGBE.StuEnrollId AS StuEnrollId
                                   FROM   #getStudentGPAByEnrollment AS GSGBE
                                   ) AS List ON List.StuEnrollId = SCS.StuEnrollId
                        LEFT JOIN  dbo.arClassSections AS ACS ON ACS.ClsSectionId = SCS.ClsSectionId
                        INNER JOIN dbo.arPrgVersions AS PV ON PV.PrgVerId = ASE.PrgVerId
                        INNER JOIN dbo.arGradeSystems AS GS ON GS.GrdSystemId = PV.GrdSystemId
                        INNER JOIN dbo.arGradeSystemDetails AS GSD ON GSD.GrdSystemId = GS.GrdSystemId
                        WHERE      (
                                   SCS.FinalScore IS NOT NULL
                                   OR SCS.FinalGrade IS NOT NULL
                                   )
                                   AND SCS.FinalGrade = GSD.Grade
                        ORDER BY   SCS.TermStartDate
                                  ,SCS.TermDescrip
                                  ,SCS.ReqDescrip
                                  ,SCS.StuEnrollId;

        -- Update  RowNumberMultipleEnrollment for those that are retaken and multiple enrollment
        --UPDATE     CT3
        --SET        CT3.RowNumberMultEnrollNoRetaken = CT2.RowNumberMultEnrollNoRetaken
        --FROM       #CoursesTaken AS CT3
        --INNER JOIN (
        --           SELECT CT1.CoursesTakenId
        --                 ,ROW_NUMBER () OVER (PARTITION BY CT1.StudentId
        --                                                  ,CT1.TermId
        --                                                  ,CT1.ReqId
        --                                                  ,CT1.ClsSectionId
        --                                      ORDER BY CT1.StuEnrollId
        --                                              ,CT1.TermStartDate
        --                                              ,CT1.CourseEndDate
        --                                              ,CT1.CourseStarDate
        --                                     ) AS RowNumberMultEnrollNoRetaken
        --           FROM   #CoursesTaken AS CT1
        --           ) AS CT2 ON CT2.CoursesTakenId = CT3.CoursesTakenId;
        -- Update RowNumberOverAllByClassSection  TO 0 for those that are ClsSectionId IS NULL
        --UPDATE CT
        --SET    CT.RowNumberOverAllByClassSection = 0
        --FROM   #CoursesTaken AS CT
        --WHERE  CT.ClsSectionId IS NULL;
        END;
        -- END  --  03) Get courses taked to informe and courses used for GPA Calculations (not multiple enrollment -same term-  and not retaked)

        -- 04) Get GPA for StuEnrollId, TermId
        BEGIN
            DECLARE getNodes_Cursor CURSOR FAST_FORWARD FORWARD_ONLY FOR
                SELECT DISTINCT GSGBE.StudentId
                      ,GSGBE.StuEnrollId
                      ,GSGBE.TermId
                FROM   #getStudentGPAByEnrollment AS GSGBE;
            OPEN getNodes_Cursor;
            FETCH NEXT FROM getNodes_Cursor
            INTO @curStudentId
                ,@curStuEnrollId
                ,@curTermId;
            WHILE @@FETCH_STATUS = 0
                BEGIN
                    SET @cumTermAverage = (
                                          SELECT TOP 1 CT.SCS_Average
                                          FROM   #CoursesTaken AS CT
                                          WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                 AND CT.TermId IN ( @curTermId )
                                                 AND (
                                                     CT.RowNumberMultEnrollNoRetaken = 1
                                                     OR CT.RowNumberSameTermRetaked = 1
                                                     )
                                          );
                    SET @cumEnroTermSimple_CourseCredits = (
                                                           SELECT COUNT(CT.SCS_coursecredits)
                                                           FROM   #CoursesTaken AS CT
                                                           WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                                  AND CT.TermId IN ( @curTermId )
                                                                  AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                                  AND ( CT.RowNumberSameTermRetaked = 1
                                                                      --OR CT.RowNumberMultipleEnrollment = 1
                                                                      )
                                                           );
                    SET @cumEnroTermSimple_GPACredits = (
                                                        SELECT SUM(CT.FinalGPA_TermCalculations)
                                                        FROM   #CoursesTaken AS CT
                                                        WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                               AND CT.TermId IN ( @curTermId )
                                                               AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                               AND ( CT.RowNumberSameTermRetaked = 1
                                                                   --OR CT.RowNumberMultipleEnrollment = 1
                                                                   )
                                                        );
                    SET @cumEnroTermWeight_CourseCredits = (
                                                           SELECT SUM(CT.SCS_coursecredits)
                                                           FROM   #CoursesTaken AS CT
                                                           WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                                  AND CT.TermId IN ( @curTermId )
                                                                  AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                                  AND ( CT.RowNumberSameTermRetaked = 1
                                                                      --OR CT.RowNumberMultipleEnrollment = 1
                                                                      )
                                                           );
                    SET @cumEnroTermWeight_GPACredits = (
                                                        SELECT SUM(CT.SCS_coursecredits * CT.FinalGPA_TermCalculations)
                                                        FROM   #CoursesTaken AS CT
                                                        WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                               AND CT.TermId IN ( @curTermId )
                                                               AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                               AND ( CT.RowNumberSameTermRetaked = 1
                                                                   --OR CT.RowNumberMultipleEnrollment = 1
                                                                   )
                                                        );
                    SET @cumStudTermSimple_CourseCredits = (
                                                           SELECT COUNT(CT.SCS_coursecredits)
                                                           FROM   #CoursesTaken AS CT
                                                           WHERE  CT.StudentId IN ( @curStudentId )
                                                                  AND CT.TermId IN ( @curTermId )
                                                                  AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                                  AND ( CT.RowNumberSameTermRetaked = 1
                                                                      --OR CT.RowNumberMultipleEnrollment = 1
                                                                      )
                                                           );
                    SET @cumStudTermSimple_GPACredits = (
                                                        SELECT SUM(CT.FinalGPA_TermCalculations)
                                                        FROM   #CoursesTaken AS CT
                                                        WHERE  CT.StudentId IN ( @curStudentId )
                                                               AND CT.TermId IN ( @curTermId )
                                                               AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                               AND ( CT.RowNumberSameTermRetaked = 1
                                                                   --OR CT.RowNumberMultipleEnrollment = 1
                                                                   )
                                                        );
                    SET @cumStudTermWeight_CourseCredits = (
                                                           SELECT SUM(CT.SCS_coursecredits)
                                                           FROM   #CoursesTaken AS CT
                                                           WHERE  CT.StudentId IN ( @curStudentId )
                                                                  AND CT.TermId IN ( @curTermId )
                                                                  AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                                  AND ( CT.RowNumberSameTermRetaked = 1
                                                                      --OR CT.RowNumberMultipleEnrollment = 1
                                                                      )
                                                           );
                    SET @cumStudTermWeight_GPACredits = (
                                                        SELECT SUM(CT.SCS_coursecredits * CT.FinalGPA_TermCalculations)
                                                        FROM   #CoursesTaken AS CT
                                                        WHERE  CT.StudentId IN ( @curStudentId )
                                                               AND CT.TermId IN ( @curTermId )
                                                               AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                               AND ( CT.RowNumberSameTermRetaked = 1
                                                                   --OR CT.RowNumberMultipleEnrollment = 1
                                                                   )
                                                        );

                    IF @cumEnroTermSimple_CourseCredits > 0
                        BEGIN
                            SET @cumEnroTermSimple_GPA = @cumEnroTermSimple_GPACredits / @cumEnroTermSimple_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumEnroTermSimple_GPA = 0.0;
                        END;
                    IF @cumEnroTermWeight_CourseCredits > 0.0
                        BEGIN
                            SET @cumEnroTermWeight_GPA = @cumEnroTermWeight_GPACredits / @cumEnroTermWeight_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumEnroTermWeight_GPA = 0.0;
                        END;
                    IF @cumStudTermSimple_CourseCredits > 0.0
                        BEGIN
                            SET @cumStudTermSimple_GPA = @cumStudTermSimple_GPACredits / @cumStudTermSimple_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumStudTermSimple_GPA = 0.0;
                        END;
                    IF @cumStudTermWeight_CourseCredits > 0.0
                        BEGIN
                            SET @cumStudTermWeight_GPA = @cumStudTermWeight_GPACredits / @cumStudTermWeight_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumStudTermWeight_GPA = 0.0;
                        END;

                    UPDATE GSGAT
                    SET    GSGAT.TermAverage = @cumTermAverage
                          ,GSGAT.EnroTermSimple_CourseCredits = @cumEnroTermSimple_CourseCredits
                          ,GSGAT.EnroTermSimple_GPACredits = @cumEnroTermSimple_GPACredits
                          ,GSGAT.EnroTermSimple_GPA = @cumEnroTermSimple_GPA
                          ,GSGAT.EnroTermWeight_CoursesCredits = @cumEnroTermWeight_CourseCredits
                          ,GSGAT.EnroTermWeight_GPACredits = @cumEnroTermWeight_GPACredits
                          ,GSGAT.EnroTermWeight_GPA = @cumEnroTermWeight_GPA
                          ,GSGAT.StudTermSimple_CourseCredits = @cumStudTermSimple_CourseCredits
                          ,GSGAT.StudTermSimple_GPACredits = @cumStudTermSimple_GPACredits
                          ,GSGAT.StudTermSimple_GPA = @cumStudTermSimple_GPA
                          ,GSGAT.StudTermWeight_CoursesCredits = @cumStudTermWeight_CourseCredits
                          ,GSGAT.StudTermWeight_GPACredits = @cumStudTermWeight_GPACredits
                          ,GSGAT.StudTermWeight_GPA = @cumStudTermWeight_GPA
                    FROM   #getStudentGPAbyTerms AS GSGAT
                    WHERE  GSGAT.StuEnrollId = @curStuEnrollId
                           AND GSGAT.TermId = @curTermId;

                    FETCH NEXT FROM getNodes_Cursor
                    INTO @curStudentId
                        ,@curStuEnrollId
                        ,@curTermId;
                END;
            CLOSE getNodes_Cursor;
            DEALLOCATE getNodes_Cursor;
        END;
        -- END  --  04)Get GPA for StuEnrollId, TermId

        -- 05) Get GPA for StudentId and StuEnrollId
        BEGIN
            DECLARE getNodes_Cursor CURSOR FAST_FORWARD FORWARD_ONLY FOR
                SELECT DISTINCT GSGBE.StudentId
                      ,GSGBE.StuEnrollId
                FROM   #getStudentGPAByEnrollment AS GSGBE;
            OPEN getNodes_Cursor;
            FETCH NEXT FROM getNodes_Cursor
            INTO @curStudentId
                ,@curStuEnrollId;
            WHILE @@FETCH_STATUS = 0
                BEGIN

                    SET @cumEnroSimple_CourseCredits = (
                                                       SELECT COUNT(CT.SCS_coursecredits)
                                                       FROM   #CoursesTaken AS CT
                                                       WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                              AND CT.FinalGPA_Calculations IS NOT NULL
                                                              AND CT.RowNumberRetaked = 1
                                                       );
                    SET @cumEnroSimple_GPACredits = (
                                                    SELECT SUM(CT.FinalGPA_Calculations)
                                                    FROM   #CoursesTaken AS CT
                                                    WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                           AND CT.FinalGPA_Calculations IS NOT NULL
                                                           AND CT.RowNumberRetaked = 1
                                                    );
                    SET @cumEnroWeight_CourseCredits = (
                                                       SELECT SUM(CT.SCS_coursecredits)
                                                       FROM   #CoursesTaken AS CT
                                                       WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                              AND CT.FinalGPA_Calculations IS NOT NULL
                                                              AND CT.RowNumberRetaked = 1
                                                       );
                    SET @cumEnroWeight_GPACredits = (
                                                    SELECT SUM(CT.SCS_coursecredits * CT.FinalGPA_Calculations)
                                                    FROM   #CoursesTaken AS CT
                                                    WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                           AND CT.FinalGPA_Calculations IS NOT NULL
                                                           AND CT.RowNumberRetaked = 1
                                                    );
                    SET @cumStudSimple_CourseCredits = (
                                                       SELECT COUNT(CT.SCS_coursecredits)
                                                       FROM   #CoursesTaken AS CT
                                                       WHERE  CT.StudentId IN ( @curStudentId )
                                                              AND CT.FinalGPA_Calculations IS NOT NULL
                                                              AND CT.RowNumberRetaked = 1
                                                              AND RowNumberOverAllByClassSection = 1
                                                       );
                    SET @cumStudSimple_GPACredits = (
                                                    SELECT SUM(CT.FinalGPA_Calculations)
                                                    FROM   #CoursesTaken AS CT
                                                    WHERE  CT.StudentId IN ( @curStudentId )
                                                           AND CT.FinalGPA_Calculations IS NOT NULL
                                                           AND CT.RowNumberRetaked = 1
                                                           AND RowNumberOverAllByClassSection = 1
                                                    );
                    SET @cumStudWeight_CourseCredits = (
                                                       SELECT SUM(CT.SCS_coursecredits)
                                                       FROM   #CoursesTaken AS CT
                                                       WHERE  CT.StudentId IN ( @curStudentId )
                                                              AND CT.FinalGPA_Calculations IS NOT NULL
                                                              AND CT.RowNumberRetaked = 1
                                                              AND RowNumberOverAllByClassSection = 1
                                                       );
                    SET @cumStudWeight_GPACredits = (
                                                    SELECT SUM(CT.SCS_coursecredits * CT.FinalGPA_Calculations)
                                                    FROM   #CoursesTaken AS CT
                                                    WHERE  CT.StudentId IN ( @curStudentId )
                                                           AND CT.FinalGPA_Calculations IS NOT NULL
                                                           AND RowNumberOverAllByClassSection = 1
                                                           AND CT.RowNumberRetaked = 1
                                                    );

                    IF @cumEnroSimple_CourseCredits > 0.0
                        BEGIN
                            SET @cumEnroSimple_GPA = @cumEnroSimple_GPACredits / @cumEnroSimple_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumEnroSimple_GPA = 0.0;
                        END;
                    IF @cumStudSimple_CourseCredits > 0.0
                        BEGIN
                            SET @cumStudSimple_GPA = @cumStudSimple_GPACredits / @cumStudSimple_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumStudSimple_GPA = 0.0;
                        END;
                    IF @cumEnroWeight_CourseCredits > 0.0
                        BEGIN
                            SET @cumEnroWeight_GPA = @cumEnroWeight_GPACredits / @cumEnroWeight_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumEnroWeight_GPA = 0.0;
                        END;
                    IF @cumStudWeight_CourseCredits > 0.0
                        BEGIN
                            SET @cumStudWeight_GPA = @cumStudWeight_GPACredits / @cumStudWeight_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumStudWeight_CourseCredits = 0.0;
                        END;
                    --********************* Making SAP *******************************************************
                    SELECT @IsMakingSAP = ASCR1.IsMakingSAP
                    FROM   (
                           SELECT ROW_NUMBER() OVER ( PARTITION BY ASCR.StuEnrollId
                                                      ORDER BY ASCR.DatePerformed DESC
                                                    ) AS N
                                 ,ISNULL(ASCR.IsMakingSAP, 0) AS IsMakingSAP
                           FROM   arSAPChkResults AS ASCR
                           WHERE  ASCR.StuEnrollId = @curStuEnrollId
                           ) AS ASCR1
                    WHERE  ASCR1.N = 1;
                    --********************* Update #getStudentGPAByEnrollment *******************************************************
                    UPDATE GSGBE
                    SET    GSGBE.IsMakingSAP = ISNULL(@IsMakingSAP, 0)
                          ,GSGBE.EnroSimple_CourseCredits = @cumEnroSimple_CourseCredits
                          ,GSGBE.EnroSimple_GPACredits = @cumEnroSimple_GPACredits
                          ,GSGBE.EnroSimple_GPA = @cumEnroSimple_GPA
                          ,GSGBE.EnroWeight_CoursesCredits = @cumEnroWeight_CourseCredits
                          ,GSGBE.EnroWeight_GPACredits = @cumEnroWeight_GPACredits
                          ,GSGBE.EnroWeight_GPA = @cumEnroWeight_GPA
                          ,GSGBE.StudSimple_CourseCredits = @cumStudSimple_CourseCredits
                          ,GSGBE.StudSimple_GPACredits = @cumStudSimple_GPACredits
                          ,GSGBE.StudSimple_GPA = @cumStudSimple_GPA
                          ,GSGBE.StudWeight_CoursesCredits = @cumStudWeight_CourseCredits
                          ,GSGBE.StudWeight_GPA_Credits = @cumStudWeight_GPACredits
                          ,GSGBE.StudWeight_GPA = @cumStudWeight_GPA
                    FROM   #getStudentGPAByEnrollment AS GSGBE
                    WHERE  GSGBE.StuEnrollId = @curStuEnrollId;

                    FETCH NEXT FROM getNodes_Cursor
                    INTO @curStudentId
                        ,@curStuEnrollId;
                END;
            CLOSE getNodes_Cursor;
            DEALLOCATE getNodes_Cursor;
        END;
        -- END  -- 05) Get GPA for StudentId and StuEnrollId

        -- 06) Get ScheduleDays and ActualDay for each course taked (coursed and tranferred)
        BEGIN
            UPDATE     CT
            SET        CT.ReqCode = AR.Code
                      ,CT.ReqDescription = AR.Descrip
                      ,CT.ReqCodeDescrip = ''('' + AR.Code + '' ) '' + AR.Descrip
                      ,CT.ReqCredits = AR.Credits
                      ,CT.CreditsEarned_Calculations = ( CASE WHEN (
                                                                   CT.SCS_CreditsEarned > 0
                                                                   AND CT.RowNumberRetaked <> 1
                                                                   ) THEN 0.0
                                                              ELSE CT.SCS_CreditsEarned
                                                         END
                                                       )
                      ,CT.ScheduleDays = AR.Hours
                      ,CT.ActualDay = ( CASE WHEN ( CT.IsCreditsEarned = 0 ) THEN 0.0
                                             ELSE AR.Hours
                                        END
                                      )
                      ,CT.ActualDay_Calculations = ( CASE WHEN (
                                                               CT.IsCreditsEarned = 1
                                                               AND CT.RowNumberRetaked <> 1
                                                               ) THEN 0.0
                                                          WHEN ( CT.IsCreditsEarned = 0 ) THEN 0.0
                                                          ELSE AR.Hours
                                                     END
                                                   )
                      ,CT.MinVal = (
                                   SELECT     MIN(GCD.MinVal)
                                   FROM       dbo.arGradeScaleDetails GCD
                                   INNER JOIN dbo.arGradeSystemDetails GSD ON GSD.GrdSysDetailId = GCD.GrdSysDetailId
                                   WHERE      GSD.IsPass = 1
                                              AND GCD.GrdScaleId = ACS.GrdScaleId
                                   )
                      ,CT.GrdBkWgtDetailsCount = (
                                                 SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                                 FROM   dbo.arGrdBkResults AS AGBR
                                                 WHERE  AGBR.StuEnrollId = CT.StuEnrollId
                                                        AND AGBR.ClsSectionId = CT.ClsSectionId
                                                 )
            FROM       #CoursesTaken AS CT
            INNER JOIN #getStudentGPAByEnrollment GSGBE ON GSGBE.StuEnrollId = CT.StuEnrollId
                                                           AND GSGBE.TermId = CT.TermId
            INNER JOIN dbo.arClassSections AS ACS ON ACS.ClsSectionId = CT.ClsSectionId
                                                     AND ACS.ReqId = CT.ReqId
            INNER JOIN dbo.arReqs AS AR ON AR.ReqId = CT.ReqId;

            UPDATE     CT
            SET        CT.ReqCode = AR.Code
                      ,CT.ReqDescription = AR.Descrip
                      ,CT.ReqCodeDescrip = ''('' + AR.Code + '' ) '' + AR.Descrip
                      ,CT.ReqCredits = AR.Credits
                      ,CT.CreditsEarned_Calculations = ( CASE WHEN (
                                                                   CT.SCS_CreditsEarned > 0
                                                                   AND CT.RowNumberRetaked <> 1
                                                                   ) THEN 0.0
                                                              ELSE CT.SCS_CreditsEarned
                                                         END
                                                       )
                      ,CT.ScheduleDays = AR.Hours
                      ,CT.ActualDay = ( CASE WHEN ( CT.IsCreditsEarned = 0 ) THEN 0.0
                                             ELSE AR.Hours
                                        END
                                      )
                      ,CT.ActualDay_Calculations = ( CASE WHEN (
                                                               CT.IsCreditsEarned = 1
                                                               AND CT.RowNumberRetaked <> 1
                                                               ) THEN 0.0
                                                          WHEN ( CT.IsCreditsEarned = 0 ) THEN 0.0
                                                          ELSE AR.Hours
                                                     END
                                                   )
                      ,CT.MinVal = (
                                   SELECT     MIN(GCD.MinVal)
                                   FROM       dbo.arGradeScaleDetails GCD
                                   INNER JOIN dbo.arGradeSystemDetails GSD ON GSD.GrdSysDetailId = GCD.GrdSysDetailId
                                   WHERE      GSD.IsPass = 1
                                   )
                      ,CT.GrdBkWgtDetailsCount = (
                                                 SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                                 FROM   dbo.arGrdBkResults AS AGBR
                                                 WHERE  AGBR.StuEnrollId = CT.StuEnrollId
                                                        AND AGBR.ClsSectionId = CT.ClsSectionId
                                                 )
            FROM       #CoursesTaken AS CT
            INNER JOIN #getStudentGPAByEnrollment GSGBE ON GSGBE.StuEnrollId = CT.StuEnrollId
                                                           AND GSGBE.TermId = CT.TermId
            INNER JOIN dbo.arTransferGrades AS ATG ON ATG.StuEnrollId = CT.StuEnrollId
                                                      AND ATG.TermId = CT.TermId
                                                      AND ATG.ReqId = CT.ReqId
            INNER JOIN dbo.arReqs AS AR ON AR.ReqId = CT.ReqId;

        END;
        -- END  --  06) Get ScheduleDays and ActualDay for each course taked (coursed and tranferred)

        -- 07) Summary update
        BEGIN
            INSERT INTO #GPASummary
                        SELECT     GSGBE.StudentId
                                  ,GSGBE.LastName
                                  ,GSGBE.FirstName
                                  ,GSGBE.MiddleName
                                  ,GSGBE.SSN
                                  ,GSGBE.StudentNumber
                                  ,GSGBE.StuEnrollId
                                  ,GSGBE.EnrollmentID
                                  ,GSGBE.PrgVerId
                                  ,GSGBE.PrgVerDescrip
                                  ,GSGBE.PrgVersionTrackCredits
                                  ,GSGBE.GrdSystemId
                                  ,GSGBE.AcademicType AS AcademicType
                                  ,GSGBE.ClockHourProgram AS ClockHourProgram
                                  ,GSGBE.IsMakingSAP AS IsMakingSAP
                                  ,GSGAT.TermId
                                  ,GSGAT.TermDescrip
                                  ,GSGAT.TermStartDate
                                  ,GSGAT.TermEndDate
                                  ,GSGAT.DescripXTranscript
                                  ,GSGAT.TermAverage
                                  ,GSGAT.EnroTermSimple_CourseCredits
                                  ,GSGAT.EnroTermSimple_GPACredits
                                  ,GSGAT.EnroTermSimple_GPA
                                  ,GSGAT.EnroTermWeight_CoursesCredits
                                  ,GSGAT.EnroTermWeight_GPACredits
                                  ,GSGAT.EnroTermWeight_GPA
                                  ,GSGAT.StudTermSimple_CourseCredits
                                  ,GSGAT.StudTermSimple_GPACredits
                                  ,GSGAT.StudTermSimple_GPA
                                  ,GSGAT.StudTermWeight_CoursesCredits
                                  ,GSGAT.StudTermWeight_GPACredits
                                  ,GSGAT.StudTermWeight_GPA
                                  ,GSGBE.EnroSimple_CourseCredits
                                  ,GSGBE.EnroSimple_GPACredits
                                  ,GSGBE.EnroSimple_GPA
                                  ,GSGBE.EnroWeight_CoursesCredits
                                  ,GSGBE.EnroWeight_GPACredits
                                  ,GSGBE.EnroWeight_GPA
                                  ,GSGBE.StudSimple_CourseCredits
                                  ,GSGBE.StudSimple_GPACredits
                                  ,GSGBE.StudSimple_GPA
                                  ,GSGBE.StudWeight_CoursesCredits
                                  ,GSGBE.StudWeight_GPA_Credits
                                  ,GSGBE.StudWeight_GPA
                                  ,@GradesFormat AS GradesFormat
                                  ,@GPAMethod AS GPAMethod
                                  ,@GradeBookAt AS GradeBookAt
                                  ,@RetakenPolicy AS RetakenPolicy
                        FROM       #getStudentGPAbyTerms AS GSGAT
                        INNER JOIN #getStudentGPAByEnrollment GSGBE ON GSGBE.StuEnrollId = GSGAT.StuEnrollId
                                                                       AND GSGAT.TermId = GSGBE.TermId
                        ORDER BY   GSGBE.StudentId
                                  ,GSGBE.StuEnrollId
                                  ,GSGAT.TermStartDate
                                  ,GSGAT.TermDescrip
                                  ,GSGAT.TermId;

        END;
        -- END  --  07) Summary update and 

        -- to Test
        --SELECT * FROM #getStudentGPAbyTerms AS GSGAT ORDER BY  GSGAT.StuEnrollId, GSGAT.TermId
        --SELECT * FROM #getStudentGPAByEnrollment AS GSGBE
        --SELECT * FROM #CoursesTaken AS CT ORDER BY CT.StudentId, CT.StuEnrollId, CT.TermId
        --SELECT * FROM #GPASummary AS GS ORDER BY GS.StudentId, GS.StuEnrollId, GS.TermId

        -- 08) Save saving prepared GPA Information
        BEGIN
            SET @SQL01 = '''';
            SET @SQL01 = @SQL01 + ''INSERT INTO tempdb.dbo.'' + @CoursesTakenTableName + '' '' + CHAR(10);
            SET @SQL01 = @SQL01 + ''SELECT CT.* '' + CHAR(10);
            SET @SQL01 = @SQL01 + ''FROM  #CoursesTaken AS CT '' + CHAR(10);
            --PRINT @SQL01;
            EXECUTE ( @SQL01 );

            SET @SQL02 = '''';
            SET @SQL02 = @SQL02 + ''INSERT INTO tempdb.dbo.'' + @GPASummaryTableName + '' '' + CHAR(10);
            SET @SQL02 = @SQL02 + ''       SELECT GS.*     '' + CHAR(10);
            SET @SQL02 = @SQL02 + ''       FROM  #GPASummary AS GS '' + CHAR(10);
            --PRINT @SQL02;
            EXECUTE ( @SQL02 );
        END;
        -- END  --08) Save saving prepared GPA Information

        --09) DropTempTables
        BEGIN
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#CoursesTaken'')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#getStudentGPAbyTerms'')
                      )
                BEGIN
                    DROP TABLE #getStudentGPAbyTerms;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#getStudentGPAByEnrollment'')
                      )
                BEGIN
                    DROP TABLE #getStudentGPAByEnrollment;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#GPASummary'')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpEquivalentCoursesTakenByStudent'')
                      )
                BEGIN
                    DROP TABLE #tmpEquivalentCoursesTakenByStudent;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpStudentsWhoHasGradedEquivalentCourse'')
                      )
                BEGIN
                    DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourse;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpEquivalentCoursesTakenByStudentInTerm'')
                      )
                BEGIN
                    DROP TABLE #tmpEquivalentCoursesTakenByStudentInTerm;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpStudentsWhoHasGradedEquivalentCourseInTerm'')
                      )
                BEGIN
                    DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourseInTerm;
                END;
        END;
    -- END  --  09) DropTempTables

    -- 10) show tables names
    --BEGIN
    --    SELECT @CoursesTakenTableName AS CoursesTakenTableName
    --          ,@GPASummaryTableName AS GPASummaryTableName;
    --END;
    -- END  --  10) show tables names
    END;
-- =========================================================================================================
-- END  --  USP_TR_Sub03_PrepareGPA
-- =========================================================================================================

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END