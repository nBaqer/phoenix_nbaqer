﻿/*
Run this script on:

        dev-com-db1\Adv.Tricoci410    -  This database will be modified

to synchronize it with a database with the schema represented by:

        Source

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.7.10021 from Red Gate Software Ltd at 6/19/2019 4:17:11 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
/*
* Use this Pre-Deployment script to perform tasks before the deployment of the project.
* Read more at https://www.red-gate.com/SOC7/pre-deployment-script-help
*/
UPDATE dbo.arClsSectMeetings
SET PeriodId = NULL
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)

DELETE FROM syPeriodsWorkDays
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[arPrgGrp]'
GO
IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_arPrgGrp_syCampGrps_CampGrpId_CampGrpId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[arPrgGrp]', 'U'))
ALTER TABLE [dbo].[arPrgGrp] DROP CONSTRAINT [FK_arPrgGrp_syCampGrps_CampGrpId_CampGrpId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [UIX_arPrograms_ProgCode_ProgDescrip_CampGrpId] from [dbo].[arPrgGrp]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'UIX_arPrograms_ProgCode_ProgDescrip_CampGrpId' AND object_id = OBJECT_ID(N'[dbo].[arPrgGrp]'))
DROP INDEX [UIX_arPrograms_ProgCode_ProgDescrip_CampGrpId] ON [dbo].[arPrgGrp]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[usp_EdExpress_GetPendingPayments]'
GO
IF OBJECT_ID(N'[dbo].[usp_EdExpress_GetPendingPayments]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[usp_EdExpress_GetPendingPayments]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_GetIn-SchoolToday]'
GO
IF OBJECT_ID(N'[dbo].[USP_GetIn-SchoolToday]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_GetIn-SchoolToday]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[GetGradesFormatGivenStudentEnrollId]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetGradesFormatGivenStudentEnrollId]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[GetGradesFormatGivenStudentEnrollId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[GetACIdForStudent]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetACIdForStudent]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[GetACIdForStudent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[CalculateStudentAverage]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateStudentAverage]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[CalculateStudentAverage]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[GetSchoolNameAndCampusAddress]'
GO
IF OBJECT_ID(N'[dbo].[GetSchoolNameAndCampusAddress]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[GetSchoolNameAndCampusAddress]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_TR_Sub11_ComponentsByCourse_GivenComponentTypeId]'
GO
IF OBJECT_ID(N'[dbo].[Usp_TR_Sub11_ComponentsByCourse_GivenComponentTypeId]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_TR_Sub11_ComponentsByCourse_GivenComponentTypeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[StudentScheduledHours]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StudentScheduledHours]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[StudentScheduledHours]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[RS_ProgramDetails_Courses]'
GO
IF OBJECT_ID(N'[dbo].[RS_ProgramDetails_Courses]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[RS_ProgramDetails_Courses]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_TR_Sub06_Courses]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub06_Courses]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_TR_Sub06_Courses]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_TitleIV_QualitativeAndQuantitative]'
GO
IF OBJECT_ID(N'[dbo].[USP_TitleIV_QualitativeAndQuantitative]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_TitleIV_QualitativeAndQuantitative]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_PR_Sub3_Terms_forAllEnrolmnents]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub3_Terms_forAllEnrolmnents]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_PR_Sub3_Terms_forAllEnrolmnents]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_EnrollLead]'
GO
IF OBJECT_ID(N'[dbo].[USP_EnrollLead]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_EnrollLead]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_PR_Sub2_Enrollment_Summary]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub2_Enrollment_Summary]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_PR_Sub2_Enrollment_Summary]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_TR_Sub07_TotalCourses]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub07_TotalCourses]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_TR_Sub07_TotalCourses]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_GetAbsentToday]'
GO
IF OBJECT_ID(N'[dbo].[USP_GetAbsentToday]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_GetAbsentToday]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[usp_GetClassroomWorkServices]'
GO
IF OBJECT_ID(N'[dbo].[usp_GetClassroomWorkServices]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[usp_GetClassroomWorkServices]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_GPACalculator]'
GO
IF OBJECT_ID(N'[dbo].[USP_GPACalculator]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_GPACalculator]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[arR2T4OverrideInput]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[arR2T4OverrideInput] ALTER COLUMN [CompletedTime] [decimal] (18, 2) NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[arR2T4OverrideInput] ALTER COLUMN [TotalTime] [decimal] (18, 2) NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[arR2T4OverrideResults]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[arR2T4OverrideResults] ALTER COLUMN [CompletedTime] [decimal] (18, 2) NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[arR2T4OverrideResults] ALTER COLUMN [TotalTime] [decimal] (18, 2) NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[arR2T4Results]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[arR2T4Results] ALTER COLUMN [CompletedTime] [decimal] (18, 2) NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[arR2T4Results] ALTER COLUMN [TotalTime] [decimal] (18, 2) NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudent]'
GO
IF OBJECT_ID(N'[dbo].[arStudent]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CalculateStudentAverage]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateStudentAverage]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'-- =============================================
-- Author:		FAME Inc.
-- Create date: 6/11/2019
-- Description:	Calculated Student GPA Based on a set of parameters - Numeric ( Weighted & Unweighted currently implemented)
--Referenced in :
--[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents] -> via db function CalculateStudentAverage
--[Usp_PR_Sub3_Terms_forAllEnrolmnents] -> via db function CalculateStudentAverage
--ANY CHANGES TO THIS FILE MUST BE REFLECTED IN SP GPA_Calculator
-- =============================================
CREATE FUNCTION [dbo].[CalculateStudentAverage]
    (
        @EnrollmentId VARCHAR(50)
       ,@BeginDate DATETIME = NULL
       ,@EndDate DATETIME = NULL
       ,@ClassId UNIQUEIDENTIFIER = NULL
       ,@TermId UNIQUEIDENTIFIER = NULL
    )
RETURNS DECIMAL(16, 2)
AS
    BEGIN
        DECLARE @GPAResult AS DECIMAL(16, 2);
        DECLARE @UseWeightForGPA BIT = 1;

        IF ( @EnrollmentId IS NULL )
            RETURN @GPAResult;

        SET @UseWeightForGPA = (
                               SELECT TOP 1 PV.DoCourseWeightOverallGPA
                               FROM   dbo.arStuEnrollments E
                               JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                               WHERE  E.StuEnrollId = @EnrollmentId
                               );

        -----------------Numeric GPA Grades Format------------------------

        SET @GPAResult = (
                         SELECT ( CASE WHEN @UseWeightForGPA = 1 THEN
                                           ROUND(( SUM(b.CourseWeight * b.WeightedCourseAverage / 100) / SUM(b.CourseWeight)) * 100, 2)
                                       ELSE AVG(b.UnweightedCourseAverage)
                                  END
                                ) AS WeightedGPA
                         FROM   (
                                SELECT   SUM(OurSingleClassGradeFactor) AS CourseFactor
                                        ,SUM(a.GradeWeight) AS GradeWeight
                                        ,SUM(a.Score) AS SumOfScores
                                        ,( SUM(OurSingleClassGradeFactor) / SUM(a.GradeWeight)) * 100 AS WeightedCourseAverage
                                        ,( AVG(a.Score)) AS UnweightedCourseAverage
                                        ,ClsSectionId
                                        ,a.CourseWeight
                                FROM     (
                                         SELECT ( c.Weight * a.Score / 100 ) AS OurSingleClassGradeFactor
                                               ,c.Weight AS GradeWeight
                                               ,a.Score
                                               ,a.ClsSectionId
                                               ,d.CourseWeight
                                               ,c.Descrip AS ClassDescrip
                                         FROM   (
                                                SELECT   StuEnrollId
                                                        ,a.ClsSectionId
                                                        ,a.InstrGrdBkWgtDetailId
                                                        ,AVG(Score) AS Score
                                                FROM     dbo.arGrdBkResults a
                                                JOIN     dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                                JOIN     dbo.arGrdComponentTypes f ON f.GrdComponentTypeId = c.GrdComponentTypeId
                                                JOIN     dbo.arClassSections ON arClassSections.ClsSectionId = a.ClsSectionId
                                                WHERE    a.StuEnrollId = @EnrollmentId
                                                         --AND a.IsCompGraded = 1
                                                         AND a.Score IS NOT NULL
                                                         AND a.PostDate IS NOT NULL
                                                         AND (
                                                             @EndDate IS NULL
                                                             OR ( a.PostDate <= @EndDate )
                                                             )
                                                         AND (
                                                             @ClassId IS NULL
                                                             OR ( @ClassId = a.ClsSectionId )
                                                             )
                                                         AND (
                                                             @TermId IS NULL
                                                             OR ( @TermId = TermId )
                                                             )
                                                GROUP BY StuEnrollId
                                                        ,a.ClsSectionId
                                                        ,a.InstrGrdBkWgtDetailId
                                                ) a -- students grades
                                         JOIN   dbo.arClassSections b ON b.ClsSectionId = a.ClsSectionId
                                         JOIN   dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                         JOIN   dbo.arProgVerDef d ON d.ReqId = b.ReqId
                                         --ORDER BY f.Descrip, a.Score desc
                                         ) a
                                GROUP BY ClsSectionId
                                        ,a.CourseWeight

                                --ORDER BY CourseGPA DESC
                                ) b
                         );
        --END;
        RETURN @GPAResult;
    END;





'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[GetACIdForStudent]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetACIdForStudent]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'-- =============================================
-- Author:		FAME Inc.
-- Create date: 6/11/2019
-- Description:	Given student enrollment id, returns the ACId for the program version student is enrolled in 
-- =============================================
CREATE   FUNCTION [dbo].[GetACIdForStudent]
    (
        @EnrollmentId VARCHAR(50)
    )
RETURNS INT
AS
    BEGIN
        RETURN (
               SELECT TOP 1 ACId
               FROM   dbo.arStuEnrollments
               JOIN   dbo.arPrgVersions ON arPrgVersions.PrgVerId = arStuEnrollments.PrgVerId
               JOIN   dbo.arPrograms ON arPrograms.ProgId = arPrgVersions.ProgId
               WHERE  dbo.arStuEnrollments.StuEnrollId = @EnrollmentId
               );
    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[GetGradesFormatGivenStudentEnrollId]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetGradesFormatGivenStudentEnrollId]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'-- =============================================
-- Author:		FAME Inc.
-- Create date: 6/11/2019
-- Description:	Gets grade format based on campus student enrollment is tied to 
-- =============================================
CREATE   FUNCTION [dbo].[GetGradesFormatGivenStudentEnrollId]
    (
        @StuEnrollId VARCHAR(50)
    )
RETURNS VARCHAR(50)
AS
    BEGIN
        RETURN ( LOWER(LTRIM(RTRIM((
                                   SELECT dbo.GetAppSettingValueByKeyName(''GradesFormat''
                                                                         ,(
                                                                          SELECT CampusId
                                                                          FROM   dbo.arStuEnrollments
                                                                          WHERE  StuEnrollId = @StuEnrollId
                                                                          )
                                                                         )
                                   )
                                  )
                            )
                      )
               );
    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[StudentScheduledHours]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[StudentScheduledHours]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'-- =============================================
-- Author:		FAME Inc.
-- Create date: 5/23/2019
-- Description:	Student scheduled table valued function. Returns table student enrollment id , scheduled hours for day of the week , students must all be from same single campus
CREATE FUNCTION [dbo].[StudentScheduledHours]
    (
        @StuEnrollIdList VARCHAR(MAX) = NULL
       ,@Date DATETIME
    )
RETURNS TABLE
AS
    RETURN (
           SELECT ScheduledData.StuEnrollId
                 ,CASE WHEN ScheduledData.ScheduleDetailId IS NULL THEN 0
                       WHEN CAST(( ScheduledData.ScheduledMinutes - ScheduledData.LunchMinutes - ScheduledData.HolidayMinutes + LunchHolidayOverlapMinutes )
                                 / 60.0 AS DECIMAL(16, 2)) < 0 THEN 0
                       ELSE
                           CAST(( ScheduledData.ScheduledMinutes - ScheduledData.LunchMinutes - ScheduledData.HolidayMinutes + LunchHolidayOverlapMinutes )
                                / 60.0 AS DECIMAL(16, 2))
                  END AS CalculatedScheduledHours
                 ,ScheduledData.ScheduleId
           FROM   (
                  SELECT      arStudentSchedules.StuEnrollId
                             ,CampusId
                             ,total
                             ,timein
                             ,timeout
                             ,lunchin
                             ,lunchout
                             ,arProgSchedules.ScheduleId
                             ,dbo.arProgScheduleDetails.ScheduleDetailId
                             ,ISNULL(DATEDIFF(MINUTE, timein, timeout), 0) AS ScheduledMinutes
                             ,ISNULL(DATEDIFF(MINUTE, lunchout, lunchin), 0) AS LunchMinutes
                             ,ISNULL(
                                        CASE WHEN Holiday.AllDay = 1 THEN DATEDIFF(MINUTE, timein, timeout)
                                             WHEN (
                                                  CAST(Holiday.BeginTimeInteral AS TIME) <= CAST(timeout AS TIME)
                                                  AND CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME) >= CAST(timein AS TIME)
                                                  ) -- holiday falls entirely inside students schedule

                                        THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME))
                                             WHEN (
                                                  CAST(Holiday.BeginTimeInteral AS TIME) >= CAST(timein AS TIME)
                                                  AND CAST(Holiday.BeginTimeInteral AS TIME) <= CAST(timeout AS TIME)
                                                  ) -- holiday start time falls betweem time in and time out 
                                        THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(timeout AS TIME))
                                             WHEN (
                                                  CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME) >= CAST(timein AS TIME)
                                                  AND CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME) <= CAST(timeout AS TIME)
                                                  ) -- holiday end time falls betweem time in and time out 
                                        THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(ISNULL(Holiday.EndTimeInterval, timeout) AS TIME))
                                             ELSE 0
                                        END
                                       ,0
                                    ) AS HolidayMinutes
                             ,ISNULL(
                                        CASE WHEN Holiday.AllDay = 1 THEN DATEDIFF(MINUTE, lunchout, lunchin)
                                             WHEN (
                                                  CAST(Holiday.BeginTimeInteral AS TIME) <= CAST(lunchin AS TIME)
                                                  AND CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME) >= CAST(lunchout AS TIME)
                                                  ) -- holiday falls entirely inside holiday schedule
                                        THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME))
                                             WHEN (
                                                  CAST(Holiday.BeginTimeInteral AS TIME) >= CAST(lunchout AS TIME)
                                                  AND CAST(Holiday.BeginTimeInteral AS TIME) <= CAST(lunchin AS TIME)
                                                  ) -- holiday start time falls between lunch
                                        THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(lunchin AS TIME))
                                             WHEN (
                                                  CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME) >= CAST(lunchout AS TIME)
                                                  AND CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME) <= CAST(lunchin AS TIME)
                                                  ) -- holiday end time falls between lunch 
                                        THEN     DATEDIFF(MINUTE, CAST(Holiday.BeginTimeInteral AS TIME), CAST(ISNULL(Holiday.EndTimeInterval, lunchin) AS TIME))
                                             ELSE 0
                                        END
                                       ,0
                                    ) AS LunchHolidayOverlapMinutes -- lunch and holiday may overlap, calculate this time and add it back to student scheduled hours bucket
                             ,Holiday.AllDay
                             ,Holiday.BeginTimeInteral
                             ,Holiday.EndTimeInterval
                             ,Holiday.HolidayDescrip
                  FROM        dbo.arStuEnrollments
                  JOIN        dbo.arStudentSchedules ON arStudentSchedules.StuEnrollId = arStuEnrollments.StuEnrollId
                  JOIN        dbo.arProgSchedules ON arProgSchedules.ScheduleId = arStudentSchedules.ScheduleId
                  LEFT JOIN   dbo.arProgScheduleDetails ON arProgScheduleDetails.ScheduleId = arStudentSchedules.ScheduleId
                  OUTER APPLY (
                              SELECT    TOP 1 HolidayDescrip
                                       ,HolidayStartDate
                                       ,HolidayEndDate
                                       ,AllDay
                                       ,a.TimeIntervalDescrip AS BeginTimeInteral
                                       ,b.TimeIntervalDescrip AS EndTimeInterval
                              FROM      dbo.syHolidays
                              LEFT JOIN dbo.cmTimeInterval a ON a.TimeIntervalId = syHolidays.StartTimeId
                              LEFT JOIN dbo.cmTimeInterval b ON b.TimeIntervalId = syHolidays.EndTimeId
                              WHERE     (
                                        CAST(@Date AS DATE) >= CAST(HolidayStartDate AS DATE)
                                        AND CAST(@Date AS DATE) <= CAST(HolidayEndDate AS DATE)
                                        )
                                        AND syHolidays.CampGrpId IN (
                                                                    SELECT CampGrpId
                                                                    FROM   dbo.syCmpGrpCmps
                                                                    WHERE  dbo.syCmpGrpCmps.CampusId = (
                                                                                                       SELECT CampusId
                                                                                                       FROM   dbo.arStuEnrollments
                                                                                                       WHERE  StuEnrollId = (
                                                                                                                            SELECT TOP 1 Val
                                                                                                                            FROM   MultipleValuesForReportParameters(
                                                                                                                                                                        @StuEnrollIdList
                                                                                                                                                                       ,'',''
                                                                                                                                                                       ,1
                                                                                                                                                                    )
                                                                                                                            )
                                                                                                       )
                                                                    )
                                        AND syHolidays.StatusId = (
                                                                  SELECT TOP 1 StatusId
                                                                  FROM   dbo.syStatuses
                                                                  WHERE  StatusCode = ''A''
                                                                  )
                              ) Holiday
                  WHERE       (
                              (
                              DATEPART(dw, @Date) - 1 = 0
                              AND dw = 7
                              )
                              OR dw = ( DATEPART(dw, @Date) - 1 )
                              )
                              AND arStuEnrollments.StuEnrollId IN (
                                                                  SELECT Val
                                                                  FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                  )
                  ) ScheduledData
           );


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudAddresses]'
GO
IF OBJECT_ID(N'[dbo].[arStudAddresses]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudAddresses]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[GetSchoolNameAndCampusAddress]'
GO
IF OBJECT_ID(N'[dbo].[GetSchoolNameAndCampusAddress]', 'P') IS NULL
EXEC sp_executesql N'
-- =========================================================================================================

-- Author:		Jose Alfredo Garcia guirado
-- Create date: 7/14/2012
-- Description:	Get the school name and the name 
-- and address of the  CampusId entered as parameter.
-- =============================================
CREATE PROCEDURE [dbo].[GetSchoolNameAndCampusAddress]
    -- Add the parameters for the stored procedure here
    @CampusId VARCHAR(38) = NULL
   ,@StuEnrollIdList VARCHAR(MAX) = NULL
   ,@StudentGrpId VARCHAR(MAX) = NULL
AS
    BEGIN

        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;
        -- First Get the School Name from parameters
        DECLARE @SchoolName TABLE
            (
                Name VARCHAR(100)
            );

        DECLARE @CampusInfo TABLE
            (
                SchoolName VARCHAR(50)
               ,StuEnrollId UNIQUEIDENTIFIER
               ,CampusId UNIQUEIDENTIFIER
               ,CampusName VARCHAR(50)
               ,CampusCode VARCHAR(10)
               ,Address1 VARCHAR(50)
               ,Address2 VARCHAR(50)
               ,City VARCHAR(80)
               ,State VARCHAR(80)
               ,StateCode VARCHAR(12)
               ,ZIP VARCHAR(5)
               ,Country VARCHAR(50)
               ,Phone1 VARCHAR(30)
               ,Phone2 VARCHAR(30)
               ,Phone3 VARCHAR(30)
               ,Fax VARCHAR(30)
               ,Website VARCHAR(50)
            );

        INSERT INTO @SchoolName (
                                Name
                                )
        VALUES ( dbo.GetAppSettingValueByKeyName(''CorporateName'', NULL));

        -- If @CampusId is Null and @StuEnrollIdList is not Null we cang get CampusId from the first StuEnrollId in the list
        IF ( @CampusId IS NULL )
            BEGIN
                IF NOT ( @StuEnrollIdList IS NULL )
                    BEGIN
                        SELECT @CampusId = ASE.CampusId
                        FROM   arStuEnrollments AS ASE
                        WHERE  ASE.StuEnrollId = (
                                                 SELECT TOP 1 Val
                                                 FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                 );
                    END;
                ELSE IF NOT ( @StudentGrpId IS NULL )
                         BEGIN
                             SELECT     @CampusId = ASE.CampusId
                             FROM       arStuEnrollments AS ASE
                             INNER JOIN adLeadByLeadGroups LLG ON LLG.StuEnrollId = ASE.StuEnrollId
                             WHERE      LLG.LeadGrpId = (
                                                        SELECT TOP 1 Val
                                                        FROM   MultipleValuesForReportParameters(@StudentGrpId, '','', 1)
                                                        );
                         END;
            END;
        -- Second Get the campusId Name and Address and other grass
        DECLARE @CampusGuid UNIQUEIDENTIFIER = CAST(@CampusId AS UNIQUEIDENTIFIER);
        INSERT INTO @CampusInfo
                    SELECT          TOP 1    c.SchoolName AS SchoolName
                                            ,se.StuEnrollId
                                            ,c.CampusId
                                            ,c.CampDescrip AS CampusName
                                            ,c.CampCode AS CampusCode
                                            ,RTRIM(LTRIM(c.Address1))
                                            ,RTRIM(LTRIM(c.Address2))
                                            ,RTRIM(LTRIM(c.City))
                                            ,sta.StateDescrip AS State
                                            ,sta.StateCode AS StateCode
                                            ,CASE WHEN LEN(c.Zip) > 5
                                                       AND co.CountryDescrip = ''USA'' THEN SUBSTRING(c.Zip, 1, 5)
                                                  ELSE c.Zip
                                             END AS ZIP
                                            ,co.CountryDescrip AS Country
                                            ,c.Phone1
                                            ,c.Phone2
                                            ,c.Phone3
                                            ,c.Fax
                                            ,c.Website
                    FROM            dbo.syCampuses c
                    INNER JOIN      dbo.adCountries co ON co.CountryId = c.CountryId
                    INNER JOIN      dbo.syStates sta ON sta.StateId = c.StateId
                    INNER JOIN      dbo.arStuEnrollments se ON se.CampusId = c.CampusId
                    LEFT OUTER JOIN adLeadByLeadGroups LLG ON LLG.StuEnrollId = se.StuEnrollId
                                                              AND (
                                                                  @StuEnrollIdList IS NULL
                                                                  OR se.StuEnrollId IN (
                                                                                       SELECT Val
                                                                                       FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                                       )
                                                                  )
                                                              AND (
                                                                  @StudentGrpId IS NULL
                                                                  OR LLG.LeadGrpId IN (
                                                                                      SELECT Val
                                                                                      FROM   MultipleValuesForReportParameters(@StudentGrpId, '','', 1)
                                                                                      )
                                                                  )
                    WHERE           c.CampusId = @CampusGuid;

        SELECT SchoolName
              ,StuEnrollId
              ,CampusId
              ,CampusName
              ,CampusCode
              ,Address1
              ,Address2
              ,City
              ,State
              ,StateCode
              ,ZIP
              ,Country
              ,Phone1
              ,Phone2
              ,Phone3
              ,Fax
              ,Website
        FROM   @CampusInfo;

    END;

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[RS_ProgramDetails_Courses]'
GO
IF OBJECT_ID(N'[dbo].[RS_ProgramDetails_Courses]', 'P') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[RS_ProgramDetails_Courses]
	-- Add the parameters for the stored procedure here
    @PrgVerId VARCHAR(36) = ''5FEF3CF7-5B12-4942-A1F1-8B87E01997C9'' -- Electrolysis (historical)
   ,@CampusId VARCHAR(36) = ''DC42A60A-5EB9-49B6-ADF6-535966F2E34A'' -- Hieleach
   ,@StatusId VARCHAR(36) = ''F23DE1E2-D90A-4720-B4C7-0F6FB09C9965'' -- Active
   ,@CoursesId VARCHAR(MAX) = ''5BA9B098-D3D8-451D-A638-64E8C72EE425,BBD4989F-07E3-49CD-85DA-EC73C008EC32,7CCA4B9A-D5F2-46AA-A125-1C5B07FE6B4B,8D02B5EB-F46D-43B9-802E-190BD56AB0E0,00A65FB1-9E3B-4CFE-AF49-D5A8DB968EE3,34AF9A35-7927-410F-A51F-C959F96EBF9A,DF
4918B9-1495-43ED-B698-42C4DFE5F92D,D38E6886-65BE-46B6-9B28-1A08DC279EA2,AC5FCE97-92BC-4D6C-B358-FBC37A9F845B,8F136D0C-C726-44C1-BF18-A00D500F79EC''
AS
    BEGIN
        SET NOCOUNT ON;
	-- Security delete any preexist tempo table.
	--IF OBJECT_ID(N''tempdb..@MYTempoTable'',''U'') IS NOT NULL DROP TABLE @MYTempoTable;
        IF OBJECT_ID(N''tempdb..#EfectivesDates'',''U'') IS NOT NULL
            DROP TABLE #EfectivesDates;
	--IF OBJECT_ID(N''tempdb..@ResultTable'', ''U'') IS NOT NULL DROP TABLE @ResultTable;

	-- Process Parameters to Guid
        DECLARE @PrgVerGuid UNIQUEIDENTIFIER = @PrgVerId; 
        DECLARE @CampusGuid UNIQUEIDENTIFIER = @CampusId;

	-- Process Courses Id Parameter
        IF @CoursesId = ''''
            SET @CoursesId = NULL;

-- Try
        BEGIN TRY
-- 1 Select Courses for program version and CampusId
            DECLARE @MYTempoTable TABLE
                (
                 IsRequired BIT
                ,Code VARCHAR(15)
                ,CourseDescript VARCHAR(100)
                ,Credits DECIMAL
                ,Hours DECIMAL(10,2)
                ,Weight DECIMAL(10,2)
                ,GradeDescript VARCHAR(50)
                ,EffectiveDate DATETIME
                ,Number DECIMAL
                ,Resource VARCHAR(200)
                ,ResourceID SMALLINT
                ,FinAidCredits DECIMAL
                );

            DECLARE @ResultTable TABLE
                (
                 IsRequired BIT
                ,Code VARCHAR(15)
                ,CourseDescript VARCHAR(100)
                ,Credits DECIMAL
                ,Hours DECIMAL(10,2)
                ,Weight DECIMAL(10,2)
                ,GradeDescript VARCHAR(50)
                ,EffectiveDate DATETIME
                ,Number DECIMAL
                ,Resource VARCHAR(200)
                ,ResourceID SMALLINT
                ,FinAidCredits DECIMAL
                );

            INSERT  INTO @MYTempoTable
                    EXEC dbo.RS_ProgramDetails_CoursesAllGrades @CampusId,@PrgVerId,@StatusId,@CoursesId;
  
-- 2 Get the list of Different EffectiveData in the query
            SELECT DISTINCT
                    EffectiveDate
            INTO    #EfectivesDates
            FROM    @MYTempoTable
            ORDER BY EffectiveDate DESC;

-- 3   Get The number of effective data in the table #FectivesDates
            DECLARE @DateCount INTEGER = (
                                           SELECT   COUNT(*)
                                           FROM     #EfectivesDates
                                         );
		
		-- The following line is only for debug purpose
		--SELECT * FROM @MYTempoTable
		
-- 4A  IF only exists 1 or 0 EffectiveDates return the table as is.
            IF @DateCount < 2 
-- 5A     Return Values
                SELECT  *
                FROM    @MYTempoTable
                ORDER BY CourseDescript
                       ,GradeDescript;
            ELSE
                BEGIN
-- 4B		If exists more....
                    INSERT  INTO @ResultTable
                            SELECT  *
                            FROM    @MYTempoTable
                            WHERE   EffectiveDate = (
                                                      SELECT TOP 1
                                                                EffectiveDate
                                                      FROM      #EfectivesDates
                                                    );
                    WITH    Q AS (
                                   SELECT TOP 1
                                            *
                                   FROM     #EfectivesDates
                                   ORDER BY EffectiveDate DESC
                                 )
                        DELETE  FROM Q;
                    SET @DateCount = (
                                       SELECT   COUNT(*)
                                       FROM     #EfectivesDates
                                     );
                    WHILE @DateCount > 0
                        BEGIN
                            INSERT  INTO @ResultTable
                                    SELECT  *
                                    FROM    @MYTempoTable
                                    WHERE   EffectiveDate = (
                                                              SELECT TOP 1
                                                                        EffectiveDate
                                                              FROM      #EfectivesDates
                                                            )
                                            AND CourseDescript NOT IN ( SELECT  CourseDescript
                                                                        FROM    @ResultTable );
                            WITH    Q AS (
                                           SELECT TOP 1
                                                    *
                                           FROM     #EfectivesDates
                                           ORDER BY EffectiveDate DESC
                                         )
                                DELETE  FROM Q;
                            SET @DateCount = (
                                               SELECT   COUNT(*)
                                               FROM     #EfectivesDates
                                             );
                        END;

                    INSERT  INTO @ResultTable
                            SELECT  *
                            FROM    @MYTempoTable
                            WHERE   EffectiveDate IS NULL;
-- 5B		Return the values
                    SELECT  *
                    FROM    @ResultTable
                    ORDER BY CourseDescript
                           ,Resource
                           ,GradeDescript;
                END;
        END TRY
        BEGIN CATCH
		--IF OBJECT_ID(N''tempdb..@MYTempoTable'',''U'') IS NOT NULL DROP TABLE @MYTempoTable;
            IF OBJECT_ID(N''tempdb..#EfectivesDates'',''U'') IS NOT NULL
                DROP TABLE #EfectivesDates;
		--IF OBJECT_ID(N''tempdb..@ResultTable'', ''U'') IS NOT NULL DROP TABLE @ResultTable;
		-- rethrow the error
            DECLARE @ErrorMessage NVARCHAR(4000);
            DECLARE @ErrorSeverity INT;
            DECLARE @ErrorState INT;
            SELECT  @ErrorMessage = ERROR_MESSAGE()
                   ,@ErrorSeverity = ERROR_SEVERITY()
                   ,@ErrorState = ERROR_STATE();

            RAISERROR (@ErrorMessage, -- Message text.
               @ErrorSeverity, -- Severity.
               @ErrorState -- State.
               );

        END CATCH;
-- 6  Destroy Temporaly Tables.
		--IF OBJECT_ID(N''tempdb..@MYTempoTable'',''U'') IS NOT NULL DROP TABLE @MYTempoTable;
        IF OBJECT_ID(N''tempdb..#EfectivesDates'',''U'') IS NOT NULL
            DROP TABLE #EfectivesDates;
		--IF OBJECT_ID(N''tempdb..@ResultTable'', ''U'') IS NOT NULL DROP TABLE @ResultTable;
    END;




'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudentPhone]'
GO
IF OBJECT_ID(N'[dbo].[arStudentPhone]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudentPhone]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_EnrollLead]'
GO
IF OBJECT_ID(N'[dbo].[USP_EnrollLead]', 'P') IS NULL
EXEC sp_executesql N'-- =========================================================================================================          
-- USP_EnrollLead          
-- =========================================================================================================          
CREATE PROCEDURE [dbo].[USP_EnrollLead]
    (
        @leadId AS UNIQUEIDENTIFIER
       ,@campus UNIQUEIDENTIFIER
       ,@gender AS UNIQUEIDENTIFIER
       ,@familyIncome UNIQUEIDENTIFIER = NULL
       ,@housingType UNIQUEIDENTIFIER = NULL
       ,@educationLevel UNIQUEIDENTIFIER = NULL
       ,@adminCriteria UNIQUEIDENTIFIER = NULL
       ,@degSeekCert UNIQUEIDENTIFIER = NULL
       ,@attendType UNIQUEIDENTIFIER = NULL
       ,@race UNIQUEIDENTIFIER = NULL
       ,@citizenship UNIQUEIDENTIFIER = NULL
       ,@prgVersion UNIQUEIDENTIFIER = NULL
       ,@programId UNIQUEIDENTIFIER = NULL
       ,@areaId UNIQUEIDENTIFIER = NULL
       ,@prgVerType INT = 0
       ,@startDt DATETIME
       --,@studentId UNIQUEIDENTIFIER          
       ,@expectedStartDt DATETIME = NULL
       ,@contractGrdDt DATETIME
       ,@tuitionCategory UNIQUEIDENTIFIER
       ,@enrollDt DATETIME
       ,@transferhr DECIMAL(18, 2) = NULL
       ,@dob AS DATETIME = NULL
       ,@ssn AS VARCHAR(50) = NULL
       ,@depencytype UNIQUEIDENTIFIER = NULL
       ,@maritalstatus UNIQUEIDENTIFIER = NULL
       ,@nationality UNIQUEIDENTIFIER = NULL
       ,@geographicType UNIQUEIDENTIFIER = NULL
       ,@state UNIQUEIDENTIFIER = NULL
       ,@disabled BIT = NULL
       ,@disableAutoCharge BIT = NULL
       ,@thirdPartyContract BIT = 0
       ,@firsttimeSchool BIT = NULL
       ,@firsttimePostSec BIT = NULL
       ,@schedule UNIQUEIDENTIFIER = NULL
       ,@shift UNIQUEIDENTIFIER = NULL
       ,@midptDate DATETIME = NULL
       ,@transferDt DATETIME = NULL
       ,@entranceInterviewDt DATETIME = NULL
       ,@chargingMethod UNIQUEIDENTIFIER = NULL
       ,@fAAvisor UNIQUEIDENTIFIER = NULL
       ,@acedemicAdvisor UNIQUEIDENTIFIER = NULL
       ,@badgeNum VARCHAR(50) = NULL
       ,@studentNumber VARCHAR(50) = ''''
       ,@modUser UNIQUEIDENTIFIER = NULL
       ,@enrollId VARCHAR(50) = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @transDate DATETIME;
        DECLARE @MyError AS INTEGER;
        DECLARE @Message AS NVARCHAR(MAX);
        DECLARE @modUserN VARCHAR(50)
               ,@studentStatusId UNIQUEIDENTIFIER
               ,@studentEnrolStatus UNIQUEIDENTIFIER
               ,@studentId UNIQUEIDENTIFIER
               ,@stuEnrollId UNIQUEIDENTIFIER
               ,@admissionRepId UNIQUEIDENTIFIER;
        SET @transDate = GETDATE();
        SET @studentStatusId = (
                               SELECT StatusId
                               FROM   syStatuses
                               WHERE  StatusCode = ''A''
                               );
        --,@originalStatus UNIQUEIDENTIFIER;           
        SET @studentId = NEWID();
        --set @studentEnrolStatus to system future start          
        SET @studentEnrolStatus = (
                                  SELECT     DISTINCT TOP 1 A.StatusCodeId
                                  FROM       syStatusCodes A
                                  INNER JOIN sySysStatus B ON B.SysStatusId = A.SysStatusId
                                  INNER JOIN syStatuses C ON C.StatusId = A.StatusId
                                  WHERE      C.Status = ''Active''
                                             AND B.SysStatusId = 7
                                             AND A.CampGrpId IN (
                                                                SELECT CampGrpId
                                                                FROM   dbo.syCmpGrpCmps
                                                                WHERE  CampusId = @campus
                                                                )
                                  );
        DECLARE @acCalendarID INT;
        SELECT @modUserN = UserName
        FROM   syUsers
        WHERE  UserId = @modUser;
        SET @MyError = 0; -- all good          
        SET @Message = N'''';
        BEGIN TRANSACTION;
        BEGIN TRY
            --step 1: temperary insert into arstudent          
            DECLARE @lName VARCHAR(50)
                   ,@fName VARCHAR(50)
                   ,@mName VARCHAR(50);

            SET @lName = (
                         SELECT LastName
                         FROM   adLeads
                         WHERE  LeadId = @leadId
                         );
            SET @fName = (
                         SELECT FirstName
                         FROM   adLeads
                         WHERE  LeadId = @leadId
                         );
            SET @mName = (
                         SELECT MiddleName
                         FROM   adLeads
                         WHERE  LeadId = @leadId
                         );

            IF ( @MyError = 0 )
                BEGIN
                    --update the adleads table          
                    DECLARE @enrolLeadStatus UNIQUEIDENTIFIER;
                    SET @enrolLeadStatus = (
                                           SELECT   TOP 1 StatusCodeId
                                           FROM     syStatusCodes
                                           WHERE    SysStatusId = 6
                                                    AND StatusId = ''F23DE1E2-D90A-4720-B4C7-0F6FB09C9965''
                                                    AND CampGrpId IN (
                                                                     SELECT CampGrpId
                                                                     FROM   dbo.syCmpGrpCmps
                                                                     WHERE  CampusId = @campus
                                                                     )
                                           ORDER BY ModDate
                                           );
                    DECLARE @stuNum VARCHAR(50);
                    SET @stuNum = (
                                  SELECT StudentNumber
                                  FROM   adLeads
                                  WHERE  LeadId = @leadId
                                  );
                    IF ( @stuNum = '''' )
                        BEGIN
                            -- generate student number          
                            DECLARE @formatType INT
                                   ,@yrNo INT
                                   ,@monthNo INT
                                   ,@dateNo INT
                                   ,@lNameNo INT
                                   ,@fNameNo INT;
                            DECLARE @lNameStu VARCHAR(50)
                                   ,@fNameStu VARCHAR(50)
                                   ,@yr VARCHAR(10)
                                   ,@mon VARCHAR(10)
                                   ,@dt VARCHAR(10)
                                   ,@seqStartNo INT;
                            SELECT @formatType = CAST(FormatType AS INT)
                                  ,@yrNo = ISNULL(YearNumber, 0)
                                  ,@monthNo = ISNULL(MonthNumber, 0)
                                  ,@dateNo = ISNULL(DateNumber, 0)
                                  ,@lNameNo = ISNULL(LNameNumber, 0)
                                  ,@fNameNo = ISNULL(FNameNumber, 0)
                                  ,@seqStartNo = SeqStartingNumber
                            FROM   syStudentFormat;
                            IF ( @formatType = 1 )
                                BEGIN
                                    SET @stuNum = '''';
                                END;
                            ELSE IF ( @formatType = 3 )
                                     BEGIN
                                         SET @lNameStu = SUBSTRING(@lName, 1, ISNULL(@lNameNo, 0));
                                         SET @fNameStu = SUBSTRING(@fName, 1, ISNULL(@fNameNo, 0));
                                         SET @yr = '''';
                                         SET @yr = CASE @yrNo
                                                        WHEN 0 THEN ''''
                                                        WHEN 4 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 1, 4)
                                                        WHEN 3 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 2, 3)
                                                        WHEN 2 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 3, 2)
                                                        WHEN 1 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 4, 1)
                                                   END;
                                         SET @mon = SUBSTRING(CAST(MONTH(GETDATE()) AS VARCHAR(10)), 1, @monthNo);
                                         SET @dt = SUBSTRING(CAST(DAY(GETDATE()) AS VARCHAR(10)), 1, @dateNo);
                                         IF (
                                            LEN(@mon) = 1
                                            AND @monthNo = 2
                                            )
                                             BEGIN
                                                 SET @mon = ''0'' + @mon;
                                             END;
                                         IF (
                                            LEN(@dt) = 1
                                            AND @dateNo = 2
                                            )
                                             BEGIN
                                                 SET @dt = ''0'' + @dt;
                                             END;
                                         SET NOCOUNT ON;
                                         DECLARE @stuId INT;
                                         SET @stuId = (
                                                      SELECT MAX(Student_SeqId) AS Student_SeqID
                                                      FROM   syGenerateStudentFormatID
                                                      ) + 1;
                                         INSERT INTO syGenerateStudentFormatID (
                                                                               Student_SeqId
                                                                              ,ModDate
                                                                               )
                                         VALUES ( @stuId, GETDATE());

                                         IF ( @yr = '''' )
                                             BEGIN
                                                 SET @stuNum = @mon + @dt + @lNameStu + @fNameStu + CAST(@stuId AS VARCHAR(5));
                                             END;
                                         ELSE
                                             BEGIN
                                                 SET @stuNum = @yr + @mon + @dt + @lNameStu + @fNameStu + CAST(@stuId AS VARCHAR(5));
                                             END;
                                     --SELECT @stuNum          
                                     END;
                            ELSE IF ( @formatType = 4 )
                                     BEGIN
                                         SET @stuNum = @badgeNum;
                                     END;
                            ELSE
                                     BEGIN
                                         SET NOCOUNT ON;

                                         IF EXISTS (
                                                   SELECT *
                                                   FROM   syGenerateStudentSeq
                                                   )
                                             BEGIN
                                                 SET @stuId = (
                                                              SELECT MAX(Student_SeqId) AS Student_SeqID
                                                              FROM   syGenerateStudentSeq
                                                              ) + 1;
                                             END;
                                         ELSE
                                             BEGIN
                                                 SET @stuId = @seqStartNo;
                                             END;
                                         INSERT INTO syGenerateStudentSeq (
                                                                          Student_SeqId
                                                                         ,ModDate
                                                                          )
                                         VALUES ( @stuId, GETDATE());
                                         SET @stuNum = CAST(@stuId AS VARCHAR(10));
                                     END;

                        END;
                    UPDATE adLeads
                    SET    Gender = @gender
                          ,BirthDate = @dob                             --ISNULL(@dob,BirthDate)          
                          ,SSN = @ssn                                   --ISNULL(@ssn,SSN)          
                          ,DependencyTypeId = @depencytype              --ISNULL(@depencytype,DependencyTypeId)          
                          ,MaritalStatus = @maritalstatus               --ISNULL(@maritalstatus,MaritalStatus)          
                          ,FamilyIncome = @familyIncome
                          ,HousingId = @housingType
                          ,admincriteriaid = @adminCriteria
                          ,DegCertSeekingId = @degSeekCert
                          ,AttendTypeId = @attendType
                          ,Race = @race
                          ,Nationality = @nationality                   --ISNULL(@nationality,Nationality)          
                          ,Citizen = @citizenship
                          ,GeographicTypeId = @geographicType           --ISNULL(@geographicType,GeographicTypeId)          
                          ,IsDisabled = @disabled                       --ISNULL(@disabled,IsDisabled)          
                          ,IsFirstTimeInSchool = @firsttimeSchool       --ISNULL(@firsttimeSchool,IsFirstTimeInSchool)          
                          ,CampusId = @campus
                          ,IsFirstTimePostSecSchool = @firsttimePostSec --ISNULL(@firsttimePostSec,IsFirstTimePostSecSchool)          
                          ,PrgVerId = @prgVersion
                          ,ProgramID = @programId
                          ,AreaID = @areaId
                          ,ProgramScheduleId = @schedule                --ISNULL(@schedule,ProgramScheduleId)          
                          ,ShiftID = @shift                             --ISNULL(@shift,ShiftID)          
                          ,ExpectedStart = @expectedStartDt
                          ,EntranceInterviewDate = @entranceInterviewDt --ISNULL(@entranceInterviewDt,EntranceInterviewDate)          
                          ,StudentNumber = ISNULL(@stuNum, '''')          --ISNULL(@studentNumber,StudentNumber)          
                          ,StudentStatusId = ISNULL(@studentStatusId, StudentStatusId)
                          ,StudentId = ISNULL(@studentId, StudentId)
                          ,ModUser = ISNULL(@modUserN, ModUser)
                          ,ModDate = @transDate
                          ,LeadStatus = @enrolLeadStatus                --update to enrolled          
                          ,PreviousEducation = @educationLevel
                          ,EnrollStateId = @state
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N''Failed to update in adleads'';
                        END;
                    SET @admissionRepId = (
                                          SELECT AdmissionsRep
                                          FROM   adLeads
                                          WHERE  LeadId = @leadId
                                          );
                END;

            IF ( @MyError = 0 )
                BEGIN
                    --insert into arStuEnrollments          
                    SET @stuEnrollId = NEWID();
                    INSERT INTO arStuEnrollments (
                                                 StuEnrollId
                                                ,StudentId
                                                ,EnrollDate
                                                ,PrgVerId
                                                ,StartDate
                                                ,ExpStartDate
                                                ,MidPtDate
                                                ,TransferDate
                                                ,ShiftId
                                                ,BillingMethodId
                                                ,CampusId
                                                ,StatusCodeId
                                                ,ModDate
                                                ,ModUser
                                                ,EnrollmentId
                                                ,AcademicAdvisor
                                                ,LeadId
                                                ,TuitionCategoryId
                                                ,EdLvlId
                                                ,FAAdvisorId
                                                ,attendtypeid
                                                ,degcertseekingid
                                                ,BadgeNumber
                                                ,ContractedGradDate
                                                ,TransferHours
                                                ,PrgVersionTypeId
                                                ,IsDisabled
                                                ,DisableAutoCharge
                                                ,ThirdPartyContract
                                                ,IsFirstTimeInSchool
                                                ,IsFirstTimePostSecSchool
                                                ,EntranceInterviewDate
                                                ,AdmissionsRep
                                                ,ExpGradDate
                                                ,CohortStartDate
                                                 )
                    VALUES ( @stuEnrollId, @studentId             -- StudentId - uniqueidentifier          
                            ,@enrollDt                            -- EnrollDate - datetime          
                            ,@prgVersion                          -- PrgVerId - uniqueidentifier          
                            ,CONVERT(DATE, @startDt, 101)         -- StartDate - datetime          
                            ,CONVERT(DATE, @expectedStartDt, 101) -- ExpStartDate - datetime          
                            ,@midptDate                           -- MidPtDate - datetime                   
                            ,@transferDt                          -- TransferDate - datetime          
                            ,@shift                               -- ShiftId - uniqueidentifier          
                            ,@chargingMethod                      -- BillingMethodId - uniqueidentifier          
                            ,@campus                              -- CampusId - uniqueidentifier          
                            ,@studentEnrolStatus                  -- StatusCodeId - uniqueidentifier                   
                            ,@transDate                           -- ModDate - datetime          
                            ,@modUserN                            -- ModUser - varchar(50)          
                            ,@enrollId                            -- EnrollmentId - varchar(50)                   
                            ,@acedemicAdvisor                     -- AcademicAdvisor - uniqueidentifier          
                            ,@leadId                              -- LeadId - uniqueidentifier          
                            ,@tuitionCategory                     -- TuitionCategoryId - uniqueidentifier          
                            ,@educationLevel                      -- EdLvlId - uniqueidentifier          
                            ,@fAAvisor                            -- FAAdvisorId - uniqueidentifier          
                            ,@attendType                          -- attendtypeid - uniqueidentifier          
                            ,@degSeekCert                         -- degcertseekingid - uniqueidentifier          
                            ,@badgeNum                            -- BadgeNumber - varchar(50)          
                            ,@contractGrdDt                       -- ContractedGradDate - datetime          
                            ,@transferhr                          -- TransferHours - decimal          
                            ,@prgVerType                          -- PrgVersionTypeId - int          
                            ,@disabled                            -- IsDisabled - bit    
                            ,@disableAutoCharge                   -- DisableAutoCharge - bit    
                            ,@thirdPartyContract                  -- ThirdPartyContract - bit      
                            ,@firsttimeSchool                     -- IsFirstTimeInSchool - bit          
                            ,@firsttimePostSec                    -- IsFirstTimePostSecSchool - bit          
                            ,@entranceInterviewDt                 -- EntranceInterviewDate - datetime          
                            ,@admissionRepId                      -- AdmissionsRep uniqueidentifier          
                            ,@contractGrdDt                       -- ExpGradDate DateTime          
                            ,CONVERT(DATE, @expectedStartDt, 101) -- cohort start date                    
                        );


                    IF ((
                        SELECT TOP 1 RTRIM(LTRIM(FormatType))
                        FROM   dbo.syStudentFormat
                        ) = ''4''
                       )
                        BEGIN
                            DECLARE @badgeId VARCHAR(50) = (
                                                           SELECT TOP 1 BadgeNumber
                                                           FROM   dbo.arStuEnrollments
                                                           WHERE  StuEnrollId = @stuEnrollId
                                                           );

                            UPDATE adLeads
                            SET    StudentNumber = @badgeId
                            WHERE  LeadId = @leadId;
                        END;


                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N''Failed to insert in arStuEnrollments'';
                        END;
                END;
            -- insert into arStudentSchedules if schedule is not null
            IF ( @MyError = 0 )
                BEGIN
                    IF @schedule IS NOT NULL
                        BEGIN
                            INSERT INTO arStudentSchedules (
                                                           StuScheduleId
                                                          ,StuEnrollId
                                                          ,ScheduleId
                                                          ,StartDate
                                                          ,EndDate
                                                          ,Active
                                                          ,ModUser
                                                          ,ModDate
                                                          ,Source
                                                           )
                            VALUES ( NEWID()      -- StuScheduleId - uniqueidentifier
                                    ,@stuEnrollId -- StuEnrollId - uniqueidentifier
                                    ,@schedule    -- ScheduleId - uniqueidentifier
                                    ,NULL         -- StartDate - smalldatetime
                                    ,NULL         -- EndDate - smalldatetime
                                    ,1            -- Active - bit
                                    ,@modUserN    -- ModUser - varchar(50)
                                    ,GETDATE()    -- ModDate - smalldatetime
                                    ,''attendance'' -- Source - varchar(50)
                                );
                        END;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N''Failed to insert in arStuEnrollments'';
                        END;
                END;
            IF ( @MyError = 0 )
                BEGIN
                    IF @stuEnrollId IS NOT NULL
                        BEGIN
                            IF ( @MyError = 0 )
                                BEGIN
                                    --insert into syStudentStatusChanges          
                                    DECLARE @OriginalleadStatus UNIQUEIDENTIFIER;
                                    SET @OriginalleadStatus = (
                                                              SELECT LeadStatus
                                                              FROM   adLeads
                                                              WHERE  LeadId = @leadId
                                                              );
                                    INSERT INTO syStudentStatusChanges (
                                                                       StudentStatusChangeId
                                                                      ,StuEnrollId
                                                                      ,OrigStatusId
                                                                      ,NewStatusId
                                                                      ,CampusId
                                                                      ,ModDate
                                                                      ,ModUser
                                                                      ,IsReversal
                                                                      ,DropReasonId
                                                                      ,DateOfChange
                                                                      ,Lda
                                                                       )
                                    VALUES ( NEWID(), @stuEnrollId, @OriginalleadStatus, @studentEnrolStatus, @campus, @transDate, @modUserN, 0, NULL
                                            ,@enrollDt, NULL );
                                    SET @MyError = @@ERROR;
                                    IF ( @MyError <> 0 )
                                        BEGIN
                                            SET @MyError = 1;
                                            SET @Message = @Message + N''Failed to insert in syStudentStatusChanges'';
                                        END;
                                END;
                            --insert into plStudentDocs          
                            INSERT INTO plStudentDocs (
                                                      StudentId
                                                     ,DocumentId
                                                     ,DocStatusId
                                                     ,ReceiveDate
                                                     ,ScannedId
                                                     ,ModDate
                                                     ,ModUser
                                                     ,ModuleID
                                                      )
                                        SELECT @studentId
                                              ,DocumentId
                                              ,DocStatusId
                                              ,ReceiveDate
                                              ,1
                                              ,@transDate
                                              ,@modUserN
                                              ,(
                                               SELECT DISTINCT ModuleId
                                               FROM   adReqs
                                               WHERE  adReqId = adLeadDocsReceived.DocumentId
                                               )
                                        FROM   adLeadDocsReceived
                                        WHERE  LeadId = @leadId;
                        END;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N''Failed to insert in plStudentDocs'';
                        END;
                END;
            IF ( @MyError = 0 )
                BEGIN
                    --update syDocumentHistory          
                    UPDATE syDocumentHistory
                    SET    StudentId = @studentId
                          ,ModUser = @modUserN
                          ,ModDate = @transDate
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N''Failed to update in syDocumentHistory'';
                        END;
                END;
            --update adLeadByLeadGroups          
            IF ( @MyError = 0 )
                BEGIN
                    UPDATE adLeadByLeadGroups
                    SET    StuEnrollId = @stuEnrollId
                          ,ModUser = @modUserN
                          ,ModDate = @transDate
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N''Failed to update in adLeadByLeadGroups'';
                        END;

                END;
            --insert in adLeadEntranceTest          
            IF ( @MyError = 0 )
                BEGIN

                    UPDATE adLeadEntranceTest
                    SET    StudentId = @studentId
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N''Failed to update in adLeadEntranceTest'';

                        END;
                END;
            --copy all the lead requirements adEntrTestOverRide          
            IF ( @MyError = 0 )
                BEGIN

                    UPDATE adEntrTestOverRide
                    SET    StudentId = @studentId
                    WHERE  LeadId = @leadId;

                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N''Failed to update in adEntrTestOverRide'';
                        END;
                END;
            --exec USP_AD_CopyLeadTransactions          
            IF ( @MyError = 0 )
                BEGIN
                    DECLARE @rc INT;
                    EXECUTE @rc = dbo.USP_AD_CopyLeadTransactions @leadId
                                                                 ,@stuEnrollId
                                                                 ,@campus;
                    SET @MyError = @@ERROR;
                    IF (
                       @MyError <> 0
                       AND @rc = 0
                       )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N''Failed to execute USP_AD_CopyLeadTransactions'';
                        END;
                END;
            --exec fees related SP          
            SET @acCalendarID = (
                                SELECT COUNT(P.ACId)
                                FROM   arPrograms P
                                      ,arPrgVersions PV
                                      ,arStuEnrollments SE
                                WHERE  PV.PrgVerId = SE.PrgVerId
                                       AND SE.StuEnrollId = @stuEnrollId
                                       AND P.ProgId = PV.ProgId
                                       AND ACId = 5
                                );
            --IF @acCalendarID >= 1          
            --    BEGIN          
            --apply fees by progver for clock hr programs          
            INSERT INTO saTransactions (
                                       TransactionId
                                      ,StuEnrollId
                                      ,TermId
                                      ,CampusId
                                      ,TransDate
                                      ,TransCodeId
                                      ,TransReference
                                      ,TransDescrip
                                      ,TransAmount
                                      ,FeeLevelId
                                      ,AcademicYearId
                                      ,TransTypeId
                                      ,IsPosted
                                      ,FeeId
                                      ,CreateDate
                                      ,IsAutomatic
                                      ,ModUser
                                      ,ModDate
                                       )
                        SELECT     NEWID()
                                  ,@stuEnrollId
                                  ,NULL
                                  ,@campus
                                  ,SE.ExpStartDate
                                  ,PVF.TransCodeId
                                  ,PV.PrgVerDescrip AS TransDescrip
                                  ,ST.TransCodeDescrip + ''-'' + PV.PrgVerDescrip AS TransCodeDescrip
                                  ,CASE PVF.UnitId
                                        WHEN 0 THEN PVF.Amount * PV.Credits
                                        WHEN 1 THEN PVF.Amount * PV.Hours
                                        WHEN 2 THEN PVF.Amount
                                   END AS TransAmount
                                  ,2 -- program version fees          
                                  ,NULL
                                  ,0
                                  ,1
                                  ,PVF.PrgVerFeeId
                                  ,@transDate
                                  ,0
                                  ,@modUserN
                                  ,@transDate
                        FROM       saProgramVersionFees AS PVF
                        INNER JOIN arPrgVersions PV ON PV.PrgVerId = PVF.PrgVerId
                        INNER JOIN arStuEnrollments SE ON SE.PrgVerId = PVF.PrgVerId
                        INNER JOIN saTransCodes AS ST ON ST.TransCodeId = PVF.TransCodeId
                        WHERE      SE.StuEnrollId = @stuEnrollId
                                   AND PVF.TuitionCategoryId = SE.TuitionCategoryId
                                   AND RateScheduleId IS NULL;

            --charge Program Fees if applicable          
            IF ( @MyError = 0 )
                BEGIN
                    DECLARE @Result INT;
                    DECLARE @currdt DATETIME;
                    SET @currdt = @transDate;
                    EXEC USP_ApplyFeesByProgramVersion @stuEnrollId    -- uniqueidentifier          
                                                      ,@modUserN       -- varchar(100)          
                                                      ,@campus         -- uniqueidentifier          
                                                      ,@currdt         -- datetime          
                                                      ,@Result OUTPUT; -- int          
                    SET @MyError = @@ERROR;
                END;
            IF ( @MyError = 0 )
                BEGIN
                    IF ( @prgVersion IS NOT NULL )
                        BEGIN
                            EXEC dbo.USP_REGISTER_STUDENT_AUTOMATICALLY @prgVersion
                                                                       ,@stuEnrollId
                                                                       ,@modUserN;
                        END;
                    SET @MyError = @@ERROR;
                END;

            IF (
               @MyError = 0
               AND @Result = 1
               )
                BEGIN
                    COMMIT TRANSACTION;
                    SET @Message = N''Successful COMMIT TRANSACTION''; -- do not change this message. It is used for verification in C# code.          
                    SELECT @Message;
                END;
            ELSE
                BEGIN
                    ROLLBACK TRANSACTION;
                    SET @Message = @Message + N''rollback failed transaction'';
                    SELECT @Message;
                END;
        END TRY
        BEGIN CATCH
            ROLLBACK TRANSACTION;
            SET @Message = @Message + N''Failed ROLLBACK TRANSACTION  Catching error'';

            SET @Message = ERROR_MESSAGE() + @Message + N''try failed transaction'';
            SELECT @Message;
        END CATCH;
    END;
-- =========================================================================================================          
-- END  --  USP_EnrollLead          
-- =========================================================================================================          

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_GetAbsentToday]'
GO
IF OBJECT_ID(N'[dbo].[USP_GetAbsentToday]', 'P') IS NULL
EXEC sp_executesql N'

--=================================================================================================
-- USP_GetAbsentToday
--=================================================================================================
-- =============================================
-- Author:		Kimberly 
-- Create date: 05/13/2019
-- Description: Get List of students that are Absent Today
--             
--                 
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetAbsentToday]
    @DateRun DATE,
	@CampusId UNIQUEIDENTIFIER
AS
    BEGIN

        DECLARE @dayOfWeek INT;
        SET @dayOfWeek = (
                         SELECT CASE DATENAME(WEEKDAY, @DateRun)
                                     WHEN ''Monday'' THEN 1
                                     WHEN ''Tuesday'' THEN 2
                                     WHEN ''Wednesday'' THEN 3
                                     WHEN ''Thursday'' THEN 4
                                     WHEN ''Friday'' THEN 5
                                     WHEN ''Saturday'' THEN 6
                                     WHEN ''Sunday'' THEN 7
                                END AS wDay
                         );

         SELECT DISTINCT 
               LTRIM(RTRIM(cp.CampDescrip)) AS CampDescrip
              ,l.StudentNumber
              ,( FirstName + '' '' + LastName ) AS FullName
              ,FirstName
              ,LastName
              ,(
               SELECT STUFF(STUFF(STUFF((
                                        SELECT   TOP ( 1 ) Phone
                                        FROM     adLeadPhone
                                        WHERE    LeadId = l.LeadId
                                        ORDER BY IsBest DESC
                                                ,IsShowOnLeadPage DESC
                                                ,Position DESC
                                        )
                                       ,1
                                       ,0
                                       ,''(''
                                       )
                                 ,5
                                 ,0
                                 ,'') ''
                                 )
                           ,10
                           ,0
                           ,''-''
                           )
               ) AS Phone
              ,sc.StatusCodeDescrip
              ,(SELECT   TOP ( 1 ) CONVERT(DATE, RecordDate)
                                    FROM     arStudentClockAttendance
                                    WHERE    SchedHours > 0
                                             AND ActualHours > 0
                                             AND StuEnrollId = e.StuEnrollId
                                    ORDER BY RecordDate DESC) AS LDA
        FROM   dbo.arStuEnrollments e
        JOIN   syStatusCodes sc ON sc.StatusCodeId = e.StatusCodeId
        JOIN   dbo.arStudentSchedules ss ON ss.StuEnrollId = e.StuEnrollId
        JOIN   dbo.arProgSchedules ps ON ps.ScheduleId = ss.ScheduleId
        JOIN   dbo.arProgScheduleDetails psd ON psd.ScheduleId = ss.ScheduleId
        JOIN   arStudentTimeClockPunches p ON p.StuEnrollId = e.StuEnrollId
        JOIN   adLeads l ON l.LeadId = e.LeadId
        JOIN   dbo.adLeadPhone lp ON lp.LeadId = l.LeadId
        JOIN   dbo.syCampuses cp ON cp.CampusId = e.CampusId
        WHERE  e.ExpStartDate < @DateRun
               AND e.ExpGradDate > @DateRun
               AND psd.dw = @dayOfWeek
               AND psd.total > 0
			   AND p.Status =1
			   AND e.CampusId = @CampusId
               AND e.StuEnrollId NOT IN (
                                        SELECT DISTINCT sp.StuEnrollId
                                        FROM   arStudentTimeClockPunches sp
                                        JOIN   dbo.arStuEnrollments e ON e.StuEnrollId = sp.StuEnrollId
                                        WHERE  CONVERT(DATE, PunchTime) = @DateRun 
                                        )
               AND SysStatusId IN ( 9, 20, 6 )
			   ORDER BY l.LastName, l.FirstName;

    END;
--=================================================================================================
-- END  --  USP_GetAbsentToday
--=================================================================================================


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[usp_GetClassroomWorkServices]'
GO
IF OBJECT_ID(N'[dbo].[usp_GetClassroomWorkServices]', 'P') IS NULL
EXEC sp_executesql N'
/* 
PURPOSE: 
Get component summaries for a selected student enrollment, class section and component type

CREATED: 


MODIFIED:
10/25/2013 WP	US4547 Refactor Post Services by Student process

ResourceId
Homework = 499
LabWork = 500
Exam = 501
Final = 502
LabHours = 503
Externship = 544

*/

CREATE PROCEDURE [dbo].[usp_GetClassroomWorkServices]
    (
        @StuEnrollId UNIQUEIDENTIFIER
       ,@ClsSectionId UNIQUEIDENTIFIER
       ,@GradeBookComponentType INT
    )
AS
    SET NOCOUNT ON;


    DECLARE @ReqId AS UNIQUEIDENTIFIER;
    SELECT @ReqId = ReqId
    FROM   arClassSections
    WHERE  ClsSectionId = @ClsSectionId;

    SELECT *
    INTO   #PostedServices
    FROM   arGrdBkResults
    WHERE  StuEnrollId = @StuEnrollId
           AND ClsSectionId = @ClsSectionId
           AND InstrGrdBkWgtDetailId IN (
                                        SELECT InstrGrdBkWgtDetailId
                                        FROM   arGrdBkWgtDetails posted_gbwd
                                        JOIN   arGrdComponentTypes posted_gct ON posted_gct.GrdComponentTypeId = posted_gbwd.GrdComponentTypeId
                                        WHERE  posted_gct.SysComponentTypeId = @GradeBookComponentType
                                        );

--SELECT * FROM #PostedServices


SELECT    gct.Descrip
             ,ISNULL(gbwd.Number, 0) AS Required
             ,CASE WHEN @GradeBookComponentType = 503 THEN 
			  ISNULL((
                               SELECT TOP (1) Score
                               FROM   #PostedServices
                               WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                               )
                              ,0
                              )
                   ELSE ISNULL((
                               SELECT COUNT(*)
                               FROM   #PostedServices
                               WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                               )
                              ,0
                              )
              END AS Completed
             ,CASE WHEN @GradeBookComponentType = 503 THEN CASE WHEN ( ISNULL(gbwd.Number, 0) - ISNULL(( ps.Score ), 0)) <= 0 THEN 0
                                                                ELSE ( ISNULL(gbwd.Number, 0) - ISNULL(( ps.Score ), 0))
                                                           END
                   ELSE CASE WHEN ( ISNULL(gbwd.Number, 0) - ISNULL((
                                                                    SELECT COUNT(*)
                                                                    FROM   #PostedServices
                                                                    WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                                                    )
                                                                   ,0
                                                                   )
                                  ) <= 0 THEN 0
                             ELSE ( ISNULL(gbwd.Number, 0) - ISNULL((
                                                                    SELECT COUNT(*)
                                                                    FROM   #PostedServices
                                                                    WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                                                    )
                                                                   ,0
                                                                   )
                                  )
                        END
              END AS Remaining
             ,CASE WHEN @GradeBookComponentType = 503 THEN NULL
                   ELSE AVG(ps.Score)
              END AS Average
             ,se.EnrollDate
             ,gbwd.InstrGrdBkWgtDetailId
             ,@ClsSectionId AS ClsSectionId
			 ,gbwd.Seq
    FROM      arGrdBkWeights gbw
    LEFT JOIN arGrdBkWgtDetails gbwd ON gbwd.InstrGrdBkWgtId = gbw.InstrGrdBkWgtId
    LEFT JOIN arGrdComponentTypes gct ON gct.GrdComponentTypeId = gbwd.GrdComponentTypeId
    LEFT JOIN arClassSections cs ON cs.ReqId = gbw.ReqId
    LEFT JOIN arResults ar ON ar.TestId = cs.ClsSectionId
    LEFT JOIN arStuEnrollments se ON ar.StuEnrollId = se.StuEnrollId
    LEFT JOIN #PostedServices ps ON ps.InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
    WHERE     gct.SysComponentTypeId = @GradeBookComponentType
              AND gbw.ReqId = @ReqId
              AND ar.TestId = @ClsSectionId
              AND ar.StuEnrollId = @StuEnrollId
    GROUP BY  gct.Descrip
             ,gbwd.Number
             ,se.EnrollDate
             ,gbwd.InstrGrdBkWgtDetailId
			 ,ps.Score
			 ,gbwd.Seq;



    DROP TABLE #PostedServices;






'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_GetIn-SchoolToday]'
GO
IF OBJECT_ID(N'[dbo].[USP_GetIn-SchoolToday]', 'P') IS NULL
EXEC sp_executesql N'

--=================================================================================================
--  USP_GetIn-SchoolToday
--=================================================================================================
-- =============================================
-- Author:		Kimberly 
-- Create date: 06/10/2019
-- Description: Get List of students that are in School Today.
--             
--                 
-- =============================================
CREATE PROCEDURE [dbo].[USP_GetIn-SchoolToday]
    @DateRun DATE,
    @CampusId UNIQUEIDENTIFIER

AS
    BEGIN

        DECLARE @dayOfWeek INT;
        SET @dayOfWeek = (
                         SELECT CASE DATENAME(WEEKDAY, @DateRun)
                                     WHEN ''Monday'' THEN 1
                                     WHEN ''Tuesday'' THEN 2
                                     WHEN ''Wednesday'' THEN 3
                                     WHEN ''Thursday'' THEN 4
                                     WHEN ''Friday'' THEN 5
                                     WHEN ''Saturday'' THEN 6
                                     WHEN ''Sunday'' THEN 7
                                END AS wDay
                         );

         SELECT DISTINCT 
               LTRIM(RTRIM(cp.CampDescrip)) AS CampDescrip
              ,l.StudentNumber
              ,( FirstName + '' '' + LastName ) AS FullName
              ,FirstName
              ,LastName
              ,(
               SELECT STUFF(STUFF(STUFF((
                                        SELECT   TOP ( 1 ) Phone
                                        FROM     adLeadPhone
                                        WHERE    LeadId = l.LeadId
                                        ORDER BY IsBest DESC
                                                ,IsShowOnLeadPage DESC
                                                ,Position DESC
                                        )
                                       ,1
                                       ,0
                                       ,''(''
                                       )
                                 ,5
                                 ,0
                                 ,'') ''
                                 )
                           ,10
                           ,0
                           ,''-''
                           )
               ) AS Phone
              ,sc.StatusCodeDescrip
              ,(SELECT   TOP ( 1 ) CONVERT(DATE, RecordDate)
                                    FROM     arStudentClockAttendance
                                    WHERE    SchedHours > 0
                                             AND ActualHours > 0
                                             AND StuEnrollId = e.StuEnrollId
                                    ORDER BY RecordDate DESC) AS LDA
        FROM   dbo.arStuEnrollments e
        JOIN   syStatusCodes sc ON sc.StatusCodeId = e.StatusCodeId
        JOIN   dbo.arStudentSchedules ss ON ss.StuEnrollId = e.StuEnrollId
        JOIN   dbo.arProgSchedules ps ON ps.ScheduleId = ss.ScheduleId
        JOIN   dbo.arProgScheduleDetails psd ON psd.ScheduleId = ss.ScheduleId
        JOIN   arStudentTimeClockPunches p ON p.StuEnrollId = e.StuEnrollId
        JOIN   adLeads l ON l.LeadId = e.LeadId
        JOIN   dbo.adLeadPhone lp ON lp.LeadId = l.LeadId
        JOIN   dbo.syCampuses cp ON cp.CampusId = e.CampusId
        WHERE  e.ExpStartDate < @DateRun
               AND e.ExpGradDate > @DateRun
               AND psd.dw = @dayOfWeek
               AND psd.total > 0
			   AND p.Status =1
			   AND e.CampusId = @CampusId
               AND e.StuEnrollId  IN (
                                        SELECT DISTINCT sp.StuEnrollId
                                        FROM   arStudentTimeClockPunches sp
                                        JOIN   dbo.arStuEnrollments e ON e.StuEnrollId = sp.StuEnrollId
                                        WHERE  CONVERT(DATE, PunchTime) = @DateRun 
                                        )
               AND SysStatusId IN ( 9, 20, 6 )
			   ORDER BY l.LastName, l.FirstName;

    END;
--=================================================================================================
-- END  USP_GetIn-SchoolToday
--=================================================================================================


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_GPACalculator]'
GO
IF OBJECT_ID(N'[dbo].[USP_GPACalculator]', 'P') IS NULL
EXEC sp_executesql N'-- =============================================
-- Author:		FAME Inc.
-- Create date: 11/1/2018
-- Description:	Calculated Student GPA Based on a set of parameters - Numeric ( Weighted & Unweighted currently implemented)
--Referenced in :
--[Usp_PR_Sub2_Enrollment_Summary]
--[Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId]
--[USP_TitleIV_QualitativeAndQuantitative]
--CreditSummaryDB.vb
--Core Web API - EnrollmentService.GetEnrollmentProgramSummary
--[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents] -> via db function CalculateStudentAverage
--[Usp_PR_Sub3_Terms_forAllEnrolmnents] -> via db function CalculateStudentAverage
-- =============================================
CREATE PROCEDURE [dbo].[USP_GPACalculator]
    (
        @EnrollmentId VARCHAR(50) = NULL
       ,@BeginDate DATETIME = NULL
       ,@EndDate DATETIME = NULL
       ,@ClassId UNIQUEIDENTIFIER = NULL
       ,@TermId UNIQUEIDENTIFIER = NULL
       ,@StudentGPA AS DECIMAL(16, 2) OUTPUT
    )
AS
    BEGIN
        DECLARE @GPAResult AS DECIMAL(16, 2);
        -----------------Numeric GPA Grades Format------------------------

        SET @GPAResult = (
                         SELECT TOP 1 dbo.CalculateStudentAverage(@EnrollmentId, @BeginDate, @EndDate, @ClassId, @TermId)
                         );

        --END;
        SET @StudentGPA = @GPAResult;
    END;





'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_PR_Sub2_Enrollment_Summary]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub2_Enrollment_Summary]', 'P') IS NULL
EXEC sp_executesql N'-- =========================================================================================================
-- Usp_PR_Sub2_Enrollment_Summary
-- =========================================================================================================
CREATE PROCEDURE [dbo].[Usp_PR_Sub2_Enrollment_Summary]
    @StuEnrollId VARCHAR(50)
   ,@TermId VARCHAR(4000) = NULL
   ,@TermStartDate DATETIME = NULL
   ,@TermStartDateModifier VARCHAR(10) = NULL
   ,@ShowFinanceCalculations BIT = 0
   -- by default hide cost and current balance
   ,@ShowWeeklySchedule BIT = 0
-- , @TrackSapAttendance VARCHAR(10) = NULL
-- , @displayhours VARCHAR(10) = NULL
AS
    BEGIN
        DECLARE @TrackSapAttendance VARCHAR(1000);
        DECLARE @displayHours AS VARCHAR(1000);
        DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
        DECLARE @GradesFormat AS VARCHAR(50);

        IF @TermStartDate IS NULL
           OR @TermStartDate = ''''
            BEGIN
                SET @TermStartDate = CONVERT(DATETIME, GETDATE(), 120);
            END;
        SET @StuEnrollCampusId = COALESCE((
                                          SELECT ASE.CampusId
                                          FROM   arStuEnrollments AS ASE
                                          WHERE  ASE.StuEnrollId = @StuEnrollId
                                          )
                                         ,NULL
                                         );
        SET @TrackSapAttendance = dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', @StuEnrollCampusId);
        IF ( @TrackSapAttendance IS NOT NULL )
            BEGIN
                SET @TrackSapAttendance = LOWER(LTRIM(RTRIM(@TrackSapAttendance)));
            END;
        --
        SET @displayHours = dbo.GetAppSettingValueByKeyName(''DisplayAttendanceUnitForProgressReportByClass'', @StuEnrollCampusId);
        IF ( @displayHours IS NOT NULL )
            BEGIN
                SET @displayHours = LOWER(LTRIM(RTRIM(@displayHours)));
            END;
        --
        SET @GradesFormat = dbo.GetAppSettingValueByKeyName(''GradesFormat'',@StuEnrollCampusId);
        IF ( @GradesFormat IS NOT NULL )
            BEGIN
                SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
            END;

        IF ( @TermId IS NOT NULL )
            BEGIN
                SET @TermStartDate = (
                                     SELECT TOP 1 EndDate
                                     FROM   arTerm
                                     WHERE  TermId = @TermId
                                     );
                IF @TermStartDate IS NULL
                    BEGIN
                        SET @TermStartDate = (
                                             SELECT TOP 1 StartDate
                                             FROM   arTerm
                                             WHERE  TermId = @TermId
                                             );
                    END;
                SET @TermStartDateModifier = ''<='';
            END;



        DECLARE @ReturnValue VARCHAR(100);
        SELECT     @ReturnValue = COALESCE(@ReturnValue, '''') + Descrip + '',  ''
        FROM       adLeadByLeadGroups t1
        INNER JOIN adLeadGroups t2 ON t1.LeadGrpId = t2.LeadGrpId
        WHERE      t1.StuEnrollId = @StuEnrollId;
        SET @ReturnValue = SUBSTRING(@ReturnValue, 1, LEN(@ReturnValue) - 1);


        /************  Logic to calculate GPA Starts Here *****************************/

        DECLARE @CoursesNotRepeated TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,TermId UNIQUEIDENTIFIER
               ,TermDescrip VARCHAR(100)
               ,ReqId UNIQUEIDENTIFIER
               ,ReqDescrip VARCHAR(100)
               ,ClsSectionId VARCHAR(50)
               ,CreditsEarned DECIMAL(18, 2)
               ,CreditsAttempted DECIMAL(18, 2)
               ,CurrentScore DECIMAL(18, 2)
               ,CurrentGrade VARCHAR(10)
               ,FinalScore DECIMAL(18, 2)
               ,FinalGrade VARCHAR(10)
               ,Completed BIT
               ,FinalGPA DECIMAL(18, 2)
               ,Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
               ,Count_WeightedAverage_Credits DECIMAL(18, 2)
               ,Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
               ,Count_SimpleAverage_Credits DECIMAL(18, 2)
               ,ModUser VARCHAR(50)
               ,ModDate DATETIME
               ,TermGPA_Simple DECIMAL(18, 2)
               ,TermGPA_Weighted DECIMAL(18, 2)
               ,coursecredits DECIMAL(18, 2)
               ,CumulativeGPA DECIMAL(18, 2)
               ,CumulativeGPA_Simple DECIMAL(18, 2)
               ,FACreditsEarned DECIMAL(18, 2)
               ,Average DECIMAL(18, 2)
               ,CumAverage DECIMAL(18, 2)
               ,TermStartDate DATETIME
               ,rownumber INT
            );

        INSERT INTO @CoursesNotRepeated
                    SELECT StuEnrollId
                          ,TermId
                          ,TermDescrip
                          ,ReqId
                          ,ReqDescrip
                          ,ClsSectionId
                          ,CreditsEarned
                          ,CreditsAttempted
                          ,CurrentScore
                          ,CurrentGrade
                          ,FinalScore
                          ,FinalGrade
                          ,Completed
                          ,FinalGPA
                          ,Product_WeightedAverage_Credits_GPA
                          ,Count_WeightedAverage_Credits
                          ,Product_SimpleAverage_Credits_GPA
                          ,Count_SimpleAverage_Credits
                          ,ModUser
                          ,ModDate
                          ,TermGPA_Simple
                          ,TermGPA_Weighted
                          ,coursecredits
                          ,CumulativeGPA
                          ,CumulativeGPA_Simple
                          ,FACreditsEarned
                          ,Average
                          ,CumAverage
                          ,TermStartDate
                          ,NULL AS rownumber
                    FROM   syCreditSummary
                    WHERE  StuEnrollId = @StuEnrollId
                           AND ReqId IN (
                                        SELECT ReqId
                                        FROM   (
                                               SELECT   ReqId
                                                       ,ReqDescrip
                                                       ,COUNT(*) AS counter
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                               GROUP BY ReqId
                                                       ,ReqDescrip
                                               HAVING   COUNT(*) = 1
                                               ) dt
                                        );

        DECLARE @GradeCourseRepetitionsMethod VARCHAR(50);
        SET @GradeCourseRepetitionsMethod = (
                                            SELECT     Value
                                            FROM       dbo.syConfigAppSetValues t1
                                            INNER JOIN dbo.syConfigAppSettings t2 ON t1.SettingId = t2.SettingId
                                                                                     AND t2.KeyName = ''GradeCourseRepetitionsMethod''
                                            );

        IF LOWER(@GradeCourseRepetitionsMethod) = ''latest''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT   dt2.StuEnrollId
                                    ,dt2.TermId
                                    ,dt2.TermDescrip
                                    ,dt2.ReqId
                                    ,dt2.ReqDescrip
                                    ,dt2.ClsSectionId
                                    ,dt2.CreditsEarned
                                    ,dt2.CreditsAttempted
                                    ,dt2.CurrentScore
                                    ,dt2.CurrentGrade
                                    ,dt2.FinalScore
                                    ,dt2.FinalGrade
                                    ,dt2.Completed
                                    ,dt2.FinalGPA
                                    ,dt2.Product_WeightedAverage_Credits_GPA
                                    ,dt2.Count_WeightedAverage_Credits
                                    ,dt2.Product_SimpleAverage_Credits_GPA
                                    ,dt2.Count_SimpleAverage_Credits
                                    ,dt2.ModUser
                                    ,dt2.ModDate
                                    ,dt2.TermGPA_Simple
                                    ,dt2.TermGPA_Weighted
                                    ,dt2.coursecredits
                                    ,dt2.CumulativeGPA
                                    ,dt2.CumulativeGPA_Simple
                                    ,dt2.FACreditsEarned
                                    ,dt2.Average
                                    ,dt2.CumAverage
                                    ,dt2.TermStartDate
                                    ,dt2.RowNumber
                            FROM     (
                                     SELECT StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                                ORDER BY TermStartDate DESC
                                                              ) AS RowNumber
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND (
                                                FinalScore IS NOT NULL
                                                OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN (
                                                         SELECT ReqId
                                                         FROM   (
                                                                SELECT   ReqId
                                                                        ,ReqDescrip
                                                                        ,COUNT(*) AS Counter
                                                                FROM     syCreditSummary
                                                                WHERE    StuEnrollId = @StuEnrollId
                                                                GROUP BY ReqId
                                                                        ,ReqDescrip
                                                                HAVING   COUNT(*) > 1
                                                                ) dt
                                                         )
                                     ) dt2
                            WHERE    RowNumber = 1
                            ORDER BY TermStartDate DESC;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''best''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                            FROM   (
                                   SELECT StuEnrollId
                                         ,TermId
                                         ,TermDescrip
                                         ,ReqId
                                         ,ReqDescrip
                                         ,ClsSectionId
                                         ,CreditsEarned
                                         ,CreditsAttempted
                                         ,CurrentScore
                                         ,CurrentGrade
                                         ,FinalScore
                                         ,FinalGrade
                                         ,Completed
                                         ,FinalGPA
                                         ,Product_WeightedAverage_Credits_GPA
                                         ,Count_WeightedAverage_Credits
                                         ,Product_SimpleAverage_Credits_GPA
                                         ,Count_SimpleAverage_Credits
                                         ,ModUser
                                         ,ModDate
                                         ,TermGPA_Simple
                                         ,TermGPA_Weighted
                                         ,coursecredits
                                         ,CumulativeGPA
                                         ,CumulativeGPA_Simple
                                         ,FACreditsEarned
                                         ,Average
                                         ,CumAverage
                                         ,TermStartDate
                                         ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                              ORDER BY FinalGPA DESC
                                                            ) AS RowNumber
                                   FROM   syCreditSummary
                                   WHERE  StuEnrollId = @StuEnrollId
                                          AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                              )
                                          AND ReqId IN (
                                                       SELECT ReqId
                                                       FROM   (
                                                              SELECT   ReqId
                                                                      ,ReqDescrip
                                                                      ,COUNT(*) AS Counter
                                                              FROM     syCreditSummary
                                                              WHERE    StuEnrollId = @StuEnrollId
                                                              GROUP BY ReqId
                                                                      ,ReqDescrip
                                                              HAVING   COUNT(*) > 1
                                                              ) dt
                                                       )
                                   ) dt2
                            WHERE  RowNumber = 1;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''average''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                            FROM   (
                                   SELECT StuEnrollId
                                         ,TermId
                                         ,TermDescrip
                                         ,ReqId
                                         ,ReqDescrip
                                         ,ClsSectionId
                                         ,CreditsEarned
                                         ,CreditsAttempted
                                         ,CurrentScore
                                         ,CurrentGrade
                                         ,FinalScore
                                         ,FinalGrade
                                         ,Completed
                                         ,FinalGPA
                                         ,Product_WeightedAverage_Credits_GPA
                                         ,Count_WeightedAverage_Credits
                                         ,Product_SimpleAverage_Credits_GPA
                                         ,Count_SimpleAverage_Credits
                                         ,ModUser
                                         ,ModDate
                                         ,TermGPA_Simple
                                         ,TermGPA_Weighted
                                         ,coursecredits
                                         ,CumulativeGPA
                                         ,CumulativeGPA_Simple
                                         ,FACreditsEarned
                                         ,Average
                                         ,CumAverage
                                         ,TermStartDate
                                         ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                              ORDER BY FinalGPA DESC
                                                            ) AS RowNumber
                                   FROM   syCreditSummary
                                   WHERE  StuEnrollId = @StuEnrollId
                                          AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                              )
                                          AND ReqId IN (
                                                       SELECT ReqId
                                                       FROM   (
                                                              SELECT   ReqId
                                                                      ,ReqDescrip
                                                                      ,COUNT(*) AS Counter
                                                              FROM     syCreditSummary
                                                              WHERE    StuEnrollId = @StuEnrollId
                                                              GROUP BY ReqId
                                                                      ,ReqDescrip
                                                              HAVING   COUNT(*) > 1
                                                              ) dt
                                                       )
                                   ) dt2;
            END;

        /*** Calculate Cumulative Simple GPA Starts Here ****/
        DECLARE @cumSimpleCourseCredits DECIMAL(18, 2)
               ,@cumSimple_GPA_Credits DECIMAL(18, 2)
               ,@cumSimpleGPA DECIMAL(18, 2);
        SET @cumSimpleGPA = 0;
        SET @cumSimpleCourseCredits = (
                                      SELECT COUNT(coursecredits)
                                      FROM   @CoursesNotRepeated
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND FinalGPA IS NOT NULL
                                      );
        SET @cumSimple_GPA_Credits = (
                                     SELECT SUM(FinalGPA)
                                     FROM   @CoursesNotRepeated
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalGPA IS NOT NULL
                                     );

        -- 3.7 SP2 Changes
        -- Include Equivalent Courses
        DECLARE @EquivCourse_SA_CC INT
               ,@EquivCourse_SA_GPA DECIMAL(18, 2);
        SET @EquivCourse_SA_CC = (
                                 SELECT ISNULL(COUNT(Credits), 0) AS Credits
                                 FROM   arReqs
                                 WHERE  ReqId IN (
                                                 SELECT     CE.EquivReqId
                                                 FROM       arCourseEquivalent CE
                                                 INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                 INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                 WHERE      StuEnrollId = @StuEnrollId
                                                            AND R.GrdSysDetailId IS NOT NULL
                                                 )
                                 );

        SET @EquivCourse_SA_GPA = (
                                  SELECT     SUM(ISNULL(GSD.GPA, 0.00))
                                  FROM       arCourseEquivalent CE
                                  INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                  INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                  INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                  WHERE      StuEnrollId = @StuEnrollId
                                             AND R.GrdSysDetailId IS NOT NULL
                                  );

        -- PRINT ''Equiv Course Simple ''
        -- PRINT @EquivCourse_SA_CC
        -- PRINT ''Equiv Course GPA Simple''
        -- PRINT @EquivCourse_SA_GPA

        -- PRINT ''Before Course Credits Simple''
        -- PRINT @cumSimpleCourseCredits
        -- PRINT ''Before WE GPA Simple''
        -- PRINT @cumSimple_GPA_Credits

        SET @cumSimpleCourseCredits = @cumSimpleCourseCredits; -- + ISNULL(@EquivCourse_SA_CC,0.00);
        SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits; -- + ISNULL(@EquivCourse_SA_GPA,0.00);

        IF @cumSimpleCourseCredits >= 1
            BEGIN
                SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
            END;

        -- PRINT ''After Course Credits Simple''
        -- PRINT @cumSimpleCourseCredits
        -- PRINT ''After WE GPA Simple''
        -- PRINT @cumSimple_GPA_Credits
        -- PRINT @cumSimpleGPA

        /*** Calculate Cumulative Simple GPA Ends Here ****/

        /**** Calculate Cumulative GPA Weighted *********************/
        DECLARE @cumCourseCredits DECIMAL(18, 2)
               ,@cumWeighted_GPA_Credits DECIMAL(18, 2)
               ,@cumWeightedGPA DECIMAL(18, 2);
        SET @cumWeightedGPA = 0;
        SET @cumCourseCredits = (
                                SELECT SUM(coursecredits)
                                FROM   @CoursesNotRepeated
                                WHERE  StuEnrollId = @StuEnrollId
                                       AND FinalGPA IS NOT NULL
                                );
        SET @cumWeighted_GPA_Credits = (
                                       SELECT SUM(coursecredits * FinalGPA)
                                       FROM   @CoursesNotRepeated
                                       WHERE  StuEnrollId = @StuEnrollId
                                              AND FinalGPA IS NOT NULL
                                       );


        DECLARE @doesThisCourseHaveAEquivalentCourse INT;

        DECLARE @EquivCourse_WGPA_CC1 INT
               ,@EquivCourse_WGPA_GPA1 DECIMAL(18, 2);
        SET @EquivCourse_WGPA_CC1 = (
                                    SELECT SUM(ISNULL(Credits, 0)) AS Credits
                                    FROM   arReqs
                                    WHERE  ReqId IN (
                                                    SELECT     CE.EquivReqId
                                                    FROM       arCourseEquivalent CE
                                                    INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                    INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                    WHERE      StuEnrollId = @StuEnrollId
                                                               AND R.GrdSysDetailId IS NOT NULL
                                                    )
                                    );

        SET @EquivCourse_WGPA_GPA1 = (
                                     SELECT     SUM(GSD.GPA * R1.Credits)
                                     FROM       arCourseEquivalent CE
                                     INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                     INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                     INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                     INNER JOIN arReqs R1 ON R1.ReqId = CE.ReqId
                                     WHERE      StuEnrollId = @StuEnrollId
                                                AND R.GrdSysDetailId IS NOT NULL
                                     );

        -- PRINT ''Equiv Course ''
        -- PRINT @EquivCourse_WGPA_CC1
        -- PRINT ''Equiv Course GPA''
        -- PRINT @EquivCourse_WGPA_GPA1

        -- PRINT ''Before Course Credits''
        -- PRINT @cumCourseCredits
        -- PRINT ''Before WE GPA''
        -- PRINT @cumWeighted_GPA_Credits


        SET @cumCourseCredits = @cumCourseCredits; -- + ISNULL(@EquivCourse_WGPA_CC1,0.00);
        SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits; -- + ISNULL(@EquivCourse_WGPA_GPA1,0.00);

        IF @cumCourseCredits >= 1
            BEGIN
                SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
            END;

        -- PRINT ''After Course Credits''
        -- PRINT @cumCourseCredits
        -- PRINT ''After WE GPA''
        -- PRINT @cumWeighted_GPA_Credits

        -- PRINT ''After Cal GPA''
        -- PRINT @cumWeightedGPA

        /**************** Cumulative GPA Ends Here ************/


        /************  Logic to calculate GPA Ends Here *****************************/



        DECLARE @SUMCreditsAttempted DECIMAL(18, 2);
        DECLARE @SUMCreditsEarned DECIMAL(18, 2);
        DECLARE @SUMFACreditsEarned DECIMAL(18, 2);
        SET @SUMCreditsAttempted = 0;
        SET @SUMCreditsEarned = 0;
        SET @SUMFACreditsEarned = 0;

        DECLARE @EquivCourse_SA_CC2 DECIMAL(18, 2);
        SET @EquivCourse_SA_CC2 = (
                                  SELECT ISNULL(SUM(Credits), 0.00) AS Credits
                                  FROM   arReqs
                                  WHERE  ReqId IN (
                                                  SELECT     CE.EquivReqId
                                                  FROM       arCourseEquivalent CE
                                                  INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                  INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                  WHERE      StuEnrollId = @StuEnrollId
                                                             AND R.GrdSysDetailId IS NOT NULL
                                                  )
                                  );

        SELECT @SUMCreditsAttempted = SUM(ISNULL(SCS.CreditsAttempted, 0))
              ,@SUMCreditsEarned = SUM(ISNULL(SCS.CreditsEarned, 0))
        FROM   dbo.syCreditSummary AS SCS
        WHERE  SCS.StuEnrollId = @StuEnrollId;

        --SET @SUMCreditsEarned = @SUMCreditsEarned + @EquivCourse_SA_CC2;  

        SET @SUMFACreditsEarned = (
                                  SELECT SUM(FACreditsEarned)
                                  FROM   syCreditSummary
                                  WHERE  StuEnrollId = @StuEnrollId
                                  );

        DECLARE @Scheduledhours DECIMAL(18, 2)
               ,@ActualPresentDays_ConvertTo_Hours DECIMAL(18, 2);
        DECLARE @ActualAbsentDays_ConvertTo_Hours DECIMAL(18, 2)
               ,@ActualTardyDays_ConvertTo_Hours DECIMAL(18, 2);
        SET @Scheduledhours = 0;
        SET @Scheduledhours = (
                              SELECT SUM(ISNULL(ScheduledHours,0))
                              FROM   syStudentAttendanceSummary
                              WHERE  StuEnrollId = @StuEnrollId
                              );

        SET @ActualPresentDays_ConvertTo_Hours = (
                                                 SELECT SUM(ISNULL(ActualPresentDays_ConvertTo_Hours,0))
                                                 FROM   syStudentAttendanceSummary
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                 );

        SET @ActualAbsentDays_ConvertTo_Hours = (
                                                SELECT SUM(ISNULL(ActualAbsentDays_ConvertTo_Hours,0))
                                                FROM   syStudentAttendanceSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                );


        SET @ActualTardyDays_ConvertTo_Hours = (
                                               SELECT SUM(ISNULL(ActualTardyDays_ConvertTo_Hours,0))
                                               FROM   syStudentAttendanceSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                               );


        --Average calculation
        DECLARE @termAverageSum DECIMAL(18, 2)
               ,@CumAverage DECIMAL(18, 2)
               ,@cumAverageSum DECIMAL(18, 2)
               ,@cumAveragecount INT;
        DECLARE @TermAverageCount INT
               ,@TermAverage DECIMAL(18, 2);


        DECLARE @TardiesMakingAbsence INT
               ,@UnitTypeDescrip VARCHAR(20)
               ,@OriginalTardiesMakingAbsence INT;
        SET @TardiesMakingAbsence = (
                                    SELECT TOP 1 t1.TardiesMakingAbsence
                                    FROM   arPrgVersions t1
                                          ,arStuEnrollments t2
                                    WHERE  t1.PrgVerId = t2.PrgVerId
                                           AND t2.StuEnrollId = @StuEnrollId
                                    );

        SET @UnitTypeDescrip = (
                               SELECT     LTRIM(RTRIM(AAUT.UnitTypeDescrip))
                               FROM       arAttUnitType AS AAUT
                               INNER JOIN arPrgVersions AS APV ON APV.UnitTypeId = AAUT.UnitTypeId
                               INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                               WHERE      ASE.StuEnrollId = @StuEnrollId
                               );

        SET @OriginalTardiesMakingAbsence = (
                                            SELECT TOP 1 tardiesmakingabsence
                                            FROM   syStudentAttendanceSummary
                                            WHERE  StuEnrollId = @StuEnrollId
                                            );
        DECLARE @termstartdate1 DATETIME;

        DECLARE @MeetDate DATETIME
               ,@WeekDay VARCHAR(15)
               ,@StartDate DATETIME
               ,@EndDate DATETIME;
        DECLARE @PeriodDescrip VARCHAR(50)
               ,@Actual DECIMAL(18, 2)
               ,@Excused DECIMAL(18, 2)
               ,@ClsSectionId UNIQUEIDENTIFIER;
        DECLARE @Absent DECIMAL(18, 2)
               ,@SchedHours DECIMAL(18, 2)
               ,@TardyMinutes DECIMAL(18, 2);
        DECLARE @tardy DECIMAL(18, 2)
               ,@tracktardies INT
               ,@rownumber INT
               ,@IsTardy BIT
               ,@ActualRunningScheduledHours DECIMAL(18, 2);
        DECLARE @ActualRunningPresentHours DECIMAL(18, 2)
               ,@ActualRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningTardyHours DECIMAL(18, 2)
               ,@ActualRunningMakeupHours DECIMAL(18, 2);
        DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
               ,@intTardyBreakPoint INT
               ,@AdjustedRunningPresentHours DECIMAL(18, 2)
               ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningScheduledDays DECIMAL(18, 2);
        --declare @Scheduledhours decimal(18,2),@ActualPresentDays_ConvertTo_Hours decimal(18,2),@ActualAbsentDays_ConvertTo_Hours decimal(18,2),@AttendanceTrack varchar(50)
        DECLARE @TermDescrip VARCHAR(50)
               ,@PrgVerId UNIQUEIDENTIFIER;
        DECLARE @TardyHit VARCHAR(10)
               ,@ScheduledMinutes DECIMAL(18, 2);
        DECLARE @Scheduledhours_noperiods DECIMAL(18, 2)
               ,@ActualPresentDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2)
               ,@ActualAbsentDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2);
        DECLARE @ActualTardyDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2);

        --Select * from arAttUnitType
        -- By Class and present absent, none
        /*
	IF @UnitTypeDescrip IN ( ''none'',''present absent'' )
		AND @TrackSapAttendance = ''byclass''
		BEGIN
			---- PRINT ''Step1!!!!''
			DELETE  FROM syStudentAttendanceSummary
			WHERE   StuEnrollId = @StuEnrollId;
			DECLARE @boolReset BIT
			   ,@MakeupHours DECIMAL(18,2)
			   ,@AdjustedPresentDaysComputed DECIMAL(18,2);
			DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD
			FOR
				SELECT  *
					   ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
				FROM    ( SELECT DISTINCT
									t1.StuEnrollId
								   ,t1.ClsSectionId
								   ,t1.MeetDate
								   ,DATENAME(dw,t1.MeetDate) AS WeekDay
								   ,t4.StartDate
								   ,t4.EndDate
								   ,t5.PeriodDescrip
								   ,t1.Actual
								   ,t1.Excused
								   ,CASE WHEN ( t1.Actual = 0
												AND t1.Excused = 0
											  ) THEN t1.Scheduled
										 ELSE CASE WHEN ( t1.Actual <> 9999.00
														  AND t1.Actual < t1.Scheduled
														  AND t1.Excused <> 1
														) THEN ( t1.Scheduled - t1.Actual )
												   ELSE 0
											  END
									END AS Absent
								   ,t1.Scheduled AS ScheduledMinutes
								   ,CASE WHEN ( t1.Actual > 0
												AND t1.Actual < t1.Scheduled
											  ) THEN ( t1.Scheduled - t1.Actual )
										 ELSE 0
									END AS TardyMinutes
								   ,t1.Tardy AS Tardy
								   ,t3.TrackTardies
								   ,t3.TardiesMakingAbsence
								   ,t3.PrgVerId
						  FROM      atClsSectAttendance t1
						  INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
						  INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
						  INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
						  INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
															 AND ( CONVERT(CHAR(10),t1.MeetDate,101) >= CONVERT(CHAR(10),t4.StartDate,101)
																   AND CONVERT(CHAR(10),t1.MeetDate,101) <= CONVERT(CHAR(10),t4.EndDate,101)
																 )
															 AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
						  INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
						  INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
						  INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
						  INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
						  INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
						  INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
						  WHERE     t2.StuEnrollId = @StuEnrollId
									AND ( AAUT1.UnitTypeDescrip IN ( ''None'',''Present Absent'' )
										  OR AAUT2.UnitTypeDescrip IN ( ''None'',''Present Absent'' )
										)
									AND t1.Actual <> 9999
						) dt
				ORDER BY StuEnrollId
					   ,MeetDate;
			OPEN GetAttendance_Cursor;
			FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,
				@PeriodDescrip,@Actual,@Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,
				@TardiesMakingAbsence,@PrgVerId,@rownumber;
			SET @ActualRunningPresentHours = 0;
			SET @ActualRunningPresentHours = 0;
			SET @ActualRunningAbsentHours = 0;
			SET @ActualRunningTardyHours = 0;
			SET @ActualRunningMakeupHours = 0;
			SET @intTardyBreakPoint = 0;
			SET @AdjustedRunningPresentHours = 0;
			SET @AdjustedRunningAbsentHours = 0;
			SET @ActualRunningScheduledDays = 0;
			SET @boolReset = 0;
			SET @MakeupHours = 0;
			WHILE @@FETCH_STATUS = 0
				BEGIN

					IF @PrevStuEnrollId <> @StuEnrollId
						BEGIN
							SET @ActualRunningPresentHours = 0;
							SET @ActualRunningAbsentHours = 0;
							SET @intTardyBreakPoint = 0;
							SET @ActualRunningTardyHours = 0;
							SET @AdjustedRunningPresentHours = 0;
							SET @AdjustedRunningAbsentHours = 0;
							SET @ActualRunningScheduledDays = 0;
							SET @MakeupHours = 0;
							SET @boolReset = 1;
						END;

					  
					-- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
					IF @Actual <> 9999.00
						BEGIN
							SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual + @Excused;
							SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual + @Excused;
							
							-- If there are make up hrs deduct that otherwise it will be added again in progress report
							IF ( @Actual > 0
								 AND @Actual > @ScheduledMinutes
								 AND @Actual <> 9999.00
							   )
								BEGIN
									SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual
																									- @ScheduledMinutes );
									SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual
																										- @ScheduledMinutes );
								END;
							  
							SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;                      
						END;
				   
					-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
					SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
					SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
			  
					-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
					IF ( @Actual > 0
						 AND @Actual < @ScheduledMinutes
						 AND @tardy = 1
					   )
						BEGIN
							SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
						END;
							
					-- Track how many days student has been tardy only when 
					-- program version requires to track tardy
					IF ( @tracktardies = 1
						 AND @tardy = 1
					   )
						BEGIN
							SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
						END;	    
		   
			
					-- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
					-- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
					-- when student is tardy the second time, that second occurance will be considered as
					-- absence
					-- Variable @intTardyBreakpoint tracks how many times the student was tardy
					-- Variable @tardiesMakingAbsence tracks the tardy rule
					IF ( @tracktardies = 1
						 AND @intTardyBreakPoint = @TardiesMakingAbsence
					   )
						BEGIN
							SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
							SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent )
								+ @ScheduledMinutes; --@TardyMinutes
							SET @intTardyBreakPoint = 0;
						END;
				   
				   -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
					IF ( @Actual > 0
						 AND @Actual > @ScheduledMinutes
						 AND @Actual <> 9999.00
					   )
						BEGIN
							SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
						END;
			  
					IF ( @tracktardies = 1 )
						BEGIN
							SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
						END;
					ELSE
						BEGIN
							SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
						END;
				
				---- PRINT @MeetDate
				---- PRINT @ActualRunningAbsentHours
				
					DELETE  FROM syStudentAttendanceSummary
					WHERE   StuEnrollId = @StuEnrollId
							AND ClsSectionId = @ClsSectionId
							AND StudentAttendedDate = @MeetDate;
					INSERT  INTO syStudentAttendanceSummary
							( StuEnrollId
							,ClsSectionId
							,StudentAttendedDate
							,ScheduledDays
							,ActualDays
							,ActualRunningScheduledDays
							,ActualRunningPresentDays
							,ActualRunningAbsentDays
							,ActualRunningMakeupDays
							,ActualRunningTardyDays
							,AdjustedPresentDays
							,AdjustedAbsentDays
							,AttendanceTrackType
							,ModUser
							,ModDate
							,tardiesmakingabsence
							)
					VALUES  ( @StuEnrollId
							,@ClsSectionId
							,@MeetDate
							,@ScheduledMinutes
							,@Actual
							,@ActualRunningScheduledDays
							,@ActualRunningPresentHours
							,@ActualRunningAbsentHours
							,ISNULL(@MakeupHours,0)
							,@ActualRunningTardyHours
							,@AdjustedPresentDaysComputed
							,@AdjustedRunningAbsentHours
							,''Post Attendance by Class Min''
							,''sa''
							,GETDATE()
							,@TardiesMakingAbsence
							);

				--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
					SET @PrevStuEnrollId = @StuEnrollId; 

					FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,
						@EndDate,@PeriodDescrip,@Actual,@Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,
						@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
				END;
			CLOSE GetAttendance_Cursor;
			DEALLOCATE GetAttendance_Cursor;
		END;			
*/
        IF @UnitTypeDescrip IN ( ''none'', ''present absent'' )
           AND @TrackSapAttendance = ''byclass''
            BEGIN
                ---- PRINT ''Step1!!!!''
                DELETE FROM syStudentAttendanceSummary
                WHERE StuEnrollId = @StuEnrollId;
                DECLARE @boolReset BIT
                       ,@MakeupHours DECIMAL(18, 2)
                       ,@AdjustedPresentDaysComputed DECIMAL(18, 2);
                DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
                    SELECT   dt.StuEnrollId
                            ,dt.ClsSectionId
                            ,dt.MeetDate
                            ,dt.WeekDay
                            ,dt.StartDate
                            ,dt.EndDate
                            ,dt.PeriodDescrip
                            ,dt.Actual
                            ,dt.Excused
                            ,dt.Absent
                            ,dt.ScheduledMinutes
                            ,dt.TardyMinutes
                            ,dt.Tardy
                            ,dt.TrackTardies
                            ,dt.TardiesMakingAbsence
                            ,dt.PrgVerId
                            ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                    FROM     (
                             SELECT     DISTINCT t1.StuEnrollId
                                                ,t1.ClsSectionId
                                                ,t1.MeetDate
                                                ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                ,t4.StartDate
                                                ,t4.EndDate
                                                ,t5.PeriodDescrip
                                                ,t1.Actual
                                                ,t1.Excused
                                                ,CASE WHEN (
                                                           t1.Actual = 0
                                                           AND t1.Excused = 0
                                                           ) THEN t1.Scheduled
                                                      ELSE CASE WHEN (
                                                                     t1.Actual <> 9999.00
                                                                     AND t1.Actual < t1.Scheduled
                                                                     --AND t1.Excused <> 1
                                                                     ) THEN ( t1.Scheduled - t1.Actual )
                                                                ELSE 0
                                                           END
                                                 END AS Absent
                                                ,t1.Scheduled AS ScheduledMinutes
                                                ,CASE WHEN (
                                                           t1.Actual > 0
                                                           AND t1.Actual < t1.Scheduled
                                                           ) THEN ( t1.Scheduled - t1.Actual )
                                                      ELSE 0
                                                 END AS TardyMinutes
                                                ,t1.Tardy AS Tardy
                                                ,t3.TrackTardies
                                                ,t3.TardiesMakingAbsence
                                                ,t3.PrgVerId
                             FROM       atClsSectAttendance t1
                             INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                             INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                             INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                             INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                AND (
                                                                    CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                    AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                    )
                                                                AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                             INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                             INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                             INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                             INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                             INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                             INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                             WHERE      t2.StuEnrollId = @StuEnrollId
                                        AND (
                                            AAUT1.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                            OR AAUT2.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                            )
                                        AND t1.Actual <> 9999
                             ) dt
                    ORDER BY StuEnrollId
                            ,MeetDate;
                OPEN GetAttendance_Cursor;
                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@ClsSectionId
                    ,@MeetDate
                    ,@WeekDay
                    ,@StartDate
                    ,@EndDate
                    ,@PeriodDescrip
                    ,@Actual
                    ,@Excused
                    ,@Absent
                    ,@ScheduledMinutes
                    ,@TardyMinutes
                    ,@tardy
                    ,@tracktardies
                    ,@TardiesMakingAbsence
                    ,@PrgVerId
                    ,@rownumber;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                SET @boolReset = 0;
                SET @MakeupHours = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                                SET @MakeupHours = 0;
                                SET @boolReset = 1;
                            END;


                        -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                        IF @Actual <> 9999.00
                            BEGIN
                                -- Commented by Balaji on 2.6.2015 as Excused Absence is still considered an absence
                                -- @Excused is not added to Present Hours
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual; -- + @Excused
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; -- + @Excused

                                -- If there are make up hrs deduct that otherwise it will be added again in progress report
                                IF (
                                   @Actual > 0
                                   AND @Actual > @ScheduledMinutes
                                   AND @Actual <> 9999.00
                                   )
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                    END;

                                SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                            END;

                        -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                        -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                        IF (
                           @Actual > 0
                           AND @Actual < @ScheduledMinutes
                           AND @tardy = 1
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                            END;
                        ELSE IF (
                                @Actual = 1
                                AND @Actual = @ScheduledMinutes
                                AND @tardy = 1
                                )
                                 BEGIN
                                     SET @ActualRunningTardyHours += 1;
                                 END;

                        -- Track how many days student has been tardy only when 
                        -- program version requires to track tardy
                        IF (
                           @tracktardies = 1
                           AND @tardy = 1
                           )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;


                        -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
                        -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
                        -- when student is tardy the second time, that second occurance will be considered as
                        -- absence
                        -- Variable @intTardyBreakpoint tracks how many times the student was tardy
                        -- Variable @tardiesMakingAbsence tracks the tardy rule
                        IF (
                           @tracktardies = 1
                           AND @TardiesMakingAbsence > 0
                           AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
                                SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;

                        -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                        IF (
                           @Actual > 0
                           AND @Actual > @ScheduledMinutes
                           AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                            END;

                        IF ( @tracktardies = 1 )
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                            END;
                        ELSE
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                            END;

                        ---- PRINT @MeetDate
                        ---- PRINT @ActualRunningAbsentHours

                        DELETE FROM syStudentAttendanceSummary
                        WHERE StuEnrollId = @StuEnrollId
                              AND ClsSectionId = @ClsSectionId
                              AND StudentAttendedDate = @MeetDate;
                        INSERT INTO syStudentAttendanceSummary (
                                                               StuEnrollId
                                                              ,ClsSectionId
                                                              ,StudentAttendedDate
                                                              ,ScheduledDays
                                                              ,ActualDays
                                                              ,ActualRunningScheduledDays
                                                              ,ActualRunningPresentDays
                                                              ,ActualRunningAbsentDays
                                                              ,ActualRunningMakeupDays
                                                              ,ActualRunningTardyDays
                                                              ,AdjustedPresentDays
                                                              ,AdjustedAbsentDays
                                                              ,AttendanceTrackType
                                                              ,ModUser
                                                              ,ModDate
                                                              ,tardiesmakingabsence
                                                               )
                        VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                                ,@ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours, @AdjustedPresentDaysComputed
                                ,@AdjustedRunningAbsentHours, ''Post Attendance by Class Min'', ''sa'', GETDATE(), @TardiesMakingAbsence );

                        --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                        SET @PrevStuEnrollId = @StuEnrollId;

                        FETCH NEXT FROM GetAttendance_Cursor
                        INTO @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@WeekDay
                            ,@StartDate
                            ,@EndDate
                            ,@PeriodDescrip
                            ,@Actual
                            ,@Excused
                            ,@Absent
                            ,@ScheduledMinutes
                            ,@TardyMinutes
                            ,@tardy
                            ,@tracktardies
                            ,@TardiesMakingAbsence
                            ,@PrgVerId
                            ,@rownumber;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;

                DECLARE @MyTardyTable TABLE
                    (
                        ClsSectionId UNIQUEIDENTIFIER
                       ,TardiesMakingAbsence INT
                       ,AbsentHours DECIMAL(18, 2)
                    );

                INSERT INTO @MyTardyTable
                            SELECT     ClsSectionId
                                      ,PV.TardiesMakingAbsence
                                      ,CASE WHEN ( COUNT(*) >= PV.TardiesMakingAbsence ) THEN ( COUNT(*) / PV.TardiesMakingAbsence )
                                            ELSE 0
                                       END AS AbsentHours
                            --Count(*) as NumberofTimesTardy 
                            FROM       syStudentAttendanceSummary SAS
                            INNER JOIN arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                            WHERE      SE.StuEnrollId = @StuEnrollId
                                       AND SAS.IsTardy = 1
                                       AND PV.TrackTardies = 1
                            GROUP BY   ClsSectionId
                                      ,PV.TardiesMakingAbsence;

                --Drop table @MyTardyTable
                DECLARE @TotalTardyAbsentDays DECIMAL(18, 2);
                SET @TotalTardyAbsentDays = (
                                            SELECT ISNULL(SUM(AbsentHours), 0)
                                            FROM   @MyTardyTable
                                            );

                --Print @TotalTardyAbsentDays

                UPDATE syStudentAttendanceSummary
                SET    AdjustedPresentDays = AdjustedPresentDays - @TotalTardyAbsentDays
                      ,AdjustedAbsentDays = AdjustedAbsentDays + @TotalTardyAbsentDays
                WHERE  StuEnrollId = @StuEnrollId
                       AND StudentAttendedDate = (
                                                 SELECT   TOP 1 StudentAttendedDate
                                                 FROM     syStudentAttendanceSummary
                                                 WHERE    StuEnrollId = @StuEnrollId
                                                 ORDER BY StudentAttendedDate DESC
                                                 );

            END;

        -- By Class and Attendance Unit Type - Minutes and Clock Hour

        IF @UnitTypeDescrip IN ( ''minutes'', ''clock hours'' )
           AND @TrackSapAttendance = ''byclass''
            BEGIN
                DELETE FROM syStudentAttendanceSummary
                WHERE StuEnrollId = @StuEnrollId;

                DECLARE GetAttendance_Cursor CURSOR FOR
                    SELECT   *
                            ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                    FROM     (
                             SELECT     DISTINCT t1.StuEnrollId
                                                ,t1.ClsSectionId
                                                ,t1.MeetDate
                                                ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                ,t4.StartDate
                                                ,t4.EndDate
                                                ,t5.PeriodDescrip
                                                ,t1.Actual
                                                ,t1.Excused
                                                ,CASE WHEN (
                                                           t1.Actual = 0
                                                           AND t1.Excused = 0
                                                           ) THEN t1.Scheduled
                                                      ELSE CASE WHEN (
                                                                     t1.Actual <> 9999.00
                                                                     AND t1.Actual < t1.Scheduled
                                                                     ) THEN ( t1.Scheduled - t1.Actual )
                                                                ELSE 0
                                                           END
                                                 END AS Absent
                                                ,t1.Scheduled AS ScheduledMinutes
                                                ,CASE WHEN (
                                                           t1.Actual > 0
                                                           AND t1.Actual < t1.Scheduled
                                                           ) THEN ( t1.Scheduled - t1.Actual )
                                                      ELSE 0
                                                 END AS TardyMinutes
                                                ,t1.Tardy AS Tardy
                                                ,t3.TrackTardies
                                                ,t3.TardiesMakingAbsence
                                                ,t3.PrgVerId
                             FROM       atClsSectAttendance t1
                             INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                             INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                             INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                             INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                AND (
                                                                    CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                    AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                    )
                                                                AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                             INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                             INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                             INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                             INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                             INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                             INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                             WHERE      t2.StuEnrollId = @StuEnrollId
                                        AND (
                                            AAUT1.UnitTypeDescrip IN ( ''Minutes'', ''Clock Hours'' )
                                            OR AAUT2.UnitTypeDescrip IN ( ''Minutes'', ''Clock Hours'' )
                                            )
                                        AND t1.Actual <> 9999
                             UNION
                             SELECT     DISTINCT t1.StuEnrollId
                                                ,NULL AS ClsSectionId
                                                ,t1.RecordDate AS MeetDate
                                                ,DATENAME(dw, t1.RecordDate) AS WeekDay
                                                ,NULL AS StartDate
                                                ,NULL AS EndDate
                                                ,NULL AS PeriodDescrip
                                                ,t1.ActualHours
                                                ,NULL AS Excused
                                                ,CASE WHEN ( t1.ActualHours = 0 ) THEN t1.SchedHours
                                                      ELSE 0
                                                 --ELSE 
                                                 --Case when (t1.ActualHours <> 9999.00 and t1.ActualHours < t1.SchedHours)
                                                 --		THEN (t1.SchedHours - t1.ActualHours)
                                                 --		ELSE 
                                                 --			0
                                                 --		End
                                                 END AS Absent
                                                ,t1.SchedHours AS ScheduledMinutes
                                                ,CASE WHEN (
                                                           t1.ActualHours <> 9999.00
                                                           AND t1.ActualHours > 0
                                                           AND t1.ActualHours < t1.SchedHours
                                                           ) THEN ( t1.SchedHours - t1.ActualHours )
                                                      ELSE 0
                                                 END AS TardyMinutes
                                                ,NULL AS Tardy
                                                ,t3.TrackTardies
                                                ,t3.TardiesMakingAbsence
                                                ,t3.PrgVerId
                             FROM       arStudentClockAttendance t1
                             INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                             INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                             WHERE      t2.StuEnrollId = @StuEnrollId
                                        AND t1.Converted = 1
                                        AND t1.ActualHours <> 9999
                             ) dt
                    ORDER BY StuEnrollId
                            ,MeetDate;
                OPEN GetAttendance_Cursor;
                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@ClsSectionId
                    ,@MeetDate
                    ,@WeekDay
                    ,@StartDate
                    ,@EndDate
                    ,@PeriodDescrip
                    ,@Actual
                    ,@Excused
                    ,@Absent
                    ,@ScheduledMinutes
                    ,@TardyMinutes
                    ,@tardy
                    ,@tracktardies
                    ,@TardiesMakingAbsence
                    ,@PrgVerId
                    ,@rownumber;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                SET @boolReset = 0;
                SET @MakeupHours = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                                SET @MakeupHours = 0;
                                SET @boolReset = 1;
                            END;


                        -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                        IF @Actual <> 9999.00
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                                -- If there are make up hrs deduct that otherwise it will be added again in progress report
                                IF (
                                   @Actual > 0
                                   AND @Actual > @ScheduledMinutes
                                   AND @Actual <> 9999.00
                                   )
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                    END;
                                SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                            END;

                        -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                        -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                        IF (
                           @Actual > 0
                           AND @Actual < @ScheduledMinutes
                           AND @tardy = 1
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                            END;

                        -- Track how many days student has been tardy only when 
                        -- program version requires to track tardy
                        IF (
                           @tracktardies = 1
                           AND @tardy = 1
                           )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;


                        -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
                        -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
                        -- when student is tardy the second time, that second occurance will be considered as
                        -- absence
                        -- Variable @intTardyBreakpoint tracks how many times the student was tardy
                        -- Variable @tardiesMakingAbsence tracks the tardy rule
                        IF (
                           @tracktardies = 1
                           AND @TardiesMakingAbsence > 0
                           AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;

                        -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                        IF (
                           @Actual > 0
                           AND @Actual > @ScheduledMinutes
                           AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                            END;


                        IF ( @tracktardies = 1 )
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                            END;
                        ELSE
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                            END;




                        DELETE FROM syStudentAttendanceSummary
                        WHERE StuEnrollId = @StuEnrollId
                              AND ClsSectionId = @ClsSectionId
                              AND StudentAttendedDate = @MeetDate;
                        INSERT INTO syStudentAttendanceSummary (
                                                               StuEnrollId
                                                              ,ClsSectionId
                                                              ,StudentAttendedDate
                                                              ,ScheduledDays
                                                              ,ActualDays
                                                              ,ActualRunningScheduledDays
                                                              ,ActualRunningPresentDays
                                                              ,ActualRunningAbsentDays
                                                              ,ActualRunningMakeupDays
                                                              ,ActualRunningTardyDays
                                                              ,AdjustedPresentDays
                                                              ,AdjustedAbsentDays
                                                              ,AttendanceTrackType
                                                              ,ModUser
                                                              ,ModDate
                                                               )
                        VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                                ,@ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours, @AdjustedPresentDaysComputed
                                ,@AdjustedRunningAbsentHours, ''Post Attendance by Class Min'', ''sa'', GETDATE());

                        UPDATE syStudentAttendanceSummary
                        SET    tardiesmakingabsence = @TardiesMakingAbsence
                        WHERE  StuEnrollId = @StuEnrollId;
                        SET @PrevStuEnrollId = @StuEnrollId;

                        FETCH NEXT FROM GetAttendance_Cursor
                        INTO @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@WeekDay
                            ,@StartDate
                            ,@EndDate
                            ,@PeriodDescrip
                            ,@Actual
                            ,@Excused
                            ,@Absent
                            ,@ScheduledMinutes
                            ,@TardyMinutes
                            ,@tardy
                            ,@tracktardies
                            ,@TardiesMakingAbsence
                            ,@PrgVerId
                            ,@rownumber;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;
            END;
        --Select * from arAttUnitType


        -- By Minutes/Day
        -- remove clock hour from here
        IF @UnitTypeDescrip IN ( ''minutes'' )
           AND @TrackSapAttendance = ''byday''
            -- -- PRINT GETDATE();	
            ---- PRINT @UnitTypeId;
            ---- PRINT @TrackSapAttendance;
            BEGIN
                --Delete from syStudentAttendanceSummary where StuEnrollId=@StuEnrollId

                DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
                    SELECT     t1.StuEnrollId
                              ,NULL AS ClsSectionId
                              ,t1.RecordDate AS MeetDate
                              ,t1.ActualHours
                              ,t1.SchedHours AS ScheduledMinutes
                              ,CASE WHEN (
                                         (
                                         t1.SchedHours >= 1
                                         AND t1.SchedHours NOT IN ( 999, 9999 )
                                         )
                                         AND t1.ActualHours = 0
                                         ) THEN t1.SchedHours
                                    ELSE 0
                               END AS Absent
                              ,t1.isTardy
                              ,(
                               SELECT ISNULL(SUM(SchedHours), 0)
                               FROM   arStudentClockAttendance
                               WHERE  StuEnrollId = t1.StuEnrollId
                                      AND RecordDate <= t1.RecordDate
                                      AND (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999, 9999 )
                                          AND t1.ActualHours NOT IN ( 999, 9999 )
                                          )
                               ) AS ActualRunningScheduledHours
                              ,(
                               SELECT SUM(ActualHours)
                               FROM   arStudentClockAttendance
                               WHERE  StuEnrollId = t1.StuEnrollId
                                      AND RecordDate <= t1.RecordDate
                                      AND (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999, 9999 )
                                          )
                                      AND ActualHours >= 1
                                      AND ActualHours NOT IN ( 999, 9999 )
                               ) AS ActualRunningPresentHours
                              ,(
                               SELECT COUNT(ActualHours)
                               FROM   arStudentClockAttendance
                               WHERE  StuEnrollId = t1.StuEnrollId
                                      AND RecordDate <= t1.RecordDate
                                      AND (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999, 9999 )
                                          )
                                      AND ActualHours = 0
                                      AND ActualHours NOT IN ( 999, 9999 )
                               ) AS ActualRunningAbsentHours
                              ,(
                               SELECT SUM(ActualHours)
                               FROM   arStudentClockAttendance
                               WHERE  StuEnrollId = t1.StuEnrollId
                                      AND RecordDate <= t1.RecordDate
                                      AND SchedHours = 0
                                      AND ActualHours >= 1
                                      AND ActualHours NOT IN ( 999, 9999 )
                               ) AS ActualRunningMakeupHours
                              ,(
                               SELECT SUM(SchedHours - ActualHours)
                               FROM   arStudentClockAttendance
                               WHERE  StuEnrollId = t1.StuEnrollId
                                      AND RecordDate <= t1.RecordDate
                                      AND (
                                          (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999, 9999 )
                                          )
                                          AND ActualHours >= 1
                                          AND ActualHours NOT IN ( 999, 9999 )
                                          )
                                      AND isTardy = 1
                               ) AS ActualRunningTardyHours
                              ,t3.TrackTardies
                              ,t3.TardiesMakingAbsence
                              ,t3.PrgVerId
                              ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
                    FROM       arStudentClockAttendance t1
                    INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                    INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                    INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                    --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                    WHERE      AAUT1.UnitTypeDescrip IN ( ''Minutes'' )
                               AND t2.StuEnrollId = @StuEnrollId
                               AND t1.ActualHours <> 9999.00
                    ORDER BY   t1.StuEnrollId
                              ,MeetDate;
                OPEN GetAttendance_Cursor;
                --Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@ClsSectionId
                    ,@MeetDate
                    ,@Actual
                    ,@ScheduledMinutes
                    ,@Absent
                    ,@IsTardy
                    ,@ActualRunningScheduledHours
                    ,@ActualRunningPresentHours
                    ,@ActualRunningAbsentHours
                    ,@ActualRunningMakeupHours
                    ,@ActualRunningTardyHours
                    ,@tracktardies
                    ,@TardiesMakingAbsence
                    ,@PrgVerId
                    ,@rownumber;

                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                            END;

                        IF (
                           @ScheduledMinutes >= 1
                           AND (
                               @Actual <> 9999
                               AND @Actual <> 999
                               )
                           AND (
                               @ScheduledMinutes <> 9999
                               AND @Actual <> 999
                               )
                           )
                            BEGIN
                                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + ISNULL(@ScheduledMinutes, 0);
                            END;
                        ELSE
                            BEGIN
                                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0);
                            END;

                        IF (
                           @Actual <> 9999
                           AND @Actual <> 999
                           )
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                            END;
                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;

                        IF (
                           @Actual > 0
                           AND @Actual < @ScheduledMinutes
                           )
                            BEGIN
                                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                            END;
                        -- Make up hours
                        --sched=5, Actual =7, makeup= 2,ab = 0
                        --sched=0, Actual =7, makeup= 7,ab = 0
                        IF (
                           @Actual > 0
                           AND @ScheduledMinutes > 0
                           AND @Actual > @ScheduledMinutes
                           AND (
                               @Actual <> 9999
                               AND @Actual <> 999
                               )
                           )
                            BEGIN
                                SET @ActualRunningMakeupHours = @ActualRunningMakeupHours + ( @Actual - @ScheduledMinutes );
                            END;


                        IF (
                           @Actual <> 9999
                           AND @Actual <> 999
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                            END;
                        IF (
                           @Actual = 0
                           AND @ScheduledMinutes >= 1
                           AND @ScheduledMinutes NOT IN ( 999, 9999 )
                           )
                            BEGIN
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                            END;
                        -- Absent hours
                        --1. sched = 5, Actual = 2 then Ab = (5-3) = 2
                        IF (
                           @Absent = 0
                           AND @ActualRunningAbsentHours > 0
                           AND ( @Actual < @ScheduledMinutes )
                           )
                            BEGIN
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                            END;
                        IF (
                           @Actual > 0
                           AND @Actual < @ScheduledMinutes
                           AND (
                               @Actual <> 9999.00
                               AND @Actual <> 999.00
                               )
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                            END;

                        IF @tracktardies = 1
                           AND (
                               @TardyMinutes > 0
                               OR @IsTardy = 1
                               )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;
                        IF (
                           @tracktardies = 1
                           AND @TardiesMakingAbsence > 0
                           AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Actual; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;
                        DELETE FROM syStudentAttendanceSummary
                        WHERE StuEnrollId = @StuEnrollId
                              AND StudentAttendedDate = @MeetDate;
                        INSERT INTO syStudentAttendanceSummary (
                                                               StuEnrollId
                                                              ,ClsSectionId
                                                              ,StudentAttendedDate
                                                              ,ScheduledDays
                                                              ,ActualDays
                                                              ,ActualRunningScheduledDays
                                                              ,ActualRunningPresentDays
                                                              ,ActualRunningAbsentDays
                                                              ,ActualRunningMakeupDays
                                                              ,ActualRunningTardyDays
                                                              ,AdjustedPresentDays
                                                              ,AdjustedAbsentDays
                                                              ,AttendanceTrackType
                                                              ,ModUser
                                                              ,ModDate
                                                               )
                        VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                                ,@ActualRunningAbsentHours, @ActualRunningMakeupHours, @ActualRunningTardyHours, @AdjustedRunningPresentHours
                                ,@AdjustedRunningAbsentHours, ''Post Attendance by Class'', ''sa'', GETDATE());

                        UPDATE syStudentAttendanceSummary
                        SET    tardiesmakingabsence = @TardiesMakingAbsence
                        WHERE  StuEnrollId = @StuEnrollId;
                        --end
                        SET @PrevStuEnrollId = @StuEnrollId;
                        FETCH NEXT FROM GetAttendance_Cursor
                        INTO @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@Actual
                            ,@ScheduledMinutes
                            ,@Absent
                            ,@IsTardy
                            ,@ActualRunningScheduledHours
                            ,@ActualRunningPresentHours
                            ,@ActualRunningAbsentHours
                            ,@ActualRunningMakeupHours
                            ,@ActualRunningTardyHours
                            ,@tracktardies
                            ,@TardiesMakingAbsence
                            ,@PrgVerId
                            ,@rownumber;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;
            END;

        ---- PRINT ''Does it get to Clock Hour/PA'';
        ---- PRINT @UnitTypeId;
        ---- PRINT @TrackSapAttendance;
        -- By Day and PA, Clock Hour
        IF @UnitTypeDescrip IN ( ''present absent'', ''clock hours'' )
           AND @TrackSapAttendance = ''byday''
            BEGIN
                ---- PRINT ''By Day inside day'';
                DECLARE GetAttendance_Cursor CURSOR FOR
                    SELECT     t1.StuEnrollId
                              ,t1.RecordDate
                              ,t1.ActualHours
                              ,t1.SchedHours
                              ,CASE WHEN (
                                         (
                                         t1.SchedHours >= 1
                                         AND t1.SchedHours NOT IN ( 999, 9999 )
                                         )
                                         AND t1.ActualHours = 0
                                         ) THEN t1.SchedHours
                                    ELSE 0
                               END AS Absent
                              ,t1.isTardy
                              ,t3.TrackTardies
                              ,t3.TardiesMakingAbsence
                    FROM       arStudentClockAttendance t1
                    INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                    INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                    INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                    --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                    WHERE -- Unit Types: Present Absent and Clock Hour
                               AAUT1.UnitTypeDescrip IN ( ''present absent'', ''clock hours'' )
                               AND ActualHours <> 9999.00
                               AND t2.StuEnrollId = @StuEnrollId
                    ORDER BY   t1.StuEnrollId
                              ,t1.RecordDate;
                OPEN GetAttendance_Cursor;
                --Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@MeetDate
                    ,@Actual
                    ,@ScheduledMinutes
                    ,@Absent
                    ,@IsTardy
                    ,@tracktardies
                    ,@TardiesMakingAbsence;

                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                SET @MakeupHours = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                                SET @MakeupHours = 0;
                            END;

                        IF (
                           @Actual <> 9999
                           AND @Actual <> 999
                           )
                            BEGIN
                                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + @ScheduledMinutes;
                                SET @ActualRunningPresentHours = ISNULL(@ActualRunningPresentHours, 0) + @Actual;
                                SET @AdjustedRunningPresentHours = ISNULL(@AdjustedRunningPresentHours, 0) + @Actual;
                            END;
                        SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + @Absent;
                        IF (
                           @Actual = 0
                           AND @ScheduledMinutes >= 1
                           AND @ScheduledMinutes NOT IN ( 999, 9999 )
                           )
                            BEGIN
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                            END;

                        -- NWH 
                        -- If Scheduled = 3.0 hrs and Actual = 1.0 Then 2 hrs is considered absent
                        IF (
                           @ScheduledMinutes >= 1
                           AND @ScheduledMinutes NOT IN ( 999, 9999 )
                           AND @Actual > 0
                           AND ( @Actual < @ScheduledMinutes )
                           )
                            BEGIN
                                SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                                SET @AdjustedRunningAbsentHours = ISNULL(@AdjustedRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                            END;

                        IF (
                           @tracktardies = 1
                           AND @IsTardy = 1
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + 1;
                            END;
                        --commented by balaji on 10/22/2012 as report (rdl) doesn''t add days attended and make up days
                        ---- If there are make up hrs deduct that otherwise it will be added again in progress report
                        IF (
                           @Actual > 0
                           AND @Actual > @ScheduledMinutes
                           AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                            END;

                        IF @tracktardies = 1
                           AND (
                               @TardyMinutes > 0
                               OR @IsTardy = 1
                               )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;



                        -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                        IF (
                           @Actual > 0
                           AND @Actual > @ScheduledMinutes
                           AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                            END;

                        IF (
                           @tracktardies = 1
                           AND @TardiesMakingAbsence > 0
                           AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;

                        DELETE FROM syStudentAttendanceSummary
                        WHERE StuEnrollId = @StuEnrollId
                              AND StudentAttendedDate = @MeetDate;
                        INSERT INTO syStudentAttendanceSummary (
                                                               StuEnrollId
                                                              ,ClsSectionId
                                                              ,StudentAttendedDate
                                                              ,ScheduledDays
                                                              ,ActualDays
                                                              ,ActualRunningScheduledDays
                                                              ,ActualRunningPresentDays
                                                              ,ActualRunningAbsentDays
                                                              ,ActualRunningMakeupDays
                                                              ,ActualRunningTardyDays
                                                              ,AdjustedPresentDays
                                                              ,AdjustedAbsentDays
                                                              ,AttendanceTrackType
                                                              ,ModUser
                                                              ,ModDate
                                                              ,tardiesmakingabsence
                                                               )
                        VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, ISNULL(@ScheduledMinutes, 0), @Actual, ISNULL(@ActualRunningScheduledDays, 0)
                                ,ISNULL(@ActualRunningPresentHours, 0), ISNULL(@ActualRunningAbsentHours, 0), ISNULL(@MakeupHours, 0)
                                ,ISNULL(@ActualRunningTardyHours, 0), ISNULL(@AdjustedRunningPresentHours, 0), ISNULL(@AdjustedRunningAbsentHours, 0)
                                ,''Post Attendance by Class'', ''sa'', GETDATE(), @TardiesMakingAbsence );

                        --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                        --end
                        SET @PrevStuEnrollId = @StuEnrollId;
                        FETCH NEXT FROM GetAttendance_Cursor
                        INTO @StuEnrollId
                            ,@MeetDate
                            ,@Actual
                            ,@ScheduledMinutes
                            ,@Absent
                            ,@IsTardy
                            ,@tracktardies
                            ,@TardiesMakingAbsence;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;
            END;
        --end

        -- Based on grade rounding round the final score and current score
        --Declare @PrevTermId uniqueidentifier,@PrevReqId uniqueidentifier,@RowCount int,@FinalScore decimal(18,4),@CurrentScore decimal(18,4)
        --declare @ReqId uniqueidentifier,@CourseCodeDescrip varchar(100),@FinalGrade varchar(10),@sysComponentTypeId int
        --declare @CreditsAttempted decimal(18,4),@Grade varchar(10),@IsPass bit,@IsCreditsAttempted bit,@IsCreditsEarned bit
        --declare @IsInGPA bit,@FinAidCredits decimal(18,4),@CurrentGrade varchar(10),@CourseCredits decimal(18,4)
        DECLARE @GPA DECIMAL(18, 4);

        DECLARE @PrevReqId UNIQUEIDENTIFIER
               ,@PrevTermId UNIQUEIDENTIFIER
               ,@CreditsAttempted DECIMAL(18, 2)
               ,@CreditsEarned DECIMAL(18, 2);
        DECLARE @reqid UNIQUEIDENTIFIER
               ,@CourseCodeDescrip VARCHAR(50)
               ,@FinalGrade VARCHAR(50)
               ,@FinalScore DECIMAL(18, 2)
               ,@Grade VARCHAR(50);
        DECLARE @IsPass BIT
               ,@IsCreditsAttempted BIT
               ,@IsCreditsEarned BIT
               ,@CurrentScore DECIMAL(18, 2)
               ,@CurrentGrade VARCHAR(10)
               ,@FinalGPA DECIMAL(18, 2);
        DECLARE @sysComponentTypeId INT
               ,@RowCount INT;
        DECLARE @IsInGPA BIT;
        DECLARE @CourseCredits DECIMAL(18, 2);
        DECLARE @FinAidCreditsEarned DECIMAL(18, 2)
               ,@FinAidCredits DECIMAL(18, 2);




        DECLARE GetCreditsSummary_Cursor CURSOR FOR
            SELECT     DISTINCT SE.StuEnrollId
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate
                               ,R.ReqId
                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                               ,RES.Score AS FinalScore
                               ,RES.GrdSysDetailId AS FinalGrade
                               ,GCT.SysComponentTypeId
                               ,R.Credits AS CreditsAttempted
                               ,CS.ClsSectionId
                               ,GSD.Grade
                               ,GSD.IsPass
                               ,GSD.IsCreditsAttempted
                               ,GSD.IsCreditsEarned
                               ,SE.PrgVerId
                               ,GSD.IsInGPA
                               ,R.FinAidCredits AS FinAidCredits
            FROM       arStuEnrollments SE
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
            INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
            INNER JOIN arTerm T ON CS.TermId = T.TermId
            INNER JOIN arReqs R ON CS.ReqId = R.ReqId
            LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
            LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
            LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
            LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            WHERE      SE.StuEnrollId = @StuEnrollId
            ORDER BY   T.StartDate
                      ,T.TermDescrip
                      ,R.ReqId;

        DECLARE @varGradeRounding VARCHAR(3);
        DECLARE @roundfinalscore DECIMAL(18, 4);
        SET @varGradeRounding = (
                                SELECT Value
                                FROM   syConfigAppSetValues
                                WHERE  SettingId = 45
                                );

        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor
        INTO @StuEnrollId
            ,@TermId
            ,@TermDescrip
            ,@termstartdate1
            ,@reqid
            ,@CourseCodeDescrip
            ,@FinalScore
            ,@FinalGrade
            ,@sysComponentTypeId
            ,@CreditsAttempted
            ,@ClsSectionId
            ,@Grade
            ,@IsPass
            ,@IsCreditsAttempted
            ,@IsCreditsEarned
            ,@PrgVerId
            ,@IsInGPA
            ,@FinAidCredits;
        WHILE @@FETCH_STATUS = 0
            BEGIN


                SET @CurrentScore = (
                                    SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                ELSE NULL
                                           END AS CurrentScore
                                    FROM   (
                                           SELECT   InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight AS GradeBookWeight
                                                   ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                         ELSE 0
                                                    END AS ActualWeight
                                           FROM     (
                                                    SELECT   C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,ISNULL(C.Weight, 0) AS Weight
                                                            ,C.Number AS MinNumber
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter AS Param
                                                            ,X.GrdScaleId
                                                            ,SUM(GR.Score) AS Score
                                                            ,COUNT(D.Descrip) AS NumberOfComponents
                                                    FROM     (
                                                             SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                    ,A.EffectiveDate
                                                                                    ,B.GrdScaleId
                                                             FROM     arGrdBkWeights A
                                                                     ,arClassSections B
                                                             WHERE    A.ReqId = B.ReqId
                                                                      AND A.EffectiveDate <= B.StartDate
                                                                      AND B.ClsSectionId = @ClsSectionId
                                                             ORDER BY A.EffectiveDate DESC
                                                             ) X
                                                            ,arGrdBkWgtDetails C
                                                            ,arGrdComponentTypes D
                                                            ,arGrdBkResults GR
                                                    WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                             AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                             AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                             AND D.SysComponentTypeId NOT IN ( 500, 503 )
                                                             AND GR.StuEnrollId = @StuEnrollId
                                                             AND GR.ClsSectionId = @ClsSectionId
                                                             AND GR.Score IS NOT NULL
                                                    GROUP BY C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,C.Weight
                                                            ,C.Number
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter
                                                            ,X.GrdScaleId
                                                    ) S
                                           GROUP BY InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight
                                                   ,NumberOfComponents
                                           ) FinalTblToComputeCurrentScore
                                    );
                IF ( @CurrentScore IS NULL )
                    BEGIN
                        -- instructor grade books
                        SET @CurrentScore = (
                                            SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                        ELSE NULL
                                                   END AS CurrentScore
                                            FROM   (
                                                   SELECT   InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                   FROM     (
                                                            SELECT   C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,ISNULL(C.Weight, 0) AS Weight
                                                                    ,C.Number AS MinNumber
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter AS Param
                                                                    ,X.GrdScaleId
                                                                    ,SUM(GR.Score) AS Score
                                                                    ,COUNT(D.Descrip) AS NumberOfComponents
                                                            FROM     (
                                                                     --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
                                                                     --FROM          arGrdBkWeights A,arClassSections B        
                                                                     --WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
                                                                     --ORDER BY      A.EffectiveDate DESC
                                                                     SELECT DISTINCT TOP 1 t1.InstrGrdBkWgtId
                                                                                          ,t1.GrdScaleId
                                                                     FROM   arClassSections t1
                                                                           ,arGrdBkWeights t2
                                                                     WHERE  t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                            AND t1.ClsSectionId = @ClsSectionId
                                                                     ) X
                                                                    ,arGrdBkWgtDetails C
                                                                    ,arGrdComponentTypes D
                                                                    ,arGrdBkResults GR
                                                            WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                     AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                     AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                     AND
                                                                -- D.SysComponentTypeID not in (500,503) and 
                                                                GR.StuEnrollId = @StuEnrollId
                                                                     AND GR.ClsSectionId = @ClsSectionId
                                                                     AND GR.Score IS NOT NULL
                                                            GROUP BY C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,C.Weight
                                                                    ,C.Number
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter
                                                                    ,X.GrdScaleId
                                                            ) S
                                                   GROUP BY InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                   ) FinalTblToComputeCurrentScore
                                            );

                    END;


                -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                IF ( LOWER(@varGradeRounding) = ''yes'' )
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @FinalScore = ROUND(@FinalScore, 0);
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;
                ELSE
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;

                UPDATE syCreditSummary
                SET    FinalScore = @FinalScore
                      ,CurrentScore = @CurrentScore
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId
                       AND ReqId = @reqid;

                --Average calculation

                -- Term Average
                SET @TermAverageCount = (
                                        SELECT COUNT(*)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND TermId = @TermId
                                               AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                      SELECT SUM(FinalScore)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount;

                -- Cumulative Average
                SET @cumAveragecount = (
                                       SELECT COUNT(*)
                                       FROM   syCreditSummary
                                       WHERE  StuEnrollId = @StuEnrollId
                                              AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                     SELECT SUM(FinalScore)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount;

                UPDATE syCreditSummary
                SET    Average = @TermAverage
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId;

                --Update Cumulative GPA
                UPDATE syCreditSummary
                SET    CumAverage = @CumAverage
                WHERE  StuEnrollId = @StuEnrollId;


                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@FinalGrade
                    ,@sysComponentTypeId
                    ,@CreditsAttempted
                    ,@ClsSectionId
                    ,@Grade
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;


        DECLARE GetCreditsSummary_Cursor CURSOR FOR
            SELECT     DISTINCT SE.StuEnrollId
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate
                               ,R.ReqId
                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                               ,RES.Score AS FinalScore
                               ,RES.GrdSysDetailId AS FinalGrade
                               ,GCT.SysComponentTypeId
                               ,R.Credits AS CreditsAttempted
                               ,CS.ClsSectionId
                               ,GSD.Grade
                               ,GSD.IsPass
                               ,GSD.IsCreditsAttempted
                               ,GSD.IsCreditsEarned
                               ,SE.PrgVerId
                               ,GSD.IsInGPA
                               ,R.FinAidCredits AS FinAidCredits
            FROM       arStuEnrollments SE
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
            INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                             AND RES.TermId = CS.TermId
            INNER JOIN arTerm T ON CS.TermId = T.TermId
            INNER JOIN arReqs R ON CS.ReqId = R.ReqId
            LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
            LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
            LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
            LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            WHERE      SE.StuEnrollId = @StuEnrollId
            ORDER BY   T.StartDate
                      ,T.TermDescrip
                      ,R.ReqId;


        SET @varGradeRounding = (
                                SELECT Value
                                FROM   syConfigAppSetValues
                                WHERE  SettingId = 45
                                );

        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor
        INTO @StuEnrollId
            ,@TermId
            ,@TermDescrip
            ,@termstartdate1
            ,@reqid
            ,@CourseCodeDescrip
            ,@FinalScore
            ,@FinalGrade
            ,@sysComponentTypeId
            ,@CreditsAttempted
            ,@ClsSectionId
            ,@Grade
            ,@IsPass
            ,@IsCreditsAttempted
            ,@IsCreditsEarned
            ,@PrgVerId
            ,@IsInGPA
            ,@FinAidCredits;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                SET @CurrentScore = (
                                    SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                ELSE NULL
                                           END AS CurrentScore
                                    FROM   (
                                           SELECT   InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight AS GradeBookWeight
                                                   ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                         ELSE 0
                                                    END AS ActualWeight
                                           FROM     (
                                                    SELECT   C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,ISNULL(C.Weight, 0) AS Weight
                                                            ,C.Number AS MinNumber
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter AS Param
                                                            ,X.GrdScaleId
                                                            ,SUM(GR.Score) AS Score
                                                            ,COUNT(D.Descrip) AS NumberOfComponents
                                                    FROM     (
                                                             SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                    ,A.EffectiveDate
                                                                                    ,B.GrdScaleId
                                                             FROM     arGrdBkWeights A
                                                                     ,arClassSections B
                                                             WHERE    A.ReqId = B.ReqId
                                                                      AND A.EffectiveDate <= B.StartDate
                                                                      AND B.ClsSectionId = @ClsSectionId
                                                             ORDER BY A.EffectiveDate DESC
                                                             ) X
                                                            ,arGrdBkWgtDetails C
                                                            ,arGrdComponentTypes D
                                                            ,arGrdBkResults GR
                                                    WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                             AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                             AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                             AND D.SysComponentTypeId NOT IN ( 500, 503 )
                                                             AND GR.StuEnrollId = @StuEnrollId
                                                             AND GR.ClsSectionId = @ClsSectionId
                                                             AND GR.Score IS NOT NULL
                                                    GROUP BY C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,C.Weight
                                                            ,C.Number
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter
                                                            ,X.GrdScaleId
                                                    ) S
                                           GROUP BY InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight
                                                   ,NumberOfComponents
                                           ) FinalTblToComputeCurrentScore
                                    );
                IF ( @CurrentScore IS NULL )
                    BEGIN
                        -- instructor grade books
                        SET @CurrentScore = (
                                            SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                        ELSE NULL
                                                   END AS CurrentScore
                                            FROM   (
                                                   SELECT   InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                   FROM     (
                                                            SELECT   C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,ISNULL(C.Weight, 0) AS Weight
                                                                    ,C.Number AS MinNumber
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter AS Param
                                                                    ,X.GrdScaleId
                                                                    ,SUM(GR.Score) AS Score
                                                                    ,COUNT(D.Descrip) AS NumberOfComponents
                                                            FROM     (
                                                                     --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
                                                                     --FROM          arGrdBkWeights A,arClassSections B        
                                                                     --WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
                                                                     --ORDER BY      A.EffectiveDate DESC
                                                                     SELECT DISTINCT TOP 1 t1.InstrGrdBkWgtId
                                                                                          ,t1.GrdScaleId
                                                                     FROM   arClassSections t1
                                                                           ,arGrdBkWeights t2
                                                                     WHERE  t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                            AND t1.ClsSectionId = @ClsSectionId
                                                                     ) X
                                                                    ,arGrdBkWgtDetails C
                                                                    ,arGrdComponentTypes D
                                                                    ,arGrdBkResults GR
                                                            WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                     AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                     AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                     AND
                                                                -- D.SysComponentTypeID not in (500,503) and 
                                                                GR.StuEnrollId = @StuEnrollId
                                                                     AND GR.ClsSectionId = @ClsSectionId
                                                                     AND GR.Score IS NOT NULL
                                                            GROUP BY C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,C.Weight
                                                                    ,C.Number
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter
                                                                    ,X.GrdScaleId
                                                            ) S
                                                   GROUP BY InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                   ) FinalTblToComputeCurrentScore
                                            );

                    END;

                -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                IF ( LOWER(@varGradeRounding) = ''yes'' )
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @FinalScore = ROUND(@FinalScore, 0);
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;
                ELSE
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;

                UPDATE syCreditSummary
                SET    FinalScore = @FinalScore
                      ,CurrentScore = @CurrentScore
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId
                       AND ReqId = @reqid;

                --Average calculation
                --			declare @CumAverage decimal(18,2),@cumAverageSum decimal(18,2),@cumAveragecount int

                -- Term Average
                SET @TermAverageCount = (
                                        SELECT COUNT(*)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND TermId = @TermId
                                               AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                      SELECT SUM(FinalScore)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount;

                -- Cumulative Average
                SET @cumAveragecount = (
                                       SELECT COUNT(*)
                                       FROM   syCreditSummary
                                       WHERE  StuEnrollId = @StuEnrollId
                                              AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                     SELECT SUM(FinalScore)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount;

                UPDATE syCreditSummary
                SET    Average = @TermAverage
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId;

                --Update Cumulative GPA
                UPDATE syCreditSummary
                SET    CumAverage = @CumAverage
                WHERE  StuEnrollId = @StuEnrollId;


                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@FinalGrade
                    ,@sysComponentTypeId
                    ,@CreditsAttempted
                    ,@ClsSectionId
                    ,@Grade
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;

        DECLARE @GradeSystemDetailId UNIQUEIDENTIFIER
               ,@IsGPA BIT;
        DECLARE GetCreditsSummary_Cursor CURSOR FOR
            SELECT     DISTINCT SE.StuEnrollId
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate
                               ,R.ReqId
                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                               ,NULL AS FinalScore
                               ,RES.GrdSysDetailId AS GradeSystemDetailId
                               ,GSD.Grade AS FinalGrade
                               ,GSD.Grade AS CurrentGrade
                               ,R.Credits
                               ,NULL AS ClsSectionId
                               ,GSD.IsPass
                               ,GSD.IsCreditsAttempted
                               ,GSD.IsCreditsEarned
                               ,GSD.GPA
                               ,GSD.IsInGPA
                               ,SE.PrgVerId
                               ,GSD.IsInGPA
                               ,R.FinAidCredits AS FinAidCredits
            FROM       arStuEnrollments SE
            INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
            INNER JOIN syCreditSummary CS ON CS.StuEnrollId = RES.StuEnrollId
            INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            INNER JOIN arReqs R ON RES.ReqId = R.ReqId
            INNER JOIN arTerm T ON RES.TermId = T.TermId
            WHERE      RES.ReqId NOT IN (
                                        SELECT DISTINCT ReqId
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = SE.StuEnrollId
                                               AND TermId = T.TermId
                                        )
                       AND SE.StuEnrollId = @StuEnrollId;

        SET @varGradeRounding = (
                                SELECT Value
                                FROM   syConfigAppSetValues
                                WHERE  SettingId = 45
                                );

        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor
        INTO @StuEnrollId
            ,@TermId
            ,@TermDescrip
            ,@termstartdate1
            ,@reqid
            ,@CourseCodeDescrip
            ,@FinalScore
            ,@GradeSystemDetailId
            ,@FinalGrade
            ,@CurrentGrade
            ,@CourseCredits
            ,@ClsSectionId
            ,@IsPass
            ,@IsCreditsAttempted
            ,@IsCreditsEarned
            ,@GPA
            ,@IsGPA
            ,@PrgVerId
            ,@IsInGPA
            ,@FinAidCredits;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                IF ( LOWER(@varGradeRounding) = ''yes'' )
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @FinalScore = ROUND(@FinalScore, 0);
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;
                ELSE
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;

                UPDATE syCreditSummary
                SET    FinalScore = @FinalScore
                      ,CurrentScore = @CurrentScore
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId
                       AND ReqId = @reqid;

                --Average calculation

                -- Term Average
                SET @TermAverageCount = (
                                        SELECT COUNT(*)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND TermId = @TermId
                                               AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                      SELECT SUM(FinalScore)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount;

                -- Cumulative Average
                SET @cumAveragecount = (
                                       SELECT COUNT(*)
                                       FROM   syCreditSummary
                                       WHERE  StuEnrollId = @StuEnrollId
                                              AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                     SELECT SUM(FinalScore)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount;

                UPDATE syCreditSummary
                SET    Average = @TermAverage
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId;

                --Update Cumulative GPA
                UPDATE syCreditSummary
                SET    CumAverage = @CumAverage
                WHERE  StuEnrollId = @StuEnrollId;


                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@GradeSystemDetailId
                    ,@FinalGrade
                    ,@CurrentGrade
                    ,@CourseCredits
                    ,@ClsSectionId
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@GPA
                    ,@IsGPA
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;

        DECLARE GetCreditsSummary_Cursor CURSOR FOR
            SELECT     DISTINCT SE.StuEnrollId
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate
                               ,R.ReqId
                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                               ,NULL AS FinalScore
                               ,RES.GrdSysDetailId AS GradeSystemDetailId
                               ,GSD.Grade AS FinalGrade
                               ,GSD.Grade AS CurrentGrade
                               ,R.Credits
                               ,NULL AS ClsSectionId
                               ,GSD.IsPass
                               ,GSD.IsCreditsAttempted
                               ,GSD.IsCreditsEarned
                               ,GSD.GPA
                               ,GSD.IsInGPA
                               ,SE.PrgVerId
                               ,GSD.IsInGPA
                               ,R.FinAidCredits AS FinAidCredits
            FROM       arStuEnrollments SE
            INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
            INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            INNER JOIN arReqs R ON RES.ReqId = R.ReqId
            INNER JOIN arTerm T ON RES.TermId = T.TermId
            WHERE      SE.StuEnrollId NOT IN (
                                             SELECT DISTINCT StuEnrollId
                                             FROM   syCreditSummary
                                             )
                       AND SE.StuEnrollId = @StuEnrollId;

        SET @varGradeRounding = (
                                SELECT Value
                                FROM   syConfigAppSetValues
                                WHERE  SettingId = 45
                                );

        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor
        INTO @StuEnrollId
            ,@TermId
            ,@TermDescrip
            ,@termstartdate1
            ,@reqid
            ,@CourseCodeDescrip
            ,@FinalScore
            ,@GradeSystemDetailId
            ,@FinalGrade
            ,@CurrentGrade
            ,@CourseCredits
            ,@ClsSectionId
            ,@IsPass
            ,@IsCreditsAttempted
            ,@IsCreditsEarned
            ,@GPA
            ,@IsGPA
            ,@PrgVerId
            ,@IsInGPA
            ,@FinAidCredits;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                IF ( LOWER(@varGradeRounding) = ''yes'' )
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @FinalScore = ROUND(@FinalScore, 0);
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;
                ELSE
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;

                UPDATE syCreditSummary
                SET    FinalScore = @FinalScore
                      ,CurrentScore = @CurrentScore
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId
                       AND ReqId = @reqid;

                --Average calculation
                SET @TermAverageCount = (
                                        SELECT COUNT(*)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND TermId = @TermId
                                               AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                      SELECT SUM(FinalScore)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount;

                -- Cumulative Average
                SET @cumAveragecount = (
                                       SELECT COUNT(*)
                                       FROM   syCreditSummary
                                       WHERE  StuEnrollId = @StuEnrollId
                                              AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                     SELECT SUM(FinalScore)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount;

                UPDATE syCreditSummary
                SET    Average = @TermAverage
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId;

                --Update Cumulative GPA
                UPDATE syCreditSummary
                SET    CumAverage = @CumAverage
                WHERE  StuEnrollId = @StuEnrollId;


                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@GradeSystemDetailId
                    ,@FinalGrade
                    ,@CurrentGrade
                    ,@CourseCredits
                    ,@ClsSectionId
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@GPA
                    ,@IsGPA
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;


        SET @TermAverageCount = (
                                SELECT COUNT(*)
                                FROM   syCreditSummary
                                WHERE  StuEnrollId = @StuEnrollId
                                       AND FinalScore IS NOT NULL
                                );
        SET @termAverageSum = (
                              SELECT SUM(FinalScore)
                              FROM   syCreditSummary
                              WHERE  StuEnrollId = @StuEnrollId
                                     AND FinalScore IS NOT NULL
                              );
        IF @TermAverageCount >= 1
            BEGIN
                SET @TermAverage = @termAverageSum / @TermAverageCount;
            END;

        -- Cumulative Average
        SET @cumAveragecount = (
                               SELECT COUNT(*)
                               FROM   syCreditSummary
                               WHERE  StuEnrollId = @StuEnrollId
                                      AND FinalScore IS NOT NULL
                               );
        ---- PRINT @cumAveragecount

        SET @cumAverageSum = (
                             SELECT SUM(FinalScore)
                             FROM   syCreditSummary
                             WHERE  StuEnrollId = @StuEnrollId
                                    AND FinalScore IS NOT NULL
                             );
        ---- PRINT @CumAverageSum

        IF @cumAveragecount >= 1
            BEGIN
                SET @CumAverage = @cumAverageSum / @cumAveragecount;
            END;
        ---- PRINT @CumAverage

        ---- PRINT @UnitTypeId;

        ---- PRINT @TrackSapAttendance;
        ---- PRINT @displayHours;

        DECLARE @UseWeightForGPA INT = 0;
        SET @UseWeightForGPA = (
                               SELECT TOP 1 PV.DoCourseWeightOverallGPA
                               FROM   dbo.arStuEnrollments E
                               JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                               WHERE  E.StuEnrollId = @StuEnrollId
                               );

        IF @UseWeightForGPA = 1
            BEGIN
                DECLARE @TotalWeight DECIMAL = (
                                               SELECT SUM(PVD.CourseWeight)
                                               FROM   dbo.syCreditSummary CS
                                               JOIN   dbo.arProgVerDef PVD ON PVD.ReqId = CS.ReqId
                                               WHERE  StuEnrollId = @StuEnrollId
                                               );

                DECLARE @ExpectedCoursesGraded INT = (
                                                     SELECT COUNT(CS.ReqId)
                                                     FROM   dbo.syCreditSummary CS
                                                     JOIN   dbo.arProgVerDef PVD ON PVD.ReqId = CS.ReqId
                                                     WHERE  StuEnrollId = @StuEnrollId
                                                     );
                DECLARE @CoursesGraded INT = (
                                             SELECT COUNT(CS.ReqId)
                                             FROM   dbo.syCreditSummary CS
                                             JOIN   dbo.arProgVerDef PVD ON PVD.ReqId = CS.ReqId
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND CS.CurrentScore IS NOT NULL
                                                    AND CS.Completed = 1
                                             );

                SET @CumAverage = CASE WHEN @TotalWeight = 100
                                            AND @ExpectedCoursesGraded = @CoursesGraded THEN (
                                                                                             SELECT SUM(PVD.CourseWeight / 100 * CS.CurrentScore)
                                                                                             FROM   dbo.syCreditSummary CS
                                                                                             JOIN   dbo.arProgVerDef PVD ON PVD.ReqId = CS.ReqId
                                                                                             WHERE  StuEnrollId = @StuEnrollId
                                                                                             )
                                       ELSE ((
                                             SELECT SUM(CS.CurrentScore)
                                             FROM   dbo.syCreditSummary CS
                                             JOIN   dbo.arProgVerDef PVD ON PVD.ReqId = CS.ReqId
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND CS.CurrentScore IS NOT NULL
                                             ) / (
                                                 SELECT COUNT(CS.CurrentScore)
                                                 FROM   dbo.syCreditSummary CS
                                                 JOIN   dbo.arProgVerDef PVD ON PVD.ReqId = CS.ReqId
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND CS.CurrentScore IS NOT NULL
                                                 )
                                            )
                                  END;
            END;

		--If Clock Hour / Numeric - use New Average Calculation
		IF (
		   SELECT TOP 1 P.ACId
		   FROM   dbo.arStuEnrollments E
		   JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
		   JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
		   WHERE  E.StuEnrollId = @StuEnrollId
		   ) = 5
		   AND @GradesFormat = ''numeric''
			BEGIN
				EXEC dbo.USP_GPACalculator @EnrollmentId = @StuEnrollId
                                          ,@StudentGPA = @CumAverage OUTPUT;
			END;

        -- Main query starts here 
        SELECT     DISTINCT TOP 1 S.SSN
                                 ,S.FirstName
                                 ,S.LastName
                                 ,SC.StatusCodeDescrip
                                 ,SE.StartDate AS StudentStartDate
                                 ,SE.ExpGradDate AS StudentExpectedGraduationDate
                                 ,PV.PrgVerId AS PrgVerId
                                 ,PV.PrgVerDescrip AS PrgVerDescrip
                                 ,PV.UnitTypeId AS UnitTypeId
                                 ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                       ELSE 0
                                  END AS PrgVersionTrackCredits
                                 ,AAUT1.UnitTypeDescrip AS UnitTypeDescripFrom_arAttUnitType
                                 ,CASE WHEN @TermStartDate IS NULL THEN (
                                                                        SELECT MAX(ISNULL(LDA,''1753-01-01 00:00:00.0''))
                                                                        FROM   (
                                                                               SELECT MAX(AttendedDate) AS LDA
                                                                               FROM   arExternshipAttendance
                                                                               WHERE  StuEnrollId = @StuEnrollId AND AttendedDate IS NOT NULL
                                                                               UNION ALL
                                                                               SELECT MAX(MeetDate) AS LDA
                                                                               FROM   atClsSectAttendance
                                                                               WHERE  StuEnrollId = @StuEnrollId
                                                                                      AND Actual > 0
                                                                                      AND Actual <> 99.00
                                                                                      AND Actual <> 999.00
                                                                                      AND Actual <> 9999.00 AND MeetDate IS NOT NULL
                                                                               UNION ALL
                                                                               --SELECT    MAX(AttendanceDate) AS LDA
                                                                               --FROM      atAttendance
                                                                               --WHERE     EnrollId = @StuEnrollId
                                                                               --          AND Actual > 0
                                                                               --UNION ALL
                                                                               SELECT MAX(RecordDate) AS LDA
                                                                               FROM   arStudentClockAttendance
                                                                               WHERE  StuEnrollId = @StuEnrollId
                                                                                      AND (
                                                                                          ActualHours > 0
                                                                                          AND ActualHours <> 99.00
                                                                                          AND ActualHours <> 999.00
                                                                                          AND ActualHours <> 9999.00
                                                                                          ) AND RecordDate IS NOT NULL
                                                                               UNION ALL
                                                                               SELECT MAX(MeetDate) AS LDA
                                                                               FROM   atConversionAttendance
                                                                               WHERE  StuEnrollId = @StuEnrollId
                                                                                      AND (
                                                                                          Actual > 0
                                                                                          AND Actual <> 99.00
                                                                                          AND Actual <> 999.00
                                                                                          AND Actual <> 9999.00
                                                                                          ) AND MeetDate IS NOT NULL
                                                                               UNION ALL
                                                                               SELECT TOP 1 LDA
                                                                               FROM   arStuEnrollments
                                                                               WHERE  StuEnrollId = @StuEnrollId AND LDA IS NOT NULL
                                                                               ) TR
                                                                        )
                                       ELSE
                                  -- Get the LDA Based on Term Start Date
                                  (
                                  SELECT MAX(ISNULL(LDA,''1753-01-01 00:00:00.0''))
                                  FROM   (
                                         SELECT TOP 1 LDA
                                         FROM   arStuEnrollments
                                         WHERE  StuEnrollId = @StuEnrollId AND LDA IS NOT NULL
                                         UNION ALL
                                         SELECT MAX(AttendedDate) AS LDA
                                         FROM   arExternshipAttendance
                                         WHERE  StuEnrollId = @StuEnrollId AND AttendedDate IS NOT NULL
                                         UNION ALL
                                         SELECT MAX(MeetDate) AS LDA
                                         FROM   atClsSectAttendance t1
                                               ,arClassSections t2
                                         WHERE  t1.ClsSectionId = t2.ClsSectionId
                                                AND StuEnrollId = @StuEnrollId
                                                AND Actual > 0
                                                AND Actual <> 99.00
                                                AND Actual <> 999.00
                                                AND Actual <> 9999.00 AND MeetDate IS NOT NULL
                                         UNION ALL
                                         SELECT MAX(RecordDate) AS LDA
                                         FROM   arStudentClockAttendance
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND (
                                                    ActualHours > 0
                                                    AND ActualHours <> 99.00
                                                    AND ActualHours <> 999.00
                                                    AND ActualHours <> 9999.00
                                                    ) AND RecordDate IS NOT NULL
                                         UNION ALL
                                         SELECT MAX(MeetDate) AS LDA
                                         FROM   atConversionAttendance
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND (
                                                    Actual > 0
                                                    AND Actual <> 99.00
                                                    AND Actual <> 999.00
                                                    AND Actual <> 9999.00
                                                    ) AND MeetDate IS NOT NULL
                                         ) TR
                                  )
                                  END AS StudentLastDateAttended
                                 ,( CASE WHEN ( AAUT1.UnitTypeId = ''2600592A-9739-4A13-BDCE-7A25FE4A7478'' ) -- NONE
                                 THEN        0.00
                                         ELSE (
                                              SELECT     SUM(ISNULL(APSD.total, 0))
                                              FROM       arProgScheduleDetails AS APSD
                                              INNER JOIN arProgSchedules AS APS ON APS.ScheduleId = APSD.ScheduleId
                                              INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = APS.PrgVerId
                                              INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                                              INNER JOIN dbo.arStudentSchedules SS ON SS.StuEnrollId = ASE.StuEnrollId
                                                                                      AND SS.ScheduleId = APSD.ScheduleId
                                              WHERE      ASE.StuEnrollId = @StuEnrollId
                                              )
                                    END
                                  ) AS WeeklySchedule
                                 ,CASE WHEN (
                                            @TrackSapAttendance = ''byclass''
                                            AND @displayHours = ''hours''
                                            AND AAUT1.UnitTypeDescrip = ''Present Absent''
                                            ) -- PA
                                 THEN      @ActualPresentDays_ConvertTo_Hours
                                       ELSE CASE WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AttendedDays / 60
                                                          ELSE SAS_ForTerm.AttendedDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.AttendedDays / 60 )
                                                          ELSE ( SAS_ForTerm.AttendedDays / 60 )
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.AttendedDays / 60 )
                                                          ELSE ( SAS_ForTerm.AttendedDays / 60 )
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AttendedDays / 60
                                                          ELSE SAS_ForTerm.AttendedDays / 60
                                                     END
                                                 ELSE CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AttendedDays
                                                           ELSE SAS_ForTerm.AttendedDays
                                                      END
                                            END
                                  END AS TotalDaysAttended
                                 ,CASE WHEN (
                                            @TrackSapAttendance = ''byclass''
                                            AND @displayHours = ''hours''
                                            AND AAUT1.UnitTypeDescrip = ''Present Absent''
                                            ) THEN @Scheduledhours
                                       ELSE CASE WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                                                          ELSE SAS_ForTerm.ScheduledDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                                                          ELSE SAS_ForTerm.ScheduledDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                                                          ELSE SAS_ForTerm.ScheduledDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                                                          ELSE SAS_ForTerm.ScheduledDays / 60
                                                     END
                                                 ELSE CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.ScheduledDays )
                                                           ELSE ( SAS_ForTerm.ScheduledDays )
                                                      END
                                            END
                                  END AS ScheduledDays
                                 ,CASE WHEN (
                                            @TrackSapAttendance = ''byclass''
                                            AND @displayHours = ''hours''
                                            AND AAUT1.UnitTypeDescrip = ''Present Absent''
                                            ) THEN @ActualAbsentDays_ConvertTo_Hours
                                       ELSE CASE WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AbsentDays / 60
                                                          ELSE SAS_ForTerm.AbsentDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AbsentDays / 60
                                                          ELSE SAS_ForTerm.AbsentDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AbsentDays / 60
                                                          ELSE SAS_ForTerm.AbsentDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AbsentDays / 60
                                                          ELSE SAS_ForTerm.AbsentDays / 60
                                                     END
                                                 ELSE CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.AbsentDays )
                                                           ELSE ( SAS_ForTerm.AbsentDays )
                                                      END
                                            END
                                  END AS DaysAbsent
                                 ,CASE WHEN (
                                            @TrackSapAttendance = ''byclass''
                                            AND LOWER(RTRIM(LTRIM(@displayHours))) = ''hours''
                                            AND AAUT1.UnitTypeDescrip = ''Present Absent''
                                            ) THEN SAS_ForTerm.MakeupDays
                                       ELSE CASE WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.MakeupDays / 60
                                                          ELSE SAS_ForTerm.MakeupDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.MakeupDays / 60
                                                          ELSE SAS_ForTerm.MakeupDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.MakeupDays / 60
                                                          ELSE SAS_ForTerm.MakeupDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.MakeupDays / 60
                                                          ELSE SAS_ForTerm.MakeupDays / 60
                                                     END
                                                 ELSE CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.MakeupDays )
                                                           ELSE ( SAS_ForTerm.MakeupDays )
                                                      END
                                            END
                                  END AS MakeupDays
                                 ,SE.StuEnrollId
                                 ,@SUMCreditsAttempted AS CreditsAttempted
                                 ,@SUMCreditsEarned AS CreditsEarned
                                 ,@SUMFACreditsEarned AS FACreditsEarned
                                 ,SE.DateDetermined AS DateDetermined
                                 ,SYS.InSchool
                                 ,P.ProgDescrip
                                 ,(
                                  SELECT SUM(ISNULL(TransAmount, 0))
                                  FROM   saTransactions t1
                                        ,saTransCodes t2
                                  WHERE  t1.TransCodeId = t2.TransCodeId
                                         AND t1.StuEnrollId = @StuEnrollId
                                         AND t2.IsInstCharge = 1
                                         AND t1.TransTypeId IN ( 0, 1 )
                                         AND t1.Voided = 0
                                  ) AS TotalCost
                                 ,(
                                  SELECT SUM(ISNULL(TransAmount, 0)) AS CurrentBalance
                                  FROM   saTransactions
                                  WHERE  StuEnrollId = @StuEnrollId
                                         AND Voided = 0
                                  ) AS CurrentBalance
                                 ,@CumAverage AS OverallAverage
                                 ,@cumWeightedGPA AS WeightedAverage_CumGPA
                                 ,@cumSimpleGPA AS SimpleAverage_CumGPA
                                 ,@ReturnValue AS StudentGroups
                                 ,CASE WHEN P.ACId = 5 THEN ''True''
                                       ELSE ''False''
                                  END AS ClockHourProgram
                                 ,CASE WHEN AAUT1.UnitTypeDescrip = ''Present Absent'' --LTRIM(RTRIM(PV.UnitTypeId)) = ''EF5535C2-142C-4223-AE3C-25A50A153CC6''    Present Absent
                                            AND @displayHours = ''hours'' THEN ''Hours''
                                       WHEN AAUT1.UnitTypeDescrip = ''Present Absent'' --LTRIM(RTRIM(PV.UnitTypeId)) = ''EF5535C2-142C-4223-AE3C-25A50A153CC6''      Present Absent
                                            AND @displayHours <> ''hours'' THEN ''Days''
                                       ELSE ''Hours''
                                  END AS UnitTypeDescrip
                                 ,SE.TransferHours AS TransferHours
                                 ,SE.ContractedGradDate AS ContractedGradDate
        FROM       arStudent S
        INNER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
        INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = PV.UnitTypeId
        INNER JOIN arPrograms P ON PV.ProgId = P.ProgId
        INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
        INNER JOIN sySysStatus SYS ON SC.SysStatusId = SYS.SysStatusId
        LEFT JOIN  (
                   SELECT   StuEnrollId
                           ,MAX(ISNULL(ActualRunningScheduledDays,0)) AS ScheduledDays
                           ,MAX(ISNULL(AdjustedPresentDays,0)) AS AttendedDays
                           ,MAX(ISNULL(AdjustedAbsentDays,0)) AS AbsentDays
                           ,MAX(ISNULL(ActualRunningMakeupDays,0)) AS MakeupDays
                           ,MAX(StudentAttendedDate) AS LDA
                   FROM     syStudentAttendanceSummary
                   GROUP BY StuEnrollId
                   ) SAS_NoTerm ON SE.StuEnrollId = SAS_NoTerm.StuEnrollId
        LEFT JOIN  (
                   SELECT   StuEnrollId
                           ,MAX(ISNULL(ActualRunningScheduledDays,0)) AS ScheduledDays
                           ,MAX(ISNULL(AdjustedPresentDays,0)) AS AttendedDays
                           ,MAX(ISNULL(AdjustedAbsentDays,0)) AS AbsentDays
                           ,MAX(ISNULL(ActualRunningMakeupDays,0)) AS MakeupDays
                           ,MAX(StudentAttendedDate) AS LDA
                   FROM     syStudentAttendanceSummary
                   GROUP BY StuEnrollId
                   ) SAS_ForTerm ON SE.StuEnrollId = SAS_ForTerm.StuEnrollId
        WHERE      ( SE.StuEnrollId = @StuEnrollId );
    END;
-- =========================================================================================================
-- END  --  Usp_PR_Sub2_Enrollment_Summary
-- =========================================================================================================

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_PR_Sub3_Terms_forAllEnrolmnents]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub3_Terms_forAllEnrolmnents]', 'P') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[Usp_PR_Sub3_Terms_forAllEnrolmnents]
    @StuEnrollIdList VARCHAR(MAX)
   ,@TermId VARCHAR(MAX) = NULL
   ,@TermStartDate VARCHAR(50) = NULL
   ,@TermStartDateModifier VARCHAR(10) = NULL
   ,@ShowAllEnrollments BIT = 0
AS
    DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
    DECLARE @GradesFormat VARCHAR(50);
    DECLARE @GPAMethod VARCHAR(50);
    IF @TermStartDate IS NULL
       OR @TermStartDate = ''''
        BEGIN
            SET @TermStartDate = CONVERT(VARCHAR(10), GETDATE(), 120);
        END;
    SET @StuEnrollCampusId = COALESCE((
                                      SELECT TOP 1 ASE.CampusId
                                      FROM   arStuEnrollments AS ASE
                                      WHERE  ASE.StuEnrollId IN (
                                                                SELECT Val
                                                                FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                )
                                      )
                                     ,NULL
                                     );
    SET @GradesFormat = dbo.GetAppSettingValueByKeyName(''GradesFormat'', @StuEnrollCampusId);
    IF ( @GradesFormat IS NOT NULL )
        BEGIN
            SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
        END;
    SET @GPAMethod = dbo.GetAppSettingValueByKeyName(''GPAMethod'', @StuEnrollCampusId);
    IF ( @GPAMethod IS NOT NULL )
        BEGIN
            SET @GPAMethod = LOWER(LTRIM(RTRIM(@GPAMethod)));
        END;

    DECLARE @TermCourseDataVar TABLE
        (
            StudentId UNIQUEIDENTIFIER
           ,TermId UNIQUEIDENTIFIER
           ,CourseId UNIQUEIDENTIFIER
           ,CreditsEarned DECIMAL(18, 2)
           ,CreditsAttempted DECIMAL(18, 2)
           ,Completed BIT
           ,FACreditsEarned DECIMAL(18, 2)
        );


    DECLARE @TermDataVar TABLE
        (
            PrgVerId UNIQUEIDENTIFIER
           ,PrgVerDescrip VARCHAR(100)
           ,PrgVersionTrackCredits BIT
           ,TermId UNIQUEIDENTIFIER
           ,TermDescription VARCHAR(100)
           ,TermStartDate DATETIME
           ,TermEndDate DATETIME
           ,CourseId UNIQUEIDENTIFIER
           ,CourseCode VARCHAR(100)
           ,CourseDescription VARCHAR(100)
           ,CourseCodeDescription VARCHAR(100)
           ,CourseCredits DECIMAL(18, 2)
           ,CourseFinAidCredits DECIMAL(18, 2)
           ,MinVal DECIMAL(18, 2)
           ,CourseScore DECIMAL(18, 2)
           ,StuEnrollId UNIQUEIDENTIFIER
           ,CreditsAttempted DECIMAL(18, 2)
           ,CreditsEarned DECIMAL(18, 2)
           ,Completed BIT
           ,CurrentScore DECIMAL(18, 2)
           ,FinalScore DECIMAL(18, 2)
           ,FinalGrade VARCHAR(10)
           ,WeightedAverage_GPA DECIMAL(18, 2)
           ,SimpleAverage_GPA DECIMAL(18, 2)
           ,CampusId UNIQUEIDENTIFIER
           ,CampDescrip VARCHAR(100)
           ,FirstName VARCHAR(100)
           ,LastName VARCHAR(100)
           ,MiddleName VARCHAR(100)
           ,TermGPA_Simple DECIMAL(18, 2)
           ,TermGPA_Weighted DECIMAL(18, 2)
           ,Term_CreditsAttempted DECIMAL(18, 2)
           ,Term_CreditsEarned DECIMAL(18, 2)
           ,termAverage DECIMAL(18, 2)
           ,GrdBkWgtDetailsCount INT
           ,ClockHourProgram BIT
           ,GradesFormat VARCHAR(50)
           ,GPAMethod VARCHAR(50)
           ,StudentId UNIQUEIDENTIFIER
           ,WasRegisteredByProgramType BIT
           ,rownumber1 INT
        );


    INSERT INTO @TermDataVar
                SELECT dt.PrgVerId
                      ,dt.PrgVerDescrip
                      ,dt.PrgVersionTrackCredits
                      ,dt.TermId
                      ,dt.TermDescription
                      ,dt.TermStartDate
                      ,dt.TermEndDate
                      ,dt.CourseId
                      ,dt.CourseCode
                      ,dt.CourseDescription
                      ,dt.CourseCodeDescription
                      ,dt.CourseCredits
                      ,dt.CourseFinAidCredits
                      ,dt.MinVal
                      ,dt.CourseScore
                      ,dt.StuEnrollId
                      ,dt.CreditsAttempted
                      ,dt.CreditsEarned
                      ,dt.Completed
                      ,dt.CurrentScore
                      ,dt.FinalScore
                      ,dt.FinalGrade
                      ,dt.WeightedAverage_GPA
                      ,dt.SimpleAverage_GPA
                      ,dt.CampusId
                      ,dt.CampDescrip
                      ,dt.FirstName
                      ,dt.LastName
                      ,dt.MiddleName
                      ,dt.TermGPA_Simple
                      ,dt.TermGPA_Weighted
                      ,dt.Term_CreditsAttempted
                      ,dt.Term_CreditsEarned
                      ,dt.termAverage
                      ,dt.GrdBkWgtDetailsCount
                      ,dt.ClockHourProgram
                      ,@GradesFormat AS GradesFormat
                      ,@GPAMethod AS GPAMethod
                      ,dt.StudentId AS StudentId
                      ,dt.WasRegisteredByProgramType AS WasRegisteredByProgramType
                      ,ROW_NUMBER() OVER ( PARTITION BY TermId
                                           ORDER BY TermStartDate
                                                   ,TermEndDate
                                                   ,TermDescription
                                         ) AS rownumber1
                FROM   (
                       SELECT     DISTINCT PV.PrgVerId AS PrgVerId
                                 ,PV.PrgVerDescrip AS PrgVerDescrip
                                 ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                       ELSE 0
                                  END AS PrgVersionTrackCredits
                                 ,T.TermId AS TermId
                                 ,T.TermDescrip AS TermDescription
                                 ,T.StartDate AS TermStartDate
                                 ,T.EndDate AS TermEndDate
                                 ,CS.ReqId AS CourseId
                                 ,R.Code AS CourseCode
                                 ,R.Descrip AS CourseDescription
                                 ,''('' + R.Code + '')'' + R.Descrip AS CourseCodeDescription
                                 ,R.Credits AS CourseCredits
                                 ,CASE WHEN ( @ShowAllEnrollments = 1 ) THEN R.FinAidCredits
                                       ELSE (
                                            SELECT SUM(SCS1.FACreditsEarned)
                                            FROM   syCreditSummary AS SCS1
                                            WHERE  SCS1.StuEnrollId = SCS.StuEnrollId
                                                   AND SCS1.TermId = SCS.TermId
                                            )
                                  END AS CourseFinAidCredits
                                 ,(
                                  SELECT MIN(MinVal)
                                  FROM   arGradeScaleDetails GCD
                                        ,arGradeSystemDetails GSD
                                  WHERE  GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                         AND GSD.IsPass = 1
                                         AND GCD.GrdScaleId = CS.GrdScaleId
                                  ) AS MinVal
                                 ,RES.Score AS CourseScore
                                 ,SE.StuEnrollId AS StuEnrollId
                                 --, NULL AS MinResult
                                 --, NULL AS GradeComponentDescription -- Student data   
                                 ,SCS.CreditsAttempted AS CreditsAttempted
                                 ,SCS.CreditsEarned AS CreditsEarned
                                 ,SCS.Completed AS Completed
                                 ,SCS.CurrentScore AS CurrentScore
                                 ,SCS.CurrentGrade AS CurrentGrade
                                 ,SCS.FinalScore AS FinalScore
                                 ,SCS.FinalGrade AS FinalGrade
                                 ,( CASE WHEN (
                                              SELECT SUM(SCS2.Count_WeightedAverage_Credits)
                                              FROM   syCreditSummary AS SCS2
                                              WHERE  SCS2.StuEnrollId = SE.StuEnrollId
                                                     AND SCS2.TermId = T.TermId
                                              ) > 0 THEN (
                                                         SELECT SUM(SCS2.Product_WeightedAverage_Credits_GPA) / SUM(SCS2.Count_WeightedAverage_Credits)
                                                         FROM   syCreditSummary AS SCS2
                                                         WHERE  SCS2.StuEnrollId = SE.StuEnrollId
                                                                AND SCS2.TermId = T.TermId
                                                         )
                                         ELSE 0
                                    END
                                  ) AS WeightedAverage_GPA
                                 ,( CASE WHEN (
                                              SELECT SUM(Count_SimpleAverage_Credits)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = SE.StuEnrollId
                                                     AND TermId = T.TermId
                                              ) > 0 THEN (
                                                         SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                         )
                                         ELSE 0
                                    END
                                  ) AS SimpleAverage_GPA
                                 --, NULL AS WeightedAverage_CumGPA
                                 --, NULL AS SimpleAverage_CumGPA
                                 ,C.CampusId AS CampusId
                                 ,C.CampDescrip AS CampDescrip
                                 --, NULL AS rownumber
                                 ,AST.FirstName AS FirstName
                                 ,AST.LastName AS LastName
                                 ,AST.MiddleName AS MiddleName
                                 -- Newly added fields
                                 ,SCS.TermGPA_Simple AS TermGPA_Simple
                                 ,SCS.TermGPA_Weighted AS TermGPA_Weighted
                                 --, NULL AS newcol
                                 ,(
                                  SELECT SUM(ISNULL(CreditsAttempted, 0))
                                  FROM   syCreditSummary
                                  WHERE  TermId = T.TermId
                                         AND StuEnrollId = SE.StuEnrollId
                                  ) AS Term_CreditsAttempted
                                 ,(
                                  SELECT SUM(ISNULL(CreditsEarned, 0))
                                  FROM   syCreditSummary
                                  WHERE  TermId = T.TermId
                                         AND StuEnrollId = SE.StuEnrollId
                                  ) AS Term_CreditsEarned
                                 --Newly added ends here
                                 ,(
                                  SELECT TOP 1 Average
                                  FROM   syCreditSummary
                                  WHERE  StuEnrollId = SE.StuEnrollId
                                         AND TermId = T.TermId
                                  ) AS termAverage
                                 ,(
                                  SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                  FROM   arGrdBkResults
                                  WHERE  StuEnrollId = SE.StuEnrollId
                                         AND ClsSectionId = RES.TestId
                                  ) AS GrdBkWgtDetailsCount
                                 ,CASE WHEN P.ACId = 5 THEN ''True''
                                       ELSE ''False''
                                  END AS ClockHourProgram
                                 ,AST.StudentId
                                 ,CASE WHEN PV.ProgramRegistrationType IS NOT NULL
                                            AND PV.ProgramRegistrationType = 1 THEN 1
                                       ELSE 0
                                  END AS WasRegisteredByProgramType
                       FROM       arClassSections CS
                       INNER JOIN arResults GBR ON CS.ClsSectionId = GBR.TestId
                       INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
                       INNER JOIN adLeads AS AST ON AST.StudentId = SE.StudentId
                       INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                       INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                       INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                       INNER JOIN arTerm T ON CS.TermId = T.TermId
                       INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                       INNER JOIN arResults RES ON RES.StuEnrollId = GBR.StuEnrollId
                                                   AND RES.TestId = CS.ClsSectionId
                       LEFT JOIN  syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                         AND T.TermId = SCS.TermId
                                                         AND R.ReqId = SCS.ReqId
                       WHERE -- SE.StuEnrollId = @StuEnrollId
                                  SE.StuEnrollId IN (
                                                    SELECT Val
                                                    FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                    )
                                  AND (
                                      @TermStartDate IS NULL
                                      OR @TermStartDateModifier IS NULL
                                      OR T.StartDate IS NULL
                                      OR (
                                         (
                                         ( @TermStartDateModifier <> ''='' )
                                         OR ( T.StartDate = @TermStartDate )
                                         )
                                         AND (
                                             ( @TermStartDateModifier <> ''>'' )
                                             OR ( T.StartDate > @TermStartDate )
                                             )
                                         AND (
                                             ( @TermStartDateModifier <> ''<'' )
                                             OR ( T.StartDate < @TermStartDate )
                                             )
                                         AND (
                                             ( @TermStartDateModifier <> ''>='' )
                                             OR ( T.StartDate >= @TermStartDate )
                                             )
                                         AND (
                                             ( @TermStartDateModifier <> ''<='' )
                                             OR ( T.StartDate <= @TermStartDate )
                                             )
                                         )
                                      )
                                  AND (
                                      @TermId IS NULL
                                      OR T.TermId IN (
                                                     SELECT Val
                                                     FROM   MultipleValuesForReportParameters(@TermId, '','', 1)
                                                     )
                                      )
                       UNION
                       SELECT     DISTINCT PV.PrgVerId AS PrgVerId
                                 ,PV.PrgVerDescrip AS PrgVerDescrip
                                 ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                       ELSE 0
                                  END AS PrgVersionTrackCredits
                                 ,T.TermId AS TermId
                                 ,T.TermDescrip AS TermDescription
                                 ,T.StartDate AS TermStartDate
                                 ,T.EndDate AS TermEndDate
                                 ,GBCR.ReqId AS CourseId
                                 ,R.Code AS CourseCode
                                 ,R.Descrip AS CourseDescrip
                                 ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                 ,R.Credits AS CourseCredits
                                 ,CASE WHEN ( @ShowAllEnrollments = 1 ) THEN R.FinAidCredits
                                       ELSE (
                                            SELECT SUM(SCS1.FACreditsEarned)
                                            FROM   syCreditSummary AS SCS1
                                            WHERE  SCS1.StuEnrollId = SCS.StuEnrollId
                                                   AND SCS1.TermId = SCS.TermId
                                            )
                                  END AS CourseFinAidCredits
                                 ,(
                                  SELECT MIN(MinVal)
                                  FROM   arGradeScaleDetails GCD
                                        ,arGradeSystemDetails GSD
                                  WHERE  GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                         AND GSD.IsPass = 1
                                  ) AS MinVal
                                 ,GBCR.Score AS CourseScore
                                 ,SE.StuEnrollId AS StuEnrollId
                                 --, NULL AS MinResult
                                 --, NULL AS GradeComponentDescription -- Student data    
                                 ,SCS.CreditsAttempted AS CreditsAttempted
                                 ,SCS.CreditsEarned AS CreditsEarned
                                 ,SCS.Completed AS Completed
                                 ,SCS.CurrentScore AS CurrentScore
                                 ,SCS.CurrentGrade AS CurrentGrade
                                 ,SCS.FinalScore AS FinalScore
                                 ,SCS.FinalGrade AS FinalGrade
                                 ,( CASE WHEN (
                                              SELECT SUM(Count_WeightedAverage_Credits)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = SE.StuEnrollId
                                                     AND TermId = T.TermId
                                              ) > 0 THEN (
                                                         SELECT SUM(Product_WeightedAverage_Credits_GPA) / SUM(Count_WeightedAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                         )
                                         ELSE 0
                                    END
                                  ) AS WeightedAverage_GPA
                                 ,( CASE WHEN (
                                              SELECT SUM(Count_SimpleAverage_Credits)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = SE.StuEnrollId
                                                     AND TermId = T.TermId
                                              ) > 0 THEN (
                                                         SELECT SUM(Product_SimpleAverage_Credits_GPA) / SUM(Count_SimpleAverage_Credits)
                                                         FROM   syCreditSummary
                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                AND TermId = T.TermId
                                                         )
                                         ELSE 0
                                    END
                                  ) AS SimpleAverage_GPA
                                 --, NULL AS WeightedAverage_CumGPA
                                 --, NULL AS SimpleAverage_CumGPA
                                 ,C.CampusId
                                 ,C.CampDescrip
                                 --, NULL AS rownumber
                                 ,AST.FirstName AS FirstName
                                 ,AST.LastName AS LastName
                                 ,AST.MiddleName AS MiddleName
                                 -- Newly added fields
                                 ,SCS.TermGPA_Simple AS TermGPA_Simple
                                 ,SCS.TermGPA_Weighted AS TermGPA_Weighted
                                 --, NULL AS newcol
                                 ,(
                                  SELECT SUM(ISNULL(CreditsAttempted, 0))
                                  FROM   syCreditSummary
                                  WHERE  TermId = T.TermId
                                         AND StuEnrollId = SE.StuEnrollId
                                  ) AS Term_CreditsAttempted
                                 ,(
                                  SELECT SUM(ISNULL(CreditsEarned, 0))
                                  FROM   syCreditSummary
                                  WHERE  TermId = T.TermId
                                         AND StuEnrollId = SE.StuEnrollId
                                  ) AS Term_CreditsEarned
                                 --Newly added ends here
                                 ,(
                                  SELECT TOP 1 Average
                                  FROM   syCreditSummary
                                  WHERE  StuEnrollId = SE.StuEnrollId
                                         AND TermId = T.TermId
                                  ) AS termAverage
                                 ,(
                                  SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                  FROM   arGrdBkConversionResults
                                  WHERE  StuEnrollId = SE.StuEnrollId
                                         AND TermId = T.TermId
                                         AND ReqId = R.ReqId
                                  ) AS GrdBkWgtDetailsCount
                                 ,CASE WHEN P.ACId = 5 THEN ''True''
                                       ELSE ''False''
                                  END AS ClockHourProgram
                                 ,AST.StudentId
                                 ,CASE WHEN PV.ProgramRegistrationType IS NOT NULL
                                            AND PV.ProgramRegistrationType = 1 THEN 1
                                       ELSE 0
                                  END AS WasRegisteredByProgramType
                       FROM       arTransferGrades GBCR
                       INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                       INNER JOIN adLeads AS AST ON AST.StudentId = SE.StudentId
                       INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                       INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                       INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                       INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                       INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                       --INNER JOIN arResults AR ON GBCR.StuEnrollId = AR.StuEnrollId
                       LEFT JOIN  syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                         AND T.TermId = SCS.TermId
                                                         AND R.ReqId = SCS.ReqId
                       WHERE -- SE.StuEnrollId = @StuEnrollId
                                  SE.StuEnrollId IN (
                                                    SELECT Val
                                                    FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                    )
                                  AND (
                                      @TermStartDate IS NULL
                                      OR @TermStartDateModifier IS NULL
                                      OR T.StartDate IS NULL
                                      OR (
                                         (
                                         ( @TermStartDateModifier <> ''='' )
                                         OR ( T.StartDate = @TermStartDate )
                                         )
                                         AND (
                                             ( @TermStartDateModifier <> ''>'' )
                                             OR ( T.StartDate > @TermStartDate )
                                             )
                                         AND (
                                             ( @TermStartDateModifier <> ''<'' )
                                             OR ( T.StartDate < @TermStartDate )
                                             )
                                         AND (
                                             ( @TermStartDateModifier <> ''>='' )
                                             OR ( T.StartDate >= @TermStartDate )
                                             )
                                         AND (
                                             ( @TermStartDateModifier <> ''<='' )
                                             OR ( T.StartDate <= @TermStartDate )
                                             )
                                         )
                                      )
                                  AND (
                                      @TermId IS NULL
                                      OR T.TermId IN (
                                                     SELECT Val
                                                     FROM   MultipleValuesForReportParameters(@TermId, '','', 1)
                                                     )
                                      )
                       ) dt;

    --SELECT * FROM @TermDataVar

    IF @ShowAllEnrollments = 1
        BEGIN
            INSERT INTO @TermCourseDataVar
                        SELECT DISTINCT StudentId
                              ,TermId
                              ,CourseId
                              ,CreditsEarned
                              ,CreditsAttempted
                              ,Completed
                              ,CourseFinAidCredits
                        FROM   @TermDataVar
                        WHERE  Completed = 1;

            SELECT   PrgVerId
                    ,PrgVerDescrip
                    ,PrgVersionTrackCredits
                    ,TermId
                    ,TermDescription
                    ,TermStartDate
                    ,TermEndDate
                    ,CourseId
                    ,CourseCode
                    ,CourseDescription
                    ,CourseCodeDescription
                    ,CourseCredits
                    ,(
                     SELECT   SUM(FACreditsEarned)
                     FROM     @TermCourseDataVar
                     WHERE    TermId = [@TermDataVar].TermId
                     GROUP BY StudentId
                     ) AS CourseFinAidCredits
                    --,(SELECT SUM(SCS1.FACreditsEarned)
                    --                         FROM   syCreditSummary AS SCS1
                    --                         WHERE  SCS1.StuEnrollId = ''0235B76B-B4AB-40C1-85C1-64ED042D1205'' 
                    --                                AND SCS1.TermId = ''E7430FF3-B021-4189-805D-5B54544D9615'')
                    ,MinVal
                    ,CourseScore
                    ,StuEnrollId
                    ,CreditsAttempted
                    ,CreditsEarned
                    ,Completed
                    ,CurrentScore
                    ,FinalScore
                    ,FinalGrade
                    ,WeightedAverage_GPA
                    ,SimpleAverage_GPA
                    ,CampusId
                    ,CampDescrip
                    ,FirstName
                    ,LastName
                    ,MiddleName
                    ,TermGPA_Simple
                    ,TermGPA_Weighted
                    ,(
                     SELECT   SUM(CreditsAttempted)
                     FROM     @TermCourseDataVar
                     WHERE    TermId = [@TermDataVar].TermId
                     GROUP BY StudentId
                     ) AS Term_CreditsAttempted
                    ,(
                     SELECT   SUM(CreditsEarned)
                     FROM     @TermCourseDataVar
                     WHERE    TermId = [@TermDataVar].TermId
                     GROUP BY StudentId
                     ) AS Term_CreditsEarned
                    ,CASE WHEN (
                               SELECT TOP 1 P.ACId
                               FROM   dbo.arStuEnrollments E
                               JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                               JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
                               WHERE  E.StuEnrollId = StuEnrollId
                               ) = 5
                               AND @GradesFormat = ''numeric'' THEN dbo.CalculateStudentAverage(StuEnrollId, NULL, NULL, NULL, TermId)
                          ELSE termAverage
                     END AS termAverage
                    ,GrdBkWgtDetailsCount
                    ,ClockHourProgram
                    ,GradesFormat
                    ,GPAMethod
                    ,WasRegisteredByProgramType
                    ,rownumber1
            FROM     @TermDataVar
            WHERE    rownumber1 = 1
            ORDER BY PrgVerDescrip
                    ,TermStartDate
                    ,TermEndDate
                    ,TermDescription
                    ,FinalGrade DESC
                    ,FinalScore DESC
                    ,CourseCode;
        END;
    ELSE
        BEGIN
            SELECT   PrgVerId
                    ,PrgVerDescrip
                    ,PrgVersionTrackCredits
                    ,TermId
                    ,TermDescription
                    ,TermStartDate
                    ,TermEndDate
                    ,CourseId
                    ,CourseCode
                    ,CourseDescription
                    ,CourseCodeDescription
                    ,CourseCredits
                    ,CourseFinAidCredits
                    ,MinVal
                    ,CourseScore
                    ,StuEnrollId
                    ,CreditsAttempted
                    ,CreditsEarned
                    ,Completed
                    ,CurrentScore
                    ,FinalScore
                    ,FinalGrade
                    ,WeightedAverage_GPA
                    ,SimpleAverage_GPA
                    ,CampusId
                    ,CampDescrip
                    ,FirstName
                    ,LastName
                    ,MiddleName
                    ,TermGPA_Simple
                    ,TermGPA_Weighted
                    ,Term_CreditsAttempted
                    ,Term_CreditsEarned
                    ,CASE WHEN (
                               SELECT TOP 1 P.ACId
                               FROM   dbo.arStuEnrollments E
                               JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                               JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
                               WHERE  E.StuEnrollId = StuEnrollId
                               ) = 5
                               AND @GradesFormat = ''numeric'' THEN dbo.CalculateStudentAverage(StuEnrollId, NULL, NULL, NULL, TermId)
                          ELSE termAverage
                     END AS termAverage
                    ,GrdBkWgtDetailsCount
                    ,ClockHourProgram
                    ,GradesFormat
                    ,GPAMethod
                    ,StudentId
                    ,WasRegisteredByProgramType
                    ,rownumber1
            FROM     @TermDataVar
            WHERE    rownumber1 = 1
            ORDER BY PrgVerDescrip
                    ,TermStartDate
                    ,TermEndDate
                    ,TermDescription
                    ,FinalGrade DESC
                    ,FinalScore DESC
                    ,CourseCode;
        END;

--SELECT * FROM @TermCourseDataVar








'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]', 'P') IS NULL
EXEC sp_executesql N'
-- ========================================================================================================= 
-- Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents 
-- ========================================================================================================= 
CREATE PROCEDURE [dbo].[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents]
    @StuEnrollIdList VARCHAR(MAX)
   ,@TermId VARCHAR(50) = NULL
   ,@SysComponentTypeId VARCHAR(50) = NULL
AS -- source usp_pr_main_ssrs_subreport3_byterm  
    BEGIN
        DECLARE @GradesFormat AS VARCHAR(50);
        DECLARE @GPAMethod AS VARCHAR(50);
        DECLARE @GradeBookAt AS VARCHAR(50);
        DECLARE @StuEnrollCampusId AS UNIQUEIDENTIFIER;
        DECLARE @Counter AS INT;
        DECLARE @times AS INT;
        DECLARE @TermStartDate1 AS DATETIME;
        DECLARE @Score AS DECIMAL(18, 2);
        DECLARE @GrdCompDescrip AS VARCHAR(50);

        DECLARE @curId AS UNIQUEIDENTIFIER;
        DECLARE @curReqId AS UNIQUEIDENTIFIER;
        DECLARE @curStuEnrollId AS UNIQUEIDENTIFIER;
        DECLARE @curDescrip AS VARCHAR(50);
        DECLARE @curNumber AS INT;
        DECLARE @curGrdComponentTypeId AS INT;
        DECLARE @curMinResult AS DECIMAL(18, 2);
        DECLARE @curGrdComponentDescription AS VARCHAR(50);
        DECLARE @curClsSectionId AS UNIQUEIDENTIFIER;
        DECLARE @curRownumber AS INT;


        CREATE TABLE #tempTermWorkUnitCount
            (
                TermId UNIQUEIDENTIFIER
               ,ReqId UNIQUEIDENTIFIER
               ,sysComponentTypeId INT
               ,WorkUnitCount INT
            );
        CREATE TABLE #Temp21
            (
                Id UNIQUEIDENTIFIER
               ,TermId UNIQUEIDENTIFIER
               ,ReqId UNIQUEIDENTIFIER
               ,GradeBookDescription VARCHAR(50)
               ,Number INT
               ,GradeBookSysComponentTypeId INT
               ,GradeBookScore DECIMAL(18, 2)
               ,MinResult DECIMAL(18, 2)
               ,GradeComponentDescription VARCHAR(50)
               ,ClsSectionId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,RowNumber INT
            );
        CREATE TABLE #Temp22
            (
                ReqId UNIQUEIDENTIFIER
               ,EffectiveDate DATETIME
            );
        SET @StuEnrollCampusId = COALESCE((
                                          SELECT TOP 1 ASE.CampusId
                                          FROM   arStuEnrollments AS ASE
                                          WHERE  ASE.StuEnrollId IN (
                                                                    SELECT Val
                                                                    FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                    )
                                          )
                                         ,NULL
                                         );
        SET @GradesFormat = dbo.GetAppSettingValueByKeyName(''GradesFormat'', @StuEnrollCampusId);
        IF ( @GradesFormat IS NOT NULL )
            BEGIN
                SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
            END;
        SET @GPAMethod = dbo.GetAppSettingValueByKeyName(''GPAMethod'', @StuEnrollCampusId);
        IF ( @GPAMethod IS NOT NULL )
            BEGIN
                SET @GPAMethod = LOWER(LTRIM(RTRIM(@GPAMethod)));
            END;
        SET @GradeBookAt = dbo.GetAppSettingValueByKeyName(''GradeBookWeightingLevel'', @StuEnrollCampusId);
        IF ( @GradeBookAt IS NOT NULL )
            BEGIN
                SET @GradeBookAt = LOWER(LTRIM(RTRIM(@GradeBookAt)));
            END;

        IF LOWER(@GradeBookAt) = ''instructorlevel''
            BEGIN
                INSERT INTO #tempTermWorkUnitCount
                            SELECT   dt.TermId
                                    ,dt.ReqId
                                    ,dt.GradeBookSysComponentTypeId
                                    ,COUNT(dt.GradeBookDescription)
                            FROM     (
                                     SELECT     d.ReqId
                                               ,d.TermId
                                               ,CASE WHEN a.Descrip IS NULL THEN e.Descrip
                                                     --THEN (  
                                                     --      SELECT   Resource  
                                                     --      FROM     syResources  
                                                     --      WHERE    ResourceID = e.SysComponentTypeId  
                                                     --     )  
                                                     ELSE a.Descrip
                                                END AS GradeBookDescription
                                               ,( CASE e.SysComponentTypeId
                                                       WHEN 500 THEN a.Number
                                                       WHEN 503 THEN a.Number
                                                       WHEN 544 THEN a.Number
                                                       ELSE (
                                                            SELECT MIN(MinVal)
                                                            FROM   arGradeScaleDetails GSD
                                                                  ,arGradeSystemDetails GSS
                                                            WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                   AND GSS.IsPass = 1
                                                                   AND GSD.GrdScaleId = d.GrdScaleId
                                                            )
                                                  END
                                                ) AS MinResult
                                               ,a.Required
                                               ,a.MustPass
                                               ,ISNULL(e.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId
                                               ,a.Number
                                               ,(
                                                SELECT Resource
                                                FROM   syResources
                                                WHERE  ResourceID = e.SysComponentTypeId
                                                ) AS GradeComponentDescription
                                               ,a.InstrGrdBkWgtDetailId
                                               ,c.StuEnrollId
                                               ,0 AS IsExternShip
                                               ,CASE e.SysComponentTypeId
                                                     WHEN 544 THEN (
                                                                   SELECT ISNULL(SUM(HoursAttended), ''0.00'')
                                                                   FROM   arExternshipAttendance
                                                                   WHERE  StuEnrollId = c.StuEnrollId
                                                                   )
                                                     ELSE (
                                                          SELECT SUM(AGBR.Score)
                                                          FROM   arGrdBkResults AS AGBR
                                                          WHERE  AGBR.StuEnrollId = c.StuEnrollId
                                                                 AND AGBR.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                                                 AND AGBR.ClsSectionId = d.ClsSectionId
                                                          )
                                                END AS GradeBookScore
                                               ,ROW_NUMBER() OVER ( PARTITION BY ST.StuEnrollId
                                                                                --T.TermId, R.ReqId ORDER BY ST.CampusId, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, RES.Resource, e.Descrip) AS rownumber  
                                                                                ,T.TermId
                                                                                ,R.ReqId
                                                                    ORDER BY e.SysComponentTypeId
                                                                            ,a.Descrip
                                                                  ) AS rownumber
                                     FROM       arGrdBkWgtDetails AS a
                                     INNER JOIN arGrdBkWeights AS b ON b.InstrGrdBkWgtId = a.InstrGrdBkWgtId
                                     INNER JOIN arClassSections AS d ON d.InstrGrdBkWgtId = a.InstrGrdBkWgtId
                                     INNER JOIN arResults AS c ON c.TestId = d.ClsSectionId
                                     INNER JOIN arGrdComponentTypes AS e ON e.GrdComponentTypeId = a.GrdComponentTypeId
                                     INNER JOIN arStuEnrollments AS ST ON ST.StuEnrollId = c.StuEnrollId
                                     INNER JOIN arTerm AS T ON T.TermId = d.TermId
                                     INNER JOIN arReqs AS R ON R.ReqId = d.ReqId
                                     INNER JOIN syResources AS RES ON RES.ResourceID = e.SysComponentTypeId
                                     WHERE      c.StuEnrollId IN (
                                                                 SELECT Val
                                                                 FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                 )
                                                AND (
                                                    @SysComponentTypeId IS NULL
                                                    OR e.SysComponentTypeId IN (
                                                                               SELECT Val
                                                                               FROM   MultipleValuesForReportParameters(@SysComponentTypeId, '','', 1)
                                                                               )
                                                    )
                                     UNION
                                     SELECT     R.ReqId
                                               ,T.TermId
                                               ,CASE WHEN GBW.Descrip IS NULL THEN (
                                                                                   SELECT Resource
                                                                                   FROM   syResources
                                                                                   WHERE  ResourceID = GCT.SysComponentTypeId
                                                                                   )
                                                     ELSE GBW.Descrip
                                                END AS GradeBookDescription
                                               ,( CASE GCT.SysComponentTypeId
                                                       WHEN 500 THEN GBWD.Number
                                                       WHEN 503 THEN GBWD.Number
                                                       WHEN 544 THEN GBWD.Number
                                                       ELSE (
                                                            SELECT MIN(MinVal)
                                                            FROM   arGradeScaleDetails GSD
                                                                  ,arGradeSystemDetails GSS
                                                            WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                   AND GSS.IsPass = 1
                                                                   AND GSD.GrdScaleId = CSC.GrdScaleId
                                                            )
                                                  END
                                                ) AS MinResult
                                               ,GBWD.Required
                                               ,GBWD.MustPass
                                               ,ISNULL(GCT.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId
                                               ,GBWD.Number
                                               ,(
                                                SELECT Resource
                                                FROM   syResources
                                                WHERE  ResourceID = GCT.SysComponentTypeId
                                                ) AS GradeComponentDescription
                                               ,GBWD.InstrGrdBkWgtDetailId
                                               ,GBCR.StuEnrollId
                                               ,0 AS IsExternShip
                                               ,CASE GCT.SysComponentTypeId
                                                     WHEN 544 THEN (
                                                                   SELECT ISNULL(SUM(HoursAttended), ''0.00'')
                                                                   FROM   arExternshipAttendance
                                                                   WHERE  StuEnrollId = SE.StuEnrollId
                                                                   )
                                                     ELSE (
                                                          SELECT ISNULL(SUM(AGBR.Score), 0.00)
                                                          FROM   arGrdBkResults AS AGBR
                                                          WHERE  AGBR.StuEnrollId = SE.StuEnrollId
                                                                 AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                 AND ClsSectionId = CSC.ClsSectionId
                                                          )
                                                END AS GradeBookScore
                                               ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId
                                                                                ,T.TermId
                                                                                ,R.ReqId
                                                                    ORDER BY SE.CampusId
                                                                            ,T.StartDate
                                                                            ,T.EndDate
                                                                            ,T.TermId
                                                                            ,T.TermDescrip
                                                                            ,R.ReqId
                                                                            ,R.Descrip
                                                                            ,SYRES.Resource
                                                                            ,GCT.Descrip
                                                                  ) AS rownumber
                                     FROM       arGrdBkConversionResults GBCR
                                     INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                     INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                     INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                     INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                     INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                     INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                          AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                     INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                      AND GBCR.ReqId = GBW.ReqId
                                     INNER JOIN (
                                                SELECT   ReqId
                                                        ,MAX(EffectiveDate) AS EffectiveDate
                                                FROM     arGrdBkWeights
                                                GROUP BY ReqId
                                                ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                                     INNER JOIN (
                                                SELECT Resource
                                                      ,ResourceID
                                                FROM   syResources
                                                WHERE  ResourceTypeID = 10
                                                ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                     INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                     INNER JOIN arClassSections CSC ON CSC.TermId = T.TermId
                                                                       AND CSC.ReqId = R.ReqId
                                     WHERE      MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                                                AND SE.StuEnrollId IN (
                                                                      SELECT Val
                                                                      FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                      )
                                                AND (
                                                    @SysComponentTypeId IS NULL
                                                    OR GCT.SysComponentTypeId IN (
                                                                                 SELECT Val
                                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeId, '','', 1)
                                                                                 )
                                                    )
                                     ) AS dt
                            GROUP BY dt.TermId
                                    ,dt.ReqId
                                    ,dt.GradeBookSysComponentTypeId;
            END;
        ELSE
            BEGIN
                SET @Counter = 0;
                SET @TermStartDate1 = (
                                      SELECT AT.StartDate
                                      FROM   dbo.arTerm AS AT
                                      WHERE  AT.TermId = @TermId
                                      );
                INSERT INTO #Temp22
                            SELECT     AGBW.ReqId
                                      ,MAX(AGBW.EffectiveDate) AS EffectiveDate
                            FROM       dbo.arGrdBkWeights AS AGBW
                            INNER JOIN dbo.syCreditSummary AS SCS ON SCS.ReqId = AGBW.ReqId
                            WHERE      SCS.TermId = @TermId
                                       AND SCS.StuEnrollId IN (
                                                              SELECT Val
                                                              FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                              )
                                       AND AGBW.EffectiveDate <= @TermStartDate1
                            GROUP BY   AGBW.ReqId;

                DECLARE getUsers_Cursor CURSOR FOR
                    SELECT   dt.ID
                            ,dt.ReqId
                            ,dt.Descrip
                            ,dt.Number
                            ,dt.SysComponentTypeId
                            ,dt.MinResult
                            ,dt.GradeComponentDescription
                            ,dt.ClsSectionId
                            ,dt.StuEnrollId
                            ,ROW_NUMBER() OVER ( PARTITION BY dt.StuEnrollId
                                                             ,@TermId
                                                             ,dt.SysComponentTypeId
                                                 ORDER BY dt.SysComponentTypeId
                                                         ,dt.Descrip
                                               ) AS rownumber
                    FROM     (
                             SELECT     ISNULL(AGBWD.InstrGrdBkWgtDetailId, NEWID()) AS ID
                                       ,AR.ReqId
                                       ,AGBWD.Descrip
                                       ,AGBWD.Number
                                       ,AGCT.SysComponentTypeId
                                       ,( CASE WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN AGBWD.Number
                                               ELSE (
                                                    SELECT     MIN(AGSD.MinVal) AS MinVal
                                                    FROM       dbo.arGradeScaleDetails AS AGSD
                                                    INNER JOIN dbo.arGradeSystemDetails AS AGSDetails ON AGSDetails.GrdSysDetailId = AGSD.GrdSysDetailId
                                                    WHERE      AGSDetails.IsPass = 1
                                                               AND AGSD.GrdScaleId = ACS.GrdScaleId
                                                    )
                                          END
                                        ) AS MinResult
                                       ,SR.Resource AS GradeComponentDescription
                                       ,ACS.ClsSectionId
                                       ,AGBR.StuEnrollId
                             FROM       dbo.arGrdBkResults AS AGBR
                             INNER JOIN dbo.arGrdBkWgtDetails AS AGBWD ON AGBWD.InstrGrdBkWgtDetailId = AGBR.InstrGrdBkWgtDetailId
                             INNER JOIN dbo.arGrdBkWeights AS AGBW ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                             INNER JOIN dbo.arGrdComponentTypes AS AGCT ON AGCT.GrdComponentTypeId = AGBWD.GrdComponentTypeId
                             INNER JOIN dbo.syResources AS SR ON SR.ResourceID = AGCT.SysComponentTypeId
                             --INNER JOIN #Temp22 AS T2 ON T2.ReqId = AGBW.ReqId 
                             INNER JOIN dbo.arReqs AS AR ON AR.ReqId = AGBW.ReqId
                             INNER JOIN dbo.arClassSections AS ACS ON ACS.ReqId = AR.ReqId
                                                                      AND ACS.ClsSectionId = AGBR.ClsSectionId
                             INNER JOIN dbo.arResults AS AR2 ON AR2.TestId = ACS.ClsSectionId
                                                                AND AR2.StuEnrollId = AGBR.StuEnrollId --AND AR2.TestId = AGBR.ClsSectionId 

                             WHERE      (
                                        @StuEnrollIdList IS NULL
                                        OR AGBR.StuEnrollId IN (
                                                               SELECT Val
                                                               FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                               )
                                        )
                                        --AND T2.EffectiveDate = AGBW.EffectiveDate 
                                        AND AGBWD.Number > 0
                             ) dt
                    ORDER BY SysComponentTypeId
                            ,rownumber;
                OPEN getUsers_Cursor;
                FETCH NEXT FROM getUsers_Cursor
                INTO @curId
                    ,@curReqId
                    ,@curDescrip
                    ,@curNumber
                    ,@curGrdComponentTypeId
                    ,@curMinResult
                    ,@curGrdComponentDescription
                    ,@curClsSectionId
                    ,@curStuEnrollId
                    ,@curRownumber;
                SET @Counter = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN
                        --PRINT @curNumber  
                        SET @times = 1;
                        WHILE @times <= @curNumber
                            BEGIN
                                --  PRINT @times  
                                IF @curNumber > 1
                                    BEGIN
                                        SET @GrdCompDescrip = @curDescrip + CAST(@times AS CHAR);


                                        IF @curGrdComponentTypeId = 544
                                            BEGIN
                                                SET @Score = (
                                                             SELECT ISNULL(SUM(HoursAttended), ''0.00'')
                                                             FROM   arExternshipAttendance
                                                             WHERE  StuEnrollId = @curStuEnrollId
                                                             );
                                            END;
                                        ELSE
                                            BEGIN
                                                SET @Score = (
                                                             SELECT ISNULL(SUM(Score), 0.00)
                                                             FROM   arGrdBkResults
                                                             WHERE  StuEnrollId = @curStuEnrollId
                                                                    AND InstrGrdBkWgtDetailId = @curId
                                                                    AND ResNum = @times
                                                                    AND ClsSectionId = @curClsSectionId
                                                             );
                                            END;

                                        SET @curRownumber = @times;
                                    END;
                                ELSE
                                    BEGIN
                                        SET @GrdCompDescrip = @curDescrip;

                                        IF @curGrdComponentTypeId = 544
                                            BEGIN
                                                SET @Score = (
                                                             SELECT ISNULL(SUM(HoursAttended), ''0.00'')
                                                             FROM   arExternshipAttendance
                                                             WHERE  StuEnrollId = @curStuEnrollId
                                                             );
                                            END;
                                        ELSE
                                            BEGIN
                                                SET @Score = (
                                                             SELECT ISNULL(SUM(Score), 0.00)
                                                             FROM   arGrdBkResults
                                                             WHERE  StuEnrollId = @curStuEnrollId
                                                                    AND InstrGrdBkWgtDetailId = @curId
                                                                    AND ResNum = @times
                                                             );
                                            END;


                                    END;
                                --PRINT @Score  
                                INSERT INTO #Temp21
                                VALUES ( @curId, @TermId, @curReqId, @GrdCompDescrip, @curNumber, @curGrdComponentTypeId, @Score, @curMinResult
                                        ,@curGrdComponentDescription, @curClsSectionId, @curStuEnrollId, @curRownumber );

                                SET @times = @times + 1;
                            END;
                        FETCH NEXT FROM getUsers_Cursor
                        INTO @curId
                            ,@curReqId
                            ,@curDescrip
                            ,@curNumber
                            ,@curGrdComponentTypeId
                            ,@curMinResult
                            ,@curGrdComponentDescription
                            ,@curClsSectionId
                            ,@curStuEnrollId
                            ,@curRownumber;
                    END;
                CLOSE getUsers_Cursor;
                DEALLOCATE getUsers_Cursor;

                INSERT INTO #tempTermWorkUnitCount
                            SELECT   dt.TermId
                                    ,dt.ReqId
                                    ,dt.GradeBookSysComponentTypeId
                                    ,COUNT(dt.GradeBookDescription)
                            FROM     (
                                     SELECT T21.TermId
                                           ,T21.ReqId
                                           ,T21.GradeBookDescription
                                           ,T21.GradeBookSysComponentTypeId
                                     FROM   #Temp21 AS T21
                                     UNION
                                     SELECT     T.TermId
                                               ,GBCR.ReqId
                                               ,GCT.Descrip AS GradeBookDescription
                                               ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                     FROM       arGrdBkConversionResults GBCR
                                     INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                     INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                     INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                     INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                     INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                     INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                          AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                     INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                      AND GBCR.ReqId = GBW.ReqId
                                     INNER JOIN (
                                                SELECT   ReqId
                                                        ,MAX(EffectiveDate) AS EffectiveDate
                                                FROM     arGrdBkWeights
                                                GROUP BY ReqId
                                                ) AS MED ON MED.ReqId = GBCR.ReqId -- MED --> MaxEffectiveDatesByCourse  
                                     INNER JOIN (
                                                SELECT Resource
                                                      ,ResourceID
                                                FROM   syResources
                                                WHERE  ResourceTypeID = 10
                                                ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                     INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                     WHERE      MED.EffectiveDate <= T.StartDate
                                                AND SE.StuEnrollId IN (
                                                                      SELECT Val
                                                                      FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                                      )
                                                AND T.TermId = @TermId
                                                AND (
                                                    @SysComponentTypeId IS NULL
                                                    OR GCT.SysComponentTypeId IN (
                                                                                 SELECT Val
                                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeId, '','', 1)
                                                                                 )
                                                    )
                                     ) dt
                            GROUP BY dt.TermId
                                    ,dt.ReqId
                                    ,dt.GradeBookSysComponentTypeId;
            END;

        DROP TABLE #Temp22;
        DROP TABLE #Temp21;

        -- SELECT * FROM #tempTermWorkUnitCount AS TTWUC  ORDER BY TTWUC.TermId  -- , TTWUC.StuEnrollId  


        SELECT   dt1.PrgVerId
                ,dt1.PrgVerDescrip
                ,dt1.PrgVersionTrackCredits
                ,dt1.TermId
                ,dt1.TermDescription
                ,dt1.TermStartDate
                ,dt1.TermEndDate
                ,dt1.CourseId
                ,dt1.CouseStartDate
                ,dt1.CourseCode
                ,dt1.CourseDescription
                ,dt1.CourseCodeDescription
                ,dt1.CourseCredits
                ,dt1.CourseFinAidCredits
                ,dt1.MinVal
                ,dt1.CourseScore
                ,dt1.GradeBook_ResultId
                ,dt1.GradeBookDescription
                ,dt1.GradeBookScore
                ,dt1.GradeBookPostDate
                ,dt1.GradeBookPassingGrade
                ,dt1.GradeBookWeight
                ,dt1.GradeBookRequired
                ,dt1.GradeBookMustPass
                ,dt1.GradeBookSysComponentTypeId
                ,dt1.GradeBookHoursRequired
                ,dt1.GradeBookHoursCompleted
                ,dt1.StuEnrollId
                ,dt1.MinResult
                ,dt1.GradeComponentDescription -- Student data     
                ,dt1.CreditsAttempted
                ,dt1.CreditsEarned
                ,dt1.Completed
                ,dt1.CurrentScore
                ,dt1.CurrentGrade
                ,CASE WHEN (
                           SELECT TOP 1 P.ACId
                           FROM   dbo.arStuEnrollments E
                           JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                           JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
                           WHERE  E.StuEnrollId = dt1.StuEnrollId
                           ) = 5
                           AND @GradesFormat = ''numeric'' THEN dbo.CalculateStudentAverage(dt1.StuEnrollId, NULL, NULL, dt1.ClsSectionId, NULL)
                      ELSE dt1.FinalScore
                 END AS FinalScore
                ,dt1.FinalGrade
                ,dt1.CampusId
                ,dt1.CampDescrip
                ,dt1.rownumber
                ,dt1.FirstName
                ,dt1.LastName
                ,dt1.MiddleName
                ,dt1.GrdBkWgtDetailsCount
                ,dt1.ClockHourProgram
                ,dt1.GradesFormat
                ,dt1.GPAMethod
                ,dt1.WorkUnitCount
                ,dt1.WorkUnitCount_501
                ,dt1.WorkUnitCount_544
                ,dt1.WorkUnitCount_502
                ,dt1.WorkUnitCount_499
                ,dt1.WorkUnitCount_503
                ,dt1.WorkUnitCount_500
                ,dt1.WorkUnitCount_533
                ,dt1.rownumber2
        FROM     (
                 SELECT dt.PrgVerId
                       ,dt.PrgVerDescrip
                       ,dt.PrgVersionTrackCredits
                       ,dt.TermId
                       ,dt.TermDescription
                       ,dt.TermStartDate
                       ,dt.TermEndDate
                       ,dt.CourseId
                       ,dt.CouseStartDate
                       ,dt.CourseCode
                       ,dt.CourseDescription
                       ,dt.CourseCodeDescription
                       ,dt.CourseCredits
                       ,dt.CourseFinAidCredits
                       ,dt.MinVal
                       ,dt.CourseScore
                       ,dt.GradeBook_ResultId
                       ,dt.GradeBookDescription
                       ,dt.GradeBookScore
                       ,dt.GradeBookPostDate
                       ,dt.GradeBookPassingGrade
                       ,dt.GradeBookWeight
                       ,dt.GradeBookRequired
                       ,dt.GradeBookMustPass
                       ,dt.GradeBookSysComponentTypeId
                       ,dt.GradeBookHoursRequired
                       ,dt.GradeBookHoursCompleted
                       ,dt.StuEnrollId
                       ,dt.MinResult
                       ,dt.GradeComponentDescription -- Student data     
                       ,dt.CreditsAttempted
                       ,dt.CreditsEarned
                       ,dt.Completed
                       ,dt.CurrentScore
                       ,dt.CurrentGrade
                       ,dt.FinalScore
                       ,dt.FinalGrade
                       ,dt.CampusId
                       ,dt.CampDescrip
                       ,dt.rownumber
                       ,dt.FirstName
                       ,dt.LastName
                       ,dt.MiddleName
                       ,dt.GrdBkWgtDetailsCount
                       ,dt.ClockHourProgram
                       ,@GradesFormat AS GradesFormat
                       ,@GPAMethod AS GPAMethod
                       ,(
                        SELECT SUM(WorkUnitCount)
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  T.TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId IN ( 501, 544, 502, 499, 503, 500, 533 )
                        ) AS WorkUnitCount
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 501
                        ) AS WorkUnitCount_501
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 544
                        ) AS WorkUnitCount_544
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 502
                        ) AS WorkUnitCount_502
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 499
                        ) AS WorkUnitCount_499
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 503
                        ) AS WorkUnitCount_503
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 500
                        ) AS WorkUnitCount_500
                       ,(
                        SELECT WorkUnitCount
                        FROM   #tempTermWorkUnitCount AS T
                        WHERE  TermId = dt.TermId
                               AND T.ReqId = dt.CourseId
                               AND sysComponentTypeId = 533
                        ) AS WorkUnitCount_533
                       ,ROW_NUMBER() OVER ( PARTITION BY StuEnrollId
                                                        ,TermId
                                                        ,CourseId
                                            ORDER BY TermStartDate
                                                    ,TermEndDate
                                                    ,TermDescription
                                                    ,CourseDescription
                                          ) AS rownumber2
                       ,dt.ClsSectionId
                 FROM   (
                        SELECT     DISTINCT PV.PrgVerId
                                  ,PV.PrgVerDescrip
                                  ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                        ELSE 0
                                   END AS PrgVersionTrackCredits
                                  ,T.TermId
                                  ,T.TermDescrip AS TermDescription
                                  ,T.StartDate AS TermStartDate
                                  ,T.EndDate AS TermEndDate
                                  ,CS.ReqId AS CourseId
                                  ,CS.StartDate AS CouseStartDate
                                  ,R.Code AS CourseCode
                                  ,R.Descrip AS CourseDescription
                                  ,''('' + R.Code + '') '' + R.Descrip AS CourseCodeDescription
                                  ,R.Credits AS CourseCredits
                                  ,R.FinAidCredits AS CourseFinAidCredits
                                  ,(
                                   SELECT MIN(MinVal)
                                   FROM   arGradeScaleDetails GCD
                                         ,arGradeSystemDetails GSD
                                   WHERE  GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                          AND GSD.IsPass = 1
                                          AND GCD.GrdScaleId = CS.GrdScaleId
                                   ) AS MinVal
                                  ,RES.Score AS CourseScore
                                  ,NULL AS GradeBook_ResultId
                                  ,NULL AS GradeBookDescription
                                  ,NULL AS GradeBookScore
                                  ,NULL AS GradeBookPostDate
                                  ,NULL AS GradeBookPassingGrade
                                  ,NULL AS GradeBookWeight
                                  ,NULL AS GradeBookRequired
                                  ,NULL AS GradeBookMustPass
                                  ,NULL AS GradeBookSysComponentTypeId
                                  ,NULL AS GradeBookHoursRequired
                                  ,NULL AS GradeBookHoursCompleted
                                  ,SE.StuEnrollId
                                  ,NULL AS MinResult
                                  ,NULL AS GradeComponentDescription -- Student data     
                                  ,ISNULL(SCS.CreditsAttempted, 0) AS CreditsAttempted
                                  ,ISNULL(SCS.CreditsEarned, 0) AS CreditsEarned
                                  ,SCS.Completed AS Completed
                                  ,SCS.CurrentScore AS CurrentScore
                                  ,SCS.CurrentGrade AS CurrentGrade
                                  ,SCS.FinalScore AS FinalScore
                                  ,SCS.FinalGrade AS FinalGrade
                                  ,C.CampusId
                                  ,C.CampDescrip
                                  ,NULL AS rownumber
                                  ,S.FirstName AS FirstName
                                  ,S.LastName AS LastName
                                  ,S.MiddleName
                                  ,(
                                   SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                   FROM   arGrdBkResults
                                   WHERE  StuEnrollId = SE.StuEnrollId
                                          AND ClsSectionId = RES.TestId
                                   ) AS GrdBkWgtDetailsCount
                                  ,CASE WHEN P.ACId = 5 THEN ''True''
                                        ELSE ''False''
                                   END AS ClockHourProgram
                                  ,CS.ClsSectionId
                        FROM       arClassSections CS
                        INNER JOIN arResults GBR ON CS.ClsSectionId = GBR.TestId
                        INNER JOIN arStuEnrollments SE ON GBR.StuEnrollId = SE.StuEnrollId
                        INNER JOIN (
                                   SELECT StudentId
                                         ,FirstName
                                         ,LastName
                                         ,MiddleName
                                   FROM   adLeads
                                   ) S ON S.StudentId = SE.StudentId
                        INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                        INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                        INNER JOIN arTerm T ON CS.TermId = T.TermId
                        INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                        INNER JOIN arResults RES ON RES.StuEnrollId = GBR.StuEnrollId
                                                    AND RES.TestId = CS.ClsSectionId
                        LEFT JOIN  syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                          AND T.TermId = SCS.TermId
                                                          AND R.ReqId = SCS.ReqId
                        WHERE      SE.StuEnrollId IN (
                                                     SELECT Val
                                                     FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                     )
                                   AND (
                                       @TermId IS NULL
                                       OR T.TermId = @TermId
                                       )
                                   AND R.IsAttendanceOnly = 0
                        UNION
                        SELECT     DISTINCT PV.PrgVerId
                                  ,PV.PrgVerDescrip
                                  ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                        ELSE 0
                                   END AS PrgVersionTrackCredits
                                  ,T.TermId
                                  ,T.TermDescrip
                                  ,T.StartDate
                                  ,T.EndDate
                                  ,GBCR.ReqId
                                  ,T.StartDate AS CouseStartDate     -- tranfered  
                                  ,R.Code AS CourseCode
                                  ,R.Descrip AS CourseDescrip
                                  ,''('' + R.Code + '') '' + R.Descrip AS CourseCodeDescrip
                                  ,R.Credits
                                  ,R.FinAidCredits
                                  ,(
                                   SELECT MIN(MinVal)
                                   FROM   arGradeScaleDetails GCD
                                         ,arGradeSystemDetails GSD
                                   WHERE  GCD.GrdSysDetailId = GSD.GrdSysDetailId
                                          AND GSD.IsPass = 1
                                   )
                                  ,ISNULL(GBCR.Score, 0)
                                  ,NULL
                                  ,NULL
                                  ,NULL
                                  ,NULL
                                  ,NULL
                                  ,NULL
                                  ,NULL
                                  ,NULL
                                  ,NULL
                                  ,NULL
                                  ,NULL
                                  ,SE.StuEnrollId
                                  ,NULL AS MinResult
                                  ,NULL AS GradeComponentDescription -- Student data      
                                  ,ISNULL(SCS.CreditsAttempted, 0) AS CreditsAttempted
                                  ,ISNULL(SCS.CreditsEarned, 0) AS CreditsEarned
                                  ,SCS.Completed AS Completed
                                  ,SCS.CurrentScore AS CurrentScore
                                  ,SCS.CurrentGrade AS CurrentGrade
                                  ,SCS.FinalScore AS FinalScore
                                  ,SCS.FinalGrade AS FinalGrade
                                  ,C.CampusId
                                  ,C.CampDescrip
                                  ,NULL AS rownumber
                                  ,S.FirstName AS FirstName
                                  ,S.LastName AS LastName
                                  ,S.MiddleName
                                  ,(
                                   SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                   FROM   arGrdBkConversionResults
                                   WHERE  StuEnrollId = SE.StuEnrollId
                                          AND TermId = GBCR.TermId
                                          AND ReqId = GBCR.ReqId
                                   ) AS GrdBkWgtDetailsCount
                                  ,CASE WHEN P.ACId = 5 THEN ''True''
                                        ELSE ''False''
                                   END AS ClockHourProgram
                                  ,SCS.ClsSectionId
                        FROM       arTransferGrades GBCR
                        INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                        INNER JOIN (
                                   SELECT StudentId
                                         ,FirstName
                                         ,LastName
                                         ,MiddleName
                                   FROM   adLeads
                                   ) S ON S.StudentId = SE.StudentId
                        INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                        INNER JOIN arPrograms P ON P.ProgId = PV.ProgId
                        INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                        INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                        --INNER JOIN arTransferGrades AR ON GBCR.StuEnrollId = AR.StuEnrollId  
                        LEFT JOIN  syCreditSummary SCS ON SE.StuEnrollId = SCS.StuEnrollId
                                                          AND T.TermId = SCS.TermId
                                                          AND R.ReqId = SCS.ReqId
                        WHERE      SE.StuEnrollId IN (
                                                     SELECT Val
                                                     FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                     )
                                   AND (
                                       @TermId IS NULL
                                       OR T.TermId = @TermId
                                       )
                                   AND R.IsAttendanceOnly = 0
                        ) dt
                 ) dt1
        --WHERE rownumber2<=2  
        ORDER BY TermStartDate
                ,TermEndDate
                -- , TermDescription  
                ,CouseStartDate
                ,CourseCode;
    --, CASE WHEN LTRIM(RTRIM(LOWER(@GradesFormat))) = ''letter''  
    --       THEN (RANK() OVER (ORDER BY FinalGrade DESC))  
    --  END  
    --, CASE WHEN LTRIM(RTRIM(LOWER(@GradesFormat))) <> ''letter''  
    --       THEN (RANK() OVER (ORDER BY FinalScore DESC))  
    --  END;  
    END;
-- ========================================================================================================= 
-- END  --  Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents 
-- ========================================================================================================= 

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_TitleIV_QualitativeAndQuantitative]'
GO
IF OBJECT_ID(N'[dbo].[USP_TitleIV_QualitativeAndQuantitative]', 'P') IS NULL
EXEC sp_executesql N'-- =============================================
-- Author:		<Author,,Edwin Sosa>
-- Create date: <Create Date,,>
-- Description:	<This procedure calculates the qualitative and quantitative values for Title IV by using an offset date. The attendance portion of this procedure
-- removes the makeup hours from the actual hours before storing it into the sy attendance summary table. This differs slightly from the sprocs called in the attendance job to acheive the correct results.
-- When calculating the quantitative value, the makeup hours are added back into the actuals to get a consisten result. This sproc should be refactored to use the same procedures the attendance job uses internally.>
-- =============================================
CREATE PROCEDURE [dbo].[USP_TitleIV_QualitativeAndQuantitative]
    -- Add the parameters for the stored procedure here
    @StuEnrollId VARCHAR(MAX) = NULL
   ,@QuantMinUnitTypeId INTEGER
   ,@OffsetDate DATETIME
   ,@CumAverage DECIMAL(18, 2) OUTPUT
   ,@cumWeightedGPA DECIMAL(18, 2) OUTPUT
   ,@cumSimpleGPA DECIMAL(18, 2) OUTPUT
   ,@Qualitative DECIMAL(18, 2) OUTPUT
   ,@Quantitative DECIMAL(18, 2) OUTPUT
   ,@ActualHours DECIMAL(18, 2) OUTPUT
   ,@ScheduledHours DECIMAL(18, 2) OUTPUT
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @attendanceSummary TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ClsSectionId UNIQUEIDENTIFIER
               ,StudentAttendedDate DATETIME
               ,ScheduledDays DECIMAL(18, 2)
               ,ActualDays DECIMAL(18, 2)
               ,ActualRunningScheduledDays DECIMAL(18, 2)
               ,ActualRunningPresentDays DECIMAL(18, 2)
               ,ActualRunningAbsentDays DECIMAL(18, 2)
               ,ActualRunningMakeupDays DECIMAL(18, 2)
               ,ActualRunningTardyDays DECIMAL(18, 2)
               ,AdjustedPresentDays DECIMAL(18, 2)
               ,AdjustedAbsentDays DECIMAL(18, 2)
               ,AttendanceTrackType VARCHAR(50)
               ,ModUser VARCHAR(50)
               ,ModDate DATETIME
               ,TardiesMakingAbsence INT
            );
        -- Insert statements for procedure here
        DECLARE @ClsSectionIntervalMinutes AS TABLE
            (
                ClsSectionId UNIQUEIDENTIFIER NOT NULL
               ,IntervalInMinutes DECIMAL(18, 2)
            );

        DECLARE @TrackSapAttendance VARCHAR(1000);
        DECLARE @displayHours AS VARCHAR(1000);
        DECLARE @GradeCourseRepetitionsMethod VARCHAR(1000);
        DECLARE @GradesFormat AS VARCHAR(1000);
        DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;

        SET @StuEnrollCampusId = COALESCE((
                                          SELECT ASE.CampusId
                                          FROM   arStuEnrollments AS ASE
                                          WHERE  ASE.StuEnrollId = @StuEnrollId
                                          )
                                         ,NULL
                                         );
        SET @TrackSapAttendance = dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', @StuEnrollCampusId);
        IF ( @TrackSapAttendance IS NOT NULL )
            BEGIN
                SET @TrackSapAttendance = LOWER(LTRIM(RTRIM(@TrackSapAttendance)));
            END;

        SET @GradeCourseRepetitionsMethod = dbo.GetAppSettingValueByKeyName(''GradeCourseRepetitionsMethod'', @StuEnrollCampusId);
        SET @GradesFormat = dbo.GetAppSettingValueByKeyName(''GradesFormat'', @StuEnrollCampusId);
        IF ( @GradesFormat IS NOT NULL )
            BEGIN
                SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
            END;



        DECLARE @CoursesNotRepeated TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,TermId UNIQUEIDENTIFIER
               ,TermDescrip VARCHAR(100)
               ,ReqId UNIQUEIDENTIFIER
               ,ReqDescrip VARCHAR(100)
               ,ClsSectionId VARCHAR(50)
               ,CreditsEarned DECIMAL(18, 2)
               ,CreditsAttempted DECIMAL(18, 2)
               ,CurrentScore DECIMAL(18, 2)
               ,CurrentGrade VARCHAR(10)
               ,FinalScore DECIMAL(18, 2)
               ,FinalGrade VARCHAR(10)
               ,Completed BIT
               ,FinalGPA DECIMAL(18, 2)
               ,Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
               ,Count_WeightedAverage_Credits DECIMAL(18, 2)
               ,Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
               ,Count_SimpleAverage_Credits DECIMAL(18, 2)
               ,ModUser VARCHAR(50)
               ,ModDate DATETIME
               ,TermGPA_Simple DECIMAL(18, 2)
               ,TermGPA_Weighted DECIMAL(18, 2)
               ,coursecredits DECIMAL(18, 2)
               ,CumulativeGPA DECIMAL(18, 2)
               ,CumulativeGPA_Simple DECIMAL(18, 2)
               ,FACreditsEarned DECIMAL(18, 2)
               ,Average DECIMAL(18, 2)
               ,CumAverage DECIMAL(18, 2)
               ,TermStartDate DATETIME
               ,rownumber INT
            );

        INSERT INTO @CoursesNotRepeated
                    SELECT StuEnrollId
                          ,TermId
                          ,TermDescrip
                          ,ReqId
                          ,ReqDescrip
                          ,ClsSectionId
                          ,CreditsEarned
                          ,CreditsAttempted
                          ,CurrentScore
                          ,CurrentGrade
                          ,FinalScore
                          ,FinalGrade
                          ,Completed
                          ,FinalGPA
                          ,Product_WeightedAverage_Credits_GPA
                          ,Count_WeightedAverage_Credits
                          ,Product_SimpleAverage_Credits_GPA
                          ,Count_SimpleAverage_Credits
                          ,ModUser
                          ,ModDate
                          ,TermGPA_Simple
                          ,TermGPA_Weighted
                          ,coursecredits
                          ,CumulativeGPA
                          ,CumulativeGPA_Simple
                          ,FACreditsEarned
                          ,Average
                          ,CumAverage
                          ,TermStartDate
                          ,NULL AS rownumber
                    FROM   (
                           SELECT     sCS.StuEnrollId
                                     ,sCS.TermId
                                     ,sCS.TermDescrip
                                     ,sCS.ReqId
                                     ,sCS.ReqDescrip
                                     ,sCS.ClsSectionId
                                     ,sCS.CreditsEarned
                                     ,sCS.CreditsAttempted
                                     ,sCS.CurrentScore
                                     ,sCS.CurrentGrade
                                     ,sCS.FinalScore
                                     ,sCS.FinalGrade
                                     ,sCS.Completed
                                     ,sCS.FinalGPA
                                     ,sCS.Product_WeightedAverage_Credits_GPA
                                     ,sCS.Count_WeightedAverage_Credits
                                     ,sCS.Product_SimpleAverage_Credits_GPA
                                     ,sCS.Count_SimpleAverage_Credits
                                     ,sCS.ModUser
                                     ,sCS.ModDate
                                     ,sCS.TermGPA_Simple
                                     ,sCS.TermGPA_Weighted
                                     ,sCS.coursecredits
                                     ,sCS.CumulativeGPA
                                     ,sCS.CumulativeGPA_Simple
                                     ,sCS.FACreditsEarned
                                     ,sCS.Average
                                     ,sCS.CumAverage
                                     ,sCS.TermStartDate
                           FROM       dbo.syCreditSummary sCS
                           INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                           INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                           WHERE      sCS.StuEnrollId = @StuEnrollId
                                      AND CS.ReqId = sCS.ReqId
                                      AND (
                                          R.DateCompleted IS NOT NULL
                                          AND R.DateCompleted <= @OffsetDate
                                          )
                           UNION ALL
                           (SELECT     sCS.StuEnrollId
                                      ,sCS.TermId
                                      ,sCS.TermDescrip
                                      ,sCS.ReqId
                                      ,sCS.ReqDescrip
                                      ,sCS.ClsSectionId
                                      ,sCS.CreditsEarned
                                      ,sCS.CreditsAttempted
                                      ,sCS.CurrentScore
                                      ,sCS.CurrentGrade
                                      ,sCS.FinalScore
                                      ,sCS.FinalGrade
                                      ,sCS.Completed
                                      ,sCS.FinalGPA
                                      ,sCS.Product_WeightedAverage_Credits_GPA
                                      ,sCS.Count_WeightedAverage_Credits
                                      ,sCS.Product_SimpleAverage_Credits_GPA
                                      ,sCS.Count_SimpleAverage_Credits
                                      ,sCS.ModUser
                                      ,sCS.ModDate
                                      ,sCS.TermGPA_Simple
                                      ,sCS.TermGPA_Weighted
                                      ,sCS.coursecredits
                                      ,sCS.CumulativeGPA
                                      ,sCS.CumulativeGPA_Simple
                                      ,sCS.FACreditsEarned
                                      ,sCS.Average
                                      ,sCS.CumAverage
                                      ,sCS.TermStartDate
                            FROM       dbo.syCreditSummary sCS
                            INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                            WHERE      sCS.StuEnrollId = @StuEnrollId
                                       AND tG.ReqId = sCS.ReqId
                                       AND (
                                           tG.CompletedDate IS NOT NULL
                                           AND tG.CompletedDate <= @OffsetDate
                                           ))
                           ) sCSEA
                    WHERE  StuEnrollId = @StuEnrollId
                           AND ReqId IN (
                                        SELECT ReqId
                                        FROM   (
                                               SELECT   ReqId
                                                       ,ReqDescrip
                                                       ,COUNT(*) AS counter
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                               GROUP BY ReqId
                                                       ,ReqDescrip
                                               HAVING   COUNT(*) = 1
                                               ) dt
                                        );



        IF LOWER(@GradeCourseRepetitionsMethod) = ''latest''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT   dt2.StuEnrollId
                                    ,dt2.TermId
                                    ,dt2.TermDescrip
                                    ,dt2.ReqId
                                    ,dt2.ReqDescrip
                                    ,dt2.ClsSectionId
                                    ,dt2.CreditsEarned
                                    ,dt2.CreditsAttempted
                                    ,dt2.CurrentScore
                                    ,dt2.CurrentGrade
                                    ,dt2.FinalScore
                                    ,dt2.FinalGrade
                                    ,dt2.Completed
                                    ,dt2.FinalGPA
                                    ,dt2.Product_WeightedAverage_Credits_GPA
                                    ,dt2.Count_WeightedAverage_Credits
                                    ,dt2.Product_SimpleAverage_Credits_GPA
                                    ,dt2.Count_SimpleAverage_Credits
                                    ,dt2.ModUser
                                    ,dt2.ModDate
                                    ,dt2.TermGPA_Simple
                                    ,dt2.TermGPA_Weighted
                                    ,dt2.coursecredits
                                    ,dt2.CumulativeGPA
                                    ,dt2.CumulativeGPA_Simple
                                    ,dt2.FACreditsEarned
                                    ,dt2.Average
                                    ,dt2.CumAverage
                                    ,dt2.TermStartDate
                                    ,dt2.RowNumber
                            FROM     (
                                     SELECT StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                                ORDER BY TermStartDate DESC
                                                              ) AS RowNumber
                                     FROM   (
                                            SELECT     sCS.StuEnrollId
                                                      ,sCS.TermId
                                                      ,sCS.TermDescrip
                                                      ,sCS.ReqId
                                                      ,sCS.ReqDescrip
                                                      ,sCS.ClsSectionId
                                                      ,sCS.CreditsEarned
                                                      ,sCS.CreditsAttempted
                                                      ,sCS.CurrentScore
                                                      ,sCS.CurrentGrade
                                                      ,sCS.FinalScore
                                                      ,sCS.FinalGrade
                                                      ,sCS.Completed
                                                      ,sCS.FinalGPA
                                                      ,sCS.Product_WeightedAverage_Credits_GPA
                                                      ,sCS.Count_WeightedAverage_Credits
                                                      ,sCS.Product_SimpleAverage_Credits_GPA
                                                      ,sCS.Count_SimpleAverage_Credits
                                                      ,sCS.ModUser
                                                      ,sCS.ModDate
                                                      ,sCS.TermGPA_Simple
                                                      ,sCS.TermGPA_Weighted
                                                      ,sCS.coursecredits
                                                      ,sCS.CumulativeGPA
                                                      ,sCS.CumulativeGPA_Simple
                                                      ,sCS.FACreditsEarned
                                                      ,sCS.Average
                                                      ,sCS.CumAverage
                                                      ,sCS.TermStartDate
                                            FROM       dbo.syCreditSummary sCS
                                            INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                            INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                            WHERE      sCS.StuEnrollId = @StuEnrollId
                                                       AND CS.ReqId = sCS.ReqId
                                                       AND (
                                                           R.DateCompleted IS NOT NULL
                                                           AND R.DateCompleted <= @OffsetDate
                                                           )
                                            UNION ALL
                                            (SELECT     sCS.StuEnrollId
                                                       ,sCS.TermId
                                                       ,sCS.TermDescrip
                                                       ,sCS.ReqId
                                                       ,sCS.ReqDescrip
                                                       ,sCS.ClsSectionId
                                                       ,sCS.CreditsEarned
                                                       ,sCS.CreditsAttempted
                                                       ,sCS.CurrentScore
                                                       ,sCS.CurrentGrade
                                                       ,sCS.FinalScore
                                                       ,sCS.FinalGrade
                                                       ,sCS.Completed
                                                       ,sCS.FinalGPA
                                                       ,sCS.Product_WeightedAverage_Credits_GPA
                                                       ,sCS.Count_WeightedAverage_Credits
                                                       ,sCS.Product_SimpleAverage_Credits_GPA
                                                       ,sCS.Count_SimpleAverage_Credits
                                                       ,sCS.ModUser
                                                       ,sCS.ModDate
                                                       ,sCS.TermGPA_Simple
                                                       ,sCS.TermGPA_Weighted
                                                       ,sCS.coursecredits
                                                       ,sCS.CumulativeGPA
                                                       ,sCS.CumulativeGPA_Simple
                                                       ,sCS.FACreditsEarned
                                                       ,sCS.Average
                                                       ,sCS.CumAverage
                                                       ,sCS.TermStartDate
                                             FROM       dbo.syCreditSummary sCS
                                             INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                             WHERE      sCS.StuEnrollId = @StuEnrollId
                                                        AND tG.ReqId = sCS.ReqId
                                                        AND (
                                                            tG.CompletedDate IS NOT NULL
                                                            AND tG.CompletedDate <= @OffsetDate
                                                            ))
                                            ) sCSEA
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND (
                                                FinalScore IS NOT NULL
                                                OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN (
                                                         SELECT ReqId
                                                         FROM   (
                                                                SELECT   ReqId
                                                                        ,ReqDescrip
                                                                        ,COUNT(*) AS Counter
                                                                FROM     syCreditSummary
                                                                WHERE    StuEnrollId = @StuEnrollId
                                                                GROUP BY ReqId
                                                                        ,ReqDescrip
                                                                HAVING   COUNT(*) > 1
                                                                ) dt
                                                         )
                                     ) dt2
                            WHERE    RowNumber = 1
                            ORDER BY TermStartDate DESC;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''best''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                            FROM   (
                                   SELECT StuEnrollId
                                         ,TermId
                                         ,TermDescrip
                                         ,ReqId
                                         ,ReqDescrip
                                         ,ClsSectionId
                                         ,CreditsEarned
                                         ,CreditsAttempted
                                         ,CurrentScore
                                         ,CurrentGrade
                                         ,FinalScore
                                         ,FinalGrade
                                         ,Completed
                                         ,FinalGPA
                                         ,Product_WeightedAverage_Credits_GPA
                                         ,Count_WeightedAverage_Credits
                                         ,Product_SimpleAverage_Credits_GPA
                                         ,Count_SimpleAverage_Credits
                                         ,ModUser
                                         ,ModDate
                                         ,TermGPA_Simple
                                         ,TermGPA_Weighted
                                         ,coursecredits
                                         ,CumulativeGPA
                                         ,CumulativeGPA_Simple
                                         ,FACreditsEarned
                                         ,Average
                                         ,CumAverage
                                         ,TermStartDate
                                         ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                              ORDER BY Completed DESC
                                                                      ,CreditsEarned DESC
                                                                      ,FinalGPA DESC
                                                            ) AS RowNumber
                                   FROM   (
                                          SELECT     sCS.StuEnrollId
                                                    ,sCS.TermId
                                                    ,sCS.TermDescrip
                                                    ,sCS.ReqId
                                                    ,sCS.ReqDescrip
                                                    ,sCS.ClsSectionId
                                                    ,sCS.CreditsEarned
                                                    ,sCS.CreditsAttempted
                                                    ,sCS.CurrentScore
                                                    ,sCS.CurrentGrade
                                                    ,sCS.FinalScore
                                                    ,sCS.FinalGrade
                                                    ,sCS.Completed
                                                    ,sCS.FinalGPA
                                                    ,sCS.Product_WeightedAverage_Credits_GPA
                                                    ,sCS.Count_WeightedAverage_Credits
                                                    ,sCS.Product_SimpleAverage_Credits_GPA
                                                    ,sCS.Count_SimpleAverage_Credits
                                                    ,sCS.ModUser
                                                    ,sCS.ModDate
                                                    ,sCS.TermGPA_Simple
                                                    ,sCS.TermGPA_Weighted
                                                    ,sCS.coursecredits
                                                    ,sCS.CumulativeGPA
                                                    ,sCS.CumulativeGPA_Simple
                                                    ,sCS.FACreditsEarned
                                                    ,sCS.Average
                                                    ,sCS.CumAverage
                                                    ,sCS.TermStartDate
                                          FROM       dbo.syCreditSummary sCS
                                          INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                          INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                          WHERE      sCS.StuEnrollId = @StuEnrollId
                                                     AND CS.ReqId = sCS.ReqId
                                                     AND (
                                                         R.DateCompleted IS NOT NULL
                                                         AND R.DateCompleted <= @OffsetDate
                                                         )
                                          UNION ALL
                                          (SELECT     sCS.StuEnrollId
                                                     ,sCS.TermId
                                                     ,sCS.TermDescrip
                                                     ,sCS.ReqId
                                                     ,sCS.ReqDescrip
                                                     ,sCS.ClsSectionId
                                                     ,sCS.CreditsEarned
                                                     ,sCS.CreditsAttempted
                                                     ,sCS.CurrentScore
                                                     ,sCS.CurrentGrade
                                                     ,sCS.FinalScore
                                                     ,sCS.FinalGrade
                                                     ,sCS.Completed
                                                     ,sCS.FinalGPA
                                                     ,sCS.Product_WeightedAverage_Credits_GPA
                                                     ,sCS.Count_WeightedAverage_Credits
                                                     ,sCS.Product_SimpleAverage_Credits_GPA
                                                     ,sCS.Count_SimpleAverage_Credits
                                                     ,sCS.ModUser
                                                     ,sCS.ModDate
                                                     ,sCS.TermGPA_Simple
                                                     ,sCS.TermGPA_Weighted
                                                     ,sCS.coursecredits
                                                     ,sCS.CumulativeGPA
                                                     ,sCS.CumulativeGPA_Simple
                                                     ,sCS.FACreditsEarned
                                                     ,sCS.Average
                                                     ,sCS.CumAverage
                                                     ,sCS.TermStartDate
                                           FROM       dbo.syCreditSummary sCS
                                           INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                           WHERE      sCS.StuEnrollId = @StuEnrollId
                                                      AND tG.ReqId = sCS.ReqId
                                                      AND (
                                                          tG.CompletedDate IS NOT NULL
                                                          AND tG.CompletedDate <= @OffsetDate
                                                          ))
                                          ) sCSEA
                                   WHERE  StuEnrollId = @StuEnrollId
                                          AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                              )
                                          AND ReqId IN (
                                                       SELECT ReqId
                                                       FROM   (
                                                              SELECT   ReqId
                                                                      ,ReqDescrip
                                                                      ,COUNT(*) AS Counter
                                                              FROM     syCreditSummary
                                                              WHERE    StuEnrollId = @StuEnrollId
                                                              GROUP BY ReqId
                                                                      ,ReqDescrip
                                                              HAVING   COUNT(*) > 1
                                                              ) dt
                                                       )
                                   ) dt2
                            WHERE  RowNumber = 1;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''average''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                            FROM   (
                                   SELECT StuEnrollId
                                         ,TermId
                                         ,TermDescrip
                                         ,ReqId
                                         ,ReqDescrip
                                         ,ClsSectionId
                                         ,CreditsEarned
                                         ,CreditsAttempted
                                         ,CurrentScore
                                         ,CurrentGrade
                                         ,FinalScore
                                         ,FinalGrade
                                         ,Completed
                                         ,FinalGPA
                                         ,Product_WeightedAverage_Credits_GPA
                                         ,Count_WeightedAverage_Credits
                                         ,Product_SimpleAverage_Credits_GPA
                                         ,Count_SimpleAverage_Credits
                                         ,ModUser
                                         ,ModDate
                                         ,TermGPA_Simple
                                         ,TermGPA_Weighted
                                         ,coursecredits
                                         ,CumulativeGPA
                                         ,CumulativeGPA_Simple
                                         ,FACreditsEarned
                                         ,Average
                                         ,CumAverage
                                         ,TermStartDate
                                         ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                              ORDER BY FinalGPA DESC
                                                            ) AS RowNumber
                                   FROM   (
                                          SELECT     sCS.StuEnrollId
                                                    ,sCS.TermId
                                                    ,sCS.TermDescrip
                                                    ,sCS.ReqId
                                                    ,sCS.ReqDescrip
                                                    ,sCS.ClsSectionId
                                                    ,sCS.CreditsEarned
                                                    ,sCS.CreditsAttempted
                                                    ,sCS.CurrentScore
                                                    ,sCS.CurrentGrade
                                                    ,sCS.FinalScore
                                                    ,sCS.FinalGrade
                                                    ,sCS.Completed
                                                    ,sCS.FinalGPA
                                                    ,sCS.Product_WeightedAverage_Credits_GPA
                                                    ,sCS.Count_WeightedAverage_Credits
                                                    ,sCS.Product_SimpleAverage_Credits_GPA
                                                    ,sCS.Count_SimpleAverage_Credits
                                                    ,sCS.ModUser
                                                    ,sCS.ModDate
                                                    ,sCS.TermGPA_Simple
                                                    ,sCS.TermGPA_Weighted
                                                    ,sCS.coursecredits
                                                    ,sCS.CumulativeGPA
                                                    ,sCS.CumulativeGPA_Simple
                                                    ,sCS.FACreditsEarned
                                                    ,sCS.Average
                                                    ,sCS.CumAverage
                                                    ,sCS.TermStartDate
                                          FROM       dbo.syCreditSummary sCS
                                          INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                          INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                          WHERE      sCS.StuEnrollId = @StuEnrollId
                                                     AND CS.ReqId = sCS.ReqId
                                                     AND (
                                                         R.DateCompleted IS NOT NULL
                                                         AND R.DateCompleted <= @OffsetDate
                                                         )
                                          UNION ALL
                                          (SELECT     sCS.StuEnrollId
                                                     ,sCS.TermId
                                                     ,sCS.TermDescrip
                                                     ,sCS.ReqId
                                                     ,sCS.ReqDescrip
                                                     ,sCS.ClsSectionId
                                                     ,sCS.CreditsEarned
                                                     ,sCS.CreditsAttempted
                                                     ,sCS.CurrentScore
                                                     ,sCS.CurrentGrade
                                                     ,sCS.FinalScore
                                                     ,sCS.FinalGrade
                                                     ,sCS.Completed
                                                     ,sCS.FinalGPA
                                                     ,sCS.Product_WeightedAverage_Credits_GPA
                                                     ,sCS.Count_WeightedAverage_Credits
                                                     ,sCS.Product_SimpleAverage_Credits_GPA
                                                     ,sCS.Count_SimpleAverage_Credits
                                                     ,sCS.ModUser
                                                     ,sCS.ModDate
                                                     ,sCS.TermGPA_Simple
                                                     ,sCS.TermGPA_Weighted
                                                     ,sCS.coursecredits
                                                     ,sCS.CumulativeGPA
                                                     ,sCS.CumulativeGPA_Simple
                                                     ,sCS.FACreditsEarned
                                                     ,sCS.Average
                                                     ,sCS.CumAverage
                                                     ,sCS.TermStartDate
                                           FROM       dbo.syCreditSummary sCS
                                           INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                           WHERE      sCS.StuEnrollId = @StuEnrollId
                                                      AND tG.ReqId = sCS.ReqId
                                                      AND (
                                                          tG.CompletedDate IS NOT NULL
                                                          AND tG.CompletedDate <= @OffsetDate
                                                          ))
                                          ) sCSEA
                                   WHERE  StuEnrollId = @StuEnrollId
                                          AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                              )
                                          AND ReqId IN (
                                                       SELECT ReqId
                                                       FROM   (
                                                              SELECT   ReqId
                                                                      ,ReqDescrip
                                                                      ,COUNT(*) AS Counter
                                                              FROM     syCreditSummary
                                                              WHERE    StuEnrollId = @StuEnrollId
                                                              GROUP BY ReqId
                                                                      ,ReqDescrip
                                                              HAVING   COUNT(*) > 1
                                                              ) dt
                                                       )
                                   ) dt2;
            END;


        DECLARE @cumSimpleCourseCredits DECIMAL(18, 2);
        DECLARE @cumSimple_GPA_Credits DECIMAL(18, 2);
        -- (OUTPUT parameter)  DECLARE @cumSimpleGPA DECIMAL(18, 2)



        DECLARE @cumCourseCredits DECIMAL(18, 2)
               ,@cumWeighted_GPA_Credits DECIMAL(18, 2);
        DECLARE @cumCourseCredits_repeated DECIMAL(18, 2)
               ,@cumWeighted_GPA_Credits_repeated DECIMAL(18, 2);

        PRINT @GradeCourseRepetitionsMethod;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''average''
            BEGIN
                SET @cumWeightedGPA = 0;
                SET @cumSimpleGPA = 0;

                SET @cumSimpleCourseCredits = (
                                              SELECT COUNT(*)
                                              FROM   @CoursesNotRepeated
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND FinalGPA IS NOT NULL
                                                     AND (
                                                         rownumber = 0
                                                         OR rownumber IS NULL
                                                         )
                                              );

                DECLARE @cumSimpleCourseCredits_repeated DECIMAL(18, 2);
                SET @cumSimpleCourseCredits_repeated = (
                                                       SELECT SUM(CourseCreditCount)
                                                       FROM   (
                                                              SELECT   ReqId
                                                                      ,COUNT(coursecredits) AS CourseCreditCount
                                                              FROM     @CoursesNotRepeated
                                                              WHERE    StuEnrollId IN ( @StuEnrollId )
                                                                       AND FinalGPA IS NOT NULL
                                                                       AND rownumber = 1
                                                              GROUP BY ReqId
                                                              ) dt
                                                       );
                SET @cumSimpleCourseCredits = @cumSimpleCourseCredits + ISNULL(@cumSimpleCourseCredits_repeated, 0);

                DECLARE @cumSimple_GPA_Credits_repeated DECIMAL(18, 2);

                SET @cumSimple_GPA_Credits = (
                                             SELECT SUM(FinalGPA)
                                             FROM   @CoursesNotRepeated
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                                    AND (
                                                        rownumber = 0
                                                        OR rownumber IS NULL
                                                        )
                                             );

                SET @cumSimple_GPA_Credits_repeated = (
                                                      SELECT SUM(AverageGPA)
                                                      FROM   (
                                                             SELECT   ReqId
                                                                     ,SUM(FinalGPA) / MAX(rownumber) AS AverageGPA
                                                             FROM     @CoursesNotRepeated
                                                             WHERE    StuEnrollId IN ( @StuEnrollId )
                                                                      AND FinalGPA IS NOT NULL
                                                                      AND rownumber >= 1
                                                             GROUP BY ReqId
                                                             ) dt
                                                      );

                SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits + ISNULL(@cumSimple_GPA_Credits_repeated, 0.00);

                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END;

                SET @cumCourseCredits = (
                                        SELECT SUM(coursecredits)
                                        FROM   @CoursesNotRepeated
                                        WHERE  StuEnrollId IN ( @StuEnrollId )
                                               AND FinalGPA IS NOT NULL
                                               AND (
                                                   rownumber = 0
                                                   OR rownumber IS NULL
                                                   )
                                        );

                SET @cumCourseCredits_repeated = (
                                                 SELECT SUM(CourseCreditCount)
                                                 FROM   (
                                                        SELECT   ReqId
                                                                ,MAX(coursecredits) AS CourseCreditCount
                                                        FROM     @CoursesNotRepeated
                                                        WHERE    StuEnrollId IN ( @StuEnrollId )
                                                                 AND FinalGPA IS NOT NULL
                                                                 AND rownumber >= 1
                                                        GROUP BY ReqId
                                                        ) dt
                                                 );
                SET @cumCourseCredits = @cumCourseCredits + ISNULL(@cumCourseCredits_repeated, 0.00);

                PRINT @cumCourseCredits;

                SET @cumWeighted_GPA_Credits = (
                                               SELECT SUM(coursecredits * FinalGPA)
                                               FROM   @CoursesNotRepeated
                                               WHERE  StuEnrollId IN ( @StuEnrollId )
                                                      AND FinalGPA IS NOT NULL
                                                      AND (
                                                          rownumber = 0
                                                          OR rownumber IS NULL
                                                          )
                                               );
                SET @cumWeighted_GPA_Credits_repeated = (
                                                        SELECT SUM(CourseCreditCount)
                                                        FROM   (
                                                               SELECT   ReqId
                                                                       ,SUM(coursecredits * FinalGPA) / MAX(rownumber) AS CourseCreditCount
                                                               FROM     @CoursesNotRepeated
                                                               WHERE    StuEnrollId IN ( @StuEnrollId )
                                                                        AND FinalGPA IS NOT NULL
                                                                        AND rownumber >= 1
                                                               GROUP BY ReqId
                                                               ) dt
                                                        );



                SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits + ISNULL(@cumWeighted_GPA_Credits_repeated, 0);
                PRINT @cumWeighted_GPA_Credits;

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END;
            END;
        ELSE
            BEGIN
                SET @cumSimpleGPA = 0;

                SET @cumSimpleCourseCredits = (
                                              SELECT COUNT(*)
                                              FROM   @CoursesNotRepeated
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND FinalGPA IS NOT NULL
                                              );
                SET @cumSimple_GPA_Credits = (
                                             SELECT SUM(FinalGPA)
                                             FROM   @CoursesNotRepeated
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                             );
                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END;
                SET @cumWeightedGPA = 0;
                SET @cumCourseCredits = (
                                        SELECT SUM(coursecredits)
                                        FROM   @CoursesNotRepeated
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND FinalGPA IS NOT NULL
                                        );
                SET @cumWeighted_GPA_Credits = (
                                               SELECT SUM(coursecredits * FinalGPA)
                                               FROM   @CoursesNotRepeated
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalGPA IS NOT NULL
                                               );

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END;

            END;





        IF @cumCourseCredits >= 1
            BEGIN
                SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
            END;


        DECLARE @TermAverageCount INT;
        DECLARE @TermAverage DECIMAL(18, 2);
        -- (output parameter)  DECLARE @CumAverage DECIMAL(18, 2)
        IF @GradesFormat <> ''letter''
           OR @QuantMinUnitTypeId = 3
            BEGIN

                DECLARE @termAverageSum DECIMAL(18, 2)
                       ,@cumAverageSum DECIMAL(18, 2)
                       ,@cumAveragecount INT;
                DECLARE @TardiesMakingAbsence INT
                       ,@UnitTypeDescrip VARCHAR(20)
                       ,@OriginalTardiesMakingAbsence INT;
                SET @TardiesMakingAbsence = (
                                            SELECT TOP 1 t1.TardiesMakingAbsence
                                            FROM   arPrgVersions t1
                                                  ,arStuEnrollments t2
                                            WHERE  t1.PrgVerId = t2.PrgVerId
                                                   AND t2.StuEnrollId = @StuEnrollId
                                            );

                SET @UnitTypeDescrip = (
                                       SELECT     LTRIM(RTRIM(AAUT.UnitTypeDescrip))
                                       FROM       arAttUnitType AS AAUT
                                       INNER JOIN arPrgVersions AS APV ON APV.UnitTypeId = AAUT.UnitTypeId
                                       INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                                       WHERE      ASE.StuEnrollId = @StuEnrollId
                                       );

                SET @OriginalTardiesMakingAbsence = (
                                                    SELECT TOP 1 tardiesmakingabsence
                                                    FROM   syStudentAttendanceSummary
                                                    WHERE  StuEnrollId = @StuEnrollId
                                                    );
                DECLARE @termstartdate1 DATETIME;

                -- Declare Variables
                BEGIN -- Declare Variables
                    DECLARE @MeetDate DATETIME
                           ,@WeekDay VARCHAR(15)
                           ,@StartDate DATETIME
                           ,@EndDate DATETIME;
                    DECLARE @PeriodDescrip VARCHAR(50)
                           ,@Actual DECIMAL(18, 2)
                           ,@Excused DECIMAL(18, 2)
                           ,@ClsSectionId UNIQUEIDENTIFIER;
                    DECLARE @Absent DECIMAL(18, 2)
                           ,@SchedHours DECIMAL(18, 2)
                           ,@TardyMinutes DECIMAL(18, 2);
                    DECLARE @tardy DECIMAL(18, 2)
                           ,@tracktardies INT
                           ,@rownumber INT
                           ,@IsTardy BIT
                           ,@ActualRunningScheduledHours DECIMAL(18, 2);
                    DECLARE @ActualRunningPresentHours DECIMAL(18, 2)
                           ,@ActualRunningAbsentHours DECIMAL(18, 2)
                           ,@ActualRunningTardyHours DECIMAL(18, 2)
                           ,@ActualRunningMakeupHours DECIMAL(18, 2);
                    DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
                           ,@intTardyBreakPoint INT
                           ,@AdjustedRunningPresentHours DECIMAL(18, 2)
                           ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
                           ,@ActualRunningScheduledDays DECIMAL(18, 2);
                    --declare @Scheduledhours decimal(18,2),@ActualPresentDays_ConvertTo_Hours decimal(18,2),@ActualAbsentDays_ConvertTo_Hours decimal(18,2),@AttendanceTrack varchar(50)
                    DECLARE @TermId VARCHAR(50)
                           ,@TermDescrip VARCHAR(50)
                           ,@PrgVerId UNIQUEIDENTIFIER;
                    DECLARE @TardyHit VARCHAR(10)
                           ,@ScheduledMinutes DECIMAL(18, 2);
                    DECLARE @Scheduledhours_noperiods DECIMAL(18, 2)
                           ,@ActualPresentDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2)
                           ,@ActualAbsentDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2);
                    DECLARE @ActualTardyDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2);
                    DECLARE @boolReset BIT
                           ,@MakeupHours DECIMAL(18, 2)
                           ,@AdjustedPresentDaysComputed DECIMAL(18, 2);
                END;

                -- Step 3  --  InsertAttendance_Class_PresentAbsent   -- UnitTypeDescrip = (''None'', ''Present Absent'') 
                --         --  TrackSapAttendance = ''byclass''
                BEGIN -- Step 3  --  InsertAttendance_Class_PresentAbsent   -- UnitTypeDescrip = (''None'', ''Present Absent'') 
                    --         --  TrackSapAttendance = ''byclass''
                    IF @UnitTypeDescrip IN ( ''none'', ''present absent'' )
                       AND @TrackSapAttendance = ''byclass''
                        BEGIN
                            PRINT ''Present absent, by class'';
                            ---- PRINT ''Step1!!!!''
                            --BEGIN
                            --    --DELETE FROM syStudentAttendanceSummary
                            --    --WHERE StuEnrollId = @StuEnrollId
                            --    --      AND StudentAttendedDate <= @OffsetDate;
                            --END;
                            INSERT INTO @ClsSectionIntervalMinutes (
                                                                   ClsSectionId
                                                                  ,IntervalInMinutes
                                                                   )
                                        SELECT     DISTINCT t1.ClsSectionId
                                                           ,DATEDIFF(MI, t6.TimeIntervalDescrip, t7.TimeIntervalDescrip)
                                        FROM       atClsSectAttendance t1
                                        INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                        INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                        INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                        INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                           AND (
                                                                               CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                               AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                               )
                                                                           AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                        INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                        INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                        INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                        INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                        INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                        INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                        WHERE      t2.StuEnrollId = @StuEnrollId
                                                   AND (
                                                       AAUT1.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                                       OR AAUT2.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                                       );

                            DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
                                SELECT   *
                                        ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                FROM     (
                                         SELECT     DISTINCT t1.StuEnrollId
                                                            ,t1.ClsSectionId
                                                            ,t1.MeetDate
                                                            ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                            ,t4.StartDate
                                                            ,t4.EndDate
                                                            ,t5.PeriodDescrip
                                                            ,t1.Actual
                                                            ,t1.Excused
                                                            ,CASE WHEN (
                                                                       t1.Actual = 0
                                                                       AND t1.Excused = 0
                                                                       ) THEN t1.Scheduled
                                                                  ELSE CASE WHEN (
                                                                                 t1.Actual <> 9999.00
                                                                                 AND t1.Actual < t1.Scheduled
                                                                                 --AND t1.Excused <> 1
                                                                                 ) THEN ( t1.Scheduled - t1.Actual )
                                                                            ELSE 0
                                                                       END
                                                             END AS Absent
                                                            ,t1.Scheduled AS ScheduledMinutes
                                                            ,CASE WHEN (
                                                                       t1.Actual > 0
                                                                       AND t1.Actual < t1.Scheduled
                                                                       ) THEN ( t1.Scheduled - t1.Actual )
                                                                  ELSE 0
                                                             END AS TardyMinutes
                                                            ,t1.Tardy AS Tardy
                                                            ,t3.TrackTardies
                                                            ,t3.TardiesMakingAbsence
                                                            ,t3.PrgVerId
                                         FROM       atClsSectAttendance t1
                                         INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                         INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                         INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                         INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                            AND (
                                                                                CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                                AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                                )
                                                                            AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                         INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                         INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                         INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                         INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                         INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                         INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                         WHERE      t2.StuEnrollId = @StuEnrollId
                                                    AND (
                                                        AAUT1.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                                        OR AAUT2.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                                        )
                                                    AND t1.Actual <> 9999
                                                    AND t1.MeetDate <= @OffsetDate
                                         ) dt
                                ORDER BY StuEnrollId
                                        ,MeetDate;
                            OPEN GetAttendance_Cursor;
                            FETCH NEXT FROM GetAttendance_Cursor
                            INTO @StuEnrollId
                                ,@ClsSectionId
                                ,@MeetDate
                                ,@WeekDay
                                ,@StartDate
                                ,@EndDate
                                ,@PeriodDescrip
                                ,@Actual
                                ,@Excused
                                ,@Absent
                                ,@ScheduledMinutes
                                ,@TardyMinutes
                                ,@tardy
                                ,@tracktardies
                                ,@TardiesMakingAbsence
                                ,@PrgVerId
                                ,@rownumber;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @boolReset = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                            SET @boolReset = 1;
                                        END;


                                    -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                                    IF @Actual <> 9999.00
                                        BEGIN
                                            -- Commented by Balaji on 2.6.2015 as Excused Absence is still considered an absence
                                            -- @Excused is not added to Present Hours
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual; -- + @Excused
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; -- + @Excused

                                            -- If there are make up hrs deduct that otherwise it will be added again in progress report
                                            IF (
                                               @Actual > 0
                                               AND @Actual > @ScheduledMinutes
                                               AND @Actual <> 9999.00
                                               )
                                                BEGIN
                                                    SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                END;

                                            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                                        END;

                                    -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                                    SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                                    -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                                    IF (
                                       @Actual > 0
                                       AND @Actual < @ScheduledMinutes
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                        END;
                                    ELSE IF (
                                            @Actual = 1
                                            AND @Actual = @ScheduledMinutes
                                            AND @tardy = 1
                                            )
                                             BEGIN
                                                 SET @ActualRunningTardyHours += 1;
                                             END;

                                    -- Track how many days student has been tardy only when 
                                    -- program version requires to track tardy
                                    IF (
                                       @tracktardies = 1
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;


                                    -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
                                    -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
                                    -- when student is tardy the second time, that second occurance will be considered as
                                    -- absence
                                    -- Variable @intTardyBreakpoint tracks how many times the student was tardy
                                    -- Variable @tardiesMakingAbsence tracks the tardy rule
                                    IF (
                                       @tracktardies = 1
                                       AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
                                            SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;

                                    -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                       @Actual > 0
                                       AND @Actual > @ScheduledMinutes
                                       AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;

                                    IF ( @tracktardies = 1 )
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                                        END;

                                    ---- PRINT @MeetDate
                                    ---- PRINT @ActualRunningAbsentHours



                                    INSERT INTO @attendanceSummary (
                                                                   StuEnrollId
                                                                  ,ClsSectionId
                                                                  ,StudentAttendedDate
                                                                  ,ScheduledDays
                                                                  ,ActualDays
                                                                  ,ActualRunningScheduledDays
                                                                  ,ActualRunningPresentDays
                                                                  ,ActualRunningAbsentDays
                                                                  ,ActualRunningMakeupDays
                                                                  ,ActualRunningTardyDays
                                                                  ,AdjustedPresentDays
                                                                  ,AdjustedAbsentDays
                                                                  ,AttendanceTrackType
                                                                  ,ModUser
                                                                  ,ModDate
                                                                  ,TardiesMakingAbsence
                                                                   )
                                    VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays
                                            ,@ActualRunningPresentHours, @ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours
                                            ,@AdjustedPresentDaysComputed, @AdjustedRunningAbsentHours, ''Post Attendance by Class Min'', ''sa'', GETDATE()
                                            ,@TardiesMakingAbsence );

                                    --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                                    SET @PrevStuEnrollId = @StuEnrollId;

                                    FETCH NEXT FROM GetAttendance_Cursor
                                    INTO @StuEnrollId
                                        ,@ClsSectionId
                                        ,@MeetDate
                                        ,@WeekDay
                                        ,@StartDate
                                        ,@EndDate
                                        ,@PeriodDescrip
                                        ,@Actual
                                        ,@Excused
                                        ,@Absent
                                        ,@ScheduledMinutes
                                        ,@TardyMinutes
                                        ,@tardy
                                        ,@tracktardies
                                        ,@TardiesMakingAbsence
                                        ,@PrgVerId
                                        ,@rownumber;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;

                            DECLARE @MyTardyTable TABLE
                                (
                                    ClsSectionId UNIQUEIDENTIFIER
                                   ,TardiesMakingAbsence INT
                                   ,AbsentHours DECIMAL(18, 2)
                                );

                            INSERT INTO @MyTardyTable
                                        SELECT     ClsSectionId
                                                  ,PV.TardiesMakingAbsence
                                                  ,CASE WHEN ( COUNT(*) >= PV.TardiesMakingAbsence ) THEN ( COUNT(*) / PV.TardiesMakingAbsence )
                                                        ELSE 0
                                                   END AS AbsentHours
                                        --Count(*) as NumberofTimesTardy 
                                        FROM       dbo.syStudentAttendanceSummary SAS
                                        INNER JOIN arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                                        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                        WHERE      SE.StuEnrollId = @StuEnrollId
                                                   AND SAS.IsTardy = 1
                                                   AND PV.TrackTardies = 1
                                        GROUP BY   ClsSectionId
                                                  ,PV.TardiesMakingAbsence;

                            --Drop table @MyTardyTable
                            DECLARE @TotalTardyAbsentDays DECIMAL(18, 2);
                            SET @TotalTardyAbsentDays = (
                                                        SELECT ISNULL(SUM(AbsentHours), 0)
                                                        FROM   @MyTardyTable
                                                        );

                            --Print @TotalTardyAbsentDays

                            UPDATE @attendanceSummary
                            SET    AdjustedPresentDays = AdjustedPresentDays - @TotalTardyAbsentDays
                                  ,AdjustedAbsentDays = AdjustedAbsentDays + @TotalTardyAbsentDays
                            WHERE  StuEnrollId = @StuEnrollId
                                   AND StudentAttendedDate = (
                                                             SELECT   TOP 1 StudentAttendedDate
                                                             FROM     @attendanceSummary
                                                             WHERE    StuEnrollId = @StuEnrollId
                                                             ORDER BY StudentAttendedDate DESC
                                                             );

                        END;
                END; -- Step 3 
                -- END -- Step 3 

                -- Step 4  --  InsertAttendance_Class_Minutes
                --         --  TrackSapAttendance = ''byclass''
                BEGIN -- Step 4  --  InsertAttendance_Class_Minutes
                    -- By Class and Attendance Unit Type - Minutes and Clock Hour

                    IF @UnitTypeDescrip IN ( ''minutes'', ''clock hours'' )
                       AND @TrackSapAttendance = ''byclass''
                        BEGIN

                            --BEGIN
                            --    --DELETE FROM syStudentAttendanceSummary
                            --    --WHERE StuEnrollId = @StuEnrollId
                            --    --      AND StudentAttendedDate <= @OffsetDate;
                            --END;
                            DECLARE GetAttendance_Cursor CURSOR FOR
                                SELECT   *
                                        ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                FROM     (
                                         SELECT     DISTINCT t1.StuEnrollId
                                                            ,t1.ClsSectionId
                                                            ,t1.MeetDate
                                                            ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                            ,t4.StartDate
                                                            ,t4.EndDate
                                                            ,t5.PeriodDescrip
                                                            ,t1.Actual
                                                            ,t1.Excused
                                                            ,CASE WHEN (
                                                                       t1.Actual = 0
                                                                       AND t1.Excused = 0
                                                                       ) THEN t1.Scheduled
                                                                  ELSE CASE WHEN (
                                                                                 t1.Actual <> 9999.00
                                                                                 AND t1.Actual < t1.Scheduled
                                                                                 ) THEN ( t1.Scheduled - t1.Actual )
                                                                            ELSE 0
                                                                       END
                                                             END AS Absent
                                                            ,t1.Scheduled AS ScheduledMinutes
                                                            ,CASE WHEN (
                                                                       t1.Actual > 0
                                                                       AND t1.Actual < t1.Scheduled
                                                                       ) THEN ( t1.Scheduled - t1.Actual )
                                                                  ELSE 0
                                                             END AS TardyMinutes
                                                            ,t1.Tardy AS Tardy
                                                            ,t3.TrackTardies
                                                            ,t3.TardiesMakingAbsence
                                                            ,t3.PrgVerId
                                         FROM       atClsSectAttendance t1
                                         INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                         INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                         INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                         INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                            AND (
                                                                                CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                                AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                                )
                                                                            AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                         INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                         INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                         INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                         INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                         INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                         INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                         WHERE      t2.StuEnrollId = @StuEnrollId
                                                    AND (
                                                        AAUT1.UnitTypeDescrip IN ( ''Minutes'', ''Clock Hours'' )
                                                        OR AAUT2.UnitTypeDescrip IN ( ''Minutes'', ''Clock Hours'' )
                                                        )
                                                    AND t1.Actual <> 9999
                                                    AND t1.MeetDate <= @OffsetDate
                                         UNION
                                         SELECT     DISTINCT t1.StuEnrollId
                                                            ,NULL AS ClsSectionId
                                                            ,t1.RecordDate AS MeetDate
                                                            ,DATENAME(dw, t1.RecordDate) AS WeekDay
                                                            ,NULL AS StartDate
                                                            ,NULL AS EndDate
                                                            ,NULL AS PeriodDescrip
                                                            ,t1.ActualHours
                                                            ,NULL AS Excused
                                                            ,CASE WHEN ( t1.ActualHours = 0 ) THEN t1.SchedHours
                                                                  ELSE 0
                                                             --ELSE 
                                                             --Case when (t1.ActualHours <> 9999.00 and t1.ActualHours < t1.SchedHours)
                                                             --		THEN (t1.SchedHours - t1.ActualHours)
                                                             --		ELSE 
                                                             --			0
                                                             --		End
                                                             END AS Absent
                                                            ,t1.SchedHours AS ScheduledMinutes
                                                            ,CASE WHEN (
                                                                       t1.ActualHours <> 9999.00
                                                                       AND t1.ActualHours > 0
                                                                       AND t1.ActualHours < t1.SchedHours
                                                                       ) THEN ( t1.SchedHours - t1.ActualHours )
                                                                  ELSE 0
                                                             END AS TardyMinutes
                                                            ,NULL AS Tardy
                                                            ,t3.TrackTardies
                                                            ,t3.TardiesMakingAbsence
                                                            ,t3.PrgVerId
                                         FROM       arStudentClockAttendance t1
                                         INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                         INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                         WHERE      t2.StuEnrollId = @StuEnrollId
                                                    AND t1.Converted = 1
                                                    AND t1.ActualHours <> 9999
                                                    AND t1.RecordDate <= @OffsetDate
                                         ) dt
                                ORDER BY StuEnrollId
                                        ,MeetDate;
                            OPEN GetAttendance_Cursor;
                            FETCH NEXT FROM GetAttendance_Cursor
                            INTO @StuEnrollId
                                ,@ClsSectionId
                                ,@MeetDate
                                ,@WeekDay
                                ,@StartDate
                                ,@EndDate
                                ,@PeriodDescrip
                                ,@Actual
                                ,@Excused
                                ,@Absent
                                ,@ScheduledMinutes
                                ,@TardyMinutes
                                ,@tardy
                                ,@tracktardies
                                ,@TardiesMakingAbsence
                                ,@PrgVerId
                                ,@rownumber;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @boolReset = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                            SET @boolReset = 1;
                                        END;


                                    -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                                    IF @Actual <> 9999.00
                                        BEGIN
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                                            -- If there are make up hrs deduct that otherwise it will be added again in progress report
                                            IF (
                                               @Actual > 0
                                               AND @Actual > @ScheduledMinutes
                                               AND @Actual <> 9999.00
                                               )
                                                BEGIN
                                                    SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                END;
                                            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                                        END;

                                    -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                                    SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                                    -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                                    IF (
                                       @Actual > 0
                                       AND @Actual < @ScheduledMinutes
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                        END;

                                    -- Track how many days student has been tardy only when 
                                    -- program version requires to track tardy
                                    IF (
                                       @tracktardies = 1
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;


                                    -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
                                    -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
                                    -- when student is tardy the second time, that second occurance will be considered as
                                    -- absence
                                    -- Variable @intTardyBreakpoint tracks how many times the student was tardy
                                    -- Variable @tardiesMakingAbsence tracks the tardy rule
                                    IF (
                                       @tracktardies = 1
                                       AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                            SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;

                                    -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                       @Actual > 0
                                       AND @Actual > @ScheduledMinutes
                                       AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;


                                    IF ( @tracktardies = 1 )
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                                        END;



                                    INSERT INTO @attendanceSummary (
                                                                   StuEnrollId
                                                                  ,ClsSectionId
                                                                  ,StudentAttendedDate
                                                                  ,ScheduledDays
                                                                  ,ActualDays
                                                                  ,ActualRunningScheduledDays
                                                                  ,ActualRunningPresentDays
                                                                  ,ActualRunningAbsentDays
                                                                  ,ActualRunningMakeupDays
                                                                  ,ActualRunningTardyDays
                                                                  ,AdjustedPresentDays
                                                                  ,AdjustedAbsentDays
                                                                  ,AttendanceTrackType
                                                                  ,ModUser
                                                                  ,ModDate
                                                                   )
                                    VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays
                                            ,@ActualRunningPresentHours, @ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours
                                            ,@AdjustedPresentDaysComputed, @AdjustedRunningAbsentHours, ''Post Attendance by Class Min'', ''sa'', GETDATE());

                                    UPDATE @attendanceSummary
                                    SET    TardiesMakingAbsence = @TardiesMakingAbsence
                                    WHERE  StuEnrollId = @StuEnrollId;
                                    SET @PrevStuEnrollId = @StuEnrollId;

                                    FETCH NEXT FROM GetAttendance_Cursor
                                    INTO @StuEnrollId
                                        ,@ClsSectionId
                                        ,@MeetDate
                                        ,@WeekDay
                                        ,@StartDate
                                        ,@EndDate
                                        ,@PeriodDescrip
                                        ,@Actual
                                        ,@Excused
                                        ,@Absent
                                        ,@ScheduledMinutes
                                        ,@TardyMinutes
                                        ,@tardy
                                        ,@tracktardies
                                        ,@TardiesMakingAbsence
                                        ,@PrgVerId
                                        ,@rownumber;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
                        END;
                END; --  Step 3  --   InsertAttendance_Class_Minutes
                --END --  Step 3  --   InsertAttendance_Class_Minutes


                --Select * from arAttUnitType


                -- By Minutes/Day
                -- remove clock hour from here
                IF @UnitTypeDescrip IN ( ''minutes'' )
                   AND @TrackSapAttendance = ''byday''
                    -- -- PRINT GETDATE();	
                    ---- PRINT @UnitTypeId;
                    ---- PRINT @TrackSapAttendance;
                    BEGIN
                        --BEGIN
                        --    --DELETE FROM syStudentAttendanceSummary
                        --    --WHERE StuEnrollId = @StuEnrollId
                        --    --      AND StudentAttendedDate <= @OffsetDate;
                        --END;
                        DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
                            SELECT     t1.StuEnrollId
                                      ,NULL AS ClsSectionId
                                      ,t1.RecordDate AS MeetDate
                                      ,t1.ActualHours
                                      ,t1.SchedHours AS ScheduledMinutes
                                      ,CASE WHEN (
                                                 (
                                                 t1.SchedHours >= 1
                                                 AND t1.SchedHours NOT IN ( 999, 9999 )
                                                 )
                                                 AND t1.ActualHours = 0
                                                 ) THEN t1.SchedHours
                                            ELSE 0
                                       END AS Absent
                                      ,t1.isTardy
                                      ,(
                                       SELECT ISNULL(SUM(SchedHours), 0)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999, 9999 )
                                                  AND t1.ActualHours NOT IN ( 999, 9999 )
                                                  )
                                       ) AS ActualRunningScheduledHours
                                      ,(
                                       SELECT SUM(ActualHours)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999, 9999 )
                                                  )
                                              AND ActualHours >= 1
                                              AND ActualHours NOT IN ( 999, 9999 )
                                       ) AS ActualRunningPresentHours
                                      ,(
                                       SELECT COUNT(ActualHours)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999, 9999 )
                                                  )
                                              AND ActualHours = 0
                                              AND ActualHours NOT IN ( 999, 9999 )
                                       ) AS ActualRunningAbsentHours
                                      ,(
                                       SELECT SUM(ActualHours)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND SchedHours = 0
                                              AND ActualHours >= 1
                                              AND ActualHours NOT IN ( 999, 9999 )
                                       ) AS ActualRunningMakeupHours
                                      ,(
                                       SELECT SUM(SchedHours - ActualHours)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND (
                                                  (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999, 9999 )
                                                  )
                                                  AND ActualHours >= 1
                                                  AND ActualHours NOT IN ( 999, 9999 )
                                                  )
                                              AND isTardy = 1
                                       ) AS ActualRunningTardyHours
                                      ,t3.TrackTardies
                                      ,t3.TardiesMakingAbsence
                                      ,t3.PrgVerId
                                      ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
                            FROM       arStudentClockAttendance t1
                            INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                            INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                            INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                            --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                            WHERE      AAUT1.UnitTypeDescrip IN ( ''Minutes'' )
                                       AND t2.StuEnrollId = @StuEnrollId
                                       AND t1.ActualHours <> 9999.00
                                       AND t1.RecordDate <= @OffsetDate
                            ORDER BY   t1.StuEnrollId
                                      ,MeetDate;
                        OPEN GetAttendance_Cursor;
                        --Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                        FETCH NEXT FROM GetAttendance_Cursor
                        INTO @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@Actual
                            ,@ScheduledMinutes
                            ,@Absent
                            ,@IsTardy
                            ,@ActualRunningScheduledHours
                            ,@ActualRunningPresentHours
                            ,@ActualRunningAbsentHours
                            ,@ActualRunningMakeupHours
                            ,@ActualRunningTardyHours
                            ,@tracktardies
                            ,@TardiesMakingAbsence
                            ,@PrgVerId
                            ,@rownumber;

                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @ActualRunningMakeupHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        WHILE @@FETCH_STATUS = 0
                            BEGIN

                                IF @PrevStuEnrollId <> @StuEnrollId
                                    BEGIN
                                        SET @ActualRunningPresentHours = 0;
                                        SET @ActualRunningAbsentHours = 0;
                                        SET @intTardyBreakPoint = 0;
                                        SET @ActualRunningTardyHours = 0;
                                        SET @AdjustedRunningPresentHours = 0;
                                        SET @AdjustedRunningAbsentHours = 0;
                                        SET @ActualRunningScheduledDays = 0;
                                    END;

                                IF (
                                   @ScheduledMinutes >= 1
                                   AND (
                                       @Actual <> 9999
                                       AND @Actual <> 999
                                       )
                                   AND (
                                       @ScheduledMinutes <> 9999
                                       AND @Actual <> 999
                                       )
                                   )
                                    BEGIN
                                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + ISNULL(@ScheduledMinutes, 0);
                                    END;
                                ELSE
                                    BEGIN
                                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0);
                                    END;

                                IF (
                                   @Actual <> 9999
                                   AND @Actual <> 999
                                   )
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual - ( @Actual - @ScheduledMinutes );
                                    END;
                                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;

                                IF (
                                   @Actual > 0
                                   AND @Actual < @ScheduledMinutes
                                   )
                                    BEGIN
                                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                                    END;
                                -- Make up hours
                                --sched=5, Actual =7, makeup= 2,ab = 0
                                --sched=0, Actual =7, makeup= 7,ab = 0
                                IF (
                                   @Actual > 0
                                   AND @ScheduledMinutes > 0
                                   AND @Actual > @ScheduledMinutes
                                   AND (
                                       @Actual <> 9999
                                       AND @Actual <> 999
                                       )
                                   )
                                    BEGIN
                                        SET @ActualRunningMakeupHours = @ActualRunningMakeupHours + ( @Actual - @ScheduledMinutes );
                                    END;


                                IF (
                                   @Actual <> 9999
                                   AND @Actual <> 999
                                   )
                                    BEGIN
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                                    END;
                                IF (
                                   @Actual = 0
                                   AND @ScheduledMinutes >= 1
                                   AND @ScheduledMinutes NOT IN ( 999, 9999 )
                                   )
                                    BEGIN
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                                    END;
                                -- Absent hours
                                --1. sched = 5, Actual = 2 then Ab = (5-3) = 2
                                IF (
                                   @Absent = 0
                                   AND @ActualRunningAbsentHours > 0
                                   AND ( @Actual < @ScheduledMinutes )
                                   )
                                    BEGIN
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                                    END;
                                IF (
                                   @Actual > 0
                                   AND @Actual < @ScheduledMinutes
                                   AND (
                                       @Actual <> 9999.00
                                       AND @Actual <> 999.00
                                       )
                                   )
                                    BEGIN
                                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                    END;

                                IF @tracktardies = 1
                                   AND (
                                       @TardyMinutes > 0
                                       OR @IsTardy = 1
                                       )
                                    BEGIN
                                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                    END;
                                IF (
                                   @tracktardies = 1
                                   AND @intTardyBreakPoint = @TardiesMakingAbsence
                                   )
                                    BEGIN
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Actual; --@TardyMinutes
                                        SET @intTardyBreakPoint = 0;
                                    END;

                                INSERT INTO @attendanceSummary (
                                                               StuEnrollId
                                                              ,ClsSectionId
                                                              ,StudentAttendedDate
                                                              ,ScheduledDays
                                                              ,ActualDays
                                                              ,ActualRunningScheduledDays
                                                              ,ActualRunningPresentDays
                                                              ,ActualRunningAbsentDays
                                                              ,ActualRunningMakeupDays
                                                              ,ActualRunningTardyDays
                                                              ,AdjustedPresentDays
                                                              ,AdjustedAbsentDays
                                                              ,AttendanceTrackType
                                                              ,ModUser
                                                              ,ModDate
                                                               )
                                VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays
                                        ,@ActualRunningPresentHours, @ActualRunningAbsentHours, @ActualRunningMakeupHours, @ActualRunningTardyHours
                                        ,@AdjustedRunningPresentHours, @AdjustedRunningAbsentHours, ''Post Attendance by Class'', ''sa'', GETDATE());

                                UPDATE @attendanceSummary
                                SET    TardiesMakingAbsence = @TardiesMakingAbsence
                                WHERE  StuEnrollId = @StuEnrollId;
                                --end
                                SET @PrevStuEnrollId = @StuEnrollId;
                                FETCH NEXT FROM GetAttendance_Cursor
                                INTO @StuEnrollId
                                    ,@ClsSectionId
                                    ,@MeetDate
                                    ,@Actual
                                    ,@ScheduledMinutes
                                    ,@Absent
                                    ,@IsTardy
                                    ,@ActualRunningScheduledHours
                                    ,@ActualRunningPresentHours
                                    ,@ActualRunningAbsentHours
                                    ,@ActualRunningMakeupHours
                                    ,@ActualRunningTardyHours
                                    ,@tracktardies
                                    ,@TardiesMakingAbsence
                                    ,@PrgVerId
                                    ,@rownumber;
                            END;
                        CLOSE GetAttendance_Cursor;
                        DEALLOCATE GetAttendance_Cursor;
                    END;

                ---- PRINT ''Does it get to Clock Hour/PA'';
                ---- PRINT @UnitTypeId;
                ---- PRINT @TrackSapAttendance;
                -- By Day and PA, Clock Hour
                -- -- Step 2  --  InsertAttendance_Day_PresentAbsent  -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'')  and TrackSapAttendance = ''byday''
                BEGIN -- Step 2  --  InsertAttendance_Day_PresentAbsent  -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'') and TrackSapAttendance = ''byday''
                    IF @UnitTypeDescrip IN ( ''present absent'', ''clock hours'' )
                       AND @TrackSapAttendance = ''byday''
                        BEGIN
                            --BEGIN
                            --    --DELETE FROM syStudentAttendanceSummary
                            --    --WHERE StuEnrollId = @StuEnrollId
                            --    --      AND StudentAttendedDate <= @OffsetDate;
                            --END;
                            ---- PRINT ''By Day inside day'';
                            DECLARE GetAttendance_Cursor CURSOR FOR
                                SELECT     t1.StuEnrollId
                                          ,t1.RecordDate
                                          ,t1.ActualHours
                                          ,t1.SchedHours
                                          ,CASE WHEN (
                                                     (
                                                     t1.SchedHours >= 1
                                                     AND t1.SchedHours NOT IN ( 999, 9999 )
                                                     )
                                                     AND t1.ActualHours = 0
                                                     ) THEN t1.SchedHours
                                                ELSE 0
                                           END AS Absent
                                          ,t1.isTardy
                                          ,t3.TrackTardies
                                          ,t3.TardiesMakingAbsence
                                FROM       arStudentClockAttendance t1
                                INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                                WHERE -- Unit Types: Present Absent and Clock Hour
                                           AAUT1.UnitTypeDescrip IN ( ''present absent'', ''clock hours'' )
                                           AND ActualHours <> 9999.00
                                           AND t2.StuEnrollId = @StuEnrollId
                                           AND t1.RecordDate <= @OffsetDate
                                ORDER BY   t1.StuEnrollId
                                          ,t1.RecordDate;
                            OPEN GetAttendance_Cursor;
                            --Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                            FETCH NEXT FROM GetAttendance_Cursor
                            INTO @StuEnrollId
                                ,@MeetDate
                                ,@Actual
                                ,@ScheduledMinutes
                                ,@Absent
                                ,@IsTardy
                                ,@tracktardies
                                ,@TardiesMakingAbsence;

                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                        END;

                                    IF (
                                       @Actual <> 9999
                                       AND @Actual <> 999
                                       )
                                        BEGIN
                                            SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + @ScheduledMinutes;
                                            SET @ActualRunningPresentHours = ISNULL(@ActualRunningPresentHours, 0) + @Actual;
                                            SET @AdjustedRunningPresentHours = ISNULL(@AdjustedRunningPresentHours, 0) + @Actual;
                                        END;
                                    SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + @Absent;
                                    IF (
                                       @Actual = 0
                                       AND @ScheduledMinutes >= 1
                                       AND @ScheduledMinutes NOT IN ( 999, 9999 )
                                       )
                                        BEGIN
                                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                                        END;

                                    -- NWH 
                                    -- If Scheduled = 3.0 hrs and Actual = 1.0 Then 2 hrs is considered absent
                                    IF (
                                       @ScheduledMinutes >= 1
                                       AND @ScheduledMinutes NOT IN ( 999, 9999 )
                                       AND @Actual > 0
                                       AND ( @Actual < @ScheduledMinutes )
                                       )
                                        BEGIN
                                            SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                                            SET @AdjustedRunningAbsentHours = ISNULL(@AdjustedRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                                        END;

                                    IF (
                                       @tracktardies = 1
                                       AND @IsTardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + 1;
                                        END;
                                    --commented by balaji on 10/22/2012 as report (rdl) doesn''t add days attended and make up days
                                    ---- If there are make up hrs deduct that otherwise it will be added again in progress report
                                    IF (
                                       @Actual > 0
                                       AND @Actual > @ScheduledMinutes
                                       AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                        END;

                                    IF @tracktardies = 1
                                       AND (
                                           @TardyMinutes > 0
                                           OR @IsTardy = 1
                                           )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;



                                    -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                       @Actual > 0
                                       AND @Actual > @ScheduledMinutes
                                       AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;

                                    IF (
                                       @tracktardies = 1
                                       AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;


                                    INSERT INTO @attendanceSummary (
                                                                   StuEnrollId
                                                                  ,ClsSectionId
                                                                  ,StudentAttendedDate
                                                                  ,ScheduledDays
                                                                  ,ActualDays
                                                                  ,ActualRunningScheduledDays
                                                                  ,ActualRunningPresentDays
                                                                  ,ActualRunningAbsentDays
                                                                  ,ActualRunningMakeupDays
                                                                  ,ActualRunningTardyDays
                                                                  ,AdjustedPresentDays
                                                                  ,AdjustedAbsentDays
                                                                  ,AttendanceTrackType
                                                                  ,ModUser
                                                                  ,ModDate
                                                                  ,TardiesMakingAbsence
                                                                   )
                                    VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, ISNULL(@ScheduledMinutes, 0), @Actual
                                            ,ISNULL(@ActualRunningScheduledDays, 0), ISNULL(@ActualRunningPresentHours, 0)
                                            ,ISNULL(@ActualRunningAbsentHours, 0), ISNULL(@MakeupHours, 0), ISNULL(@ActualRunningTardyHours, 0)
                                            ,ISNULL(@AdjustedRunningPresentHours, 0), ISNULL(@AdjustedRunningAbsentHours, 0), ''Post Attendance by Class'', ''sa''
                                            ,GETDATE(), @TardiesMakingAbsence );

                                    --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                                    --end
                                    SET @PrevStuEnrollId = @StuEnrollId;
                                    FETCH NEXT FROM GetAttendance_Cursor
                                    INTO @StuEnrollId
                                        ,@MeetDate
                                        ,@Actual
                                        ,@ScheduledMinutes
                                        ,@Absent
                                        ,@IsTardy
                                        ,@tracktardies
                                        ,@TardiesMakingAbsence;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
                        END;
                END;

                --end

                -- Based on grade rounding round the final score and current score
                --Declare @PrevTermId uniqueidentifier,@PrevReqId uniqueidentifier,@RowCount int,@FinalScore decimal(18,4),@CurrentScore decimal(18,4)
                --declare @ReqId uniqueidentifier,@CourseCodeDescrip varchar(100),@FinalGrade varchar(10),@sysComponentTypeId int
                --declare @CreditsAttempted decimal(18,4),@Grade varchar(10),@IsPass bit,@IsCreditsAttempted bit,@IsCreditsEarned bit
                --declare @IsInGPA bit,@FinAidCredits decimal(18,4),@CurrentGrade varchar(10),@CourseCredits decimal(18,4)
                DECLARE @GPA DECIMAL(18, 4);

                DECLARE @PrevReqId UNIQUEIDENTIFIER
                       ,@PrevTermId UNIQUEIDENTIFIER
                       ,@CreditsAttempted DECIMAL(18, 2)
                       ,@CreditsEarned DECIMAL(18, 2);
                DECLARE @reqid UNIQUEIDENTIFIER
                       ,@CourseCodeDescrip VARCHAR(50)
                       ,@FinalGrade VARCHAR(50)
                       ,@FinalScore DECIMAL(18, 2)
                       ,@Grade VARCHAR(50);
                DECLARE @IsPass BIT
                       ,@IsCreditsAttempted BIT
                       ,@IsCreditsEarned BIT
                       ,@CurrentScore DECIMAL(18, 2)
                       ,@CurrentGrade VARCHAR(10)
                       ,@FinalGPA DECIMAL(18, 2);
                DECLARE @sysComponentTypeId INT
                       ,@RowCount INT;
                DECLARE @IsInGPA BIT;
                DECLARE @CourseCredits DECIMAL(18, 2);
                DECLARE @FinAidCreditsEarned DECIMAL(18, 2)
                       ,@FinAidCredits DECIMAL(18, 2);

                DECLARE GetCreditsSummary_Cursor CURSOR FOR
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,T.TermId
                                       ,T.TermDescrip
                                       ,T.StartDate
                                       ,R.ReqId
                                       ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                       ,RES.Score AS FinalScore
                                       ,RES.GrdSysDetailId AS FinalGrade
                                       ,GCT.SysComponentTypeId
                                       ,R.Credits AS CreditsAttempted
                                       ,CS.ClsSectionId
                                       ,GSD.Grade
                                       ,GSD.IsPass
                                       ,GSD.IsCreditsAttempted
                                       ,GSD.IsCreditsEarned
                                       ,SE.PrgVerId
                                       ,GSD.IsInGPA
                                       ,R.FinAidCredits AS FinAidCredits
                    FROM       arStuEnrollments SE
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                    INNER JOIN arTerm T ON CS.TermId = T.TermId
                    INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                    LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                    LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                    LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                    LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    WHERE      SE.StuEnrollId = @StuEnrollId
                               AND (
                                   RES.DateCompleted IS NOT NULL
                                   AND RES.DateCompleted <= @OffsetDate
                                   )
                    ORDER BY   T.StartDate
                              ,T.TermDescrip
                              ,R.ReqId;

                DECLARE @varGradeRounding VARCHAR(3);
                DECLARE @roundfinalscore DECIMAL(18, 4);
                SET @varGradeRounding = (
                                        SELECT Value
                                        FROM   syConfigAppSetValues
                                        WHERE  SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@FinalGrade
                    ,@sysComponentTypeId
                    ,@CreditsAttempted
                    ,@ClsSectionId
                    ,@Grade
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN


                        SET @CurrentScore = (
                                            SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                        ELSE NULL
                                                   END AS CurrentScore
                                            FROM   (
                                                   SELECT   InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                   FROM     (
                                                            SELECT   C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,ISNULL(C.Weight, 0) AS Weight
                                                                    ,C.Number AS MinNumber
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter AS Param
                                                                    ,X.GrdScaleId
                                                                    ,SUM(GR.Score) AS Score
                                                                    ,COUNT(D.Descrip) AS NumberOfComponents
                                                            FROM     (
                                                                     SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                            ,A.EffectiveDate
                                                                                            ,B.GrdScaleId
                                                                     FROM     arGrdBkWeights A
                                                                             ,arClassSections B
                                                                     WHERE    A.ReqId = B.ReqId
                                                                              AND A.EffectiveDate <= B.StartDate
                                                                              AND B.ClsSectionId = @ClsSectionId
                                                                     ORDER BY A.EffectiveDate DESC
                                                                     ) X
                                                                    ,arGrdBkWgtDetails C
                                                                    ,arGrdComponentTypes D
                                                                    ,arGrdBkResults GR
                                                            WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                     AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                     AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                     AND D.SysComponentTypeId NOT IN ( 500, 503 )
                                                                     AND GR.StuEnrollId = @StuEnrollId
                                                                     AND GR.ClsSectionId = @ClsSectionId
                                                                     AND GR.Score IS NOT NULL
                                                            GROUP BY C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,C.Weight
                                                                    ,C.Number
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter
                                                                    ,X.GrdScaleId
                                                            ) S
                                                   GROUP BY InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                   ) FinalTblToComputeCurrentScore
                                            );
                        IF ( @CurrentScore IS NULL )
                            BEGIN
                                -- instructor grade books
                                SET @CurrentScore = (
                                                    SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                                ELSE NULL
                                                           END AS CurrentScore
                                                    FROM   (
                                                           SELECT   InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight AS GradeBookWeight
                                                                   ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                         ELSE 0
                                                                    END AS ActualWeight
                                                           FROM     (
                                                                    SELECT   C.InstrGrdBkWgtDetailId
                                                                            ,D.Code
                                                                            ,D.Descrip
                                                                            ,ISNULL(C.Weight, 0) AS Weight
                                                                            ,C.Number AS MinNumber
                                                                            ,C.GrdPolicyId
                                                                            ,C.Parameter AS Param
                                                                            ,X.GrdScaleId
                                                                            ,SUM(GR.Score) AS Score
                                                                            ,COUNT(D.Descrip) AS NumberOfComponents
                                                                    FROM     (
                                                                             --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
                                                                             --FROM          arGrdBkWeights A,arClassSections B        
                                                                             --WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
                                                                             --ORDER BY      A.EffectiveDate DESC
                                                                             SELECT DISTINCT TOP 1 t1.InstrGrdBkWgtId
                                                                                                  ,t1.GrdScaleId
                                                                             FROM   arClassSections t1
                                                                                   ,arGrdBkWeights t2
                                                                             WHERE  t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                                    AND t1.ClsSectionId = @ClsSectionId
                                                                             ) X
                                                                            ,arGrdBkWgtDetails C
                                                                            ,arGrdComponentTypes D
                                                                            ,arGrdBkResults GR
                                                                    WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                             AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                             AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                             AND
                                                                        -- D.SysComponentTypeID not in (500,503) and 
                                                                        GR.StuEnrollId = @StuEnrollId
                                                                             AND GR.ClsSectionId = @ClsSectionId
                                                                             AND GR.Score IS NOT NULL
                                                                    GROUP BY C.InstrGrdBkWgtDetailId
                                                                            ,D.Code
                                                                            ,D.Descrip
                                                                            ,C.Weight
                                                                            ,C.Number
                                                                            ,C.GrdPolicyId
                                                                            ,C.Parameter
                                                                            ,X.GrdScaleId
                                                                    ) S
                                                           GROUP BY InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight
                                                                   ,NumberOfComponents
                                                           ) FinalTblToComputeCurrentScore
                                                    );

                            END;


                        -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore, 0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;

                        UPDATE syCreditSummary
                        SET    FinalScore = @FinalScore
                              ,CurrentScore = @CurrentScore
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId
                               AND ReqId = @reqid;

                        --Average calculation

                        -- Term Average
                        SET @TermAverageCount = (
                                                SELECT COUNT(*)
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                       AND TermId = @TermId
                                                       AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                              SELECT SUM(FinalScore)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND TermId = @TermId
                                                     AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount;

                        -- Cumulative Average
                        SET @cumAveragecount = (
                                               SELECT COUNT(*)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                             SELECT SUM(FinalScore)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount;

                        UPDATE syCreditSummary
                        SET    Average = @TermAverage
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId;

                        --Update Cumulative GPA
                        UPDATE syCreditSummary
                        SET    CumAverage = @CumAverage
                        WHERE  StuEnrollId = @StuEnrollId;


                        FETCH NEXT FROM GetCreditsSummary_Cursor
                        INTO @StuEnrollId
                            ,@TermId
                            ,@TermDescrip
                            ,@termstartdate1
                            ,@reqid
                            ,@CourseCodeDescrip
                            ,@FinalScore
                            ,@FinalGrade
                            ,@sysComponentTypeId
                            ,@CreditsAttempted
                            ,@ClsSectionId
                            ,@Grade
                            ,@IsPass
                            ,@IsCreditsAttempted
                            ,@IsCreditsEarned
                            ,@PrgVerId
                            ,@IsInGPA
                            ,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;


                DECLARE GetCreditsSummary_Cursor CURSOR FOR
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,T.TermId
                                       ,T.TermDescrip
                                       ,T.StartDate
                                       ,R.ReqId
                                       ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                       ,RES.Score AS FinalScore
                                       ,RES.GrdSysDetailId AS FinalGrade
                                       ,GCT.SysComponentTypeId
                                       ,R.Credits AS CreditsAttempted
                                       ,CS.ClsSectionId
                                       ,GSD.Grade
                                       ,GSD.IsPass
                                       ,GSD.IsCreditsAttempted
                                       ,GSD.IsCreditsEarned
                                       ,SE.PrgVerId
                                       ,GSD.IsInGPA
                                       ,R.FinAidCredits AS FinAidCredits
                    FROM       arStuEnrollments SE
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                                     AND RES.TermId = CS.TermId
                    INNER JOIN arTerm T ON CS.TermId = T.TermId
                    INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                    LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                    LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                    LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                    LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    WHERE      SE.StuEnrollId = @StuEnrollId
                               AND (
                                   RES.CompletedDate IS NOT NULL
                                   AND RES.CompletedDate <= @OffsetDate
                                   )
                    ORDER BY   T.StartDate
                              ,T.TermDescrip
                              ,R.ReqId;


                SET @varGradeRounding = (
                                        SELECT Value
                                        FROM   syConfigAppSetValues
                                        WHERE  SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@FinalGrade
                    ,@sysComponentTypeId
                    ,@CreditsAttempted
                    ,@ClsSectionId
                    ,@Grade
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        SET @CurrentScore = (
                                            SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                        ELSE NULL
                                                   END AS CurrentScore
                                            FROM   (
                                                   SELECT   InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                   FROM     (
                                                            SELECT   C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,ISNULL(C.Weight, 0) AS Weight
                                                                    ,C.Number AS MinNumber
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter AS Param
                                                                    ,X.GrdScaleId
                                                                    ,SUM(GR.Score) AS Score
                                                                    ,COUNT(D.Descrip) AS NumberOfComponents
                                                            FROM     (
                                                                     SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                            ,A.EffectiveDate
                                                                                            ,B.GrdScaleId
                                                                     FROM     arGrdBkWeights A
                                                                             ,arClassSections B
                                                                     WHERE    A.ReqId = B.ReqId
                                                                              AND A.EffectiveDate <= B.StartDate
                                                                              AND B.ClsSectionId = @ClsSectionId
                                                                     ORDER BY A.EffectiveDate DESC
                                                                     ) X
                                                                    ,arGrdBkWgtDetails C
                                                                    ,arGrdComponentTypes D
                                                                    ,arGrdBkResults GR
                                                            WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                     AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                     AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                     AND D.SysComponentTypeId NOT IN ( 500, 503 )
                                                                     AND GR.StuEnrollId = @StuEnrollId
                                                                     AND GR.ClsSectionId = @ClsSectionId
                                                                     AND GR.Score IS NOT NULL
                                                            GROUP BY C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,C.Weight
                                                                    ,C.Number
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter
                                                                    ,X.GrdScaleId
                                                            ) S
                                                   GROUP BY InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                   ) FinalTblToComputeCurrentScore
                                            );
                        IF ( @CurrentScore IS NULL )
                            BEGIN
                                -- instructor grade books
                                SET @CurrentScore = (
                                                    SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                                ELSE NULL
                                                           END AS CurrentScore
                                                    FROM   (
                                                           SELECT   InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight AS GradeBookWeight
                                                                   ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                         ELSE 0
                                                                    END AS ActualWeight
                                                           FROM     (
                                                                    SELECT   C.InstrGrdBkWgtDetailId
                                                                            ,D.Code
                                                                            ,D.Descrip
                                                                            ,ISNULL(C.Weight, 0) AS Weight
                                                                            ,C.Number AS MinNumber
                                                                            ,C.GrdPolicyId
                                                                            ,C.Parameter AS Param
                                                                            ,X.GrdScaleId
                                                                            ,SUM(GR.Score) AS Score
                                                                            ,COUNT(D.Descrip) AS NumberOfComponents
                                                                    FROM     (
                                                                             --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
                                                                             --FROM          arGrdBkWeights A,arClassSections B        
                                                                             --WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
                                                                             --ORDER BY      A.EffectiveDate DESC
                                                                             SELECT DISTINCT TOP 1 t1.InstrGrdBkWgtId
                                                                                                  ,t1.GrdScaleId
                                                                             FROM   arClassSections t1
                                                                                   ,arGrdBkWeights t2
                                                                             WHERE  t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                                    AND t1.ClsSectionId = @ClsSectionId
                                                                             ) X
                                                                            ,arGrdBkWgtDetails C
                                                                            ,arGrdComponentTypes D
                                                                            ,arGrdBkResults GR
                                                                    WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                             AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                             AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                             AND
                                                                        -- D.SysComponentTypeID not in (500,503) and 
                                                                        GR.StuEnrollId = @StuEnrollId
                                                                             AND GR.ClsSectionId = @ClsSectionId
                                                                             AND GR.Score IS NOT NULL
                                                                    GROUP BY C.InstrGrdBkWgtDetailId
                                                                            ,D.Code
                                                                            ,D.Descrip
                                                                            ,C.Weight
                                                                            ,C.Number
                                                                            ,C.GrdPolicyId
                                                                            ,C.Parameter
                                                                            ,X.GrdScaleId
                                                                    ) S
                                                           GROUP BY InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight
                                                                   ,NumberOfComponents
                                                           ) FinalTblToComputeCurrentScore
                                                    );

                            END;

                        -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore, 0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;

                        UPDATE syCreditSummary
                        SET    FinalScore = @FinalScore
                              ,CurrentScore = @CurrentScore
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId
                               AND ReqId = @reqid;

                        --Average calculation
                        --			declare @CumAverage decimal(18,2),@cumAverageSum decimal(18,2),@cumAveragecount int

                        -- Term Average
                        SET @TermAverageCount = (
                                                SELECT COUNT(*)
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                       AND TermId = @TermId
                                                       AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                              SELECT SUM(FinalScore)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND TermId = @TermId
                                                     AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount;

                        -- Cumulative Average
                        SET @cumAveragecount = (
                                               SELECT COUNT(*)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                             SELECT SUM(FinalScore)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount;

                        UPDATE syCreditSummary
                        SET    Average = @TermAverage
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId;

                        --Update Cumulative GPA
                        UPDATE syCreditSummary
                        SET    CumAverage = @CumAverage
                        WHERE  StuEnrollId = @StuEnrollId;


                        FETCH NEXT FROM GetCreditsSummary_Cursor
                        INTO @StuEnrollId
                            ,@TermId
                            ,@TermDescrip
                            ,@termstartdate1
                            ,@reqid
                            ,@CourseCodeDescrip
                            ,@FinalScore
                            ,@FinalGrade
                            ,@sysComponentTypeId
                            ,@CreditsAttempted
                            ,@ClsSectionId
                            ,@Grade
                            ,@IsPass
                            ,@IsCreditsAttempted
                            ,@IsCreditsEarned
                            ,@PrgVerId
                            ,@IsInGPA
                            ,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

                DECLARE @GradeSystemDetailId UNIQUEIDENTIFIER
                       ,@IsGPA BIT;
                DECLARE GetCreditsSummary_Cursor CURSOR FOR
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,T.TermId
                                       ,T.TermDescrip
                                       ,T.StartDate
                                       ,R.ReqId
                                       ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                       ,NULL AS FinalScore
                                       ,RES.GrdSysDetailId AS GradeSystemDetailId
                                       ,GSD.Grade AS FinalGrade
                                       ,GSD.Grade AS CurrentGrade
                                       ,R.Credits
                                       ,NULL AS ClsSectionId
                                       ,GSD.IsPass
                                       ,GSD.IsCreditsAttempted
                                       ,GSD.IsCreditsEarned
                                       ,GSD.GPA
                                       ,GSD.IsInGPA
                                       ,SE.PrgVerId
                                       ,GSD.IsInGPA
                                       ,R.FinAidCredits AS FinAidCredits
                    FROM       arStuEnrollments SE
                    INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
                    INNER JOIN syCreditSummary CS ON CS.StuEnrollId = RES.StuEnrollId
                    INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                    INNER JOIN arTerm T ON RES.TermId = T.TermId
                    WHERE      RES.ReqId NOT IN (
                                                SELECT DISTINCT ReqId
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = SE.StuEnrollId
                                                       AND TermId = T.TermId
                                                )
                               AND SE.StuEnrollId = @StuEnrollId
                               AND (
                                   RES.CompletedDate IS NOT NULL
                                   AND RES.CompletedDate <= @OffsetDate
                                   );

                SET @varGradeRounding = (
                                        SELECT Value
                                        FROM   syConfigAppSetValues
                                        WHERE  SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@GradeSystemDetailId
                    ,@FinalGrade
                    ,@CurrentGrade
                    ,@CourseCredits
                    ,@ClsSectionId
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@GPA
                    ,@IsGPA
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore, 0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;

                        UPDATE syCreditSummary
                        SET    FinalScore = @FinalScore
                              ,CurrentScore = @CurrentScore
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId
                               AND ReqId = @reqid;

                        --Average calculation

                        -- Term Average
                        SET @TermAverageCount = (
                                                SELECT COUNT(*)
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                       AND TermId = @TermId
                                                       AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                              SELECT SUM(FinalScore)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND TermId = @TermId
                                                     AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount;

                        -- Cumulative Average
                        SET @cumAveragecount = (
                                               SELECT COUNT(*)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                               --AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                             SELECT SUM(FinalScore)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount;

                        UPDATE syCreditSummary
                        SET    Average = @TermAverage
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId;

                        --Update Cumulative GPA
                        UPDATE syCreditSummary
                        SET    CumAverage = @CumAverage
                        WHERE  StuEnrollId = @StuEnrollId;


                        FETCH NEXT FROM GetCreditsSummary_Cursor
                        INTO @StuEnrollId
                            ,@TermId
                            ,@TermDescrip
                            ,@termstartdate1
                            ,@reqid
                            ,@CourseCodeDescrip
                            ,@FinalScore
                            ,@GradeSystemDetailId
                            ,@FinalGrade
                            ,@CurrentGrade
                            ,@CourseCredits
                            ,@ClsSectionId
                            ,@IsPass
                            ,@IsCreditsAttempted
                            ,@IsCreditsEarned
                            ,@GPA
                            ,@IsGPA
                            ,@PrgVerId
                            ,@IsInGPA
                            ,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

                DECLARE GetCreditsSummary_Cursor CURSOR FOR
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,T.TermId
                                       ,T.TermDescrip
                                       ,T.StartDate
                                       ,R.ReqId
                                       ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                       ,NULL AS FinalScore
                                       ,RES.GrdSysDetailId AS GradeSystemDetailId
                                       ,GSD.Grade AS FinalGrade
                                       ,GSD.Grade AS CurrentGrade
                                       ,R.Credits
                                       ,NULL AS ClsSectionId
                                       ,GSD.IsPass
                                       ,GSD.IsCreditsAttempted
                                       ,GSD.IsCreditsEarned
                                       ,GSD.GPA
                                       ,GSD.IsInGPA
                                       ,SE.PrgVerId
                                       ,GSD.IsInGPA
                                       ,R.FinAidCredits AS FinAidCredits
                    FROM       arStuEnrollments SE
                    INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
                    INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                    INNER JOIN arTerm T ON RES.TermId = T.TermId
                    WHERE      SE.StuEnrollId NOT IN (
                                                     SELECT DISTINCT StuEnrollId
                                                     FROM   syCreditSummary
                                                     )
                               AND SE.StuEnrollId = @StuEnrollId
                               AND (
                                   RES.CompletedDate IS NOT NULL
                                   AND RES.CompletedDate <= @OffsetDate
                                   );

                SET @varGradeRounding = (
                                        SELECT Value
                                        FROM   syConfigAppSetValues
                                        WHERE  SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@GradeSystemDetailId
                    ,@FinalGrade
                    ,@CurrentGrade
                    ,@CourseCredits
                    ,@ClsSectionId
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@GPA
                    ,@IsGPA
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore, 0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;

                        UPDATE syCreditSummary
                        SET    FinalScore = @FinalScore
                              ,CurrentScore = @CurrentScore
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId
                               AND ReqId = @reqid;

                        --Average calculation
                        SET @TermAverageCount = (
                                                SELECT COUNT(*)
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                       AND TermId = @TermId
                                                       AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                              SELECT SUM(FinalScore)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND TermId = @TermId
                                                     AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount;

                        -- Cumulative Average
                        SET @cumAveragecount = (
                                               SELECT COUNT(*)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                             SELECT SUM(FinalScore)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount;

                        UPDATE syCreditSummary
                        SET    Average = @TermAverage
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId;

                        --Update Cumulative GPA
                        UPDATE syCreditSummary
                        SET    CumAverage = @CumAverage
                        WHERE  StuEnrollId = @StuEnrollId;


                        FETCH NEXT FROM GetCreditsSummary_Cursor
                        INTO @StuEnrollId
                            ,@TermId
                            ,@TermDescrip
                            ,@termstartdate1
                            ,@reqid
                            ,@CourseCodeDescrip
                            ,@FinalScore
                            ,@GradeSystemDetailId
                            ,@FinalGrade
                            ,@CurrentGrade
                            ,@CourseCredits
                            ,@ClsSectionId
                            ,@IsPass
                            ,@IsCreditsAttempted
                            ,@IsCreditsEarned
                            ,@GPA
                            ,@IsGPA
                            ,@PrgVerId
                            ,@IsInGPA
                            ,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

            ---- PRINT ''@CurrentBal_Compute'', 
            ---- PRINT @CurrentBal_Compute


            END;

        SET @TermAverageCount = (
                                SELECT COUNT(*)
                                FROM   syCreditSummary
                                WHERE  StuEnrollId = @StuEnrollId
                                       AND FinalScore IS NOT NULL
                                );
        SET @termAverageSum = (
                              SELECT SUM(FinalScore)
                              FROM   syCreditSummary
                              WHERE  StuEnrollId = @StuEnrollId
                                     AND FinalScore IS NOT NULL
                              );
        IF @TermAverageCount >= 1
            BEGIN
                SET @TermAverage = @termAverageSum / @TermAverageCount;
            END;

        -- Cumulative Average
        SET @cumAveragecount = (
                               SELECT COUNT(*)
                               FROM   (
                                      SELECT     sCS.StuEnrollId
                                                ,sCS.TermId
                                                ,sCS.TermDescrip
                                                ,sCS.ReqId
                                                ,sCS.ReqDescrip
                                                ,sCS.ClsSectionId
                                                ,sCS.CreditsEarned
                                                ,sCS.CreditsAttempted
                                                ,sCS.CurrentScore
                                                ,sCS.CurrentGrade
                                                ,sCS.FinalScore
                                                ,sCS.FinalGrade
                                                ,sCS.Completed
                                                ,sCS.FinalGPA
                                                ,sCS.Product_WeightedAverage_Credits_GPA
                                                ,sCS.Count_WeightedAverage_Credits
                                                ,sCS.Product_SimpleAverage_Credits_GPA
                                                ,sCS.Count_SimpleAverage_Credits
                                                ,sCS.ModUser
                                                ,sCS.ModDate
                                                ,sCS.TermGPA_Simple
                                                ,sCS.TermGPA_Weighted
                                                ,sCS.coursecredits
                                                ,sCS.CumulativeGPA
                                                ,sCS.CumulativeGPA_Simple
                                                ,sCS.FACreditsEarned
                                                ,sCS.Average
                                                ,sCS.CumAverage
                                                ,sCS.TermStartDate
                                      FROM       dbo.syCreditSummary sCS
                                      INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                      INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                      WHERE      sCS.StuEnrollId = @StuEnrollId
                                                 AND CS.ReqId = sCS.ReqId
                                                 AND (
                                                     R.DateCompleted IS NOT NULL
                                                     AND R.DateCompleted <= @OffsetDate
                                                     )
                                      UNION ALL
                                      (SELECT     sCS.StuEnrollId
                                                 ,sCS.TermId
                                                 ,sCS.TermDescrip
                                                 ,sCS.ReqId
                                                 ,sCS.ReqDescrip
                                                 ,sCS.ClsSectionId
                                                 ,sCS.CreditsEarned
                                                 ,sCS.CreditsAttempted
                                                 ,sCS.CurrentScore
                                                 ,sCS.CurrentGrade
                                                 ,sCS.FinalScore
                                                 ,sCS.FinalGrade
                                                 ,sCS.Completed
                                                 ,sCS.FinalGPA
                                                 ,sCS.Product_WeightedAverage_Credits_GPA
                                                 ,sCS.Count_WeightedAverage_Credits
                                                 ,sCS.Product_SimpleAverage_Credits_GPA
                                                 ,sCS.Count_SimpleAverage_Credits
                                                 ,sCS.ModUser
                                                 ,sCS.ModDate
                                                 ,sCS.TermGPA_Simple
                                                 ,sCS.TermGPA_Weighted
                                                 ,sCS.coursecredits
                                                 ,sCS.CumulativeGPA
                                                 ,sCS.CumulativeGPA_Simple
                                                 ,sCS.FACreditsEarned
                                                 ,sCS.Average
                                                 ,sCS.CumAverage
                                                 ,sCS.TermStartDate
                                       FROM       dbo.syCreditSummary sCS
                                       INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                       WHERE      sCS.StuEnrollId = @StuEnrollId
                                                  AND tG.ReqId = sCS.ReqId
                                                  AND (
                                                      tG.CompletedDate IS NOT NULL
                                                      AND tG.CompletedDate <= @OffsetDate
                                                      ))
                                      ) sCSEA
                               WHERE  StuEnrollId = @StuEnrollId
                                      AND FinalScore IS NOT NULL
                               );
        SET @cumAverageSum = (
                             SELECT SUM(FinalScore)
                             FROM   (
                                    SELECT     sCS.StuEnrollId
                                              ,sCS.TermId
                                              ,sCS.TermDescrip
                                              ,sCS.ReqId
                                              ,sCS.ReqDescrip
                                              ,sCS.ClsSectionId
                                              ,sCS.CreditsEarned
                                              ,sCS.CreditsAttempted
                                              ,sCS.CurrentScore
                                              ,sCS.CurrentGrade
                                              ,sCS.FinalScore
                                              ,sCS.FinalGrade
                                              ,sCS.Completed
                                              ,sCS.FinalGPA
                                              ,sCS.Product_WeightedAverage_Credits_GPA
                                              ,sCS.Count_WeightedAverage_Credits
                                              ,sCS.Product_SimpleAverage_Credits_GPA
                                              ,sCS.Count_SimpleAverage_Credits
                                              ,sCS.ModUser
                                              ,sCS.ModDate
                                              ,sCS.TermGPA_Simple
                                              ,sCS.TermGPA_Weighted
                                              ,sCS.coursecredits
                                              ,sCS.CumulativeGPA
                                              ,sCS.CumulativeGPA_Simple
                                              ,sCS.FACreditsEarned
                                              ,sCS.Average
                                              ,sCS.CumAverage
                                              ,sCS.TermStartDate
                                    FROM       dbo.syCreditSummary sCS
                                    INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                    INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                    WHERE      sCS.StuEnrollId = @StuEnrollId
                                               AND CS.ReqId = sCS.ReqId
                                               AND (
                                                   R.DateCompleted IS NOT NULL
                                                   AND R.DateCompleted <= @OffsetDate
                                                   )
                                    UNION ALL
                                    (SELECT     sCS.StuEnrollId
                                               ,sCS.TermId
                                               ,sCS.TermDescrip
                                               ,sCS.ReqId
                                               ,sCS.ReqDescrip
                                               ,sCS.ClsSectionId
                                               ,sCS.CreditsEarned
                                               ,sCS.CreditsAttempted
                                               ,sCS.CurrentScore
                                               ,sCS.CurrentGrade
                                               ,sCS.FinalScore
                                               ,sCS.FinalGrade
                                               ,sCS.Completed
                                               ,sCS.FinalGPA
                                               ,sCS.Product_WeightedAverage_Credits_GPA
                                               ,sCS.Count_WeightedAverage_Credits
                                               ,sCS.Product_SimpleAverage_Credits_GPA
                                               ,sCS.Count_SimpleAverage_Credits
                                               ,sCS.ModUser
                                               ,sCS.ModDate
                                               ,sCS.TermGPA_Simple
                                               ,sCS.TermGPA_Weighted
                                               ,sCS.coursecredits
                                               ,sCS.CumulativeGPA
                                               ,sCS.CumulativeGPA_Simple
                                               ,sCS.FACreditsEarned
                                               ,sCS.Average
                                               ,sCS.CumAverage
                                               ,sCS.TermStartDate
                                     FROM       dbo.syCreditSummary sCS
                                     INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                     WHERE      sCS.StuEnrollId = @StuEnrollId
                                                AND tG.ReqId = sCS.ReqId
                                                AND (
                                                    tG.CompletedDate IS NOT NULL
                                                    AND tG.CompletedDate <= @OffsetDate
                                                    ))
                                    ) sCSEA
                             WHERE  StuEnrollId = @StuEnrollId
                                    AND FinalScore IS NOT NULL
                             );

        IF @cumAveragecount >= 1
            BEGIN
                SET @CumAverage = @cumAverageSum / @cumAveragecount;
            END;

		--If Clock Hour / Numeric - use New Average Calculation
        IF (
           SELECT TOP 1 P.ACId
           FROM   dbo.arStuEnrollments E
           JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
           JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
           WHERE  E.StuEnrollId = @StuEnrollId
           ) = 5
           AND @GradesFormat = ''numeric''
            BEGIN
                EXEC dbo.USP_GPACalculator @EnrollmentId = @StuEnrollId
                                          ,@EndDate = @OffsetDate
                                          ,@StudentGPA = @CumAverage OUTPUT;
        END;

        IF @GradesFormat <> ''letter''
            BEGIN
                SET @Qualitative = @CumAverage;
            END;
        ELSE
            SET @Qualitative = @cumWeightedGPA;

        IF ( @QuantMinUnitTypeId = 3 )
            BEGIN
                IF @UnitTypeDescrip IN ( ''none'', ''present absent'' )
                   AND @TrackSapAttendance = ''byclass''
                    BEGIN
                        SELECT @Quantitative = ( CAST(sas.Present AS DECIMAL) / sas.Scheduled ) * 100 
                              ,@ActualHours = sas.Present
                              ,@ScheduledHours = sas.Scheduled
                        FROM   (
                               SELECT   MAX(convertedSAS.ConvertedActualRunningScheduledDays) AS Scheduled
                                       ,MAX(convertedSAS.ConvertedActualRunningPresentDays) + MAX(convertedSAS.ConvertedActualRunningMakeupDays) AS Present -- add makeup to present since procedure splits it before added into syattendance summary
                               FROM     (
                                        SELECT    sySas.StuEnrollId
                                                 ,sySas.StudentAttendedDate
                                                 ,sySas.ClsSectionId
                                                 ,sySas.ActualRunningScheduledDays * ISNULL(cim.IntervalInMinutes, 1) AS ConvertedActualRunningScheduledDays
                                                 ,sySas.ActualRunningPresentDays * ISNULL(cim.IntervalInMinutes, 1) AS ConvertedActualRunningPresentDays
                                                 ,sySas.ActualRunningMakeupDays * ISNULL(cim.IntervalInMinutes, 1) AS ConvertedActualRunningMakeupDays
                                        FROM      @attendanceSummary sySas
                                        LEFT JOIN @ClsSectionIntervalMinutes cim ON cim.ClsSectionId = sySas.ClsSectionId
                                        ) AS convertedSAS
                               WHERE    convertedSAS.StuEnrollId = @StuEnrollId
                                        AND convertedSAS.StudentAttendedDate <= @OffsetDate
                               GROUP BY StuEnrollId
                               ) AS sas;



                    END;
                ELSE
                    BEGIN

                        SELECT @Quantitative = ( CAST(sas.Present AS DECIMAL) / sas.Scheduled ) * 100 
                              ,@ActualHours = sas.Present
                              ,@ScheduledHours = sas.Scheduled
                        FROM   (
                               SELECT   MAX(ActualRunningScheduledDays) AS Scheduled
                                       ,MAX(ActualRunningPresentDays) + MAX(ActualRunningMakeupDays) AS Present -- add makeup to present since procedure splits it before added into syattendance summary
                               FROM     @attendanceSummary
                               WHERE    StuEnrollId = @StuEnrollId
                                        AND StudentAttendedDate <= @OffsetDate
                               GROUP BY StuEnrollId
                               ) sas;
                    END;

            END;
        ELSE
            BEGIN
                SET @Quantitative = (
                                    SELECT ( CAST(sCSEA.Earned AS DECIMAL)/ sCSEA.Attempted ) * 100 
                                    FROM   (
                                           SELECT     SUM(sCS.CreditsEarned) Earned
                                                     ,SUM(sCS.CreditsAttempted) Attempted
                                           FROM       dbo.syCreditSummary sCS
                                           INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                           INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                           WHERE      sCS.StuEnrollId = @StuEnrollId
                                                      AND CS.ReqId = sCS.ReqId
                                                      AND (
                                                          R.DateCompleted IS NOT NULL
                                                          AND R.DateCompleted <= @OffsetDate
                                                          )
                                           GROUP BY   sCS.StuEnrollId
                                           UNION ALL
                                           (SELECT     SUM(sCS.CreditsEarned)
                                                      ,SUM(sCS.CreditsAttempted)
                                            FROM       dbo.syCreditSummary sCS
                                            INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                            WHERE      sCS.StuEnrollId = @StuEnrollId
                                                       AND tG.ReqId = sCS.ReqId
                                                       AND (
                                                           tG.CompletedDate IS NOT NULL
                                                           AND tG.CompletedDate <= @OffsetDate
                                                           )
                                            GROUP BY   sCS.StuEnrollId)
                                           ) sCSEA
                                    );
            END;

    --SELECT * FROM dbo.syCreditSummary WHERE StuEnrollId  = ''F3616299-DCF2-4F59-BC8F-2F3034ED01A3''

    ---- PRINT @cumAveragecount 
    ---- PRINT @CumAverageSum
    ---- PRINT @CumAverage
    ---- PRINT @UnitTypeId;
    ---- PRINT @TrackSapAttendance;
    ---- PRINT @displayHours;
    END;



'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_TR_Sub06_Courses]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub06_Courses]', 'P') IS NULL
EXEC sp_executesql N'
-- =========================================================================================================
-- USP_TR_Sub06_Courses
-- =========================================================================================================
CREATE PROCEDURE [dbo].[USP_TR_Sub06_Courses]
    @CoursesTakenTableName NVARCHAR(200)
   ,@GPASummaryTableName NVARCHAR(200)
   ,@StuEnrollIdList NVARCHAR(MAX)
   ,@TermId UNIQUEIDENTIFIER = NULL
   ,@ShowMultipleEnrollments BIT = 0
AS --
    BEGIN
        -- 0) Declare varialbles and Set initial values
        BEGIN

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#CoursesTaken'')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            CREATE TABLE #CoursesTaken
                (
                    CoursesTakenId INTEGER NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,ReqId UNIQUEIDENTIFIER
                   ,ReqCode VARCHAR(100)
                   ,ReqDescription VARCHAR(100)
                   ,ReqCodeDescrip VARCHAR(100)
                   ,ReqCredits DECIMAL(18, 2)
                   ,MinVal DECIMAL(18, 2)
                   ,ClsSectionId VARCHAR(50)
                   ,CourseStarDate DATETIME
                   ,CourseEndDate DATETIME
                   ,SCS_CreditsAttempted DECIMAL(18, 2)
                   ,SCS_CreditsEarned DECIMAL(18, 2)
                   ,SCS_CurrentScore DECIMAL(18, 2)
                   ,SCS_CurrentGrade VARCHAR(10)
                   ,SCS_FinalScore DECIMAL(18, 2)
                   ,SCS_FinalGrade VARCHAR(10)
                   ,SCS_Completed BIT
                   ,SCS_FinalGPA DECIMAL(18, 2)
                   ,SCS_Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_WeightedAverage_Credits DECIMAL(18, 2)
                   ,SCS_Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_SimpleAverage_Credits DECIMAL(18, 2)
                   ,SCS_ModUser VARCHAR(50)
                   ,SCS_ModDate DATETIME
                   ,SCS_TermGPA_Simple DECIMAL(18, 2)
                   ,SCS_TermGPA_Weighted DECIMAL(18, 2)
                   ,SCS_coursecredits DECIMAL(18, 2)
                   ,SCS_CumulativeGPA DECIMAL(18, 2)
                   ,SCS_CumulativeGPA_Simple DECIMAL(18, 2)
                   ,SCS_FACreditsEarned DECIMAL(18, 2)
                   ,SCS_Average DECIMAL(18, 2)
                   ,SCS_CumAverage DECIMAL(18, 2)
                   ,ScheduleDays DECIMAL(18, 2)
                   ,ActualDay DECIMAL(18, 2)
                   ,FinalGPA_Calculations DECIMAL(18, 2)
                   ,FinalGPA_TermCalculations DECIMAL(18, 2)
                   ,CreditsEarned_Calculations DECIMAL(18, 2)
                   ,ActualDay_Calculations DECIMAL(18, 2)
                   ,GrdBkWgtDetailsCount INTEGER
                   ,CountMultipleEnrollment INTEGER
                   ,RowNumberMultipleEnrollment INTEGER
                   ,CountRepeated INTEGER
                   ,RowNumberRetaked INTEGER
                   ,SameTermCountRetaken INTEGER
                   ,RowNumberSameTermRetaked INTEGER
                   ,RowNumberMultEnrollNoRetaken INTEGER
                   ,RowNumberOverAllByClassSection INTEGER
                   ,IsCreditsEarned BIT
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#GPASummary'')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
            CREATE TABLE #GPASummary
                (
                    GPASummaryId INTEGER NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,LastName VARCHAR(50)
                   ,FirstName VARCHAR(50)
                   ,MiddleName VARCHAR(50)
                   ,SSN VARCHAR(11)
                   ,StudentNumber VARCHAR(50)
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,EnrollmentID VARCHAR(50)
                   ,PrgVerId UNIQUEIDENTIFIER
                   ,PrgVerDescrip VARCHAR(50)
                   ,PrgVersionTrackCredits BIT
                   ,GrdSystemId UNIQUEIDENTIFIER
                   ,AcademicType VARCHAR(50)
                   ,ClockHourProgram VARCHAR(10)
                   ,IsMakingSAP BIT
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,TermEndDate DATETIME
                   ,DescripXTranscript VARCHAR(250)
                   ,TermAverage DECIMAL(18, 2)
                   ,EnroTermSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPACredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPA DECIMAL(18, 2)
                   ,EnroTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPACredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPA DECIMAL(18, 2)
                   ,StudTermSimple_CourseCredits DECIMAL(18, 2)
                   ,StudTermSimple_GPACredits DECIMAL(18, 2)
                   ,StudTermSimple_GPA DECIMAL(18, 2)
                   ,StudTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudTermWeight_GPACredits DECIMAL(18, 2)
                   ,StudTermWeight_GPA DECIMAL(18, 2)
                   ,EnroSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroSimple_GPACredits DECIMAL(18, 2)
                   ,EnroSimple_GPA DECIMAL(18, 2)
                   ,EnroWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroWeight_GPACredits DECIMAL(18, 2)
                   ,EnroWeight_GPA DECIMAL(18, 2)
                   ,StudSimple_CourseCredits DECIMAL(18, 2)
                   ,StudSimple_GPACredits DECIMAL(18, 2)
                   ,StudSimple_GPA DECIMAL(18, 2)
                   ,StudWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudWeight_GPACredits DECIMAL(18, 2)
                   ,StudWeight_GPA DECIMAL(18, 2)
                   ,GradesFormat VARCHAR(50)
                   ,GPAMethod VARCHAR(50)
                   ,GradeBookAt VARCHAR(50)
                   ,RetakenPolicy VARCHAR(50)
                );
        END;
        -- END --  0) Declare varialbles and Set initial values

        --  1)Fill temp tables
        BEGIN
            INSERT INTO #CoursesTaken
            EXEC dbo.USP_TR_Sub03_GetPreparedGPATable @TableName = @CoursesTakenTableName;


            INSERT INTO #GPASummary
            EXEC dbo.USP_TR_Sub03_GetPreparedGPATable @TableName = @GPASummaryTableName;

        END;
        -- END  --  1)Fill temp tables

        --  3) select info for courses
        BEGIN
            SELECT   DT.PrgVerId
                    ,DT.TermId
                    ,DT.TermDescription AS TermDescription
                    ,DT.TermStartDate
                    ,DT.AcademicType
                    ,DT.ReqId
                    ,DT.Course
                    ,DT.CourseStartDate
                    ,DT.ReqCode
                    ,DT.ReqDescription
                    ,DT.ReqCodeDescrip
                    ,DT.ReqCredits
                    ,DT.MinVal
                    ,DT.SCS_ReqDescrip
                    ,DT.SCS_CreditsAttempted
                    ,DT.SCS_CreditsEarned
                    ,DT.SCS_CurrentScore
                    ,DT.SCS_CurrentGrade
                    ,DT.FinalScore
                    ,DT.FinalGrade
                    ,DT.FinalGPA
                    ,DT.ScheduleDays
                    ,DT.ActualDay
                    ,DT.GradesFormat
            INTO     #FinalResult
            FROM     (
                     SELECT     DISTINCT GS.PrgVerId AS PrgVerId
                                        ,GS.TermId AS TermId
                                        ,GS.TermDescrip AS TermDescription
                                        ,GS.TermStartDate AS TermStartDate
                                        ,GS.AcademicType AS AcademicType
                                        ,CT.ReqId AS ReqId
                                        ,CT.ReqCode AS Course
                                        ,CT.CourseStarDate AS CourseStartDate
                                        ,CT.ReqCode AS ReqCode
                                        ,CT.ReqDescription AS ReqDescription
                                        ,CT.ReqCodeDescrip AS ReqCodeDescrip
                                        ,CT.ReqCredits AS ReqCredits
                                        ,CT.MinVal AS MinVal
                                        ,CT.ReqDescription AS SCS_ReqDescrip
                                        ,CT.SCS_CreditsAttempted AS SCS_CreditsAttempted
                                        ,CT.SCS_CreditsEarned AS SCS_CreditsEarned
                                        ,CT.SCS_CurrentScore AS SCS_CurrentScore
                                        ,CT.SCS_CurrentGrade AS SCS_CurrentGrade
                                        ,CT.SCS_FinalScore AS FinalScore
                                        ,CT.SCS_FinalGrade AS FinalGrade
                                        ,CT.SCS_FinalGPA AS FinalGPA
                                        ,CT.ScheduleDays AS ScheduleDays
                                        ,CT.ActualDay AS ActualDay
                                        ,GS.GradesFormat AS GradesFormat
                                        ,CASE WHEN @TermId IS NOT NULL THEN D.Seq
                                              ELSE 0
                                         END AS Seq
                     FROM       #GPASummary AS GS
                     INNER JOIN #CoursesTaken AS CT ON CT.StuEnrollId = GS.StuEnrollId
                                                       AND CT.TermId = GS.TermId
                     INNER JOIN (
                                SELECT T.Val
                                FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1) AS T
                                ) AS List ON List.Val = GS.StuEnrollId
                     LEFT JOIN  dbo.arClassSections CS ON CS.ReqId = CT.ReqId
                     LEFT JOIN  dbo.arGrdBkWgtDetails D ON D.InstrGrdBkWgtId = CS.InstrGrdBkWgtId
                     WHERE      (
                                @TermId IS NULL
                                OR GS.TermId = @TermId
                                )
                                AND (
                                    (
                                    @ShowMultipleEnrollments = 1
                                    AND CT.RowNumberMultEnrollNoRetaken = 1
                                    )
                                    OR @ShowMultipleEnrollments = 0
                                    )
                     ) AS DT
            ORDER BY CASE WHEN @TermId IS NOT NULL THEN ( RANK() OVER ( ORDER BY DT.TermStartDate
                                                                                ,DT.TermDescription
                                                                                ,DT.Seq
                                                                                ,DT.ReqDescription
                                                                      )
                                                        )
                          ELSE ( RANK() OVER ( ORDER BY DT.CourseStartDate
                                                       ,DT.ReqDescription
                                             )
                               )
                     END;


            SELECT DISTINCT *
            FROM   #FinalResult;

            DROP TABLE #FinalResult;
        END;
        -- END  --  3) select info for courses

        -- 4) drop temp tables
        BEGIN
            --Drop temp tables
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#CoursesTaken'')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#GPASummary'')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
        END;
    -- END  --  4) drop temp tables
    END;
-- =========================================================================================================
-- END  --  USP_TR_Sub06_Courses
-- =========================================================================================================


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_TR_Sub07_TotalCourses]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub07_TotalCourses]', 'P') IS NULL
EXEC sp_executesql N'
-- =========================================================================================================  
-- USP_TR_Sub07_TotalCourses   
-- =========================================================================================================  
CREATE PROCEDURE [dbo].[USP_TR_Sub07_TotalCourses]
    @CoursesTakenTableName NVARCHAR(200)
   ,@GPASummaryTableName NVARCHAR(200)
   ,@StuEnrollIdList NVARCHAR(MAX)
   ,@TermId UNIQUEIDENTIFIER = NULL
   ,@ShowMultipleEnrollments BIT = 0
AS --  
    BEGIN

        -- 0) Declare varialbles and Set initial values  
        BEGIN
            --DECLARE @SQL01 AS NVARCHAR(MAX);  
            --DECLARE @SQL02 AS NVARCHAR(MAX);  

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#CoursesTaken'')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            CREATE TABLE #CoursesTaken
                (
                    CoursesTakenId INTEGER NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,ReqId UNIQUEIDENTIFIER
                   ,ReqCode VARCHAR(100)
                   ,ReqDescription VARCHAR(100)
                   ,ReqCodeDescrip VARCHAR(100)
                   ,ReqCredits DECIMAL(18, 2)
                   ,MinVal DECIMAL(18, 2)
                   ,ClsSectionId VARCHAR(50)
                   ,CourseStarDate DATETIME
                   ,CourseEndDate DATETIME
                   ,SCS_CreditsAttempted DECIMAL(18, 2)
                   ,SCS_CreditsEarned DECIMAL(18, 2)
                   ,SCS_CurrentScore DECIMAL(18, 2)
                   ,SCS_CurrentGrade VARCHAR(10)
                   ,SCS_FinalScore DECIMAL(18, 2)
                   ,SCS_FinalGrade VARCHAR(10)
                   ,SCS_Completed BIT
                   ,SCS_FinalGPA DECIMAL(18, 2)
                   ,SCS_Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_WeightedAverage_Credits DECIMAL(18, 2)
                   ,SCS_Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_SimpleAverage_Credits DECIMAL(18, 2)
                   ,SCS_ModUser VARCHAR(50)
                   ,SCS_ModDate DATETIME
                   ,SCS_TermGPA_Simple DECIMAL(18, 2)
                   ,SCS_TermGPA_Weighted DECIMAL(18, 2)
                   ,SCS_coursecredits DECIMAL(18, 2)
                   ,SCS_CumulativeGPA DECIMAL(18, 2)
                   ,SCS_CumulativeGPA_Simple DECIMAL(18, 2)
                   ,SCS_FACreditsEarned DECIMAL(18, 2)
                   ,SCS_Average DECIMAL(18, 2)
                   ,SCS_CumAverage DECIMAL(18, 2)
                   ,ScheduleDays DECIMAL(18, 2)
                   ,ActualDay DECIMAL(18, 2)
                   ,FinalGPA_Calculations DECIMAL(18, 2)
                   ,FinalGPA_TermCalculations DECIMAL(18, 2)
                   ,CreditsEarned_Calculations DECIMAL(18, 2)
                   ,ActualDay_Calculations DECIMAL(18, 2)
                   ,GrdBkWgtDetailsCount INTEGER
                   ,CountMultipleEnrollment INTEGER
                   ,RowNumberMultipleEnrollment INTEGER
                   ,CountRepeated INTEGER
                   ,RowNumberRetaked INTEGER
                   ,SameTermCountRetaken INTEGER
                   ,RowNumberSameTermRetaked INTEGER
                   ,RowNumberMultEnrollNoRetaken INTEGER
                   ,RowNumberOverAllByClassSection INTEGER
                   ,IsCreditsEarned BIT
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#GPASummary'')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
            CREATE TABLE #GPASummary
                (
                    GPASummaryId INTEGER NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,LastName VARCHAR(50)
                   ,FirstName VARCHAR(50)
                   ,MiddleName VARCHAR(50)
                   ,SSN VARCHAR(11)
                   ,StudentNumber VARCHAR(50)
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,EnrollmentID VARCHAR(50)
                   ,PrgVerId UNIQUEIDENTIFIER
                   ,PrgVerDescrip VARCHAR(50)
                   ,PrgVersionTrackCredits BIT
                   ,GrdSystemId UNIQUEIDENTIFIER
                   ,AcademicType VARCHAR(50)
                   ,ClockHourProgram VARCHAR(10)
                   ,IsMakingSAP BIT
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,TermEndDate DATETIME
                   ,DescripXTranscript VARCHAR(250)
                   ,TermAverage DECIMAL(18, 2)
                   ,EnroTermSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPACredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPA DECIMAL(18, 2)
                   ,EnroTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPACredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPA DECIMAL(18, 2)
                   ,StudTermSimple_CourseCredits DECIMAL(18, 2)
                   ,StudTermSimple_GPACredits DECIMAL(18, 2)
                   ,StudTermSimple_GPA DECIMAL(18, 2)
                   ,StudTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudTermWeight_GPACredits DECIMAL(18, 2)
                   ,StudTermWeight_GPA DECIMAL(18, 2)
                   ,EnroSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroSimple_GPACredits DECIMAL(18, 2)
                   ,EnroSimple_GPA DECIMAL(18, 2)
                   ,EnroWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroWeight_GPACredits DECIMAL(18, 2)
                   ,EnroWeight_GPA DECIMAL(18, 2)
                   ,StudSimple_CourseCredits DECIMAL(18, 2)
                   ,StudSimple_GPACredits DECIMAL(18, 2)
                   ,StudSimple_GPA DECIMAL(18, 2)
                   ,StudWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudWeight_GPACredits DECIMAL(18, 2)
                   ,StudWeight_GPA DECIMAL(18, 2)
                   ,GradesFormat VARCHAR(50)
                   ,GPAMethod VARCHAR(50)
                   ,GradeBookAt VARCHAR(50)
                   ,RetakenPolicy VARCHAR(50)
                );
        END;
        -- END --  0) Declare varialbles and Set initial values  

        --  1)Fill temp tables  
        BEGIN
            INSERT INTO #CoursesTaken
            EXEC USP_TR_Sub03_GetPreparedGPATable @TableName = @CoursesTakenTableName;

            INSERT INTO #GPASummary
            EXEC USP_TR_Sub03_GetPreparedGPATable @TableName = @GPASummaryTableName;
        END;
        -- END  --  1)Fill temp tables  

        -- to Test  
        -- SELECT * FROM #CoursesTaken AS CT ORDER BY CT.StudentId, CT.StuEnrollId, CT.TermId  
        -- SELECT * FROM #GPASummary AS GS ORDER BY GS.StudentId, GS.StuEnrollId, GS.TermId  



        --  2) Selet Totals  
        BEGIN
            IF ( @ShowMultipleEnrollments = 0 )
                BEGIN
                    SELECT     GS.StuEnrollId AS Id
                              ,MIN(GS.GradesFormat) AS GradesFormat
                              ,MIN(GS.GPAMethod) AS GPAMethod
                              ,MIN(   CASE WHEN (
                                                dbo.GetACIdForStudent(GS.StuEnrollId) = 5
                                                AND dbo.GetGradesFormatGivenStudentEnrollId(GS.StuEnrollId) = ''numeric''
                                                ) THEN dbo.CalculateStudentAverage(GS.StuEnrollId, NULL, NULL, NULL, @TermId)
                                           ELSE GS.EnroSimple_GPA
                                      END
                                  ) AS EnrollmentSimpleGPA
                              ,MIN(   CASE WHEN (
                                                dbo.GetACIdForStudent(GS.StuEnrollId) = 5
                                                AND dbo.GetGradesFormatGivenStudentEnrollId(GS.StuEnrollId) = ''numeric''
                                                ) THEN dbo.CalculateStudentAverage(GS.StuEnrollId, NULL, NULL, NULL, @TermId)
                                           ELSE GS.EnroWeight_GPA
                                      END
                                  ) AS EnrollmentWeightedGPA
                              ,MAX(CONVERT(INTEGER, GS.IsMakingSAP)) AS IsMakingSAP
                              ,MIN(GS.AcademicType) AS AcademicType
                              ,MIN(CONVERT(INTEGER, GS.PrgVersionTrackCredits)) AS PrgVersionTrackCredits
                              ,COUNT(CT.CoursesTakenId) AS CoursesTaked
                              ,SUM(ISNULL(CT.SCS_CreditsAttempted, 0)) AS CreditsAttempted
                              ,SUM(ISNULL(CT.CreditsEarned_Calculations, 0)) AS CreditsEarned
                              ,SUM(ISNULL(CT.ScheduleDays, 0)) AS ScheduleDays
                              ,SUM(ISNULL(CT.ActualDay_Calculations, 0)) AS ActualDay
                              ,SUM(ISNULL(CT.FinalGPA_Calculations, 0)) AS GradePoints
                    FROM       #GPASummary AS GS
                    INNER JOIN #CoursesTaken AS CT ON CT.StuEnrollId = GS.StuEnrollId
                                                      AND CT.TermId = GS.TermId
                    WHERE      EXISTS (
                                      SELECT Val
                                      FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                      WHERE  CONVERT(UNIQUEIDENTIFIER, Val) = CT.StuEnrollId
                                      )
                               AND (
                                   @TermId IS NULL
                                   OR GS.TermId = @TermId
                                   )
                    GROUP BY   GS.StuEnrollId;
                END;
            ELSE
                BEGIN
                    SELECT     GS.StudentId AS Id
                              ,MIN(GS.GradesFormat) AS GradesFormat
                              ,MIN(GS.GPAMethod) AS GPAMethod
                              ,MIN(GS.StudSimple_GPA) AS EnrollmentSimpleGPA
                              ,MIN(GS.StudWeight_GPA) AS EnrollmentWeightedGPA
                              ,MAX(CONVERT(INTEGER, GS.IsMakingSAP)) AS IsMakingSAP
                              ,MIN(GS.AcademicType) AS AcademicType
                              ,MIN(CONVERT(INTEGER, GS.PrgVersionTrackCredits)) AS PrgVersionTrackCredits
                              ,COUNT(CT.CoursesTakenId) AS CoursesTaked
                              ,SUM(ISNULL(CT.SCS_CreditsAttempted, 0)) AS CreditsAttempted
                              ,SUM(ISNULL(CT.CreditsEarned_Calculations, 0)) AS CreditsEarned
                              ,SUM(ISNULL(CT.ScheduleDays, 0)) AS ScheduleDays
                              ,SUM(ISNULL(CT.ActualDay_Calculations, 0)) AS ActualDay
                              ,SUM(ISNULL(CT.FinalGPA_Calculations, 0)) AS GradePoints
                    FROM       #GPASummary AS GS
                    INNER JOIN #CoursesTaken AS CT ON CT.StuEnrollId = GS.StuEnrollId
                                                      AND CT.TermId = GS.TermId
                    WHERE      CT.RowNumberMultEnrollNoRetaken = 1
                               AND EXISTS (
                                          SELECT Val
                                          FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                          WHERE  CONVERT(UNIQUEIDENTIFIER, Val) = CT.StuEnrollId
                                          )
                               AND (
                                   @TermId IS NULL
                                   OR GS.TermId = @TermId
                                   )
                    GROUP BY   GS.StudentId;
                END;
        END;
        -- END  --  2) Selet Totals  

        -- 3) drop temp tables  
        BEGIN
            -- Drop temp tables  
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#CoursesTaken'')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#GPASummary'')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
        END;
    -- END  --  3) drop temp tables  
    END;
-- =========================================================================================================  
-- END  --  USP_TR_Sub07_TotalCourses   
-- =========================================================================================================  

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_TR_Sub11_ComponentsByCourse_GivenComponentTypeId]'
GO
IF OBJECT_ID(N'[dbo].[Usp_TR_Sub11_ComponentsByCourse_GivenComponentTypeId]', 'P') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[Usp_TR_Sub11_ComponentsByCourse_GivenComponentTypeId]
--EXEC Usp_TR_Sub11_ComponentsByCourse_GivenComponentTypeId ''2825E1F6-ADD1-4365-9E88-0028F7E9C567'',NULL,NULL,''5C188D11-7C81-45E5-8280-7E1BE8D2CC12'',NULL
    @StuEnrollIdList VARCHAR(MAX)
   ,@TermId VARCHAR(50) = NULL
   ,@SysComponentTypeIdList VARCHAR(50) = NULL
   ,@ReqId VARCHAR(50)
   ,@SysComponentTypeId VARCHAR(10) = NULL
AS
    DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
    DECLARE @GradeBookAt VARCHAR(50);
    DECLARE @CharInt INT;
    DECLARE @Counter AS INT;  
    DECLARE @times AS INT;
	--DECLARE @TermStartDate1 AS DATETIME;     
    DECLARE @Score AS DECIMAL(18,2);
    DECLARE @GrdCompDescrip AS VARCHAR(50);


    DECLARE @curId AS UNIQUEIDENTIFIER;
    DECLARE @curReqId AS UNIQUEIDENTIFIER;
    DECLARE @curStuEnrollId AS UNIQUEIDENTIFIER;
    DECLARE @curDescrip AS VARCHAR(50);
    DECLARE @curNumber AS INT;
    DECLARE @curGrdComponentTypeId AS INT;
    DECLARE @curMinResult AS DECIMAL(18,2); 
    DECLARE @curGrdComponentDescription AS VARCHAR(50);   
    DECLARE @curClsSectionId AS UNIQUEIDENTIFIER;
    DECLARE @curTermId AS UNIQUEIDENTIFIER;
    DECLARE @curRownumber AS INT;
	 DECLARE @curRowSeq AS INT;
    DECLARE @DefaulSysComponentTypeIdList VARCHAR(MAX);
	
    SET @DefaulSysComponentTypeIdList = ''501,544,502,499,503,500,533'';
 
    CREATE TABLE #Temp1
        (
         Id UNIQUEIDENTIFIER
        ,TermId UNIQUEIDENTIFIER
        ,ReqId UNIQUEIDENTIFIER
        ,GradeBookDescription VARCHAR(50)
        ,Number INT
        ,GradeBookSysComponentTypeId INT
        ,GradeBookScore DECIMAL(18,2)
        ,MinResult DECIMAL(18,2)
        ,GradeComponentDescription VARCHAR(50)
        ,ClsSectionId UNIQUEIDENTIFIER
        ,StuEnrollId UNIQUEIDENTIFIER
        ,RowNumber INT
		,Seq int
        );
    CREATE TABLE #Temp2
        (
         ReqId UNIQUEIDENTIFIER
        ,EffectiveDate DATETIME
        );

    IF @SysComponentTypeIdList IS NULL
        BEGIN
            SET @SysComponentTypeIdList = @DefaulSysComponentTypeIdList;
        END;

    SET @StuEnrollCampusId = COALESCE((
                                        SELECT TOP 1
                                                ASE.CampusId
                                        FROM    arStuEnrollments AS ASE
                                        WHERE   ASE.StuEnrollId IN ( SELECT Val
                                                                     FROM   MultipleValuesForReportParameters(@StuEnrollIdList,'','',1) )
                                      ),NULL);
    SET @GradeBookAt = ( dbo.GetAppSettingValueByKeyName(''GradeBookWeightingLevel'',@StuEnrollCampusId) );     

    IF ( @GradeBookAt IS NOT NULL )
        BEGIN
            SET @GradeBookAt = LOWER(LTRIM(RTRIM(@GradeBookAt)));
        END;

    SET @CharInt = (
                     SELECT CHARINDEX(@SysComponentTypeId,@SysComponentTypeIdList)
                   );

    
    IF @GradeBookAt = ''instructorlevel''
        BEGIN
            SELECT  CASE WHEN GradeBookDescription IS NULL THEN GradeComponentDescription
                         ELSE GradeBookDescription
                    END AS GradeBookDescription
                   ,MinResult
                   ,GradeBookScore
                   ,GradeComponentDescription
                   ,GradeBookSysComponentTypeId
                   ,StuEnrollId
                   ,rownumber
                   ,TermId
                   ,ReqId
            FROM    (
                      SELECT    AR2.ReqId
                               ,AT.TermId
                               ,CASE WHEN AGBWD.Descrip IS NULL THEN AGCT.Descrip
                                     ELSE AGBWD.Descrip
                                END AS GradeBookDescription
                               ,( CASE WHEN AGCT.SysComponentTypeId IN ( 500,503,544 ) THEN AGBWD.Number
                                       ELSE (
                                              SELECT    MIN(MinVal)
                                              FROM      arGradeScaleDetails GSD
                                                       ,arGradeSystemDetails GSS
                                              WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                        AND GSS.IsPass = 1
                                                        AND GSD.GrdScaleId = ACS.GrdScaleId
                                            )
                                  END ) AS MinResult
                               ,AGBWD.Required
                               ,AGBWD.MustPass
                               ,ISNULL(AGCT.SysComponentTypeId,0) AS GradeBookSysComponentTypeId
                               ,AGBWD.Number
                               ,SR.Resource AS GradeComponentDescription
                               ,AGBWD.InstrGrdBkWgtDetailId
                               ,ASE.StuEnrollId
                               ,0 AS IsExternShip
                               ,CASE AGCT.SysComponentTypeId
                                  WHEN 544 THEN (
                                                  SELECT    SUM(ISNULL(AEA.HoursAttended,0.0))
                                                  FROM      arExternshipAttendance AS AEA
                                                  WHERE     AEA.StuEnrollId = ASE.StuEnrollId
                                                )
                                  ELSE (
                                         SELECT SUM(ISNULL(AGBR1.Score,0.0))
                                         FROM   arGrdBkResults AS AGBR1
                                         WHERE  AGBR1.StuEnrollId = ASE.StuEnrollId
                                                AND AGBR1.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                                                AND AGBR1.ClsSectionId = ACS.ClsSectionId
                                       )
                                END AS GradeBookScore,
								AGBWD.Seq
                               ,ROW_NUMBER() OVER ( PARTITION BY ASE.StuEnrollId,AT.TermId,AR2.ReqId ORDER BY AGCT.SysComponentTypeId, AGBWD.Descrip ) AS rownumber
                      FROM      arGrdBkWgtDetails AS AGBWD
                      INNER JOIN arGrdBkResults AS AGBR ON AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId 
							--INNER JOIN arGrdBkWeights AS AGBW ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                      INNER JOIN arClassSections AS ACS ON ACS.ClsSectionId = AGBR.ClsSectionId
                      INNER JOIN arResults AS AR ON AR.TestId = ACS.ClsSectionId
                      INNER JOIN arGrdComponentTypes AS AGCT ON AGCT.GrdComponentTypeId = AGBWD.GrdComponentTypeId
                      INNER JOIN arStuEnrollments AS ASE ON ASE.StuEnrollId = AR.StuEnrollId
                      INNER JOIN arTerm AS AT ON AT.TermId = ACS.TermId
                      INNER JOIN arReqs AS AR2 ON AR2.ReqId = ACS.ReqId
                      INNER JOIN syResources AS SR ON SR.ResourceID = AGCT.SysComponentTypeId
                      WHERE     AR.StuEnrollId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@StuEnrollIdList,'','',1) )
                                AND AT.TermId = @TermId
                                AND AR2.ReqId = @ReqId
                                AND AGCT.SysComponentTypeId IN ( SELECT Val
                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeIdList,'','',1) )
                      UNION
                      SELECT    AR2.ReqId
                               ,AT.TermId
                               ,CASE WHEN AGBW.Descrip IS NULL THEN (
                                                                      SELECT    Resource
                                                                      FROM      syResources
                                                                      WHERE     ResourceID = AGCT.SysComponentTypeId
                                                                    )
                                     ELSE AGBW.Descrip
                                END AS GradeBookDescription
                               ,CASE WHEN AGCT.SysComponentTypeId IN ( 500,503,544 ) THEN AGBWD.Number
                                     ELSE (
                                            SELECT  MIN(MinVal)
                                            FROM    arGradeScaleDetails GSD
                                                   ,arGradeSystemDetails GSS
                                            WHERE   GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                    AND GSS.IsPass = 1
                                                    AND GSD.GrdScaleId = ACS.GrdScaleId
                                          )
                                END AS MinResult
                               ,AGBWD.Required
                               ,AGBWD.MustPass
                               ,ISNULL(AGCT.SysComponentTypeId,0) AS GradeBookSysComponentTypeId
                               ,AGBWD.Number
                               ,(
                                  SELECT    Resource
                                  FROM      syResources
                                  WHERE     ResourceID = AGCT.SysComponentTypeId
                                ) AS GradeComponentDescription
                               ,AGBWD.InstrGrdBkWgtDetailId
                               ,ASE.StuEnrollId
                               ,0 AS IsExternShip
                               ,CASE AGCT.SysComponentTypeId
                                  WHEN 544 THEN (
                                                  SELECT    SUM(ISNULL(AEA.HoursAttended,0.0))
                                                  FROM      arExternshipAttendance AS AEA
                                                  WHERE     AEA.StuEnrollId = ASE.StuEnrollId
                                                )
                                  ELSE (
                                         SELECT SUM(ISNULL(AGBR.Score,0.0))
                                         FROM   arGrdBkResults AS AGBR
                                         WHERE  AGBR.StuEnrollId = ASE.StuEnrollId
                                                AND AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                                                AND AGBR.ClsSectionId = ACS.ClsSectionId
                                       )
                                END AS GradeBookScore
								, AGBWD.Seq
                               ,ROW_NUMBER() OVER ( PARTITION BY ASE.StuEnrollId,AT.TermId,AR2.ReqId ORDER BY AGCT.SysComponentTypeId, AGCT.Descrip ) AS rownumber
                      FROM      arGrdBkConversionResults GBCR
                      INNER JOIN arStuEnrollments AS ASE ON ASE.StuEnrollId = GBCR.StuEnrollId
                      INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId
                      INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
                      INNER JOIN arTerm AS AT ON AT.TermId = GBCR.TermId
                      INNER JOIN arReqs AS AR2 ON AR2.ReqId = GBCR.ReqId
                      INNER JOIN arGrdComponentTypes AS AGCT ON AGCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                      INNER JOIN arGrdBkWgtDetails AS AGBWD ON AGBWD.GrdComponentTypeId = AGCT.GrdComponentTypeId
                                                               AND AGBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                      INNER JOIN arGrdBkWeights AS AGBW ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                                                           AND AGBW.ReqId = GBCR.ReqId
                      INNER JOIN (
                                   SELECT   AGBW.ReqId
                                           ,MAX(AGBW.EffectiveDate) AS EffectiveDate
                                   FROM     arGrdBkWeights AS AGBW
                                   GROUP BY AGBW.ReqId
                                 ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                      INNER JOIN (
                                   SELECT   SR.Resource
                                           ,SR.ResourceID
                                   FROM     syResources AS SR
                                   WHERE    SR.ResourceTypeID = 10
                                 ) SYRES ON SYRES.ResourceID = AGCT.SysComponentTypeId
                      INNER JOIN syCampuses AS SC ON SC.CampusId = ASE.CampusId
                      INNER JOIN arClassSections AS ACS ON ACS.TermId = AT.TermId
                                                           AND ACS.ReqId = AR2.ReqId
                      LEFT JOIN arGrdBkResults AS AGBR ON AGBR.StuEnrollId = ASE.StuEnrollId
                                                          AND AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                                                          AND AGBR.ClsSectionId = ACS.ClsSectionId
                      WHERE     MaxEffectiveDatesByCourse.EffectiveDate <= AT.StartDate
                                AND ASE.StuEnrollId IN ( SELECT Val
                                                         FROM   MultipleValuesForReportParameters(@StuEnrollIdList,'','',1) )
                                AND AT.TermId = @TermId
                                AND AR2.ReqId = @ReqId
                                AND AGCT.SysComponentTypeId IN ( SELECT Val
                                                                 FROM   MultipleValuesForReportParameters(@SysComponentTypeIdList,'','',1) )
                    ) dt
            WHERE   (
                      @SysComponentTypeId IS NULL
                      OR dt.GradeBookSysComponentTypeId = @SysComponentTypeId
                    )
            ORDER BY dt.Seq,
			 GradeComponentDescription
                   ,rownumber
                   ,GradeBookDescription; 
        END;
    ELSE
        BEGIN


            SELECT  AGBWD.InstrGrdBkWgtDetailId
                   ,AGBWD.InstrGrdBkWgtId
                   ,AGBWD.Code
                   ,AGBWD.Descrip
                   ,AGBWD.Weight
                   ,AGBWD.Seq
                   ,AGBWD.ModUser
                   ,AGBWD.ModDate
                   ,AGBWD.GrdComponentTypeId
                   ,AGBWD.Parameter
                   ,AGBWD.Number
                   ,AGBWD.GrdPolicyId
                   ,AGBWD.Required
                   ,AGBWD.MustPass
                   ,AGBWD.CreditsPerService
            INTO    #tmpInnerQueryTable
            FROM    arGrdBkWgtDetails AS AGBWD
            INNER JOIN arGrdBkResults AS AGBR ON AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
            INNER JOIN arClassSections AS ACS ON ACS.ClsSectionId = AGBR.ClsSectionId
            WHERE   AGBR.StuEnrollId IN ( SELECT    Val
                                          FROM      MultipleValuesForReportParameters(@StuEnrollIdList,'','',1) )
                    AND ACS.ReqId = @ReqId;

            SELECT  ReqId
                   ,MAX(EffectiveDate) AS EffectiveDate
            INTO    #tmpGrdBkWeightsWithMaxEffectiveDates
            FROM    arGrdBkWeights
            GROUP BY ReqId;
                                        --HAVING ReqId=@ReqId

			--SET @TermStartDate1 = (
			--                       SELECT   AT.StartDate
			--                       FROM     arTerm AS AT
			--                       WHERE    AT.TermId = @TermId
			--                      );
            INSERT  INTO #Temp2
                    SELECT  AGBW.ReqId
                           ,MAX(AGBW.EffectiveDate) AS EffectiveDate
                    FROM    arGrdBkWeights AS AGBW
                    INNER JOIN syCreditSummary AS SCS ON SCS.ReqId = AGBW.ReqId
                    WHERE   SCS.StuEnrollId IN ( SELECT Val
                                                 FROM   MultipleValuesForReportParameters(@StuEnrollIdList,'','',1) ) 
				   -- AND SCS.TermId = @TermId
				   -- AND AGBW.EffectiveDate <= @TermStartDate1
                            AND SCS.ReqId = @ReqId
                    GROUP BY AGBW.ReqId;

            DECLARE getUsers_Cursor CURSOR FAST_FORWARD FORWARD_ONLY
            FOR
                SELECT  dt.ID
                       ,dt.ReqId
                       ,dt.Descrip
                       ,dt.Number
                       ,dt.SysComponentTypeId
                       ,dt.MinResult
                       ,dt.GradeComponentDescription
                       ,dt.ClsSectionId
                       ,dt.StuEnrollId
                       ,dt.TermId
                       ,ROW_NUMBER() OVER ( PARTITION BY dt.StuEnrollId,dt.TermId,dt.SysComponentTypeId ORDER BY dt.SysComponentTypeId, dt.Descrip ) AS rownumber
					   ,dt.seq
                FROM    (
                          SELECT DISTINCT
                                    ISNULL(GD.InstrGrdBkWgtDetailId,NEWID()) AS ID
                                   ,AR.ReqId
                                   ,GC.Descrip
                                   ,GD.Number
                                   ,GC.SysComponentTypeId
                                   ,( CASE WHEN GC.SysComponentTypeId IN ( 500,503,544 ) THEN GD.Number
                                           ELSE (
                                                  SELECT    MIN(MinVal)
                                                  FROM      arGradeScaleDetails GSD
                                                           ,arGradeSystemDetails GSS
                                                  WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                            AND GSS.IsPass = 1
                                                            AND GSD.GrdScaleId = CS.GrdScaleId
                                                )
                                      END ) AS MinResult
                                   ,S.Resource AS GradeComponentDescription
                                   ,CS.ClsSectionId
                                   ,RES.StuEnrollId
                                   ,T.TermId
								   ,GD.Seq
                          FROM      arGrdComponentTypes AS GC
                          INNER JOIN #tmpInnerQueryTable AS GD ON GD.GrdComponentTypeId = GC.GrdComponentTypeId
                          INNER JOIN arGrdBkWeights AS AGBW1 ON AGBW1.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
                          INNER JOIN arReqs AS AR ON AR.ReqId = AGBW1.ReqId
                          INNER JOIN arClassSections AS CS ON CS.ReqId = AR.ReqId
                          INNER JOIN syResources AS S ON S.ResourceID = GC.SysComponentTypeId
                          INNER JOIN arResults AS RES ON RES.TestId = CS.ClsSectionId
                          INNER JOIN arTerm AS T ON T.TermId = CS.TermId
                          WHERE     RES.StuEnrollId IN ( SELECT Val
                                                         FROM   MultipleValuesForReportParameters(@StuEnrollIdList,'','',1) )
                                    AND GD.Number > 0
                                    AND AR.ReqId = @ReqId
                        ) dt
                ORDER BY SysComponentTypeId
                       ,rownumber;
            OPEN getUsers_Cursor;
            FETCH NEXT FROM getUsers_Cursor
					INTO @curId,@curReqId,@curDescrip,@curNumber,@curGrdComponentTypeId,@curMinResult,@curGrdComponentDescription,@curClsSectionId,
                @curStuEnrollId,@curTermId,@curRownumber, @curRowSeq;
            SET @Counter = 0;
            WHILE @@FETCH_STATUS = 0
                BEGIN
					--PRINT @Number;
                    SET @times = 1;
                    WHILE @times <= @curNumber
                        BEGIN
							--PRINT @times;
                            IF @curNumber > 1
                                BEGIN
                                    SET @GrdCompDescrip = @curDescrip + CAST(@times AS CHAR);
                                    IF @curGrdComponentTypeId = 544
                                        BEGIN
                                            SET @Score = (
                                                           SELECT   SUM(ISNULL(AEA.HoursAttended,0.0))
                                                           FROM     arExternshipAttendance AS AEA
                                                           WHERE    AEA.StuEnrollId = @curStuEnrollId
                                                         );
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @Score = (
                                                           SELECT   SUM(ISNULL(AGBR.Score,0.0))
                                                           FROM     arGrdBkResults AS AGBR
                                                           WHERE    AGBR.StuEnrollId = @curStuEnrollId
                                                                    AND AGBR.InstrGrdBkWgtDetailId = @curId
                                                                    AND AGBR.ResNum = @times
                                                                    AND AGBR.ClsSectionId = @curClsSectionId
                                                         );
                                        END;
                                    SET @curRownumber = @times;
                                END;
                            ELSE
                                BEGIN
                                    SET @GrdCompDescrip = @curDescrip;
                                    SET @Score = (
                                                   SELECT TOP 1
                                                            Score
                                                   FROM     arGrdBkResults
                                                   WHERE    StuEnrollId = @curStuEnrollId
                                                            AND InstrGrdBkWgtDetailId = @curId
                                                            AND ResNum = @times
                                                 );
                                    IF @Score IS NULL
                                        BEGIN
                                            SET @Score = (
                                                           SELECT TOP 1
                                                                    Score
                                                           FROM     arGrdBkResults
                                                           WHERE    StuEnrollId = @curStuEnrollId
                                                                    AND InstrGrdBkWgtDetailId = @curId
                                                                    AND ResNum = ( @times - 1 )
                                                         ); 
                                        END;
                                    IF @curGrdComponentTypeId = 544
                                        BEGIN
                                            SET @Score = (
                                                           SELECT   SUM(ISNULL(AEA.HoursAttended,0.0))
                                                           FROM     arExternshipAttendance AS AEA
                                                           WHERE    AEA.StuEnrollId = @curStuEnrollId
                                                         );
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @Score = (
                                                           SELECT   SUM(ISNULL(AGBR.Score,0.0))
                                                           FROM     arGrdBkResults AS AGBR
                                                           WHERE    AGBR.StuEnrollId = @curStuEnrollId
                                                                    AND AGBR.InstrGrdBkWgtDetailId = @curId
                                                                    AND AGBR.ResNum = @times
                                                         );
                                        END;
                                END;
							--PRINT @Score;
                            INSERT  INTO #Temp1
                            VALUES  ( @curId,@curTermId,@curReqId,@GrdCompDescrip,@curNumber,@curGrdComponentTypeId,@Score,@curMinResult,
                                      @curGrdComponentDescription,@curClsSectionId,@curStuEnrollId,@curRownumber, @curRowSeq );
							
                            SET @times = @times + 1;
                        END;

                    FETCH NEXT FROM getUsers_Cursor
					INTO @curId,@curReqId,@curDescrip,@curNumber,@curGrdComponentTypeId,@curMinResult,@curGrdComponentDescription,@curClsSectionId,
                        @curStuEnrollId,@curTermId,@curRownumber, @curRowSeq;
                END;
            CLOSE getUsers_Cursor; 
            DEALLOCATE getUsers_Cursor; 

            SET @CharInt = (
                             SELECT CHARINDEX(@SysComponentTypeId,@SysComponentTypeIdList)
                           );
		
            IF ( @CharInt >= 1 )
                BEGIN
                    SELECT  GradeBookDescription
                           ,MinResult
                           ,GradeBookScore
                           ,GradeComponentDescription
                           ,GradeBookSysComponentTypeId
                           ,StuEnrollId
                           ,RowNumber
                           ,TermId
                           ,ReqId
						   ,seq
                          --   Id  , Number, ClsSectionId
                    FROM    #Temp1
                    WHERE   GradeBookSysComponentTypeId = @SysComponentTypeId
                    UNION
                    SELECT  CASE WHEN GBWD.Descrip IS NULL THEN GCT.Descrip
                                 ELSE GBWD.Descrip
                            END AS GradeBookDescription
                           ,GBCR.MinResult AS MinResult
                           ,GBCR.Score AS GradeBookScore
                           ,SYRES.Resource AS GradeComponentDescription
                           ,ISNULL(GCT.SysComponentTypeId,0) AS GradeBookSysComponentTypeId
                           ,SE.StuEnrollId
                           ,ROW_NUMBER() OVER ( PARTITION BY T.TermId,GCT.SysComponentTypeId ORDER BY GCT.SysComponentTypeId, GCT.Descrip ) AS rownumber
                           ,T.TermId
                           ,R.ReqId
						   ,GBWD.Seq
                         --, GBWD.InstrGrdBkWgtDetailId, GBWD.Number, SYRES.Resource
                          --, (
                          --   SELECT TOP 1
                          --          ClsSectionId
                          --   FROM   arClassSections
                          --   WHERE  TermId = T.TermId
                          --          AND ReqId = R.ReqId
                          --  ) AS ClsSectionId
                    FROM    arGrdBkConversionResults GBCR
                    INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arStudent S ON S.StudentId = SE.StudentId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                    INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                    INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                    INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                         AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
							--INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
							--                                 AND GBCR.ReqId = GBW.ReqId
                    INNER JOIN #tmpGrdBkWeightsWithMaxEffectiveDates AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                    INNER JOIN (
                                 SELECT Resource
                                       ,ResourceID
                                 FROM   syResources
                                 WHERE  ResourceTypeID = 10
                               ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                    INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                    WHERE   MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                            AND SE.StuEnrollId IN ( SELECT  Val
                                                    FROM    MultipleValuesForReportParameters(@StuEnrollIdList,'','',1) )
                            AND T.TermId = @TermId
                            AND R.ReqId = @ReqId
                            AND GCT.SysComponentTypeId IN ( SELECT  Val
                                                            FROM    MultipleValuesForReportParameters(@SysComponentTypeIdList,'','',1) )
                            AND (
                                  @SysComponentTypeId IS NULL
                                  OR GCT.SysComponentTypeId = @SysComponentTypeId
                                )
                    ORDER BY seq,
					GradeBookSysComponentTypeId
                           ,GradeBookDescription
                           ,rownumber; 
                END; 
            DROP TABLE #tmpInnerQueryTable;
        END;
    DROP TABLE #Temp2;
    DROP TABLE #Temp1;
	


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[adLeadsView]'
GO
IF OBJECT_ID(N'[dbo].[adLeadsView]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[adLeadsView]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[GlTransView1]'
GO
IF OBJECT_ID(N'[dbo].[GlTransView1]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[GlTransView1]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[GlTransView2]'
GO
IF OBJECT_ID(N'[dbo].[GlTransView2]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[GlTransView2]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[NewStudentSearch]'
GO
IF OBJECT_ID(N'[dbo].[NewStudentSearch]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[NewStudentSearch]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[plStudentEducation]'
GO
IF OBJECT_ID(N'[dbo].[plStudentEducation]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[plStudentEducation]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[plStudentEmployment]'
GO
IF OBJECT_ID(N'[dbo].[plStudentEmployment]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[plStudentEmployment]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[plStudentExtraCurriculars]'
GO
IF OBJECT_ID(N'[dbo].[plStudentExtraCurriculars]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[plStudentExtraCurriculars]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[plStudentSkills]'
GO
IF OBJECT_ID(N'[dbo].[plStudentSkills]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[plStudentSkills]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[postfinalgrades]'
GO
IF OBJECT_ID(N'[dbo].[postfinalgrades]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[postfinalgrades]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[SettingStdScoreLimit]'
GO
IF OBJECT_ID(N'[dbo].[SettingStdScoreLimit]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[SettingStdScoreLimit]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[StudentEnrollmentSearch]'
GO
IF OBJECT_ID(N'[dbo].[StudentEnrollmentSearch]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[StudentEnrollmentSearch]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[syStudentContactAddresses]'
GO
IF OBJECT_ID(N'[dbo].[syStudentContactAddresses]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[syStudentContactAddresses]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[syStudentContactPhones]'
GO
IF OBJECT_ID(N'[dbo].[syStudentContactPhones]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[syStudentContactPhones]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[syStudentContacts]'
GO
IF OBJECT_ID(N'[dbo].[syStudentContacts]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[syStudentContacts]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[syStudentNotes]'
GO
IF OBJECT_ID(N'[dbo].[syStudentNotes]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[syStudentNotes]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[UnscheduledDates]'
GO
IF OBJECT_ID(N'[dbo].[UnscheduledDates]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[UnscheduledDates]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[VIEW_GetHistory]'
GO
IF OBJECT_ID(N'[dbo].[VIEW_GetHistory]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[VIEW_GetHistory]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[VIEW_LeadUserTask]'
GO
IF OBJECT_ID(N'[dbo].[VIEW_LeadUserTask]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[VIEW_LeadUserTask]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[View_Messages]'
GO
IF OBJECT_ID(N'[dbo].[View_Messages]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[View_Messages]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[VIEW_SySDFModuleValue_Lead]'
GO
IF OBJECT_ID(N'[dbo].[VIEW_SySDFModuleValue_Lead]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[VIEW_SySDFModuleValue_Lead]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[View_TaskNotes]'
GO
IF OBJECT_ID(N'[dbo].[View_TaskNotes]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[View_TaskNotes]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[VIEW1]'
GO
IF OBJECT_ID(N'[dbo].[VIEW1]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[VIEW1]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[usp_EdExpress_GetPendingPayments]'
GO
IF OBJECT_ID(N'[dbo].[usp_EdExpress_GetPendingPayments]', 'P') IS NULL
EXEC sp_executesql N'

CREATE PROCEDURE [dbo].[usp_EdExpress_GetPendingPayments]
    @FileName VARCHAR(250)
   ,@CampusIds VARCHAR(1000)
   ,@EnrollmentStatustIds VARCHAR(1000)
   ,@FundSourceIds VARCHAR(1000)
   ,@LenderIds VARCHAR(1000)
   ,@ExpDateFrom DATETIME
   ,@ExpDateTo DATETIME
AS
    BEGIN

        DECLARE @SQL VARCHAR(MAX);

        SET @SQL = '' Select ST.ParentId, DT.DetailId,ST.StudentAwardID, DT.AwardScheduleId, ST.FirstName, ST.LastName,ST.SSN,ST.OriginalSSN, ST.CampusName,ST.StuEnrollmentId, ST.CampusId,'';
        SET @SQL = @SQL
            + '' (select Max(LDA) from ( select max(AttendedDate)as LDA from arExternshipAttendance where StuEnrollId=ST.StuEnrollmentId union all select max(MeetDate) as LDA from atClsSectAttendance where StuEnrollId=ST.StuEnrollmentId and Actual >= 1 uni
on all select max(AttendanceDate) as LDA from atAttendance where EnrollId=ST.StuEnrollmentId and Actual >=1 union all select max(RecordDate) as LDA from arStudentClockAttendance where StuEnrollId=ST.StuEnrollmentId and (ActualHours >=1.00 and ActualHours 
<> 99.00 and ActualHours <> 999.00 and ActualHours <> 9999.00) union all select max(MeetDate) as LDA from atConversionAttendance where StuEnrollId=ST.StuEnrollmentId and (Actual >=1.00 and Actual <> 99.00 and Actual <> 999.00 and Actual <> 9999.00) )TR) a
s LDA, '';
        SET @SQL = @SQL
            + '' ST.AwardId, CASE(ST.DbIn) When ''''T'''' Then Case (ST.GrantType )when '''''''' then ''''PELL'''' When ''''P'''' then ''''PELL'''' When ''''A'''' then ''''ACG'''' when ''''S'''' then ''''SMART'''' when ''''T'''' then ''''SMART'''' else '''''''' End Else ''''TEACH'''' End AS GrantType, DT.Di
sbDate as ExpDisbDate ,DT.DisbNum,DT.DusbSeqNum,DT.SimittedDisbAmount,DT.AccDisbAmount,DT.AccDisbAmount as PaymentAmount, DT.DisbDate, ST.IsInSchool AS Pay, ST.IsInSchool, ST.AcademicYearId ''; 
        SET @SQL = @SQL
            + '' From syEDExpNotPostPell_ACG_SMART_Teach_StudentTable ST, syEDExpNotPostPell_ACG_SMART_Teach_DisbursementTable DT, arStuEnrollments SE , faStudentAwards SA ''; 
        SET @SQL = @SQL + '' Where ST.ParentId=DT.ParentId and SE.StuEnrollId=ST.StuEnrollmentId and SA.StudentAwardId=ST.StudentAwardId And ST.FileName=''''''
            + @FileName + '''''''';
        IF @CampusIds <> ''''
            BEGIN
                SET @SQL = @SQL + @CampusIds;
            END;
        IF @EnrollmentStatustIds <> ''''
            BEGIN
                SET @SQL = @SQL + @EnrollmentStatustIds;
            END;
        IF @FundSourceIds <> ''''
            BEGIN
                SET @SQL = @SQL + @FundSourceIds;
            END;
        IF @LenderIds <> ''''
            BEGIN
                SET @SQL = @SQL + @LenderIds;
            END;
        IF @ExpDateFrom IS NOT NULL
            AND @ExpDateTo IS NOT NULL
            BEGIN
                SET @SQL = @SQL + '' And DT.DisbDate>='''''' + CONVERT(VARCHAR(10),@ExpDateFrom,101) + '''''' and DT.DisbDate<='''''' + CONVERT(VARCHAR(10),@ExpDateTo,101)
                    + '''''''';
            END;
        IF @ExpDateFrom IS NOT NULL
            AND @ExpDateTo IS NULL
            BEGIN
                SET @SQL = @SQL + '' And DT.DisbDate>='''''' + CONVERT(VARCHAR(10),@ExpDateFrom,101) + '''''''';
            END;
        IF @ExpDateFrom IS NULL
            AND @ExpDateTo IS NOT NULL
            BEGIN
                SET @SQL = @SQL + '' And DT.DisbDate<='''''' + CONVERT(VARCHAR(10),@ExpDateTo,101) + '''''''';
            END;
        SET @SQL = @SQL + '' Order by DisbNum'';
        PRINT @SQL;
        EXEC (@SQL); 
    END;




'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[arPrgGrp]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_arPrgGrp_syCampGrps_CampGrpId_CampGrpId]','F') AND parent_object_id = OBJECT_ID(N'[dbo].[arPrgGrp]', 'U'))
ALTER TABLE [dbo].[arPrgGrp] ADD CONSTRAINT [FK_arPrgGrp_syCampGrps_CampGrpId_CampGrpId] FOREIGN KEY ([CampGrpId]) REFERENCES [dbo].[syCampGrps] ([CampGrpId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END