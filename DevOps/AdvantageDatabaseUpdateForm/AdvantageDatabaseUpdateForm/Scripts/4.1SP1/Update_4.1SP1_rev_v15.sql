﻿/*
Run this script on:

        dev-com-db1.dev.fameinc.com\adv.AvedaLive    -  This database will be modified

to synchronize it with a database with the schema represented by:

        Source

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.7.10021 from Red Gate Software Ltd at 8/12/2019 3:29:51 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
/*
* Use this Pre-Deployment script to perform tasks before the deployment of the project.
* Read more at https://www.red-gate.com/SOC7/pre-deployment-script-help
*/
UPDATE dbo.arClsSectMeetings
SET PeriodId = NULL
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)

DELETE FROM syPeriodsWorkDays
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[Trigger_UpdateProgressOfCourses] from [dbo].[arGrdBkResults]'
GO
IF OBJECT_ID(N'[dbo].[Trigger_UpdateProgressOfCourses]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[Trigger_UpdateProgressOfCourses]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[Trigger_arResults_IsClinicsSatisified] from [dbo].[arResults]'
GO
IF OBJECT_ID(N'[dbo].[Trigger_arResults_IsClinicsSatisified]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[Trigger_arResults_IsClinicsSatisified]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_RE_REGISTER_STUDENT_RESULTS]'
GO
IF OBJECT_ID(N'[dbo].[USP_RE_REGISTER_STUDENT_RESULTS]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_RE_REGISTER_STUDENT_RESULTS]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[usp_GetClassroomWorkServices]'
GO
IF OBJECT_ID(N'[dbo].[usp_GetClassroomWorkServices]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[usp_GetClassroomWorkServices]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_WeeklyAttendance]'
GO
IF OBJECT_ID(N'[dbo].[USP_WeeklyAttendance]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_WeeklyAttendance]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[UDF_GetEnrollmentStatusAtGivenDate]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UDF_GetEnrollmentStatusAtGivenDate]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[UDF_GetEnrollmentStatusAtGivenDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_TR_Sub07_TotalCourses]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub07_TotalCourses]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_TR_Sub07_TotalCourses]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[CalculateStudentAverage]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateStudentAverage]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[CalculateStudentAverage]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CalculateStudentAverage]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateStudentAverage]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'-- =============================================
-- Author:		FAME Inc.
-- Create date: 6/11/2019
-- Description:	Calculated Student GPA Based on a set of parameters - Numeric ( Weighted & Unweighted currently implemented)
--Referenced in :
--[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents] -> via db function CalculateStudentAverage
--[Usp_PR_Sub3_Terms_forAllEnrolmnents] -> via db function CalculateStudentAverage
--ANY CHANGES TO THIS FILE MUST BE REFLECTED IN SP GPA_Calculator
-- =============================================
CREATE FUNCTION [dbo].[CalculateStudentAverage]
    (
        @EnrollmentId VARCHAR(50)
       ,@BeginDate DATETIME = NULL
       ,@EndDate DATETIME = NULL
       ,@ClassId UNIQUEIDENTIFIER = NULL
       ,@TermId UNIQUEIDENTIFIER = NULL
    )
RETURNS DECIMAL(16, 2)
AS
    BEGIN
        DECLARE @GPAResult AS DECIMAL(16, 2);
        DECLARE @UseWeightForGPA BIT = 1;

        IF ( @EnrollmentId IS NULL )
            RETURN @GPAResult;

        SET @UseWeightForGPA = (
                               SELECT TOP 1 PV.DoCourseWeightOverallGPA
                               FROM   dbo.arStuEnrollments E
                               JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                               WHERE  E.StuEnrollId = @EnrollmentId
                               );

        -----------------Numeric GPA Grades Format------------------------

        SET @GPAResult = ISNULL(
                         (
                         SELECT ( CASE WHEN @UseWeightForGPA = 1 THEN
                                           ROUND(( SUM(b.CourseWeight * b.WeightedCourseAverage / 100) / SUM(b.CourseWeight)) * 100, 2)
                                       ELSE AVG(b.UnweightedCourseAverage)
                                  END
                                ) AS WeightedGPA
                         FROM   (
                                SELECT   SUM(OurSingleClassGradeFactor) AS CourseFactor
                                        ,SUM(a.GradeWeight) AS GradeWeight
                                        ,SUM(a.Score) AS SumOfScores
                                        ,( SUM(OurSingleClassGradeFactor) / SUM(a.GradeWeight)) * 100 AS WeightedCourseAverage
                                        ,( AVG(a.Score)) AS UnweightedCourseAverage
                                        ,ClsSectionId
                                        ,a.CourseWeight
                                FROM     (
                                         SELECT ( c.Weight * a.Score / 100 ) AS OurSingleClassGradeFactor
                                               ,c.Weight AS GradeWeight
                                               ,a.Score
                                               ,a.ClsSectionId
                                               ,CASE WHEN d.CourseWeight = 0
                                                          AND NOT EXISTS (
                                                                         SELECT 1
                                                                         FROM   dbo.arProgVerDef CPVC
                                                                         WHERE  CPVC.CourseWeight > 0
                                                                                AND CPVC.PrgVerId = d.PrgVerId
                                                                         )
                                                          AND PV.DoCourseWeightOverallGPA = 0 THEN 100 / (
                                                                                                         SELECT COUNT(*)
                                                                                                         FROM   dbo.arGrdBkResults Cn
                                                                                                         WHERE  Cn.StuEnrollId = a.StuEnrollId
                                                                                                                AND Cn.ClsSectionId = a.ClsSectionId
                                                                                                         )
                                                     ELSE d.CourseWeight
                                                END AS courseweight
                                               ,c.Descrip AS ClassDescrip
                                         FROM   (
                                                SELECT   a.StuEnrollId
                                                        ,a.ClsSectionId
                                                        ,a.InstrGrdBkWgtDetailId
                                                        ,AVG(Score) AS Score
                                                FROM     dbo.arGrdBkResults a
                                                JOIN     dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                                JOIN     dbo.arGrdComponentTypes f ON f.GrdComponentTypeId = c.GrdComponentTypeId
                                                JOIN     dbo.arClassSections ON arClassSections.ClsSectionId = a.ClsSectionId
                                                WHERE    a.StuEnrollId = @EnrollmentId
                                                         --AND a.IsCompGraded = 1
                                                         AND a.Score >= 0
                                                         AND a.Score IS NOT NULL
                                                         AND a.PostDate IS NOT NULL
                                                         AND (
                                                             @EndDate IS NULL
                                                             OR ( a.PostDate <= @EndDate )
                                                             )
                                                         AND (
                                                             @ClassId IS NULL
                                                             OR ( @ClassId = a.ClsSectionId )
                                                             )
                                                         AND (
                                                             @TermId IS NULL
                                                             OR ( @TermId = TermId )
                                                             )
                                                GROUP BY StuEnrollId
                                                        ,a.ClsSectionId
                                                        ,a.InstrGrdBkWgtDetailId
                                                ) a -- students grades
                                         JOIN   dbo.arClassSections b ON b.ClsSectionId = a.ClsSectionId
                                         JOIN   dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                         JOIN   dbo.arProgVerDef d ON d.ReqId = b.ReqId
                                         JOIN   dbo.arStuEnrollments E ON E.StuEnrollId = a.StuEnrollId
                                         JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId                                       
                                         ) a
                                WHERE    a.CourseWeight > 0
                                GROUP BY ClsSectionId
                                        ,a.CourseWeight


                                --ORDER BY CourseGPA DESC
                                ) b
                         )
                        ,0.00
                               );
        --END;
        RETURN @GPAResult;
    END;








'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[Trigger_UpdateProgressOfCourses] on [dbo].[arGrdBkResults]'
GO
IF OBJECT_ID(N'[dbo].[Trigger_UpdateProgressOfCourses]', 'TR') IS NULL
EXEC sp_executesql N'
CREATE   TRIGGER [dbo].[Trigger_UpdateProgressOfCourses]
ON [dbo].[arGrdBkResults]
FOR INSERT, UPDATE
AS
SET NOCOUNT ON;

    BEGIN
        DECLARE @Error AS INTEGER;

        SET @Error = 0;

        BEGIN TRANSACTION FixCoursesScore;
        BEGIN TRY

            DECLARE @CourseResultsToUpdate AS TABLE
                (
                    FinalScore DECIMAL(18, 2) NULL
                   ,NumberOfComponents INT NULL
                   ,NumberOfComponentsScored INT NULL
                   ,ClsSectionId UNIQUEIDENTIFIER NULL
                   ,StuEnrollId UNIQUEIDENTIFIER NULL
                );

            DECLARE @GradeRoundingTableByEnrollment AS TABLE
                (
                    StuEnrollId UNIQUEIDENTIFIER
                   ,GradeRounding VARCHAR(10)
                );

            INSERT INTO @GradeRoundingTableByEnrollment (
                                                        StuEnrollId
                                                       ,GradeRounding
                                                        )
                        SELECT StuEnrollId
                              ,LOWER(ISNULL(dbo.GetAppSettingValueByKeyName(''GradeRounding'', CampusId), ''''))
                        FROM   dbo.arStuEnrollments E
                        WHERE  E.StuEnrollId IN (
                                                SELECT Inserted.StuEnrollId
                                                FROM   Inserted
                                                );


            INSERT INTO @CourseResultsToUpdate (
                                               FinalScore
                                              ,NumberOfComponents
                                              ,NumberOfComponentsScored
                                              ,ClsSectionId
                                              ,StuEnrollId
                                               )
                        SELECT     CASE WHEN grdRounding.GradeRounding = ''yes'' THEN
                                            ROUND(dbo.CalculateStudentAverage(enrollments.StuEnrollId, NULL, NULL, classSections.ClsSectionId, NULL), 2)
                                        ELSE dbo.CalculateStudentAverage(enrollments.StuEnrollId, NULL, NULL, classSections.ClsSectionId, NULL)
                                   END AS FinalScore
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   (
                                          SELECT   StuEnrollId
                                          FROM     dbo.arGrdBkResults
                                          WHERE    StuEnrollId = enrollments.StuEnrollId
                                                   AND ClsSectionId = classSections.ClsSectionId
                                          GROUP BY StuEnrollId
                                                  ,ClsSectionId
                                                  ,InstrGrdBkWgtDetailId
                                          ) components
                                   ) AS NumberOfComponents
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   (
                                          SELECT   StuEnrollId
                                          FROM     dbo.arGrdBkResults
                                          WHERE    StuEnrollId = enrollments.StuEnrollId
                                                   AND ClsSectionId = classSections.ClsSectionId
                                                   AND Score IS NOT NULL
                                                   AND PostDate IS NOT NULL
                                          GROUP BY StuEnrollId
                                                  ,ClsSectionId
                                                  ,InstrGrdBkWgtDetailId
                                          ) components
                                   ) AS NumberOfComponentsScored
                                  ,classSections.ClsSectionId
                                  ,courseResults.StuEnrollId
                        FROM       dbo.arResults courseResults
                        INNER JOIN dbo.arStuEnrollments enrollments ON enrollments.StuEnrollId = courseResults.StuEnrollId
                        INNER JOIN dbo.syStatusCodes statusCodes ON statusCodes.StatusCodeId = enrollments.StatusCodeId
                        INNER JOIN dbo.arClassSections classSections ON classSections.ClsSectionId = courseResults.TestId
                        INNER JOIN dbo.arGrdBkResults gradebookResults ON gradebookResults.StuEnrollId = courseResults.StuEnrollId
                                                                          AND gradebookResults.ClsSectionId = classSections.ClsSectionId
                        INNER JOIN dbo.arGrdBkWgtDetails gradebookWeight ON gradebookWeight.InstrGrdBkWgtDetailId = gradebookResults.InstrGrdBkWgtDetailId
                        INNER JOIN dbo.arReqs courses ON courses.ReqId = classSections.ReqId
                        INNER JOIN @GradeRoundingTableByEnrollment grdRounding ON grdRounding.StuEnrollId = courseResults.StuEnrollId
                        INNER JOIN Inserted I ON (
                                                 I.StuEnrollId = courseResults.StuEnrollId
                                                 AND I.ClsSectionId = courseResults.TestId
                                                 )
                        WHERE      statusCodes.SysStatusId NOT IN ( 8, 12, 14, 19 )
                        ORDER BY   courseResults.Score;

            UPDATE     courses
            SET        courses.Score = coursesToUpdate.FinalScore
                      ,courses.IsInComplete = CASE WHEN coursesToUpdate.NumberOfComponents = coursesToUpdate.NumberOfComponentsScored THEN 0
                                                   ELSE 1
                                              END
                      ,courses.DateCompleted = CASE WHEN coursesToUpdate.NumberOfComponents = coursesToUpdate.NumberOfComponentsScored THEN GETDATE()
                                                    ELSE courses.DateCompleted
                                               END
                      ,courses.ModDate = GETDATE()
                      ,courses.IsCourseCompleted = CASE WHEN coursesToUpdate.NumberOfComponents = coursesToUpdate.NumberOfComponentsScored THEN 1
                                                        ELSE 0
                                                   END
                      ,courses.GrdSysDetailId = (
                                                SELECT     TOP ( 1 ) gradeDetails.GrdSysDetailId
                                                FROM       dbo.arStuEnrollments enrollments
                                                INNER JOIN dbo.arPrgVersions programVersion ON programVersion.PrgVerId = enrollments.PrgVerId
                                                INNER JOIN dbo.arGradeSystemDetails gradeDetails ON gradeDetails.GrdSystemId = programVersion.GrdSystemId
                                                INNER JOIN dbo.arGradeScaleDetails gradeScale ON gradeScale.GrdSysDetailId = gradeDetails.GrdSysDetailId
                                                WHERE      enrollments.StuEnrollId = courses.StuEnrollId
                                                           AND gradeScale.MaxVal >= coursesToUpdate.FinalScore
                                                           AND gradeScale.MinVal <= coursesToUpdate.FinalScore
                                                ORDER BY   gradeDetails.Grade
                                                )
            FROM       dbo.arResults courses
            INNER JOIN @CourseResultsToUpdate coursesToUpdate ON coursesToUpdate.ClsSectionId = courses.TestId
                                                                 AND coursesToUpdate.StuEnrollId = courses.StuEnrollId
            INNER JOIN dbo.arStuEnrollments enrollments ON enrollments.StuEnrollId = courses.StuEnrollId
            INNER JOIN dbo.arPrgVersions programVersion ON programVersion.PrgVerId = enrollments.PrgVerId
            WHERE      programVersion.ProgramRegistrationType = 1
                       AND courses.IsGradeOverridden = 0;

            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;

        END TRY
        BEGIN CATCH
            DECLARE @msg NVARCHAR(MAX);
            DECLARE @severity INT;
            DECLARE @state INT;
            SELECT @msg = ERROR_MESSAGE()
                  ,@severity = ERROR_SEVERITY()
                  ,@state = ERROR_STATE();
            RAISERROR(@msg, @severity, @state);
            SET @Error = 1;
        END CATCH;

        IF ( @Error = 0 )
            BEGIN
                COMMIT TRANSACTION FixCoursesScore;

            END;
        ELSE
            BEGIN
                ROLLBACK TRANSACTION FixCoursesScore;
            END;

    END;


SET NOCOUNT OFF;

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[Trigger_arResults_IsClinicsSatisified] on [dbo].[arResults]'
GO
IF OBJECT_ID(N'[dbo].[Trigger_arResults_IsClinicsSatisified]', 'TR') IS NULL
EXEC sp_executesql N'
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--17381: Modified the Trigger Trigger_arResults_IsClinicsSatisified 17366: Review - Ross - Progress Report Information 
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE TRIGGER [dbo].[Trigger_arResults_IsClinicsSatisified]
ON [dbo].[arResults]
FOR UPDATE
AS
SET NOCOUNT ON;

DECLARE @ClsSectionId UNIQUEIDENTIFIER;
DECLARE @ResultId UNIQUEIDENTIFIER
       ,@GrdSystemDetailId UNIQUEIDENTIFIER;
DECLARE @courseisclinic INT
       ,@reqid UNIQUEIDENTIFIER;
DECLARE @MinVal DECIMAL(18, 2);
DECLARE @MaxVal DECIMAL(18, 2);
DECLARE @score DECIMAL(18, 2);
DECLARE @graderounding VARCHAR(50);

    BEGIN
        UPDATE arResults
        SET    isClinicsSatisfied = 1
        FROM   dbo.arResults
        JOIN   Inserted ON Inserted.ResultId = arResults.ResultId
        WHERE  arResults.Score IS NOT NULL
               AND arResults.GrdSysDetailId IS NOT NULL;
    END;
DECLARE UpdateExistingScore CURSOR FOR
    SELECT     DISTINCT t1.ResultId
                       ,t2.ClsSectionId
                       ,t4.MinVal
                       ,t4.MaxVal
                       ,t1.Score
    FROM       arResults t1
    INNER JOIN inserted I ON (
                             t1.ResultId = I.ResultId
                             AND t1.TestId = I.TestId
                             )
    INNER JOIN arClassSections t2 ON t2.ClsSectionId = t1.TestId
    INNER JOIN arGradeScales t3 ON t3.GrdScaleId = t2.GrdScaleId
    INNER JOIN arGradeScaleDetails t4 ON t4.GrdScaleId = t3.GrdScaleId
    WHERE      t1.Score IS NOT NULL; --and t1.GrdSysDetailId is null
OPEN UpdateExistingScore;
FETCH NEXT FROM UpdateExistingScore
INTO @ResultId
    ,@ClsSectionId
    ,@MinVal
    ,@MaxVal
    ,@score;
WHILE @@FETCH_STATUS = 0
    BEGIN
        -- Example:Score = 78.5 and MinValue=0 and MxValue = 79
        IF (
           @score >= @MinVal
           AND @score <= @MaxVal
           )
            BEGIN
                SET @GrdSystemDetailId = (
                                         SELECT DISTINCT t4.GrdSysDetailId
                                         FROM   arResults t1
                                               ,arClassSections t2
                                               ,arGradeScales t3
                                               ,arGradeScaleDetails t4
                                         WHERE  t1.TestId = t2.ClsSectionId
                                                AND t2.GrdScaleId = t3.GrdScaleId
                                                AND t3.GrdScaleId = t4.GrdScaleId
                                                AND t1.ResultId = @ResultId
                                                AND t2.ClsSectionId = @ClsSectionId
                                                AND (
                                                    t4.MinVal >= @MinVal
                                                    AND t4.MaxVal <= @MaxVal
                                                    )
                                         );
                UPDATE arResults
                SET    GrdSysDetailId = @GrdSystemDetailId
                WHERE  ResultId = @ResultId;
            END;
        ELSE
            -- If score is 79.5, it doesn''t fall in range 0-79 and 80-89,so
            -- based on the value from GradesFormat entry in web.config
            -- either round to 80 or floor it to 79.
            BEGIN
                SET @graderounding = (
                                     SELECT DISTINCT graderoundingvalue
                                     FROM   syGradeRounding
                                     );
                IF @graderounding = ''yes''
                    BEGIN
                        SET @score = ROUND(@score, 0);
                        IF (
                           @score >= @MinVal
                           AND @score <= @MaxVal
                           )
                            BEGIN
                                SET @GrdSystemDetailId = (
                                                         SELECT DISTINCT t4.GrdSysDetailId
                                                         FROM   arResults t1
                                                               ,arClassSections t2
                                                               ,arGradeScales t3
                                                               ,arGradeScaleDetails t4
                                                         WHERE  t1.TestId = t2.ClsSectionId
                                                                AND t2.GrdScaleId = t3.GrdScaleId
                                                                AND t3.GrdScaleId = t4.GrdScaleId
                                                                AND t1.ResultId = @ResultId
                                                                AND t2.ClsSectionId = @ClsSectionId
                                                                AND (
                                                                    t4.MinVal >= @MinVal
                                                                    AND t4.MaxVal <= @MaxVal
                                                                    )
                                                         );
                                UPDATE arResults
                                SET    GrdSysDetailId = @GrdSystemDetailId
                                WHERE  ResultId = @ResultId;
                            END;
                    END;
                ELSE
                    BEGIN
                        SET @score = FLOOR(@score);
                        IF (
                           @score >= @MinVal
                           AND @score <= @MaxVal
                           )
                            BEGIN
                                SET @GrdSystemDetailId = (
                                                         SELECT DISTINCT t4.GrdSysDetailId
                                                         FROM   arResults t1
                                                               ,arClassSections t2
                                                               ,arGradeScales t3
                                                               ,arGradeScaleDetails t4
                                                         WHERE  t1.TestId = t2.ClsSectionId
                                                                AND t2.GrdScaleId = t3.GrdScaleId
                                                                AND t3.GrdScaleId = t4.GrdScaleId
                                                                AND t1.ResultId = @ResultId
                                                                AND t2.ClsSectionId = @ClsSectionId
                                                                AND (
                                                                    t4.MinVal >= @MinVal
                                                                    AND t4.MaxVal <= @MaxVal
                                                                    )
                                                         );
                                UPDATE arResults
                                SET    GrdSysDetailId = @GrdSystemDetailId
                                WHERE  ResultId = @ResultId;
                            END;
                    END;
            END;
        FETCH NEXT FROM UpdateExistingScore
        INTO @ResultId
            ,@ClsSectionId
            ,@MinVal
            ,@MaxVal
            ,@score;
    END;
CLOSE UpdateExistingScore;
DEALLOCATE UpdateExistingScore;




SET NOCOUNT OFF;

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[UDF_GetEnrollmentStatusAtGivenDate]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UDF_GetEnrollmentStatusAtGivenDate]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'--------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Function to return the status of an enrollment at a specified date
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[UDF_GetEnrollmentStatusAtGivenDate]
(
    @StuEnrollId UNIQUEIDENTIFIER,
    @ReportDate DATETIME
)
RETURNS VARCHAR(50)
AS
BEGIN
    DECLARE @ReturnValue VARCHAR(50);
    DECLARE @CurrentStatus VARCHAR(50);
    DECLARE @StartDate DATETIME;
    DECLARE @LastDateOfChange DATETIME;
    DECLARE @LastNewStatus VARCHAR(50);

    SET @ReturnValue = '''';

    SELECT @CurrentStatus = sc.StatusCodeDescrip,
           @StartDate = se.StartDate
    FROM dbo.arStuEnrollments se
        INNER JOIN dbo.syStatusCodes sc
            ON sc.StatusCodeId = se.StatusCodeId
    WHERE se.StuEnrollId = @StuEnrollId;

    SET @LastDateOfChange =
    (
        SELECT MAX(DateOfChange)
        FROM dbo.syStudentStatusChanges
        WHERE StuEnrollId = @StuEnrollId
    );

    SET @LastNewStatus =
    (
        SELECT TOP 1
               sc.StatusCodeDescrip
        FROM dbo.syStudentStatusChanges ssc
            INNER JOIN dbo.syStatusCodes sc
                ON sc.StatusCodeId = ssc.NewStatusId
        WHERE ssc.StuEnrollId = @StuEnrollId
        ORDER BY ssc.DateOfChange DESC,
                 ssc.ModDate DESC
    );


    --If there are no status changes for the enrollment and the given date is >= enrollment start date we can simply return the current status. 
    IF NOT EXISTS
    (
        SELECT 1
        FROM dbo.syStudentStatusChanges
        WHERE StuEnrollId = @StuEnrollId
    )
    BEGIN
        IF @ReportDate >= @StartDate
        BEGIN
            SET @ReturnValue = @CurrentStatus;
        END;
    END;
    ELSE
    BEGIN
        --If the report date > last date of change for the enrollment then we can simply return the last new status (should be same as current status)
        IF @ReportDate > @LastDateOfChange
           AND @ReportDate >= @StartDate
        BEGIN
            SET @ReturnValue = @LastNewStatus;
        END;
        ELSE
        BEGIN
            --If the report date is the same as a date of change then we can simply return the new status for that date of change.
            --Use the FORMAT function just in case the DateOfChange field in the database has a time component on it such as 2018-10-19 05:00:00.000
            IF EXISTS
            (
                SELECT 1
                FROM dbo.syStudentStatusChanges
                WHERE StuEnrollId = @StuEnrollId
                      AND CONVERT(VARCHAR, DateOfChange, 101) = CONVERT(VARCHAR, @ReportDate, 101)
            )
            BEGIN
                SET @ReturnValue =
                (
                    SELECT TOP 1
                           sc.StatusCodeDescrip
                    FROM dbo.syStudentStatusChanges ssc
                        INNER JOIN dbo.syStatusCodes sc
                            ON sc.StatusCodeId = ssc.NewStatusId
                    WHERE ssc.StuEnrollId = @StuEnrollId
                          AND CONVERT(VARCHAR, DateOfChange, 101) = CONVERT(VARCHAR, @ReportDate, 101)
                    ORDER BY ssc.DateOfChange DESC,
                             ssc.ModDate DESC
                );

            END;
            ELSE
            BEGIN
                --At this point we can get the last change record with DateOfChange that is less than the report date
                SET @ReturnValue =
                (
                    SELECT TOP 1
                           sc.StatusCodeDescrip
                    FROM dbo.syStudentStatusChanges ssc
                        INNER JOIN dbo.syStatusCodes sc
                            ON sc.StatusCodeId = ssc.NewStatusId
                    WHERE ssc.StuEnrollId = @StuEnrollId
                          AND ssc.DateOfChange < @ReportDate
                    ORDER BY ssc.DateOfChange DESC,
                             ssc.ModDate DESC
                );
            END;

        END;

    END;

    RETURN ISNULL(@ReturnValue, '''');
END;



'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[usp_GetClassroomWorkServices]'
GO
IF OBJECT_ID(N'[dbo].[usp_GetClassroomWorkServices]', 'P') IS NULL
EXEC sp_executesql N'
/* 
PURPOSE: 
Get component summaries for a selected student enrollment, class section and component type

CREATED: 


MODIFIED:
10/25/2013 WP	US4547 Refactor Post Services by Student process

ResourceId
Homework = 499
LabWork = 500
Exam = 501
Final = 502
LabHours = 503
Externship = 544

*/

CREATE PROCEDURE [dbo].[usp_GetClassroomWorkServices]
    (
        @StuEnrollId UNIQUEIDENTIFIER
       ,@ClsSectionId UNIQUEIDENTIFIER
       ,@GradeBookComponentType INT
    )
AS
    SET NOCOUNT ON;


    DECLARE @ReqId AS UNIQUEIDENTIFIER;
    SELECT @ReqId = ReqId
    FROM   arClassSections
    WHERE  ClsSectionId = @ClsSectionId;

    SELECT *
    INTO   #PostedServices
    FROM   arGrdBkResults
    WHERE  StuEnrollId = @StuEnrollId
           AND ClsSectionId = @ClsSectionId
           AND InstrGrdBkWgtDetailId IN (
                                        SELECT InstrGrdBkWgtDetailId
                                        FROM   arGrdBkWgtDetails posted_gbwd
                                        JOIN   arGrdComponentTypes posted_gct ON posted_gct.GrdComponentTypeId = posted_gbwd.GrdComponentTypeId
                                        WHERE  posted_gct.SysComponentTypeId = @GradeBookComponentType
                                        );

    --SELECT * FROM #PostedServices

    DECLARE @ClinicScoresEnabled BIT = (
                                       SELECT CASE WHEN ISNULL(
                                                                  dbo.GetAppSettingValueByKeyName(
                                                                                                     ''ClinicServicesScoresEnabled''
                                                                                                    ,(
                                                                                                    SELECT CampusId
                                                                                                    FROM   dbo.arStuEnrollments
                                                                                                    WHERE  StuEnrollId = @StuEnrollId
                                                                                                    )
                                                                                                 )
                                                                 ,''''
                                                              ) = ''No'' THEN 0
                                                   ELSE 1
                                              END
                                       );

    SELECT    gct.Descrip
             ,ISNULL(gbwd.Number, 0) AS Required
             ,CASE WHEN @GradeBookComponentType = 503 THEN ISNULL((
                                                                  SELECT SUM(Score)
                                                                  FROM   #PostedServices
                                                                  WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                                                  )
                                                                 ,0
                                                                 )
                   ELSE ISNULL((
                               SELECT COUNT(*)
                               FROM   #PostedServices
                               WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                               )
                              ,0
                              )
              END AS Completed
             ,CASE WHEN @GradeBookComponentType = 503 THEN CASE WHEN ( ISNULL(gbwd.Number, 0) - ISNULL(( SUM(ps.Score)), 0)) <= 0 THEN 0
                                                                ELSE ( ISNULL(gbwd.Number, 0) - ISNULL(( SUM(ps.Score)), 0))
                                                           END
                   ELSE CASE WHEN ( ISNULL(gbwd.Number, 0) - ISNULL((
                                                                    SELECT COUNT(*)
                                                                    FROM   #PostedServices
                                                                    WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                                                    )
                                                                   ,0
                                                                   )
                                  ) <= 0 THEN 0
                             ELSE ( ISNULL(gbwd.Number, 0) - ISNULL((
                                                                    SELECT COUNT(*)
                                                                    FROM   #PostedServices
                                                                    WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                                                    )
                                                                   ,0
                                                                   )
                                  )
                        END
              END AS Remaining
             ,CASE WHEN @GradeBookComponentType = 503 THEN ISNULL((
                                                                  SELECT SUM(Score)
                                                                  FROM   #PostedServices
                                                                  WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                                                         AND PostDate IS NOT NULL
                                                                         AND Score IS NOT NULL
                                                                  )
                                                                 ,0
                                                                 )
                   WHEN @ClinicScoresEnabled = 1 THEN AVG(ps.Score)
                   ELSE ( ISNULL((
                                 SELECT COUNT(*)
                                 FROM   #PostedServices
                                 WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                        AND PostDate IS NOT NULL
                                        AND Score IS NOT NULL
                                 )
                                ,0
                                )
                        )
              END AS Average
             ,se.EnrollDate
             ,gbwd.InstrGrdBkWgtDetailId
             ,@ClsSectionId AS ClsSectionId
             ,gbwd.Seq
    FROM      arGrdBkWeights gbw
    LEFT JOIN arGrdBkWgtDetails gbwd ON gbwd.InstrGrdBkWgtId = gbw.InstrGrdBkWgtId
    LEFT JOIN arGrdComponentTypes gct ON gct.GrdComponentTypeId = gbwd.GrdComponentTypeId
    LEFT JOIN arClassSections cs ON cs.ReqId = gbw.ReqId
    LEFT JOIN arResults ar ON ar.TestId = cs.ClsSectionId
    LEFT JOIN arStuEnrollments se ON ar.StuEnrollId = se.StuEnrollId
    LEFT JOIN #PostedServices ps ON ps.InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
    WHERE     gct.SysComponentTypeId = @GradeBookComponentType
              AND gbw.ReqId = @ReqId
              AND ar.TestId = @ClsSectionId
              AND ar.StuEnrollId = @StuEnrollId
    GROUP BY  gct.Descrip
             ,gbwd.Number
             ,se.EnrollDate
             ,gbwd.InstrGrdBkWgtDetailId
             ,gbwd.Seq
    ORDER BY  gbwd.Seq
             ,gct.Descrip;



    DROP TABLE #PostedServices;








'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]', 'P') IS NULL
EXEC sp_executesql N'-- =========================================================================================================
-- Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId
-- =========================================================================================================
CREATE PROCEDURE [dbo].[Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId]
    @StuEnrollIdList VARCHAR(MAX),
    @TermId VARCHAR(50) = NULL,
    @SysComponentTypeIdList VARCHAR(50) = NULL,
    @ReqId VARCHAR(50),
    @SysComponentTypeId VARCHAR(10) = NULL,
    @ShowIncomplete BIT = 1
AS
BEGIN
    DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
    DECLARE @SetGradeBookAt VARCHAR(50);
    DECLARE @CharInt INT;
    DECLARE @Counter AS INT;
    DECLARE @times AS INT;
    DECLARE @TermStartDate1 AS DATETIME;
    DECLARE @Score AS DECIMAL(18, 2);
    DECLARE @GrdCompDescrip AS VARCHAR(50);


    DECLARE @curId AS UNIQUEIDENTIFIER;
    DECLARE @curReqId AS UNIQUEIDENTIFIER;
    DECLARE @curStuEnrollId AS UNIQUEIDENTIFIER;
    DECLARE @curDescrip AS VARCHAR(50);
    DECLARE @curNumber AS INT;
    DECLARE @curGrdComponentTypeId AS INT;
    DECLARE @curMinResult AS DECIMAL(18, 2);
    DECLARE @curGrdComponentDescription AS VARCHAR(50);
    DECLARE @curClsSectionId AS UNIQUEIDENTIFIER;
    DECLARE @curRownumber AS INT;
		 DECLARE @curRowSeq AS INT;
    DECLARE @ExecutedSproc BIT = 0;

    CREATE TABLE #Temp21
    (
        Id UNIQUEIDENTIFIER,
        TermId UNIQUEIDENTIFIER,
        ReqId UNIQUEIDENTIFIER,
        GradeBookDescription VARCHAR(50),
        Number INT,
        GradeBookSysComponentTypeId INT,
        GradeBookScore DECIMAL(18, 2),
        MinResult DECIMAL(18, 2),
        GradeComponentDescription VARCHAR(50),
        ClsSectionId UNIQUEIDENTIFIER,
        StuEnrollId UNIQUEIDENTIFIER,
        RowNumber INT
			,Seq int
    );
    CREATE TABLE #Temp22
    (
        ReqId UNIQUEIDENTIFIER,
        EffectiveDate DATETIME
    );

    IF @SysComponentTypeIdList IS NULL
    BEGIN
        SET @SysComponentTypeIdList = ''501,544,502,499,503,500,533'';
    END;

    SET @StuEnrollCampusId = COALESCE(
                             (
                                 SELECT TOP 1
                                        ASE.CampusId
                                 FROM dbo.arStuEnrollments AS ASE
                                 WHERE EXISTS
                                 (
                                     SELECT Val
                                     FROM dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                     WHERE Val = ASE.StuEnrollId
                                 )
                             ),
                             NULL
                                     );
    SET @SetGradeBookAt = (dbo.GetAppSettingValueByKeyName(''GradeBookWeightingLevel'', @StuEnrollCampusId));
    IF (@SetGradeBookAt IS NOT NULL)
    BEGIN
        SET @SetGradeBookAt = LOWER(LTRIM(RTRIM(@SetGradeBookAt)));
    END;


    SET @CharInt =
    (
        SELECT CHARINDEX(@SysComponentTypeId, @SysComponentTypeIdList)
    );

    IF @SetGradeBookAt = ''instructorlevel''
    BEGIN
        SELECT CASE
                   WHEN GradeBookDescription IS NULL THEN
                       GradeComponentDescription
                   ELSE
                       GradeBookDescription
               END AS GradeBookDescription,
               MinResult,
               GradeBookScore,
               GradeComponentDescription,
               GradeBookSysComponentTypeId,
               StuEnrollId,
               rownumber
        FROM
        (
            SELECT AR2.ReqId,
                   AT.TermId,
                   CASE
                       WHEN AGBWD.Descrip IS NULL THEN
                           AGCT.Descrip
                       ELSE
                           AGBWD.Descrip
                   END AS GradeBookDescription,
                   (CASE
                        WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN
                            AGBWD.Number
                        ELSE
                    (
                        SELECT MIN(MinVal)
                        FROM arGradeScaleDetails GSD,
                             arGradeSystemDetails GSS
                        WHERE GSD.GrdSysDetailId = GSS.GrdSysDetailId
                              AND GSS.IsPass = 1
                              AND GSD.GrdScaleId = ACS.GrdScaleId
                    )
                    END
                   ) AS MinResult,
                   AGBWD.Required,
                   AGBWD.MustPass,
                   ISNULL(AGCT.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId,
                   AGBWD.Number,
                   SR.Resource AS GradeComponentDescription,
                   AGBWD.InstrGrdBkWgtDetailId,
                   ASE.StuEnrollId,
                   0 AS IsExternShip,
                   (CASE AGCT.SysComponentTypeId
                        WHEN 544 THEN
                        (
                            SELECT SUM(HoursAttended)
                            FROM arExternshipAttendance
                            WHERE StuEnrollId = ASE.StuEnrollId
                        )
                        ELSE
                    (
                        SELECT SUM(ISNULL(Score, 0))
                        FROM arGrdBkResults
                        WHERE StuEnrollId = ASE.StuEnrollId
                              AND InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                              AND ClsSectionId = ACS.ClsSectionId
                    )
                    END
                   ) AS GradeBookScore,
				   AGBWD.Seq,
                   ROW_NUMBER() OVER (PARTITION BY ASE.StuEnrollId,
                                                   AT.TermId,
                                                   AR2.ReqId
                                      ORDER BY AGCT.SysComponentTypeId,
                                               AGBWD.Descrip
                                     ) AS rownumber
            FROM arGrdBkWgtDetails AS AGBWD
                INNER JOIN arGrdBkWeights AS AGBW
                    ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                INNER JOIN arClassSections AS ACS
                    ON ACS.InstrGrdBkWgtId = AGBW.InstrGrdBkWgtId
                INNER JOIN arResults AS AR
                    ON AR.TestId = ACS.ClsSectionId
                INNER JOIN arGrdComponentTypes AS AGCT
                    ON AGCT.GrdComponentTypeId = AGBWD.GrdComponentTypeId
                INNER JOIN arStuEnrollments AS ASE
                    ON ASE.StuEnrollId = AR.StuEnrollId
                INNER JOIN arTerm AS AT
                    ON AT.TermId = ACS.TermId
                INNER JOIN arReqs AS AR2
                    ON AR2.ReqId = ACS.ReqId
                INNER JOIN syResources AS SR
                    ON SR.ResourceID = AGCT.SysComponentTypeId
                LEFT JOIN arGrdBkResults AS AGBR
                    ON AGBR.StuEnrollId = ASE.StuEnrollId
                       AND AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                       AND AGBR.ClsSectionId = ACS.ClsSectionId
            WHERE AR.StuEnrollId IN
                  (
                      SELECT Val
                      FROM MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                  )
                  AND AT.TermId = @TermId
                  AND AR2.ReqId = @ReqId
                  AND
                  (
                      @SysComponentTypeIdList IS NULL
                      OR AGCT.SysComponentTypeId IN
                         (
                             SELECT Val
                             FROM MultipleValuesForReportParameters(@SysComponentTypeIdList, '','', 1)
                         )
                  )
            UNION
            SELECT AR2.ReqId,
                   AT.TermId,
                   CASE
                       WHEN AGBW.Descrip IS NULL THEN
                       (
                           SELECT Resource
                           FROM syResources
                           WHERE ResourceID = AGCT.SysComponentTypeId
                       )
                       ELSE
                           AGBW.Descrip
                   END AS GradeBookDescription,
                   (CASE
                        WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN
                            AGBWD.Number
                        ELSE
                    (
                        SELECT MIN(MinVal)
                        FROM arGradeScaleDetails GSD,
                             arGradeSystemDetails GSS
                        WHERE GSD.GrdSysDetailId = GSS.GrdSysDetailId
                              AND GSS.IsPass = 1
                              AND GSD.GrdScaleId = ACS.GrdScaleId
                    )
                    END
                   ) AS MinResult,
                   AGBWD.Required,
                   AGBWD.MustPass,
                   ISNULL(AGCT.SysComponentTypeId, 0) AS GradeBookSysComponentTypeId,
                   AGBWD.Number,
                   (
                       SELECT Resource
                       FROM syResources
                       WHERE ResourceID = AGCT.SysComponentTypeId
                   ) AS GradeComponentDescription,
                   AGBWD.InstrGrdBkWgtDetailId,
                   ASE.StuEnrollId,
                   0 AS IsExternShip,
                   (CASE AGCT.SysComponentTypeId
                        WHEN 544 THEN
                        (
                            SELECT SUM(HoursAttended)
                            FROM arExternshipAttendance
                            WHERE StuEnrollId = ASE.StuEnrollId
                        )
                        ELSE
                    (
                        SELECT SUM(ISNULL(Score, 0))
                        FROM arGrdBkResults
                        WHERE StuEnrollId = ASE.StuEnrollId
                              AND InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                              AND ClsSectionId = ACS.ClsSectionId
                    )
                    END
                   ) AS GradeBookScore,
				   AGBWD.seq,
                   ROW_NUMBER() OVER (PARTITION BY ASE.StuEnrollId,
                                                   AT.TermId,
                                                   AR2.ReqId
                                      ORDER BY AGCT.SysComponentTypeId,
                                               AGCT.Descrip
                                     ) AS rownumber
            FROM arGrdBkConversionResults GBCR
                INNER JOIN arStuEnrollments AS ASE
                    ON ASE.StuEnrollId = GBCR.StuEnrollId
                INNER JOIN adLeads AS AST
                    ON AST.StudentId = ASE.StudentId
                INNER JOIN arPrgVersions AS APV
                    ON APV.PrgVerId = ASE.PrgVerId
                INNER JOIN arTerm AS AT
                    ON AT.TermId = GBCR.TermId
                INNER JOIN arReqs AS AR2
                    ON AR2.ReqId = GBCR.ReqId
                INNER JOIN arGrdComponentTypes AS AGCT
                    ON AGCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWgtDetails AS AGBWD
                    ON AGBWD.GrdComponentTypeId = AGCT.GrdComponentTypeId
                       AND AGBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWeights AS AGBW
                    ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                       AND AGBW.ReqId = GBCR.ReqId
                INNER JOIN
                (
                    SELECT ReqId,
                           MAX(EffectiveDate) AS EffectiveDate
                    FROM arGrdBkWeights AS AGBW
                    GROUP BY ReqId
                ) AS MaxEffectiveDatesByCourse
                    ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                INNER JOIN
                (
                    SELECT Resource,
                           ResourceID
                    FROM syResources
                    WHERE ResourceTypeID = 10
                ) SYRES
                    ON SYRES.ResourceID = AGCT.SysComponentTypeId
                INNER JOIN syCampuses AS SC
                    ON SC.CampusId = ASE.CampusId
                INNER JOIN arClassSections AS ACS
                    ON ACS.TermId = AT.TermId
                       AND ACS.ReqId = AR2.ReqId
                LEFT JOIN arGrdBkResults AS AGBR
                    ON AGBR.StuEnrollId = ASE.StuEnrollId
                       AND AGBR.InstrGrdBkWgtDetailId = AGBWD.InstrGrdBkWgtDetailId
                       AND AGBR.ClsSectionId = ACS.ClsSectionId
            WHERE MaxEffectiveDatesByCourse.EffectiveDate <= AT.StartDate
                  AND ASE.StuEnrollId IN
                      (
                          SELECT Val
                          FROM MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                      )
                  AND AT.TermId = @TermId
                  AND AR2.ReqId = @ReqId
                  AND
                  (
                      @SysComponentTypeIdList IS NULL
                      OR AGCT.SysComponentTypeId IN
                         (
                             SELECT Val
                             FROM MultipleValuesForReportParameters(@SysComponentTypeIdList, '','', 1)
                         )
                  )
        ) dt
        WHERE (
                  @SysComponentTypeId IS NULL
                  OR dt.GradeBookSysComponentTypeId = @SysComponentTypeId
              )
              AND
              (
                  @ShowIncomplete = 1
                  OR
                  (
                      @ShowIncomplete = 0
                      AND dt.GradeBookScore IS NOT NULL
                  )
              )
        ORDER BY seq,GradeComponentDescription,
                 rownumber,
                 GradeBookDescription;
    END;
    ELSE
    BEGIN

        SET @TermStartDate1 =
        (
            SELECT AT.StartDate FROM dbo.arTerm AS AT WHERE AT.TermId = @TermId
        );
        INSERT INTO #Temp22
        SELECT AGBW.ReqId,
               MAX(AGBW.EffectiveDate) AS EffectiveDate
        FROM dbo.arGrdBkWeights AS AGBW
            INNER JOIN dbo.syCreditSummary AS SCS
                ON SCS.ReqId = AGBW.ReqId
        WHERE SCS.TermId = @TermId
              AND SCS.StuEnrollId IN
                  (
                      SELECT Val
                      FROM dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                  )
              AND AGBW.EffectiveDate <= @TermStartDate1
        GROUP BY AGBW.ReqId;


        DECLARE getUsers_Cursor CURSOR FOR
        SELECT dt.ID,
               dt.ReqId,
               dt.Descrip,
               dt.Number,
               dt.SysComponentTypeId,
               dt.MinResult,
               dt.GradeComponentDescription,
               dt.ClsSectionId,
               dt.StuEnrollId,
               ROW_NUMBER() OVER (PARTITION BY dt.StuEnrollId,
                                               @TermId,
                                               dt.SysComponentTypeId
                                  ORDER BY dt.SysComponentTypeId,
                                           dt.Descrip
                                 ) AS rownumber,
								 dt.Seq
        FROM
        (
            SELECT ISNULL(AGBWD.InstrGrdBkWgtDetailId, NEWID()) AS ID,
                   AR.ReqId,
                   AGBWD.Descrip,
                   AGBWD.Number,
                   AGCT.SysComponentTypeId,
                   (CASE
                        WHEN AGCT.SysComponentTypeId IN ( 500, 503, 544 ) THEN
                            AGBWD.Number
                        ELSE
                    (
                        SELECT MIN(AGSD.MinVal) AS MinVal
                        FROM dbo.arGradeScaleDetails AS AGSD
                            INNER JOIN dbo.arGradeSystemDetails AS AGSDetails
                                ON AGSDetails.GrdSysDetailId = AGSD.GrdSysDetailId
                        WHERE AGSDetails.IsPass = 1
                              AND AGSD.GrdScaleId = ACS.GrdScaleId
                    )
                    END
                   ) AS MinResult,
                   SR.Resource AS GradeComponentDescription,
                   ACS.ClsSectionId,
                   AGBR.StuEnrollId,
				   AGBWD.Seq
            FROM dbo.arGrdBkResults AS AGBR
                INNER JOIN dbo.arGrdBkWgtDetails AS AGBWD
                    ON AGBWD.InstrGrdBkWgtDetailId = AGBR.InstrGrdBkWgtDetailId
                INNER JOIN dbo.arGrdBkWeights AS AGBW
                    ON AGBW.InstrGrdBkWgtId = AGBWD.InstrGrdBkWgtId
                INNER JOIN dbo.arGrdComponentTypes AS AGCT
                    ON AGCT.GrdComponentTypeId = AGBWD.GrdComponentTypeId
                INNER JOIN dbo.syResources AS SR
                    ON SR.ResourceID = AGCT.SysComponentTypeId
                --                             INNER JOIN #Temp22 AS T2 ON T2.ReqId = AGBW.ReqId
                INNER JOIN dbo.arReqs AS AR
                    ON AR.ReqId = AGBW.ReqId
                INNER JOIN dbo.arClassSections AS ACS
                    ON ACS.ReqId = AR.ReqId
                       AND ACS.ClsSectionId = AGBR.ClsSectionId
                INNER JOIN dbo.arResults AS AR2
                    ON AR2.TestId = ACS.ClsSectionId
                       AND AR2.StuEnrollId = AGBR.StuEnrollId --AND AR2.TestId = AGBR.ClsSectionId

            WHERE (
                      @StuEnrollIdList IS NULL
                      OR AGBR.StuEnrollId IN
                         (
                             SELECT Val
                             FROM dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                         )
                  )
                  --                                        AND T2.EffectiveDate = AGBW.EffectiveDate
                  AND AGBWD.Number > 0
                  AND AR.ReqId = @ReqId
        ) dt
        ORDER BY SysComponentTypeId,
                 rownumber;

        DECLARE @schoolTypeSetting VARCHAR(50);
        SET @schoolTypeSetting = dbo.GetAppSettingValueByKeyName(''gradesformat'', NULL);


        OPEN getUsers_Cursor;
        FETCH NEXT FROM getUsers_Cursor
        INTO @curId,
             @curReqId,
             @curDescrip,
             @curNumber,
             @curGrdComponentTypeId,
             @curMinResult,
             @curGrdComponentDescription,
             @curClsSectionId,
             @curStuEnrollId,
             @curRownumber,
			 @curRowSeq
        SET @Counter = 0;
        WHILE @@FETCH_STATUS = 0
        BEGIN
            --PRINT @Number; 

            IF @schoolTypeSetting = ''Numeric''
            BEGIN
                SET @times = 1;

                SET @GrdCompDescrip = @curDescrip;
                IF (@curGrdComponentTypeId <> 500 AND @curGrdComponentTypeId <> 503)
                BEGIN
                    SET @Score =
                    (
                        SELECT TOP 1
                               Score
                        FROM arGrdBkResults
                        WHERE StuEnrollId = @curStuEnrollId
                              AND InstrGrdBkWgtDetailId = @curId
                              AND ResNum = @times
                    );
                    IF @Score IS NULL
                    BEGIN
                        SET @Score =
                        (
                            SELECT TOP 1
                                   Score
                            FROM arGrdBkResults
                            WHERE StuEnrollId = @curStuEnrollId
                                  AND InstrGrdBkWgtDetailId = @curId
                                  AND ResNum = (@times - 1)
                        );
                    END;

                    IF @curGrdComponentTypeId = 544
                    BEGIN
                        SET @Score =
                        (
                            SELECT SUM(HoursAttended)
                            FROM arExternshipAttendance
                            WHERE StuEnrollId = @curStuEnrollId
                        );
                    END;
                    INSERT INTO #Temp21
                    VALUES
                    (@curId, @TermId, @curReqId, @GrdCompDescrip, @curNumber, @curGrdComponentTypeId, @Score,
                     @curMinResult, @curGrdComponentDescription, @curClsSectionId, @curStuEnrollId, @curRownumber, @curRowSeq);
                END;
                ELSE
                BEGIN
                    IF (@curGrdComponentTypeId = 500 OR @curGrdComponentTypeId = 503)
                       AND @ExecutedSproc = 0
                    BEGIN


                        DECLARE @Services TABLE
                        (
                            Descrip VARCHAR(50),
                            Required DECIMAL(18, 2) NULL,
                            Completed INT NULL,
                            Remaining DECIMAL(18, 2) NULL,
                            Average DECIMAL(18, 2) NULL,
                            EnrollDate DATETIME NULL,
                            InstrGrdBkWgtDetailId UNIQUEIDENTIFIER,
                            ClsSectionId UNIQUEIDENTIFIER,
							Seq INT NULL
                        );


                        INSERT INTO @Services
                        (
                            Descrip,
                            Required,
                            Completed,
                            Remaining,
                            Average,
                            EnrollDate,
                            InstrGrdBkWgtDetailId,
                            ClsSectionId,
							Seq
                        )
                        EXEC dbo.usp_GetClassroomWorkServices @StuEnrollId = @curStuEnrollId,   -- uniqueidentifier
                                                              @ClsSectionId = @curClsSectionId, -- uniqueidentifier
                                                              @GradeBookComponentType = @curGrdComponentTypeId;    -- int



                        INSERT INTO #Temp21
                        SELECT @curId,
                               @TermId,
                               @curReqId,
                               LTRIM(RTRIM(Descrip)) + '' ('' + CAST(Completed AS VARCHAR(10)) + ''/''
                               + CAST(Required AS VARCHAR(10)) + '')'',
                               @curNumber,
                               @curGrdComponentTypeId,
                               Average,
                               [Required],
                               @curGrdComponentDescription,
                               @curClsSectionId,
                               @curStuEnrollId,
                               @curRownumber,
							   @curRowSeq
                        FROM @Services;


                        SET @ExecutedSproc = 1;
                    END;
                END;



            END;
            ELSE
            BEGIN

                SET @times = 1;
                WHILE @times <= @curNumber
                BEGIN
                    --PRINT @times; 
                    IF @curNumber > 1
                    BEGIN
                        SET @GrdCompDescrip = @curDescrip + CAST(@times AS CHAR);

                        IF @curGrdComponentTypeId = 544
                        BEGIN
                            SET @Score =
                            (
                                SELECT SUM(HoursAttended)
                                FROM arExternshipAttendance
                                WHERE StuEnrollId = @curStuEnrollId
                            );
                        END;
                        ELSE
                        BEGIN
                            SET @Score =
                            (
                                SELECT Score
                                FROM arGrdBkResults
                                WHERE StuEnrollId = @curStuEnrollId
                                      AND InstrGrdBkWgtDetailId = @curId
                                      AND ResNum = @times
                                      AND ClsSectionId = @curClsSectionId
                            );
                        END;


                        SET @curRownumber = @times;
                    END;
                    ELSE
                    BEGIN
                        SET @GrdCompDescrip = @curDescrip;
                        SET @Score =
                        (
                            SELECT TOP 1
                                   Score
                            FROM arGrdBkResults
                            WHERE StuEnrollId = @curStuEnrollId
                                  AND InstrGrdBkWgtDetailId = @curId
                                  AND ResNum = @times
                        );
                        IF @Score IS NULL
                        BEGIN
                            SET @Score =
                            (
                                SELECT TOP 1
                                       Score
                                FROM arGrdBkResults
                                WHERE StuEnrollId = @curStuEnrollId
                                      AND InstrGrdBkWgtDetailId = @curId
                                      AND ResNum = (@times - 1)
                            );
                        END;

                        IF @curGrdComponentTypeId = 544
                        BEGIN
                            SET @Score =
                            (
                                SELECT SUM(HoursAttended)
                                FROM arExternshipAttendance
                                WHERE StuEnrollId = @curStuEnrollId
                            );
                        END;

                    END;
                    --PRINT @Score; 
                    INSERT INTO #Temp21
                    VALUES
                    (@curId, @TermId, @curReqId, @GrdCompDescrip, @curNumber, @curGrdComponentTypeId, @Score,
                     @curMinResult, @curGrdComponentDescription, @curClsSectionId, @curStuEnrollId, @curRownumber);

                    SET @times = @times + 1;
                END;

            END;
            FETCH NEXT FROM getUsers_Cursor
            INTO @curId,
                 @curReqId,
                 @curDescrip,
                 @curNumber,
                 @curGrdComponentTypeId,
                 @curMinResult,
                 @curGrdComponentDescription,
                 @curClsSectionId,
                 @curStuEnrollId,
                 @curRownumber,
				 @curRowSeq
        END;
        CLOSE getUsers_Cursor;
        DEALLOCATE getUsers_Cursor;

        SET @CharInt =
        (
            SELECT CHARINDEX(@SysComponentTypeId, @SysComponentTypeIdList)
        );

        IF (@SysComponentTypeIdList IS NULL)
           OR (@CharInt >= 1)
        BEGIN
            SELECT Id,
                   --, StuEnrollId 
                   TermId,
                   GradeBookDescription,
                   Number,
                   GradeBookSysComponentTypeId,
                   GradeBookScore,
                   MinResult,
                   GradeComponentDescription,
                   RowNumber,
                   ClsSectionId
				      ,Seq
            FROM #Temp21
            WHERE GradeBookSysComponentTypeId = @SysComponentTypeId
			AND
              (
                  @ShowIncomplete = 1
                  OR
                  (
                      @ShowIncomplete = 0
                      AND GradeBookScore IS NOT NULL
                  )
              )
            UNION
            SELECT GBWD.InstrGrdBkWgtDetailId,
                   --, SE.StuEnrollId 
                   T.TermId,
                   GCT.Descrip AS GradeBookDescription,
                   GBWD.Number,
                   GCT.SysComponentTypeId,
                   GBCR.Score,
                   GBCR.MinResult,
                   SYRES.Resource,
                   ROW_NUMBER() OVER (PARTITION BY --SE.StuEnrollId,  
                                          T.TermId,
                                          GCT.SysComponentTypeId
                                      ORDER BY GCT.SysComponentTypeId,
                                               GCT.Descrip
                                     ) AS rownumber,
                   (
                       SELECT TOP 1
                              ClsSectionId
                       FROM arClassSections
                       WHERE TermId = T.TermId
                             AND ReqId = R.ReqId
                   ) AS ClsSectionId
				      ,GBWD.Seq
            FROM arGrdBkConversionResults GBCR
                INNER JOIN arStuEnrollments SE
                    ON GBCR.StuEnrollId = SE.StuEnrollId
                INNER JOIN
                (SELECT StudentId, FirstName, LastName, MiddleName FROM adLeads) S
                    ON S.StudentId = SE.StudentId
                INNER JOIN arPrgVersions PV
                    ON SE.PrgVerId = PV.PrgVerId
                INNER JOIN arTerm T
                    ON GBCR.TermId = T.TermId
                INNER JOIN arReqs R
                    ON GBCR.ReqId = R.ReqId
                INNER JOIN arGrdComponentTypes GCT
                    ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWgtDetails GBWD
                    ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                       AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                INNER JOIN arGrdBkWeights GBW
                    ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                       AND GBCR.ReqId = GBW.ReqId
                INNER JOIN
                (
                    SELECT ReqId,
                           MAX(EffectiveDate) AS EffectiveDate
                    FROM arGrdBkWeights
                    GROUP BY ReqId
                ) AS MaxEffectiveDatesByCourse
                    ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                INNER JOIN
                (
                    SELECT Resource,
                           ResourceID
                    FROM syResources
                    WHERE ResourceTypeID = 10
                ) SYRES
                    ON SYRES.ResourceID = GCT.SysComponentTypeId
                INNER JOIN syCampuses C
                    ON SE.CampusId = C.CampusId
            WHERE MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                  AND SE.StuEnrollId IN
                      (
                          SELECT Val
                          FROM MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                      )
                  AND T.TermId = @TermId
                  AND R.ReqId = @ReqId
                  AND
                  (
                      @SysComponentTypeIdList IS NULL
                      OR GCT.SysComponentTypeId IN
                         (
                             SELECT Val
                             FROM MultipleValuesForReportParameters(@SysComponentTypeIdList, '','', 1)
                         )
                  )
                  AND
                  (
                      @SysComponentTypeId IS NULL
                      OR GCT.SysComponentTypeId = @SysComponentTypeId
                  )
				  AND
              (
                  @ShowIncomplete = 1
                  OR
                  (
                      @ShowIncomplete = 0
                      AND GBCR.Score IS NOT NULL
                  )
              )
            ORDER BY GradeBookSysComponentTypeId,
			seq,
                     GradeBookDescription,
                     RowNumber;
        END;
    END;
    DROP TABLE #Temp22;
    DROP TABLE #Temp21;
END;
-- =========================================================================================================
-- END  --  Usp_PR_Sub5_ComponentsByCourse_GivenComponentTypeId
-- =========================================================================================================




'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_RE_REGISTER_STUDENT_RESULTS]'
GO
IF OBJECT_ID(N'[dbo].[USP_RE_REGISTER_STUDENT_RESULTS]', 'P') IS NULL
EXEC sp_executesql N'-- =============================================
-- Author: <Kimberly,Befera>
-- Create date: <8/5/19>
-- Description:	<Adds arResults place holder for student moved back to Future Start >
-- =============================================
CREATE  PROCEDURE [dbo].[USP_RE_REGISTER_STUDENT_RESULTS]
    @StuEnrollId UNIQUEIDENTIFIER,
    @ModUser VARCHAR(50)
AS

BEGIN

	DECLARE @ProgramVersionId UNIQUEIDENTIFIER
	 SET @ProgramVersionId =
    (
        SELECT TOP (1) PrgVerId
        FROM dbo.arStuEnrollments
		WHERE StuEnrollId =  @StuEnrollId
    );

   DECLARE @RegistrationType INT;
    SET @RegistrationType =
    (
        SELECT TOP (1)
               ProgramRegistrationType
        FROM dbo.arPrgVersions
        WHERE PrgVerId = @ProgramVersionId
        ORDER BY ModDate DESC
    );

    DECLARE @ShowROSSOnlyTabsForStudent VARCHAR(50);
    SET @ShowROSSOnlyTabsForStudent =
    (
        SELECT TOP (1)
               Value
        FROM dbo.syConfigAppSetValues AS configValues
            INNER JOIN dbo.syConfigAppSettings settings
                ON settings.SettingId = configValues.SettingId
        WHERE settings.KeyName = ''ShowROSSOnlyTabsForStudent''
        ORDER BY configValues.ModDate DESC
    );

    IF (@RegistrationType = 1)
    BEGIN
        INSERT INTO dbo.arResults
        (
            ResultId,
            TestId,
            Score,
            GrdSysDetailId,
            Cnt,
            Hours,
            StuEnrollId,
            IsInComplete,
            DroppedInAddDrop,
            ModUser,
            ModDate,
            IsTransfered,
            isClinicsSatisfied,
            DateDetermined,
            IsCourseCompleted,
            IsGradeOverridden,
            GradeOverriddenBy,
            GradeOverriddenDate,
            DateCompleted
        )
        SELECT NEWID(),              -- ResultId - uniqueidentifier
               classes.ClsSectionId, -- TestId - uniqueidentifier
               NULL,                 -- Score - decimal(18, 2)
               NULL,                 -- GrdSysDetailId - uniqueidentifier
               0,                    -- Cnt - int
               0,                    -- Hours - int
               @StuEnrollId, -- StuEnrollId - uniqueidentifier
               NULL,                 -- IsInComplete - bit
               NULL,                 -- DroppedInAddDrop - bit
               @ModUser,        -- ModUser - varchar(50)
               GETDATE(),            -- ModDate - datetime
               NULL,                 -- IsTransfered - bit
               NULL,                 -- isClinicsSatisfied - bit
               GETDATE(),            -- DateDetermined - datetime
               0,                    -- IsCourseCompleted - bit
               0,                    -- IsGradeOverridden - bit
               '''',                   -- GradeOverriddenBy - varchar(50)
               GETDATE(),            -- GradeOverriddenDate - datetime
               GETDATE()             -- DateCompleted - datetime
        FROM dbo.arPrgVersions programVersion
            INNER JOIN dbo.arTerm terms
                ON terms.ProgramVersionId = programVersion.PrgVerId
            INNER JOIN dbo.arClassSections classes
                ON classes.TermId = terms.TermId
        WHERE programVersion.PrgVerId = @ProgramVersionId;
END;
END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_TR_Sub07_TotalCourses]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub07_TotalCourses]', 'P') IS NULL
EXEC sp_executesql N'
-- =========================================================================================================  
-- USP_TR_Sub07_TotalCourses   
-- =========================================================================================================  
CREATE PROCEDURE [dbo].[USP_TR_Sub07_TotalCourses]
    @CoursesTakenTableName NVARCHAR(200)
   ,@GPASummaryTableName NVARCHAR(200)
   ,@StuEnrollIdList NVARCHAR(MAX)
   ,@TermId UNIQUEIDENTIFIER = NULL
   ,@ShowMultipleEnrollments BIT = 0
AS --  
    BEGIN

        -- 0) Declare varialbles and Set initial values  
        BEGIN
            --DECLARE @SQL01 AS NVARCHAR(MAX);  
            --DECLARE @SQL02 AS NVARCHAR(MAX);  

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#CoursesTaken'')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            CREATE TABLE #CoursesTaken
                (
                    CoursesTakenId INTEGER NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,ReqId UNIQUEIDENTIFIER
                   ,ReqCode VARCHAR(100)
                   ,ReqDescription VARCHAR(100)
                   ,ReqCodeDescrip VARCHAR(100)
                   ,ReqCredits DECIMAL(18, 2)
                   ,MinVal DECIMAL(18, 2)
                   ,ClsSectionId VARCHAR(50)
                   ,CourseStarDate DATETIME
                   ,CourseEndDate DATETIME
                   ,SCS_CreditsAttempted DECIMAL(18, 2)
                   ,SCS_CreditsEarned DECIMAL(18, 2)
                   ,SCS_CurrentScore DECIMAL(18, 2)
                   ,SCS_CurrentGrade VARCHAR(10)
                   ,SCS_FinalScore DECIMAL(18, 2)
                   ,SCS_FinalGrade VARCHAR(10)
                   ,SCS_Completed BIT
                   ,SCS_FinalGPA DECIMAL(18, 2)
                   ,SCS_Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_WeightedAverage_Credits DECIMAL(18, 2)
                   ,SCS_Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_SimpleAverage_Credits DECIMAL(18, 2)
                   ,SCS_ModUser VARCHAR(50)
                   ,SCS_ModDate DATETIME
                   ,SCS_TermGPA_Simple DECIMAL(18, 2)
                   ,SCS_TermGPA_Weighted DECIMAL(18, 2)
                   ,SCS_coursecredits DECIMAL(18, 2)
                   ,SCS_CumulativeGPA DECIMAL(18, 2)
                   ,SCS_CumulativeGPA_Simple DECIMAL(18, 2)
                   ,SCS_FACreditsEarned DECIMAL(18, 2)
                   ,SCS_Average DECIMAL(18, 2)
                   ,SCS_CumAverage DECIMAL(18, 2)
                   ,ScheduleDays DECIMAL(18, 2)
                   ,ActualDay DECIMAL(18, 2)
                   ,FinalGPA_Calculations DECIMAL(18, 2)
                   ,FinalGPA_TermCalculations DECIMAL(18, 2)
                   ,CreditsEarned_Calculations DECIMAL(18, 2)
                   ,ActualDay_Calculations DECIMAL(18, 2)
                   ,GrdBkWgtDetailsCount INTEGER
                   ,CountMultipleEnrollment INTEGER
                   ,RowNumberMultipleEnrollment INTEGER
                   ,CountRepeated INTEGER
                   ,RowNumberRetaked INTEGER
                   ,SameTermCountRetaken INTEGER
                   ,RowNumberSameTermRetaked INTEGER
                   ,RowNumberMultEnrollNoRetaken INTEGER
                   ,RowNumberOverAllByClassSection INTEGER
                   ,IsCreditsEarned BIT
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#GPASummary'')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
            CREATE TABLE #GPASummary
                (
                    GPASummaryId INTEGER NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,LastName VARCHAR(50)
                   ,FirstName VARCHAR(50)
                   ,MiddleName VARCHAR(50)
                   ,SSN VARCHAR(11)
                   ,StudentNumber VARCHAR(50)
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,EnrollmentID VARCHAR(50)
                   ,PrgVerId UNIQUEIDENTIFIER
                   ,PrgVerDescrip VARCHAR(50)
                   ,PrgVersionTrackCredits BIT
                   ,GrdSystemId UNIQUEIDENTIFIER
                   ,AcademicType VARCHAR(50)
                   ,ClockHourProgram VARCHAR(10)
                   ,IsMakingSAP BIT
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,TermEndDate DATETIME
                   ,DescripXTranscript VARCHAR(250)
                   ,TermAverage DECIMAL(18, 2)
                   ,EnroTermSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPACredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPA DECIMAL(18, 2)
                   ,EnroTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPACredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPA DECIMAL(18, 2)
                   ,StudTermSimple_CourseCredits DECIMAL(18, 2)
                   ,StudTermSimple_GPACredits DECIMAL(18, 2)
                   ,StudTermSimple_GPA DECIMAL(18, 2)
                   ,StudTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudTermWeight_GPACredits DECIMAL(18, 2)
                   ,StudTermWeight_GPA DECIMAL(18, 2)
                   ,EnroSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroSimple_GPACredits DECIMAL(18, 2)
                   ,EnroSimple_GPA DECIMAL(18, 2)
                   ,EnroWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroWeight_GPACredits DECIMAL(18, 2)
                   ,EnroWeight_GPA DECIMAL(18, 2)
                   ,StudSimple_CourseCredits DECIMAL(18, 2)
                   ,StudSimple_GPACredits DECIMAL(18, 2)
                   ,StudSimple_GPA DECIMAL(18, 2)
                   ,StudWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudWeight_GPACredits DECIMAL(18, 2)
                   ,StudWeight_GPA DECIMAL(18, 2)
                   ,GradesFormat VARCHAR(50)
                   ,GPAMethod VARCHAR(50)
                   ,GradeBookAt VARCHAR(50)
                   ,RetakenPolicy VARCHAR(50)
                );
        END;
        -- END --  0) Declare varialbles and Set initial values  

        --  1)Fill temp tables  
        BEGIN
            INSERT INTO #CoursesTaken
            EXEC USP_TR_Sub03_GetPreparedGPATable @TableName = @CoursesTakenTableName;

            INSERT INTO #GPASummary
            EXEC USP_TR_Sub03_GetPreparedGPATable @TableName = @GPASummaryTableName;
        END;
        -- END  --  1)Fill temp tables  

        -- to Test  
        -- SELECT * FROM #CoursesTaken AS CT ORDER BY CT.StudentId, CT.StuEnrollId, CT.TermId  
        -- SELECT * FROM #GPASummary AS GS ORDER BY GS.StudentId, GS.StuEnrollId, GS.TermId  

		DECLARE @includeRepeatedCourses VARCHAR(50) = (SELECT dbo.GetAppSettingValueByKeyName(''IncludeCreditsAttemptedForRepeatedCourses'', null))

        --  2) Selet Totals  
        BEGIN
            IF ( @ShowMultipleEnrollments = 0 )
                BEGIN

                    SELECT     GS.StuEnrollId AS Id
                              ,MIN(GS.GradesFormat) AS GradesFormat
                              ,MIN(GS.GPAMethod) AS GPAMethod
                              ,MIN(   CASE WHEN (
                                                dbo.GetACIdForStudent(GS.StuEnrollId) = 5
                                                AND dbo.GetGradesFormatGivenStudentEnrollId(GS.StuEnrollId) = ''numeric''
                                                ) THEN dbo.CalculateStudentAverage(GS.StuEnrollId, NULL, NULL, NULL, @TermId)
                                           ELSE GS.EnroSimple_GPA
                                      END
                                  ) AS EnrollmentSimpleGPA
                              ,MIN(   CASE WHEN (
                                                dbo.GetACIdForStudent(GS.StuEnrollId) = 5
                                                AND dbo.GetGradesFormatGivenStudentEnrollId(GS.StuEnrollId) = ''numeric''
                                                ) THEN dbo.CalculateStudentAverage(GS.StuEnrollId, NULL, NULL, NULL, @TermId)
                                           ELSE GS.EnroWeight_GPA
                                      END
                                  ) AS EnrollmentWeightedGPA
                              ,MAX(CONVERT(INTEGER, GS.IsMakingSAP)) AS IsMakingSAP
                              ,MIN(GS.AcademicType) AS AcademicType
                              ,MIN(CONVERT(INTEGER, GS.PrgVersionTrackCredits)) AS PrgVersionTrackCredits
                              ,CASE WHEN @includeRepeatedCourses = ''Yes'' THEN COUNT(CT.CoursesTakenId)
                                    ELSE COUNT(DISTINCT CT.ReqId)
                               END AS CoursesTaked
                              ,SUM(ISNULL(CT.SCS_CreditsAttempted, 0)) AS CreditsAttempted
                              ,SUM(ISNULL(CT.CreditsEarned_Calculations, 0)) AS CreditsEarned
                              ,SUM(ISNULL(CT.ScheduleDays, 0)) AS ScheduleDays
                              ,SUM(ISNULL(CT.ActualDay_Calculations, 0)) AS ActualDay
                              ,SUM(ISNULL(CT.FinalGPA_Calculations, 0)) AS GradePoints
                    FROM       #GPASummary AS GS
                    INNER JOIN #CoursesTaken AS CT ON CT.StuEnrollId = GS.StuEnrollId
                                                      AND CT.TermId = GS.TermId
                    WHERE      EXISTS (
                                      SELECT Val
                                      FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                      WHERE  CONVERT(UNIQUEIDENTIFIER, Val) = CT.StuEnrollId
                                      )
                               AND (
                                   @TermId IS NULL
                                   OR GS.TermId = @TermId
                                   )
                    GROUP BY   GS.StuEnrollId;
                END;
            ELSE
                BEGIN
                    SELECT     GS.StudentId AS Id
                              ,MIN(GS.GradesFormat) AS GradesFormat
                              ,MIN(GS.GPAMethod) AS GPAMethod
                              ,MIN(GS.StudSimple_GPA) AS EnrollmentSimpleGPA
                              ,MIN(GS.StudWeight_GPA) AS EnrollmentWeightedGPA
                              ,MAX(CONVERT(INTEGER, GS.IsMakingSAP)) AS IsMakingSAP
                              ,MIN(GS.AcademicType) AS AcademicType
                              ,MIN(CONVERT(INTEGER, GS.PrgVersionTrackCredits)) AS PrgVersionTrackCredits
                              ,CASE WHEN @includeRepeatedCourses  = ''Yes'' THEN COUNT(CT.CoursesTakenId)
                                    ELSE COUNT(DISTINCT CT.ReqId)
                               END AS CoursesTaked
                              ,SUM(ISNULL(CT.SCS_CreditsAttempted, 0)) AS CreditsAttempted
                              ,SUM(ISNULL(CT.CreditsEarned_Calculations, 0)) AS CreditsEarned
                              ,SUM(ISNULL(CT.ScheduleDays, 0)) AS ScheduleDays
                              ,SUM(ISNULL(CT.ActualDay_Calculations, 0)) AS ActualDay
                              ,SUM(ISNULL(CT.FinalGPA_Calculations, 0)) AS GradePoints
                    FROM       #GPASummary AS GS
                    INNER JOIN #CoursesTaken AS CT ON CT.StuEnrollId = GS.StuEnrollId
                                                      AND CT.TermId = GS.TermId
                    WHERE      CT.RowNumberMultEnrollNoRetaken = 1
                               AND EXISTS (
                                          SELECT Val
                                          FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                          WHERE  CONVERT(UNIQUEIDENTIFIER, Val) = CT.StuEnrollId
                                          )
                               AND (
                                   @TermId IS NULL
                                   OR GS.TermId = @TermId
                                   )
                    GROUP BY   GS.StudentId;
                END;
        END;
        -- END  --  2) Selet Totals  

        -- 3) drop temp tables  
        BEGIN
            -- Drop temp tables  
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#CoursesTaken'')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#GPASummary'')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
        END;
    -- END  --  3) drop temp tables  
    END;
-- =========================================================================================================  
-- END  --  USP_TR_Sub07_TotalCourses   
-- =========================================================================================================  


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_WeeklyAttendance]'
GO
IF OBJECT_ID(N'[dbo].[USP_WeeklyAttendance]', 'P') IS NULL
EXEC sp_executesql N'--================================================================================================= 
-- USP_WeeklyAttendance 
--================================================================================================= 
CREATE PROCEDURE [dbo].[USP_WeeklyAttendance]
    @WeekStartDate DATETIME
   ,@WeekEndDate DATETIME
   ,@CampusID UNIQUEIDENTIFIER
   ,@TermId VARCHAR(8000) = NULL
   ,@CourseId VARCHAR(8000) = NULL
   ,@InstructorId VARCHAR(8000) = NULL
   ,@ShiftId VARCHAR(8000) = NULL
   ,@ClassId VARCHAR(8000) = NULL
   ,@EnrollmentId VARCHAR(8000) = NULL
AS
    BEGIN
        DECLARE @StudentIdentifier AS NVARCHAR(1000);
        DECLARE @TrackSapAttendance AS NVARCHAR(1000);
        DECLARE @AttendanceWindow TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,RecordDate DATETIME
               ,SchedHours DECIMAL(18, 2)
               ,ActualHours DECIMAL(18, 2)
               ,IsTardy BIT
            );
        SELECT     @StudentIdentifier = SCASV.Value
        FROM       syConfigAppSettings AS SCAS
        INNER JOIN syConfigAppSetValues AS SCASV ON SCASV.SettingId = SCAS.SettingId
        WHERE      SCAS.KeyName = ''StudentIdentifier''; -- @StudentIdentifier -->  ''SSN'', ''EnrollmentId'', ''StudentId''            

        SET @TrackSapAttendance = dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', @CampusID);
        IF ( @TrackSapAttendance IS NOT NULL )
            BEGIN
                SET @TrackSapAttendance = LOWER(LTRIM(RTRIM(@TrackSapAttendance)));
            END;

        IF ( @TrackSapAttendance = ''byday'' )
            BEGIN

                SELECT   C.StuEnrollId
                        ,C.StudentIdentifier
                        ,C.StudentName
                        ,CONVERT(DECIMAL(15, 2), SUM(C.Mon) / ActualDivisor) AS Mon
                        ,CONVERT(DECIMAL(15, 2), SUM(C.Tue) / ActualDivisor) AS Tue
                        ,CONVERT(DECIMAL(15, 2), SUM(C.Wed) / ActualDivisor) AS Wed
                        ,CONVERT(DECIMAL(15, 2), SUM(C.Thur) / ActualDivisor) AS Thur
                        ,CONVERT(DECIMAL(15, 2), SUM(C.Fri) / ActualDivisor) AS Fri
                        ,CONVERT(DECIMAL(15, 2), SUM(C.Sat) / ActualDivisor) AS Sat
                        ,CONVERT(DECIMAL(15, 2), SUM(C.Sun) / ActualDivisor) AS Sun
                        ,CONVERT(DECIMAL(15, 2), SUM(C.WeekTotal) / ActualDivisor) AS WeekTotal
                        ,CONVERT(DECIMAL(15, 2), SUM(C.NewTotal) / ActualDivisor) AS NewTotal
                        ,CONVERT(DECIMAL(15, 2), SUM(C.NewTotalSched) / ActualDivisor) AS NewTotalSched
                        ,CONVERT(DECIMAL(15, 2), SUM(C.NewTotalDaysAbsent) / ActualDivisor) AS NewTotalDaysAbsent
                        ,CONVERT(DECIMAL(15, 2), SUM(C.NewTotalMakeUpDays) / ActualDivisor) AS NewTotalMakeUpDays
                        ,CONVERT(DECIMAL(15, 2), SUM(C.NewNetDaysAbsent) / ActualDivisor) AS NewNetDaysAbsent
                        ,CONVERT(DECIMAL(15, 2), SUM(C.NewSchedDaysatLDA) / ActualDivisor) AS NewSchedDaysatLDA
                        ,LEFT(CONVERT(VARCHAR, C.LDA, 120), 10) AS LDA
                        --,C.CampGrpDescrip                       
                        ,C.AttTypeID
                        ,C.SuppressDate
                        ,NULL AS InstructorId
                        ,NULL AS ShiftId
                        ,StatusCodeId
                        ,StatusCodeDescrip
						,C.UnitTypeDescrip
                FROM     (
                         SELECT   B.*
                         FROM     (
                                  SELECT     A.StuEnrollId
                                            ,CASE WHEN @StudentIdentifier = ''SSN'' THEN ''***-**-'' + SUBSTRING(S.SSN, 6, 4)
                                                  WHEN @StudentIdentifier = ''EnrollmentId'' THEN SE.EnrollmentId
                                                  WHEN @StudentIdentifier = ''StudentId'' THEN S.StudentNumber
                                                  ELSE S.StudentNumber
                                             END AS StudentIdentifier
                                            --S.SSN AS StudentIdentifier                   
                                            ,ISNULL((
                                                    SELECT LastName
                                                    FROM   arStudent
                                                    WHERE  StudentId IN (
                                                                        SELECT StudentId
                                                                        FROM   arStuEnrollments
                                                                        WHERE  StuEnrollId = A.StuEnrollId
                                                                        )
                                                    )
                                                   ,'' ''
                                                   ) + '','' + ISNULL((
                                                                    SELECT FirstName
                                                                    FROM   arStudent
                                                                    WHERE  StudentId IN (
                                                                                        SELECT StudentId
                                                                                        FROM   arStuEnrollments
                                                                                        WHERE  StuEnrollId = A.StuEnrollId
                                                                                        )
                                                                    )
                                                                   ,'' ''
                                                                   ) + '' '' + ISNULL((
                                                                                    SELECT MiddleName
                                                                                    FROM   arStudent
                                                                                    WHERE  StudentId IN (
                                                                                                        SELECT StudentId
                                                                                                        FROM   arStuEnrollments
                                                                                                        WHERE  StuEnrollId = A.StuEnrollId
                                                                                                        )
                                                                                    )
                                                                                   ,'' ''
                                                                                   ) + '' '' AS StudentName
                                            ,(
                                             SELECT SUM(ActualHours)
                                             FROM   dbo.arStudentClockAttendance
                                             WHERE  StuEnrollId = A.StuEnrollId
                                                    AND CONVERT(DATE, RecordDate, 101) = @WeekStartDate
                                                    AND ActualHours > 0
                                                    AND (
                                                        ActualHours <> 99.00
                                                        AND ActualHours <> 999.00
                                                        AND ActualHours <> 9999.00
                                                        )
                                             ) AS Mon
                                            ,(
                                             SELECT SUM(ActualHours)
                                             FROM   dbo.arStudentClockAttendance
                                             WHERE  StuEnrollId = A.StuEnrollId
                                                    AND CONVERT(DATE, RecordDate, 101) = DATEADD(DAY, 1, @WeekStartDate)
                                                    AND ActualHours > 0
                                                    AND (
                                                        ActualHours <> 99.00
                                                        AND ActualHours <> 999.00
                                                        AND ActualHours <> 9999.00
                                                        )
                                             ) AS Tue
                                            ,(
                                             SELECT SUM(ActualHours)
                                             FROM   dbo.arStudentClockAttendance
                                             WHERE  StuEnrollId = A.StuEnrollId
                                                    AND CONVERT(DATE, RecordDate, 101) = DATEADD(DAY, 2, @WeekStartDate)
                                                    AND ActualHours > 0
                                                    AND (
                                                        ActualHours <> 99.00
                                                        AND ActualHours <> 999.00
                                                        AND ActualHours <> 9999.00
                                                        )
                                             ) AS Wed
                                            ,(
                                             SELECT SUM(ActualHours)
                                             FROM   dbo.arStudentClockAttendance
                                             WHERE  StuEnrollId = A.StuEnrollId
                                                    AND CONVERT(DATE, RecordDate, 101) = DATEADD(DAY, 3, @WeekStartDate)
                                                    AND ActualHours > 0
                                                    AND (
                                                        ActualHours <> 99.00
                                                        AND ActualHours <> 999.00
                                                        AND ActualHours <> 9999.00
                                                        )
                                             ) AS Thur
                                            ,(
                                             SELECT SUM(ActualHours)
                                             FROM   dbo.arStudentClockAttendance
                                             WHERE  StuEnrollId = A.StuEnrollId
                                                    AND CONVERT(DATE, RecordDate, 101) = DATEADD(DAY, 4, @WeekStartDate)
                                                    AND ActualHours > 0
                                                    AND (
                                                        ActualHours <> 99.00
                                                        AND ActualHours <> 999.00
                                                        AND ActualHours <> 9999.00
                                                        )
                                             ) AS Fri
                                            ,(
                                             SELECT SUM(ActualHours)
                                             FROM   dbo.arStudentClockAttendance
                                             WHERE  StuEnrollId = A.StuEnrollId
                                                    AND CONVERT(DATE, RecordDate, 101) = DATEADD(DAY, 5, @WeekStartDate)
                                                    AND ActualHours > 0
                                                    AND (
                                                        ActualHours <> 99.00
                                                        AND ActualHours <> 999.00
                                                        AND ActualHours <> 9999.00
                                                        )
                                             ) AS Sat
                                            ,(
                                             SELECT SUM(ActualHours)
                                             FROM   dbo.arStudentClockAttendance
                                             WHERE  StuEnrollId = A.StuEnrollId
                                                    AND CONVERT(DATE, RecordDate, 101) = @WeekEndDate
                                                    AND ActualHours > 0
                                                    AND (
                                                        ActualHours <> 99.00
                                                        AND ActualHours <> 999.00
                                                        AND ActualHours <> 9999.00
                                                        )
                                             ) AS Sun
                                            ,ISNULL((
                                                    SELECT SUM(ActualHours)
                                                    FROM   dbo.arStudentClockAttendance
                                                    WHERE  ( CONVERT(DATE, RecordDate, 101)) <= @WeekEndDate
                                                           AND ( CONVERT(DATE, RecordDate, 101)) >= @WeekStartDate
                                                           AND StuEnrollId = A.StuEnrollId
                                                           AND ActualHours > 0
                                                           AND (
                                                               ActualHours <> 99.00
                                                               AND ActualHours <> 999.00
                                                               AND ActualHours <> 9999.00
                                                               )
                                                    )
                                                   ,0.00
                                                   ) AS WeekTotal
                                            ,ISNULL((
                                                    SELECT SUM(ActualHours)
                                                    FROM   dbo.arStudentClockAttendance
                                                    WHERE  ( CONVERT(DATE, RecordDate, 101)) <= @WeekEndDate
                                                           AND StuEnrollId = A.StuEnrollId
                                                           AND ActualHours > 0
                                                           AND (
                                                               ActualHours <> 99.00
                                                               AND ActualHours <> 999.00
                                                               AND ActualHours <> 9999.00
                                                               )
                                                    )
                                                   ,0.00
                                                   ) AS NewTotal
                                            ,ISNULL((
                                                    SELECT SUM(SchedHours)
                                                    FROM   dbo.arStudentClockAttendance
                                                    WHERE  ( CONVERT(DATE, RecordDate, 101)) <= @WeekEndDate
                                                           AND StuEnrollId = A.StuEnrollId
                                                           AND SchedHours > 0
                                                           AND ActualHours IS NOT NULL
                                                           AND (
                                                               ActualHours <> 99.00
                                                               AND ActualHours <> 999.00
                                                               AND ActualHours <> 9999.00
                                                               )
                                                    )
                                                   ,0.00
                                                   ) AS NewTotalSched
                                            ,ISNULL((
                                                    SELECT SUM(SchedHours - ActualHours)
                                                    FROM   dbo.arStudentClockAttendance
                                                    WHERE  ( CONVERT(DATE, RecordDate, 101)) <= @WeekEndDate
                                                           AND StuEnrollId = A.StuEnrollId
                                                           AND (
                                                               ActualHours <> 99.00
                                                               AND ActualHours <> 999.00
                                                               AND ActualHours <> 9999.00
                                                               )
                                                           AND SchedHours > 0
                                                           AND ActualHours IS NOT NULL
                                                           AND ( ActualHours < SchedHours )
                                                    )
                                                   ,0.00
                                                   ) AS NewTotalDaysAbsent
                                            ,ISNULL((
                                                    SELECT SUM(ActualHours - SchedHours)
                                                    FROM   dbo.arStudentClockAttendance
                                                    WHERE  ( CONVERT(DATE, RecordDate, 101)) <= @WeekEndDate
                                                           AND StuEnrollId = A.StuEnrollId
                                                           AND (
                                                               ActualHours <> 99.00
                                                               AND ActualHours <> 999.00
                                                               AND ActualHours <> 9999.00
                                                               )
                                                           AND SchedHours IS NOT NULL
                                                           AND ActualHours IS NOT NULL
                                                           AND ActualHours <> 0
                                                           AND ActualHours > SchedHours
                                                    )
                                                   ,0.00
                                                   ) AS NewTotalMakeUpDays
                                            ,ISNULL(((
                                                     SELECT SUM(SchedHours - ActualHours)
                                                     FROM   dbo.arStudentClockAttendance
                                                     WHERE  ( CONVERT(DATE, RecordDate, 101)) <= @WeekEndDate
                                                            AND StuEnrollId = A.StuEnrollId
                                                            AND (
                                                                ActualHours <> 99.00
                                                                AND ActualHours <> 999.00
                                                                AND ActualHours <> 9999.00
                                                                )
                                                            AND SchedHours > 0
                                                            AND ActualHours IS NOT NULL
                                                            AND ( ActualHours < SchedHours )
                                                     ) - (
                                                         SELECT SUM(ActualHours - SchedHours)
                                                         FROM   dbo.arStudentClockAttendance
                                                         WHERE  ( CONVERT(DATE, RecordDate, 101)) <= @WeekEndDate
                                                                AND StuEnrollId = A.StuEnrollId
                                                                AND (
                                                                    ActualHours <> 99.00
                                                                    AND ActualHours <> 999.00
                                                                    AND ActualHours <> 9999.00
                                                                    )
                                                                AND SchedHours IS NOT NULL
                                                                AND ActualHours IS NOT NULL
                                                                AND ActualHours <> 0
                                                                AND ActualHours > SchedHours
                                                         )
                                                    )
                                                   ,0.00
                                                   ) AS NewNetDaysAbsent
                                            ,ISNULL((
                                                    SELECT SUM(SchedHours)
                                                    FROM   dbo.arStudentClockAttendance
                                                    WHERE  StuEnrollId = A.StuEnrollId
                                                           AND (
                                                               ActualHours <> 99.00
                                                               AND ActualHours <> 999.00
                                                               AND ActualHours <> 9999.00
                                                               )
                                                           AND CONVERT(DATE, RecordDate, 101) <= dbo.GetLDA(A.StuEnrollId)
                                                    )
                                                   ,0.00
                                                   ) AS NewSchedDaysatLDA
                                            ,( dbo.GetLDA(A.StuEnrollId)) AS LDA
                                            --,CG.CampGrpDescrip                       
                                            ,F.CampDescrip
                                            ,(
                                             SELECT UnitTypeId
                                             FROM   arPrgVersions PV
                                             WHERE  PV.PrgVerId IN (
                                                                   SELECT PrgVerId
                                                                   FROM   arStuEnrollments
                                                                   WHERE  StuEnrollId = A.StuEnrollId
                                                                   )
                                             ) AS AttTypeID
                                            ,(
                                             SELECT     aut.UnitTypeDescrip
                                             FROM       arPrgVersions PV
                                             INNER JOIN dbo.arAttUnitType aut ON aut.UnitTypeId = PV.UnitTypeId
                                             WHERE      PV.PrgVerId IN (
                                                                       SELECT PrgVerId
                                                                       FROM   arStuEnrollments
                                                                       WHERE  StuEnrollId = A.StuEnrollId
                                                                       )
                                             ) AS UnitTypeDescrip
                                            ,''yes'' AS SuppressDate
                                            ,SE.StatusCodeId
                                            ,SC.StatusCodeDescrip
                                            ,1 AS ActualDivisor
                                  FROM       dbo.arStudentClockAttendance A
                                  INNER JOIN arStuEnrollments SE ON SE.StuEnrollId = A.StuEnrollId
                                  INNER JOIN arStudent S ON S.StudentId = SE.StudentId
                                  INNER JOIN syCampuses F ON F.CampusId = SE.CampusId
                                  INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                                  WHERE      ( CONVERT(DATE, A.RecordDate, 101)) >= @WeekStartDate
                                             AND ( CONVERT(DATE, A.RecordDate, 101)) <= @WeekEndDate
                                             AND SE.CampusId = F.CampusId
                                             AND F.CampusId = @CampusID
                                             AND (
                                                 @EnrollmentId IS NULL
                                                 OR @EnrollmentId = ''''
                                                 OR SE.StatusCodeId IN (
                                                                       SELECT strval
                                                                       FROM   dbo.SPLIT(@EnrollmentId)
                                                                       )
                                                 )
                                  ) B
                         GROUP BY B.StuEnrollId
                                 ,B.StudentName
                                 ,B.StudentIdentifier
                                 --,B.CampGrpDescrip                       
                                 ,B.CampDescrip
                                 ,B.Mon
                                 ,B.Tue
                                 ,B.Wed
                                 ,B.Thur
                                 ,B.Fri
                                 ,B.Sat
                                 ,B.Sun
                                 ,B.WeekTotal
                                 ,B.NewTotal
                                 ,B.NewTotalSched
                                 ,B.NewTotalDaysAbsent
                                 ,B.NewTotalMakeUpDays
                                 ,B.NewNetDaysAbsent
                                 ,B.NewSchedDaysatLDA
                                 ,B.LDA
                                 ,B.AttTypeID
                                 ,B.SuppressDate
                                 ,B.StatusCodeId
                                 ,B.StatusCodeDescrip
                                 ,B.ActualDivisor
								 ,B.UnitTypeDescrip
                         ) C
                GROUP BY C.StuEnrollId
                        ,C.StudentIdentifier
                        ,C.StudentName
                        ,C.LDA
                        --,C.CampGrpDescrip                       
                        ,C.AttTypeID
                        ,C.SuppressDate
                        ,C.StatusCodeId
                        ,C.StatusCodeDescrip
                        ,C.ActualDivisor
						,C.UnitTypeDescrip
                ORDER BY C.StudentName
                        ,C.StudentIdentifier;

            END;
        ELSE
            SELECT   C.StuEnrollId
                    ,C.StudentIdentifier
                    ,C.StudentName
                    ,CONVERT(DECIMAL(15, 2), SUM(C.Mon) / ActualDivisor) AS Mon
                    ,CONVERT(DECIMAL(15, 2), SUM(C.Tue) / ActualDivisor) AS Tue
                    ,CONVERT(DECIMAL(15, 2), SUM(C.Wed) / ActualDivisor) AS Wed
                    ,CONVERT(DECIMAL(15, 2), SUM(C.Thur) / ActualDivisor) AS Thur
                    ,CONVERT(DECIMAL(15, 2), SUM(C.Fri) / ActualDivisor) AS Fri
                    ,CONVERT(DECIMAL(15, 2), SUM(C.Sat) / ActualDivisor) AS Sat
                    ,CONVERT(DECIMAL(15, 2), SUM(C.Sun) / ActualDivisor) AS Sun
                    ,CONVERT(DECIMAL(15, 2), SUM(C.WeekTotal) / ActualDivisor) AS WeekTotal
                    ,CONVERT(DECIMAL(15, 2), SUM(C.NewTotal) / ActualDivisor) AS NewTotal
                    ,CONVERT(DECIMAL(15, 2), SUM(C.NewTotalSched) / ActualDivisor) AS NewTotalSched
                    ,CONVERT(DECIMAL(15, 2), SUM(C.NewTotalDaysAbsent) / ActualDivisor) AS NewTotalDaysAbsent
                    ,CONVERT(DECIMAL(15, 2), SUM(C.NewTotalMakeUpDays) / ActualDivisor) AS NewTotalMakeUpDays
                    ,CONVERT(DECIMAL(15, 2), SUM(C.NewNetDaysAbsent) / ActualDivisor) AS NewNetDaysAbsent
                    ,CONVERT(DECIMAL(15, 2), SUM(C.NewSchedDaysatLDA) / ActualDivisor) AS NewSchedDaysatLDA
                    ,C.LDA
                    --,C.CampGrpDescrip                       
                    ,C.AttTypeID
                    ,C.SuppressDate
                    ,C.ClsSectionId
                    ,C.ClsSection
                    ,Descrip
                    ,FullName
                    ,TermId
                    ,TermDescrip
                    ,ReqId
                    ,Code
                    ,InstructorId
                    ,ShiftId
                    ,StatusCodeId
                    ,StatusCodeDescrip
                    ,UnitTypeDescrip
            FROM     (
                     SELECT   B.*
                     FROM     (
                              SELECT     A.StuEnrollId
                                        ,CASE WHEN @StudentIdentifier = ''SSN'' THEN ''***-**-'' + SUBSTRING(S.SSN, 6, 4)
                                              WHEN @StudentIdentifier = ''EnrollmentId'' THEN SE.EnrollmentId
                                              WHEN @StudentIdentifier = ''StudentId'' THEN S.StudentNumber
                                              ELSE S.StudentNumber
                                         END AS StudentIdentifier
                                        --S.SSN AS StudentIdentifier                   
                                        ,ISNULL((
                                                SELECT LastName
                                                FROM   arStudent
                                                WHERE  StudentId IN (
                                                                    SELECT StudentId
                                                                    FROM   arStuEnrollments
                                                                    WHERE  StuEnrollId = A.StuEnrollId
                                                                    )
                                                )
                                               ,'' ''
                                               ) + '','' + ISNULL((
                                                                SELECT FirstName
                                                                FROM   arStudent
                                                                WHERE  StudentId IN (
                                                                                    SELECT StudentId
                                                                                    FROM   arStuEnrollments
                                                                                    WHERE  StuEnrollId = A.StuEnrollId
                                                                                    )
                                                                )
                                                               ,'' ''
                                                               ) + '' '' + ISNULL((
                                                                                SELECT MiddleName
                                                                                FROM   arStudent
                                                                                WHERE  StudentId IN (
                                                                                                    SELECT StudentId
                                                                                                    FROM   arStuEnrollments
                                                                                                    WHERE  StuEnrollId = A.StuEnrollId
                                                                                                    )
                                                                                )
                                                                               ,'' ''
                                                                               ) + '' '' AS StudentName
                                        ,(
                                         SELECT SUM(Actual)
                                         FROM   atClsSectAttendance
                                         WHERE  StuEnrollId = A.StuEnrollId
                                                AND ClsSectionId = A.ClsSectionId
                                                AND CONVERT(DATE, MeetDate, 101) = @WeekStartDate
                                                AND Actual <> 999.0
                                                AND Actual <> 9999.0
                                         ) AS Mon
                                        ,(
                                         SELECT SUM(Actual)
                                         FROM   atClsSectAttendance
                                         WHERE  StuEnrollId = A.StuEnrollId
                                                AND ClsSectionId = A.ClsSectionId
                                                AND CONVERT(DATE, MeetDate, 101) = DATEADD(DAY, 1, @WeekStartDate)
                                                AND Actual <> 999.0
                                                AND Actual <> 9999.0
                                         ) AS Tue
                                        ,(
                                         SELECT SUM(Actual)
                                         FROM   atClsSectAttendance
                                         WHERE  StuEnrollId = A.StuEnrollId
                                                AND ClsSectionId = A.ClsSectionId
                                                AND CONVERT(DATE, MeetDate, 101) = DATEADD(DAY, 2, @WeekStartDate)
                                                AND Actual <> 999.0
                                                AND Actual <> 9999.0
                                         ) AS Wed
                                        ,(
                                         SELECT SUM(Actual)
                                         FROM   atClsSectAttendance
                                         WHERE  StuEnrollId = A.StuEnrollId
                                                AND ClsSectionId = A.ClsSectionId
                                                AND CONVERT(DATE, MeetDate, 101) = DATEADD(DAY, 3, @WeekStartDate)
                                                AND Actual <> 999.0
                                                AND Actual <> 9999.0
                                         ) AS Thur
                                        ,(
                                         SELECT SUM(Actual)
                                         FROM   atClsSectAttendance
                                         WHERE  StuEnrollId = A.StuEnrollId
                                                AND ClsSectionId = A.ClsSectionId
                                                AND CONVERT(DATE, MeetDate, 101) = DATEADD(DAY, 4, @WeekStartDate)
                                                AND Actual <> 999.0
                                                AND Actual <> 9999.0
                                         ) AS Fri
                                        ,(
                                         SELECT SUM(Actual)
                                         FROM   atClsSectAttendance
                                         WHERE  StuEnrollId = A.StuEnrollId
                                                AND ClsSectionId = A.ClsSectionId
                                                AND CONVERT(DATE, MeetDate, 101) = DATEADD(DAY, 5, @WeekStartDate)
                                                AND Actual <> 999.0
                                                AND Actual <> 9999.0
                                         ) AS Sat
                                        ,(
                                         SELECT SUM(Actual)
                                         FROM   atClsSectAttendance
                                         WHERE  StuEnrollId = A.StuEnrollId
                                                AND ClsSectionId = A.ClsSectionId
                                                AND CONVERT(DATE, MeetDate, 101) = @WeekEndDate
                                                AND Actual <> 999.0
                                                AND Actual <> 9999.0
                                         ) AS Sun
                                        ,ISNULL((
                                                SELECT SUM(Actual)
                                                FROM   atClsSectAttendance
                                                WHERE  ( CONVERT(DATE, MeetDate, 101)) <= @WeekEndDate
                                                       AND ( CONVERT(DATE, MeetDate, 101)) >= @WeekStartDate
                                                       AND StuEnrollId = A.StuEnrollId
                                                       AND ClsSectionId = A.ClsSectionId
                                                       AND Actual <> 999.0
                                                       AND Actual <> 9999.0
                                                )
                                               ,0.00
                                               ) AS WeekTotal
                                        ,ISNULL((
                                                SELECT SUM(Actual)
                                                FROM   atClsSectAttendance
                                                WHERE  ( CONVERT(DATE, MeetDate, 101)) <= @WeekEndDate
                                                       AND StuEnrollId = A.StuEnrollId
                                                       AND ClsSectionId = A.ClsSectionId
                                                       AND Actual <> 999.0
                                                       AND Actual <> 9999.0
                                                )
                                               ,0.00
                                               ) AS NewTotal
                                        ,ISNULL((
                                                SELECT SUM(Scheduled)
                                                FROM   atClsSectAttendance
                                                WHERE  ( CONVERT(DATE, MeetDate, 101)) <= @WeekEndDate
                                                       AND StuEnrollId = A.StuEnrollId
                                                       AND ClsSectionId = A.ClsSectionId
                                                       AND Actual <> 999.0
                                                       AND Actual <> 9999.0
                                                )
                                               ,0.00
                                               ) AS NewTotalSched
                                        ,ISNULL((
                                                SELECT SUM(Scheduled - Actual)
                                                FROM   atClsSectAttendance
                                                WHERE  ( CONVERT(DATE, MeetDate, 101)) <= @WeekEndDate
                                                       AND StuEnrollId = A.StuEnrollId
                                                       AND ClsSectionId = A.ClsSectionId
                                                       AND Actual <> 999.0
                                                       AND Actual <> 9999.0
                                                       AND Scheduled IS NOT NULL
                                                       AND Scheduled <> 0
                                                       AND Actual IS NOT NULL
                                                       AND Scheduled > Actual
                                                )
                                               ,0.00
                                               ) AS NewTotalDaysAbsent
                                        ,ISNULL((
                                                SELECT SUM(Actual - Scheduled)
                                                FROM   atClsSectAttendance
                                                WHERE  ( CONVERT(DATE, MeetDate, 101)) <= @WeekEndDate
                                                       AND StuEnrollId = A.StuEnrollId
                                                       AND ClsSectionId = A.ClsSectionId
                                                       AND Actual <> 999.0
                                                       AND Actual <> 9999.0
                                                       AND Scheduled IS NOT NULL
                                                       AND Actual IS NOT NULL
                                                       AND Actual <> 0
                                                       AND Scheduled < Actual
                                                )
                                               ,0.00
                                               ) AS NewTotalMakeUpDays
                                        ,ISNULL(((
                                                 SELECT SUM(Scheduled - Actual)
                                                 FROM   atClsSectAttendance
                                                 WHERE  ( CONVERT(DATE, MeetDate, 101)) <= @WeekEndDate
                                                        AND StuEnrollId = A.StuEnrollId
                                                        AND ClsSectionId = A.ClsSectionId
                                                        AND Actual <> 999.0
                                                        AND Actual <> 9999.0
                                                        AND Scheduled IS NOT NULL
                                                        AND Scheduled <> 0
                                                        AND Actual IS NOT NULL
                                                        AND Scheduled > Actual
                                                 ) - (
                                                     SELECT SUM(Actual - Scheduled)
                                                     FROM   atClsSectAttendance
                                                     WHERE  ( CONVERT(DATE, MeetDate, 101)) <= @WeekEndDate
                                                            AND StuEnrollId = A.StuEnrollId
                                                            AND ClsSectionId = A.ClsSectionId
                                                            AND Actual <> 999.0
                                                            AND Actual <> 9999.0
                                                            AND Scheduled IS NOT NULL
                                                            AND Actual IS NOT NULL
                                                            AND Actual <> 0
                                                            AND Scheduled < Actual
                                                     )
                                                )
                                               ,0.00
                                               ) AS NewNetDaysAbsent
                                        ,ISNULL((
                                                SELECT SUM(Scheduled)
                                                FROM   atClsSectAttendance
                                                WHERE  StuEnrollId = A.StuEnrollId
                                                       AND ClsSectionId = A.ClsSectionId
                                                       AND Actual <> 999.0
                                                       AND Actual <> 9999.0
                                                       AND CONVERT(DATE, MeetDate, 101) <= dbo.GetLDA(A.StuEnrollId)
                                                )
                                               ,0.00
                                               ) AS NewSchedDaysatLDA
                                        ,(
                                         SELECT CONVERT(VARCHAR, MAX(MeetDate), 101)
                                         FROM   atClsSectAttendance
                                         WHERE  StuEnrollId = A.StuEnrollId
                                                AND Actual <> 999.0
                                                AND Actual <> 9999.0
                                                AND Actual <> 0.00
                                         ) AS LDA
                                        --,CG.CampGrpDescrip                       
                                        ,F.CampDescrip
                                        ,(
                                         SELECT UnitTypeId
                                         FROM   arPrgVersions PV
                                         WHERE  PV.PrgVerId IN (
                                                               SELECT PrgVerId
                                                               FROM   arStuEnrollments
                                                               WHERE  StuEnrollId = A.StuEnrollId
                                                               )
                                         ) AS AttTypeID
                                        ,''yes'' AS SuppressDate
                                        ,A.ClsSectionId
                                        ,CS.ClsSection
                                        ,R.Descrip
                                        ,U.FullName
                                        ,CS.TermId
                                        ,T.TermDescrip
                                        ,CS.ReqId
                                        ,R.Code
                                        ,CS.InstructorId
                                        ,CS.ShiftId
                                        ,SE.StatusCodeId
                                        ,SC.StatusCodeDescrip
                                        ,CASE WHEN UT.UnitTypeId = ''B937C92E-FD7A-455E-A731-527A9918C734'' THEN 60
                                              ELSE 1
                                         END AS ActualDivisor
                                        ,UT.UnitTypeDescrip
                              FROM       atClsSectAttendance A
                              INNER JOIN arStuEnrollments SE ON SE.StuEnrollId = A.StuEnrollId
                              INNER JOIN arStudent S ON S.StudentId = SE.StudentId
                              INNER JOIN syCampuses F ON F.CampusId = SE.CampusId
                              INNER JOIN dbo.arClassSections CS ON CS.ClsSectionId = A.ClsSectionId
                              INNER JOIN dbo.arTerm T ON CS.TermId = T.TermId
                              INNER JOIN dbo.arReqs R ON R.ReqId = CS.ReqId
                              INNER JOIN dbo.syUsers U ON U.UserId = CS.InstructorId
                              INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                              INNER JOIN dbo.arAttUnitType UT ON R.UnitTypeId = UT.UnitTypeId
                              WHERE      ( CONVERT(DATE, MeetDate, 101)) >= @WeekStartDate
                                         AND ( CONVERT(DATE, MeetDate, 101)) <= @WeekEndDate
                                         AND SE.CampusId = F.CampusId
                                         AND A.ClsSectionId = CS.ClsSectionId
                                         AND F.CampusId = @CampusID
                                         AND (
                                             @TermId IS NULL
                                             OR @TermId = ''''
                                             OR T.TermId IN (
                                                            SELECT strval
                                                            FROM   dbo.SPLIT(@TermId)
                                                            )
                                             )
                                         AND (
                                             @CourseId IS NULL
                                             OR @CourseId = ''''
                                             OR R.ReqId IN (
                                                           SELECT strval
                                                           FROM   dbo.SPLIT(@CourseId)
                                                           )
                                             )
                                         AND (
                                             @InstructorId IS NULL
                                             OR @InstructorId = ''''
                                             OR CS.InstructorId IN (
                                                                   SELECT strval
                                                                   FROM   dbo.SPLIT(@InstructorId)
                                                                   )
                                             )
                                         AND (
                                             @ShiftId IS NULL
                                             OR @ShiftId = ''''
                                             OR CS.ShiftId IN (
                                                              SELECT strval
                                                              FROM   dbo.SPLIT(@ShiftId)
                                                              )
                                             )
                                         AND (
                                             @ClassId IS NULL
                                             OR @ClassId = ''''
                                             OR CS.ClsSectionId IN (
                                                                   SELECT strval
                                                                   FROM   dbo.SPLIT(@ClassId)
                                                                   )
                                             )
                                         AND (
                                             @EnrollmentId IS NULL
                                             OR @EnrollmentId = ''''
                                             OR SE.StatusCodeId IN (
                                                                   SELECT strval
                                                                   FROM   dbo.SPLIT(@EnrollmentId)
                                                                   )
                                             )
                              ) B
                     GROUP BY B.StuEnrollId
                             ,B.StudentName
                             ,B.StudentIdentifier
                             --,B.CampGrpDescrip                       
                             ,B.CampDescrip
                             ,B.Mon
                             ,B.Tue
                             ,B.Wed
                             ,B.Thur
                             ,B.Fri
                             ,B.Sat
                             ,B.Sun
                             ,B.WeekTotal
                             ,B.NewTotal
                             ,B.NewTotalSched
                             ,B.NewTotalDaysAbsent
                             ,B.NewTotalMakeUpDays
                             ,B.NewNetDaysAbsent
                             ,B.NewSchedDaysatLDA
                             ,B.LDA
                             ,B.AttTypeID
                             ,B.SuppressDate
                             ,B.ClsSectionId
                             ,B.ClsSection
                             ,B.Descrip
                             ,B.FullName
                             ,B.TermId
                             ,B.TermDescrip
                             ,B.ReqId
                             ,B.Code
                             ,B.InstructorId
                             ,B.ShiftId
                             ,B.StatusCodeId
                             ,B.StatusCodeDescrip
                             ,B.ActualDivisor
                             ,B.UnitTypeDescrip
                     ) C
            GROUP BY C.StuEnrollId
                    ,C.StudentIdentifier
                    ,C.StudentName
                    ,C.LDA
                    --,C.CampGrpDescrip                       
                    ,C.AttTypeID
                    ,C.SuppressDate
                    ,C.ClsSectionId
                    ,C.ClsSection
                    ,C.Descrip
                    ,C.FullName
                    ,TermId
                    ,TermDescrip
                    ,C.ReqId
                    ,C.Code
                    ,C.InstructorId
                    ,C.ShiftId
                    ,C.StatusCodeId
                    ,C.StatusCodeDescrip
                    ,C.ActualDivisor
                    ,C.UnitTypeDescrip
            ORDER BY C.StudentName
                    ,C.StudentIdentifier
                    ,C.ClsSectionId;
    END;
--================================================================================================= 
-- END  --  USP_WeeklyAttendance 
--================================================================================================= 
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END