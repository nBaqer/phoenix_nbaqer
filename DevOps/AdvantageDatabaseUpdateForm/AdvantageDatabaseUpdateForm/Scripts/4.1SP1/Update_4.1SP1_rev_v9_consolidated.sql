﻿--=================================================================================================
-- START AD-15683 : Tricoci - Error Exporting adhoc Pending Grad report to Excel
-- changing the query of totalAidDue to match the projected amount in ledger
--=================================================================================================
DECLARE @Error AS INT;
SET @Error = 0;
BEGIN TRANSACTION TotalAidDueAdhoc;
BEGIN TRY
    DECLARE @tdFldId INT;
    SET @tdFldId = (
                   SELECT FldId
                   FROM   syFields
                   WHERE  FldName = 'TotalAidDue'
                   );
    UPDATE syFieldCalculation
    SET    CalculationSql = ' FORMAT(( SELECT COALESCE(SUM(Amount), 0)  FROM  faStudentAwards INNER JOIN faStudentAwardSchedule SAS ON SAS.StudentAwardId = faStudentAwards.StudentAwardId  WHERE   faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId AND NOT EXISTS ( SELECT 1 FROM   saPmtDisbRel WHERE  AwardScheduleId = SAS.AwardScheduleId AND SAS.Amount <= ( SELECT   SUM(Amount) FROM     saPmtDisbRel WHERE    AwardScheduleId = SAS.AwardScheduleId GROUP BY AwardScheduleId )) ) - ( SELECT     COALESCE(SUM(SP.Amount), 0) FROM  faStudentAwardSchedule SAS INNER JOIN saPmtDisbRel SP ON SP.AwardScheduleId = SAS.AwardScheduleId INNER JOIN faStudentAwards  ON faStudentAwards.StudentAwardId = SAS.StudentAwardId INNER JOIN saTransactions TR ON TR.TransactionId = SP.TransactionId WHERE faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId AND TR.Voided = 0 AND SAS.Amount > ( SELECT   SUM(Amount) FROM     saPmtDisbRel  WHERE    AwardScheduleId = SAS.AwardScheduleId  GROUP BY AwardScheduleId ) ),''C'',''en-us'') AS TotalAidDue '
    WHERE  FldId = @tdFldId;
	DECLARE @tdFldIdR INT;
	SET @tdFldIdR = (
                   SELECT FldId
                   FROM   syFields
                   WHERE  FldName = 'TotalAidReceived'
                   );
    UPDATE syFieldCalculation SET CalculationSql = ' FORMAT(((   SELECT ( ISNULL(SUM(ISNULL(A.recAmt, 0)), 0) * -1 ) AS ReceivedAmt FROM   (   SELECT   ISNULL(SUM(tr.TransAmount), 0) AS recAmt FROM     dbo.faStudentAwards  INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId  INNER JOIN dbo.saTransactions tr ON tr.TransactionId = dbo.saPmtDisbRel.TransactionId WHERE    dbo.faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId GROUP BY faStudentAwards.StudentAwardId ) AS A )    - (   SELECT ISNULL(SUM(refAmt), 0)  FROM   (   SELECT   RefundAmount AS refAmt FROM     dbo.faStudentAwards  INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId   INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId INNER JOIN saRefunds ON saRefunds.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId   WHERE    faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId GROUP BY faStudentAwards.StudentAwardId ,  saRefunds.RefundAmount ) AS RefundMoney )) ,''C'' ,''en-us'') AS TotalAidReceived '  
WHERE FldId = @tdFldIdR
END TRY
BEGIN CATCH
    SET @Error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN
        ROLLBACK TRANSACTION TotalAidDueAdhoc;
        PRINT 'Failed transcation TotalAidDueAdhoc.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION TotalAidDueAdhoc;
        PRINT 'successful transaction TotalAidDueAdhoc.';
    END;
GO
--=================================================================================================
-- END AD-15683 : Tricoci - Error Exporting adhoc Pending Grad report to Excel
-- changing the query of totalAidDue to match the projected amount in ledger
--=================================================================================================
--=================================================================================================
-- START AD-15430 ArResults Backfill script for transcript report component grades - Run after code changes
--=================================================================================================
DECLARE @Error AS INT;
SET @Error = 0;
BEGIN TRANSACTION ArResults15430;
BEGIN TRY
	BEGIN
		UPDATE dbo.arGrdBkResults
		SET ModDate = DATEADD(second, 5, a.ModDate)
		FROM       arResults a
		JOIN       dbo.arStuEnrollments ON arStuEnrollments.StuEnrollId = a.StuEnrollId
		INNER JOIN dbo.syStatusCodes statusCodes ON statusCodes.StatusCodeId = dbo.arStuEnrollments.StatusCodeId
		INNER JOIN dbo.arGrdBkResults ON (arGrdBkResults.StuEnrollId = arStuEnrollments.StuEnrollId AND a.TestId = ClsSectionId)
		WHERE      a.Score IS NULL
				   AND EXISTS (
							  SELECT *
							  FROM   arGrdBkResults b
							  WHERE  b.ClsSectionId = a.TestId
									 AND b.StuEnrollId = a.StuEnrollId
									 AND b.PostDate IS NOT NULL
									 AND b.Score IS NOT NULL
							  )
				   AND statusCodes.SysStatusId NOT IN ( 8, 12, 14, 19 );

		UPDATE arResults
		SET    IsCourseCompleted = 1
		FROM dbo.arResults
		JOIN dbo.arStuEnrollments ON arStuEnrollments.StuEnrollId = arResults.StuEnrollId
		JOIN dbo.arPrgVersions ON arPrgVersions.PrgVerId = arStuEnrollments.PrgVerId
		WHERE  Score IS NOT NULL
			   AND IsCourseCompleted = 0 AND ProgramRegistrationType = 1;
	END;
END TRY
BEGIN CATCH
    SET @Error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
    BEGIN
        ROLLBACK TRANSACTION ArResults15430;
        PRINT 'Failed transaction ArResults15430.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION ArResults15430;
        PRINT 'Successful transaction ArResults15430.';
    END;
GO
--=================================================================================================
-- END AD-15430 ArResults Backfill script for transcript report component grades - Run after code changes
--=================================================================================================
--=================================================================================================
-- START AD-15819 Post services - Support schools without scores
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION Trans15819;
BEGIN TRY
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syConfigAppSettings
                  WHERE  KeyName = 'ClinicServicesScoresEnabled'
                  )
        BEGIN
            DECLARE @settingId INT;
            DECLARE @modDate DATETIME;
            SET @modDate = GETDATE();

            SET @settingId = (
                             SELECT MAX(SettingId) + 1
                             FROM   dbo.syConfigAppSetValues
                             );

            SET IDENTITY_INSERT dbo.syConfigAppSettings ON;

            INSERT INTO syConfigAppSettings (
                                            KeyName
                                           ,Description
                                           ,ModUser
                                           ,ModDate
                                           ,CampusSpecific
                                           ,ExtraConfirmation
                                           ,SettingId
                                            )
            VALUES ( 'ClinicServicesScoresEnabled'                                          -- KeyName - varchar(200)
                    ,'Yes/No campus specific value to enable clinic service score posting.' -- Description - varchar(1000)
                    ,'Support'                                                              -- ModUser - varchar(50)
                    ,@modDate                                                               -- ModDate - datetime
                    ,1                                                                      -- CampusSpecific - bit
                    ,0, @settingId                                                          -- ExtraConfirmation - bit
                );

            SET IDENTITY_INSERT dbo.syConfigAppSettings OFF;

            SET @settingId = (
                             SELECT SettingId
                             FROM   syConfigAppSettings
                             WHERE  KeyName = 'ClinicServicesScoresEnabled'
                             );

            INSERT INTO syConfigAppSet_Lookup (
                                              LookUpId
                                             ,SettingId
                                             ,ValueOptions
                                             ,ModUser
                                             ,ModDate
                                              )
            VALUES ( NEWID()    -- LookUpId - uniqueidentifier
                    ,@settingId -- SettingId - int
                    ,'No'       -- ValueOptions - varchar(50)
                    ,'Support'  -- ModUser - varchar(50)
                    ,@modDate   -- ModDate - datetime
                );
            INSERT INTO syConfigAppSet_Lookup (
                                              LookUpId
                                             ,SettingId
                                             ,ValueOptions
                                             ,ModUser
                                             ,ModDate
                                              )
            VALUES ( NEWID()    -- LookUpId - uniqueidentifier
                    ,@settingId -- SettingId - int
                    ,'Yes'      -- ValueOptions - varchar(50)
                    ,'Support'  -- ModUser - varchar(50)
                    ,@modDate   -- ModDate - datetime
                );
            INSERT INTO syConfigAppSetValues (
                                             ValueId
                                            ,SettingId
                                            ,CampusId
                                            ,Value
                                            ,ModUser
                                            ,ModDate
                                            ,Active
                                             )
            VALUES ( NEWID()    -- ValueId - uniqueidentifier
                    ,@settingId -- SettingId - int
                    ,NULL       -- CampusId - uniqueidentifier
                    ,'Yes'       -- Value - varchar(1000)
                    ,'Support'  -- ModUser - varchar(50)
                    ,@modDate   -- ModDate - datetime
                    ,1          -- Active - bit
                );
        END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION Trans15819;
        PRINT 'Failed transcation Trans15819.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION Trans15819;
        PRINT 'successful transaction Trans15819.';
    END;
GO

--=================================================================================================
-- END AD-15819 Post services - Support schools without scores
--=================================================================================================
--========================================================================================================================
-- AD-12878 : Allow AdHoc report to specify if a column is required (inner join) or not (left join) on a query.
--Insert a record to SyConfigSettings for adding 'ShowBlankFieldsOptionInAdhoc' key to Manage Configuration Settings page
--========================================================================================================================
UPDATE dbo.syUserResources SET useLeftJoin = 0 
DECLARE @KeyName VARCHAR(50) = 'ShowBlankFieldsOptionInAdhoc';
DECLARE @KeyDescription VARCHAR(300)
    = 'Values can be set to Yes or No. If value is set to Yes, it will display the checkBox in Set up adhoc Report page.';

IF NOT EXISTS (SELECT * FROM syConfigAppSettings WHERE KeyName = @KeyName)
BEGIN TRY
    BEGIN
        BEGIN TRANSACTION ShowBlankFields;

        DECLARE @MaxId INT;

        INSERT INTO dbo.syConfigAppSettings
        (
            KeyName,
            Description,
            ModUser,
            ModDate,
            CampusSpecific,
            ExtraConfirmation
        )
        VALUES
        (   @KeyName,        -- KeyName - varchar(200)
            @KeyDescription, -- Description - varchar(1000)
            'Support',       -- ModUser - varchar(50)
            GETDATE(),       -- ModDate - datetime
            1,               -- CampusSpecific - bit
            0                -- ExtraConfirmation - bit
            );

        SET @MaxId =
        (
            SELECT TOP (1)
                   SettingId
            FROM syConfigAppSettings
            WHERE KeyName = @KeyName
            ORDER BY KeyName
        );



        INSERT INTO dbo.syConfigAppSetValues
        (
            ValueId,
            SettingId,
            CampusId,
            Value,
            ModUser,
            ModDate,
            Active
        )
        VALUES
        (   NEWID(),   -- ValueId - uniqueidentifier
            @MaxId,    -- SettingId - int
            NULL,      -- CampusId - uniqueidentifier
            'No',   -- Value - varchar(1000)
            'Support', -- ModUser - varchar(50)
            GETDATE(), -- ModDate - datetime
            1          -- Active - bit
            );

        INSERT INTO dbo.syConfigAppSet_Lookup
        (
            LookUpId,
            SettingId,
            ValueOptions,
            ModUser,
            ModDate
        )
        VALUES
        (   NEWID(),   -- LookUpId - uniqueidentifier
            @MaxId,    -- SettingId - int
            'No',   -- ValueOptions - varchar(50)
            'Support', -- ModUser - varchar(50)
            GETDATE()  -- ModDate - datetime
            );

        INSERT INTO dbo.syConfigAppSet_Lookup
        (
            LookUpId,
            SettingId,
            ValueOptions,
            ModUser,
            ModDate
        )
        VALUES
        (   NEWID(),   -- LookUpId - uniqueidentifier
            @MaxId,    -- SettingId - int
            'Yes',    -- ValueOptions - varchar(50)
            'Support', -- ModUser - varchar(50)
            GETDATE()  -- ModDate - datetime
            );

    END;
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
    BEGIN
        ROLLBACK TRANSACTION ShowBlankFields;
        PRINT 'Exception Ocurred!';
    END;
END CATCH;

IF @@TRANCOUNT > 0
BEGIN
    COMMIT TRANSACTION ShowBlankFields;
END;

--===============================================================================================================================
-- END --  AD-12878 : Allow AdHoc report to specify if a column is required (inner join) or not (left join) on a query.
--Insert a record to SyConfigSettings for adding 'ShowBlankFieldsOptionInAdhoc' key to Manage Configuration Settings page
--================================================================================================