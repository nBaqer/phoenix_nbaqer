﻿/*
Run this script on:

        dev-com-db1\Adv.AvedaLive    -  This database will be modified

to synchronize it with a database with the schema represented by:

        Source

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.7.10021 from Red Gate Software Ltd at 8/28/2019 4:18:28 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
/*
* Use this Pre-Deployment script to perform tasks before the deployment of the project.
* Read more at https://www.red-gate.com/SOC7/pre-deployment-script-help
*/
UPDATE dbo.arClsSectMeetings
SET PeriodId = NULL
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)

DELETE FROM syPeriodsWorkDays
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[Trigger_UpdateProgressOfCourses] from [dbo].[arGrdBkResults]'
GO
IF OBJECT_ID(N'[dbo].[Trigger_UpdateProgressOfCourses]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[Trigger_UpdateProgressOfCourses]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[TR_InsertCreditSummary] from [dbo].[arResults]'
GO
IF OBJECT_ID(N'[dbo].[TR_InsertCreditSummary]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[TR_InsertCreditSummary]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_EnrollLead]'
GO
IF OBJECT_ID(N'[dbo].[USP_EnrollLead]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_EnrollLead]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_PR_Main_DBConfigs]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Main_DBConfigs]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_PR_Main_DBConfigs]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_AT_Step05_InsertAttendance_Day_Minutes]'
GO
IF OBJECT_ID(N'[dbo].[USP_AT_Step05_InsertAttendance_Day_Minutes]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_AT_Step05_InsertAttendance_Day_Minutes]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]'
GO
IF OBJECT_ID(N'[dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_AT_Step04_InsertAttendance_Class_MinutesClockHours]'
GO
IF OBJECT_ID(N'[dbo].[USP_AT_Step04_InsertAttendance_Class_MinutesClockHours]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_AT_Step04_InsertAttendance_Class_MinutesClockHours]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_AT_Step03_InsertAttendance_Class_PresentAbsent]'
GO
IF OBJECT_ID(N'[dbo].[USP_AT_Step03_InsertAttendance_Class_PresentAbsent]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_AT_Step03_InsertAttendance_Class_PresentAbsent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_TR_Sub03_PrepareGPA]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub03_PrepareGPA]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_TR_Sub03_PrepareGPA]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_Update_SyCreditsSummary]'
GO
IF OBJECT_ID(N'[dbo].[Usp_Update_SyCreditsSummary]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_Update_SyCreditsSummary]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_UpdateTransferCredits_SyCreditsSummary]'
GO
IF OBJECT_ID(N'[dbo].[Usp_UpdateTransferCredits_SyCreditsSummary]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_UpdateTransferCredits_SyCreditsSummary]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_AT_Step06_InsertAttendance_Class_ClockHour]'
GO
IF OBJECT_ID(N'[dbo].[USP_AT_Step06_InsertAttendance_Class_ClockHour]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_AT_Step06_InsertAttendance_Class_ClockHour]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[usp_AR_ArGrdBkResults_Insert]'
GO
IF OBJECT_ID(N'[dbo].[usp_AR_ArGrdBkResults_Insert]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[usp_AR_ArGrdBkResults_Insert]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[Trigger_UpdateProgressOfCourses] on [dbo].[arGrdBkResults]'
GO
IF OBJECT_ID(N'[dbo].[Trigger_UpdateProgressOfCourses]', 'TR') IS NULL
EXEC sp_executesql N'
CREATE   TRIGGER [dbo].[Trigger_UpdateProgressOfCourses]
ON [dbo].[arGrdBkResults]
FOR INSERT, UPDATE
AS
SET NOCOUNT ON;

    BEGIN
        DECLARE @Error AS INTEGER;

        SET @Error = 0;

        BEGIN TRANSACTION FixCoursesScore;
        BEGIN TRY

            DECLARE @CourseResultsToUpdate AS TABLE
                (
                    FinalScore DECIMAL(18, 2) NULL
                   ,NumberOfComponents INT NULL
                   ,NumberOfComponentsScored INT NULL
                   ,ClsSectionId UNIQUEIDENTIFIER NULL
                   ,StuEnrollId UNIQUEIDENTIFIER NULL
                );

            DECLARE @GradeRoundingTableByEnrollment AS TABLE
                (
                    StuEnrollId UNIQUEIDENTIFIER
                   ,GradeRounding VARCHAR(10)
                );

            INSERT INTO @GradeRoundingTableByEnrollment (
                                                        StuEnrollId
                                                       ,GradeRounding
                                                        )
                        SELECT StuEnrollId
                              ,LOWER(ISNULL(dbo.GetAppSettingValueByKeyName(''GradeRounding'', CampusId), ''''))
                        FROM   dbo.arStuEnrollments E
                        WHERE  E.StuEnrollId IN (
                                                SELECT Inserted.StuEnrollId
                                                FROM   Inserted
                                                );


            INSERT INTO @CourseResultsToUpdate (
                                               FinalScore
                                              ,NumberOfComponents
                                              ,NumberOfComponentsScored
                                              ,ClsSectionId
                                              ,StuEnrollId
                                               )
                        SELECT     CASE WHEN grdRounding.GradeRounding = ''yes'' THEN
                                            ROUND(dbo.CalculateStudentAverage(enrollments.StuEnrollId, NULL, NULL, classSections.ClsSectionId, NULL), 2)
                                        ELSE dbo.CalculateStudentAverage(enrollments.StuEnrollId, NULL, NULL, classSections.ClsSectionId, NULL)
                                   END AS FinalScore
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   (
                                          SELECT   StuEnrollId
                                          FROM     dbo.arGrdBkResults
                                          WHERE    StuEnrollId = enrollments.StuEnrollId
                                                   AND ClsSectionId = classSections.ClsSectionId
                                          GROUP BY StuEnrollId
                                                  ,ClsSectionId
                                                  ,InstrGrdBkWgtDetailId
                                          ) components
                                   ) AS NumberOfComponents
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   (
                                          SELECT   StuEnrollId
                                          FROM     dbo.arGrdBkResults
                                          WHERE    StuEnrollId = enrollments.StuEnrollId
                                                   AND ClsSectionId = classSections.ClsSectionId
                                                   AND Score IS NOT NULL
                                                   AND PostDate IS NOT NULL
                                          GROUP BY StuEnrollId
                                                  ,ClsSectionId
                                                  ,InstrGrdBkWgtDetailId
                                          ) components
                                   ) AS NumberOfComponentsScored
                                  ,classSections.ClsSectionId
                                  ,courseResults.StuEnrollId
                        FROM       dbo.arResults courseResults
                        INNER JOIN dbo.arStuEnrollments enrollments ON enrollments.StuEnrollId = courseResults.StuEnrollId
                        INNER JOIN dbo.syStatusCodes statusCodes ON statusCodes.StatusCodeId = enrollments.StatusCodeId
                        INNER JOIN dbo.arClassSections classSections ON classSections.ClsSectionId = courseResults.TestId
                        INNER JOIN dbo.arGrdBkResults gradebookResults ON gradebookResults.StuEnrollId = courseResults.StuEnrollId
                                                                          AND gradebookResults.ClsSectionId = classSections.ClsSectionId
                        INNER JOIN dbo.arGrdBkWgtDetails gradebookWeight ON gradebookWeight.InstrGrdBkWgtDetailId = gradebookResults.InstrGrdBkWgtDetailId
                        INNER JOIN dbo.arReqs courses ON courses.ReqId = classSections.ReqId
                        INNER JOIN @GradeRoundingTableByEnrollment grdRounding ON grdRounding.StuEnrollId = courseResults.StuEnrollId
                        INNER JOIN Inserted I ON (
                                                 I.StuEnrollId = courseResults.StuEnrollId
                                                 AND I.ClsSectionId = courseResults.TestId
                                                 )
                        ORDER BY   courseResults.Score;

            UPDATE     courses
            SET        courses.Score = coursesToUpdate.FinalScore
                      ,courses.IsInComplete = CASE WHEN coursesToUpdate.NumberOfComponents = coursesToUpdate.NumberOfComponentsScored THEN 0
                                                   ELSE 1
                                              END
                      ,courses.DateCompleted = CASE WHEN coursesToUpdate.NumberOfComponents = coursesToUpdate.NumberOfComponentsScored THEN GETDATE()
                                                    ELSE courses.DateCompleted
                                               END
                      ,courses.ModDate = GETDATE()
                      ,courses.IsCourseCompleted = CASE WHEN coursesToUpdate.NumberOfComponents = coursesToUpdate.NumberOfComponentsScored THEN 1
                                                        ELSE 0
                                                   END
                      ,courses.GrdSysDetailId = (
                                                SELECT     TOP ( 1 ) gradeDetails.GrdSysDetailId
                                                FROM       dbo.arStuEnrollments enrollments
                                                INNER JOIN dbo.arPrgVersions programVersion ON programVersion.PrgVerId = enrollments.PrgVerId
                                                INNER JOIN dbo.arGradeSystemDetails gradeDetails ON gradeDetails.GrdSystemId = programVersion.GrdSystemId
                                                INNER JOIN dbo.arGradeScaleDetails gradeScale ON gradeScale.GrdSysDetailId = gradeDetails.GrdSysDetailId
                                                WHERE      enrollments.StuEnrollId = courses.StuEnrollId
                                                           AND gradeScale.MaxVal >= coursesToUpdate.FinalScore
                                                           AND gradeScale.MinVal <= coursesToUpdate.FinalScore
                                                ORDER BY   gradeDetails.Grade
                                                )
            FROM       dbo.arResults courses
            INNER JOIN @CourseResultsToUpdate coursesToUpdate ON coursesToUpdate.ClsSectionId = courses.TestId
                                                                 AND coursesToUpdate.StuEnrollId = courses.StuEnrollId
            INNER JOIN dbo.arStuEnrollments enrollments ON enrollments.StuEnrollId = courses.StuEnrollId
            INNER JOIN dbo.arPrgVersions programVersion ON programVersion.PrgVerId = enrollments.PrgVerId
            WHERE      programVersion.ProgramRegistrationType = 1
                       AND courses.IsGradeOverridden = 0;

            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;

        END TRY
        BEGIN CATCH
            DECLARE @msg NVARCHAR(MAX);
            DECLARE @severity INT;
            DECLARE @state INT;
            SELECT @msg = ERROR_MESSAGE()
                  ,@severity = ERROR_SEVERITY()
                  ,@state = ERROR_STATE();
            RAISERROR(@msg, @severity, @state);
            SET @Error = 1;
        END CATCH;

        IF ( @Error = 0 )
            BEGIN
                COMMIT TRANSACTION FixCoursesScore;

            END;
        ELSE
            BEGIN
                ROLLBACK TRANSACTION FixCoursesScore;
            END;

    END;


SET NOCOUNT OFF;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[TR_InsertCreditSummary] on [dbo].[arResults]'
GO
IF OBJECT_ID(N'[dbo].[TR_InsertCreditSummary]', 'TR') IS NULL
EXEC sp_executesql N'---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


--==========================================================================================
-- TRIGGER TR_InsertCreditSummary
-- AFTER UPDATE  
--==========================================================================================
CREATE TRIGGER [dbo].[TR_InsertCreditSummary] ON [dbo].[arResults]
    AFTER INSERT,UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @PrgVerId UNIQUEIDENTIFIER
       ,@rownumber INT
       ,@StuEnrollId UNIQUEIDENTIFIER;
    DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
       ,@PrevReqId UNIQUEIDENTIFIER
       ,@PrevTermId UNIQUEIDENTIFIER
       ,@CreditsAttempted DECIMAL(18,2)
       ,@CreditsEarned DECIMAL(18,2)
       ,@TermId UNIQUEIDENTIFIER
       ,@TermDescrip VARCHAR(50);
    DECLARE @reqid UNIQUEIDENTIFIER
       ,@CourseCodeDescrip VARCHAR(50)
       ,@FinalGrade UNIQUEIDENTIFIER
       ,@FinalScore DECIMAL(18,2)
       ,@ClsSectionId UNIQUEIDENTIFIER
       ,@Grade VARCHAR(50)
       ,@IsGradeBookNotSatisified BIT
       ,@TermStartDate DATETIME;
    DECLARE @IsPass BIT
       ,@IsCreditsAttempted BIT
       ,@IsCreditsEarned BIT
       ,@Completed BIT
       ,@CurrentScore DECIMAL(18,2)
       ,@CurrentGrade VARCHAR(10)
       ,@FinalGradeDesc VARCHAR(50)
       ,@FinalGPA DECIMAL(18,2)
       ,@GrdBkResultId UNIQUEIDENTIFIER;
    DECLARE @Product_WeightedAverage_Credits_GPA DECIMAL(18,2)
       ,@Count_WeightedAverage_Credits DECIMAL(18,2)
       ,@Product_SimpleAverage_Credits_GPA DECIMAL(18,2)
       ,@Count_SimpleAverage_Credits DECIMAL(18,2);
    DECLARE @CreditsPerService DECIMAL(18,2)
       ,@NumberOfServicesAttempted INT
       ,@boolCourseHasLabWorkOrLabHours INT
       ,@sysComponentTypeId INT
       ,@RowCount INT;
    DECLARE @decGPALoop DECIMAL(18,2)
       ,@intCourseCount INT
       ,@decWeightedGPALoop DECIMAL(18,2)
       ,@IsInGPA BIT
       ,@isGradeEligibleForCreditsEarned BIT
       ,@isGradeEligibleForCreditsAttempted BIT;
    DECLARE @ComputedSimpleGPA DECIMAL(18,2)
       ,@ComputedWeightedGPA DECIMAL(18,2)
       ,@CourseCredits DECIMAL(18,2);
    DECLARE @FinAidCreditsEarned DECIMAL(18,2)
       ,@FinAidCredits DECIMAL(18,2)
       ,@TermAverage DECIMAL(18,2)
       ,@TermAverageCount INT
	   ,@IsResultCompleted bit
    DECLARE @IsWeighted INT
       ,@StuEnrollCampusId UNIQUEIDENTIFIER;
    SET @decGPALoop = 0;
    SET @intCourseCount = 0;
    SET @decWeightedGPALoop = 0;
    SET @ComputedSimpleGPA = 0;
    SET @ComputedWeightedGPA = 0;
    SET @CourseCredits = 0;

    SET @StuEnrollId = (
                         SELECT TOP 1
                                StuEnrollId
                         FROM   INSERTED
                       );

--DELETE FROM tmpCredits

--INSERT INTO tmpCredits SELECT DISTINCT ResultId FROM INSERTED 
    DECLARE GetCreditsSummary_Cursor CURSOR
    FOR
        SELECT	DISTINCT
                SE.StuEnrollId
               ,T.TermId
               ,T.TermDescrip
               ,T.StartDate
               ,R.ReqId
               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
               ,RES.Score AS FinalScore
               ,RES.GrdSysDetailId AS FinalGrade
               ,GCT.SysComponentTypeId
               ,R.Credits AS CreditsAttempted
               ,CS.ClsSectionId
               ,GSD.Grade
               ,GSD.IsPass
               ,GSD.IsCreditsAttempted
               ,GSD.IsCreditsEarned
               ,SE.PrgVerId
               ,GSD.IsInGPA
               ,R.FinAidCredits AS FinAidCredits
			   ,RES.IsCourseCompleted
        FROM    arStuEnrollments SE
        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
        INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
        INNER JOIN INSERTED t1 ON t1.ResultId = RES.ResultId
        INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
        INNER JOIN arTerm T ON CS.TermId = T.TermId
        INNER JOIN arReqs R ON CS.ReqId = R.ReqId
        LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
        LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
        LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
        LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
				--where SE.StuEnrollId=''00EB9C62-CAFB-4D5B-9346-34F5DD16FFC2''
        ORDER BY T.StartDate
               ,T.TermDescrip
               ,R.ReqId; 
    OPEN GetCreditsSummary_Cursor;
    SET @PrevStuEnrollId = NULL;
    SET @PrevTermId = NULL;
    SET @PrevReqId = NULL;
    SET @RowCount = 0;
    FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@TermStartDate,@reqid,@CourseCodeDescrip,@FinalScore,@FinalGrade,
        @sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,@FinAidCredits, @IsResultCompleted;
--,@GrdBkResultId
    WHILE @@FETCH_STATUS = 0
        BEGIN
	
            SET @CourseCredits = @CreditsAttempted; 
            SET @RowCount = @RowCount + 1;
				
				-- modified by Janet on 11/4/2011 get campusid from arStuEnrollments
            SET @StuEnrollCampusId = COALESCE((
                                                SELECT  CampusId
                                                FROM    arStuEnrollments
                                                WHERE   StuEnrollId = @StuEnrollId
                                              ),NULL);
				
				-- Changes made on 12/28/2010 starts here
            DECLARE @ShowROSSOnlyTabsForStudent_Value BIT
               ,@SetGradeBookAt VARCHAR(50)
               ,@GradesFormat VARCHAR(50);
            SET @ShowROSSOnlyTabsForStudent_Value = (
                                                      SELECT    dbo.GetAppSettingValue(68,@StuEnrollCampusId)
                                                    );
            SET @SetGradeBookAt = (
                                    SELECT  dbo.GetAppSettingValue(43,@StuEnrollCampusId)
                                  );
            SET @GradesFormat = (
                                  SELECT    dbo.GetAppSettingValue(47,@StuEnrollCampusId)
                                );
								
            SET @FinalGradeDesc = @FinalGrade;
				-- Changes made on 12/28/2010 ends here

				
				-- If the output is greater than or equal to 1 there are some grade books not satisfied
				--Modified by Balaji on 11/8/2010
            SET @IsGradeBookNotSatisified = (
                                              SELECT    COUNT(*) AS UnsatisfiedWorkUnits
                                              FROM      (
                                                          SELECT DISTINCT
                                                                    D.*
                                                                   ,
															--Case When D.MinimumScore > D.Score Then (D.MinimumScore-D.Score) else 0 end as Remaining,
															--Case When (D.MinimumScore > D.Score) And (D.MustPass=1) then 0 
															--When (SysComponentTypeId=500 OR SysComponentTypeId=503 OR SysComponentTypeId=544) AND (@ShowROSSOnlyTabsForStudent_Value=0) and (D.Score is not null) and (D.MinimumScore > D.Score) then 0
															---- When (@ShowROSSOnlyTabsForStudent_Value=0) and (D.Score is not null) and (D.MinimumScore > D.Score) then 0
															----When (@ShowROSSOnlyTabsForStudent_Value=1) and (D.Score is not null) and (D.MinimumScore > D.Score) and D.Required=1 AND D.MustPass=0 AND (@sysComponentTypeId = 500 OR @sysComponentTypeId=503 OR @sysComponentTypeId=544) then 0
															----When (@ShowROSSOnlyTabsForStudent_Value=1) and (D.Score is not null) and (D.MinimumScore > D.Score) and D.Required=1 AND D.MustPass=1 then 0
															--When @ShowROSSOnlyTabsForStudent_Value=1 and D.Score is null and D.Required=1 then 0
															--When D.Score is Null and D.FinalScore is null And (D.Required=1) then 0
															--else 1 end as IsWorkUnitSatisfied
															-- Changes made on 12/28/2010 starts here
                                                                    CASE WHEN D.MinimumScore > D.Score THEN ( D.MinimumScore - D.Score )
                                                                         ELSE 0
                                                                    END AS Remaining
                                                                   ,CASE WHEN ( D.MinimumScore > D.Score )
                                                                              AND ( D.MustPass = 1 ) THEN 0
                                                                         WHEN (
                                                                                SysComponentTypeId = 500
                                                                                OR SysComponentTypeId = 503
                                                                                OR SysComponentTypeId = 544
                                                                              )
                                                                              AND ( @ShowROSSOnlyTabsForStudent_Value = 0 )
                                                                              AND ( D.Score IS NOT NULL )
                                                                              AND ( D.MinimumScore > D.Score ) THEN 0
															--When (@ShowROSSOnlyTabsForStudent_Value=0) and (D.Score is not null) and (D.MinimumScore > D.Score) then 0
															--When (@ShowROSSOnlyTabsForStudent_Value=1) and (D.Score is not null) and (D.MinimumScore > D.Score) and D.Required=1 then 0
															--Non Ross/ Course Level/ No LabHrLabWorkExternship/Numeric/FinalScore is not posted
                                                                         WHEN @ShowROSSOnlyTabsForStudent_Value = 0
                                                                              AND LOWER(LTRIM(RTRIM(@SetGradeBookAt))) = ''courselevel''
                                                                              AND LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
                                                                              AND (
                                                                                    SysComponentTypeId IS NULL
                                                                                    OR (
                                                                                         SysComponentTypeId <> 500
                                                                                         AND SysComponentTypeId <> 503
                                                                                         AND SysComponentTypeId <> 544
                                                                                       )
                                                                                  )
                                                                              AND @FinalScore IS NULL THEN 0
															--Non Ross/ Course Level/ No LabHrLabWorkExternship/Letter/Final Grade is not posted
                                                                         WHEN @ShowROSSOnlyTabsForStudent_Value = 0
                                                                              AND LOWER(LTRIM(RTRIM(@SetGradeBookAt))) = ''courselevel''
                                                                              AND LOWER(LTRIM(RTRIM(@GradesFormat))) = ''letter''
                                                                              AND (
                                                                                    SysComponentTypeId IS NULL
                                                                                    OR (
                           SysComponentTypeId <> 500
                                                                                         AND SysComponentTypeId <> 503
                                                                                         AND SysComponentTypeId <> 544
                                                                                       )
                                                                                  )
                                                                              AND @FinalGradeDesc IS NULL THEN 0
                                                                         WHEN @ShowROSSOnlyTabsForStudent_Value = 1
                                                                              AND D.Score IS NULL
                                                                              AND D.Required = 1 THEN 0
                                                                         WHEN D.Score IS NULL
                                                                              AND D.FinalScore IS NULL
                                                                              AND ( D.Required = 1 ) THEN 0
                                                                         ELSE 1
                                                                    END AS IsWorkUnitSatisfied
															-- Changes made on 12/28/2010 ends here
                                                          FROM      (
                                                                      SELECT	DISTINCT
                                                                                T.TermId
                                                                               ,T.TermDescrip
                                                                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                                                               ,R.ReqId
                                                                               ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                                               ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,544 ) THEN GBWD.Number
                                                                                       ELSE (
                                                                                              SELECT    MIN(MinVal)
                                                                                              FROM      arGradeScaleDetails GSD
                                                                                                       ,arGradeSystemDetails GSS
                                                                                              WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                        AND GSS.IsPass = 1
                                                                                            )
                                                                                  END ) AS MinimumScore
                                                                               ,
                														--GBR.Score as Score,  
                                                                                GBWD.Weight AS Weight
                                                                               ,RES.Score AS FinalScore
                                                                               ,RES.GrdSysDetailId AS FinalGrade
                                                                               ,GBWD.Required
                                                                               ,GBWD.MustPass
                                                                               ,GBWD.GrdPolicyId
                                                      ,( CASE GCT.SysComponentTypeId
                                                                                    WHEN 544 THEN (
                                                                                                    SELECT  SUM(HoursAttended)
                                                                                                    FROM    arExternshipAttendance
                                                                                                    WHERE   StuEnrollId = SE.StuEnrollId
                                                                                                  )
                                                                                    WHEN 503
                                                                                    THEN (
                                                                                           SELECT   SUM(Score)
                                                                                           FROM     arGrdBkResults
                                                                                           WHERE    StuEnrollId = SE.StuEnrollId
                                                                                                    AND ClsSectionId = CS.ClsSectionId
                                                                                                    AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                                         )
                                                                                    ELSE GBR.Score
                                                                                  END ) AS Score
                                                                               ,GCT.SysComponentTypeId
                                                                               ,SE.StuEnrollId
                                                                               ,
																		--GBR.GrdBkResultId,
                                                                                R.Credits AS CreditsAttempted
                                                                               ,CS.ClsSectionId
                                                                               ,GSD.Grade
                                                                               ,GSD.IsPass
                                                                               ,GSD.IsCreditsAttempted
                                                                               ,GSD.IsCreditsEarned
                                                                      FROM      arStuEnrollments SE
                                                                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                                      INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                                                                      INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                                                                      INNER JOIN arTerm T ON CS.TermId = T.TermId
                                                                      INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                                                      LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                                                                                      AND GBR.StuEnrollId = SE.StuEnrollId
                                                                      LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                      LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                      LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                                                      WHERE     SE.StuEnrollId = @StuEnrollId
                                                                                AND T.TermId = @TermId
                                                                                AND R.ReqId = @reqid 
																			--and GBR.ResNum>=1
                                                                    ) D
                                                        ) E
                                              WHERE     IsWorkUnitSatisfied = 0
                                            ); 
				
            SET @IsWeighted = (
                                SELECT  COUNT(*) AS WeightsCount
                                FROM    (
                                          SELECT    T.TermId
                                                   ,T.TermDescrip
                                                   ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                                   ,R.ReqId
                                                   ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                   ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,544 ) THEN GBWD.Number
                                                           ELSE (
                                                                  SELECT    MIN(MinVal)
                                                                  FROM      arGradeScaleDetails GSD
                                                                           ,arGradeSystemDetails GSS
                                                                  WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                            AND GSS.IsPass = 1
                                                                )
                                                      END ) AS MinimumScore
                                                   ,GBR.Score AS Score
                                                   ,GBWD.Weight AS Weight
                                                   ,RES.Score AS FinalScore
                                                   ,RES.GrdSysDetailId AS FinalGrade
                                                   ,GBWD.Required
                                                   ,GBWD.MustPass
                                                   ,GBWD.GrdPolicyId
                                                   ,( CASE GCT.SysComponentTypeId
                                                        WHEN 544 THEN (
                                                                        SELECT  SUM(HoursAttended)
                                                                        FROM    arExternshipAttendance
                                                                        WHERE   StuEnrollId = SE.StuEnrollId
                                                                      )
                                                        ELSE GBR.Score
                                                      END ) AS GradeBookResult
                                                   ,GCT.SysComponentTypeId
                                                   ,SE.StuEnrollId
                                                   ,GBR.GrdBkResultId
                                                   ,R.Credits AS CreditsAttempted
                                                   ,CS.ClsSectionId
                                                   ,GSD.Grade
                                                   ,GSD.IsPass
                                                   ,GSD.IsCreditsAttempted
                                   ,GSD.IsCreditsEarned
                                          FROM      arStuEnrollments SE
                                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                          INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                                          INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                                          INNER JOIN arTerm T ON CS.TermId = T.TermId
                                          INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                          LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                                                          AND GBR.StuEnrollId = SE.StuEnrollId
                                          LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                          LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                          LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                          WHERE     SE.StuEnrollId = @StuEnrollId
                                                    AND T.TermId = @TermId
                                                    AND R.ReqId = @reqid 
																			--and GBR.ResNum>=1
                                        ) D
                                WHERE   Weight >= 1
                              );
		
				
	--Check if IsCreditsAttempted is set to True
            IF (
                 @IsCreditsAttempted IS NULL
                 OR @IsCreditsAttempted = 0
               )
                BEGIN
                    SET @CreditsAttempted = 0;
                END;
            IF (
                 @IsCreditsEarned IS NULL
                 OR @IsCreditsEarned = 0
               )
                BEGIN
                    SET @CreditsEarned = 0;
                END;		
			
			
            IF ( @IsGradeBookNotSatisified >= 1 )
                BEGIN
                    IF ( @FinalScore IS NULL )
                        BEGIN
                            SET @CreditsEarned = 0;
                            SET @Completed = 0;
                        END;
                END;
            ELSE
                BEGIN
                    SET @GrdBkResultId = (
                                           SELECT TOP 1
                                                    GrdBkResultId
                                           FROM     arGrdBkResults
                                           WHERE    StuEnrollId = @StuEnrollId
                                                    AND ClsSectionId = @ClsSectionId
                                         ); 
                    IF @GrdBkResultId IS NOT NULL
                        BEGIN
                            IF ( @IsCreditsEarned = 1 )
                                BEGIN
                                    SET @CreditsEarned = @CreditsAttempted;
                                    SET @FinAidCreditsEarned = @FinAidCredits;
                                END; 
                            SET @Completed = 1;										
                        END;
						
                    IF (
                         @GrdBkResultId IS NULL
                         AND @Grade IS NOT NULL
                       )
                        BEGIN
                            IF ( @IsCreditsEarned = 1 )
                                BEGIN
                                    SET @CreditsEarned = @CreditsAttempted;
                                    SET @FinAidCreditsEarned = @FinAidCredits;
                                END; 
                            SET @Completed = 1;	
                        END;
                END;
				
				
				
            IF (
                 @FinalScore IS NOT NULL
                 AND @Grade IS NOT NULL
               )
                BEGIN
                    IF ( @IsCreditsEarned = 1 )
                        BEGIN
                            SET @CreditsEarned = @CreditsAttempted;
                            SET @FinAidCreditsEarned = @FinAidCredits;
                        END; 
                    SET @Completed = 1;
				
                END;
				
				-- If course is not part of the program version definition do not add credits earned and credits attempted
				-- set the credits earned and attempted to zero
            DECLARE @coursepartofdefinition INT;
            SET @coursepartofdefinition = 0;
				
								
            SET @coursepartofdefinition = (
                                            SELECT  COUNT(*) AS RowCountOfProgramDefinition
                                            FROM    (
                                                      SELECT    *
                                                      FROM      arProgVerDef
                                                      WHERE     PrgVerId = @PrgVerId
                                                                AND ReqId = @reqid
                                                      UNION
                                                      SELECT    *
                                                      FROM      arProgVerDef
                                                      WHERE     PrgVerId = @PrgVerId
                                                                AND ReqId IN ( SELECT   GrpId
                                                                               FROM     arReqGrpDef
                                                                               WHERE    ReqId = @reqid )
                                                    ) dt
                                          );
            IF ( @coursepartofdefinition = 0 )
                BEGIN
                    SET @CreditsEarned = 0;
                    SET @CreditsAttempted = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
		
				-- Check the grade scale associated with the class section and figure out of the final score was a passing score
            DECLARE @coursepassrowcount INT;
            SET @coursepassrowcount = 0;
            IF ( @FinalScore IS NOT NULL )
					
					-- If the student scores 56 and the score is a passing score then we consider this course as completed
                BEGIN
                    SET @coursepassrowcount = (
                                                SELECT  COUNT(t2.MinVal) AS IsCourseCompleted
                                                FROM    arClassSections t1
                                                INNER JOIN arGradeScaleDetails t2 ON t1.GrdScaleId = t2.GrdScaleId
                                                INNER JOIN arGradeSystemDetails t3 ON t2.GrdSysDetailId = t3.GrdSysDetailId
                                                WHERE   t1.ClsSectionId = @ClsSectionId
                                                        AND t3.IsPass = 1
                                                        AND @FinalScore >= t2.MinVal
                                              );
                    IF @coursepassrowcount >= 1 AND (@sysComponentTypeId IN (503,500) OR (@IsResultCompleted = 1 AND @sysComponentTypeId NOT IN (500,503)))
                        BEGIN
                            SET @Completed = 1;
                        END;
                    ELSE
                        BEGIN
                            SET @Completed = 0;
                        END;
                END;
				-- If Student Scored a Failing Grade (IsPass set to 0 in Grade System)
				-- then mark this course as Incomplete
            IF ( @FinalGrade IS NOT NULL )
                BEGIN
                    IF ( @IsPass = 0 )
                        BEGIN
                            SET @Completed = 0;
                            IF ( @IsCreditsEarned = 0 )
                                BEGIN
                                    SET @CreditsEarned = 0; 
                                    SET @FinAidCreditsEarned = 0;
                                END; 
                            IF ( @IsCreditsAttempted = 0 )
                                BEGIN
                                    SET @CreditsAttempted = 0;
                                END;
                        END;
                END;
			
            SET @CurrentScore = (
                                  SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                 ELSE NULL
                                            END AS CurrentScore
                                  FROM      (
                                              SELECT    InstrGrdBkWgtDetailId
                                                       ,Code
                                                       ,Descrip
                                                       ,Weight AS GradeBookWeight
                                                       ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                             ELSE 0
                                                        END AS ActualWeight
                                              FROM      (
                                                          SELECT    C.InstrGrdBkWgtDetailId
                                                                   ,D.Code
                                                                   ,D.Descrip
                                                                   ,ISNULL(C.Weight,0) AS Weight
                                                                   ,C.Number AS MinNumber
                                                                   ,C.GrdPolicyId
                                                                   ,C.Parameter AS Param
                                                                   ,X.GrdScaleId
                                                                   ,SUM(GR.Score) AS Score
                                                                   ,COUNT(D.Descrip) AS NumberOfComponents
                                                          FROM      (
                                                                      SELECT DISTINCT TOP 1
                                                                                A.InstrGrdBkWgtId
                                                                               ,A.EffectiveDate
                                                                               ,B.GrdScaleId
                                                                      FROM      arGrdBkWeights A
                                                                               ,arClassSections B
                                                                      WHERE     A.ReqId = B.ReqId
                                                                                AND A.EffectiveDate <= B.StartDate
                                                                                AND B.ClsSectionId = @ClsSectionId
                                                                      ORDER BY  A.EffectiveDate DESC
                                                                    ) X
                                                                   ,arGrdBkWgtDetails C
                                                                   ,arGrdComponentTypes D
                                                                   ,arGrdBkResults GR
                                                          WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                    AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                    AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                    AND D.SysComponentTypeId NOT IN ( 500,503 )
                                                                    AND GR.StuEnrollId = @StuEnrollId
                                                                    AND GR.ClsSectionId = @ClsSectionId
                                                                    AND GR.Score IS NOT NULL
                                                          GROUP BY  C.InstrGrdBkWgtDetailId
                                                                   ,D.Code
                                                                   ,D.Descrip
                                                                   ,C.Weight
                                                                   ,C.Number
                                                                   ,C.GrdPolicyId
                                                                   ,C.Parameter
                                                                   ,X.GrdScaleId
                                                        ) S
                                              GROUP BY  InstrGrdBkWgtDetailId
                                                       ,Code
                                                       ,Descrip
                                                       ,Weight
                                                       ,NumberOfComponents
                                            ) FinalTblToComputeCurrentScore
                                );
            IF ( @CurrentScore IS NULL )
                BEGIN
				-- instructor grade books
                    SET @CurrentScore = (
                                          SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                         ELSE NULL
                                                    END AS CurrentScore
                                          FROM      (
                                                      SELECT    InstrGrdBkWgtDetailId
                                                               ,Code
                                                               ,Descrip
                                                               ,Weight AS GradeBookWeight
                                                               ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                     ELSE 0
                                                                END AS ActualWeight
                                                      FROM      (
                                                                  SELECT    C.InstrGrdBkWgtDetailId
                                                                           ,D.Code
                                                                           ,D.Descrip
                                                                           ,ISNULL(C.Weight,0) AS Weight
                                                                           ,C.Number AS MinNumber
                                                                           ,C.GrdPolicyId
                                                                           ,C.Parameter AS Param
                                                                           ,X.GrdScaleId
                                                                           ,SUM(GR.Score) AS Score
                                                                           ,COUNT(D.Descrip) AS NumberOfComponents
                                                                  FROM      (
                            --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
															--FROM          arGrdBkWeights A,arClassSections B        
															--WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
															--ORDER BY      A.EffectiveDate DESC
															
															SELECT DISTINCT TOP 1
                                                                    t1.InstrGrdBkWgtId
                                                                   ,t1.GrdScaleId
                                                            FROM    arClassSections t1
                                                                   ,arGrdBkWeights t2
                                                            WHERE   t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                    AND t1.ClsSectionId = @ClsSectionId
                                                                            ) X
                                                                           ,arGrdBkWgtDetails C
                                                                           ,arGrdComponentTypes D
                                                                           ,arGrdBkResults GR
                                                                  WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                            AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                            AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                            AND
													-- D.SysComponentTypeID not in (500,503) and 
                                                                            GR.StuEnrollId = @StuEnrollId
                                                                            AND GR.ClsSectionId = @ClsSectionId
                                                                            AND GR.Score IS NOT NULL
                                                                  GROUP BY  C.InstrGrdBkWgtDetailId
                                                                           ,D.Code
                                                                           ,D.Descrip
                                                                           ,C.Weight
                                                                           ,C.Number
                                                                           ,C.GrdPolicyId
                                                                           ,C.Parameter
                                                                           ,X.GrdScaleId
                                                                ) S
                                                      GROUP BY  InstrGrdBkWgtDetailId
                                                               ,Code
                                                               ,Descrip
                                                               ,Weight
                                                               ,NumberOfComponents
                                                    ) FinalTblToComputeCurrentScore
                                        );	
			
                END;

            IF ( @CurrentScore IS NOT NULL )
                BEGIN
                    SET @CurrentGrade = (
                                          SELECT    t2.Grade
                                          FROM      arGradeScaleDetails t1
                                                   ,arGradeSystemDetails t2
                                          WHERE     t1.GrdSysDetailId = t2.GrdSysDetailId
                                                    AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                           FROM     arClassSections
                                                                           WHERE    ClsSectionId = @ClsSectionId )
                                                    AND @CurrentScore >= t1.MinVal
                                                    AND @CurrentScore <= t1.MaxVal
                                        );
						
                END;	
            ELSE
                BEGIN
                    SET @CurrentGrade = NULL;
                END;
		
			
            IF (
                 @CurrentScore IS NULL
                 AND @CurrentGrade IS NULL
                 AND @FinalScore IS NULL
                 AND @FinalGrade IS NULL
               )
                BEGIN
                    SET @Completed = 0;
                    SET @CreditsAttempted = 0;
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
			
			
            IF (
                 @FinalScore IS NOT NULL
                 OR @FinalGrade IS NOT NULL
               )
                BEGIN

                    SET @FinalGradeDesc = (
                                            SELECT  Grade
                                            FROM    arGradeSystemDetails
                                            WHERE   GrdSysDetailId = @FinalGrade
                                          );
		
                    IF ( @FinalGradeDesc IS NULL )
                        BEGIN
                            SET @FinalGradeDesc = (
                                                    SELECT  t2.Grade
                                                    FROM    arGradeScaleDetails t1
                                                           ,arGradeSystemDetails t2
                                                    WHERE   t1.GrdSysDetailId = t2.GrdSysDetailId
                                                            AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                                   FROM     arClassSections
                                                                                   WHERE    ClsSectionId = @ClsSectionId )
                                                            AND @FinalScore >= t1.MinVal
                                                            AND @FinalScore <= t1.MaxVal
                                                  );
                        END;
                    SET @FinalGPA = (
                                      SELECT    GPA
                                      FROM      arGradeSystemDetails
                                      WHERE     GrdSysDetailId = @FinalGrade
                                    );
                    IF @FinalGPA IS NULL
                        BEGIN
                            SET @FinalGPA = (
                                              SELECT    t2.GPA
                                              FROM      arGradeScaleDetails t1
                                                       ,arGradeSystemDetails t2
                                              WHERE     t1.GrdSysDetailId = t2.GrdSysDetailId
                                                        AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                               FROM     arClassSections
                                                                               WHERE    ClsSectionId = @ClsSectionId )
                                                        AND @FinalScore >= t1.MinVal
                                                        AND @FinalScore <= t1.MaxVal
                                            );
                        END;
                END;
            ELSE
                BEGIN
                    SET @FinalGradeDesc = NULL;
                    SET @FinalGPA = NULL;
                END;
			

            SET @isGradeEligibleForCreditsEarned = (
                                                     SELECT t2.IsCreditsEarned
                                                     FROM   arGradeScaleDetails t1
                                                           ,arGradeSystemDetails t2
                                                     WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                            AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                                   FROM     arClassSections
                                                                                   WHERE    ClsSectionId = @ClsSectionId )
                                                            AND t2.Grade = @FinalGradeDesc
                                                   ); 
												
            SET @isGradeEligibleForCreditsAttempted = (
                                                        SELECT  t2.IsCreditsAttempted
                                                        FROM    arGradeScaleDetails t1
                                                               ,arGradeSystemDetails t2
                                                        WHERE   t1.GrdSysDetailId = t2.GrdSysDetailId
                                                                AND t1.GrdScaleId IN ( SELECT   GrdScaleId
                                                                                       FROM     arClassSections
                                                                                       WHERE    ClsSectionId = @ClsSectionId )
                                                                AND t2.Grade = @FinalGradeDesc
                                                      ); 
												
            IF ( @isGradeEligibleForCreditsEarned IS NULL )
                BEGIN
                    SET @isGradeEligibleForCreditsEarned = (
                                                             SELECT TOP 1
                                                                    t2.IsCreditsEarned
                                                             FROM   arGradeSystemDetails t2
                                                             WHERE  t2.Grade = @FinalGradeDesc
                                                           );
                END;
												
            IF ( @isGradeEligibleForCreditsAttempted IS NULL )
                BEGIN
                    SET @isGradeEligibleForCreditsAttempted = (
                                                                SELECT TOP 1
                                                                        t2.IsCreditsAttempted
                                                                FROM    arGradeSystemDetails t2
                                                                WHERE   t2.Grade = @FinalGradeDesc
                                                              );
                END;			
				
            IF @isGradeEligibleForCreditsEarned = 0
                BEGIN
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
            IF @isGradeEligibleForCreditsAttempted = 0
                BEGIN
                    SET @CreditsAttempted = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
												
            IF ( @IsPass = 0 )
                BEGIN
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
			--For Letter Grade Schools if the score is null but final grade was posted then the 
			--Final grade will be the current grade
            IF @CurrentGrade IS NULL
                AND @FinalGradeDesc IS NOT NULL
                BEGIN
                    SET @CurrentGrade = @FinalGradeDesc;
      END;

            IF (
                 @sysComponentTypeId = 503
                 OR @sysComponentTypeId = 500
               ) -- Lab work or Lab Hours
                BEGIN
				-- This course has lab work and lab hours
                    IF ( @Completed = 0 )
                        BEGIN
                            SET @CreditsPerService = (
                                                       SELECT TOP 1
                                                                GD.CreditsPerService
                                                       FROM     arGrdBkWeights GBW
                                                               ,arGrdComponentTypes GC
                                                               ,arGrdBkWgtDetails GD
                                                       WHERE    GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
                                                                AND GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                                                AND GBW.ReqId = @reqid
                                                                AND GC.SysComponentTypeId IN ( 500,503 )
                                                     );
                            SET @NumberOfServicesAttempted = (
                                                               SELECT TOP 1
                                                                        GBR.Score AS NumberOfServicesAttempted
                                                               FROM     arStuEnrollments SE
                                                               INNER JOIN arGrdBkResults GBR ON SE.StuEnrollId = GBR.StuEnrollId
                                                                                                AND GBR.ClsSectionId = @ClsSectionId
                                                             );

                            SET @CreditsEarned = ISNULL(@CreditsPerService,0) * ISNULL(@NumberOfServicesAttempted,0);
                        END;
                END;
			
            DECLARE @rowAlreadyInserted INT; 
            SET @rowAlreadyInserted = 0;
			
			-- Get the final Gpa only when IsCreditsAttempted is set to 1 and IsInGPA is set to 1
            IF @IsInGPA = 1
                BEGIN
                    IF ( @IsCreditsAttempted = 0 )
                        BEGIN
                            SET @FinalGPA = NULL; 
                        END;
                END;
            ELSE
                BEGIN
                    SET @FinalGPA = NULL; 
                END;

            IF @FinalScore IS NOT NULL
                BEGIN
                    SET @CurrentScore = @FinalScore; 
                END;
			
			-- Get the number of components that have scores
            DECLARE @CountComponentsThatHasScores INT;
            SET @CountComponentsThatHasScores = (
                                                  SELECT    COUNT(*)
                                                  FROM      arGrdBkResults
                                                  WHERE     StuEnrollId = @StuEnrollId
                                                            AND ClsSectionId IN ( SELECT DISTINCT
                                                                                            ClsSectionId
                                                                                  FROM      arClassSections
                                                                                  WHERE     TermId = @TermId
                                                                                            AND ReqId = @reqid )
                                                            AND Score IS NOT NULL
                                                );
												

            DECLARE @CourseComponentsThatNeedsToBeScored INT;
            DECLARE @clsStartDate DATETIME;
            SET @clsStartDate = (
                                  SELECT TOP 1
                                            StartDate
                                  FROM      arClassSections
                                  WHERE     TermId = @TermId
                                            AND ReqId = @reqid
                                );
            IF LOWER(@SetGradeBookAt) = ''instructorlevel''
                BEGIN
                    SET @CourseComponentsThatNeedsToBeScored = (
                                                                 SELECT COUNT(*)
                                                                 FROM   (
                                                                          SELECT    4 AS Tag
                                                                                   ,3 AS Parent
                                                                                   ,PV.PrgVerId
                                                                                   ,PV.PrgVerDescrip
                                                                                   ,NULL AS ProgramCredits
                                                                                   ,T.TermId
                                                                                   ,T.TermDescrip AS TermDescription
                                                                                   ,T.StartDate AS TermStartDate
                                                                                   ,T.EndDate AS TermEndDate
                                                                                   ,R.ReqId AS CourseId
                                                                                   ,R.Descrip AS CourseDescription
                                                                                   ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                                                                   ,NULL AS CourseCredits
                                                                                   ,NULL AS CourseFinAidCredits
                                                                                   ,NULL AS CoursePassingGrade
                                                                                   ,NULL AS CourseScore
                                                                                   ,(
                                                                                      SELECT TOP 1
                                                                                                GrdBkResultId
                                                                                      FROM      arGrdBkResults
                                                                                      WHERE     StuEnrollId = SE.StuEnrollId
                                                                                                AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                                                AND ClsSectionId = CS.ClsSectionId
                                                                                    ) AS GrdBkResultId
                                                                                   ,CASE WHEN LOWER(@SetGradeBookAt) = ''instructorlevel''
                                                                                         THEN RTRIM(GBWD.Descrip)
                                                                                         ELSE GCT.Descrip
                                                                                    END AS GradeBookDescription
                                                                                   ,( CASE GCT.SysComponentTypeId
                                                                                        WHEN 544 THEN (
                                                                                          SELECT  SUM(HoursAttended)
                                                                                                        FROM    arExternshipAttendance
                                                                                                        WHERE   StuEnrollId = SE.StuEnrollId
                                                                                                      )
                                                                                        ELSE (
                                                                                               SELECT TOP 1
                                                                                                        Score
                                                                                               FROM     arGrdBkResults
                                                                                               WHERE    StuEnrollId = SE.StuEnrollId
                                                                                                        AND InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                                                                        AND ClsSectionId = CS.ClsSectionId
                                                                                               ORDER BY ModDate DESC
                                                                                             )
                                                                                      END ) AS GradeBookScore
                                                                                   ,NULL AS GradeBookPostDate
                                                                                   ,NULL AS GradeBookPassingGrade
                                                                                   ,NULL AS GradeBookWeight
                                                                                   ,NULL AS GradeBookRequired
                                                                                   ,NULL AS GradeBookMustPass
                                                                                   ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                                                                   ,NULL AS GradeBookHoursRequired
                                                                                   ,NULL AS GradeBookHoursCompleted
                                                                                   ,SE.StuEnrollId
                                                                                   ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,544 ) THEN GBWD.Number
                                                                                           ELSE (
                                                                                                  SELECT    MIN(MinVal)
                                                                                                  FROM      arGradeScaleDetails GSD
                                                                                                           ,arGradeSystemDetails GSS
                                                                                                  WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                            AND GSS.IsPass = 1
                                                                                                            AND GSD.GrdScaleId = CS.GrdScaleId
                                                                                                )
                                                                                      END ) AS MinResult
                                                                                 ,SYRES.Resource AS GradeComponentDescription
                                                                                   ,NULL AS CreditsAttempted
                                                                                   ,NULL AS CreditsEarned
                                                                                   ,NULL AS Completed
                                                                                   ,NULL AS CurrentScore
                                                                                   ,NULL AS CurrentGrade
                                                                                   ,GCT.SysComponentTypeId AS FinalScore
                                                                                   ,NULL AS FinalGrade
                                                                                   ,NULL AS WeightedAverage_GPA
                                                                                   ,NULL AS SimpleAverage_GPA
                                                                                   ,NULL AS WeightedAverage_CumGPA
                                                                                   ,NULL AS SimpleAverage_CumGPA
                                                                                   ,C.CampusId
                                                                                   ,C.CampDescrip
                                                                                   ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId,R.ReqId ORDER BY C.CampDescrip, PV.PrgVerDescrip, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, GCT
.SysComponentTypeId, GCT.Descrip ) AS rownumber
                                                                                   ,S.FirstName AS FirstName
                                                                                   ,S.LastName AS LastName
                                                                                   ,S.MiddleName
                                                                                   ,SYRES.ResourceID
                                                                          FROM      -- MOdified by Balaji
                                                                                    arStuEnrollments SE
                                                                          INNER JOIN (
                                                                                       SELECT   StudentId
                                                                                               ,FirstName
                                                                                               ,LastName
                                                                                               ,MiddleName
                                                                                       FROM     arStudent
                                                                                     ) S ON S.StudentId = SE.StudentId
                                                                          INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId -- RES.TestId = CS.ClsSectionId -- and RES.StuEnrollId = GBR.StuEnrollId
                                                                          INNER JOIN arClassSections CS ON CS.ClsSectionId = RES.TestId
                                                                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                                          INNER JOIN arTerm T ON CS.TermId = T.TermId
                                                                          INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                                                          LEFT OUTER JOIN arGrdBkWgtDetails GBWD ON GBWD.InstrGrdBkWgtId = CS.InstrGrdBkWgtId --.InstrGrdBkWgtDetailId = CS.InstrGrdBkWgtDetailId
                                                                          INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                          INNER JOIN (
                                                                                       SELECT   Resource
                                                                                               ,ResourceID
                                                                                       FROM     syResources
                                                                                       WHERE    ResourceTypeID = 10
                                                                                     ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                                                          INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                                                          WHERE     SE.StuEnrollId = @StuEnrollId
                                                                                    AND T.TermId = @TermId
                                                                                    AND R.ReqId = @reqid
                                                                                    AND (
                                                                                          @sysComponentTypeId IS NULL
                                                                                          OR GCT.SysComponentTypeId IN (
                                                                                          SELECT    Val
                                                                                          FROM      MultipleValuesForReportParameters(@sysComponentTypeId,'','',
                                                                                                                                        1) )
                                                                                        )
                                                                          UNION
                                                                          SELECT    4 AS Tag
                                                                                   ,3
                                                                                   ,PV.PrgVerId
                                                                                   ,PV.PrgVerDescrip
                                                                                   ,NULL
                                                                                   ,T.TermId
                                                                                   ,T.TermDescrip
                                                                                   ,T.StartDate AS termStartdate
                                                                                   ,T.EndDate AS TermEndDate
                                                                                   ,GBCR.ReqId
                                                                                   ,R.Descrip AS CourseDescrip
                                                                                   ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                                                                   ,NULL
                                                                                   ,NULL
                                                                                   ,NULL
                                                                                   ,NULL
        ,GBCR.ConversionResultId AS GrdBkResultId
                                                                                   ,CASE WHEN LOWER(@SetGradeBookAt) = ''instructorlevel''
                                                                                         THEN RTRIM(GBWD.Descrip)
                                                                                         ELSE GCT.Descrip
                                                                                    END AS GradeBookDescription
                                                                                   ,GBCR.Score AS GradeBookResult
                                                                                   ,NULL
                                                                                   ,NULL
                                                                                   ,NULL
                                                                                   ,NULL
                                                                                   ,NULL
                                                                                   ,GCT.SysComponentTypeId
                                                                                   ,NULL
                                                                                   ,NULL
                                                                                   ,SE.StuEnrollId
                                                                                   ,GBCR.MinResult
                                                                                   ,SYRES.Resource -- Student data  
                                                                                   ,NULL AS CreditsAttempted
                                                                                   ,NULL AS CreditsEarned
                                                                                   ,NULL AS Completed
                                                                                   ,NULL AS CurrentScore
                                                                                   ,NULL AS CurrentGrade
                                                                                   ,NULL AS FinalScore
                                                                                   ,NULL AS FinalGrade
                                                                                   ,NULL AS WeightedAverage_GPA
                                                                                   ,NULL AS SimpleAverage_GPA
                                                                                   ,NULL
                                                                                   ,NULL
                                                                                   ,C.CampusId
                                                                                   ,C.CampDescrip
                                                                                   ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId,R.ReqId ORDER BY C.CampDescrip, PV.PrgVerDescrip, T.StartDate, T.EndDate, T.TermId, T.TermDescrip, R.ReqId, R.Descrip, GCT.
SysComponentTypeId, GCT.Descrip ) AS rownumber
                                                                                   ,S.FirstName AS FirstName
                                                                                   ,S.LastName AS LastName
                                                                                   ,S.MiddleName
                                                                                   ,SYRES.ResourceID
                                                                          FROM      arGrdBkConversionResults GBCR
                                                                          INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                                                          INNER JOIN (
                                                                                       SELECT   StudentId
                                                                                               ,FirstName
                                                                                               ,LastName
                                                                                               ,MiddleName
                                                                                       FROM     arStudent
                                                                                     ) S ON S.StudentId = SE.StudentId
                                                                          INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                                          INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                                                          INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                                                          INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                                                          INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                                                               AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                                                          INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                                                           AND GBCR.ReqId = GBW.ReqId
                                                                          INNER JOIN (
                                                                                       SELECT   ReqId
                                                                                               ,MAX(EffectiveDate) AS EffectiveDate
                                                                                       FROM     arGrdBkWeights
                                                                                       GROUP BY ReqId
                                                                                     ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                                                                          INNER JOIN (
                                                                                       SELECT   Resource
                                                                                               ,ResourceID
                                                                                       FROM     syResources
                                                                                       WHERE    ResourceTypeID = 10
                                                                                     ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                                                          INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                                                          WHERE     MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                                                                                    AND SE.StuEnrollId = @StuEnrollId
                                                                                    AND T.TermId = @TermId
                                                                                    AND R.ReqId = @reqid
                                                                                    AND (
                                                       @sysComponentTypeId IS NULL
                                                                                          OR GCT.SysComponentTypeId IN (
                                                                                          SELECT    Val
                                                                                          FROM      MultipleValuesForReportParameters(@sysComponentTypeId,'','',
                                                                                                                                        1) )
                                                                                        )
                                                                        ) dt
                                                               );
                END;
            ELSE
                BEGIN
                    SET @CourseComponentsThatNeedsToBeScored = (
                                                                 SELECT COUNT(*)
                                                                 FROM   (
                                                                          SELECT DISTINCT
                                                                                    GradeBookDescription
                                                                                   ,GradeBookScore
                                                                                   ,MinResult
                                                                                   ,GradeBookSysComponentTypeId
                                                                                   ,GradeComponentDescription
                                                                                   ,CampDescrip
                                                                                   ,FirstName
                                                                                   ,LastName
                                                                                   ,MiddleName
                                                                                   ,GrdBkResultId
                                                                                   ,TermStartDate
                                                                                   ,TermEndDate
                                                                                   ,TermDescription
                                                                                   ,CourseDescription
                                                                                   ,PrgVerDescrip
                                                                                   ,StuEnrollId
                                                                                   ,CourseId
                                                                                   ,ResourceID
                                                                                   ,Required
                                                                          FROM      (
                                                                                      SELECT  DISTINCT
                                                                                                4 AS Tag
                                                                                               ,3 AS Parent
                                                                                               ,PV.PrgVerId
                                                                                               ,PV.PrgVerDescrip
                                                                                               ,NULL AS ProgramCredits
                                                                                               ,T.TermId
                                                              ,T.TermDescrip AS TermDescription
                                                                                               ,T.StartDate AS TermStartDate
                                                                                               ,T.EndDate AS TermEndDate
                                                                                               ,R.ReqId AS CourseId
                                                                                               ,R.Descrip AS CourseDescription
                                                                                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                                                                               ,NULL AS CourseCredits
                                                                                               ,NULL AS CourseFinAidCredits
                                                                                               ,NULL AS CoursePassingGrade
                                                                                               ,NULL AS CourseScore
                                                                                               ,(
                                                                                                  SELECT TOP 1
                                                                                                            GrdBkResultId
                                                                                                  FROM      arGrdBkResults
                                                                                                  WHERE     StuEnrollId = SE.StuEnrollId
                                                                                                            AND InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                                            AND ClsSectionId = CS.ClsSectionId
                                                                                                ) AS GrdBkResultId
                                                                                               ,RTRIM(GCT.Descrip) + ( CASE WHEN a.ResNum IN ( 0,1 ) THEN ''''
                                                                                                                            ELSE CAST(a.ResNum AS CHAR)
                                                                                                                       END ) AS GradeBookDescription
                                                                                               ,( CASE GCT.SysComponentTypeId
                                                                                                    WHEN 544 THEN (
                                                                                                                    SELECT  SUM(HoursAttended)
                                                                                                                    FROM    arExternshipAttendance
                                                                                                                    WHERE   StuEnrollId = SE.StuEnrollId
                                                                                                                  )
                                                                                                    ELSE 
--								(select Top 1 Score from arGrdBkResults where StuEnrollId=SE.StuEnrollId and 
--								InstrGrdBkWgtDetailId=b.InstrGrdBkWgtDetailId and
--								ClsSectionId=CS.ClsSectionId order by moddate desc) 
                                                                                                         a.Score
                                            END ) AS GradeBookScore
                                                                                               ,NULL AS GradeBookPostDate
                                                                                               ,NULL AS GradeBookPassingGrade
                                                                                               ,NULL AS GradeBookWeight
                                                                                               ,NULL AS GradeBookRequired
                                                                                               ,NULL AS GradeBookMustPass
                                                                                               ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                                                                               ,NULL AS GradeBookHoursRequired
                                                                                               ,NULL AS GradeBookHoursCompleted
                                                                                               ,SE.StuEnrollId
                                                                                               ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,544 )
                                                                                                       THEN b.Number
                                                                                                       ELSE (
                                                                                                              SELECT    MIN(MinVal)
                                                                                                              FROM      arGradeScaleDetails GSD
                                                                                                                       ,arGradeSystemDetails GSS
                                                                                                              WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                                        AND GSS.IsPass = 1
                                                                                                                        AND GSD.GrdScaleId = CS.GrdScaleId
                                                                                                            )
                                                                                                  END ) AS MinResult
                                                                                               ,SYRES.Resource AS GradeComponentDescription
                                                                                               ,NULL AS CreditsAttempted
                                                                                               ,NULL AS CreditsEarned
                                                                                               ,NULL AS Completed
                                                                                               ,NULL AS CurrentScore
                                                                                               ,NULL AS CurrentGrade
                                                                                               ,GCT.SysComponentTypeId AS FinalScore
                                                                                               ,NULL AS FinalGrade
                                                                                               ,NULL AS WeightedAverage_GPA
                                                                                               ,NULL AS SimpleAverage_GPA
                          ,NULL AS WeightedAverage_CumGPA
                                                                                               ,NULL AS SimpleAverage_CumGPA
                                                                                               ,C1.CampusId
                                                                                               ,C1.CampDescrip
                                                                                               , 
						--ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C1.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
                                                                                                c.FirstName AS FirstName
                                                                                               ,c.LastName AS LastName
                                                                                               ,c.MiddleName
                                                                                               ,SYRES.ResourceID
                                                                                               ,b.Required
                                                                                      FROM      arGrdBkResults a
                                                                                               ,arGrdBkWgtDetails b
                                                                                               ,arStudent c
                                                                                               ,arStuEnrollments SE
                                                                                               ,arResults e
                                                                                               ,arClassSections CS
                                                                                               ,arReqs R
                                                                                               ,arTerm T
                                                                                               ,arGrdComponentTypes GCT
                                                                                               ,(
                                                                                                  SELECT    Resource
                                                                                                           ,ResourceID
                                                                                                  FROM      syResources
                                                                                                  WHERE     ResourceTypeID = 10
                                                                                                ) SYRES
                                                                                               ,syCampuses C1
                                                                                               ,arPrgVersions PV
                                                                                      WHERE     a.InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                                AND a.StuEnrollId = @StuEnrollId
                                                                                                AND a.StuEnrollId = SE.StuEnrollId
                                                                                                AND SE.StudentId = c.StudentId
                                                                                                AND a.StuEnrollId = e.StuEnrollId
                                                                                                AND a.ClsSectionId = e.TestId
                                                                                                AND e.TestId = CS.ClsSectionId
                                                                                                AND CS.ReqId = R.ReqId
                                                                                                AND CS.TermId = T.TermId
                                                                                                AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                                                                                AND SYRES.ResourceID = GCT.SysComponentTypeId
                                                                                                AND SE.CampusId = C1.CampusId
                                                                                                AND SE.PrgVerId = PV.PrgVerId 
						--and a.ClsSectionId = ''CD64FA81-7E91-4E22-A323-34E7B5695E2E'' 
                                                                                                AND R.ReqId = @reqid
                                                                                                AND T.TermId = @TermId 
						--and (@SysComponentTypeId is null or GCT.SysComponentTypeId in (Select Val from [MultipleValuesForReportParameters](@SysComponentTypeId,'','',1)))
                                                                                                AND a.ResNum >= 1
                                                                                      UNION
                                                                                      SELECT  DISTINCT
                                                                                                4 AS Tag
                                                                                               ,3 AS Parent
                                                                                               ,PV.PrgVerId
                                                                                               ,PV.PrgVerDescrip
                                                                                               ,NULL AS ProgramCredits
                                                                                               ,T.TermId
                                                                                               ,T.TermDescrip AS TermDescription
                                                                                               ,T.StartDate AS TermStartDate
                                                                                               ,T.EndDate AS TermEndDate
                                                                                               ,R.ReqId AS CourseId
                                                                                               ,R.Descrip AS CourseDescription
                                                                                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                                                                               ,NULL AS CourseCredits
                                                                                               ,NULL AS CourseFinAidCredits
                                                                                               ,NULL AS CoursePassingGrade
                                                                                               ,NULL AS CourseScore
                                                                                               ,(
                                                                                                  SELECT TOP 1
                                                                                                            GrdBkResultId
                                                                    FROM      arGrdBkResults
                                                                                                  WHERE     StuEnrollId = SE.StuEnrollId
                                                                                                            AND InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                                            AND ClsSectionId = CS.ClsSectionId
                                                                                                ) AS GrdBkResultId
                                                                                               ,RTRIM(GCT.Descrip) + ( CASE WHEN a.ResNum IN ( 0,1 ) THEN ''''
                                                                                                                            ELSE CAST(a.ResNum AS CHAR)
                                                                                                                       END ) AS GradeBookDescription
                                                                                               ,( CASE GCT.SysComponentTypeId
                                                                                                    WHEN 544 THEN (
                                                                                                                    SELECT  SUM(HoursAttended)
                                                                                                                    FROM    arExternshipAttendance
                                                                                                                    WHERE   StuEnrollId = SE.StuEnrollId
                                                                                                                  )
                                                                                                    ELSE 
--								(select Top 1 Score from arGrdBkResults where StuEnrollId=SE.StuEnrollId and 
--								InstrGrdBkWgtDetailId=b.InstrGrdBkWgtDetailId and
--								ClsSectionId=CS.ClsSectionId order by moddate desc) 
                                                                                                         a.Score
                                                                                                  END ) AS GradeBookScore
                                                                                               ,NULL AS GradeBookPostDate
                                                                                               ,NULL AS GradeBookPassingGrade
                                                                                               ,NULL AS GradeBookWeight
                                                                                               ,NULL AS GradeBookRequired
                                                                                               ,NULL AS GradeBookMustPass
                                                                                               ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                                                                               ,NULL AS GradeBookHoursRequired
                                                                                               ,NULL AS GradeBookHoursCompleted
                                                                                               ,SE.StuEnrollId
                                                                                               ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,544 )
                                                                                                       THEN b.Number
                                                                                                       ELSE (
                                                                                                              SELECT    MIN(MinVal)
                                                                                                              FROM      arGradeScaleDetails GSD
                                                                                                                       ,arGradeSystemDetails GSS
                                                                                                              WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                                        AND GSS.IsPass = 1
                                                                                                                        AND GSD.GrdScaleId = CS.GrdScaleId
                                                                                                            )
                                                                                                  END ) AS MinResult
                                                                                               ,SYRES.Resource AS GradeComponentDescription
                                                                                               ,NULL AS CreditsAttempted
                                                                                               ,NULL AS CreditsEarned
                                                                                               ,NULL AS Completed
                                                                                               ,NULL AS CurrentScore
                                                                                               ,NULL AS CurrentGrade
                                                                                               ,GCT.SysComponentTypeId AS FinalScore
                                                                                               ,NULL AS FinalGrade
                                                                                               ,NULL AS WeightedAverage_GPA
                                                                                               ,NULL AS SimpleAverage_GPA
                                                                                               ,NULL AS WeightedAverage_CumGPA
                                                                                               ,NULL AS SimpleAverage_CumGPA
                                                                                               ,C1.CampusId
                                                                                               ,C1.CampDescrip
                                                                                               , 
						--ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C1.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
                                                                                                c.FirstName AS FirstName
                                                                                               ,c.LastName AS LastName
                                                                                               ,c.MiddleName
                                                                                               ,SYRES.ResourceID
                                                                                               ,b.Required
                                                                                      FROM      arGrdBkResults a
                                                                                               ,arGrdBkWgtDetails b
                                 ,arStudent c
                                                                                               ,arStuEnrollments SE
                                                                                               ,arResults e
                                                                                               ,arClassSections CS
                                                                                               ,arReqs R
                                                                                               ,arTerm T
                                                                                               ,arGrdComponentTypes GCT
                                                                                               ,(
                                                                                                  SELECT    Resource
                                                                                                           ,ResourceID
                                                                                                  FROM      syResources
                                                                                                  WHERE     ResourceTypeID = 10
                                                                                                ) SYRES
                                                                                               ,syCampuses C1
                                                                                               ,arPrgVersions PV
                                                                                      WHERE     a.InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                                AND a.StuEnrollId = @StuEnrollId
                                                                                                AND a.StuEnrollId = SE.StuEnrollId
                                                                                                AND SE.StudentId = c.StudentId
                                                                                                AND a.StuEnrollId = e.StuEnrollId
                                                                                                AND a.ClsSectionId = e.TestId
                                                                                                AND e.TestId = CS.ClsSectionId
                                                                                                AND CS.ReqId = R.ReqId
                                                                                                AND CS.TermId = T.TermId
                                                                                                AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                                                                                AND SYRES.ResourceID = GCT.SysComponentTypeId
                                                                                                AND SE.CampusId = C1.CampusId
                                                                                                AND SE.PrgVerId = PV.PrgVerId 
						--and a.ClsSectionId = ''CD64FA81-7E91-4E22-A323-34E7B5695E2E'' 
                                                                                                AND R.ReqId = @reqid
                                                                                                AND T.TermId = @TermId 
						--and (@SysComponentTypeId is null or GCT.SysComponentTypeId in (Select Val from [MultipleValuesForReportParameters](@SysComponentTypeId,'','',1)))
                                                                                                AND a.ResNum = 0
                     AND a.GrdBkResultId NOT IN (
                                                                                                SELECT DISTINCT
                                                                                                        GrdBkResultId
                                                                                                FROM    arGrdBkResults a
                                                                                                       ,arGrdBkWgtDetails b
                                                                                                       ,arStudent c
                                                                                                       ,arStuEnrollments SE
                                                                                                       ,arResults e
                                                                                                       ,arClassSections CS
                                                                                                       ,arReqs R
                                                                                                       ,arTerm T
                                                                                                       ,arGrdComponentTypes GCT
                                                                                                       ,(
                                                                                                          SELECT    Resource
                                                                                                                   ,ResourceID
                                                                                                          FROM      syResources
                                                                                                          WHERE     ResourceTypeID = 10
                                                                                                        ) SYRES
                                                                                                       ,syCampuses C1
                                                                                                       ,arPrgVersions PV
                                                                                                WHERE   a.InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                                        AND a.StuEnrollId = @StuEnrollId
                                                                                                        AND a.StuEnrollId = SE.StuEnrollId
                                                                                                        AND SE.StudentId = c.StudentId
                                                                                                        AND a.StuEnrollId = e.StuEnrollId
                                                                                                        AND a.ClsSectionId = e.TestId
                                                                                                        AND e.TestId = CS.ClsSectionId
                                                                                                        AND CS.ReqId = R.ReqId
                                                                                                        AND CS.TermId = T.TermId
                                                                                                        AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                                                                                        AND SYRES.ResourceID = GCT.SysComponentTypeId
                                                                                                        AND SE.CampusId = C1.CampusId
                                                                                                        AND SE.PrgVerId = PV.PrgVerId
                                                                                                        AND R.ReqId = @reqid
                                                                                                        AND T.TermId = @TermId
                                                                                                        AND (
                                                                                                              @sysComponentTypeId IS NULL
                                                                                                              OR GCT.SysComponentTypeId IN (
                                                                                                              SELECT    Val
                                                                                                              FROM      MultipleValuesForReportParameters(@sysComponentTypeId,
                                                                                                                                                '','',1) )
                                                                                                            )
                                                                                                        AND a.ResNum >= 1 )
                                                                                      UNION
                                                                                      SELECT    *
                                                                                      FROM      (
                                                                                                  SELECT  DISTINCT
                                                                                                            4 AS Tag
                                                                                                           ,3 AS Parent
                                                                                                           ,PV.PrgVerId
                                                                                                           ,PV.PrgVerDescrip
                                                                                                           ,NULL AS ProgramCredits
                                                                                                           ,T.TermId
                                                                                                           ,T.TermDescrip AS TermDescription
                                                                                                           ,T.StartDate AS TermStartDate
                                                                                                           ,T.EndDate AS TermEndDate
                                                                                                           ,R.ReqId AS CourseId
                                                                                                           ,R.Descrip AS CourseDescription
                                                                                                           ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                                                                                           ,NULL AS CourseCredits
                                                                                                           ,NULL AS CourseFinAidCredits
                                                                                                           ,NULL AS CoursePassingGrade
 ,NULL AS CourseScore
                                                                                                           ,(
                                                                                                              SELECT TOP 1
                                                                                                                        GrdBkResultId
                                                                                                              FROM      arGrdBkResults
                                                                                                              WHERE     StuEnrollId = SE.StuEnrollId
                                                                                                                        AND InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                                                        AND ClsSectionId = CS.ClsSectionId
                                                                                                            ) AS GrdBkResultId
                                                                                                           ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                                                                           ,( CASE GCT.SysComponentTypeId
                                                                                                                WHEN 544
                                                                                                                THEN (
                                                                                                                       SELECT   SUM(HoursAttended)
                                                                                                                       FROM     arExternshipAttendance
                                                                                                                       WHERE    StuEnrollId = SE.StuEnrollId
                                                                                                                     )
                                                                                                                ELSE 
--								(select Top 1 Score from arGrdBkResults where StuEnrollId=SE.StuEnrollId and 
--								InstrGrdBkWgtDetailId=b.InstrGrdBkWgtDetailId and
--								ClsSectionId=CS.ClsSectionId order by moddate desc) 
                                                                                                                     NULL
                                                                                                              END ) AS GradeBookScore
                                                                                                           ,NULL AS GradeBookPostDate
                                                                                                           ,NULL AS GradeBookPassingGrade
                                                                                                           ,NULL AS GradeBookWeight
                                                                                                           ,NULL AS GradeBookRequired
                                                                                                           ,NULL AS GradeBookMustPass
                                                                                                           ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                                                                                           ,NULL AS GradeBookHoursRequired
                                                                                                           ,NULL AS GradeBookHoursCompleted
                                                                  ,SE.StuEnrollId
                                                                                                           ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,
                                                                                                                                                544 )
                                                                                                                   THEN b.Number
                                                                                                                   ELSE (
                                                                                                                          SELECT    MIN(MinVal)
                                                                                                                          FROM      arGradeScaleDetails GSD
                                                                                                                                   ,arGradeSystemDetails GSS
                                                                                                                          WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                                                    AND GSS.IsPass = 1
                                                                                                                                    AND GSD.GrdScaleId = CS.GrdScaleId
                                                                                                                        )
                                                                                                              END ) AS MinResult
                                                                                                           ,SYRES.Resource AS GradeComponentDescription
                                                                                                           ,NULL AS CreditsAttempted
                                                                                                           ,NULL AS CreditsEarned
                                                                                                           ,NULL AS Completed
                                                                                                           ,NULL AS CurrentScore
                                                                                                           ,NULL AS CurrentGrade
                                                                                                           ,GCT.SysComponentTypeId AS FinalScore
                                                                                                           ,NULL AS FinalGrade
                                                                                                           ,NULL AS WeightedAverage_GPA
                                                                                                           ,NULL AS SimpleAverage_GPA
                                                                                                           ,NULL AS WeightedAverage_CumGPA
                                                                                                           ,NULL AS SimpleAverage_CumGPA
                                                                                                           ,C1.CampusId
                                                                                                           ,C1.CampDescrip
                                                                                                           , 
						--ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C1.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
                                                                                                            c.FirstName AS FirstName
                                                                                                           ,c.LastName AS LastName
                                                                                                           ,c.MiddleName
                                                                                                           ,SYRES.ResourceID
                                                                                                           ,b.Required
                                                                                                  FROM      arResults e
                                                                                                           ,arStuEnrollments SE
                                                                                                           ,arStudent c
                                                                                                           ,arClassSections CS
                                                                                                           ,arReqs R
                                                                                                           ,arTerm T
                                                                                                           ,arGrdComponentTypes GCT
                                                                                                           ,(
                                                                                                              SELECT    Resource
                                                                                                                       ,ResourceID
                                                                                                              FROM      syResources
                                                                                                              WHERE     ResourceTypeID = 10
                                                                                                            ) SYRES
                                                                                                           ,syCampuses C1
                                                                                                           ,arPrgVersions PV
                                                                                                           ,(
                                                                                                              SELECT DISTINCT TOP 1
                                                                                                                        A.InstrGrdBkWgtId
                                                                                                                       ,A.EffectiveDate
                                                                                                                       ,b.GrdScaleId
                                                                                                                       ,b.ReqId
                                                                                                              FROM      arGrdBkWeights A
                                                                                                                       ,arClassSections b
                                                                                                              WHERE     A.ReqId = b.ReqId
                                                                                                                        AND A.EffectiveDate <= b.StartDate
                                                AND b.TermId = @TermId
                                                                                                                        AND b.ReqId = @reqid
                                                                                                              ORDER BY  EffectiveDate DESC
                                                                                                            ) a
                                                                                                           ,arGrdBkWgtDetails b
                                                                                                  WHERE     e.StuEnrollId = SE.StuEnrollId
                                                                                                            AND SE.StudentId = c.StudentId
                                                                                                            AND e.TestId = CS.ClsSectionId
                                                                                                            AND CS.ReqId = R.ReqId
                                                                                                            AND CS.TermId = T.TermId
                                                                                                            AND a.InstrGrdBkWgtId = b.InstrGrdBkWgtId
                                                                                                            AND a.ReqId = R.ReqId
                                                                                                            AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                                                                                            AND SYRES.ResourceID = GCT.SysComponentTypeId
                                                                                                            AND SE.CampusId = C1.CampusId
                                                                                                            AND SE.PrgVerId = PV.PrgVerId
                                                                                                            AND SE.StuEnrollId = @StuEnrollId
                                                                                                            AND (
                                                                                                                  @sysComponentTypeId IS NULL
                                                                                                                  OR GCT.SysComponentTypeId IN (
                                                                                                                  SELECT    Val
                                                                                                                  FROM      MultipleValuesForReportParameters(@sysComponentTypeId,
                                                                                                                                                '','',1) )
                                                                                                                )
                                                                                                ) dt5
                                                                                      WHERE     GrdBkResultId IS NULL
                                                                                      UNION
                                                                                      SELECT  DISTINCT
                                                                                                4 AS Tag
                                                                                               ,3 AS Parent
          ,PV.PrgVerId
                                                                                               ,PV.PrgVerDescrip
                                                                                               ,NULL AS ProgramCredits
                                                                                               ,T.TermId
                                                                                               ,T.TermDescrip AS TermDescription
                                                                                               ,T.StartDate AS TermStartDate
                                                                                               ,T.EndDate AS TermEndDate
                                                                                               ,R.ReqId AS CourseId
                                                                                               ,R.Descrip AS CourseDescription
                                                                                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                                                                               ,NULL AS CourseCredits
                                                                                               ,NULL AS CourseFinAidCredits
                                                                                               ,NULL AS CoursePassingGrade
                                                                                               ,NULL AS CourseScore
                                                                                               ,(
                                                                                                  SELECT TOP 1
                                                                                                            GrdBkResultId
                                                                                                  FROM      arGrdBkResults
                                                                                                  WHERE     StuEnrollId = SE.StuEnrollId
                                                                                                            AND InstrGrdBkWgtDetailId = b.InstrGrdBkWgtDetailId
                                                                                                            AND ClsSectionId = CS.ClsSectionId
                                                                                                ) AS GrdBkResultId
                                                                                               ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                                                               ,( CASE GCT.SysComponentTypeId
                                                                                                    WHEN 544 THEN (
                                                                                                                    SELECT  SUM(HoursAttended)
                                                                                                                    FROM    arExternshipAttendance
                                                                                                                    WHERE   StuEnrollId = SE.StuEnrollId
                                                                                                                  )
                                                                                                    ELSE 
--								(select Top 1 Score from arGrdBkResults where StuEnrollId=SE.StuEnrollId and 
--								InstrGrdBkWgtDetailId=b.InstrGrdBkWgtDetailId and
--								ClsSectionId=CS.ClsSectionId order by moddate desc) 
             NULL
                                                                                                  END ) AS GradeBookScore
                                                                                               ,NULL AS GradeBookPostDate
                                                                                               ,NULL AS GradeBookPassingGrade
                                                                                               ,NULL AS GradeBookWeight
                                                                                               ,NULL AS GradeBookRequired
                                                                                               ,NULL AS GradeBookMustPass
                                                                                               ,GCT.SysComponentTypeId AS GradeBookSysComponentTypeId
                                                                                               ,NULL AS GradeBookHoursRequired
                                                                                               ,NULL AS GradeBookHoursCompleted
                                                                                               ,SE.StuEnrollId
                                                                                               ,( CASE WHEN GCT.SysComponentTypeId IN ( 500,503,504,544 )
                                                                                                       THEN b.Number
                                                                                                       ELSE (
                                                                                                              SELECT    MIN(MinVal)
                                                                                                              FROM      arGradeScaleDetails GSD
                                                                                                                       ,arGradeSystemDetails GSS
                                                                                                              WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                                                        AND GSS.IsPass = 1
                                                                                                                        AND GSD.GrdScaleId = CS.GrdScaleId
                                                                                                            )
                                                                                                  END ) AS MinResult
                                                                                               ,SYRES.Resource AS GradeComponentDescription
                                                                                               ,NULL AS CreditsAttempted
                                                                                               ,NULL AS CreditsEarned
                                                                                               ,NULL AS Completed
                                                                                               ,NULL AS CurrentScore
                                                                                               ,NULL AS CurrentGrade
                                                                                               ,GCT.SysComponentTypeId AS FinalScore
                                                                                               ,NULL AS FinalGrade
                                                                                               ,NULL AS WeightedAverage_GPA
                                                                                               ,NULL AS SimpleAverage_GPA
                                                                                               ,NULL AS WeightedAverage_CumGPA
                                                                                               ,NULL AS SimpleAverage_CumGPA
                                                                                               ,C1.CampusId
                                                                                               ,C1.CampDescrip
                                                                                               , 
						--ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C1.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
                                                                                                c.FirstName AS FirstName
                                                                                               ,c.LastName AS LastName
                                                                                               ,c.MiddleName
                                                                                               ,SYRES.ResourceID
                                                                                               ,b.Required
                                                                                      FROM      arResults e
                                                                                               ,arStuEnrollments SE
                                                                                               ,arStudent c
                                                                                               ,arClassSections CS
                                                                                               ,arReqs R
                                                                                               ,arTerm T
                                                                                               ,arGrdComponentTypes GCT
                                                                                               ,(
                                                                                                  SELECT    Resource
                                                                                                           ,ResourceID
                                                                                                  FROM      syResources
                                                                                                  WHERE     ResourceTypeID = 10
                                                                                                ) SYRES
                                                                                               ,syCampuses C1
                                                                                               ,arPrgVersions PV
                                                                                               ,(
                                                                                                  SELECT DISTINCT TOP 1
                                                                                                            A.InstrGrdBkWgtId
                                                                                                           ,A.EffectiveDate
                                                                                                           ,b.GrdScaleId
                                                                                                           ,b.ReqId
                                                                                                  FROM      arGrdBkWeights A
               ,arClassSections b
                                                                                                  WHERE     A.ReqId = b.ReqId
                                                                                                            AND A.EffectiveDate <= b.StartDate
                                                                                                            AND b.TermId = @TermId
                                                                                                            AND b.ReqId = @reqid
                                                                                                  ORDER BY  A.EffectiveDate DESC
                                                                                                ) a
                                                                                               ,arGrdBkWgtDetails b
                                                                                      WHERE     e.StuEnrollId = SE.StuEnrollId
                                                                                                AND SE.StudentId = c.StudentId
                                                                                                AND e.TestId = CS.ClsSectionId
                                                                                                AND CS.ReqId = R.ReqId
                                                                                                AND CS.TermId = T.TermId
                                                                                                AND a.InstrGrdBkWgtId = b.InstrGrdBkWgtId
                                                                                                AND a.ReqId = R.ReqId
                                                                                                AND GCT.GrdComponentTypeId = b.GrdComponentTypeId
                                                                                                AND SYRES.ResourceID = GCT.SysComponentTypeId
                                                                                                AND SE.CampusId = C1.CampusId
                                                                                                AND SE.PrgVerId = PV.PrgVerId
                                                                                                AND SE.StuEnrollId = @StuEnrollId
                                                                                                AND R.ReqId = @reqid
                                                                                                AND T.TermId = @TermId
                                                                                                AND (
                                                                                                      @sysComponentTypeId IS NULL
                                                                                                      OR GCT.SysComponentTypeId IN (
                                                                                                      SELECT    Val
                                                                                                      FROM      MultipleValuesForReportParameters(@sysComponentTypeId,
                                                                                                                                                '','',1) )
                                                                                                    )
                                                                                                AND e.TestId NOT IN ( SELECT    ClsSectionId
                                                                                                                      FROM      arGrdBkResults
                           WHERE     StuEnrollId = e.StuEnrollId
                                                                                                                                AND ClsSectionId = e.TestId )
                                                                                      UNION
                                                                                      SELECT    4 AS Tag
                                                                                               ,3
                                                                                               ,PV.PrgVerId
                                                                                               ,PV.PrgVerDescrip
                                                                                               ,NULL
                                                                                               ,T.TermId
                                                                                               ,T.TermDescrip
                                                                                               ,T.StartDate AS termStartdate
                                                                                               ,T.EndDate AS TermEndDate
                                                                                               ,GBCR.ReqId
                                                                                               ,R.Descrip AS CourseDescrip
                                                                                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                                                                               ,NULL
                                                                                               ,NULL
                                                                                               ,NULL
                                                                                               ,NULL
                                                                                               ,GBCR.ConversionResultId AS GrdBkResultId
                                                                                               ,GBCR.Comments AS GradeBookDescription
                                                                                               ,GBCR.Score AS GradeBookResult
                                                                                               ,NULL
                                                                                               ,NULL
                                                                                               ,NULL
                                                                                               ,NULL
                                                                                               ,NULL
                                                                                               ,GCT.SysComponentTypeId
                                                                                               ,NULL
                                                                                               ,NULL
                                                                                               ,SE.StuEnrollId
                                                                                               ,GBCR.MinResult
                                                                                               ,SYRES.Resource -- Student data  
                                                                                               ,NULL AS CreditsAttempted
                                                                                               ,NULL AS CreditsEarned
                            ,NULL AS Completed
                                                                                               ,NULL AS CurrentScore
                                                                                               ,NULL AS CurrentGrade
                                                                                               ,NULL AS FinalScore
                                                                                               ,NULL AS FinalGrade
                                                                                               ,NULL AS WeightedAverage_GPA
                                                                                               ,NULL AS SimpleAverage_GPA
                                                                                               ,NULL
                                                                                               ,NULL
                                                                                               ,C.CampusId
                                                                                               ,C.CampDescrip
                                                                                               ,
						--ROW_NUMBER() OVER (partition by SE.StuEnrollId,R.ReqId Order By C.CampDescrip,PV.PrgVerDescrip,T.StartDate,T.EndDate,T.TermId,T.TermDescrip,R.ReqId,R.Descrip,GCT.SysComponentTypeId,GCT.Descrip) as rownumber,
                                                                                                S.FirstName AS FirstName
                                                                                               ,S.LastName AS LastName
                                                                                               ,S.MiddleName
                                                                                               ,SYRES.ResourceID
                                                                                               ,GBWD.Required
                                                                                      FROM      arGrdBkConversionResults GBCR
                                                                                      INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                                                                                      INNER JOIN (
                                                                                                   SELECT   StudentId
                                                                                                           ,FirstName
                                                                                                           ,LastName
                                                                                                           ,MiddleName
                                                                                                   FROM     arStudent
                                                                                                 ) S ON S.StudentId = SE.StudentId
                                                                                      INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                                                      INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                                                                                      INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                                                                                      INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                                                                      INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                  AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                                                                                      INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                                                                                       AND GBCR.ReqId = GBW.ReqId
                                                                                      INNER JOIN (
                                                                                                   SELECT   Resource
                                                                                                           ,ResourceID
                                                                                                   FROM     syResources
                                                                                                   WHERE    ResourceTypeID = 10
                                                                                                 ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                                                                                      INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                                                                                      WHERE     --MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate and
                                                                                                SE.StuEnrollId = @StuEnrollId
                                                                                                AND T.TermId = @TermId
                                                                                                AND R.ReqId = @reqid
                                                                                                AND (
                                                                                                      @sysComponentTypeId IS NULL
                                                                                                      OR GCT.SysComponentTypeId IN (
                                                                                                      SELECT    Val
                                                                                                      FROM      MultipleValuesForReportParameters(@sysComponentTypeId,
                                                                                                                                                '','',1) )
                                                                                                    )
                                                                                    ) dt
                                                                        ) dt1
                                                                 WHERE  Required = 1
                                                               );	
                END;
			
		
            DECLARE @hasallscoresbeenpostedforrequiredcomponents BIT;
            IF @CountComponentsThatHasScores >= @CourseComponentsThatNeedsToBeScored
                BEGIN
                    SET @hasallscoresbeenpostedforrequiredcomponents = 1;
                END;
            ELSE
                BEGIN
                    SET @hasallscoresbeenpostedforrequiredcomponents = 0;
                END;
		
			/************************************************* Changes for Build 2816 *********************/
			-- Rally case DE 738 KeyBoarding Courses
			-- Modified by Balaji on 11/8/2010
			-- modified by Janet on 11/4/2011 to check for student enroll campus
            SET @GradesFormat = (
                                  SELECT    dbo.GetAppSettingValue(47,@StuEnrollCampusId)
                                );
			
				-- This condition is met only for numeric grade schools
            IF (
                 @IsGradeBookNotSatisified = 0
                 AND @IsWeighted = 0
                 AND @FinalScore IS NULL
                 AND @FinalGradeDesc IS NULL
                 AND LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
               )
                AND @CountComponentsThatHasScores >= 1
                BEGIN
								-- Balaji Comments : Keyboarding components do not have any weights set (@IsWeighted will be zero)
								-- For non key boarding components, weights will be set (@IsWeighted>0)
								-- For key boarding courses, if all grade books are satisfied set credits earned, attempted and completed
                    SET @CreditsAttempted = (
                                              SELECT    Credits
                                              FROM      arReqs
                                              WHERE     ReqId = @reqid
                                            );	
									
                    IF @hasallscoresbeenpostedforrequiredcomponents = 1
                        BEGIN
                            SET @FinAidCredits = (
                                                   SELECT   FinAidCredits
                                                   FROM     arReqs
                                                   WHERE    ReqId = @reqid
                                                 );	
                            SET @CreditsEarned = (
                                                   SELECT   Credits
                                                   FROM     arReqs
                                                   WHERE    ReqId = @reqid
                                                 );
                            SET @Completed = 1;
                        END;
                END;
			
			-- DE748 Name: ROSS: Completed field should also check for the Must Pass property of the work unit. 
            IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
                AND @IsGradeBookNotSatisified >= 1
                BEGIN
                    SET @Completed = 0;
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;
                END;
			
			--DE738 Name: ROSS: Progress Report not taking care of courses that are not weighted. 
            IF (
                 LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
                 AND @Completed = 1
                 AND @FinalScore IS NULL
                 AND @FinalGradeDesc IS NULL
               )
                BEGIN
                    SET @CreditsAttempted = @CreditsAttempted; 
                    SET @CreditsEarned = @CreditsAttempted; 
                    SET @FinAidCreditsEarned = @FinAidCredits;
                END;

					-- In Ross Example : Externship, the student may not have completed the course but once he attempts a work unit
					-- we need to take the credits as attempted
            IF (
                 LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
                 AND @Completed = 0
                 AND @FinalScore IS NULL
                 AND @FinalGradeDesc IS NULL
               )
                BEGIN
                    DECLARE @rowcount4 INT;
                    SET @rowcount4 = (
                                       SELECT   COUNT(*)
                                       FROM     arGrdBkResults
                                       WHERE    StuEnrollId = @StuEnrollId
                                                AND ClsSectionId = @ClsSectionId
                                                AND Score IS NOT NULL
                                     );
                    IF @rowcount4 >= 1
                        BEGIN
										-- Print ''Gets in to if''
                            SET @CreditsAttempted = (
                                                      SELECT    Credits
                                                      FROM      arReqs
                                         WHERE     ReqId = @reqid
                                                    );
                            SET @CreditsEarned = 0;
                            SET @FinAidCreditsEarned = 0;
                        END;
                    ELSE
                        BEGIN
                            SET @rowcount4 = (
                                               SELECT   COUNT(*)
                                               FROM     arGrdBkConversionResults
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND ReqId = @reqid
                                                        AND TermId = @TermId
                                                        AND Score IS NOT NULL
                                             );
                            IF @rowcount4 >= 1
                                BEGIN
                                    SET @CreditsAttempted = (
                                                              SELECT    Credits
                                                              FROM      arReqs
                                                              WHERE     ReqId = @reqid
                                                            );
                                    SET @CreditsEarned = 0;
                                    SET @FinAidCreditsEarned = 0;
                                END;
                        END;

							--For Externship Attendance						
                    IF @sysComponentTypeId = 544
                        BEGIN
                            SET @rowcount4 = (
                                               SELECT   COUNT(*)
                                               FROM     arExternshipAttendance
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND HoursAttended >= 1
                                             );
                            IF @rowcount4 >= 1
                                BEGIN
                                    SET @CreditsAttempted = (
                                                              SELECT    Credits
                                                              FROM      arReqs
                                                              WHERE     ReqId = @reqid
                                                            );
                                    SET @CreditsEarned = 0;
                                    SET @FinAidCreditsEarned = 0;
                                END;

                        END;
				
                END;
			/************************************************* Changes for Build 2816 *********************/
			-- If the final grade is not null the final grade will over ride current grade 
            IF @FinalGradeDesc IS NOT NULL
                BEGIN
                    SET @CurrentGrade = @FinalGradeDesc; 
				
                END;

			/************************* Case added for brownson starts here ******************/
			-- For Letter Grade Schools, if any one of work unit is attempted and if no final grade is posted then 
			-- set credit attempted
            DECLARE @IsScorePostedForAnyWorkUnit INT; -- If value>=1 then score was posted
            SET @IsScorePostedForAnyWorkUnit = (
                                                 SELECT COUNT(*)
                                                 FROM   arGrdBkResults
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND ClsSectionId = @ClsSectionId
                                                        AND Score IS NOT NULL
                                               );
            IF (
                 LOWER(LTRIM(RTRIM(@GradesFormat))) = ''letter''
           AND @FinalGradeDesc IS NULL
                 AND @IsScorePostedForAnyWorkUnit >= 1
               )
                BEGIN
                    SET @Completed = 0;
                    SET @CreditsAttempted = (
                                              SELECT    Credits
                                              FROM      arReqs
                                              WHERE     ReqId = @reqid
                                            );
                    SET @CreditsEarned = 0;
                    SET @FinAidCreditsEarned = 0;				
                END;
			/************************* Case added for brownson ends here ******************/
			
			-- DE 996 Transfer Grades has completed set to no even when the credits were earned.
			-- If Credits was earned set completed to yes
			-- DE 996 Transfer Grades has completed set to no even when the credits were earned.
			-- If Credits was earned set completed to yes
            IF @IsCreditsEarned = 1
                BEGIN
                    IF (
                         @IsGradeBookNotSatisified = 0
                         AND @IsWeighted > 0 AND (@sysComponentTypeId IN (503,500) OR (@IsResultCompleted = 1 AND @sysComponentTypeId NOT IN (500,503)))
                       ) -- If all Grade books are satisfied and all courses are weighted
                        BEGIN
                            SET @Completed = 1;
                        END;
                    ELSE
                        BEGIN
                            SET @Completed = 0;
                        END;
                END;
			

			-- For Letter grade schools no need to check for grade books satisifed condition
            IF LOWER(LTRIM(RTRIM(@GradesFormat))) = ''letter''
                BEGIN
                    IF @IsCreditsEarned = 1
                        BEGIN
                            SET @Completed = 1;
                        END;
                END;

					-- numeric and non ross schools
			-- no need to check if student satisfied grade book and weights
			
            IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
                AND @ShowROSSOnlyTabsForStudent_Value = 0
                BEGIN
                    IF @IsCreditsEarned = 1
                        OR (
                             @FinalScore IS NOT NULL
                             AND @IsPass = 1
                           )
                        BEGIN
                            SET @Completed = 1;		
                        END;
                END;

			-- Modified by Balaji on 11/8/2010
			-- This condition does not apply for key boarding courses, as no final score or grade is posted and
			-- isCreditsEarned will always be NULL
            IF @IsCreditsEarned IS NULL
                BEGIN
                    SET @Completed = 0;
					-- Only for Key boarding courses
                    IF (
                         @IsGradeBookNotSatisified = 0
                         AND @IsWeighted = 0
                         AND @FinalScore IS NULL
                         AND @FinalGradeDesc IS NULL
                         AND LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
                       )
                        AND @CountComponentsThatHasScores >= 1
                        BEGIN
                            SET @CreditsAttempted = (
                                                      SELECT    Credits
                                                      FROM      arReqs
                                                      WHERE     ReqId = @reqid
                                                    );	
                            IF @hasallscoresbeenpostedforrequiredcomponents = 1
                                BEGIN
                                    SET @FinAidCredits = (
                                                           SELECT   FinAidCredits
                                                           FROM     arReqs
                                                           WHERE    ReqId = @reqid
                                         );	
                                    SET @CreditsEarned = (
                                                           SELECT   Credits
                                                           FROM     arReqs
                                                           WHERE    ReqId = @reqid
                                                         );
													
                                    SET @Completed = 1;
                                END;
                        END;
                END;

			

			-- DE1148 
            IF @Completed = 1
                AND @IsCreditsEarned = 1
                BEGIN
                    SET @CreditsEarned = (
                                           SELECT   Credits
                                           FROM     arReqs
                                           WHERE    ReqId = @reqid
                                         );
                    SET @FinAidCreditsEarned = @FinAidCredits;
                END;

            DECLARE @varGradeRounding VARCHAR(3);
            DECLARE @roundfinalscore DECIMAL(18,4);
            SET @varGradeRounding = (
                                      SELECT    dbo.GetAppSettingValue(45,@StuEnrollCampusId)
                                    );
			
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
            IF ( LOWER(@varGradeRounding) = ''yes'' )
                BEGIN
                    IF @FinalScore IS NOT NULL
                        BEGIN
                            SET @FinalScore = ROUND(@FinalScore,0);
                        END;
                    IF @CurrentScore IS NOT NULL
                        BEGIN
                            SET @CurrentScore = ROUND(@CurrentScore,0);
                        END;
                END;

							
            IF @CourseComponentsThatNeedsToBeScored >= 1
                AND @CountComponentsThatHasScores = 0
                BEGIN
                    SET @CreditsAttempted = 0;
                END;

            IF @CountComponentsThatHasScores >= 1
                BEGIN
                    SET @CreditsAttempted = (
                                              SELECT    Credits
                                              FROM      arReqs
                                              WHERE     ReqId = @reqid
                                            );
                END;
			
            IF LOWER(LTRIM(RTRIM(@GradesFormat))) = ''letter''
                AND @FinalGrade IS NOT NULL
                AND @CourseComponentsThatNeedsToBeScored = 0
                AND @IsCreditsAttempted = 1
                BEGIN
                    SET @CreditsAttempted = (
                                              SELECT    Credits
                                              FROM      arReqs
                                              WHERE     ReqId = @reqid
                                            );
                END;
			
            IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
                AND @FinalScore IS NOT NULL
                AND @CourseComponentsThatNeedsToBeScored = 0
                AND @IsCreditsAttempted = 1
                BEGIN
                    SET @CreditsAttempted = (
                                              SELECT    Credits
                                              FROM      arReqs
                                              WHERE     ReqId = @reqid
                                            );
                END;
			
					
			-- Check if student passed the course
            IF LOWER(LTRIM(RTRIM(@GradesFormat))) = ''letter''
                AND @FinalGrade IS NOT NULL
                AND @IsPass = 0
                BEGIN
                    SET @Completed = 0;
                    IF @IsCreditsEarned IS NULL
                        BEGIN
                            SET @CreditsEarned = 0;
                        END;
                END;
			
			---- Unitek/Ross : If the externship component was not satisfied then set completed to no
			--	-- Unitek : If the externship component was not satisfied then set completed to no
			--IF @sysComponentTypeId = 544 OR @sysComponentTypeId = 500 OR @sysComponentTypeId =503
			--		BEGIN
			--			IF @IsGradeBookNotSatisified>=1 -- work unit comp not satisfied
			--				BEGIN
			--					SET @Completed = 0
			--				END
			--			ELSE
			--				BEGIN
			--					SET @Completed = 1
			--				end
			--		END
			
            DECLARE @CountWorkUnitsNotSatisfied_500503544 INT;
	-- Work unit not satisfied		
            IF LOWER(LTRIM(RTRIM(@SetGradeBookAt))) = ''courselevel''
                BEGIN
		
                    CREATE TABLE #Temp1
                        (
                         Id UNIQUEIDENTIFIER
                        ,StuEnrollId UNIQUEIDENTIFIER
                        ,TermId UNIQUEIDENTIFIER
                        ,GradeBookDescription VARCHAR(50)
                        ,Number INT
                        ,GradeBookSysComponentTypeId INT
                        ,GradeBookScore DECIMAL(18,2)
                        ,MinResult DECIMAL(18,2)
                        ,GradeComponentDescription VARCHAR(50)
                        ,RowNumber INT
                        ,ClsSectionId UNIQUEIDENTIFIER
                        );
                    DECLARE @Id UNIQUEIDENTIFIER
                       ,@Descrip VARCHAR(50)
                       ,@Number INT
                       ,@GrdComponentTypeId INT
                       ,@Counter INT
                       ,@times INT;
                    DECLARE @MinResult DECIMAL(18,2)
                       ,@GrdComponentDescription VARCHAR(50);
		
                    SET @Counter = 0;
		
                    DECLARE @TermStartDate1 DATETIME;
                    SET @TermStartDate1 = (
                                            SELECT  StartDate
                                            FROM    arTerm
                                            WHERE   TermId = @TermId
                                          );
		
                    CREATE TABLE #temp2
                        (
                         ReqId UNIQUEIDENTIFIER
                        ,EffectiveDate DATETIME
                        );
                    INSERT  INTO #temp2
                            SELECT  ReqId
                                   ,MAX(EffectiveDate) AS EffectiveDate
                            FROM    arGrdBkWeights
                            WHERE   ReqId = @reqid
                                    AND EffectiveDate <= @TermStartDate1
                            GROUP BY ReqId;
		
                    DECLARE getUsers_Cursor CURSOR
                    FOR
                        SELECT  *
                               ,ROW_NUMBER() OVER ( PARTITION BY @StuEnrollId,@TermId,SysComponentTypeId ORDER BY SysComponentTypeId, Descrip ) AS rownumber
                        FROM    (
                                  SELECT DISTINCT
                                            ISNULL(GD.InstrGrdBkWgtDetailId,NEWID()) AS ID
                                           ,GC.Descrip
                                           ,GD.Number
                                           ,GC.SysComponentTypeId
                                           ,( CASE WHEN GC.SysComponentTypeId IN ( 500,503,504,544 ) THEN GD.Number
                                                   ELSE (
                                                          SELECT    MIN(MinVal)
                                                          FROM      arGradeScaleDetails GSD
                                                                   ,arGradeSystemDetails GSS
                                                          WHERE     GSD.GrdSysDetailId = GSS.GrdSysDetailId
                   AND GSS.IsPass = 1
                                                                    AND GSD.GrdScaleId = CS.GrdScaleId
                                                        )
                                              END ) AS MinResult
                                           ,S.Resource AS GradeComponentDescription
                                           ,CS.ClsSectionId
				--,MaxEffectiveDatesByCourse.ReqId 
                                  FROM      arGrdComponentTypes GC
                                           ,(
                                              SELECT    *
                                              FROM      arGrdBkWgtDetails
                                              WHERE     InstrGrdBkWgtId IN ( SELECT t1.InstrGrdBkWgtId
                                                                             FROM   arGrdBkWeights t1
                                                                                   ,#temp2 t2
                                                                             WHERE  t1.ReqId = t2.ReqId
                                                                                    AND t1.EffectiveDate = t2.EffectiveDate )
                                            ) GD
                                           ,arGrdBkWeights GW
                                           ,arReqs R
                                           ,arClassSections CS
                                           ,syResources S
                                           ,arResults RES
                                           ,arTerm T
                                  WHERE     GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                            AND GD.InstrGrdBkWgtId = GW.InstrGrdBkWgtId
                                            AND GW.ReqId = R.ReqId
                                            AND R.ReqId = CS.ReqId
                                            AND CS.TermId = @TermId
                                            AND RES.TestId = CS.ClsSectionId
                                            AND RES.StuEnrollId = @StuEnrollId
                                            AND GD.Number > 0
                                            AND GC.SysComponentTypeId = S.ResourceID
                                            AND CS.TermId = T.TermId
                                            AND R.ReqId = @reqid
                                ) dt
                        ORDER BY SysComponentTypeId
                               ,rownumber;
                    OPEN getUsers_Cursor;
                    FETCH NEXT FROM getUsers_Cursor
		INTO @Id,@Descrip,@Number,@GrdComponentTypeId,@MinResult,@GrdComponentDescription,@ClsSectionId,@rownumber;
                    SET @Counter = 0;
                    DECLARE @Score DECIMAL(18,2)
                       ,@GrdCompDescrip VARCHAR(50);
                    WHILE @@FETCH_STATUS = 0
                        BEGIN
                            PRINT @Number;
                            SET @times = 1;
			
			--if (@GrdComponentTypeId = 500 or @GrdComponentTypeId=503 or @GrdComponentTypeId=544)
			--	begin
			--		set @GrdCompDescrip = @Descrip
			--			set @Score = (select Top 1 Score from arGrdBkResults where StuEnrollId=@StuEnrollId and InstrGrdBkWgtDetailId=@Id and ResNum=@times and ClsSectionId=@ClsSectionId) 
			--			if @Score is NULL
			--				begin
			--					set @Score = (select Top 1 Score from arGrdBkResults where StuEnrollId=@StuEnrollId and InstrGrdBkWgtDetailId=@Id and ResNum=(@times-1) and ClsSectionId=@ClsSectionId) 	
			--				end
			--			insert into #temp1 values(@Id,@StuEnrollId,@TermId,
			--			@GrdCompDescrip,@Number,@GrdComponentTypeId,@Score,@MinResult,@GrdComponentDescription,@rownumber,@ClsSectionId)
			--	end
                            IF (
                                 @GrdComponentTypeId = 500
 OR @GrdComponentTypeId = 503
                                 OR @GrdComponentTypeId = 544
                               )
                                BEGIN
                                    SET @GrdCompDescrip = @Descrip;
                                    IF (
                                         @GrdComponentTypeId = 500
                                         OR @GrdComponentTypeId = 503
                                       )
                                        BEGIN
                                            SET @Score = (
                                                           SELECT   SUM(Score)
                                                           FROM     arGrdBkResults
                                                           WHERE    StuEnrollId = @StuEnrollId
                                                                    AND InstrGrdBkWgtDetailId = @Id
                                                                    AND ClsSectionId = @ClsSectionId
                                                         ); 
                                        END;
                                    IF ( @GrdComponentTypeId = 544 )
                                        BEGIN
                                            SET @Score = (
                                                           SELECT   SUM(HoursAttended)
                                                           FROM     arExternshipAttendance
                                                           WHERE    StuEnrollId = @StuEnrollId
                                                         ); 
                                        END;
                                    INSERT  INTO #Temp1
                                    VALUES  ( @Id,@StuEnrollId,@TermId,@GrdCompDescrip,@Number,@GrdComponentTypeId,@Score,@MinResult,@GrdComponentDescription,
                                              @rownumber,@ClsSectionId );
                                END;
                            ELSE
                                BEGIN
                                    WHILE @times <= @Number
                                        BEGIN
                                            PRINT @times;
							
                                            IF @Number > 1
                                                BEGIN
                                                    SET @GrdCompDescrip = @Descrip + CAST(@times AS CHAR);
                                                    SET @Score = (
                                                                   SELECT   Score
                                                                   FROM     arGrdBkResults
                                                                   WHERE    StuEnrollId = @StuEnrollId
                                                                            AND InstrGrdBkWgtDetailId = @Id
                                                                            AND ResNum = @times
                                                                            AND ClsSectionId = @ClsSectionId
                                                                 );
												  
																	
                                                    SET @rownumber = @times;
                                                END;
                                            ELSE
                                                BEGIN
                                                    SET @GrdCompDescrip = @Descrip;
                                                    SET @Score = (
                                                                   SELECT TOP 1
                                                                            Score
                                                                   FROM     arGrdBkResults
                                                                   WHERE    StuEnrollId = @StuEnrollId
                                                                            AND InstrGrdBkWgtDetailId = @Id
                                                                            AND ResNum = @times
                                                                            AND ClsSectionId = @ClsSectionId
                                                                 ); 
                                                    IF @Score IS NULL
                                                        BEGIN
                                                            SET @Score = (
                                                                           SELECT TOP 1
                                                                                    Score
                                                                           FROM     arGrdBkResults
                                                                           WHERE    StuEnrollId = @StuEnrollId
                                                                                    AND InstrGrdBkWgtDetailId = @Id
                                                                                    AND ResNum = ( @times - 1 )
                                                                                    AND ClsSectionId = @ClsSectionId
                                                                         ); 	
                                                        END;
                                                END;
                                            INSERT  INTO #Temp1
                                            VALUES  ( @Id,@StuEnrollId,@TermId,@GrdCompDescrip,@Number,@GrdComponentTypeId,@Score,@MinResult,
                                                      @GrdComponentDescription,@rownumber,@ClsSectionId );
							
                                            SET @times = @times + 1;
                                        END;
                                END;
                            FETCH NEXT FROM getUsers_Cursor
			INTO @Id,@Descrip,@Number,@GrdComponentTypeId,@MinResult,@GrdComponentDescription,@ClsSectionId,@rownumber;
                        END;
                    CLOSE getUsers_Cursor;
                    DEALLOCATE getUsers_Cursor;
		
			-- Changes made on 12/28/2010 starts here
		--Create table #Temp3(GetCountOfWorkUnit_500503544_NotSatisfied INT)
		--INSERT INTO #Temp3 
		--SELECT COUNT(*) AS RowNumber FROM 
                    SELECT  *
                    INTO    #temp3
                    FROM    (
                              SELECT    *
                              FROM      #Temp1 --where GradeBookSysComponentTypeId=501
		--order by 
		--	GradeBookSysComponentTypeId,GradeBookDescription,RowNumber
                              UNION
                              SELECT    GBWD.InstrGrdBkWgtDetailId
                                       ,SE.StuEnrollId
                                       ,T.TermId
                                       ,GCT.Descrip AS GradeBookDescription
                                       ,GBWD.Number
                                       ,GCT.SysComponentTypeId
                                       ,GBCR.Score
                                       ,GBCR.MinResult
                                       ,SYRES.Resource
                                       ,ROW_NUMBER() OVER ( PARTITION BY SE.StuEnrollId,T.TermId,GCT.SysComponentTypeId ORDER BY GCT.SysComponentTypeId, GCT.Descrip ) AS rownumber
                                       ,(
                                          SELECT TOP 1
                                                    ClsSectionId
                                          FROM      arClassSections
                                          WHERE     TermId = T.TermId
                                                    AND ReqId = R.ReqId
                                        ) AS ClsSectionId
                              FROM      arGrdBkConversionResults GBCR
                              INNER JOIN arStuEnrollments SE ON GBCR.StuEnrollId = SE.StuEnrollId
                              INNER JOIN (
                                           SELECT   StudentId
                                                   ,FirstName
                                                   ,LastName
                                                   ,MiddleName
                                           FROM     arStudent
                                         ) S ON S.StudentId = SE.StudentId
                              INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                              INNER JOIN arTerm T ON GBCR.TermId = T.TermId
                              INNER JOIN arReqs R ON GBCR.ReqId = R.ReqId
                              INNER JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBCR.GrdComponentTypeId
                              INNER JOIN arGrdBkWgtDetails GBWD ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                                   AND GBWD.GrdComponentTypeId = GBCR.GrdComponentTypeId
                              INNER JOIN arGrdBkWeights GBW ON GBWD.InstrGrdBkWgtId = GBW.InstrGrdBkWgtId
                                                               AND GBCR.ReqId = GBW.ReqId
                              INNER JOIN (
                                           SELECT   ReqId
                                                   ,MAX(EffectiveDate) AS EffectiveDate
                                           FROM     arGrdBkWeights
                                           GROUP BY ReqId
                                         ) AS MaxEffectiveDatesByCourse ON GBCR.ReqId = MaxEffectiveDatesByCourse.ReqId
                              INNER JOIN (
                                           SELECT   Resource
                                                   ,ResourceID
                                           FROM     syResources
                                           WHERE    ResourceTypeID = 10
                                         ) SYRES ON SYRES.ResourceID = GCT.SysComponentTypeId
                              INNER JOIN syCampuses C ON SE.CampusId = C.CampusId
                              WHERE     MaxEffectiveDatesByCourse.EffectiveDate <= T.StartDate
                                        AND SE.StuEnrollId = @StuEnrollId
                                        AND T.TermId = @TermId
                                        AND --R.ReqId = @ReqId and 
                                        (
                                          @sysComponentTypeId IS NULL
                                          OR GCT.SysComponentTypeId IN ( SELECT Val
                                                                         FROM   MultipleValuesForReportParameters(@sysComponentTypeId,'','',1) )
                                        )
								--and GCT.SysComponentTypeId=501
                            ) derT1; --WHERE GradeBookSysComponentTypeId IN (500,503,544) AND (GradeBookScore IS NULL OR MinResult>GradeBookScore)
		--order by 
		--	GradeBookSysComponentTypeId,GradeBookDescription,RowNumber
		--SELECT * FROM #temp3
		
                    DECLARE @DoesCourseHaveClinicalComponents INT
                       ,@DoesCourseHaveNonClinicalComponents INT;
                    SET @DoesCourseHaveClinicalComponents = (
                                                              SELECT    COUNT(*)
                                                              FROM      #temp3
                                                              WHERE     GradeBookSysComponentTypeId IN ( 500,503,544 )
                                                            );
                    SET @DoesCourseHaveNonClinicalComponents = (
                                                                 SELECT COUNT(*)
                                                                 FROM   #temp3
                                                                 WHERE  GradeBookSysComponentTypeId NOT IN ( 500,503,544 )
                                                               );
                    SET @CountWorkUnitsNotSatisfied_500503544 = (
                                                                  SELECT    COUNT(*)
                                                                  FROM      #temp3
                                                                  WHERE     GradeBookSysComponentTypeId IN ( 500,503,544 )
                                                                            AND (
                                                                                  GradeBookScore IS NULL
                                                                                  OR MinResult > GradeBookScore
                                                                                )
                                                                );
		
		
	
		
                    IF (
                         @DoesCourseHaveClinicalComponents >= 1
                         AND @DoesCourseHaveNonClinicalComponents >= 1
                       )
                        BEGIN 
				-- There is a combination
				-- First check if clinical has been satisfied
                            IF @CountWorkUnitsNotSatisfied_500503544 >= 1
                                BEGIN
                                    SET @Completed = 0;  --If Clinical comp not satisfied course is incomplete
                                END; 
				
				-- if course has been satisfied
                            IF @CountWorkUnitsNotSatisfied_500503544 = 0
                                BEGIN
							
						-- Check if final score is posted for letter grade schools
                                    IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
                                        AND @FinalScore IS NULL
                                        BEGIN
                                            SET @Completed = 0;
                                        END; 
                                    IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
                                        AND @FinalScore IS NOT NULL
                                        AND @IsPass = 1
                                        BEGIN

                                            SET @Completed = 1;
                                        END; 
						-- Check if final grade is posted for numeric grade schools
                                    IF LOWER(LTRIM(RTRIM(@GradesFormat))) = ''letter''
                                        AND @FinalGradeDesc IS NULL
                                        BEGIN
                                            SET @Completed = 0;
                                        END; 
                                    IF LOWER(LTRIM(RTRIM(@GradesFormat))) = ''letter''
                                        AND @FinalGradeDesc IS NOT NULL
                                        AND @IsPass = 1
                                        BEGIN
                                            SET @Completed = 1;
                                        END; 
							-- For Keyboarding course  no final score is posted but completed should be set to yes
                                    IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
                                        AND @FinalScore IS NULL
                                        AND @ShowROSSOnlyTabsForStudent_Value = 1
                                        BEGIN
                                            SET @Completed = 1;
                                        END; 
                                END;
				
                            IF @IsGradeBookNotSatisified >= 1
                                BEGIN
             SET @Completed = 0;
                                END;
					
                        END;
			-- If course has only clinic components
                    IF (
                         @DoesCourseHaveClinicalComponents >= 1
                         AND @DoesCourseHaveNonClinicalComponents = 0
                       )
                        BEGIN 
                            IF @CountWorkUnitsNotSatisfied_500503544 >= 1
                                BEGIN
                                    SET @Completed = 0;  --If Clinical comp not satisfied course is incomplete
                                END; 
                            IF @CountWorkUnitsNotSatisfied_500503544 = 0
                                BEGIN
                                    SET @Completed = 1;  --If Clinical comp not satisfied course is incomplete
                                END; 
                        END;
			--If course has only non clinical courses
                    IF (
                         @DoesCourseHaveClinicalComponents = 0
                         AND @DoesCourseHaveNonClinicalComponents >= 1
                       )
                        BEGIN 
                            IF @IsGradeBookNotSatisified >= 1
                                BEGIN
                                    SET @Completed = 0;
								
                                END;
                            IF @IsGradeBookNotSatisified = 0
                                AND @IsPass = 1
                                BEGIN
                                    SET @Completed = 1;
                                END;
                        END;
			-- Changes made on 12/28/2010 ends here
			
			
                    DROP TABLE #temp3;
                    DROP TABLE #temp2;
                    DROP TABLE #Temp1;
                END;
					
			
			-- Unitek/Ross : If the externship component was not satisfied then set completed to no
			-- Unitek : If the externship component was not satisfied then set completed to no
			--if  LOWER(LTRIM(RTRIM(@SetGradeBookAt))) = ''courselevel''
			--BEGIN 
			--	IF @CountWorkUnitsNotSatisfied_500503544>=1 OR @IsGradeBookNotSatisified>=1 
			--		BEGIN
			--			SET @Completed = 0
			--		END
			--	ELSE
			--		BEGIN
			--			SET @Completed = 1
			--			IF (@FinalScore IS NOT NULL OR @FinalGradeDesc IS NOT NULL) AND @isPass=0
			--			begin
			--				SET @Completed = 0
			--			END
			--			IF (@FinalScore IS NULL OR @FinalGradeDesc IS NULL) and LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter'' AND @CourseComponentsThatNeedsToBeScored>=1 and @CountComponentsThatHasScores=0
			--				BEGIN
			--					SET @Completed=0
			--				END
			--			IF (@FinalScore IS NULL OR @FinalGradeDesc IS NULL) and LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter'' and (@SysComponentTypeId IS NULL OR @SysComponentTypeId <> 544) and @CountComponentsThatHasScores=0 --AND @CourseComponentsThatNeedsToBeScored=0
 
			--				BEGIN
			--					SET @Completed=0
			--				END
			--			IF (@FinalGradeDesc IS NULL) and LOWER(LTRIM(RTRIM(@GradesFormat))) = ''letter'' and @CountComponentsThatHasScores=0 --AND @CourseComponentsThatNeedsToBeScored=0 
			--				BEGIN
			--					SET @Completed=0
			--				end
			--		END
			--end
			
            DELETE  FROM syCreditSummary
            WHERE   StuEnrollId = @StuEnrollId
                    AND TermId = @TermId
                    AND ReqId = @reqid
                    AND ClsSectionId = @ClsSectionId;

            INSERT  INTO syCreditSummary
            VALUES  ( @StuEnrollId,@TermId,@TermDescrip,@reqid,@CourseCodeDescrip,@ClsSectionId,ISNULL(@CreditsEarned,0),@CreditsAttempted,@CurrentScore,
                      @CurrentGrade,@FinalScore,@FinalGradeDesc,@Completed,@FinalGPA,@Product_WeightedAverage_Credits_GPA,@Count_WeightedAverage_Credits,
                      @Product_SimpleAverage_Credits_GPA,@Count_SimpleAverage_Credits,''sa'',GETDATE(),@ComputedSimpleGPA,@ComputedWeightedGPA,@CourseCredits,NULL,
                      NULL,@FinAidCreditsEarned,NULL,NULL,@TermStartDate );

            DECLARE @wCourseCredits DECIMAL(18,2)
               ,@wWeighted_GPA_Credits DECIMAL(18,2)
               ,@sCourseCredits DECIMAL(18,2)
               ,@sSimple_GPA_Credits DECIMAL(18,2);
			-- For weighted average
            SET @ComputedWeightedGPA = 0;
            SET @ComputedSimpleGPA = 0;
            SET @wCourseCredits = (
                                    SELECT  SUM(coursecredits)
                                    FROM    syCreditSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND TermId = @TermId
                                            AND FinalGPA IS NOT NULL
                                  );
            SET @wWeighted_GPA_Credits = (
                                           SELECT   SUM(coursecredits * FinalGPA)
                                           FROM     syCreditSummary
                                           WHERE    StuEnrollId = @StuEnrollId
                                                    AND TermId = @TermId
                                                    AND FinalGPA IS NOT NULL
                                         );
			
            IF @wCourseCredits >= 1
                BEGIN
                    SET @ComputedWeightedGPA = @wWeighted_GPA_Credits / @wCourseCredits;
                END;

			--For Simple Average
            SET @sCourseCredits = (
                                    SELECT  COUNT(*)
                                    FROM    syCreditSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND TermId = @TermId
                                            AND FinalGPA IS NOT NULL
                                  );
            SET @sSimple_GPA_Credits = (
                                         SELECT SUM(FinalGPA)
                                         FROM   syCreditSummary
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND TermId = @TermId
                                                AND FinalGPA IS NOT NULL
                                       );
            IF @sCourseCredits >= 1
                BEGIN
                    SET @ComputedSimpleGPA = @sSimple_GPA_Credits / @sCourseCredits;
                END; 
					
			--CumulativeGPA
            DECLARE @cumCourseCredits DECIMAL(18,2)
               ,@cumWeighted_GPA_Credits DECIMAL(18,2)
               ,@cumWeightedGPA DECIMAL(18,2);
            SET @cumWeightedGPA = 0;
            SET @cumCourseCredits = (
                                      SELECT    SUM(coursecredits)
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = @StuEnrollId
                                                AND FinalGPA IS NOT NULL
                                    );
            SET @cumWeighted_GPA_Credits = (
                                             SELECT SUM(coursecredits * FinalGPA)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                           );
			
            IF @cumCourseCredits >= 1
                BEGIN
                    SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                END; 
			
			--CumulativeSimpleGPA
            DECLARE @cumSimpleCourseCredits DECIMAL(18,2)
               ,@cumSimple_GPA_Credits DECIMAL(18,2)
               ,@cumSimpleGPA DECIMAL(18,2);
            SET @cumSimpleGPA = 0;
            SET @cumSimpleCourseCredits = (
                                            SELECT COUNT(coursecredits)
                                            FROM    syCreditSummary
                                            WHERE   StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                          );
            SET @cumSimple_GPA_Credits = (
                                           SELECT   SUM(FinalGPA)
                                           FROM     syCreditSummary
                                           WHERE    StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                         );
			
            IF @cumSimpleCourseCredits >= 1
                BEGIN
                    SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                END; 
						
			--Average calculation
            DECLARE @termAverageSum DECIMAL(18,2)
               ,@CumAverage DECIMAL(18,2)
               ,@cumAverageSum DECIMAL(18,2)
               ,@cumAveragecount INT;
			
			-- Term Average
            SET @TermAverageCount = (
                                      SELECT    COUNT(*)
                                      FROM      syCreditSummary
                                      WHERE     StuEnrollId = @StuEnrollId 
									--and Completed=1 
                                                AND TermId = @TermId
                                                AND FinalScore IS NOT NULL
                                    );
            SET @termAverageSum = (
                                    SELECT  SUM(FinalScore)
                                    FROM    syCreditSummary
                                    WHERE   StuEnrollId = @StuEnrollId 
									--and Completed=1 
                                            AND TermId = @TermId
                                            AND FinalScore IS NOT NULL
                                  );
            SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
            SET @cumAveragecount = (
                                     SELECT COUNT(*)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId 
									--and Completed=1 
                                            AND FinalScore IS NOT NULL
                                   );
            SET @cumAverageSum = (
                                   SELECT   SUM(FinalScore)
                                   FROM     syCreditSummary
                                   WHERE    StuEnrollId = @StuEnrollId
                                            AND 
									--Completed=1 and 
                                            FinalScore IS NOT NULL
                                 );
            SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
            UPDATE  syCreditSummary
            SET     TermGPA_Simple = @ComputedSimpleGPA
                   ,TermGPA_Weighted = @ComputedWeightedGPA
                   ,Average = @TermAverage
            WHERE   StuEnrollId = @StuEnrollId
                    AND TermId = @TermId; 
			
			--Update Cumulative GPA
            UPDATE  syCreditSummary
            SET     CumulativeGPA = @cumWeightedGPA
                   ,CumulativeGPA_Simple = @cumSimpleGPA
                   ,CumAverage = @CumAverage
            WHERE   StuEnrollId = @StuEnrollId;
			
		 
														
            SET @PrevStuEnrollId = @StuEnrollId; 
            SET @PrevTermId = @TermId; 
            SET @PrevReqId = @reqid;

            FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@TermStartDate,@reqid,@CourseCodeDescrip,@FinalScore,@FinalGrade,
                @sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,@FinAidCredits, @IsResultCompleted;
        END;
    CLOSE GetCreditsSummary_Cursor;
    DEALLOCATE GetCreditsSummary_Cursor;
 --==========================================================================================
-- END  --  TRIGGER TR_InsertCreditSummary  --  AFTER UPDATE  
--==========================================================================================


    SET NOCOUNT OFF; 


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[usp_AR_ArGrdBkResults_Insert]'
GO
IF OBJECT_ID(N'[dbo].[usp_AR_ArGrdBkResults_Insert]', 'P') IS NULL
EXEC sp_executesql N'
/* 
US4257 Course marked as incomplete when score of one of the component is removed

CREATED: 
7/24/2013 TT

PURPOSE: 
Insert arGrdBkResults


MODIFIED:


*/
CREATE   PROCEDURE [dbo].[usp_AR_ArGrdBkResults_Insert]
    @GrdBkResultId UNIQUEIDENTIFIER
   ,@ClsSectionId UNIQUEIDENTIFIER
   ,@StuEnrollId UNIQUEIDENTIFIER
   ,@InstrGrdBkWgtDetailId UNIQUEIDENTIFIER
   ,@Score DECIMAL(6, 2)
   ,@Comments VARCHAR(50)
   ,@ResNum INT = 0
   ,@ModUser VARCHAR(50)
   ,@IsCompGraded BIT
   ,@DateCompleted DATE = NULL
   ,@PostDate DATE = NULL
   ,@Adjustment INT = NULL
AS
    BEGIN

        DECLARE @NewResNum INT = 0;
        SET @NewResNum = ( ISNULL((
                                  SELECT MAX(ResNum)
                                  FROM   arGrdBkResults
                                  WHERE  StuEnrollId = @StuEnrollId
                                         AND ClsSectionId = @ClsSectionId
                                         AND InstrGrdBkWgtDetailId = @InstrGrdBkWgtDetailId
                                  )
                                 ,0
                                 )
                         ) + 1;



        SET NOCOUNT ON;


        --new code to insert arGrdBkResults for clinic services by bulk
        IF ( @Adjustment > 1 )
            BEGIN

                DECLARE @ToInsert TABLE
                    (
                        [GrdBkResultId] UNIQUEIDENTIFIER
                       ,[ClsSectionId] UNIQUEIDENTIFIER
                       ,[InstrGrdBkWgtDetailId] UNIQUEIDENTIFIER
                       ,[Score] DECIMAL(6, 2)
                       ,[Comments] VARCHAR(50)
                       ,[StuEnrollId] UNIQUEIDENTIFIER
                       ,[ModUser] VARCHAR(50)
                       ,[ModDate] DATETIME
                       ,[ResNum] INT
                       ,[PostDate] SMALLDATETIME
                       ,[IsCompGraded] BIT
                       ,[isCourseCredited] BIT
                       ,[DateCompleted] DATE
                    );

                --DECLARE @Numbers TABLE
                --    (
                --        num INT IDENTITY(1, 1)
                --    );


                INSERT INTO @ToInsert (
                                      GrdBkResultId
                                     ,ClsSectionId
                                     ,StuEnrollId
                                     ,InstrGrdBkWgtDetailId
                                     ,Score
                                     ,Comments
                                     ,ResNum
                                     ,ModUser
                                     ,ModDate
                                     ,PostDate
                                     ,IsCompGraded
                                     ,DateCompleted
                                      )
                VALUES ( @GrdBkResultId
                                       -- GrdBkResultId - uniqueidentifier
                        ,@ClsSectionId
                                       -- ClsSectionId - uniqueidentifier
                        ,@StuEnrollId
                                       -- StuEnrollId - uniqueidentifier
                        ,@InstrGrdBkWgtDetailId
                                       -- InstrGrdBkWgtDetailId- uniqueidentifier
                        ,@Score
                                       -- Score  - decimal
                        ,@Comments
                                       -- Comments - varchar
                        ,@Adjustment
                                       -- ResNum - int
                        ,@ModUser
                                       -- ModUser - varchar
                        ,GETDATE()
                                       -- ModDate - datetime
                        ,CASE WHEN ( @PostDate IS NULL ) THEN GETDATE()
                              ELSE @PostDate
                         END
                                       -- PostDate - datetime
                        ,@IsCompGraded --IsCompGraded - bit
                                       -- DateCompleted - date
                        ,@PostDate );


                SELECT     TOP 1000 IDENTITY(INT, 1, 1) AS num
                INTO       #numbers
                FROM       sys.objects s1 --use sys.columns if you don''t get enough rows returned to generate all the numbers you need
                CROSS JOIN sys.objects s2;


                INSERT INTO dbo.arGrdBkResults
                            SELECT   NEWID() AS GrdBkResultId
                                    ,yt.ClsSectionId
                                    ,yt.InstrGrdBkWgtDetailId
                                    ,yt.Score
                                    ,yt.Comments
                                    ,yt.StuEnrollId
                                    ,yt.ModUser
                                    ,yt.ModDate
                                    ,n.num + @NewResNum - 1 AS ResNum
                                    ,yt.PostDate
                                    ,yt.IsCompGraded
                                    ,yt.isCourseCredited
                                    ,yt.DateCompleted
                            FROM     @ToInsert yt
                            JOIN     #numbers n ON n.num <= yt.ResNum
                            ORDER BY yt.ResNum
                                    ,n.num;

                DROP TABLE #numbers;


            END;
        ELSE
            BEGIN
                INSERT INTO arGrdBkResults (
                                           GrdBkResultId
                                          ,ClsSectionId
                                          ,StuEnrollId
                                          ,InstrGrdBkWgtDetailId
                                          ,Score
                                          ,Comments
                                          ,ResNum
                                          ,ModUser
                                          ,ModDate
                                          ,PostDate
                                          ,IsCompGraded
                                          ,DateCompleted
                                           )
                VALUES ( NEWID()
                                       -- GrdBkResultId - uniqueidentifier
                        ,@ClsSectionId
                                       -- ClsSectionId - uniqueidentifier
                        ,@StuEnrollId
                                       -- StuEnrollId - uniqueidentifier
                        ,@InstrGrdBkWgtDetailId
                                       -- InstrGrdBkWgtDetailId- uniqueidentifier
                        ,@Score
                                       -- Score  - decimal
                        ,@Comments
                                       -- Comments - varchar
                        ,@NewResNum
                                       -- ResNum - int
                        ,@ModUser
                                       -- ModUser - varchar
                        ,GETDATE()
                                       -- ModDate - datetime
                        ,CASE WHEN ( @PostDate IS NULL ) THEN GETDATE()
                              ELSE @PostDate
                         END
                                       -- PostDate - datetime
                        ,@IsCompGraded --IsCompGraded - bit
                                       -- DateCompleted - date
                        ,@PostDate );
            END;

        UPDATE arResults
        SET    IsCourseCompleted = 0
              ,Score = NULL
              ,GrdSysDetailId = NULL
        WHERE  TestId = @ClsSectionId
               AND StuEnrollId = @StuEnrollId;

    END;
    SET ANSI_NULLS ON;




'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]'
GO
IF OBJECT_ID(N'[dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]', 'P') IS NULL
EXEC sp_executesql N'--=================================================================================================
-- USP_AT_Step02_InsertAttendance_Day_PresentAbsent
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]
    (
        @StuEnrollIdList AS VARCHAR(MAX) = NULL
    )
AS -- 
    -- Step 2  --  InsertAttendance_Day_PresentAbsent  -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'') 
    --         --  TrackSapAttendance = ''byday''
    BEGIN
        DECLARE @StuEnrollId UNIQUEIDENTIFIER
               ,@MeetDate DATETIME
               ,@WeekDay VARCHAR(15)
               ,@StartDate DATETIME
               ,@EndDate DATETIME
               ,@PeriodDescrip VARCHAR(50)
               ,@Actual DECIMAL(18, 2)
               ,@Excused DECIMAL(18, 2)
               ,@ClsSectionId UNIQUEIDENTIFIER
               ,@Absent DECIMAL(18, 2)
               ,@ScheduledMinutes DECIMAL(18, 2)
               ,@TardyMinutes DECIMAL(18, 2)
               ,@MakeupHours DECIMAL(18, 2)
               ,@tardy DECIMAL(18, 2)
               ,@tracktardies INT
               ,@tardiesMakingAbsence INT
               ,@PrgVerId UNIQUEIDENTIFIER
               ,@rownumber INT
               ,@IsTardy BIT
               ,@ActualRunningScheduledHours DECIMAL(18, 2)
               ,@ActualRunningPresentHours DECIMAL(18, 2)
               ,@ActualRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningTardyHours DECIMAL(18, 2)
               ,@ActualRunningMakeupHours DECIMAL(18, 2)
               ,@PrevStuEnrollId UNIQUEIDENTIFIER
               ,@intTardyBreakPoint INT
               ,@AdjustedRunningPresentHours DECIMAL(18, 2)
               ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningScheduledDays DECIMAL(18, 2);

        DECLARE @attendanceSummary TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,RecordDate DATETIME
               ,ActualHours DECIMAL(18, 2)
               ,SchedHours DECIMAL(18, 2)
               ,Absent DECIMAL(18, 2)
               ,isTardy BIT
               ,TrackTardies BIT
               ,TardiesMakingAbsence BIT
            );
        DECLARE @MyEnrollments TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
            );
        INSERT INTO @MyEnrollments
                    SELECT enrollments.StuEnrollId
                    FROM   dbo.arStuEnrollments enrollments
                    WHERE  (
                           @StuEnrollIdList IS NULL
                           OR ( enrollments.StuEnrollId IN (
                                                           SELECT zz.Val
                                                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1) zz
                                                           )
                              )
                           );
        INSERT INTO @attendanceSummary (
                                       StuEnrollId
                                      ,RecordDate
                                      ,ActualHours
                                      ,SchedHours
                                      ,Absent
                                      ,isTardy
                                      ,TrackTardies
                                      ,TardiesMakingAbsence
                                       )
                    SELECT *
                    FROM   (
                           SELECT     t1.StuEnrollId
                                     ,t1.RecordDate
                                     ,t1.ActualHours
                                     ,t1.SchedHours
                                     ,CASE WHEN (
                                                (
                                                (
                                                (
                                                AAUT.UnitTypeDescrip = ''Present Absent''
                                                AND t1.SchedHours >= 1
                                                )
                                                OR (
                                                   AAUT.UnitTypeDescrip = ''Clock Hours''
                                                   AND t1.SchedHours > 0
                                                   )
                                                )
                                                AND t1.SchedHours NOT IN ( 999, 9999 )
                                                )
                                                AND t1.ActualHours = 0
                                                ) THEN t1.SchedHours
                                           ELSE 0
                                      END AS Absent
                                     ,t1.isTardy
                                     ,t3.TrackTardies
                                     ,t3.TardiesMakingAbsence
                           FROM       arStudentClockAttendance t1
                           INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                           INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                           INNER JOIN arAttUnitType AS AAUT ON AAUT.UnitTypeId = t3.UnitTypeId
						   INNER JOIN @MyEnrollments ON [@MyEnrollments].StuEnrollId = t1.StuEnrollId
                           WHERE --t3.UnitTypeId IN ( ''ef5535c2-142c-4223-ae3c-25a50a153cc6'',''B937C92E-FD7A-455E-A731-527A9918C734'' ) -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'') -
                                      AAUT.UnitTypeDescrip IN ( ''Present Absent'', ''Clock Hours'' )
                                      AND t1.ActualHours <> 9999.00
                           ) paAttendance;

        DELETE FROM dbo.syStudentAttendanceSummary
        WHERE StuEnrollId IN (
                             SELECT DISTINCT StuEnrollId
                             FROM   @attendanceSummary
                             );

        DECLARE GetAttendance_Cursor CURSOR FAST_FORWARD FORWARD_ONLY FOR
            SELECT   *
            FROM     @attendanceSummary
            ORDER BY StuEnrollId
                    ,RecordDate;
        OPEN GetAttendance_Cursor;
        DECLARE @ActualHours DECIMAL(18, 2)
               ,@SchedHours DECIMAL(18, 2);
        FETCH NEXT FROM GetAttendance_Cursor
        INTO @StuEnrollId
            ,@MeetDate
            ,@Actual
            ,@ScheduledMinutes
            ,@Absent
            ,@IsTardy
            ,@tracktardies
            ,@tardiesMakingAbsence;

        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningAbsentHours = 0;
        SET @ActualRunningTardyHours = 0;
        SET @ActualRunningMakeupHours = 0;
        SET @intTardyBreakPoint = 0;
        SET @AdjustedRunningPresentHours = 0;
        SET @AdjustedRunningAbsentHours = 0;
        SET @ActualRunningScheduledDays = 0;
        SET @MakeupHours = 0;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                IF @PrevStuEnrollId <> @StuEnrollId
                    BEGIN
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        SET @MakeupHours = 0;
                    END;

                IF (
                   @Actual <> 9999
                   AND @Actual <> 999
                   )
                    BEGIN
                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + @ScheduledMinutes;
                        SET @ActualRunningPresentHours = ISNULL(@ActualRunningPresentHours, 0) + @Actual;
                        SET @AdjustedRunningPresentHours = ISNULL(@AdjustedRunningPresentHours, 0) + @Actual;
                    END;
                SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + @Absent;
                IF (
                   @Actual = 0
                   AND @ScheduledMinutes >= 1
                   AND @ScheduledMinutes NOT IN ( 999, 9999 )
                   )
                    BEGIN
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                    END;
                -- Applies only to AMC 
                -- If Scheduled = 3.0 hrs and Actual = 1.0 Then 2 hrs is considered absent
                IF (
                   @ScheduledMinutes >= 1
                   AND @ScheduledMinutes NOT IN ( 999, 9999 )
                   AND @Actual > 0
                   AND ( @Actual < @ScheduledMinutes )
                   )
                    BEGIN
                        SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                        SET @AdjustedRunningAbsentHours = ISNULL(@AdjustedRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                    END;
                IF (
                   @tracktardies = 1
                   AND @IsTardy = 1
                   )
                    BEGIN
                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + 1;
                    END;
                -- commented by balaji on 10.22.2012 as rdl not adding attended days and makeup days
                -- If there are make up hrs deduct that otherwise it will be added again in progress report
                IF (
                   @Actual > 0
                   AND @Actual > @ScheduledMinutes
                   AND @Actual <> 9999.00
                   )
                    BEGIN
                        SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                    END;
                IF @tracktardies = 1
                   AND (
                       @TardyMinutes > 0
                       OR @IsTardy = 1
                       )
                    BEGIN
                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                    END;

                -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                IF (
                   @Actual > 0
                   AND @Actual > @ScheduledMinutes
                   AND @Actual <> 9999.00
                   )
                    BEGIN
                        SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                    END;

                IF (
                   @tracktardies = 1
                   AND @intTardyBreakPoint = @tardiesMakingAbsence
                   )
                    BEGIN
                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                        SET @intTardyBreakPoint = 0;
                    END;


                INSERT INTO syStudentAttendanceSummary (
                                                       StuEnrollId
                                                      ,ClsSectionId
                                                      ,StudentAttendedDate
                                                      ,ScheduledDays
                                                      ,ActualDays
                                                      ,ActualRunningScheduledDays
                                                      ,ActualRunningPresentDays
                                                      ,ActualRunningAbsentDays
                                                      ,ActualRunningMakeupDays
                                                      ,ActualRunningTardyDays
                                                      ,AdjustedPresentDays
                                                      ,AdjustedAbsentDays
                                                      ,AttendanceTrackType
                                                      ,ModUser
                                                      ,ModDate
                                                      ,tardiesmakingabsence
                                                       )
                VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, ISNULL(@ScheduledMinutes, 0), @Actual, ISNULL(@ActualRunningScheduledDays, 0)
                        ,ISNULL(@ActualRunningPresentHours, 0), ISNULL(@ActualRunningAbsentHours, 0), ISNULL(@MakeupHours, 0)
                        ,ISNULL(@ActualRunningTardyHours, 0), ISNULL(@AdjustedRunningPresentHours, 0), ISNULL(@AdjustedRunningAbsentHours, 0)
                        ,''Post Attendance by Class'', ''sa'', GETDATE(), @tardiesMakingAbsence );

                --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId

                SET @PrevStuEnrollId = @StuEnrollId;
                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@MeetDate
                    ,@Actual
                    ,@ScheduledMinutes
                    ,@Absent
                    ,@IsTardy
                    ,@tracktardies
                    ,@tardiesMakingAbsence;
            --@ActualRunningScheduledHours,@ActualRunningPresentHours,
            --@ActualRunningAbsentHours,@ActualRunningMakeupHours,@ActualRunningTardyHours,

            END;
        CLOSE GetAttendance_Cursor;
        DEALLOCATE GetAttendance_Cursor;

    END;
--=================================================================================================
-- END  --  USP_AT_Step02_InsertAttendance_Day_PresentAbsent
--=================================================================================================
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_AT_Step03_InsertAttendance_Class_PresentAbsent]'
GO
IF OBJECT_ID(N'[dbo].[USP_AT_Step03_InsertAttendance_Class_PresentAbsent]', 'P') IS NULL
EXEC sp_executesql N'--=================================================================================================
-- USP_AT_Step03_InsertAttendance_Class_PresentAbsent
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_AT_Step03_InsertAttendance_Class_PresentAbsent]
    (
        @StuEnrollIdList AS VARCHAR(MAX) = NULL
    )
AS --
    -- Step 3  --  InsertAttendance_Class_PresentAbsent   -- UnitTypeDescrip = (''None'', ''Present Absent'') 
    --         --  TrackSapAttendance = ''byclass''
    BEGIN
        DECLARE @StuEnrollId UNIQUEIDENTIFIER
               ,@MeetDate DATETIME
               ,@WeekDay VARCHAR(15)
               ,@StartDate DATETIME
               ,@EndDate DATETIME
               ,@PeriodDescrip VARCHAR(50)
               ,@Actual DECIMAL(18, 2)
               ,@Excused DECIMAL(18, 2)
               ,@ClsSectionId UNIQUEIDENTIFIER
               ,@Absent DECIMAL(18, 2)
               ,@ScheduledMinutes DECIMAL(18, 2)
               ,@TardyMinutes DECIMAL(18, 2)
               ,@tardy DECIMAL(18, 2)
               ,@tracktardies INT
               ,@tardiesMakingAbsence INT
               ,@PrgVerId UNIQUEIDENTIFIER
               ,@rownumber INT
               ,@ActualRunningPresentHours DECIMAL(18, 2)
               ,@ActualRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningTardyHours DECIMAL(18, 2)
               ,@ActualRunningMakeupHours DECIMAL(18, 2)
               ,@PrevStuEnrollId UNIQUEIDENTIFIER
               ,@intTardyBreakPoint INT
               ,@AdjustedRunningPresentHours DECIMAL(18, 2)
               ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningScheduledDays DECIMAL(18, 2)
               ,@AdjustedPresentDaysComputed DECIMAL(18, 2)
               ,@MakeupHours DECIMAL(18, 2)
               ,@boolReset BIT;



        DECLARE @attendanceSummary TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ClsSectionId UNIQUEIDENTIFIER
               ,MeetDate DATETIME
               ,WeekDay NVARCHAR(25)
               ,StartDate DATETIME
               ,EndDate DATETIME
               ,PeriodDescrip VARCHAR(50)
               ,Actual DECIMAL(18, 2)
               ,Excused BIT
               ,Absent DECIMAL(18, 2)
               ,ScheduledMinutes DECIMAL(18, 2)
               ,TardyMinutes DECIMAL(18, 2)
               ,Tardy BIT
               ,TrackTardies BIT
               ,TardiesMakingAbsence INT
               ,PrgVerId UNIQUEIDENTIFIER
               ,RowNumber INT
            );

        DECLARE @MyEnrollments TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
            );
        INSERT INTO @MyEnrollments
                    SELECT enrollments.StuEnrollId
                    FROM   dbo.arStuEnrollments enrollments
                    WHERE  (
                           @StuEnrollIdList IS NULL
                           OR ( enrollments.StuEnrollId IN (
                                                           SELECT zz.Val
                                                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1) zz
                                                           )
                              )
                           );
        INSERT INTO @attendanceSummary (
                                       StuEnrollId
                                      ,ClsSectionId
                                      ,MeetDate
                                      ,WeekDay
                                      ,StartDate
                                      ,EndDate
                                      ,PeriodDescrip
                                      ,Actual
                                      ,Excused
                                      ,Absent
                                      ,ScheduledMinutes
                                      ,TardyMinutes
                                      ,Tardy
                                      ,TrackTardies
                                      ,TardiesMakingAbsence
                                      ,PrgVerId
                                      ,RowNumber
                                       )
                    SELECT *
                    FROM   (
                           SELECT *
                                 ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                           FROM   (
                                  SELECT     DISTINCT t1.StuEnrollId
                                                     ,t1.ClsSectionId
                                                     ,t1.MeetDate
                                                     ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                     ,t4.StartDate
                                                     ,t4.EndDate
                                                     ,t5.PeriodDescrip
                                                     ,t1.Actual
                                                     ,t1.Excused
                                                     ,CASE WHEN (
                                                                t1.Actual = 0
                                                                AND t1.Excused = 0
                                                                ) THEN t1.Scheduled
                                                           ELSE
                                                               CASE WHEN (
                                                                         t1.Actual <> 9999.00
                                                                         AND t1.Actual < t1.Scheduled
                                                                         ) --Commented by Balaji on 2.6.2015 as Excused absence is still considered Absent, but considered present only while calculating payment period
                                                     --Case when (t1.Actual <> 9999.00 and t1.Actual < t1.Scheduled and t1.Excused <> 1) 
                                                     THEN ( t1.Scheduled - t1.Actual )
                                                                    ELSE 0
                                                               END
                                                      END AS Absent
                                                     ,t1.Scheduled AS ScheduledMinutes
                                                     ,CASE WHEN (
                                                                t1.Actual > 0
                                                                AND t1.Actual < t1.Scheduled
                                                                ) THEN ( t1.Scheduled - t1.Actual )
                                                           ELSE 0
                                                      END AS TardyMinutes
                                                     ,t1.Tardy AS Tardy
                                                     ,t3.TrackTardies
                                                     ,t3.TardiesMakingAbsence
                                                     ,t3.PrgVerId
                                  FROM       atClsSectAttendance t1
                                  INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
								  INNER JOIN @MyEnrollments ON [@MyEnrollments].StuEnrollId = t1.StuEnrollId
                                  INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                  INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                  INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                     AND (
                                                                         CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                         AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                         )
                                                                     AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                  INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                  INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                  INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                  INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                  INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                  INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                  WHERE      (
                                             --  t10.UnitTypeId IN ( ''2600592A-9739-4A13-BDCE-7A25FE4A7478'',''EF5535C2-142C-4223-AE3C-25A50A153CC6'' )  -- AAUT1.UnitTypeDescrip IN ( ''None'',''Present Absent'' )
                                             --OR t3.UnitTypeId IN ( ''2600592A-9739-4A13-BDCE-7A25FE4A7478'',''EF5535C2-142C-4223-AE3C-25A50A153CC6'' )  -- AAUT2.UnitTypeDescrip IN ( ''None'',''Present Absent'' )
                                             AAUT1.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                             OR AAUT2.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                             ) -- Minutes
                                             AND t1.Actual <> 9999
                                  ) dt
                           ) paAttendance;
        --exec Usp_ProgressReport_StudentData_GetList ''BDB21FA4-CBED-4780-AD7C-A55C50725503'',null,0,0,null,''byclass'',null,null

        DELETE FROM dbo.syStudentAttendanceSummary
        WHERE StuEnrollId IN (
                             SELECT DISTINCT StuEnrollId
                             FROM   @attendanceSummary
                             );
        DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
            SELECT   *
            FROM     @attendanceSummary
            ORDER BY StuEnrollId
                    ,MeetDate;
        OPEN GetAttendance_Cursor;
        FETCH NEXT FROM GetAttendance_Cursor
        INTO @StuEnrollId
            ,@ClsSectionId
            ,@MeetDate
            ,@WeekDay
            ,@StartDate
            ,@EndDate
            ,@PeriodDescrip
            ,@Actual
            ,@Excused
            ,@Absent
            ,@ScheduledMinutes
            ,@TardyMinutes
            ,@tardy
            ,@tracktardies
            ,@tardiesMakingAbsence
            ,@PrgVerId
            ,@rownumber;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningAbsentHours = 0;
        SET @ActualRunningTardyHours = 0;
        SET @ActualRunningMakeupHours = 0;
        SET @intTardyBreakPoint = 0;
        SET @AdjustedRunningPresentHours = 0;
        SET @AdjustedRunningAbsentHours = 0;
        SET @ActualRunningScheduledDays = 0;
        SET @boolReset = 0;
        SET @MakeupHours = 0;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                IF @PrevStuEnrollId <> @StuEnrollId
                    BEGIN
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        SET @MakeupHours = 0;
                        SET @boolReset = 1;
                    END;


                -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                IF @Actual <> 9999.00
                    BEGIN
                        -- Commented by Balaji on 2.6.2015 as Excused Absence is still considered an absence
                        -- @Excused is not added to Present Hours
                        SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual; -- + @Excused
                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; -- + @Excused

                        -- If there are make up hrs deduct that otherwise it will be added again in progress report
                        IF (
                           @Actual > 0
                           AND @Actual > @ScheduledMinutes
                           AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                            END;

                        SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                    END;

                -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                IF (
                   @Actual > 0
                   AND @Actual < @ScheduledMinutes
                   AND @tardy = 1
                   )
                    BEGIN
                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                    END;
                ELSE IF (
                        @Actual = 1
                        AND @Actual = @ScheduledMinutes
                        AND @tardy = 1
                        )
                         BEGIN
                             SET @ActualRunningTardyHours += 1;
                         END;

                -- Track how many days student has been tardy only when 
                -- program version requires to track tardy
                IF (
                   @tracktardies = 1
                   AND @tardy = 1
                   )
                    BEGIN
                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                    END;


                -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
                -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
                -- when student is tardy the second time, that second occurance will be considered as
                -- absence
                -- Variable @intTardyBreakpoint tracks how many times the student was tardy
                -- Variable @tardiesMakingAbsence tracks the tardy rule
                DECLARE @CountTardyForClass INT;
                SET @CountTardyForClass = (
                                          SELECT COUNT(*)
                                          FROM   atClsSectAttendance
                                          WHERE  StuEnrollId = @StuEnrollId
                                                 AND ClsSectionId = @ClsSectionId
                                                 AND Tardy = 1
                                          );
                --IF (@tracktardies = 1 AND @intTardyBreakPoint = @tardiesMakingAbsence) 
                /*
				IF (@tracktardies = 1 AND @intTardyBreakPoint = @tardiesMakingAbsence) 
                BEGIN
				Commented by Balaji on 2.9.2015 as this tardy calculation doesn''t take class section in to account
                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused
                    SET @AdjustedRunningAbsentHours = (@AdjustedRunningAbsentHours - @Absent) + @ScheduledMinutes --@TardyMinutes
                    SET @intTardyBreakPoint = 0
				
                END
			*/

                -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                IF (
                   @Actual > 0
                   AND @Actual > @ScheduledMinutes
                   AND @Actual <> 9999.00
                   )
                    BEGIN
                        SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                    END;

                IF ( @tracktardies = 1 )
                    BEGIN
                        SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                    END;
                ELSE
                    BEGIN
                        SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                    END;

                DELETE FROM syStudentAttendanceSummary
                WHERE StuEnrollId = @StuEnrollId
                      AND ClsSectionId = @ClsSectionId
                      AND StudentAttendedDate = @MeetDate;
                INSERT INTO syStudentAttendanceSummary (
                                                       StuEnrollId
                                                      ,ClsSectionId
                                                      ,StudentAttendedDate
                                                      ,ScheduledDays
                                                      ,ActualDays
                                                      ,ActualRunningScheduledDays
                                                      ,ActualRunningPresentDays
                                                      ,ActualRunningAbsentDays
                                                      ,ActualRunningMakeupDays
                                                      ,ActualRunningTardyDays
                                                      ,AdjustedPresentDays
                                                      ,AdjustedAbsentDays
                                                      ,AttendanceTrackType
                                                      ,ModUser
                                                      ,ModDate
                                                      ,tardiesmakingabsence
                                                      ,IsTardy
                                                       )
                VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                        ,@ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours, @AdjustedPresentDaysComputed
                        ,@AdjustedRunningAbsentHours, ''Post Attendance by Class Min'', ''sa'', GETDATE(), @tardiesMakingAbsence, @tardy );

                UPDATE syStudentAttendanceSummary
                SET    tardiesmakingabsence = @tardiesMakingAbsence
                WHERE  StuEnrollId = @StuEnrollId;
                SET @PrevStuEnrollId = @StuEnrollId;

                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@ClsSectionId
                    ,@MeetDate
                    ,@WeekDay
                    ,@StartDate
                    ,@EndDate
                    ,@PeriodDescrip
                    ,@Actual
                    ,@Excused
                    ,@Absent
                    ,@ScheduledMinutes
                    ,@TardyMinutes
                    ,@tardy
                    ,@tracktardies
                    ,@tardiesMakingAbsence
                    ,@PrgVerId
                    ,@rownumber;
            END;
        CLOSE GetAttendance_Cursor;
        DEALLOCATE GetAttendance_Cursor;

        DECLARE @MyTardyTable TABLE
            (
                ClsSectionId UNIQUEIDENTIFIER
               ,TardiesMakingAbsence INT
               ,AbsentHours DECIMAL(18, 2)
            );

        INSERT INTO @MyTardyTable
                    SELECT     ClsSectionId
                              ,PV.TardiesMakingAbsence
                              ,CASE WHEN ( COUNT(*) >= PV.TardiesMakingAbsence ) THEN ( COUNT(*) / PV.TardiesMakingAbsence )
                                    ELSE 0
                               END AS AbsentHours
                    --Count(*) as NumberofTimesTardy 
                    FROM       syStudentAttendanceSummary SAS
                    INNER JOIN arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    WHERE      SE.StuEnrollId = @StuEnrollId
                               AND SAS.IsTardy = 1
                               AND PV.TrackTardies = 1
                    GROUP BY   ClsSectionId
                              ,PV.TardiesMakingAbsence;

        --Drop table @MyTardyTable
        DECLARE @TotalTardyAbsentDays DECIMAL(18, 2);
        SET @TotalTardyAbsentDays = (
                                    SELECT ISNULL(SUM(AbsentHours), 0)
                                    FROM   @MyTardyTable
                                    );

        --Print @TotalTardyAbsentDays

        UPDATE syStudentAttendanceSummary
        SET    AdjustedPresentDays = AdjustedPresentDays - @TotalTardyAbsentDays
              ,AdjustedAbsentDays = AdjustedAbsentDays + @TotalTardyAbsentDays
        WHERE  StuEnrollId = @StuEnrollId
               AND StudentAttendedDate = (
                                         SELECT   TOP 1 StudentAttendedDate
                                         FROM     syStudentAttendanceSummary
                                         WHERE    StuEnrollId = @StuEnrollId
                                         ORDER BY StudentAttendedDate DESC
                                         );



    END;
--=================================================================================================
-- END  --  USP_AT_Step03_InsertAttendance_Class_PresentAbsent
--=================================================================================================
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_AT_Step04_InsertAttendance_Class_MinutesClockHours]'
GO
IF OBJECT_ID(N'[dbo].[USP_AT_Step04_InsertAttendance_Class_MinutesClockHours]', 'P') IS NULL
EXEC sp_executesql N'--=================================================================================================
-- USP_AT_Step04_InsertAttendance_Class_MinutesClockHours
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_AT_Step04_InsertAttendance_Class_MinutesClockHours]
    (
        @StuEnrollIdList AS VARCHAR(MAX) = NULL
    )
AS --
    -- Step 4  --  InsertAttendance_Class_Minutes  -- UnitTypeDescrip = ( ''Minutes'',''Clock Hours'' )
    --         --  TrackSapAttendance = ''byclass''
    BEGIN -- Step 4  --  InsertAttendance_Class_Minutes
        DECLARE @StuEnrollId UNIQUEIDENTIFIER
               ,@MeetDate DATETIME
               ,@WeekDay VARCHAR(15)
               ,@StartDate DATETIME
               ,@EndDate DATETIME
               ,@PeriodDescrip VARCHAR(50)
               ,@Actual DECIMAL(18, 2)
               ,@Excused DECIMAL(18, 2)
               ,@ClsSectionId UNIQUEIDENTIFIER
               ,@Absent DECIMAL(18, 2)
               ,@ScheduledMinutes DECIMAL(18, 2)
               ,@TardyMinutes DECIMAL(18, 2)
               ,@tardy DECIMAL(18, 2)
               ,@tracktardies INT
               ,@tardiesMakingAbsence INT
               ,@PrgVerId UNIQUEIDENTIFIER
               ,@rownumber INT
               ,@ActualRunningPresentHours DECIMAL(18, 2)
               ,@ActualRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningTardyHours DECIMAL(18, 2)
               ,@ActualRunningMakeupHours DECIMAL(18, 2)
               ,@PrevStuEnrollId UNIQUEIDENTIFIER
               ,@intTardyBreakPoint INT
               ,@AdjustedRunningPresentHours DECIMAL(18, 2)
               ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningScheduledDays DECIMAL(18, 2)
               ,@AdjustedPresentDaysComputed DECIMAL(18, 2)
               ,@MakeupHours DECIMAL(18, 2)
               ,@boolReset BIT;



        DECLARE @attendanceSummary TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ClsSectionId UNIQUEIDENTIFIER
               ,MeetDate DATETIME
               ,WeekDay NVARCHAR(25)
               ,StartDate DATETIME
               ,EndDate DATETIME
               ,PeriodDescrip VARCHAR(50)
               ,Actual DECIMAL(18, 2)
               ,Excused BIT
               ,Absent DECIMAL(18, 2)
               ,ScheduledMinutes DECIMAL(18, 2)
               ,TardyMinutes DECIMAL(18, 2)
               ,Tardy BIT
               ,TrackTardies BIT
               ,TardiesMakingAbsence INT
               ,PrgVerId UNIQUEIDENTIFIER
               ,RowNumber INT
            );
        DECLARE @MyEnrollments TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
            );
        INSERT INTO @MyEnrollments
                    SELECT enrollments.StuEnrollId
                    FROM   dbo.arStuEnrollments enrollments
                    WHERE  (
                           @StuEnrollIdList IS NULL
                           OR ( enrollments.StuEnrollId IN (
                                                           SELECT zz.Val
                                                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1) zz
                                                           )
                              )
                           );
        INSERT INTO @attendanceSummary (
                                       StuEnrollId
                                      ,ClsSectionId
                                      ,MeetDate
                                      ,WeekDay
                                      ,StartDate
                                      ,EndDate
                                      ,PeriodDescrip
                                      ,Actual
                                      ,Excused
                                      ,Absent
                                      ,ScheduledMinutes
                                      ,TardyMinutes
                                      ,Tardy
                                      ,TrackTardies
                                      ,TardiesMakingAbsence
                                      ,PrgVerId
                                      ,RowNumber
                                       )
                    SELECT *
                    FROM   (
                           SELECT *
                                 ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                           FROM   (
                                  SELECT     DISTINCT t1.StuEnrollId
                                                     ,t1.ClsSectionId
                                                     ,t1.MeetDate
                                                     ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                     ,t4.StartDate
                                                     ,t4.EndDate
                                                     ,t5.PeriodDescrip
                                                     ,t1.Actual
                                                     ,t1.Excused
                                                     ,CASE WHEN (
                                                                t1.Actual = 0
                                                                AND t1.Excused = 0
                                                                ) THEN t1.Scheduled
                                                           ELSE CASE WHEN (
                                                                          t1.Actual <> 9999.00
                                                                          AND t1.Actual < t1.Scheduled
                                                                          ) THEN ( t1.Scheduled - t1.Actual )
                                                                     ELSE 0
                                                                END
                                                      END AS Absent
                                                     ,t1.Scheduled AS ScheduledMinutes
                                                     ,CASE WHEN (
                                                                t1.Actual > 0
                                                                AND t1.Actual < t1.Scheduled
                                                                ) THEN ( t1.Scheduled - t1.Actual )
                                                           ELSE 0
                                                      END AS TardyMinutes
                                                     ,t1.Tardy AS Tardy
                                                     ,t3.TrackTardies
                                                     ,t3.TardiesMakingAbsence
                                                     ,t3.PrgVerId
                                  FROM       atClsSectAttendance t1
                                  INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                  INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                  INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                  INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                     AND (
                                                                         CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                         AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                         )
                                                                     AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                  INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                  INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                  INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                  INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                  INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                  INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                  INNER JOIN @MyEnrollments ON [@MyEnrollments].StuEnrollId = t1.StuEnrollId
                                  WHERE --t2.StuEnrollId =@StuEnrollId and
                                             (
                                             --t10.UnitTypeId IN ( ''A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'',''B937C92E-FD7A-455E-A731-527A9918C734'' )  --  AAUT1.UnitTypeDescrip IN ( ''Minutes'',''Clock Hours'' )
                                             --OR t3.UnitTypeId IN ( ''A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'',''B937C92E-FD7A-455E-A731-527A9918C734'' )  --   AAUT2.UnitTypeDescrip IN ( ''Minutes'',''Clock Hours'' )
                                             AAUT1.UnitTypeDescrip IN ( ''Minutes'', ''Clock Hours'' )
                                             OR AAUT2.UnitTypeDescrip IN ( ''Minutes'', ''Clock Hours'' )
                                             ) -- Minutes
                                             AND t1.Actual <> 9999
                                  ) dt
                           ) minCHAttendance;
        DELETE FROM dbo.syStudentAttendanceSummary
        WHERE StuEnrollId IN (
                             SELECT DISTINCT StuEnrollId
                             FROM   @attendanceSummary
                             );
        DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
            SELECT   *
            FROM     @attendanceSummary
            ORDER BY StuEnrollId
                    ,MeetDate;
        OPEN GetAttendance_Cursor;
        FETCH NEXT FROM GetAttendance_Cursor
        INTO @StuEnrollId
            ,@ClsSectionId
            ,@MeetDate
            ,@WeekDay
            ,@StartDate
            ,@EndDate
            ,@PeriodDescrip
            ,@Actual
            ,@Excused
            ,@Absent
            ,@ScheduledMinutes
            ,@TardyMinutes
            ,@tardy
            ,@tracktardies
            ,@tardiesMakingAbsence
            ,@PrgVerId
            ,@rownumber;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningAbsentHours = 0;
        SET @ActualRunningTardyHours = 0;
        SET @ActualRunningMakeupHours = 0;
        SET @intTardyBreakPoint = 0;
        SET @AdjustedRunningPresentHours = 0;
        SET @AdjustedRunningAbsentHours = 0;
        SET @ActualRunningScheduledDays = 0;
        SET @boolReset = 0;
        SET @MakeupHours = 0;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                IF @PrevStuEnrollId <> @StuEnrollId
                    BEGIN
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        SET @MakeupHours = 0;
                        SET @boolReset = 1;
                    END;


                -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                IF @Actual <> 9999.00
                    BEGIN
                        SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;

                        -- If there are make up hrs deduct that otherwise it will be added again in progress report
                        IF (
                           @Actual > 0
                           AND @Actual > @ScheduledMinutes
                           AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                            END;

                        SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                    END;

                -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                IF (
                   @Actual > 0
                   AND @Actual < @ScheduledMinutes
                   AND @tardy = 1
                   )
                    BEGIN
                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                    END;

                -- Track how many days student has been tardy only when 
                -- program version requires to track tardy
                IF (
                   @tracktardies = 1
                   AND @tardy = 1
                   )
                    BEGIN
                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                    END;


                -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
                -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
                -- when student is tardy the second time, that second occurance will be considered as
                -- absence
                -- Variable @intTardyBreakpoint tracks how many times the student was tardy
                -- Variable @tardiesMakingAbsence tracks the tardy rule
                IF (
                   @tracktardies = 1
                   AND @intTardyBreakPoint = @tardiesMakingAbsence
                   )
                    BEGIN
                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                        SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                        SET @intTardyBreakPoint = 0;
                    END;

                -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                IF (
                   @Actual > 0
                   AND @Actual > @ScheduledMinutes
                   AND @Actual <> 9999.00
                   )
                    BEGIN
                        SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                    END;

                IF ( @tracktardies = 1 )
                    BEGIN
                        SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                    END;
                ELSE
                    BEGIN
                        SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                    END;




                DELETE FROM syStudentAttendanceSummary
                WHERE StuEnrollId = @StuEnrollId
                      AND ClsSectionId = @ClsSectionId
                      AND StudentAttendedDate = @MeetDate;
                INSERT INTO syStudentAttendanceSummary (
                                                       StuEnrollId
                                                      ,ClsSectionId
                                                      ,StudentAttendedDate
                                                      ,ScheduledDays
                                                      ,ActualDays
                                                      ,ActualRunningScheduledDays
                                                      ,ActualRunningPresentDays
                                                      ,ActualRunningAbsentDays
                                                      ,ActualRunningMakeupDays
                                                      ,ActualRunningTardyDays
                                                      ,AdjustedPresentDays
                                                      ,AdjustedAbsentDays
                                                      ,AttendanceTrackType
                                                      ,ModUser
                                                      ,ModDate
                                                      ,tardiesmakingabsence
                                                       )
                VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                        ,@ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours, @AdjustedPresentDaysComputed
                        ,@AdjustedRunningAbsentHours, ''Post Attendance by Class Min'', ''sa'', GETDATE(), @tardiesMakingAbsence );

                --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                SET @PrevStuEnrollId = @StuEnrollId;

                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@ClsSectionId
                    ,@MeetDate
                    ,@WeekDay
                    ,@StartDate
                    ,@EndDate
                    ,@PeriodDescrip
                    ,@Actual
                    ,@Excused
                    ,@Absent
                    ,@ScheduledMinutes
                    ,@TardyMinutes
                    ,@tardy
                    ,@tracktardies
                    ,@tardiesMakingAbsence
                    ,@PrgVerId
                    ,@rownumber;
            END;
        CLOSE GetAttendance_Cursor;
        DEALLOCATE GetAttendance_Cursor;
    END;
--=================================================================================================
-- END  --  USP_AT_Step04_InsertAttendance_Class_MinutesClockHours
--=================================================================================================
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_AT_Step05_InsertAttendance_Day_Minutes]'
GO
IF OBJECT_ID(N'[dbo].[USP_AT_Step05_InsertAttendance_Day_Minutes]', 'P') IS NULL
EXEC sp_executesql N'--=================================================================================================
-- USP_AT_Step05_InsertAttendance_Day_Minutes
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_AT_Step05_InsertAttendance_Day_Minutes]
    (
        @StuEnrollIdList AS VARCHAR(MAX) = NULL
    )
AS --
    -- Step 5  --  InsertAttendance_Class_Minutes  -- UnitTypeDescrip = (''Minutes'') 
    --         --  TrackSapAttendance = ''byday''
    BEGIN -- Step 5  --  InsertAttendance_Day_Minutes
        DECLARE @StuEnrollId UNIQUEIDENTIFIER
               ,@MeetDate DATETIME
               ,@WeekDay VARCHAR(15)
               ,@StartDate DATETIME
               ,@EndDate DATETIME
               ,@PeriodDescrip VARCHAR(50)
               ,@Actual DECIMAL(18, 2)
               ,@Excused DECIMAL(18, 2)
               ,@ClsSectionId UNIQUEIDENTIFIER
               ,@Absent DECIMAL(18, 2)
               ,@ScheduledMinutes DECIMAL(18, 2)
               ,@TardyMinutes DECIMAL(18, 2)
               ,@tardy DECIMAL(18, 2)
               ,@tracktardies INT
               ,@tardiesMakingAbsence INT
               ,@PrgVerId UNIQUEIDENTIFIER
               ,@rownumber INT
               ,@IsTardy BIT
               ,@ActualRunningScheduledHours DECIMAL(18, 2)
               ,@ActualRunningPresentHours DECIMAL(18, 2)
               ,@ActualRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningTardyHours DECIMAL(18, 2)
               ,@ActualRunningMakeupHours DECIMAL(18, 2)
               ,@PrevStuEnrollId UNIQUEIDENTIFIER
               ,@intTardyBreakPoint INT
               ,@AdjustedRunningPresentHours DECIMAL(18, 2)
               ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningScheduledDays DECIMAL(18, 2);



        DECLARE @attendanceSummary TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ClsSectionId UNIQUEIDENTIFIER
               ,MeetDate DATETIME
               ,ActualHours DECIMAL(18, 2)
               ,ScheduledMinutes DECIMAL(18, 2)
               ,Absent DECIMAL(18, 2)
               ,isTardy BIT
               ,ActualRunningScheduledHours DECIMAL(18, 2)
               ,ActualRunningPresentHours DECIMAL(18, 2)
               ,ActualRunningAbsentHours DECIMAL(18, 2)
               ,ActualRunningMakeupHours DECIMAL(18, 2)
               ,ActualRunningTardyHours DECIMAL(18, 2)
               ,TrackTardies BIT
               ,TardiesMakingAbsence INT
               ,PrgVerId UNIQUEIDENTIFIER
               ,RowNumber INT
            );

        DECLARE @MyEnrollments TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
            );
        INSERT INTO @MyEnrollments
                    SELECT enrollments.StuEnrollId
                    FROM   dbo.arStuEnrollments enrollments
                    WHERE  (
                           @StuEnrollIdList IS NULL
                           OR ( enrollments.StuEnrollId IN (
                                                           SELECT zz.Val
                                                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1) zz
                                                           )
                              )
                           );

        INSERT INTO @attendanceSummary (
                                       StuEnrollId
                                      ,ClsSectionId
                                      ,MeetDate
                                      ,ActualHours
                                      ,ScheduledMinutes
                                      ,Absent
                                      ,isTardy
                                      ,ActualRunningScheduledHours
                                      ,ActualRunningPresentHours
                                      ,ActualRunningAbsentHours
                                      ,ActualRunningMakeupHours
                                      ,ActualRunningTardyHours
                                      ,TrackTardies
                                      ,TardiesMakingAbsence
                                      ,PrgVerId
                                      ,RowNumber
                                       )
                    SELECT *
                    FROM   (
                           SELECT     t1.StuEnrollId
                                     ,NULL AS ClsSectionId
                                     ,t1.RecordDate AS MeetDate
                                     ,t1.ActualHours
                                     ,t1.SchedHours AS ScheduledMinutes
                                     ,CASE WHEN (
                                                (
                                                t1.SchedHours >= 1
                                                AND t1.SchedHours NOT IN ( 999, 9999 )
                                                )
                                                AND t1.ActualHours = 0
                                                ) THEN t1.SchedHours
                                           ELSE 0
                                      END AS Absent
                                     ,t1.isTardy
                                     ,(
                                      SELECT SUM(SchedHours)
                                      FROM   arStudentClockAttendance
                                      WHERE  StuEnrollId = t1.StuEnrollId
                                             AND RecordDate <= t1.RecordDate
                                             AND (
                                                 t1.SchedHours >= 1
                                                 AND t1.SchedHours NOT IN ( 999, 9999 )
                                                 AND t1.ActualHours NOT IN ( 999, 9999 )
                                                 )
                                      ) AS ActualRunningScheduledHours
                                     ,(
                                      SELECT SUM(ActualHours)
                                      FROM   arStudentClockAttendance
                                      WHERE  StuEnrollId = t1.StuEnrollId
                                             AND RecordDate <= t1.RecordDate
                                             AND (
                                                 t1.SchedHours >= 1
                                                 AND t1.SchedHours NOT IN ( 999, 9999 )
                                                 )
                                             AND ActualHours >= 1
                                             AND ActualHours NOT IN ( 999, 9999 )
                                      ) AS ActualRunningPresentHours
                                     ,(
                                      SELECT COUNT(ActualHours)
                                      FROM   arStudentClockAttendance
                                      WHERE  StuEnrollId = t1.StuEnrollId
                                             AND RecordDate <= t1.RecordDate
                                             AND (
                                                 t1.SchedHours >= 1
                                                 AND t1.SchedHours NOT IN ( 999, 9999 )
                                                 )
                                             AND ActualHours = 0
                                             AND ActualHours NOT IN ( 999, 9999 )
                                      ) AS ActualRunningAbsentHours
                                     ,(
                                      SELECT SUM(ActualHours)
                                      FROM   arStudentClockAttendance
                                      WHERE  StuEnrollId = t1.StuEnrollId
                                             AND RecordDate <= t1.RecordDate
                                             AND SchedHours = 0
                                             AND ActualHours >= 1
                                             AND ActualHours NOT IN ( 999, 9999 )
                                      ) AS ActualRunningMakeupHours
                                     ,(
                                      SELECT SUM(SchedHours - ActualHours)
                                      FROM   arStudentClockAttendance
                                      WHERE  StuEnrollId = t1.StuEnrollId
                                             AND RecordDate <= t1.RecordDate
                                             AND (
                                                 (
                                                 t1.SchedHours >= 1
                                                 AND t1.SchedHours NOT IN ( 999, 9999 )
                                                 )
                                                 AND ActualHours >= 1
                                                 AND ActualHours NOT IN ( 999, 9999 )
                                                 )
                                             AND isTardy = 1
                                      ) AS ActualRunningTardyHours
                                     ,t3.TrackTardies
                                     ,t3.TardiesMakingAbsence
                                     ,t3.PrgVerId
                                     ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
                           FROM       arStudentClockAttendance t1
                           INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                           INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                           INNER JOIN arAttUnitType AS AAUT ON AAUT.UnitTypeId = t3.UnitTypeId
                           INNER JOIN @MyEnrollments ON [@MyEnrollments].StuEnrollId = t1.StuEnrollId
                           WHERE --t3.UnitTypeId IN ( ''A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'' ) -- UnitTypeDescrip = (''Minutes'') 
                                      AAUT.UnitTypeDescrip IN ( ''Minutes'' )
                                      AND t1.ActualHours <> 9999.00
                           ) minAttendance;
        DELETE FROM dbo.syStudentAttendanceSummary
        WHERE StuEnrollId IN (
                             SELECT DISTINCT StuEnrollId
                             FROM   @attendanceSummary
                             );
        DECLARE GetAttendance_Cursor CURSOR FOR
            SELECT   *
            FROM     @attendanceSummary
            ORDER BY StuEnrollId
                    ,MeetDate;
        OPEN GetAttendance_Cursor;
        DECLARE @ActualHours DECIMAL(18, 2)
               ,@SchedHours DECIMAL(18, 2);
        FETCH NEXT FROM GetAttendance_Cursor
        INTO @StuEnrollId
            ,@ClsSectionId
            ,@MeetDate
            ,@Actual
            ,@ScheduledMinutes
            ,@Absent
            ,@IsTardy
            ,@ActualRunningScheduledHours
            ,@ActualRunningPresentHours
            ,@ActualRunningAbsentHours
            ,@ActualRunningMakeupHours
            ,@ActualRunningTardyHours
            ,@tracktardies
            ,@tardiesMakingAbsence
            ,@PrgVerId
            ,@rownumber;

        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningAbsentHours = 0;
        SET @ActualRunningTardyHours = 0;
        SET @ActualRunningMakeupHours = 0;
        SET @intTardyBreakPoint = 0;
        SET @AdjustedRunningPresentHours = 0;
        SET @AdjustedRunningAbsentHours = 0;
        SET @ActualRunningScheduledDays = 0;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                IF @PrevStuEnrollId <> @StuEnrollId
                    BEGIN
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                    END;

                IF (
                   @ScheduledMinutes >= 1
                   AND (
                       @Actual <> 9999
                       AND @Actual <> 999
                       )
                   AND (
                       @ScheduledMinutes <> 9999
                       AND @Actual <> 999
                       )
                   )
                    BEGIN
                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + ISNULL(@ScheduledMinutes, 0);
                    END;
                ELSE
                    BEGIN
                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0);
                    END;

                IF (
                   @Actual <> 9999
                   AND @Actual <> 999
                   )
                    BEGIN
                        SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                    END;
                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                -- Absent hours
                --1. sched = 5, Actual = 2 then Ab = (5-3) = 2
                IF (
                   @Actual > 0
                   AND @Actual < @ScheduledMinutes
                   )
                    BEGIN
                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                    END;
                -- Make up hours
                --sched=5, Actual =7, makeup= 2,ab = 0
                --sched=0, Actual =7, makeup= 7,ab = 0
                IF (
                   @Actual > 0
                   AND @ScheduledMinutes > 0
                   AND @Actual > @ScheduledMinutes
                   AND (
                       @Actual <> 9999
                       AND @Actual <> 999
                       )
                   )
                    BEGIN
                        SET @ActualRunningMakeupHours = @ActualRunningMakeupHours + ( @Actual - @ScheduledMinutes );
                    END;
                --		if (@Actual>0 and @ScheduledMinutes=0 and (@Actual <> 9999.00 and @Actual <> 999.00))
                --		begin
                --			set @ActualRunningMakeupHours = @ActualRunningMakeupHours + (@Actual-@ScheduledMinutes)
                --		end
                IF (
                   @Actual <> 9999
                   AND @Actual <> 999
                   )
                    BEGIN
                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                    END;
                IF (
                   @Actual = 0
                   AND @ScheduledMinutes >= 1
                   AND @ScheduledMinutes NOT IN ( 999, 9999 )
                   )
                    BEGIN
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                    END;
                -- Absent hours
                --1. sched = 5, Actual = 2 then Ab = (5-3) = 2
                IF (
                   @Absent = 0
                   AND @ActualRunningAbsentHours > 0
                   AND ( @Actual < @ScheduledMinutes )
                   )
                    BEGIN
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                    END;
                IF (
                   @Actual > 0
                   AND @Actual < @ScheduledMinutes
                   AND (
                       @Actual <> 9999.00
                       AND @Actual <> 999.00
                       )
                   )
                    BEGIN
                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                    END;

                IF @tracktardies = 1
                   AND (
                       @TardyMinutes > 0
                       OR @IsTardy = 1
                       )
                    BEGIN
                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                    END;
                IF (
                   @tracktardies = 1
                   AND @intTardyBreakPoint = @tardiesMakingAbsence
                   )
                    BEGIN
                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                        SET @intTardyBreakPoint = 0;
                    END;

                INSERT INTO syStudentAttendanceSummary (
                                                       StuEnrollId
                                                      ,ClsSectionId
                                                      ,StudentAttendedDate
                                                      ,ScheduledDays
                                                      ,ActualDays
                                                      ,ActualRunningScheduledDays
                                                      ,ActualRunningPresentDays
                                                      ,ActualRunningAbsentDays
                                                      ,ActualRunningMakeupDays
                                                      ,ActualRunningTardyDays
                                                      ,AdjustedPresentDays
                                                      ,AdjustedAbsentDays
                                                      ,AttendanceTrackType
                                                      ,ModUser
                                                      ,ModDate
                                                       )
                VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                        ,@ActualRunningAbsentHours, @ActualRunningMakeupHours, @ActualRunningTardyHours, @AdjustedRunningPresentHours
                        ,@AdjustedRunningAbsentHours, ''Post Attendance by Class'', ''sa'', GETDATE());

                UPDATE syStudentAttendanceSummary
                SET    tardiesmakingabsence = @tardiesMakingAbsence
                WHERE  StuEnrollId = @StuEnrollId;
                --end
                SET @PrevStuEnrollId = @StuEnrollId;

                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@ClsSectionId
                    ,@MeetDate
                    ,@Actual
                    ,@ScheduledMinutes
                    ,@Absent
                    ,@IsTardy
                    ,@ActualRunningScheduledHours
                    ,@ActualRunningPresentHours
                    ,@ActualRunningAbsentHours
                    ,@ActualRunningMakeupHours
                    ,@ActualRunningTardyHours
                    ,@tracktardies
                    ,@tardiesMakingAbsence
                    ,@PrgVerId
                    ,@rownumber;
            END;
        CLOSE GetAttendance_Cursor;
        DEALLOCATE GetAttendance_Cursor;

    END;
--=================================================================================================
-- END  --  USP_AT_Step05_InsertAttendance_Day_Minutes
--=================================================================================================
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_AT_Step06_InsertAttendance_Class_ClockHour]'
GO
IF OBJECT_ID(N'[dbo].[USP_AT_Step06_InsertAttendance_Class_ClockHour]', 'P') IS NULL
EXEC sp_executesql N'
--=================================================================================================
-- USP_AT_Step06_InsertAttendance_Class_ClockHour
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_AT_Step06_InsertAttendance_Class_ClockHour]
    (
        @StuEnrollIdList AS VARCHAR(MAX) = NULL
    )
AS --
    -- Step 6  --  InsertAttendance_Class_ClockHour  -- UnitTypeDescrip =  ''Clock Hours'' 
    --         --  TrackSapAttendance = ''byclass''
    BEGIN -- Step 6  --  InsertAttendance_Class_ClockHour
        DECLARE @StuEnrollId UNIQUEIDENTIFIER
               ,@MeetDate DATETIME
               ,@WeekDay VARCHAR(15)
               ,@StartDate DATETIME
               ,@EndDate DATETIME
               ,@PeriodDescrip VARCHAR(50)
               ,@Actual DECIMAL(18, 2)
               ,@Excused DECIMAL(18, 2)
               ,@ClsSectionId UNIQUEIDENTIFIER
               ,@Absent DECIMAL(18, 2)
               ,@ScheduledMinutes DECIMAL(18, 2)
               ,@TardyMinutes DECIMAL(18, 2)
               ,@tardy DECIMAL(18, 2)
               ,@tracktardies INT
               ,@tardiesMakingAbsence INT
               ,@PrgVerId UNIQUEIDENTIFIER
               ,@rownumber INT
               ,@ActualRunningPresentHours DECIMAL(18, 2)
               ,@ActualRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningTardyHours DECIMAL(18, 2)
               ,@ActualRunningMakeupHours DECIMAL(18, 2)
               ,@PrevStuEnrollId UNIQUEIDENTIFIER
               ,@intTardyBreakPoint INT
               ,@AdjustedRunningPresentHours DECIMAL(18, 2)
               ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningScheduledDays DECIMAL(18, 2)
               ,@ActualRunningExcusedDays INT
               ,@AdjustedPresentDaysComputed DECIMAL(18, 2)
               ,@PeriodDescrip1 VARCHAR(500)
               ,@MakeupHours DECIMAL(18, 2);
        DECLARE @MyEnrollments TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
            );
        INSERT INTO @MyEnrollments
                    SELECT enrollments.StuEnrollId
                    FROM   dbo.arStuEnrollments enrollments
                    WHERE  (
                           @StuEnrollIdList IS NULL
                           OR ( enrollments.StuEnrollId IN (
                                                           SELECT zz.Val
                                                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1) zz
                                                           )
                              )
                           );
        DECLARE GetAttendance_Cursor CURSOR FOR
            SELECT   *
                    ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
            FROM     (
                     SELECT     DISTINCT t1.StuEnrollId
                                        ,t1.ClsSectionId
                                        ,(
                                         SELECT Descrip
                                         FROM   arReqs A1
                                               ,dbo.arClassSections A2
                                         WHERE  A1.ReqId = A2.ReqId
                                                AND A2.ClsSectionId = t1.ClsSectionId
                                         ) + ''  '' + t5.PeriodDescrip AS PeriodDescrip1
                                        ,t1.MeetDate
                                        ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                        ,t4.StartDate
                                        ,t4.EndDate
                                        ,t5.PeriodDescrip
                                        ,CASE WHEN (
                                                   t1.Actual >= 0
                                                   AND t1.Actual <> 9999
                                                   ) THEN t1.Actual / CAST(60 AS FLOAT)
                                              ELSE t1.Actual
                                         END AS Actual
                                        ,t1.Excused
                                        ,CASE WHEN (
                                                   t1.Actual >= 0
                                                   AND t1.Actual <> 9999
                                                   AND ( t1.Actual / CAST(60 AS FLOAT)) < t1.Scheduled / CAST(60 AS FLOAT)
                                                   ) THEN (( t1.Scheduled / CAST(60 AS FLOAT)) - ( t1.Actual / CAST(60 AS FLOAT)))
                                              ELSE 0
                                         END AS Absent
                                        ,CASE WHEN t1.Scheduled > 0 THEN ( t1.Scheduled / CAST(60 AS FLOAT))
                                              ELSE 0
                                         END AS ScheduledMinutes
                                        ,NULL AS TardyMinutes
                                        ,CASE WHEN t1.Tardy = 1 THEN 1
                                              ELSE 0
                                         END AS Tardy
                                        ,t3.TrackTardies
                                        ,t3.TardiesMakingAbsence
                                        ,t3.PrgVerId
                     FROM       atClsSectAttendance t1
                     INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                     INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                     INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                     INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                        AND (
                                                            CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                            AND DATEDIFF(DAY, t1.MeetDate, t4.EndDate) >= 0
                                                            )
                                                        AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8522 line added
                     INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                     INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                     INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                     INNER JOIN syPeriodsWorkDays PWD ON t5.PeriodId = PWD.PeriodId
                     INNER JOIN plWorkDays WD ON WD.WorkDaysId = PWD.WorkDayId
                     --inner join Inserted t20 on t20.ClsSectAttId = t1.ClsSectAttId --Inserted Table
                     INNER JOIN dbo.arClassSections t8 ON t8.ClsSectionId = t1.ClsSectionId
                     INNER JOIN dbo.arReqs t9 ON t9.ReqId = t8.ReqId
                     INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t9.UnitTypeId
                     INNER JOIN @MyEnrollments ON [@MyEnrollments].StuEnrollId = t1.StuEnrollId
                     WHERE --t2.StuEnrollId=''525B5DB6-6EF8-47AE-A5C3-8DAFC71FB471'' and
                                (
                                --t9.UnitTypeId IN ( ''B937C92E-FD7A-455E-A731-527A9918C734'' )  -- AAUT1.UnitTypeDescrip IN ( ''Clock Hours'' )
                                --OR t3.UnitTypeId IN ( ''B937C92E-FD7A-455E-A731-527A9918C734'' )  -- AAUT2.UnitTypeDescrip IN ( ''Clock Hours'' )
                                AAUT1.UnitTypeDescrip IN ( ''Clock Hours'' )
                                OR AAUT2.UnitTypeDescrip IN ( ''Clock Hours'' )
                                ) -- Clock Hours
                                AND t1.Actual <> 9999.00
                                AND SUBSTRING(DATENAME(dw, t1.MeetDate), 1, 3) = SUBSTRING(WD.WorkDaysDescrip, 1, 3)

                     -- Some times Makeup days don''t fall inside the schedule
                     -- ex: there may be a schedule for T-Fri and school may mark attendance for saturday
                     -- the following query will bring whatever was left out in above query

                     ) dt
            ORDER BY StuEnrollId
                    ,MeetDate;


        DELETE FROM dbo.syStudentAttendanceSummary
        WHERE StuEnrollId IN (
                             SELECT DISTINCT StuEnrollId
                             FROM   @MyEnrollments
                             );

        OPEN GetAttendance_Cursor;
        FETCH NEXT FROM GetAttendance_Cursor
        INTO @StuEnrollId
            ,@ClsSectionId
            ,@PeriodDescrip1
            ,@MeetDate
            ,@WeekDay
            ,@StartDate
            ,@EndDate
            ,@PeriodDescrip
            ,@Actual
            ,@Excused
            ,@Absent
            ,@ScheduledMinutes
            ,@TardyMinutes
            ,@tardy
            ,@tracktardies
            ,@tardiesMakingAbsence
            ,@PrgVerId
            ,@rownumber;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningAbsentHours = 0;
        SET @ActualRunningTardyHours = 0;
        SET @ActualRunningMakeupHours = 0;
        SET @intTardyBreakPoint = 0;
        SET @AdjustedRunningPresentHours = 0;
        SET @AdjustedRunningAbsentHours = 0;
        SET @ActualRunningScheduledDays = 0;
        SET @MakeupHours = 0;
        SET @ActualRunningExcusedDays = 0;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                IF @PrevStuEnrollId <> @StuEnrollId
                    BEGIN
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        SET @MakeupHours = 0;
                        SET @ActualRunningExcusedDays = 0;
                    END;

                -- Calculate : Actual Running Scheduled Days
                SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;

                -- Calculate: Actual and Adjusted Running Present Hours
                IF @Actual <> 9999
                    BEGIN
                        IF (
                           @Actual > 0
                           AND @Actual > @ScheduledMinutes
                           ) -- Makeup Hours
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual - ( @Actual - @ScheduledMinutes ); -- Subtract Makeup Hours from Actual
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual - ( @Actual - @ScheduledMinutes ); -- Subtract Makeup Hours from Actual
                            END;
                        ELSE
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                            END;
                    END;

                -- Calculate: Adjusted Running Absent Hours
                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                SET @TardyMinutes = ( @ScheduledMinutes - @Actual );
                IF ( @Excused = 1 )
                    BEGIN
                        SET @ActualRunningExcusedDays = @ActualRunningExcusedDays + 1;
                    END;

                IF (
                   @Actual > 0
                   AND @Actual < @ScheduledMinutes
                   AND @tardy = 1
                   )
                    BEGIN
                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                    END;

                IF (
                   @Actual > 0
                   AND @Actual > @ScheduledMinutes
                   )
                    BEGIN
                        SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                    END;
                IF (
                   @tracktardies = 1
                   AND @TardyMinutes > 0
                   AND @tardy = 1
                   )
                    BEGIN
                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                    END;
                --IF (@tracktardies = 1 AND @intTardyBreakPoint = @tardiesMakingAbsence) 
                --    BEGIN
                --  SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual
                --        SET @AdjustedRunningAbsentHours = (@AdjustedRunningAbsentHours - @Absent) + @ScheduledMinutes --@TardyMinutes
                --        SET @ActualRunningTardyHours = @ActualRunningTardyHours - (@ScheduledMinutes - @Actual)
                --        SET @intTardyBreakPoint = 0
                --    END


                -- if (@tracktardies=1)
                --  begin
                --	set @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours --+IsNULL(@ActualRunningTardyHours,0)
                --  end
                -- else
                BEGIN
                    SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                END;



                INSERT INTO syStudentAttendanceSummary (
                                                       StuEnrollId
                                                      ,ClsSectionId
                                                      ,StudentAttendedDate
                                                      ,ScheduledDays
                                                      ,ActualDays
                                                      ,ActualRunningScheduledDays
                                                      ,ActualRunningPresentDays
                                                      ,ActualRunningAbsentDays
                                                      ,ActualRunningMakeupDays
                                                      ,ActualRunningTardyDays
                                                      ,AdjustedPresentDays
                                                      ,AdjustedAbsentDays
                                                      ,AttendanceTrackType
                                                      ,ModUser
                                                      ,ModDate
                                                      ,IsExcused
                                                      ,ActualRunningExcusedDays
                                                      ,IsTardy
                                                       )
                VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                        ,@ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours, @AdjustedPresentDaysComputed
                        ,@AdjustedRunningAbsentHours, ''Post Attendance by Class Clock'', ''sa'', GETDATE(), @Excused, @ActualRunningExcusedDays, @tardy );

                UPDATE syStudentAttendanceSummary
                SET    tardiesmakingabsence = @tardiesMakingAbsence
                WHERE  StuEnrollId = @StuEnrollId;
                SET @PrevStuEnrollId = @StuEnrollId;

                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@ClsSectionId
                    ,@PeriodDescrip1
                    ,@MeetDate
                    ,@WeekDay
                    ,@StartDate
                    ,@EndDate
                    ,@PeriodDescrip
                    ,@Actual
                    ,@Excused
                    ,@Absent
                    ,@ScheduledMinutes
                    ,@TardyMinutes
                    ,@tardy
                    ,@tracktardies
                    ,@tardiesMakingAbsence
                    ,@PrgVerId
                    ,@rownumber;
            END;
        CLOSE GetAttendance_Cursor;
        DEALLOCATE GetAttendance_Cursor;

        DECLARE @MyTardyTable TABLE
            (
                ClsSectionId UNIQUEIDENTIFIER
               ,TardiesMakingAbsence INT
               ,AbsentHours DECIMAL(18, 2)
            );

        INSERT INTO @MyTardyTable
                    SELECT     ClsSectionId
                              ,PV.TardiesMakingAbsence
                              ,CASE WHEN ( COUNT(*) >= PV.TardiesMakingAbsence ) THEN (( COUNT(*) / PV.TardiesMakingAbsence ) * SAS.ScheduledDays )
                                    ELSE 0
                               END AS AbsentHours
                    --Count(*) as NumberofTimesTardy 
                    FROM       syStudentAttendanceSummary SAS
                    INNER JOIN arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    WHERE      SE.StuEnrollId = @StuEnrollId
                               AND SAS.IsTardy = 1
                               AND PV.TrackTardies = 1
                    GROUP BY   ClsSectionId
                              ,PV.TardiesMakingAbsence
                              ,SAS.ScheduledDays;

        --Drop table @MyTardyTable
        DECLARE @TotalTardyAbsentDays DECIMAL(18, 2);
        SET @TotalTardyAbsentDays = (
                                    SELECT ISNULL(SUM(AbsentHours), 0)
                                    FROM   @MyTardyTable
                                    );

        --Print @TotalTardyAbsentDays

        UPDATE syStudentAttendanceSummary
        SET    AdjustedPresentDays = AdjustedPresentDays - @TotalTardyAbsentDays
              ,AdjustedAbsentDays = AdjustedAbsentDays + @TotalTardyAbsentDays
        WHERE  StuEnrollId = @StuEnrollId
               AND StudentAttendedDate = (
                                         SELECT   TOP 1 StudentAttendedDate
                                         FROM     syStudentAttendanceSummary
                                         WHERE    StuEnrollId = @StuEnrollId
                                         ORDER BY StudentAttendedDate DESC
                                         );

    END;
--=================================================================================================
-- END  --  USP_AT_Step06_InsertAttendance_Class_ClockHour
--=================================================================================================
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_EnrollLead]'
GO
IF OBJECT_ID(N'[dbo].[USP_EnrollLead]', 'P') IS NULL
EXEC sp_executesql N'-- =========================================================================================================          
-- USP_EnrollLead          
-- =========================================================================================================          
CREATE PROCEDURE [dbo].[USP_EnrollLead]
    (
        @leadId AS UNIQUEIDENTIFIER
       ,@campus UNIQUEIDENTIFIER
       ,@gender AS UNIQUEIDENTIFIER
       ,@familyIncome UNIQUEIDENTIFIER = NULL
       ,@housingType UNIQUEIDENTIFIER = NULL
       ,@educationLevel UNIQUEIDENTIFIER = NULL
       ,@adminCriteria UNIQUEIDENTIFIER = NULL
       ,@degSeekCert UNIQUEIDENTIFIER = NULL
       ,@attendType UNIQUEIDENTIFIER = NULL
       ,@race UNIQUEIDENTIFIER = NULL
       ,@citizenship UNIQUEIDENTIFIER = NULL
       ,@prgVersion UNIQUEIDENTIFIER = NULL
       ,@programId UNIQUEIDENTIFIER = NULL
       ,@areaId UNIQUEIDENTIFIER = NULL
       ,@prgVerType INT = 0
       ,@startDt DATETIME
       --,@studentId UNIQUEIDENTIFIER          
       ,@expectedStartDt DATETIME = NULL
       ,@contractGrdDt DATETIME
       ,@tuitionCategory UNIQUEIDENTIFIER
       ,@enrollDt DATETIME
       ,@transferhr DECIMAL(18, 2) = NULL
       ,@dob AS DATETIME = NULL
       ,@ssn AS VARCHAR(50) = NULL
       ,@depencytype UNIQUEIDENTIFIER = NULL
       ,@maritalstatus UNIQUEIDENTIFIER = NULL
       ,@nationality UNIQUEIDENTIFIER = NULL
       ,@geographicType UNIQUEIDENTIFIER = NULL
       ,@state UNIQUEIDENTIFIER = NULL
       ,@disabled BIT = NULL
       ,@disableAutoCharge BIT = NULL
       ,@thirdPartyContract BIT = 0
       ,@firsttimeSchool BIT = NULL
       ,@firsttimePostSec BIT = NULL
       ,@schedule UNIQUEIDENTIFIER = NULL
       ,@shift UNIQUEIDENTIFIER = NULL
       ,@midptDate DATETIME = NULL
       ,@transferDt DATETIME = NULL
       ,@entranceInterviewDt DATETIME = NULL
       ,@chargingMethod UNIQUEIDENTIFIER = NULL
       ,@fAAvisor UNIQUEIDENTIFIER = NULL
       ,@acedemicAdvisor UNIQUEIDENTIFIER = NULL
       ,@badgeNum VARCHAR(50) = NULL
       ,@studentNumber VARCHAR(50) = ''''
       ,@modUser UNIQUEIDENTIFIER = NULL
       ,@enrollId VARCHAR(50) = NULL
    )
AS
    BEGIN
        SET NOCOUNT ON;
        DECLARE @transDate DATETIME;
        DECLARE @MyError AS INTEGER;
        DECLARE @Message AS NVARCHAR(MAX);
        DECLARE @modUserN VARCHAR(50)
               ,@studentStatusId UNIQUEIDENTIFIER
               ,@studentEnrolStatus UNIQUEIDENTIFIER
               ,@studentId UNIQUEIDENTIFIER
               ,@stuEnrollId UNIQUEIDENTIFIER
               ,@admissionRepId UNIQUEIDENTIFIER;
        SET @transDate = GETDATE();
        SET @studentStatusId = (
                               SELECT StatusId
                               FROM   syStatuses
                               WHERE  StatusCode = ''A''
                               );
        --,@originalStatus UNIQUEIDENTIFIER;           
        SET @studentId = NEWID();
        --set @studentEnrolStatus to system future start          
        SET @studentEnrolStatus = (
                                  SELECT     DISTINCT TOP 1 A.StatusCodeId
                                  FROM       syStatusCodes A
                                  INNER JOIN sySysStatus B ON B.SysStatusId = A.SysStatusId
                                  INNER JOIN syStatuses C ON C.StatusId = A.StatusId
                                  WHERE      C.Status = ''Active''
                                             AND B.SysStatusId = 7
                                             AND A.CampGrpId IN (
                                                                SELECT CampGrpId
                                                                FROM   dbo.syCmpGrpCmps
                                                                WHERE  CampusId = @campus
                                                                )
                                  );
        DECLARE @acCalendarID INT;
        SELECT @modUserN = UserName
        FROM   syUsers
        WHERE  UserId = @modUser;
        SET @MyError = 0; -- all good          
        SET @Message = N'''';
        BEGIN TRANSACTION;
        BEGIN TRY
            --step 1: temperary insert into arstudent          
            DECLARE @lName VARCHAR(50)
                   ,@fName VARCHAR(50)
                   ,@mName VARCHAR(50);

            SET @lName = (
                         SELECT LastName
                         FROM   adLeads
                         WHERE  LeadId = @leadId
                         );
            SET @fName = (
                         SELECT FirstName
                         FROM   adLeads
                         WHERE  LeadId = @leadId
                         );
            SET @mName = (
                         SELECT MiddleName
                         FROM   adLeads
                         WHERE  LeadId = @leadId
                         );

            IF ( @MyError = 0 )
                BEGIN
                    --update the adleads table          
                    DECLARE @enrolLeadStatus UNIQUEIDENTIFIER;
                    SET @enrolLeadStatus = (
                                           SELECT   TOP 1 StatusCodeId
                                           FROM     syStatusCodes
                                           WHERE    SysStatusId = 6
                                                    AND StatusId = ''F23DE1E2-D90A-4720-B4C7-0F6FB09C9965''
                                                    AND CampGrpId IN (
                                                                     SELECT CampGrpId
                                                                     FROM   dbo.syCmpGrpCmps
                                                                     WHERE  CampusId = @campus
                                                                     )
                                           ORDER BY ModDate
                                           );
                    DECLARE @stuNum VARCHAR(50);
                    SET @stuNum = (
                                  SELECT StudentNumber
                                  FROM   adLeads
                                  WHERE  LeadId = @leadId
                                  );
                    IF ( @stuNum = '''' )
                        BEGIN
                            -- generate student number          
                            DECLARE @formatType INT
                                   ,@yrNo INT
                                   ,@monthNo INT
                                   ,@dateNo INT
                                   ,@lNameNo INT
                                   ,@fNameNo INT;
                            DECLARE @lNameStu VARCHAR(50)
                                   ,@fNameStu VARCHAR(50)
                                   ,@yr VARCHAR(10)
                                   ,@mon VARCHAR(10)
                                   ,@dt VARCHAR(10)
                                   ,@seqStartNo INT;
                            SELECT @formatType = CAST(FormatType AS INT)
                                  ,@yrNo = ISNULL(YearNumber, 0)
                                  ,@monthNo = ISNULL(MonthNumber, 0)
                                  ,@dateNo = ISNULL(DateNumber, 0)
                                  ,@lNameNo = ISNULL(LNameNumber, 0)
                                  ,@fNameNo = ISNULL(FNameNumber, 0)
                                  ,@seqStartNo = SeqStartingNumber
                            FROM   syStudentFormat;
                            IF ( @formatType = 1 )
                                BEGIN
                                    SET @stuNum = '''';
                                END;
                            ELSE IF ( @formatType = 3 )
                                     BEGIN
                                         SET @lNameStu = SUBSTRING(@lName, 1, ISNULL(@lNameNo, 0));
                                         SET @fNameStu = SUBSTRING(@fName, 1, ISNULL(@fNameNo, 0));
                                         SET @yr = '''';
                                         SET @yr = CASE @yrNo
                                                        WHEN 0 THEN ''''
                                                        WHEN 4 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 1, 4)
                                                        WHEN 3 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 2, 3)
                                                        WHEN 2 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 3, 2)
                                                        WHEN 1 THEN SUBSTRING(CAST(YEAR(GETDATE()) AS VARCHAR(10)), 4, 1)
                                                   END;
                                         SET @mon = SUBSTRING(CAST(MONTH(GETDATE()) AS VARCHAR(10)), 1, @monthNo);
                                         SET @dt = SUBSTRING(CAST(DAY(GETDATE()) AS VARCHAR(10)), 1, @dateNo);
                                         IF (
                                            LEN(@mon) = 1
                                            AND @monthNo = 2
                                            )
                                             BEGIN
                                                 SET @mon = ''0'' + @mon;
                                             END;
                                         IF (
                                            LEN(@dt) = 1
                                            AND @dateNo = 2
                                            )
                                             BEGIN
                                                 SET @dt = ''0'' + @dt;
                                             END;
                                         SET NOCOUNT ON;
                                         DECLARE @stuId INT;
                                         SET @stuId = (
                                                      SELECT MAX(Student_SeqId) AS Student_SeqID
                                                      FROM   syGenerateStudentFormatID
                                                      ) + 1;
                                         INSERT INTO syGenerateStudentFormatID (
                                                                               Student_SeqId
                                                                              ,ModDate
                                                                               )
                                         VALUES ( @stuId, GETDATE());

                                         IF ( @yr = '''' )
                                             BEGIN
                                                 SET @stuNum = @mon + @dt + @lNameStu + @fNameStu + CAST(@stuId AS VARCHAR(5));
                                             END;
                                         ELSE
                                             BEGIN
                                                 SET @stuNum = @yr + @mon + @dt + @lNameStu + @fNameStu + CAST(@stuId AS VARCHAR(5));
                                             END;
                                     --SELECT @stuNum          
                                     END;
                            ELSE IF ( @formatType = 4 )
                                     BEGIN
                                         SET @stuNum = @badgeNum;
                                     END;
                            ELSE
                                     BEGIN
                                         SET NOCOUNT ON;

                                         IF EXISTS (
                                                   SELECT *
                                                   FROM   syGenerateStudentSeq
                                                   )
                                             BEGIN
                                                 SET @stuId = (
                                                              SELECT MAX(Student_SeqId) AS Student_SeqID
                                                              FROM   syGenerateStudentSeq
                                                              ) + 1;
                                             END;
                                         ELSE
                                             BEGIN
                                                 SET @stuId = @seqStartNo;
                                             END;
                                         INSERT INTO syGenerateStudentSeq (
                                                                          Student_SeqId
                                                                         ,ModDate
                                                                          )
                                         VALUES ( @stuId, GETDATE());
                                         SET @stuNum = CAST(@stuId AS VARCHAR(10));
                                     END;

                        END;
                    UPDATE adLeads
                    SET    Gender = @gender
                          ,BirthDate = @dob                             --ISNULL(@dob,BirthDate)          
                          ,SSN = @ssn                                   --ISNULL(@ssn,SSN)          
                          ,DependencyTypeId = @depencytype              --ISNULL(@depencytype,DependencyTypeId)          
                          ,MaritalStatus = @maritalstatus               --ISNULL(@maritalstatus,MaritalStatus)          
                          ,FamilyIncome = @familyIncome
                          ,HousingId = @housingType
                          ,admincriteriaid = @adminCriteria
                          ,DegCertSeekingId = @degSeekCert
                          ,AttendTypeId = @attendType
                          ,Race = @race
                          ,Nationality = @nationality                   --ISNULL(@nationality,Nationality)          
                          ,Citizen = @citizenship
                          ,GeographicTypeId = @geographicType           --ISNULL(@geographicType,GeographicTypeId)          
                          ,IsDisabled = @disabled                       --ISNULL(@disabled,IsDisabled)          
                          ,IsFirstTimeInSchool = @firsttimeSchool       --ISNULL(@firsttimeSchool,IsFirstTimeInSchool)          
                          ,CampusId = @campus
                          ,IsFirstTimePostSecSchool = @firsttimePostSec --ISNULL(@firsttimePostSec,IsFirstTimePostSecSchool)          
                          ,PrgVerId = @prgVersion
                          ,ProgramID = @programId
                          ,AreaID = @areaId
                          ,ProgramScheduleId = @schedule                --ISNULL(@schedule,ProgramScheduleId)          
                          ,ShiftID = @shift                             --ISNULL(@shift,ShiftID)          
                          ,ExpectedStart = @expectedStartDt
                          ,EntranceInterviewDate = @entranceInterviewDt --ISNULL(@entranceInterviewDt,EntranceInterviewDate)          
                          ,StudentNumber = ISNULL(@stuNum, '''')          --ISNULL(@studentNumber,StudentNumber)          
                          ,StudentStatusId = ISNULL(@studentStatusId, StudentStatusId)
                          ,StudentId = ISNULL(@studentId, StudentId)
                          ,ModUser = ISNULL(@modUserN, ModUser)
                          ,ModDate = @transDate
                          ,LeadStatus = @enrolLeadStatus                --update to enrolled          
                          ,PreviousEducation = @educationLevel
                          ,EnrollStateId = @state
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N''Failed to update in adleads'';
                        END;
                    SET @admissionRepId = (
                                          SELECT AdmissionsRep
                                          FROM   adLeads
                                          WHERE  LeadId = @leadId
                                          );
                END;

            IF ( @MyError = 0 )
                BEGIN
                    --insert into arStuEnrollments          
                    SET @stuEnrollId = NEWID();
                    INSERT INTO arStuEnrollments (
                                                 StuEnrollId
                                                ,StudentId
                                                ,EnrollDate
                                                ,PrgVerId
                                                ,StartDate
                                                ,ExpStartDate
                                                ,MidPtDate
                                                ,TransferDate
                                                ,ShiftId
                                                ,BillingMethodId
                                                ,CampusId
                                                ,StatusCodeId
                                                ,ModDate
                                                ,ModUser
                                                ,EnrollmentId
                                                ,AcademicAdvisor
                                                ,LeadId
                                                ,TuitionCategoryId
                                                ,EdLvlId
                                                ,FAAdvisorId
                                                ,attendtypeid
                                                ,degcertseekingid
                                                ,BadgeNumber
                                                ,ContractedGradDate
                                                ,TransferHours
                                                ,PrgVersionTypeId
                                                ,IsDisabled
                                                ,DisableAutoCharge
                                                ,ThirdPartyContract
                                                ,IsFirstTimeInSchool
                                                ,IsFirstTimePostSecSchool
                                                ,EntranceInterviewDate
                                                ,AdmissionsRep
                                                ,ExpGradDate
                                                ,CohortStartDate
                                                 )
                    VALUES ( @stuEnrollId, @studentId             -- StudentId - uniqueidentifier          
                            ,@enrollDt                            -- EnrollDate - datetime          
                            ,@prgVersion                          -- PrgVerId - uniqueidentifier          
                            ,CONVERT(DATE, @startDt, 101)         -- StartDate - datetime          
                            ,CONVERT(DATE, @expectedStartDt, 101) -- ExpStartDate - datetime          
                            ,@midptDate                           -- MidPtDate - datetime                   
                            ,@transferDt                          -- TransferDate - datetime          
                            ,@shift                               -- ShiftId - uniqueidentifier          
                            ,@chargingMethod                      -- BillingMethodId - uniqueidentifier          
                            ,@campus                              -- CampusId - uniqueidentifier          
                            ,@studentEnrolStatus                  -- StatusCodeId - uniqueidentifier                   
                            ,@transDate                           -- ModDate - datetime          
                            ,@modUserN                            -- ModUser - varchar(50)          
                            ,@enrollId                            -- EnrollmentId - varchar(50)                   
                            ,@acedemicAdvisor                     -- AcademicAdvisor - uniqueidentifier          
                            ,@leadId                              -- LeadId - uniqueidentifier          
                            ,@tuitionCategory                     -- TuitionCategoryId - uniqueidentifier          
                            ,@educationLevel                      -- EdLvlId - uniqueidentifier          
                            ,@fAAvisor                            -- FAAdvisorId - uniqueidentifier          
                            ,@attendType                          -- attendtypeid - uniqueidentifier          
                            ,@degSeekCert                         -- degcertseekingid - uniqueidentifier          
                            ,@badgeNum                            -- BadgeNumber - varchar(50)          
                            ,@contractGrdDt                       -- ContractedGradDate - datetime          
                            ,@transferhr                          -- TransferHours - decimal          
                            ,@prgVerType                          -- PrgVersionTypeId - int          
                            ,@disabled                            -- IsDisabled - bit    
                            ,@disableAutoCharge                   -- DisableAutoCharge - bit    
                            ,@thirdPartyContract                  -- ThirdPartyContract - bit      
                            ,@firsttimeSchool                     -- IsFirstTimeInSchool - bit          
                            ,@firsttimePostSec                    -- IsFirstTimePostSecSchool - bit          
                            ,@entranceInterviewDt                 -- EntranceInterviewDate - datetime          
                            ,@admissionRepId                      -- AdmissionsRep uniqueidentifier          
                            ,@contractGrdDt                       -- ExpGradDate DateTime          
                            ,CONVERT(DATE, @expectedStartDt, 101) -- cohort start date                    
                        );


                    IF ((
                        SELECT TOP 1 RTRIM(LTRIM(FormatType))
                        FROM   dbo.syStudentFormat
                        ) = ''4''
                       )
                        BEGIN
                            DECLARE @badgeId VARCHAR(50) = (
                                                           SELECT TOP 1 BadgeNumber
                                                           FROM   dbo.arStuEnrollments
                                                           WHERE  StuEnrollId = @stuEnrollId
                                                           );

                            UPDATE adLeads
                            SET    StudentNumber = @badgeId
                            WHERE  LeadId = @leadId;
                        END;


                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N''Failed to insert in arStuEnrollments'';
                        END;
                END;
            -- insert into arStudentSchedules if schedule is not null
            IF ( @MyError = 0 )
                BEGIN
                    IF @schedule IS NOT NULL
                        BEGIN
                            INSERT INTO arStudentSchedules (
                                                           StuScheduleId
                                                          ,StuEnrollId
                                                          ,ScheduleId
                                                          ,StartDate
                                                          ,EndDate
                                                          ,Active
                                                          ,ModUser
                                                          ,ModDate
                                                          ,Source
                                                           )
                            VALUES ( NEWID()      -- StuScheduleId - uniqueidentifier
                                    ,@stuEnrollId -- StuEnrollId - uniqueidentifier
                                    ,@schedule    -- ScheduleId - uniqueidentifier
                                    ,NULL         -- StartDate - smalldatetime
                                    ,NULL         -- EndDate - smalldatetime
                                    ,1            -- Active - bit
                                    ,@modUserN    -- ModUser - varchar(50)
                                    ,GETDATE()    -- ModDate - smalldatetime
                                    ,''attendance'' -- Source - varchar(50)
                                );
                        END;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N''Failed to insert in arStuEnrollments'';
                        END;
                END;
            IF ( @MyError = 0 )
                BEGIN
                    IF @stuEnrollId IS NOT NULL
                        BEGIN
                            IF ( @MyError = 0 )
                                BEGIN
                                    --insert into syStudentStatusChanges          
                                    DECLARE @OriginalleadStatus UNIQUEIDENTIFIER;
                                    SET @OriginalleadStatus = (
                                                              SELECT LeadStatus
                                                              FROM   adLeads
                                                              WHERE  LeadId = @leadId
                                                              );
                                    INSERT INTO syStudentStatusChanges (
                                                                       StudentStatusChangeId
                                                                      ,StuEnrollId
                                                                      ,OrigStatusId
                                                                      ,NewStatusId
                                                                      ,CampusId
                                                                      ,ModDate
                                                                      ,ModUser
                                                                      ,IsReversal
                                                                      ,DropReasonId
                                                                      ,DateOfChange
                                                                      ,Lda
                                                                       )
                                    VALUES ( NEWID(), @stuEnrollId, @OriginalleadStatus, @studentEnrolStatus, @campus, @transDate, @modUserN, 0, NULL
                                            ,@transDate, NULL );
                                    SET @MyError = @@ERROR;
                                    IF ( @MyError <> 0 )
                                        BEGIN
                                            SET @MyError = 1;
                                            SET @Message = @Message + N''Failed to insert in syStudentStatusChanges'';
                                        END;
                                END;
                            --insert into plStudentDocs          
                            INSERT INTO plStudentDocs (
                                                      StudentId
                                                     ,DocumentId
                                                     ,DocStatusId
                                                     ,ReceiveDate
                                                     ,ScannedId
                                                     ,ModDate
                                                     ,ModUser
                                                     ,ModuleID
                                                      )
                                        SELECT @studentId
                                              ,DocumentId
                                              ,DocStatusId
                                              ,ReceiveDate
                                              ,1
                                              ,@transDate
                                              ,@modUserN
                                              ,(
                                               SELECT DISTINCT ModuleId
                                               FROM   adReqs
                                               WHERE  adReqId = adLeadDocsReceived.DocumentId
                                               )
                                        FROM   adLeadDocsReceived
                                        WHERE  LeadId = @leadId;
                        END;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N''Failed to insert in plStudentDocs'';
                        END;
                END;
            IF ( @MyError = 0 )
                BEGIN
                    --update syDocumentHistory          
                    UPDATE syDocumentHistory
                    SET    StudentId = @studentId
                          ,ModUser = @modUserN
                          ,ModDate = @transDate
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N''Failed to update in syDocumentHistory'';
                        END;
                END;
            --update adLeadByLeadGroups          
            IF ( @MyError = 0 )
                BEGIN
                    UPDATE adLeadByLeadGroups
                    SET    StuEnrollId = @stuEnrollId
                          ,ModUser = @modUserN
                          ,ModDate = @transDate
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N''Failed to update in adLeadByLeadGroups'';
                        END;

                END;
            --insert in adLeadEntranceTest          
            IF ( @MyError = 0 )
                BEGIN

                    UPDATE adLeadEntranceTest
                    SET    StudentId = @studentId
                    WHERE  LeadId = @leadId;
                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N''Failed to update in adLeadEntranceTest'';

                        END;
                END;
            --copy all the lead requirements adEntrTestOverRide          
            IF ( @MyError = 0 )
                BEGIN

                    UPDATE adEntrTestOverRide
                    SET    StudentId = @studentId
                    WHERE  LeadId = @leadId;

                    SET @MyError = @@ERROR;
                    IF ( @MyError <> 0 )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N''Failed to update in adEntrTestOverRide'';
                        END;
                END;
            --exec USP_AD_CopyLeadTransactions          
            IF ( @MyError = 0 )
                BEGIN
                    DECLARE @rc INT;
                    EXECUTE @rc = dbo.USP_AD_CopyLeadTransactions @leadId
                                                                 ,@stuEnrollId
                                                                 ,@campus;
                    SET @MyError = @@ERROR;
                    IF (
                       @MyError <> 0
                       AND @rc = 0
                       )
                        BEGIN
                            SET @MyError = 1;
                            SET @Message = @Message + N''Failed to execute USP_AD_CopyLeadTransactions'';
                        END;
                END;
            --exec fees related SP          
            SET @acCalendarID = (
                                SELECT COUNT(P.ACId)
                                FROM   arPrograms P
                                      ,arPrgVersions PV
                                      ,arStuEnrollments SE
                                WHERE  PV.PrgVerId = SE.PrgVerId
                                       AND SE.StuEnrollId = @stuEnrollId
                                       AND P.ProgId = PV.ProgId
                                       AND ACId = 5
                                );
            --IF @acCalendarID >= 1          
            --    BEGIN          
            --apply fees by progver for clock hr programs          
            INSERT INTO saTransactions (
                                       TransactionId
                                      ,StuEnrollId
                                      ,TermId
                                      ,CampusId
                                      ,TransDate
                                      ,TransCodeId
                                      ,TransReference
                                      ,TransDescrip
                                      ,TransAmount
                                      ,FeeLevelId
                                      ,AcademicYearId
                                      ,TransTypeId
                                      ,IsPosted
                                      ,FeeId
                                      ,CreateDate
                                      ,IsAutomatic
                                      ,ModUser
                                      ,ModDate
                                       )
                        SELECT     NEWID()
                                  ,@stuEnrollId
                                  ,NULL
                                  ,@campus
                                  ,SE.ExpStartDate
                                  ,PVF.TransCodeId
                                  ,PV.PrgVerDescrip AS TransDescrip
                                  ,ST.TransCodeDescrip + ''-'' + PV.PrgVerDescrip AS TransCodeDescrip
                                  ,CASE PVF.UnitId
                                        WHEN 0 THEN PVF.Amount * PV.Credits
                                        WHEN 1 THEN PVF.Amount * PV.Hours
                                        WHEN 2 THEN PVF.Amount
                                   END AS TransAmount
                                  ,2 -- program version fees          
                                  ,NULL
                                  ,0
                                  ,1
                                  ,PVF.PrgVerFeeId
                                  ,@transDate
                                  ,0
                                  ,@modUserN
                                  ,@transDate
                        FROM       saProgramVersionFees AS PVF
                        INNER JOIN arPrgVersions PV ON PV.PrgVerId = PVF.PrgVerId
                        INNER JOIN arStuEnrollments SE ON SE.PrgVerId = PVF.PrgVerId
                        INNER JOIN saTransCodes AS ST ON ST.TransCodeId = PVF.TransCodeId
                        WHERE      SE.StuEnrollId = @stuEnrollId
                                   AND PVF.TuitionCategoryId = SE.TuitionCategoryId
                                   AND RateScheduleId IS NULL;

            --charge Program Fees if applicable          
            IF ( @MyError = 0 )
                BEGIN
                    DECLARE @Result INT;
                    DECLARE @currdt DATETIME;
                    SET @currdt = @transDate;
                    EXEC USP_ApplyFeesByProgramVersion @stuEnrollId    -- uniqueidentifier          
                                                      ,@modUserN       -- varchar(100)          
                                                      ,@campus         -- uniqueidentifier          
                                                      ,@currdt         -- datetime          
                                                      ,@Result OUTPUT; -- int          
                    SET @MyError = @@ERROR;
                END;
            IF ( @MyError = 0 )
                BEGIN
                    IF ( @prgVersion IS NOT NULL )
                        BEGIN
                            EXEC dbo.USP_REGISTER_STUDENT_AUTOMATICALLY @prgVersion
                                                                       ,@stuEnrollId
                                                                       ,@modUserN;
                        END;
                    SET @MyError = @@ERROR;
                END;

            IF (
               @MyError = 0
               AND @Result = 1
               )
                BEGIN
                    COMMIT TRANSACTION;
                    SET @Message = N''Successful COMMIT TRANSACTION''; -- do not change this message. It is used for verification in C# code.          
                    SELECT @Message;
                END;
            ELSE
                BEGIN
                    ROLLBACK TRANSACTION;
                    SET @Message = @Message + N''rollback failed transaction'';
                    SELECT @Message;
                END;
        END TRY
        BEGIN CATCH
            ROLLBACK TRANSACTION;
            SET @Message = @Message + N''Failed ROLLBACK TRANSACTION  Catching error'';

            SET @Message = ERROR_MESSAGE() + @Message + N''try failed transaction'';
            SELECT @Message;
        END CATCH;
    END;
-- =========================================================================================================          
-- END  --  USP_EnrollLead          
-- =========================================================================================================          

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_PR_Main_DBConfigs]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Main_DBConfigs]', 'P') IS NULL
EXEC sp_executesql N'
CREATE PROCEDURE [dbo].[Usp_PR_Main_DBConfigs]
    @CampusId UNIQUEIDENTIFIER
AS
    BEGIN
        DECLARE @VaribleTable TABLE
            (
             SchoolName VARCHAR(1000)
            ,DisplayAttendanceUnitForProgressReportByClass VARCHAR(1000)
            ,StudentIdentifier VARCHAR(1000)
            ,GPAMethod VARCHAR(1000)
            ,GradesFormat VARCHAR(1000)
            ,TrackSapAttendance VARCHAR(1000)
            ,GradeBookWeightingLevel VARCHAR(1000)
            );

        INSERT  @VaribleTable
                (
                 SchoolName
                ,DisplayAttendanceUnitForProgressReportByClass
                ,StudentIdentifier
                ,GPAMethod
                ,GradesFormat
                ,TrackSapAttendance
                ,GradeBookWeightingLevel
				)
        VALUES  (
                 -- SchoolName - varchar(1000)
                 ( SELECT RTRIM(LTRIM(SchoolName)) FROM dbo.syCampuses WHERE CampusId = @CampusId)
				  -- DisplayAttendanceUnitForProgressReportByClass - varchar(1000)   -- Display Hours
                ,( dbo.GetAppSettingValueByKeyName(''DisplayAttendanceUnitForProgressReportByClass'',@CampusId) )  
				  -- StudentIdentifier - varchar(1000)
                ,( dbo.GetAppSettingValueByKeyName(''StudentIdentifier'',@CampusId) ) 
				  -- GPAMethod - varchar(1000)
                ,( dbo.GetAppSettingValueByKeyName(''GPAMethod'',@CampusId) ) 
				  -- GradesFormat - varchar(1000)
                ,( dbo.GetAppSettingValueByKeyName(''GradesFormat'',@CampusId) ) 
				  -- TrackSapAttendance - varchar(1000)
                ,( dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'',@CampusId) )
				  -- SetGradeBookAt - varchar(1000)
                ,( dbo.GetAppSettingValueByKeyName(''GradeBookWeightingLevel'',@CampusId) )
                );
 
        SELECT TOP 1
                VT.SchoolName
               ,VT.DisplayAttendanceUnitForProgressReportByClass
               ,VT.StudentIdentifier
               ,VT.GPAMethod
               ,VT.GradesFormat
               ,VT.TrackSapAttendance
               ,VT.GradeBookWeightingLevel
        FROM    @VaribleTable AS VT;   

    END; 





'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_UpdateTransferCredits_SyCreditsSummary]'
GO
IF OBJECT_ID(N'[dbo].[Usp_UpdateTransferCredits_SyCreditsSummary]', 'P') IS NULL
EXEC sp_executesql N'
-- =========================================================================================================
-- Usp_UpdateTransferCredits_SyCreditsSummary
-- =========================================================================================================
CREATE PROCEDURE [dbo].[Usp_UpdateTransferCredits_SyCreditsSummary]
    @StuEnrollIdList NVARCHAR(MAX) = NULL
AS
    BEGIN
        DECLARE @PrgVerId UNIQUEIDENTIFIER
               ,@rownumber INT
               ,@StuEnrollId UNIQUEIDENTIFIER;
        DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
               ,@PrevReqId UNIQUEIDENTIFIER
               ,@PrevTermId UNIQUEIDENTIFIER
               ,@CreditsAttempted DECIMAL(18, 2)
               ,@CreditsEarned DECIMAL(18, 2)
               ,@TermId UNIQUEIDENTIFIER
               ,@TermDescrip VARCHAR(50);
        DECLARE @reqid UNIQUEIDENTIFIER
               ,@CourseCodeDescrip VARCHAR(50)
               ,@FinalGrade UNIQUEIDENTIFIER
               ,@FinalScore DECIMAL(18, 2)
               ,@ClsSectionId UNIQUEIDENTIFIER
               ,@Grade VARCHAR(50)
               ,@IsGradeBookNotSatisified BIT
               ,@TermStartDate DATETIME;
        DECLARE @IsPass BIT
               ,@IsCreditsAttempted BIT
               ,@IsCreditsEarned BIT
               ,@Completed BIT
               ,@CurrentScore DECIMAL(18, 2)
               ,@CurrentGrade VARCHAR(10)
               ,@FinalGradeDesc VARCHAR(50)
               ,@FinalGPA DECIMAL(18, 2)
               ,@GrdBkResultId UNIQUEIDENTIFIER;
        DECLARE @Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
               ,@Count_WeightedAverage_Credits DECIMAL(18, 2)
               ,@Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
               ,@Count_SimpleAverage_Credits DECIMAL(18, 2);
        DECLARE @CreditsPerService DECIMAL(18, 2)
               ,@NumberOfServicesAttempted INT
               ,@boolCourseHasLabWorkOrLabHours INT
               ,@sysComponentTypeId INT
               ,@RowCount INT;
        DECLARE @decGPALoop DECIMAL(18, 2)
               ,@intCourseCount INT
               ,@decWeightedGPALoop DECIMAL(18, 2)
               ,@IsInGPA BIT
               ,@isGradeEligibleForCreditsEarned BIT
               ,@isGradeEligibleForCreditsAttempted BIT;
        DECLARE @ComputedSimpleGPA DECIMAL(18, 2)
               ,@ComputedWeightedGPA DECIMAL(18, 2)
               ,@CourseCredits DECIMAL(18, 2);
        DECLARE @FinAidCreditsEarned DECIMAL(18, 2)
               ,@FinAidCredits DECIMAL(18, 2)
               ,@TermAverage DECIMAL(18, 2)
               ,@TermAverageCount INT;
        DECLARE @IsWeighted INT;
        DECLARE @IsTransferred BIT;
        DECLARE @MyEnrollments TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
            );
        INSERT INTO @MyEnrollments
                    SELECT enrollments.StuEnrollId
                    FROM   dbo.arStuEnrollments enrollments
                    WHERE  (
                           @StuEnrollIdList IS NULL
                           OR ( enrollments.StuEnrollId IN (
                                                           SELECT Val
                                                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1) 
                                                           )
                              )
                           );
        SET @decGPALoop = 0;
        SET @intCourseCount = 0;
        SET @decWeightedGPALoop = 0;
        SET @ComputedSimpleGPA = 0;
        SET @ComputedWeightedGPA = 0;
        SET @CourseCredits = 0;
        DECLARE GetCreditsSummary_Cursor CURSOR FOR
            SELECT     DISTINCT SE.StuEnrollId
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate
                               ,R.ReqId
                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                               ,RES.Score AS FinalScore
                               ,RES.GrdSysDetailId AS FinalGrade
                               ,NULL
                               ,R.Credits AS CreditsAttempted
                               ,NULL AS ClsSectionId
                               ,GSD.Grade
                               ,GSD.IsPass
                               ,GSD.IsCreditsAttempted
                               ,GSD.IsCreditsEarned
                               ,SE.PrgVerId
                               ,GSD.IsInGPA
                               ,R.FinAidCredits AS FinAidCredits
                               ,RES.IsTransferred
            FROM       arStuEnrollments SE
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
            --INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId and RES.TermId=CS.TermId 
            INNER JOIN arTerm T ON RES.TermId = T.TermId
            INNER JOIN arReqs R ON RES.ReqId = R.ReqId
            INNER JOIN @MyEnrollments ON [@MyEnrollments].StuEnrollId = RES.StuEnrollId
            --LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId=GBR.ClsSectionId
            --LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
            --LEFT JOIN arGrdComponentTypes GCT on GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId 
            LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            ORDER BY   T.StartDate
                      ,T.TermDescrip
                      ,R.ReqId;
        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor
        INTO @StuEnrollId
            ,@TermId
            ,@TermDescrip
            ,@TermStartDate
            ,@reqid
            ,@CourseCodeDescrip
            ,@FinalScore
            ,@FinalGrade
            ,@sysComponentTypeId
            ,@CreditsAttempted
            ,@ClsSectionId
            ,@Grade
            ,@IsPass
            ,@IsCreditsAttempted
            ,@IsCreditsEarned
            ,@PrgVerId
            ,@IsInGPA
            ,@FinAidCredits
            ,@IsTransferred; --,@GrdBkResultId
        WHILE @@FETCH_STATUS = 0
            BEGIN
                SET @CourseCredits = @CreditsAttempted;
                SET @RowCount = @RowCount + 1;
                SET @IsGradeBookNotSatisified = (
                                                SELECT COUNT(*) AS UnsatisfiedWorkUnits
                                                FROM   (
                                                       SELECT D.*
                                                             ,CASE WHEN D.MinimumScore > D.Score THEN ( D.MinimumScore - D.Score )
                                                                   ELSE 0
                                                              END AS Remaining
                                                             ,CASE WHEN ( D.MinimumScore > D.Score )
                                                                        AND ( D.MustPass = 1 ) THEN 0
                                                                   WHEN D.Score IS NULL
                                                                        AND ( D.Required = 1 ) THEN 0
                                                                   ELSE 1
                                                              END AS IsWorkUnitSatisfied
                                                       FROM   (
                                                              SELECT     T.TermId
                                                                        ,T.TermDescrip
                                                                        ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                                                        ,R.ReqId
                                                                        ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                                        ,( CASE WHEN GCT.SysComponentTypeId IN ( 500, 503, 504 ) THEN GBWD.Number
                                                                                ELSE (
                                                                                     SELECT MIN(MinVal)
                                                                                     FROM   arGradeScaleDetails GSD
                                                                                           ,arGradeSystemDetails GSS
                                                                                     WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                            AND GSS.IsPass = 1
                                                                                     )
                                                                           END
                                                                         ) AS MinimumScore
                                                                        ,GBR.Score AS Score
                                                                        ,GBWD.Weight AS Weight
                                                                        ,RES.Score AS FinalScore
                                                                        ,RES.GrdSysDetailId AS FinalGrade
                                                                        ,GBWD.Required
                                                                        ,GBWD.MustPass
                                                                        ,GBWD.GrdPolicyId
                                                                        ,( CASE GCT.SysComponentTypeId
                                                                                WHEN 544 THEN (
                                                                                              SELECT SUM(HoursAttended)
                                                                                              FROM   arExternshipAttendance
                                                                                              WHERE  StuEnrollId = SE.StuEnrollId
                                                                                              )
                                                                                ELSE GBR.Score
                                                                           END
                                                                         ) AS GradeBookResult
                                                                        ,GCT.SysComponentTypeId
                                                                        ,SE.StuEnrollId
                                                                        ,GBR.GrdBkResultId
                                                                        ,R.Credits AS CreditsAttempted
                                                                        ,CS.ClsSectionId
                                                                        ,GSD.Grade
                                                                        ,GSD.IsPass
                                                                        ,GSD.IsCreditsAttempted
                                                                        ,GSD.IsCreditsEarned
                                                              FROM       arStuEnrollments SE
                                                              INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                              INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                                                              INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                                                                               AND RES.TermId = CS.TermId
                                                              INNER JOIN arTerm T ON CS.TermId = T.TermId
                                                              INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                                              LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                                                                               AND GBR.StuEnrollId = SE.StuEnrollId
                                                              LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                              LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                              LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                                              WHERE      SE.StuEnrollId = @StuEnrollId
                                                                         AND T.TermId = @TermId
                                                                         AND R.ReqId = @reqid
                                                              ) D
                                                       ) E
                                                WHERE  IsWorkUnitSatisfied = 0
                                                );


                --Check if IsCreditsAttempted is set to True
                IF (
                   @IsCreditsAttempted IS NULL
                   OR @IsCreditsAttempted = 0
                   )
                    BEGIN
                        SET @CreditsAttempted = 0;
                    END;
                IF (
                   @IsCreditsEarned IS NULL
                   OR @IsCreditsEarned = 0
                   )
                    BEGIN
                        SET @CreditsEarned = 0;
                    END;

                IF ( @IsGradeBookNotSatisified >= 1 )
                    BEGIN
                        SET @CreditsEarned = 0;
                        SET @Completed = 0;
                    END;
                ELSE
                    BEGIN
                        SET @GrdBkResultId = (
                                             SELECT TOP 1 GrdBkResultId
                                             FROM   arGrdBkResults
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND ClsSectionId = @ClsSectionId
                                             );
                        IF @GrdBkResultId IS NOT NULL
                            BEGIN
                                IF ( @IsCreditsEarned = 1 )
                                    BEGIN
                                        SET @CreditsEarned = @CreditsAttempted;
                                        SET @FinAidCreditsEarned = @FinAidCredits;
                                    END;
                                SET @Completed = 1;
                            END;
                        IF (
                           @GrdBkResultId IS NULL
                           AND @Grade IS NOT NULL
                           )
                            BEGIN
                                IF ( @IsCreditsEarned = 1 )
                                    BEGIN
                                        SET @CreditsEarned = @CreditsAttempted;
                                        SET @FinAidCreditsEarned = @FinAidCredits;
                                    END;
                                SET @Completed = 1;
                            END;
                    END;
                IF (
                   @FinalScore IS NOT NULL
                   AND @Grade IS NOT NULL
                   )
                    BEGIN
                        IF ( @IsCreditsEarned = 1 )
                            BEGIN
                                SET @CreditsEarned = @CreditsAttempted;
                                SET @FinAidCreditsEarned = @FinAidCredits;
                            END;
                        SET @Completed = 1;

                    END;

                -- If course is not part of the program version definition do not add credits earned and credits attempted
                -- set the credits earned and attempted to zero
                DECLARE @coursepartofdefinition INT;
                SET @coursepartofdefinition = 0;

                -- Commented by Balaji on 1/16/2016 as it conflicts with equivalent courses
                --AMC Miguel Carabello, course:Swedish & Medical Management
                --	set @coursepartofdefinition = (select COUNT(*) as RowCountOfProgramDefinition from 
                --									(
                --										select * from arProgVerDef where PrgVerId=@PrgVerId and ReqId=@ReqId
                --											union
                --										select * from arProgVerDef where PrgVerId=@PrgVerId and ReqId in
                --											(select GrpId from arReqGrpDef where ReqId=@ReqId)
                --									) dt
                --								)
                --if (@coursepartofdefinition = 0)
                --	begin
                --		set @CreditsEarned = 0
                --		set @CreditsAttempted = 0
                --		set @FinAidCreditsEarned = 0
                --	end


                -- Check the grade scale associated with the class section and figure out of the final score was a passing score
                DECLARE @coursepassrowcount INT;
                SET @coursepassrowcount = 0;
                IF (
                   @FinalScore IS NOT NULL
                   AND @IsTransferred = 0
                   )

                    -- If the student scores 56 and the score is a passing score then we consider this course as completed
                    BEGIN
                        SET @coursepassrowcount = (
                                                  SELECT     COUNT(t2.MinVal) AS IsCourseCompleted
                                                  FROM       arClassSections t1
                                                  INNER JOIN arGradeScaleDetails t2 ON t1.GrdScaleId = t2.GrdScaleId
                                                  INNER JOIN arGradeSystemDetails t3 ON t2.GrdSysDetailId = t3.GrdSysDetailId
                                                  WHERE      t1.ClsSectionId = @ClsSectionId
                                                             AND t3.IsPass = 1
                                                             AND @FinalScore >= t2.MinVal
                                                  );
                        IF @coursepassrowcount >= 1
                            BEGIN
                                SET @Completed = 1;
                            END;
                        ELSE
                            BEGIN
                                SET @Completed = 0;
                            END;
                    END;

                -- If Student Scored a Failing Grade (IsPass set to 0 in Grade System)
                -- then mark this course as Incomplete
                IF ( @FinalGrade IS NOT NULL )
                    BEGIN
                        IF ( @IsPass = 0 )
                            BEGIN
                                SET @Completed = 0;
                                IF ( @IsCreditsEarned = 0 )
                                    BEGIN
                                        SET @CreditsEarned = 0;
                                        SET @FinAidCreditsEarned = 0;
                                    END;
                                IF ( @IsCreditsAttempted = 0 )
                                    BEGIN
                                        SET @CreditsAttempted = 0;
                                    END;
                            END;
                    END;

                SET @CurrentScore = (
                                    SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                ELSE NULL
                                           END AS CurrentScore
                                    FROM   (
                                           SELECT   InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight AS GradeBookWeight
                                                   ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                         ELSE 0
                                                    END AS ActualWeight
                                           FROM     (
                                                    SELECT   C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,ISNULL(C.Weight, 0) AS Weight
                                                            ,C.Number AS MinNumber
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter AS Param
                                                            ,X.GrdScaleId
                                                            ,SUM(GR.Score) AS Score
                                                            ,COUNT(D.Descrip) AS NumberOfComponents
                                                    FROM     (
                                                             SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                    ,A.EffectiveDate
                                                                                    ,B.GrdScaleId
                                                             FROM     arGrdBkWeights A
                                                                     ,arClassSections B
                                                             WHERE    A.ReqId = B.ReqId
                                                                      AND A.EffectiveDate <= B.StartDate
                                                                      AND B.ClsSectionId = @ClsSectionId
                                                             ORDER BY A.EffectiveDate DESC
                                                             ) X
                                                            ,arGrdBkWgtDetails C
                                                            ,arGrdComponentTypes D
                                                            ,arGrdBkResults GR
                                                    WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                             AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                             AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                             AND D.SysComponentTypeId NOT IN ( 500, 503 )
                                                             AND GR.StuEnrollId = @StuEnrollId
                                                             AND GR.ClsSectionId = @ClsSectionId
                                                             AND GR.Score IS NOT NULL
                                                    GROUP BY C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,C.Weight
                                                            ,C.Number
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter
                                                            ,X.GrdScaleId
                                                    ) S
                                           GROUP BY InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight
                                                   ,NumberOfComponents
                                           ) FinalTblToComputeCurrentScore
                                    );
                IF ( @CurrentScore IS NULL )
                    BEGIN
                        -- instructor grade books
                        SET @CurrentScore = (
                                            SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                        ELSE NULL
                                                   END AS CurrentScore
                                            FROM   (
                                                   SELECT   InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                   FROM     (
                                                            SELECT   C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,ISNULL(C.Weight, 0) AS Weight
                                                                    ,C.Number AS MinNumber
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter AS Param
                                                                    ,X.GrdScaleId
                                                                    ,SUM(GR.Score) AS Score
                                                                    ,COUNT(D.Descrip) AS NumberOfComponents
                                                            FROM     (
                                                                     --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
                                                                     --FROM          arGrdBkWeights A,arClassSections B        
                                                                     --WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
                                                                     --ORDER BY      A.EffectiveDate DESC
                                                                     SELECT DISTINCT TOP 1 t1.InstrGrdBkWgtId
                                                                                          ,t1.GrdScaleId
                                                                     FROM   arClassSections t1
                                                                           ,arGrdBkWeights t2
                                                                     WHERE  t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                            AND t1.ClsSectionId = @ClsSectionId
                                                                     ) X
                                                                    ,arGrdBkWgtDetails C
                                                                    ,arGrdComponentTypes D
                                                                    ,arGrdBkResults GR
                                                            WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                     AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                     AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                     AND
                                                                -- D.SysComponentTypeID not in (500,503) and 
                                                                GR.StuEnrollId = @StuEnrollId
                                                                     AND GR.ClsSectionId = @ClsSectionId
                                                                     AND GR.Score IS NOT NULL
                                                            GROUP BY C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,C.Weight
                                                                    ,C.Number
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter
                                                                    ,X.GrdScaleId
                                                            ) S
                                                   GROUP BY InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                   ) FinalTblToComputeCurrentScore
                                            );

                    END;

                IF ( @CurrentScore IS NOT NULL )
                    BEGIN
                        SET @CurrentGrade = (
                                            SELECT t2.Grade
                                            FROM   arGradeScaleDetails t1
                                                  ,arGradeSystemDetails t2
                                            WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                   AND t1.GrdScaleId IN (
                                                                        SELECT GrdScaleId
                                                                        FROM   arClassSections
                                                                        WHERE  ClsSectionId = @ClsSectionId
                                                                        )
                                                   AND @CurrentScore >= t1.MinVal
                                                   AND @CurrentScore <= t1.MaxVal
                                            );

                    END;
                ELSE
                    BEGIN
                        SET @CurrentGrade = NULL;
                    END;

                IF (
                   @CurrentScore IS NULL
                   AND @CurrentGrade IS NULL
                   AND @FinalScore IS NULL
                   AND @FinalGrade IS NULL
                   )
                    BEGIN
                        SET @Completed = 0;
                        SET @CreditsAttempted = 0;
                        SET @CreditsEarned = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;

                IF (
                   @FinalScore IS NOT NULL
                   OR @FinalGrade IS NOT NULL
                   )
                    BEGIN

                        SET @FinalGradeDesc = (
                                              SELECT Grade
                                              FROM   arGradeSystemDetails
                                              WHERE  GrdSysDetailId = @FinalGrade
                                              );



                        IF ( @FinalGradeDesc IS NULL )
                            BEGIN
                                SET @FinalGradeDesc = (
                                                      SELECT t2.Grade
                                                      FROM   arGradeScaleDetails t1
                                                            ,arGradeSystemDetails t2
                                                      WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                             AND t1.GrdScaleId IN (
                                                                                  SELECT GrdScaleId
                                                                                  FROM   arClassSections
                                                                                  WHERE  ClsSectionId = @ClsSectionId
                                                                                  )
                                                             AND @FinalScore >= t1.MinVal
                                                             AND @FinalScore <= t1.MaxVal
                                                      );
                            END;
                        SET @FinalGPA = (
                                        SELECT GPA
                                        FROM   arGradeSystemDetails
                                        WHERE  GrdSysDetailId = @FinalGrade
                                        );
                        IF @FinalGPA IS NULL
                            BEGIN
                                SET @FinalGPA = (
                                                SELECT t2.GPA
                                                FROM   arGradeScaleDetails t1
                                                      ,arGradeSystemDetails t2
                                                WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                       AND t1.GrdScaleId IN (
                                                                            SELECT GrdScaleId
                                                                            FROM   arClassSections
                                                                            WHERE  ClsSectionId = @ClsSectionId
                                                                            )
                                                       AND @FinalScore >= t1.MinVal
                                                       AND @FinalScore <= t1.MaxVal
                                                );
                            END;
                    END;
                ELSE
                    BEGIN
                        SET @FinalGradeDesc = NULL;
                        SET @FinalGPA = NULL;
                    END;

                --set @IsInGPA = (SELECT t2.IsInGPA FROM arGradeScaleDetails t1,arGradeSystemDetails t2
                --										WHERE  t1.GrdSysDetailId=t2.GrdSysDetailId and 
                --										t1.GrdScaleId In (Select GrdScaleId from arClassSections where ClsSectionId =@ClsSectionId) 
                --										and t2.Grade=@FinalGradeDesc)

                SET @isGradeEligibleForCreditsEarned = (
                                                       SELECT t2.IsCreditsEarned
                                                       FROM   arGradeScaleDetails t1
                                                             ,arGradeSystemDetails t2
                                                       WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                              AND t1.GrdScaleId IN (
                                                                                   SELECT GrdScaleId
                                                                                   FROM   arClassSections
                                                                                   WHERE  ClsSectionId = @ClsSectionId
                                                                                   )
                                                              AND t2.Grade = @FinalGradeDesc
                                                       );

                SET @isGradeEligibleForCreditsAttempted = (
                                                          SELECT t2.IsCreditsAttempted
                                                          FROM   arGradeScaleDetails t1
                                                                ,arGradeSystemDetails t2
                                                          WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                                 AND t1.GrdScaleId IN (
                                                                                      SELECT GrdScaleId
                                                                                      FROM   arClassSections
                                                                                      WHERE  ClsSectionId = @ClsSectionId
                                                                                      )
                                                                 AND t2.Grade = @FinalGradeDesc
                                                          );

                IF ( @isGradeEligibleForCreditsEarned IS NULL )
                    BEGIN
                        --Print   ''Credits Earned is NULL''
                        SET @isGradeEligibleForCreditsEarned = (
                                                               SELECT TOP 1 t2.IsCreditsEarned
                                                               FROM   arGradeSystemDetails t2
                                                               WHERE  t2.Grade = @FinalGradeDesc
                                                               );
                    END;

                IF ( @isGradeEligibleForCreditsAttempted IS NULL )
                    BEGIN
                        --Print   ''Credits Attempted is NULL''
                        SET @isGradeEligibleForCreditsAttempted = (
                                                                  SELECT TOP 1 t2.IsCreditsAttempted
                                                                  FROM   arGradeSystemDetails t2
                                                                  WHERE  t2.Grade = @FinalGradeDesc
                                                                  );
                    END;

                IF @isGradeEligibleForCreditsEarned = 0
                    BEGIN
                        SET @CreditsEarned = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;
                IF @isGradeEligibleForCreditsAttempted = 0
                    BEGIN
                        SET @CreditsAttempted = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;

                IF ( @IsPass = 0 )
                    BEGIN
                        SET @CreditsEarned = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;

                --For Letter Grade Schools if the score is null but final grade was posted then the 
                --Final grade will be the current grade
                IF @CurrentGrade IS NULL
                   AND @FinalGradeDesc IS NOT NULL
                    BEGIN
                        SET @CurrentGrade = @FinalGradeDesc;
                    END;

                --			if Trim(@PrevTermId) = Trim(@TermId)
                --			begin
                --				set @Sum_Product_WeightedAverage_Credits_GPA = @Sum_Product_WeightedAverage_Credits_GPA + (@Product_WeightedAverage_Credits_GPA)
                --				set @Sum
                --			end


                --Check if course has lab work or lab hours
                --			set @boolCourseHasLabWorkOrLabHours = (select distinct Count(GC.Descrip) from arGrdBkWeights GBW,arGrdComponentTypes GC, arGrdBkWgtDetails GD  where 
                --													GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId And GC.GrdComponentTypeId = GD.GrdComponentTypeId 
                --													and     GBW.ReqId = @ReqId and GC.SysComponentTypeID in (500,503))

                IF (
                   @sysComponentTypeId = 503
                   OR @sysComponentTypeId = 500
                   ) -- Lab work or Lab Hours
                    BEGIN
                        -- This course has lab work and lab hours
                        IF ( @Completed = 0 )
                            BEGIN
                                SET @CreditsPerService = (
                                                         SELECT TOP 1 GD.CreditsPerService
                                                         FROM   arGrdBkWeights GBW
                                                               ,arGrdComponentTypes GC
                                                               ,arGrdBkWgtDetails GD
                                                         WHERE  GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
                                                                AND GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                                                AND GBW.ReqId = @reqid
                                                                AND GC.SysComponentTypeId IN ( 500, 503 )
                                                         );
                                SET @NumberOfServicesAttempted = (
                                                                 SELECT     TOP 1 GBR.Score AS NumberOfServicesAttempted
                                                                 FROM       arStuEnrollments SE
                                                                 INNER JOIN arGrdBkResults GBR ON SE.StuEnrollId = GBR.StuEnrollId
                                                                                                  AND GBR.ClsSectionId = @ClsSectionId
                                                                 );

                                SET @CreditsEarned = ISNULL(@CreditsPerService, 0) * ISNULL(@NumberOfServicesAttempted, 0);
                            END;
                    END;

                DECLARE @rowAlreadyInserted INT;
                SET @rowAlreadyInserted = 0;

                -- Get the final Gpa only when IsCreditsAttempted is set to 1 and IsInGPA is set to 1
                IF @IsInGPA = 1
                    BEGIN
                        IF ( @IsCreditsAttempted = 0 )
                            BEGIN
                                SET @FinalGPA = NULL;
                            END;
                    END;
                ELSE
                    BEGIN
                        SET @FinalGPA = NULL;
                    END;

                IF @FinalScore IS NOT NULL
                   AND @CurrentScore IS NULL
                    BEGIN
                        SET @CurrentScore = @FinalScore;
                    END;

                -- Rally case DE 738 KeyBoarding Courses
                SET @IsWeighted = (
                                  SELECT COUNT(*) AS WeightsCount
                                  FROM   (
                                         SELECT     T.TermId
                                                   ,T.TermDescrip
                                                   ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                                   ,R.ReqId
                                                   ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                   ,( CASE WHEN GCT.SysComponentTypeId IN ( 500, 503, 504, 544 ) THEN GBWD.Number
                                                           ELSE (
                                                                SELECT MIN(MinVal)
                                                                FROM   arGradeScaleDetails GSD
                                                                      ,arGradeSystemDetails GSS
                                                                WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                       AND GSS.IsPass = 1
                                                                )
                                                      END
                                                    ) AS MinimumScore
                                                   ,GBR.Score AS Score
                                                   ,GBWD.Weight AS Weight
                                                   ,RES.Score AS FinalScore
                                                   ,RES.GrdSysDetailId AS FinalGrade
                                                   ,GBWD.Required
                                                   ,GBWD.MustPass
                                                   ,GBWD.GrdPolicyId
                                                   ,( CASE GCT.SysComponentTypeId
                                                           WHEN 544 THEN (
                                                                         SELECT SUM(HoursAttended)
                                                                         FROM   arExternshipAttendance
                                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                         )
                                                           ELSE GBR.Score
                                                      END
                                                    ) AS GradeBookResult
                                                   ,GCT.SysComponentTypeId
                                                   ,SE.StuEnrollId
                                                   ,GBR.GrdBkResultId
                                                   ,R.Credits AS CreditsAttempted
                                                   ,CS.ClsSectionId
                                                   ,GSD.Grade
                                                   ,GSD.IsPass
                                                   ,GSD.IsCreditsAttempted
                                                   ,GSD.IsCreditsEarned
                                         FROM       arStuEnrollments SE
                                         INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                         INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                                         --inner join Inserted t4 on RES.TransferId = t4.TransferId
                                         INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                                                          AND RES.TermId = CS.TermId
                                         INNER JOIN arTerm T ON CS.TermId = T.TermId
                                         INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                         LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                         LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                         LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                         LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                         WHERE      SE.StuEnrollId = @StuEnrollId
                                                    AND T.TermId = @TermId
                                                    AND R.ReqId = @reqid
                                         ) D
                                  WHERE  Weight >= 1
                                  );

                /************************************************* Changes for Build 2816 *********************/
                -- Rally case DE 738 KeyBoarding Courses
                DECLARE @GradesFormat VARCHAR(50);
                SET @GradesFormat = (
                                    SELECT Value
                                    FROM   syConfigAppSetValues
                                    WHERE  SettingId = 47
                                    ); -- 47 refers to grades format
                -- This condition is met only for numeric grade schools
                IF (
                   @IsGradeBookNotSatisified = 0
                   AND @IsWeighted = 0
                   AND @FinalScore IS NULL
                   AND @FinalGradeDesc IS NULL
                   AND LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
                   )
                    BEGIN
                        SET @Completed = 1;
                        SET @CreditsAttempted = (
                                                SELECT Credits
                                                FROM   arReqs
                                                WHERE  ReqId = @reqid
                                                );
                        SET @FinAidCredits = (
                                             SELECT FinAidCredits
                                             FROM   arReqs
                                             WHERE  ReqId = @reqid
                                             );
                        SET @CreditsAttempted = @CreditsAttempted;
                        SET @CreditsEarned = @CreditsAttempted;
                        SET @FinAidCreditsEarned = @FinAidCredits;
                    END;

                -- DE748 Name: ROSS: Completed field should also check for the Must Pass property of the work unit. 
                IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
                   AND @IsGradeBookNotSatisified >= 1
                    BEGIN
                        SET @Completed = 0;
                        SET @CreditsEarned = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;

                --DE738 Name: ROSS: Progress Report not taking care of courses that are not weighted. 

                -- Print @TermDescrip
                -- Print @CourseCodeDescrip
                -- Print @Completed
                --			-- Print LOWER(LTRIM(RTRIM(@GradesFormat)))
                --			-- Print @FinalScore
                --			-- Print @FinalGradedesc
                --			-- Print ''@IsWeighted=''
                --			-- Print @IsWeighted

                IF (
                   LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
                   AND @Completed = 1
                   AND @FinalScore IS NULL
                   AND @FinalGradeDesc IS NULL
                   )
                    BEGIN
                        SET @CreditsAttempted = @CreditsAttempted;
                        SET @CreditsEarned = @CreditsAttempted;
                        SET @FinAidCreditsEarned = @FinAidCredits;
                    END;

                -- In Ross Example : Externship, the student may not have completed the course but once he attempts a work unit
                -- we need to take the credits as attempted
                IF (
                   LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
                   AND @Completed = 0
                   AND @FinalScore IS NULL
                   AND @FinalGradeDesc IS NULL
                   )
                    BEGIN
                        DECLARE @rowcount4 INT;
                        SET @rowcount4 = (
                                         SELECT COUNT(*)
                                         FROM   arGrdBkResults
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND ClsSectionId = @ClsSectionId
                                                AND Score IS NOT NULL
                                         );
                        IF @rowcount4 >= 1
                            BEGIN
                                -- Print ''Gets in to if''
                                SET @CreditsAttempted = (
                                                        SELECT Credits
                                                        FROM   arReqs
                                                        WHERE  ReqId = @reqid
                                                        );
                                SET @CreditsEarned = 0;
                                SET @FinAidCreditsEarned = 0;
                            -- Print @CreditsAttempted
                            END;
                        ELSE
                            BEGIN
                                SET @rowcount4 = (
                                                 SELECT COUNT(*)
                                                 FROM   arGrdBkConversionResults
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND ReqId = @reqid
                                                        AND TermId = @TermId
                                                        AND Score IS NOT NULL
                                                 );
                                IF @rowcount4 >= 1
                                    BEGIN
                                        SET @CreditsAttempted = (
                                                                SELECT Credits
                                                                FROM   arReqs
                                                                WHERE  ReqId = @reqid
                                                                );
                                        SET @CreditsEarned = 0;
                                        SET @FinAidCreditsEarned = 0;
                                    END;
                            END;

                        --For Externship Attendance						
                        IF @sysComponentTypeId = 544
                            BEGIN
                                SET @rowcount4 = (
                                                 SELECT COUNT(*)
                                                 FROM   arExternshipAttendance
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND HoursAttended >= 1
                                                 );
                                IF @rowcount4 >= 1
                                    BEGIN
                                        SET @CreditsAttempted = (
                                                                SELECT Credits
                                                                FROM   arReqs
                                                                WHERE  ReqId = @reqid
                                                                );
                                        SET @CreditsEarned = 0;
                                        SET @FinAidCreditsEarned = 0;
                                    END;

                            END;

                    END;
                /************************************************* Changes for Build 2816 *********************/

                -- If the final grade is not null the final grade will over ride current grade 
                IF @FinalGradeDesc IS NOT NULL
                    BEGIN
                        SET @CurrentGrade = @FinalGradeDesc;

                    END;
                -- If Credits was earned set completed to yes because here we are working with a Transfer credits
                IF @IsCreditsEarned = 1
                    BEGIN
                        SET @CreditsEarned = @CourseCredits;
                        SET @Completed = 1;
                    END;

                IF @IsCreditsAttempted = 0
                    BEGIN
                        SET @CreditsAttempted = 0;
                    END;

                DELETE FROM syCreditSummary
                WHERE StuEnrollId = @StuEnrollId
                      AND TermId = @TermId
                      AND ReqId = @reqid
                      AND (
                          ClsSectionId = @ClsSectionId
                          OR ClsSectionId IS NULL
                          );

                INSERT INTO syCreditSummary
                VALUES ( @StuEnrollId, @TermId, @TermDescrip, @reqid, @CourseCodeDescrip, @ClsSectionId, @CreditsEarned, @CreditsAttempted, @CurrentScore
                        ,@CurrentGrade, @FinalScore, @FinalGradeDesc, @Completed, @FinalGPA, @Product_WeightedAverage_Credits_GPA
                        ,@Count_WeightedAverage_Credits, @Product_SimpleAverage_Credits_GPA, @Count_SimpleAverage_Credits, ''sa'', GETDATE(), @ComputedSimpleGPA
                        ,@ComputedWeightedGPA, @CourseCredits, NULL, NULL, @FinAidCreditsEarned, NULL, NULL, @TermStartDate );

                DECLARE @wCourseCredits DECIMAL(18, 2)
                       ,@wWeighted_GPA_Credits DECIMAL(18, 2)
                       ,@sCourseCredits DECIMAL(18, 2)
                       ,@sSimple_GPA_Credits DECIMAL(18, 2);
                -- For weighted average
                SET @ComputedWeightedGPA = 0;
                SET @ComputedSimpleGPA = 0;
                SET @wCourseCredits = (
                                      SELECT SUM(coursecredits)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalGPA IS NOT NULL
                                      );
                SET @wWeighted_GPA_Credits = (
                                             SELECT SUM(coursecredits * FinalGPA)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND TermId = @TermId
                                                    AND FinalGPA IS NOT NULL
                                             );

                IF @wCourseCredits >= 1
                    BEGIN
                        SET @ComputedWeightedGPA = @wWeighted_GPA_Credits / @wCourseCredits;
                    END;



                --For Simple Average
                SET @sCourseCredits = (
                                      SELECT COUNT(*)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalGPA IS NOT NULL
                                      );
                SET @sSimple_GPA_Credits = (
                                           SELECT SUM(FinalGPA)
                                           FROM   syCreditSummary
                                           WHERE  StuEnrollId = @StuEnrollId
                                                  AND TermId = @TermId
                                                  AND FinalGPA IS NOT NULL
                                           );
                IF @sCourseCredits >= 1
                    BEGIN
                        SET @ComputedSimpleGPA = @sSimple_GPA_Credits / @sCourseCredits;
                    END;


                --CumulativeGPA
                DECLARE @cumCourseCredits DECIMAL(18, 2)
                       ,@cumWeighted_GPA_Credits DECIMAL(18, 2)
                       ,@cumWeightedGPA DECIMAL(18, 2);
                SET @cumWeightedGPA = 0;
                SET @cumCourseCredits = (
                                        SELECT SUM(coursecredits)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND FinalGPA IS NOT NULL
                                        );
                SET @cumWeighted_GPA_Credits = (
                                               SELECT SUM(coursecredits * FinalGPA)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalGPA IS NOT NULL
                                               );

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END;

                --CumulativeSimpleGPA
                DECLARE @cumSimpleCourseCredits DECIMAL(18, 2)
                       ,@cumSimple_GPA_Credits DECIMAL(18, 2)
                       ,@cumSimpleGPA DECIMAL(18, 2);
                SET @cumSimpleGPA = 0;
                SET @cumSimpleCourseCredits = (
                                              SELECT COUNT(coursecredits)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND FinalGPA IS NOT NULL
                                              );
                SET @cumSimple_GPA_Credits = (
                                             SELECT SUM(FinalGPA)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                             );

                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END;

                --Average calculation
                DECLARE @termAverageSum DECIMAL(18, 2)
                       ,@CumAverage DECIMAL(18, 2)
                       ,@cumAverageSum DECIMAL(18, 2)
                       ,@cumAveragecount INT;

                -- Term Average
                SET @TermAverageCount = (
                                        SELECT COUNT(*)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               --and Completed=1 
                                               AND TermId = @TermId
                                               AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                      SELECT SUM(FinalScore)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             --and Completed=1 
                                             AND TermId = @TermId
                                             AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount;

                -- Cumulative Average
                SET @cumAveragecount = (
                                       SELECT COUNT(*)
                                       FROM   syCreditSummary
                                       WHERE  StuEnrollId = @StuEnrollId
                                              --and Completed=1 
                                              AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                     SELECT SUM(FinalScore)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND
                                         --Completed=1 and 
                                         FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount;


                UPDATE syCreditSummary
                SET    TermGPA_Simple = @ComputedSimpleGPA
                      ,TermGPA_Weighted = @ComputedWeightedGPA
                      ,Average = @TermAverage
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId;

                --Update Cumulative GPA
                UPDATE syCreditSummary
                SET    CumulativeGPA = @cumWeightedGPA
                      ,CumulativeGPA_Simple = @cumSimpleGPA
                      ,CumAverage = @CumAverage
                WHERE  StuEnrollId = @StuEnrollId;



                SET @PrevStuEnrollId = @StuEnrollId;
                SET @PrevTermId = @TermId;
                SET @PrevReqId = @reqid;

                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@TermStartDate
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@FinalGrade
                    ,@sysComponentTypeId
                    ,@CreditsAttempted
                    ,@ClsSectionId
                    ,@Grade
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits
                    ,@IsTransferred;
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;
    END;
-- =========================================================================================================
-- END  --  Usp_UpdateTransferCredits_SyCreditsSummary 
-- =========================================================================================================


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_Update_SyCreditsSummary]'
GO
IF OBJECT_ID(N'[dbo].[Usp_Update_SyCreditsSummary]', 'P') IS NULL
EXEC sp_executesql N'-- =========================================================================================================
-- Usp_Update_SyCreditsSummary 
-- =========================================================================================================
CREATE PROCEDURE [dbo].[Usp_Update_SyCreditsSummary]
    @StuEnrollIdList NVARCHAR(MAX) = NULL
AS
    BEGIN
        DECLARE @PrgVerId UNIQUEIDENTIFIER
               ,@rownumber INT
               ,@StuEnrollId UNIQUEIDENTIFIER;
        DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
               ,@PrevReqId UNIQUEIDENTIFIER
               ,@PrevTermId UNIQUEIDENTIFIER
               ,@CreditsAttempted DECIMAL(18, 2)
               ,@CreditsEarned DECIMAL(18, 2)
               ,@TermId UNIQUEIDENTIFIER
               ,@TermDescrip VARCHAR(50);
        DECLARE @reqid UNIQUEIDENTIFIER
               ,@CourseCodeDescrip VARCHAR(50)
               ,@FinalGrade UNIQUEIDENTIFIER
               ,@FinalScore DECIMAL(18, 2)
               ,@ClsSectionId UNIQUEIDENTIFIER
               ,@Grade VARCHAR(50)
               ,@IsGradeBookNotSatisified BIT
               ,@TermStartDate DATETIME;
        DECLARE @IsPass BIT
               ,@IsCreditsAttempted BIT
               ,@IsCreditsEarned BIT
               ,@Completed BIT
               ,@CurrentScore DECIMAL(18, 2)
               ,@CurrentGrade VARCHAR(10)
               ,@FinalGradeDesc VARCHAR(50)
               ,@FinalGPA DECIMAL(18, 2)
               ,@GrdBkResultId UNIQUEIDENTIFIER;
        DECLARE @Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
               ,@Count_WeightedAverage_Credits DECIMAL(18, 2)
               ,@Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
               ,@Count_SimpleAverage_Credits DECIMAL(18, 2);
        DECLARE @CreditsPerService DECIMAL(18, 2)
               ,@NumberOfServicesAttempted INT
               ,@boolCourseHasLabWorkOrLabHours INT
               ,@sysComponentTypeId INT
               ,@RowCount INT;
        DECLARE @decGPALoop DECIMAL(18, 2)
               ,@intCourseCount INT
               ,@decWeightedGPALoop DECIMAL(18, 2)
               ,@IsInGPA BIT
               ,@isGradeEligibleForCreditsEarned BIT
               ,@isGradeEligibleForCreditsAttempted BIT;
        DECLARE @ComputedSimpleGPA DECIMAL(18, 2)
               ,@ComputedWeightedGPA DECIMAL(18, 2)
               ,@CourseCredits DECIMAL(18, 2);
        DECLARE @FinAidCreditsEarned DECIMAL(18, 2)
               ,@FinAidCredits DECIMAL(18, 2)
               ,@TermAverage DECIMAL(18, 2)
               ,@TermAverageCount INT
               ,@IsCourseCompleted INT;
        DECLARE @IsWeighted INT;

        DECLARE @MyEnrollments TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
            );
        INSERT INTO @MyEnrollments
                    SELECT enrollments.StuEnrollId
                    FROM   dbo.arStuEnrollments enrollments
                    WHERE  (
                           @StuEnrollIdList IS NULL
                           OR ( enrollments.StuEnrollId IN (
                                                           SELECT Val
                                                           FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                           )
                              )
                           );

        SET @decGPALoop = 0;
        SET @intCourseCount = 0;
        SET @decWeightedGPALoop = 0;
        SET @ComputedSimpleGPA = 0;
        SET @ComputedWeightedGPA = 0;
        SET @CourseCredits = 0;
        DECLARE GetCreditsSummary_Cursor CURSOR FOR
            SELECT     DISTINCT SE.StuEnrollId
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate
                               ,R.ReqId
                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                               ,RES.Score AS FinalScore
                               ,RES.GrdSysDetailId AS FinalGrade
                               ,GCT.SysComponentTypeId
                               ,R.Credits AS CreditsAttempted
                               ,CS.ClsSectionId
                               ,GSD.Grade
                               ,GSD.IsPass
                               ,GSD.IsCreditsAttempted
                               ,GSD.IsCreditsEarned
                               ,SE.PrgVerId
                               ,GSD.IsInGPA
                               ,R.FinAidCredits AS FinAidCredits
                               ,RES.IsCourseCompleted
            FROM       arStuEnrollments SE
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
            INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
            INNER JOIN arTerm T ON CS.TermId = T.TermId
            INNER JOIN arReqs R ON CS.ReqId = R.ReqId
            INNER JOIN @MyEnrollments ON [@MyEnrollments].StuEnrollId = RES.StuEnrollId
            LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                             AND GBR.StuEnrollId = SE.StuEnrollId
            LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
            LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
            LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            ORDER BY   T.StartDate
                      ,T.TermDescrip
                      ,R.ReqId;
        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor
        INTO @StuEnrollId
            ,@TermId
            ,@TermDescrip
            ,@TermStartDate
            ,@reqid
            ,@CourseCodeDescrip
            ,@FinalScore
            ,@FinalGrade
            ,@sysComponentTypeId
            ,@CreditsAttempted
            ,@ClsSectionId
            ,@Grade
            ,@IsPass
            ,@IsCreditsAttempted
            ,@IsCreditsEarned
            ,@PrgVerId
            ,@IsInGPA
            ,@FinAidCredits
            ,@IsCourseCompleted;
        --,@GrdBkResultId
        WHILE @@FETCH_STATUS = 0
            BEGIN

                SET @CourseCredits = @CreditsAttempted;
                SET @RowCount = @RowCount + 1;

                DECLARE @DefaultCampusId UNIQUEIDENTIFIER;
                SET @DefaultCampusId = NULL;
                SET @DefaultCampusId = (
                                       SELECT TOP 1 CampusId
                                       FROM   arStuEnrollments
                                       WHERE  StuEnrollId = @StuEnrollId
                                       );

                -- If the output is greater than or equal to 1 there are some grade books not satisfied
                SET @IsGradeBookNotSatisified = (
                                                SELECT COUNT(*) AS UnsatisfiedWorkUnits
                                                FROM   (
                                                       SELECT D.*
                                                             ,CASE WHEN D.MinimumScore > D.Score THEN ( D.MinimumScore - D.Score )
                                                                   ELSE 0
                                                              END AS Remaining
                                                             ,CASE WHEN ( D.MinimumScore > D.Score )
                                                                        AND ( D.MustPass = 1 ) THEN 0
                                                                   WHEN D.Score IS NULL
                                                                        AND ( D.Required = 1 ) THEN 0
                                                                   ELSE 1
                                                              END AS IsWorkUnitSatisfied
                                                       FROM   (
                                                              SELECT     T.TermId
                                                                        ,T.TermDescrip
                                                                        ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                                                        ,R.ReqId
                                                                        ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                                        ,( CASE WHEN GCT.SysComponentTypeId IN ( 500, 503, 504, 544 ) THEN GBWD.Number
                                                                                ELSE (
                                                                                     SELECT MIN(MinVal)
                                                                                     FROM   arGradeScaleDetails GSD
                                                                                           ,arGradeSystemDetails GSS
                                                                                     WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                                            AND GSS.IsPass = 1
                                                                                     )
                                                                           END
                                                                         ) AS MinimumScore
                                                                        ,GBR.Score AS Score
                                                                        ,GBWD.Weight AS Weight
                                                                        ,RES.Score AS FinalScore
                                                                        ,RES.GrdSysDetailId AS FinalGrade
                                                                        ,GBWD.Required
                                                                        ,GBWD.MustPass
                                                                        ,GBWD.GrdPolicyId
                                                                        ,( CASE GCT.SysComponentTypeId
                                                                                WHEN 544 THEN (
                                                                                              SELECT SUM(HoursAttended)
                                                                                              FROM   arExternshipAttendance
                                                                                              WHERE  StuEnrollId = SE.StuEnrollId
                                                                                              )
                                                                                ELSE GBR.Score
                                                                           END
                                                                         ) AS GradeBookResult
                                                                        ,GCT.SysComponentTypeId
                                                                        ,SE.StuEnrollId
                                                                        ,GBR.GrdBkResultId
                                                                        ,R.Credits AS CreditsAttempted
                                                                        ,CS.ClsSectionId
                                                                        ,GSD.Grade
                                                                        ,GSD.IsPass
                                                                        ,GSD.IsCreditsAttempted
                                                                        ,GSD.IsCreditsEarned
                                                              FROM       arStuEnrollments SE
                                                              INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                                              INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                                                              INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                                                              INNER JOIN arTerm T ON CS.TermId = T.TermId
                                                              INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                                              LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                                                                               AND GBR.StuEnrollId = SE.StuEnrollId
                                                              LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                                              LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                                              LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                                              WHERE      SE.StuEnrollId = @StuEnrollId
                                                                         AND T.TermId = @TermId
                                                                         AND R.ReqId = @reqid
                                                              ) D
                                                       ) E
                                                WHERE  IsWorkUnitSatisfied = 0
                                                );

                SET @IsWeighted = (
                                  SELECT COUNT(*) AS WeightsCount
                                  FROM   (
                                         SELECT     T.TermId
                                                   ,T.TermDescrip
                                                   ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                                   ,R.ReqId
                                                   ,RTRIM(GCT.Descrip) AS GradeBookDescription
                                                   ,( CASE WHEN GCT.SysComponentTypeId IN ( 500, 503, 504, 544 ) THEN GBWD.Number
                                                           ELSE (
                                                                SELECT MIN(MinVal)
                                                                FROM   arGradeScaleDetails GSD
                                                                      ,arGradeSystemDetails GSS
                                                                WHERE  GSD.GrdSysDetailId = GSS.GrdSysDetailId
                                                                       AND GSS.IsPass = 1
                                                                )
                                                      END
                                                    ) AS MinimumScore
                                                   ,GBR.Score AS Score
                                                   ,GBWD.Weight AS Weight
                                                   ,RES.Score AS FinalScore
                                                   ,RES.GrdSysDetailId AS FinalGrade
                                                   ,GBWD.Required
                                                   ,GBWD.MustPass
                                                   ,GBWD.GrdPolicyId
                                                   ,( CASE GCT.SysComponentTypeId
                                                           WHEN 544 THEN (
                                                                         SELECT SUM(HoursAttended)
                                                                         FROM   arExternshipAttendance
                                                                         WHERE  StuEnrollId = SE.StuEnrollId
                                                                         )
                                                           ELSE GBR.Score
                                                      END
                                                    ) AS GradeBookResult
                                                   ,GCT.SysComponentTypeId
                                                   ,SE.StuEnrollId
                                                   ,GBR.GrdBkResultId
                                                   ,R.Credits AS CreditsAttempted
                                                   ,CS.ClsSectionId
                                                   ,GSD.Grade
                                                   ,GSD.IsPass
                                                   ,GSD.IsCreditsAttempted
                                                   ,GSD.IsCreditsEarned
                                         FROM       arStuEnrollments SE
                                         INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                         INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                                         INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                                         INNER JOIN arTerm T ON CS.TermId = T.TermId
                                         INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                                         LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                                                                          AND GBR.StuEnrollId = SE.StuEnrollId
                                         LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                                         LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                                         LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                                         WHERE      SE.StuEnrollId = @StuEnrollId
                                                    AND T.TermId = @TermId
                                                    AND R.ReqId = @reqid
                                         ) D
                                  WHERE  Weight >= 1
                                  );


                --Check if IsCreditsAttempted is set to True
                IF (
                   @IsCreditsAttempted IS NULL
                   OR @IsCreditsAttempted = 0
                   )
                    BEGIN
                        SET @CreditsAttempted = 0;
                    END;
                IF (
                   @IsCreditsEarned IS NULL
                   OR @IsCreditsEarned = 0
                   )
                    BEGIN
                        SET @CreditsEarned = 0;
                    END;


                IF ( @IsGradeBookNotSatisified >= 1 )
                    BEGIN
                        IF ( @FinalScore IS NULL )
                            BEGIN
                                SET @CreditsEarned = 0;
                                SET @Completed = 0;
                            END;
                    END;
                ELSE
                    BEGIN
                        SET @GrdBkResultId = (
                                             SELECT TOP 1 GrdBkResultId
                                             FROM   arGrdBkResults
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND ClsSectionId = @ClsSectionId
                                             );
                        IF @GrdBkResultId IS NOT NULL
                            BEGIN
                                IF ( @IsCreditsEarned = 1 )
                                    BEGIN
                                        SET @CreditsEarned = @CreditsAttempted;
                                        SET @FinAidCreditsEarned = @FinAidCredits;
                                    END;
                                SET @Completed = 1;
                            END;

                        IF (
                           @GrdBkResultId IS NULL
                           AND @Grade IS NOT NULL
                           )
                            BEGIN
                                IF ( @IsCreditsEarned = 1 )
                                    BEGIN
                                        SET @CreditsEarned = @CreditsAttempted;
                                        SET @FinAidCreditsEarned = @FinAidCredits;
                                    END;
                                SET @Completed = 1;
                            END;
                    END;



                IF (
                   @FinalScore IS NOT NULL
                   AND @Grade IS NOT NULL
                   )
                    BEGIN
                        IF ( @IsCreditsEarned = 1 )
                            BEGIN
                                SET @CreditsEarned = @CreditsAttempted;
                                SET @FinAidCreditsEarned = @FinAidCredits;
                            END;
                        SET @Completed = 1;

                    END;

                -- If course is not part of the program version definition do not add credits earned and credits attempted
                -- set the credits earned and attempted to zero
                DECLARE @coursepartofdefinition INT;
                SET @coursepartofdefinition = 0;

                --Commented by Balaji on 1/16/2016 as it conflicts with Course Equivalency
                -- Due to following logic if a student takes a equivalent course, credits attempted considered as 0							

                --set @coursepartofdefinition = (select COUNT(*) as RowCountOfProgramDefinition from 
                --									(
                --										select * from arProgVerDef where PrgVerId=@PrgVerId and ReqId=@ReqId
                --											union
                --										select * from arProgVerDef where PrgVerId=@PrgVerId and ReqId in
                --											(select GrpId from arReqGrpDef where ReqId=@ReqId)
                --									) dt

                --			)
                --if (@coursepartofdefinition = 0)
                --	begin
                --		set @CreditsEarned = 0
                --		set @CreditsAttempted = 0
                --		set @FinAidCreditsEarned = 0
                --	end

                -- Check the grade scale associated with the class section and figure out of the final score was a passing score
                DECLARE @coursepassrowcount INT;
                SET @coursepassrowcount = 0;
                IF ( @FinalScore IS NOT NULL )

                    -- If the student scores 56 and the score is a passing score then we consider this course as completed
                    BEGIN
                        SET @coursepassrowcount = (
                                                  SELECT     COUNT(t2.MinVal) AS IsCourseCompleted
                                                  FROM       arClassSections t1
                                                  INNER JOIN arGradeScaleDetails t2 ON t1.GrdScaleId = t2.GrdScaleId
                                                  INNER JOIN arGradeSystemDetails t3 ON t2.GrdSysDetailId = t3.GrdSysDetailId
                                                  WHERE      t1.ClsSectionId = @ClsSectionId
                                                             AND t3.IsPass = 1
                                                             AND @FinalScore >= t2.MinVal
                                                  );
                        IF @coursepassrowcount >= 1
                            BEGIN
                                SET @Completed = 1;
                            END;
                        ELSE
                            BEGIN
                                SET @Completed = 0;
                            END;
                    END;
                -- If Student Scored a Failing Grade (IsPass set to 0 in Grade System)
                -- then mark this course as Incomplete
                IF ( @FinalGrade IS NOT NULL )
                    BEGIN
                        IF ( @IsPass = 0 )
                            BEGIN
                                SET @Completed = 0;
                                IF ( @IsCreditsEarned = 0 )
                                    BEGIN
                                        SET @CreditsEarned = 0;
                                        SET @FinAidCreditsEarned = 0;
                                    END;
                                IF ( @IsCreditsAttempted = 0 )
                                    BEGIN
                                        SET @CreditsAttempted = 0;
                                    END;
                            END;
                    END;

                SET @CurrentScore = (
                                    SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                ELSE NULL
                                           END AS CurrentScore
                                    FROM   (
                                           SELECT   InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight AS GradeBookWeight
                                                   ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                         ELSE 0
                                                    END AS ActualWeight
                                           FROM     (
                                                    SELECT   C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,ISNULL(C.Weight, 0) AS Weight
                                                            ,C.Number AS MinNumber
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter AS Param
                                                            ,X.GrdScaleId
                                                            ,SUM(GR.Score) AS Score
                                                            ,COUNT(D.Descrip) AS NumberOfComponents
                                                    FROM     (
                                                             SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                    ,A.EffectiveDate
                                                                                    ,B.GrdScaleId
                                                             FROM     arGrdBkWeights A
                                                                     ,arClassSections B
                                                             WHERE    A.ReqId = B.ReqId
                                                                      AND A.EffectiveDate <= B.StartDate
                                                                      AND B.ClsSectionId = @ClsSectionId
                                                             ORDER BY A.EffectiveDate DESC
                                                             ) X
                                                            ,arGrdBkWgtDetails C
                                                            ,arGrdComponentTypes D
                                                            ,arGrdBkResults GR
                                                    WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                             AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                             AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                             AND D.SysComponentTypeId NOT IN ( 500, 503 )
                                                             AND GR.StuEnrollId = @StuEnrollId
                                                             AND GR.ClsSectionId = @ClsSectionId
                                                             AND GR.Score IS NOT NULL
                                                    GROUP BY C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,C.Weight
                                                            ,C.Number
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter
                                                            ,X.GrdScaleId
                                                    ) S
                                           GROUP BY InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight
                                                   ,NumberOfComponents
                                           ) FinalTblToComputeCurrentScore
                                    );
                IF ( @CurrentScore IS NULL )
                    BEGIN
                        -- instructor grade books
                        SET @CurrentScore = (
                                            SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                        ELSE NULL
                                                   END AS CurrentScore
                                            FROM   (
                                                   SELECT   InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                   FROM     (
                                                            SELECT   C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,ISNULL(C.Weight, 0) AS Weight
                                                                    ,C.Number AS MinNumber
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter AS Param
                                                                    ,X.GrdScaleId
                                                                    ,SUM(GR.Score) AS Score
                                                                    ,COUNT(D.Descrip) AS NumberOfComponents
                                                            FROM     (
                                                                     --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
                                                                     --FROM          arGrdBkWeights A,arClassSections B        
                                                                     --WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
                                                                     --ORDER BY      A.EffectiveDate DESC
                                                                     SELECT DISTINCT TOP 1 t1.InstrGrdBkWgtId
                                                                                          ,t1.GrdScaleId
                                                                     FROM   arClassSections t1
                                                                           ,arGrdBkWeights t2
                                                                     WHERE  t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                            AND t1.ClsSectionId = @ClsSectionId
                                                                     ) X
                                                                    ,arGrdBkWgtDetails C
                                                                    ,arGrdComponentTypes D
                                                                    ,arGrdBkResults GR
                                                            WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                     AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                     AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                     AND
                                                                -- D.SysComponentTypeID not in (500,503) and 
                                                                GR.StuEnrollId = @StuEnrollId
                                                                     AND GR.ClsSectionId = @ClsSectionId
                                                                     AND GR.Score IS NOT NULL
                                                            GROUP BY C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,C.Weight
                                                                    ,C.Number
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter
                                                                    ,X.GrdScaleId
                                                            ) S
                                                   GROUP BY InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                   ) FinalTblToComputeCurrentScore
                                            );

                    END;

                IF ( @CurrentScore IS NOT NULL )
                    BEGIN
                        SET @CurrentGrade = (
                                            SELECT t2.Grade
                                            FROM   arGradeScaleDetails t1
                                                  ,arGradeSystemDetails t2
                                            WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                   AND t1.GrdScaleId IN (
                                                                        SELECT GrdScaleId
                                                                        FROM   arClassSections
                                                                        WHERE  ClsSectionId = @ClsSectionId
                                                                        )
                                                   AND @CurrentScore >= t1.MinVal
                                                   AND @CurrentScore <= t1.MaxVal
                                            );

                    END;
                ELSE
                    BEGIN
                        SET @CurrentGrade = NULL;
                    END;


                IF (
                   @CurrentScore IS NULL
                   AND @CurrentGrade IS NULL
                   AND @FinalScore IS NULL
                   AND @FinalGrade IS NULL
                   )
                    BEGIN
                        SET @Completed = 0;
                        SET @CreditsAttempted = 0;
                        SET @CreditsEarned = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;


                IF (
                   @FinalScore IS NOT NULL
                   OR @FinalGrade IS NOT NULL
                   )
                    BEGIN

                        SET @FinalGradeDesc = (
                                              SELECT Grade
                                              FROM   arGradeSystemDetails
                                              WHERE  GrdSysDetailId = @FinalGrade
                                              );



                        IF ( @FinalGradeDesc IS NULL )
                            BEGIN
                                SET @FinalGradeDesc = (
                                                      SELECT t2.Grade
                                                      FROM   arGradeScaleDetails t1
                                                            ,arGradeSystemDetails t2
                                                      WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                             AND t1.GrdScaleId IN (
                                                                                  SELECT GrdScaleId
                                                                                  FROM   arClassSections
                                                                                  WHERE  ClsSectionId = @ClsSectionId
                                                                                  )
                                                             AND @FinalScore >= t1.MinVal
                                                             AND @FinalScore <= t1.MaxVal
                                                      );
                            END;
                        SET @FinalGPA = (
                                        SELECT GPA
                                        FROM   arGradeSystemDetails
                                        WHERE  GrdSysDetailId = @FinalGrade
                                        );
                        IF @FinalGPA IS NULL
                            BEGIN
                                SET @FinalGPA = (
                                                SELECT t2.GPA
                                                FROM   arGradeScaleDetails t1
                                                      ,arGradeSystemDetails t2
                                                WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                       AND t1.GrdScaleId IN (
                                                                            SELECT GrdScaleId
                                                                            FROM   arClassSections
                                                                            WHERE  ClsSectionId = @ClsSectionId
                                                                            )
                                                       AND @FinalScore >= t1.MinVal
                                                       AND @FinalScore <= t1.MaxVal
                                                );
                            END;
                    END;
                ELSE
                    BEGIN
                        SET @FinalGradeDesc = NULL;
                        SET @FinalGPA = NULL;
                    END;


                SET @isGradeEligibleForCreditsEarned = (
                                                       SELECT t2.IsCreditsEarned
                                                       FROM   arGradeScaleDetails t1
                                                             ,arGradeSystemDetails t2
                                                       WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                              AND t1.GrdScaleId IN (
                                                                                   SELECT GrdScaleId
                                                                                   FROM   arClassSections
                                                                                   WHERE  ClsSectionId = @ClsSectionId
                                                                                   )
                                                              AND t2.Grade = @FinalGradeDesc
                                                       );

                SET @isGradeEligibleForCreditsAttempted = (
                                                          SELECT t2.IsCreditsAttempted
                                                          FROM   arGradeScaleDetails t1
                                                                ,arGradeSystemDetails t2
                                                          WHERE  t1.GrdSysDetailId = t2.GrdSysDetailId
                                                                 AND t1.GrdScaleId IN (
                                                                                      SELECT GrdScaleId
                                                                                      FROM   arClassSections
                                                                                      WHERE  ClsSectionId = @ClsSectionId
                                                                                      )
                                                                 AND t2.Grade = @FinalGradeDesc
                                                          );

                IF ( @isGradeEligibleForCreditsEarned IS NULL )
                    BEGIN
                        SET @isGradeEligibleForCreditsEarned = (
                                                               SELECT TOP 1 t2.IsCreditsEarned
                                                               FROM   arGradeSystemDetails t2
                                                               WHERE  t2.Grade = @FinalGradeDesc
                                                               );
                    END;

                IF ( @isGradeEligibleForCreditsAttempted IS NULL )
                    BEGIN
                        SET @isGradeEligibleForCreditsAttempted = (
                                                                  SELECT TOP 1 t2.IsCreditsAttempted
                                                                  FROM   arGradeSystemDetails t2
                                                                  WHERE  t2.Grade = @FinalGradeDesc
                                                                  );
                    END;

                IF @isGradeEligibleForCreditsEarned = 0
                    BEGIN
                        SET @CreditsEarned = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;
                IF @isGradeEligibleForCreditsAttempted = 0
                    BEGIN
                        SET @CreditsAttempted = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;

                IF ( @IsPass = 0 )
                    BEGIN
                        SET @CreditsEarned = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;
                --For Letter Grade Schools if the score is null but final grade was posted then the 
                --Final grade will be the current grade
                IF @CurrentGrade IS NULL
                   AND @FinalGradeDesc IS NOT NULL
                    BEGIN
                        SET @CurrentGrade = @FinalGradeDesc;
                    END;

                IF (
                   @sysComponentTypeId = 503
                   OR @sysComponentTypeId = 500
                   ) -- Lab work or Lab Hours
                    BEGIN
                        -- This course has lab work and lab hours
                        IF ( @Completed = 0 )
                            BEGIN
                                SET @CreditsPerService = (
                                                         SELECT TOP 1 GD.CreditsPerService
                                                         FROM   arGrdBkWeights GBW
                                                               ,arGrdComponentTypes GC
                                                               ,arGrdBkWgtDetails GD
                                                         WHERE  GBW.InstrGrdBkWgtId = GD.InstrGrdBkWgtId
                                                                AND GC.GrdComponentTypeId = GD.GrdComponentTypeId
                                                                AND GBW.ReqId = @reqid
                                                                AND GC.SysComponentTypeId IN ( 500, 503 )
                                                         );
                                SET @NumberOfServicesAttempted = (
                                                                 SELECT     TOP 1 GBR.Score AS NumberOfServicesAttempted
                                                                 FROM       arStuEnrollments SE
                                                                 INNER JOIN arGrdBkResults GBR ON SE.StuEnrollId = GBR.StuEnrollId
                                                                                                  AND GBR.ClsSectionId = @ClsSectionId
                                                                 );

                                SET @CreditsEarned = ISNULL(@CreditsPerService, 0) * ISNULL(@NumberOfServicesAttempted, 0);
                            END;
                    END;

                DECLARE @rowAlreadyInserted INT;
                SET @rowAlreadyInserted = 0;

                -- Get the final Gpa only when IsCreditsAttempted is set to 1 and IsInGPA is set to 1
                IF @IsInGPA = 1
                    BEGIN
                        IF ( @IsCreditsAttempted = 0 )
                            BEGIN
                                SET @FinalGPA = NULL;
                            END;
                    END;
                ELSE
                    BEGIN
                        SET @FinalGPA = NULL;
                    END;

                IF @FinalScore IS NOT NULL --and @CurrentScore is null
                    BEGIN
                        SET @CurrentScore = @FinalScore;
                    END;

                --Select * from syConfigAppSetValues


                -- Rally case DE 738 KeyBoarding Courses
                DECLARE @GradesFormat VARCHAR(50);
                ---- PRINT @DefaultCampusId
                --(select Top 1 Value from syConfigAppSetValues where SettingId=47 and (CampusId=@DefaultCampusId or CampusId is NULL))
                SET @GradesFormat = (
                                    SELECT TOP 1 Value
                                    FROM   syConfigAppSetValues
                                    WHERE  SettingId = 47
                                           AND (
                                               CampusId = @DefaultCampusId
                                               OR CampusId IS NULL
                                               )
                                    ); -- 47 refers to grades format

                ---- PRINT @GradesFormat
                -- This condition is met only for numeric grade schools
                IF (
                   @IsGradeBookNotSatisified = 0
                   AND @IsWeighted = 0
                   AND @FinalScore IS NULL
                   AND @IsCourseCompleted = 1
                   AND @FinalGradeDesc IS NULL
                   AND LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
                   )
                    BEGIN
                        SET @Completed = 1;
                        SET @CreditsAttempted = (
                                                SELECT Credits
                                                FROM   arReqs
                                                WHERE  ReqId = @reqid
                                                );
                        SET @FinAidCredits = (
                                             SELECT FinAidCredits
                                             FROM   arReqs
                                             WHERE  ReqId = @reqid
                                             );
                        SET @CreditsAttempted = @CreditsAttempted;
                        SET @CreditsEarned = @CreditsAttempted;
                        SET @FinAidCreditsEarned = @FinAidCredits;
                    END;

                -- DE748 Name: ROSS: Completed field should also check for the Must Pass property of the work unit. 
                IF LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
                   AND @IsGradeBookNotSatisified >= 1
                    BEGIN
                        SET @Completed = 0;
                        SET @CreditsEarned = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;

                --DE738 Name: ROSS: Progress Report not taking care of courses that are not weighted. 
                IF (
                   LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
                   AND @Completed = 1
                   AND @FinalScore IS NULL
                   AND @FinalGradeDesc IS NULL
                   )
                    BEGIN
                        SET @CreditsAttempted = @CreditsAttempted;
                        SET @CreditsEarned = @CreditsAttempted;
                        SET @FinAidCreditsEarned = @FinAidCredits;
                    END;

                -- In Ross Example : Externship, the student may not have completed the course but once he attempts a work unit
                -- we need to take the credits as attempted
                IF (
                   LOWER(LTRIM(RTRIM(@GradesFormat))) <> ''letter''
                   AND @Completed = 0
                   AND @FinalScore IS NULL
                   AND @FinalGradeDesc IS NULL
                   )
                    BEGIN
                        DECLARE @rowcount4 INT;
                        SET @rowcount4 = (
                                         SELECT COUNT(*)
                                         FROM   arGrdBkResults
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND ClsSectionId = @ClsSectionId
                                                AND Score IS NOT NULL
                                         );
                        IF @rowcount4 >= 1
                            BEGIN
                                -- -- PRINT ''Gets in to if''
                                SET @CreditsAttempted = (
                                                        SELECT Credits
                                                        FROM   arReqs
                                                        WHERE  ReqId = @reqid
                                                        );
                                SET @CreditsEarned = 0;
                                SET @FinAidCreditsEarned = 0;
                            END;
                        ELSE
                            BEGIN
                                SET @rowcount4 = (
                                                 SELECT COUNT(*)
                                                 FROM   arGrdBkConversionResults
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND ReqId = @reqid
                                                        AND TermId = @TermId
                                                        AND Score IS NOT NULL
                                                 );
                                IF @rowcount4 >= 1
                                    BEGIN
                                        SET @CreditsAttempted = (
                                                                SELECT Credits
                                                                FROM   arReqs
                                                                WHERE  ReqId = @reqid
                                                                );
                                        SET @CreditsEarned = 0;
                                        SET @FinAidCreditsEarned = 0;
                                    END;
                            END;

                        --For Externship Attendance						
                        IF @sysComponentTypeId = 544
                            BEGIN
                                SET @rowcount4 = (
                                                 SELECT COUNT(*)
                                                 FROM   arExternshipAttendance
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND HoursAttended >= 1
                                                 );
                                IF @rowcount4 >= 1
                                    BEGIN
                                        SET @CreditsAttempted = (
                                                                SELECT Credits
                                                                FROM   arReqs
                                                                WHERE  ReqId = @reqid
                                                                );
                                        SET @CreditsEarned = 0;
                                        SET @FinAidCreditsEarned = 0;
                                    END;

                            END;

                    END;

                -- If the final grade is not null the final grade will over ride current grade 
                IF @FinalGradeDesc IS NOT NULL
                    BEGIN
                        SET @CurrentGrade = @FinalGradeDesc;

                    END;


                -- For Letter Grade Schools, if any one of work unit is attempted and if no final grade is posted then 
                -- set credit attempted
                DECLARE @IsScorePostedForAnyWorkUnit INT; -- If value>=1 then score was posted
                SET @IsScorePostedForAnyWorkUnit = (
                                                   SELECT COUNT(*)
                                                   FROM   arGrdBkResults
                                                   WHERE  StuEnrollId = @StuEnrollId
                                                          AND ClsSectionId = @ClsSectionId
                                                          AND Score IS NOT NULL
                                                   );
                IF (
                   LOWER(LTRIM(RTRIM(@GradesFormat))) = ''letter''
                   AND @FinalGradeDesc IS NULL
                   AND @IsScorePostedForAnyWorkUnit >= 1
                   )
                    BEGIN
                        SET @Completed = 0;
                        SET @CreditsAttempted = (
                                                SELECT Credits
                                                FROM   arReqs
                                                WHERE  ReqId = @reqid
                                                );
                        SET @CreditsEarned = 0;
                        SET @FinAidCreditsEarned = 0;
                    END;


                -- DE 996 Transfer Grades has completed set to no even when the credits were earned.
                -- If Credits was earned set completed to yes
                IF @IsCreditsEarned = 1
                    BEGIN
                        SET @Completed = 1;
                    END;

                -- Modified by Balaji to take IsCourseCompleted flag in to account
                IF @Completed = 0
                   AND @IsCourseCompleted = 1
                    BEGIN
                        SET @Completed = 1;
                    END;


                IF @Completed = 0
                   AND @IsCourseCompleted = 1
                    BEGIN
                        SET @Completed = 1;
                    END;

                DECLARE @IsExternship INT
                       ,@intExternshipCount INT;
                SET @intExternshipCount = (
                                          SELECT COUNT(*)
                                          FROM   arReqs
                                          WHERE  ReqId = @reqid
                                                 AND IsExternship = 1
                                          );


                --Externship Courses
                IF @intExternshipCount = 1
                   AND @Completed = 1
                   AND @CreditsAttempted > 0
                    BEGIN
                        SET @IsCreditsAttempted = 1;
                        SET @IsCreditsEarned = 1;
                        SET @CreditsEarned = @CreditsAttempted;

                    END;

                --Lab Work and Lab Hours
                IF (
                   @sysComponentTypeId = 500
                   OR @sysComponentTypeId = 503
                   )
                   AND @Completed = 1
                   AND @CreditsAttempted > 0
                    BEGIN
                        SET @IsCreditsAttempted = 1;
                        SET @IsCreditsEarned = 1;
                        SET @CreditsEarned = @CreditsAttempted;
                    END;

                IF @Completed = 1
                    BEGIN
                        SET @FinAidCreditsEarned = @FinAidCredits;
                    END;

                -- If course is externship and no attendance yet, set credits attempted to 0
                IF @intExternshipCount = 1
                    BEGIN
                        DECLARE @ExternshipAttValue DECIMAL(18, 2);
                        SET @ExternshipAttValue = 0.00;
                        SET @ExternshipAttValue = (
                                                  SELECT SUM(ISNULL(HoursAttended, 0.00))
                                                  FROM   arExternshipAttendance
                                                  WHERE  StuEnrollId = @StuEnrollId
                                                  );
                        IF @ExternshipAttValue = 0.00
                           OR @ExternshipAttValue IS NULL
                            BEGIN
                                SET @CreditsAttempted = 0;
                                SET @CreditsEarned = 0;
                                SET @Completed = 0;
                            END;
                    END;


                --declare @rowcount_CA int
                --set @rowcount_CA = (select Count(*) from arGrdBkResults where StuEnrollId=@StuEnrollId and ClsSectionId=@ClsSectionId and Score is not null)
                --if @rowcount_CA<=0 OR @rowcount IS NULL
                --	begin
                --				set @CreditsAttempted = 0
                --				set @CreditsEarned = 0
                --				set @FinAidCreditsEarned = 0
                --	end


                DELETE FROM syCreditSummary
                WHERE StuEnrollId = @StuEnrollId
                      AND TermId = @TermId
                      AND ReqId = @reqid
                      AND (
                          ClsSectionId = @ClsSectionId
                          OR ClsSectionId IS NULL
                          );

                INSERT INTO syCreditSummary
                VALUES ( @StuEnrollId, @TermId, @TermDescrip, @reqid, @CourseCodeDescrip, @ClsSectionId, @CreditsEarned, @CreditsAttempted, @CurrentScore
                        ,@CurrentGrade, @FinalScore, @FinalGradeDesc, @Completed, @FinalGPA, @Product_WeightedAverage_Credits_GPA
                        ,@Count_WeightedAverage_Credits, @Product_SimpleAverage_Credits_GPA, @Count_SimpleAverage_Credits, ''sa'', GETDATE(), @ComputedSimpleGPA
                        ,@ComputedWeightedGPA, @CourseCredits, NULL, NULL, @FinAidCreditsEarned, NULL, NULL, @TermStartDate );

                DECLARE @wCourseCredits DECIMAL(18, 2)
                       ,@wWeighted_GPA_Credits DECIMAL(18, 2)
                       ,@sCourseCredits DECIMAL(18, 2)
                       ,@sSimple_GPA_Credits DECIMAL(18, 2);
                -- For weighted average
                SET @ComputedWeightedGPA = 0;
                SET @ComputedSimpleGPA = 0;
                SET @wCourseCredits = (
                                      SELECT SUM(coursecredits)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalGPA IS NOT NULL
                                      );
                SET @wWeighted_GPA_Credits = (
                                             SELECT SUM(coursecredits * FinalGPA)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND TermId = @TermId
                                                    AND FinalGPA IS NOT NULL
                                             );

                IF @wCourseCredits >= 1
                    BEGIN
                        SET @ComputedWeightedGPA = @wWeighted_GPA_Credits / @wCourseCredits;
                    END;

                --For Simple Average
                SET @sCourseCredits = (
                                      SELECT COUNT(*)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalGPA IS NOT NULL
                                      );
                SET @sSimple_GPA_Credits = (
                                           SELECT SUM(FinalGPA)
                                           FROM   syCreditSummary
                                           WHERE  StuEnrollId = @StuEnrollId
                                                  AND TermId = @TermId
                                                  AND FinalGPA IS NOT NULL
                                           );


                -- 3.7 SP2 Changes
                -- Include Equivalent Courses
                DECLARE @EquivCourse_SA_CC INT
                       ,@EquivCourse_SA_GPA DECIMAL(18, 2);
                SET @EquivCourse_SA_CC = (
                                         SELECT ISNULL(COUNT(Credits), 0) AS Credits
                                         FROM   arReqs
                                         WHERE  ReqId IN (
                                                         SELECT     CE.EquivReqId
                                                         FROM       arCourseEquivalent CE
                                                         INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                         INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                         WHERE      StuEnrollId = @StuEnrollId
                                                                    AND R.GrdSysDetailId IS NOT NULL
                                                         )
                                         );

                SET @EquivCourse_SA_GPA = (
                                          SELECT     SUM(ISNULL(GSD.GPA, 0.00))
                                          FROM       arCourseEquivalent CE
                                          INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                          INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                          INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                          WHERE      StuEnrollId = @StuEnrollId
                                                     AND R.GrdSysDetailId IS NOT NULL
                                          );

                SET @sCourseCredits = @sCourseCredits; --+ ISNULL(@EquivCourse_SA_CC,0.00);
                SET @sSimple_GPA_Credits = @sSimple_GPA_Credits; --+ ISNULL(@EquivCourse_SA_GPA,0.00);

                IF @sCourseCredits >= 1
                    BEGIN
                        SET @ComputedSimpleGPA = @sSimple_GPA_Credits / @sCourseCredits;
                    END;


                --CumulativeGPA
                DECLARE @cumCourseCredits DECIMAL(18, 2)
                       ,@cumWeighted_GPA_Credits DECIMAL(18, 2)
                       ,@cumWeightedGPA DECIMAL(18, 2);
                SET @cumWeightedGPA = 0;
                SET @cumCourseCredits = (
                                        SELECT SUM(coursecredits)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND FinalGPA IS NOT NULL
                                        );
                SET @cumWeighted_GPA_Credits = (
                                               SELECT SUM(coursecredits * FinalGPA)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalGPA IS NOT NULL
                                               );

                DECLARE @EquivCourse_WGPA_CC1 INT
                       ,@EquivCourse_WGPA_GPA1 DECIMAL(18, 2);
                SET @EquivCourse_WGPA_CC1 = (
                                            SELECT SUM(ISNULL(Credits, 0)) AS Credits
                                            FROM   arReqs
                                            WHERE  ReqId IN (
                                                            SELECT     CE.EquivReqId
                                                            FROM       arCourseEquivalent CE
                                                            INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                            INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                            WHERE      StuEnrollId = @StuEnrollId
                                                                       AND R.GrdSysDetailId IS NOT NULL
                                                            )
                                            );

                SET @EquivCourse_WGPA_GPA1 = (
                                             SELECT     SUM(GSD.GPA * R1.Credits)
                                             FROM       arCourseEquivalent CE
                                             INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                             INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                             INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                             INNER JOIN arReqs R1 ON R1.ReqId = CE.ReqId
                                             WHERE      StuEnrollId = @StuEnrollId
                                                        AND R.GrdSysDetailId IS NOT NULL
                                             );

                SET @cumCourseCredits = @sCourseCredits; -- + ISNULL(@EquivCourse_WGPA_CC1,0.00);
                SET @cumWeighted_GPA_Credits = @sSimple_GPA_Credits; -- + ISNULL(@EquivCourse_WGPA_GPA1,0.00);

                SET @cumCourseCredits = @cumCourseCredits; -- + ISNULL(@EquivCourse_WGPA_CC1,0.00);
                SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits; -- + ISNULL(@EquivCourse_WGPA_GPA1,0.00);


                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END;

                --CumulativeSimpleGPA
                DECLARE @cumSimpleCourseCredits DECIMAL(18, 2)
                       ,@cumSimple_GPA_Credits DECIMAL(18, 2)
                       ,@cumSimpleGPA DECIMAL(18, 2);
                SET @cumSimpleGPA = 0;
                SET @cumSimpleCourseCredits = (
                                              SELECT COUNT(coursecredits)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND FinalGPA IS NOT NULL
                                              );
                SET @cumSimple_GPA_Credits = (
                                             SELECT SUM(FinalGPA)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                             );

                DECLARE @EquivCourse_SA_CC2 INT
                       ,@EquivCourse_SA_GPA2 DECIMAL(18, 2);
                SET @EquivCourse_SA_CC2 = (
                                          SELECT ISNULL(COUNT(Credits), 0) AS Credits
                                          FROM   arReqs
                                          WHERE  ReqId IN (
                                                          SELECT     CE.EquivReqId
                                                          FROM       arCourseEquivalent CE
                                                          INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                          INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                          WHERE      StuEnrollId = @StuEnrollId
                                                                     AND R.GrdSysDetailId IS NOT NULL
                                                          )
                                          );

                SET @EquivCourse_SA_GPA2 = (
                                           SELECT     SUM(ISNULL(GSD.GPA, 0.00))
                                           FROM       arCourseEquivalent CE
                                           INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                           INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                           INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                           WHERE      StuEnrollId = @StuEnrollId
                                                      AND R.GrdSysDetailId IS NOT NULL
                                           );


                SET @cumSimpleCourseCredits = @cumSimpleCourseCredits; -- + ISNULL(@EquivCourse_SA_CC2,0.00);
                SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits; -- + ISNULL(@EquivCourse_SA_GPA2,0.00);

                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END;

                --Average calculation
                DECLARE @termAverageSum DECIMAL(18, 2)
                       ,@CumAverage DECIMAL(18, 2)
                       ,@cumAverageSum DECIMAL(18, 2)
                       ,@cumAveragecount INT;

                -- Term Average
                SET @TermAverageCount = (
                                        SELECT COUNT(*)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               --and Completed=1 
                                               AND TermId = @TermId
                                               AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                      SELECT SUM(FinalScore)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             --and Completed=1 
                                             AND TermId = @TermId
                                             AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount;

                -- Cumulative Average
                SET @cumAveragecount = (
                                       SELECT COUNT(*)
                                       FROM   syCreditSummary
                                       WHERE  StuEnrollId = @StuEnrollId
                                              --and Completed=1 
                                              AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                     SELECT SUM(FinalScore)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND
                                         --Completed=1 and 
                                         FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount;

                UPDATE syCreditSummary
                SET    TermGPA_Simple = @ComputedSimpleGPA
                      ,TermGPA_Weighted = @ComputedWeightedGPA
                      ,Average = @TermAverage
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId;

                --Update Cumulative GPA
                UPDATE syCreditSummary
                SET    CumulativeGPA = @cumWeightedGPA
                      ,CumulativeGPA_Simple = @cumSimpleGPA
                      ,CumAverage = @CumAverage
                WHERE  StuEnrollId = @StuEnrollId;


                SET @PrevStuEnrollId = @StuEnrollId;
                SET @PrevTermId = @TermId;
                SET @PrevReqId = @reqid;

                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@TermStartDate
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@FinalGrade
                    ,@sysComponentTypeId
                    ,@CreditsAttempted
                    ,@ClsSectionId
                    ,@Grade
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits
                    ,@IsCourseCompleted;
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;
    END;
-- =========================================================================================================
-- END  --  Usp_Update_SyCreditsSummary 
-- =========================================================================================================
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_TR_Sub03_PrepareGPA]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub03_PrepareGPA]', 'P') IS NULL
EXEC sp_executesql N'-- =========================================================================================================
-- USP_TR_Sub03_PrepareGPA
-- =========================================================================================================
CREATE PROCEDURE [dbo].[USP_TR_Sub03_PrepareGPA]
    @StuEnrollIdList VARCHAR(MAX)
   ,@ShowMultipleEnrollments BIT = 0
   ,@CoursesTakenTableName NVARCHAR(200) OUTPUT
   ,@GPASummaryTableName NVARCHAR(200) OUTPUT
AS --
    BEGIN
        -- 00) Variable Definition, Create Temp Table and Set Initial Values
        BEGIN
            DECLARE @MinimunDate AS DATETIME;
            DECLARE @MaximunDate AS DATETIME;
            DECLARE @StuEnrollCampusId AS UNIQUEIDENTIFIER;

            DECLARE @GradesFormat AS VARCHAR(50);
            DECLARE @GPAMethod AS VARCHAR(50);
            DECLARE @GradeBookAt AS VARCHAR(50);
            DECLARE @RetakenPolicy AS VARCHAR(50);

            DECLARE @curStudentId AS VARCHAR(50);
            DECLARE @curStuEnrollId AS VARCHAR(50);
            DECLARE @curTermId AS UNIQUEIDENTIFIER;

            DECLARE @cumTermAverage AS DECIMAL(18, 2);

            DECLARE @cumEnroTermSimple_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumEnroTermSimple_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumEnroTermSimple_GPA AS DECIMAL(18, 2);

            DECLARE @cumEnroTermWeight_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumEnroTermWeight_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumEnroTermWeight_GPA AS DECIMAL(18, 2);

            DECLARE @cumStudTermSimple_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumStudTermSimple_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumStudTermSimple_GPA AS DECIMAL(18, 2);

            DECLARE @cumStudTermWeight_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumStudTermWeight_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumStudTermWeight_GPA AS DECIMAL(18, 2);

            DECLARE @cumEnroSimple_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumEnroSimple_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumEnroSimple_GPA AS DECIMAL(18, 2);

            DECLARE @cumEnroWeight_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumEnroWeight_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumEnroWeight_GPA AS DECIMAL(18, 2);

            DECLARE @cumStudSimple_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumStudSimple_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumStudSimple_GPA AS DECIMAL(18, 2);

            DECLARE @cumStudWeight_CourseCredits AS DECIMAL(18, 2);
            DECLARE @cumStudWeight_GPACredits AS DECIMAL(18, 2);
            DECLARE @cumStudWeight_GPA AS DECIMAL(18, 2);

            DECLARE @IsMakingSAP AS BIT;
            DECLARE @NewID AS NVARCHAR(50);
            DECLARE @SQL01 AS NVARCHAR(MAX);
            DECLARE @SQL02 AS NVARCHAR(MAX);

            DECLARE @MyEnrollments TABLE
                (
                    StuEnrollId UNIQUEIDENTIFIER
                );
            INSERT INTO @MyEnrollments
                        SELECT enrollments.StuEnrollId
                        FROM   dbo.arStuEnrollments enrollments
                        WHERE  (
                               @StuEnrollIdList IS NULL
                               OR ( enrollments.StuEnrollId IN (
                                                               SELECT zz.Val
                                                               FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1) zz
                                                               )
                                  )
                               );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#CoursesTaken'')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            CREATE TABLE #CoursesTaken
                (
                    CoursesTakenId INTEGER IDENTITY(1, 1) NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,ReqId UNIQUEIDENTIFIER
                   ,ReqCode VARCHAR(100)
                   ,ReqDescription VARCHAR(100)
                   ,ReqCodeDescrip VARCHAR(100)
                   ,ReqCredits DECIMAL(18, 2)
                   ,MinVal DECIMAL(18, 2)
                   ,ClsSectionId VARCHAR(50)
                   ,CourseStarDate DATETIME
                   ,CourseEndDate DATETIME
                   ,SCS_CreditsAttempted DECIMAL(18, 2)
                   ,SCS_CreditsEarned DECIMAL(18, 2)
                   ,SCS_CurrentScore DECIMAL(18, 2)
                   ,SCS_CurrentGrade VARCHAR(10)
                   ,SCS_FinalScore DECIMAL(18, 2)
                   ,SCS_FinalGrade VARCHAR(10)
                   ,SCS_Completed BIT
                   ,SCS_FinalGPA DECIMAL(18, 2)
                   ,SCS_Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_WeightedAverage_Credits DECIMAL(18, 2)
                   ,SCS_Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_SimpleAverage_Credits DECIMAL(18, 2)
                   ,SCS_ModUser VARCHAR(50)
                   ,SCS_ModDate DATETIME
                   ,SCS_TermGPA_Simple DECIMAL(18, 2)
                   ,SCS_TermGPA_Weighted DECIMAL(18, 2)
                   ,SCS_coursecredits DECIMAL(18, 2)
                   ,SCS_CumulativeGPA DECIMAL(18, 2)
                   ,SCS_CumulativeGPA_Simple DECIMAL(18, 2)
                   ,SCS_FACreditsEarned DECIMAL(18, 2)
                   ,SCS_Average DECIMAL(18, 2)
                   ,SCS_CumAverage DECIMAL(18, 2)
                   ,ScheduleDays DECIMAL(18, 2)
                   ,ActualDay DECIMAL(18, 2)
                   ,FinalGPA_Calculations DECIMAL(18, 2)
                   ,FinalGPA_TermCalculations DECIMAL(18, 2)
                   ,CreditsEarned_Calculations DECIMAL(18, 2)
                   ,ActualDay_Calculations DECIMAL(18, 2)
                   ,GrdBkWgtDetailsCount INTEGER
                   ,CountMultipleEnrollment INTEGER
                   ,RowNumberMultipleEnrollment INTEGER
                   ,CountRepeated INTEGER
                   ,RowNumberRetaked INTEGER
                   ,SameTermCountRetaken INTEGER
                   ,RowNumberSameTermRetaked INTEGER
                   ,RowNumberMultEnrollNoRetaken INTEGER
                   ,RowNumberOverAllByClassSection INTEGER
                   ,IsCreditsEarned BIT
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#getStudentGPAbyTerms'')
                      )
                BEGIN
                    DROP TABLE #getStudentGPAbyTerms;
                END;
            CREATE TABLE #getStudentGPAbyTerms
                (
                    StudentGPAbyTermsId INTEGER IDENTITY(1, 1) NOT NULL PRIMARY KEY
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(50)
                   ,TermStartDate DATETIME
                   ,TermEndDate DATETIME
                   ,DescripXTranscript VARCHAR(250)
                   ,TermAverage DECIMAL(18, 2)
                   ,EnroTermSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPACredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPA DECIMAL(18, 2)
                   ,EnroTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPACredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPA DECIMAL(18, 2)
                   ,StudTermSimple_CourseCredits DECIMAL(18, 2)
                   ,StudTermSimple_GPACredits DECIMAL(18, 2)
                   ,StudTermSimple_GPA DECIMAL(18, 2)
                   ,StudTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudTermWeight_GPACredits DECIMAL(18, 2)
                   ,StudTermWeight_GPA DECIMAL(18, 2)
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#getStudentGPAByEnrollment'')
                      )
                BEGIN
                    DROP TABLE #getStudentGPAByEnrollment;
                END;
            CREATE TABLE #getStudentGPAByEnrollment
                (
                    StudentGPAByEnrollmentId INTEGER IDENTITY(1, 1) NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,LastName VARCHAR(50)
                   ,FirstName VARCHAR(50)
                   ,MiddleName VARCHAR(50)
                   ,SSN VARCHAR(11)
                   ,StudentNumber VARCHAR(50)
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,EnrollmentID VARCHAR(50)
                   ,PrgVerId UNIQUEIDENTIFIER
                   ,PrgVerDescrip VARCHAR(50)
                   ,PrgVersionTrackCredits BIT
                   ,GrdSystemId UNIQUEIDENTIFIER
                   ,AcademicType VARCHAR(50)
                   ,ClockHourProgram VARCHAR(10)
                   ,IsMakingSAP BIT
                   ,TermId UNIQUEIDENTIFIER
                   --,TermDescription VARCHAR(50)
                   ,TermAverage DECIMAL(18, 2)
                   ,EnroSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroSimple_GPACredits DECIMAL(18, 2)
                   ,EnroSimple_GPA DECIMAL(18, 2)
                   ,EnroWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroWeight_GPACredits DECIMAL(18, 2)
                   ,EnroWeight_GPA DECIMAL(18, 2)
                   ,StudSimple_CourseCredits DECIMAL(18, 2)
                   ,StudSimple_GPACredits DECIMAL(18, 2)
                   ,StudSimple_GPA DECIMAL(18, 2)
                   ,StudWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudWeight_GPA_Credits DECIMAL(18, 2)
                   ,StudWeight_GPA DECIMAL(18, 2)
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#GPASummary'')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
            CREATE TABLE #GPASummary
                (
                    GPASummaryId INTEGER IDENTITY(1, 1) NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,LastName VARCHAR(50)
                   ,FirstName VARCHAR(50)
                   ,MiddleName VARCHAR(50)
                   ,SSN VARCHAR(11)
                   ,StudentNumber VARCHAR(50)
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,EnrollmentID VARCHAR(50)
                   ,PrgVerId UNIQUEIDENTIFIER
                   ,PrgVerDescrip VARCHAR(50)
                   ,PrgVersionTrackCredits BIT
                   ,GrdSystemId UNIQUEIDENTIFIER
                   ,AcademicType VARCHAR(50)
                   ,ClockHourProgram VARCHAR(10)
                   ,IsMakingSAP BIT
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,TermEndDate DATETIME
                   ,DescripXTranscript VARCHAR(250)
                   ,TermAverage DECIMAL(18, 2)
                   ,EnroTermSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPACredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPA DECIMAL(18, 2)
                   ,EnroTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPACredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPA DECIMAL(18, 2)
                   ,StudTermSimple_CourseCredits DECIMAL(18, 2)
                   ,StudTermSimple_GPACredits DECIMAL(18, 2)
                   ,StudTermSimple_GPA DECIMAL(18, 2)
                   ,StudTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudTermWeight_GPACredits DECIMAL(18, 2)
                   ,StudTermWeight_GPA DECIMAL(18, 2)
                   ,EnroSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroSimple_GPACredits DECIMAL(18, 2)
                   ,EnroSimple_GPA DECIMAL(18, 2)
                   ,EnroWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroWeight_GPACredits DECIMAL(18, 2)
                   ,EnroWeight_GPA DECIMAL(18, 2)
                   ,StudSimple_CourseCredits DECIMAL(18, 2)
                   ,StudSimple_GPACredits DECIMAL(18, 2)
                   ,StudSimple_GPA DECIMAL(18, 2)
                   ,StudWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudWeight_GPACredits DECIMAL(18, 2)
                   ,StudWeight_GPA DECIMAL(18, 2)
                   ,GradesFormat VARCHAR(50)
                   ,GPAMethod VARCHAR(50)
                   ,GradeBookAt VARCHAR(50)
                   ,RetakenPolicy VARCHAR(50)
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpEquivalentCoursesTakenByStudent'')
                      )
                BEGIN
                    DROP TABLE #tmpEquivalentCoursesTakenByStudent;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpStudentsWhoHasGradedEquivalentCourse'')
                      )
                BEGIN
                    DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourse;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpEquivalentCoursesTakenByStudentInTerm'')
                      )
                BEGIN
                    DROP TABLE #tmpEquivalentCoursesTakenByStudentInTerm;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpStudentsWhoHasGradedEquivalentCourseInTerm'')
                      )
                BEGIN
                    DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourseInTerm;
                END;

            SET @MinimunDate = ''1900-01-01'';
            SET @MaximunDate = ''2199-01-01'';
            SET @NewID = REPLACE(CONVERT(NVARCHAR(50), NEWID()), ''-'', ''_'');
            SET @CoursesTakenTableName = ''CoursesTaked_'' + @NewID;
            SET @GPASummaryTableName = ''GPASummary_'' + @NewID;

            SET @StuEnrollCampusId = COALESCE(
                                     (
                                     SELECT     TOP 1 ASE.CampusId
                                     FROM       dbo.arStuEnrollments AS ASE
                                     INNER JOIN (
                                                SELECT Val
                                                FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                ) AS LIST ON LIST.Val = ASE.EnrollmentId
                                     )
                                    ,NULL
                                             );

            SET @GradesFormat = dbo.GetAppSettingValueByKeyName(''GradesFormat'', @StuEnrollCampusId);
            IF ( @GradesFormat IS NOT NULL )
                BEGIN
                    SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
                END;
            SET @GPAMethod = dbo.GetAppSettingValueByKeyName(''GPAMethod'', @StuEnrollCampusId);
            IF ( @GPAMethod IS NOT NULL )
                BEGIN
                    SET @GPAMethod = LOWER(LTRIM(RTRIM(@GPAMethod)));
                END;
            SET @GradeBookAt = dbo.GetAppSettingValueByKeyName(''GradeBookWeightingLevel'', @StuEnrollCampusId);
            IF ( @GradeBookAt IS NOT NULL )
                BEGIN
                    SET @GradeBookAt = LOWER(LTRIM(RTRIM(@GradeBookAt)));
                END;
            SET @RetakenPolicy = (
                                 SELECT     TOP 1 SCASV.Value
                                 FROM       dbo.syConfigAppSetValues AS SCASV
                                 INNER JOIN dbo.syConfigAppSettings AS SCAS ON SCAS.SettingId = SCASV.SettingId
                                                                               AND SCAS.KeyName = ''GradeCourseRepetitionsMethod''
                                 );
        END;
        -- END  --  00) Variable Definition and Set Initial Values

        -- 01) Create Temp Table 
        BEGIN
            IF NOT EXISTS (
                          SELECT 1
                          FROM   tempdb.sys.objects AS O
                          WHERE  O.type IN ( ''U'' )
                                 AND O.name = @CoursesTakenTableName
                          )
                BEGIN
                    SET @SQL01 = '''';
                    SET @SQL01 = @SQL01 + ''CREATE TABLE tempdb.dbo.'' + @CoursesTakenTableName + '' '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''    ( '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''        CoursesTakenId INTEGER NOT NULL PRIMARY KEY '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,StudentId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,StuEnrollId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,TermId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,TermDescrip VARCHAR(100) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,TermStartDate DATETIME '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ReqId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ReqCode VARCHAR(100) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ReqDescription VARCHAR(100) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ReqCodeDescrip VARCHAR(100) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ReqCredits DECIMAL(18,2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,MinVal DECIMAL(18,2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ClsSectionId VARCHAR(50) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,CourseStarDate DATETIME '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,CourseEndDate DATETIME '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CreditsAttempted DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CreditsEarned DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CurrentScore DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CurrentGrade VARCHAR(10) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_FinalScore DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_FinalGrade VARCHAR(10) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_Completed BIT '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_FinalGPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_Product_WeightedAverage_Credits_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_Count_WeightedAverage_Credits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_Product_SimpleAverage_Credits_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_Count_SimpleAverage_Credits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_ModUser VARCHAR(50) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_ModDate DATETIME '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_TermGPA_Simple DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_TermGPA_Weighted DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_coursecredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CumulativeGPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CumulativeGPA_Simple DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_FACreditsEarned DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_Average DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SCS_CumAverage DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ScheduleDays DECIMAL(18,2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ActualDay DECIMAL(18,2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,FinalGPA_Calculations DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,FinalGPA_TermCalculations DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,CreditsEarned_Calculations DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,ActualDay_Calculations DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,GrdBkWgtDetailsCount INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,CountMultipleEnrollment INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,RowNumberMultipleEnrollment INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,CountRepeated INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,RowNumberRetaked INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,SameTermCountRetaken INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,RowNumberSameTermRetaked INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,RowNumberMultEnrollNoRetaken INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,RowNumberOverAllByClassSection INTEGER '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''       ,IsCreditsEarned BIT '' + CHAR(10);
                    SET @SQL01 = @SQL01 + ''    ) '' + CHAR(10);
                    --PRINT @SQL01;
                    EXECUTE ( @SQL01 );
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   tempdb.sys.objects AS O
                          WHERE  O.type IN ( ''U'' )
                                 AND O.name = @GPASummaryTableName
                          )
                BEGIN
                    SET @SQL02 = '''';
                    SET @SQL02 = @SQL02 + ''CREATE TABLE tempdb.dbo.'' + @GPASummaryTableName + '' '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''    ( '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''         GPASummaryId INTEGER NOT NULL PRIMARY KEY '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudentId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,LastName VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,FirstName VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,MiddleName VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,SSN VARCHAR(11) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudentNumber VARCHAR(50)'' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StuEnrollId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnrollmentID VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,PrgVerId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,PrgVerDescrip VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,PrgVersionTrackCredits BIT '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,GrdSystemId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,AcademicType NVARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,ClockHourProgram VARCHAR(10) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,IsMakingSAP BIT '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,TermId UNIQUEIDENTIFIER '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,TermDescrip VARCHAR(100) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,TermStartDate DATETIME '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,TermEndDate DATETIME '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,DescripXTranscript VARCHAR(250) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,TermAverage DECIMAL(18,2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroTermSimple_CourseCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroTermSimple_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroTermSimple_GPA DECIMAL(18, 2)				    '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroTermWeight_CoursesCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroTermWeight_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroTermWeight_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudTermSimple_CourseCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudTermSimple_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudTermSimple_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudTermWeight_CoursesCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudTermWeight_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudTermWeight_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroSimple_CourseCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroSimple_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroSimple_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroWeight_CoursesCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,EnroWeight_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''  	   ,EnroWeight_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudSimple_CourseCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudSimple_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudSimple_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudWeight_CoursesCredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudWeight_GPACredits DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,StudWeight_GPA DECIMAL(18, 2) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,GradesFormat VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,GPAMethod VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,GradeBookAt VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''        ,RetakenPolicy VARCHAR(50) '' + CHAR(10);
                    SET @SQL02 = @SQL02 + ''    ) '' + CHAR(10);
                    --PRINT @SQL02;
                    EXECUTE ( @SQL02 );
                END;
        END;
        -- END --  01) Create Temp Table

        -- 02) -- Refresh the Credit Summary Table Just in case any modifications were made
        BEGIN
            EXECUTE dbo.Usp_Update_SyCreditsSummary @StuEnrollIdList = @StuEnrollIdList;
            EXECUTE dbo.Usp_UpdateTransferCredits_SyCreditsSummary @StuEnrollIdList = @StuEnrollIdList;
        END;
        --  END  --  02) -- Refresh the Credit Summary Table Just in case any modifications were made

        -- 03) Get courses taked to informe and courses used for GPA Calculations (not multiple enrollment -same term-  and not retaked)	
        BEGIN
            INSERT INTO #getStudentGPAbyTerms
                        SELECT    DISTINCT T1.StuEnrollId
                                          ,T1.TermId
                                          ,T1.TermDescrip
                                          ,T1.StartDate
                                          ,T1.EndDate
                                          ,ISNULL(ATES.DescripXTranscript, '''') AS DescripXTranscript
                                          ,NULL
                                          ,NULL
                                          ,NULL
                                          ,NULL
                                          ,NULL
                                          ,NULL
                                          ,NULL
                                          ,NULL
                                          ,NULL
                                          ,NULL
                                          ,NULL
                                          ,NULL
                                          ,NULL
                        FROM      (
                                  SELECT     DISTINCT AR.StuEnrollId AS StuEnrollId
                                                     ,T.TermId AS TermId
                                                     ,T.TermDescrip AS TermDescrip
                                                     ,T.StartDate AS StartDate
                                                     ,T.EndDate AS EndDate
                                  FROM       dbo.arClassSections AS ACS
                                  INNER JOIN dbo.arResults AS AR ON AR.TestId = ACS.ClsSectionId
                                  INNER JOIN dbo.arTerm AS T ON T.TermId = ACS.TermId
                                  INNER JOIN dbo.arStuEnrollments E ON AR.StuEnrollId = E.StuEnrollId
                                  INNER JOIN dbo.arPrgVersions P ON P.PrgVerId = E.PrgVerId
                                  INNER JOIN @MyEnrollments ON [@MyEnrollments].StuEnrollId = AR.StuEnrollId
                                  WHERE      AR.IsCourseCompleted = 1
                                             OR P.ProgramRegistrationType = 1
                                  UNION
                                  SELECT     DISTINCT ATG.StuEnrollId
                                                     ,T.TermId
                                                     ,T.TermDescrip
                                                     ,T.StartDate
                                                     ,T.EndDate
                                  FROM       dbo.arTransferGrades AS ATG
                                  INNER JOIN dbo.arTerm AS T ON T.TermId = ATG.TermId
                                  INNER JOIN @MyEnrollments ON [@MyEnrollments].StuEnrollId = ATG.StuEnrollId
                                  ) AS T1
                        LEFT JOIN dbo.arTermEnrollSummary AS ATES ON ATES.TermId = T1.TermId
                                                                     AND ATES.StuEnrollId = T1.StuEnrollId
                        ORDER BY  T1.StartDate
                                 ,T1.TermDescrip;

            INSERT INTO #getStudentGPAByEnrollment
                        SELECT     DISTINCT AL.StudentId
                                           ,AL.LastName
                                           ,AL.FirstName
                                           ,AL.MiddleName
                                           ,AL.SSN
                                           ,AL.StudentNumber
                                           ,ASE.StuEnrollId
                                           ,ASE.EnrollmentId
                                           ,APV.PrgVerId
                                           ,APV.PrgVerDescrip
                                           ,CASE WHEN ( APV.Credits > 0.0 ) THEN 1
                                                 ELSE 0
                                            END AS PrgVersionTrackCredits
                                           ,APV.GrdSystemId
                                           ,CASE WHEN (
                                                      APV.Credits > 0.0
                                                      AND APV.Hours > 0
                                                      )
                                                      OR ( APV.IsContinuingEd = 1 ) THEN ''Credits-ClockHours''
                                                 WHEN APV.Credits > 0.0 THEN ''Credits''
                                                 WHEN APV.Hours > 0.0 THEN ''ClockHours''
                                                 ELSE ''''
                                            END AcademicType
                                           ,CASE WHEN AP.ACId = 5 THEN ''True''
                                                 ELSE ''False''
                                            END AS ClockHourProgram
                                           ,0 AS IsMakingSAP
                                           ,GSGAT.TermId
                                           --,NULL  --,GSGAT.TermDescrip
                                           ,0.0 AS TermAverage
                                           ,NULL AS EnroSimple_CourseCredits
                                           ,NULL AS EnroSimple_Credits
                                           ,NULL AS EnroSimple_GPA
                                           ,NULL AS EnroWeight_CoursesCredits
                                           ,NULL AS EnroWeight_GPA_Credits
                                           ,NULL AS EnroWeight_GPA
                                           ,NULL AS StudSimple_CourseCredits
                                           ,NULL AS StudSimple_GPA_Credits
                                           ,NULL AS StudSimple_GPA
                                           ,NULL AS StudWeight_CoursesCredits
                                           ,NULL AS StudWeight_GPA_Credits
                                           ,NULL AS StudWeight_GPA
                        FROM       adLeads AS AL --arStudent 
                        INNER JOIN arStuEnrollments ASE ON ASE.StudentId = AL.StudentId
                        INNER JOIN arPrgVersions APV ON ASE.PrgVerId = APV.PrgVerId
                        INNER JOIN arPrograms AS AP ON AP.ProgId = APV.ProgId
                        INNER JOIN #getStudentGPAbyTerms AS GSGAT ON GSGAT.StuEnrollId = ASE.StuEnrollId
                        INNER JOIN (
                                   SELECT DISTINCT CONVERT(UNIQUEIDENTIFIER, StuEnrollId) AS StuEnrollId
                                   FROM   @MyEnrollments
                                   ) AS List ON List.StuEnrollId = ASE.StuEnrollId;

            INSERT INTO #CoursesTaken
                        SELECT     ASE.StudentId
                                  ,SCS.StuEnrollId AS StuEnrollId
                                  ,SCS.TermId AS TermId
                                  ,SCS.TermDescrip AS TermDescrip
                                  ,SCS.TermStartDate AS TermStartDate
                                  ,SCS.ReqId AS ReqId
                                  ,NULL AS ReqCode
                                  ,NULL AS ReqDescription
                                  ,NULL AS ReqCodeDescrip
                                  ,NULL AS ReqCredits
                                  ,NULL AS MinVal
                                  ,SCS.ClsSectionId AS ClsSectionId
                                  ,ISNULL(ACS.StartDate, @MaximunDate) AS CourseStarDate
                                  ,ISNULL(ACS.EndDate, @MaximunDate) AS CourseEndDate
                                  ,ISNULL(SCS.CreditsAttempted, 0) AS SCS_CreditsAttempted
                                  ,ISNULL(SCS.CreditsEarned, 0) AS SCS_CreditsEarned
                                  ,SCS.CurrentScore AS SCS_CurrentScore
                                  ,SCS.CurrentGrade AS SCS_CurrentGrade
                                  ,SCS.FinalScore AS SCS_FinalScore
                                  ,SCS.FinalGrade AS SCS_FinalGrade
                                  ,SCS.Completed AS SCS_Completed
                                  ,SCS.FinalGPA AS SCS_FinalGPA
                                  ,SCS.Product_WeightedAverage_Credits_GPA
                                  ,SCS.Count_WeightedAverage_Credits
                                  ,SCS.Product_SimpleAverage_Credits_GPA
                                  ,SCS.Count_SimpleAverage_Credits
                                  ,SCS.ModUser
                                  ,SCS.ModDate
                                  ,SCS.TermGPA_Simple
                                  ,SCS.TermGPA_Weighted
                                  ,SCS.coursecredits
                                  ,SCS.CumulativeGPA
                                  ,SCS.CumulativeGPA_Simple
                                  ,SCS.FACreditsEarned
                                  ,SCS.Average
                                  ,SCS.CumAverage
                                  ,0.0 AS ScheduleDays
                                  ,0.0 AS ActualDay
                                                                                      -- For Transfer Courses  (TR) SCS.FinalGPA should be Null so it will filter for GPA Calculations
                                  ,CASE WHEN ( LOWER(@RetakenPolicy) = ''average'' ) THEN AVG(SCS.FinalGPA) OVER ( PARTITION BY SCS.StuEnrollId
                                                                                                                             ,SCS.ReqId
                                                                                                               )
                                        ELSE SCS.FinalGPA
                                   END AS FinalGPA_Calculations
                                  ,CASE WHEN ( LOWER(@RetakenPolicy) = ''average'' ) THEN AVG(SCS.FinalGPA) OVER ( PARTITION BY SCS.StuEnrollId
                                                                                                                             ,SCS.TermId
                                                                                                                             ,SCS.ReqId
                                                                                                               )
                                        ELSE SCS.FinalGPA
                                   END AS FinalGPA_TermCalculations
                                  ,0.0 AS CreditsEarned_Calculations
                                  ,0.0 AS ActualDay_Calculations
                                  ,0 AS GrdBkWgtDetailsCount
                                  ,COUNT(*) OVER ( PARTITION BY ASE.StudentId --DISTINCT SCS.StuEnrollId
                                                               ,SCS.TermId
                                                               ,SCS.ReqId
                                                 ) AS CountMultipleEnrollment
                                  ,ROW_NUMBER() OVER ( PARTITION BY ASE.StudentId
                                                                   ,SCS.TermId
                                                                   ,SCS.ReqId
                                                       ORDER BY ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                               ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                               ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                               ,SCS.StuEnrollId
                                                     ) AS RowNumberMultipleEnrollment --  same course -same termRowNumberMultipleEnrollment INTEGER
                                  ,COUNT(*) OVER ( PARTITION BY SCS.StuEnrollId
                                                               ,SCS.ReqId
                                                 ) AS CountRepeated
                                  ,CASE WHEN ( LOWER(@RetakenPolicy) = ''latest'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.TermStartDate DESC
                                                                        ,ISNULL(ACS.EndDate, @MinimunDate) DESC   -- DESC Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MinimunDate) DESC -- DESC Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC                    -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                        WHEN ( LOWER(@RetakenPolicy) = ''average'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.TermStartDate
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                        WHEN ( LOWER(@RetakenPolicy) = ''best'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.FinalGPA DESC
                                                                        ,GSD.IsPass DESC
                                                                        ,SCS.TermStartDate DESC
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                        ELSE
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId -- ''best''
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.FinalGPA DESC
                                                                        ,SCS.TermStartDate
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                   END AS RowNumberRetaked
                                  ,COUNT(*) OVER ( PARTITION BY SCS.StuEnrollId
                                                               ,SCS.TermId
                                                               ,SCS.ReqId
                                                 ) AS SameTermCountRetaken
                                  ,CASE WHEN ( LOWER(@RetakenPolicy) = ''latest'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                            ,SCS.TermId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.TermStartDate DESC
                                                                        ,ISNULL(ACS.EndDate, @MinimunDate) DESC   -- DESC Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MinimunDate) DESC -- DESC Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC                    -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                        WHEN ( LOWER(@RetakenPolicy) = ''average'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                            ,SCS.TermId
                                                                            ,SCS.ReqId
                                                                ORDER BY ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                        WHEN ( LOWER(@RetakenPolicy) = ''best'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId
                                                                            ,SCS.TermId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.FinalGPA DESC
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                        ELSE
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId -- ''best''
                                                                            ,SCS.TermId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.FinalGPA DESC
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate)
                                                                        ,SCS.ClsSectionId DESC -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                              )
                                   END AS RowNumberSameTermRetaked
                                  ,ROW_NUMBER() OVER ( PARTITION BY ASE.StudentId
                                                                   ,SCS.TermId
                                                                   ,SCS.ReqId
                                                                   ,SCS.ClsSectionId
                                                       ORDER BY ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                               ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                               ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                               ,SCS.StuEnrollId
                                                     ) AS RowNumberMultEnrollNoRetaken
                                  ,CASE WHEN ( LOWER(@RetakenPolicy) = ''latest'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY ASE.StudentId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.TermStartDate DESC
                                                                        ,ISNULL(ACS.EndDate, @MinimunDate) DESC   -- DESC Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MinimunDate) DESC -- DESC Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC                    -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                                        ,SCS.StuEnrollId
                                                              )
                                        WHEN ( LOWER(@RetakenPolicy) = ''average'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY ASE.StudentId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.TermStartDate
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                                        ,SCS.StuEnrollId
                                                              )
                                        WHEN ( LOWER(@RetakenPolicy) = ''best'' ) THEN
                                            ROW_NUMBER() OVER ( PARTITION BY ASE.StudentId
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.FinalGPA DESC
                                                                        ,SCS.TermStartDate
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                                        ,SCS.StuEnrollId
                                                              )
                                        ELSE
                                            ROW_NUMBER() OVER ( PARTITION BY SCS.StuEnrollId -- ''best''
                                                                            ,SCS.ReqId
                                                                ORDER BY SCS.FinalGPA DESC
                                                                        ,SCS.TermStartDate
                                                                        ,ISNULL(ACS.EndDate, @MaximunDate)   -- Asc Order by Date and implies nulls last
                                                                        ,ISNULL(ACS.StartDate, @MaximunDate) -- Asc Order by Date and implies nulls last
                                                                        ,SCS.ClsSectionId DESC               -- implies nulls last and do not matter the value because it is Uniqueidentifier
                                                                        ,SCS.StuEnrollId
                                                              )
                                   END AS RowNumberOverAllByClassSection
                                  ,GSD.IsCreditsEarned
                        FROM       dbo.syCreditSummary AS SCS
                        INNER JOIN dbo.arStuEnrollments AS ASE ON ASE.StuEnrollId = SCS.StuEnrollId
                        INNER JOIN (
                                   SELECT DISTINCT GSGBE.StuEnrollId AS StuEnrollId
                                   FROM   #getStudentGPAByEnrollment AS GSGBE
                                   ) AS List ON List.StuEnrollId = SCS.StuEnrollId
                        LEFT JOIN  dbo.arClassSections AS ACS ON ACS.ClsSectionId = SCS.ClsSectionId
                        INNER JOIN dbo.arPrgVersions AS PV ON PV.PrgVerId = ASE.PrgVerId
                        INNER JOIN dbo.arGradeSystems AS GS ON GS.GrdSystemId = PV.GrdSystemId
                        INNER JOIN dbo.arGradeSystemDetails AS GSD ON GSD.GrdSystemId = GS.GrdSystemId
                        WHERE      (
                                   SCS.FinalScore IS NOT NULL
                                   OR SCS.FinalGrade IS NOT NULL
                                   )
                                   AND SCS.FinalGrade = GSD.Grade
                        ORDER BY   SCS.TermStartDate
                                  ,SCS.TermDescrip
                                  ,SCS.ReqDescrip
                                  ,SCS.StuEnrollId;

        -- Update  RowNumberMultipleEnrollment for those that are retaken and multiple enrollment
        --UPDATE     CT3
        --SET        CT3.RowNumberMultEnrollNoRetaken = CT2.RowNumberMultEnrollNoRetaken
        --FROM       #CoursesTaken AS CT3
        --INNER JOIN (
        --           SELECT CT1.CoursesTakenId
        --                 ,ROW_NUMBER () OVER (PARTITION BY CT1.StudentId
        --                                                  ,CT1.TermId
        --                                                  ,CT1.ReqId
        --                                                  ,CT1.ClsSectionId
        --                                      ORDER BY CT1.StuEnrollId
        --                                              ,CT1.TermStartDate
        --                                              ,CT1.CourseEndDate
        --                                              ,CT1.CourseStarDate
        --                                     ) AS RowNumberMultEnrollNoRetaken
        --           FROM   #CoursesTaken AS CT1
        --           ) AS CT2 ON CT2.CoursesTakenId = CT3.CoursesTakenId;
        -- Update RowNumberOverAllByClassSection  TO 0 for those that are ClsSectionId IS NULL
        --UPDATE CT
        --SET    CT.RowNumberOverAllByClassSection = 0
        --FROM   #CoursesTaken AS CT
        --WHERE  CT.ClsSectionId IS NULL;
        END;
        -- END  --  03) Get courses taked to informe and courses used for GPA Calculations (not multiple enrollment -same term-  and not retaked)

        -- 04) Get GPA for StuEnrollId, TermId
        BEGIN
            DECLARE getNodes_Cursor CURSOR FAST_FORWARD FORWARD_ONLY FOR
                SELECT DISTINCT GSGBE.StudentId
                               ,GSGBE.StuEnrollId
                               ,GSGBE.TermId
                FROM   #getStudentGPAByEnrollment AS GSGBE;
            OPEN getNodes_Cursor;
            FETCH NEXT FROM getNodes_Cursor
            INTO @curStudentId
                ,@curStuEnrollId
                ,@curTermId;
            WHILE @@FETCH_STATUS = 0
                BEGIN
                    SET @cumTermAverage = (
                                          SELECT TOP 1 CT.SCS_Average
                                          FROM   #CoursesTaken AS CT
                                          WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                 AND CT.TermId IN ( @curTermId )
                                                 AND (
                                                     CT.RowNumberMultEnrollNoRetaken = 1
                                                     OR CT.RowNumberSameTermRetaked = 1
                                                     )
                                          );
                    SET @cumEnroTermSimple_CourseCredits = (
                                                           SELECT COUNT(CT.SCS_coursecredits)
                                                           FROM   #CoursesTaken AS CT
                                                           WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                                  AND CT.TermId IN ( @curTermId )
                                                                  AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                                  AND ( CT.RowNumberSameTermRetaked = 1
                                                                      --OR CT.RowNumberMultipleEnrollment = 1
                                                                      )
                                                           );
                    SET @cumEnroTermSimple_GPACredits = (
                                                        SELECT SUM(CT.FinalGPA_TermCalculations)
                                                        FROM   #CoursesTaken AS CT
                                                        WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                               AND CT.TermId IN ( @curTermId )
                                                               AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                               AND ( CT.RowNumberSameTermRetaked = 1
                                                                   --OR CT.RowNumberMultipleEnrollment = 1
                                                                   )
                                                        );
                    SET @cumEnroTermWeight_CourseCredits = (
                                                           SELECT SUM(CT.SCS_coursecredits)
                                                           FROM   #CoursesTaken AS CT
                                                           WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                                  AND CT.TermId IN ( @curTermId )
                                                                  AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                                  AND ( CT.RowNumberSameTermRetaked = 1
                                                                      --OR CT.RowNumberMultipleEnrollment = 1
                                                                      )
                                                           );
                    SET @cumEnroTermWeight_GPACredits = (
                                                        SELECT SUM(CT.SCS_coursecredits * CT.FinalGPA_TermCalculations)
                                                        FROM   #CoursesTaken AS CT
                                                        WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                               AND CT.TermId IN ( @curTermId )
                                                               AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                               AND ( CT.RowNumberSameTermRetaked = 1
                                                                   --OR CT.RowNumberMultipleEnrollment = 1
                                                                   )
                                                        );
                    SET @cumStudTermSimple_CourseCredits = (
                                                           SELECT COUNT(CT.SCS_coursecredits)
                                                           FROM   #CoursesTaken AS CT
                                                           WHERE  CT.StudentId IN ( @curStudentId )
                                                                  AND CT.TermId IN ( @curTermId )
                                                                  AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                                  AND ( CT.RowNumberSameTermRetaked = 1
                                                                      --OR CT.RowNumberMultipleEnrollment = 1
                                                                      )
                                                           );
                    SET @cumStudTermSimple_GPACredits = (
                                                        SELECT SUM(CT.FinalGPA_TermCalculations)
                                                        FROM   #CoursesTaken AS CT
                                                        WHERE  CT.StudentId IN ( @curStudentId )
                                                               AND CT.TermId IN ( @curTermId )
                                                               AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                               AND ( CT.RowNumberSameTermRetaked = 1
                                                                   --OR CT.RowNumberMultipleEnrollment = 1
                                                                   )
                                                        );
                    SET @cumStudTermWeight_CourseCredits = (
                                                           SELECT SUM(CT.SCS_coursecredits)
                                                           FROM   #CoursesTaken AS CT
                                                           WHERE  CT.StudentId IN ( @curStudentId )
                                                                  AND CT.TermId IN ( @curTermId )
                                                                  AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                                  AND ( CT.RowNumberSameTermRetaked = 1
                                                                      --OR CT.RowNumberMultipleEnrollment = 1
                                                                      )
                                                           );
                    SET @cumStudTermWeight_GPACredits = (
                                                        SELECT SUM(CT.SCS_coursecredits * CT.FinalGPA_TermCalculations)
                                                        FROM   #CoursesTaken AS CT
                                                        WHERE  CT.StudentId IN ( @curStudentId )
                                                               AND CT.TermId IN ( @curTermId )
                                                               AND CT.FinalGPA_TermCalculations IS NOT NULL
                                                               AND ( CT.RowNumberSameTermRetaked = 1
                                                                   --OR CT.RowNumberMultipleEnrollment = 1
                                                                   )
                                                        );

                    IF @cumEnroTermSimple_CourseCredits > 0
                        BEGIN
                            SET @cumEnroTermSimple_GPA = @cumEnroTermSimple_GPACredits / @cumEnroTermSimple_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumEnroTermSimple_GPA = 0.0;
                        END;
                    IF @cumEnroTermWeight_CourseCredits > 0.0
                        BEGIN
                            SET @cumEnroTermWeight_GPA = @cumEnroTermWeight_GPACredits / @cumEnroTermWeight_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumEnroTermWeight_GPA = 0.0;
                        END;
                    IF @cumStudTermSimple_CourseCredits > 0.0
                        BEGIN
                            SET @cumStudTermSimple_GPA = @cumStudTermSimple_GPACredits / @cumStudTermSimple_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumStudTermSimple_GPA = 0.0;
                        END;
                    IF @cumStudTermWeight_CourseCredits > 0.0
                        BEGIN
                            SET @cumStudTermWeight_GPA = @cumStudTermWeight_GPACredits / @cumStudTermWeight_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumStudTermWeight_GPA = 0.0;
                        END;

                    UPDATE GSGAT
                    SET    GSGAT.TermAverage = @cumTermAverage
                          ,GSGAT.EnroTermSimple_CourseCredits = @cumEnroTermSimple_CourseCredits
                          ,GSGAT.EnroTermSimple_GPACredits = @cumEnroTermSimple_GPACredits
                          ,GSGAT.EnroTermSimple_GPA = @cumEnroTermSimple_GPA
                          ,GSGAT.EnroTermWeight_CoursesCredits = @cumEnroTermWeight_CourseCredits
                          ,GSGAT.EnroTermWeight_GPACredits = @cumEnroTermWeight_GPACredits
                          ,GSGAT.EnroTermWeight_GPA = @cumEnroTermWeight_GPA
                          ,GSGAT.StudTermSimple_CourseCredits = @cumStudTermSimple_CourseCredits
                          ,GSGAT.StudTermSimple_GPACredits = @cumStudTermSimple_GPACredits
                          ,GSGAT.StudTermSimple_GPA = @cumStudTermSimple_GPA
                          ,GSGAT.StudTermWeight_CoursesCredits = @cumStudTermWeight_CourseCredits
                          ,GSGAT.StudTermWeight_GPACredits = @cumStudTermWeight_GPACredits
                          ,GSGAT.StudTermWeight_GPA = @cumStudTermWeight_GPA
                    FROM   #getStudentGPAbyTerms AS GSGAT
                    WHERE  GSGAT.StuEnrollId = @curStuEnrollId
                           AND GSGAT.TermId = @curTermId;

                    FETCH NEXT FROM getNodes_Cursor
                    INTO @curStudentId
                        ,@curStuEnrollId
                        ,@curTermId;
                END;
            CLOSE getNodes_Cursor;
            DEALLOCATE getNodes_Cursor;
        END;
        -- END  --  04)Get GPA for StuEnrollId, TermId

        -- 05) Get GPA for StudentId and StuEnrollId
        BEGIN
            DECLARE getNodes_Cursor CURSOR FAST_FORWARD FORWARD_ONLY FOR
                SELECT DISTINCT GSGBE.StudentId
                               ,GSGBE.StuEnrollId
                FROM   #getStudentGPAByEnrollment AS GSGBE;
            OPEN getNodes_Cursor;
            FETCH NEXT FROM getNodes_Cursor
            INTO @curStudentId
                ,@curStuEnrollId;
            WHILE @@FETCH_STATUS = 0
                BEGIN

                    SET @cumEnroSimple_CourseCredits = (
                                                       SELECT COUNT(CT.SCS_coursecredits)
                                                       FROM   #CoursesTaken AS CT
                                                       WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                              AND CT.FinalGPA_Calculations IS NOT NULL
                                                              AND CT.RowNumberRetaked = 1
                                                       );
                    SET @cumEnroSimple_GPACredits = (
                                                    SELECT SUM(CT.FinalGPA_Calculations)
                                                    FROM   #CoursesTaken AS CT
                                                    WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                           AND CT.FinalGPA_Calculations IS NOT NULL
                                                           AND CT.RowNumberRetaked = 1
                                                    );
                    SET @cumEnroWeight_CourseCredits = (
                                                       SELECT SUM(CT.SCS_coursecredits)
                                                       FROM   #CoursesTaken AS CT
                                                       WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                              AND CT.FinalGPA_Calculations IS NOT NULL
                                                              AND CT.RowNumberRetaked = 1
                                                       );
                    SET @cumEnroWeight_GPACredits = (
                                                    SELECT SUM(CT.SCS_coursecredits * CT.FinalGPA_Calculations)
                                                    FROM   #CoursesTaken AS CT
                                                    WHERE  CT.StuEnrollId IN ( @curStuEnrollId )
                                                           AND CT.FinalGPA_Calculations IS NOT NULL
                                                           AND CT.RowNumberRetaked = 1
                                                    );
                    SET @cumStudSimple_CourseCredits = (
                                                       SELECT COUNT(CT.SCS_coursecredits)
                                                       FROM   #CoursesTaken AS CT
                                                       WHERE  CT.StudentId IN ( @curStudentId )
                                                              AND CT.FinalGPA_Calculations IS NOT NULL
                                                              AND CT.RowNumberRetaked = 1
                                                              AND RowNumberOverAllByClassSection = 1
                                                       );
                    SET @cumStudSimple_GPACredits = (
                                                    SELECT SUM(CT.FinalGPA_Calculations)
                                                    FROM   #CoursesTaken AS CT
                                                    WHERE  CT.StudentId IN ( @curStudentId )
                                                           AND CT.FinalGPA_Calculations IS NOT NULL
                                                           AND CT.RowNumberRetaked = 1
                                                           AND RowNumberOverAllByClassSection = 1
                                                    );
                    SET @cumStudWeight_CourseCredits = (
                                                       SELECT SUM(CT.SCS_coursecredits)
                                                       FROM   #CoursesTaken AS CT
                                                       WHERE  CT.StudentId IN ( @curStudentId )
                                                              AND CT.FinalGPA_Calculations IS NOT NULL
                                                              AND CT.RowNumberRetaked = 1
                                                              AND RowNumberOverAllByClassSection = 1
                                                       );
                    SET @cumStudWeight_GPACredits = (
                                                    SELECT SUM(CT.SCS_coursecredits * CT.FinalGPA_Calculations)
                                                    FROM   #CoursesTaken AS CT
                                                    WHERE  CT.StudentId IN ( @curStudentId )
                                                           AND CT.FinalGPA_Calculations IS NOT NULL
                                                           AND RowNumberOverAllByClassSection = 1
                                                           AND CT.RowNumberRetaked = 1
                                                    );

                    IF @cumEnroSimple_CourseCredits > 0.0
                        BEGIN
                            SET @cumEnroSimple_GPA = @cumEnroSimple_GPACredits / @cumEnroSimple_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumEnroSimple_GPA = 0.0;
                        END;
                    IF @cumStudSimple_CourseCredits > 0.0
                        BEGIN
                            SET @cumStudSimple_GPA = @cumStudSimple_GPACredits / @cumStudSimple_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumStudSimple_GPA = 0.0;
                        END;
                    IF @cumEnroWeight_CourseCredits > 0.0
                        BEGIN
                            SET @cumEnroWeight_GPA = @cumEnroWeight_GPACredits / @cumEnroWeight_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumEnroWeight_GPA = 0.0;
                        END;
                    IF @cumStudWeight_CourseCredits > 0.0
                        BEGIN
                            SET @cumStudWeight_GPA = @cumStudWeight_GPACredits / @cumStudWeight_CourseCredits;
                        END;
                    ELSE
                        BEGIN
                            SET @cumStudWeight_CourseCredits = 0.0;
                        END;
                    --********************* Making SAP *******************************************************
                    SELECT @IsMakingSAP = ASCR1.IsMakingSAP
                    FROM   (
                           SELECT ROW_NUMBER() OVER ( PARTITION BY ASCR.StuEnrollId
                                                      ORDER BY ASCR.DatePerformed DESC
                                                    ) AS N
                                 ,ISNULL(ASCR.IsMakingSAP, 0) AS IsMakingSAP
                           FROM   arSAPChkResults AS ASCR
                           WHERE  ASCR.StuEnrollId = @curStuEnrollId
                           ) AS ASCR1
                    WHERE  ASCR1.N = 1;
                    --********************* Update #getStudentGPAByEnrollment *******************************************************
                    UPDATE GSGBE
                    SET    GSGBE.IsMakingSAP = ISNULL(@IsMakingSAP, 0)
                          ,GSGBE.EnroSimple_CourseCredits = @cumEnroSimple_CourseCredits
                          ,GSGBE.EnroSimple_GPACredits = @cumEnroSimple_GPACredits
                          ,GSGBE.EnroSimple_GPA = @cumEnroSimple_GPA
                          ,GSGBE.EnroWeight_CoursesCredits = @cumEnroWeight_CourseCredits
                          ,GSGBE.EnroWeight_GPACredits = @cumEnroWeight_GPACredits
                          ,GSGBE.EnroWeight_GPA = @cumEnroWeight_GPA
                          ,GSGBE.StudSimple_CourseCredits = @cumStudSimple_CourseCredits
                          ,GSGBE.StudSimple_GPACredits = @cumStudSimple_GPACredits
                          ,GSGBE.StudSimple_GPA = @cumStudSimple_GPA
                          ,GSGBE.StudWeight_CoursesCredits = @cumStudWeight_CourseCredits
                          ,GSGBE.StudWeight_GPA_Credits = @cumStudWeight_GPACredits
                          ,GSGBE.StudWeight_GPA = @cumStudWeight_GPA
                    FROM   #getStudentGPAByEnrollment AS GSGBE
                    WHERE  GSGBE.StuEnrollId = @curStuEnrollId;

                    FETCH NEXT FROM getNodes_Cursor
                    INTO @curStudentId
                        ,@curStuEnrollId;
                END;
            CLOSE getNodes_Cursor;
            DEALLOCATE getNodes_Cursor;
        END;
        -- END  -- 05) Get GPA for StudentId and StuEnrollId

        -- 06) Get ScheduleDays and ActualDay for each course taked (coursed and tranferred)
        BEGIN
            UPDATE     CT
            SET        CT.ReqCode = AR.Code
                      ,CT.ReqDescription = AR.Descrip
                      ,CT.ReqCodeDescrip = ''('' + AR.Code + '' ) '' + AR.Descrip
                      ,CT.ReqCredits = AR.Credits
                      ,CT.CreditsEarned_Calculations = ( CASE WHEN (
                                                                   CT.SCS_CreditsEarned > 0
                                                                   AND CT.RowNumberRetaked <> 1
                                                                   ) THEN 0.0
                                                              ELSE CT.SCS_CreditsEarned
                                                         END
                                                       )
                      ,CT.ScheduleDays = AR.Hours
                      ,CT.ActualDay = ( CASE WHEN ( CT.IsCreditsEarned = 0 ) THEN 0.0
                                             ELSE AR.Hours
                                        END
                                      )
                      ,CT.ActualDay_Calculations = ( CASE WHEN (
                                                               CT.IsCreditsEarned = 1
                                                               AND CT.RowNumberRetaked <> 1
                                                               ) THEN 0.0
                                                          WHEN ( CT.IsCreditsEarned = 0 ) THEN 0.0
                                                          ELSE AR.Hours
                                                     END
                                                   )
                      ,CT.MinVal = (
                                   SELECT     MIN(GCD.MinVal)
                                   FROM       dbo.arGradeScaleDetails GCD
                                   INNER JOIN dbo.arGradeSystemDetails GSD ON GSD.GrdSysDetailId = GCD.GrdSysDetailId
                                   WHERE      GSD.IsPass = 1
                                              AND GCD.GrdScaleId = ACS.GrdScaleId
                                   )
                      ,CT.GrdBkWgtDetailsCount = (
                                                 SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                                 FROM   dbo.arGrdBkResults AS AGBR
                                                 WHERE  AGBR.StuEnrollId = CT.StuEnrollId
                                                        AND AGBR.ClsSectionId = CT.ClsSectionId
                                                 )
            FROM       #CoursesTaken AS CT
            INNER JOIN #getStudentGPAByEnrollment GSGBE ON GSGBE.StuEnrollId = CT.StuEnrollId
                                                           AND GSGBE.TermId = CT.TermId
            INNER JOIN dbo.arClassSections AS ACS ON ACS.ClsSectionId = CT.ClsSectionId
                                                     AND ACS.ReqId = CT.ReqId
            INNER JOIN dbo.arReqs AS AR ON AR.ReqId = CT.ReqId;

            UPDATE     CT
            SET        CT.ReqCode = AR.Code
                      ,CT.ReqDescription = AR.Descrip
                      ,CT.ReqCodeDescrip = ''('' + AR.Code + '' ) '' + AR.Descrip
                      ,CT.ReqCredits = AR.Credits
                      ,CT.CreditsEarned_Calculations = ( CASE WHEN (
                                                                   CT.SCS_CreditsEarned > 0
                                                                   AND CT.RowNumberRetaked <> 1
                                                                   ) THEN 0.0
                                                              ELSE CT.SCS_CreditsEarned
                                                         END
                                                       )
                      ,CT.ScheduleDays = AR.Hours
                      ,CT.ActualDay = ( CASE WHEN ( CT.IsCreditsEarned = 0 ) THEN 0.0
                                             ELSE AR.Hours
                                        END
                                      )
                      ,CT.ActualDay_Calculations = ( CASE WHEN (
                                                               CT.IsCreditsEarned = 1
                                                               AND CT.RowNumberRetaked <> 1
                                                               ) THEN 0.0
                                                          WHEN ( CT.IsCreditsEarned = 0 ) THEN 0.0
                                                          ELSE AR.Hours
                                                     END
                                                   )
                      ,CT.MinVal = (
                                   SELECT     MIN(GCD.MinVal)
                                   FROM       dbo.arGradeScaleDetails GCD
                                   INNER JOIN dbo.arGradeSystemDetails GSD ON GSD.GrdSysDetailId = GCD.GrdSysDetailId
                                   WHERE      GSD.IsPass = 1
                                   )
                      ,CT.GrdBkWgtDetailsCount = (
                                                 SELECT COUNT(*) AS GrdBkWgtDetailsCount
                                                 FROM   dbo.arGrdBkResults AS AGBR
                                                 WHERE  AGBR.StuEnrollId = CT.StuEnrollId
                                                        AND AGBR.ClsSectionId = CT.ClsSectionId
                                                 )
            FROM       #CoursesTaken AS CT
            INNER JOIN #getStudentGPAByEnrollment GSGBE ON GSGBE.StuEnrollId = CT.StuEnrollId
                                                           AND GSGBE.TermId = CT.TermId
            INNER JOIN dbo.arTransferGrades AS ATG ON ATG.StuEnrollId = CT.StuEnrollId
                                                      AND ATG.TermId = CT.TermId
                                                      AND ATG.ReqId = CT.ReqId
            INNER JOIN dbo.arReqs AS AR ON AR.ReqId = CT.ReqId;

        END;
        -- END  --  06) Get ScheduleDays and ActualDay for each course taked (coursed and tranferred)

        -- 07) Summary update
        BEGIN
            INSERT INTO #GPASummary
                        SELECT     GSGBE.StudentId
                                  ,GSGBE.LastName
                                  ,GSGBE.FirstName
                                  ,GSGBE.MiddleName
                                  ,GSGBE.SSN
                                  ,GSGBE.StudentNumber
                                  ,GSGBE.StuEnrollId
                                  ,GSGBE.EnrollmentID
                                  ,GSGBE.PrgVerId
                                  ,GSGBE.PrgVerDescrip
                                  ,GSGBE.PrgVersionTrackCredits
                                  ,GSGBE.GrdSystemId
                                  ,GSGBE.AcademicType AS AcademicType
                                  ,GSGBE.ClockHourProgram AS ClockHourProgram
                                  ,GSGBE.IsMakingSAP AS IsMakingSAP
                                  ,GSGAT.TermId
                                  ,GSGAT.TermDescrip
                                  ,GSGAT.TermStartDate
                                  ,GSGAT.TermEndDate
                                  ,GSGAT.DescripXTranscript
                                  ,GSGAT.TermAverage
                                  ,GSGAT.EnroTermSimple_CourseCredits
                                  ,GSGAT.EnroTermSimple_GPACredits
                                  ,GSGAT.EnroTermSimple_GPA
                                  ,GSGAT.EnroTermWeight_CoursesCredits
                                  ,GSGAT.EnroTermWeight_GPACredits
                                  ,GSGAT.EnroTermWeight_GPA
                                  ,GSGAT.StudTermSimple_CourseCredits
                                  ,GSGAT.StudTermSimple_GPACredits
                                  ,GSGAT.StudTermSimple_GPA
                                  ,GSGAT.StudTermWeight_CoursesCredits
                                  ,GSGAT.StudTermWeight_GPACredits
                                  ,GSGAT.StudTermWeight_GPA
                                  ,GSGBE.EnroSimple_CourseCredits
                                  ,GSGBE.EnroSimple_GPACredits
                                  ,GSGBE.EnroSimple_GPA
                                  ,GSGBE.EnroWeight_CoursesCredits
                                  ,GSGBE.EnroWeight_GPACredits
                                  ,GSGBE.EnroWeight_GPA
                                  ,GSGBE.StudSimple_CourseCredits
                                  ,GSGBE.StudSimple_GPACredits
                                  ,GSGBE.StudSimple_GPA
                                  ,GSGBE.StudWeight_CoursesCredits
                                  ,GSGBE.StudWeight_GPA_Credits
                                  ,GSGBE.StudWeight_GPA
                                  ,@GradesFormat AS GradesFormat
                                  ,@GPAMethod AS GPAMethod
                                  ,@GradeBookAt AS GradeBookAt
                                  ,@RetakenPolicy AS RetakenPolicy
                        FROM       #getStudentGPAbyTerms AS GSGAT
                        INNER JOIN #getStudentGPAByEnrollment GSGBE ON GSGBE.StuEnrollId = GSGAT.StuEnrollId
                                                                       AND GSGAT.TermId = GSGBE.TermId
                        ORDER BY   GSGBE.StudentId
                                  ,GSGBE.StuEnrollId
                                  ,GSGAT.TermStartDate
                                  ,GSGAT.TermDescrip
                                  ,GSGAT.TermId;

        END;
        -- END  --  07) Summary update and 

        -- to Test
        --SELECT * FROM #getStudentGPAbyTerms AS GSGAT ORDER BY  GSGAT.StuEnrollId, GSGAT.TermId
        --SELECT * FROM #getStudentGPAByEnrollment AS GSGBE
        --SELECT * FROM #CoursesTaken AS CT ORDER BY CT.StudentId, CT.StuEnrollId, CT.TermId
        --SELECT * FROM #GPASummary AS GS ORDER BY GS.StudentId, GS.StuEnrollId, GS.TermId

        -- 08) Save saving prepared GPA Information
        BEGIN
            SET @SQL01 = '''';
            SET @SQL01 = @SQL01 + ''INSERT INTO tempdb.dbo.'' + @CoursesTakenTableName + '' '' + CHAR(10);
            SET @SQL01 = @SQL01 + ''SELECT CT.* '' + CHAR(10);
            SET @SQL01 = @SQL01 + ''FROM  #CoursesTaken AS CT '' + CHAR(10);
            --PRINT @SQL01;
            EXECUTE ( @SQL01 );

            SET @SQL02 = '''';
            SET @SQL02 = @SQL02 + ''INSERT INTO tempdb.dbo.'' + @GPASummaryTableName + '' '' + CHAR(10);
            SET @SQL02 = @SQL02 + ''       SELECT GS.*     '' + CHAR(10);
            SET @SQL02 = @SQL02 + ''       FROM  #GPASummary AS GS '' + CHAR(10);
            --PRINT @SQL02;
            EXECUTE ( @SQL02 );
        END;
        -- END  --08) Save saving prepared GPA Information

        --09) DropTempTables
        BEGIN
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#CoursesTaken'')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#getStudentGPAbyTerms'')
                      )
                BEGIN
                    DROP TABLE #getStudentGPAbyTerms;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#getStudentGPAByEnrollment'')
                      )
                BEGIN
                    DROP TABLE #getStudentGPAByEnrollment;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#GPASummary'')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpEquivalentCoursesTakenByStudent'')
                      )
                BEGIN
                    DROP TABLE #tmpEquivalentCoursesTakenByStudent;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpStudentsWhoHasGradedEquivalentCourse'')
                      )
                BEGIN
                    DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourse;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpEquivalentCoursesTakenByStudentInTerm'')
                      )
                BEGIN
                    DROP TABLE #tmpEquivalentCoursesTakenByStudentInTerm;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#tmpStudentsWhoHasGradedEquivalentCourseInTerm'')
                      )
                BEGIN
                    DROP TABLE #tmpStudentsWhoHasGradedEquivalentCourseInTerm;
                END;
        END;
    -- END  --  09) DropTempTables

    -- 10) show tables names
    --BEGIN
    --    SELECT @CoursesTakenTableName AS CoursesTakenTableName
    --          ,@GPASummaryTableName AS GPASummaryTableName;
    --END;
    -- END  --  10) show tables names
    END;
-- =========================================================================================================
-- END  --  USP_TR_Sub03_PrepareGPA
-- =========================================================================================================

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
GO
SET QUOTED_IDENTIFIER ON
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		FAME Inc.
-- Create date: 6/11/2019
-- Description:	Calculated Student GPA Based on a set of parameters - Numeric ( Weighted & Unweighted currently implemented)
--Referenced in :
--[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents] -> via db function CalculateStudentAverage
--[Usp_PR_Sub3_Terms_forAllEnrolmnents] -> via db function CalculateStudentAverage
--ANY CHANGES TO THIS FILE MUST BE REFLECTED IN SP GPA_Calculator
-- =============================================
ALTER FUNCTION [dbo].[CalculateStudentAverage]
    (
        @EnrollmentId VARCHAR(50)
       ,@BeginDate DATETIME = NULL
       ,@EndDate DATETIME = NULL
       ,@ClassId UNIQUEIDENTIFIER = NULL
       ,@TermId UNIQUEIDENTIFIER = NULL
    )
RETURNS DECIMAL(16, 2)
AS
    BEGIN
        DECLARE @GPAResult AS DECIMAL(16, 2);
        DECLARE @UseWeightForGPA BIT = 1;

        IF ( @EnrollmentId IS NULL )
            RETURN @GPAResult;

        SET @UseWeightForGPA = (
                               SELECT TOP 1 PV.DoCourseWeightOverallGPA
                               FROM   dbo.arStuEnrollments E
                               JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                               WHERE  E.StuEnrollId = @EnrollmentId
                               );

        -----------------Numeric GPA Grades Format------------------------

        SET @GPAResult = ISNULL(
                         (
                         SELECT ( CASE WHEN @UseWeightForGPA = 1 AND SUM(b.CourseWeight) > 0 THEN
                                           ROUND(( SUM(b.CourseWeight * b.WeightedCourseAverage / 100) / SUM(b.CourseWeight)) * 100, 2)
                                       ELSE AVG(b.UnweightedCourseAverage)
                                  END
                                ) AS WeightedGPA
                         FROM   (
                                SELECT   SUM(OurSingleClassGradeFactor) AS CourseFactor
                                        ,SUM(a.GradeWeight) AS GradeWeight
                                        ,SUM(a.Score) AS SumOfScores
										,CASE WHEN SUM(a.GradeWeight) > 0 THEN ( SUM(OurSingleClassGradeFactor) / SUM(a.GradeWeight)) * 100
                                                     ELSE null
                                                END AS WeightedCourseAverage
                                        ,( AVG(a.Score)) AS UnweightedCourseAverage
                                        ,ClsSectionId
                                        ,a.CourseWeight
                                FROM     (
                                         SELECT ( c.Weight * a.Score / 100 ) AS OurSingleClassGradeFactor
                                               ,c.Weight AS GradeWeight
                                               ,a.Score
                                               ,a.ClsSectionId
                                               ,CASE WHEN d.CourseWeight = 0
                                                          AND NOT EXISTS (
                                                                         SELECT 1
                                                                         FROM   dbo.arProgVerDef CPVC
                                                                         WHERE  CPVC.CourseWeight > 0
                                                                                AND CPVC.PrgVerId = d.PrgVerId
                                                                         )
                                                          AND PV.DoCourseWeightOverallGPA = 0 THEN 100 / (
                                                                                                         SELECT COUNT(*)
                                                                                                         FROM   dbo.arGrdBkResults Cn
                                                                                                         WHERE  Cn.StuEnrollId = a.StuEnrollId
                                                                                                                AND Cn.ClsSectionId = a.ClsSectionId
                                                                                                         )
                                                     ELSE d.CourseWeight
                                                END AS courseweight
                                               ,c.Descrip AS ClassDescrip
                                         FROM   (
                                                SELECT   a.StuEnrollId
                                                        ,a.ClsSectionId
                                                        ,a.InstrGrdBkWgtDetailId
                                                        ,AVG(Score) AS Score
                                                FROM     dbo.arGrdBkResults a
                                                JOIN     dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                                JOIN     dbo.arGrdComponentTypes f ON f.GrdComponentTypeId = c.GrdComponentTypeId
                                                JOIN     dbo.arClassSections ON arClassSections.ClsSectionId = a.ClsSectionId
                                                WHERE    a.StuEnrollId = @EnrollmentId
                                                         --AND a.IsCompGraded = 1
                                                         AND a.Score >= 0
                                                         AND a.Score IS NOT NULL
                                                         AND a.PostDate IS NOT NULL
                                                         AND (
                                                             @EndDate IS NULL
                                                             OR ( a.PostDate <= @EndDate )
                                                             )
                                                         AND (
                                                             @ClassId IS NULL
                                                             OR ( @ClassId = a.ClsSectionId )
                                                             )
                                                         AND (
                                                             @TermId IS NULL
                                                             OR ( @TermId = TermId )
                                                             )
                                                GROUP BY StuEnrollId
                                                        ,a.ClsSectionId
                                                        ,a.InstrGrdBkWgtDetailId
                                                ) a -- students grades
                                         JOIN   dbo.arClassSections b ON b.ClsSectionId = a.ClsSectionId
                                         JOIN   dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                         JOIN   dbo.arProgVerDef d ON d.ReqId = b.ReqId
                                         JOIN   dbo.arStuEnrollments E ON E.StuEnrollId = a.StuEnrollId
                                         JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId                                       
                                         ) a
                                WHERE    a.CourseWeight > 0
                                GROUP BY ClsSectionId
                                        ,a.CourseWeight


                                --ORDER BY CourseGPA DESC
                                ) b
                         )
                        ,0.00
                               );
        --END;
        RETURN @GPAResult;
    END;