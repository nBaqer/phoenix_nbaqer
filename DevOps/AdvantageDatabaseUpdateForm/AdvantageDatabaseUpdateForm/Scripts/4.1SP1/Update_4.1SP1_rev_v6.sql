﻿/*
Run this script on:

        dev2.internal.fameinc.com\A1.Prod_AMC_310_To_400    -  This database will be modified

to synchronize it with:

        dev-com-db1\Adv.Tricoci

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.7.10021 from Red Gate Software Ltd at 7/23/2019 3:56:54 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[UDF_GetEnrollmentStatusAtGivenDate]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UDF_GetEnrollmentStatusAtGivenDate]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[UDF_GetEnrollmentStatusAtGivenDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[UDF_GetEnrollmentStatusAtGivenDate]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UDF_GetEnrollmentStatusAtGivenDate]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Function to return the status of an enrollment at a specified date
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[UDF_GetEnrollmentStatusAtGivenDate]
(
    @StuEnrollId UNIQUEIDENTIFIER,
    @ReportDate DATETIME
)
RETURNS VARCHAR(50)
AS
BEGIN
    DECLARE @ReturnValue VARCHAR(50);
    DECLARE @CurrentStatus VARCHAR(50);
    DECLARE @StartDate DATETIME;
    DECLARE @LastDateOfChange DATETIME;
    DECLARE @LastNewStatus VARCHAR(50);

    SET @ReturnValue = '''';

    SELECT @CurrentStatus = sc.StatusCodeDescrip,
           @StartDate = se.StartDate
    FROM dbo.arStuEnrollments se
        INNER JOIN dbo.syStatusCodes sc
            ON sc.StatusCodeId = se.StatusCodeId
    WHERE se.StuEnrollId = @StuEnrollId;

    SET @LastDateOfChange =
    (
        SELECT MAX(DateOfChange)
        FROM dbo.syStudentStatusChanges
        WHERE StuEnrollId = @StuEnrollId
    );

    SET @LastNewStatus =
    (
        SELECT TOP 1
               sc.StatusCodeDescrip
        FROM dbo.syStudentStatusChanges ssc
            INNER JOIN dbo.syStatusCodes sc
                ON sc.StatusCodeId = ssc.NewStatusId
        WHERE ssc.StuEnrollId = @StuEnrollId
        ORDER BY ssc.DateOfChange DESC,
                 ssc.ModDate DESC
    );


    --If there are no status changes for the enrollment and the given date is >= enrollment start date we can simply return the current status. 
    IF NOT EXISTS
    (
        SELECT 1
        FROM dbo.syStudentStatusChanges
        WHERE StuEnrollId = @StuEnrollId
    )
    BEGIN
        IF @ReportDate >= @StartDate
        BEGIN
            SET @ReturnValue = @CurrentStatus;
        END;
    END;
    ELSE
    BEGIN
        --If the report date > last date of change for the enrollment then we can simply return the last new status (should be same as current status)
        IF @ReportDate > @LastDateOfChange
           AND @ReportDate >= @StartDate
        BEGIN
            SET @ReturnValue = @LastNewStatus;
        END;
        ELSE
        BEGIN
            --If the report date is the same as a date of change then we can simply return the new status for that date of change.
            --Use the FORMAT function just in case the DateOfChange field in the database has a time component on it such as 2018-10-19 05:00:00.000
            IF EXISTS
            (
                SELECT 1
                FROM dbo.syStudentStatusChanges
                WHERE StuEnrollId = @StuEnrollId
                      AND CONVERT(VARCHAR, DateOfChange, 101) = CONVERT(VARCHAR, @ReportDate, 101)
            )
            BEGIN
                SET @ReturnValue =
                (
                    SELECT TOP 1
                           sc.StatusCodeDescrip
                    FROM dbo.syStudentStatusChanges ssc
                        INNER JOIN dbo.syStatusCodes sc
                            ON sc.StatusCodeId = ssc.NewStatusId
                    WHERE ssc.StuEnrollId = @StuEnrollId
                          AND CONVERT(VARCHAR, DateOfChange, 101) = CONVERT(VARCHAR, @ReportDate, 101)
                    ORDER BY ssc.DateOfChange DESC,
                             ssc.ModDate DESC
                );

            END;
            ELSE
            BEGIN
                --At this point we can get the last change record with DateOfChange that is less than the report date
                SET @ReturnValue =
                (
                    SELECT TOP 1
                           sc.StatusCodeDescrip
                    FROM dbo.syStudentStatusChanges ssc
                        INNER JOIN dbo.syStatusCodes sc
                            ON sc.StatusCodeId = ssc.NewStatusId
                    WHERE ssc.StuEnrollId = @StuEnrollId
                          AND ssc.DateOfChange < @ReportDate
                    ORDER BY ssc.DateOfChange DESC,
                             ssc.ModDate DESC
                );
            END;

        END;

    END;

    RETURN ISNULL(@ReturnValue, '''');
END;



'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END