/*
Run this script on:

        dev2.internal.fameinc.com\A1.Prod_Aveda_41SP1    -  This database will be modified

to synchronize it with a database with the schema represented by:

        Source

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.7.10021 from Red Gate Software Ltd at 7/23/2019 4:34:51 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
/*
* Use this Pre-Deployment script to perform tasks before the deployment of the project.
* Read more at https://www.red-gate.com/SOC7/pre-deployment-script-help
*/
UPDATE dbo.arClsSectMeetings
SET PeriodId = NULL
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)

DELETE FROM syPeriodsWorkDays
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[Trigger_UpdateProgressOfCourses] from [dbo].[arGrdBkResults]'
GO
IF OBJECT_ID(N'[dbo].[Trigger_UpdateProgressOfCourses]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[Trigger_UpdateProgressOfCourses]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[saDeferredRevenues_Audit_Delete] from [dbo].[saDeferredRevenues]'
GO
IF OBJECT_ID(N'[dbo].[saDeferredRevenues_Audit_Delete]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[saDeferredRevenues_Audit_Delete]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[saDeferredRevenues_Audit_Insert] from [dbo].[saDeferredRevenues]'
GO
IF OBJECT_ID(N'[dbo].[saDeferredRevenues_Audit_Insert]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[saDeferredRevenues_Audit_Insert]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[saDeferredRevenues_Audit_Update] from [dbo].[saDeferredRevenues]'
GO
IF OBJECT_ID(N'[dbo].[saDeferredRevenues_Audit_Update]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[saDeferredRevenues_Audit_Update]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_SA_getFinancialAidDisbursement]'
GO
IF OBJECT_ID(N'[dbo].[USP_SA_getFinancialAidDisbursement]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_SA_getFinancialAidDisbursement]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[SetupAdvantageTenant]'
GO
IF OBJECT_ID(N'[dbo].[SetupAdvantageTenant]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[SetupAdvantageTenant]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[CreateAdvantageDbPermission]'
GO
IF OBJECT_ID(N'[dbo].[CreateAdvantageDbPermission]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[CreateAdvantageDbPermission]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[UDF_GetEnrollmentStatusAtGivenDate]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UDF_GetEnrollmentStatusAtGivenDate]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[UDF_GetEnrollmentStatusAtGivenDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[CalculateStudentAverage]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateStudentAverage]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[CalculateStudentAverage]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[syUserResources]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[syUserResources]', N'UseLeftJoin') IS NULL
ALTER TABLE [dbo].[syUserResources] ADD[UseLeftJoin] [bit] NULL CONSTRAINT [DF_syUserResources_UseLeftJoin] DEFAULT ((0))
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudent]'
GO
IF OBJECT_ID(N'[dbo].[arStudent]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CalculateStudentAverage]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[CalculateStudentAverage]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'-- =============================================
-- Author:		FAME Inc.
-- Create date: 6/11/2019
-- Description:	Calculated Student GPA Based on a set of parameters - Numeric ( Weighted & Unweighted currently implemented)
--Referenced in :
--[Usp_PR_Sub4_Courses_forOneTermAllEnrolmnents] -> via db function CalculateStudentAverage
--[Usp_PR_Sub3_Terms_forAllEnrolmnents] -> via db function CalculateStudentAverage
--ANY CHANGES TO THIS FILE MUST BE REFLECTED IN SP GPA_Calculator
-- =============================================
CREATE FUNCTION [dbo].[CalculateStudentAverage]
    (
        @EnrollmentId VARCHAR(50)
       ,@BeginDate DATETIME = NULL
       ,@EndDate DATETIME = NULL
       ,@ClassId UNIQUEIDENTIFIER = NULL
       ,@TermId UNIQUEIDENTIFIER = NULL
    )
RETURNS DECIMAL(16, 2)
AS
    BEGIN
        DECLARE @GPAResult AS DECIMAL(16, 2);
        DECLARE @UseWeightForGPA BIT = 1;

        IF ( @EnrollmentId IS NULL )
            RETURN @GPAResult;

        SET @UseWeightForGPA = (
                               SELECT TOP 1 PV.DoCourseWeightOverallGPA
                               FROM   dbo.arStuEnrollments E
                               JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                               WHERE  E.StuEnrollId = @EnrollmentId
                               );

        -----------------Numeric GPA Grades Format------------------------

        SET @GPAResult = isnull(
                         (SELECT ( CASE WHEN @UseWeightForGPA = 1 THEN
                                           ROUND(( SUM(b.CourseWeight * b.WeightedCourseAverage / 100) / SUM(b.CourseWeight)) * 100, 2)
                                       ELSE AVG(b.UnweightedCourseAverage)
                                  END
                                ) AS WeightedGPA
                         FROM   (
                                SELECT   SUM(OurSingleClassGradeFactor) AS CourseFactor
                                        ,SUM(a.GradeWeight) AS GradeWeight
                                        ,SUM(a.Score) AS SumOfScores
                                        ,( SUM(OurSingleClassGradeFactor) / SUM(a.GradeWeight)) * 100 AS WeightedCourseAverage
                                        ,( AVG(a.Score)) AS UnweightedCourseAverage
                                        ,ClsSectionId
                                        ,a.CourseWeight
                                FROM     (
                                         SELECT ( c.Weight * a.Score / 100 ) AS OurSingleClassGradeFactor
                                               ,c.Weight AS GradeWeight
                                               ,a.Score
                                               ,a.ClsSectionId
                                               ,d.CourseWeight
                                               ,c.Descrip AS ClassDescrip
                                         FROM   (
                                                SELECT   StuEnrollId
                                                        ,a.ClsSectionId
                                                        ,a.InstrGrdBkWgtDetailId
                                                        ,AVG(Score) AS Score
                                                FROM     dbo.arGrdBkResults a
                                                JOIN     dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                                JOIN     dbo.arGrdComponentTypes f ON f.GrdComponentTypeId = c.GrdComponentTypeId
                                                JOIN     dbo.arClassSections ON arClassSections.ClsSectionId = a.ClsSectionId
                                                WHERE    a.StuEnrollId = @EnrollmentId
                                                         --AND a.IsCompGraded = 1
														 AND a.Score >= 0
                                                         AND a.Score IS NOT NULL
                                                         AND a.PostDate IS NOT NULL
                                                         AND (
                                                             @EndDate IS NULL
                                                             OR ( a.PostDate <= @EndDate )
                                                             )
                                                         AND (
                                                             @ClassId IS NULL
                                                             OR ( @ClassId = a.ClsSectionId )
                                                             )
                                                         AND (
                                                             @TermId IS NULL
                                                             OR ( @TermId = TermId )
                                                             )
                                                GROUP BY StuEnrollId
                                                        ,a.ClsSectionId
                                                        ,a.InstrGrdBkWgtDetailId
                                                ) a -- students grades
                                         JOIN   dbo.arClassSections b ON b.ClsSectionId = a.ClsSectionId
                                         JOIN   dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                         JOIN   dbo.arProgVerDef d ON d.ReqId = b.ReqId
                                         --ORDER BY f.Descrip, a.Score desc
                                         ) a
								WHERE a.CourseWeight > 0
                                GROUP BY ClsSectionId
                                        ,a.CourseWeight

                                --ORDER BY CourseGPA DESC
                                ) b
                         ), 0.00);
        --END;
        RETURN @GPAResult
    END;







'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[Trigger_UpdateProgressOfCourses] on [dbo].[arGrdBkResults]'
GO
IF OBJECT_ID(N'[dbo].[Trigger_UpdateProgressOfCourses]', 'TR') IS NULL
EXEC sp_executesql N'
CREATE TRIGGER [dbo].[Trigger_UpdateProgressOfCourses]
ON [dbo].[arGrdBkResults]
FOR INSERT, UPDATE
AS
SET NOCOUNT ON;

    BEGIN
        DECLARE @Error AS INTEGER;

        SET @Error = 0;

        BEGIN TRANSACTION FixCoursesScore;
        BEGIN TRY

            DECLARE @CourseResultsToUpdate AS TABLE
                (
                    FinalScore DECIMAL(18, 2) NULL
                   ,NumberOfComponents INT NULL
                   ,NumberOfComponentsScored INT NULL
                   ,ClsSectionId UNIQUEIDENTIFIER NULL
                   ,StuEnrollId UNIQUEIDENTIFIER NULL
                );

            DECLARE @GradeRounding VARCHAR(10);
            SET @GradeRounding = (
                                 SELECT     LOWER(Value)
                                 FROM       dbo.syConfigAppSetValues configValues
                                 INNER JOIN dbo.syConfigAppSettings configSettings ON configSettings.SettingId = configValues.SettingId
                                 WHERE      configSettings.KeyName = ''GradeRounding''
                                 );


            INSERT INTO @CourseResultsToUpdate (
                                               FinalScore
                                              ,NumberOfComponents
                                              ,NumberOfComponentsScored
                                              ,ClsSectionId
                                              ,StuEnrollId
                                               )
                        SELECT     CASE WHEN @GradeRounding = ''yes'' THEN
                                            ROUND(dbo.CalculateStudentAverage(enrollments.StuEnrollId, NULL, NULL, classSections.ClsSectionId, NULL), 2)
                                        ELSE dbo.CalculateStudentAverage(enrollments.StuEnrollId, NULL, NULL, classSections.ClsSectionId, NULL)
                                   END AS FinalScore
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   dbo.arGrdBkResults
                                   WHERE  StuEnrollId = enrollments.StuEnrollId
                                          AND ClsSectionId = classSections.ClsSectionId
                                   ) AS NumberOfComponents
                                  ,(
                                   SELECT COUNT(*)
                                   FROM   dbo.arGrdBkResults
                                   WHERE  StuEnrollId = enrollments.StuEnrollId
                                          AND ClsSectionId = classSections.ClsSectionId
                                          AND Score IS NOT NULL
                                   ) AS NumberOfComponentsScored
                                  ,classSections.ClsSectionId
                                  ,courseResults.StuEnrollId
                        FROM       dbo.arResults courseResults
                        INNER JOIN dbo.arStuEnrollments enrollments ON enrollments.StuEnrollId = courseResults.StuEnrollId
                        INNER JOIN dbo.syStatusCodes statusCodes ON statusCodes.StatusCodeId = enrollments.StatusCodeId
                        INNER JOIN dbo.arClassSections classSections ON classSections.ClsSectionId = courseResults.TestId
                        INNER JOIN dbo.arGrdBkResults graebookResults ON graebookResults.StuEnrollId = courseResults.StuEnrollId
                                                                         AND graebookResults.ClsSectionId = classSections.ClsSectionId
                        INNER JOIN dbo.arGrdBkWgtDetails gradebookWeight ON gradebookWeight.InstrGrdBkWgtDetailId = graebookResults.InstrGrdBkWgtDetailId
                        INNER JOIN dbo.arReqs courses ON courses.ReqId = classSections.ReqId
                        WHERE      statusCodes.SysStatusId NOT IN ( 8, 12, 14, 19 )
                                   AND (
                                       SELECT MAX(ModDate)
                                       FROM   dbo.arGrdBkResults results
                                       WHERE  results.StuEnrollId = enrollments.StuEnrollId
                                              AND results.ClsSectionId = classSections.ClsSectionId
                                       ) >= courseResults.ModDate
                                   AND graebookResults.StuEnrollId IN (
                                                                      SELECT Inserted.StuEnrollId
                                                                      FROM   Inserted
                                                                      )
                                   AND graebookResults.ClsSectionId IN (
                                                                       SELECT Inserted.ClsSectionId
                                                                       FROM   Inserted
                                                                       )
                        ORDER BY   courseResults.Score;

            UPDATE     courses
            SET        courses.Score = coursesToUpdate.FinalScore
                      ,courses.IsInComplete = CASE WHEN coursesToUpdate.NumberOfComponents = coursesToUpdate.NumberOfComponentsScored THEN 0
                                                   ELSE 1
                                              END
                      ,courses.DateCompleted = CASE WHEN coursesToUpdate.NumberOfComponents = coursesToUpdate.NumberOfComponentsScored THEN GETDATE()
                                                    ELSE courses.DateCompleted
                                               END
                      ,courses.ModDate = GETDATE()
                      ,courses.IsCourseCompleted = 1
                      ,courses.GrdSysDetailId = (
                                                SELECT     TOP 1 gradeDetails.GrdSysDetailId
                                                FROM       dbo.arStuEnrollments enrollments
                                                INNER JOIN dbo.arPrgVersions programVersion ON programVersion.PrgVerId = enrollments.PrgVerId
                                                INNER JOIN dbo.arGradeSystemDetails gradeDetails ON gradeDetails.GrdSystemId = programVersion.GrdSystemId
                                                INNER JOIN dbo.arGradeScaleDetails gradeScale ON gradeScale.GrdSysDetailId = gradeDetails.GrdSysDetailId
                                                WHERE      enrollments.StuEnrollId = courses.StuEnrollId
                                                           AND gradeScale.MinVal >= coursesToUpdate.FinalScore
                                                           AND gradeScale.MaxVal <= coursesToUpdate.FinalScore
                                                ORDER BY   gradeDetails.Grade
                                                )
            FROM       dbo.arResults courses
            INNER JOIN @CourseResultsToUpdate coursesToUpdate ON coursesToUpdate.ClsSectionId = courses.TestId
                                                                 AND coursesToUpdate.StuEnrollId = courses.StuEnrollId
            INNER JOIN dbo.arStuEnrollments enrollments ON enrollments.StuEnrollId = courses.StuEnrollId
            INNER JOIN dbo.arPrgVersions programVersion ON programVersion.PrgVerId = enrollments.PrgVerId
            WHERE      programVersion.ProgramRegistrationType = 1;

            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;

        END TRY
        BEGIN CATCH
            DECLARE @msg NVARCHAR(MAX);
            DECLARE @severity INT;
            DECLARE @state INT;
            SELECT @msg = ERROR_MESSAGE()
                  ,@severity = ERROR_SEVERITY()
                  ,@state = ERROR_STATE();
            RAISERROR(@msg, @severity, @state);
            SET @Error = 1;
        END CATCH;

        IF ( @Error = 0 )
            BEGIN
                COMMIT TRANSACTION FixCoursesScore;

            END;
        ELSE
            BEGIN
                ROLLBACK TRANSACTION FixCoursesScore;
            END;

    END;


SET NOCOUNT OFF;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[saDeferredRevenues_Audit_Delete] on [dbo].[saDeferredRevenues]'
GO
IF OBJECT_ID(N'[dbo].[saDeferredRevenues_Audit_Delete]', 'TR') IS NULL
EXEC sp_executesql N'
CREATE TRIGGER [dbo].[saDeferredRevenues_Audit_Delete] ON [dbo].[saDeferredRevenues]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,''saDeferredRevenues'',''D'',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,''Amount''
                               ,CONVERT(VARCHAR(8000),Old.Amount,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,''TransactionId''
                               ,CONVERT(VARCHAR(8000),Old.TransactionId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,''DefRevenueDate''
                               ,CONVERT(VARCHAR(8000),Old.DefRevenueDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,''Source''
                               ,CONVERT(VARCHAR(8000),Old.Source,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,''Type''
                               ,CONVERT(VARCHAR(8000),Old.Type,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,''IsPosted''
                               ,CONVERT(VARCHAR(8000),Old.IsPosted,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[saDeferredRevenues_Audit_Insert] on [dbo].[saDeferredRevenues]'
GO
IF OBJECT_ID(N'[dbo].[saDeferredRevenues_Audit_Insert]', 'TR') IS NULL
EXEC sp_executesql N'
CREATE TRIGGER [dbo].[saDeferredRevenues_Audit_Insert] ON [dbo].[saDeferredRevenues]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,''saDeferredRevenues'',''I'',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,''Amount''
                               ,CONVERT(VARCHAR(8000),New.Amount,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,''TransactionId''
                               ,CONVERT(VARCHAR(8000),New.TransactionId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,''DefRevenueDate''
                               ,CONVERT(VARCHAR(8000),New.DefRevenueDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,''Source''
                               ,CONVERT(VARCHAR(8000),New.Source,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,''Type''
                               ,CONVERT(VARCHAR(8000),New.Type,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,''IsPosted''
                               ,CONVERT(VARCHAR(8000),New.IsPosted,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[saDeferredRevenues_Audit_Update] on [dbo].[saDeferredRevenues]'
GO
IF OBJECT_ID(N'[dbo].[saDeferredRevenues_Audit_Update]', 'TR') IS NULL
EXEC sp_executesql N'
CREATE TRIGGER [dbo].[saDeferredRevenues_Audit_Update] ON [dbo].[saDeferredRevenues]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,''saDeferredRevenues'',''U'',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(Amount)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,''Amount''
                                   ,CONVERT(VARCHAR(8000),Old.Amount,121)
                                   ,CONVERT(VARCHAR(8000),New.Amount,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.Amount <> New.Amount
                                    OR (
                                         Old.Amount IS NULL
                                         AND New.Amount IS NOT NULL
                                       )
                                    OR (
                                         New.Amount IS NULL
                                         AND Old.Amount IS NOT NULL
                                       ); 
                IF UPDATE(TransactionId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,''TransactionId''
                                   ,CONVERT(VARCHAR(8000),Old.TransactionId,121)
                                   ,CONVERT(VARCHAR(8000),New.TransactionId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.TransactionId <> New.TransactionId
                                    OR (
                                         Old.TransactionId IS NULL
                                         AND New.TransactionId IS NOT NULL
                                       )
                                    OR (
                                         New.TransactionId IS NULL
                                         AND Old.TransactionId IS NOT NULL
                                       ); 
                IF UPDATE(DefRevenueDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,''DefRevenueDate''
                                   ,CONVERT(VARCHAR(8000),Old.DefRevenueDate,121)
                                   ,CONVERT(VARCHAR(8000),New.DefRevenueDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.DefRevenueDate <> New.DefRevenueDate
                                    OR (
                                         Old.DefRevenueDate IS NULL
                                         AND New.DefRevenueDate IS NOT NULL
                                       )
                                    OR (
                                         New.DefRevenueDate IS NULL
                                         AND Old.DefRevenueDate IS NOT NULL
                                       ); 
                IF UPDATE(Source)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,''Source''
                                   ,CONVERT(VARCHAR(8000),Old.Source,121)
                                   ,CONVERT(VARCHAR(8000),New.Source,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.Source <> New.Source
                                    OR (
                                         Old.Source IS NULL
                                         AND New.Source IS NOT NULL
                                       )
                                    OR (
                                         New.Source IS NULL
                                         AND Old.Source IS NOT NULL
                                       ); 
                IF UPDATE(Type)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,''Type''
                                   ,CONVERT(VARCHAR(8000),Old.Type,121)
                                   ,CONVERT(VARCHAR(8000),New.Type,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.Type <> New.Type
                                    OR (
                                         Old.Type IS NULL
                                         AND New.Type IS NOT NULL
                                       )
                                    OR (
                                         New.Type IS NULL
                                         AND Old.Type IS NOT NULL
                                       ); 
                IF UPDATE(IsPosted)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,''IsPosted''
                                   ,CONVERT(VARCHAR(8000),Old.IsPosted,121)
                                   ,CONVERT(VARCHAR(8000),New.IsPosted,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.IsPosted <> New.IsPosted
                                    OR (
                                         Old.IsPosted IS NULL
                                         AND New.IsPosted IS NOT NULL
                                       )
                                    OR (
                                         New.IsPosted IS NULL
                                         AND Old.IsPosted IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[UDF_GetEnrollmentStatusAtGivenDate]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UDF_GetEnrollmentStatusAtGivenDate]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'--------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Function to return the status of an enrollment at a specified date
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[UDF_GetEnrollmentStatusAtGivenDate]
(
    @StuEnrollId UNIQUEIDENTIFIER,
    @ReportDate DATETIME
)
RETURNS VARCHAR(50)
AS
BEGIN
    DECLARE @ReturnValue VARCHAR(50);
    DECLARE @CurrentStatus VARCHAR(50);
    DECLARE @StartDate DATETIME;
    DECLARE @LastDateOfChange DATETIME;
    DECLARE @LastNewStatus VARCHAR(50);

    SET @ReturnValue = '''';

    SELECT @CurrentStatus = sc.StatusCodeDescrip,
           @StartDate = se.StartDate
    FROM dbo.arStuEnrollments se
        INNER JOIN dbo.syStatusCodes sc
            ON sc.StatusCodeId = se.StatusCodeId
    WHERE se.StuEnrollId = @StuEnrollId;

    SET @LastDateOfChange =
    (
        SELECT MAX(DateOfChange)
        FROM dbo.syStudentStatusChanges
        WHERE StuEnrollId = @StuEnrollId
    );

    SET @LastNewStatus =
    (
        SELECT TOP 1
               sc.StatusCodeDescrip
        FROM dbo.syStudentStatusChanges ssc
            INNER JOIN dbo.syStatusCodes sc
                ON sc.StatusCodeId = ssc.NewStatusId
        WHERE ssc.StuEnrollId = @StuEnrollId
        ORDER BY ssc.DateOfChange DESC,
                 ssc.ModDate DESC
    );


    --If there are no status changes for the enrollment and the given date is >= enrollment start date we can simply return the current status. 
    IF NOT EXISTS
    (
        SELECT 1
        FROM dbo.syStudentStatusChanges
        WHERE StuEnrollId = @StuEnrollId
    )
    BEGIN
        IF @ReportDate >= @StartDate
        BEGIN
            SET @ReturnValue = @CurrentStatus;
        END;
    END;
    ELSE
    BEGIN
        --If the report date > last date of change for the enrollment then we can simply return the last new status (should be same as current status)
        IF @ReportDate > @LastDateOfChange
           AND @ReportDate >= @StartDate
        BEGIN
            SET @ReturnValue = @LastNewStatus;
        END;
        ELSE
        BEGIN
            --If the report date is the same as a date of change then we can simply return the new status for that date of change.
            --Use the FORMAT function just in case the DateOfChange field in the database has a time component on it such as 2018-10-19 05:00:00.000
            IF EXISTS
            (
                SELECT 1
                FROM dbo.syStudentStatusChanges
                WHERE StuEnrollId = @StuEnrollId
                      AND CONVERT(VARCHAR, DateOfChange, 101) = CONVERT(VARCHAR, @ReportDate, 101)
            )
            BEGIN
                SET @ReturnValue =
                (
                    SELECT TOP 1
                           sc.StatusCodeDescrip
                    FROM dbo.syStudentStatusChanges ssc
                        INNER JOIN dbo.syStatusCodes sc
                            ON sc.StatusCodeId = ssc.NewStatusId
                    WHERE ssc.StuEnrollId = @StuEnrollId
                          AND CONVERT(VARCHAR, DateOfChange, 101) = CONVERT(VARCHAR, @ReportDate, 101)
                    ORDER BY ssc.DateOfChange DESC,
                             ssc.ModDate DESC
                );

            END;
            ELSE
            BEGIN
                --At this point we can get the last change record with DateOfChange that is less than the report date
                SET @ReturnValue =
                (
                    SELECT TOP 1
                           sc.StatusCodeDescrip
                    FROM dbo.syStudentStatusChanges ssc
                        INNER JOIN dbo.syStatusCodes sc
                            ON sc.StatusCodeId = ssc.NewStatusId
                    WHERE ssc.StuEnrollId = @StuEnrollId
                          AND ssc.DateOfChange < @ReportDate
                    ORDER BY ssc.DateOfChange DESC,
                             ssc.ModDate DESC
                );
            END;

        END;

    END;

    RETURN ISNULL(@ReturnValue, '''');
END;



'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[SetupAdvantageTenant]'
GO
IF OBJECT_ID(N'[dbo].[SetupAdvantageTenant]', 'P') IS NULL
EXEC sp_executesql N'create PROCEDURE [dbo].[SetupAdvantageTenant]
    @ServerName NVARCHAR(200)
   ,@DBUser NVARCHAR(100)
   ,@DBPassword NVARCHAR(100)
   ,@Environment INT
   ,@SiteURL NVARCHAR(300)
   ,@AdvantageApiURL NVARCHAR(300)
   ,@ServiceURL NVARCHAR(300)
   ,@DBOUser NVARCHAR(100)
   ,@ReportExecutionServices VARCHAR(100)
   ,@ReportServices VARCHAR(100)
AS
    BEGIN
        DECLARE @DatabaseName NVARCHAR(100);
        DECLARE @AuthKey UNIQUEIDENTIFIER = NEWID();
		DECLARE @SUPPORT_ID UNIQUEIDENTIFIER = ''864335ee-c9c6-47c9-bbcb-deee766f27a1''
		DECLARE @DATA_CONNECTION_ID INT
		DECLARE @TENANT_ID int
        SET @DatabaseName = (
                            SELECT DB_NAME() AS [Current Database]
                            );

        SET @TENANT_ID = (
                         SELECT TOP 1 TenantId
                         FROM   TenantAuthDB..Tenant
                         WHERE  [TenantName] = @DatabaseName
                         );



        IF ( @TENANT_ID IS NULL )
            BEGIN
                PRINT ''TENANT DOES NOT EXIST'';
                BEGIN TRANSACTION;
                INSERT INTO TenantAuthDB..Tenant
                VALUES ( @DatabaseName, GETDATE(), GETDATE(), ''SA'', @ServerName, @DatabaseName, @DBUser, @DBPassword, @Environment );

                SET @TENANT_ID = (
                                 SELECT TOP 1 TenantId
                                 FROM   TenantAuthDB..Tenant
                                 WHERE  [TenantName] = @DatabaseName
                                 );

                PRINT ''TENANT CREATED : '' + CONVERT(NVARCHAR, @TENANT_ID);
                COMMIT;
            END;

        SET @DATA_CONNECTION_ID = (
                                  SELECT TOP 1 [ConnectionStringId]
                                  FROM   TenantAuthDB..DataConnection
                                  WHERE  DatabaseName = @DatabaseName
                                  );
        IF ( @DATA_CONNECTION_ID IS NULL )
            BEGIN
                PRINT ''CONNECTION STRING DOES NOT EXIST'';
                BEGIN TRANSACTION;
                INSERT INTO TenantAuthDB..[DataConnection]
                VALUES ( @ServerName, @DatabaseName, @DBUser, NULL, @DBPassword );

                SET @DATA_CONNECTION_ID = (
                                          SELECT TOP 1 [ConnectionStringId]
                                          FROM   TenantAuthDB..DataConnection
                                          WHERE  DatabaseName = @DatabaseName
                                          );

                PRINT ''CONNECTION STRING CREATED : '' + CONVERT(NVARCHAR, @DATA_CONNECTION_ID);
                COMMIT;
            END;

        IF NOT EXISTS (
                      SELECT *
                      FROM   TenantAuthDB..[TenantAccess]
                      WHERE  [TenantId] = @TENANT_ID
                             AND [EnvironmentId] = @Environment
                             AND [ConnectionStringId] = @DATA_CONNECTION_ID
                      )
            BEGIN
                PRINT ''TENANT ACCESS DOES NOT EXIST'';
                BEGIN TRANSACTION;
                INSERT INTO TenantAuthDB..[TenantAccess]
                VALUES ( @TENANT_ID, @Environment, @DATA_CONNECTION_ID );

                PRINT ''TENANT ACCESS CREATED'';
                COMMIT;
            END;
        IF NOT EXISTS (
                      SELECT *
                      FROM   TenantAuthDB..TenantUsers
                      WHERE  [TenantId] = @TENANT_ID
                             AND [UserId] = @SUPPORT_ID
                      )
            BEGIN
                PRINT ''TENANT SUPPORT USER DOES NOT EXIST'';
                BEGIN TRANSACTION;
                INSERT INTO TenantAuthDB..[TenantUsers]
                VALUES ( @TENANT_ID, @SUPPORT_ID, 0, 1 );

                PRINT ''TENANT SUPPORT USER CREATED'';
                COMMIT;
            END;
        IF NOT EXISTS (
                      SELECT *
                      FROM   TenantAuthDB..[ApiAuthenticationKey]
                      WHERE  [TenantId] = @TENANT_ID
                      )
            BEGIN
                PRINT ''TENANT ApiAuthenticationKey DOES NOT EXIST'';
                BEGIN TRANSACTION;
                INSERT INTO TenantAuthDB..[ApiAuthenticationKey]
                VALUES ( @TENANT_ID, @AuthKey, GETDATE());

                PRINT ''TENANT ApiAuthenticationKey CREATED'';
                COMMIT;
            END;
        ELSE
            BEGIN
                UPDATE A
                SET    [Key] = @AuthKey
                FROM   TenantAuthDB..[ApiAuthenticationKey] A
                WHERE  [TenantId] = @TENANT_ID;
            END;


        EXEC dbo.sp_changedbowner @loginame = @DBOUser
                                 ,@map = false;
        PRINT ''DB Owner Changed'';

        UPDATE [syConfigAppSetValues]
        SET    Value = @SiteURL
        WHERE  SettingId = (
                           SELECT val.[SettingId]
                           FROM   [dbo].[syConfigAppSetValues] val
                           JOIN   [dbo].[syConfigAppSettings] ON syConfigAppSettings.SettingId = val.SettingId
                           WHERE  KeyName = ''AdvantageSiteUri''
                           );

        UPDATE [syConfigAppSetValues]
        SET    Value = @ServiceURL
        WHERE  SettingId = (
                           SELECT val.[SettingId]
                           FROM   [dbo].[syConfigAppSetValues] val
                           JOIN   [dbo].[syConfigAppSettings] ON syConfigAppSettings.SettingId = val.SettingId
                           WHERE  KeyName = ''AdvantageServiceUri''
                           );

        UPDATE [syConfigAppSetValues]
        SET    Value = @AuthKey
        WHERE  SettingId = (
                           SELECT val.[SettingId]
                           FROM   [dbo].[syConfigAppSetValues] val
                           JOIN   [dbo].[syConfigAppSettings] ON syConfigAppSettings.SettingId = val.SettingId
                           WHERE  KeyName = ''AdvantageServiceAuthenticationKey''
                           );

        UPDATE [syConfigAppSetValues]
        SET    Value = @ReportExecutionServices
        WHERE  SettingId = (
                           SELECT val.[SettingId]
                           FROM   [dbo].[syConfigAppSetValues] val
                           JOIN   [dbo].[syConfigAppSettings] ON syConfigAppSettings.SettingId = val.SettingId
                           WHERE  KeyName = ''ReportExecutionServices''
                           );

        UPDATE [syConfigAppSetValues]
        SET    Value = @ReportServices
        WHERE  SettingId = (
                           SELECT val.[SettingId]
                           FROM   [dbo].[syConfigAppSetValues] val
                           JOIN   [dbo].[syConfigAppSettings] ON syConfigAppSettings.SettingId = val.SettingId
                           WHERE  KeyName = ''ReportServices''
                           );


        UPDATE [syConfigAppSetValues]
        SET    Value = @AdvantageApiURL
        WHERE  SettingId = (
                           SELECT val.[SettingId]
                           FROM   [dbo].[syConfigAppSetValues] val
                           JOIN   [dbo].[syConfigAppSettings] ON syConfigAppSettings.SettingId = val.SettingId
                           WHERE  KeyName = ''AdvantageApiURL''
                           );

        PRINT ''Site, Service and AuthKey Updated'';

        SET @DatabaseName = ''['' + @DatabaseName + '']'';

        PRINT ''Executing SP UpdateAdvantageSupportUser'';

        EXEC TenantAuthDB..UpdateAdvantageSupportUser @DatabaseName;

        PRINT ''SP UpdateAdvantageSupportUser Executed'';
		SET NOCOUNT OFF
    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_SA_getFinancialAidDisbursement]'
GO
IF OBJECT_ID(N'[dbo].[USP_SA_getFinancialAidDisbursement]', 'P') IS NULL
EXEC sp_executesql N'CREATE PROCEDURE [dbo].[USP_SA_getFinancialAidDisbursement]
(
    @CampusId AS UNIQUEIDENTIFIER,
    @StartDate AS DATETIME,
    @EndDate AS DATETIME,
    @TransTypes AS VARCHAR(100)
)
AS
SELECT COUNT(*) AS RecordType,
       enrollments.StudentId,
       students.StudentNumber AS StudentIdentifier,
       students.LastName + '', '' + students.FirstName + ISNULL('' '' + students.MiddleName, '''') AS StudentName,
       CASE transactions.TransTypeId
           WHEN 2 THEN
               transactions.PaymentCodeId
           ELSE
               transactions.TransCodeId
       END AS TC,
       CASE transactions.TransTypeId
           WHEN 2 THEN
               paymentCodes.TransCodeDescrip
           ELSE
               transationCodes.TransCodeDescrip
       END AS TransCodeDescrip,
       transactions.TransDescrip,
       transactions.TransDate,
       transactions.TransAmount * -1 AS TransAmount,
       transactions.TransReference,
       transactionTypes.Description,
       CASE payments.PaymentTypeId
           WHEN 2 THEN
               ''Check Number: '' + payments.CheckNumber
           WHEN 3 THEN
               ''C/C Authorization: '' + payments.CheckNumber
           WHEN 4 THEN
               ''EFT Number: '' + payments.CheckNumber
           WHEN 5 THEN
               ''Money Order Number: '' + payments.CheckNumber
           WHEN 6 THEN
               ''Non Cash Reference #: '' + payments.CheckNumber
           ELSE
               payments.CheckNumber
       END AS CheckNumber,
       payments.PaymentTypeId,
       programVeresion.PrgVerDescrip,
       academicYears.AcademicYearDescrip,
       terms.TermDescrip,
       transactions.Voided,
       transactions.CampusId,
       transactions.ModUser,
       transactions.ModDate
FROM dbo.saTransactions AS transactions
    INNER JOIN dbo.arStuEnrollments enrollments
        ON enrollments.StuEnrollId = transactions.StuEnrollId
    INNER JOIN dbo.adLeads students
        ON students.LeadId = enrollments.LeadId
    INNER JOIN dbo.arPrgVersions programVeresion
        ON programVeresion.PrgVerId = enrollments.PrgVerId
    INNER JOIN dbo.saTransTypes transactionTypes
        ON transactionTypes.TransTypeId = transactions.TransTypeId
    LEFT JOIN saAcademicYears academicYears
        ON academicYears.AcademicYearId = transactions.AcademicYearId
    LEFT JOIN dbo.arTerm terms
        ON terms.TermId = transactions.TermId
    LEFT JOIN dbo.saTransCodes transationCodes
        ON transationCodes.TransCodeId = transactions.TransCodeId
    LEFT JOIN dbo.saTransCodes paymentCodes
        ON transationCodes.TransCodeId = transactions.PaymentCodeId
    LEFT JOIN saPayments payments
        ON payments.TransactionId = transactions.TransactionId
    LEFT JOIN saAppliedPayments appliedPayments
        ON appliedPayments.TransactionId = transactions.TransactionId
WHERE (
          CONVERT(VARCHAR(10), transactions.TransDate, 101) >= @StartDate
          AND CONVERT(VARCHAR(10), transactions.TransDate, 101) <= @EndDate
      )
      AND transactions.CampusId = @CampusId
      AND transactions.IsPosted = 1
      AND transactions.Voided = 0
      AND transactions.TransDescrip = @TransTypes
GROUP BY enrollments.StudentId,
         students.StudentNumber,
         students.LastName,
         students.FirstName,
         students.MiddleName,
         payments.PaymentTypeId,
         payments.CheckNumber,
         payments.PaymentTypeId,
         programVeresion.PrgVerDescrip,
         transactions.TransTypeId,
         paymentCodes.TransCodeDescrip,
         transationCodes.TransCodeDescrip,
         transactions.TransDescrip,
         transactions.TransDate,
         transactions.TransAmount,
         transactions.TransReference,
         academicYears.AcademicYearDescrip,
         terms.TermDescrip,
         transactionTypes.Description,
         transactions.Voided,
         transactions.CampusId,
         transactions.ModUser,
         transactions.ModDate,
         transactions.PaymentCodeId,
         transactions.TransCodeId
ORDER BY transactions.TransDescrip,
         students.LastName,
         transactions.TransDate;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[GlTransView1]'
GO
IF OBJECT_ID(N'[dbo].[GlTransView1]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[GlTransView1]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[GlTransView2]'
GO
IF OBJECT_ID(N'[dbo].[GlTransView2]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[GlTransView2]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[CreateAdvantageDbPermission]'
GO
IF OBJECT_ID(N'[dbo].[CreateAdvantageDbPermission]', 'P') IS NULL
EXEC sp_executesql N'create PROCEDURE [dbo].[CreateAdvantageDbPermission]
    @User VARCHAR(100)
AS
    BEGIN
	DECLARE @DatabaseName NVARCHAR(100);
	DECLARE @sql NVARCHAR(max);
	SET @DatabaseName = (
                            SELECT DB_NAME() AS [Current Database]
                            );
	SET @sql = ''use '' + @DatabaseName + '' CREATE USER ['' + @User  + ''] FOR LOGIN ['' + @User + ''];  ALTER ROLE [db_owner] ADD MEMBER ['' + @User + ''];''
	EXEC(@sql)
        SET NOCOUNT OFF;
    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Enabling constraints on [dbo].[saDeferredRevenues]'
GO
ALTER TABLE [dbo].[saDeferredRevenues] WITH CHECK CHECK CONSTRAINT [FK_saDeferredRevenues_saTransactions_TransactionId_TransactionId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END