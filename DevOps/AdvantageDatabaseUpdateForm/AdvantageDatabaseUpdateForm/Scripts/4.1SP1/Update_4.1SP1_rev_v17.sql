﻿/*
Run this script on:

        dev-com-db1\Adv.AvedaLive    -  This database will be modified

to synchronize it with a database with the schema represented by:

        Source

You are recommended to back up your database before running this script

Script created by SQL Compare version 13.7.7.10021 from Red Gate Software Ltd at 8/20/2019 8:05:33 AM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
/*
* Use this Pre-Deployment script to perform tasks before the deployment of the project.
* Read more at https://www.red-gate.com/SOC7/pre-deployment-script-help
*/
UPDATE dbo.arClsSectMeetings
SET PeriodId = NULL
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)

DELETE FROM syPeriodsWorkDays
WHERE PeriodId NOT IN (SELECT PeriodId FROM dbo.syPeriods)
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_TR_Sub07_TotalCourses]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub07_TotalCourses]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_TR_Sub07_TotalCourses]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]'
GO
IF OBJECT_ID(N'[dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_Invoice_GetStudentInfo]'
GO
IF OBJECT_ID(N'[dbo].[USP_Invoice_GetStudentInfo]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_Invoice_GetStudentInfo]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudent]'
GO
IF OBJECT_ID(N'[dbo].[arStudent]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudAddresses]'
GO
IF OBJECT_ID(N'[dbo].[arStudAddresses]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudAddresses]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]'
GO
IF OBJECT_ID(N'[dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]', 'P') IS NULL
EXEC sp_executesql N'--=================================================================================================
-- USP_AT_Step02_InsertAttendance_Day_PresentAbsent
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_AT_Step02_InsertAttendance_Day_PresentAbsent]
    (
        @StuEnrollIdList AS VARCHAR(MAX) = NULL
    )
AS -- 
    -- Step 2  --  InsertAttendance_Day_PresentAbsent  -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'') 
    --         --  TrackSapAttendance = ''byday''
    BEGIN
        DECLARE @StuEnrollId UNIQUEIDENTIFIER
               ,@MeetDate DATETIME
               ,@WeekDay VARCHAR(15)
               ,@StartDate DATETIME
               ,@EndDate DATETIME
               ,@PeriodDescrip VARCHAR(50)
               ,@Actual DECIMAL(18, 2)
               ,@Excused DECIMAL(18, 2)
               ,@ClsSectionId UNIQUEIDENTIFIER
               ,@Absent DECIMAL(18, 2)
               ,@ScheduledMinutes DECIMAL(18, 2)
               ,@TardyMinutes DECIMAL(18, 2)
               ,@MakeupHours DECIMAL(18, 2)
               ,@tardy DECIMAL(18, 2)
               ,@tracktardies INT
               ,@tardiesMakingAbsence INT
               ,@PrgVerId UNIQUEIDENTIFIER
               ,@rownumber INT
               ,@IsTardy BIT
               ,@ActualRunningScheduledHours DECIMAL(18, 2)
               ,@ActualRunningPresentHours DECIMAL(18, 2)
               ,@ActualRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningTardyHours DECIMAL(18, 2)
               ,@ActualRunningMakeupHours DECIMAL(18, 2)
               ,@PrevStuEnrollId UNIQUEIDENTIFIER
               ,@intTardyBreakPoint INT
               ,@AdjustedRunningPresentHours DECIMAL(18, 2)
               ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningScheduledDays DECIMAL(18, 2);

        DECLARE @attendanceSummary TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,RecordDate DATETIME
               ,ActualHours DECIMAL(18, 2)
               ,SchedHours DECIMAL(18, 2)
               ,Absent DECIMAL(18, 2)
               ,isTardy BIT
               ,TrackTardies BIT
               ,TardiesMakingAbsence BIT
            );

        INSERT INTO @attendanceSummary (
                                       StuEnrollId
                                      ,RecordDate
                                      ,ActualHours
                                      ,SchedHours
                                      ,Absent
                                      ,isTardy
                                      ,TrackTardies
                                      ,TardiesMakingAbsence
                                       )
                    SELECT *
                    FROM   (
                           SELECT     t1.StuEnrollId
                                     ,t1.RecordDate
                                     ,t1.ActualHours
                                     ,t1.SchedHours
                                     ,CASE WHEN (
                                                (
                                                ((AAUT.UnitTypeDescrip = ''Present Absent'' AND t1.SchedHours >= 1) OR (AAUT.UnitTypeDescrip = ''Clock Hours'' AND t1.SchedHours > 0))
                                                AND t1.SchedHours NOT IN ( 999, 9999 )
                                                )
                                                AND t1.ActualHours = 0
                                                ) THEN t1.SchedHours
                                           ELSE 0
                                      END AS Absent
                                     ,t1.isTardy
                                     ,t3.TrackTardies
                                     ,t3.TardiesMakingAbsence
                           FROM       arStudentClockAttendance t1
                           INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                           INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                           INNER JOIN arAttUnitType AS AAUT ON AAUT.UnitTypeId = t3.UnitTypeId
                           WHERE --t3.UnitTypeId IN ( ''ef5535c2-142c-4223-ae3c-25a50a153cc6'',''B937C92E-FD7A-455E-A731-527A9918C734'' ) -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'') -
                                      AAUT.UnitTypeDescrip IN ( ''Present Absent'', ''Clock Hours'' )
                                      AND t1.ActualHours <> 9999.00
                                      AND (
                                          @StuEnrollIdList IS NULL
                                          OR t2.StuEnrollId IN (
                                                               SELECT Val
                                                               FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                               )
                                          )
                           ) paAttendance;

        DELETE FROM dbo.syStudentAttendanceSummary
        WHERE StuEnrollId IN (
                             SELECT DISTINCT StuEnrollId
                             FROM   @attendanceSummary
                             );

        DECLARE GetAttendance_Cursor CURSOR FAST_FORWARD FORWARD_ONLY FOR
            SELECT   *
            FROM     @attendanceSummary
            ORDER BY StuEnrollId
                    ,RecordDate;
        OPEN GetAttendance_Cursor;
        DECLARE @ActualHours DECIMAL(18, 2)
               ,@SchedHours DECIMAL(18, 2);
        FETCH NEXT FROM GetAttendance_Cursor
        INTO @StuEnrollId
            ,@MeetDate
            ,@Actual
            ,@ScheduledMinutes
            ,@Absent
            ,@IsTardy
            ,@tracktardies
            ,@tardiesMakingAbsence;

        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningAbsentHours = 0;
        SET @ActualRunningTardyHours = 0;
        SET @ActualRunningMakeupHours = 0;
        SET @intTardyBreakPoint = 0;
        SET @AdjustedRunningPresentHours = 0;
        SET @AdjustedRunningAbsentHours = 0;
        SET @ActualRunningScheduledDays = 0;
        SET @MakeupHours = 0;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                IF @PrevStuEnrollId <> @StuEnrollId
                    BEGIN
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        SET @MakeupHours = 0;
                    END;

                IF (
                   @Actual <> 9999
                   AND @Actual <> 999
                   )
                    BEGIN
                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + @ScheduledMinutes;
                        SET @ActualRunningPresentHours = ISNULL(@ActualRunningPresentHours, 0) + @Actual;
                        SET @AdjustedRunningPresentHours = ISNULL(@AdjustedRunningPresentHours, 0) + @Actual;
                    END;
                SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + @Absent;
                IF (
                   @Actual = 0
                   AND @ScheduledMinutes >= 1
                   AND @ScheduledMinutes NOT IN ( 999, 9999 )
                   )
                    BEGIN
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                    END;
                -- Applies only to AMC 
                -- If Scheduled = 3.0 hrs and Actual = 1.0 Then 2 hrs is considered absent
                IF (
                   @ScheduledMinutes >= 1
                   AND @ScheduledMinutes NOT IN ( 999, 9999 )
                   AND @Actual > 0
                   AND ( @Actual < @ScheduledMinutes )
                   )
                    BEGIN
                        SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                        SET @AdjustedRunningAbsentHours = ISNULL(@AdjustedRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                    END;
                IF (
                   @tracktardies = 1
                   AND @IsTardy = 1
                   )
                    BEGIN
                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + 1;
                    END;
                -- commented by balaji on 10.22.2012 as rdl not adding attended days and makeup days
                -- If there are make up hrs deduct that otherwise it will be added again in progress report
                IF (
                   @Actual > 0
                   AND @Actual > @ScheduledMinutes
                   AND @Actual <> 9999.00
                   )
                    BEGIN
                        SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                    END;
                IF @tracktardies = 1
                   AND (
                       @TardyMinutes > 0
                       OR @IsTardy = 1
                       )
                    BEGIN
                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                    END;

                -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                IF (
                   @Actual > 0
                   AND @Actual > @ScheduledMinutes
                   AND @Actual <> 9999.00
                   )
                    BEGIN
                        SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                    END;

                IF (
                   @tracktardies = 1
                   AND @intTardyBreakPoint = @tardiesMakingAbsence
                   )
                    BEGIN
                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                        SET @intTardyBreakPoint = 0;
                    END;


                INSERT INTO syStudentAttendanceSummary (
                                                       StuEnrollId
                                                      ,ClsSectionId
                                                      ,StudentAttendedDate
                                                      ,ScheduledDays
                                                      ,ActualDays
                                                      ,ActualRunningScheduledDays
                                                      ,ActualRunningPresentDays
                                                      ,ActualRunningAbsentDays
                                                      ,ActualRunningMakeupDays
                                                      ,ActualRunningTardyDays
                                                      ,AdjustedPresentDays
                                                      ,AdjustedAbsentDays
                                                      ,AttendanceTrackType
                                                      ,ModUser
                                                      ,ModDate
                                                      ,tardiesmakingabsence
                                                       )
                VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, ISNULL(@ScheduledMinutes, 0), @Actual, ISNULL(@ActualRunningScheduledDays, 0)
                        ,ISNULL(@ActualRunningPresentHours, 0), ISNULL(@ActualRunningAbsentHours, 0), ISNULL(@MakeupHours, 0)
                        ,ISNULL(@ActualRunningTardyHours, 0), ISNULL(@AdjustedRunningPresentHours, 0), ISNULL(@AdjustedRunningAbsentHours, 0)
                        ,''Post Attendance by Class'', ''sa'', GETDATE(), @tardiesMakingAbsence );

                --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId

                SET @PrevStuEnrollId = @StuEnrollId;
                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@MeetDate
                    ,@Actual
                    ,@ScheduledMinutes
                    ,@Absent
                    ,@IsTardy
                    ,@tracktardies
                    ,@tardiesMakingAbsence;
            --@ActualRunningScheduledHours,@ActualRunningPresentHours,
            --@ActualRunningAbsentHours,@ActualRunningMakeupHours,@ActualRunningTardyHours,

            END;
        CLOSE GetAttendance_Cursor;
        DEALLOCATE GetAttendance_Cursor;

    END;
--=================================================================================================
-- END  --  USP_AT_Step02_InsertAttendance_Day_PresentAbsent
--=================================================================================================
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_Invoice_GetStudentInfo]'
GO
IF OBJECT_ID(N'[dbo].[USP_Invoice_GetStudentInfo]', 'P') IS NULL
EXEC sp_executesql N'-- =============================================
-- Author:		Edwin Sosa
-- Create date: 11/19/2018
-- Description:	Procedure for getting invoice info per student.
-- =============================================
CREATE PROCEDURE [dbo].[USP_Invoice_GetStudentInfo]
    -- Add the parameters for the stored procedure here
    @campusId UNIQUEIDENTIFIER
   ,@stuEnrollIdList VARCHAR(MAX)
   ,@refDate DATETIME
AS


    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        SELECT   payPlansAmounts.Address
                ,payPlansAmounts.City
                ,payPlansAmounts.Zip
                ,payPlansAmounts.StateDescrip
                ,payPlansAmounts.CountryDescrip
                ,SUM(PlanAmount) - SUM(payPlansAmounts.AmountPaid) AS OutstandingBalance
                ,SUM(payPlansAmounts.PlanAmount) AS OriginalTotal
                ,payPlansAmounts.StuEnrollId
                ,payPlansAmounts.SSN
                ,payPlansAmounts.StudentNumber
                ,payPlansAmounts.LastName
                ,payPlansAmounts.FirstName
                ,payPlansAmounts.MiddleName
                ,payPlansAmounts.Description
                ,payPlansAmounts.PaymentPlanId
				,payPlansAmounts.CampusName
				,payPlansAmounts.StartDate
				,payPlansAmounts.StatusCodeDescrip
				,payPlansAmounts.ProgDescrip
        FROM     (
                 SELECT     ASA2.Address
                           ,ASA2.City
                           ,ASA2.Zip
                           ,ASA2.StateDescrip
                           ,ASA2.CountryDescrip
                           ,ISNULL(FSPPS.Amount, 0) AS PlanAmount
                           ,ISNULL(SPDR2.AmountPaid, 0) AS AmountPaid
                           ,ASE.StuEnrollId
                           ,AST.SSN
                           ,AST.StudentNumber
                           ,AST.LastName
                           ,AST.FirstName
                           ,AST.MiddleName
                           ,FSPP.PayPlanDescrip AS Description
                           ,FSPP.PaymentPlanId
						   ,camp.CampDescrip AS CampusName
						   ,ASE.StartDate
						   ,SSC.StatusCodeDescrip
						   ,PRO.ProgDescrip
                 FROM       faStudentPaymentPlans AS FSPP
                 INNER JOIN faStuPaymentPlanSchedule AS FSPPS ON FSPPS.PaymentPlanId = FSPP.PaymentPlanId
                 INNER JOIN arStuEnrollments AS ASE ON ASE.StuEnrollId = FSPP.StuEnrollId
				 JOIN dbo.syCampuses camp ON camp.CampusId = ASE.CampusId
                 INNER JOIN arStudent AS AST ON AST.StudentId = ASE.StudentId
                 INNER JOIN syStatusCodes AS SSC ON SSC.StatusCodeId = ASE.StatusCodeId
                 INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = ASE.PrgVerId
				 INNER JOIN dbo.arPrograms AS PRO ON PRO.ProgId = APV.ProgId
                 LEFT JOIN  (
                            SELECT CASE WHEN amountsPaid.AmountPaid < 0 THEN amountsPaid.AmountPaid * -1
                                        ELSE amountsPaid.AmountPaid
                                   END AS AmountPaid
                                  ,amountsPaid.PayPlanScheduleId
                            FROM   (
                                   SELECT     SUM(ISNULL(SPDR.Amount, 0)) AS AmountPaid
                                             ,SPDR.PayPlanScheduleId
                                   FROM       saPmtDisbRel AS SPDR
                                   INNER JOIN saTransactions AS ST ON ST.TransactionId = SPDR.TransactionId
                                   INNER JOIN faStuPaymentPlanSchedule AS FSPPS2 ON FSPPS2.PayPlanScheduleId = SPDR.PayPlanScheduleId
                                   WHERE      ST.Voided = 0
                                   GROUP BY   SPDR.PayPlanScheduleId
                                   ) AS amountsPaid
                            ) AS SPDR2 ON SPDR2.PayPlanScheduleId = FSPPS.PayPlanScheduleId
                 LEFT JOIN  (
                            SELECT T.StudentId
                                  ,T.StdAddressId
                                  ,T.Address
                                  ,T.City
                                  ,T.Zip
                                  ,T.StateDescrip
                                  ,T.CountryDescrip
                            FROM   (
                                   SELECT     ASA.StudentId
                                             ,ASA.StdAddressId
                                             ,( Address1 + '' '' + ISNULL(Address2, '''')) AS Address
                                             ,ASA.City
                                             ,ASA.Zip
                                             ,ISNULL(adc.CountryDescrip, '''') AS CountryDescrip
                                             ,ISNULL(SS2.StateDescrip, '''') AS StateDescrip
                                             ,ROW_NUMBER() OVER ( PARTITION BY ASA.StudentId
                                                                  ORDER BY ASA.default1 DESC
                                                                ) AS RowNumber
                                   FROM       arStudAddresses AS ASA
                                   INNER JOIN syStatuses AS SS ON SS.StatusId = ASA.StatusId
                                   LEFT JOIN  syStates AS SS2 ON SS2.StateId = ASA.StateId
                                   LEFT JOIN  dbo.adCountries adc ON adc.CountryId = SS2.CountryId
                                   WHERE      SS.StatusCode = ''A''
                                   ) AS T
                            WHERE  T.RowNumber = 1
                            ) AS ASA2 ON ASA2.StudentId = AST.StudentId
                 WHERE      FSPPS.ExpectedDate <= @refDate -- RefDate  
                            AND ASE.CampusId = @campusId -- CampusId 
                            AND @stuEnrollIdList IS NULL
                            OR ( ASE.StuEnrollId IN (
                                                    SELECT Val
                                                    FROM   MultipleValuesForReportParameters(@stuEnrollIdList, '','', 1)
                                                    )
                               )
                 ) payPlansAmounts
        GROUP BY payPlansAmounts.Address
                ,payPlansAmounts.City
                ,payPlansAmounts.Zip
                ,payPlansAmounts.StateDescrip
                ,payPlansAmounts.CountryDescrip
                ,payPlansAmounts.StuEnrollId
                ,payPlansAmounts.SSN
                ,payPlansAmounts.StudentNumber
                ,payPlansAmounts.LastName
                ,payPlansAmounts.FirstName
                ,payPlansAmounts.MiddleName
                ,payPlansAmounts.Description
                ,payPlansAmounts.PaymentPlanId
				,payPlansAmounts.CampusName
				,payPlansAmounts.StartDate
				,payPlansAmounts.StatusCodeDescrip
				,payPlansAmounts.ProgDescrip
        ORDER BY payPlansAmounts.StuEnrollId;

    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_TR_Sub07_TotalCourses]'
GO
IF OBJECT_ID(N'[dbo].[USP_TR_Sub07_TotalCourses]', 'P') IS NULL
EXEC sp_executesql N'
-- =========================================================================================================  
-- USP_TR_Sub07_TotalCourses   
-- =========================================================================================================  
CREATE PROCEDURE [dbo].[USP_TR_Sub07_TotalCourses]
    @CoursesTakenTableName NVARCHAR(200)
   ,@GPASummaryTableName NVARCHAR(200)
   ,@StuEnrollIdList NVARCHAR(MAX)
   ,@TermId UNIQUEIDENTIFIER = NULL
   ,@ShowMultipleEnrollments BIT = 0
AS --  
    BEGIN

        -- 0) Declare varialbles and Set initial values  
        BEGIN
            --DECLARE @SQL01 AS NVARCHAR(MAX);  
            --DECLARE @SQL02 AS NVARCHAR(MAX);  

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#CoursesTaken'')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            CREATE TABLE #CoursesTaken
                (
                    CoursesTakenId INTEGER NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,ReqId UNIQUEIDENTIFIER
                   ,ReqCode VARCHAR(100)
                   ,ReqDescription VARCHAR(100)
                   ,ReqCodeDescrip VARCHAR(100)
                   ,ReqCredits DECIMAL(18, 2)
                   ,MinVal DECIMAL(18, 2)
                   ,ClsSectionId VARCHAR(50)
                   ,CourseStarDate DATETIME
                   ,CourseEndDate DATETIME
                   ,SCS_CreditsAttempted DECIMAL(18, 2)
                   ,SCS_CreditsEarned DECIMAL(18, 2)
                   ,SCS_CurrentScore DECIMAL(18, 2)
                   ,SCS_CurrentGrade VARCHAR(10)
                   ,SCS_FinalScore DECIMAL(18, 2)
                   ,SCS_FinalGrade VARCHAR(10)
                   ,SCS_Completed BIT
                   ,SCS_FinalGPA DECIMAL(18, 2)
                   ,SCS_Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_WeightedAverage_Credits DECIMAL(18, 2)
                   ,SCS_Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
                   ,SCS_Count_SimpleAverage_Credits DECIMAL(18, 2)
                   ,SCS_ModUser VARCHAR(50)
                   ,SCS_ModDate DATETIME
                   ,SCS_TermGPA_Simple DECIMAL(18, 2)
                   ,SCS_TermGPA_Weighted DECIMAL(18, 2)
                   ,SCS_coursecredits DECIMAL(18, 2)
                   ,SCS_CumulativeGPA DECIMAL(18, 2)
                   ,SCS_CumulativeGPA_Simple DECIMAL(18, 2)
                   ,SCS_FACreditsEarned DECIMAL(18, 2)
                   ,SCS_Average DECIMAL(18, 2)
                   ,SCS_CumAverage DECIMAL(18, 2)
                   ,ScheduleDays DECIMAL(18, 2)
                   ,ActualDay DECIMAL(18, 2)
                   ,FinalGPA_Calculations DECIMAL(18, 2)
                   ,FinalGPA_TermCalculations DECIMAL(18, 2)
                   ,CreditsEarned_Calculations DECIMAL(18, 2)
                   ,ActualDay_Calculations DECIMAL(18, 2)
                   ,GrdBkWgtDetailsCount INTEGER
                   ,CountMultipleEnrollment INTEGER
                   ,RowNumberMultipleEnrollment INTEGER
                   ,CountRepeated INTEGER
                   ,RowNumberRetaked INTEGER
                   ,SameTermCountRetaken INTEGER
                   ,RowNumberSameTermRetaked INTEGER
                   ,RowNumberMultEnrollNoRetaken INTEGER
                   ,RowNumberOverAllByClassSection INTEGER
                   ,IsCreditsEarned BIT
                );

            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#GPASummary'')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
            CREATE TABLE #GPASummary
                (
                    GPASummaryId INTEGER NOT NULL PRIMARY KEY
                   ,StudentId UNIQUEIDENTIFIER
                   ,LastName VARCHAR(50)
                   ,FirstName VARCHAR(50)
                   ,MiddleName VARCHAR(50)
                   ,SSN VARCHAR(11)
                   ,StudentNumber VARCHAR(50)
                   ,StuEnrollId UNIQUEIDENTIFIER
                   ,EnrollmentID VARCHAR(50)
                   ,PrgVerId UNIQUEIDENTIFIER
                   ,PrgVerDescrip VARCHAR(50)
                   ,PrgVersionTrackCredits BIT
                   ,GrdSystemId UNIQUEIDENTIFIER
                   ,AcademicType VARCHAR(50)
                   ,ClockHourProgram VARCHAR(10)
                   ,IsMakingSAP BIT
                   ,TermId UNIQUEIDENTIFIER
                   ,TermDescrip VARCHAR(100)
                   ,TermStartDate DATETIME
                   ,TermEndDate DATETIME
                   ,DescripXTranscript VARCHAR(250)
                   ,TermAverage DECIMAL(18, 2)
                   ,EnroTermSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPACredits DECIMAL(18, 2)
                   ,EnroTermSimple_GPA DECIMAL(18, 2)
                   ,EnroTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPACredits DECIMAL(18, 2)
                   ,EnroTermWeight_GPA DECIMAL(18, 2)
                   ,StudTermSimple_CourseCredits DECIMAL(18, 2)
                   ,StudTermSimple_GPACredits DECIMAL(18, 2)
                   ,StudTermSimple_GPA DECIMAL(18, 2)
                   ,StudTermWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudTermWeight_GPACredits DECIMAL(18, 2)
                   ,StudTermWeight_GPA DECIMAL(18, 2)
                   ,EnroSimple_CourseCredits DECIMAL(18, 2)
                   ,EnroSimple_GPACredits DECIMAL(18, 2)
                   ,EnroSimple_GPA DECIMAL(18, 2)
                   ,EnroWeight_CoursesCredits DECIMAL(18, 2)
                   ,EnroWeight_GPACredits DECIMAL(18, 2)
                   ,EnroWeight_GPA DECIMAL(18, 2)
                   ,StudSimple_CourseCredits DECIMAL(18, 2)
                   ,StudSimple_GPACredits DECIMAL(18, 2)
                   ,StudSimple_GPA DECIMAL(18, 2)
                   ,StudWeight_CoursesCredits DECIMAL(18, 2)
                   ,StudWeight_GPACredits DECIMAL(18, 2)
                   ,StudWeight_GPA DECIMAL(18, 2)
                   ,GradesFormat VARCHAR(50)
                   ,GPAMethod VARCHAR(50)
                   ,GradeBookAt VARCHAR(50)
                   ,RetakenPolicy VARCHAR(50)
                );
        END;
        -- END --  0) Declare varialbles and Set initial values  

        --  1)Fill temp tables  
        BEGIN
            INSERT INTO #CoursesTaken
            EXEC USP_TR_Sub03_GetPreparedGPATable @TableName = @CoursesTakenTableName;

            INSERT INTO #GPASummary
            EXEC USP_TR_Sub03_GetPreparedGPATable @TableName = @GPASummaryTableName;
        END;
        -- END  --  1)Fill temp tables  

        -- to Test  
        -- SELECT * FROM #CoursesTaken AS CT ORDER BY CT.StudentId, CT.StuEnrollId, CT.TermId  
        -- SELECT * FROM #GPASummary AS GS ORDER BY GS.StudentId, GS.StuEnrollId, GS.TermId  

        DECLARE @includeRepeatedCourses VARCHAR(50) = (
                                                      SELECT dbo.GetAppSettingValueByKeyName(''IncludeCreditsAttemptedForRepeatedCourses'', NULL)
                                                      );

        --  2) Selet Totals  
        BEGIN
            IF ( @ShowMultipleEnrollments = 0 )
                BEGIN
           
                    SELECT     GS.StuEnrollId AS Id
                              ,MIN(GS.GradesFormat) AS GradesFormat
                              ,MIN(GS.GPAMethod) AS GPAMethod
                              ,MIN(   CASE WHEN (
                                                dbo.GetACIdForStudent(GS.StuEnrollId) = 5
                                                AND dbo.GetGradesFormatGivenStudentEnrollId(GS.StuEnrollId) = ''numeric''
                                                ) THEN dbo.CalculateStudentAverage(GS.StuEnrollId, NULL, NULL, NULL, @TermId)
                                           ELSE GS.EnroSimple_GPA
                                      END
                                  ) AS EnrollmentSimpleGPA
                              ,MIN(   CASE WHEN (
                                                dbo.GetACIdForStudent(GS.StuEnrollId) = 5
                                                AND dbo.GetGradesFormatGivenStudentEnrollId(GS.StuEnrollId) = ''numeric''
                                                ) THEN dbo.CalculateStudentAverage(GS.StuEnrollId, NULL, NULL, NULL, @TermId)
                                           ELSE GS.EnroWeight_GPA
                                      END
                                  ) AS EnrollmentWeightedGPA
                              ,MAX(CONVERT(INTEGER, GS.IsMakingSAP)) AS IsMakingSAP
                              ,MIN(GS.AcademicType) AS AcademicType
                              ,MIN(CONVERT(INTEGER, GS.PrgVersionTrackCredits)) AS PrgVersionTrackCredits
                              ,CASE WHEN @includeRepeatedCourses = ''Yes'' THEN COUNT(CT.CoursesTakenId)
                                    ELSE COUNT(DISTINCT CT.ReqId)
                               END AS CoursesTaked
                              ,CASE WHEN @includeRepeatedCourses = ''Yes'' THEN SUM(ISNULL(CT.SCS_CreditsAttempted, 0))
                                    ELSE (
                                         SELECT SUM(CA.SCS_CreditsAttempted)
                                         FROM   (
                                                SELECT DISTINCT CAT.ReqId
                                                               ,CAT.SCS_CreditsAttempted
                                                FROM   #CoursesTaken CAT
                                                WHERE  CAT.StuEnrollId = GS.StuEnrollId
                                                ) CA
                                         )
                               END AS CreditsAttempted
                              ,SUM(ISNULL(CT.CreditsEarned_Calculations, 0)) AS CreditsEarned
                              ,CASE WHEN @includeRepeatedCourses = ''Yes'' THEN SUM(ISNULL(CT.ScheduleDays, 0))
                                    ELSE (
                                         SELECT SUM(SD.ScheduleDays)
                                         FROM   (
                                                SELECT DISTINCT SDT.ReqId
                                                               ,SDT.ScheduleDays
                                                FROM   #CoursesTaken SDT
                                                WHERE  SDT.StuEnrollId = GS.StuEnrollId
                                                ) SD
                                         )
                               END AS ScheduleDays
                              ,SUM(ISNULL(CT.ActualDay_Calculations, 0)) AS ActualDay
                              ,SUM(ISNULL(CT.FinalGPA_Calculations, 0)) AS GradePoints
                    FROM       #GPASummary AS GS
                    INNER JOIN #CoursesTaken AS CT ON CT.StuEnrollId = GS.StuEnrollId
                                                      AND CT.TermId = GS.TermId
                    WHERE      EXISTS (
                                      SELECT Val
                                      FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                      WHERE  CONVERT(UNIQUEIDENTIFIER, Val) = CT.StuEnrollId
                                      )
                               AND (
                                   @TermId IS NULL
                                   OR GS.TermId = @TermId
                                   )
                    GROUP BY   GS.StuEnrollId;
                END;
            ELSE
                BEGIN
                    SELECT     GS.StudentId AS Id
                              ,MIN(GS.GradesFormat) AS GradesFormat
                              ,MIN(GS.GPAMethod) AS GPAMethod
                              ,MIN(GS.StudSimple_GPA) AS EnrollmentSimpleGPA
                              ,MIN(GS.StudWeight_GPA) AS EnrollmentWeightedGPA
                              ,MAX(CONVERT(INTEGER, GS.IsMakingSAP)) AS IsMakingSAP
                              ,MIN(GS.AcademicType) AS AcademicType
                              ,MIN(CONVERT(INTEGER, GS.PrgVersionTrackCredits)) AS PrgVersionTrackCredits
                              ,CASE WHEN @includeRepeatedCourses = ''Yes'' THEN COUNT(CT.CoursesTakenId)
                                    ELSE COUNT(DISTINCT CT.ReqId)
                               END AS CoursesTaked
                              ,(
                               SELECT SUM(CA.SCS_CreditsAttempted)
                               FROM   (
                                      SELECT DISTINCT CAT.ReqId
                                                     ,CAT.SCS_CreditsAttempted
                                      FROM   #CoursesTaken CAT
                                      WHERE  CAT.StudentId = GS.StudentId
                                      ) CA
                               ) AS CreditsAttempted
                              ,SUM(ISNULL(CT.CreditsEarned_Calculations, 0)) AS CreditsEarned
                              ,CASE WHEN @includeRepeatedCourses = ''Yes'' THEN SUM(ISNULL(CT.ScheduleDays, 0))
                                    ELSE (
                                         SELECT SUM(SD.ScheduleDays)
                                         FROM   (
                                                SELECT DISTINCT SDT.ReqId
                                                               ,SDT.ScheduleDays
                                                FROM   #CoursesTaken SDT
                                                WHERE  SDT.StudentId = GS.StudentId
                                                ) SD
                                         )
                               END AS ScheduleDays
                              ,SUM(ISNULL(CT.ActualDay_Calculations, 0)) AS ActualDay
                              ,SUM(ISNULL(CT.FinalGPA_Calculations, 0)) AS GradePoints
                    FROM       #GPASummary AS GS
                    INNER JOIN #CoursesTaken AS CT ON CT.StuEnrollId = GS.StuEnrollId
                                                      AND CT.TermId = GS.TermId
                    WHERE      CT.RowNumberMultEnrollNoRetaken = 1
                               AND EXISTS (
                                          SELECT Val
                                          FROM   dbo.MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                          WHERE  CONVERT(UNIQUEIDENTIFIER, Val) = CT.StuEnrollId
                                          )
                               AND (
                                   @TermId IS NULL
                                   OR GS.TermId = @TermId
                                   )
                    GROUP BY   GS.StudentId;
                END;
        END;
        -- END  --  2) Selet Totals  

        -- 3) drop temp tables  
        BEGIN
            -- Drop temp tables  
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#CoursesTaken'')
                      )
                BEGIN
                    DROP TABLE #CoursesTaken;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   tempdb.sys.objects AS O
                      WHERE  O.type IN ( ''U'' )
                             AND O.object_id = OBJECT_ID(N''tempdb..#GPASummary'')
                      )
                BEGIN
                    DROP TABLE #GPASummary;
                END;
        END;
    -- END  --  3) drop temp tables  
    END;
-- =========================================================================================================  
-- END  --  USP_TR_Sub07_TotalCourses   
-- =========================================================================================================  



'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
-- This statement writes to the SQL Server Log so SQL Monitor can show this deployment.
IF HAS_PERMS_BY_NAME(N'sys.xp_logevent', N'OBJECT', N'EXECUTE') = 1
BEGIN
    DECLARE @databaseName AS nvarchar(2048), @eventMessage AS nvarchar(2048)
    SET @databaseName = REPLACE(REPLACE(DB_NAME(), N'\', N'\\'), N'"', N'\"')
    SET @eventMessage = N'Redgate SQL Compare: { "deployment": { "description": "Redgate SQL Compare deployed to ' + @databaseName + N'", "database": "' + @databaseName + N'" }}'
    EXECUTE sys.xp_logevent 55000, @eventMessage
END
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END