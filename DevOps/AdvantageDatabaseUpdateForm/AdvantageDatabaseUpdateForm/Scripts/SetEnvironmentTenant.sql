﻿-- Set Environment record if not exists
IF NOT EXISTS (SELECT * FROM dbo.Environment WHERE EnvironmentName = @EnviromentName)
BEGIN

INSERT INTO dbo.Environment
        (
         EnvironmentName
        ,VersionNumber
        ,ApplicationURL
        )
VALUES  (
         @EnviromentName  -- EnvironmentName - varchar(50)
        ,'Current'  -- VersionNumber - varchar(50)
        ,'~/../../current/Site/dash.aspx?RESID=264&desc=dashboard&userid={user-id}&tenantid={tenant-id}'  -- ApplicationURL - varchar(1000)
        )
END

