﻿DECLARE @UserId VARCHAR(38) = (
                        SELECT  UserId
                        FROM    dbo.aspnet_Users
                        WHERE   LoweredUserName = 'support@fameinc.com'
                      );

IF NOT EXISTS ( SELECT  *
                FROM    dbo.TenantUsers
                WHERE   TenantId = @TenantId )
    BEGIN

        INSERT  INTO dbo.TenantUsers
                (
                 TenantId
                ,UserId
                ,IsDefaultTenant
                ,IsSupportUser
                )
        VALUES  (
                 @TenantId  -- TenantId - int
                ,@UserId  -- UserId - uniqueidentifier
                ,@IsDefaultTenant  -- IsDefaultTenant - bit
                ,@IsSupportUser  -- IsSupportUser - bit
                );
    END;