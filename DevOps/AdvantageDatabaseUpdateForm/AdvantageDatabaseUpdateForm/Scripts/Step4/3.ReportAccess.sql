
BEGIN TRANSACTION ReportAccess;
BEGIN TRY
    DECLARE @resId INT;
    SET @resId = (
                   SELECT   ResourceID
                   FROM     syResources
                   WHERE    Resource = 'Reports'
                 );
    DECLARE Role_cursor CURSOR
    FOR
        SELECT  RoleId
        FROM    syRoles
        WHERE   SysRoleId IN ( 8,3,1 );
	
    DECLARE @roleId UNIQUEIDENTIFIER;
    OPEN Role_cursor;
    FETCH NEXT FROM Role_cursor INTO @roleId;
    WHILE @@FETCH_STATUS = 0
        BEGIN
            IF NOT EXISTS ( SELECT  *
                            FROM    syRlsResLvls
                            WHERE   RoleId = @roleId
                                    AND ResourceID = @resId )
                BEGIN	
                    INSERT  INTO syRlsResLvls
                            (
                             RoleId
                            ,ResourceID
                            ,AccessLevel
                            ,ModDate
                            ,ModUser
                            ,ParentId
                            )
                    VALUES  (
                             @roleId
                            ,@resId
                            ,15
                            ,GETDATE()
                            ,'SUPPORT'
                            ,NULL
                            );
                END;	
            FETCH NEXT FROM Role_cursor INTO @roleId;		
        END;
    CLOSE Role_cursor;
    DEALLOCATE Role_cursor;
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION ReportAccess;
END CATCH;
COMMIT TRANSACTION ReportAccess;