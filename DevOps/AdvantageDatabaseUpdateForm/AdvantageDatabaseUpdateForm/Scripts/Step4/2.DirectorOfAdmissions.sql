UPDATE  syResources
SET     ResourceTypeID = 3
       ,UsedIn = 975
WHERE   Resource = 'LeadAssignment';                    
GO
IF EXISTS ( SELECT  *
            FROM    syNavigationNodes
            WHERE   ResourceId = (
                                   SELECT   ResourceID
                                   FROM     syResources
                                   WHERE    Resource = 'Assign Leads to Campus'
                                 ) )
    BEGIN
        UPDATE  syNavigationNodes
        SET     ResourceId = (
                               SELECT   ResourceID
                               FROM     syResources
                               WHERE    Resource = 'Lead Queue'
                             )
        WHERE   syNavigationNodes.ResourceId = (
                                                 SELECT ResourceID
                                                 FROM   syResources
                                                 WHERE  Resource = 'Assign Leads to Campus'
                                               );
    END;
GO	
IF EXISTS ( SELECT  *
            FROM    syNavigationNodes
            WHERE   ResourceId = (
                                   SELECT   ResourceID
                                   FROM     syResources
                                   WHERE    Resource = 'Assign Leads to Admissions Rep'
                                 ) )
    BEGIN
        UPDATE  syNavigationNodes
        SET     ResourceId = (
                               SELECT   ResourceID
                               FROM     syResources
                               WHERE    Resource = 'LeadAssignment'
                             )
        WHERE   syNavigationNodes.ResourceId = (
                                                 SELECT ResourceID
                                                 FROM   syResources
                                                 WHERE  Resource = 'Assign Leads to Admissions Rep'
                                               );
    END;	
GO	
DECLARE @roleId UNIQUEIDENTIFIER;
SET @roleId = (
                SELECT TOP 1
                        RoleId
                FROM    syRoles
                WHERE   SysRoleId = 8
                ORDER BY ModDate ASC
              );

DECLARE @resId INT;
SET @resId = (
               SELECT   ResourceID
               FROM     syResources
               WHERE    Resource = 'Reports'
             );
IF NOT EXISTS ( SELECT  *
                FROM    syRlsResLvls
                WHERE   RoleId = @roleId
                        AND ResourceID = @resId )
    BEGIN	
        INSERT  INTO syRlsResLvls
                (
                 RoleId
                ,ResourceID
                ,AccessLevel
                ,ModDate
                ,ModUser
                ,ParentId
                )
        VALUES  (
                 @roleId
                ,@resId
                ,15
                ,GETDATE()
                ,'SUPPORT'
                ,NULL
                );
    END;	
GO