IF NOT EXISTS ( SELECT  *
                FROM    syResources
                WHERE   ResourceID = 576 )
    BEGIN	
        INSERT  INTO dbo.syResources
                (
                 ResourceID
                ,Resource
                ,ResourceTypeID
                ,ResourceURL
                ,SummListId
                ,ChildTypeId
                ,ModDate
                ,ModUser
                ,AllowSchlReqFlds
                ,MRUTypeId
                ,UsedIn
                ,TblFldsId
                ,DisplayName
                )
        VALUES  (
                 576
                ,'Import Leads'
                ,3
                ,'~/AD/ImportLeads_NEW.aspx'
                ,NULL
                ,NULL
                ,GETDATE()
                ,'Support'
                ,0
                ,4
                ,975
                ,NULL
                ,NULL
                );

        UPDATE  syRlsResLvls
        SET     ResourceID = 576
        WHERE   ResourceID = 498;
        UPDATE  syNavigationNodes
        SET     ResourceId = 576
        WHERE   ResourceId = 498;
        DELETE  FROM syResources
        WHERE   ResourceID = 498;
    END;	
GO