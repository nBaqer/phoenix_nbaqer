﻿-- *********************************************************************
-- Resource PELL Correction for school in 3.8 or higher 
-- 6.PellCorrectionForVersion38
-- This resource update db to 3.8.0.35
--  JAGG
-- *********************************************************************
-- If does not exists resource 840 proceed

DECLARE @KeyPell INT = 837;

-- Create 840 PELL
BEGIN TRANSACTION Create840Pell;
DECLARE @ResourceId INT = 837;
BEGIN TRY
    BEGIN
        DECLARE @ResId INT = 837
           ,@MenuItemId INT
           ,@ParentId INT; 
	
        IF NOT EXISTS ( SELECT  *
                        FROM    syResources
                        WHERE   ResourceId = @ResourceId )
            BEGIN
                INSERT  INTO syResources
                VALUES  ( @ResId,'Pell Recipients & Stafford Sub Recipients without Pell',5,'~/sy/ParamReport.aspx',NULL,5,GETDATE(),'sa',0,1,975,NULL,NULL );
            END;
				
        IF NOT EXISTS ( SELECT  *
                        FROM    syMenuItems
                        WHERE   ResourceId = @ResourceId )
            BEGIN
                SET @ParentId = (
                                  SELECT    MenuItemId
                                  FROM      syMenuItems
                                  WHERE     MenuName = 'Winter - Admissions'
                                );
                SET @MenuItemId = (
                                    SELECT  MAX(MenuItemId) + 1
                                    FROM    dbo.syMenuItems
                                  );
                INSERT  INTO dbo.syMenuItems
                        (
                         MenuName
                        ,DisplayName
                        ,Url
                        ,MenuItemTypeId
                        ,ParentId
                        ,DisplayOrder
                        ,IsPopup
                        ,ModDate
                        ,ModUser
                        ,IsActive
                        ,ResourceId
                        ,HierarchyId
                        ,ModuleCode
                        ,MRUType
                        ,HideStatusBar
		                )
                VALUES  (
                         'Pell Recipients & Stafford Sub Recipients without Pell'  -- MenuName - varchar(250)
                        ,'Pell Recipients & Stafford Sub Recipients without Pell'  -- DisplayName - varchar(250)
                        ,N'/sy/ParamReport.aspx'  -- Url - nvarchar(250)
                        ,4  -- MenuItemTypeId - smallint
                        ,@ParentId  -- ParentId - int
                        ,500  -- DisplayOrder - int
                        ,0  -- IsPopup - bit
                        ,GETDATE()  -- ModDate - datetime
                        ,'sa'  -- ModUser - varchar(50)
                        ,1  -- IsActive - bit
                        ,@ResId  -- ResourceId - smallint
                        ,NULL  -- HierarchyId - uniqueidentifier
                        ,NULL  -- ModuleCode - varchar(5)
                        ,0  -- MRUType - int
                        ,NULL  -- HideStatusBar - bit
		                );
            END;
    END;

    BEGIN
        DECLARE @ParamSetId INT;
        DECLARE @SetId INT;
        IF NOT EXISTS ( SELECT  *
                        FROM    ParamSet
                        WHERE   SetDisplayName = 'PellRecipientsandStaffordReportSet' )
            BEGIN
                INSERT  INTO dbo.ParamSet
                        (
                         SetName
                        ,SetDisplayName
                        ,SetDescription
                        ,SetType
		                )
                VALUES  (
                         N'PellRecipientsandStaffordReportCustomOptionsSet'  -- SetName - nvarchar(50)
                        ,N'PellRecipientsandStaffordReportSet'  -- SetDisplayName - nvarchar(50)
                        ,N'Custom Options for Pell Recipients & Stafford Sub Recipients without Pell'  -- SetDescription - nvarchar(1000)
                        ,N'Custom'  -- SetType - nvarchar(50)
		                );
            END;
        SET @SetId = (
                       SELECT TOP 1
                                SetId
                       FROM     ParamSet
                       WHERE    SetDisplayName = 'PellRecipientsandStaffordReportSet'
                     );
        IF NOT EXISTS ( SELECT  *
                        FROM    ParamSection
                        WHERE   SectionName = 'ParamsForPellRecipientsandStaffordReport' )
            BEGIN

                INSERT  INTO dbo.ParamSection
                        (
                         SectionName
                        ,SectionCaption
                        ,SetId
                        ,SectionSeq
                        ,SectionDescription
                        ,SectionType
		                )
                VALUES  (
                         N'ParamsForPellRecipientsandStaffordReport'  -- SectionName - nvarchar(50)
                        ,N'Pell Recipients & Stafford Sub Recipients without Pell Report Parameters'  -- SectionCaption - nvarchar(100)
                        ,@SetId  -- SetId - bigint
                        ,1  -- SectionSeq - int
                        ,N'Choose Pell Recipients & Stafford Sub Recipients without Pell Report Parameters'  -- SectionDescription - nvarchar(500)
                        ,0  -- SectionType - int
		                );
            END;
        IF NOT EXISTS ( SELECT  *
                        FROM    dbo.ParamItem
                        WHERE   Caption = 'PellRecipientsandStaffordReportOptions' )
            BEGIN
                INSERT  INTO dbo.ParamItem
                        (
                         Caption
                        ,ControllerClass
                        ,valueprop
                        ,ReturnValueName
                        ,IsItemOverriden
		                )
                VALUES  (
                         N'PellRecipientsandStaffordReportOptions'
                        ,N'ParamIPEDS.ascx'
                        ,0
                        ,'PellRecipientsandStaffordReportOptions'
                        ,NULL
		                );
            END;
        DECLARE @ItemId INT;
        SET @ItemId = (
                        SELECT  ItemId
                        FROM    ParamItem
                        WHERE   Caption = 'PellRecipientsandStaffordReportOptions'
                      );
        IF NOT EXISTS ( SELECT  *
                        FROM    ParamItemProp
                        WHERE   itemid = @ItemId )
            BEGIN
                INSERT  INTO dbo.ParamItemProp
                        (
                         itemid
                        ,childcontrol
                        ,propname
                        ,value
                        ,valuetype
		                )
                VALUES  (
                         @ItemId  -- itemid - bigint
                        ,NULL  -- childcontrol - nvarchar(100)
                        ,N'IPEDSControlType'  -- propname - nvarchar(100)
                        ,21  -- value - nvarchar(max)
                        ,N'Enum'  -- valuetype - nvarchar(25)
		                );
            END;
        DECLARE @SectionId INT
           ,@DetailId INT;
        SET @SectionId = (
                           SELECT TOP 1
                                    SectionId
                           FROM     ParamSection
                           WHERE    SectionName = 'ParamsForPellRecipientsandStaffordReport'
                         );
        SET @ItemId = (
                        SELECT TOP 1
                                ItemId
                        FROM    dbo.ParamItem
                        WHERE   Caption = 'PellRecipientsandStaffordReportOptions'
                      );
        IF NOT EXISTS ( SELECT  *
                        FROM    ParamDetail
                        WHERE   ItemId = @ItemId
                                AND SectionId = @SectionId )
            BEGIN
                INSERT  INTO dbo.ParamDetail
                        (
                         SectionId
                        ,ItemId
                        ,CaptionOverride
                        ,ItemSeq
		                )
                VALUES  (
                         @SectionId  -- SectionId - bigint
                        ,@ItemId  -- ItemId - bigint
                        ,N'Report Options'  -- CaptionOverride - nvarchar(50)
                        ,1  -- ItemSeq - int
		                );
            END;
        SET @DetailId = (
                          SELECT TOP 1
                                    DetailId
                          FROM      ParamDetail
                          WHERE     SectionId = @SectionId
                                    AND ItemId = @ItemId
                        );

        IF NOT EXISTS ( SELECT  *
                        FROM    dbo.ParamDetailProp
                        WHERE   detailid = @DetailId
                                AND propname = 'IPEDSControlType' )
            BEGIN
                INSERT  INTO dbo.ParamDetailProp
                        (
                         detailid
                        ,childcontrol
                        ,propname
                        ,value
                        ,valuetype
		                )
                VALUES  (
                         @DetailId  -- detailid - bigint
                        ,NULL  -- childcontrol - nvarchar(100)
                        ,N'IPEDSControlType'  -- propname - nvarchar(100)
                        ,21  -- value - nvarchar(max)
                        ,N'Enum'  -- valuetype - nvarchar(25)
		                );
            END;
        IF NOT EXISTS ( SELECT  *
                        FROM    dbo.ParamDetailProp
                        WHERE   detailid = @DetailId
                                AND propname = 'SelectedDatePartProgram' )
            BEGIN
                INSERT  INTO dbo.ParamDetailProp
                        (
                         detailid
                        ,childcontrol
                        ,propname
                        ,value
                        ,valuetype
		                )
                VALUES  (
                         @DetailId  -- detailid - bigint
                        ,NULL  -- childcontrol - nvarchar(100)
                        ,N'SelectedDatePartProgram'  -- propname - nvarchar(100)
                        ,'10/31/'  -- value - nvarchar(max)
                        ,N'String'  -- valuetype - nvarchar(25)
		                );
            END;
  
    --SET @ResourceId = (
    --                    SELECT  ResourceID
    --                    FROM    syResources
    --                    WHERE   Resource = 'Pell Recipients & Stafford Sub Recipients without Pell'
    --                  );
        IF NOT EXISTS ( SELECT  *
                        FROM    syReports
                        WHERE   ResourceId = @ResourceId )
            BEGIN
                INSERT  INTO dbo.syReports
                        (
                         ReportId
                        ,ResourceId
                        ,ReportName
                        ,ReportDescription
                        ,RDLName
                        ,AssemblyFilePath
                        ,ReportClass
                        ,CreationMethod
                        ,WebServiceUrl
                        ,RecordLimit
                        ,AllowedExportTypes
                        ,ReportTabLayout
                        ,ShowPerformanceMsg
                        ,ShowFilterMode
		                )
                VALUES  (
                         NEWID()  -- ReportId - uniqueidentifier
                        ,@ResourceId  -- ResourceId - int
                        ,'PellRecipientsandStaffordDetailReport'  -- ReportName - varchar(50)
                        ,'Pell Recipients & Stafford Sub Recipients without Pell Detail Report'  -- ReportDescription - varchar(500)
                        ,'IPEDS/WINTER/Spring_GradRates_PellRecipients_DetailAndSummary'  -- RDLName - varchar(200)
                        ,'~/Bin/Reporting.dll'  -- AssemblyFilePath - varchar(200)
                        ,'FAME.Advantage.Reporting.Logic.IPEDSStudentsMissingData'  -- ReportClass - varchar(200)
                        ,'BuildReport'  -- CreationMethod - varchar(200)
                        ,'futurefield'  -- WebServiceUrl - varchar(200)
                        ,400  -- RecordLimit - bigint
                        ,0  -- AllowedExportTypes - int
                        ,1  -- ReportTabLayout - int
                        ,0  -- ShowPerformanceMsg - bit
                        ,0  -- ShowFilterMode - bit
		                );
            END;
        DECLARE @ReportId UNIQUEIDENTIFIER;
        SET @ReportId = (
                          SELECT    ReportId
                          FROM      syReports
                          WHERE     ReportName = 'PellRecipientsandStaffordDetailReport'
                        );
        IF NOT EXISTS ( SELECT  *
                        FROM    syReportTabs
                        WHERE   ReportId = @ReportId )
            BEGIN
                INSERT  INTO syReportTabs
                VALUES  ( @ReportId,'RadRptPage_Options','ParamPanelBarSetCustomOptions',@SetId,'ParamSetPanelBarControl.ascx',
                          'Custom Options Set for PellRecipientsandStaffordReport','PellRecipientsandStaffordDetailReportCustomOptionsSet','Options' );
		
            END;
    END;

    UPDATE  syReports
    SET     ReportClass = 'FAME.Advantage.Reporting.Logic.IPEDSStudentsMissingData'
           ,ReportDescription = 'Pell Recipients & Stafford Sub Recipients without Pell Detail Report'
    WHERE   ResourceId = @ResourceId;
    COMMIT TRANSACTION Create840Pell;
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION Create840Pell;
    DECLARE @ErrorMessage NVARCHAR(MAX)
       ,@ErrorSeverity INT
       ,@ErrorState INT
       ,@LastLine INT;

    SELECT  @ErrorMessage = ERROR_MESSAGE() + ' Line ' + CAST(ERROR_LINE() AS NVARCHAR(5))
           ,@ErrorSeverity = ERROR_SEVERITY()
           ,@ErrorState = ERROR_STATE()
           ,@LastLine = ERROR_LINE();
    IF ( @@TRANCOUNT > 0 )
        BEGIN
            ROLLBACK TRANSACTION UpdateMenu;
        END;
    RAISERROR (@ErrorMessage, @ErrorSeverity, @ErrorState);
END CATCH;
   
GO

 -- *********************************************************************
-- Resource PELL Correction for school in 3.8 or higher 
-- This resource update db to 3.8.0.35
--  END JAGG
-- *********************************************************************
