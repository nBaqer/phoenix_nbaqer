-- ===============================================================================================
-- Consolidated Script Version 3.12
-- Data Changes Zone
-- Please do not deploy your schema changes in this area
-- Please use SQL Prompt format before insert here please
-- Please run after Schema changes....
-- ===============================================================================================
--=================================================================================================
-- START AD-4468 Create Title IV SAP (FA SAP) statuses
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION titleIvSap;
BEGIN TRY
    DECLARE @CreatedById UNIQUEIDENTIFIER;
    DECLARE @CreatedDate DATETIME;
    DECLARE @StatusId UNIQUEIDENTIFIER;

    SET @CreatedById =
    (
        SELECT TOP 1
               UserId
        FROM dbo.syUsers
        WHERE IsAdvantageSuperUser = 1
              OR UserName = 'support'
        ORDER BY FullName ASC
    );
    SET @CreatedDate = GETDATE();
    SET @StatusId =
    (
        SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
    );

    IF NOT EXISTS (SELECT * FROM syTitleIVSapStatus WHERE Code = 'Passed')
    BEGIN
        INSERT INTO syTitleIVSapStatus
        VALUES
        ('Passed', 'Title IV Passed', @CreatedById, @CreatedDate, NULL, NULL, @StatusId, NULL);
    END;

    IF NOT EXISTS (SELECT * FROM syTitleIVSapStatus WHERE Code = 'Warning')
    BEGIN
        INSERT INTO syTitleIVSapStatus
        VALUES
        ('Warning', 'Title IV Warning', @CreatedById, @CreatedDate, NULL, NULL, @StatusId, NULL);
    END;

    IF NOT EXISTS (SELECT * FROM syTitleIVSapStatus WHERE Code = 'Ineligible')
    BEGIN
        INSERT INTO syTitleIVSapStatus
        VALUES
        ('Ineligible', 'Title IV Ineligible', @CreatedById, @CreatedDate, NULL, NULL, @StatusId, NULL);
    END;

    IF NOT EXISTS (SELECT * FROM syTitleIVSapStatus WHERE Code = 'Probation')
    BEGIN
        INSERT INTO syTitleIVSapStatus
        VALUES
        ('Probation', 'Title IV Probation', @CreatedById, @CreatedDate, NULL, NULL, @StatusId, NULL);
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    --ROLLBACK TRANSACTION titleIvSap;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION titleIvSap;
    PRINT 'Failed to Insert Create Title IV SAP (FA SAP) statuses';

END;
ELSE
BEGIN
    COMMIT TRANSACTION titleIvSap;
    PRINT 'Inserted Title IV SAP (FA SAP) statuses';
END;
GO

--=================================================================================================
-- END -- AD-4468 Create Title IV SAP (FA SAP) statuses
--=================================================================================================
--=================================================================================================
--START
-- AD-679--Undo Student Termination
--INSERT a record to syResources for the Undo Student Termination Url
--INSERT a record to syMenuItems for the Undo Student Termination Url
--START
--=================================================================================================
DECLARE @ResourceURL VARCHAR(100) = '~/AR/UndoStudentTermination.aspx';
DECLARE @DisplayName VARCHAR(200) = 'Undo Student Termination';
DECLARE @MaxID INT;
DECLARE @MaxResourceID INT;
DECLARE @TerminateStudentRoleID UNIQUEIDENTIFIER;
DECLARE @TerminateStudentResourceId INT;
DECLARE @error INT = 0;
DECLARE @ActiveStatusId UNIQUEIDENTIFIER;
BEGIN TRANSACTION undoTermination;

BEGIN TRY
    SET @ActiveStatusId =
    (
        SELECT TOP (1)
               StatusId
        FROM dbo.syStatuses
        WHERE StatusCode = 'A'
        ORDER BY StatusCode
    );

    IF NOT EXISTS
    (
        SELECT TOP (1)
               RoleId,
               *
        FROM dbo.syRoles
        WHERE SysRoleId = 18
              AND StatusId = @ActiveStatusId
    )
    BEGIN
        INSERT INTO dbo.syRoles
        (
            RoleId,
            Role,
            Code,
            StatusId,
            SysRoleId,
            ModDate,
            ModUser
        )
        VALUES
        (NEWID(), 'Student Termination', 'Termination', @ActiveStatusId, 18, GETDATE(), 'support');
    END;
    ELSE
    BEGIN
        UPDATE syRoles
        SET Role = 'Student Termination',
            Code = 'Termination'
        WHERE Role IN ( 'Termination', 'StueTErm', 'Student Term' )
              OR Code IN ( 'Termiantion', 'StueTerm', 'Student Term' );
    END;

    SET @TerminateStudentRoleID =
    (
        SELECT RoleId FROM syRoles WHERE Role = 'Student Termination'
    );

    SELECT @MaxID = 876;
    IF NOT EXISTS (SELECT * FROM syResources WHERE ResourceURL = @ResourceURL)
    BEGIN
        INSERT INTO dbo.syResources
        (
            ResourceID,
            Resource,
            ResourceTypeID,
            ResourceURL,
            SummListId,
            ChildTypeId,
            ModDate,
            ModUser,
            AllowSchlReqFlds,
            MRUTypeId,
            UsedIn,
            TblFldsId,
            DisplayName
        )
        VALUES
        (   @MaxID,       -- ResourceID - smallint
            @DisplayName, -- Resource - varchar(200)
            3,            -- ResourceTypeID - tinyint
            @ResourceURL, -- ResourceURL - varchar(100)
            0,            -- SummListId - smallint
            0,            -- ChildTypeId - tinyint
            GETDATE(),    -- ModDate - datetime
            'Support',    -- ModUser - varchar(50)
            NULL,         -- AllowSchlReqFlds - bit
            0,            -- MRUTypeId - smallint
            0,            -- UsedIn - int
            0,            -- TblFldsId - int
            @DisplayName  -- DisplayName - varchar(200)
            );
    END;
    ELSE
    BEGIN
        DECLARE @prevResourceId INT;
        SET @prevResourceId =
        (
            SELECT ResourceID
            FROM syResources
            WHERE ResourceURL = '~/AR/UndoStudentTermination.aspx'
        );
        IF EXISTS (SELECT 1 FROM syRlsResLvls WHERE ResourceID = @prevResourceId)
        BEGIN
            DELETE FROM syRlsResLvls
            WHERE ResourceID = @prevResourceId;
            DELETE FROM syNavigationNodes
            WHERE ResourceId = @prevResourceId;
            UPDATE syResources
            SET ResourceID = 876
            WHERE ResourceURL = '~/AR/UndoStudentTermination.aspx';
        END;
    END;
    IF @@ERROR > 0
    BEGIN
        SET @error = 1;
    END;
    SELECT @MaxResourceID = ResourceID
    FROM syResources
    WHERE ResourceURL = @ResourceURL;

    IF NOT EXISTS
    (
        SELECT TOP (1)
               *
        FROM dbo.syRlsResLvls
        WHERE RoleId = @TerminateStudentRoleID
              AND ResourceID = @MaxResourceID
              AND AccessLevel = 15
    )
    BEGIN

        INSERT INTO dbo.syRlsResLvls
        (
            RRLId,
            RoleId,
            ResourceID,
            AccessLevel,
            ModDate,
            ModUser,
            ParentId
        )
        VALUES
        (   NEWID(),                 -- RRLId - uniqueidentifier
            @TerminateStudentRoleID, -- RoleId - uniqueidentifier
            @MaxResourceID,          -- ResourceID - smallint
            15,                      -- AccessLevel - smallint
            GETDATE(),               -- ModDate - datetime
            'Support',               -- ModUser - varchar(50)
            NULL                     -- ParentId - int
            );
    END;
    IF @@ERROR > 0
    BEGIN
        SET @error = 1;
    END;

END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;

    SET @error = 1;
END CATCH;

IF @error > 0
BEGIN
    ROLLBACK TRANSACTION undoTermination;
    PRINT 'Failed to undoTermination page to syresouce and menuItems.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION undoTermination;
    PRINT 'Successfully added undoTermination Page to syresource and MenuItems.';
END;
GO

-------------------
DECLARE @error AS INT;
SET @error = 0;
DECLARE @hierarchyId UNIQUEIDENTIFIER;
DECLARE @parentId UNIQUEIDENTIFIER;
DECLARE @parentMenuId INT;
DECLARE @modDate DATETIME;
DECLARE @ResourceId INT;
DECLARE @ResourceURL VARCHAR(100) = '~/AR/UndoStudentTermination.aspx',
        @MenuItemTypeId INT = 4;

SELECT @ResourceId = ResourceID
FROM syResources
WHERE ResourceURL = @ResourceURL;


BEGIN TRANSACTION UndoMenuItem;

BEGIN TRY

    SET @modDate = GETDATE();
    IF NOT EXISTS
    (
        SELECT 1
        FROM syMenuItems
        WHERE MenuItemTypeId = @MenuItemTypeId
              AND MenuName = 'Undo Student Termination'
    )
    BEGIN
        SET @parentMenuId =
        (
            SELECT TOP 1 MenuItemId FROM syMenuItems WHERE ResourceId = 693
        );
        SET @hierarchyId = NEWID();

        INSERT INTO syMenuItems
        (
            MenuName,
            DisplayName,
            Url,
            MenuItemTypeId,
            ParentId,
            DisplayOrder,
            IsPopup,
            ModDate,
            ModUser,
            IsActive,
            ResourceId,
            HierarchyId,
            ModuleCode,
            MRUType,
            HideStatusBar
        )
        VALUES
        (   'Undo Student Termination',         -- MenuName - varchar(250)
            'Undo Student Termination',         -- DisplayName - varchar(250)
            N'/AR/UndoStudentTermination.aspx', -- Url - nvarchar(250)
            @MenuItemTypeId,                    -- MenuItemTypeId - smallint
            @parentMenuId,                      -- ParentId - int
            300,                                -- DisplayOrder - int
            0,                                  -- IsPopup - bit
            @modDate,                           -- ModDate - datetime
            'support',                          -- ModUser - varchar(50)
            1,                                  -- IsActive - bit
            @ResourceId,                        -- ResourceId - smallint
            @hierarchyId,                       -- HierarchyId - uniqueidentifier
            NULL,                               -- ModuleCode - varchar(5)
            NULL,                               -- MRUType - int
            NULL                                -- HideStatusBar - bit
            );
    END;
    ELSE
    BEGIN
        SET @parentMenuId =
        (
            SELECT TOP 1 MenuItemId FROM syMenuItems WHERE ResourceId = 693
        );

        UPDATE syMenuItems
        SET ResourceId = 876,
            ParentId = @parentMenuId
        WHERE MenuName = 'Undo Student Termination';

    END;
    IF @@ERROR > 0
    BEGIN
        SET @error = 1;
    END;
    IF NOT EXISTS (SELECT 1 FROM syNavigationNodes WHERE ResourceId = 876)
    BEGIN
        DECLARE @parentHeirachyId UNIQUEIDENTIFIER;
        DECLARE @resourceHeirarchyId UNIQUEIDENTIFIER;
        SET @parentHeirachyId =
        (
            SELECT ParentId FROM syNavigationNodes WHERE ResourceId = 317
        );
        SET @resourceHeirarchyId =
        (
            SELECT HierarchyId FROM syMenuItems WHERE ResourceId = 876
        );
        INSERT INTO syNavigationNodes
        (
            HierarchyId,
            HierarchyIndex,
            ResourceId,
            ParentId,
            ModUser,
            ModDate,
            IsPopupWindow,
            IsShipped
        )
        VALUES
        (   @resourceHeirarchyId, -- HierarchyId - uniqueidentifier
            3,                    -- HierarchyIndex - smallint
            876,                  -- ResourceId - smallint
            @parentHeirachyId,    -- ParentId - uniqueidentifier
            'Support',            -- ModUser - varchar(50)
            GETDATE(),            -- ModDate - datetime
            0,                    -- IsPopupWindow - bit
            1                     -- IsShipped - bit
            );
    END;
    IF @@ERROR > 0
    BEGIN
        SET @error = 1;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;


END CATCH;

IF @error > 0
BEGIN
    ROLLBACK TRANSACTION UndoMenuItem;
END;
ELSE
BEGIN
    COMMIT TRANSACTION UndoMenuItem;
END;
GO

--=================================================================================================
--END
-- AD-679--Undo Student Termination
--INSERT a record to syResources for the Undo Student Termination Url
--INSERT a record to syMenuItems for the Undo Student Termination Url
-- AD-679--Undo Student Termination
--Updating where condition to syNavigationNodes for the Undo Student Termination 
--END
--=================================================================================================

--=================================================================================================
--START
-- AD-900--Ability to view R2T4 and Termination details document for a student on Student Docs page
--INSERT records to syConfigAppSettingsfor the document management related to R2T4
--INSERT records to syConfigAppSetValuesthe document management related to R2T4
--INSERT records to adReqs for the document management related to R2T4
--INSERT records to adReqsEffectiveDates for the document management related to R2T4
--START
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;


BEGIN TRANSACTION configSetR2T4;

IF NOT EXISTS (SELECT * FROM syConfigAppSettings WHERE KeyName = 'R2T4')
BEGIN TRY
    INSERT INTO syConfigAppSettings
    (
        KeyName,
        Description,
        ModUser,
        ModDate,
        CampusSpecific,
        ExtraConfirmation
    )
    VALUES
    ('R2T4', 'Value of server path for R2T4 related student docs', 'sa', GETDATE(), 0, 0);


    INSERT INTO syConfigAppSetValues
    (
        ValueId,
        SettingId,
        Value,
        ModUser,
        ModDate,
        Active
    )
    VALUES
    (   NEWID(),
        (
            SELECT SettingId FROM dbo.syConfigAppSettings WHERE KeyName = 'R2T4'
        ), 'C:\AdvDocuments\R2T4\', 'sa', GETDATE(), 1);

    IF @@ERROR > 0
    BEGIN
        SET @error = 1;
    END;

END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;
    SET @error = 1;

END CATCH;

IF @error > 0
BEGIN
    ROLLBACK TRANSACTION configSetR2T4;
END;
ELSE
BEGIN
    COMMIT TRANSACTION configSetR2T4;
END;

GO
DECLARE @error INT = 0;
BEGIN TRANSACTION configSetTermination;

IF NOT EXISTS
(
    SELECT *
    FROM syConfigAppSettings
    WHERE KeyName = 'Termination details'
)
BEGIN TRY
    INSERT INTO syConfigAppSettings
    (
        KeyName,
        Description,
        ModUser,
        ModDate,
        CampusSpecific,
        ExtraConfirmation
    )
    VALUES
    ('Termination details', 'Value of server path for termination details related student docs', 'sa', GETDATE(), 0, 0);


    INSERT INTO syConfigAppSetValues
    (
        ValueId,
        SettingId,
        Value,
        ModUser,
        ModDate,
        Active
    )
    VALUES
    (   NEWID(),
        (
            SELECT SettingId
            FROM dbo.syConfigAppSettings
            WHERE KeyName = 'Termination details'
        ), 'C:\AdvDocuments\Termination Details\', 'sa', GETDATE(), 1);


    IF @@ERROR > 0
    BEGIN
        SET @error = 1;
    END;
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;
    SET @error = 1;
END CATCH;

IF @error > 0
BEGIN
    ROLLBACK TRANSACTION configSetTermination;
END;
ELSE
BEGIN
    COMMIT TRANSACTION configSetTermination;
END;
GO

-------------------------------------------
DECLARE @error INT = 0;
BEGIN TRANSACTION AdreqTerminate;
IF NOT EXISTS (SELECT * FROM dbo.adReqs WHERE Code = 'Termination details')
BEGIN TRY
    INSERT INTO dbo.adReqs
    (
        adReqId,
        Code,
        Descrip,
        StatusId,
        CampGrpId,
        ModuleId,
        adReqTypeId,
        ModUser,
        ModDate,
        EdLvlId,
        ReqforEnrollment,
        ReqforFinancialAid,
        ReqforGraduation,
        IPEDSValue,
        RequiredForTermination
    )
    VALUES
    (   NEWID(),               -- adReqId - uniqueidentifier
        'Termination details', -- Code - varchar(50)
        'Termination details', -- Descrip - varchar(80)
        (
            SELECT StatusId FROM syStatuses WHERE Status = 'Active'
        ),                     -- StatusId - uniqueidentifier
        (
            SELECT CampGrpId FROM syCampGrps WHERE CampGrpCode = 'All'
        ),                     -- CampGrpId - uniqueidentifier
        (
            SELECT ModuleID FROM dbo.syModules WHERE ModuleCode = 'AR'
        ),                     -- ModuleId - int
        (
            SELECT adReqTypeId FROM adReqTypes WHERE Descrip = 'Document'
        ),                     -- adReqTypeId - int
        'SA',                  -- ModUser - varchar(80)
        GETDATE(),             -- ModDate - datetime
        NULL,                  -- EdLvlId - uniqueidentifier
        0,                     -- ReqforEnrollment - bit
        0,                     -- ReqforFinancialAid - bit
        0,                     -- ReqforGraduation - bit
        0,                     -- IPEDSValue - int
        1                      -- RequiredForTermination int
        );
    IF @@ERROR > 0
    BEGIN
        SET @error = 1;
    END;
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;

    SET @error = 1;
END CATCH;

IF @error > 0
BEGIN
    ROLLBACK TRANSACTION AdreqTerminate;
END;
ELSE
BEGIN
    COMMIT TRANSACTION AdreqTerminate;
END;



BEGIN TRANSACTION R2T4Req;

IF NOT EXISTS (SELECT * FROM dbo.adReqs WHERE Code = 'R2T4')
BEGIN TRY
    INSERT INTO dbo.adReqs
    (
        adReqId,
        Code,
        Descrip,
        StatusId,
        CampGrpId,
        ModuleId,
        adReqTypeId,
        ModUser,
        ModDate,
        EdLvlId,
        ReqforEnrollment,
        ReqforFinancialAid,
        ReqforGraduation,
        IPEDSValue,
        RequiredForTermination
    )
    VALUES
    (   NEWID(),   -- adReqId - uniqueidentifier
        'R2T4',    -- Code - varchar(50)
        'R2T4',    -- Descrip - varchar(80)
        (
            SELECT StatusId FROM syStatuses WHERE Status = 'Active'
        ),         -- StatusId - uniqueidentifier
        (
            SELECT CampGrpId FROM syCampGrps WHERE CampGrpCode = 'All'
        ),         -- CampGrpId - uniqueidentifier
        (
            SELECT ModuleID FROM dbo.syModules WHERE ModuleCode = 'AR'
        ),         -- ModuleId - int
        (
            SELECT adReqTypeId FROM adReqTypes WHERE Descrip = 'Document'
        ),         -- adReqTypeId - int
        'sa',      -- ModUser - varchar(80)
        GETDATE(), -- ModDate - datetime
        NULL,      -- EdLvlId - uniqueidentifier
        0,         -- ReqforEnrollment - bit
        0,         -- ReqforFinancialAid - bit
        0,         -- ReqforGraduation - bit
        0,         -- IPEDSValue - int
        1          -- RequiredForTermination int
        );

END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
    BEGIN
        ROLLBACK TRANSACTION R2T4Req;
        PRINT 'Exception Ocurred!';
    END;
END CATCH;

IF @@TRANCOUNT > 0
BEGIN
    COMMIT TRANSACTION R2T4Req;
END;




BEGIN TRANSACTION R2T4ReqEffDate;

IF NOT EXISTS
(
    SELECT *
    FROM dbo.adReqsEffectiveDates
    WHERE adReqId =
    (
        SELECT adReqId FROM adReqs WHERE Code = 'R2T4'
    )
)
BEGIN TRY
    INSERT INTO dbo.adReqsEffectiveDates
    (
        adReqEffectiveDateId,
        adReqId,
        StartDate,
        EndDate,
        MinScore,
        ModUser,
        ModDate,
        MandatoryRequirement,
        ValidDays
    )
    VALUES
    (   NEWID(),        -- adReqEffectiveDateId - uniqueidentifier
        (
            SELECT adReqId FROM adReqs WHERE Code = 'R2T4'
        ),              -- adReqId - uniqueidentifier
        01 / 01 / 1980, -- StartDate - datetime
        GETDATE(),      -- EndDate - datetime
        0.000,          -- MinScore - decimal(19, 3)
        'SA',           -- ModUser - varchar(50)
        GETDATE(),      -- ModDate - datetime
        0,              -- MandatoryRequirement - bit
        NULL            -- ValidDays - int
        );
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
    BEGIN
        ROLLBACK TRANSACTION R2T4ReqEffDate;
        PRINT 'Exception Ocurred!';
    END;
END CATCH;

IF @@TRANCOUNT > 0
BEGIN
    COMMIT TRANSACTION R2T4ReqEffDate;
END;



BEGIN TRANSACTION TerminationDetailsReq;

IF NOT EXISTS
(
    SELECT *
    FROM dbo.adReqsEffectiveDates
    WHERE adReqId =
    (
        SELECT adReqId FROM adReqs WHERE Code = 'Termination details'
    )
)
BEGIN TRY

    INSERT INTO dbo.adReqsEffectiveDates
    (
        adReqEffectiveDateId,
        adReqId,
        StartDate,
        EndDate,
        MinScore,
        ModUser,
        ModDate,
        MandatoryRequirement,
        ValidDays
    )
    VALUES
    (   NEWID(),        -- adReqEffectiveDateId - uniqueidentifier
        (
            SELECT adReqId FROM adReqs WHERE Code = 'Termination details'
        ),              -- adReqId - uniqueidentifier
        01 / 01 / 1980, -- StartDate - datetime
        GETDATE(),      -- EndDate - datetime
        0.000,          -- MinScore - decimal(19, 3)
        'SA',           -- ModUser - varchar(50)
        GETDATE(),      -- ModDate - datetime
        0,              -- MandatoryRequirement - bit
        NULL            -- ValidDays - int
        );

END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
    BEGIN
        ROLLBACK TRANSACTION TerminationDetailsReq;
        PRINT 'Exception Ocurred!';
    END;
END CATCH;

IF @@TRANCOUNT > 0
BEGIN
    COMMIT TRANSACTION TerminationDetailsReq;
END;
GO

--=================================================================================================
--END
-- AD-900--Ability to view R2T4 and Termination details document for a student on Student Docs page
--INSERT records to syConfigAppSettingsfor the document management related to R2T4
--INSERT records to syConfigAppSetValuesthe document management related to R2T4
--INSERT records to adReqs for the document management related to R2T4
--INSERT records to adReqsEffectiveDates for the document management related to R2T4
--END
--=================================================================================================						

--=================================================================================================
--START
-- AD-918--Ability to view R2T4 and Termination details document for a student on Document Management page
-- UPDATE syResources and syMenuItems - value for 'ResourceURL' column for 'Document Management' needs to be updated to the 'ResourceURL' value for 'Student Docs'
--START
--=================================================================================================

DECLARE @ResourceURL VARCHAR(100) = '~/PL/StudentDocumentManagement.aspx';
DECLARE @DisplayName VARCHAR(200) = 'Student Document Management';

BEGIN TRANSACTION DocuManagement;

BEGIN TRY

    BEGIN

        UPDATE dbo.syResources
        SET ResourceURL = @ResourceURL
        WHERE ResourceID IN
              (
                  SELECT ResourceID FROM syResources WHERE Resource = 'Student Docs'
              );

        UPDATE dbo.syResources
        SET ResourceURL = @ResourceURL
        WHERE ResourceID IN
              (
                  SELECT ResourceID FROM syResources WHERE Resource = 'Document Management'
              );

        UPDATE syMenuItems
        SET Url = '/PL/StudentDocumentManagement.aspx',
            IsPopup = 0
        WHERE MenuItemId IN
              (
                  SELECT MenuItemId FROM syMenuItems WHERE MenuName = 'Student Docs'
              );

        UPDATE syMenuItems
        SET Url = '/PL/StudentDocumentManagement.aspx',
            IsPopup = 0
        WHERE MenuItemId IN
              (
                  SELECT MenuItemId FROM syMenuItems WHERE MenuName = 'Document Management'
              );
    END;
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
    BEGIN
        ROLLBACK TRANSACTION DocuManagement;
        PRINT 'Exception Ocurred!';
    END;
END CATCH;

IF @@TRANCOUNT > 0
BEGIN
    COMMIT TRANSACTION DocuManagement;
END;

GO

--=================================================================================================
--END
-- AD-918--Ability to view R2T4 and Termination details document for a student on Document Management page
--UPDATE syResources - value for 'ResourceURL' column for 'Document Management' needs to be updated to the 'ResourceURL' value for 'Student Docs'
--END
--=================================================================================================
--========================================================================================================================
-- AD-1241-Insert a record to SyConfigSettings for adding 'FAMETitleIVService' key to Manage Configuration Settings page
--========================================================================================================================

DECLARE @KeyName VARCHAR(50) = 'FAMETitleIVService';
DECLARE @KeyDescription VARCHAR(300)
    = 'Values can be set to True or False. If value is set to True, it indicates FAME is the Title IV service provider for the school. If value is set to False, it indicates that FAME is not the Title IV service provider for the school.';

IF NOT EXISTS (SELECT * FROM syConfigAppSettings WHERE KeyName = @KeyName)
BEGIN TRY
    BEGIN
        BEGIN TRANSACTION FAMETitleIVService;

        DECLARE @MaxId INT;

        INSERT INTO dbo.syConfigAppSettings
        (
            KeyName,
            Description,
            ModUser,
            ModDate,
            CampusSpecific,
            ExtraConfirmation
        )
        VALUES
        (   @KeyName,        -- KeyName - varchar(200)
            @KeyDescription, -- Description - varchar(1000)
            'Support',       -- ModUser - varchar(50)
            GETDATE(),       -- ModDate - datetime
            1,               -- CampusSpecific - bit
            0                -- ExtraConfirmation - bit
            );

        SET @MaxId =
        (
            SELECT TOP (1)
                   SettingId
            FROM syConfigAppSettings
            WHERE KeyName = @KeyName
            ORDER BY KeyName
        );



        INSERT INTO dbo.syConfigAppSetValues
        (
            ValueId,
            SettingId,
            CampusId,
            Value,
            ModUser,
            ModDate,
            Active
        )
        VALUES
        (   NEWID(),   -- ValueId - uniqueidentifier
            @MaxId,    -- SettingId - int
            NULL,      -- CampusId - uniqueidentifier
            'False',   -- Value - varchar(1000)
            'Support', -- ModUser - varchar(50)
            GETDATE(), -- ModDate - datetime
            1          -- Active - bit
            );

        INSERT INTO dbo.syConfigAppSet_Lookup
        (
            LookUpId,
            SettingId,
            ValueOptions,
            ModUser,
            ModDate
        )
        VALUES
        (   NEWID(),   -- LookUpId - uniqueidentifier
            @MaxId,    -- SettingId - int
            'False',   -- ValueOptions - varchar(50)
            'Support', -- ModUser - varchar(50)
            GETDATE()  -- ModDate - datetime
            );

        INSERT INTO dbo.syConfigAppSet_Lookup
        (
            LookUpId,
            SettingId,
            ValueOptions,
            ModUser,
            ModDate
        )
        VALUES
        (   NEWID(),   -- LookUpId - uniqueidentifier
            @MaxId,    -- SettingId - int
            'True',    -- ValueOptions - varchar(50)
            'Support', -- ModUser - varchar(50)
            GETDATE()  -- ModDate - datetime
            );

    END;
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
    BEGIN
        ROLLBACK TRANSACTION FAMETitleIVService;
        PRINT 'Exception Ocurred!';
    END;
END CATCH;

IF @@TRANCOUNT > 0
BEGIN
    COMMIT TRANSACTION FAMETitleIVService;
END;
GO

--===============================================================================================================================
-- END --  AD-1241-Insert a record to SyConfigSettings for adding 'FAMETitleIVService' key to Manage Configuration Settings page
--===============================================================================================================================
--========================================================================================================================
--START
-- AD-7188:As per the mockup, Undo Termination menu option should be below Terminate Student
--START
--========================================================================================================================

BEGIN TRANSACTION UndoStudentTermination;

BEGIN TRY

    BEGIN

        UPDATE syMenuItems
        SET DisplayOrder = 600
        WHERE MenuName = 'Undo Student Termination';

        UPDATE syMenuItems
        SET DisplayOrder = 500
        WHERE MenuName = 'Terminate Student';


    END;
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
    BEGIN
        ROLLBACK TRANSACTION UndoStudentTermination;
        PRINT 'Exception Ocurred!';
    END;
END CATCH;

IF @@TRANCOUNT > 0
BEGIN
    COMMIT TRANSACTION UndoStudentTermination;
END;

GO


--===============================================================================================================================
-- END --
-- AD-7188:As per the mockup, Undo Termination menu option should be below Terminate Student
-- END
--===============================================================================================================================

--=================================================================================================
--START
-- AD-1487 Ability to select the period used for R2T4 calculation on Program Version screen
-- Inserting data into resource tables for R2T4 Calculation Period Types 
--START
--=================================================================================================
DECLARE @ResourceURL VARCHAR(100) = '~/AR/CalculationPeriodTypes.aspx';
DECLARE @ResourceID SMALLINT;
DECLARE @pk INT;
DECLARE @DDLId INT;
DECLARE @TblName VARCHAR(50) = N'arPrgVersions';
DECLARE @TableId INT;
DECLARE @DispFldId INT;
DECLARE @DispFldName VARCHAR(200) = 'CalculationPeriodDescription';
DECLARE @ValFldId INT;
DECLARE @ValFldName VARCHAR(200) = 'CalculationPeriodTypeId';
DECLARE @DDLName VARCHAR(100) = 'Period used for R2T4 Calculation';
DECLARE @PageResourceId SMALLINT;
DECLARE @ResourceName VARCHAR(100) = 'Program Versions';

BEGIN TRANSACTION CalculationPeriodTypes;

BEGIN TRY
    IF NOT EXISTS (SELECT * FROM syResources WHERE ResourceURL = @ResourceURL)
    BEGIN
        SET @ResourceID = 878;
        INSERT INTO dbo.syResources
        (
            ResourceID,
            Resource,
            ResourceTypeID,
            ResourceURL,
            SummListId,
            ChildTypeId,
            ModDate,
            ModUser,
            AllowSchlReqFlds,
            MRUTypeId,
            UsedIn,
            TblFldsId,
            DisplayName
        )
        VALUES
        (   @ResourceID,                -- ResourceID - smallint
            'Calculation Period Types', -- Resource - varchar(200)
            4,                          -- ResourceTypeID - tinyint
            @ResourceURL,               -- ResourceURL - varchar(100)
            NULL,                       -- SummListId - smallint
            NULL,                       -- ChildTypeId - tinyint
            GETDATE(),                  -- ModDate - datetime
            'Supoort',                  -- ModUser - varchar(50)
            0,                          -- AllowSchlReqFlds - bit
            1,                          -- MRUTypeId - smallint
            975,                        -- UsedIn - int
            NULL,                       -- TblFldsId - int
            NULL                        -- DisplayName - varchar(200)
            );
    END;

    IF NOT EXISTS
    (
        SELECT FldName
        FROM dbo.syFields
        WHERE FldName = 'CalculationPeriodTypeId'
    )
    BEGIN
        SET @PageResourceId =
        (
            SELECT ResourceID FROM dbo.syResources WHERE Resource = @ResourceName
        );
        SET @pk =
        (
            SELECT TblPK FROM dbo.syTables WHERE TblName = 'arPrgVersions'
        );
        IF @pk IS NOT NULL
        BEGIN
            EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'CalculationPeriodTypeId',
                                                              @FldTypeId = 72,
                                                              @FldLen = 16,
                                                              @DerivedField = 0,
                                                              @SchlReq = 0,
                                                              @LogChanges = 1,
                                                              @Mask = NULL,
                                                              @FieldCaption = N'Period used for calculation',
                                                              @LanguageId = 1,
                                                              @TblName = N'arPrgVersions',
                                                              @TblDescription = N'Period used for calculation',
                                                              @CategoryId = NULL,
                                                              @FKColDescrip = NULL,
                                                              @PageResourceId = @PageResourceId,
                                                              @Required = 0,
                                                              @ControlName = NULL;
        END;
    END;

    IF NOT EXISTS
    (
        SELECT FldName
        FROM dbo.syFields
        WHERE FldName = 'CalculationPeriodCode'
    )
    BEGIN
        SET @pk =
        (
            SELECT TblPK FROM dbo.syTables WHERE TblName = 'arPrgVersions'
        );
        IF @pk IS NOT NULL
        BEGIN
            EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'CalculationPeriodCode',
                                                              @FldTypeId = 200,
                                                              @FldLen = 50,
                                                              @DerivedField = 0,
                                                              @SchlReq = 0,
                                                              @LogChanges = 1,
                                                              @Mask = NULL,
                                                              @FieldCaption = N'Code',
                                                              @LanguageId = 1,
                                                              @TblName = N'arPrgVersions',
                                                              @TblDescription = N'Code',
                                                              @CategoryId = NULL,
                                                              @FKColDescrip = NULL,
                                                              @PageResourceId = @ResourceID,
                                                              @Required = 0,
                                                              @ControlName = NULL;
        END;
    END;

    IF NOT EXISTS
    (
        SELECT FldName
        FROM dbo.syFields
        WHERE FldName = 'CalculationPeriodDescription'
    )
    BEGIN
        SET @pk =
        (
            SELECT TblPK FROM dbo.syTables WHERE TblName = 'arPrgVersions'
        );
        IF @pk IS NOT NULL
        BEGIN
            EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'CalculationPeriodDescription',
                                                              @FldTypeId = 200,
                                                              @FldLen = 50,
                                                              @DerivedField = 0,
                                                              @SchlReq = 0,
                                                              @LogChanges = 1,
                                                              @Mask = NULL,
                                                              @FieldCaption = N'Description',
                                                              @LanguageId = 1,
                                                              @TblName = N'arPrgVersions',
                                                              @TblDescription = N'Description',
                                                              @CategoryId = NULL,
                                                              @FKColDescrip = NULL,
                                                              @PageResourceId = @ResourceID,
                                                              @Required = 0,
                                                              @ControlName = NULL;
        END;
    END;

    IF NOT EXISTS (SELECT * FROM syDDLS WHERE DDLName = @DDLName)
    BEGIN
        SET @DDLId =
        (
            SELECT MAX(DDLId) + 1 FROM syDDLS
        );
        SET @TableId =
        (
            SELECT TOP 1 TblId FROM syTables WHERE TblName = @TblName
        );
        SET @DispFldId =
        (
            SELECT TOP 1 FldId FROM syFields WHERE FldName = @DispFldName
        );
        SET @ValFldId =
        (
            SELECT TOP 1 FldId FROM syFields WHERE FldName = @ValFldName
        );
        INSERT INTO dbo.syDDLS
        (
            DDLId,
            DDLName,
            TblId,
            DispFldId,
            ValFldId,
            ResourceId,
            CulDependent
        )
        VALUES
        (   @DDLId,      -- DDLId - int
            @DDLName,    -- DDLName - varchar(100)
            @TableId,    -- TblId - int
            @DispFldId,  -- DispFldId - int
            @ValFldId,   -- ValFldId - int
            @ResourceID, -- ResourceId - int
            NULL         -- CulDependent - bit
            );

        UPDATE dbo.syFields
        SET DDLId = @DDLId
        WHERE FldName = 'CalculationPeriodTypeId';
    END;
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
    BEGIN
        ROLLBACK TRANSACTION CalculationPeriodTypes;
        PRINT 'Exception Ocurred!';
    END;
END CATCH;

IF @@TRANCOUNT > 0
BEGIN
    COMMIT TRANSACTION CalculationPeriodTypes;
END;
GO

--=================================================================================================
--END
-- AD-1487 Ability to select the period used for R2T4 calculation on Program Version screen
-- Inserting data into resource tables for R2T4 Calculation Period Types 
--END
--=================================================================================================

--=================================================================================================
--START
-- AD-2013 Ability to specify the percentage of excused absences allowed in a payment period for a Clock Hour program.
-- Inserting data into resource tables for AllowExcusAbsPerPayPrd.
--START
--=================================================================================================

BEGIN TRANSACTION AllowExcusAbsPerPayPrd;

BEGIN TRY
    DECLARE @pk INT;
    IF NOT EXISTS
    (
        SELECT FldName
        FROM dbo.syFields
        WHERE FldName = 'AllowExcusAbsPerPayPrd'
    )
    BEGIN
        SET @pk =
        (
            SELECT TblPK FROM dbo.syTables WHERE TblName = 'arPrgVersions'
        );
        IF @pk IS NOT NULL
        BEGIN
            EXEC dbo.DEVELOPMENT_InsertUpdateFieldInResources @FieldName = N'AllowExcusAbsPerPayPrd',
                                                              @FldTypeId = 5,
                                                              @FldLen = 8,
                                                              @DerivedField = 0,
                                                              @SchlReq = 0,
                                                              @LogChanges = 1,
                                                              @Mask = NULL,
                                                              @FieldCaption = N'AllowExcusAbsPerPayPrd',
                                                              @LanguageId = 1,
                                                              @TblName = N'arPrgVersions',
                                                              @TblDescription = N'AllowExcusAbsPerPayPrd',
                                                              @CategoryId = NULL,
                                                              @FKColDescrip = NULL,
                                                              @PageResourceId = 61,
                                                              @Required = 0,
                                                              @ControlName = NULL;
        END;
    END;
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
    BEGIN
        ROLLBACK TRANSACTION AllowExcusAbsPerPayPrd;
        PRINT 'Exception Ocurred!';
    END;
END CATCH;

IF @@TRANCOUNT > 0
BEGIN
    COMMIT TRANSACTION AllowExcusAbsPerPayPrd;
END;
GO

--=================================================================================================
--END
-- AD-2013 Ability to specify the percentage of excused absences allowed in a payment period for Clock Hour program.
-- Inserting data into resource tables for AllowExcusAbsPerPayPrd
--END
--=================================================================================================
--========================================================================================================================
--START
-- AD-4805 - Custom Verbiage for SAP Notice.
-- Inserting default messages for SAP Notice Policy.
--START
--========================================================================================================================

DECLARE @StatusId UNIQUEIDENTIFIER;
SET @StatusId =
(
    SELECT StatusId FROM dbo.syStatuses WHERE UPPER(StatusCode) = 'A'
);

BEGIN TRANSACTION SAPNotice;

BEGIN TRY

    IF EXISTS
    (
        SELECT 1
        FROM dbo.syTitleIVSapStatus
        WHERE UPPER(Code) = 'PASSED'
              AND StatusId = @StatusId
              AND DefaultMessage IS NULL
    )
    BEGIN
        UPDATE dbo.syTitleIVSapStatus
        SET UpdatedDate = GETDATE(),
            DefaultMessage = 'You have successfully met our school�s financial aid satisfactory progress standards of <<Minimum quantity>> and a <<MinimumGPA>> cumulative grade.'
        WHERE UPPER(Code) = 'PASSED'
              AND StatusId = @StatusId;
    END;

    IF EXISTS
    (
        SELECT 1
        FROM dbo.syTitleIVSapStatus
        WHERE UPPER(Code) = 'WARNING'
              AND StatusId = @StatusId
              AND DefaultMessage IS NULL
    )
    BEGIN
        UPDATE dbo.syTitleIVSapStatus
        SET UpdatedDate = GETDATE(),
            DefaultMessage = 'You have been placed on Title IV Warning for failure to complete the minimum required portion of your program and/or a low cumulative grade.
At the end of the next payment period your progress will be re-evaluated. Failure to achieve our school�s financial aid satisfactory progress standards of <<Minimum quantity>> and/or a <<MinimumGPA>> cumulative grade will result in your Title IV financial funding being terminated.'
        WHERE UPPER(Code) = 'WARNING'
              AND StatusId = @StatusId;
    END;
    IF EXISTS
    (
        SELECT 1
        FROM dbo.syTitleIVSapStatus
        WHERE UPPER(Code) = 'INELIGIBLE'
              AND StatusId = @StatusId
              AND DefaultMessage IS NULL
    )
    BEGIN
        UPDATE dbo.syTitleIVSapStatus
        SET UpdatedDate = GETDATE(),
            DefaultMessage = 'Failure to complete the minimum required portion of your program and/or a low cumulative grade, and having a previous status of Title IV Warning, has made you ineligible to receive any further Title IV funding.
At the end of the next payment period your progress will be re-evaluated. You must achieve our school�s financial aid satisfactory progress standards of <<Minimum quantity>> and/or a <<MinimumGPA>> cumulative grade to resume eligibility to receive any Title IV funding.'
        WHERE UPPER(Code) = 'INELIGIBLE'
              AND StatusId = @StatusId;
    END;
    IF EXISTS
    (
        SELECT 1
        FROM dbo.syTitleIVSapStatus
        WHERE UPPER(Code) = 'PROBATION'
              AND StatusId = @StatusId
              AND DefaultMessage IS NULL
    )
    BEGIN
        UPDATE dbo.syTitleIVSapStatus
        SET UpdatedDate = GETDATE(),
            DefaultMessage = 'You have failed to complete the minimum required portion of your program and/or a low cumulative grade, and having a previous status of Title IV Warning, you are ineligible to receive any further Title IV funding.
Your appeal has been approved and you are now placed on Title IV probation which will temporarily resume your Title IV eligibility.
At the end of the next payment period your progress will be re-evaluated. You must achieve our school�s financial aid satisfactory progress standards of <<Minimum quantity>> and/or a <<MinimumGPA>> cumulative grade to continue to receive Title IV aid.'
        WHERE UPPER(Code) = 'PROBATION'
              AND StatusId = @StatusId;
    END;

END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
    BEGIN
        ROLLBACK TRANSACTION SAPNotice;
        PRINT 'Exception Ocurred!';
    END;
END CATCH;

IF @@TRANCOUNT > 0
BEGIN
    COMMIT TRANSACTION SAPNotice;
END;

GO


--========================================================================================================================
--END
-- AD-4805 - Custom Verbiage for SAP Notice.
-- Inserting default messages for SAP Notice Policy..
--END
--========================================================================================================================
--=================================================================================================
-- START AD-7488 Manage Config Setting for AFA Integration
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION EnableAFAIntegrationManageConfig;
BEGIN TRY
    IF NOT EXISTS
    (
        SELECT 1
        FROM syConfigAppSettings
        WHERE KeyName = 'EnableAFAIntegration'
    )
    BEGIN
        DECLARE @settingId INT;
        DECLARE @modDate DATETIME;
        SET @modDate = GETDATE();

        INSERT INTO syConfigAppSettings
        (
            KeyName,
            Description,
            ModUser,
            ModDate,
            CampusSpecific,
            ExtraConfirmation
        )
        VALUES
        (   'EnableAFAIntegration',                                                -- KeyName - varchar(200)
            'Yes/No campus specific value to enable the AFA Integration service.', -- Description - varchar(1000)
            'Support',                                                             -- ModUser - varchar(50)
            @modDate,                                                              -- ModDate - datetime
            1,                                                                     -- CampusSpecific - bit
            0                                                                      -- ExtraConfirmation - bit
            );
        SET @settingId =
        (
            SELECT SettingId
            FROM syConfigAppSettings
            WHERE KeyName = 'EnableAFAIntegration'
        );

        INSERT INTO syConfigAppSet_Lookup
        (
            LookUpId,
            SettingId,
            ValueOptions,
            ModUser,
            ModDate
        )
        VALUES
        (   NEWID(),    -- LookUpId - uniqueidentifier
            @settingId, -- SettingId - int
            'No',       -- ValueOptions - varchar(50)
            'Support',  -- ModUser - varchar(50)
            @modDate    -- ModDate - datetime
            );
        INSERT INTO syConfigAppSet_Lookup
        (
            LookUpId,
            SettingId,
            ValueOptions,
            ModUser,
            ModDate
        )
        VALUES
        (   NEWID(),    -- LookUpId - uniqueidentifier
            @settingId, -- SettingId - int
            'Yes',      -- ValueOptions - varchar(50)
            'Support',  -- ModUser - varchar(50)
            @modDate    -- ModDate - datetime
            );
        INSERT INTO syConfigAppSetValues
        (
            ValueId,
            SettingId,
            CampusId,
            Value,
            ModUser,
            ModDate,
            Active
        )
        VALUES
        (   NEWID(),    -- ValueId - uniqueidentifier
            @settingId, -- SettingId - int
            NULL,       -- CampusId - uniqueidentifier
            'No',       -- Value - varchar(1000)
            'Support',  -- ModUser - varchar(50)
            @modDate,   -- ModDate - datetime
            1           -- Active - bit
            );
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION EnableAFAIntegrationManageConfig;
    PRINT 'Failed transcation EnableAFAIntegrationManageConfig.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION EnableAFAIntegrationManageConfig;
    PRINT 'successful transaction EnableAFAIntegrationManageConfig.';
END;
GO

--=================================================================================================
-- END AD-7488 Manage Config Setting for AFA Integration
--=================================================================================================
--=================================================================================================
-- START AD-7802  : TitleIV checkbox is checked when FAMEApprovedTitleIV checkbox is checked for a 
-- program which belongs to All campus AD-6067
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION TitleIVFld;
BEGIN TRY
    DECLARE @resourceId INT;
    DECLARE @idFldname VARCHAR(MAX) = 'CalculationPeriodTypeId';
    DECLARE @codeFldname VARCHAR(MAX) = 'CalculationPeriodCode';
    DECLARE @descriptionFldname VARCHAR(MAX) = 'CalculationPeriodDescription';
    DECLARE @fldname VARCHAR(MAX) = 'AllowExcusAbsPerPayPrd';
    DECLARE @ddlname VARCHAR(MAX) = 'Period used for R2T4 Calculation';
    DECLARE @resourcename VARCHAR(MAX) = 'Calculation Period Types';
    DECLARE @fldid INT;
    DECLARE @TblFldsId INT;
    DECLARE @ddlid INT;

    SET @ddlid =
    (
        SELECT DDLId FROM syDDLS WHERE DDLName = @ddlname
    );
    IF @ddlid > 0
    BEGIN
        DELETE FROM syDDLS
        WHERE DDLId = @ddlid;
    END;

    SET @resourceId =
    (
        SELECT ResourceID FROM syResources WHERE Resource = @resourcename
    );
    IF @resourceId > 0
    BEGIN
        DELETE FROM syResources
        WHERE ResourceID = @resourceId;
    END;

    SET @fldid =
    (
        SELECT FldId FROM syFields WHERE FldName = @idFldname
    );
    IF @fldid > 0
    BEGIN
        DELETE FROM syFldCaptions
        WHERE FldId = @fldid;
        SET @TblFldsId =
        (
            SELECT TblFldsId FROM syTblFlds WHERE FldId = @fldid
        );
        IF @TblFldsId > 0
        BEGIN
            DELETE FROM syResTblFlds
            WHERE TblFldsId = @TblFldsId;
        END;
        DELETE FROM syTblFlds
        WHERE FldId = @fldid;
        DELETE FROM syFields
        WHERE FldName = @idFldname;
    END;
    SET @fldid =
    (
        SELECT FldId FROM syFields WHERE FldName = @codeFldname
    );
    IF @fldid > 0
    BEGIN
        DELETE FROM syFldCaptions
        WHERE FldId = @fldid;
        SET @TblFldsId =
        (
            SELECT TblFldsId FROM syTblFlds WHERE FldId = @fldid
        );
        IF @TblFldsId > 0
        BEGIN
            DELETE FROM syResTblFlds
            WHERE TblFldsId = @TblFldsId;
        END;
        DELETE FROM syTblFlds
        WHERE FldId = @fldid;
        DELETE FROM syFields
        WHERE FldName = @codeFldname;
    END;
    SET @fldid =
    (
        SELECT FldId FROM syFields WHERE FldName = @descriptionFldname
    );
    IF @fldid > 0
    BEGIN
        DELETE FROM syFldCaptions
        WHERE FldId = @fldid;
        SET @TblFldsId =
        (
            SELECT TblFldsId FROM syTblFlds WHERE FldId = @fldid
        );
        IF @TblFldsId > 0
        BEGIN
            DELETE FROM syResTblFlds
            WHERE TblFldsId = @TblFldsId;
        END;
        DELETE FROM syTblFlds
        WHERE FldId = @fldid;
        DELETE FROM syFields
        WHERE FldName = @descriptionFldname;
    END;
    SET @fldid =
    (
        SELECT FldId FROM syFields WHERE FldName = @fldname
    );
    IF @fldid > 0
    BEGIN
        DELETE FROM syFldCaptions
        WHERE FldId = @fldid;
        SET @TblFldsId =
        (
            SELECT TblFldsId FROM syTblFlds WHERE FldId = @fldid
        );
        IF @TblFldsId > 0
        BEGIN
            DELETE FROM syResTblFlds
            WHERE TblFldsId = @TblFldsId;
        END;
        DELETE FROM syTblFlds
        WHERE FldId = @fldid;
        DELETE FROM syFields
        WHERE FldName = @fldname;
    END;
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
    BEGIN
        ROLLBACK TRANSACTION TitleIVFld;
        PRINT 'Exception Ocurred!';
    END;
END CATCH;

IF @@TRANCOUNT > 0
BEGIN
    COMMIT TRANSACTION TitleIVFld;
END;
GO

--=================================================================================================
-- END AD-7802  : TitleIV checkbox is checked when FAMEApprovedTitleIV checkbox is checked for a 
-- program which belongs to All campus AD-6067
--=================================================================================================
--========================================================================================================================
--START
-- AD-4809 - Create SAP Results Tab
--START
--========================================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION AddTtitleIVSAPResultsMenu;
BEGIN TRY
    DECLARE @newResourceId SMALLINT;
    SET @newResourceId = 861; --based on excell

    IF NOT EXISTS
    (
        SELECT TOP 1
               ResourceID
        FROM dbo.syResources
        WHERE ResourceID = @newResourceId
    )
    BEGIN

        INSERT INTO dbo.syResources
        (
            ResourceID,
            Resource,
            ResourceTypeID,
            ResourceURL,
            SummListId,
            ChildTypeId,
            ModDate,
            ModUser,
            AllowSchlReqFlds,
            MRUTypeId,
            UsedIn,
            TblFldsId,
            DisplayName
        )
        VALUES
        (   @newResourceId,            -- ResourceID - smallint
            'Title IV SAP Results',    -- Resource - varchar(200)
            3,                         -- ResourceTypeID - tinyint
            N'~/AR/FASAPResults.aspx', -- ResourceURL - varchar(100)
            NULL,                      -- SummListId - smallint
            NULL,                      -- ChildTypeId - tinyint
            GETDATE(),                 -- ModDate - datetime
            'sa',                      -- ModUser - varchar(50)
            NULL,                      -- AllowSchlReqFlds - bit
            0,                         -- MRUTypeId - smallint
            975,                       -- UsedIn - int
            NULL,                      -- TblFldsId - int
            NULL                       -- DisplayName - varchar(200)
            );
    END;


    DECLARE @GreatGrandMenuParent INT;
    SELECT @GreatGrandMenuParent =
    (
        SELECT MenuItemId
        FROM syMenuItems
        WHERE MenuName = 'Financial Aid'
              AND MenuItemTypeId = 1
    );
    DECLARE @GrandMenuParent INT;
    SELECT @GrandMenuParent =
    (
        SELECT MenuItemId
        FROM syMenuItems
        WHERE MenuName = 'Manage Students'
              AND ParentId = @GreatGrandMenuParent
    );
    DECLARE @MenuParent INT;
    SET @MenuParent =
    (
        SELECT MenuItemId
        FROM syMenuItems
        WHERE MenuName = 'Academics'
              AND MenuItemTypeId = 3
              AND ParentId = @GrandMenuParent
    );
    IF NOT EXISTS
    (
        SELECT TOP 1
               MenuItemId
        FROM dbo.syMenuItems
        WHERE MenuName = 'Title IV SAP Results'
              AND DisplayName = 'Title IV SAP Results'
              AND ResourceId = @newResourceId
              AND Url = '/AR/FASAPResults.aspx'
    )
    BEGIN
        INSERT INTO dbo.syMenuItems
        (
            MenuName,
            DisplayName,
            Url,
            MenuItemTypeId,
            ParentId,
            DisplayOrder,
            IsPopup,
            ModDate,
            ModUser,
            IsActive,
            ResourceId,
            HierarchyId,
            ModuleCode,
            MRUType,
            HideStatusBar
        )
        VALUES
        (   'Title IV SAP Results',   -- MenuName - varchar(250)
            'Title IV SAP Results',   -- DisplayName - varchar(250)
            N'/AR/FASAPResults.aspx', -- Url - nvarchar(250)
            4,                        -- MenuItemTypeId - smallint
            @MenuParent,              -- ParentId - int
            1100,                     -- DisplayOrder - int
            0,                        -- IsPopup - bit
            GETDATE(),                -- ModDate - datetime
            'sa',                     -- ModUser - varchar(50)
            1,                        -- IsActive - bit
            @newResourceId,           -- ResourceId - smallint
            NEWID(),                  -- HierarchyId - uniqueidentifier
            '',                       -- ModuleCode - varchar(5)
            0,                        -- MRUType - int
            NULL                      -- HideStatusBar - bit
            );
    END;
    ELSE
    BEGIN
        IF NOT EXISTS
        (
            SELECT 1
            FROM syMenuItems
            WHERE MenuName = 'Title IV SAP Results'
                  AND Url = '/AR/FASAPResults.aspx'
                  AND ParentId = @MenuParent
                  AND ResourceId = @newResourceId
        )
        BEGIN
            UPDATE dbo.syMenuItems
            SET ResourceId = @newResourceId,
                ParentId = @MenuParent
            WHERE MenuName = 'Title IV SAP Results'
                  AND Url = '/AR/FASAPResults.aspx';
        END;

    END;

    -- insert into synavigationNodes
    DECLARE @hierarcyId UNIQUEIDENTIFIER;
    SET @hierarcyId =
    (
        SELECT HierarchyId
        FROM syMenuItems
        WHERE ResourceId = @newResourceId
              AND Url = '/AR/FASAPResults.aspx'
    );
    IF NOT EXISTS
    (
        SELECT 1
        FROM syNavigationNodes
        WHERE ResourceId = @newResourceId
    )
    BEGIN
        DECLARE @ParentNavigation AS UNIQUEIDENTIFIER;
        SET @ParentNavigation =
        (
            SELECT TOP 1
                   childNode.HierarchyId
            FROM dbo.syNavigationNodes grandParentNode
                INNER JOIN dbo.syNavigationNodes parentNode
                    ON parentNode.ParentId = grandParentNode.HierarchyId
                INNER JOIN dbo.syNavigationNodes childNode
                    ON childNode.ParentId = parentNode.HierarchyId
            WHERE parentNode.ResourceId = 394
                  AND grandParentNode.ResourceId = 191
                  AND childNode.ResourceId = 739
        );
        INSERT INTO syNavigationNodes
        (
            HierarchyId,
            HierarchyIndex,
            ResourceId,
            ParentId,
            ModUser,
            ModDate,
            IsPopupWindow,
            IsShipped
        )
        VALUES
        (   @hierarcyId,       -- HierarchyId - uniqueidentifier
            8,                 -- HierarchyIndex - smallint
            @newResourceId,    -- ResourceId - smallint
            @ParentNavigation, -- ParentId - uniqueidentifier
            'Support',         -- ModUser - varchar(50)
            GETDATE(),         -- ModDate - datetime
            0,                 -- IsPopupWindow - bit
            1                  -- IsShipped - bit
            );
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION AddTtitleIVSAPResultsMenu;
    PRINT 'Failed transcation AddTtitleIVSAPResultsMenu.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION AddTtitleIVSAPResultsMenu;
    PRINT 'successful transaction AddTtitleIVSAPResultsMenu.';
END;
GO

--========================================================================================================================
--END
-- AD-4809 - Create SAP Results Tab
--END
--========================================================================================================================
--=================================================================================================
PRINT 'Generate Title IV SAP Results Report';
--=================================================================================================
--=================================================================================================
-- START AD-5499 Generate Title IV SAP Results Report
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION TitleIVSetup;
BEGIN TRY
    DECLARE @fasapResourceId INT;
    DECLARE @reportId UNIQUEIDENTIFIER;
    DECLARE @setid INT;
    DECLARE @sectionId INT;
    DECLARE @titleIvItemId INT;

    IF NOT EXISTS
    (
        SELECT TOP 1
               ItemId
        FROM ParamItem
        WHERE Caption = 'TitleIvSapCheckResultsReport'
    )
    BEGIN
        INSERT INTO dbo.ParamItem
        (
            Caption,
            ControllerClass,
            valueprop,
            ReturnValueName,
            IsItemOverriden
        )
        VALUES
        (   N'TitleIvSapCheckResultsReport',       -- Caption - nvarchar(50)
            N'ParamTitleIvSapResultsReport.ascx',  -- ControllerClass - nvarchar(50)
            0,                                     -- valueprop - int
            N'ParamTitleIvSapResultsReportOption', -- ReturnValueName - nvarchar(50)
            NULL                                   -- IsItemOverriden - bit
            );

    END;

    UPDATE dbo.ParamItem
    SET ControllerClass = 'ParamTitleIvSapResultsReport.ascx'
    WHERE Caption = 'TitleIvSapCheckResultsReport';

    UPDATE dbo.syMenuItems
    SET DisplayName = 'Title IV SAP Results',
        MenuName = 'Title IV SAP Results',
        ResourceId = 805
    WHERE Url = '/SY/ParamReport.aspx'
          AND ResourceId = 861;

    SET @titleIvItemId =
    (
        SELECT TOP 1 ItemId
        FROM ParamItem
        WHERE Caption = 'TitleIvSapCheckResultsReport'
    );

    UPDATE dbo.syResources
    SET Resource = 'Title IV SAP Results'
    WHERE ResourceID = 805;

    UPDATE dbo.syMenuItems
    SET DisplayName = 'Title IV SAP Results',
        MenuName = 'Title IV SAP Results'
    WHERE ResourceId = 805;

    IF NOT EXISTS
    (
        SELECT TOP 1 ReportId
        FROM dbo.syReports
        WHERE ReportName = 'FASAPReport'
    )
    BEGIN
        INSERT INTO dbo.syReports
        (
            ReportId,
            ResourceId,
            ReportName,
            ReportDescription,
            RDLName,
            AssemblyFilePath,
            ReportClass,
            CreationMethod,
            WebServiceUrl,
            RecordLimit,
            AllowedExportTypes,
            ReportTabLayout,
            ShowPerformanceMsg,
            ShowFilterMode
        )
        VALUES
        (   NEWID(),                                     -- ReportId - uniqueidentifier
            805,                                         -- ResourceId - int
            'FASAPReport',                               -- ReportName - varchar(50)
            'FASAP Check Results',                       -- ReportDescription - varchar(500)
            'FASAPCheck/FASAPReport',                    -- RDLName - varchar(200)
            '~/Bin/Reporting.dll',                       -- AssemblyFilePath - varchar(200)
            'FAME.Advantage.Reporting.Logic.FASAPCheck', -- ReportClass - varchar(200)
            'BuildReport',                               -- CreationMethod - varchar(200)
            'BuildReport',                               -- WebServiceUrl - varchar(200)
            600,                                         -- RecordLimit - bigint
            0,                                           -- AllowedExportTypes - int
            1,                                           -- ReportTabLayout - int
            0,                                           -- ShowPerformanceMsg - bit
            0                                            -- ShowFilterMode - bit
            );
    END;

    SET @reportId =
    (
        SELECT TOP 1 ReportId FROM dbo.syReports WHERE ReportName = 'FASAPReport'
    );

    IF NOT EXISTS
    (
        SELECT TOP 1 SetId
        FROM dbo.syReportTabs
        WHERE ReportId = @reportId
    )
    BEGIN
        INSERT INTO dbo.syReportTabs
        (
            ReportId,
            PageViewId,
            UserControlName,
            SetId,
            ControllerClass,
            DisplayName,
            ParameterSetLookUp,
            TabName
        )
        VALUES
        (   @reportId,                                    -- ReportId - uniqueidentifier
            N'RadRptPage_Options',                        -- PageViewId - nvarchar(200)
            N'ParamPanelBarSetCustomOptions',             -- UserControlName - nvarchar(200)
            (
                SELECT MAX(SetId) + 1 FROM dbo.syReportTabs
            ),                                            -- SetId - bigint
            N'ParamSetPanelBarControl.ascx',              -- ControllerClass - nvarchar(200)
            N'Custom Options Set for FASAP Check Report', -- DisplayName - nvarchar(200)
            N'FASAPCheckResultsFilterSet',                -- ParameterSetLookUp - nvarchar(200)
            N'Options'                                    -- TabName - nvarchar(200)
            );
    END;

    SET @setid =
    (
        SELECT SetId FROM dbo.syReportTabs WHERE ReportId = @reportId
    );

    IF NOT EXISTS (SELECT TOP 1 SectionId FROM dbo.ParamSection WHERE SetId = @setid)
    BEGIN
        INSERT INTO dbo.ParamSection
        (
            SectionName,
            SectionCaption,
            SetId,
            SectionSeq,
            SectionDescription,
            SectionType
        )
        VALUES
        (   N'Parameters for FASAP Check  Report',    -- SectionName - nvarchar(50)
            N'FASAP Check  Parameters',               -- SectionCaption - nvarchar(100)
            @setid,                                   -- SetId - bigint
            1,                                        -- SectionSeq - int
            N'Choose FASAP Check  report parameters', -- SectionDescription - nvarchar(500)
            0                                         -- SectionType - int
            );
    END;

    SET @sectionId =
    (
        SELECT SectionId FROM dbo.ParamSection WHERE SetId = @setid
    );

    UPDATE dbo.ParamSet
    SET SetType = 'Custom'
    WHERE SetName = 'FASAPCheckResultsFilterSet';

    DELETE FROM dbo.ParamDetail
    WHERE SectionId = @sectionId;

    IF NOT EXISTS
    (
        SELECT TOP 1
               DetailId
        FROM dbo.ParamDetail
        WHERE SectionId = @sectionId
              AND ItemId = @titleIvItemId
              AND CaptionOverride = 'Report Options'
    )
    BEGIN
        INSERT INTO dbo.ParamDetail
        (
            SectionId,
            ItemId,
            CaptionOverride,
            ItemSeq
        )
        VALUES
        (   @sectionId,        -- SectionId - bigint
            @titleIvItemId,    -- ItemId - bigint
            N'Report Options', -- CaptionOverride - nvarchar(50)
            1                  -- ItemSeq - int
            );
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION TitleIVSetup;
    PRINT 'Failed transcation TitleIvSapCheckResultsReport.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION TitleIVSetup;
    PRINT 'successful transaction TitleIvSapCheckResultsReport.';
END;
GO

--=================================================================================================
-- End AD-5499 Generate Title IV SAP Results Report
--=================================================================================================
PRINT 'addNewNavigation';
--=================================================================================================
-- START -- AD-7100 Create security for SAP Check Results page (let this be for Title IV SAP Results Page (861 resourceID))
--=================================================================================================
DECLARE @Error AS INTEGER;
SET @Error = 0;

BEGIN TRANSACTION addNewNavigation;
BEGIN TRY
    DECLARE @PageName VARCHAR(100) = 'Title IV SAP Results';
    DECLARE @PageResourceId INT;
    SET @PageResourceId = 861;
    IF EXISTS
    (
        SELECT TOP (1)
               ResourceID
        FROM dbo.syResources
        WHERE ResourceID = @PageResourceId
    )
    BEGIN

        DECLARE @ParentHierarchyId UNIQUEIDENTIFIER;
        DECLARE @hierarcyID UNIQUEIDENTIFIER;
        SET @hierarcyID =
        (
            SELECT HierarchyId
            FROM syMenuItems
            WHERE ResourceId = 861
                  AND Url = '/AR/FASAPResults.aspx'
        );
        IF NOT EXISTS
        (
            SELECT TOP (1)
                   ResourceId
            FROM dbo.syNavigationNodes
            WHERE ResourceId = @PageResourceId
        )
        BEGIN

            SET @ParentHierarchyId =
            (
                SELECT TOP 1
                       childNode.HierarchyId
                FROM dbo.syNavigationNodes grandParentNode
                    INNER JOIN dbo.syNavigationNodes parentNode
                        ON parentNode.ParentId = grandParentNode.HierarchyId
                    INNER JOIN dbo.syNavigationNodes childNode
                        ON childNode.ParentId = parentNode.HierarchyId
                WHERE parentNode.ResourceId = 394
                      AND grandParentNode.ResourceId = 191
                      AND childNode.ResourceId = 739
            );
            INSERT INTO dbo.syNavigationNodes
            (
                HierarchyId,
                HierarchyIndex,
                ResourceId,
                ParentId,
                ModUser,
                ModDate,
                IsPopupWindow,
                IsShipped
            )
            SELECT @hierarcyID,
                   8,
                   @PageResourceId,
                   @ParentHierarchyId,
                   'support',
                   GETDATE(),
                   0,
                   1;

        END;
        ELSE
        BEGIN
            UPDATE syNavigationNodes
            SET HierarchyId = @hierarcyID
            WHERE ResourceId = 861;
        END;
    END;
    IF (@@ERROR > 0)
    BEGIN
        SET @Error = @@ERROR;
    END;

END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @Error = 1;
END CATCH;

IF (@Error = 0)
BEGIN
    COMMIT TRANSACTION addNewNavigation;
END;
ELSE
BEGIN
    ROLLBACK TRANSACTION addNewNavigation;
END;
--=================================================================================================
-- END -- AD-7100 Create security for SAP Check Results page
--=================================================================================================
GO
--=================================================================================================
PRINT 'PostFinalGrades';
--========================================================================================================================

--========================================================================================================================
--START
-- AD-2093 Ability to specify the course completion date on Post Final Grades screen
-- Set existing completed courses date completed to mod date.
--START
--========================================================================================================================

--BEGIN TRANSACTION PostFinalGrades;

--BEGIN TRY
--    DISABLE TRIGGER ALL ON dbo.arResults;
--    IF EXISTS
--    (
--        SELECT 1
--        FROM dbo.arResults
--        WHERE GrdSysDetailId IS NOT NULL
--              AND DateCompleted IS NULL
--    )
--    BEGIN
--        UPDATE dbo.arResults
--        SET DateCompleted = ModDate
--        WHERE GrdSysDetailId IS NOT NULL
--              AND DateCompleted IS NULL;
--    END;
--    ENABLE TRIGGER ALL ON dbo.arResults;
--END TRY
--BEGIN CATCH
--    SELECT ERROR_NUMBER() AS ErrorNumber;
--    SELECT ERROR_SEVERITY() AS ErrorSeverity;
--    SELECT ERROR_STATE() AS ErrorState;
--    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
--    SELECT ERROR_LINE() AS ErrorLine;
--    SELECT ERROR_MESSAGE() AS ErrorMessage;

--    IF @@TRANCOUNT > 0
--    BEGIN
--        ROLLBACK TRANSACTION PostFinalGrades;
--        PRINT 'Exception Ocurred!';
--    END;
--END CATCH;

--IF @@TRANCOUNT > 0
--BEGIN
--    COMMIT TRANSACTION PostFinalGrades;
--END;

--GO
--========================================================================================================================
--END
-- AD-2093 Ability to specify the course completion date on Post Final Grades screen
-- Set existing completed courses date completed to mod date.
--END
--========================================================================================================================
PRINT 'addPostAttendanceMenuItem';
--========================================================================================================================
--START
-- AD-7935 SPIKE: TrackSAPAttendance configuration key value when changed by Day is not making available the Post attendance page as expected
-- To add Post attendance menu item in the syMenuItems table
--START
--========================================================================================================================

DECLARE @Error AS INTEGER;
SET @Error = 0;

BEGIN TRANSACTION addPostAttendanceMenuItem;
BEGIN TRY

    --insert into syNavigationNodes for 'Post Attendance' Page
    DECLARE @PostAttendanceResourceID INT;
    SET @PostAttendanceResourceID =
    (
        SELECT ResourceID FROM syResources WHERE Resource = 'Post Attendance'
    );
    IF NOT EXISTS
    (
        SELECT 1
        FROM syNavigationNodes
        WHERE ResourceId = @PostAttendanceResourceID
    )
    BEGIN
        DECLARE @parentHeirarchyID UNIQUEIDENTIFIER;
        SET @parentHeirarchyID =
        (
            SELECT HierarchyId FROM syNavigationNodes WHERE ResourceId = 697
        );
        INSERT INTO syNavigationNodes
        (
            HierarchyId,
            HierarchyIndex,
            ResourceId,
            ParentId,
            ModUser,
            ModDate,
            IsPopupWindow,
            IsShipped
        )
        VALUES
        (   NEWID(),                   -- HierarchyId - uniqueidentifier
            6,                         -- HierarchyIndex - smallint
            @PostAttendanceResourceID, -- ResourceId - smallint
            @parentHeirarchyID,        -- ParentId - uniqueidentifier
            'Support',                 -- ModUser - varchar(50)
            GETDATE(),                 -- ModDate - datetime
            0,                         -- IsPopupWindow - bit
            1                          -- IsShipped - bit
            );
        IF (@@ERROR > 0)
        BEGIN
            SET @Error = @@ERROR;
        END;
    END;
    -- adding post attendance record to symenuItems
    IF (@Error = 0)
    BEGIN
        IF NOT EXISTS
        (
            SELECT 1
            FROM syMenuItems
            WHERE ResourceId = @PostAttendanceResourceID
        )
        BEGIN
            DECLARE @heirarcyPostAttendance UNIQUEIDENTIFIER;
            SET @heirarcyPostAttendance =
            (
                SELECT HierarchyId
                FROM syNavigationNodes
                WHERE ResourceId = @PostAttendanceResourceID
            );
            DECLARE @PostAttendanceParentId INT;
            SET @PostAttendanceParentId =
            (
                SELECT MenuItemId FROM syMenuItems WHERE ResourceId = 697
            );
            INSERT INTO syMenuItems
            (
                MenuName,
                DisplayName,
                Url,
                MenuItemTypeId,
                ParentId,
                DisplayOrder,
                IsPopup,
                ModDate,
                ModUser,
                IsActive,
                ResourceId,
                HierarchyId,
                ModuleCode,
                MRUType,
                HideStatusBar
            )
            VALUES
            (   'Post Attendance',               -- MenuName - varchar(250)
                'Post Attendance',               -- DisplayName - varchar(250)
                N'/FA/PostClockAttendance.aspx', -- Url - nvarchar(250)
                4,                               -- MenuItemTypeId - smallint
                @PostAttendanceParentId,         -- ParentId - int
                300,                             -- DisplayOrder - int
                0,                               -- IsPopup - bit
                GETDATE(),                       -- ModDate - datetime
                'Support',                       -- ModUser - varchar(50)
                1,                               -- IsActive - bit
                @PostAttendanceResourceID,       -- ResourceId - smallint
                @heirarcyPostAttendance,         -- HierarchyId - uniqueidentifier
                NULL,                            -- ModuleCode - varchar(5)
                NULL,                            -- MRUType - int
                NULL                             -- HideStatusBar - bit
                );
            IF (@@ERROR > 0)
            BEGIN
                SET @Error = @@ERROR;
            END;
        END;
    END;
END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @Error = 1;
END CATCH;

IF (@Error = 0)
BEGIN
    COMMIT TRANSACTION addPostAttendanceMenuItem;
END;
ELSE
BEGIN
    ROLLBACK TRANSACTION addPostAttendanceMenuItem;
END;
GO

--========================================================================================================================
--END
-- AD-7935 SPIKE: TrackSAPAttendance configuration key value when changed by Day is not making available the Post attendance page as expected
-- To add Post attendance menu item in the syMenuItems table
--END
--========================================================================================================================
--=================================================================================================
-- START AD-7486 Generate Title IV SAP Notice Landing Page
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION TitleIVNoticeReportSetup;
BEGIN TRY
    DECLARE @fasapNoticeReportResourceId INT = 862;
    DECLARE @titleIvSapSSRSReportName VARCHAR(30) = 'TitleIVNotice';
    DECLARE @titleIvSapNoticesCaption VARCHAR(30) = 'TitleIvSapNoticesReport';
    DECLARE @titleIvSapNoticesSet VARCHAR(30) = 'TitleIvSapNoticesReportSet';
    DECLARE @titleIvSapNoticesSection VARCHAR(30) = 'ParametersForTitleIvSapNoticesReport';
    DECLARE @titleIvSapNoticesMenuName VARCHAR(30) = 'Title IV SAP Notices';
    DECLARE @reportId UNIQUEIDENTIFIER;
    DECLARE @titleIvSapNoticesSetId INT;
    DECLARE @titleIvSapNoticesSectionId INT;
    DECLARE @titleIvSapNoticesItemId INT;

    --insert new resource id
    IF NOT EXISTS
    (
        SELECT TOP 1
               ResourceID
        FROM dbo.syResources
        WHERE ResourceID = @fasapNoticeReportResourceId
    )
    BEGIN
        INSERT INTO dbo.syResources
        (
            ResourceID,
            Resource,
            ResourceTypeID,
            ResourceURL,
            SummListId,
            ChildTypeId,
            ModDate,
            ModUser,
            AllowSchlReqFlds,
            MRUTypeId,
            UsedIn,
            TblFldsId,
            DisplayName
        )
        VALUES
        (   @fasapNoticeReportResourceId, -- ResourceID - smallint
            'Title IV SAP Notices',       -- Resource - varchar(200)
            5,                            -- ResourceTypeID - tinyint
            '~/SY/ParamReport.aspx',      -- ResourceURL - varchar(100)
            0,                            -- SummListId - smallint
            0,                            -- ChildTypeId - tinyint
            GETDATE(),                    -- ModDate - datetime
            'sa',                         -- ModUser - varchar(50)
            NULL,                         -- AllowSchlReqFlds - bit
            0,                            -- MRUTypeId - smallint
            975,                          -- UsedIn - int
            0,                            -- TblFldsId - int
            ''                            -- DisplayName - varchar(200)
            );
    END;

    -- insert param item 
    IF NOT EXISTS
    (
        SELECT TOP 1
               ItemId
        FROM dbo.ParamItem
        WHERE Caption = @titleIvSapNoticesCaption
    )
    BEGIN
        INSERT INTO dbo.ParamItem
        (
            Caption,
            ControllerClass,
            valueprop,
            ReturnValueName,
            IsItemOverriden
        )
        VALUES
        (   @titleIvSapNoticesCaption,            -- Caption - nvarchar(50)
            N'ParamTitleIvSapNoticeReport.ascx',  -- ControllerClass - nvarchar(50)
            0,                                    -- valueprop - int
            N'ParamTitleIvSapNoticeReportOption', -- ReturnValueName - nvarchar(50)
            NULL                                  -- IsItemOverriden - bit
            );
    END;

    SET @titleIvSapNoticesItemId =
    (
        SELECT TOP 1
               ItemId
        FROM dbo.ParamItem
        WHERE Caption = @titleIvSapNoticesCaption
    );

    --insert set id
    IF NOT EXISTS
    (
        SELECT TOP 1
               SetId
        FROM dbo.ParamSet
        WHERE SetName = @titleIvSapNoticesSet
    )
    BEGIN
        INSERT INTO dbo.ParamSet
        (
            SetName,
            SetDisplayName,
            SetDescription,
            SetType
        )
        VALUES
        (   @titleIvSapNoticesSet,                     -- SetName - nvarchar(50)
            N'Title IV SAP Report Filter Set',         -- SetDisplayName - nvarchar(50)
            N'Filter for Title IV SAP Notices Report', -- SetDescription - nvarchar(1000)
            N'Custom'                                  -- SetType - nvarchar(50)
            );
    END;

    SET @titleIvSapNoticesSetId =
    (
        SELECT TOP 1 SetId FROM dbo.ParamSet WHERE SetName = @titleIvSapNoticesSet
    );

    -- insert section id
    IF NOT EXISTS
    (
        SELECT TOP 1
               SectionId
        FROM dbo.ParamSection
        WHERE SectionName = @titleIvSapNoticesSection
    )
    BEGIN
        INSERT INTO dbo.ParamSection
        (
            SectionName,
            SectionCaption,
            SetId,
            SectionSeq,
            SectionDescription,
            SectionType
        )
        VALUES
        (   @titleIvSapNoticesSection,                                -- SectionName - nvarchar(50)
            N'Title IV SAP Notices',                                  -- SectionCaption - nvarchar(100)
            @titleIvSapNoticesSetId,                                  -- SetId - bigint
            1,                                                        -- SectionSeq - int
            N'Choose the parameters for Title IV SAP Notices Report', -- SectionDescription - nvarchar(500)
            0                                                         -- SectionType - int
            );
    END;

    SET @titleIvSapNoticesSectionId =
    (
        SELECT TOP 1
               SectionId
        FROM dbo.ParamSection
        WHERE SectionName = @titleIvSapNoticesSection
    );

    IF NOT EXISTS
    (
        SELECT TOP 1
               DetailId
        FROM dbo.ParamDetail
        WHERE SectionId = @titleIvSapNoticesSectionId
              AND ItemId = @titleIvSapNoticesItemId
    )
    BEGIN
        INSERT INTO dbo.ParamDetail
        (
            SectionId,
            ItemId,
            CaptionOverride,
            ItemSeq
        )
        VALUES
        (   @titleIvSapNoticesSectionId, -- SectionId - bigint
            @titleIvSapNoticesItemId,    -- ItemId - bigint
            N'Report Options',           -- CaptionOverride - nvarchar(50)
            1                            -- ItemSeq - int
            );
    END;


    IF NOT EXISTS
    (
        SELECT TOP 1
               MenuItemId
        FROM dbo.syMenuItems
        WHERE MenuName = @titleIvSapNoticesMenuName
    )
    BEGIN
        INSERT INTO dbo.syMenuItems
        (
            MenuName,
            DisplayName,
            Url,
            MenuItemTypeId,
            ParentId,
            DisplayOrder,
            IsPopup,
            ModDate,
            ModUser,
            IsActive,
            ResourceId,
            HierarchyId,
            ModuleCode,
            MRUType,
            HideStatusBar
        )
        VALUES
        (   @titleIvSapNoticesMenuName,   -- MenuName - varchar(250)
            @titleIvSapNoticesMenuName,   -- DisplayName - varchar(250)
            N'/sy/ParamReport.aspx',      -- Url - nvarchar(250)
            4,                            -- MenuItemTypeId - smallint
            153,                          -- ParentId - int
            1025,                         -- DisplayOrder - int
            0,                            -- IsPopup - bit
            GETDATE(),                    -- ModDate - datetime
            'sa',                         -- ModUser - varchar(50)
            1,                            -- IsActive - bit
            @fasapNoticeReportResourceId, -- ResourceId - smallint
            NULL,                         -- HierarchyId - uniqueidentifier
            '',                           -- ModuleCode - varchar(5)
            0,                            -- MRUType - int
            NULL                          -- HideStatusBar - bit
            );
    END;

    --get module id
    DECLARE @ModuleId INT =
            (
                SELECT TOP 1
                       MenuItemId
                FROM dbo.syMenuItems
                WHERE MenuName = 'reports'
                      AND ParentId IS NULL
                      AND MenuItemTypeId = 1
            );


    DECLARE @SubMenuId INT =
            (
                SELECT TOP 1
                       MenuItemId
                FROM dbo.syMenuItems
                WHERE MenuName = 'financial aid'
                      AND ParentId = @ModuleId
                      AND MenuItemTypeId = 2
            );

    DECLARE @PageGroupId INT =
            (
                SELECT TOP 1
                       MenuItemId
                FROM dbo.syMenuItems
                WHERE DisplayName = 'general reports'
                      AND ParentId = @SubMenuId
                      AND MenuItemTypeId = 3
            );

    --get resource id for the page group menu item id
    DECLARE @PageGroupResourceId INT =
            (
                SELECT TOP 1
                       ResourceId
                FROM dbo.syMenuItems
                WHERE MenuItemId = @PageGroupId
            );


    IF NOT EXISTS
    (
        SELECT TOP 1
               HierarchyId
        FROM dbo.syNavigationNodes
        WHERE ResourceId = @fasapNoticeReportResourceId
    )
    BEGIN
        --get parent id/hierarchy (page group)
        DECLARE @reportParentId UNIQUEIDENTIFIER =
                (
                    SELECT TOP 1
                           HierarchyId
                    FROM dbo.syNavigationNodes
                    WHERE ResourceId = @PageGroupResourceId
                );
        INSERT INTO dbo.syNavigationNodes
        (
            HierarchyId,
            HierarchyIndex,
            ResourceId,
            ParentId,
            ModUser,
            ModDate,
            IsPopupWindow,
            IsShipped
        )
        VALUES
        (   NEWID(),                      -- HierarchyId - uniqueidentifier
            8,                            -- HierarchyIndex - smallint
            @fasapNoticeReportResourceId, -- ResourceId - smallint
            @reportParentId,              -- ParentId - uniqueidentifier
            'sa',                         -- ModUser - varchar(50)
            GETDATE(),                    -- ModDate - datetime
            0,                            -- IsPopupWindow - bit
            1                             -- IsShipped - bit
            );
    END;



    IF NOT EXISTS
    (
        SELECT TOP 1
               ReportId
        FROM dbo.syReports
        WHERE ReportName = @titleIvSapNoticesCaption + '/' + @titleIvSapSSRSReportName
    )
    BEGIN
        INSERT INTO dbo.syReports
        (
            ReportId,
            ResourceId,
            ReportName,
            ReportDescription,
            RDLName,
            AssemblyFilePath,
            ReportClass,
            CreationMethod,
            WebServiceUrl,
            RecordLimit,
            AllowedExportTypes,
            ReportTabLayout,
            ShowPerformanceMsg,
            ShowFilterMode
        )
        VALUES
        (   NEWID(),                                                     -- ReportId - uniqueidentifier
            @fasapNoticeReportResourceId,                                -- ResourceId - int
            @titleIvSapSSRSReportName,                                   -- ReportName - varchar(50)
            @titleIvSapSSRSReportName,                                   -- ReportDescription - varchar(500)
            @titleIvSapSSRSReportName + '/' + @titleIvSapSSRSReportName, -- RDLName - varchar(200)
            '~/Bin/Reporting.dll',                                       -- AssemblyFilePath - varchar(200)
            'FAME.Advantage.Reporting.Logic.TitleIvSapNotice',           -- ReportClass - varchar(200)
            'BuildReport',                                               -- CreationMethod - varchar(200)
            'futurefield',                                               -- WebServiceUrl - varchar(200)
            400,                                                         -- RecordLimit - bigint
            0,                                                           -- AllowedExportTypes - int
            1,                                                           -- ReportTabLayout - int
            0,                                                           -- ShowPerformanceMsg - bit
            0                                                            -- ShowFilterMode - bit
            );
    END;

    UPDATE syReports
    SET ReportDescription = @titleIvSapSSRSReportName,
        RDLName = @titleIvSapSSRSReportName + '/' + @titleIvSapSSRSReportName
    WHERE ResourceId = @fasapNoticeReportResourceId;


    SET @reportId =
    (
        SELECT TOP 1
               ReportId
        FROM dbo.syReports
        WHERE ReportName = @titleIvSapSSRSReportName
    );
    --handle multiple inserted to delete and leave one
    IF (
       (
           SELECT COUNT(ResourceId)
           FROM dbo.syReports
           WHERE ResourceId = @fasapNoticeReportResourceId
       ) > 1
       )
    BEGIN

        DELETE FROM syReports
        WHERE ResourceId = @fasapNoticeReportResourceId
              AND ReportId <> @reportId;
    END;


    IF NOT EXISTS
    (
        SELECT TOP 1
               ReportId
        FROM dbo.syReportTabs
        WHERE ReportId = @reportId
    )
    BEGIN
        INSERT INTO dbo.syReportTabs
        (
            ReportId,
            PageViewId,
            UserControlName,
            SetId,
            ControllerClass,
            DisplayName,
            ParameterSetLookUp,
            TabName
        )
        VALUES
        (   @reportId,                                -- ReportId - uniqueidentifier
            N'RadRptPage_Options',                    -- PageViewId - nvarchar(200)
            N'ParamPanelBarSetCustomOptions',         -- UserControlName - nvarchar(200)
            @titleIvSapNoticesSetId,                  -- SetId - bigint
            N'ParamSetPanelBarControl.ascx',          -- ControllerClass - nvarchar(200)
            N'Custom Option for Title IV SAP Notice', -- DisplayName - nvarchar(200)
            @titleIvSapNoticesSet,                    -- ParameterSetLookUp - nvarchar(200)
            N'Options'                                -- TabName - nvarchar(200)
            );
    END;


    --get hierarchy id
    DECLARE @HierarchyId UNIQUEIDENTIFIER =
            (
                SELECT TOP 1
                       HierarchyId
                FROM dbo.syNavigationNodes
                WHERE ResourceId = @fasapNoticeReportResourceId
            );

    --update hierarchyid for title iv notice for permissions
    UPDATE dbo.syMenuItems
    SET HierarchyId = @HierarchyId
    WHERE ResourceId = @fasapNoticeReportResourceId;



    --update parent id for title iv notice report
    UPDATE dbo.syMenuItems
    SET ParentId = @PageGroupId
    WHERE ResourceId = @fasapNoticeReportResourceId;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION TitleIVNoticeReportSetup;
    PRINT 'Failed transaction TitleIVNoticeReportSetup.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION TitleIVNoticeReportSetup;
    PRINT 'successful transaction TitleIVNoticeReportSetup.';
END;
GO
--=================================================================================================
-- End AD-7486 Generate Title IV SAP Notice Landing Page
--=================================================================================================

--========================================================================================================================
--START
-- AD-5806 Recalculate Title IV SAP Results - All Students
-- Set old FA Sap Check link to point to Recalculate Title IV page.
--START
--========================================================================================================================

BEGIN TRANSACTION RecalcTitleIV;

BEGIN TRY
    DECLARE @oldName VARCHAR(100) = 'FA SAP Check';
    DECLARE @linkName VARCHAR(100) = 'Recalculate Title IV';
    DECLARE @url VARCHAR(100) = '/FA/RecalculateTitleIV.aspx';
    IF EXISTS (SELECT 1 FROM dbo.syMenuItems WHERE DisplayName = @oldName)
    BEGIN
        UPDATE dbo.syMenuItems
        SET MenuName = @linkName,
            DisplayName = @linkName,
            Url = @url,
            ModDate = GETDATE()
        WHERE MenuName = @oldName
              AND DisplayName = @oldName;
    END;

    IF EXISTS (SELECT 1 FROM dbo.syResources WHERE Resource = @oldName)
    BEGIN
        UPDATE dbo.syResources
        SET Resource = @linkName,
            ResourceURL = '~' + @url,
            ModDate = GETDATE()
        WHERE Resource = @oldName;
    END;
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
    BEGIN
        ROLLBACK TRANSACTION RecalcTitleIV;
        PRINT 'Exception Ocurred! RecalcTitleIV';
    END;
END CATCH;

IF @@TRANCOUNT > 0
BEGIN
    COMMIT TRANSACTION RecalcTitleIV;
END;

GO
--========================================================================================================================
--END
-- AD-5806 Recalculate Title IV SAP Results - All Students
-- Set old FA Sap Check link to point to Recalculate Title IV page.
--END
--========================================================================================================================
--=================================================================================================
--START AD-8143: Add CMS ID to the Campus Setup page
--=================================================================================================
DECLARE @Error AS INTEGER;
SET @Error = 0;

BEGIN TRANSACTION addCmsIdColumn;
BEGIN TRY
    DECLARE @fldId INT;
    DECLARE @tblId INT;
    DECLARE @TblFldsId INT;
    DECLARE @FldCapId INT;
    DECLARE @ResDefId INT;
    IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'CmsId')
    BEGIN
        SET @fldId =
        (
            SELECT MAX(FldId) FROM syFields
        ) + 1;
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,   -- FldId - int
            'CmsId',  -- FldName - varchar(200)
            200,      -- FldTypeId - int
            7,        -- FldLen - int
            NULL,     -- DDLId - int
            0,        -- DerivedFld - bit
            0,        -- SchlReq - bit
            1,        -- LogChanges - bit
            '####-##' -- Mask - varchar(50)
            );
        IF (@@ERROR > 0)
        BEGIN
            SET @Error = @@ERROR;
        END;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'sycampuses'
        );
        IF NOT EXISTS
        (
            SELECT 1
            FROM syTblFlds
            WHERE TblId = @tblId
                  AND FldId = @fldId
        )
        BEGIN
            SET @TblFldsId =
            (
                SELECT MAX(TblFldsId) FROM syTblFlds
            ) + 1;
            INSERT INTO syTblFlds
            (
                TblFldsId,
                TblId,
                FldId,
                CategoryId,
                FKColDescrip
            )
            VALUES
            (   @TblFldsId, -- TblFldsId - int
                @tblId,     -- TblId - int
                @fldId,     -- FldId - int
                NULL,       -- CategoryId - int
                NULL        -- FKColDescrip - varchar(50)
                );
            IF (@@ERROR > 0)
            BEGIN
                SET @Error = @@ERROR;
            END;
        END;
        IF NOT EXISTS (SELECT 1 FROM syFldCaptions WHERE FldId = @fldId)
        BEGIN
            SET @FldCapId =
            (
                SELECT MAX(FldCapId) FROM syFldCaptions
            ) + 1;
            INSERT INTO syFldCaptions
            (
                FldCapId,
                FldId,
                LangId,
                Caption,
                FldDescrip
            )
            VALUES
            (   @FldCapId,                   -- FldCapId - int
                @fldId,                      -- FldId - int
                1,                           -- LangId - tinyint
                'CMS ID',                    -- Caption - varchar(100)
                'CMS Id fro AFA integration' -- FldDescrip - varchar(150)
                );
            IF (@@ERROR > 0)
            BEGIN
                SET @Error = @@ERROR;
            END;
        END;
        IF NOT EXISTS
        (
            SELECT 1
            FROM syResTblFlds
            WHERE TblFldsId = @TblFldsId
                  AND ResourceId = 1
        )
        BEGIN
            SET @ResDefId =
            (
                SELECT MAX(ResDefId) FROM syResTblFlds
            ) + 1;
            INSERT INTO syResTblFlds
            (
                ResDefId,
                ResourceId,
                TblFldsId,
                Required,
                SchlReq,
                ControlName,
                UsePageSetup
            )
            VALUES
            (   @ResDefId,  -- ResDefId - int
                1,          -- ResourceId - smallint
                @TblFldsId, -- TblFldsId - int
                1,          -- Required - bit
                0,          -- SchlReq - bit
                NULL,       -- ControlName - varchar(50)
                1           -- UsePageSetup - bit
                );
            IF (@@ERROR > 0)
            BEGIN
                SET @Error = @@ERROR;
            END;
        END;
    END;
END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @Error = 1;
END CATCH;

IF (@Error = 0)
BEGIN
    COMMIT TRANSACTION addCmsIdColumn;
END;
ELSE
BEGIN
    ROLLBACK TRANSACTION addCmsIdColumn;
END;
GO
--=================================================================================================
--END AD-8143: Add CMS ID to the Campus Setup page
--=================================================================================================
--=================================================================================================
-- START AD-8062 Allow Klass App to Get a Progress Report from Advantage via API
-- INSERT into sySysRoles table the record for "Klass App" with RoleTypeId = 2 (vendor) and SysRoleId = 19
--=================================================================================================
DECLARE @KlassApp VARCHAR(50) = 'Klass App';
DECLARE @Error AS INTEGER;
SET @Error = 0;

BEGIN TRANSACTION addKlassAppRole;
BEGIN TRY

    IF NOT EXISTS (SELECT 1 FROM dbo.sySysRoles WHERE SysRoleId = 19)
    BEGIN
        INSERT INTO dbo.sySysRoles
        (
            SysRoleId,
            Descrip,
            StatusId,
            ModUser,
            ModDate,
            RoleTypeId,
            Permission
        )
        VALUES
        (   19,                                     -- SysRoleId - int
            @KlassApp,                              -- Descrip - varchar(80)
            'F23DE1E2-D90A-4720-B4C7-0F6FB09C9965', -- StatusId - uniqueidentifier
            'SUPPORT',                              -- ModUser - varchar(50)
            GETDATE(),                              -- ModDate - datetime
            2, '{"modules": [ {"name": "KlassApp","level": "read"}, {"name": "SY","level": "read"}]}');
    END;
    ELSE
    BEGIN
        UPDATE SSR
        SET SSR.Descrip = @KlassApp
        FROM sySysRoles AS SSR
        WHERE SSR.Descrip <> @KlassApp
              AND SSR.SysRoleId = 19;
    END;

END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @Error = 1;
END CATCH;

IF (@Error = 0)
BEGIN
    COMMIT TRANSACTION addKlassAppRole;
END;
ELSE
BEGIN
    ROLLBACK TRANSACTION addKlassAppRole;
END;
GO
--=================================================================================================
-- END AD-8062 Allow Klass App to Get a Progress Report from Advantage via API
-- INSERT into sySysRoles table the record for "Klass App" with RoleTypeId = 2 (vendor) and SysRoleId = 19
--=================================================================================================

--=================================================================================================
-- START AD-7318 Generate State Board Report links
-- INSERT into syResources 
-- INSERT into and UPDATE syMenuItems 
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION StateBoardReport;
BEGIN TRY
    DECLARE @StateBoardReportResourceId INT = 864;
    DECLARE @StateBoardReportMenuName VARCHAR(30) = 'State Board Report';
    DECLARE @StateBoardMenuName VARCHAR(30) = 'State Board';
    DECLARE @MenuParent1Id INT;
    DECLARE @MenuParent2Id INT;
    DECLARE @MenuItemId INT;


    --insert new resource id
    IF NOT EXISTS
    (
        SELECT TOP 1
               ResourceID
        FROM dbo.syResources
        WHERE ResourceID = @StateBoardReportResourceId
    )
    BEGIN
        INSERT INTO dbo.syResources
        (
            ResourceID,
            Resource,
            ResourceTypeID,
            ResourceURL,
            SummListId,
            ChildTypeId,
            ModDate,
            ModUser,
            AllowSchlReqFlds,
            MRUTypeId,
            UsedIn,
            TblFldsId,
            DisplayName
        )
        VALUES
        (   @StateBoardReportResourceId, -- ResourceID - smallint
            'State Board Report',        -- Resource - varchar(200)
            2,                           -- ResourceTypeID - tinyint
            NULL,                        -- ResourceURL - varchar(100)
            0,                           -- SummListId - smallint
            5,                           -- ChildTypeId - tinyint
            GETDATE(),                   -- ModDate - datetime
            NULL,                        -- ModUser - varchar(50)
            0,                           -- AllowSchlReqFlds - bit
            1,                           -- MRUTypeId - smallint
            207,                         -- UsedIn - int
            NULL,                        -- TblFldsId - int
            NULL                         -- DisplayName - varchar(200)
            );
    END;

    SET @MenuParent1Id =
    (
        SELECT MenuItemId
        FROM dbo.syMenuItems
        WHERE MenuName = 'Reports'
              AND Url IS NULL
              AND MenuItemTypeId = 1
    );
    SET @MenuParent2Id =
    (
        SELECT MenuItemId
        FROM dbo.syMenuItems
        WHERE MenuName = 'Academics'
              AND ParentId = @MenuParent1Id
    );

    IF NOT EXISTS
    (
        SELECT TOP 1
               MenuItemId
        FROM dbo.syMenuItems
        WHERE MenuName = @StateBoardReportMenuName
    )
    BEGIN
        INSERT INTO dbo.syMenuItems
        (
            MenuName,
            DisplayName,
            Url,
            MenuItemTypeId,
            ParentId,
            DisplayOrder,
            IsPopup,
            ModDate,
            ModUser,
            IsActive,
            ResourceId,
            HierarchyId,
            ModuleCode,
            MRUType,
            HideStatusBar
        )
        VALUES
        (   @StateBoardReportMenuName,   -- MenuName - varchar(250)
            @StateBoardReportMenuName,   -- DisplayName - varchar(250)
            NULL,                        -- Url - nvarchar(250)
            3,                           -- MenuItemTypeId - smallint
            @MenuParent2Id,              -- ParentId - int
            400,                         -- DisplayOrder - int
            0,                           -- IsPopup - bit
            GETDATE(),                   -- ModDate - datetime
            NULL,                        -- ModUser - varchar(50)
            1,                           -- IsActive - bit
            @StateBoardReportResourceId, -- ResourceId - smallint
            NULL,                        -- HierarchyId - uniqueidentifier
            NULL,                        -- ModuleCode - varchar(5)
            NULL,                        -- MRUType - int
            NULL                         -- HideStatusBar - bit
            );
    END;
    SET @MenuItemId =
    (
        SELECT MenuItemId
        FROM dbo.syMenuItems
        WHERE MenuName = @StateBoardReportMenuName
              AND ResourceId = @StateBoardReportResourceId
    );
    UPDATE dbo.syMenuItems
    SET ParentId = @MenuItemId
    WHERE MenuName = @StateBoardMenuName;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION StateBoardReport;
    PRINT 'Failed transcation State Board Report.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION StateBoardReport;
    PRINT 'successful transaction State Board Report.';
END;
GO
--=================================================================================================
-- End AD-7318 Generate State Board Report links
--=================================================================================================

--=================================================================================================
-- START AD-7318 Generate Navigation State Board Report
-- INSERT into and UPDATE syNavigationNodes
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION NavigationStateBoardReport;
BEGIN TRY
    DECLARE @StateBoardReportResourceId INT = 864;
    DECLARE @parentId AS UNIQUEIDENTIFIER;
    DECLARE @heirarcyId AS UNIQUEIDENTIFIER;
    SET @heirarcyId = NEWID();
    SET @parentId =
    (
        SELECT ParentId
        FROM syNavigationNodes
            INNER JOIN syResources
                ON syResources.ResourceID = syNavigationNodes.ResourceId
        WHERE Resource = 'Class Schedule Reports'
    );
    --insert new resource id
    IF NOT EXISTS
    (
        SELECT TOP 1
               ResourceId
        FROM dbo.syNavigationNodes
        WHERE ResourceId = @StateBoardReportResourceId
    )
    BEGIN
        INSERT INTO dbo.syNavigationNodes
        (
            HierarchyId,
            HierarchyIndex,
            ResourceId,
            ParentId,
            ModUser,
            ModDate,
            IsPopupWindow,
            IsShipped
        )
        VALUES
        (   @heirarcyId,                 -- HierarchyId - uniqueidentifier
            1,                           -- HierarchyIndex - smallint
            @StateBoardReportResourceId, -- ResourceId - smallint
            @parentId,                   -- ParentId - uniqueidentifier
            'Support',                   -- ModUser - varchar(50)
            GETDATE(),                   -- ModDate - datetime
            0,                           -- IsPopupWindow - bit
            1                            -- IsShipped - bit
            );

        UPDATE dbo.syNavigationNodes
        SET ParentId = @heirarcyId
        WHERE ResourceId = 808;
    END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION NavigationStateBoardReport;
    PRINT 'Failed transcation Generate Navigation State Board Report.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION NavigationStateBoardReport;
    PRINT 'successful transaction Generate Navigation State Board Report.';
END;
GO
--=================================================================================================
-- End AD-7318 Generate Navigation State Board Report
--=================================================================================================
--=================================================================================================
-- START AD-7992 State board settings initial page
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION StateBoardSettingsPageSetup;
BEGIN TRY
    DECLARE @StateBoardSettingsPageSetupResourceId INT = 863;
    DECLARE @StaetBoardSettingsMenuName VARCHAR(30) = 'State Board Settings';
    DECLARE @maintenanceMenuId INT;
    DECLARE @maintenanceSystemMenuId INT;
    DECLARE @maintenanceSystemGeneralOptionsMenuId INT;
    DECLARE @StateBoardReportid UNIQUEIDENTIFIER;
    DECLARE @IndiateStateId UNIQUEIDENTIFIER;

    SET @maintenanceMenuId =
    (
        SELECT TOP 1
               MenuItemId
        FROM dbo.syMenuItems
        WHERE DisplayName = 'Maintenance'
              AND ParentId IS NULL
    );

    SET @maintenanceSystemMenuId =
    (
        SELECT TOP 1
               MenuItemId
        FROM dbo.syMenuItems
        WHERE DisplayName = 'System'
              AND ParentId = @maintenanceMenuId
    );

    SET @maintenanceSystemGeneralOptionsMenuId =
    (
        SELECT TOP 1
               MenuItemId
        FROM dbo.syMenuItems
        WHERE DisplayName = 'General Options'
              AND ParentId = @maintenanceSystemMenuId
    );


    --insert new resource id
    IF NOT EXISTS
    (
        SELECT TOP 1
               ResourceID
        FROM dbo.syResources
        WHERE ResourceID = @StateBoardSettingsPageSetupResourceId
    )
    BEGIN
        INSERT INTO dbo.syResources
        (
            ResourceID,
            Resource,
            ResourceTypeID,
            ResourceURL,
            SummListId,
            ChildTypeId,
            ModDate,
            ModUser,
            AllowSchlReqFlds,
            MRUTypeId,
            UsedIn,
            TblFldsId,
            DisplayName
        )
        VALUES
        (   @StateBoardSettingsPageSetupResourceId, -- ResourceID - smallint
            'StateBoardSettings',                   -- Resource - varchar(200)
            5,                                      -- ResourceTypeID - tinyint
            '~/SY/StateBoardSettings.aspx',         -- ResourceURL - varchar(100)
            0,                                      -- SummListId - smallint
            0,                                      -- ChildTypeId - tinyint
            GETDATE(),                              -- ModDate - datetime
            'sa',                                   -- ModUser - varchar(50)
            NULL,                                   -- AllowSchlReqFlds - bit
            0,                                      -- MRUTypeId - smallint
            975,                                    -- UsedIn - int
            0,                                      -- TblFldsId - int
            ''                                      -- DisplayName - varchar(200)
            );
    END;

    IF NOT EXISTS
    (
        SELECT TOP 1
               MenuItemId
        FROM dbo.syMenuItems
        WHERE MenuName = @StaetBoardSettingsMenuName
    )
    BEGIN
        INSERT INTO dbo.syMenuItems
        (
            MenuName,
            DisplayName,
            Url,
            MenuItemTypeId,
            ParentId,
            DisplayOrder,
            IsPopup,
            ModDate,
            ModUser,
            IsActive,
            ResourceId,
            HierarchyId,
            ModuleCode,
            MRUType,
            HideStatusBar
        )
        VALUES
        (   @StaetBoardSettingsMenuName,            -- MenuName - varchar(250)
            @StaetBoardSettingsMenuName,            -- DisplayName - varchar(250)
            N'/sy/StateBoardSettings.aspx',         -- Url - nvarchar(250)
            4,                                      -- MenuItemTypeId - smallint
            @maintenanceSystemGeneralOptionsMenuId, -- ParentId - int
            1400,                                   -- DisplayOrder - int
            0,                                      -- IsPopup - bit
            GETDATE(),                              -- ModDate - datetime
            'sa',                                   -- ModUser - varchar(50)
            1,                                      -- IsActive - bit
            @StateBoardSettingsPageSetupResourceId, -- ResourceId - smallint
            NULL,                                   -- HierarchyId - uniqueidentifier
            '',                                     -- ModuleCode - varchar(5)
            0,                                      -- MRUType - int
            NULL                                    -- HideStatusBar - bit
            );
    END;

    UPDATE dbo.syStates
    SET IsStateBoard = 1
    WHERE StateDescrip = 'Indiana';

    SET @IndiateStateId =
    (
        SELECT TOP 1 StateId FROM syStates WHERE StateDescrip = 'Indiana'
    );
    SET @StateBoardReportid =
    (
        SELECT TOP 1
               ReportId
        FROM dbo.syReports
        WHERE ReportName = 'StateBoardReport'
    );

    IF (@IndiateStateId IS NOT NULL AND @StateBoardReportid IS NOT NULL)
    BEGIN

        IF NOT EXISTS
        (
            SELECT TOP 1
                   StateReportId
            FROM dbo.syStateReports
            WHERE StateId = @IndiateStateId
                  AND ReportId = @StateBoardReportid
        )
        BEGIN
            INSERT INTO dbo.syStateReports
            (
                StateId,
                ReportId,
                ModifiedDate,
                ModifiedUser
            )
            VALUES
            (   @IndiateStateId,     -- StateId - uniqueidentifier
                @StateBoardReportid, -- ReportId - uniqueidentifier
                GETDATE(),           -- ModifiedDate - datetime
                'sa'                 -- ModifiedUser - varchar(50)
                );
        END;


    END;


END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION StateBoardSettingsPageSetup;
    PRINT 'Failed transcation StateBoardSettingsPageSetup.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION StateBoardSettingsPageSetup;
    PRINT 'successful transaction StateBoardSettingsPageSetup.';
END;
GO
--=================================================================================================
-- END AD-7992 State board settings initial page
--=================================================================================================
--=================================================================================================
-- START AD-8010 Campus details section on 'State Board Settings' page
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION StateBoardCampusDetailUpdates;
BEGIN TRY
    -- update country code and country description
    UPDATE dbo.adCountries
    SET CountryCode = 'USA',
        CountryDescrip = 'USA'
    WHERE CountryCode IN ( 'USA', 'US', 'United States', 'United States of America' );

    --get the country id for USA
    DECLARE @countryId UNIQUEIDENTIFIER;
    SET @countryId =
    (
        SELECT TOP 1 CountryId FROM dbo.adCountries WHERE CountryCode = 'USA'
    );

    --update states with country id
    UPDATE dbo.syStates
    SET CountryId = @countryId
    WHERE StateDescrip IN ( 'South Carolina', 'Palau', 'Iowa', 'North Carolina', 'Wisconsin', 'Washington', 'Oklahoma',
                            'Utah', 'Rhode Island', 'Ohio', 'Delaware', 'Oregon', 'California', 'Massachusetts',
                            'North Dakota', 'Tennessee', 'Hawaii', 'Kentucky', 'Northern Marianas', 'Wyoming', 'Texas',
                            'New Jersey', 'American Samoa', 'Kansas', 'Missouri', 'Nebraska',
                            'Federated States of Micronesia', 'Virgin Islands', 'Minnesota', 'South Dakota',
                            'Illinois', 'Alaska', 'Puerto Rico', 'New Mexico', 'Indiana', 'Georgia', 'Arkansas',
                            'Marshall Islands', 'Pennsylvania', 'Maryland', 'Mississippi', 'Florida', 'Connecticut',
                            'Montana', 'Virginia', 'West Virginia', 'Colorado', 'Alabama', 'New Hampshire', 'Vermont',
                            'Arizona', 'Maine', 'Louisiana', 'New York', 'Nevada', 'District of Columbia', 'Guam',
                            'Idaho', 'Michigan'
                          );
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION StateBoardCampusDetailUpdates;
    PRINT 'Failed transcation StateBoardCampusDetailUpdates.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION StateBoardCampusDetailUpdates;
    PRINT 'successful transaction StateBoardCampusDetailUpdates.';
END;
GO
--=================================================================================================
-- END AD-8010 Campus details section on 'State Board Settings' page
--=================================================================================================
--=================================================================================================
-- START AD-8094 State board licensing agency details section on 'State Board Settings' page
--=================================================================================================
DECLARE @error AS INT;
DECLARE @statusId AS UNIQUEIDENTIFIER;
DECLARE @stateId AS UNIQUEIDENTIFIER;
DECLARE @userId AS UNIQUEIDENTIFIER;

SET @error = 0;
BEGIN TRANSACTION StateBoardAgencyeSetup;
BEGIN TRY
    SET @stateId =
    (
        SELECT TOP 1 StateId FROM dbo.syStates WHERE StateDescrip = 'Indiana'
    );
    DECLARE @countryId UNIQUEIDENTIFIER;
    SET @countryId =
    (
        SELECT TOP 1 CountryId FROM dbo.adCountries WHERE CountryCode = 'USA'
    );
    SET @userId =
    (
        SELECT TOP 1
               UserId
        FROM syUsers
        WHERE IsAdvantageSuperUser = 1
              OR UserName = 'Support'
    );

    SET @statusId =
    (
        SELECT StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
    );
    IF NOT EXISTS
    (
        SELECT 1
        FROM dbo.syStateBoardAgencies
        WHERE Description = 'INDIANA PROFESSIONAL LICENSING AGENCY'
              AND StatusId = @statusId
              AND StateId = @stateId
    )
    BEGIN

        INSERT INTO dbo.syStateBoardAgencies
        (
            Code,
            Description,
            StatusId,
            StateId,
            LicensingAddress1,
            LicensingCountryId,
            LicensingStateId,
            LicensingCity,
            LicensingZipCode,
            CreatedById,
            CreatedDate
        )
        VALUES
        ('IPLA', 'INDIANA PROFESSIONAL LICENSING AGENCY', @statusId, @stateId, '402 West Washington Street',
         @countryId, @stateId, 'Indianapolis', '46204', @userId, GETDATE());

    END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION StateBoardAgencyeSetup;
    PRINT 'Failed transcation StateBoardAgencyeSetup.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION StateBoardAgencyeSetup;
    PRINT 'successful transaction StateBoardAgencyeSetup.';
END;
GO
--=================================================================================================
-- END AD-8094 State board licensing agency details section on 'State Board Settings' page
--=================================================================================================
--=================================================================================================
--=================================================================================================
-- START AD-8095 'Campus program version mapping to licensing agency course code' section on 'State Board Settings' page
--=================================================================================================
DECLARE @error AS INT;
DECLARE @statusId AS UNIQUEIDENTIFIER;
DECLARE @userId AS UNIQUEIDENTIFIER;
DECLARE @stateId AS UNIQUEIDENTIFIER;
SET @error = 0;
BEGIN TRANSACTION StateBoardCoursesSetup;
BEGIN TRY
    SET @stateId =
    (
        SELECT TOP 1 StateId FROM dbo.syStates WHERE StateDescrip = 'Indiana'
    );
    SET @statusId =
    (
        SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
    );
    SET @userId =
    (
        SELECT TOP 1
               UserId
        FROM dbo.syUsers
        WHERE IsAdvantageSuperUser = 1
              OR UserName = 'Support'
    );
    IF NOT EXISTS
    (
        SELECT 1
        FROM dbo.syStateBoardCourses
        WHERE Description = 'Cosmetology'
              AND StateId = @stateId
              AND StatusId = @statusId
    )
    BEGIN
        INSERT INTO dbo.syStateBoardCourses
        (
            Code,
            Description,
            StatusId,
            StateId,
            CreatedById,
            CreatedDate
        )
        VALUES
        ('C', 'Cosmetology', @statusId, @stateId, @userId, GETDATE());
    END;
    IF NOT EXISTS
    (
        SELECT 1
        FROM dbo.syStateBoardCourses
        WHERE Description = 'Manicure'
              AND StateId = @stateId
              AND StatusId = @statusId
    )
    BEGIN
        INSERT INTO dbo.syStateBoardCourses
        (
            Code,
            Description,
            StatusId,
            StateId,
            CreatedById,
            CreatedDate
        )
        VALUES
        ('M', 'Manicure', @statusId, @stateId, @userId, GETDATE());
    END;
    IF NOT EXISTS
    (
        SELECT 1
        FROM dbo.syStateBoardCourses
        WHERE Description = 'Esthetics'
              AND StateId = @stateId
              AND StatusId = @statusId
    )
    BEGIN
        INSERT INTO dbo.syStateBoardCourses
        (
            Code,
            Description,
            StatusId,
            StateId,
            CreatedById,
            CreatedDate
        )
        VALUES
        ('ES', 'Esthetics', @statusId, @stateId, @userId, GETDATE());
    END;
    IF NOT EXISTS
    (
        SELECT 1
        FROM dbo.syStateBoardCourses
        WHERE Description = 'Instructor Training'
              AND StateId = @stateId
              AND StatusId = @statusId
    )
    BEGIN
        INSERT INTO dbo.syStateBoardCourses
        (
            Code,
            Description,
            StatusId,
            StateId,
            CreatedById,
            CreatedDate
        )
        VALUES
        ('IT', 'Instructor Training', @statusId, @stateId, @userId, GETDATE());
    END;
    IF NOT EXISTS
    (
        SELECT 1
        FROM dbo.syStateBoardCourses
        WHERE Description = 'Shampoo'
              AND StateId = @stateId
              AND StatusId = @statusId
    )
    BEGIN
        INSERT INTO dbo.syStateBoardCourses
        (
            Code,
            Description,
            StatusId,
            StateId,
            CreatedById,
            CreatedDate
        )
        VALUES
        ('S', 'Shampoo', @statusId, @stateId, @userId, GETDATE());
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION StateBoardCoursesSetup;
    PRINT 'Failed transaction StateBoardCoursesSetup.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION StateBoardCoursesSetup;
    PRINT 'successful transaction StateBoardCoursesSetup.';
END;
GO
--=================================================================================================
-- END AD-8095 'Campus program version mapping to licensing agency course code' section on 'State Board Settings' page
--=================================================================================================

--=================================================================================================
-- START AD 7321 Indiana State Board Repor
-- INSERT into syResources 
-- INSERT into syReports 
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION StateBoardReport_rdl;
BEGIN TRY
    DECLARE @StateBoardReportResourceId INT = 865;
    DECLARE @StateBoardReportName VARCHAR(30) = 'StateBoardReport';
    DECLARE @StateBoardNameDescription VARCHAR(30) = 'State Board Report';
    DECLARE @MenuParent1Id INT;
    DECLARE @MenuParent2Id INT;
    DECLARE @MenuItemId INT;
    DECLARE @StateBoardReportID UNIQUEIDENTIFIER;
    SET @StateBoardReportID = NEWID();

    --insert new resource id
    IF NOT EXISTS
    (
        SELECT TOP 1
               ResourceID
        FROM dbo.syResources
        WHERE ResourceID = @StateBoardReportResourceId
    )
    BEGIN
        INSERT INTO dbo.syResources
        (
            ResourceID,
            Resource,
            ResourceTypeID,
            ResourceURL,
            SummListId,
            ChildTypeId,
            ModDate,
            ModUser,
            AllowSchlReqFlds,
            MRUTypeId,
            UsedIn,
            TblFldsId,
            DisplayName
        )
        VALUES
        (   @StateBoardReportResourceId,  -- ResourceID - smallint
            'Indiana State Board Report', -- Resource - varchar(200)
            5,                            -- ResourceTypeID - tinyint
            '~/sy/ParamReport.aspx',      -- ResourceURL - varchar(100)
            0,                            -- SummListId - smallint
            0,                            -- ChildTypeId - tinyint
            GETDATE(),                    -- ModDate - datetime
            'sa',                         -- ModUser - varchar(50)
            0,                            -- AllowSchlReqFlds - bit
            1,                            -- MRUTypeId - smallint
            975,                          -- UsedIn - int
            NULL,                         -- TblFldsId - int
            NULL                          -- DisplayName - varchar(200)
            );
    END;


    --insert new Reports
    IF NOT EXISTS
    (
        SELECT TOP 1
               ResourceId
        FROM dbo.syReports
        WHERE ResourceId = @StateBoardReportResourceId
    )
    BEGIN
        INSERT INTO dbo.syReports
        (
            ReportId,
            ResourceId,
            ReportName,
            ReportDescription,
            RDLName,
            AssemblyFilePath,
            ReportClass,
            CreationMethod,
            WebServiceUrl,
            RecordLimit,
            AllowedExportTypes,
            ReportTabLayout,
            ShowPerformanceMsg,
            ShowFilterMode
        )
        VALUES
        (   @StateBoardReportID,                               -- ReportId - uniqueidentifier
            @StateBoardReportResourceId,                       -- ResourceId - int
            @StateBoardReportName,                             -- ReportName - varchar(50)
            @StateBoardNameDescription,                        -- ReportDescription - varchar(500)
            'StateBoard/IndianaStateBoardMain',                -- RDLName - varchar(200)
            '~/Bin/Reporting.dll',                             -- varchar(200)
            'FAME.Advantage.Reporting.Logic.StateBoardReport', -- ReportClass - varchar(200)
            'BuildReport',                                     -- CreationMethod - varchar(200)
            'BuildReport',                                     -- WebServiceUrl - varchar(200)
            600,                                               -- RecordLimit - bigint
            0,                                                 -- AllowedExportTypes - int
            1,                                                 -- ReportTabLayout - int
            0,                                                 -- ShowPerformanceMsg - bit
            0                                                  -- ShowFilterMode - bit
            );
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION StateBoardReport_rdl;
    PRINT 'Failed transcation State Board Report Resources for rdl.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION StateBoardReport_rdl;
    PRINT 'successful transaction State Board Report Resources for rdl.';
END;
GO
--=================================================================================================
-- End AD 7321 Indiana State Board Report
--=================================================================================================

--=================================================================================================
-- START AD 8757 Tech: Finish integration of the Indiana State Board report 
-- WITH the Report Engine so it the report can be generated

--=================================================================================================

DECLARE @errorStateBoardFixes AS INT;
SET @errorStateBoardFixes = 0;
BEGIN TRANSACTION StateBoardReport_Fixes;
BEGIN TRY
    --update both state reports to use same class (TN, IN)
    UPDATE syReports
    SET ReportClass = 'FAME.Advantage.Reporting.Logic.StateBoardReport'
    WHERE ResourceId IN ( 808, 865 );

    DECLARE @oldStateReportId UNIQUEIDENTIFIER;
    DECLARE @newStateBoardReportId UNIQUEIDENTIFIER;

    SET @oldStateReportId =
    (
        SELECT TOP 1 ReportId FROM dbo.syReports WHERE ResourceId = 808
    );
    SET @newStateBoardReportId =
    (
        SELECT TOP 1 ReportId FROM dbo.syReports WHERE ResourceId = 865
    );

    -- update old records inserted that were using old state report(TN)
    UPDATE dbo.syStateReports
    SET ReportId = @newStateBoardReportId
    WHERE ReportId = @oldStateReportId;

    UPDATE dbo.sySchoolStateBoardReports
    SET ReportId = @newStateBoardReportId
    WHERE ReportId = @oldStateReportId;


END TRY
BEGIN CATCH
    SET @errorStateBoardFixes = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @errorStateBoardFixes > 0
BEGIN
    ROLLBACK TRANSACTION StateBoardReport_Fixes;
    PRINT 'Failed transcation State Board Report Resources for rdl.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION StateBoardReport_Fixes;
    PRINT 'successful transaction State Board Report Resources for rdl.';
END;
GO
--=================================================================================================
-- End AD 8757 Tech: Finish integration of the Indiana State Board report 
-- WITH the Report Engine so it the report can be generated

--=================================================================================================
-- START AD-6119 Tech: Install Google Analytics in Advantage 
--=================================================================================================

BEGIN TRANSACTION UpdateGoogleAnalytics;
DECLARE @GoogleAnalyticsUpdateError AS INTEGER = 0;
BEGIN TRY
    IF NOT EXISTS
    (
        SELECT TOP 1
               SettingId
        FROM dbo.syConfigAppSettings
        WHERE KeyName = 'GoogleAnalyticsCode'
    )
    BEGIN

        DECLARE @GoogleAnalyticsCodeSettingId INT;
        SET IDENTITY_INSERT dbo.syConfigAppSettings OFF;
        INSERT INTO dbo.syConfigAppSettings
        (
            KeyName,
            Description,
            ModUser,
            ModDate,
            CampusSpecific,
            ExtraConfirmation
        )
        VALUES
        ('GoogleAnalyticsCode', 'Code for Google Analytics', 'sa', GETDATE(), 0, 0);

        SET @GoogleAnalyticsCodeSettingId =
        (
            SELECT TOP 1
                   SettingId
            FROM dbo.syConfigAppSettings
            WHERE KeyName = 'GoogleAnalyticsCode'
        );

        IF (@GoogleAnalyticsCodeSettingId > 0)
        BEGIN
            INSERT INTO dbo.syConfigAppSetValues
            (
                SettingId,
                CampusId,
                Value,
                ModUser,
                ModDate,
                Active
            )
            VALUES
            (@GoogleAnalyticsCodeSettingId, NULL, '', 'sa', GETDATE(), 1);
        END;
    END;

END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @GoogleAnalyticsUpdateError = 1;

END CATCH;
IF (@GoogleAnalyticsUpdateError = 0)
BEGIN
    COMMIT TRANSACTION UpdateGoogleAnalytics;
END;
ELSE
BEGIN
    ROLLBACK TRANSACTION UpdateGoogleAnalytics;
END;
GO


--=================================================================================================
-- END AD-6119 Tech: Install Google Analytics in Advantage 
--=================================================================================================
--=================================================================================================
--=================================================================================================
--=================================================================================================
-- START AD-8513: Create New AFA Integration Windows Service
--=================================================================================================
DECLARE @IdExtOperation INT;
DECLARE @IdAllowedServices INT;
DECLARE @IdExternalCompanies INT;
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION afaIntegration;
BEGIN TRY
    IF NOT EXISTS
    (
        SELECT 1
        FROM syWapiExternalOperationMode
        WHERE Code = 'AFA_INTEGRATION_SERVICE'
    )
    BEGIN
        INSERT INTO syWapiExternalOperationMode
        (
            Code,
            Description,
            IsActive
        )
        VALUES
        (   'AFA_INTEGRATION_SERVICE',                   -- Code - varchar(50)
            'This is for AFA and Advantage integration', -- Description - varchar(100)
            0                                            -- IsActive - bit
            );
        IF (@error > 0)
        BEGIN
            SET @error = 0;
        END;
    END;

    IF NOT EXISTS
    (
        SELECT 1
        FROM syWapiAllowedServices
        WHERE Code = 'AFA_INTEGRATION_GET_STUDENT_INFO'
    )
    BEGIN
        INSERT INTO syWapiAllowedServices
        (
            Code,
            Description,
            Url,
            IsActive
        )
        VALUES
        (   'AFA_INTEGRATION_GET_STUDENT_INFO',                          -- Code - varchar(50)
            'Service to get student/lead info from AFA',                 -- Description - varchar(100)
            'http://advqa.fameinc.com/AFAservices/QA/AFAService.WebApi', -- Url - varchar(2048)
            0                                                            -- IsActive - bit
            );
        IF (@error > 0)
        BEGIN
            SET @error = 0;
        END;
    END;

    SET @IdAllowedServices =
    (
        SELECT Id
        FROM syWapiAllowedServices
        WHERE Code = 'AFA_INTEGRATION_GET_STUDENT_INFO'
    );

    SET @IdExternalCompanies =
    (
        SELECT Id
        FROM syWapiExternalCompanies
        WHERE Code = 'FAME_2154673_default'
              AND IsActive = 1
    );
    IF NOT EXISTS
    (
        SELECT 1
        FROM syWapiBridgeExternalCompanyAllowedServices
        WHERE IdAllowedServices = @IdAllowedServices
    )
    BEGIN
        INSERT INTO syWapiBridgeExternalCompanyAllowedServices
        (
            IdExternalCompanies,
            IdAllowedServices
        )
        VALUES
        (   @IdExternalCompanies, -- IdExternalCompanies - int
            @IdAllowedServices    -- IdAllowedServices - int --syWapiAllowedServices
            );
        IF (@error > 0)
        BEGIN
            SET @error = 0;
        END;
        SET @IdExtOperation =
        (
            SELECT Id
            FROM syWapiExternalOperationMode
            WHERE Code = 'AFA_INTEGRATION_SERVICE'
        );
        IF NOT EXISTS
        (
            SELECT 1
            FROM syWapiSettings
            WHERE CodeOperation = 'AFA_INTEGRATION'
        )
        BEGIN
            INSERT INTO syWapiSettings
            (
                CodeOperation,
                ExternalUrl,
                IdExtCompany,
                IdExtOperation,
                IdAllowedService,
                IdSecondAllowedService,
                FirstAllowedServiceQueryString,
                SecondAllowedServiceQueryString,
                ConsumerKey,
                PrivateKey,
                OperationSecondTimeInterval,
                PollSecondForOnDemandOperation,
                FlagOnDemandOperation,
                FlagRefreshConfiguration,
                IsActive,
                DateMod,
                DateLastExecution,
                UserMod,
                UserName
            )
            VALUES
            (   'AFA_INTEGRATION',                                           -- CodeOperation - varchar(50)
                'http://advqa.fameinc.com/AFAservices/QA/AFAService.WebApi', -- ExternalUrl - varchar(2048)
                @IdExternalCompanies,                                        -- IdExtCompany - int
                @IdExtOperation,                                             -- IdExtOperation - int
                @IdAllowedServices,                                          -- IdAllowedService - int
                NULL,                                                        -- IdSecondAllowedService - int
                '',                                                          -- FirstAllowedServiceQueryString - varchar(200)
                '',                                                          -- SecondAllowedServiceQueryString - varchar(200)
                '24D41E6A-A42E-48A1-9BAD-A84B04B5505E',                      -- ConsumerKey - varchar(100)
                'pass',                                                      -- PrivateKey - varchar(100)
                60,                                                          -- OperationSecondTimeInterval - int
                10,                                                          -- PollSecondForOnDemandOperation - int
                0,                                                           -- FlagOnDemandOperation - bit
                0,                                                           -- FlagRefreshConfiguration - bit
                1,                                                           -- IsActive - bit
                SYSDATETIME(),                                               -- DateMod - datetime2(7)
                SYSDATETIME(),                                               -- DateLastExecution - datetime2(7)
                'SUPPORT',                                                   -- UserMod - varchar(50)
                'famedev');
            IF (@error > 0)
            BEGIN
                SET @error = 0;
            END;
        END;
    END;
    ---AdavantageAPI service
    IF NOT EXISTS
    (
        SELECT 1
        FROM syWapiExternalOperationMode
        WHERE Code = 'ADVANTAGE_API_SERVICE '
    )
    BEGIN
        INSERT INTO syWapiExternalOperationMode
        (
            Code,
            Description,
            IsActive
        )
        VALUES
        (   'ADVANTAGE_API_SERVICE',                                                                   -- Code - varchar(50)
            'This is for AFA and Advantage integration to use Advantage API for the lead integration', -- Description - varchar(100)
            0                                                                                          -- IsActive - bit
            );
        IF (@error > 0)
        BEGIN
            SET @error = 0;
        END;
    END;


    IF NOT EXISTS
    (
        SELECT 1
        FROM syWapiAllowedServices
        WHERE Code = 'ADVANTAGE_API'
    )
    BEGIN
        INSERT INTO syWapiAllowedServices
        (
            Code,
            Description,
            Url,
            IsActive
        )
        VALUES
        (   'ADVANTAGE_API',                                                 -- Code - varchar(50)
            'Service to insert/Update lead into Advantage',                  -- Description - varchar(100)
            'https://advqa.fameinc.com/Advantage/QA/develop/3.12.0/API/API', -- Url - varchar(2048)
            0                                                                -- IsActive - bit
            );
        IF (@error > 0)
        BEGIN
            SET @error = 0;
        END;
    END;


    SET @IdAllowedServices =
    (
        SELECT Id FROM syWapiAllowedServices WHERE Code = 'ADVANTAGE_API'
    );

    SET @IdExternalCompanies =
    (
        SELECT Id
        FROM syWapiExternalCompanies
        WHERE Code = 'FAME_2154673_default'
              AND IsActive = 1
    );
    IF NOT EXISTS
    (
        SELECT 1
        FROM syWapiBridgeExternalCompanyAllowedServices
        WHERE IdAllowedServices = @IdAllowedServices
    )
    BEGIN
        INSERT INTO syWapiBridgeExternalCompanyAllowedServices
        (
            IdExternalCompanies,
            IdAllowedServices
        )
        VALUES
        (   @IdExternalCompanies, -- IdExternalCompanies - int
            @IdAllowedServices    -- IdAllowedServices - int --syWapiAllowedServices
            );

        IF (@error > 0)
        BEGIN
            SET @error = 0;
        END;
        SET @IdExtOperation =
        (
            SELECT Id
            FROM syWapiExternalOperationMode
            WHERE Code = 'ADVANTAGE_API_SERVICE'
        );
        IF NOT EXISTS
        (
            SELECT 1
            FROM syWapiSettings
            WHERE CodeOperation = 'ADVANTAGE_API'
        )
        BEGIN
            INSERT INTO syWapiSettings
            (
                CodeOperation,
                ExternalUrl,
                IdExtCompany,
                IdExtOperation,
                IdAllowedService,
                IdSecondAllowedService,
                FirstAllowedServiceQueryString,
                SecondAllowedServiceQueryString,
                ConsumerKey,
                PrivateKey,
                OperationSecondTimeInterval,
                PollSecondForOnDemandOperation,
                FlagOnDemandOperation,
                FlagRefreshConfiguration,
                IsActive,
                DateMod,
                DateLastExecution,
                UserMod,
                UserName
            )
            VALUES
            (   'ADVANTAGE_API',                                                 -- CodeOperation - varchar(50)
                'https://advqa.fameinc.com/Advantage/QA/develop/3.12.0/API/API', -- ExternalUrl - varchar(2048)
                @IdExternalCompanies,                                            -- IdExtCompany - int
                @IdExtOperation,                                                 -- IdExtOperation - int
                @IdAllowedServices,                                              -- IdAllowedService - int
                NULL,                                                            -- IdSecondAllowedService - int
                '',                                                              -- FirstAllowedServiceQueryString - varchar(200)
                '',                                                              -- SecondAllowedServiceQueryString - varchar(200)
                'Tricoci312',                                                    -- ConsumerKey - varchar(100)
                'f@m3.Supp0rt!',                                                 -- PrivateKey - varchar(100)
                36000000,                                                        -- OperationSecondTimeInterval - int
                100000,                                                          -- PollSecondForOnDemandOperation - int
                0,                                                               -- FlagOnDemandOperation - bit
                0,                                                               -- FlagRefreshConfiguration - bit
                0,                                                               -- IsActive - bit
                SYSDATETIME(),                                                   -- DateMod - datetime2(7)
                SYSDATETIME(),                                                   -- DateLastExecution - datetime2(7)
                'SUPPORT',                                                       -- UserMod - varchar(50)
                'support@fameinc.com');
            IF (@error > 0)
            BEGIN
                SET @error = 0;
            END;
        END;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION afaIntegration;
    PRINT 'Failed transaction afaIntegration';

END;
ELSE
BEGIN
    COMMIT TRANSACTION afaIntegration;
    PRINT 'successful transaction afaIntegration.';
END;
GO
--=================================================================================================
-- END AD-8513: Create New AFA Integration Windows Service
--=================================================================================================
--=================================================================================================
-- START AD-8513: Create New AFA Integration Windows Service (insert canadian states)
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION InsertCanadianState;
BEGIN TRY
    DECLARE @activeStatusId UNIQUEIDENTIFIER;
    DECLARE @allCampusGrp UNIQUEIDENTIFIER;
    SET @activeStatusId =
    (
        SELECT StatusId FROM syStatuses WHERE StatusCode = 'A'
    );
    SET @allCampusGrp =
    (
        SELECT CampGrpId FROM syCampGrps WHERE CampGrpCode = 'All'
    );
    IF NOT EXISTS (SELECT 1 FROM syStates WHERE StateCode = 'AB')
    BEGIN
        INSERT INTO syStates
        (
            StateId,
            StateCode,
            StatusId,
            StateDescrip,
            CampGrpId,
            fipscode
        )
        VALUES
        (   NEWID(),         -- StateId - uniqueidentifier
            'AB',            -- StateCode - varchar(12)
            @activeStatusId, -- StatusId - uniqueidentifier
            'Alberta',       -- StateDescrip - varchar(80)
            @allCampusGrp,   -- CampGrpId - uniqueidentifier
            NULL             -- fipscode - int
            );
    END;

    IF NOT EXISTS (SELECT 1 FROM syStates WHERE StateCode = 'BC')
    BEGIN
        INSERT INTO syStates
        (
            StateId,
            StateCode,
            StatusId,
            StateDescrip,
            CampGrpId,
            fipscode
        )
        VALUES
        (   NEWID(),            -- StateId - uniqueidentifier
            'BC',               -- StateCode - varchar(12)
            @activeStatusId,    -- StatusId - uniqueidentifier
            'British Columbia', -- StateDescrip - varchar(80)
            @allCampusGrp,      -- CampGrpId - uniqueidentifier
            NULL                -- fipscode - int
            );
    END;
    IF NOT EXISTS (SELECT 1 FROM syStates WHERE StateCode = 'CZ')
    BEGIN
        INSERT INTO syStates
        (
            StateId,
            StateCode,
            StatusId,
            StateDescrip,
            CampGrpId,
            fipscode
        )
        VALUES
        (   NEWID(),         -- StateId - uniqueidentifier
            'CZ',            -- StateCode - varchar(12)
            @activeStatusId, -- StatusId - uniqueidentifier
            'Canal Zone',    -- StateDescrip - varchar(80)
            @allCampusGrp,   -- CampGrpId - uniqueidentifier
            NULL             -- fipscode - int
            );
    END;

    IF NOT EXISTS (SELECT 1 FROM syStates WHERE StateCode = 'MB')
    BEGIN
        INSERT INTO syStates
        (
            StateId,
            StateCode,
            StatusId,
            StateDescrip,
            CampGrpId,
            fipscode
        )
        VALUES
        (   NEWID(),         -- StateId - uniqueidentifier
            'MB',            -- StateCode - varchar(12)
            @activeStatusId, -- StatusId - uniqueidentifier
            'Manitoba',      -- StateDescrip - varchar(80)
            @allCampusGrp,   -- CampGrpId - uniqueidentifier
            NULL             -- fipscode - int
            );
    END;
    IF NOT EXISTS (SELECT 1 FROM syStates WHERE StateCode = 'AA')
    BEGIN
        INSERT INTO syStates
        (
            StateId,
            StateCode,
            StatusId,
            StateDescrip,
            CampGrpId,
            fipscode
        )
        VALUES
        (   NEWID(),             -- StateId - uniqueidentifier
            'AA',                -- StateCode - varchar(12)
            @activeStatusId,     -- StatusId - uniqueidentifier
            'Military-Americas', -- StateDescrip - varchar(80)
            @allCampusGrp,       -- CampGrpId - uniqueidentifier
            NULL                 -- fipscode - int
            );
    END;
    IF NOT EXISTS (SELECT 1 FROM syStates WHERE StateCode = 'AE')
    BEGIN
        INSERT INTO syStates
        (
            StateId,
            StateCode,
            StatusId,
            StateDescrip,
            CampGrpId,
            fipscode
        )
        VALUES
        (   NEWID(),           -- StateId - uniqueidentifier
            'AE',              -- StateCode - varchar(12)
            @activeStatusId,   -- StatusId - uniqueidentifier
            'Military-Europe', -- StateDescrip - varchar(80)
            @allCampusGrp,     -- CampGrpId - uniqueidentifier
            NULL               -- fipscode - int
            );
    END;
    IF NOT EXISTS (SELECT 1 FROM syStates WHERE StateCode = 'AP')
    BEGIN
        INSERT INTO syStates
        (
            StateId,
            StateCode,
            StatusId,
            StateDescrip,
            CampGrpId,
            fipscode
        )
        VALUES
        (   NEWID(),            -- StateId - uniqueidentifier
            'AP',               -- StateCode - varchar(12)
            @activeStatusId,    -- StatusId - uniqueidentifier
            'Military-Pacific', -- StateDescrip - varchar(80)
            @allCampusGrp,      -- CampGrpId - uniqueidentifier
            NULL                -- fipscode - int
            );
    END;
    IF NOT EXISTS (SELECT 1 FROM syStates WHERE StateCode = 'NB')
    BEGIN
        INSERT INTO syStates
        (
            StateId,
            StateCode,
            StatusId,
            StateDescrip,
            CampGrpId,
            fipscode
        )
        VALUES
        (   NEWID(),         -- StateId - uniqueidentifier
            'NB',            -- StateCode - varchar(12)
            @activeStatusId, -- StatusId - uniqueidentifier
            'New Brunswick', -- StateDescrip - varchar(80)
            @allCampusGrp,   -- CampGrpId - uniqueidentifier
            NULL             -- fipscode - int
            );
    END;
    IF NOT EXISTS (SELECT 1 FROM syStates WHERE StateCode = 'NF')
    BEGIN
        INSERT INTO syStates
        (
            StateId,
            StateCode,
            StatusId,
            StateDescrip,
            CampGrpId,
            fipscode
        )
        VALUES
        (   NEWID(),         -- StateId - uniqueidentifier
            'NF',            -- StateCode - varchar(12)
            @activeStatusId, -- StatusId - uniqueidentifier
            'Newfoundland',  -- StateDescrip - varchar(80)
            @allCampusGrp,   -- CampGrpId - uniqueidentifier
            NULL             -- fipscode - int
            );
    END;
    IF NOT EXISTS (SELECT 1 FROM syStates WHERE StateCode = 'NL')
    BEGIN
        INSERT INTO syStates
        (
            StateId,
            StateCode,
            StatusId,
            StateDescrip,
            CampGrpId,
            fipscode
        )
        VALUES
        (   NEWID(),                     -- StateId - uniqueidentifier
            'NL',                        -- StateCode - varchar(12)
            @activeStatusId,             -- StatusId - uniqueidentifier
            'Newfoundland And Labrador', -- StateDescrip - varchar(80)
            @allCampusGrp,               -- CampGrpId - uniqueidentifier
            NULL                         -- fipscode - int
            );
    END;
    IF NOT EXISTS (SELECT 1 FROM syStates WHERE StateCode = 'NT')
    BEGIN
        INSERT INTO syStates
        (
            StateId,
            StateCode,
            StatusId,
            StateDescrip,
            CampGrpId,
            fipscode
        )
        VALUES
        (   NEWID(),                 -- StateId - uniqueidentifier
            'NT',                    -- StateCode - varchar(12)
            @activeStatusId,         -- StatusId - uniqueidentifier
            'Northwest Territories', -- StateDescrip - varchar(80)
            @allCampusGrp,           -- CampGrpId - uniqueidentifier
            NULL                     -- fipscode - int
            );
    END;
    IF NOT EXISTS (SELECT 1 FROM syStates WHERE StateCode = 'NS')
    BEGIN
        INSERT INTO syStates
        (
            StateId,
            StateCode,
            StatusId,
            StateDescrip,
            CampGrpId,
            fipscode
        )
        VALUES
        (   NEWID(),         -- StateId - uniqueidentifier
            'NS',            -- StateCode - varchar(12)
            @activeStatusId, -- StatusId - uniqueidentifier
            'Nova Scotia',   -- StateDescrip - varchar(80)
            @allCampusGrp,   -- CampGrpId - uniqueidentifier
            NULL             -- fipscode - int
            );
    END;
    IF NOT EXISTS (SELECT 1 FROM syStates WHERE StateCode = 'NU')
    BEGIN
        INSERT INTO syStates
        (
            StateId,
            StateCode,
            StatusId,
            StateDescrip,
            CampGrpId,
            fipscode
        )
        VALUES
        (   NEWID(),         -- StateId - uniqueidentifier
            'NU',            -- StateCode - varchar(12)
            @activeStatusId, -- StatusId - uniqueidentifier
            'Nunavut',       -- StateDescrip - varchar(80)
            @allCampusGrp,   -- CampGrpId - uniqueidentifier
            NULL             -- fipscode - int
            );
    END;
    IF NOT EXISTS (SELECT 1 FROM syStates WHERE StateCode = 'ON')
    BEGIN
        INSERT INTO syStates
        (
            StateId,
            StateCode,
            StatusId,
            StateDescrip,
            CampGrpId,
            fipscode
        )
        VALUES
        (   NEWID(),         -- StateId - uniqueidentifier
            'ON',            -- StateCode - varchar(12)
            @activeStatusId, -- StatusId - uniqueidentifier
            'Ontario',       -- StateDescrip - varchar(80)
            @allCampusGrp,   -- CampGrpId - uniqueidentifier
            NULL             -- fipscode - int
            );
    END;
    IF NOT EXISTS (SELECT 1 FROM syStates WHERE StateCode = 'PE')
    BEGIN
        INSERT INTO syStates
        (
            StateId,
            StateCode,
            StatusId,
            StateDescrip,
            CampGrpId,
            fipscode
        )
        VALUES
        (   NEWID(),                -- StateId - uniqueidentifier
            'PE',                   -- StateCode - varchar(12)
            @activeStatusId,        -- StatusId - uniqueidentifier
            'Prince Edward Island', -- StateDescrip - varchar(80)
            @allCampusGrp,          -- CampGrpId - uniqueidentifier
            NULL                    -- fipscode - int
            );
    END;
    IF NOT EXISTS (SELECT 1 FROM syStates WHERE StateCode = 'QC')
    BEGIN
        INSERT INTO syStates
        (
            StateId,
            StateCode,
            StatusId,
            StateDescrip,
            CampGrpId,
            fipscode
        )
        VALUES
        (   NEWID(),         -- StateId - uniqueidentifier
            'QC',            -- StateCode - varchar(12)
            @activeStatusId, -- StatusId - uniqueidentifier
            'Quebec',        -- StateDescrip - varchar(80)
            @allCampusGrp,   -- CampGrpId - uniqueidentifier
            NULL             -- fipscode - int
            );
    END;
    IF NOT EXISTS (SELECT 1 FROM syStates WHERE StateCode = 'SK')
    BEGIN
        INSERT INTO syStates
        (
            StateId,
            StateCode,
            StatusId,
            StateDescrip,
            CampGrpId,
            fipscode
        )
        VALUES
        (   NEWID(),         -- StateId - uniqueidentifier
            'SK',            -- StateCode - varchar(12)
            @activeStatusId, -- StatusId - uniqueidentifier
            'Saskatchewan',  -- StateDescrip - varchar(80)
            @allCampusGrp,   -- CampGrpId - uniqueidentifier
            NULL             -- fipscode - int
            );
    END;
    IF NOT EXISTS (SELECT 1 FROM syStates WHERE StateCode = 'YT')
    BEGIN
        INSERT INTO syStates
        (
            StateId,
            StateCode,
            StatusId,
            StateDescrip,
            CampGrpId,
            fipscode
        )
        VALUES
        (   NEWID(),         -- StateId - uniqueidentifier
            'YT',            -- StateCode - varchar(12)
            @activeStatusId, -- StatusId - uniqueidentifier
            'Yukon',         -- StateDescrip - varchar(80)
            @allCampusGrp,   -- CampGrpId - uniqueidentifier
            NULL             -- fipscode - int
            );
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION InsertCanadianState;
    PRINT 'Failed transcation InsertCanadianState.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION InsertCanadianState;
    PRINT 'successful transaction InsertCanadianState.';
END;
GO
--=================================================================================================
-- END AD-8513: Create New AFA Integration Windows Service
--=================================================================================================
--=================================================================================================
-- START AD-8513: Create New AFA Integration Windows Service
-- Insert into the mapping tables
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION InsertAfaCatalogMapping;
BEGIN TRY
    DECLARE @tbId INT;
    DECLARE @modDate DATETIME;
    SET @modDate = GETDATE();
    DECLARE @AfaMappingId UNIQUEIDENTIFIER = NULL;
    SET @tbId =
    (
        SELECT TblId FROM syTables WHERE TblName = 'adGenders'
    );
    IF NOT EXISTS
    (
        SELECT *
        FROM AfaCatalogMapping
        WHERE TableId = @tbId
              AND AfaCode = '1'
    )
        INSERT INTO dbo.AfaCatalogMapping
        (
            Id,
            AfaCode,
            TableId,
            ModDate,
            Username
        )
        VALUES
        (NEWID(), '1', @tbId, @modDate, 'Support');
    IF (@@ERROR > 0)
    BEGIN
        SET @error = 1;
    END;
    IF NOT EXISTS
    (
        SELECT *
        FROM AfaCatalogMapping
        WHERE TableId = @tbId
              AND AfaCode = '2'
    )
        INSERT INTO dbo.AfaCatalogMapping
        (
            Id,
            AfaCode,
            TableId,
            ModDate,
            Username
        )
        VALUES
        (NEWID(), '2', @tbId, @modDate, 'Support');
    IF (@@ERROR > 0)
    BEGIN
        SET @error = 1;
    END;


    IF NOT EXISTS (SELECT 1 FROM adGenders WHERE AfaMappingId IS NOT NULL)
    BEGIN
        SET @AfaMappingId =
        (
            SELECT Id FROM AfaCatalogMapping WHERE TableId = @tbId AND AfaCode = '1'
        );
        UPDATE adGenders
        SET AfaMappingId = @AfaMappingId,
            ModUser = 'Support',
            ModDate = @modDate
        WHERE GenderCode IN ( 'M', 'Male' );
        SET @AfaMappingId =
        (
            SELECT Id FROM AfaCatalogMapping WHERE TableId = @tbId AND AfaCode = '2'
        );
        UPDATE adGenders
        SET AfaMappingId = @AfaMappingId,
            ModUser = 'Support',
            ModDate = @modDate
        WHERE GenderCode IN ( 'F', 'Female' );
    END;


    SET @tbId =
    (
        SELECT TblId FROM syTables WHERE TblName = 'adCitizenships'
    );
    IF NOT EXISTS
    (
        SELECT *
        FROM AfaCatalogMapping
        WHERE TableId = @tbId
              AND AfaCode = '1'
    )
        INSERT INTO dbo.AfaCatalogMapping
        (
            Id,
            AfaCode,
            TableId,
            ModDate,
            Username
        )
        VALUES
        (NEWID(), '1', @tbId, @modDate, 'Support');
    IF (@@ERROR > 0)
    BEGIN
        SET @error = 1;
    END;
    IF NOT EXISTS
    (
        SELECT *
        FROM AfaCatalogMapping
        WHERE TableId = @tbId
              AND AfaCode = '2'
    )
        INSERT INTO dbo.AfaCatalogMapping
        (
            Id,
            AfaCode,
            TableId,
            ModDate,
            Username
        )
        VALUES
        (NEWID(), '2', @tbId, @modDate, 'Support');
    IF (@@ERROR > 0)
    BEGIN
        SET @error = 1;
    END;
    IF NOT EXISTS
    (
        SELECT *
        FROM AfaCatalogMapping
        WHERE TableId = @tbId
              AND AfaCode = '3'
    )
        INSERT INTO dbo.AfaCatalogMapping
        (
            Id,
            AfaCode,
            TableId,
            ModDate,
            Username
        )
        VALUES
        (NEWID(), '3', @tbId, @modDate, 'Support');
    IF (@@ERROR > 0)
    BEGIN
        SET @error = 1;
    END;
    IF NOT EXISTS (SELECT 1 FROM adCitizenships WHERE AfaMappingId IS NOT NULL)
    BEGIN
        SET @AfaMappingId =
        (
            SELECT Id FROM AfaCatalogMapping WHERE TableId = @tbId AND AfaCode = '1'
        );
        UPDATE adCitizenships
        SET AfaMappingId = @AfaMappingId,
            ModUser = 'Support',
            ModDate = @modDate
        WHERE CitizenshipCode IN ( 'Citizen', 'USCITIZEN' );
        SET @AfaMappingId =
        (
            SELECT Id FROM AfaCatalogMapping WHERE TableId = @tbId AND AfaCode = '2'
        );
        UPDATE adCitizenships
        SET AfaMappingId = @AfaMappingId,
            ModUser = 'Support',
            ModDate = @modDate
        WHERE CitizenshipCode IN ( 'ELIGIBLENONC', 'ELIGIBLE', 'Eligible' );
        SET @AfaMappingId =
        (
            SELECT Id FROM AfaCatalogMapping WHERE TableId = @tbId AND AfaCode = '3'
        );
        UPDATE adCitizenships
        SET AfaMappingId = @AfaMappingId,
            ModUser = 'Support',
            ModDate = @modDate
        WHERE CitizenshipCode IN ( 'Non-Citizen' );
    END;


    SET @tbId =
    (
        SELECT TblId FROM syTables WHERE TblName = 'adMaritalStatus'
    );
    IF NOT EXISTS
    (
        SELECT *
        FROM AfaCatalogMapping
        WHERE TableId = @tbId
              AND AfaCode = '1'
    )
        INSERT INTO dbo.AfaCatalogMapping
        (
            Id,
            AfaCode,
            TableId,
            ModDate,
            Username
        )
        VALUES
        (NEWID(), '1', @tbId, @modDate, 'Support');
    IF (@@ERROR > 0)
    BEGIN
        SET @error = 1;
    END;
    IF NOT EXISTS
    (
        SELECT *
        FROM AfaCatalogMapping
        WHERE TableId = @tbId
              AND AfaCode = '2'
    )
        INSERT INTO dbo.AfaCatalogMapping
        (
            Id,
            AfaCode,
            TableId,
            ModDate,
            Username
        )
        VALUES
        (NEWID(), '2', @tbId, @modDate, 'Support');
    IF (@@ERROR > 0)
    BEGIN
        SET @error = 1;
    END;
    IF NOT EXISTS
    (
        SELECT *
        FROM AfaCatalogMapping
        WHERE TableId = @tbId
              AND AfaCode = '3'
    )
        INSERT INTO dbo.AfaCatalogMapping
        (
            Id,
            AfaCode,
            TableId,
            ModDate,
            Username
        )
        VALUES
        (NEWID(), '3', @tbId, @modDate, 'Support');
    IF (@@ERROR > 0)
    BEGIN
        SET @error = 1;
    END;
    IF NOT EXISTS
    (
        SELECT *
        FROM AfaCatalogMapping
        WHERE TableId = @tbId
              AND AfaCode = '4'
    )
        INSERT INTO dbo.AfaCatalogMapping
        (
            Id,
            AfaCode,
            TableId,
            ModDate,
            Username
        )
        VALUES
        (NEWID(), '4', @tbId, @modDate, 'Support');
    IF (@@ERROR > 0)
    BEGIN
        SET @error = 1;
    END;
    IF NOT EXISTS
    (
        SELECT 1
        FROM adMaritalStatus
        WHERE AfaMappingId IS NOT NULL
    )
    BEGIN
        SET @AfaMappingId =
        (
            SELECT Id FROM AfaCatalogMapping WHERE TableId = @tbId AND AfaCode = '1'
        );
        UPDATE adMaritalStatus
        SET AfaMappingId = @AfaMappingId,
            ModUser = 'Support',
            ModDate = @modDate
        WHERE MaritalStatDescrip IN ( 'Single' );
        SET @AfaMappingId =
        (
            SELECT Id FROM AfaCatalogMapping WHERE TableId = @tbId AND AfaCode = '2'
        );
        UPDATE adMaritalStatus
        SET AfaMappingId = @AfaMappingId,
            ModUser = 'Support',
            ModDate = @modDate
        WHERE MaritalStatDescrip IN ( 'Married' );
        SET @AfaMappingId =
        (
            SELECT Id FROM AfaCatalogMapping WHERE TableId = @tbId AND AfaCode = '3'
        );
        UPDATE adMaritalStatus
        SET AfaMappingId = @AfaMappingId,
            ModUser = 'Support',
            ModDate = @modDate
        WHERE MaritalStatDescrip IN ( 'Separated' );
        SET @AfaMappingId =
        (
            SELECT Id FROM AfaCatalogMapping WHERE TableId = @tbId AND AfaCode = '4'
        );
        UPDATE adMaritalStatus
        SET AfaMappingId = @AfaMappingId,
            ModUser = 'Support',
            ModDate = @modDate
        WHERE MaritalStatDescrip IN ( 'Divorced', 'Widowed' );
    END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION InsertAfaCatalogMapping;
    PRINT 'Failed transcation InsertAfaCatalogMapping.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION InsertAfaCatalogMapping;
    PRINT 'successful transaction InsertAfaCatalogMapping.';
END;
GO
--=================================================================================================
-- END AD-8513: Create New AFA Integration Windows Service
--==================================================================================================
--=================================================================================================
--START AD-8679 Add Completed Hours to Adhoc Enrollment
--=================================================================================================
DECLARE @Error AS INTEGER;
SET @Error = 0;

BEGIN TRANSACTION addCompletedHours;
BEGIN TRY
    DECLARE @fldId INT;
    DECLARE @tblId INT;
    DECLARE @TblFldsId INT;
    DECLARE @FldCapId INT;
    DECLARE @CatId INT;

    IF EXISTS (SELECT 1 FROM syFields WHERE FldName = 'Completed Hours')
    BEGIN
        UPDATE syFields
        SET FldName = 'CompletedHours'
        WHERE FldName = 'Completed Hours';
    END;
    ELSE
    BEGIN
        IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'CompletedHours')
        BEGIN
            SET @fldId =
            (
                SELECT MAX(FldId) FROM syFields
            ) + 1;
            INSERT INTO syFields
            (
                FldId,
                FldName,
                FldTypeId,
                FldLen,
                DDLId,
                DerivedFld,
                SchlReq,
                LogChanges,
                Mask
            )
            VALUES
            (   @fldId,           -- FldId - int
                'CompletedHours', -- FldName - varchar(200)
                3,                -- FldTypeId - int
                5,                -- FldLen - int
                NULL,             -- DDLId - int
                NULL,             -- DerivedFld - bit
                NULL,             -- SchlReq - bit
                NULL,             -- LogChanges - bit
                NULL              -- Mask - varchar(50)
                );
            IF (@@ERROR > 0)
            BEGIN
                SET @Error = @@ERROR;
            END;
            SET @tblId =
            (
                SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
            );
            IF NOT EXISTS
            (
                SELECT 1
                FROM syTblFlds
                WHERE TblId = @tblId
                      AND FldId = @fldId
            )
            BEGIN
                SET @TblFldsId =
                (
                    SELECT MAX(TblFldsId) FROM syTblFlds
                ) + 1;
                SET @CatId =
                (
                    SELECT CategoryId
                    FROM syFldCategories
                    WHERE Descrip = 'Enrollment'
                          AND EntityId = 394
                );

                INSERT INTO syTblFlds
                (
                    TblFldsId,
                    TblId,
                    FldId,
                    CategoryId,
                    FKColDescrip
                )
                VALUES
                (   @TblFldsId, -- TblFldsId - int
                    @tblId,     -- TblId - int
                    @fldId,     -- FldId - int
                    @CatId,     -- CategoryId - int 
                    NULL        -- FKColDescrip - varchar(50)
                    );
                IF (@@ERROR > 0)
                BEGIN
                    SET @Error = @@ERROR;
                END;
            END;
            IF NOT EXISTS (SELECT 1 FROM syFldCaptions WHERE FldId = @fldId)
            BEGIN
                SET @FldCapId =
                (
                    SELECT MAX(FldCapId) FROM syFldCaptions
                ) + 1;
                INSERT INTO syFldCaptions
                (
                    FldCapId,
                    FldId,
                    LangId,
                    Caption,
                    FldDescrip
                )
                VALUES
                (   @FldCapId,         -- FldCapId - int
                    @fldId,            -- FldId - int
                    1,                 -- LangId - tinyint
                    'Completed Hours', -- Caption - varchar(100)
                    NULL               -- FldDescrip - varchar(150)
                    );
                IF (@@ERROR > 0)
                BEGIN
                    SET @Error = @@ERROR;
                END;
            END;
            IF NOT EXISTS (SELECT 1 FROM syFieldCalculation WHERE FldId = @fldId)
                INSERT INTO dbo.syFieldCalculation
                (
                    FldId,
                    CalculationSql
                )
                VALUES
                (   @fldId, -- FldId - int
                    '(Select CAST(ISNULL(round(SUM(PresentDays + MakeupDays),2),0) AS decimal(18,2)) as CompletedHours
                                    FROM ((SELECT ISNULL( MAX(ActualRunningPresentDays)/60, 0) AS PresentDays, ISNULL(MAX(ActualRunningMakeupDays)/60,0) AS MakeupDays 
                                    FROM SyStudentAttendanceSummary WHERE StuEnrollId =arStuEnrollments.StuEnrollId AND ActualRunningPresentDays <> 9999 AND ActualRunningMakeupDays <> 9999))t1)as CompletedHours
                                    ' -- CalculationSql - varchar(8000)
                    );

        END;


    END;

END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @Error = 1;
END CATCH;

IF (@Error = 0)
BEGIN
    COMMIT TRANSACTION addCompletedHours;
END;
ELSE
BEGIN
    ROLLBACK TRANSACTION addCompletedHours;
END;
GO
--=================================================================================================
--END AD-8679 Add Completed Hours to Adhoc Enrollment
--=================================================================================================
--=================================================================================================
--START AD-8681 Add Attendance % to Adhoc Enrollment 
--=================================================================================================
DECLARE @Error AS INTEGER;
SET @Error = 0;

BEGIN TRANSACTION addAttendancePercent;
BEGIN TRY
    DECLARE @fldId INT;
    DECLARE @tblId INT;
    DECLARE @TblFldsId INT;
    DECLARE @FldCapId INT;
    DECLARE @CatId INT;
    DECLARE @ldName VARCHAR(50);
    SET @ldName = 'Attendance %';
    IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'AttendancePercent')
    BEGIN
        SET @fldId =
        (
            SELECT MAX(FldId) FROM syFields
        ) + 1;
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,              -- FldId - int
            'AttendancePercent', -- FldName - varchar(200)
            3,                   -- FldTypeId - int
            5,                   -- FldLen - int
            NULL,                -- DDLId - int
            NULL,                -- DerivedFld - bit
            NULL,                -- SchlReq - bit
            NULL,                -- LogChanges - bit
            NULL                 -- Mask - varchar(50)
            );
        IF (@@ERROR > 0)
        BEGIN
            SET @Error = @@ERROR;
        END;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        IF NOT EXISTS
        (
            SELECT 1
            FROM syTblFlds
            WHERE TblId = @tblId
                  AND FldId = @fldId
        )
        BEGIN
            SET @TblFldsId =
            (
                SELECT MAX(TblFldsId) FROM syTblFlds
            ) + 1;
            SET @CatId =
            (
                SELECT CategoryId
                FROM syFldCategories
                WHERE Descrip = 'Enrollment'
                      AND EntityId = 394
            );

            INSERT INTO syTblFlds
            (
                TblFldsId,
                TblId,
                FldId,
                CategoryId,
                FKColDescrip
            )
            VALUES
            (   @TblFldsId, -- TblFldsId - int
                @tblId,     -- TblId - int
                @fldId,     -- FldId - int
                @CatId,     -- CategoryId - int 
                NULL        -- FKColDescrip - varchar(50)
                );
            IF (@@ERROR > 0)
            BEGIN
                SET @Error = @@ERROR;
            END;
        END;
        IF NOT EXISTS (SELECT 1 FROM syFldCaptions WHERE FldId = @fldId)
        BEGIN
            SET @FldCapId =
            (
                SELECT MAX(FldCapId) FROM syFldCaptions
            ) + 1;
            INSERT INTO syFldCaptions
            (
                FldCapId,
                FldId,
                LangId,
                Caption,
                FldDescrip
            )
            VALUES
            (   @FldCapId, -- FldCapId - int
                @fldId,    -- FldId - int
                1,         -- LangId - tinyint
                @ldName,   -- Caption - varchar(100)
                NULL       -- FldDescrip - varchar(150)
                );
            IF (@@ERROR > 0)
            BEGIN
                SET @Error = @@ERROR;
            END;
        END;
        IF NOT EXISTS (SELECT 1 FROM syFieldCalculation WHERE FldId = @fldId)
            INSERT INTO dbo.syFieldCalculation
            (
                FldId,
                CalculationSql
            )
            VALUES
            (   @fldId, -- FldId - int
                '((SELECT CAST(ISNULL(ROUND((SELECT MAX(ActualRunningPresentDays + ActualRunningMakeupDays) FROM SyStudentAttendanceSummary WHERE StuEnrollId = arStuEnrollments.StuEnrollId) * 100.0 / MAX(ActualRunningScheduledDays), 1), 0)AS decimal(18,2)) as AttendancePercent FROM SyStudentAttendanceSummary WHERE StuEnrollId = arStuEnrollments.StuEnrollId AND ActualRunningScheduledDays > 0)) AS AttendancePercent'

                -- CalculationSql - varchar(8000)
                );

    END;
END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @Error = 1;
END CATCH;

IF (@Error = 0)
BEGIN
    COMMIT TRANSACTION addAttendancePercent;
END;
ELSE
BEGIN
    ROLLBACK TRANSACTION addAttendancePercent;
END;
GO
--=================================================================================================
--END AD-8681 Add Attendance % to Adhoc Enrollment 
--=================================================================================================
--=================================================================================================
-- START AD-8144 ssign the AFA Student ID to Matching Leads and Generate Exception List
--=================================================================================================
DECLARE @Error AS INTEGER;
SET @Error = 0;

BEGIN TRANSACTION addIntegrationPage;
BEGIN TRY
    DECLARE @modDate DATETIME;
    SET @modDate = GETDATE();
    IF NOT EXISTS
    (
        SELECT 1
        FROM syResources
        WHERE Resource = 'Integration'
              AND ResourceID = 866
    )
    BEGIN
        INSERT INTO syResources
        (
            ResourceID,
            Resource,
            ResourceTypeID,
            ResourceURL,
            SummListId,
            ChildTypeId,
            ModDate,
            ModUser,
            AllowSchlReqFlds,
            MRUTypeId,
            UsedIn,
            TblFldsId,
            DisplayName
        )
        VALUES
        (   866,           -- ResourceID - smallint
            'Integration', -- Resource - varchar(200)
            2,             -- ResourceTypeID - tinyint
            NULL,          -- ResourceURL - varchar(100)
            NULL,          -- SummListId - smallint
            4,             -- ChildTypeId - tinyint
            @modDate,      -- ModDate - datetime
            'Support',     -- ModUser - varchar(50)
            0,             -- AllowSchlReqFlds - bit
            NULL,          -- MRUTypeId - smallint
            975,           -- UsedIn - int
            NULL,          -- TblFldsId - int
            'Integration'  -- DisplayName - varchar(200)
            );
    END;
    IF NOT EXISTS
    (
        SELECT 1
        FROM syResources
        WHERE Resource = 'AFA Integration'
              AND ResourceID = 867
    )
    BEGIN
        INSERT INTO syResources
        (
            ResourceID,
            Resource,
            ResourceTypeID,
            ResourceURL,
            SummListId,
            ChildTypeId,
            ModDate,
            ModUser,
            AllowSchlReqFlds,
            MRUTypeId,
            UsedIn,
            TblFldsId,
            DisplayName
        )
        VALUES
        (   867,                        -- ResourceID - smallint
            'AFA Integration',          -- Resource - varchar(200)
            3,                          -- ResourceTypeID - tinyint
            '~/SY/AfaIntegration.aspx', -- ResourceURL - varchar(100)
            NULL,                       -- SummListId - smallint
            NULL,                       -- ChildTypeId - tinyint
            @modDate,                   -- ModDate - datetime
            'Support',                  -- ModUser - varchar(50)
            NULL,                       -- AllowSchlReqFlds - bit
            NULL,                       -- MRUTypeId - smallint
            975,                        -- UsedIn - int
            NULL,                       -- TblFldsId - int
            'AFA Integration'           -- DisplayName - varchar(200)
            );
    END;
    DECLARE @parentMenuId INT;
    DECLARE @parentParentId INT;
    IF NOT EXISTS (SELECT * FROM syMenuItems WHERE MenuName = 'Integration')
    BEGIN
        SET @parentParentId =
        (
            SELECT MenuItemId
            FROM syMenuItems
            WHERE MenuName = 'Maintenance'
                  AND MenuItemTypeId = 1
        );
        SET @parentMenuId =
        (
            SELECT MenuItemId
            FROM syMenuItems
            WHERE MenuName = 'System'
                  AND ParentId = @parentParentId
        );
        INSERT INTO syMenuItems
        (
            MenuName,
            DisplayName,
            Url,
            MenuItemTypeId,
            ParentId,
            DisplayOrder,
            IsPopup,
            ModDate,
            ModUser,
            IsActive,
            ResourceId,
            HierarchyId,
            ModuleCode,
            MRUType,
            HideStatusBar
        )
        VALUES
        (   'Integration', -- MenuName - varchar(250)
            'Integration', -- DisplayName - varchar(250)
            NULL,          -- Url - nvarchar(250)
            3,             -- MenuItemTypeId - smallint
            @parentMenuId, -- ParentId - int
            800,           -- DisplayOrder - int
            0,             -- IsPopup - bit
            @modDate,      -- ModDate - datetime
            'Support',     -- ModUser - varchar(50)
            1,             -- IsActive - bit
            866,           -- ResourceId - smallint
            NULL,          -- HierarchyId - uniqueidentifier
            NULL,          -- ModuleCode - varchar(5)
            NULL,          -- MRUType - int
            NULL           -- HideStatusBar - bit
            );
        IF NOT EXISTS
        (
            SELECT 1
            FROM syMenuItems
            WHERE MenuName = 'AFA Integration'
        )
        BEGIN
            SET @parentMenuId =
            (
                SELECT MenuItemId
                FROM syMenuItems
                WHERE MenuName = 'Integration'
                      AND Url IS NULL
            );
            INSERT INTO syMenuItems
            (
                MenuName,
                DisplayName,
                Url,
                MenuItemTypeId,
                ParentId,
                DisplayOrder,
                IsPopup,
                ModDate,
                ModUser,
                IsActive,
                ResourceId,
                HierarchyId,
                ModuleCode,
                MRUType,
                HideStatusBar
            )
            VALUES
            (   'AFA Integration',          -- MenuName - varchar(250)
                'AFA Integration',          -- DisplayName - varchar(250)
                N'/SY/AfaIntegration.aspx', -- Url - nvarchar(250)
                4,                          -- MenuItemTypeId - smallint
                @parentMenuId,              -- ParentId - int
                100,                        -- DisplayOrder - int
                0,                          -- IsPopup - bit
                @modDate,                   -- ModDate - datetime
                'Support',                  -- ModUser - varchar(50)
                1,                          -- IsActive - bit
                867,                        -- ResourceId - smallint
                NULL,                       -- HierarchyId - uniqueidentifier
                NULL,                       -- ModuleCode - varchar(5)
                NULL,                       -- MRUType - int
                NULL                        -- HideStatusBar - bit
                );
        END;
    END;
END TRY
BEGIN CATCH
    SET @Error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);

END CATCH;

IF (@Error = 0)
BEGIN
    COMMIT TRANSACTION addIntegrationPage;
END;
ELSE
BEGIN
    ROLLBACK TRANSACTION addIntegrationPage;
END;
GO
--=================================================================================================
-- END AD-8144 ssign the AFA Student ID to Matching Leads and Generate Exception List
--=================================================================================================
--=================================================================================================
-- START AD-9526  Access State Board Settings
-- INSERT into syNavigationNodes UPDATE syMenuItems and syResources
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION NavigationStateBoardSettings;
BEGIN TRY
    DECLARE @StateBoardReportResourceId INT = 863;
    DECLARE @parentId AS UNIQUEIDENTIFIER;
    DECLARE @heirarcyId AS UNIQUEIDENTIFIER;
    SET @heirarcyId = NEWID();
    SET @parentId =
    (
        SELECT ParentId
        FROM syNavigationNodes
            INNER JOIN syResources
                ON syResources.ResourceID = syNavigationNodes.ResourceId
        WHERE Resource = 'Upload Logo'
    );
    --insert new resource id
    IF NOT EXISTS
    (
        SELECT TOP 1
               ResourceId
        FROM dbo.syNavigationNodes
        WHERE ResourceId = @StateBoardReportResourceId
    )
    BEGIN
        INSERT INTO dbo.syNavigationNodes
        (
            HierarchyId,
            HierarchyIndex,
            ResourceId,
            ParentId,
            ModUser,
            ModDate,
            IsPopupWindow,
            IsShipped
        )
        VALUES
        (   @heirarcyId,                 -- HierarchyId - uniqueidentifier
            8,                           -- HierarchyIndex - smallint
            @StateBoardReportResourceId, -- ResourceId - smallint
            @parentId,                   -- ParentId - uniqueidentifier
            'Support',                   -- ModUser - varchar(50)
            GETDATE(),                   -- ModDate - datetime
            0,                           -- IsPopupWindow - bit
            1                            -- IsShipped - bit
            );
        UPDATE dbo.syMenuItems
        SET HierarchyId = @heirarcyId
        WHERE ResourceId = @StateBoardReportResourceId;
        UPDATE dbo.syResources
        SET ChildTypeId = NULL
        WHERE ResourceID = @StateBoardReportResourceId;
    END;


END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION NavigationStateBoardSettings;
    PRINT 'Failed transcation Generate Access State Board Settings.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION NavigationStateBoardSettings;
    PRINT 'successful transaction Generate Access State Board Settings.';
END;
GO
--=================================================================================================
-- End AD-9526  Access State Board Settings
--=================================================================================================

--=================================================================================================
-- START AD-9653  Create Summary Page Layout
-- INSERT into syResource new student navigation page
--=================================================================================================
BEGIN TRANSACTION AddNewStudentSummaryPage;
BEGIN TRY
    DECLARE @ErrorAddNewStudentSummaryPage INT = 0;
    DECLARE @ResourceIdNewStudentSummaryPage INT = 868;
    IF NOT EXISTS
    (
        SELECT ResourceID
        FROM dbo.syResources
        WHERE ResourceID = @ResourceIdNewStudentSummaryPage
    )
    BEGIN
        INSERT INTO dbo.syResources
        (
            ResourceID,
            Resource,
            ResourceTypeID,
            ResourceURL,
            SummListId,
            ChildTypeId,
            ModDate,
            ModUser,
            AllowSchlReqFlds,
            MRUTypeId,
            UsedIn,
            TblFldsId,
            DisplayName
        )
        VALUES
        (   @ResourceIdNewStudentSummaryPage, -- ResourceID - smallint
            'Student Summary',                -- Resource - varchar(200)
            3,                                -- ResourceTypeID - tinyint
            '~/PL/StudentMasterSummary.aspx', -- ResourceURL - varchar(100)
            NULL,                             -- SummListId - smallint
            NULL,                             -- ChildTypeId - tinyint
            GETDATE(),                        -- ModDate - datetime
            'sa',                             -- ModUser - varchar(50)
            1,                                -- AllowSchlReqFlds - bit
            1,                                -- MRUTypeId - smallint
            207,                              -- UsedIn - int
            534,                              -- TblFldsId - int
            NULL                              -- DisplayName - varchar(200)
            );
    END;
END TRY
BEGIN CATCH
    SET @ErrorAddNewStudentSummaryPage = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);

END CATCH;

IF (@ErrorAddNewStudentSummaryPage = 0)
BEGIN
    COMMIT TRANSACTION AddNewStudentSummaryPage;
END;
ELSE
BEGIN
    ROLLBACK TRANSACTION AddNewStudentSummaryPage;
END;
GO
--=================================================================================================
-- End AD-9653  Create Summary Page Layout
--=================================================================================================

--=================================================================================================
-- START AD-9780  Add Summary Page to Academics\Manage Student menu
-- INSERT into symenus and security tables
--=================================================================================================
BEGIN TRANSACTION AddNewStudentSummaryMenuItem;
BEGIN TRY
    DECLARE @ErrorAddNewStudentSummaryMenuItem INT = 0;

    --Resource has been added on a previous US(AD-9653), resourceId = 868
    DECLARE @NewStudentSummaryPageResourceId INT = 868;
    DECLARE @NewStudentSummaryPageMenuName VARCHAR(30) = 'Student Summary';
    DECLARE @NewStudentSummaryPageUrl VARCHAR(30) = '/PL/StudentMasterSummary.aspx';

    DECLARE @ModuleId INT =
            (
                SELECT TOP 1
                       MenuItemId
                FROM dbo.syMenuItems
                WHERE DisplayName = 'academics'
                      AND ParentId IS NULL
                      AND MenuItemTypeId = 1
            );

    DECLARE @SubMenuId INT =
            (
                SELECT TOP 1
                       MenuItemId
                FROM dbo.syMenuItems
                WHERE DisplayName = 'manage students'
                      AND ParentId = @ModuleId
                      AND MenuItemTypeId = 2
            );

    DECLARE @PageGroupId INT =
            (
                SELECT TOP 1
                       MenuItemId
                FROM dbo.syMenuItems
                WHERE DisplayName = 'academics'
                      AND ParentId = @SubMenuId
                      AND MenuItemTypeId = 3
            );

    DECLARE @PageGroupHierarchyId UNIQUEIDENTIFIER =
            (
                SELECT TOP 1
                       HierarchyId
                FROM dbo.syMenuItems
                WHERE DisplayName = 'academics'
                      AND ParentId = @SubMenuId
                      AND MenuItemTypeId = 3
            );


    --if not exists insert into synavigation based on resource id
    IF NOT EXISTS
    (
        SELECT TOP 1
               *
        FROM dbo.syNavigationNodes
        WHERE ResourceId = @NewStudentSummaryPageResourceId
    )
    BEGIN
        INSERT INTO dbo.syNavigationNodes
        (
            HierarchyId,
            HierarchyIndex,
            ResourceId,
            ParentId,
            ModUser,
            ModDate,
            IsPopupWindow,
            IsShipped
        )
        VALUES
        (   NEWID(),                          -- HierarchyId - uniqueidentifier
            5,                                -- HierarchyIndex - smallint
            @NewStudentSummaryPageResourceId, -- ResourceId - smallint
            @PageGroupHierarchyId,            -- ParentId - uniqueidentifier
            'sa',                             -- ModUser - varchar(50)
            GETDATE(),                        -- ModDate - datetime
            0,                                -- IsPopupWindow - bit
            1                                 -- IsShipped - bit
            );
    END;

    --get hierarchy id for the synavitation
    DECLARE @HierarchyId UNIQUEIDENTIFIER =
            (
                SELECT TOP 1
                       HierarchyId
                FROM dbo.syNavigationNodes
                WHERE ResourceId = @NewStudentSummaryPageResourceId
            );


    -- if not exist insert into the symenyitems table
    IF NOT EXISTS
    (
        SELECT TOP 1
               *
        FROM dbo.syMenuItems
        WHERE ResourceId = @NewStudentSummaryPageResourceId
    )
    BEGIN
        INSERT INTO dbo.syMenuItems
        (
            MenuName,
            DisplayName,
            Url,
            MenuItemTypeId,
            ParentId,
            DisplayOrder,
            IsPopup,
            ModDate,
            ModUser,
            IsActive,
            ResourceId,
            HierarchyId,
            ModuleCode,
            MRUType,
            HideStatusBar
        )
        VALUES
        (   @NewStudentSummaryPageMenuName,   -- MenuName - varchar(250)
            @NewStudentSummaryPageMenuName,   -- DisplayName - varchar(250)
            @NewStudentSummaryPageUrl,        -- Url - nvarchar(250)
            4,                                -- MenuItemTypeId - smallint
            @PageGroupId,                     -- ParentId - int
            950,                              -- DisplayOrder - int
            0,                                -- IsPopup - bit
            GETDATE(),                        -- ModDate - datetime
            'sa',                             -- ModUser - varchar(50)
            1,                                -- IsActive - bit
            @NewStudentSummaryPageResourceId, -- ResourceId - smallint
            @HierarchyId,                     -- HierarchyId - uniqueidentifier
            NULL,                             -- ModuleCode - varchar(5)
            1,                                -- MRUType - int
            NULL                              -- HideStatusBar - bit
            );
    END;

    -- start section to copy security 
    DECLARE @CurrentRow INT = 0;
    DECLARE @RoleId UNIQUEIDENTIFIER;
    DECLARE @AccessLevel INT = 0;

    DECLARE @SecurityTempTable TABLE
    (
        RoleId UNIQUEIDENTIFIER,
        AccessLevel INT,
        RowNumber INT
    );

    INSERT INTO @SecurityTempTable
    (
        RoleId,
        AccessLevel,
        RowNumber
    )
    SELECT RoleId,
           AccessLevel,
           ROW_NUMBER() OVER (ORDER BY RoleId)
    FROM dbo.syRlsResLvls
    WHERE ResourceID = 203;


    WHILE (1 = 1)
    BEGIN
        -- Get next customerId
        SELECT TOP 1
               @RoleId = RoleId,
               @AccessLevel = AccessLevel,
               @CurrentRow = RowNumber
        FROM @SecurityTempTable
        WHERE RowNumber > @CurrentRow
        ORDER BY RowNumber;

        -- Exit loop if no more customers
        IF @@ROWCOUNT = 0
            BREAK;

        --check if security already exist for a given role
        IF NOT EXISTS
        (
            SELECT *
            FROM dbo.syRlsResLvls
            WHERE ResourceID = @NewStudentSummaryPageResourceId
                  AND RoleId = @RoleId
        )
        BEGIN
            INSERT INTO dbo.syRlsResLvls
            (
                RRLId,
                RoleId,
                ResourceID,
                AccessLevel,
                ModDate,
                ModUser,
                ParentId
            )
            VALUES
            (   NEWID(),                          -- RRLId - uniqueidentifier
                @RoleId,                          -- RoleId - uniqueidentifier
                @NewStudentSummaryPageResourceId, -- ResourceID - smallint
                @AccessLevel,                     -- AccessLevel - smallint
                GETDATE(),                        -- ModDate - datetime
                'sa',                             -- ModUser - varchar(50)
                NULL                              -- ParentId - int
                );
        END;

    END;

END TRY
BEGIN CATCH
    SET @ErrorAddNewStudentSummaryMenuItem = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);

END CATCH;

IF (@ErrorAddNewStudentSummaryMenuItem = 0)
BEGIN
    COMMIT TRANSACTION AddNewStudentSummaryMenuItem;
END;
ELSE
BEGIN
    ROLLBACK TRANSACTION AddNewStudentSummaryMenuItem;
END;
GO

--=================================================================================================
-- End AD-9780  Create Summary Page Layout
--=================================================================================================


--=================================================================================================

--=================================================================================================
-- START AD-8655 Student Search
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION StudentSearchT4Notices;
BEGIN TRY

    DECLARE @parentId AS INT;
    DECLARE @SectionName AS VARCHAR(50) = 'StudentSearchForTitleIvSapNotices';
    DECLARE @SectionCaption AS VARCHAR(100) = 'Student Enrollment Search';
    DECLARE @SectionId AS INT;
    DECLARE @SectionIdDELETE AS INT;
    SET @parentId =
    (
        SELECT SetId FROM ParamSet WHERE SetName = 'TitleIvSapNoticesReportSet'
    );

    SET @SectionIdDELETE =
    (
        SELECT TOP 1
               SectionId
        FROM dbo.ParamSection
        WHERE SectionName = 'ParametersForTitleIvSapNotices'
              AND SetId = @parentId
    );

    DELETE FROM dbo.ParamSection
    WHERE SectionId = @SectionIdDELETE;
    DELETE FROM dbo.ParamDetail
    WHERE SectionId = @SectionIdDELETE;
    --insert new resource id
    IF NOT EXISTS
    (
        SELECT TOP 1
               SectionName
        FROM dbo.ParamSection
        WHERE SectionName = @SectionName
    )
    BEGIN
        INSERT INTO dbo.ParamSection
        (
            SectionName,
            SectionCaption,
            SetId,
            SectionSeq,
            SectionDescription,
            SectionType
        )
        VALUES
        (   @SectionName,                          -- SectionName - nvarchar(50)
            @SectionCaption,                       -- SectionCaption - nvarchar(100)
            @parentId,                             -- SetId - bigint
            1,                                     -- SectionSeq - int
            'Filter report by student enrollment', -- SectionDescription - nvarchar(500)
            0                                      -- SectionType - int
            );
    END;
    SET @SectionId =
    (
        SELECT TOP 1
               SectionId
        FROM dbo.ParamSection
        WHERE SectionName = @SectionName
              AND SectionCaption = @SectionCaption
    );


    IF NOT EXISTS
    (
        SELECT TOP 1
               CaptionOverride
        FROM dbo.ParamDetail
        WHERE SectionId = @SectionId
    )
    BEGIN
        INSERT INTO dbo.ParamDetail
        (
            SectionId,
            ItemId,
            CaptionOverride,
            ItemSeq
        )
        VALUES
        (   @SectionId,      -- SectionId - bigint
            12,              -- ItemId - bigint
            @SectionCaption, -- CaptionOverride - nvarchar(50)
            1                -- ItemSeq - int
            );
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION StudentSearchT4Notices;
    PRINT 'Failed transcation Student Search on Title IV SAP Notice.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION StudentSearchT4Notices;
    PRINT 'successful transaction Student Search on Title IV SAP Notice.';
END;
GO
--=================================================================================================
-- End AD-8655 Student Search
--=================================================================================================
--=================================================================================================
-- START AD-10125: ad-8725- There should be no enrollments and messages at the Term Progress report for students in ABP
--=================================================================================================
UPDATE arPrgVersions
SET ProgramRegistrationType = 0
WHERE ProgramRegistrationType IS NULL;
GO
--=================================================================================================
-- END AD-10125: ad-8725- There should be no enrollments and messages at the Term Progress report for students in ABP
--=================================================================================================

--=================================================================================================
-- START AD-9661 Create Widget to show unprinted Title IV SAP results
--update the notice report name rdl
--=================================================================================================
UPDATE syReports
SET RDLName = 'TitleIVNotice/TitleIVNotice_Main'
WHERE ResourceId = 862;
--=================================================================================================
-- End AD-9661 Create Widget to show unprinted Title IV SAP results
--=================================================================================================
--=================================================================================================
-- START Tech: Add the Post Exam Results in Faculty-->ManageStudents
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION PostExamResultFaculty;
BEGIN TRY
    DECLARE @FacultyParentId INT;
    SET @FacultyParentId =
    (
        SELECT TOP 1
               MenuItemId
        FROM syMenuItems
        WHERE MenuName = 'Faculty'
              AND ModuleCode = 'FC'
    );

    DECLARE @manageStudentsParentId INT;
    SET @manageStudentsParentId =
    (
        SELECT TOP 1
               MenuItemId
        FROM syMenuItems
        WHERE ParentId = @FacultyParentId
              AND MenuName = 'Manage Students'
    );

    DECLARE @parentId INT;
    SET @parentId =
    (
        SELECT TOP 1
               MenuItemId
        FROM syMenuItems
        WHERE ParentId = @manageStudentsParentId
              AND MenuName = 'Documents & Test Scores'
    );
    IF NOT EXISTS
    (
        SELECT 1
        FROM syMenuItems
        WHERE ResourceId = 542
              AND ParentId = @parentId
    )
    BEGIN
        DECLARE @heirarcyId UNIQUEIDENTIFIER = NEWID();
        INSERT INTO syMenuItems
        (
            MenuName,
            DisplayName,
            Url,
            MenuItemTypeId,
            ParentId,
            DisplayOrder,
            IsPopup,
            ModDate,
            ModUser,
            IsActive,
            ResourceId,
            HierarchyId,
            ModuleCode,
            MRUType,
            HideStatusBar
        )
        VALUES
        (   'Post Exam Results',         -- MenuName - varchar(250)
            'Post Exam Results',         -- DisplayName - varchar(250)
            N'/AR/PostExamResults.aspx', -- Url - nvarchar(250)
            4,                           -- MenuItemTypeId - smallint
            @parentId,                   -- ParentId - int
            0,                           -- DisplayOrder - int
            0,                           -- IsPopup - bit
            GETDATE(),                   -- ModDate - datetime
            'Support',                   -- ModUser - varchar(50)
            1,                           -- IsActive - bit
            542,                         -- ResourceId - smallint
            @heirarcyId,                 -- HierarchyId - uniqueidentifier
            NULL,                        -- ModuleCode - varchar(5)
            1,                           -- MRUType - int
            NULL                         -- HideStatusBar - bit
            );
        -- navigation nodes
        DECLARE @FacultyheirachyId UNIQUEIDENTIFIER;
        SET @FacultyheirachyId =
        (
            SELECT TOP 1 HierarchyId FROM syNavigationNodes WHERE ResourceId = 300
        );
        DECLARE @parentHierarcyId UNIQUEIDENTIFIER;
        SET @parentHierarcyId =
        (
            SELECT TOP 1
                   HierarchyId
            FROM syNavigationNodes
            WHERE ParentId = @FacultyheirachyId
                  AND ResourceId = 394
        );
        IF NOT EXISTS
        (
            SELECT 1
            FROM syNavigationNodes
            WHERE ParentId = @parentHierarcyId
                  AND ResourceId = 542
        )
        BEGIN
            INSERT INTO syNavigationNodes
            (
                HierarchyId,
                HierarchyIndex,
                ResourceId,
                ParentId,
                ModUser,
                ModDate,
                IsPopupWindow,
                IsShipped
            )
            VALUES
            (   @heirarcyId,       -- HierarchyId - uniqueidentifier
                12,                -- HierarchyIndex - smallint
                542,               -- ResourceId - smallint
                @parentHierarcyId, -- ParentId - uniqueidentifier
                'Support',         -- ModUser - varchar(50)
                GETDATE(),         -- ModDate - datetime
                0,                 -- IsPopupWindow - bit
                1                  -- IsShipped - bit
                );
        END;
    END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION PostExamResultFaculty;
    PRINT 'Failed to Create Post exam results menuItem';

END;
ELSE
BEGIN
    COMMIT TRANSACTION PostExamResultFaculty;
    PRINT 'created post exam results menuItem successfully';
END;
GO
--=================================================================================================
-- END Tech: Add the Post Exam Results in Faculty-->ManageStudents
--=================================================================================================
--=================================================================================================
-- START AD-10292: Cannot Sort by Student Group in Crystal Reports
--=================================================================================================
IF NOT EXISTS
(
    SELECT 1
    FROM syRptParams
    WHERE SortSec = 1
          AND RptCaption = 'Student Group'
          AND ResourceId = 229
)
BEGIN
    UPDATE syRptParams
    SET SortSec = 1
    WHERE RptCaption = 'Student Group'
          AND ResourceId = 229;
END;
GO
--=================================================================================================
-- END AD-10292: Cannot Sort by Student Group in Crystal Reports
--=================================================================================================
--=================================================================================================
-- START AD-10520: Ledger Balance in adhoc does not seem to be enrollment specific.
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION LedgerBalanceAdhocEnroll;
BEGIN TRY
    DECLARE @ledgerFldId INT;
    SET @ledgerFldId =
    (
        SELECT FldId FROM syFields WHERE FldName = 'LedgerBalance'
    );
    UPDATE syFieldCalculation
    SET CalculationSql = 'ISNULL(( SELECT FORMAT(SUM(T.TransAmount),''C'',''en-us'') FROM   saTransactions T WHERE  T.StuEnrollId IN ( SELECT StuEnrollId FROM arStuEnrollments b WHERE b.StuEnrollId = arStuEnrollments.StuEnrollId) AND T.Voided = 0),0) AS LedgerBalance'
    WHERE FldId = @ledgerFldId;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION LedgerBalanceAdhocEnroll;
    PRINT 'Failed to update LedgerBalance';

END;
ELSE
BEGIN
    COMMIT TRANSACTION LedgerBalanceAdhocEnroll;
    PRINT 'LedgerBalance updated';
END;
GO
--=================================================================================================
-- END AD-10520: Ledger Balance in adhoc does not seem to be enrollment specific.
--=================================================================================================

--=================================================================================================
-- START AD-10514: NO Klass app Role implemented for the Progress report Api call
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION changeKlassAppToReportApi;
BEGIN TRY
    DECLARE @sysRoleId INT;
    DECLARE @activeStatusId UNIQUEIDENTIFIER;
    DECLARE @progressReportRoleId UNIQUEIDENTIFIER;

    SET @activeStatusId =
    (
        SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
    );

    IF @activeStatusId IS NOT NULL
    BEGIN
        SET @sysRoleId =
        (
            SELECT TOP 1 SysRoleId FROM dbo.sySysRoles WHERE Descrip = 'Klass App'
        );

        IF @sysRoleId IS NOT NULL
        BEGIN
            UPDATE dbo.sySysRoles
            SET Descrip = 'Reports Api',
                Permission = '{"modules": [ {"name": "ReportsApi","level": "read"}, {"name": "SY","level": "read"}]}'
            WHERE SysRoleId = @sysRoleId;
        END;

        SET @sysRoleId =
        (
            SELECT TOP 1 SysRoleId FROM dbo.sySysRoles WHERE Descrip = 'Reports Api'
        );
        IF @sysRoleId IS NOT NULL
        BEGIN
            SET @progressReportRoleId =
            (
                SELECT TOP 1
                       RoleId
                FROM dbo.syRoles
                WHERE SysRoleId = @sysRoleId
                      AND StatusId = @activeStatusId
                      AND Role = 'Reports Api'
            );
            IF @progressReportRoleId IS NULL
            BEGIN
                PRINT 'inserting report api role';
                INSERT INTO dbo.syRoles
                (
                    RoleId,
                    Role,
                    Code,
                    StatusId,
                    SysRoleId,
                    ModDate,
                    ModUser
                )
                VALUES
                (   NEWID(),         -- RoleId - uniqueidentifier
                    'Reports Api',   -- Role - varchar(60)
                    'Reports-Api',   -- Code - varchar(12)
                    @activeStatusId, -- StatusId - uniqueidentifier
                    @sysRoleId,      -- SysRoleId - int
                    GETDATE(),       -- ModDate - datetime
                    'Support'        -- ModUser - varchar(50)
                    );
            END;
        END;
    END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION changeKlassAppToReportApi;
    PRINT 'Failed to update klass app permission to report api';

END;
ELSE
BEGIN
    COMMIT TRANSACTION changeKlassAppToReportApi;
    PRINT 'Updated klass app permission to report api';
END;
GO
--=================================================================================================
-- END AD-10514: NO Klass app Role implemented for the Progress report Api call
--=================================================================================================
--=================================================================================================
-- START AD-10537: Add Tuition Cost field to Adhoc
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION TuitionAdhocReport;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Money'
    );
    IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'Tuition')
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,     -- FldId - int
            'Tuition',  -- FldName - varchar(200)
            @fldTypeId, -- FldTypeId - int
            10,         -- FldLen - int
            NULL,       -- DDLId - int
            NULL,       -- DerivedFld - bit
            NULL,       -- SchlReq - bit
            NULL,       -- LogChanges - bit
            NULL        -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Ledger'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,                  -- FldCapId - int
            @fldId,                     -- FldId - int
            1,                          -- LangId - tinyint
            'Tuition',                  -- Caption - varchar(100)
            'Tuition for Adhoc reports' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            NULL         -- FKColDescrip - varchar(50)
            );
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @fldId, -- FldId - int
            '  ISNULL((SELECT FORMAT(SUM(F.TransAmount), ''C'', ''en-us'') FROM    ((SELECT SUM(isnull(T.TransAmount,0)) AS TransAmount FROM   saTransactions T INNER JOIN saTransCodes TC ON TC.TransCodeId = T.TransCodeId WHERE  TC.SysTransCodeId = 1 AND  Voided = 0 AND T.StuEnrollId IN ( SELECT StuEnrollId FROM arStuEnrollments b WHERE b.StuEnrollId = arStuEnrollments.StuEnrollId) )    UNION (SELECT SUM(ISNULL(T.TransAmount,0)) AS TransAmount FROM   saTransactions T WHERE  t.TransDescrip = ''DROP Out Adjustment to Charges'' AND Voided = 0 AND T.StuEnrollId IN (    SELECT StuEnrollId  FROM   arStuEnrollments b WHERE  b.StuEnrollId = arStuEnrollments.StuEnrollId))) AS F),0) AS Tuition ');
    END;
    IF EXISTS (SELECT 1 FROM syFields WHERE FldName = 'Tuition')
    BEGIN
        DECLARE @tuiFld INT;
        SET @tuiFld =
        (
            SELECT FldId FROM syFields WHERE FldName = 'Tuition'
        );
        UPDATE syFieldCalculation
        SET CalculationSql = '  ISNULL((SELECT FORMAT(SUM(F.TransAmount), ''C'', ''en-us'') FROM    ((SELECT SUM(isnull(T.TransAmount,0)) AS TransAmount FROM   saTransactions T INNER JOIN saTransCodes TC ON TC.TransCodeId = T.TransCodeId WHERE  TC.SysTransCodeId = 1 AND  Voided = 0 AND T.StuEnrollId IN ( SELECT StuEnrollId FROM arStuEnrollments b WHERE b.StuEnrollId = arStuEnrollments.StuEnrollId) )    UNION (SELECT SUM(ISNULL(T.TransAmount,0)) AS TransAmount FROM   saTransactions T WHERE  t.TransDescrip = ''DROP Out Adjustment to Charges'' AND Voided = 0 AND T.StuEnrollId IN (    SELECT StuEnrollId  FROM   arStuEnrollments b WHERE  b.StuEnrollId = arStuEnrollments.StuEnrollId))) AS F),0) AS Tuition '
        WHERE FldId = @tuiFld;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION TuitionAdhocReport;
    PRINT 'Failed to Add Tuition to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION TuitionAdhocReport;
    PRINT 'Added Tuition to Adhoc Report';
END;
GO
--=================================================================================================
-- END AD-10537: Add Tuition Cost field to Adhoc
--=================================================================================================
--=================================================================================================
-- START AD10540: Add Kit transaction code field to Adhoc
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION KitAdhocReport;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Money'
    );
    IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'Kit')
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,     -- FldId - int
            'Kit',      -- FldName - varchar(200)
            @fldTypeId, -- FldTypeId - int
            10,         -- FldLen - int
            NULL,       -- DDLId - int
            NULL,       -- DerivedFld - bit
            NULL,       -- SchlReq - bit
            NULL,       -- LogChanges - bit
            NULL        -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Ledger'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,              -- FldCapId - int
            @fldId,                 -- FldId - int
            1,                      -- LangId - tinyint
            'Kit',                  -- Caption - varchar(100)
            'Kit for Adhoc reports' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            NULL         -- FKColDescrip - varchar(50)
            );
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @fldId,                                                                                                                                                                                                                                                                                                                           -- FldId - int
            'ISNULL(( SELECT FORMAT(SUM(T.TransAmount),''C'',''en-us'') FROM   saTransactions T INNER JOIN saTransCodes TC ON TC.TransCodeId = T.TransCodeId WHERE TC.SysTransCodeId = 17 AND  Voided = 0 AND  T.StuEnrollId IN ( SELECT StuEnrollId FROM arStuEnrollments b WHERE b.StuEnrollId = arStuEnrollments.StuEnrollId) ),0) AS Kit' -- CalculationSql - varchar(8000)
            );
    END;
    IF EXISTS (SELECT 1 FROM syFields WHERE FldName = 'Kit')
    BEGIN
        DECLARE @KitFld INT;
        SET @KitFld =
        (
            SELECT FldId FROM syFields WHERE FldName = 'Kit'
        );
        UPDATE syFieldCalculation
        SET CalculationSql = 'ISNULL(( SELECT FORMAT(SUM(T.TransAmount),''C'',''en-us'') FROM   saTransactions T INNER JOIN saTransCodes TC ON TC.TransCodeId = T.TransCodeId WHERE TC.SysTransCodeId = 17 AND  Voided = 0 AND  T.StuEnrollId IN ( SELECT StuEnrollId FROM arStuEnrollments b WHERE b.StuEnrollId = arStuEnrollments.StuEnrollId) ),0) AS Kit'
        WHERE FldId = @KitFld;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION KitAdhocReport;
    PRINT 'Failed to Add Kit to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION KitAdhocReport;
    PRINT 'Added Kit to Adhoc Report';
END;
GO
--=================================================================================================
-- END AD10540: Add Kit transaction code field to Adhoc
--=================================================================================================
--=================================================================================================
-- START AD-9661: Create Widget to show unprinted Title IV SAP results
-- Creates syWidget and SyWidgetResourceSetting records
--=================================================================================================

DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION AddWidgets;
BEGIN TRY

    DECLARE @ActiveStatusId UNIQUEIDENTIFIER =
            (
                SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
            );

    DECLARE @InactiveStatusId UNIQUEIDENTIFIER =
            (
                SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'I'
            );

    DECLARE @widgetId UNIQUEIDENTIFIER;

    DECLARE @LastInsertedTable TABLE
    (
        ID UNIQUEIDENTIFIER,
        TimeInserted DATETIME
    );

    ----------Title IV Sap Notices Widget--------

    IF NOT EXISTS (SELECT 1 FROM dbo.syWidgets WHERE Code = 'T4SAPNotice')
    BEGIN
        --Insert widget record
        INSERT INTO dbo.syWidgets
        (
            Code,
            Description,
            Rows,
            Columns,
            StatusId
        )
        OUTPUT Inserted.WidgetId,
               GETDATE()
        INTO @LastInsertedTable
        VALUES
        (   'T4SAPNotice',          -- Code - varchar(50)
            'Title IV Sap Notices', -- Description - varchar(200)
            0,                      -- Rows - int
            0,                      -- Columns - int
            @ActiveStatusId         -- StatusId - uniqueidentifier
            );

        SET @widgetId =
        (
            SELECT TOP 1 ID FROM @LastInsertedTable ORDER BY TimeInserted DESC
        );


        INSERT INTO dbo.syWidgetResourceRoles
        (
            WidgetId,
            ResourceId,
            SysRoleId,
            StatusId
        )
        VALUES
        (   @widgetId,      -- WidgetId - uniqueidentifier
            264,            -- ResourceId - smallint
            7,              -- SysRoleId - int
            @ActiveStatusId -- StatusId - uniqueidentifier
            );

        INSERT INTO dbo.syWidgetResourceRoles
        (
            WidgetId,
            ResourceId,
            SysRoleId,
            StatusId
        )
        VALUES
        (   @widgetId,      -- WidgetId - uniqueidentifier
            264,            -- ResourceId - smallint
            9,              -- SysRoleId - int
            @ActiveStatusId -- StatusId - uniqueidentifier
            );

        --Give Financial Aid Advisor AR read access
        UPDATE dbo.sySysRoles
        SET Permission = REPLACE(
                                    CAST(Permission AS VARCHAR(MAX)),
                                    '{"name": "AR","level": "none"}',
                                    '{"name": "AR","level": "read"}'
                                )
        FROM sySysRoles
        WHERE CHARINDEX('{"name": "AR","level": "none"}', CAST(Permission AS VARCHAR(MAX))) > 0
              AND SysRoleId = 7;


        --Give Director of Financial Aid AR read access
        UPDATE dbo.sySysRoles
        SET Permission = REPLACE(
                                    CAST(Permission AS VARCHAR(MAX)),
                                    '{"name": "AR","level": "none"}',
                                    '{"name": "AR","level": "read"}'
                                )
        FROM sySysRoles
        WHERE CHARINDEX('{"name": "AR","level": "none"}', CAST(Permission AS VARCHAR(MAX))) > 0
              AND SysRoleId = 9;

    END;

----------END : Title IV Sap Notices Widget--------

--add new widgets here....

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION AddWidgets;
    PRINT 'Failed to create widget records';

END;
ELSE
BEGIN
    COMMIT TRANSACTION AddWidgets;
    PRINT 'Created widget records successfully';
END;
GO
--=================================================================================================
-- END AD-9661: Create Widget to show unprinted Title IV SAP results
--=================================================================================================
--=================================================================================================
-- START TECH:  ADHOC REPORT for schedule hr and actual hrs
--=================================================================================================
DECLARE @fldId INT;
SET @fldId =
(
    SELECT FldId FROM syFields WHERE FldName = 'ScheduledHours'
);
UPDATE syFieldCalculation
SET CalculationSql = '(Select CAST(SUM(ScheduledHours) AS NVARCHAR(50)) as ScheduledHours from    (    (Select IsNULL(round(SUM(scheduled)/60,2),0) as ScheduledHours     FROM dbo.atClsSectAttendance a where a.StuEnrollId=arStuEnrollments.StuEnrollId     AND a.Actual <> 9999      and Scheduled>=0     Union     Select Sum(SchedHours) as ScheduledHours FROM dbo.arStudentClockAttendance a     where   a.StuEnrollId=arStuEnrollments.StuEnrollId AND a.SchedHours>0      )   ) t1) as ScheduledHours'
WHERE FldId = @fldId;
SET @fldId =
(
    SELECT FldId FROM syFields WHERE FldName = 'ActualHours'
);
UPDATE syFieldCalculation
SET CalculationSql = '(Select CAST(SUM(ActualHours) AS NVARCHAR(50)) as ActualHours from ((Select IsNULL(round(SUM(Actual)/60,2),0) as ActualHours FROM dbo.atClsSectAttendance a where a.StuEnrollId=arStuEnrollments.StuEnrollId AND a.Actual <> 9999 and Scheduled>=0 Union Select Sum(ActualHours) as ActualHours FROM dbo.arStudentClockAttendance a where   a.StuEnrollId=arStuEnrollments.StuEnrollId and ActualHours <> 9999 )) t1) as ActualHours'
WHERE FldId = @fldId;
GO
--=================================================================================================
-- END TECH: ADHOC REPORT for schedule hr and actual hrs
--=================================================================================================
--=================================================================================================
-- START TECH: giving API roles to access the student summary page to the instructor and acedemic advisor and admin rep 
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION PermissionInstructor;
BEGIN TRY
    UPDATE dbo.sySysRoles
    SET Permission = REPLACE(
                                CAST(Permission AS VARCHAR(MAX)),
                                '{"name": "AR","level": "none"}',
                                '{"name": "AR","level": "read"}'
                            )
    FROM sySysRoles
    WHERE CHARINDEX('{"name": "AR","level": "none"}', CAST(Permission AS VARCHAR(MAX))) > 0
          AND SysRoleId IN ( 2, 3, 4 );
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION PermissionInstructor;
    PRINT 'Failed to update API Permissions';

END;
ELSE
BEGIN
    COMMIT TRANSACTION PermissionInstructor;
    PRINT 'updated API Permissions successfully';
END;
GO
--=================================================================================================
-- END TECH: giving API roles to access the student summary page to the instructor and acedemic advisor and admin rep
--=================================================================================================
--=================================================================================================
-- START AD-10788 Adhoc "SAP Summary" incorrect Data
--================================================================================================= 
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION attPercentageAdhoc;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT FldId FROM syFields WHERE FldName = 'AttendancePercent'
    );
    UPDATE syFieldCalculation
    SET CalculationSql = '((SELECT CAST(ISNULL((SELECT MAX(ActualRunningPresentDays + ActualRunningMakeupDays) FROM SyStudentAttendanceSummary WHERE StuEnrollId = arStuEnrollments.StuEnrollId) * 100.0 / MAX(ActualRunningScheduledDays), 0)AS decimal(18,2)) as AttendancePercent FROM SyStudentAttendanceSummary WHERE StuEnrollId = arStuEnrollments.StuEnrollId AND ActualRunningScheduledDays > 0)) AS AttendancePercent'
    WHERE FldId = @fldId;
    DECLARE @GPAFldId INT;
    DECLARE @stuEnrollTablId INT;
    DECLARE @categoryID INT;
    SET @categoryID =
    (
        SELECT CategoryId
        FROM syFldCategories
        WHERE Descrip = 'SAP'
              AND EntityId = 394
    );
    SET @GPAFldId =
    (
        SELECT FldId FROM syFields WHERE FldName = 'GPA'
    );
    SET @stuEnrollTablId =
    (
        SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
    );

    UPDATE syTblFlds
    SET TblId = @stuEnrollTablId
    WHERE FldId = @GPAFldId
          AND CategoryId = @categoryID;
    IF NOT EXISTS (SELECT 1 FROM syFieldCalculation WHERE FldId = @GPAFldId)
    BEGIN
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @GPAFldId, -- FldId - int
            ' (SELECT ROUND(  ( SUM(b.CourseWeight * b.WeightedCourseGPA / 100) / SUM(b.CourseWeight)) * 100 , 2) AS WeightedGPA
FROM   (   SELECT   SUM(OurSingleClassGradeFactor) AS CourseFactor , SUM(a.GradeWeight) AS GradeWeight , SUM(a.Score) AS SumOfScores ,
 ( SUM(OurSingleClassGradeFactor) / SUM(a.GradeWeight)) * 100 AS WeightedCourseGPA , ( SUM(a.Score) / COUNT(*)) AS UnweightedCourseGPA , ClsSectionId ,
 a.CourseWeight  FROM     (   SELECT ( c.Weight * a.Score / 100 ) AS OurSingleClassGradeFactor , c.Weight AS GradeWeight , a.Score ,a.ClsSectionId , d.CourseWeight ,
c.Descrip AS ClassDescrip FROM   (   SELECT   StuEnrollId , ClsSectionId ,  a.InstrGrdBkWgtDetailId ,AVG(Score) AS Score FROM     dbo.arGrdBkResults a
   JOIN dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId JOIN dbo.arGrdComponentTypes f ON f.GrdComponentTypeId = c.GrdComponentTypeId
   WHERE    a.StuEnrollId = arStuEnrollments.StuEnrollId  AND a.Score IS NOT NULL AND a.PostDate IS NOT NULL GROUP BY StuEnrollId , ClsSectionId ,
a.InstrGrdBkWgtDetailId ) a JOIN dbo.arClassSections b ON b.ClsSectionId = a.ClsSectionId JOIN dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
 JOIN dbo.arProgVerDef d ON d.ReqId = b.ReqId ) a  GROUP BY ClsSectionId , a.CourseWeight ) b) as GPA ' -- CalculationSql - varchar(8000)
            );
    END;
    ELSE
    BEGIN
        UPDATE syFieldCalculation
        SET CalculationSql = ' (SELECT ROUND(  ( SUM(b.CourseWeight * b.WeightedCourseGPA / 100) / SUM(b.CourseWeight)) * 100 , 2) AS WeightedGPA
FROM   (   SELECT   SUM(OurSingleClassGradeFactor) AS CourseFactor , SUM(a.GradeWeight) AS GradeWeight , SUM(a.Score) AS SumOfScores ,
 ( SUM(OurSingleClassGradeFactor) / SUM(a.GradeWeight)) * 100 AS WeightedCourseGPA , ( SUM(a.Score) / COUNT(*)) AS UnweightedCourseGPA , ClsSectionId ,
 a.CourseWeight  FROM     (   SELECT ( c.Weight * a.Score / 100 ) AS OurSingleClassGradeFactor , c.Weight AS GradeWeight , a.Score ,a.ClsSectionId , d.CourseWeight ,
c.Descrip AS ClassDescrip FROM   (   SELECT   StuEnrollId , ClsSectionId ,  a.InstrGrdBkWgtDetailId ,AVG(Score) AS Score FROM     dbo.arGrdBkResults a
   JOIN dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId JOIN dbo.arGrdComponentTypes f ON f.GrdComponentTypeId = c.GrdComponentTypeId
   WHERE    a.StuEnrollId = arStuEnrollments.StuEnrollId  AND a.Score IS NOT NULL AND a.PostDate IS NOT NULL GROUP BY StuEnrollId , ClsSectionId ,
a.InstrGrdBkWgtDetailId ) a JOIN dbo.arClassSections b ON b.ClsSectionId = a.ClsSectionId JOIN dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
 JOIN dbo.arProgVerDef d ON d.ReqId = b.ReqId ) a  GROUP BY ClsSectionId , a.CourseWeight ) b) as GPA '
        WHERE FldId = @GPAFldId;
    END;

    DECLARE @schHrFldid INT;
    SET @schHrFldid =
    (
        SELECT FldId FROM syFields WHERE FldName = 'ScheduledHours'
    );

    UPDATE syFieldCalculation
    SET CalculationSql = '   ( SELECT TOP 1 (CASE WHEN (  LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'',arStuEnrollments.CampusId)))) = ''byclass''  AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''DisplayAttendanceUnitForProgressReportByClass'',arStuEnrollments.CampusId)))) = ''hours''
            AND AAUT1.UnitTypeDescrip = ''Present Absent'' ) THEN ( SELECT SUM(ScheduledHours) FROM   syStudentAttendanceSummary WHERE  StuEnrollId = arStuEnrollments.StuEnrollId )
          ELSE CASE WHEN (  LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'',arStuEnrollments.CampusId)))) = ''byclass''
            AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''DisplayAttendanceUnitForProgressReportByClass'',arStuEnrollments.CampusId)))) = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes'' )  THEN                CASE WHEN (SELECT TOP 1 EndDate FROM arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE PrgVerId = arStuEnrollments.PrgVerId) IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                  ELSE SAS_ForTerm.ScheduledDays / 60 END WHEN (  LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'',arStuEnrollments.CampusId)))) = ''byclass''
               AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''DisplayAttendanceUnitForProgressReportByClass'',arStuEnrollments.CampusId)))) = ''hours''
            AND AAUT1.UnitTypeDescrip = ''Clock Hours'')  THEN                CASE WHEN (SELECT TOP 1 EndDate FROM arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE PrgVerId = arStuEnrollments.PrgVerId) IS NULL THEN SAS_NoTerm.ScheduledDays / 60
               ELSE SAS_ForTerm.ScheduledDays / 60 END  WHEN (  LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'',arStuEnrollments.CampusId)))) = ''byclass''
              AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''DisplayAttendanceUnitForProgressReportByClass'',arStuEnrollments.CampusId)))) = ''presentabsent'' AND AAUT1.UnitTypeDescrip = ''Clock Hours''
               )   THEN                CASE WHEN (SELECT TOP 1 EndDate FROM arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE PrgVerId = arStuEnrollments.PrgVerId) IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                  ELSE SAS_ForTerm.ScheduledDays / 60 END  WHEN ( LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'',arStuEnrollments.CampusId)))) = ''byclass''
         AND LOWER(LTRIM(RTRIM(dbo.GetAppSettingValueByKeyName(''DisplayAttendanceUnitForProgressReportByClass'',arStuEnrollments.CampusId)))) = ''presentabsent''
           AND AAUT1.UnitTypeDescrip = ''Minutes''  )   THEN                CASE WHEN (SELECT TOP 1 EndDate FROM arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE PrgVerId = arStuEnrollments.PrgVerId) IS NULL THEN SAS_NoTerm.ScheduledDays / 60
             ELSE SAS_ForTerm.ScheduledDays / 60  END ELSE CASE WHEN (SELECT TOP 1 EndDate FROM arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE PrgVerId = arStuEnrollments.PrgVerId) IS NULL THEN ( SAS_NoTerm.ScheduledDays )
               ELSE ( SAS_ForTerm.ScheduledDays )   END END END) FROM   arAttUnitType AAUT1 INNER JOIN arPrgVersions arp ON arp.UnitTypeId = AAUT1.UnitTypeId
       LEFT JOIN (   SELECT   StuEnrollId , MAX(ActualRunningScheduledDays) AS ScheduledDays ,    MAX(AdjustedPresentDays) AS AttendedDays ,MAX(AdjustedAbsentDays) AS AbsentDays ,
            MAX(ActualRunningMakeupDays) AS MakeupDays ,  MAX(StudentAttendedDate) AS LDA  FROM     syStudentAttendanceSummary GROUP BY StuEnrollId ) SAS_NoTerm ON arStuEnrollments.StuEnrollId = SAS_NoTerm.StuEnrollId
         LEFT JOIN (   SELECT   StuEnrollId ,  MAX(ActualRunningScheduledDays) AS ScheduledDays , MAX(AdjustedPresentDays) AS AttendedDays , MAX(AdjustedAbsentDays) AS AbsentDays ,
            MAX(ActualRunningMakeupDays) AS MakeupDays ,    MAX(StudentAttendedDate) AS LDA FROM     syStudentAttendanceSummary GROUP BY StuEnrollId ) SAS_ForTerm ON arStuEnrollments.StuEnrollId = SAS_ForTerm.StuEnrollId )   AS ScheduledHours'
    WHERE FldId = @schHrFldid; -- Scheduled Hours

    DECLARE @actualHrFldId INT;
    SET @actualHrFldId =
    (
        SELECT FldId FROM syFields WHERE FldName = 'CompletedHours'
    );

    UPDATE syFieldCalculation
    SET CalculationSql = ' ( (  SELECT TOP 1( CASE WHEN (   LOWER( LTRIM( RTRIM( dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'' , arStuEnrollments.CampusId)))) = ''byclass''
   AND LOWER( LTRIM( RTRIM(dbo.GetAppSettingValueByKeyName(''DisplayAttendanceUnitForProgressReportByClass'' , arStuEnrollments.CampusId)))) = ''hours''
    AND AAUT1.UnitTypeDescrip = ''Present Absent'' )  THEN      (   SELECT SUM(ActualPresentDays_ConvertTo_Hours) + SAS_ForTerm.MakeupDays  FROM   syStudentAttendanceSummary
      WHERE  StuEnrollId = arStuEnrollments.StuEnrollId ) ELSE CASE WHEN (   LOWER(LTRIM( RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'' , arStuEnrollments.CampusId)))) = ''byclass''
	   AND LOWER(LTRIM( RTRIM(dbo.GetAppSettingValueByKeyName( ''DisplayAttendanceUnitForProgressReportByClass'' ,arStuEnrollments.CampusId)))) = ''hours''
AND AAUT1.UnitTypeDescrip = ''Minutes'' )THEN CASE WHEN (   SELECT TOP 1 EndDate FROM   arTerm art  INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId
    WHERE  PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN (SAS_NoTerm.AttendedDays / 60)+(SAS_NoTerm.MakeupDays / 60) ELSE (SAS_ForTerm.AttendedDays / 60)+(SAS_ForTerm.MakeupDays / 60) END   WHEN (   LOWER( LTRIM( RTRIM( dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'' ,arStuEnrollments.CampusId)))) = ''byclass''
 AND LOWER( LTRIM(RTRIM( dbo.GetAppSettingValueByKeyName( ''DisplayAttendanceUnitForProgressReportByClass'' ,arStuEnrollments.CampusId)))) = ''hours'' AND AAUT1.UnitTypeDescrip = ''Clock Hours'' ) 
   THEN CASE WHEN (   SELECT TOP 1 EndDate FROM   arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE  PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN
 ( SAS_NoTerm.AttendedDays / 60 ) + (SAS_NoTerm.MakeupDays / 60) ELSE ( SAS_ForTerm.AttendedDays / 60 ) + ( SAS_ForTerm.MakeupDays / 60) END WHEN (   LOWER(LTRIM( RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'' ,arStuEnrollments.CampusId)))) = ''byclass''
   AND LOWER( LTRIM(  RTRIM(dbo.GetAppSettingValueByKeyName( ''DisplayAttendanceUnitForProgressReportByClass'' , arStuEnrollments.CampusId)))) = ''presentabsent''  AND AAUT1.UnitTypeDescrip = ''Clock Hours'' ) 
     THEN  CASE WHEN (   SELECT TOP 1 EndDate FROM   arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE  PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN
      ( SAS_NoTerm.AttendedDays / 60 ) + (SAS_NoTerm.MakeupDays / 60)  ELSE ( SAS_ForTerm.AttendedDays / 60 ) + (SAS_ForTerm.MakeupDays / 60)  END WHEN (   LOWER(LTRIM( RTRIM( dbo.GetAppSettingValueByKeyName( ''TrackSapAttendance'' , arStuEnrollments.CampusId)))) = ''byclass''
         AND LOWER( LTRIM( RTRIM(  dbo.GetAppSettingValueByKeyName(''DisplayAttendanceUnitForProgressReportByClass'' , arStuEnrollments.CampusId)))) = ''presentabsent''AND AAUT1.UnitTypeDescrip = ''Minutes'' ) 
    THEN  CASE WHEN (   SELECT TOP 1 EndDate   FROM   arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE  PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN
        (SAS_NoTerm.AttendedDays / 60) + (SAS_NoTerm.MakeupDays / 60) ELSE (SAS_ForTerm.AttendedDays / 60) + (SAS_ForTerm.MakeupDays / 60) END ELSE CASE WHEN (   SELECT TOP 1 EndDate FROM   arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId
      WHERE  PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN (SAS_NoTerm.AttendedDays) + ( SAS_NoTerm.MakeupDays ) ELSE (SAS_ForTerm.AttendedDays)+( SAS_ForTerm.MakeupDays ) END END END ) FROM   arAttUnitType AAUT1
      INNER JOIN arPrgVersions arp ON arp.UnitTypeId = AAUT1.UnitTypeId  LEFT JOIN (   SELECT   StuEnrollId , MAX(ActualRunningScheduledDays) AS ScheduledDays ,  MAX(AdjustedPresentDays) AS AttendedDays ,
         MAX(AdjustedAbsentDays) AS AbsentDays , MAX(ActualRunningMakeupDays) AS MakeupDays ,MAX(StudentAttendedDate) AS LDA  FROM     syStudentAttendanceSummary
     GROUP BY StuEnrollId ) SAS_NoTerm ON arStuEnrollments.StuEnrollId = SAS_NoTerm.StuEnrollId  LEFT JOIN (   SELECT   StuEnrollId ,  MAX(ActualRunningScheduledDays) AS ScheduledDays ,
        MAX(AdjustedPresentDays) AS AttendedDays , MAX(AdjustedAbsentDays) AS AbsentDays ,  MAX(ActualRunningMakeupDays) AS MakeupDays , MAX(StudentAttendedDate) AS LDA
        FROM     syStudentAttendanceSummary  GROUP BY StuEnrollId ) SAS_ForTerm ON arStuEnrollments.StuEnrollId = SAS_ForTerm.StuEnrollId ) ) AS CompletedHours '
    WHERE FldId = @actualHrFldId;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION attPercentageAdhoc;
    PRINT 'Failed to update Query of Attendance Percentage';

END;
ELSE
BEGIN
    COMMIT TRANSACTION attPercentageAdhoc;
    PRINT 'updated Query of Attendance Percentage';
END;
GO
--=================================================================================================
-- END AD-10788 Adhoc "SAP Summary" incorrect Data
--=================================================================================================
--=================================================================================================
-- START TECH: TitleIVWidgetFilter Add Configuration Setting
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION PermissionInstructor;
BEGIN TRY

    IF NOT EXISTS
    (
        SELECT TOP 1
               SettingId
        FROM dbo.syConfigAppSettings
        WHERE KeyName = 'TitleIVWidgetFilter'
    )
    BEGIN

        INSERT INTO dbo.syConfigAppSettings
        (
            KeyName,
            Description,
            ModUser,
            ModDate,
            CampusSpecific,
            ExtraConfirmation
        )
        VALUES
        (   'TitleIVWidgetFilter',                  -- KeyName - varchar(200)
            'Filters dashboard widget SAP notices', -- Description - varchar(1000)
            'Support',                              -- ModUser - varchar(50)
            GETDATE(),                              -- ModDate - datetime
            1,                                      -- CampusSpecific - bit
            0                                       -- ExtraConfirmation - bit
            );

        DECLARE @Id INT;
        SET @Id =
        (
            SELECT TOP 1
                   SettingId
            FROM dbo.syConfigAppSettings
            WHERE KeyName = 'TitleIVWidgetFilter'
        );


        INSERT INTO dbo.syConfigAppSetValues
        (
            SettingId,
            CampusId,
            Value,
            ModUser,
            ModDate,
            Active
        )
        VALUES
        (   @Id,          -- SettingId - int
            NULL,         -- CampusId - uniqueidentifier
            '10/11/2018', -- Value - varchar(1000)
            'Support',    -- ModUser - varchar(50)
            GETDATE(),    -- ModDate - datetime
            1             -- Active - bit
            );
    END;


END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION PermissionInstructor;
    PRINT 'Failed to update TitleIV Widget Configuration Setting';

END;
ELSE
BEGIN
    COMMIT TRANSACTION PermissionInstructor;
    PRINT 'Updateded TitleIV Widget Configuration Setting';
END;
GO
--=================================================================================================
-- END TECH: TitleIVWidgetFilter Add Configuration Setting
--=================================================================================================

--=================================================================================================
-- START TECH: Set payment period number on transaction
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION updateTransactionsPaymentPeriods;
BEGIN TRY

    UPDATE dbo.saTransactions
    SET saTransactions.PaymentPeriodNumber = periodIdToNum.PeriodNumber
    FROM dbo.saTransactions trans
        INNER JOIN
        (
            SELECT TransactionId,
                   PeriodNumber
            FROM dbo.saTransactions
                INNER JOIN dbo.saPmtPeriods
                    ON saPmtPeriods.PmtPeriodId = saTransactions.PmtPeriodId
        ) periodIdToNum
            ON trans.TransactionId = periodIdToNum.TransactionId
    WHERE PaymentPeriodNumber IS NULL;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION updateTransactionsPaymentPeriods;
    PRINT 'Failed to update transaction payment periods.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION updateTransactionsPaymentPeriods;
    PRINT 'Updated transaction payment periods successfully';
END;
GO
--=================================================================================================
-- END TECH: Set payment period number on transaction
--=================================================================================================
--=================================================================================================
-- START TECH: Aid Received Report in Advantage does not have award year filter
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION AddAwrYr;
BEGIN TRY
    DECLARE @rptParamId INT;
    SET @rptParamId =
    (
        SELECT MAX(RptParamId) FROM syRptParams
    ) + 1;

    DECLARE @AidRecievedResouceId INT;
    SET @AidRecievedResouceId =
    (
        SELECT ResourceID FROM syResources WHERE Resource = 'Aid Received by Date'
    );

    DECLARE @AcademicYiFldId INT;
    SET @AcademicYiFldId =
    (
        SELECT FldId FROM syFields WHERE FldName = 'AcademicYearId'
    );

    DECLARE @saAcademicYearsTblId INT;
    SET @saAcademicYearsTblId =
    (
        SELECT TblId FROM syTables WHERE TblName = 'saAcademicYears'
    );

    DECLARE @tblFldIdAcedemic INT;
    SET @tblFldIdAcedemic =
    (
        SELECT TblFldsId
        FROM syTblFlds
        WHERE FldId = @AcademicYiFldId
              AND TblId = @saAcademicYearsTblId
    );
    IF NOT EXISTS
    (
        SELECT 1
        FROM syRptParams
        WHERE TblFldsId = @tblFldIdAcedemic
              AND ResourceId = @AidRecievedResouceId
    )
    BEGIN
        INSERT INTO syRptParams
        (
            RptParamId,
            ResourceId,
            TblFldsId,
            Required,
            SortSec,
            FilterListSec,
            FilterOtherSec,
            RptCaption
        )
        VALUES
        (   @rptParamId,           -- RptParamId - smallint
            @AidRecievedResouceId, -- ResourceId - smallint
            @tblFldIdAcedemic,     -- TblFldsId - int
            0,                     -- Required - bit
            0,                     -- SortSec - bit
            0,                     -- FilterListSec - bit
            1,                     -- FilterOtherSec - bit
            'Award Year'           -- RptCaption - varchar(50)
            );
    END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION AddAwrYr;
    PRINT 'Failed to add award year filter.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION AddAwrYr;
    PRINT 'added award year filter successfully';
END;
GO
--=================================================================================================
-- END TECH: Aid Received Report in Advantage does not have award year filter
--=================================================================================================
--=================================================================================================
--START  Add Total Hours to Adhoc Enrollment
--=================================================================================================
DECLARE @Error AS INTEGER;
SET @Error = 0;

BEGIN TRANSACTION TotalHrsAdhoc;
BEGIN TRY
    DECLARE @fldId INT;
    DECLARE @tblId INT;
    DECLARE @TblFldsId INT;
    DECLARE @FldCapId INT;
    DECLARE @CatId INT;

    IF EXISTS (SELECT 1 FROM syFields WHERE FldName = 'Total Hours')
    BEGIN
        UPDATE syFields
        SET FldName = 'TotalHours'
        WHERE FldName = 'TotalHours';
    END;
    ELSE
    BEGIN
        IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'TotalHours')
        BEGIN
            SET @fldId =
            (
                SELECT MAX(FldId) FROM syFields
            ) + 1;
            INSERT INTO syFields
            (
                FldId,
                FldName,
                FldTypeId,
                FldLen,
                DDLId,
                DerivedFld,
                SchlReq,
                LogChanges,
                Mask
            )
            VALUES
            (   @fldId,       -- FldId - int
                'TotalHours', -- FldName - varchar(200)
                3,            -- FldTypeId - int
                5,            -- FldLen - int
                NULL,         -- DDLId - int
                NULL,         -- DerivedFld - bit
                NULL,         -- SchlReq - bit
                NULL,         -- LogChanges - bit
                NULL          -- Mask - varchar(50)
                );
            IF (@@ERROR > 0)
            BEGIN
                SET @Error = @@ERROR;
            END;
            SET @tblId =
            (
                SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
            );
            IF NOT EXISTS
            (
                SELECT 1
                FROM syTblFlds
                WHERE TblId = @tblId
                      AND FldId = @fldId
            )
            BEGIN
                SET @TblFldsId =
                (
                    SELECT MAX(TblFldsId) FROM syTblFlds
                ) + 1;
                SET @CatId =
                (
                    SELECT CategoryId
                    FROM syFldCategories
                    WHERE Descrip = 'Enrollment'
                          AND EntityId = 394
                );

                INSERT INTO syTblFlds
                (
                    TblFldsId,
                    TblId,
                    FldId,
                    CategoryId,
                    FKColDescrip
                )
                VALUES
                (   @TblFldsId, -- TblFldsId - int
                    @tblId,     -- TblId - int
                    @fldId,     -- FldId - int
                    @CatId,     -- CategoryId - int 
                    NULL        -- FKColDescrip - varchar(50)
                    );
                IF (@@ERROR > 0)
                BEGIN
                    SET @Error = @@ERROR;
                END;
            END;
            IF NOT EXISTS (SELECT 1 FROM syFldCaptions WHERE FldId = @fldId)
            BEGIN
                SET @FldCapId =
                (
                    SELECT MAX(FldCapId) FROM syFldCaptions
                ) + 1;
                INSERT INTO syFldCaptions
                (
                    FldCapId,
                    FldId,
                    LangId,
                    Caption,
                    FldDescrip
                )
                VALUES
                (   @FldCapId,     -- FldCapId - int
                    @fldId,        -- FldId - int
                    1,             -- LangId - tinyint
                    'Total Hours', -- Caption - varchar(100)
                    NULL           -- FldDescrip - varchar(150)
                    );
                IF (@@ERROR > 0)
                BEGIN
                    SET @Error = @@ERROR;
                END;
            END;
            IF NOT EXISTS (SELECT 1 FROM syFieldCalculation WHERE FldId = @fldId)
                INSERT INTO dbo.syFieldCalculation
                (
                    FldId,
                    CalculationSql
                )
                VALUES
                (   @fldId, -- FldId - int
                    '(Isnull( (  SELECT TOP 1 ( CASE WHEN (   LOWER( LTRIM( RTRIM( dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'' , arStuEnrollments.CampusId)))) = ''byclass''    AND LOWER( LTRIM( RTRIM(dbo.GetAppSettingValueByKeyName(''DisplayAttendanceUnitForProgressReportByClass'' , arStuEnrollments.CampusId)))) = ''hours''     AND AAUT1.UnitTypeDescrip = ''Present Absent'' )  THEN      (   SELECT SUM(ActualPresentDays_ConvertTo_Hours) + SAS_ForTerm.MakeupDays  FROM   syStudentAttendanceSummary       WHERE  StuEnrollId = arStuEnrollments.StuEnrollId ) ELSE CASE WHEN (   LOWER(LTRIM( RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'' , arStuEnrollments.CampusId)))) = ''byclass''     AND LOWER(LTRIM( RTRIM(dbo.GetAppSettingValueByKeyName( ''DisplayAttendanceUnitForProgressReportByClass'' ,arStuEnrollments.CampusId)))) = ''hours'' AND AAUT1.UnitTypeDescrip = ''Minutes'' )THEN CASE WHEN (   SELECT TOP 1 EndDate FROM   arTerm art  INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId     WHERE  PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN (SAS_NoTerm.AttendedDays / 60)+(SAS_NoTerm.MakeupDays / 60) ELSE (SAS_ForTerm.AttendedDays / 60)+(SAS_ForTerm.MakeupDays / 60) END   WHEN (   LOWER( LTRIM( RTRIM( dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'' ,arStuEnrollments.CampusId)))) = ''byclass''  AND LOWER( LTRIM(RTRIM( dbo.GetAppSettingValueByKeyName( ''DisplayAttendanceUnitForProgressReportByClass'' ,arStuEnrollments.CampusId)))) = ''hours'' AND AAUT1.UnitTypeDescrip = ''Clock Hours'' )     THEN CASE WHEN (   SELECT TOP 1 EndDate FROM   arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE  PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN  ( SAS_NoTerm.AttendedDays / 60 ) + (SAS_NoTerm.MakeupDays / 60) ELSE ( SAS_ForTerm.AttendedDays / 60 ) + ( SAS_ForTerm.MakeupDays / 60) END WHEN (   LOWER(LTRIM( RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'' ,arStuEnrollments.CampusId)))) = ''byclass''    AND LOWER( LTRIM(  RTRIM(dbo.GetAppSettingValueByKeyName( ''DisplayAttendanceUnitForProgressReportByClass'' , arStuEnrollments.CampusId)))) = ''presentabsent''  AND AAUT1.UnitTypeDescrip = ''Clock Hours'' )       THEN  CASE WHEN (   SELECT TOP 1 EndDate FROM   arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE  PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN       ( SAS_NoTerm.AttendedDays / 60 ) + (SAS_NoTerm.MakeupDays / 60)  ELSE ( SAS_ForTerm.AttendedDays / 60 ) + (SAS_ForTerm.MakeupDays / 60)  END WHEN (   LOWER(LTRIM( RTRIM( dbo.GetAppSettingValueByKeyName( ''TrackSapAttendance'' , arStuEnrollments.CampusId)))) = ''byclass''          AND LOWER( LTRIM( RTRIM(  dbo.GetAppSettingValueByKeyName(''DisplayAttendanceUnitForProgressReportByClass'' , arStuEnrollments.CampusId)))) = ''presentabsent''AND AAUT1.UnitTypeDescrip = ''Minutes'' )      THEN  CASE WHEN (   SELECT TOP 1 EndDate   FROM   arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE  PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN         (SAS_NoTerm.AttendedDays / 60) + (SAS_NoTerm.MakeupDays / 60) ELSE (SAS_ForTerm.AttendedDays / 60) + (SAS_ForTerm.MakeupDays / 60) END ELSE CASE WHEN (   SELECT TOP 1 EndDate FROM   arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId       WHERE  PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN (SAS_NoTerm.AttendedDays) + ( SAS_NoTerm.MakeupDays ) ELSE (SAS_ForTerm.AttendedDays)+( SAS_ForTerm.MakeupDays ) END END END ) FROM   arAttUnitType AAUT1       INNER JOIN arPrgVersions arp ON arp.UnitTypeId = AAUT1.UnitTypeId  LEFT JOIN (   SELECT   StuEnrollId , MAX(ActualRunningScheduledDays) AS ScheduledDays ,  MAX(AdjustedPresentDays) AS AttendedDays ,          MAX(AdjustedAbsentDays) AS AbsentDays , MAX(ActualRunningMakeupDays) AS MakeupDays ,MAX(StudentAttendedDate) AS LDA  FROM     syStudentAttendanceSummary      GROUP BY StuEnrollId ) SAS_NoTerm ON arStuEnrollments.StuEnrollId = SAS_NoTerm.StuEnrollId  LEFT JOIN (   SELECT   StuEnrollId ,  MAX(ActualRunningScheduledDays) AS ScheduledDays ,         MAX(AdjustedPresentDays) AS AttendedDays , MAX(AdjustedAbsentDays) AS AbsentDays ,  MAX(ActualRunningMakeupDays) AS MakeupDays , MAX(StudentAttendedDate) AS LDA         FROM     syStudentAttendanceSummary  GROUP BY StuEnrollId ) SAS_ForTerm ON arStuEnrollments.StuEnrollId = SAS_ForTerm.StuEnrollId ) ,0) + isnull(arStuEnrollments.TransferHours,0)) AS TotalHours ');

        END;


    END;

END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @Error = 1;
END CATCH;

IF (@Error = 0)
BEGIN
    COMMIT TRANSACTION TotalHrsAdhoc;
END;
ELSE
BEGIN
    ROLLBACK TRANSACTION TotalHrsAdhoc;
END;
GO
--=================================================================================================
--END Add Total Hours to Adhoc Enrollment
--=================================================================================================
--=================================================================================================
-- START Add TotalProgramCost transaction code field to Adhoc
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION TotCostAdhocReport;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Money'
    );
    IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'TotalProgramCost')
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,             -- FldId - int
            'TotalProgramCost', -- FldName - varchar(200)
            @fldTypeId,         -- FldTypeId - int
            10,                 -- FldLen - int
            NULL,               -- DDLId - int
            NULL,               -- DerivedFld - bit
            NULL,               -- SchlReq - bit
            NULL,               -- LogChanges - bit
            NULL                -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Ledger'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,                           -- FldCapId - int
            @fldId,                              -- FldId - int
            1,                                   -- LangId - tinyint
            'Total Program Cost',                -- Caption - varchar(100)
            'Tuition Plus Kit for Adhoc reports' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            NULL         -- FKColDescrip - varchar(50)
            );
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @fldId,                                                                                                                                                                                                                                                                                                                             -- FldId - int
            'ISNULL(( SELECT FORMAT(SUM(T.TransAmount),''C'',''en-us'') FROM   saTransactions T INNER JOIN saTransCodes TC ON TC.TransCodeId = T.TransCodeId WHERE TC.SysTransCodeId IN (1,17) AND  T.StuEnrollId IN ( SELECT StuEnrollId FROM arStuEnrollments b WHERE b.StuEnrollId = arStuEnrollments.StuEnrollId) ),0) AS TotalProgramCost' -- CalculationSql - varchar(8000)
            );
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION TotCostAdhocReport;
    PRINT 'Failed to Add TotalProgramCost to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION TotCostAdhocReport;
    PRINT 'Added TotalProgramCost to Adhoc Report';
END;
GO
--=================================================================================================
-- END  Add TotalProgramCost transaction code field to Adhoc
--=================================================================================================
--=================================================================================================
-- START Add Monthly Payments transaction code field to Adhoc
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION MonthlyPaymentsAdhocReport;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Money'
    );
    IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'MonthlyPayments')
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,            -- FldId - int
            'MonthlyPayments', -- FldName - varchar(200)
            @fldTypeId,        -- FldTypeId - int
            10,                -- FldLen - int
            NULL,              -- DDLId - int
            NULL,              -- DerivedFld - bit
            NULL,              -- SchlReq - bit
            NULL,              -- LogChanges - bit
            NULL               -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Payments'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,                                  -- FldCapId - int
            @fldId,                                     -- FldId - int
            1,                                          -- LangId - tinyint
            'Monthly Payments',                         -- Caption - varchar(100)
            'Monthly Payments on the Payment Plan Page' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            NULL         -- FKColDescrip - varchar(50)
            );
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @fldId,                                                                                                                                                                                   -- FldId - int
            ' (SELECT FORMAT(CAST((TotalAmountDue/Disbursements) AS DECIMAL(19,2)),''C'',''en-us'') FROM faStudentPaymentPlans WHERE StuEnrollId = arStuEnrollments.StuEnrollId) AS MonthlyPayments ' -- CalculationSql - varchar(8000)
            );
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION MonthlyPaymentsAdhocReport;
    PRINT 'Failed to Add MonthlyPayments to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION MonthlyPaymentsAdhocReport;
    PRINT 'Added MonthlyPayments to Adhoc Report';
END;
GO
--=================================================================================================
-- END  Add MonthlyPayments transaction code field to Adhoc
--=================================================================================================
--=================================================================================================
-- START Add Total Aid Due field to Adhoc
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION TotalAidDueAdhocReport;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Money'
    );
    IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'TotalAidDue')
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,        -- FldId - int
            'TotalAidDue', -- FldName - varchar(200)
            @fldTypeId,    -- FldTypeId - int
            10,            -- FldLen - int
            NULL,          -- DDLId - int
            NULL,          -- DerivedFld - bit
            NULL,          -- SchlReq - bit
            NULL,          -- LogChanges - bit
            NULL           -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Awards'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,                                  -- FldCapId - int
            @fldId,                                     -- FldId - int
            1,                                          -- LangId - tinyint
            'Total Aid Due',                            -- Caption - varchar(100)
            'Total Aid Due Amount in the Awards screen' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            NULL         -- FKColDescrip - varchar(50)
            );
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @fldId,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           -- FldId - int
            '     FORMAT(( ISNULL( ((   SELECT ( ISNULL(SUM(ISNULL(GrossAmount, 0)), 0) - ISNULL(SUM(ISNULL(LoanFees, 0)), 0)) AS netAmt FROM   faStudentAwards  WHERE  StuEnrollId = arStuEnrollments.StuEnrollId )    - ((   SELECT ISNULL(SUM(ISNULL(A.recAmt, 0)), 0) AS ReceivedAmt FROM   ( SELECT  ISNULL( SUM(ISNULL(tr.TransAmount, 0)) , 0) AS recAmt FROM     dbo.faStudentAwards INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId     INNER JOIN dbo.saTransactions tr ON tr.TransactionId = dbo.saPmtDisbRel.TransactionId WHERE    dbo.faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId GROUP BY faStudentAwards.StudentAwardId ) AS A ) * -1 )) , 0)) + (   SELECT ISNULL(SUM(refAmt), 0)    FROM   (   SELECT   RefundAmount AS refAmt  FROM     dbo.faStudentAwards INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId   INNER JOIN saRefunds ON saRefunds.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId WHERE    dbo.faStudentAwards.StuEnrollId = dbo.arStuEnrollments.StuEnrollId GROUP BY faStudentAwards.StudentAwardId ,    saRefunds.RefundAmount ) AS RefundMoney ) , ''C'' ,''en-us'') AS TotalAidDue  ' -- CalculationSql - varchar(8000)
            );
    END;
    ELSE
    BEGIN
        DECLARE @tdFldId INT;
        SET @tdFldId =
        (
            SELECT FldId FROM syFields WHERE FldName = 'TotalAidDue'
        );
        UPDATE syFieldCalculation
        SET CalculationSql = '     FORMAT(( ISNULL( ((   SELECT ( ISNULL(SUM(ISNULL(GrossAmount, 0)), 0) - ISNULL(SUM(ISNULL(LoanFees, 0)), 0)) AS netAmt FROM   faStudentAwards  WHERE  StuEnrollId = arStuEnrollments.StuEnrollId )    - ((   SELECT ISNULL(SUM(ISNULL(A.recAmt, 0)), 0) AS ReceivedAmt FROM   ( SELECT  ISNULL( SUM(ISNULL(tr.TransAmount, 0)) , 0) AS recAmt FROM     dbo.faStudentAwards INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId     INNER JOIN dbo.saTransactions tr ON tr.TransactionId = dbo.saPmtDisbRel.TransactionId WHERE    dbo.faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId GROUP BY faStudentAwards.StudentAwardId ) AS A ) * -1 )) , 0)) + (   SELECT ISNULL(SUM(refAmt), 0)    FROM   (   SELECT   RefundAmount AS refAmt  FROM     dbo.faStudentAwards INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId   INNER JOIN saRefunds ON saRefunds.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId WHERE    dbo.faStudentAwards.StuEnrollId = dbo.arStuEnrollments.StuEnrollId GROUP BY faStudentAwards.StudentAwardId ,    saRefunds.RefundAmount ) AS RefundMoney ) , ''C'' ,''en-us'') AS TotalAidDue  '
        WHERE FldId = @tdFldId;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION TotalAidDueAdhocReport;
    PRINT 'Failed to Add TotalAidDue to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION TotalAidDueAdhocReport;
    PRINT 'Added TotalAidDue to Adhoc Report';
END;
GO
--=================================================================================================
-- END  Add Total Aid Due field to Adhoc
--=================================================================================================
--=================================================================================================
-- START Add Total Aid Expected field to Adhoc
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION TotalAidExpectedAdhocReport;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Money'
    );
    IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'TotalAidExpected')
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,             -- FldId - int
            'TotalAidExpected', -- FldName - varchar(200)
            @fldTypeId,         -- FldTypeId - int
            10,                 -- FldLen - int
            NULL,               -- DDLId - int
            NULL,               -- DerivedFld - bit
            NULL,               -- SchlReq - bit
            NULL,               -- LogChanges - bit
            NULL                -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Awards'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,                                       -- FldCapId - int
            @fldId,                                          -- FldId - int
            1,                                               -- LangId - tinyint
            'Total Aid Expected',                            -- Caption - varchar(100)
            'Total Aid Expected Amount in the Awards screen' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            NULL         -- FKColDescrip - varchar(50)
            );
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @fldId,                                                                                                                                                                                                                          -- FldId - int
            ' ( SELECT FORMAT(( ISNULL(SUM(ISNULL(GrossAmount, 0)), 0) - ISNULL(SUM(ISNULL(LoanFees, 0)), 0)) , ''C'' ,''en-us'') AS netAmt FROM   faStudentAwards WHERE  StuEnrollId = arStuEnrollments.StuEnrollId ) AS TotalAidExpected ' -- CalculationSql - varchar(8000)
            );
    END;
    ELSE
    BEGIN
        DECLARE @teFldId INT;
        SET @teFldId =
        (
            SELECT FldId FROM syFields WHERE FldName = 'TotalAidExpected'
        );
        UPDATE syFieldCalculation
        SET CalculationSql = ' ( SELECT FORMAT(( ISNULL(SUM(ISNULL(GrossAmount, 0)), 0) - ISNULL(SUM(ISNULL(LoanFees, 0)), 0)) , ''C'' ,''en-us'') AS netAmt FROM   faStudentAwards WHERE  StuEnrollId = arStuEnrollments.StuEnrollId ) AS TotalAidExpected '
        WHERE FldId = @teFldId;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION TotalAidExpectedAdhocReport;
    PRINT 'Failed to Add TotalAidExpected to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION TotalAidExpectedAdhocReport;
    PRINT 'Added TotalAidExpected to Adhoc Report';
END;
GO
--=================================================================================================
-- END  Add Total Aid Expected field to Adhoc
--=================================================================================================
--=================================================================================================
-- START Add Total Aid Received field to Adhoc
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION TotalAidReceivedAdhocReport;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Money'
    );
    IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'TotalAidReceived')
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,             -- FldId - int
            'TotalAidReceived', -- FldName - varchar(200)
            @fldTypeId,         -- FldTypeId - int
            10,                 -- FldLen - int
            NULL,               -- DDLId - int
            NULL,               -- DerivedFld - bit
            NULL,               -- SchlReq - bit
            NULL,               -- LogChanges - bit
            NULL                -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Awards'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,                                   -- FldCapId - int
            @fldId,                                      -- FldId - int
            1,                                           -- LangId - tinyint
            'Total Aid Received',                        -- Caption - varchar(100)
            'Total Recieved Amount in the Awards screen' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            NULL         -- FKColDescrip - varchar(50)
            );
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @fldId,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              -- FldId - int
            '    FORMAT(((   SELECT ( ISNULL(SUM(ISNULL(A.recAmt, 0)), 0) * -1 ) AS ReceivedAmt FROM   (   SELECT   ISNULL(SUM(tr.TransAmount), 0) AS recAmt FROM     dbo.faStudentAwards  INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId  INNER JOIN dbo.saTransactions tr ON tr.TransactionId = dbo.saPmtDisbRel.TransactionId WHERE    dbo.faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId GROUP BY faStudentAwards.StudentAwardId ) AS A )    - (   SELECT ISNULL(SUM(refAmt), 0)  FROM   (   SELECT   RefundAmount AS refAmt FROM     dbo.faStudentAwards  INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId   INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId INNER JOIN saRefunds ON saRefunds.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId   WHERE    dbo.faStudentAwards.StuEnrollId = dbo.arStuEnrollments.StuEnrollId GROUP BY faStudentAwards.StudentAwardId ,  saRefunds.RefundAmount ) AS RefundMoney )) ,''C'' ,''en-us'') AS TotalAidReceived  ' -- CalculationSql - varchar(8000)
            );
    END;
    ELSE
    BEGIN
        DECLARE @trFldId INT;
        SET @trFldId =
        (
            SELECT FldId FROM syFields WHERE FldName = 'TotalAidReceived'
        );
        UPDATE syFieldCalculation
        SET CalculationSql = '    FORMAT(((   SELECT ( ISNULL(SUM(ISNULL(A.recAmt, 0)), 0) * -1 ) AS ReceivedAmt FROM   (   SELECT   ISNULL(SUM(tr.TransAmount), 0) AS recAmt FROM     dbo.faStudentAwards  INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId  INNER JOIN dbo.saTransactions tr ON tr.TransactionId = dbo.saPmtDisbRel.TransactionId WHERE    dbo.faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId GROUP BY faStudentAwards.StudentAwardId ) AS A )    - (   SELECT ISNULL(SUM(refAmt), 0)  FROM   (   SELECT   RefundAmount AS refAmt FROM     dbo.faStudentAwards  INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId   INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId INNER JOIN saRefunds ON saRefunds.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId   WHERE    dbo.faStudentAwards.StuEnrollId = dbo.arStuEnrollments.StuEnrollId GROUP BY faStudentAwards.StudentAwardId ,  saRefunds.RefundAmount ) AS RefundMoney )) ,''C'' ,''en-us'') AS TotalAidReceived  '
        WHERE FldId = @trFldId;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION TotalAidReceivedAdhocReport;
    PRINT 'Failed to Add TotalAidReceived to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION TotalAidReceivedAdhocReport;
    PRINT 'Added TotalAidReceived to Adhoc Report';
END;
GO
--=================================================================================================
-- END  Add Total Aid Received field to Adhoc
--=================================================================================================
--=================================================================================================
-- START Add Total NonTitle IV Aid Due field to Adhoc
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION TotalNonTitleIVAidDueAdhocReport;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Money'
    );
    IF NOT EXISTS
    (
        SELECT 1
        FROM syFields
        WHERE FldName = 'TotalNonTitleIVAidDue'
    )
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,                  -- FldId - int
            'TotalNonTitleIVAidDue', -- FldName - varchar(200)
            @fldTypeId,              -- FldTypeId - int
            10,                      -- FldLen - int
            NULL,                    -- DDLId - int
            NULL,                    -- DerivedFld - bit
            NULL,                    -- SchlReq - bit
            NULL,                    -- LogChanges - bit
            NULL                     -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Awards'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,                                        -- FldCapId - int
            @fldId,                                           -- FldId - int
            1,                                                -- LangId - tinyint
            'Total Non Title IV Aid Due',                     -- Caption - varchar(100)
            'Total Non Title IV Aid Due in the Awards screen' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            NULL         -- FKColDescrip - varchar(50)
            );
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @fldId,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             -- FldId - int
            '    FORMAT((( SELECT ISNULL(((   SELECT ( ISNULL(SUM(ISNULL(GrossAmount, 0)), 0) - ISNULL(SUM(ISNULL(LoanFees, 0)), 0)) AS netAmt FROM   faStudentAwards INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId     WHERE  TitleIV = 0 AND StuEnrollId = arStuEnrollments.StuEnrollId ) - ((   SELECT ISNULL(SUM(ISNULL(recAmt, 0)), 0)  FROM   (   SELECT   ISNULL( SUM(ISNULL( tr.TransAmount , 0)) , 0) AS recAmt  FROM     dbo.faStudentAwards INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId     INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId INNER JOIN dbo.saTransactions tr ON tr.TransactionId = dbo.saPmtDisbRel.TransactionId INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId    WHERE    dbo.faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId AND TitleIV = 0 GROUP BY faStudentAwards.StudentAwardId ) AS NTR ) * -1 )) ,  0))) + (   SELECT ISNULL(SUM(refAmt), 0) FROM   (   SELECT   RefundAmount AS refAmt   FROM     dbo.faStudentAwards INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId     INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId INNER JOIN saRefunds ON saRefunds.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId     INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId WHERE    dbo.faStudentAwards.StuEnrollId = dbo.arStuEnrollments.StuEnrollId AND TitleIV = 0      GROUP BY faStudentAwards.StudentAwardId , saRefunds.RefundAmount ) AS RefundMoney ) , ''C'' , ''en-us'') AS TotalNonTitleIVAidDue ' -- CalculationSql - varchar(8000)
            );
    END;
    ELSE
    BEGIN
        DECLARE @nonTitleFldId INT;
        SET @nonTitleFldId =
        (
            SELECT FldId FROM syFields WHERE FldName = 'TotalNonTitleIVAidDue'
        );
        UPDATE syFldCaptions
        SET Caption = 'Total Non Title IV Aid Due'
        WHERE FldId = @nonTitleFldId;
        UPDATE syFieldCalculation
        SET CalculationSql = '    FORMAT((( SELECT ISNULL(((   SELECT ( ISNULL(SUM(ISNULL(GrossAmount, 0)), 0) - ISNULL(SUM(ISNULL(LoanFees, 0)), 0)) AS netAmt FROM   faStudentAwards INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId     WHERE  TitleIV = 0 AND StuEnrollId = arStuEnrollments.StuEnrollId ) - ((   SELECT ISNULL(SUM(ISNULL(recAmt, 0)), 0)  FROM   (   SELECT   ISNULL( SUM(ISNULL( tr.TransAmount , 0)) , 0) AS recAmt  FROM     dbo.faStudentAwards INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId     INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId INNER JOIN dbo.saTransactions tr ON tr.TransactionId = dbo.saPmtDisbRel.TransactionId INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId    WHERE    dbo.faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId AND TitleIV = 0 GROUP BY faStudentAwards.StudentAwardId ) AS NTR ) * -1 )) ,  0))) + (   SELECT ISNULL(SUM(refAmt), 0) FROM   (   SELECT   RefundAmount AS refAmt   FROM     dbo.faStudentAwards INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId     INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId INNER JOIN saRefunds ON saRefunds.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId     INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId WHERE    dbo.faStudentAwards.StuEnrollId = dbo.arStuEnrollments.StuEnrollId AND TitleIV = 0      GROUP BY faStudentAwards.StudentAwardId , saRefunds.RefundAmount ) AS RefundMoney ) , ''C'' , ''en-us'') AS TotalNonTitleIVAidDue '
        WHERE FldId = @nonTitleFldId;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION TotalNonTitleIVAidDueAdhocReport;
    PRINT 'Failed to Add TotalNonTitleIVAidDue to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION TotalNonTitleIVAidDueAdhocReport;
    PRINT 'Added TotalNonTitleIVAidDue to Adhoc Report';
END;
GO
--=================================================================================================
-- END  Add Total NonTitle IV Aid Due field to Adhoc
--=================================================================================================
--=================================================================================================
-- START Add Total Non Title IV Aid Expected field to Adhoc
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION TotalNonTitleIVAidExpAdhocReport;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Money'
    );
    IF NOT EXISTS
    (
        SELECT 1
        FROM syFields
        WHERE FldName = 'TotalNonTitleIVAidExpected'
    )
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,                       -- FldId - int
            'TotalNonTitleIVAidExpected', -- FldName - varchar(200)
            @fldTypeId,                   -- FldTypeId - int
            10,                           -- FldLen - int
            NULL,                         -- DDLId - int
            NULL,                         -- DerivedFld - bit
            NULL,                         -- SchlReq - bit
            NULL,                         -- LogChanges - bit
            NULL                          -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Awards'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,                                             -- FldCapId - int
            @fldId,                                                -- FldId - int
            1,                                                     -- LangId - tinyint
            'Total Non Title IV Aid Expected',                     -- Caption - varchar(100)
            'Total Non Title IV Aid Expected in the Awards screen' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            NULL         -- FKColDescrip - varchar(50)
            );
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @fldId,                                                                                                                                                                                                                                                                                                                                            -- FldId - int
            ' ( SELECT FORMAT(  ( ISNULL(SUM(ISNULL(GrossAmount, 0)), 0) - ISNULL(SUM(ISNULL(LoanFees, 0)), 0)) ,''C'' , ''en-us'') AS netAmt FROM   faStudentAwards INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId  WHERE  TitleIV = 0 AND StuEnrollId = arStuEnrollments.StuEnrollId ) AS TotalNonTitleIVAidExpected ' -- CalculationSql - varchar(8000)
            );
    END;
    ELSE
    BEGIN
        DECLARE @tteFldId INT;
        SET @tteFldId =
        (
            SELECT FldId FROM syFields WHERE FldName = 'TotalNonTitleIVAidExpected'
        );
        UPDATE syFieldCalculation
        SET CalculationSql = ' ( SELECT FORMAT(  ( ISNULL(SUM(ISNULL(GrossAmount, 0)), 0) - ISNULL(SUM(ISNULL(LoanFees, 0)), 0)) ,''C'' , ''en-us'') AS netAmt FROM   faStudentAwards INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId  WHERE  TitleIV = 0 AND StuEnrollId = arStuEnrollments.StuEnrollId ) AS TotalNonTitleIVAidExpected '
        WHERE FldId = @tteFldId;
    END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION TotalNonTitleIVAidExpAdhocReport;
    PRINT 'Failed to Add TotalNonTitleIVAidExpected to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION TotalNonTitleIVAidExpAdhocReport;
    PRINT 'Added TotalNonTitleIVAidExpected to Adhoc Report';
END;
GO
--=================================================================================================
-- END  Add TotalNonTitleIVAidExpected field to Adhoc
--=================================================================================================
--=================================================================================================
-- START Add Total Non Title IV Aid Received field to Adhoc
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION TotalNonTitleIVAidRecAdhocReport;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Money'
    );
    IF NOT EXISTS
    (
        SELECT 1
        FROM syFields
        WHERE FldName = 'TotalNonTitleIVAidReceived'
    )
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,                       -- FldId - int
            'TotalNonTitleIVAidReceived', -- FldName - varchar(200)
            @fldTypeId,                   -- FldTypeId - int
            10,                           -- FldLen - int
            NULL,                         -- DDLId - int
            NULL,                         -- DerivedFld - bit
            NULL,                         -- SchlReq - bit
            NULL,                         -- LogChanges - bit
            NULL                          -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Awards'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,                                             -- FldCapId - int
            @fldId,                                                -- FldId - int
            1,                                                     -- LangId - tinyint
            'Total Non Title IV Aid Received',                     -- Caption - varchar(100)
            'Total Non Title IV Aid Received in the Awards screen' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            NULL         -- FKColDescrip - varchar(50)
            );
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @fldId,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  -- FldId - int
            '  FORMAT((( SELECT ( ISNULL(SUM(ISNULL(NTR.recAmt, 0)), 0) * -1 ) AS NonTitleIVReceivedAmt  FROM   (   SELECT   ISNULL(SUM(tr.TransAmount), 0) AS recAmt FROM     dbo.faStudentAwards INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId    INNER JOIN dbo.saTransactions tr ON tr.TransactionId = dbo.saPmtDisbRel.TransactionId  INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId WHERE    dbo.faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId AND TitleIV = 0  GROUP BY faStudentAwards.StudentAwardId ) AS NTR )  - (   SELECT ISNULL(SUM(refAmt), 0)     FROM   (   SELECT   RefundAmount AS refAmt FROM     dbo.faStudentAwards  INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId    INNER JOIN saRefunds ON saRefunds.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId WHERE    dbo.faStudentAwards.StuEnrollId = dbo.arStuEnrollments.StuEnrollId AND TitleIV = 0    GROUP BY faStudentAwards.StudentAwardId , saRefunds.RefundAmount ) AS RefundMoney )) ,''C'' ,''en-us'') AS TotalNonTitleIVAidReceived  ' -- CalculationSql - varchar(8000)
            );
    END;
    ELSE
    BEGIN
        DECLARE @tntrFldId INT;
        SET @tntrFldId =
        (
            SELECT FldId FROM syFields WHERE FldName = 'TotalNonTitleIVAidReceived'
        );
        UPDATE syFieldCalculation
        SET CalculationSql = '  FORMAT((( SELECT ( ISNULL(SUM(ISNULL(NTR.recAmt, 0)), 0) * -1 ) AS NonTitleIVReceivedAmt  FROM   (   SELECT   ISNULL(SUM(tr.TransAmount), 0) AS recAmt FROM     dbo.faStudentAwards INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId    INNER JOIN dbo.saTransactions tr ON tr.TransactionId = dbo.saPmtDisbRel.TransactionId  INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId WHERE    dbo.faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId AND TitleIV = 0  GROUP BY faStudentAwards.StudentAwardId ) AS NTR )  - (   SELECT ISNULL(SUM(refAmt), 0)     FROM   (   SELECT   RefundAmount AS refAmt FROM     dbo.faStudentAwards  INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId    INNER JOIN saRefunds ON saRefunds.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId WHERE    dbo.faStudentAwards.StuEnrollId = dbo.arStuEnrollments.StuEnrollId AND TitleIV = 0    GROUP BY faStudentAwards.StudentAwardId , saRefunds.RefundAmount ) AS RefundMoney )) ,''C'' ,''en-us'') AS TotalNonTitleIVAidReceived  '
        WHERE FldId = @tntrFldId;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION TotalNonTitleIVAidRecAdhocReport;
    PRINT 'Failed to Add TotalNonTitleIVAidReceived to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION TotalNonTitleIVAidRecAdhocReport;
    PRINT 'Added TotalNonTitleIVAidReceived to Adhoc Report';
END;
GO
--=================================================================================================
-- END  Add TotalNonTitleIVAidReceived field to Adhoc
--=================================================================================================
--=================================================================================================
-- START Add Total Title IV Aid Due field to Adhoc
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION TotalTitleIVAidDueAdhocReport;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Money'
    );
    IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'TotalTitleIVAidDue')
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,               -- FldId - int
            'TotalTitleIVAidDue', -- FldName - varchar(200)
            @fldTypeId,           -- FldTypeId - int
            10,                   -- FldLen - int
            NULL,                 -- DDLId - int
            NULL,                 -- DerivedFld - bit
            NULL,                 -- SchlReq - bit
            NULL,                 -- LogChanges - bit
            NULL                  -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Awards'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,                                    -- FldCapId - int
            @fldId,                                       -- FldId - int
            1,                                            -- LangId - tinyint
            'Total Title IV Aid Due',                     -- Caption - varchar(100)
            'Total Title IV Aid Due in the Awards screen' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            NULL         -- FKColDescrip - varchar(50)
            );
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @fldId,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               -- FldId - int
            '        FORMAT( (( SELECT ISNULL(((   SELECT ( ISNULL(SUM(ISNULL(GrossAmount, 0)), 0) - ISNULL(SUM(ISNULL(LoanFees, 0)), 0)) AS netAmt FROM   faStudentAwards INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId WHERE  TitleIV = 1 AND StuEnrollId = arStuEnrollments.StuEnrollId ) - ((   SELECT ISNULL(SUM(ISNULL(recAmt, 0)), 0)    FROM   (   SELECT   ISNULL( SUM(ISNULL( tr.TransAmount , 0)) , 0) AS recAmt FROM     dbo.faStudentAwards  INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId     INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId  INNER JOIN dbo.saTransactions tr ON tr.TransactionId = dbo.saPmtDisbRel.TransactionId   INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId WHERE    dbo.faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId AND TitleIV = 1  GROUP BY faStudentAwards.StudentAwardId ) AS NTR ) * -1 )) , 0)) + (   SELECT ISNULL(SUM(refAmt), 0)  FROM   (   SELECT   RefundAmount AS refAmt      FROM     dbo.faStudentAwards INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId     INNER JOIN saRefunds ON saRefunds.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId   WHERE    dbo.faStudentAwards.StuEnrollId = dbo.arStuEnrollments.StuEnrollId    AND TitleIV = 1 GROUP BY faStudentAwards.StudentAwardId , saRefunds.RefundAmount ) AS RefundMoney )) , ''C'' ,''en-us'') AS TotalTitleIVAidDue    ' -- CalculationSql - varchar(8000)
            );
    END;
    ELSE
    BEGIN
        DECLARE @ttvdFldId INT;
        SET @ttvdFldId =
        (
            SELECT FldId FROM syFields WHERE FldName = 'TotalTitleIVAidDue'
        );
        UPDATE syFieldCalculation
        SET CalculationSql = '        FORMAT( (( SELECT ISNULL(((   SELECT ( ISNULL(SUM(ISNULL(GrossAmount, 0)), 0) - ISNULL(SUM(ISNULL(LoanFees, 0)), 0)) AS netAmt FROM   faStudentAwards INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId WHERE  TitleIV = 1 AND StuEnrollId = arStuEnrollments.StuEnrollId ) - ((   SELECT ISNULL(SUM(ISNULL(recAmt, 0)), 0)    FROM   (   SELECT   ISNULL( SUM(ISNULL( tr.TransAmount , 0)) , 0) AS recAmt FROM     dbo.faStudentAwards  INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId     INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId  INNER JOIN dbo.saTransactions tr ON tr.TransactionId = dbo.saPmtDisbRel.TransactionId   INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId WHERE    dbo.faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId AND TitleIV = 1  GROUP BY faStudentAwards.StudentAwardId ) AS NTR ) * -1 )) , 0)) + (   SELECT ISNULL(SUM(refAmt), 0)  FROM   (   SELECT   RefundAmount AS refAmt      FROM     dbo.faStudentAwards INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId     INNER JOIN saRefunds ON saRefunds.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId   WHERE    dbo.faStudentAwards.StuEnrollId = dbo.arStuEnrollments.StuEnrollId    AND TitleIV = 1 GROUP BY faStudentAwards.StudentAwardId , saRefunds.RefundAmount ) AS RefundMoney )) , ''C'' ,''en-us'') AS TotalTitleIVAidDue    '
        WHERE FldId = @ttvdFldId;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION TotalTitleIVAidDueAdhocReport;
    PRINT 'Failed to Add TotalTitleIVAidDue to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION TotalTitleIVAidDueAdhocReport;
    PRINT 'Added TotalTitleIVAidDue to Adhoc Report';
END;
GO
--=================================================================================================
-- END  Add Total Aid Due field to Adhoc
--=================================================================================================
--=================================================================================================
-- START Add Total Title IV Aid Expected field to Adhoc
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION TotalTitleIVAidExpAdhocReport;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Money'
    );
    IF NOT EXISTS
    (
        SELECT 1
        FROM syFields
        WHERE FldName = 'TotalTitleIVAidExpected'
    )
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,                    -- FldId - int
            'TotalTitleIVAidExpected', -- FldName - varchar(200)
            @fldTypeId,                -- FldTypeId - int
            10,                        -- FldLen - int
            NULL,                      -- DDLId - int
            NULL,                      -- DerivedFld - bit
            NULL,                      -- SchlReq - bit
            NULL,                      -- LogChanges - bit
            NULL                       -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Awards'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,                                         -- FldCapId - int
            @fldId,                                            -- FldId - int
            1,                                                 -- LangId - tinyint
            'Total Title IV Aid Expected',                     -- Caption - varchar(100)
            'Total Title IV Aid Expected in the Awards screen' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            NULL         -- FKColDescrip - varchar(50)
            );
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @fldId,                                                                                                                                                                                                                                                                                                                                         -- FldId - int
            '    ( SELECT FORMAT( ( ISNULL(SUM(ISNULL(GrossAmount, 0)), 0) - ISNULL(SUM(ISNULL(LoanFees, 0)), 0)) ,''C'' ,''en-us'') AS netAmt FROM   faStudentAwards INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId WHERE  TitleIV = 1 AND StuEnrollId = arStuEnrollments.StuEnrollId ) AS TotalTitleIVAidExpected ' -- CalculationSql - varchar(8000)
            );
    END;
    ELSE
    BEGIN
        DECLARE @ttveFldId INT;
        SET @ttveFldId =
        (
            SELECT FldId FROM syFields WHERE FldName = 'TotalTitleIVAidExpected'
        );
        UPDATE syFieldCalculation
        SET CalculationSql = '    ( SELECT FORMAT( ( ISNULL(SUM(ISNULL(GrossAmount, 0)), 0) - ISNULL(SUM(ISNULL(LoanFees, 0)), 0)) ,''C'' ,''en-us'') AS netAmt FROM   faStudentAwards INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId WHERE  TitleIV = 1 AND StuEnrollId = arStuEnrollments.StuEnrollId ) AS TotalTitleIVAidExpected '
        WHERE FldId = @ttveFldId;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION TotalTitleIVAidExpAdhocReport;
    PRINT 'Failed to Add TotalTitleIVAidExpected to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION TotalTitleIVAidExpAdhocReport;
    PRINT 'Added TotalTitleIVAidExpected to Adhoc Report';
END;
GO
--=================================================================================================
-- END  Add Total Title IV Aid Expected field to Adhoc
--=================================================================================================
--=================================================================================================
-- START Add Total Title IV Aid Received field to Adhoc
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION TotalTitleIVAidRecAdhocReport;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Money'
    );
    IF NOT EXISTS
    (
        SELECT 1
        FROM syFields
        WHERE FldName = 'TotalTitleIVAidReceived'
    )
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,                    -- FldId - int
            'TotalTitleIVAidReceived', -- FldName - varchar(200)
            @fldTypeId,                -- FldTypeId - int
            10,                        -- FldLen - int
            NULL,                      -- DDLId - int
            NULL,                      -- DerivedFld - bit
            NULL,                      -- SchlReq - bit
            NULL,                      -- LogChanges - bit
            NULL                       -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Awards'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,                                         -- FldCapId - int
            @fldId,                                            -- FldId - int
            1,                                                 -- LangId - tinyint
            'Total Title IV Aid Received',                     -- Caption - varchar(100)
            'Total Title IV Aid Received in the Awards screen' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            NULL         -- FKColDescrip - varchar(50)
            );
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @fldId,                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  -- FldId - int
            '    FORMAT((( SELECT ( ISNULL(SUM(ISNULL(TR.recAmt, 0)), 0) * -1 ) AS NonTitleIVReceivedAmt FROM   (   SELECT   ISNULL(SUM(TR.TransAmount), 0) AS recAmt FROM     dbo.faStudentAwards INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId    INNER JOIN dbo.saTransactions TR ON TR.TransactionId = dbo.saPmtDisbRel.TransactionId INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId WHERE    dbo.faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId AND TitleIV = 1     GROUP BY faStudentAwards.StudentAwardId ) AS TR ) - (   SELECT ISNULL(SUM(refAmt), 0) FROM   (   SELECT   RefundAmount AS refAmt FROM     dbo.faStudentAwards INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId     INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId INNER JOIN saRefunds ON saRefunds.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId     INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId WHERE    dbo.faStudentAwards.StuEnrollId = dbo.arStuEnrollments.StuEnrollId AND TitleIV = 1     GROUP BY faStudentAwards.StudentAwardId , saRefunds.RefundAmount ) AS RefundMoney )) , ''C'' , ''en-us'') AS TotalTitleIVAidReceived  ' -- CalculationSql - varchar(8000)
            );
    END;
    ELSE
    BEGIN
        DECLARE @ttvrFldId INT;
        SET @ttvrFldId =
        (
            SELECT FldId FROM syFields WHERE FldName = 'TotalTitleIVAidReceived'
        );
        UPDATE syFieldCalculation
        SET CalculationSql = '    FORMAT((( SELECT ( ISNULL(SUM(ISNULL(TR.recAmt, 0)), 0) * -1 ) AS NonTitleIVReceivedAmt FROM   (   SELECT   ISNULL(SUM(TR.TransAmount), 0) AS recAmt FROM     dbo.faStudentAwards INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId    INNER JOIN dbo.saTransactions TR ON TR.TransactionId = dbo.saPmtDisbRel.TransactionId INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId WHERE    dbo.faStudentAwards.StuEnrollId = arStuEnrollments.StuEnrollId AND TitleIV = 1     GROUP BY faStudentAwards.StudentAwardId ) AS TR ) - (   SELECT ISNULL(SUM(refAmt), 0) FROM   (   SELECT   RefundAmount AS refAmt FROM     dbo.faStudentAwards INNER JOIN dbo.faStudentAwardSchedule ON faStudentAwardSchedule.StudentAwardId = faStudentAwards.StudentAwardId     INNER JOIN dbo.saPmtDisbRel ON saPmtDisbRel.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId INNER JOIN saRefunds ON saRefunds.AwardScheduleId = faStudentAwardSchedule.AwardScheduleId     INNER JOIN saFundSources ON saFundSources.FundSourceId = faStudentAwards.AwardTypeId WHERE    dbo.faStudentAwards.StuEnrollId = dbo.arStuEnrollments.StuEnrollId AND TitleIV = 1     GROUP BY faStudentAwards.StudentAwardId , saRefunds.RefundAmount ) AS RefundMoney )) , ''C'' , ''en-us'') AS TotalTitleIVAidReceived  '
        WHERE FldId = @ttvrFldId;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION TotalTitleIVAidRecAdhocReport;
    PRINT 'Failed to Add TotalTitleIVAidReceived to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION TotalTitleIVAidRecAdhocReport;
    PRINT 'Added TotalTitleIVAidReceived to Adhoc Report';
END;
GO
--=================================================================================================
-- END  Add Total Aid Received field to Adhoc
--=================================================================================================
--=================================================================================================
--START Add Remaining Hours to Adhoc Enrollment
--=================================================================================================
DECLARE @Error AS INTEGER;
SET @Error = 0;

BEGIN TRANSACTION RemainingHrsAdhoc;
BEGIN TRY
    DECLARE @fldId INT;
    DECLARE @tblId INT;
    DECLARE @TblFldsId INT;
    DECLARE @FldCapId INT;
    DECLARE @CatId INT;

    IF EXISTS (SELECT 1 FROM syFields WHERE FldName = 'Remaining Hours')
    BEGIN
        UPDATE syFields
        SET FldName = 'RemainingHours'
        WHERE FldName = 'RemainingHours';
    END;
    ELSE
    BEGIN
        IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'RemainingHours')
        BEGIN
            SET @fldId =
            (
                SELECT MAX(FldId) FROM syFields
            ) + 1;
            INSERT INTO syFields
            (
                FldId,
                FldName,
                FldTypeId,
                FldLen,
                DDLId,
                DerivedFld,
                SchlReq,
                LogChanges,
                Mask
            )
            VALUES
            (   @fldId,           -- FldId - int
                'RemainingHours', -- FldName - varchar(200)
                5,                -- FldTypeId - int
                8,                -- FldLen - int
                NULL,             -- DDLId - int
                NULL,             -- DerivedFld - bit
                NULL,             -- SchlReq - bit
                NULL,             -- LogChanges - bit
                NULL              -- Mask - varchar(50)
                );
            IF (@@ERROR > 0)
            BEGIN
                SET @Error = @@ERROR;
            END;
            SET @tblId =
            (
                SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
            );
            IF NOT EXISTS
            (
                SELECT 1
                FROM syTblFlds
                WHERE TblId = @tblId
                      AND FldId = @fldId
            )
            BEGIN
                SET @TblFldsId =
                (
                    SELECT MAX(TblFldsId) FROM syTblFlds
                ) + 1;
                SET @CatId =
                (
                    SELECT CategoryId
                    FROM syFldCategories
                    WHERE Descrip = 'Enrollment'
                          AND EntityId = 394
                );

                INSERT INTO syTblFlds
                (
                    TblFldsId,
                    TblId,
                    FldId,
                    CategoryId,
                    FKColDescrip
                )
                VALUES
                (   @TblFldsId, -- TblFldsId - int
                    @tblId,     -- TblId - int
                    @fldId,     -- FldId - int
                    @CatId,     -- CategoryId - int 
                    NULL        -- FKColDescrip - varchar(50)
                    );
                IF (@@ERROR > 0)
                BEGIN
                    SET @Error = @@ERROR;
                END;
            END;
            IF NOT EXISTS (SELECT 1 FROM syFldCaptions WHERE FldId = @fldId)
            BEGIN
                SET @FldCapId =
                (
                    SELECT MAX(FldCapId) FROM syFldCaptions
                ) + 1;
                INSERT INTO syFldCaptions
                (
                    FldCapId,
                    FldId,
                    LangId,
                    Caption,
                    FldDescrip
                )
                VALUES
                (   @FldCapId,         -- FldCapId - int
                    @fldId,            -- FldId - int
                    1,                 -- LangId - tinyint
                    'Remaining Hours', -- Caption - varchar(100)
                    NULL               -- FldDescrip - varchar(150)
                    );
                IF (@@ERROR > 0)
                BEGIN
                    SET @Error = @@ERROR;
                END;
            END;
            IF NOT EXISTS (SELECT 1 FROM syFieldCalculation WHERE FldId = @fldId)
                INSERT INTO dbo.syFieldCalculation
                (
                    FldId,
                    CalculationSql
                )
                VALUES
                (   @fldId, -- FldId - int
                    ' (SELECT IIF(Remainin > 0, Remainin, ValueIfNegative) FROM   (   SELECT ((select isnull(arPrgVersions.Hours,0) from arPrgVersions where arPrgVersions.PrgVerId = arStuEnrollments.PrgVerId ) - (Isnull( (  SELECT TOP 1 ( CASE WHEN (   LOWER( LTRIM( RTRIM( dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'' , arStuEnrollments.CampusId)))) = ''byclass''    AND LOWER( LTRIM( RTRIM(dbo.GetAppSettingValueByKeyName(''DisplayAttendanceUnitForProgressReportByClass'' , arStuEnrollments.CampusId)))) = ''hours''     AND AAUT1.UnitTypeDescrip = ''Present Absent'' )  THEN      (   SELECT SUM(ActualPresentDays_ConvertTo_Hours) + SAS_ForTerm.MakeupDays  FROM   syStudentAttendanceSummary       WHERE  StuEnrollId = arStuEnrollments.StuEnrollId ) ELSE CASE WHEN (   LOWER(LTRIM( RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'' , arStuEnrollments.CampusId)))) = ''byclass''     AND LOWER(LTRIM( RTRIM(dbo.GetAppSettingValueByKeyName( ''DisplayAttendanceUnitForProgressReportByClass'' ,arStuEnrollments.CampusId)))) = ''hours'' AND AAUT1.UnitTypeDescrip = ''Minutes'' )THEN CASE WHEN (   SELECT TOP 1 EndDate FROM   arTerm art  INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId     WHERE  PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN (SAS_NoTerm.AttendedDays / 60)+(SAS_NoTerm.MakeupDays / 60) ELSE (SAS_ForTerm.AttendedDays / 60)+(SAS_ForTerm.MakeupDays / 60) END   WHEN (   LOWER( LTRIM( RTRIM( dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'' ,arStuEnrollments.CampusId)))) = ''byclass''  AND LOWER( LTRIM(RTRIM( dbo.GetAppSettingValueByKeyName( ''DisplayAttendanceUnitForProgressReportByClass'' ,arStuEnrollments.CampusId)))) = ''hours'' AND AAUT1.UnitTypeDescrip = ''Clock Hours'' )     THEN CASE WHEN (   SELECT TOP 1 EndDate FROM   arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE  PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN  ( SAS_NoTerm.AttendedDays / 60 ) + (SAS_NoTerm.MakeupDays / 60) ELSE ( SAS_ForTerm.AttendedDays / 60 ) + ( SAS_ForTerm.MakeupDays / 60) END WHEN (   LOWER(LTRIM( RTRIM(dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'' ,arStuEnrollments.CampusId)))) = ''byclass''    AND LOWER( LTRIM(  RTRIM(dbo.GetAppSettingValueByKeyName( ''DisplayAttendanceUnitForProgressReportByClass'' , arStuEnrollments.CampusId)))) = ''presentabsent''  AND AAUT1.UnitTypeDescrip = ''Clock Hours'' )       THEN  CASE WHEN (   SELECT TOP 1 EndDate FROM   arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE  PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN       ( SAS_NoTerm.AttendedDays / 60 ) + (SAS_NoTerm.MakeupDays / 60)  ELSE ( SAS_ForTerm.AttendedDays / 60 ) + (SAS_ForTerm.MakeupDays / 60)  END WHEN (   LOWER(LTRIM( RTRIM( dbo.GetAppSettingValueByKeyName( ''TrackSapAttendance'' , arStuEnrollments.CampusId)))) = ''byclass''          AND LOWER( LTRIM( RTRIM(  dbo.GetAppSettingValueByKeyName(''DisplayAttendanceUnitForProgressReportByClass'' , arStuEnrollments.CampusId)))) = ''presentabsent''AND AAUT1.UnitTypeDescrip = ''Minutes'' )      THEN  CASE WHEN (   SELECT TOP 1 EndDate   FROM   arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId WHERE  PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN         (SAS_NoTerm.AttendedDays / 60) + (SAS_NoTerm.MakeupDays / 60) ELSE (SAS_ForTerm.AttendedDays / 60) + (SAS_ForTerm.MakeupDays / 60) END ELSE CASE WHEN (   SELECT TOP 1 EndDate FROM   arTerm art INNER JOIN arPrgVersions arp ON arp.PrgVerId = art.ProgramVersionId       WHERE  PrgVerId = arStuEnrollments.PrgVerId ) IS NULL THEN (SAS_NoTerm.AttendedDays) + ( SAS_NoTerm.MakeupDays ) ELSE (SAS_ForTerm.AttendedDays)+( SAS_ForTerm.MakeupDays ) END END END ) FROM   arAttUnitType AAUT1       INNER JOIN arPrgVersions arp ON arp.UnitTypeId = AAUT1.UnitTypeId  LEFT JOIN (   SELECT   StuEnrollId , MAX(ActualRunningScheduledDays) AS ScheduledDays ,  MAX(AdjustedPresentDays) AS AttendedDays ,          MAX(AdjustedAbsentDays) AS AbsentDays , MAX(ActualRunningMakeupDays) AS MakeupDays ,MAX(StudentAttendedDate) AS LDA  FROM     syStudentAttendanceSummary      GROUP BY StuEnrollId ) SAS_NoTerm ON arStuEnrollments.StuEnrollId = SAS_NoTerm.StuEnrollId  LEFT JOIN (   SELECT   StuEnrollId ,  MAX(ActualRunningScheduledDays) AS ScheduledDays ,         MAX(AdjustedPresentDays) AS AttendedDays , MAX(AdjustedAbsentDays) AS AbsentDays ,  MAX(ActualRunningMakeupDays) AS MakeupDays , MAX(StudentAttendedDate) AS LDA         FROM     syStudentAttendanceSummary  GROUP BY StuEnrollId ) SAS_ForTerm ON arStuEnrollments.StuEnrollId = SAS_ForTerm.StuEnrollId ) ,0) )) AS Remainin,
                 0 AS ValueIfNegative ) b) AS RemainingHours ' -- CalculationSql - varchar(8000)
                    );

        END;


    END;

END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @Error = 1;
END CATCH;

IF (@Error = 0)
BEGIN
    COMMIT TRANSACTION RemainingHrsAdhoc;
END;
ELSE
BEGIN
    ROLLBACK TRANSACTION RemainingHrsAdhoc;
END;
GO
--=================================================================================================
--END Add Remaining Hours to Adhoc Enrollment
--=================================================================================================
--=================================================================================================
--START Add Weekly scheduled Hours to Adhoc Enrollment
--=================================================================================================
DECLARE @Error AS INTEGER;
SET @Error = 0;

BEGIN TRANSACTION WeeklyScheduledHrsAdhoc;
BEGIN TRY
    DECLARE @fldId INT;
    DECLARE @tblId INT;
    DECLARE @TblFldsId INT;
    DECLARE @FldCapId INT;
    DECLARE @CatId INT;


    IF NOT EXISTS
    (
        SELECT 1
        FROM syFields
        WHERE FldName = 'WeeklyScheduledHours'
    )
    BEGIN
        SET @fldId =
        (
            SELECT MAX(FldId) FROM syFields
        ) + 1;
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,                 -- FldId - int
            'WeeklyScheduledHours', -- FldName - varchar(200)
            5,                      -- FldTypeId - int
            8,                      -- FldLen - int
            NULL,                   -- DDLId - int
            NULL,                   -- DerivedFld - bit
            NULL,                   -- SchlReq - bit
            NULL,                   -- LogChanges - bit
            NULL                    -- Mask - varchar(50)
            );
        IF (@@ERROR > 0)
        BEGIN
            SET @Error = @@ERROR;
        END;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        IF NOT EXISTS
        (
            SELECT 1
            FROM syTblFlds
            WHERE TblId = @tblId
                  AND FldId = @fldId
        )
        BEGIN
            SET @TblFldsId =
            (
                SELECT MAX(TblFldsId) FROM syTblFlds
            ) + 1;
            SET @CatId =
            (
                SELECT CategoryId
                FROM syFldCategories
                WHERE Descrip = 'Enrollment'
                      AND EntityId = 394
            );

            INSERT INTO syTblFlds
            (
                TblFldsId,
                TblId,
                FldId,
                CategoryId,
                FKColDescrip
            )
            VALUES
            (   @TblFldsId, -- TblFldsId - int
                @tblId,     -- TblId - int
                @fldId,     -- FldId - int
                @CatId,     -- CategoryId - int 
                NULL        -- FKColDescrip - varchar(50)
                );
            IF (@@ERROR > 0)
            BEGIN
                SET @Error = @@ERROR;
            END;
        END;
        IF NOT EXISTS (SELECT 1 FROM syFldCaptions WHERE FldId = @fldId)
        BEGIN
            SET @FldCapId =
            (
                SELECT MAX(FldCapId) FROM syFldCaptions
            ) + 1;
            INSERT INTO syFldCaptions
            (
                FldCapId,
                FldId,
                LangId,
                Caption,
                FldDescrip
            )
            VALUES
            (   @FldCapId,                -- FldCapId - int
                @fldId,                   -- FldId - int
                1,                        -- LangId - tinyint
                'Weekly Scheduled Hours', -- Caption - varchar(100)
                NULL                      -- FldDescrip - varchar(150)
                );
            IF (@@ERROR > 0)
            BEGIN
                SET @Error = @@ERROR;
            END;
        END;
        IF NOT EXISTS (SELECT 1 FROM syFieldCalculation WHERE FldId = @fldId)
            INSERT INTO dbo.syFieldCalculation
            (
                FldId,
                CalculationSql
            )
            VALUES
            (   @fldId, -- FldId - int
                ' (SELECT SUM(ISNULL(total,0)) FROM arStudentSchedules INNER JOIN arProgScheduleDetails ON arProgScheduleDetails.ScheduleId = arStudentSchedules.ScheduleId WHERE arStudentSchedules.Active IS NOT NULL AND StuEnrollId = arStuEnrollments.StuEnrollId) AS WeeklyScheduledHours');

    END;




END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @Error = 1;
END CATCH;

IF (@Error = 0)
BEGIN
    COMMIT TRANSACTION WeeklyScheduledHrsAdhoc;
END;
ELSE
BEGIN
    ROLLBACK TRANSACTION WeeklyScheduledHrsAdhoc;
END;
GO
--=================================================================================================
--END Add Weekly Scheduled Hours to Adhoc Enrollment
--=================================================================================================
--=================================================================================================
-- START Add LastPaymentDate field to Adhoc
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION LastPaymentDateAdhocReport;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Datetime'
    );
    IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'LastPaymentDate')
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,            -- FldId - int
            'LastPaymentDate', -- FldName - varchar(200)
            @fldTypeId,        -- FldTypeId - int
            10,                -- FldLen - int
            NULL,              -- DDLId - int
            NULL,              -- DerivedFld - bit
            NULL,              -- SchlReq - bit
            NULL,              -- LogChanges - bit
            NULL               -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Payments'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'arStuEnrollments'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,                                   -- FldCapId - int
            @fldId,                                      -- FldId - int
            1,                                           -- LangId - tinyint
            'Last Payment Date',                         -- Caption - varchar(100)
            'Last Payment Date on the Payment Plan Page' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            NULL         -- FKColDescrip - varchar(50)
            );
        INSERT INTO syFieldCalculation
        (
            FldId,
            CalculationSql
        )
        VALUES
        (   @fldId,                                                                                                                    -- FldId - int
            ' (SELECT PayPlanEndDate FROM faStudentPaymentPlans WHERE StuEnrollId = arStuEnrollments.StuEnrollId) AS LastPaymentDate ' -- CalculationSql - varchar(8000)
            );
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION LastPaymentDateAdhocReport;
    PRINT 'Failed to Add LastPaymentDate to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION LastPaymentDateAdhocReport;
    PRINT 'Added LastPaymentDate to Adhoc Report';
END;
GO
--=================================================================================================
-- END  Add LastPaymentDate field to Adhoc
--=================================================================================================
--=================================================================================================
-- START  to make the title IV SAP notices appear in Security page
--=================================================================================================
DECLARE @resId AS INT;
SET @resId =
(
    SELECT ResourceID
    FROM syResources
    WHERE Resource = 'Title IV SAP Notices'
          AND ResourceTypeID = 5
);
UPDATE syResources
SET ChildTypeId = 5
WHERE ResourceID = @resId;
--=================================================================================================
-- END  to make the title IV SAP notices appear in Security page
--=================================================================================================
--=================================================================================================
-- START adding contracted grad date in adhoc
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION ContractGradDateAdhocReport;
BEGIN TRY
    DECLARE @ContractedGradDateFldId INT;
    SET @ContractedGradDateFldId =
    (
        SELECT TOP 1 FldId FROM syFields WHERE FldName = 'ContractedGradDate'
    );
    DECLARE @tblFldId INT;
    SET @tblFldId =
    (
        SELECT TOP 1
               TblFldsId
        FROM syTblFlds
        WHERE FldId = @ContractedGradDateFldId
    );
    DECLARE @categoryId INT;
    SET @categoryId =
    (
        SELECT TOP 1
               CategoryId
        FROM syFldCategories
        WHERE Descrip = 'Enrollment'
              AND EntityId = 394
    );
    IF NOT EXISTS
    (
        SELECT 1
        FROM syTblFlds
        WHERE CategoryId = @categoryId
              AND TblFldsId = @tblFldId
    )
    BEGIN
        UPDATE syTblFlds
        SET CategoryId = @categoryId
        WHERE TblFldsId = @tblFldId;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION ContractGradDateAdhocReport;
    PRINT 'Failed to Add ContractedGradDate to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION ContractGradDateAdhocReport;
    PRINT 'Added ContractedGradDate to Adhoc Report';
END;
GO
--=================================================================================================
-- END adding contracted grad date in adhoc
--=================================================================================================
--=================================================================================================
-- START TECH: Adding student groups to enrollment in the adhoc report
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION StudentGrpAdhoc;
BEGIN TRY
    DECLARE @fldId INT;
    SET @fldId =
    (
        SELECT MAX(FldId) FROM syFields
    ) + 1;
    DECLARE @fldTypeId INT;
    SET @fldTypeId =
    (
        SELECT FldTypeId FROM syFieldTypes WHERE FldType = 'Uniqueidentifier'
    );

    DECLARE @ddlId INT;
    SET @ddlId =
    (
        SELECT DDLId
        FROM syDDLS
        WHERE DDLName = 'Student Groups'
              AND ResourceId = 339
    );
    DECLARE @adLeadGroupTblId INT;
    SET @adLeadGroupTblId =
    (
        SELECT TblId FROM syTables WHERE TblName = 'adLeadGroups'
    );
    UPDATE syDDLS
    SET TblId = @adLeadGroupTblId
    WHERE DDLId = @ddlId;
    IF NOT EXISTS
    (
        SELECT 1
        FROM syFields
        WHERE FldName = 'LeadGrpId'
              AND DDLId = @ddlId
    )
    BEGIN
        INSERT INTO syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @fldId,      -- FldId - int
            'LeadGrpId', -- FldName - varchar(200) --LeadGrpId
            @fldTypeId,  -- FldTypeId - int
            16,          -- FldLen - int
            @ddlId,      -- DDLId - int
            0,           -- DerivedFld - bit
            0,           -- SchlReq - bit
            1,           -- LogChanges - bit
            NULL         -- Mask - varchar(50)
            );
        DECLARE @categoryId INT;
        SET @categoryId =
        (
            SELECT CategoryId
            FROM syFldCategories
            WHERE Descrip = 'Enrollment'
                  AND EntityId = 394
        );
        DECLARE @TblFldsId INT;
        SET @TblFldsId =
        (
            SELECT MAX(TblFldsId) FROM syTblFlds
        ) + 1;
        DECLARE @tblId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'adLeadByLeadGroups'
        );
        DECLARE @FldCapId INT;
        SET @FldCapId =
        (
            SELECT MAX(FldCapId) FROM syFldCaptions
        ) + 1;
        INSERT INTO syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,                         -- FldCapId - int
            @fldId,                            -- FldId - int
            1,                                 -- LangId - tinyint
            'Student Groups',                  -- Caption - varchar(100)
            'Student Groups for Adhoc reports' -- FldDescrip - varchar(150)
            );
        INSERT INTO syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId,  -- TblFldsId - int
            @tblId,      -- TblId - int
            @fldId,      -- FldId - int
            @categoryId, -- CategoryId - int
            'Descrip'    -- FKColDescrip - varchar(50)
            );
    --IF NOT EXISTS (
    --              SELECT 1
    --              FROM   syRptAdhocRelations
    --              WHERE  FkTable = 'adLeadByLeadGroups'
    --                     AND PkTable = 'arStudent'
    --              )
    --    BEGIN
    --        INSERT INTO syRptAdhocRelations (
    --                                        FkTable
    --                                       ,FkColumn
    --                                       ,PkTable
    --                                       ,PkColumn
    --                                        )
    --        VALUES ( N'adLeadByLeadGroups' -- FkTable - nvarchar(100)
    --                ,N'StudentId'          -- FkColumn - nvarchar(100)
    --                ,N'arStudent'          -- PkTable - nvarchar(100)
    --                ,N'StudentId'          -- PkColumn - nvarchar(100)
    --            );
    --    END;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION StudentGrpAdhoc;
    PRINT 'Failed to Add StudentGrp to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION StudentGrpAdhoc;
    PRINT 'Added StudentGrp to Adhoc Report';
END;
GO
--=================================================================================================
-- END TECH: Adding student groups to enrollment in the adhoc report
--=================================================================================================
--=================================================================================================
-- Start TECH: Adding Title IV status to enrollment in the adhoc report
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION TStatusAdhoc;
BEGIN TRY
    DECLARE @t4statusTblId INT;
    DECLARE @t4StaustuPkId INT;
    DECLARE @resultsTblId INT;
    DECLARE @resultsPkId INT;
    DECLARE @t4StatusFld INT;
    DECLARE @T4DdlId INT;
    DECLARE @T4CapId INT;
    DECLARE @categoryId INT;
    DECLARE @TblFldsId INT;
    DECLARE @idFldId INT;
    DECLARE @descripFldId INT;
    IF NOT EXISTS (SELECT 1 FROM syTables WHERE TblName = 'syTitleIVSapStatus')
    BEGIN

        SET @t4statusTblId =
        (
            SELECT MAX(TblId) FROM syTables
        ) + 1;
        SET @t4StaustuPkId =
        (
            SELECT MAX(TblPK) FROM syTables
        ) + 1;
        INSERT INTO syTables
        (
            TblId,
            TblName,
            TblDescrip,
            TblPK
        )
        VALUES
        (   @t4statusTblId,                       -- TblId - int
            'syTitleIVSapStatus',                 -- TblName - varchar(50)
            'system table for title IV Statuses', -- TblDescrip - varchar(100)
            @t4StaustuPkId                        -- TblPK - int
            );

        IF NOT EXISTS (SELECT 1 FROM syFields WHERE FldName = 'TitleIVStatusId')
        BEGIN
            SET @t4StatusFld =
            (
                SELECT MAX(FldId) FROM syFields
            ) + 1;
            INSERT INTO syFields
            (
                FldId,
                FldName,
                FldTypeId,
                FldLen,
                DDLId,
                DerivedFld,
                SchlReq,
                LogChanges,
                Mask
            )
            VALUES
            (   @t4StatusFld,      -- FldId - int
                'TitleIVStatusId', -- FldName - varchar(200)
                72,                -- FldTypeId - int
                16,                -- FldLen - int
                NULL,              -- DDLId - int
                0,                 -- DerivedFld - bit
                0,                 -- SchlReq - bit
                1,                 -- LogChanges - bit
                NULL               -- Mask - varchar(50)
                );
        END;
        IF NOT EXISTS (SELECT 1 FROM syDDLS WHERE DDLName = 'TitleIVSapStatus')
        BEGIN
            SET @idFldId =
            (
                SELECT TOP 1 FldId FROM syFields WHERE FldName = 'Id'
            );
            SET @descripFldId =
            (
                SELECT TOP 1 FldId FROM syFields WHERE FldName = 'Description'
            );
            SET @T4DdlId =
            (
                SELECT MAX(DDLId) FROM syDDLS
            ) + 1;
            INSERT INTO syDDLS
            (
                DDLId,
                DDLName,
                TblId,
                DispFldId,
                ValFldId,
                ResourceId,
                CulDependent
            )
            VALUES
            (   @T4DdlId,           -- DDLId - int
                'TitleIVSapStatus', -- DDLName - varchar(100)
                @t4statusTblId,     -- TblId - int
                @descripFldId,      -- DispFldId - int
                @idFldId,           -- ValFldId - int
                NULL,               -- ResourceId - int
                NULL                -- CulDependent - bit
                );
            UPDATE syFields
            SET DDLId = @T4DdlId
            WHERE FldId = @t4StatusFld;
        END;
        IF NOT EXISTS
        (
            SELECT 1
            FROM syFldCaptions
            WHERE Caption = 'Title IV Sap Status'
        )
        BEGIN
            SET @T4CapId =
            (
                SELECT MAX(FldCapId) FROM syFldCaptions
            ) + 1;
            INSERT INTO syFldCaptions
            (
                FldCapId,
                FldId,
                LangId,
                Caption,
                FldDescrip
            )
            VALUES
            (   @T4CapId,                               -- FldCapId - int
                @t4StatusFld,                           -- FldId - int
                1,                                      -- LangId - tinyint
                'Title IV Sap Status',                  -- Caption - varchar(100)
                'Title IV Sap Status for adhoc reports' -- FldDescrip - varchar(150)
                );
        END;
    END;
    SET @categoryId =
    (
        SELECT CategoryId
        FROM syFldCategories
        WHERE Descrip = 'Enrollment'
              AND EntityId = 394
    );
    IF NOT EXISTS (SELECT 1 FROM syTables WHERE TblName = 'arFASAPChkResults')
    BEGIN

        SET @resultsTblId =
        (
            SELECT MAX(TblId) FROM syTables
        ) + 1;
        SET @resultsPkId =
        (
            SELECT MAX(TblPK) FROM syTables
        ) + 1;
        INSERT INTO syTables
        (
            TblId,
            TblName,
            TblDescrip,
            TblPK
        )
        VALUES
        (   @resultsTblId,                                       -- TblId - int
            'arFASAPChkResults',                                 -- TblName - varchar(50)
            'table to store title IV sap results per increment', -- TblDescrip - varchar(100)
            @resultsPkId                                         -- TblPK - int
            );
        IF NOT EXISTS
        (
            SELECT 1
            FROM syTblFlds
            WHERE FldId = @t4StatusFld
                  AND TblId = @resultsTblId
        )
        BEGIN
            SET @TblFldsId =
            (
                SELECT MAX(TblFldsId) FROM syTblFlds
            ) + 1;
            INSERT INTO syTblFlds
            (
                TblFldsId,
                TblId,
                FldId,
                CategoryId,
                FKColDescrip
            )
            VALUES
            (   @TblFldsId,    -- TblFldsId - int
                @resultsTblId, -- TblId - int
                @t4StatusFld,  -- FldId - int
                @categoryId,   -- CategoryId - int
                'Description'  -- FKColDescrip - varchar(50)
                );
        END;
        IF NOT EXISTS
        (
            SELECT 1
            FROM syTblDependencies
            WHERE table1 = 'arStudent'
                  AND table2 = 'arFASAPChkResults'
                  AND depTable = 'arStuEnrollments'
        )
        BEGIN
            INSERT INTO syTblDependencies
            (
                table1,
                table2,
                depTable,
                TableDependenciesId
            )
            VALUES
            (   'arStudent',         -- table1 - varchar(100)
                'arFASAPChkResults', -- table2 - varchar(100)
                'arStuEnrollments',  -- depTable - varchar(100)
                NEWID()              -- TableDependenciesId - uniqueidentifier
                );
        END;
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION TStatusAdhoc;
    PRINT 'Failed to Add Title IV status to Adhoc Report';

END;
ELSE
BEGIN
    COMMIT TRANSACTION TStatusAdhoc;
    PRINT 'Added Title IV status to Adhoc Report';
END;
GO
--=================================================================================================
-- END TECH: Adding Title IV status to enrollment in the adhoc report
--=================================================================================================
--=================================================================================================
GO

--=================================================================================================
-- START AD-11051 Graduate Student Widget
--=================================================================================================
BEGIN
    DECLARE @Error AS INTEGER;

    SET @Error = 0;

    BEGIN TRANSACTION StudentsReadyForGraduation;
    BEGIN TRY

        IF NOT EXISTS
        (
            SELECT *
            FROM dbo.syWidgets
            WHERE Code = 'StudentsReadyForGraduation'
        )
        BEGIN
            DECLARE @ActiveStatusId UNIQUEIDENTIFIER;
            SET @ActiveStatusId =
            (
                SELECT StatusId FROM syStatuses WHERE StatusCode = 'A'
            );

            DECLARE @WidgetId UNIQUEIDENTIFIER;
            SET @WidgetId = NEWID();

            INSERT INTO dbo.syWidgets
            (
                WidgetId,
                Code,
                Description,
                Rows,
                Columns,
                StatusId
            )
            VALUES
            (   @WidgetId,                       -- WidgetId - uniqueidentifier
                'StudentsReadyForGraduation',    -- Code - varchar(50)
                'Students Ready For Graduation', -- Description - varchar(200)
                0,                               -- Rows - int
                1,                               -- Columns - int
                @ActiveStatusId                  -- StatusId - uniqueidentifier
                );
            SELECT *
            FROM syWidgetResourceRoles;

            INSERT INTO dbo.syWidgetResourceRoles
            (
                WidgetResourceRoleId,
                WidgetId,
                ResourceId,
                SysRoleId,
                StatusId
            )
            SELECT NEWID(),
                   @WidgetId,
                   264,
                   SysRoleId,
                   @ActiveStatusId
            FROM dbo.sySysRoles
            WHERE SysRoleId IN ( 1, 9, 10, 13, 18 );

        END;


        IF (@@ERROR > 0)
        BEGIN
            SET @Error = @@ERROR;
        END;

    END TRY
    BEGIN CATCH
        DECLARE @msg NVARCHAR(MAX);
        DECLARE @severity INT;
        DECLARE @state INT;
        SELECT @msg = ERROR_MESSAGE(),
               @severity = ERROR_SEVERITY(),
               @state = ERROR_STATE();
        RAISERROR(@msg, @severity, @state);
        SET @Error = 1;
    END CATCH;

    IF (@Error = 0)
    BEGIN
        COMMIT TRANSACTION StudentsReadyForGraduation;

        PRINT 'Transaction was committed ';
    END;
    ELSE
    BEGIN
        ROLLBACK TRANSACTION StudentsReadyForGraduation;
        PRINT 'Transaction was rolled back';
    END;
END;
GO
--=================================================================================================
-- END AD-11051 Graduate Student Widget
--=================================================================================================
--========================================================================================================================
-- START --  AD-11051 Graduate Student Widget
--========================================================================================================================

DECLARE @KeyName VARCHAR(50) = 'GraduationWidget';
DECLARE @KeyDescription VARCHAR(300)
    = 'Values can be set to True or False. If value is set to True, it indicates the Graduation widget will be enabled.';

IF NOT EXISTS (SELECT * FROM syConfigAppSettings WHERE KeyName = @KeyName)
BEGIN TRY
    BEGIN
        BEGIN TRANSACTION GraduationWidget;

        DECLARE @MaxId INT;

        INSERT INTO dbo.syConfigAppSettings
        (
            KeyName,
            Description,
            ModUser,
            ModDate,
            CampusSpecific,
            ExtraConfirmation
        )
        VALUES
        (   @KeyName,        -- KeyName - varchar(200)
            @KeyDescription, -- Description - varchar(1000)
            'Support',       -- ModUser - varchar(50)
            GETDATE(),       -- ModDate - datetime
            1,               -- CampusSpecific - bit
            0                -- ExtraConfirmation - bit
            );

        SET @MaxId =
        (
            SELECT TOP (1)
                   SettingId
            FROM syConfigAppSettings
            WHERE KeyName = @KeyName
            ORDER BY KeyName
        );

        SET @MaxId =
        (
            SELECT TOP (1)
                   SettingId
            FROM syConfigAppSettings
            WHERE KeyName = @KeyName
            ORDER BY KeyName
        );

        INSERT INTO dbo.syConfigAppSetValues
        (
            ValueId,
            SettingId,
            CampusId,
            Value,
            ModUser,
            ModDate,
            Active
        )
        VALUES
        (   NEWID(),   -- ValueId - uniqueidentifier
            @MaxId,    -- SettingId - int
            NULL,      -- CampusId - uniqueidentifier
            'False',   -- Value - varchar(1000)
            'Support', -- ModUser - varchar(50)
            GETDATE(), -- ModDate - datetime
            1          -- Active - bit
            );

        INSERT INTO dbo.syConfigAppSet_Lookup
        (
            LookUpId,
            SettingId,
            ValueOptions,
            ModUser,
            ModDate
        )
        VALUES
        (   NEWID(),   -- LookUpId - uniqueidentifier
            @MaxId,    -- SettingId - int
            'False',   -- ValueOptions - varchar(50)
            'Support', -- ModUser - varchar(50)
            GETDATE()  -- ModDate - datetime
            );

        INSERT INTO dbo.syConfigAppSet_Lookup
        (
            LookUpId,
            SettingId,
            ValueOptions,
            ModUser,
            ModDate
        )
        VALUES
        (   NEWID(),   -- LookUpId - uniqueidentifier
            @MaxId,    -- SettingId - int
            'True',    -- ValueOptions - varchar(50)
            'Support', -- ModUser - varchar(50)
            GETDATE()  -- ModDate - datetime
            );

    END;
END TRY
BEGIN CATCH
    SELECT ERROR_NUMBER() AS ErrorNumber;
    SELECT ERROR_SEVERITY() AS ErrorSeverity;
    SELECT ERROR_STATE() AS ErrorState;
    SELECT ERROR_PROCEDURE() AS ErrorProcedure;
    SELECT ERROR_LINE() AS ErrorLine;
    SELECT ERROR_MESSAGE() AS ErrorMessage;

    IF @@TRANCOUNT > 0
    BEGIN
        ROLLBACK TRANSACTION GraduationWidget;
        PRINT 'Exception Ocurred!';
    END;
END CATCH;

IF @@TRANCOUNT > 0
BEGIN
    COMMIT TRANSACTION GraduationWidget;
END;
GO

--===============================================================================================================================
-- END --  AD-11051 Graduate Student Widget
--===============================================================================================================================
--=================================================================================================
--=================================================================================================
-- START  AD-10968 REG: User with the role Termination student doesn't have an access to the Undo Termination
-- UPDATE syMenuItems
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION UndoTerminationMenuItems;
BEGIN TRY

    DECLARE @parentId AS INTEGER;
    SET @parentId =
    (
        SELECT ParentId FROM syMenuItems WHERE MenuName = 'Terminate Student'
    );

    IF NOT EXISTS
    (
        SELECT TOP 1
               ParentId
        FROM syMenuItems
        WHERE MenuName = 'Undo Student Termination'
              AND ParentId IS NOT NULL
    )
    BEGIN
        UPDATE dbo.syMenuItems
        SET ParentId = @parentId
        WHERE MenuName = 'Undo Student Termination';
    END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION UndoTerminationMenuItems;
    PRINT 'Failed transcation add Undo Termination MenuItems.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION UndoTerminationMenuItems;
    PRINT 'successful transaction add Undo Termination MenuItems.';
END;
GO
--=================================================================================================
-- End AD-10968 REG: User with the role Termination student doesn't have an access to the Undo Termination
--=================================================================================================


--=================================================================================================
------------START AD-11309: Add Calendar days options to consecutive absence reports
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION ConsecutiveAbsence;
BEGIN TRY
    DECLARE @ByClassSetName VARCHAR(50) = 'ConsecutiveAbsenceByClassOptionSet';
    DECLARE @ByProgramSetName VARCHAR(50) = 'ConsecutiveAbsenceByProgramOptionSet';
    DECLARE @ByClassSectionName VARCHAR(50) = 'ConsecutiveAbsenceByClassOptionSection';
    DECLARE @ByProgramSectionName VARCHAR(50) = 'ConsecutiveAbsenceByClassOptionSection';


    DECLARE @ConsecutiveAbsenceByProgramReportId UNIQUEIDENTIFIER =
            (
                SELECT TOP 1
                       ReportId
                FROM syReports
                WHERE ReportName = 'Consecutive Absences by Program'
            );
    DECLARE @ConsecutiveAbsenceByClassReportId UNIQUEIDENTIFIER =
            (
                SELECT TOP 1
                       ReportId
                FROM syReports
                WHERE ReportName = 'ConsecutiveAbsencesByClass'
            );
    IF NOT EXISTS
    (
        SELECT *
        FROM dbo.syReportTabs
        WHERE ReportId IN ( @ConsecutiveAbsenceByProgramReportId )
              AND PageViewId = 'RadRptPage_Filtering'
    )
    BEGIN
        UPDATE dbo.syReportTabs
        SET TabName = 'Filtering',
            PageViewId = 'RadRptPage_Filtering',
            ControllerClass = 'ParamSetPanelBarFilterControl.ascx',
            UserControlName = 'ParamSetPanelBarFilterControl'
        WHERE ReportId IN ( @ConsecutiveAbsenceByProgramReportId );
    END;

    IF NOT EXISTS
    (
        SELECT *
        FROM dbo.syReportTabs
        WHERE ReportId IN ( @ConsecutiveAbsenceByClassReportId )
              AND PageViewId = 'RadRptPage_Filtering'
    )
    BEGIN
        UPDATE dbo.syReportTabs
        SET TabName = 'Filtering',
            PageViewId = 'RadRptPage_Filtering',
            ControllerClass = 'ParamSetPanelBarFilterControl.ascx',
            UserControlName = 'ParamSetPanelBarFilterControl'
        WHERE ReportId IN ( @ConsecutiveAbsenceByClassReportId );
    END;

    UPDATE dbo.ParamSet
    SET SetType = 'Custom'
    WHERE SetId IN
          (
              SELECT SetId
              FROM syReportTabs
              WHERE ReportId IN ( @ConsecutiveAbsenceByClassReportId, @ConsecutiveAbsenceByProgramReportId )
          );


    UPDATE dbo.syReports
    SET ReportTabLayout = 0
    WHERE ReportId IN ( @ConsecutiveAbsenceByClassReportId, @ConsecutiveAbsenceByProgramReportId );



    ---- INSERT PARAM SETS-----------
    IF NOT EXISTS
    (
        SELECT TOP 1
               SetId
        FROM ParamSet
        WHERE SetName = @ByClassSetName
    )
    BEGIN
        INSERT INTO dbo.ParamSet
        (
            SetName,
            SetDisplayName,
            SetDescription,
            SetType
        )
        VALUES
        (   @ByClassSetName,                             -- SetName - nvarchar(50)
            N'Consecutive Absence By Class Options',     -- SetDisplayName - nvarchar(50)
            N'Options for Consecutive Absence By Class', -- SetDescription - nvarchar(1000)
            N'Custom'                                    -- SetType - nvarchar(50)
            );
    END;


    IF NOT EXISTS
    (
        SELECT TOP 1
               SetId
        FROM ParamSet
        WHERE SetName = @ByProgramSetName
    )
    BEGIN
        INSERT INTO dbo.ParamSet
        (
            SetName,
            SetDisplayName,
            SetDescription,
            SetType
        )
        VALUES
        (   @ByProgramSetName,                             -- SetName - nvarchar(50)
            N'Consecutive Absence By Program Options',     -- SetDisplayName - nvarchar(50)
            N'Options for Consecutive Absence By Program', -- SetDescription - nvarchar(1000)
            N'Custom'                                      -- SetType - nvarchar(50)
            );
    END;


    DECLARE @ByProgramSetId INT =
            (
                SELECT TOP 1 SetId FROM dbo.ParamSet WHERE SetName = @ByProgramSetName
            );
    DECLARE @ByClassSetId INT =
            (
                SELECT TOP 1 SetId FROM dbo.ParamSet WHERE SetName = @ByClassSetName
            );



    ----- INSERT INTO REPORT PARAMS
    IF NOT EXISTS
    (
        SELECT *
        FROM syReportTabs
        WHERE ReportId = @ConsecutiveAbsenceByProgramReportId
              AND SetId = @ByProgramSetId
    )
    BEGIN
        INSERT INTO dbo.syReportTabs
        (
            ReportId,
            PageViewId,
            UserControlName,
            SetId,
            ControllerClass,
            DisplayName,
            ParameterSetLookUp,
            TabName
        )
        VALUES
        (   @ConsecutiveAbsenceByProgramReportId,            -- ReportId - uniqueidentifier
            N'RadRptPage_Options',                           -- PageViewId - nvarchar(200)
            N'ParamPanelBarSetCustomOptions',                -- UserControlName - nvarchar(200)
            @ByProgramSetId,                                 -- SetId - bigint
            N'ParamSetPanelBarControl.ascx',                 -- ControllerClass - nvarchar(200)
            N'Options for Consecutive Abscenses by Program', -- DisplayName - nvarchar(200)
            N'ConsecutiveAbsencesByProgramOptionSet',        -- ParameterSetLookUp - nvarchar(200)
            N'Options'                                       -- TabName - nvarchar(200)
            );
    END;



    IF NOT EXISTS
    (
        SELECT *
        FROM syReportTabs
        WHERE ReportId = @ConsecutiveAbsenceByClassReportId
              AND SetId = @ByClassSetId
    )
    BEGIN
        INSERT INTO dbo.syReportTabs
        (
            ReportId,
            PageViewId,
            UserControlName,
            SetId,
            ControllerClass,
            DisplayName,
            ParameterSetLookUp,
            TabName
        )
        VALUES
        (   @ConsecutiveAbsenceByClassReportId,              -- ReportId - uniqueidentifier
            N'RadRptPage_Options',                           -- PageViewId - nvarchar(200)
            N'ParamPanelBarSetCustomOptions',                -- UserControlName - nvarchar(200)
            @ByClassSetId,                                   -- SetId - bigint
            N'ParamSetPanelBarControl.ascx',                 -- ControllerClass - nvarchar(200)
            N'Options for Consecutive Abscenses by Program', -- DisplayName - nvarchar(200)
            N'ConsecutiveAbsencesByClassOptionSet',          -- ParameterSetLookUp - nvarchar(200)
            N'Options'                                       -- TabName - nvarchar(200)
            );
    END;



    --- INSERT INTO SECTIONS
    IF NOT EXISTS
    (
        SELECT *
        FROM dbo.ParamSection
        WHERE SectionName = @ByProgramSectionName
              AND SetId = @ByProgramSetId
    )
    BEGIN
        INSERT INTO dbo.ParamSection
        (
            SectionName,
            SectionCaption,
            SetId,
            SectionSeq,
            SectionDescription,
            SectionType
        )
        VALUES
        (   @ByProgramSectionName,                                       -- SectionName - nvarchar(50)
            N'Param Section for Consecutive Absence Report By Program',  -- SectionCaption - nvarchar(100)
            @ByProgramSetId,                                             -- SetId - bigint
            1,                                                           -- SectionSeq - int
            N'Select Options for Consecutive Absence Report By Program', -- SectionDescription - nvarchar(500)
            0                                                            -- SectionType - int
            );
    END;



    IF NOT EXISTS
    (
        SELECT *
        FROM dbo.ParamSection
        WHERE SectionName = @ByClassSectionName
              AND SetId = @ByClassSetId
    )
    BEGIN
        INSERT INTO dbo.ParamSection
        (
            SectionName,
            SectionCaption,
            SetId,
            SectionSeq,
            SectionDescription,
            SectionType
        )
        VALUES
        (   @ByClassSectionName,                                       -- SectionName - nvarchar(50)
            N'Param Section for Consecutive Absence Report By Class',  -- SectionCaption - nvarchar(100)
            @ByClassSetId,                                             -- SetId - bigint
            1,                                                         -- SectionSeq - int
            N'Select Options for Consecutive Absence Report By Class', -- SectionDescription - nvarchar(500)
            0                                                          -- SectionType - int
            );
    END;


    IF NOT EXISTS
    (
        SELECT TOP 1
               ItemId
        FROM dbo.ParamItem
        WHERE Caption = 'ConsecutiveAbsenceOptions'
    )
    BEGIN
        INSERT INTO dbo.ParamItem
        (
            Caption,
            ControllerClass,
            valueprop,
            ReturnValueName,
            IsItemOverriden
        )
        VALUES
        (   N'ConsecutiveAbsenceOptions',                   -- Caption - nvarchar(50)
            N'ConsecutiveAbsenceReportOptionsControl.ascx', -- ControllerClass - nvarchar(50)
            0,                                              -- valueprop - int
            N'ConsecutiveAbsencesOption',                   -- ReturnValueName - nvarchar(50)
            NULL                                            -- IsItemOverriden - bit
            );
    END;

    DECLARE @OptionId INT =
            (
                SELECT TOP 1
                       ItemId
                FROM dbo.ParamItem
                WHERE Caption = 'ConsecutiveAbsenceOptions'
            );

    DECLARE @ByProgramSectionId INT =
            (
                SELECT TOP 1
                       SectionId
                FROM dbo.ParamSection
                WHERE SectionName = @ByProgramSectionName
                      AND SetId = @ByProgramSetId
            );


    DECLARE @ByClassSectionId INT =
            (
                SELECT TOP 1
                       SectionId
                FROM dbo.ParamSection
                WHERE SectionName = @ByClassSectionName
                      AND SetId = @ByClassSetId
            );


    IF NOT EXISTS
    (
        SELECT *
        FROM dbo.ParamDetail
        WHERE SectionId = @ByProgramSectionId
              AND ItemId = @OptionId
    )
    BEGIN
        INSERT INTO dbo.ParamDetail
        (
            SectionId,
            ItemId,
            CaptionOverride,
            ItemSeq
        )
        VALUES
        (   @ByProgramSectionId, -- SectionId - bigint
            @OptionId,           -- ItemId - bigint
            N'Options',          -- CaptionOverride - nvarchar(50)
            1                    -- ItemSeq - int
            );
    END;



    IF NOT EXISTS
    (
        SELECT *
        FROM dbo.ParamDetail
        WHERE SectionId = @ByClassSectionId
              AND ItemId = @OptionId
    )
    BEGIN
        INSERT INTO dbo.ParamDetail
        (
            SectionId,
            ItemId,
            CaptionOverride,
            ItemSeq
        )
        VALUES
        (   @ByClassSectionId, -- SectionId - bigint
            @OptionId,         -- ItemId - bigint
            N'Options',        -- CaptionOverride - nvarchar(50)
            1                  -- ItemSeq - int
            );
    END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION ConsecutiveAbsence;
    PRINT 'Failed to add consecutive absence';

END;
ELSE
BEGIN
    COMMIT TRANSACTION ConsecutiveAbsence;
    PRINT 'Successful added consecutive absence.';
END;
GO

--=================================================================================================
------------End AD-11309: Add Calendar days options to consecutive absence reports
--========================================================================================
--=================================================================================================
-- Start AD-11295 - AD-11051- Grad Widget not shown for Fin Aid Director
--=================================================================================================
BEGIN
    DECLARE @Error AS INTEGER;

    SET @Error = 0;

    BEGIN TRANSACTION FixDirectorsPermission;
    BEGIN TRY


        /*
			Giving Permissions to Director of FInancial Aid the Modify Permission on the AR Module
		*/
        UPDATE dbo.sySysRoles
        SET Permission = '{"modules": [ {"name": "AR","level": "modify"}, {"name": "AD","level": "none"},  {"name": "FC","level": "none"}, {"name": "FA","level": "delete"},  {"name": "HR","level": "none"}, {"name": "PL","level": "none"},  {"name": "SA","level": "none"}, {"name": "SY","level": "read"} ]}'
        WHERE SysRoleId = 9;
        /*
			Giving Permissions to Director of Business Office the Modify Permission on the AR Module
		*/
        UPDATE dbo.sySysRoles
        SET Permission = '{"modules": [ {"name": "AR","level": "modify"}, {"name": "AD","level": "none"},  {"name": "FC","level": "none"}, {"name": "FA","level": "none"},  {"name": "HR","level": "none"}, {"name": "PL","level": "none"},  {"name": "SA","level": "delete"}, {"name": "SY","level": "read"} ]}'
        WHERE SysRoleId = 10;



        IF (@@ERROR > 0)
        BEGIN
            SET @Error = @@ERROR;
        END;

    END TRY
    BEGIN CATCH
        DECLARE @msg NVARCHAR(MAX);
        DECLARE @severity INT;
        DECLARE @state INT;
        SELECT @msg = ERROR_MESSAGE(),
               @severity = ERROR_SEVERITY(),
               @state = ERROR_STATE();
        RAISERROR(@msg, @severity, @state);
        SET @Error = 1;
    END CATCH;

    IF (@Error = 0)
    BEGIN
        COMMIT TRANSACTION FixDirectorsPermission;

        PRINT 'Transaction was committed ';
    END;
    ELSE
    BEGIN
        ROLLBACK TRANSACTION FixDirectorsPermission;
        PRINT 'Transaction was rolled back';
    END;
END;

GO
--=================================================================================================
-- END AD-11295 - AD-11051- Grad Widget not shown for Fin Aid Director
--=================================================================================================
--=================================================================================================
-- Start AD-11369  Error when running some Adhoc Reports --Update MonthlyPayments to SUM
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION MonthlyPayments;
BEGIN TRY
    DECLARE @FldIdMonthlyPayments INTEGER;
    SET @FldIdMonthlyPayments =
    (
        SELECT FldId FROM syFields WHERE FldName LIKE 'MonthlyPayments'
    );

    IF NOT EXISTS
    (
        SELECT 1
        FROM syFieldCalculation
        WHERE FldId = @FldIdMonthlyPayments
              AND CalculationSql LIKE '%SUM%'
    )
    BEGIN


        UPDATE syFieldCalculation
        SET CalculationSql = ' (SELECT FORMAT(CAST(SUM(TotalAmountDue/Disbursements) AS DECIMAL(19,2)),''C'',''en-us'') FROM faStudentPaymentPlans WHERE StuEnrollId = arStuEnrollments.StuEnrollId) AS MonthlyPayments '
        WHERE FldId = @FldIdMonthlyPayments;


    END;


END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION MonthlyPayments;
    PRINT 'Failed to add SUM to MonthlyPayments for adhoc.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION MonthlyPayments;
    PRINT 'Successful added  SUM to MonthlyPayments for adhoc.';
END;
GO
--=================================================================================================
-- End AD-11369  Error when running some Adhoc Reports --Update MonthlyPayments to SUM
--=================================================================================================
--=================================================================================================
-- Start AD-11369  Error when running some Adhoc Reports --Update LastPaymentDate to MAX
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION LastPaymentDate;
BEGIN TRY

    DECLARE @FldIdLastPaymentDate INTEGER;
    SET @FldIdLastPaymentDate =
    (
        SELECT FldId FROM syFields WHERE FldName LIKE 'LastPaymentDate'
    );
    IF NOT EXISTS
    (
        SELECT 1
        FROM syFieldCalculation
        WHERE FldId = @FldIdLastPaymentDate
              AND CalculationSql LIKE '%MAX%'
    )
    BEGIN



        UPDATE syFieldCalculation
        SET CalculationSql = ' (SELECT MAX(PayPlanEndDate) FROM faStudentPaymentPlans WHERE StuEnrollId = arStuEnrollments.StuEnrollId) AS LastPaymentDate '
        WHERE FldId = @FldIdLastPaymentDate;


    END;


END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION LastPaymentDate;
    PRINT 'Failed to add MAX to LastPaymentDate for adhoc.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION LastPaymentDate;
    PRINT 'Successful added  MAX to LastPaymentDate for adhoc.';
END;
GO
--=================================================================================================
-- End AD-11369  Error when running some Adhoc Reports --Update LastPaymentDate to MAX
--=================================================================================================
--========================================================================================================================
--START
-- AD-5289 Create Grad Date Calculator that can be used during enrollment.
--START
--========================================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION AddGradDateCalculatorMenu;
BEGIN TRY
    DECLARE @newResourceId SMALLINT;
    SET @newResourceId = 870; --based on excell

    IF NOT EXISTS
    (
        SELECT TOP 1
               ResourceID
        FROM dbo.syResources
        WHERE ResourceID = @newResourceId
    )
    BEGIN

        INSERT INTO dbo.syResources
        (
            ResourceID,
            Resource,
            ResourceTypeID,
            ResourceURL,
            SummListId,
            ChildTypeId,
            ModDate,
            ModUser,
            AllowSchlReqFlds,
            MRUTypeId,
            UsedIn,
            TblFldsId,
            DisplayName
        )
        VALUES
        (   @newResourceId,                  -- ResourceID - smallint
            'Grad Date Calculator',          -- Resource - varchar(200)
            3,                               -- ResourceTypeID - tinyint
            N'~/AR/GradDateCalculator.aspx', -- ResourceURL - varchar(100)
            NULL,                            -- SummListId - smallint
            NULL,                            -- ChildTypeId - tinyint
            GETDATE(),                       -- ModDate - datetime
            'sa',                            -- ModUser - varchar(50)
            NULL,                            -- AllowSchlReqFlds - bit
            0,                               -- MRUTypeId - smallint
            975,                             -- UsedIn - int
            NULL,                            -- TblFldsId - int
            NULL                             -- DisplayName - varchar(200)
            );
    END;


    DECLARE @GreatGrandMenuParent INT;
    SELECT @GreatGrandMenuParent =
    (
        SELECT MenuItemId
        FROM syMenuItems
        WHERE MenuName = 'Tools'
              AND MenuItemTypeId = 1
    );
    DECLARE @GrandMenuParent INT;
    SELECT @GrandMenuParent =
    (
        SELECT MenuItemId
        FROM syMenuItems
        WHERE MenuName = 'Common Tasks'
              AND ParentId = @GreatGrandMenuParent
    );
    DECLARE @MenuParent INT;
    IF NOT EXISTS
    (
        SELECT MenuItemId
        FROM syMenuItems
        WHERE MenuName = 'Academics'
              AND MenuItemTypeId = 3
              AND ParentId = @GrandMenuParent
    )
    BEGIN
        INSERT INTO dbo.syMenuItems
        (
            MenuName,
            DisplayName,
            Url,
            MenuItemTypeId,
            ParentId,
            DisplayOrder,
            IsPopup,
            ModDate,
            ModUser,
            IsActive,
            ResourceId,
            HierarchyId,
            ModuleCode,
            MRUType,
            HideStatusBar
        )
        VALUES
        (   'Academics',      -- MenuName - varchar(250)
            'Academics',      -- DisplayName - varchar(250)
            NULL,             -- Url - nvarchar(250)
            3,                -- MenuItemTypeId - smallint
            @GrandMenuParent, -- ParentId - int
            100,              -- DisplayOrder - int
            0,                -- IsPopup - bit
            GETDATE(),        -- ModDate - datetime
            'sa',             -- ModUser - varchar(50)
            1,                -- IsActive - bit
            739,              -- ResourceId - smallint
            NEWID(),          -- HierarchyId - uniqueidentifier
            '',               -- ModuleCode - varchar(5)
            0,                -- MRUType - int
            NULL              -- HideStatusBar - bit
            );
    END;

    SET @MenuParent =
    (
        SELECT MenuItemId
        FROM syMenuItems
        WHERE MenuName = 'Academics'
              AND MenuItemTypeId = 3
              AND ParentId = @GrandMenuParent
    );
    IF NOT EXISTS
    (
        SELECT TOP 1
               MenuItemId
        FROM dbo.syMenuItems
        WHERE MenuName = 'Grad Date Calculator'
              AND DisplayName = 'Grad Date Calculator'
              AND ResourceId = @newResourceId
              AND Url = '/AR/GradDateCalculator.aspx'
    )
    BEGIN
        INSERT INTO dbo.syMenuItems
        (
            MenuName,
            DisplayName,
            Url,
            MenuItemTypeId,
            ParentId,
            DisplayOrder,
            IsPopup,
            ModDate,
            ModUser,
            IsActive,
            ResourceId,
            HierarchyId,
            ModuleCode,
            MRUType,
            HideStatusBar
        )
        VALUES
        (   'Grad Date Calculator',         -- MenuName - varchar(250)
            'Grad Date Calculator',         -- DisplayName - varchar(250)
            N'/AR/GradDateCalculator.aspx', -- Url - nvarchar(250)
            4,                              -- MenuItemTypeId - smallint
            @MenuParent,                    -- ParentId - int
            100,                            -- DisplayOrder - int
            1,                              -- IsPopup - bit
            GETDATE(),                      -- ModDate - datetime
            'sa',                           -- ModUser - varchar(50)
            1,                              -- IsActive - bit
            @newResourceId,                 -- ResourceId - smallint
            NEWID(),                        -- HierarchyId - uniqueidentifier
            '',                             -- ModuleCode - varchar(5)
            0,                              -- MRUType - int
            NULL                            -- HideStatusBar - bit
            );
    END;
    ELSE
    BEGIN
        IF NOT EXISTS
        (
            SELECT 1
            FROM syMenuItems
            WHERE MenuName = 'Grad Date Calculator'
                  AND Url = '/AR/GradDateCalculator.aspx'
                  AND ParentId = @MenuParent
                  AND ResourceId = @newResourceId
        )
        BEGIN
            UPDATE dbo.syMenuItems
            SET ResourceId = @newResourceId,
                ParentId = @MenuParent
            WHERE MenuName = 'Grad Date Calculator'
                  AND Url = '/AR/GradDateCalculator.aspx';
        END;

    END;

    -- insert into synavigationNodes
    DECLARE @hierarcyId UNIQUEIDENTIFIER;
    SET @hierarcyId =
    (
        SELECT HierarchyId
        FROM syMenuItems
        WHERE ResourceId = @newResourceId
              AND Url = '/AR/GradDateCalculator.aspx'
    );

    DECLARE @ParentNavigation AS UNIQUEIDENTIFIER;

    IF NOT EXISTS
    (
        SELECT TOP 1
               childNode.HierarchyId
        FROM dbo.syNavigationNodes grandParentNode
            INNER JOIN dbo.syNavigationNodes parentNode
                ON parentNode.ParentId = grandParentNode.HierarchyId
            INNER JOIN dbo.syNavigationNodes childNode
                ON childNode.ParentId = parentNode.HierarchyId
        WHERE parentNode.ResourceId = 687
              AND grandParentNode.ResourceId = 771
              AND childNode.ResourceId = 739
    )
    BEGIN

        INSERT INTO dbo.syNavigationNodes
        (
            HierarchyId,
            HierarchyIndex,
            ResourceId,
            ParentId,
            ModUser,
            ModDate,
            IsPopupWindow,
            IsShipped
        )
        VALUES
        (   NEWID(),   -- HierarchyId - uniqueidentifier
            1,         -- HierarchyIndex - smallint
            739,       -- ResourceId - smallint
            (
                SELECT TOP 1
                       parentNode.HierarchyId
                FROM dbo.syNavigationNodes grandParentNode
                    INNER JOIN dbo.syNavigationNodes parentNode
                        ON parentNode.ParentId = grandParentNode.HierarchyId
                    INNER JOIN dbo.syNavigationNodes childNode
                        ON childNode.ParentId = parentNode.HierarchyId
                WHERE parentNode.ResourceId = 687
                      AND grandParentNode.ResourceId = 771 -- ParentId - uniqueidentifier
            ), 'SA',   -- ModUser - varchar(50)
            GETDATE(), -- ModDate - datetime
            0,         -- IsPopupWindow - bit
            1          -- IsShipped - bit
            );


    END;

    SET @ParentNavigation =
    (
        SELECT TOP 1
               childNode.HierarchyId
        FROM dbo.syNavigationNodes grandParentNode
            INNER JOIN dbo.syNavigationNodes parentNode
                ON parentNode.ParentId = grandParentNode.HierarchyId
            INNER JOIN dbo.syNavigationNodes childNode
                ON childNode.ParentId = parentNode.HierarchyId
        WHERE parentNode.ResourceId = 687
              AND grandParentNode.ResourceId = 771
              AND childNode.ResourceId = 739
    );
    IF NOT EXISTS
    (
        SELECT 1
        FROM syNavigationNodes
        WHERE ResourceId = @newResourceId
    )
    BEGIN

        INSERT INTO syNavigationNodes
        (
            HierarchyId,
            HierarchyIndex,
            ResourceId,
            ParentId,
            ModUser,
            ModDate,
            IsPopupWindow,
            IsShipped
        )
        VALUES
        (   @hierarcyId,       -- HierarchyId - uniqueidentifier
            8,                 -- HierarchyIndex - smallint
            @newResourceId,    -- ResourceId - smallint
            @ParentNavigation, -- ParentId - uniqueidentifier
            'Support',         -- ModUser - varchar(50)
            GETDATE(),         -- ModDate - datetime
            1,                 -- IsPopupWindow - bit
            1                  -- IsShipped - bit
            );
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION AddGradDateCalculatorMenu;
    PRINT 'Failed transcation AddGradDateCalculatorMenu.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION AddGradDateCalculatorMenu;
    PRINT 'successful transaction AddGradDateCalculatorMenu.';
END;
GO
--========================================================================================================================
--END
-- AD-5289 Create Grad Date Calculator that can be used during enrollment.
--END
--========================================================================================================================
--========================================================================================================================
-- Start AD-11239 : Issue with Student Groups on Adhoc
--========================================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION deleteRelation;
BEGIN TRY
    IF EXISTS
    (
        SELECT 1
        FROM syRptAdhocRelations
        WHERE FkTable = 'adLeadByLeadGroups'
              AND PkTable = 'arStudent'
    )
    BEGIN
        DELETE FROM syRptAdhocRelations
        WHERE FkTable = 'adLeadByLeadGroups'
              AND PkTable = 'arStudent';
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION deleteRelation;
    PRINT 'Failed transcation deleteRelation.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION deleteRelation;
    PRINT 'successful transaction deleteRelation.';
END;
GO
--========================================================================================================================
-- End AD-11239 : Issue with Student Groups on Adhoc
--========================================================================================================================
--=================================================================================================
-- Start AD-11144 Spike: Determine if its possible to have more than one value per cell in Adhoc
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION addFunCode;
BEGIN TRY
    IF NOT EXISTS
    (
        SELECT 1
        FROM syTblFlds
        WHERE FKColDescrip = 'FundSourceCode'
    )
    BEGIN

        DECLARE @CategoryId INTEGER;
        SET @CategoryId =
        (
            SELECT CategoryId
            FROM dbo.syFldCategories
            WHERE EntityId = 394
                  AND Descrip = 'Awards'
        );
        DECLARE @FldId INTEGER;
        SET @FldId =
        (
            SELECT FldId FROM syFields WHERE FldName LIKE 'FundSourceCode'
        );
        UPDATE syFldCaptions
        SET Caption = 'Fund Source Code'
        WHERE Caption LIKE 'Code'
              AND FldId = @FldId;

        DECLARE @FldTypeId INTEGER;
        SET @FldTypeId =
        (
            SELECT FldTypeId FROM syFieldTypes WHERE FldType LIKE 'Uniqueidentifier'
        );

        UPDATE syFields
        SET FldTypeId = @FldTypeId,
            FldName = 'FundSourceId'
        WHERE FldName LIKE 'FundSourceCode';

        DECLARE @TblId INTEGER;
        SET @TblId =
        (
            SELECT TblId FROM syTables WHERE TblName LIKE 'saTransactions'
        );

        UPDATE syTblFlds
        SET CategoryId = @CategoryId,
            FKColDescrip = 'FundSourceCode',
            TblId = @TblId
        WHERE FldId = @FldId;

    END;
    ELSE IF NOT EXISTS
         (
             SELECT 1
             FROM syFldCaptions
             WHERE Caption LIKE 'Fund Source Code'
         )
    BEGIN
        DECLARE @FldId1 INTEGER;
        SET @FldId1 =
        (
            SELECT FldId
            FROM syFields
            WHERE FldName LIKE 'FundSourceId'
                  AND DDLId IS NULL
        );

        UPDATE syFldCaptions
        SET Caption = 'Fund Source Code'
        WHERE Caption LIKE 'Code'
              AND FldId = @FldId1;
    END;

    ELSE IF NOT EXISTS
         (
             SELECT 1
             FROM syTblFlds t
                 JOIN syFldCategories c
                     ON c.CategoryId = t.CategoryId
                 JOIN syFldCaptions f
                     ON f.FldId = t.FldId
             WHERE f.Caption = 'Fund Source Code'
                   AND
                   (
                       c.Descrip = 'Awards'
                       OR c.Descrip IS NULL
                   )
         )
    BEGIN
        DECLARE @FldId2 INTEGER;
        SET @FldId2 =
        (
            SELECT FldId
            FROM syFields
            WHERE FldName LIKE 'FundSourceId'
                  AND DDLId IS NULL
        );
        DECLARE @CategoryId1 INTEGER;
        SET @CategoryId1 =
        (
            SELECT CategoryId
            FROM dbo.syFldCategories
            WHERE EntityId = 394
                  AND Descrip = 'Awards'
        );

        UPDATE syTblFlds
        SET CategoryId = @CategoryId1
        WHERE FldId = @FldId2;
    END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION addFunCode;
    PRINT 'Failed to add the fund source code to adhoc.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION addFunCode;
    PRINT 'Successful added the fund source code to adhoc.';
END;
GO
--=================================================================================================
-- End AD-11144 Spike: Determine if its possible to have more than one value per cell in Adhoc
--=================================================================================================
--=================================================================================================
-- START - AD-11275 - 9010 Data Export Report - Mods
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION tran9010DataExport;
BEGIN TRY
    IF NOT EXISTS (SELECT * FROM dbo.syResources WHERE ResourceID = 874)
    BEGIN

        --Create new subcategory under financial aid called 90/10 Reports and move three 9010 reports under that new submenu
        --Reports -> Financial Aid -> 90/10 Reports new resource
        INSERT INTO dbo.syResources
        (
            ResourceID,
            Resource,
            ResourceTypeID,
            ResourceURL,
            SummListId,
            ChildTypeId,
            ModDate,
            ModUser,
            AllowSchlReqFlds,
            MRUTypeId,
            UsedIn,
            TblFldsId,
            DisplayName
        )
        VALUES
        (   874,             -- ResourceID - smallint
            '90/10 Reports', -- Resource - varchar(200)
            2,               -- ResourceTypeID - tinyint
            NULL,            -- ResourceURL - varchar(100)
            NULL,            -- SummListId - smallint
            5,               -- ChildTypeId - tinyint
            GETDATE(),       -- ModDate - datetime
            'sa',            -- ModUser - varchar(50)
            0,               -- AllowSchlReqFlds - bit
            1,               -- MRUTypeId - smallint
            207,             -- UsedIn - int
            NULL,            -- TblFldsId - int
            '90/10 Reports'  -- DisplayName - varchar(200)
            );

        -- get parent id of sibling Reports -> Financial Aid -> General Reports 
        DECLARE @9010MenuCategoryReportsParentId INT =
                (
                    SELECT TOP 1
                           ParentId
                    FROM dbo.syMenuItems
                    WHERE MenuName = 'General Reports'
                          AND ResourceId = 716
                );

        -- Reports -> Financial aid -> 90/10 Reports menu item
        INSERT INTO dbo.syMenuItems
        (
            MenuName,
            DisplayName,
            Url,
            MenuItemTypeId,
            ParentId,
            DisplayOrder,
            IsPopup,
            ModDate,
            ModUser,
            IsActive,
            ResourceId,
            HierarchyId,
            ModuleCode,
            MRUType,
            HideStatusBar
        )
        VALUES
        (   '90/10 Reports',                  -- MenuName - varchar(250)
            '90/10 Reports',                  -- DisplayName - varchar(250)
            NULL,                             -- Url - nvarchar(250)
            3,                                -- MenuItemTypeId - smallint
            @9010MenuCategoryReportsParentId, -- ParentId - int
            200,                              -- DisplayOrder - int
            0,                                -- IsPopup - bit
            GETDATE(),                        -- ModDate - datetime
            'sa',                             -- ModUser - varchar(50)
            1,                                -- IsActive - bit
            874,                              -- ResourceId - smallint
            NEWID(),                          -- HierarchyId - uniqueidentifier
            NULL,                             -- ModuleCode - varchar(5)
            NULL,                             -- MRUType - int
            NULL                              -- HideStatusBar - bit
            );

        DECLARE @9010ReportsMenuItemId INT =
                (
                    SELECT TOP 1
                           MenuItemId
                    FROM dbo.syMenuItems
                    WHERE MenuName = '90/10 Reports'
                          AND ParentId = @9010MenuCategoryReportsParentId
                          AND ResourceId = 874
                );

        UPDATE dbo.syMenuItems
        SET ParentId = @9010ReportsMenuItemId
        WHERE MenuItemId =
        (
            SELECT MenuItemId
            FROM dbo.syMenuItems
            WHERE DisplayName = '90/10 Summary Report by Student'
                  AND ResourceId = 636
        );

        UPDATE dbo.syMenuItems
        SET ParentId = @9010ReportsMenuItemId
        WHERE MenuItemId =
        (
            SELECT MenuItemId
            FROM dbo.syMenuItems
            WHERE DisplayName = '90/10 Detailed Report for a Student'
                  AND ResourceId = 637
        );

        --Create new resource for 9010 Data Export
        INSERT INTO dbo.syResources
        (
            ResourceID,
            Resource,
            ResourceTypeID,
            ResourceURL,
            SummListId,
            ChildTypeId,
            ModDate,
            ModUser,
            AllowSchlReqFlds,
            MRUTypeId,
            UsedIn,
            TblFldsId,
            DisplayName
        )
        VALUES
        (   875,                        --ResouceId
            '90/10 Data Export',        -- Resource - varchar(200)
            5,                          -- ResourceTypeID - tinyint
            '~/SY/DataExport9010.aspx', -- ResourceURL - varchar(100)
            NULL,                       -- SummListId - smallint
            5,                          -- ChildTypeId - tinyint
            GETDATE(),                  -- ModDate - datetime
            'sa',                       -- ModUser - varchar(50)
            0,                          -- AllowSchlReqFlds - bit
            1,                          -- MRUTypeId - smallint
            975,                        -- UsedIn - int
            NULL,                       -- TblFldsId - int
            NULL                        -- DisplayName - varchar(200)
            );

        -- 90/10 Data Export record
        INSERT INTO dbo.syMenuItems
        (
            MenuName,
            DisplayName,
            Url,
            MenuItemTypeId,
            ParentId,
            DisplayOrder,
            IsPopup,
            ModDate,
            ModUser,
            IsActive,
            ResourceId,
            HierarchyId,
            ModuleCode,
            MRUType,
            HideStatusBar
        )
        VALUES
        (   '90/10 Data Export',       -- MenuName - varchar(250)
            '90/10 Data Export',       -- DisplayName - varchar(250)
            '/SY/DataExport9010.aspx', -- Url - nvarchar(250)
            4,                         -- MenuItemTypeId - smallint
            @9010ReportsMenuItemId,    -- ParentId - int
            1,                         -- DisplayOrder - int
            0,                         -- IsPopup - bit
            GETDATE(),                 -- ModDate - datetime
            NULL,                      -- ModUser - varchar(50)
            1,                         -- IsActive - bit
            875,                       -- ResourceId - smallint
            NEWID(),                   -- HierarchyId - uniqueidentifier
            NULL,                      -- ModuleCode - varchar(5)
            NULL,                      -- MRUType - int
            NULL                       -- HideStatusBar - bit
            );

        --syNavigation nodes - security

        DECLARE @parentHiearchyId UNIQUEIDENTIFIER =
                (
                    SELECT TOP 1
                           HierarchyId
                    FROM dbo.syMenuItems
                    WHERE DisplayName = 'General Reports'
                          AND ResourceId = 716
                );

        DECLARE @9010DataExportHiearchyId UNIQUEIDENTIFIER =
                (
                    SELECT TOP 1
                           HierarchyId
                    FROM dbo.syMenuItems
                    WHERE MenuName = '90/10 Data Export'
                          AND ResourceId = 875
                );

        -- for 90/10 data export item
        INSERT INTO dbo.syNavigationNodes
        (
            HierarchyId,
            HierarchyIndex,
            ResourceId,
            ParentId,
            ModUser,
            ModDate,
            IsPopupWindow,
            IsShipped
        )
        VALUES
        (   @9010DataExportHiearchyId, -- HierarchyId - uniqueidentifier
            9,                         -- HierarchyIndex - smallint
            875,                       -- ResourceId - smallint
            @parentHiearchyId,         -- ParentId - uniqueidentifier
            'sa',                      -- ModUser - varchar(50)
            GETDATE(),                 -- ModDate - datetime
            0,                         -- IsPopupWindow - bit
            1                          -- IsShipped - bit
            );

        INSERT INTO dbo.syResources
        (
            ResourceID,
            Resource,
            ResourceTypeID,
            ResourceURL,
            SummListId,
            ChildTypeId,
            ModDate,
            ModUser,
            AllowSchlReqFlds,
            MRUTypeId,
            UsedIn,
            TblFldsId,
            DisplayName
        )
        VALUES
        (   871,       -- ResourceID - smallint
            'Reports', -- Resource - varchar(200)
            1,         -- ResourceTypeID - tinyint
            NULL,      -- ResourceURL - varchar(100)
            NULL,      -- SummListId - smallint
            NULL,      -- ChildTypeId - tinyint
            NULL,      -- ModDate - datetime
            NULL,      -- ModUser - varchar(50)
            0,         -- AllowSchlReqFlds - bit
            NULL,      -- MRUTypeId - smallint
            207,       -- UsedIn - int
            NULL,      -- TblFldsId - int
            NULL       -- DisplayName - varchar(200)
            );

        DECLARE @parentMenuItemIdForReports INT =
                (
                    SELECT TOP 1
                           MenuItemId
                    FROM dbo.syMenuItems
                    WHERE MenuName = 'Maintenance'
                          AND DisplayName = 'Maintenance'
                          AND ResourceId = 688
                          AND MenuItemTypeId = 1
                );

        INSERT INTO dbo.syMenuItems
        (
            MenuName,
            DisplayName,
            Url,
            MenuItemTypeId,
            ParentId,
            DisplayOrder,
            IsPopup,
            ModDate,
            ModUser,
            IsActive,
            ResourceId,
            HierarchyId,
            ModuleCode,
            MRUType,
            HideStatusBar
        )
        VALUES
        (   'Reports',                   -- MenuName - varchar(250)
            'Reports',                   -- DisplayName - varchar(250)
            NULL,                        -- Url - nvarchar(250)
            2,                           -- MenuItemTypeId - smallint
            @parentMenuItemIdForReports, -- ParentId - int
            800,                         -- DisplayOrder - int
            0,                           -- IsPopup - bit
            GETDATE(),                   -- ModDate - datetime
            NULL,                        -- ModUser - varchar(50)
            1,                           -- IsActive - bit
            871,                         -- ResourceId - smallint
            NEWID(),                     -- HierarchyId - uniqueidentifier
            NULL,                        -- ModuleCode - varchar(5)
            NULL,                        -- MRUType - int
            NULL                         -- HideStatusBar - bit
            );

        INSERT INTO dbo.syResources
        (
            ResourceID,
            Resource,
            ResourceTypeID,
            ResourceURL,
            SummListId,
            ChildTypeId,
            ModDate,
            ModUser,
            AllowSchlReqFlds,
            MRUTypeId,
            UsedIn,
            TblFldsId,
            DisplayName
        )
        VALUES
        (   872,               -- ResourceID - smallint
            'General Options', -- Resource - varchar(200)
            2,                 -- ResourceTypeID - tinyint
            NULL,              -- ResourceURL - varchar(100)
            NULL,              -- SummListId - smallint
            4,                 -- ChildTypeId - tinyint
            NULL,              -- ModDate - datetime
            NULL,              -- ModUser - varchar(50)
            0,                 -- AllowSchlReqFlds - bit
            1,                 -- MRUTypeId - smallint
            207,               -- UsedIn - int
            NULL,              -- TblFldsId - int
            NULL               -- DisplayName - varchar(200)
            );

        DECLARE @newReportsMenuItemId INT =
                (
                    SELECT TOP 1
                           MenuItemId
                    FROM dbo.syMenuItems
                    WHERE MenuName = 'Reports'
                          AND DisplayName = 'Reports'
                          AND ResourceId = 871
                );

        INSERT INTO dbo.syMenuItems
        (
            MenuName,
            DisplayName,
            Url,
            MenuItemTypeId,
            ParentId,
            DisplayOrder,
            IsPopup,
            ModDate,
            ModUser,
            IsActive,
            ResourceId,
            HierarchyId,
            ModuleCode,
            MRUType,
            HideStatusBar
        )
        VALUES
        (   'General Options',     -- MenuName - varchar(250)
            'General Options',     -- DisplayName - varchar(250)
            NULL,                  -- Url - nvarchar(250)
            3,                     -- MenuItemTypeId - smallint
            @newReportsMenuItemId, -- ParentId - int
            100,                   -- DisplayOrder - int
            0,                     -- IsPopup - bit
            GETDATE(),             -- ModDate - datetime
            NULL,                  -- ModUser - varchar(50)
            1,                     -- IsActive - bit
            872,                   -- ResourceId - smallint
            NEWID(),               -- HierarchyId - uniqueidentifier
            NULL,                  -- ModuleCode - varchar(5)
            NULL,                  -- MRUType - int
            NULL                   -- HideStatusBar - bit
            );

        INSERT INTO dbo.syResources
        (
            ResourceID,
            Resource,
            ResourceTypeID,
            ResourceURL,
            SummListId,
            ChildTypeId,
            ModDate,
            ModUser,
            AllowSchlReqFlds,
            MRUTypeId,
            UsedIn,
            TblFldsId,
            DisplayName
        )
        VALUES
        (   873,                      -- ResourceID - smallint
            '90/10 Mappings',         -- Resource - varchar(200)
            5,                        -- ResourceTypeID - tinyint
            '~/SY/Mappings9010.aspx', -- ResourceURL - varchar(100)
            0,                        -- SummListId - smallint
            NULL,                     -- ChildTypeId - tinyint
            GETDATE(),                -- ModDate - datetime
            'sa',                     -- ModUser - varchar(50)
            NULL,                     -- AllowSchlReqFlds - bit
            0,                        -- MRUTypeId - smallint
            975,                      -- UsedIn - int
            NULL,                     -- TblFldsId - int
            NULL                      -- DisplayName - varchar(200)
            );


        DECLARE @GeneralOptionSyMenuId INT =
                (
                    SELECT TOP 1
                           MenuItemId
                    FROM dbo.syMenuItems
                    WHERE DisplayName = 'General Options'
                          AND ResourceId = 872
                );

        INSERT INTO dbo.syMenuItems
        (
            MenuName,
            DisplayName,
            Url,
            MenuItemTypeId,
            ParentId,
            DisplayOrder,
            IsPopup,
            ModDate,
            ModUser,
            IsActive,
            ResourceId,
            HierarchyId,
            ModuleCode,
            MRUType,
            HideStatusBar
        )
        VALUES
        (   '90/10 Mappings',        -- MenuName - varchar(250)
            '90/10 Mappings',        -- DisplayName - varchar(250)
            '/SY/Mappings9010.aspx', -- Url - nvarchar(250)
            4,                       -- MenuItemTypeId - smallint
            @GeneralOptionSyMenuId,  -- ParentId - int
            100,                     -- DisplayOrder - int
            0,                       -- IsPopup - bit
            GETDATE(),               -- ModDate - datetime
            'sa',                    -- ModUser - varchar(50)
            1,                       -- IsActive - bit
            873,                     -- ResourceId - smallint
            NEWID(),                 -- HierarchyId - uniqueidentifier
            '',                      -- ModuleCode - varchar(5)
            0,                       -- MRUType - int
            NULL                     -- HideStatusBar - bit
            );

        DECLARE @9010MappingHiearchyId UNIQUEIDENTIFIER =
                (
                    SELECT TOP 1
                           HierarchyId
                    FROM dbo.syMenuItems
                    WHERE DisplayName = '90/10 Mappings'
                          AND ResourceId = 873
                );

        --Move state board report under general options report
        UPDATE dbo.syMenuItems
        SET ParentId = @GeneralOptionSyMenuId
        WHERE DisplayName = 'State Board Settings'
              AND ResourceId = 863;

		UPDATE menuItem
		SET HierarchyId = nodes.HierarchyId
		FROM dbo.syMenuItems menuItem
		INNER JOIN syNavigationNodes nodes on nodes.ResourceId = menuItem.ResourceId
		WHERE DisplayName = 'State Board Settings'
              AND menuItem.ResourceId = 863
			  AND menuItem.HierarchyId IS NULL;

        DECLARE @HiearchyParentIdToSetFor9010Mapping UNIQUEIDENTIFIER =
                (
                    SELECT TOP 1
                           ParentId
                    FROM dbo.syNavigationNodes
                    WHERE HierarchyId =
                    (
                        SELECT TOP 1
                               HierarchyId
                        FROM dbo.syMenuItems
                        WHERE DisplayName = 'State Board Settings'
                              AND ResourceId = 863
                    )
                );
        INSERT INTO dbo.syNavigationNodes
        (
            HierarchyId,
            HierarchyIndex,
            ResourceId,
            ParentId,
            ModUser,
            ModDate,
            IsPopupWindow,
            IsShipped
        )
        VALUES
        (   @9010MappingHiearchyId,               -- HierarchyId - uniqueidentifier
            17,                                   -- HierarchyIndex - smallint
            873,                                  -- ResourceId - smallint
            @HiearchyParentIdToSetFor9010Mapping, -- ParentId - uniqueidentifier
            'sa',                                 -- ModUser - varchar(50)
            GETDATE(),                            -- ModDate - datetime
            0,                                    -- IsPopupWindow - bit
            1                                     -- IsShipped - bit
            );

    END;

    -- populate award types 9010 table
    IF NOT EXISTS (SELECT * FROM dbo.syAwardTypes9010 WHERE Name = 'None')
    BEGIN
        INSERT INTO dbo.syAwardTypes9010
        (
            AwardType9010Id,
            Name,
            Description,
            DisplayOrder,
            StatusId
        )
        VALUES
        (   NEWID(), -- AwardType9010Id - uniqueidentifier
            'None',  -- Name - varchar(50)
            'None',  -- Description - int,
            100,     -- DisplayOrder - int,
            (
                SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
            )        -- StatusId - uniqueidentifier
            );

    END;

    IF NOT EXISTS
    (
        SELECT *
        FROM dbo.syAwardTypes9010
        WHERE Name = 'Tuition Discount'
    )
    BEGIN


        INSERT INTO dbo.syAwardTypes9010
        (
            AwardType9010Id,
            Name,
            Description,
            DisplayOrder,
            StatusId
        )
        VALUES
        (   NEWID(),            -- AwardType9010Id - uniqueidentifier
            'Tuition Discount', -- Name - varchar(50)
            'Tuition Discount', -- Description - int
            200,                -- DisplayOrder - int,
            (
                SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
            )                   -- StatusId - uniqueidentifier
            );

    END;

    IF NOT EXISTS
    (
        SELECT *
        FROM dbo.syAwardTypes9010
        WHERE Name = 'Outside Grant'
    )
    BEGIN

        INSERT INTO dbo.syAwardTypes9010
        (
            AwardType9010Id,
            Name,
            Description,
            DisplayOrder,
            StatusId
        )
        VALUES
        (   NEWID(),         -- AwardType9010Id - uniqueidentifier
            'Outside Grant', -- Name - varchar(50)
            'Outside Grant', -- Description - int
            300,             -- DisplayOrder - int,
            (
                SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
            )                -- StatusId - uniqueidentifier
            );

    END;

    IF NOT EXISTS
    (
        SELECT *
        FROM dbo.syAwardTypes9010
        WHERE Name = 'School Grant'
    )
    BEGIN

        INSERT INTO dbo.syAwardTypes9010
        (
            AwardType9010Id,
            Name,
            Description,
            DisplayOrder,
            StatusId
        )
        VALUES
        (   NEWID(),        -- AwardType9010Id - uniqueidentifier
            'School Grant', -- Name - varchar(50)
            'School Grant', -- Description - int
            400,            -- DisplayOrder - int,
            (
                SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
            )               -- StatusId - uniqueidentifier
            );

    END;


    IF NOT EXISTS
    (
        SELECT *
        FROM dbo.syAwardTypes9010
        WHERE Name = 'Job Training Grant'
    )
    BEGIN

        INSERT INTO dbo.syAwardTypes9010
        (
            AwardType9010Id,
            Name,
            Description,
            DisplayOrder,
            StatusId
        )
        VALUES
        (   NEWID(),              -- AwardType9010Id - uniqueidentifier
            'Job Training Grant', -- Name - varchar(50)
            'Job Training Grant', -- Description - int
            500,                  -- DisplayOrder - int,
            (
                SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
            )                     -- StatusId - uniqueidentifier
            );

    END;


    IF NOT EXISTS
    (
        SELECT *
        FROM dbo.syAwardTypes9010
        WHERE Name = 'Educational Savings'
    )
    BEGIN

        INSERT INTO dbo.syAwardTypes9010
        (
            AwardType9010Id,
            Name,
            Description,
            DisplayOrder,
            StatusId
        )
        VALUES
        (   NEWID(),               -- AwardType9010Id - uniqueidentifier
            'Educational Savings', -- Name - varchar(50)
            'Educational Savings', -- Description - int
            600,                   -- DisplayOrder - int,
            (
                SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
            )                      -- StatusId - uniqueidentifier
            );

    END;

    IF NOT EXISTS
    (
        SELECT *
        FROM dbo.syAwardTypes9010
        WHERE Name = 'School Loan'
    )
    BEGIN


        INSERT INTO dbo.syAwardTypes9010
        (
            AwardType9010Id,
            Name,
            Description,
            DisplayOrder,
            StatusId
        )
        VALUES
        (   NEWID(),       -- AwardType9010Id - uniqueidentifier
            'School Loan', -- Name - varchar(50)
            'School Loan', -- Description - int
            700,           -- DisplayOrder - int,
            (
                SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
            )              -- StatusId - uniqueidentifier
            );

    END;


    IF NOT EXISTS
    (
        SELECT *
        FROM dbo.syAwardTypes9010
        WHERE Name = 'Outside Loan'
    )
    BEGIN

        INSERT INTO dbo.syAwardTypes9010
        (
            AwardType9010Id,
            Name,
            Description,
            DisplayOrder,
            StatusId
        )
        VALUES
        (   NEWID(),        -- AwardType9010Id - uniqueidentifier
            'Outside Loan', -- Name - varchar(50)
            'Outside Loan', -- Description - int
            800,            -- DisplayOrder - int,
            (
                SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
            )               -- StatusId - uniqueidentifier
            );

    END;

    IF NOT EXISTS
    (
        SELECT *
        FROM dbo.syAwardTypes9010
        WHERE Name = 'SEOGScholarshipMatch'
    )
    BEGIN

        INSERT INTO dbo.syAwardTypes9010
        (
            AwardType9010Id,
            Name,
            Description,
            DisplayOrder,
            StatusId
        )
        VALUES
        (   NEWID(),                -- AwardType9010Id - uniqueidentifier
            'SEOGScholarshipMatch', -- Name - varchar(50)
            'SEOGScholarshipMatch', -- Description - int
            900,                    -- DisplayOrder - int,
            (
                SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
            )                       -- StatusId - uniqueidentifier
            );

    END;


    IF NOT EXISTS (SELECT * FROM syFields WHERE FldName = 'FSEOGMatchType')
    BEGIN

        DECLARE @FldId INT =
                (
                    SELECT MAX(FldId) + 1 FROM dbo.syFields
                );

        DECLARE @FldTypeId INT =
                (
                    SELECT TOP 1 FldTypeId FROM dbo.syFieldTypes WHERE FldType = 'Char'
                );

        INSERT INTO dbo.syFields
        (
            FldId,
            FldName,
            FldTypeId,
            FldLen,
            DDLId,
            DerivedFld,
            SchlReq,
            LogChanges,
            Mask
        )
        VALUES
        (   @FldId,           -- FldId - int
            'FSEOGMatchType', -- FldName - varchar(200)
            @FldTypeId,       -- FldTypeId - int
            1,                -- FldLen - int
            NULL,             -- DDLId - int
            0,                -- DerivedFld - bit
            0,                -- SchlReq - bit
            1,                -- LogChanges - bit
            NULL              -- Mask - varchar(50)
            );

        DECLARE @TblFldsId INT =
                (
                    SELECT MAX(TblFldsId) + 1 FROM dbo.syTblFlds
                );

        DECLARE @TblId INT =
                (
                    SELECT TblId FROM dbo.syTables WHERE TblName = 'syCampuses'
                );

        INSERT INTO dbo.syTblFlds
        (
            TblFldsId,
            TblId,
            FldId,
            CategoryId,
            FKColDescrip
        )
        VALUES
        (   @TblFldsId, -- TblFldsId - int
            @TblId,     -- TblId - int
            @FldId,     -- FldId - int
            NULL,       -- CategoryId - int
            NULL        -- FKColDescrip - varchar(50)
            );



        DECLARE @ResDefId INT =
                (
                    SELECT MAX(ResDefId) + 1 FROM dbo.syResTblFlds
                );

        DECLARE @ResourceIdCampus INT =
                (
                    SELECT TOP 1
                           ResourceID
                    FROM dbo.syResources
                    WHERE ResourceID = 1
                          AND Resource = 'Campus'
                          AND ResourceTypeID = 4
                );


        INSERT INTO dbo.syResTblFlds
        (
            ResDefId,
            ResourceId,
            TblFldsId,
            Required,
            SchlReq,
            ControlName,
            UsePageSetup
        )
        VALUES
        (   @ResDefId,         -- ResDefId - int
            @ResourceIdCampus, -- ResourceId - smallint
            @TblFldsId,        -- TblFldsId - int
            0,                 -- Required - bit
            0,                 -- SchlReq - bit
            NULL,              -- ControlName - varchar(50)
            1                  -- UsePageSetup - bit
            );

        DECLARE @LangId INT =
                (
                    SELECT TOP 1 LangId FROM dbo.syLangs WHERE LangName = 'EN-US'
                );

        DECLARE @FldCapId INT =
                (
                    SELECT MAX(FldCapId) + 1 FROM dbo.syFldCaptions
                );

        INSERT INTO dbo.syFldCaptions
        (
            FldCapId,
            FldId,
            LangId,
            Caption,
            FldDescrip
        )
        VALUES
        (   @FldCapId,        -- FldCapId - int
            @FldId,           -- FldId - int
            @LangId,          -- LangId - tinyint
            'FSEOGMatchType', -- Caption - varchar(100)
            'FSEOGMatchType'  -- FldDescrip - varchar(150)
            );

    END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION tran9010DataExport;
    PRINT 'Failed to prepare 9010 data for reporting.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION tran9010DataExport;
    PRINT 'Successfully prepared 9010 data for reporting.';
END;
GO
--=================================================================================================
-- END - AD-11275 - 9010 Data Export Report - Mods
--=================================================================================================
--=================================================================================================
-- START - AD-10464 : Cannot Group by Student Group in Crystal Reports
--=================================================================================================
DECLARE @error INT;
SET @error = 0;
BEGIN TRANSACTION stuGrpCryRpt;
BEGIN TRY
    IF NOT EXISTS
    (
        SELECT 1
        FROM syRptParams
        WHERE ResourceId = 229
              AND RptCaption = 'Student Group'
    )
    BEGIN
        DECLARE @tblFldId INT;
        DECLARE @tblId INT;
        DECLARE @fldId INT;
        SET @tblId =
        (
            SELECT TblId FROM syTables WHERE TblName = 'adLeadByLeadGroups'
        );
        SET @fldId =
        (
            SELECT TOP 1
                   FldId
            FROM syFields
            WHERE FldName = 'LeadGrpId'
                  AND SchlReq IS NULL
                  AND LogChanges IS NULL
        );
        SET @tblFldId =
        (
            SELECT TblFldsId FROM syTblFlds WHERE TblId = @tblId AND FldId = @fldId
        );
        SELECT @tblFldId;
        DECLARE @rptParamId INT;
        SET @rptParamId =
        (
            SELECT MAX(RptParamId) FROM syRptParams
        ) + 1;
        INSERT INTO syRptParams
        (
            RptParamId,
            ResourceId,
            TblFldsId,
            Required,
            SortSec,
            FilterListSec,
            FilterOtherSec,
            RptCaption
        )
        VALUES
        (   @rptParamId,    -- RptParamId - smallint
            229,            -- ResourceId - smallint
            @tblFldId,      -- TblFldsId - int
            0,              -- Required - bit
            1,              -- SortSec - bit
            1,              -- FilterListSec - bit
            0,              -- FilterOtherSec - bit
            'Student Group' -- RptCaption - varchar(50)
            );
    END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION stuGrpCryRpt;
    PRINT 'Failed to add student group to crystal report.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION stuGrpCryRpt;
    PRINT 'Successfully added student group to crystal report.';
END;
GO
--=================================================================================================
-- END - AD-10464 : Cannot Group by Student Group in Crystal Reports
--=================================================================================================
--=================================================================================================
-- START - AD-11536 Add ability to Export Title IV Audit report to excel.
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION addExcelExportToT4Audit;
BEGIN TRY

    DECLARE @resourceId AS INTEGER;
    DECLARE @exportType AS INTEGER = 4; --export type for pdf/excel
    SET @resourceId = 777;

    IF NOT EXISTS
    (
        SELECT TOP 1
               ReportId
        FROM dbo.syReports
        WHERE ResourceId = @resourceId
              AND AllowedExportTypes = @exportType
    )
    BEGIN
        UPDATE dbo.syReports
        SET AllowedExportTypes = @exportType
        WHERE ResourceId = @resourceId;
    END;

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION addExcelExportToT4Audit;
    PRINT 'Failed to add excel export to title iv audit report.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION addExcelExportToT4Audit;
    PRINT 'Successfully added excel export to title iv audit report.';
END;
GO
--=================================================================================================
-- End AD-11536 Add ability to Export Title IV Audit report to excel.
--=================================================================================================
--=================================================================================================
-- START AD-11517 Summary Attendance Report
--=================================================================================================
DECLARE @errorAddingSummaryAttendanceReport AS INT;
SET @errorAddingSummaryAttendanceReport = 0;
BEGIN TRANSACTION SummaryAttendanceReportSetup;
BEGIN TRY
    DECLARE @SummaryAttendanceReportResourceId INT = 877;
    DECLARE @SummaryAttendanceReportName VARCHAR(30) = 'SummaryAttendance';
    DECLARE @SummaryAttendanceCaption VARCHAR(30) = 'SummaryAttendance';
    DECLARE @SummaryAttendanceSet VARCHAR(30) = 'SummaryAttendanceReportSet';
    DECLARE @SummaryAttendanceSection VARCHAR(30) = 'ParametersForSummaryAttendanceReport';
    DECLARE @SummaryAttendanceMenuName VARCHAR(30) = 'Attendance Summary';
    DECLARE @reportId UNIQUEIDENTIFIER;
    DECLARE @SummaryAttendanceSetId INT;
    DECLARE @SummaryAttendanceSectionId INT;
    DECLARE @SummaryAttendanceItemId INT;


    --insert new resource id
    IF NOT EXISTS
    (
        SELECT TOP 1
               ResourceID
        FROM dbo.syResources
        WHERE ResourceID = @SummaryAttendanceReportResourceId
    )
    BEGIN
        INSERT INTO dbo.syResources
        (
            ResourceID,
            Resource,
            ResourceTypeID,
            ResourceURL,
            SummListId,
            ChildTypeId,
            ModDate,
            ModUser,
            AllowSchlReqFlds,
            MRUTypeId,
            UsedIn,
            TblFldsId,
            DisplayName
        )
        VALUES
        (   @SummaryAttendanceReportResourceId, -- ResourceID - smallint
            @SummaryAttendanceMenuName,         -- Resource - varchar(200)
            5,                                  -- ResourceTypeID - tinyint
            '~/SY/ParamReport.aspx',            -- ResourceURL - varchar(100)
            0,                                  -- SummListId - smallint
            0,                                  -- ChildTypeId - tinyint
            GETDATE(),                          -- ModDate - datetime
            'sa',                               -- ModUser - varchar(50)
            NULL,                               -- AllowSchlReqFlds - bit
            0,                                  -- MRUTypeId - smallint
            975,                                -- UsedIn - int
            0,                                  -- TblFldsId - int
            ''                                  -- DisplayName - varchar(200)
            );
    END;

    -- insert param item 
    IF NOT EXISTS
    (
        SELECT TOP 1
               ItemId
        FROM dbo.ParamItem
        WHERE Caption = @SummaryAttendanceCaption
    )
    BEGIN
        INSERT INTO dbo.ParamItem
        (
            Caption,
            ControllerClass,
            valueprop,
            ReturnValueName,
            IsItemOverriden
        )
        VALUES
        (   @SummaryAttendanceCaption,             -- Caption - nvarchar(50)
            N'ParamAttendanceSummaryReport.ascx',  -- ControllerClass - nvarchar(50)
            0,                                     -- valueprop - int
            N'ParamAttendanceSummaryReportOption', -- ReturnValueName - nvarchar(50)
            NULL                                   -- IsItemOverriden - bit
            );
    END;

    SET @SummaryAttendanceItemId =
    (
        SELECT TOP 1
               ItemId
        FROM dbo.ParamItem
        WHERE Caption = @SummaryAttendanceCaption
    );


    --insert set id
    IF NOT EXISTS
    (
        SELECT TOP 1
               SetId
        FROM dbo.ParamSet
        WHERE SetName = @SummaryAttendanceSet
    )
    BEGIN
        INSERT INTO dbo.ParamSet
        (
            SetName,
            SetDisplayName,
            SetDescription,
            SetType
        )
        VALUES
        (   @SummaryAttendanceSet,                   -- SetName - nvarchar(50)
            N'Summary Attendance Report Filter Set', -- SetDisplayName - nvarchar(50)
            N'Summary Attendance Report',            -- SetDescription - nvarchar(1000)
            N'Custom'                                -- SetType - nvarchar(50)
            );
    END;

    SET @SummaryAttendanceSetId =
    (
        SELECT TOP 1 SetId FROM dbo.ParamSet WHERE SetName = @SummaryAttendanceSet
    );


    -- insert section id
    IF NOT EXISTS
    (
        SELECT TOP 1
               SectionId
        FROM dbo.ParamSection
        WHERE SectionName = @SummaryAttendanceSection
    )
    BEGIN
        INSERT INTO dbo.ParamSection
        (
            SectionName,
            SectionCaption,
            SetId,
            SectionSeq,
            SectionDescription,
            SectionType
        )
        VALUES
        (   @SummaryAttendanceSection,                               -- SectionName - nvarchar(50)
            N'Program, Program Version & Student Group',             -- SectionCaption - nvarchar(100)
            @SummaryAttendanceSetId,                                 -- SetId - bigint
            2,                                                       -- SectionSeq - int
            N'Choose Program, Program Version and/or Student Group', -- SectionDescription - nvarchar(500)
            0                                                        -- SectionType - int
            );
    END;

    SET @SummaryAttendanceSectionId =
    (
        SELECT TOP 1
               SectionId
        FROM dbo.ParamSection
        WHERE SectionName = @SummaryAttendanceSection
    );


    IF NOT EXISTS
    (
        SELECT TOP 1
               DetailId
        FROM dbo.ParamDetail
        WHERE SectionId = @SummaryAttendanceSectionId
              AND ItemId = @SummaryAttendanceItemId
    )
    BEGIN
        INSERT INTO dbo.ParamDetail
        (
            SectionId,
            ItemId,
            CaptionOverride,
            ItemSeq
        )
        VALUES
        (   @SummaryAttendanceSectionId, -- SectionId - bigint
            @SummaryAttendanceItemId,    -- ItemId - bigint
            N'Report Options',           -- CaptionOverride - nvarchar(50)
            1                            -- ItemSeq - int
            );
    END;


    DECLARE @parentId AS INT;
    DECLARE @SectionNameStudentSearch AS VARCHAR(50) = 'StudentSearchForAttendanceSummary';
    DECLARE @SectionCaptionStudentSearch AS VARCHAR(100) = 'Student Enrollment Search';
    DECLARE @SectionStudentSearchId AS INT;
    SET @parentId =
    (
        SELECT SetId FROM ParamSet WHERE SetName = @SummaryAttendanceSet
    );

    --insert search section
    IF NOT EXISTS
    (
        SELECT TOP 1
               SectionName
        FROM dbo.ParamSection
        WHERE SectionName = @SectionNameStudentSearch
    )
    BEGIN
        INSERT INTO dbo.ParamSection
        (
            SectionName,
            SectionCaption,
            SetId,
            SectionSeq,
            SectionDescription,
            SectionType
        )
        VALUES
        (   @SectionNameStudentSearch,             -- SectionName - nvarchar(50)
            @SectionCaptionStudentSearch,          -- SectionCaption - nvarchar(100)
            @parentId,                             -- SetId - bigint
            1,                                     -- SectionSeq - int
            'Filter report by student enrollment', -- SectionDescription - nvarchar(500)
            0                                      -- SectionType - int
            );
    END;
    SET @SectionStudentSearchId =
    (
        SELECT TOP 1
               SectionId
        FROM dbo.ParamSection
        WHERE SectionName = @SectionNameStudentSearch
              AND SectionCaption = @SectionCaptionStudentSearch
    );


    IF NOT EXISTS
    (
        SELECT TOP 1
               CaptionOverride
        FROM dbo.ParamDetail
        WHERE SectionId = @SectionStudentSearchId
    )
    BEGIN
        INSERT INTO dbo.ParamDetail
        (
            SectionId,
            ItemId,
            CaptionOverride,
            ItemSeq
        )
        VALUES
        (   @SectionStudentSearchId,      -- SectionId - bigint
            12,                           -- ItemId - bigint
            @SectionCaptionStudentSearch, -- CaptionOverride - nvarchar(50)
            1                             -- ItemSeq - int
            );
    END;

    DECLARE @AttendanceReportMenuItemParent INT =
            (
                SELECT TOP 1 MenuItemId FROM dbo.syMenuItems WHERE ResourceId = 691
            );

    UPDATE dbo.syMenuItems
    SET DisplayName = 'Attendance by Class'
    WHERE ResourceId = 492;

    IF NOT EXISTS
    (
        SELECT TOP 1
               MenuItemId
        FROM dbo.syMenuItems
        WHERE MenuName = @SummaryAttendanceMenuName
    )
    BEGIN

        INSERT INTO dbo.syMenuItems
        (
            MenuName,
            DisplayName,
            Url,
            MenuItemTypeId,
            ParentId,
            DisplayOrder,
            IsPopup,
            ModDate,
            ModUser,
            IsActive,
            ResourceId,
            HierarchyId,
            ModuleCode,
            MRUType,
            HideStatusBar
        )
        VALUES
        (   @SummaryAttendanceMenuName,         -- MenuName - varchar(250)
            @SummaryAttendanceMenuName,         -- DisplayName - varchar(250)
            N'/sy/ParamReport.aspx',            -- Url - nvarchar(250)
            4,                                  -- MenuItemTypeId - smallint
            @AttendanceReportMenuItemParent,    -- ParentId - int
            350,                                -- DisplayOrder - int
            0,                                  -- IsPopup - bit
            GETDATE(),                          -- ModDate - datetime
            'sa',                               -- ModUser - varchar(50)
            1,                                  -- IsActive - bit
            @SummaryAttendanceReportResourceId, -- ResourceId - smallint
            NULL,                               -- HierarchyId - uniqueidentifier
            '',                                 -- ModuleCode - varchar(5)
            0,                                  -- MRUType - int
            NULL                                -- HideStatusBar - bit
            );
    END;

    IF NOT EXISTS
    (
        SELECT TOP 1
               HierarchyId
        FROM dbo.syNavigationNodes
        WHERE ResourceId = @SummaryAttendanceReportResourceId
    )
    BEGIN
        DECLARE @reportParentId UNIQUEIDENTIFIER =
                (
                    SELECT TOP 1 HierarchyId FROM dbo.syNavigationNodes WHERE ResourceId = 691
                );
        INSERT INTO dbo.syNavigationNodes
        (
            HierarchyId,
            HierarchyIndex,
            ResourceId,
            ParentId,
            ModUser,
            ModDate,
            IsPopupWindow,
            IsShipped
        )
        VALUES
        (   NEWID(),                            -- HierarchyId - uniqueidentifier
            8,                                  -- HierarchyIndex - smallint
            @SummaryAttendanceReportResourceId, -- ResourceId - smallint
            @reportParentId,                    -- ParentId - uniqueidentifier
            'sa',                               -- ModUser - varchar(50)
            GETDATE(),                          -- ModDate - datetime
            0,                                  -- IsPopupWindow - bit
            1                                   -- IsShipped - bit
            );
    END;


    IF NOT EXISTS
    (
        SELECT TOP 1
               ReportId
        FROM dbo.syReports
        WHERE ReportName = @SummaryAttendanceCaption
    )
    BEGIN
        INSERT INTO dbo.syReports
        (
            ReportId,
            ResourceId,
            ReportName,
            ReportDescription,
            RDLName,
            AssemblyFilePath,
            ReportClass,
            CreationMethod,
            WebServiceUrl,
            RecordLimit,
            AllowedExportTypes,
            ReportTabLayout,
            ShowPerformanceMsg,
            ShowFilterMode
        )
        VALUES
        (   NEWID(),                                                           -- ReportId - uniqueidentifier
            @SummaryAttendanceReportResourceId,                                -- ResourceId - int
            @SummaryAttendanceReportName,                                      -- ReportName - varchar(50)
            @SummaryAttendanceReportName + '/' + @SummaryAttendanceReportName, -- ReportDescription - varchar(500)
            @SummaryAttendanceReportName + '/' + @SummaryAttendanceReportName, -- RDLName - varchar(200)
            '~/Bin/Reporting.dll',                                             -- AssemblyFilePath - varchar(200)
            'FAME.Advantage.Reporting.Logic.SummaryAttendance',                -- ReportClass - varchar(200)
            'BuildReport',                                                     -- CreationMethod - varchar(200)
            'futurefield',                                                     -- WebServiceUrl - varchar(200)
            400,                                                               -- RecordLimit - bigint
            0,                                                                 -- AllowedExportTypes - int
            1,                                                                 -- ReportTabLayout - int
            0,                                                                 -- ShowPerformanceMsg - bit
            0                                                                  -- ShowFilterMode - bit
            );
    END;


    SET @reportId =
    (
        SELECT TOP 1
               ReportId
        FROM dbo.syReports
        WHERE ReportName = @SummaryAttendanceCaption
    );

    IF NOT EXISTS
    (
        SELECT TOP 1
               ReportId
        FROM dbo.syReportTabs
        WHERE ReportId = @reportId
    )
    BEGIN
        INSERT INTO dbo.syReportTabs
        (
            ReportId,
            PageViewId,
            UserControlName,
            SetId,
            ControllerClass,
            DisplayName,
            ParameterSetLookUp,
            TabName
        )
        VALUES
        (   @reportId,                        -- ReportId - uniqueidentifier
            N'RadRptPage_Options',            -- PageViewId - nvarchar(200)
            N'ParamPanelBarSetCustomOptions', -- UserControlName - nvarchar(200)
            @SummaryAttendanceSetId,          -- SetId - bigint
            N'ParamSetPanelBarControl.ascx',  -- ControllerClass - nvarchar(200)
            N'Filter for Summary Attendance', -- DisplayName - nvarchar(200)
            @SummaryAttendanceSet,            -- ParameterSetLookUp - nvarchar(200)
            N'Options'                        -- TabName - nvarchar(200)
            );
    END;


    ---Date Range Control

    DECLARE @DateRangeControl VARCHAR(50) = 'DateRange';
    IF NOT EXISTS
    (
        SELECT TOP 1
               ItemId
        FROM dbo.ParamItem
        WHERE Caption = @DateRangeControl
    )
    BEGIN
        INSERT INTO dbo.ParamItem
        (
            Caption,
            ControllerClass,
            valueprop,
            ReturnValueName,
            IsItemOverriden
        )
        VALUES
        (   @DateRangeControl,       -- Caption - nvarchar(50)
            N'ParamDateRange.ascx',  -- ControllerClass - nvarchar(50)
            0,                       -- valueprop - int
            N'ParamDateRangeOption', -- ReturnValueName - nvarchar(50)
            NULL                     -- IsItemOverriden - bit
            );
    END;

    DECLARE @DateRangeControlItemId INTEGER =
            (
                SELECT TOP 1 ItemId FROM dbo.ParamItem WHERE Caption = @DateRangeControl
            );

    --Date Control Set 
    DECLARE @DateRangeControlSet VARCHAR(50) = 'DateRangeSet';
    IF NOT EXISTS
    (
        SELECT TOP 1
               SetId
        FROM dbo.ParamSet
        WHERE SetName = @DateRangeControlSet
    )
    BEGIN
        INSERT INTO dbo.ParamSet
        (
            SetName,
            SetDisplayName,
            SetDescription,
            SetType
        )
        VALUES
        (   @DateRangeControlSet,     -- SetName - nvarchar(50)
            N'Date Range Filter Set', -- SetDisplayName - nvarchar(50)
            N'Date Range',            -- SetDescription - nvarchar(1000)
            N'Custom'                 -- SetType - nvarchar(50)
            );
    END;

    DECLARE @DateRangeControlSetId INTEGER =
            (
                SELECT TOP 1 SetId FROM dbo.ParamSet WHERE SetName = @DateRangeControlSet
            );

    --Date Control Section 
    DECLARE @DateRangeControlSection VARCHAR(50) = 'DateRangeSection';
    IF NOT EXISTS
    (
        SELECT TOP 1
               SectionId
        FROM dbo.ParamSection
        WHERE SectionName = @DateRangeControlSection
    )
    BEGIN
        INSERT INTO dbo.ParamSection
        (
            SectionName,
            SectionCaption,
            SetId,
            SectionSeq,
            SectionDescription,
            SectionType
        )
        VALUES
        (   @DateRangeControlSection, -- SectionName - nvarchar(50)
            N'Date Range',            -- SectionCaption - nvarchar(100)
            @SummaryAttendanceSetId,  -- SetId - bigint
            3,                        -- SectionSeq - int
            N'Choose the Date Range', -- SectionDescription - nvarchar(500)
            0                         -- SectionType - int
            );
    END;

    DECLARE @DateRangeControlSectionId INTEGER =
            (
                SELECT TOP 1
                       SectionId
                FROM dbo.ParamSection
                WHERE SectionName = @DateRangeControlSection
            );

    IF NOT EXISTS
    (
        SELECT TOP 1
               DetailId
        FROM dbo.ParamDetail
        WHERE SectionId = @DateRangeControlSectionId
              AND ItemId = @DateRangeControlItemId
    )
    BEGIN
        INSERT INTO dbo.ParamDetail
        (
            SectionId,
            ItemId,
            CaptionOverride,
            ItemSeq
        )
        VALUES
        (   @DateRangeControlSectionId, -- SectionId - bigint
            @DateRangeControlItemId,    -- ItemId - bigint
            N'Report Options',          -- CaptionOverride - nvarchar(50)
            3                           -- ItemSeq - int
            );
    END;


END TRY
BEGIN CATCH
    SET @errorAddingSummaryAttendanceReport = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @errorAddingSummaryAttendanceReport > 0
BEGIN
    ROLLBACK TRANSACTION SummaryAttendanceReportSetup;
    PRINT 'Failed transcation SummaryAttendanceReportSetup.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION SummaryAttendanceReportSetup;
    PRINT 'successful transaction SummaryAttendanceReportSetup.';
END;
GO


--=================================================================================================
-- END AD-11517 Summary Attendance Report
--=================================================================================================
--=================================================================================================
--=================================================================================================
-- START AD-11602 90/10 Mapping Screen Updates
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION mapping9010ScreenUpdates;
BEGIN TRY

    IF NOT EXISTS
    (
        SELECT *
        FROM dbo.syAwardTypes9010
        WHERE Name = 'Student Payment'
    )
    BEGIN
        INSERT INTO dbo.syAwardTypes9010
        (
            AwardType9010Id,
            Name,
            Description,
            DisplayOrder,
            StatusId
        )
        VALUES
        (   NEWID(),           -- AwardType9010Id - uniqueidentifier
            'Student Payment', -- Name - varchar(50)
            'Student Payment', -- Description - int,
            100,               -- DisplayOrder - int,
            (
                SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
            )                  -- StatusId - uniqueidentifier
            );
    END;

    IF NOT EXISTS
    (
        SELECT *
        FROM dbo.syAwardTypes9010
        WHERE Name = 'Refund to Student'
    )
    BEGIN
        INSERT INTO dbo.syAwardTypes9010
        (
            AwardType9010Id,
            Name,
            Description,
            DisplayOrder,
            StatusId
        )
        VALUES
        (   NEWID(),             -- AwardType9010Id - uniqueidentifier
            'Refund to Student', -- Name - varchar(50)
            'Refund to Student', -- Description - int,
            100,                 -- DisplayOrder - int,
            (
                SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
            )                    -- StatusId - uniqueidentifier
            );
    END;


    IF NOT EXISTS
    (
        SELECT *
        FROM dbo.syAwardTypes9010
        WHERE Name = 'Student Stipend'
    )
    BEGIN
        INSERT INTO dbo.syAwardTypes9010
        (
            AwardType9010Id,
            Name,
            Description,
            DisplayOrder,
            StatusId
        )
        VALUES
        (   NEWID(),           -- AwardType9010Id - uniqueidentifier
            'Student Stipend', -- Name - varchar(50)
            'Student Stipend', -- Description - int,
            100,               -- DisplayOrder - int,
            (
                SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
            )                  -- StatusId - uniqueidentifier
            );
    END;

    IF NOT EXISTS
    (
        SELECT *
        FROM dbo.syAwardTypes9010
        WHERE Name = 'Student Stipend'
    )
    BEGIN
        INSERT INTO dbo.syAwardTypes9010
        (
            AwardType9010Id,
            Name,
            Description,
            DisplayOrder,
            StatusId
        )
        VALUES
        (   NEWID(),           -- AwardType9010Id - uniqueidentifier
            'Student Stipend', -- Name - varchar(50)
            'Student Stipend', -- Description - int,
            100,               -- DisplayOrder - int,
            (
                SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
            )                  -- StatusId - uniqueidentifier
            );
    END;

    IF NOT EXISTS (SELECT * FROM dbo.syAwardTypes9010 WHERE Name = 'Title IV')
    BEGIN
        INSERT INTO dbo.syAwardTypes9010
        (
            AwardType9010Id,
            Name,
            Description,
            DisplayOrder,
            StatusId
        )
        VALUES
        (   NEWID(),    -- AwardType9010Id - uniqueidentifier
            'Title IV', -- Name - varchar(50)
            'Title IV', -- Description - int,
            100,        -- DisplayOrder - int,
            (
                SELECT TOP 1 StatusId FROM dbo.syStatuses WHERE StatusCode = 'A'
            )           -- StatusId - uniqueidentifier
            );
    END;

    --Remove 90/10 summary report by student and detail ( Not implemented )
    DELETE FROM dbo.syNavigationNodes
    WHERE HierarchyId =
    (
        SELECT TOP 1
               HierarchyId
        FROM dbo.syMenuItems
        WHERE DisplayName = '90/10 Summary Report by Student'
    );
    DELETE FROM dbo.syNavigationNodes
    WHERE HierarchyId =
    (
        SELECT TOP 1
               HierarchyId
        FROM dbo.syMenuItems
        WHERE DisplayName = '90/10 Detailed Report for a Student'
    );

    DELETE FROM dbo.syMenuItems
    WHERE DisplayName = '90/10 Summary Report by Student';
    DELETE FROM dbo.syMenuItems
    WHERE DisplayName = '90/10 Detailed Report for a Student';

END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
BEGIN
    ROLLBACK TRANSACTION mapping9010ScreenUpdates;
    PRINT 'Failed to update mapping screen updates 90/10.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION mapping9010ScreenUpdates;
    PRINT 'Successfully updated mapping screen updates 90/10.';
END;
GO
--=================================================================================================
-- End AD-11602 90/10 Mapping Screen Updates
--=================================================================================================
--=================================================================================================
-- START AD-11142 Fix for inserting types of calculation period types
--=================================================================================================
DECLARE @ErrorAddingCalculationPeriorType AS INT;
SET @ErrorAddingCalculationPeriorType = 0;
BEGIN TRANSACTION AddingCalculationPeriorType;
BEGIN TRY
    DECLARE @PeriodName VARCHAR(100) = 'Payment Period',
            @PeriodOfEnrollment VARCHAR(100) = 'Period of Enrollment';

    DECLARE @SupportUserId UNIQUEIDENTIFIER =
            (
                SELECT TOP 1
                       UserId
                FROM dbo.syUsers
                WHERE (
                          IsAdvantageSuperUser = 1
                          AND FullName = 'support'
                      )
                      OR
                      (
                          IsAdvantageSuperUser = 1
                          AND FullName = 'sa'
                      )
            );
    BEGIN
        IF NOT EXISTS
        (
            SELECT 1
            FROM syR2T4CalculationPeriodTypes
            WHERE Code IN ( @PeriodName )
        )
        BEGIN

            INSERT INTO syR2T4CalculationPeriodTypes
            (
                CalculationPeriodTypeId,
                Code,
                Description,
                ModDate,
                ModifiedByUserId
            )
            VALUES
            (NEWID(), @PeriodName, @PeriodName, GETDATE(), @SupportUserId);

        END;
        IF NOT EXISTS
        (
            SELECT 1
            FROM syR2T4CalculationPeriodTypes
            WHERE Code IN ( @PeriodOfEnrollment )
        )
        BEGIN
            INSERT INTO syR2T4CalculationPeriodTypes
            (
                CalculationPeriodTypeId,
                Code,
                Description,
                ModDate,
                ModifiedByUserId
            )
            VALUES
            (NEWID(), @PeriodOfEnrollment, @PeriodOfEnrollment, GETDATE(), @SupportUserId);
        END;
    END;
END TRY
BEGIN CATCH
    SET @ErrorAddingCalculationPeriorType = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @ErrorAddingCalculationPeriorType > 0
BEGIN
    ROLLBACK TRANSACTION AddingCalculationPeriorType;
    PRINT 'Failed transcation AddingCalculationPeriorType.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION AddingCalculationPeriorType;
    PRINT 'successful transaction AddingCalculationPeriorType.';
END;
GO
--=================================================================================================
-- END AD-11142 Fix for inserting types of calculation period types
--=================================================================================================