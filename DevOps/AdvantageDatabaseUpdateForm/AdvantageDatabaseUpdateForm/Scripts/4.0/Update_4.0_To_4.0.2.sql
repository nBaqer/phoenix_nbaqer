﻿SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[AdLeadEmail]'
GO
IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AdLeadEmail_syEmailType_EMailTypeId_EMailTypeId]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[AdLeadEmail]', 'U'))
ALTER TABLE [dbo].[AdLeadEmail] DROP CONSTRAINT [FK_AdLeadEmail_syEmailType_EMailTypeId_EMailTypeId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping foreign keys from [dbo].[adLeads]'
GO
IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_adLeads_adLeadGroups_LeadgrpId_LeadGrpId]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[adLeads]', 'U'))
ALTER TABLE [dbo].[adLeads] DROP CONSTRAINT [FK_adLeads_adLeadGroups_LeadgrpId_LeadGrpId]
GO
IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_adLeads_syInstitutions_HighSchoolId_HSId]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[adLeads]', 'U'))
ALTER TABLE [dbo].[adLeads] DROP CONSTRAINT [FK_adLeads_syInstitutions_HighSchoolId_HSId]
GO
IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_adLeads_syStatuses_StudentStatusId_StatusId]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[adLeads]', 'U'))
ALTER TABLE [dbo].[adLeads] DROP CONSTRAINT [FK_adLeads_syStatuses_StudentStatusId_StatusId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [_dta_index_syCreditSummary_7_1335428277__K1_17_18] from [dbo].[syCreditSummary]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'_dta_index_syCreditSummary_7_1335428277__K1_17_18' AND object_id = OBJECT_ID(N'[dbo].[syCreditSummary]'))
DROP INDEX [_dta_index_syCreditSummary_7_1335428277__K1_17_18] ON [dbo].[syCreditSummary]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [_dta_index_syCreditSummary_7_1335428277__K1_15_16] from [dbo].[syCreditSummary]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'_dta_index_syCreditSummary_7_1335428277__K1_15_16' AND object_id = OBJECT_ID(N'[dbo].[syCreditSummary]'))
DROP INDEX [_dta_index_syCreditSummary_7_1335428277__K1_15_16] ON [dbo].[syCreditSummary]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [_dta_index_syCreditSummary_7_1335428277__K1_K2_17_18] from [dbo].[syCreditSummary]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'_dta_index_syCreditSummary_7_1335428277__K1_K2_17_18' AND object_id = OBJECT_ID(N'[dbo].[syCreditSummary]'))
DROP INDEX [_dta_index_syCreditSummary_7_1335428277__K1_K2_17_18] ON [dbo].[syCreditSummary]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [_dta_index_syCreditSummary_7_1335428277__K1_K2_K4_17_18] from [dbo].[syCreditSummary]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'_dta_index_syCreditSummary_7_1335428277__K1_K2_K4_17_18' AND object_id = OBJECT_ID(N'[dbo].[syCreditSummary]'))
DROP INDEX [_dta_index_syCreditSummary_7_1335428277__K1_K2_K4_17_18] ON [dbo].[syCreditSummary]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping index [_dta_index_syCreditSummary_7_1335428277__K1_K2_K4_K8_K7_K13_K9_K10_K11_K12_15_16_17_18] from [dbo].[syCreditSummary]'
GO
IF EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'_dta_index_syCreditSummary_7_1335428277__K1_K2_K4_K8_K7_K13_K9_K10_K11_K12_15_16_17_18' AND object_id = OBJECT_ID(N'[dbo].[syCreditSummary]'))
DROP INDEX [_dta_index_syCreditSummary_7_1335428277__K1_K2_K4_K8_K7_K13_K9_K10_K11_K12_15_16_17_18] ON [dbo].[syCreditSummary]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[saDeferredRevenues_Audit_Delete] from [dbo].[saDeferredRevenues]'
GO
IF OBJECT_ID(N'[dbo].[saDeferredRevenues_Audit_Delete]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[saDeferredRevenues_Audit_Delete]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[saDeferredRevenues_Audit_Insert] from [dbo].[saDeferredRevenues]'
GO
IF OBJECT_ID(N'[dbo].[saDeferredRevenues_Audit_Insert]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[saDeferredRevenues_Audit_Insert]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[saDeferredRevenues_Audit_Update] from [dbo].[saDeferredRevenues]'
GO
IF OBJECT_ID(N'[dbo].[saDeferredRevenues_Audit_Update]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[saDeferredRevenues_Audit_Update]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[GetLDA]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLDA]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[GetLDA]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_AT_Step06_InsertAttendance_Class_ClockHour]'
GO
IF OBJECT_ID(N'[dbo].[USP_AT_Step06_InsertAttendance_Class_ClockHour]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_AT_Step06_InsertAttendance_Class_ClockHour]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[GetStudentsForPostFinalGrades]'
GO
IF OBJECT_ID(N'[dbo].[GetStudentsForPostFinalGrades]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[GetStudentsForPostFinalGrades]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[IntegrationEnrollmentTracking]'
GO
IF OBJECT_ID(N'[dbo].[IntegrationEnrollmentTracking]', 'U') IS NULL
CREATE TABLE [dbo].[IntegrationEnrollmentTracking]
(
[EnrollmentTrackingId] [uniqueidentifier] NOT NULL CONSTRAINT [DF_IntegrationEnrollmentTracking_EnrollmentTrackingId] DEFAULT (newid()),
[StuEnrollId] [uniqueidentifier] NOT NULL,
[EnrollmentStatus] [varchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[ModDate] [datetime] NOT NULL,
[ModUser] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
[Processed] [bit] NOT NULL CONSTRAINT [DF_IntegrationEnrollmentTracking_Processed] DEFAULT ((0))
)
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating primary key [PK_IntegrationEnrollmentTracking_EnrollmentTrackingId] on [dbo].[IntegrationEnrollmentTracking]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.indexes WHERE name = N'PK_IntegrationEnrollmentTracking_EnrollmentTrackingId' AND object_id = OBJECT_ID(N'[dbo].[IntegrationEnrollmentTracking]'))
ALTER TABLE [dbo].[IntegrationEnrollmentTracking] ADD CONSTRAINT [PK_IntegrationEnrollmentTracking_EnrollmentTrackingId] PRIMARY KEY CLUSTERED  ([EnrollmentTrackingId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[plExitInterview]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[plExitInterview]', N'ExitInterviewDate') IS NULL
ALTER TABLE [dbo].[plExitInterview] ADD[ExitInterviewDate] [datetime2] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[GetLDA]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GetLDA]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'

CREATE FUNCTION [dbo].[GetLDA]
    (
     @StuEnrollId UNIQUEIDENTIFIER
	)
RETURNS DATETIME
AS
    BEGIN
        DECLARE @ReturnValue DATETIME;
        SELECT  @ReturnValue = DATEADD(dd,0,DATEDIFF(dd,0,MAX(LDA)))
        FROM    (
                  SELECT    MAX(AttendedDate) AS LDA
                  FROM      arExternshipAttendance
                  WHERE     StuEnrollId = @StuEnrollId
                  UNION ALL
                  SELECT    MAX(MeetDate) AS LDA
                  FROM      atClsSectAttendance
                  WHERE     StuEnrollId = @StuEnrollId
                            AND Actual > 0
                            AND (
                                  Actual <> 99.00
                                  AND Actual <> 999.00
                                  AND Actual <> 9999.00
                                )
                  UNION ALL
                  SELECT    MAX(RecordDate) AS LDA
                  FROM      arStudentClockAttendance
                  WHERE     StuEnrollId = @StuEnrollId
                            AND (
                                  ActualHours > 0
                                  AND ActualHours <> 99.00
                                  AND ActualHours <> 999.00
                                  AND ActualHours <> 9999.00
                                )
                  UNION ALL
                  SELECT    MAX(MeetDate) AS LDA
                  FROM      atConversionAttendance
                  WHERE     StuEnrollId = @StuEnrollId
                            AND (
                                  Actual > 0
                                  AND Actual <> 99.00
                                  AND Actual <> 999.00
                                  AND Actual <> 9999.00
                                )
                  UNION ALL
                  SELECT    LDA
                  FROM      arStuEnrollments
                  WHERE     StuEnrollId = @StuEnrollId
                ) TR;
        RETURN @ReturnValue;
    END;




'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[GetStudentsForPostFinalGrades]'
GO
IF OBJECT_ID(N'[dbo].[GetStudentsForPostFinalGrades]', 'P') IS NULL
EXEC sp_executesql N'
-- =============================================
-- Author:		Ginzo, John
-- Create date: 09/03/2015
-- Description:	Get list of Students for Post Final
--				grades page
-- =============================================
CREATE PROCEDURE [dbo].[GetStudentsForPostFinalGrades]
    @ClsSectId VARCHAR(50)
   ,@UserId VARCHAR(50)
AS
    BEGIN

        SET NOCOUNT ON;

        -----------------------------------------------------------------------------------------
        -- Determine what the school uses as a student identifier based on the config value
        -----------------------------------------------------------------------------------------
        DECLARE @StudentIdentifier VARCHAR(50);
        SET @StudentIdentifier = (
                                 SELECT     v.Value
                                 FROM       dbo.syConfigAppSettings c
                                 INNER JOIN dbo.syConfigAppSetValues v ON c.SettingId = v.SettingId
                                 WHERE      KeyName = ''StudentIdentifier''
                                 );

        -----------------------------------------------------------------------------------------
        -- Initialize table hold students
        -----------------------------------------------------------------------------------------
        DECLARE @InitResults TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,StudentIdentifier VARCHAR(50)
               ,IsSSNStudentIdentifier INT
               ,LastName VARCHAR(50)
               ,FirstName VARCHAR(50)
               ,PrgVerId UNIQUEIDENTIFIER
               ,IsContinuingEd BIT
               ,StatusCodeDescrip VARCHAR(50)
               ,PostAcademics BIT
               ,StudentId UNIQUEIDENTIFIER
               ,SysStatusId INT
               ,StartDate DATETIME
               ,GraduatedDate DATETIME
               ,DateCompleted DATETIME
            );

        -----------------------------------------------------------------------------------------
        -- We have two fields that will come from different values depending on the config value,
        -- so we need to construct a different query based on the value
        -----------------------------------------------------------------------------------------
        IF @StudentIdentifier = ''EnrollmentId''
            BEGIN
                INSERT INTO @InitResults
                            SELECT   DISTINCT r.StuEnrollId
                                             ,e.EnrollmentId AS StudentIdentifier --use enrollment id as student identifier
                                             ,0 AS IsSSNStudentIdentifier         -- use 0 as ssn identifier
                                             ,s.LastName
                                             ,s.FirstName
                                             ,pv.PrgVerId
                                             ,pv.IsContinuingEd
                                             ,st.StatusCodeDescrip
                                             ,ss.PostAcademics
                                             ,s.StudentId
                                             ,ss.SysStatusId
                                             ,e.StartDate
                                             ,e.ExpGradDate
                                             ,CASE WHEN CONVERT(DATE, ISNULL(r.DateCompleted,''1900-01-01''))  <= ''1900-01-01'' THEN NULL ELSE r.DateCompleted END AS DateCompleted
                            FROM     arResults r
                            JOIN     dbo.arStuEnrollments e ON r.StuEnrollId = e.StuEnrollId
                            JOIN     dbo.arStudent s ON e.StudentId = s.StudentId
                            JOIN     dbo.arPrgVersions pv ON e.PrgVerId = pv.PrgVerId
                            JOIN     dbo.syStatusCodes st ON e.StatusCodeId = st.StatusCodeId
                            JOIN     dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
                            WHERE    r.TestId = @ClsSectId
                            ORDER BY s.LastName
                                    ,s.FirstName;
            END;
        ELSE IF @StudentIdentifier = ''StudentId''
                 BEGIN
                     INSERT INTO @InitResults
                                 SELECT   DISTINCT r.StuEnrollId
                                                  ,s.StudentNumber AS StudentIdentifier --use student Number as the student identifier
                                                  ,0 AS IsSSNStudentIdentifier          -- use 0 as the ssn identifier
                                                  ,s.LastName
                                                  ,s.FirstName
                                                  ,pv.PrgVerId
                                                  ,pv.IsContinuingEd
                                                  ,st.StatusCodeDescrip
                                                  ,ss.PostAcademics
                                                  ,s.StudentId
                                                  ,ss.SysStatusId
                                                  ,e.StartDate
                                                  ,e.ExpGradDate
                                                  ,CASE WHEN CONVERT(DATE, ISNULL(r.DateCompleted,''1900-01-01''))  <= ''1900-01-01'' THEN NULL ELSE r.DateCompleted END AS DateCompleted
                                 FROM     arResults r
                                 JOIN     dbo.arStuEnrollments e ON r.StuEnrollId = e.StuEnrollId
                                 JOIN     dbo.arStudent s ON e.StudentId = s.StudentId
                                 JOIN     dbo.arPrgVersions pv ON e.PrgVerId = pv.PrgVerId
                                 JOIN     dbo.syStatusCodes st ON e.StatusCodeId = st.StatusCodeId
                                 JOIN     dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
                                 WHERE    r.TestId = @ClsSectId
                                 ORDER BY s.LastName
                                         ,s.FirstName;
                 END;
        ELSE
                 BEGIN
                     INSERT INTO @InitResults
                                 SELECT   DISTINCT r.StuEnrollId
                                                  ,s.SSN AS StudentIdentifier  -- use ssn as the student identifier
                                                  ,1 AS IsSSNStudentIdentifier -- use 1 as the ssn identifier
                                                  ,s.LastName
                                                  ,s.FirstName
                                                  ,pv.PrgVerId
                                                  ,pv.IsContinuingEd
                                                  ,st.StatusCodeDescrip
                                                  ,ss.PostAcademics
                                                  ,s.StudentId
                                                  ,ss.SysStatusId
												  ,e.StartDate
                                                  ,e.ExpGradDate
                                                  ,CASE WHEN CONVERT(DATE, ISNULL(r.DateCompleted,''1900-01-01''))  <= ''1900-01-01'' THEN NULL ELSE r.DateCompleted END AS DateCompleted
                                 FROM     arResults r
                                 JOIN     dbo.arStuEnrollments e ON r.StuEnrollId = e.StuEnrollId
                                 JOIN     dbo.arStudent s ON e.StudentId = s.StudentId
                                 JOIN     dbo.arPrgVersions pv ON e.PrgVerId = pv.PrgVerId
                                 JOIN     dbo.syStatusCodes st ON e.StatusCodeId = st.StatusCodeId
                                 JOIN     dbo.sySysStatus ss ON st.SysStatusId = ss.SysStatusId
                                 WHERE    r.TestId = @ClsSectId
                                 ORDER BY s.LastName
                                         ,s.FirstName;
                 END;

        -----------------------------------------------------------------------------------------
        -- Check the user id to see if they are either support, sa, system admin, or director of
        -- academics.  If so, return records regardless of in or out of school status.
        -- If not, return records filtered for status 
        -----------------------------------------------------------------------------------------
        IF EXISTS (
                  SELECT     r.RoleId
                  FROM       dbo.syRoles r
                  INNER JOIN dbo.sySysRoles sr ON r.SysRoleId = sr.SysRoleId
                  INNER JOIN dbo.syUsersRolesCampGrps ur ON r.RoleId = ur.RoleId
                  INNER JOIN dbo.syUsers u ON ur.UserId = u.UserId
                  WHERE      ur.UserId = @UserId
                             AND (
                                 u.UserName IN ( ''sa'', ''support'' )
                                 OR sr.SysRoleId IN ( 1, 13 )
                                 )
                  )
            BEGIN
                SELECT StuEnrollId
                      ,StudentIdentifier
                      ,IsSSNStudentIdentifier
                      ,LastName
                      ,FirstName
                      ,PrgVerId
                      ,IsContinuingEd
                      ,StatusCodeDescrip
                      ,SysStatusId
                      ,StudentId
                      ,PostAcademics
                      ,SysStatusId
					  ,StartDate
					  ,GraduatedDate
					  ,DateCompleted
                FROM   @InitResults;
            END;
        ELSE
            BEGIN
                SELECT StuEnrollId
                      ,StudentIdentifier
                      ,IsSSNStudentIdentifier
                      ,LastName
                      ,FirstName
                      ,PrgVerId
                      ,IsContinuingEd
                      ,StatusCodeDescrip
                      ,SysStatusId
                      ,StudentId
                      ,PostAcademics
                      ,SysStatusId
					  ,StartDate
					  ,GraduatedDate
					  ,DateCompleted
                FROM   @InitResults
                WHERE  PostAcademics = 1
                       OR SysStatusId = 7;
            END;

    END;




'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_AT_Step06_InsertAttendance_Class_ClockHour]'
GO
IF OBJECT_ID(N'[dbo].[USP_AT_Step06_InsertAttendance_Class_ClockHour]', 'P') IS NULL
EXEC sp_executesql N'
--=================================================================================================
-- USP_AT_Step06_InsertAttendance_Class_ClockHour
--=================================================================================================
CREATE PROCEDURE [dbo].[USP_AT_Step06_InsertAttendance_Class_ClockHour]
    (
        @StuEnrollIdList AS VARCHAR(MAX) = NULL
    )
AS --
    -- Step 6  --  InsertAttendance_Class_ClockHour  -- UnitTypeDescrip =  ''Clock Hours'' 
    --         --  TrackSapAttendance = ''byclass''
    BEGIN -- Step 6  --  InsertAttendance_Class_ClockHour
        DECLARE @StuEnrollId UNIQUEIDENTIFIER
               ,@MeetDate DATETIME
               ,@WeekDay VARCHAR(15)
               ,@StartDate DATETIME
               ,@EndDate DATETIME
               ,@PeriodDescrip VARCHAR(50)
               ,@Actual DECIMAL(18, 2)
               ,@Excused DECIMAL(18, 2)
               ,@ClsSectionId UNIQUEIDENTIFIER
               ,@Absent DECIMAL(18, 2)
               ,@ScheduledMinutes DECIMAL(18, 2)
               ,@TardyMinutes DECIMAL(18, 2)
               ,@tardy DECIMAL(18, 2)
               ,@tracktardies INT
               ,@tardiesMakingAbsence INT
               ,@PrgVerId UNIQUEIDENTIFIER
               ,@rownumber INT
               ,@ActualRunningPresentHours DECIMAL(18, 2)
               ,@ActualRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningTardyHours DECIMAL(18, 2)
               ,@ActualRunningMakeupHours DECIMAL(18, 2)
               ,@PrevStuEnrollId UNIQUEIDENTIFIER
               ,@intTardyBreakPoint INT
               ,@AdjustedRunningPresentHours DECIMAL(18, 2)
               ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningScheduledDays DECIMAL(18, 2)
               ,@ActualRunningExcusedDays INT
               ,@AdjustedPresentDaysComputed DECIMAL(18, 2)
               ,@PeriodDescrip1 VARCHAR(500)
               ,@MakeupHours DECIMAL(18, 2);

        DECLARE GetAttendance_Cursor CURSOR FOR
            SELECT   *
                    ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
            FROM     (
                     SELECT     DISTINCT t1.StuEnrollId
                                        ,t1.ClsSectionId
                                        ,(
                                         SELECT Descrip
                                         FROM   arReqs A1
                                               ,dbo.arClassSections A2
                                         WHERE  A1.ReqId = A2.ReqId
                                                AND A2.ClsSectionId = t1.ClsSectionId
                                         ) + ''  '' + t5.PeriodDescrip AS PeriodDescrip1
                                        ,t1.MeetDate
                                        ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                        ,t4.StartDate
                                        ,t4.EndDate
                                        ,t5.PeriodDescrip
                                        ,CASE WHEN (
                                                   t1.Actual >= 0
                                                   AND t1.Actual <> 9999
                                                   ) THEN t1.Actual / CAST(60 AS FLOAT)
                                              ELSE t1.Actual
                                         END AS Actual
                                        ,t1.Excused
                                        ,CASE WHEN (
                                                   t1.Actual >= 0
                                                   AND t1.Actual <> 9999
                                                   AND ( t1.Actual / CAST(60 AS FLOAT)) < t1.Scheduled / CAST(60 AS FLOAT)
                                                   ) THEN (( t1.Scheduled / CAST(60 AS FLOAT)) - ( t1.Actual / CAST(60 AS FLOAT)))
                                              ELSE 0
                                         END AS Absent
                                        ,CASE WHEN t1.Scheduled > 0 THEN ( t1.Scheduled / CAST(60 AS FLOAT))
                                              ELSE 0
                                         END AS ScheduledMinutes
                                        ,NULL AS TardyMinutes
                                        ,CASE WHEN t1.Tardy = 1 THEN 1
                                              ELSE 0
                                         END AS Tardy
                                        ,t3.TrackTardies
                                        ,t3.TardiesMakingAbsence
                                        ,t3.PrgVerId
                     FROM       atClsSectAttendance t1
                     INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                     INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                     INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                     INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                        AND (
                                                            CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                            AND DATEDIFF(DAY, t1.MeetDate, t4.EndDate) >= 0
                                                            )
                                                        AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8522 line added
                     INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                     INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                     INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                     INNER JOIN syPeriodsWorkDays PWD ON t5.PeriodId = PWD.PeriodId
                     INNER JOIN plWorkDays WD ON WD.WorkDaysId = PWD.WorkDayId
                     --inner join Inserted t20 on t20.ClsSectAttId = t1.ClsSectAttId --Inserted Table
                     INNER JOIN dbo.arClassSections t8 ON t8.ClsSectionId = t1.ClsSectionId
                     INNER JOIN dbo.arReqs t9 ON t9.ReqId = t8.ReqId
                     INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t9.UnitTypeId
                     WHERE --t2.StuEnrollId=''525B5DB6-6EF8-47AE-A5C3-8DAFC71FB471'' and
                                (
                                --t9.UnitTypeId IN ( ''B937C92E-FD7A-455E-A731-527A9918C734'' )  -- AAUT1.UnitTypeDescrip IN ( ''Clock Hours'' )
                                --OR t3.UnitTypeId IN ( ''B937C92E-FD7A-455E-A731-527A9918C734'' )  -- AAUT2.UnitTypeDescrip IN ( ''Clock Hours'' )
                                AAUT1.UnitTypeDescrip IN ( ''Clock Hours'' )
                                OR AAUT2.UnitTypeDescrip IN ( ''Clock Hours'' )
                                ) -- Clock Hours
                                AND t1.Actual <> 9999.00
                                AND SUBSTRING(DATENAME(dw, t1.MeetDate), 1, 3) = SUBSTRING(WD.WorkDaysDescrip, 1, 3)
                                AND (
                                    @StuEnrollIdList IS NULL
                                    OR t2.StuEnrollId IN (
                                                         SELECT Val
                                                         FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                         )
                                    )
                     -- Some times Makeup days don''t fall inside the schedule
                     -- ex: there may be a schedule for T-Fri and school may mark attendance for saturday
                     -- the following query will bring whatever was left out in above query
                     UNION
                     SELECT     DISTINCT
                         --t1.ClsSectAttId,
                                t1.StuEnrollId
                               ,t1.ClsSectionId
                               ,(
                                SELECT Descrip
                                FROM   arReqs A1
                                      ,dbo.arClassSections A2
                                WHERE  A1.ReqId = A2.ReqId
                                       AND A2.ClsSectionId = t1.ClsSectionId
                                ) + ''  '' + t5.PeriodDescrip AS PeriodDescrip1
                               ,t1.MeetDate
                               ,DATENAME(dw, t1.MeetDate) AS WeekDay
                               ,t4.StartDate
                               ,t4.EndDate
                               ,t5.PeriodDescrip
                               ,CASE WHEN (
                                          t1.Actual >= 0
                                          AND t1.Actual <> 9999
                                          ) THEN t1.Actual / CAST(60 AS FLOAT)
                                     ELSE t1.Actual
                                END AS Actual
                               ,t1.Excused
                               ,CASE WHEN (
                                          t1.Actual >= 0
                                          AND t1.Actual <> 9999
                                          AND ( t1.Actual / CAST(60 AS FLOAT)) < t1.Scheduled / CAST(60 AS FLOAT)
                                          ) THEN (( t1.Scheduled / CAST(60 AS FLOAT)) - ( t1.Actual / CAST(60 AS FLOAT)))
                                     ELSE 0
                                END AS Absent
                               ,CASE WHEN t1.Scheduled > 0 THEN ( t1.Scheduled / CAST(60 AS FLOAT))
                                     ELSE 0
                                END AS ScheduledMinutes
                               ,NULL AS TardyMinutes
                               ,CASE WHEN t1.Tardy = 1 THEN 1
                                     ELSE 0
                                END AS Tardy
                               ,t3.TrackTardies
                               ,t3.TardiesMakingAbsence
                               ,t3.PrgVerId
                     FROM       atClsSectAttendance t1
                     INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                     INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                     INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                     INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                        AND (
                                                            CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                            AND DATEDIFF(DAY, t1.MeetDate, t4.EndDate) >= 1
                                                            )
                                                        AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8522 line added
                     INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                     INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                     INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                     INNER JOIN syPeriodsWorkDays PWD ON t5.PeriodId = PWD.PeriodId
                     INNER JOIN plWorkDays WD ON WD.WorkDaysId = PWD.WorkDayId
                     --inner join Inserted t20 on t20.ClsSectAttId = t1.ClsSectAttId --Inserted Table
                     INNER JOIN dbo.arClassSections t8 ON t8.ClsSectionId = t1.ClsSectionId
                     INNER JOIN dbo.arReqs t9 ON t9.ReqId = t8.ReqId
                     INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t9.UnitTypeId
                     WHERE --t2.StuEnrollId=''525B5DB6-6EF8-47AE-A5C3-8DAFC71FB471'' and
                                (
                                -- t9.UnitTypeId IN ( ''B937C92E-FD7A-455E-A731-527A9918C734'' )  -- Clock Hours
                                -- OR t3.UnitTypeId IN ( ''B937C92E-FD7A-455E-A731-527A9918C734'' )  --Clock Hours
                                AAUT1.UnitTypeDescrip IN ( ''Clock Hours'' )
                                OR AAUT2.UnitTypeDescrip IN ( ''Clock Hours'' )
                                ) -- Clock Hours
                                AND t1.Actual <> 9999.00
                                AND (
                                    @StuEnrollIdList IS NULL
                                    OR t2.StuEnrollId IN (
                                                         SELECT Val
                                                         FROM   MultipleValuesForReportParameters(@StuEnrollIdList, '','', 1)
                                                         )
                                    )
                                AND t1.MeetDate NOT IN (
                                                       SELECT     t1.MeetDate
                                                       FROM       atClsSectAttendance t1
                                                       INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                                       INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                                       INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                                       INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                                          AND (
                                                                                              CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                                              AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                                              )
                                                       INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                                       INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                                       INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                                       INNER JOIN syPeriodsWorkDays PWD ON t5.PeriodId = PWD.PeriodId
                                                       INNER JOIN plWorkDays WD ON WD.WorkDaysId = PWD.WorkDayId
                                                       WHERE --t2.StuEnrollId=''525B5DB6-6EF8-47AE-A5C3-8DAFC71FB471''   and
                                                                  (
                                                                  --t9.UnitTypeId IN ( ''B937C92E-FD7A-455E-A731-527A9918C734'' ) --  Clock Hours
                                                                  --OR t3.UnitTypeId IN ( ''B937C92E-FD7A-455E-A731-527A9918C734'' )  --  Clock Hours
                                                                  AAUT1.UnitTypeDescrip IN ( ''Clock Hours'' )
                                                                  OR AAUT2.UnitTypeDescrip IN ( ''Clock Hours'' )
                                                                  ) -- Clock Hours
                                                                  AND t1.Actual <> 9999.00
                                                                  AND t1.ClsSectionId = t8.ClsSectionId
                                                                  AND SUBSTRING(DATENAME(dw, t1.MeetDate), 1, 3) = SUBSTRING(WD.WorkDaysDescrip, 1, 3)
                                                       )
                     ) dt
            ORDER BY StuEnrollId
                    ,MeetDate;


        OPEN GetAttendance_Cursor;
        FETCH NEXT FROM GetAttendance_Cursor
        INTO @StuEnrollId
            ,@ClsSectionId
            ,@PeriodDescrip1
            ,@MeetDate
            ,@WeekDay
            ,@StartDate
            ,@EndDate
            ,@PeriodDescrip
            ,@Actual
            ,@Excused
            ,@Absent
            ,@ScheduledMinutes
            ,@TardyMinutes
            ,@tardy
            ,@tracktardies
            ,@tardiesMakingAbsence
            ,@PrgVerId
            ,@rownumber;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningPresentHours = 0;
        SET @ActualRunningAbsentHours = 0;
        SET @ActualRunningTardyHours = 0;
        SET @ActualRunningMakeupHours = 0;
        SET @intTardyBreakPoint = 0;
        SET @AdjustedRunningPresentHours = 0;
        SET @AdjustedRunningAbsentHours = 0;
        SET @ActualRunningScheduledDays = 0;
        SET @MakeupHours = 0;
        SET @ActualRunningExcusedDays = 0;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                IF @PrevStuEnrollId <> @StuEnrollId
                    BEGIN
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        SET @MakeupHours = 0;
                        SET @ActualRunningExcusedDays = 0;
                    END;

                -- Calculate : Actual Running Scheduled Days
                SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;

                -- Calculate: Actual and Adjusted Running Present Hours
                IF @Actual <> 9999
                    BEGIN
                        IF (
                           @Actual > 0
                           AND @Actual > @ScheduledMinutes
                           ) -- Makeup Hours
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual - ( @Actual - @ScheduledMinutes ); -- Subtract Makeup Hours from Actual
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual - ( @Actual - @ScheduledMinutes ); -- Subtract Makeup Hours from Actual
                            END;
                        ELSE
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                            END;
                    END;

                -- Calculate: Adjusted Running Absent Hours
                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                SET @TardyMinutes = ( @ScheduledMinutes - @Actual );
                IF ( @Excused = 1 )
                    BEGIN
                        SET @ActualRunningExcusedDays = @ActualRunningExcusedDays + 1;
                    END;

                IF (
                   @Actual > 0
                   AND @Actual < @ScheduledMinutes
                   AND @tardy = 1
                   )
                    BEGIN
                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                    END;

                IF (
                   @Actual > 0
                   AND @Actual > @ScheduledMinutes
                   )
                    BEGIN
                        SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                    END;
                IF (
                   @tracktardies = 1
                   AND @TardyMinutes > 0
                   AND @tardy = 1
                   )
                    BEGIN
                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                    END;
                --IF (@tracktardies = 1 AND @intTardyBreakPoint = @tardiesMakingAbsence) 
                --    BEGIN
                --  SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual
                --        SET @AdjustedRunningAbsentHours = (@AdjustedRunningAbsentHours - @Absent) + @ScheduledMinutes --@TardyMinutes
                --        SET @ActualRunningTardyHours = @ActualRunningTardyHours - (@ScheduledMinutes - @Actual)
                --        SET @intTardyBreakPoint = 0
                --    END


                -- if (@tracktardies=1)
                --  begin
                --	set @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours --+IsNULL(@ActualRunningTardyHours,0)
                --  end
                -- else
                BEGIN
                    SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                END;



                DELETE FROM syStudentAttendanceSummary
                WHERE StuEnrollId = @StuEnrollId
                      AND ClsSectionId = @ClsSectionId
                      AND StudentAttendedDate = @MeetDate;

                INSERT INTO syStudentAttendanceSummary (
                                                       StuEnrollId
                                                      ,ClsSectionId
                                                      ,StudentAttendedDate
                                                      ,ScheduledDays
                                                      ,ActualDays
                                                      ,ActualRunningScheduledDays
                                                      ,ActualRunningPresentDays
                                                      ,ActualRunningAbsentDays
                                                      ,ActualRunningMakeupDays
                                                      ,ActualRunningTardyDays
                                                      ,AdjustedPresentDays
                                                      ,AdjustedAbsentDays
                                                      ,AttendanceTrackType
                                                      ,ModUser
                                                      ,ModDate
                                                      ,IsExcused
                                                      ,ActualRunningExcusedDays
                                                      ,IsTardy
                                                       )
                VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                        ,@ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours, @AdjustedPresentDaysComputed
                        ,@AdjustedRunningAbsentHours, ''Post Attendance by Class Clock'', ''sa'', GETDATE(), @Excused, @ActualRunningExcusedDays, @tardy );

                UPDATE syStudentAttendanceSummary
                SET    tardiesmakingabsence = @tardiesMakingAbsence
                WHERE  StuEnrollId = @StuEnrollId;
                SET @PrevStuEnrollId = @StuEnrollId;

                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@ClsSectionId
                    ,@PeriodDescrip1
                    ,@MeetDate
                    ,@WeekDay
                    ,@StartDate
                    ,@EndDate
                    ,@PeriodDescrip
                    ,@Actual
                    ,@Excused
                    ,@Absent
                    ,@ScheduledMinutes
                    ,@TardyMinutes
                    ,@tardy
                    ,@tracktardies
                    ,@tardiesMakingAbsence
                    ,@PrgVerId
                    ,@rownumber;
            END;
        CLOSE GetAttendance_Cursor;
        DEALLOCATE GetAttendance_Cursor;

        DECLARE @MyTardyTable TABLE
            (
                ClsSectionId UNIQUEIDENTIFIER
               ,TardiesMakingAbsence INT
               ,AbsentHours DECIMAL(18, 2)
            );

        INSERT INTO @MyTardyTable
                    SELECT     ClsSectionId
                              ,PV.TardiesMakingAbsence
                              ,CASE WHEN ( COUNT(*) >= PV.TardiesMakingAbsence ) THEN (( COUNT(*) / PV.TardiesMakingAbsence ) * SAS.ScheduledDays )
                                    ELSE 0
                               END AS AbsentHours
                    --Count(*) as NumberofTimesTardy 
                    FROM       syStudentAttendanceSummary SAS
                    INNER JOIN arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    WHERE      SE.StuEnrollId = @StuEnrollId
                               AND SAS.IsTardy = 1
                               AND PV.TrackTardies = 1
                    GROUP BY   ClsSectionId
                              ,PV.TardiesMakingAbsence
                              ,SAS.ScheduledDays;

        --Drop table @MyTardyTable
        DECLARE @TotalTardyAbsentDays DECIMAL(18, 2);
        SET @TotalTardyAbsentDays = (
                                    SELECT ISNULL(SUM(AbsentHours), 0)
                                    FROM   @MyTardyTable
                                    );

        --Print @TotalTardyAbsentDays

        UPDATE syStudentAttendanceSummary
        SET    AdjustedPresentDays = AdjustedPresentDays - @TotalTardyAbsentDays
              ,AdjustedAbsentDays = AdjustedAbsentDays + @TotalTardyAbsentDays
        WHERE  StuEnrollId = @StuEnrollId
               AND StudentAttendedDate = (
                                         SELECT   TOP 1 StudentAttendedDate
                                         FROM     syStudentAttendanceSummary
                                         WHERE    StuEnrollId = @StuEnrollId
                                         ORDER BY StudentAttendedDate DESC
                                         );

    END;
--=================================================================================================
-- END  --  USP_AT_Step06_InsertAttendance_Class_ClockHour
--=================================================================================================
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[arStuEnrollments_Integration_Tracking] on [dbo].[arStuEnrollments]'
GO
IF OBJECT_ID(N'[dbo].[arStuEnrollments_Integration_Tracking]', 'TR') IS NULL
EXEC sp_executesql N'--BEGIN	
--CREATE TABLE dbo.IntegrationEnrollmentTracking
--(
-- EnrollmentTrackingId UNIQUEIDENTIFIER CONSTRAINT [PK_IntegrationEnrollmentTracking_EnrollmentTrackingId] PRIMARY KEY CLUSTERED 
-- CONSTRAINT [DF_IntegrationEnrollmentTracking_EnrollmentTrackingId] DEFAULT NEWID(),
-- StuEnrollId UNIQUEIDENTIFIER CONSTRAINT[FK_IntegrationEnrollmentTracking_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY REFERENCES dbo.arStuEnrollments NOT NULL,
--  EnrollmentStatus VARCHAR(100),
-- ModDate DATETIME NOT NULL ,
-- ModUser NVARCHAR(100),
-- Processed BIT CONSTRAINT [DF_IntegrationEnrollmentTracking_Processed] DEFAULT 0 NOT NULL 	

--)
--END
--DROP TABLE dbo.IntegrationEnrollmentTracking
CREATE TRIGGER [dbo].[arStuEnrollments_Integration_Tracking]
ON [dbo].[arStuEnrollments]
AFTER UPDATE
AS
DECLARE @INS INT
       ,@DEL INT;

SELECT @INS = COUNT(*)
FROM   INSERTED;
SELECT @DEL = COUNT(*)
FROM   DELETED;

IF @INS > 0
   AND @DEL > 0
    BEGIN


        IF UPDATE(StatusCodeId)
            BEGIN
                IF EXISTS (
                          SELECT     1
                          FROM       INSERTED i
                          INNER JOIN DELETED d ON d.StuEnrollId = i.StuEnrollId
                          WHERE      i.StatusCodeId <> d.StatusCodeId
                          )
                    BEGIN
                        IF EXISTS (
                                  SELECT     1
                                  FROM       dbo.IntegrationEnrollmentTracking IET
                                  INNER JOIN inserted i ON i.StuEnrollId = IET.StuEnrollId
                                  )
                            BEGIN


                                UPDATE     IET
                                SET        EnrollmentStatus = ss.SysStatusDescrip
                                          ,ModDate = i.ModDate
                                          ,ModUser = i.ModUser
                                          ,Processed = 0
                                FROM       dbo.IntegrationEnrollmentTracking IET
                                INNER JOIN INSERTED i ON i.StuEnrollId = IET.StuEnrollId
                                INNER JOIN dbo.arStuEnrollments se ON i.StuEnrollId = se.StuEnrollId
                                INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                INNER JOIN dbo.sySysStatus ss ON ss.SysStatusId = sc.SysStatusId
                                WHERE      IET.StuEnrollId = i.StuEnrollId;
                            END;

                        ELSE
                            BEGIN

                                INSERT INTO dbo.IntegrationEnrollmentTracking (
                                                                               EnrollmentTrackingId
                                                                              ,StuEnrollId
                                                                              ,EnrollmentStatus
                                                                              ,ModDate
                                                                              ,ModUser
                                                                              ,Processed
                                                                               )
                                            SELECT     NEWID()
                                                      ,i.StuEnrollId
                                                      ,ss.SysStatusDescrip
                                                      ,i.ModDate
                                                      ,i.ModUser
                                                      ,0
                                            FROM       INSERTED i
                                            INNER JOIN dbo.arStuEnrollments se ON i.StuEnrollId = se.StuEnrollId
                                            INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                            INNER JOIN dbo.sySysStatus ss ON ss.SysStatusId = sc.SysStatusId;

                            END;


                    END;

            END;



    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[saDeferredRevenues_Audit_Delete] on [dbo].[saDeferredRevenues]'
GO
IF OBJECT_ID(N'[dbo].[saDeferredRevenues_Audit_Delete]', 'TR') IS NULL
EXEC sp_executesql N'
CREATE TRIGGER [dbo].[saDeferredRevenues_Audit_Delete] ON [dbo].[saDeferredRevenues]
    FOR DELETE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID();
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Deleted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Deleted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Deleted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,''saDeferredRevenues'',''D'',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,''Amount''
                               ,CONVERT(VARCHAR(8000),Old.Amount,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,''TransactionId''
                               ,CONVERT(VARCHAR(8000),Old.TransactionId,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,''DefRevenueDate''
                               ,CONVERT(VARCHAR(8000),Old.DefRevenueDate,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,''Source''
                               ,CONVERT(VARCHAR(8000),Old.Source,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,''Type''
                               ,CONVERT(VARCHAR(8000),Old.Type,121)
                        FROM    Deleted Old; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,OldValue
                        )
                        SELECT  @AuditHistId
                               ,Old.DefRevenueId
                               ,''IsPosted''
                               ,CONVERT(VARCHAR(8000),Old.IsPosted,121)
                        FROM    Deleted Old; 
            END; 
        END; 


    SET NOCOUNT OFF; 
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[saDeferredRevenues_Audit_Insert] on [dbo].[saDeferredRevenues]'
GO
IF OBJECT_ID(N'[dbo].[saDeferredRevenues_Audit_Insert]', 'TR') IS NULL
EXEC sp_executesql N'
CREATE TRIGGER [dbo].[saDeferredRevenues_Audit_Insert] ON [dbo].[saDeferredRevenues]
    FOR INSERT
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50); 
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,''saDeferredRevenues'',''I'',@EventRows,@EventDate,@UserName; 
            BEGIN 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,''Amount''
                               ,CONVERT(VARCHAR(8000),New.Amount,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,''TransactionId''
                               ,CONVERT(VARCHAR(8000),New.TransactionId,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,''DefRevenueDate''
                               ,CONVERT(VARCHAR(8000),New.DefRevenueDate,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,''Source''
                               ,CONVERT(VARCHAR(8000),New.Source,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,''Type''
                               ,CONVERT(VARCHAR(8000),New.Type,121)
                        FROM    Inserted New; 
                INSERT  INTO syAuditHistDetail
                        (
                         AuditHistId
                        ,RowId
                        ,ColumnName
                        ,NewValue
                        )
                        SELECT  @AuditHistId
                               ,New.DefRevenueId
                               ,''IsPosted''
                               ,CONVERT(VARCHAR(8000),New.IsPosted,121)
                        FROM    Inserted New; 
            END; 
        END; 


    SET NOCOUNT OFF; 
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[saDeferredRevenues_Audit_Update] on [dbo].[saDeferredRevenues]'
GO
IF OBJECT_ID(N'[dbo].[saDeferredRevenues_Audit_Update]', 'TR') IS NULL
EXEC sp_executesql N'
CREATE TRIGGER [dbo].[saDeferredRevenues_Audit_Update] ON [dbo].[saDeferredRevenues]
    FOR UPDATE
AS
    SET NOCOUNT ON; 

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,''saDeferredRevenues'',''U'',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(Amount)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,''Amount''
                                   ,CONVERT(VARCHAR(8000),Old.Amount,121)
                                   ,CONVERT(VARCHAR(8000),New.Amount,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.Amount <> New.Amount
                                    OR (
                                         Old.Amount IS NULL
                                         AND New.Amount IS NOT NULL
                                       )
                                    OR (
                                         New.Amount IS NULL
                                         AND Old.Amount IS NOT NULL
                                       ); 
                IF UPDATE(TransactionId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,''TransactionId''
                                   ,CONVERT(VARCHAR(8000),Old.TransactionId,121)
                                   ,CONVERT(VARCHAR(8000),New.TransactionId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.TransactionId <> New.TransactionId
                                    OR (
                                         Old.TransactionId IS NULL
                                         AND New.TransactionId IS NOT NULL
                                       )
                                    OR (
                                         New.TransactionId IS NULL
                                         AND Old.TransactionId IS NOT NULL
                                       ); 
                IF UPDATE(DefRevenueDate)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,''DefRevenueDate''
                                   ,CONVERT(VARCHAR(8000),Old.DefRevenueDate,121)
                                   ,CONVERT(VARCHAR(8000),New.DefRevenueDate,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.DefRevenueDate <> New.DefRevenueDate
                                    OR (
                                         Old.DefRevenueDate IS NULL
                                         AND New.DefRevenueDate IS NOT NULL
                                       )
                                    OR (
                                         New.DefRevenueDate IS NULL
                                         AND Old.DefRevenueDate IS NOT NULL
                                       ); 
                IF UPDATE(Source)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,''Source''
                                   ,CONVERT(VARCHAR(8000),Old.Source,121)
                                   ,CONVERT(VARCHAR(8000),New.Source,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.Source <> New.Source
                                    OR (
                                         Old.Source IS NULL
                                         AND New.Source IS NOT NULL
                                       )
                                    OR (
                                         New.Source IS NULL
                                         AND Old.Source IS NOT NULL
                                       ); 
                IF UPDATE(Type)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,''Type''
                                   ,CONVERT(VARCHAR(8000),Old.Type,121)
                                   ,CONVERT(VARCHAR(8000),New.Type,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.Type <> New.Type
                                    OR (
                                         Old.Type IS NULL
                                         AND New.Type IS NOT NULL
                                       )
                                    OR (
                                         New.Type IS NULL
                                         AND Old.Type IS NOT NULL
                                       ); 
                IF UPDATE(IsPosted)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.DefRevenueId
                                   ,''IsPosted''
                                   ,CONVERT(VARCHAR(8000),Old.IsPosted,121)
                                   ,CONVERT(VARCHAR(8000),New.IsPosted,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.DefRevenueId = New.DefRevenueId
                            WHERE   Old.IsPosted <> New.IsPosted
                                    OR (
                                         Old.IsPosted IS NULL
                                         AND New.IsPosted IS NOT NULL
                                       )
                                    OR (
                                         New.IsPosted IS NULL
                                         AND Old.IsPosted IS NOT NULL
                                       ); 
            END; 
        END; 


    SET NOCOUNT OFF; 
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[AdLeadEmail]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AdLeadEmail_syEmailType_EmailTypeId_EmailTypeId]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[AdLeadEmail]', 'U'))
ALTER TABLE [dbo].[AdLeadEmail] ADD CONSTRAINT [FK_AdLeadEmail_syEmailType_EmailTypeId_EmailTypeId] FOREIGN KEY ([EMailTypeId]) REFERENCES [dbo].[syEmailType] ([EMailTypeId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[IntegrationEnrollmentTracking]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_IntegrationEnrollmentTracking_arStuEnrollments_StuEnrollId_StuEnrollId]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[IntegrationEnrollmentTracking]', 'U'))
ALTER TABLE [dbo].[IntegrationEnrollmentTracking] ADD CONSTRAINT [FK_IntegrationEnrollmentTracking_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY ([StuEnrollId]) REFERENCES [dbo].[arStuEnrollments] ([StuEnrollId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Adding foreign keys to [dbo].[adLeads]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_adLeads_adLeadGroups_LeadGrpId_LeadGrpId]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[adLeads]', 'U'))
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_adLeadGroups_LeadGrpId_LeadGrpId] FOREIGN KEY ([LeadgrpId]) REFERENCES [dbo].[adLeadGroups] ([LeadGrpId])
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_adLeads_SyInstitutions_HighSchoolId_HSId]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[adLeads]', 'U'))
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_adLeads_SyInstitutions_HighSchoolId_HSId] FOREIGN KEY ([HighSchoolId]) REFERENCES [dbo].[syInstitutions] ([HSId])
GO
IF NOT EXISTS (SELECT 1 FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_AdLeads_syStatuses_StudentStatusId_StatusId]', 'F') AND parent_object_id = OBJECT_ID(N'[dbo].[adLeads]', 'U'))
ALTER TABLE [dbo].[adLeads] ADD CONSTRAINT [FK_AdLeads_syStatuses_StudentStatusId_StatusId] FOREIGN KEY ([StudentStatusId]) REFERENCES [dbo].[syStatuses] ([StatusId])
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END