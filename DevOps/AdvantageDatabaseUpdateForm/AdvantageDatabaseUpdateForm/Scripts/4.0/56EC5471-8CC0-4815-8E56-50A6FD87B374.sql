/*
Run this script on:

        CORDERO-LT\SQLDEV2016.WesternTruck    -  This database will be modified

to synchronize it with a database with the schema represented by:

        \\cordero-lt\c$\_Git\Advantage\Databases\Advantage\Source\

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.0.33.3389 from Red Gate Software Ltd at 5/9/2019 5:59:20 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[arFASAPChkResults]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[arFASAPChkResults] ALTER COLUMN [HrsAttended] [decimal] (19, 2) NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[arFASAPChkResults] ALTER COLUMN [HrsEarned] [decimal] (19, 2) NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[arSAPChkResults]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[arSAPChkResults] ALTER COLUMN [HrsAttended] [decimal] (19, 2) NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[arSAPChkResults] ALTER COLUMN [HrsEarned] [decimal] (19, 2) NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[arStuEnrollments]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[arStuEnrollments]', N'LicensureLastPartWrittenOn') IS NULL
ALTER TABLE [dbo].[arStuEnrollments] ADD[LicensureLastPartWrittenOn] [datetime] NULL
IF COL_LENGTH(N'[dbo].[arStuEnrollments]', N'LicensureWrittenAllParts') IS NULL
ALTER TABLE [dbo].[arStuEnrollments] ADD[LicensureWrittenAllParts] [bit] NOT NULL CONSTRAINT [DF_arStuEnrollments_LicensureWrittenAllParts] DEFAULT ((0))
IF COL_LENGTH(N'[dbo].[arStuEnrollments]', N'LicensurePassedAllParts') IS NULL
ALTER TABLE [dbo].[arStuEnrollments] ADD[LicensurePassedAllParts] [bit] NOT NULL CONSTRAINT [DF_arStuEnrollments_LicensurePassedAllParts] DEFAULT ((0))
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[PlStudentsPlaced]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[PlStudentsPlaced] ALTER COLUMN [PlacedDate] [datetime] NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudent]'
GO
IF OBJECT_ID(N'[dbo].[arStudent]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[usp_GetClassroomWorkServices]'
GO
IF OBJECT_ID(N'[dbo].[usp_GetClassroomWorkServices]', 'P') IS NOT NULL
EXEC sp_executesql N'
/* 
PURPOSE: 
Get component summaries for a selected student enrollment, class section and component type

CREATED: 


MODIFIED:
10/25/2013 WP	US4547 Refactor Post Services by Student process

ResourceId
Homework = 499
LabWork = 500
Exam = 501
Final = 502
LabHours = 503
Externship = 544

*/

ALTER PROCEDURE [dbo].[usp_GetClassroomWorkServices]
    (
        @StuEnrollId UNIQUEIDENTIFIER
       ,@ClsSectionId UNIQUEIDENTIFIER
       ,@GradeBookComponentType INT
    )
AS
    SET NOCOUNT ON;


    DECLARE @ReqId AS UNIQUEIDENTIFIER;
    SELECT @ReqId = ReqId
    FROM   arClassSections
    WHERE  ClsSectionId = @ClsSectionId;

    SELECT *
    INTO   #PostedServices
    FROM   arGrdBkResults
    WHERE  StuEnrollId = @StuEnrollId
           AND ClsSectionId = @ClsSectionId
           AND InstrGrdBkWgtDetailId IN (
                                        SELECT InstrGrdBkWgtDetailId
                                        FROM   arGrdBkWgtDetails posted_gbwd
                                        JOIN   arGrdComponentTypes posted_gct ON posted_gct.GrdComponentTypeId = posted_gbwd.GrdComponentTypeId
                                        WHERE  posted_gct.SysComponentTypeId = @GradeBookComponentType
                                        );

--SELECT * FROM #PostedServices


SELECT    gct.Descrip
             ,ISNULL(gbwd.Number, 0) AS Required
             ,CASE WHEN @GradeBookComponentType = 503 THEN 
			  ISNULL((
                               SELECT TOP (1) Score
                               FROM   #PostedServices
                               WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                               )
                              ,0
                              )
                   ELSE ISNULL((
                               SELECT COUNT(*)
                               FROM   #PostedServices
                               WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                               )
                              ,0
                              )
              END AS Completed
             ,CASE WHEN @GradeBookComponentType = 503 THEN CASE WHEN ( ISNULL(gbwd.Number, 0) - ISNULL(( ps.Score ), 0)) <= 0 THEN 0
                                                                ELSE ( ISNULL(gbwd.Number, 0) - ISNULL(( ps.Score ), 0))
                                                           END
                   ELSE CASE WHEN ( ISNULL(gbwd.Number, 0) - ISNULL((
                                                                    SELECT COUNT(*)
                                                                    FROM   #PostedServices
                                                                    WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                                                    )
                                                                   ,0
                                                                   )
                                  ) <= 0 THEN 0
                             ELSE ( ISNULL(gbwd.Number, 0) - ISNULL((
                                                                    SELECT COUNT(*)
                                                                    FROM   #PostedServices
                                                                    WHERE  InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
                                                                    )
                                                                   ,0
                                                                   )
                                  )
                        END
              END AS Remaining
             ,CASE WHEN @GradeBookComponentType = 503 THEN NULL
                   ELSE AVG(ps.Score)
              END AS Average
             ,se.EnrollDate
             ,gbwd.InstrGrdBkWgtDetailId
             ,@ClsSectionId AS ClsSectionId
    FROM      arGrdBkWeights gbw
    LEFT JOIN arGrdBkWgtDetails gbwd ON gbwd.InstrGrdBkWgtId = gbw.InstrGrdBkWgtId
    LEFT JOIN arGrdComponentTypes gct ON gct.GrdComponentTypeId = gbwd.GrdComponentTypeId
    LEFT JOIN arClassSections cs ON cs.ReqId = gbw.ReqId
    LEFT JOIN arResults ar ON ar.TestId = cs.ClsSectionId
    LEFT JOIN arStuEnrollments se ON ar.StuEnrollId = se.StuEnrollId
    LEFT JOIN #PostedServices ps ON ps.InstrGrdBkWgtDetailId = gbwd.InstrGrdBkWgtDetailId
    WHERE     gct.SysComponentTypeId = @GradeBookComponentType
              AND gbw.ReqId = @ReqId
              AND ar.TestId = @ClsSectionId
              AND ar.StuEnrollId = @StuEnrollId
    GROUP BY  gct.Descrip
             ,gbwd.Number
             ,se.EnrollDate
             ,gbwd.InstrGrdBkWgtDetailId
			 ,ps.Score;



    DROP TABLE #PostedServices;





'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[USP_GPACalculator]'
GO
IF OBJECT_ID(N'[dbo].[USP_GPACalculator]', 'P') IS NOT NULL
EXEC sp_executesql N'-- =============================================
-- Author:		FAME Inc.
-- Create date: 11/1/2018
-- Description:	Calculated Student GPA Based on a set of parameters - Numeric ( Weighted & Unweighted currently implemented)
--Referenced in :
--[Usp_PR_Sub2_Enrollment_Summary]
--[Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId]
--[USP_TitleIV_QualitativeAndQuantitative]
--CreditSummaryDB.vb
--Core Web API - EnrollmentService.GetEnrollmentProgramSummary
-- =============================================
ALTER PROCEDURE [dbo].[USP_GPACalculator]
    (
        @EnrollmentId VARCHAR(1000) = NULL
       ,@BeginDate DATETIME = NULL
       ,@EndDate DATETIME = NULL
       ,@ClassId UNIQUEIDENTIFIER = NULL
       ,@StudentGPA AS DECIMAL(16, 2) OUTPUT
    )
AS
    BEGIN
        DECLARE @GPAResult AS DECIMAL(16, 2);
		DECLARE @UseWeightForGPA BIT = 1

        IF ( @EnrollmentId IS NULL )
            RETURN @GPAResult;

        SET @UseWeightForGPA = (
                               SELECT TOP 1 PV.DoCourseWeightOverallGPA
                               FROM   dbo.arStuEnrollments E
                               JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                               WHERE  E.StuEnrollId = @EnrollmentId
                               );

        -----------------Numeric GPA Grades Format------------------------

        SET @GPAResult = (
                         SELECT ( CASE WHEN @UseWeightForGPA = 1 THEN
                                           ROUND(( SUM(b.CourseWeight * b.WeightedCourseAverage / 100) / SUM(b.CourseWeight)) * 100, 2)
                                       ELSE AVG(b.UnweightedCourseAverage)
                                  END
                                ) AS WeightedGPA
                         FROM   (
                                SELECT   SUM(OurSingleClassGradeFactor) AS CourseFactor
                                        ,SUM(a.GradeWeight) AS GradeWeight
                                        ,SUM(a.Score) AS SumOfScores
                                        ,( SUM(OurSingleClassGradeFactor) / SUM(a.GradeWeight)) * 100 AS WeightedCourseAverage
                                        ,( AVG(a.Score)) AS UnweightedCourseAverage
                                        ,ClsSectionId
                                        ,a.CourseWeight
                                FROM     (
                                         SELECT ( c.Weight * a.Score / 100 ) AS OurSingleClassGradeFactor
                                               ,c.Weight AS GradeWeight
                                               ,a.Score
                                               ,a.ClsSectionId
                                               ,d.CourseWeight
                                               ,c.Descrip AS ClassDescrip
                                         FROM   (
                                                SELECT   StuEnrollId
                                                        ,ClsSectionId
                                                        ,a.InstrGrdBkWgtDetailId
                                                        ,AVG(Score) AS Score
                                                FROM     dbo.arGrdBkResults a
                                                JOIN     dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                                JOIN     dbo.arGrdComponentTypes f ON f.GrdComponentTypeId = c.GrdComponentTypeId
                                                WHERE    a.StuEnrollId = @EnrollmentId
                                                         --AND a.IsCompGraded = 1
                                                         AND a.Score IS NOT NULL
                                                         AND a.PostDate IS NOT NULL
                                                         AND (
                                                             @EndDate IS NULL
                                                             OR ( a.PostDate <= @EndDate )
                                                             )
                                                GROUP BY StuEnrollId
                                                        ,ClsSectionId
                                                        ,a.InstrGrdBkWgtDetailId
                                                ) a -- students grades
                                         JOIN   dbo.arClassSections b ON b.ClsSectionId = a.ClsSectionId
                                         JOIN   dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                         JOIN   dbo.arProgVerDef d ON d.ReqId = b.ReqId
                                         --ORDER BY f.Descrip, a.Score desc
                                         ) a
                                GROUP BY ClsSectionId
                                        ,a.CourseWeight

                                --ORDER BY CourseGPA DESC
                                ) b
                         );
        --END;
        SET @StudentGPA = @GPAResult;
    END;

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Usp_PR_Sub2_Enrollment_Summary]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub2_Enrollment_Summary]', 'P') IS NOT NULL
EXEC sp_executesql N'-- =========================================================================================================
-- Usp_PR_Sub2_Enrollment_Summary
-- =========================================================================================================
ALTER PROCEDURE [dbo].[Usp_PR_Sub2_Enrollment_Summary]
    @StuEnrollId VARCHAR(50)
   ,@TermId VARCHAR(4000) = NULL
   ,@TermStartDate DATETIME = NULL
   ,@TermStartDateModifier VARCHAR(10) = NULL
   ,@ShowFinanceCalculations BIT = 0
   -- by default hide cost and current balance
   ,@ShowWeeklySchedule BIT = 0
-- , @TrackSapAttendance VARCHAR(10) = NULL
-- , @displayhours VARCHAR(10) = NULL
AS
    BEGIN
        DECLARE @TrackSapAttendance VARCHAR(1000);
        DECLARE @displayHours AS VARCHAR(1000);
        DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
        DECLARE @GradesFormat AS VARCHAR(50);

        IF @TermStartDate IS NULL
           OR @TermStartDate = ''''
            BEGIN
                SET @TermStartDate = CONVERT(DATETIME, GETDATE(), 120);
            END;
        SET @StuEnrollCampusId = COALESCE((
                                          SELECT ASE.CampusId
                                          FROM   arStuEnrollments AS ASE
                                          WHERE  ASE.StuEnrollId = @StuEnrollId
                                          )
                                         ,NULL
                                         );
        SET @TrackSapAttendance = dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', @StuEnrollCampusId);
        IF ( @TrackSapAttendance IS NOT NULL )
            BEGIN
                SET @TrackSapAttendance = LOWER(LTRIM(RTRIM(@TrackSapAttendance)));
            END;
        --
        SET @displayHours = dbo.GetAppSettingValueByKeyName(''DisplayAttendanceUnitForProgressReportByClass'', @StuEnrollCampusId);
        IF ( @displayHours IS NOT NULL )
            BEGIN
                SET @displayHours = LOWER(LTRIM(RTRIM(@displayHours)));
            END;
        --
        SET @GradesFormat = dbo.GetAppSettingValueByKeyName(''GradesFormat'',@StuEnrollCampusId);
        IF ( @GradesFormat IS NOT NULL )
            BEGIN
                SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
            END;

        IF ( @TermId IS NOT NULL )
            BEGIN
                SET @TermStartDate = (
                                     SELECT TOP 1 EndDate
                                     FROM   arTerm
                                     WHERE  TermId = @TermId
                                     );
                IF @TermStartDate IS NULL
                    BEGIN
                        SET @TermStartDate = (
                                             SELECT TOP 1 StartDate
                                             FROM   arTerm
                                             WHERE  TermId = @TermId
                                             );
                    END;
                SET @TermStartDateModifier = ''<='';
            END;



        DECLARE @ReturnValue VARCHAR(100);
        SELECT     @ReturnValue = COALESCE(@ReturnValue, '''') + Descrip + '',  ''
        FROM       adLeadByLeadGroups t1
        INNER JOIN adLeadGroups t2 ON t1.LeadGrpId = t2.LeadGrpId
        WHERE      t1.StuEnrollId = @StuEnrollId;
        SET @ReturnValue = SUBSTRING(@ReturnValue, 1, LEN(@ReturnValue) - 1);


        /************  Logic to calculate GPA Starts Here *****************************/

        DECLARE @CoursesNotRepeated TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,TermId UNIQUEIDENTIFIER
               ,TermDescrip VARCHAR(100)
               ,ReqId UNIQUEIDENTIFIER
               ,ReqDescrip VARCHAR(100)
               ,ClsSectionId VARCHAR(50)
               ,CreditsEarned DECIMAL(18, 2)
               ,CreditsAttempted DECIMAL(18, 2)
               ,CurrentScore DECIMAL(18, 2)
               ,CurrentGrade VARCHAR(10)
               ,FinalScore DECIMAL(18, 2)
               ,FinalGrade VARCHAR(10)
               ,Completed BIT
               ,FinalGPA DECIMAL(18, 2)
               ,Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
               ,Count_WeightedAverage_Credits DECIMAL(18, 2)
               ,Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
               ,Count_SimpleAverage_Credits DECIMAL(18, 2)
               ,ModUser VARCHAR(50)
               ,ModDate DATETIME
               ,TermGPA_Simple DECIMAL(18, 2)
               ,TermGPA_Weighted DECIMAL(18, 2)
               ,coursecredits DECIMAL(18, 2)
               ,CumulativeGPA DECIMAL(18, 2)
               ,CumulativeGPA_Simple DECIMAL(18, 2)
               ,FACreditsEarned DECIMAL(18, 2)
               ,Average DECIMAL(18, 2)
               ,CumAverage DECIMAL(18, 2)
               ,TermStartDate DATETIME
               ,rownumber INT
            );

        INSERT INTO @CoursesNotRepeated
                    SELECT StuEnrollId
                          ,TermId
                          ,TermDescrip
                          ,ReqId
                          ,ReqDescrip
                          ,ClsSectionId
                          ,CreditsEarned
                          ,CreditsAttempted
                          ,CurrentScore
                          ,CurrentGrade
                          ,FinalScore
                          ,FinalGrade
                          ,Completed
                          ,FinalGPA
                          ,Product_WeightedAverage_Credits_GPA
                          ,Count_WeightedAverage_Credits
                          ,Product_SimpleAverage_Credits_GPA
                          ,Count_SimpleAverage_Credits
                          ,ModUser
                          ,ModDate
                          ,TermGPA_Simple
                          ,TermGPA_Weighted
                          ,coursecredits
                          ,CumulativeGPA
                          ,CumulativeGPA_Simple
                          ,FACreditsEarned
                          ,Average
                          ,CumAverage
                          ,TermStartDate
                          ,NULL AS rownumber
                    FROM   syCreditSummary
                    WHERE  StuEnrollId = @StuEnrollId
                           AND ReqId IN (
                                        SELECT ReqId
                                        FROM   (
                                               SELECT   ReqId
                                                       ,ReqDescrip
                                                       ,COUNT(*) AS counter
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                               GROUP BY ReqId
                                                       ,ReqDescrip
                                               HAVING   COUNT(*) = 1
                                               ) dt
                                        );

        DECLARE @GradeCourseRepetitionsMethod VARCHAR(50);
        SET @GradeCourseRepetitionsMethod = (
                                            SELECT     Value
                                            FROM       dbo.syConfigAppSetValues t1
                                            INNER JOIN dbo.syConfigAppSettings t2 ON t1.SettingId = t2.SettingId
                                                                                     AND t2.KeyName = ''GradeCourseRepetitionsMethod''
                                            );

        IF LOWER(@GradeCourseRepetitionsMethod) = ''latest''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT   dt2.StuEnrollId
                                    ,dt2.TermId
                                    ,dt2.TermDescrip
                                    ,dt2.ReqId
                                    ,dt2.ReqDescrip
                                    ,dt2.ClsSectionId
                                    ,dt2.CreditsEarned
                                    ,dt2.CreditsAttempted
                                    ,dt2.CurrentScore
                                    ,dt2.CurrentGrade
                                    ,dt2.FinalScore
                                    ,dt2.FinalGrade
                                    ,dt2.Completed
                                    ,dt2.FinalGPA
                                    ,dt2.Product_WeightedAverage_Credits_GPA
                                    ,dt2.Count_WeightedAverage_Credits
                                    ,dt2.Product_SimpleAverage_Credits_GPA
                                    ,dt2.Count_SimpleAverage_Credits
                                    ,dt2.ModUser
                                    ,dt2.ModDate
                                    ,dt2.TermGPA_Simple
                                    ,dt2.TermGPA_Weighted
                                    ,dt2.coursecredits
                                    ,dt2.CumulativeGPA
                                    ,dt2.CumulativeGPA_Simple
                                    ,dt2.FACreditsEarned
                                    ,dt2.Average
                                    ,dt2.CumAverage
                                    ,dt2.TermStartDate
                                    ,dt2.RowNumber
                            FROM     (
                                     SELECT StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                                ORDER BY TermStartDate DESC
                                                              ) AS RowNumber
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND (
                                                FinalScore IS NOT NULL
                                                OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN (
                                                         SELECT ReqId
                                                         FROM   (
                                                                SELECT   ReqId
                                                                        ,ReqDescrip
                                                                        ,COUNT(*) AS Counter
                                                                FROM     syCreditSummary
                                                                WHERE    StuEnrollId = @StuEnrollId
                                                                GROUP BY ReqId
                                                                        ,ReqDescrip
                                                                HAVING   COUNT(*) > 1
                                                                ) dt
                                                         )
                                     ) dt2
                            WHERE    RowNumber = 1
                            ORDER BY TermStartDate DESC;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''best''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                            FROM   (
                                   SELECT StuEnrollId
                                         ,TermId
                                         ,TermDescrip
                                         ,ReqId
                                         ,ReqDescrip
                                         ,ClsSectionId
                                         ,CreditsEarned
                                         ,CreditsAttempted
                                         ,CurrentScore
                                         ,CurrentGrade
                                         ,FinalScore
                                         ,FinalGrade
                                         ,Completed
                                         ,FinalGPA
                                         ,Product_WeightedAverage_Credits_GPA
                                         ,Count_WeightedAverage_Credits
                                         ,Product_SimpleAverage_Credits_GPA
                                         ,Count_SimpleAverage_Credits
                                         ,ModUser
                                         ,ModDate
                                         ,TermGPA_Simple
                                         ,TermGPA_Weighted
                                         ,coursecredits
                                         ,CumulativeGPA
                                         ,CumulativeGPA_Simple
                                         ,FACreditsEarned
                                         ,Average
                                         ,CumAverage
                                         ,TermStartDate
                                         ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                              ORDER BY FinalGPA DESC
                                                            ) AS RowNumber
                                   FROM   syCreditSummary
                                   WHERE  StuEnrollId = @StuEnrollId
                                          AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                              )
                                          AND ReqId IN (
                                                       SELECT ReqId
                                                       FROM   (
                                                              SELECT   ReqId
                                                                      ,ReqDescrip
                                                                      ,COUNT(*) AS Counter
                                                              FROM     syCreditSummary
                                                              WHERE    StuEnrollId = @StuEnrollId
                                                              GROUP BY ReqId
                                                                      ,ReqDescrip
                                                              HAVING   COUNT(*) > 1
                                                              ) dt
                                                       )
                                   ) dt2
                            WHERE  RowNumber = 1;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''average''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                            FROM   (
                                   SELECT StuEnrollId
                                         ,TermId
                                         ,TermDescrip
                                         ,ReqId
                                         ,ReqDescrip
                                         ,ClsSectionId
                                         ,CreditsEarned
                                         ,CreditsAttempted
                                         ,CurrentScore
                                         ,CurrentGrade
                                         ,FinalScore
                                         ,FinalGrade
                                         ,Completed
                                         ,FinalGPA
                                         ,Product_WeightedAverage_Credits_GPA
                                         ,Count_WeightedAverage_Credits
                                         ,Product_SimpleAverage_Credits_GPA
                                         ,Count_SimpleAverage_Credits
                                         ,ModUser
                                         ,ModDate
                                         ,TermGPA_Simple
                                         ,TermGPA_Weighted
                                         ,coursecredits
                                         ,CumulativeGPA
                                         ,CumulativeGPA_Simple
                                         ,FACreditsEarned
                                         ,Average
                                         ,CumAverage
                                         ,TermStartDate
                                         ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                              ORDER BY FinalGPA DESC
                                                            ) AS RowNumber
                                   FROM   syCreditSummary
                                   WHERE  StuEnrollId = @StuEnrollId
                                          AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                              )
                                          AND ReqId IN (
                                                       SELECT ReqId
                                                       FROM   (
                                                              SELECT   ReqId
                                                                      ,ReqDescrip
                                                                      ,COUNT(*) AS Counter
                                                              FROM     syCreditSummary
                                                              WHERE    StuEnrollId = @StuEnrollId
                                                              GROUP BY ReqId
                                                                      ,ReqDescrip
                                                              HAVING   COUNT(*) > 1
                                                              ) dt
                                                       )
                                   ) dt2;
            END;

        /*** Calculate Cumulative Simple GPA Starts Here ****/
        DECLARE @cumSimpleCourseCredits DECIMAL(18, 2)
               ,@cumSimple_GPA_Credits DECIMAL(18, 2)
               ,@cumSimpleGPA DECIMAL(18, 2);
        SET @cumSimpleGPA = 0;
        SET @cumSimpleCourseCredits = (
                                      SELECT COUNT(coursecredits)
                                      FROM   @CoursesNotRepeated
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND FinalGPA IS NOT NULL
                                      );
        SET @cumSimple_GPA_Credits = (
                                     SELECT SUM(FinalGPA)
                                     FROM   @CoursesNotRepeated
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalGPA IS NOT NULL
                                     );

        -- 3.7 SP2 Changes
        -- Include Equivalent Courses
        DECLARE @EquivCourse_SA_CC INT
               ,@EquivCourse_SA_GPA DECIMAL(18, 2);
        SET @EquivCourse_SA_CC = (
                                 SELECT ISNULL(COUNT(Credits), 0) AS Credits
                                 FROM   arReqs
                                 WHERE  ReqId IN (
                                                 SELECT     CE.EquivReqId
                                                 FROM       arCourseEquivalent CE
                                                 INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                 INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                 WHERE      StuEnrollId = @StuEnrollId
                                                            AND R.GrdSysDetailId IS NOT NULL
                                                 )
                                 );

        SET @EquivCourse_SA_GPA = (
                                  SELECT     SUM(ISNULL(GSD.GPA, 0.00))
                                  FROM       arCourseEquivalent CE
                                  INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                  INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                  INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                  WHERE      StuEnrollId = @StuEnrollId
                                             AND R.GrdSysDetailId IS NOT NULL
                                  );

        -- PRINT ''Equiv Course Simple ''
        -- PRINT @EquivCourse_SA_CC
        -- PRINT ''Equiv Course GPA Simple''
        -- PRINT @EquivCourse_SA_GPA

        -- PRINT ''Before Course Credits Simple''
        -- PRINT @cumSimpleCourseCredits
        -- PRINT ''Before WE GPA Simple''
        -- PRINT @cumSimple_GPA_Credits

        SET @cumSimpleCourseCredits = @cumSimpleCourseCredits; -- + ISNULL(@EquivCourse_SA_CC,0.00);
        SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits; -- + ISNULL(@EquivCourse_SA_GPA,0.00);

        IF @cumSimpleCourseCredits >= 1
            BEGIN
                SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
            END;

        -- PRINT ''After Course Credits Simple''
        -- PRINT @cumSimpleCourseCredits
        -- PRINT ''After WE GPA Simple''
        -- PRINT @cumSimple_GPA_Credits
        -- PRINT @cumSimpleGPA

        /*** Calculate Cumulative Simple GPA Ends Here ****/

        /**** Calculate Cumulative GPA Weighted *********************/
        DECLARE @cumCourseCredits DECIMAL(18, 2)
               ,@cumWeighted_GPA_Credits DECIMAL(18, 2)
               ,@cumWeightedGPA DECIMAL(18, 2);
        SET @cumWeightedGPA = 0;
        SET @cumCourseCredits = (
                                SELECT SUM(coursecredits)
                                FROM   @CoursesNotRepeated
                                WHERE  StuEnrollId = @StuEnrollId
                                       AND FinalGPA IS NOT NULL
                                );
        SET @cumWeighted_GPA_Credits = (
                                       SELECT SUM(coursecredits * FinalGPA)
                                       FROM   @CoursesNotRepeated
                                       WHERE  StuEnrollId = @StuEnrollId
                                              AND FinalGPA IS NOT NULL
                                       );


        DECLARE @doesThisCourseHaveAEquivalentCourse INT;

        DECLARE @EquivCourse_WGPA_CC1 INT
               ,@EquivCourse_WGPA_GPA1 DECIMAL(18, 2);
        SET @EquivCourse_WGPA_CC1 = (
                                    SELECT SUM(ISNULL(Credits, 0)) AS Credits
                                    FROM   arReqs
                                    WHERE  ReqId IN (
                                                    SELECT     CE.EquivReqId
                                                    FROM       arCourseEquivalent CE
                                                    INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                    INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                    WHERE      StuEnrollId = @StuEnrollId
                                                               AND R.GrdSysDetailId IS NOT NULL
                                                    )
                                    );

        SET @EquivCourse_WGPA_GPA1 = (
                                     SELECT     SUM(GSD.GPA * R1.Credits)
                                     FROM       arCourseEquivalent CE
                                     INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                     INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                     INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                     INNER JOIN arReqs R1 ON R1.ReqId = CE.ReqId
                                     WHERE      StuEnrollId = @StuEnrollId
                                                AND R.GrdSysDetailId IS NOT NULL
                                     );

        -- PRINT ''Equiv Course ''
        -- PRINT @EquivCourse_WGPA_CC1
        -- PRINT ''Equiv Course GPA''
        -- PRINT @EquivCourse_WGPA_GPA1

        -- PRINT ''Before Course Credits''
        -- PRINT @cumCourseCredits
        -- PRINT ''Before WE GPA''
        -- PRINT @cumWeighted_GPA_Credits


        SET @cumCourseCredits = @cumCourseCredits; -- + ISNULL(@EquivCourse_WGPA_CC1,0.00);
        SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits; -- + ISNULL(@EquivCourse_WGPA_GPA1,0.00);

        IF @cumCourseCredits >= 1
            BEGIN
                SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
            END;

        -- PRINT ''After Course Credits''
        -- PRINT @cumCourseCredits
        -- PRINT ''After WE GPA''
        -- PRINT @cumWeighted_GPA_Credits

        -- PRINT ''After Cal GPA''
        -- PRINT @cumWeightedGPA

        /**************** Cumulative GPA Ends Here ************/


        /************  Logic to calculate GPA Ends Here *****************************/



        DECLARE @SUMCreditsAttempted DECIMAL(18, 2);
        DECLARE @SUMCreditsEarned DECIMAL(18, 2);
        DECLARE @SUMFACreditsEarned DECIMAL(18, 2);
        SET @SUMCreditsAttempted = 0;
        SET @SUMCreditsEarned = 0;
        SET @SUMFACreditsEarned = 0;

        DECLARE @EquivCourse_SA_CC2 DECIMAL(18, 2);
        SET @EquivCourse_SA_CC2 = (
                                  SELECT ISNULL(SUM(Credits), 0.00) AS Credits
                                  FROM   arReqs
                                  WHERE  ReqId IN (
                                                  SELECT     CE.EquivReqId
                                                  FROM       arCourseEquivalent CE
                                                  INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                  INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                  WHERE      StuEnrollId = @StuEnrollId
                                                             AND R.GrdSysDetailId IS NOT NULL
                                                  )
                                  );

        SELECT @SUMCreditsAttempted = SUM(ISNULL(SCS.CreditsAttempted, 0))
              ,@SUMCreditsEarned = SUM(ISNULL(SCS.CreditsEarned, 0))
        FROM   dbo.syCreditSummary AS SCS
        WHERE  SCS.StuEnrollId = @StuEnrollId;

        --SET @SUMCreditsEarned = @SUMCreditsEarned + @EquivCourse_SA_CC2;  

        SET @SUMFACreditsEarned = (
                                  SELECT SUM(FACreditsEarned)
                                  FROM   syCreditSummary
                                  WHERE  StuEnrollId = @StuEnrollId
                                  );

        DECLARE @Scheduledhours DECIMAL(18, 2)
               ,@ActualPresentDays_ConvertTo_Hours DECIMAL(18, 2);
        DECLARE @ActualAbsentDays_ConvertTo_Hours DECIMAL(18, 2)
               ,@ActualTardyDays_ConvertTo_Hours DECIMAL(18, 2);
        SET @Scheduledhours = 0;
        SET @Scheduledhours = (
                              SELECT SUM(ScheduledHours)
                              FROM   syStudentAttendanceSummary
                              WHERE  StuEnrollId = @StuEnrollId
                              );

        SET @ActualPresentDays_ConvertTo_Hours = (
                                                 SELECT SUM(ActualPresentDays_ConvertTo_Hours)
                                                 FROM   syStudentAttendanceSummary
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                 );

        SET @ActualAbsentDays_ConvertTo_Hours = (
                                                SELECT SUM(ActualAbsentDays_ConvertTo_Hours)
                                                FROM   syStudentAttendanceSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                );


        SET @ActualTardyDays_ConvertTo_Hours = (
                                               SELECT SUM(ActualTardyDays_ConvertTo_Hours)
                                               FROM   syStudentAttendanceSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                               );


        --Average calculation
        DECLARE @termAverageSum DECIMAL(18, 2)
               ,@CumAverage DECIMAL(18, 2)
               ,@cumAverageSum DECIMAL(18, 2)
               ,@cumAveragecount INT;
        DECLARE @TermAverageCount INT
               ,@TermAverage DECIMAL(18, 2);


        DECLARE @TardiesMakingAbsence INT
               ,@UnitTypeDescrip VARCHAR(20)
               ,@OriginalTardiesMakingAbsence INT;
        SET @TardiesMakingAbsence = (
                                    SELECT TOP 1 t1.TardiesMakingAbsence
                                    FROM   arPrgVersions t1
                                          ,arStuEnrollments t2
                                    WHERE  t1.PrgVerId = t2.PrgVerId
                                           AND t2.StuEnrollId = @StuEnrollId
                                    );

        SET @UnitTypeDescrip = (
                               SELECT     LTRIM(RTRIM(AAUT.UnitTypeDescrip))
                               FROM       arAttUnitType AS AAUT
                               INNER JOIN arPrgVersions AS APV ON APV.UnitTypeId = AAUT.UnitTypeId
                               INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                               WHERE      ASE.StuEnrollId = @StuEnrollId
                               );

        SET @OriginalTardiesMakingAbsence = (
                                            SELECT TOP 1 tardiesmakingabsence
                                            FROM   syStudentAttendanceSummary
                                            WHERE  StuEnrollId = @StuEnrollId
                                            );
        DECLARE @termstartdate1 DATETIME;

        DECLARE @MeetDate DATETIME
               ,@WeekDay VARCHAR(15)
               ,@StartDate DATETIME
               ,@EndDate DATETIME;
        DECLARE @PeriodDescrip VARCHAR(50)
               ,@Actual DECIMAL(18, 2)
               ,@Excused DECIMAL(18, 2)
               ,@ClsSectionId UNIQUEIDENTIFIER;
        DECLARE @Absent DECIMAL(18, 2)
               ,@SchedHours DECIMAL(18, 2)
               ,@TardyMinutes DECIMAL(18, 2);
        DECLARE @tardy DECIMAL(18, 2)
               ,@tracktardies INT
               ,@rownumber INT
               ,@IsTardy BIT
               ,@ActualRunningScheduledHours DECIMAL(18, 2);
        DECLARE @ActualRunningPresentHours DECIMAL(18, 2)
               ,@ActualRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningTardyHours DECIMAL(18, 2)
               ,@ActualRunningMakeupHours DECIMAL(18, 2);
        DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
               ,@intTardyBreakPoint INT
               ,@AdjustedRunningPresentHours DECIMAL(18, 2)
               ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningScheduledDays DECIMAL(18, 2);
        --declare @Scheduledhours decimal(18,2),@ActualPresentDays_ConvertTo_Hours decimal(18,2),@ActualAbsentDays_ConvertTo_Hours decimal(18,2),@AttendanceTrack varchar(50)
        DECLARE @TermDescrip VARCHAR(50)
               ,@PrgVerId UNIQUEIDENTIFIER;
        DECLARE @TardyHit VARCHAR(10)
               ,@ScheduledMinutes DECIMAL(18, 2);
        DECLARE @Scheduledhours_noperiods DECIMAL(18, 2)
               ,@ActualPresentDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2)
               ,@ActualAbsentDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2);
        DECLARE @ActualTardyDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2);

        --Select * from arAttUnitType
        -- By Class and present absent, none
        /*
	IF @UnitTypeDescrip IN ( ''none'',''present absent'' )
		AND @TrackSapAttendance = ''byclass''
		BEGIN
			---- PRINT ''Step1!!!!''
			DELETE  FROM syStudentAttendanceSummary
			WHERE   StuEnrollId = @StuEnrollId;
			DECLARE @boolReset BIT
			   ,@MakeupHours DECIMAL(18,2)
			   ,@AdjustedPresentDaysComputed DECIMAL(18,2);
			DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD
			FOR
				SELECT  *
					   ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
				FROM    ( SELECT DISTINCT
									t1.StuEnrollId
								   ,t1.ClsSectionId
								   ,t1.MeetDate
								   ,DATENAME(dw,t1.MeetDate) AS WeekDay
								   ,t4.StartDate
								   ,t4.EndDate
								   ,t5.PeriodDescrip
								   ,t1.Actual
								   ,t1.Excused
								   ,CASE WHEN ( t1.Actual = 0
												AND t1.Excused = 0
											  ) THEN t1.Scheduled
										 ELSE CASE WHEN ( t1.Actual <> 9999.00
														  AND t1.Actual < t1.Scheduled
														  AND t1.Excused <> 1
														) THEN ( t1.Scheduled - t1.Actual )
												   ELSE 0
											  END
									END AS Absent
								   ,t1.Scheduled AS ScheduledMinutes
								   ,CASE WHEN ( t1.Actual > 0
												AND t1.Actual < t1.Scheduled
											  ) THEN ( t1.Scheduled - t1.Actual )
										 ELSE 0
									END AS TardyMinutes
								   ,t1.Tardy AS Tardy
								   ,t3.TrackTardies
								   ,t3.TardiesMakingAbsence
								   ,t3.PrgVerId
						  FROM      atClsSectAttendance t1
						  INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
						  INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
						  INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
						  INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
															 AND ( CONVERT(CHAR(10),t1.MeetDate,101) >= CONVERT(CHAR(10),t4.StartDate,101)
																   AND CONVERT(CHAR(10),t1.MeetDate,101) <= CONVERT(CHAR(10),t4.EndDate,101)
																 )
															 AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
						  INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
						  INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
						  INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
						  INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
						  INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
						  INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
						  WHERE     t2.StuEnrollId = @StuEnrollId
									AND ( AAUT1.UnitTypeDescrip IN ( ''None'',''Present Absent'' )
										  OR AAUT2.UnitTypeDescrip IN ( ''None'',''Present Absent'' )
										)
									AND t1.Actual <> 9999
						) dt
				ORDER BY StuEnrollId
					   ,MeetDate;
			OPEN GetAttendance_Cursor;
			FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,
				@PeriodDescrip,@Actual,@Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,
				@TardiesMakingAbsence,@PrgVerId,@rownumber;
			SET @ActualRunningPresentHours = 0;
			SET @ActualRunningPresentHours = 0;
			SET @ActualRunningAbsentHours = 0;
			SET @ActualRunningTardyHours = 0;
			SET @ActualRunningMakeupHours = 0;
			SET @intTardyBreakPoint = 0;
			SET @AdjustedRunningPresentHours = 0;
			SET @AdjustedRunningAbsentHours = 0;
			SET @ActualRunningScheduledDays = 0;
			SET @boolReset = 0;
			SET @MakeupHours = 0;
			WHILE @@FETCH_STATUS = 0
				BEGIN

					IF @PrevStuEnrollId <> @StuEnrollId
						BEGIN
							SET @ActualRunningPresentHours = 0;
							SET @ActualRunningAbsentHours = 0;
							SET @intTardyBreakPoint = 0;
							SET @ActualRunningTardyHours = 0;
							SET @AdjustedRunningPresentHours = 0;
							SET @AdjustedRunningAbsentHours = 0;
							SET @ActualRunningScheduledDays = 0;
							SET @MakeupHours = 0;
							SET @boolReset = 1;
						END;

					  
					-- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
					IF @Actual <> 9999.00
						BEGIN
							SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual + @Excused;
							SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual + @Excused;
							
							-- If there are make up hrs deduct that otherwise it will be added again in progress report
							IF ( @Actual > 0
								 AND @Actual > @ScheduledMinutes
								 AND @Actual <> 9999.00
							   )
								BEGIN
									SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual
																									- @ScheduledMinutes );
									SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual
																										- @ScheduledMinutes );
								END;
							  
							SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;                      
						END;
				   
					-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
					SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
					SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
			  
					-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
					IF ( @Actual > 0
						 AND @Actual < @ScheduledMinutes
						 AND @tardy = 1
					   )
						BEGIN
							SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
						END;
							
					-- Track how many days student has been tardy only when 
					-- program version requires to track tardy
					IF ( @tracktardies = 1
						 AND @tardy = 1
					   )
						BEGIN
							SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
						END;	    
		   
			
					-- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
					-- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
					-- when student is tardy the second time, that second occurance will be considered as
					-- absence
					-- Variable @intTardyBreakpoint tracks how many times the student was tardy
					-- Variable @tardiesMakingAbsence tracks the tardy rule
					IF ( @tracktardies = 1
						 AND @intTardyBreakPoint = @TardiesMakingAbsence
					   )
						BEGIN
							SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
							SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent )
								+ @ScheduledMinutes; --@TardyMinutes
							SET @intTardyBreakPoint = 0;
						END;
				   
				   -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
					IF ( @Actual > 0
						 AND @Actual > @ScheduledMinutes
						 AND @Actual <> 9999.00
					   )
						BEGIN
							SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
						END;
			  
					IF ( @tracktardies = 1 )
						BEGIN
							SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
						END;
					ELSE
						BEGIN
							SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
						END;
				
				---- PRINT @MeetDate
				---- PRINT @ActualRunningAbsentHours
				
					DELETE  FROM syStudentAttendanceSummary
					WHERE   StuEnrollId = @StuEnrollId
							AND ClsSectionId = @ClsSectionId
							AND StudentAttendedDate = @MeetDate;
					INSERT  INTO syStudentAttendanceSummary
							( StuEnrollId
							,ClsSectionId
							,StudentAttendedDate
							,ScheduledDays
							,ActualDays
							,ActualRunningScheduledDays
							,ActualRunningPresentDays
							,ActualRunningAbsentDays
							,ActualRunningMakeupDays
							,ActualRunningTardyDays
							,AdjustedPresentDays
							,AdjustedAbsentDays
							,AttendanceTrackType
							,ModUser
							,ModDate
							,tardiesmakingabsence
							)
					VALUES  ( @StuEnrollId
							,@ClsSectionId
							,@MeetDate
							,@ScheduledMinutes
							,@Actual
							,@ActualRunningScheduledDays
							,@ActualRunningPresentHours
							,@ActualRunningAbsentHours
							,ISNULL(@MakeupHours,0)
							,@ActualRunningTardyHours
							,@AdjustedPresentDaysComputed
							,@AdjustedRunningAbsentHours
							,''Post Attendance by Class Min''
							,''sa''
							,GETDATE()
							,@TardiesMakingAbsence
							);

				--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
					SET @PrevStuEnrollId = @StuEnrollId; 

					FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,
						@EndDate,@PeriodDescrip,@Actual,@Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,
						@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
				END;
			CLOSE GetAttendance_Cursor;
			DEALLOCATE GetAttendance_Cursor;
		END;			
*/
        IF @UnitTypeDescrip IN ( ''none'', ''present absent'' )
           AND @TrackSapAttendance = ''byclass''
            BEGIN
                ---- PRINT ''Step1!!!!''
                DELETE FROM syStudentAttendanceSummary
                WHERE StuEnrollId = @StuEnrollId;
                DECLARE @boolReset BIT
                       ,@MakeupHours DECIMAL(18, 2)
                       ,@AdjustedPresentDaysComputed DECIMAL(18, 2);
                DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
                    SELECT   dt.StuEnrollId
                            ,dt.ClsSectionId
                            ,dt.MeetDate
                            ,dt.WeekDay
                            ,dt.StartDate
                            ,dt.EndDate
                            ,dt.PeriodDescrip
                            ,dt.Actual
                            ,dt.Excused
                            ,dt.Absent
                            ,dt.ScheduledMinutes
                            ,dt.TardyMinutes
                            ,dt.Tardy
                            ,dt.TrackTardies
                            ,dt.TardiesMakingAbsence
                            ,dt.PrgVerId
                            ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                    FROM     (
                             SELECT     DISTINCT t1.StuEnrollId
                                                ,t1.ClsSectionId
                                                ,t1.MeetDate
                                                ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                ,t4.StartDate
                                                ,t4.EndDate
                                                ,t5.PeriodDescrip
                                                ,t1.Actual
                                                ,t1.Excused
                                                ,CASE WHEN (
                                                           t1.Actual = 0
                                                           AND t1.Excused = 0
                                                           ) THEN t1.Scheduled
                                                      ELSE CASE WHEN (
                                                                     t1.Actual <> 9999.00
                                                                     AND t1.Actual < t1.Scheduled
                                                                     --AND t1.Excused <> 1
                                                                     ) THEN ( t1.Scheduled - t1.Actual )
                                                                ELSE 0
                                                           END
                                                 END AS Absent
                                                ,t1.Scheduled AS ScheduledMinutes
                                                ,CASE WHEN (
                                                           t1.Actual > 0
                                                           AND t1.Actual < t1.Scheduled
                                                           ) THEN ( t1.Scheduled - t1.Actual )
                                                      ELSE 0
                                                 END AS TardyMinutes
                                                ,t1.Tardy AS Tardy
                                                ,t3.TrackTardies
                                                ,t3.TardiesMakingAbsence
                                                ,t3.PrgVerId
                             FROM       atClsSectAttendance t1
                             INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                             INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                             INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                             INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                AND (
                                                                    CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                    AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                    )
                                                                AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                             INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                             INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                             INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                             INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                             INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                             INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                             WHERE      t2.StuEnrollId = @StuEnrollId
                                        AND (
                                            AAUT1.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                            OR AAUT2.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                            )
                                        AND t1.Actual <> 9999
                             ) dt
                    ORDER BY StuEnrollId
                            ,MeetDate;
                OPEN GetAttendance_Cursor;
                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@ClsSectionId
                    ,@MeetDate
                    ,@WeekDay
                    ,@StartDate
                    ,@EndDate
                    ,@PeriodDescrip
                    ,@Actual
                    ,@Excused
                    ,@Absent
                    ,@ScheduledMinutes
                    ,@TardyMinutes
                    ,@tardy
                    ,@tracktardies
                    ,@TardiesMakingAbsence
                    ,@PrgVerId
                    ,@rownumber;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                SET @boolReset = 0;
                SET @MakeupHours = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                                SET @MakeupHours = 0;
                                SET @boolReset = 1;
                            END;


                        -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                        IF @Actual <> 9999.00
                            BEGIN
                                -- Commented by Balaji on 2.6.2015 as Excused Absence is still considered an absence
                                -- @Excused is not added to Present Hours
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual; -- + @Excused
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; -- + @Excused

                                -- If there are make up hrs deduct that otherwise it will be added again in progress report
                                IF (
                                   @Actual > 0
                                   AND @Actual > @ScheduledMinutes
                                   AND @Actual <> 9999.00
                                   )
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                    END;

                                SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                            END;

                        -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                        -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                        IF (
                           @Actual > 0
                           AND @Actual < @ScheduledMinutes
                           AND @tardy = 1
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                            END;
                        ELSE IF (
                                @Actual = 1
                                AND @Actual = @ScheduledMinutes
                                AND @tardy = 1
                                )
                                 BEGIN
                                     SET @ActualRunningTardyHours += 1;
                                 END;

                        -- Track how many days student has been tardy only when 
                        -- program version requires to track tardy
                        IF (
                           @tracktardies = 1
                           AND @tardy = 1
                           )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;


                        -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
                        -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
                        -- when student is tardy the second time, that second occurance will be considered as
                        -- absence
                        -- Variable @intTardyBreakpoint tracks how many times the student was tardy
                        -- Variable @tardiesMakingAbsence tracks the tardy rule
                        IF (
                           @tracktardies = 1
                           AND @TardiesMakingAbsence > 0
                           AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
                                SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;

                        -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                        IF (
                           @Actual > 0
                           AND @Actual > @ScheduledMinutes
                           AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                            END;

                        IF ( @tracktardies = 1 )
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                            END;
                        ELSE
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                            END;

                        ---- PRINT @MeetDate
                        ---- PRINT @ActualRunningAbsentHours

                        DELETE FROM syStudentAttendanceSummary
                        WHERE StuEnrollId = @StuEnrollId
                              AND ClsSectionId = @ClsSectionId
                              AND StudentAttendedDate = @MeetDate;
                        INSERT INTO syStudentAttendanceSummary (
                                                               StuEnrollId
                                                              ,ClsSectionId
                                                              ,StudentAttendedDate
                                                              ,ScheduledDays
                                                              ,ActualDays
                                                              ,ActualRunningScheduledDays
                                                              ,ActualRunningPresentDays
                                                              ,ActualRunningAbsentDays
                                                              ,ActualRunningMakeupDays
                                                              ,ActualRunningTardyDays
                                                              ,AdjustedPresentDays
                                                              ,AdjustedAbsentDays
                                                              ,AttendanceTrackType
                                                              ,ModUser
                                                              ,ModDate
                                                              ,tardiesmakingabsence
                                                               )
                        VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                                ,@ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours, @AdjustedPresentDaysComputed
                                ,@AdjustedRunningAbsentHours, ''Post Attendance by Class Min'', ''sa'', GETDATE(), @TardiesMakingAbsence );

                        --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                        SET @PrevStuEnrollId = @StuEnrollId;

                        FETCH NEXT FROM GetAttendance_Cursor
                        INTO @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@WeekDay
                            ,@StartDate
                            ,@EndDate
                            ,@PeriodDescrip
                            ,@Actual
                            ,@Excused
                            ,@Absent
                            ,@ScheduledMinutes
                            ,@TardyMinutes
                            ,@tardy
                            ,@tracktardies
                            ,@TardiesMakingAbsence
                            ,@PrgVerId
                            ,@rownumber;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;

                DECLARE @MyTardyTable TABLE
                    (
                        ClsSectionId UNIQUEIDENTIFIER
                       ,TardiesMakingAbsence INT
                       ,AbsentHours DECIMAL(18, 2)
                    );

                INSERT INTO @MyTardyTable
                            SELECT     ClsSectionId
                                      ,PV.TardiesMakingAbsence
                                      ,CASE WHEN ( COUNT(*) >= PV.TardiesMakingAbsence ) THEN ( COUNT(*) / PV.TardiesMakingAbsence )
                                            ELSE 0
                                       END AS AbsentHours
                            --Count(*) as NumberofTimesTardy 
                            FROM       syStudentAttendanceSummary SAS
                            INNER JOIN arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                            WHERE      SE.StuEnrollId = @StuEnrollId
                                       AND SAS.IsTardy = 1
                                       AND PV.TrackTardies = 1
                            GROUP BY   ClsSectionId
                                      ,PV.TardiesMakingAbsence;

                --Drop table @MyTardyTable
                DECLARE @TotalTardyAbsentDays DECIMAL(18, 2);
                SET @TotalTardyAbsentDays = (
                                            SELECT ISNULL(SUM(AbsentHours), 0)
                                            FROM   @MyTardyTable
                                            );

                --Print @TotalTardyAbsentDays

                UPDATE syStudentAttendanceSummary
                SET    AdjustedPresentDays = AdjustedPresentDays - @TotalTardyAbsentDays
                      ,AdjustedAbsentDays = AdjustedAbsentDays + @TotalTardyAbsentDays
                WHERE  StuEnrollId = @StuEnrollId
                       AND StudentAttendedDate = (
                                                 SELECT   TOP 1 StudentAttendedDate
                                                 FROM     syStudentAttendanceSummary
                                                 WHERE    StuEnrollId = @StuEnrollId
                                                 ORDER BY StudentAttendedDate DESC
                                                 );

            END;

        -- By Class and Attendance Unit Type - Minutes and Clock Hour

        IF @UnitTypeDescrip IN ( ''minutes'', ''clock hours'' )
           AND @TrackSapAttendance = ''byclass''
            BEGIN
                DELETE FROM syStudentAttendanceSummary
                WHERE StuEnrollId = @StuEnrollId;

                DECLARE GetAttendance_Cursor CURSOR FOR
                    SELECT   *
                            ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                    FROM     (
                             SELECT     DISTINCT t1.StuEnrollId
                                                ,t1.ClsSectionId
                                                ,t1.MeetDate
                                                ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                ,t4.StartDate
                                                ,t4.EndDate
                                                ,t5.PeriodDescrip
                                                ,t1.Actual
                                                ,t1.Excused
                                                ,CASE WHEN (
                                                           t1.Actual = 0
                                                           AND t1.Excused = 0
                                                           ) THEN t1.Scheduled
                                                      ELSE CASE WHEN (
                                                                     t1.Actual <> 9999.00
                                                                     AND t1.Actual < t1.Scheduled
                                                                     ) THEN ( t1.Scheduled - t1.Actual )
                                                                ELSE 0
                                                           END
                                                 END AS Absent
                                                ,t1.Scheduled AS ScheduledMinutes
                                                ,CASE WHEN (
                                                           t1.Actual > 0
                                                           AND t1.Actual < t1.Scheduled
                                                           ) THEN ( t1.Scheduled - t1.Actual )
                                                      ELSE 0
                                                 END AS TardyMinutes
                                                ,t1.Tardy AS Tardy
                                                ,t3.TrackTardies
                                                ,t3.TardiesMakingAbsence
                                                ,t3.PrgVerId
                             FROM       atClsSectAttendance t1
                             INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                             INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                             INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                             INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                AND (
                                                                    CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                    AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                    )
                                                                AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                             INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                             INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                             INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                             INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                             INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                             INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                             WHERE      t2.StuEnrollId = @StuEnrollId
                                        AND (
                                            AAUT1.UnitTypeDescrip IN ( ''Minutes'', ''Clock Hours'' )
                                            OR AAUT2.UnitTypeDescrip IN ( ''Minutes'', ''Clock Hours'' )
                                            )
                                        AND t1.Actual <> 9999
                             UNION
                             SELECT     DISTINCT t1.StuEnrollId
                                                ,NULL AS ClsSectionId
                                                ,t1.RecordDate AS MeetDate
                                                ,DATENAME(dw, t1.RecordDate) AS WeekDay
                                                ,NULL AS StartDate
                                                ,NULL AS EndDate
                                                ,NULL AS PeriodDescrip
                                                ,t1.ActualHours
                                                ,NULL AS Excused
                                                ,CASE WHEN ( t1.ActualHours = 0 ) THEN t1.SchedHours
                                                      ELSE 0
                                                 --ELSE 
                                                 --Case when (t1.ActualHours <> 9999.00 and t1.ActualHours < t1.SchedHours)
                                                 --		THEN (t1.SchedHours - t1.ActualHours)
                                                 --		ELSE 
                                                 --			0
                                                 --		End
                                                 END AS Absent
                                                ,t1.SchedHours AS ScheduledMinutes
                                                ,CASE WHEN (
                                                           t1.ActualHours <> 9999.00
                                                           AND t1.ActualHours > 0
                                                           AND t1.ActualHours < t1.SchedHours
                                                           ) THEN ( t1.SchedHours - t1.ActualHours )
                                                      ELSE 0
                                                 END AS TardyMinutes
                                                ,NULL AS Tardy
                                                ,t3.TrackTardies
                                                ,t3.TardiesMakingAbsence
                                                ,t3.PrgVerId
                             FROM       arStudentClockAttendance t1
                             INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                             INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                             WHERE      t2.StuEnrollId = @StuEnrollId
                                        AND t1.Converted = 1
                                        AND t1.ActualHours <> 9999
                             ) dt
                    ORDER BY StuEnrollId
                            ,MeetDate;
                OPEN GetAttendance_Cursor;
                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@ClsSectionId
                    ,@MeetDate
                    ,@WeekDay
                    ,@StartDate
                    ,@EndDate
                    ,@PeriodDescrip
                    ,@Actual
                    ,@Excused
                    ,@Absent
                    ,@ScheduledMinutes
                    ,@TardyMinutes
                    ,@tardy
                    ,@tracktardies
                    ,@TardiesMakingAbsence
                    ,@PrgVerId
                    ,@rownumber;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                SET @boolReset = 0;
                SET @MakeupHours = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                                SET @MakeupHours = 0;
                                SET @boolReset = 1;
                            END;


                        -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                        IF @Actual <> 9999.00
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                                -- If there are make up hrs deduct that otherwise it will be added again in progress report
                                IF (
                                   @Actual > 0
                                   AND @Actual > @ScheduledMinutes
                                   AND @Actual <> 9999.00
                                   )
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                    END;
                                SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                            END;

                        -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                        -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                        IF (
                           @Actual > 0
                           AND @Actual < @ScheduledMinutes
                           AND @tardy = 1
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                            END;

                        -- Track how many days student has been tardy only when 
                        -- program version requires to track tardy
                        IF (
                           @tracktardies = 1
                           AND @tardy = 1
                           )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;


                        -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
                        -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
                        -- when student is tardy the second time, that second occurance will be considered as
                        -- absence
                        -- Variable @intTardyBreakpoint tracks how many times the student was tardy
                        -- Variable @tardiesMakingAbsence tracks the tardy rule
                        IF (
                           @tracktardies = 1
                           AND @TardiesMakingAbsence > 0
                           AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;

                        -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                        IF (
                           @Actual > 0
                           AND @Actual > @ScheduledMinutes
                           AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                            END;


                        IF ( @tracktardies = 1 )
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                            END;
                        ELSE
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                            END;




                        DELETE FROM syStudentAttendanceSummary
                        WHERE StuEnrollId = @StuEnrollId
                              AND ClsSectionId = @ClsSectionId
                              AND StudentAttendedDate = @MeetDate;
                        INSERT INTO syStudentAttendanceSummary (
                                                               StuEnrollId
                                                              ,ClsSectionId
                                                              ,StudentAttendedDate
                                                              ,ScheduledDays
                                                              ,ActualDays
                                                              ,ActualRunningScheduledDays
                                                              ,ActualRunningPresentDays
                                                              ,ActualRunningAbsentDays
                                                              ,ActualRunningMakeupDays
                                                              ,ActualRunningTardyDays
                                                              ,AdjustedPresentDays
                                                              ,AdjustedAbsentDays
                                                              ,AttendanceTrackType
                                                              ,ModUser
                                                              ,ModDate
                                                               )
                        VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                                ,@ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours, @AdjustedPresentDaysComputed
                                ,@AdjustedRunningAbsentHours, ''Post Attendance by Class Min'', ''sa'', GETDATE());

                        UPDATE syStudentAttendanceSummary
                        SET    tardiesmakingabsence = @TardiesMakingAbsence
                        WHERE  StuEnrollId = @StuEnrollId;
                        SET @PrevStuEnrollId = @StuEnrollId;

                        FETCH NEXT FROM GetAttendance_Cursor
                        INTO @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@WeekDay
                            ,@StartDate
                            ,@EndDate
                            ,@PeriodDescrip
                            ,@Actual
                            ,@Excused
                            ,@Absent
                            ,@ScheduledMinutes
                            ,@TardyMinutes
                            ,@tardy
                            ,@tracktardies
                            ,@TardiesMakingAbsence
                            ,@PrgVerId
                            ,@rownumber;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;
            END;
        --Select * from arAttUnitType


        -- By Minutes/Day
        -- remove clock hour from here
        IF @UnitTypeDescrip IN ( ''minutes'' )
           AND @TrackSapAttendance = ''byday''
            -- -- PRINT GETDATE();	
            ---- PRINT @UnitTypeId;
            ---- PRINT @TrackSapAttendance;
            BEGIN
                --Delete from syStudentAttendanceSummary where StuEnrollId=@StuEnrollId

                DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
                    SELECT     t1.StuEnrollId
                              ,NULL AS ClsSectionId
                              ,t1.RecordDate AS MeetDate
                              ,t1.ActualHours
                              ,t1.SchedHours AS ScheduledMinutes
                              ,CASE WHEN (
                                         (
                                         t1.SchedHours >= 1
                                         AND t1.SchedHours NOT IN ( 999, 9999 )
                                         )
                                         AND t1.ActualHours = 0
                                         ) THEN t1.SchedHours
                                    ELSE 0
                               END AS Absent
                              ,t1.isTardy
                              ,(
                               SELECT ISNULL(SUM(SchedHours), 0)
                               FROM   arStudentClockAttendance
                               WHERE  StuEnrollId = t1.StuEnrollId
                                      AND RecordDate <= t1.RecordDate
                                      AND (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999, 9999 )
                                          AND t1.ActualHours NOT IN ( 999, 9999 )
                                          )
                               ) AS ActualRunningScheduledHours
                              ,(
                               SELECT SUM(ActualHours)
                               FROM   arStudentClockAttendance
                               WHERE  StuEnrollId = t1.StuEnrollId
                                      AND RecordDate <= t1.RecordDate
                                      AND (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999, 9999 )
                                          )
                                      AND ActualHours >= 1
                                      AND ActualHours NOT IN ( 999, 9999 )
                               ) AS ActualRunningPresentHours
                              ,(
                               SELECT COUNT(ActualHours)
                               FROM   arStudentClockAttendance
                               WHERE  StuEnrollId = t1.StuEnrollId
                                      AND RecordDate <= t1.RecordDate
                                      AND (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999, 9999 )
                                          )
                                      AND ActualHours = 0
                                      AND ActualHours NOT IN ( 999, 9999 )
                               ) AS ActualRunningAbsentHours
                              ,(
                               SELECT SUM(ActualHours)
                               FROM   arStudentClockAttendance
                               WHERE  StuEnrollId = t1.StuEnrollId
                                      AND RecordDate <= t1.RecordDate
                                      AND SchedHours = 0
                                      AND ActualHours >= 1
                                      AND ActualHours NOT IN ( 999, 9999 )
                               ) AS ActualRunningMakeupHours
                              ,(
                               SELECT SUM(SchedHours - ActualHours)
                               FROM   arStudentClockAttendance
                               WHERE  StuEnrollId = t1.StuEnrollId
                                      AND RecordDate <= t1.RecordDate
                                      AND (
                                          (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999, 9999 )
                                          )
                                          AND ActualHours >= 1
                                          AND ActualHours NOT IN ( 999, 9999 )
                                          )
                                      AND isTardy = 1
                               ) AS ActualRunningTardyHours
                              ,t3.TrackTardies
                              ,t3.TardiesMakingAbsence
                              ,t3.PrgVerId
                              ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
                    FROM       arStudentClockAttendance t1
                    INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                    INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                    INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                    --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                    WHERE      AAUT1.UnitTypeDescrip IN ( ''Minutes'' )
                               AND t2.StuEnrollId = @StuEnrollId
                               AND t1.ActualHours <> 9999.00
                    ORDER BY   t1.StuEnrollId
                              ,MeetDate;
                OPEN GetAttendance_Cursor;
                --Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@ClsSectionId
                    ,@MeetDate
                    ,@Actual
                    ,@ScheduledMinutes
                    ,@Absent
                    ,@IsTardy
                    ,@ActualRunningScheduledHours
                    ,@ActualRunningPresentHours
                    ,@ActualRunningAbsentHours
                    ,@ActualRunningMakeupHours
                    ,@ActualRunningTardyHours
                    ,@tracktardies
                    ,@TardiesMakingAbsence
                    ,@PrgVerId
                    ,@rownumber;

                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                            END;

                        IF (
                           @ScheduledMinutes >= 1
                           AND (
                               @Actual <> 9999
                               AND @Actual <> 999
                               )
                           AND (
                               @ScheduledMinutes <> 9999
                               AND @Actual <> 999
                               )
                           )
                            BEGIN
                                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + ISNULL(@ScheduledMinutes, 0);
                            END;
                        ELSE
                            BEGIN
                                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0);
                            END;

                        IF (
                           @Actual <> 9999
                           AND @Actual <> 999
                           )
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                            END;
                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;

                        IF (
                           @Actual > 0
                           AND @Actual < @ScheduledMinutes
                           )
                            BEGIN
                                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                            END;
                        -- Make up hours
                        --sched=5, Actual =7, makeup= 2,ab = 0
                        --sched=0, Actual =7, makeup= 7,ab = 0
                        IF (
                           @Actual > 0
                           AND @ScheduledMinutes > 0
                           AND @Actual > @ScheduledMinutes
                           AND (
                               @Actual <> 9999
                               AND @Actual <> 999
                               )
                           )
                            BEGIN
                                SET @ActualRunningMakeupHours = @ActualRunningMakeupHours + ( @Actual - @ScheduledMinutes );
                            END;


                        IF (
                           @Actual <> 9999
                           AND @Actual <> 999
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                            END;
                        IF (
                           @Actual = 0
                           AND @ScheduledMinutes >= 1
                           AND @ScheduledMinutes NOT IN ( 999, 9999 )
                           )
                            BEGIN
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                            END;
                        -- Absent hours
                        --1. sched = 5, Actual = 2 then Ab = (5-3) = 2
                        IF (
                           @Absent = 0
                           AND @ActualRunningAbsentHours > 0
                           AND ( @Actual < @ScheduledMinutes )
                           )
                            BEGIN
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                            END;
                        IF (
                           @Actual > 0
                           AND @Actual < @ScheduledMinutes
                           AND (
                               @Actual <> 9999.00
                               AND @Actual <> 999.00
                               )
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                            END;

                        IF @tracktardies = 1
                           AND (
                               @TardyMinutes > 0
                               OR @IsTardy = 1
                               )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;
                        IF (
                           @tracktardies = 1
                           AND @TardiesMakingAbsence > 0
                           AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Actual; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;
                        DELETE FROM syStudentAttendanceSummary
                        WHERE StuEnrollId = @StuEnrollId
                              AND StudentAttendedDate = @MeetDate;
                        INSERT INTO syStudentAttendanceSummary (
                                                               StuEnrollId
                                                              ,ClsSectionId
                                                              ,StudentAttendedDate
                                                              ,ScheduledDays
                                                              ,ActualDays
                                                              ,ActualRunningScheduledDays
                                                              ,ActualRunningPresentDays
                                                              ,ActualRunningAbsentDays
                                                              ,ActualRunningMakeupDays
                                                              ,ActualRunningTardyDays
                                                              ,AdjustedPresentDays
                                                              ,AdjustedAbsentDays
                                                              ,AttendanceTrackType
                                                              ,ModUser
                                                              ,ModDate
                                                               )
                        VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                                ,@ActualRunningAbsentHours, @ActualRunningMakeupHours, @ActualRunningTardyHours, @AdjustedRunningPresentHours
                                ,@AdjustedRunningAbsentHours, ''Post Attendance by Class'', ''sa'', GETDATE());

                        UPDATE syStudentAttendanceSummary
                        SET    tardiesmakingabsence = @TardiesMakingAbsence
                        WHERE  StuEnrollId = @StuEnrollId;
                        --end
                        SET @PrevStuEnrollId = @StuEnrollId;
                        FETCH NEXT FROM GetAttendance_Cursor
                        INTO @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@Actual
                            ,@ScheduledMinutes
                            ,@Absent
                            ,@IsTardy
                            ,@ActualRunningScheduledHours
                            ,@ActualRunningPresentHours
                            ,@ActualRunningAbsentHours
                            ,@ActualRunningMakeupHours
                            ,@ActualRunningTardyHours
                            ,@tracktardies
                            ,@TardiesMakingAbsence
                            ,@PrgVerId
                            ,@rownumber;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;
            END;

        ---- PRINT ''Does it get to Clock Hour/PA'';
        ---- PRINT @UnitTypeId;
        ---- PRINT @TrackSapAttendance;
        -- By Day and PA, Clock Hour
        IF @UnitTypeDescrip IN ( ''present absent'', ''clock hours'' )
           AND @TrackSapAttendance = ''byday''
            BEGIN
                ---- PRINT ''By Day inside day'';
                DECLARE GetAttendance_Cursor CURSOR FOR
                    SELECT     t1.StuEnrollId
                              ,t1.RecordDate
                              ,t1.ActualHours
                              ,t1.SchedHours
                              ,CASE WHEN (
                                         (
                                         t1.SchedHours >= 1
                                         AND t1.SchedHours NOT IN ( 999, 9999 )
                                         )
                                         AND t1.ActualHours = 0
                                         ) THEN t1.SchedHours
                                    ELSE 0
                               END AS Absent
                              ,t1.isTardy
                              ,t3.TrackTardies
                              ,t3.TardiesMakingAbsence
                    FROM       arStudentClockAttendance t1
                    INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                    INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                    INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                    --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                    WHERE -- Unit Types: Present Absent and Clock Hour
                               AAUT1.UnitTypeDescrip IN ( ''present absent'', ''clock hours'' )
                               AND ActualHours <> 9999.00
                               AND t2.StuEnrollId = @StuEnrollId
                    ORDER BY   t1.StuEnrollId
                              ,t1.RecordDate;
                OPEN GetAttendance_Cursor;
                --Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@MeetDate
                    ,@Actual
                    ,@ScheduledMinutes
                    ,@Absent
                    ,@IsTardy
                    ,@tracktardies
                    ,@TardiesMakingAbsence;

                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                SET @MakeupHours = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                                SET @MakeupHours = 0;
                            END;

                        IF (
                           @Actual <> 9999
                           AND @Actual <> 999
                           )
                            BEGIN
                                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + @ScheduledMinutes;
                                SET @ActualRunningPresentHours = ISNULL(@ActualRunningPresentHours, 0) + @Actual;
                                SET @AdjustedRunningPresentHours = ISNULL(@AdjustedRunningPresentHours, 0) + @Actual;
                            END;
                        SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + @Absent;
                        IF (
                           @Actual = 0
                           AND @ScheduledMinutes >= 1
                           AND @ScheduledMinutes NOT IN ( 999, 9999 )
                           )
                            BEGIN
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                            END;

                        -- NWH 
                        -- If Scheduled = 3.0 hrs and Actual = 1.0 Then 2 hrs is considered absent
                        IF (
                           @ScheduledMinutes >= 1
                           AND @ScheduledMinutes NOT IN ( 999, 9999 )
                           AND @Actual > 0
                           AND ( @Actual < @ScheduledMinutes )
                           )
                            BEGIN
                                SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                                SET @AdjustedRunningAbsentHours = ISNULL(@AdjustedRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                            END;

                        IF (
                           @tracktardies = 1
                           AND @IsTardy = 1
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + 1;
                            END;
                        --commented by balaji on 10/22/2012 as report (rdl) doesn''t add days attended and make up days
                        ---- If there are make up hrs deduct that otherwise it will be added again in progress report
                        IF (
                           @Actual > 0
                           AND @Actual > @ScheduledMinutes
                           AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                            END;

                        IF @tracktardies = 1
                           AND (
                               @TardyMinutes > 0
                               OR @IsTardy = 1
                               )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;



                        -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                        IF (
                           @Actual > 0
                           AND @Actual > @ScheduledMinutes
                           AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                            END;

                        IF (
                           @tracktardies = 1
                           AND @TardiesMakingAbsence > 0
                           AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;

                        DELETE FROM syStudentAttendanceSummary
                        WHERE StuEnrollId = @StuEnrollId
                              AND StudentAttendedDate = @MeetDate;
                        INSERT INTO syStudentAttendanceSummary (
                                                               StuEnrollId
                                                              ,ClsSectionId
                                                              ,StudentAttendedDate
                                                              ,ScheduledDays
                                                              ,ActualDays
                                                              ,ActualRunningScheduledDays
                                                              ,ActualRunningPresentDays
                                                              ,ActualRunningAbsentDays
                                                              ,ActualRunningMakeupDays
                                                              ,ActualRunningTardyDays
                                                              ,AdjustedPresentDays
                                                              ,AdjustedAbsentDays
                                                              ,AttendanceTrackType
                                                              ,ModUser
                                                              ,ModDate
                                                              ,tardiesmakingabsence
                                                               )
                        VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, ISNULL(@ScheduledMinutes, 0), @Actual, ISNULL(@ActualRunningScheduledDays, 0)
                                ,ISNULL(@ActualRunningPresentHours, 0), ISNULL(@ActualRunningAbsentHours, 0), ISNULL(@MakeupHours, 0)
                                ,ISNULL(@ActualRunningTardyHours, 0), ISNULL(@AdjustedRunningPresentHours, 0), ISNULL(@AdjustedRunningAbsentHours, 0)
                                ,''Post Attendance by Class'', ''sa'', GETDATE(), @TardiesMakingAbsence );

                        --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                        --end
                        SET @PrevStuEnrollId = @StuEnrollId;
                        FETCH NEXT FROM GetAttendance_Cursor
                        INTO @StuEnrollId
                            ,@MeetDate
                            ,@Actual
                            ,@ScheduledMinutes
                            ,@Absent
                            ,@IsTardy
                            ,@tracktardies
                            ,@TardiesMakingAbsence;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;
            END;
        --end

        -- Based on grade rounding round the final score and current score
        --Declare @PrevTermId uniqueidentifier,@PrevReqId uniqueidentifier,@RowCount int,@FinalScore decimal(18,4),@CurrentScore decimal(18,4)
        --declare @ReqId uniqueidentifier,@CourseCodeDescrip varchar(100),@FinalGrade varchar(10),@sysComponentTypeId int
        --declare @CreditsAttempted decimal(18,4),@Grade varchar(10),@IsPass bit,@IsCreditsAttempted bit,@IsCreditsEarned bit
        --declare @IsInGPA bit,@FinAidCredits decimal(18,4),@CurrentGrade varchar(10),@CourseCredits decimal(18,4)
        DECLARE @GPA DECIMAL(18, 4);

        DECLARE @PrevReqId UNIQUEIDENTIFIER
               ,@PrevTermId UNIQUEIDENTIFIER
               ,@CreditsAttempted DECIMAL(18, 2)
               ,@CreditsEarned DECIMAL(18, 2);
        DECLARE @reqid UNIQUEIDENTIFIER
               ,@CourseCodeDescrip VARCHAR(50)
               ,@FinalGrade VARCHAR(50)
               ,@FinalScore DECIMAL(18, 2)
               ,@Grade VARCHAR(50);
        DECLARE @IsPass BIT
               ,@IsCreditsAttempted BIT
               ,@IsCreditsEarned BIT
               ,@CurrentScore DECIMAL(18, 2)
               ,@CurrentGrade VARCHAR(10)
               ,@FinalGPA DECIMAL(18, 2);
        DECLARE @sysComponentTypeId INT
               ,@RowCount INT;
        DECLARE @IsInGPA BIT;
        DECLARE @CourseCredits DECIMAL(18, 2);
        DECLARE @FinAidCreditsEarned DECIMAL(18, 2)
               ,@FinAidCredits DECIMAL(18, 2);




        DECLARE GetCreditsSummary_Cursor CURSOR FOR
            SELECT     DISTINCT SE.StuEnrollId
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate
                               ,R.ReqId
                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                               ,RES.Score AS FinalScore
                               ,RES.GrdSysDetailId AS FinalGrade
                               ,GCT.SysComponentTypeId
                               ,R.Credits AS CreditsAttempted
                               ,CS.ClsSectionId
                               ,GSD.Grade
                               ,GSD.IsPass
                               ,GSD.IsCreditsAttempted
                               ,GSD.IsCreditsEarned
                               ,SE.PrgVerId
                               ,GSD.IsInGPA
                               ,R.FinAidCredits AS FinAidCredits
            FROM       arStuEnrollments SE
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
            INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
            INNER JOIN arTerm T ON CS.TermId = T.TermId
            INNER JOIN arReqs R ON CS.ReqId = R.ReqId
            LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
            LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
            LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
            LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            WHERE      SE.StuEnrollId = @StuEnrollId
            ORDER BY   T.StartDate
                      ,T.TermDescrip
                      ,R.ReqId;

        DECLARE @varGradeRounding VARCHAR(3);
        DECLARE @roundfinalscore DECIMAL(18, 4);
        SET @varGradeRounding = (
                                SELECT Value
                                FROM   syConfigAppSetValues
                                WHERE  SettingId = 45
                                );

        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor
        INTO @StuEnrollId
            ,@TermId
            ,@TermDescrip
            ,@termstartdate1
            ,@reqid
            ,@CourseCodeDescrip
            ,@FinalScore
            ,@FinalGrade
            ,@sysComponentTypeId
            ,@CreditsAttempted
            ,@ClsSectionId
            ,@Grade
            ,@IsPass
            ,@IsCreditsAttempted
            ,@IsCreditsEarned
            ,@PrgVerId
            ,@IsInGPA
            ,@FinAidCredits;
        WHILE @@FETCH_STATUS = 0
            BEGIN


                SET @CurrentScore = (
                                    SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                ELSE NULL
                                           END AS CurrentScore
                                    FROM   (
                                           SELECT   InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight AS GradeBookWeight
                                                   ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                         ELSE 0
                                                    END AS ActualWeight
                                           FROM     (
                                                    SELECT   C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,ISNULL(C.Weight, 0) AS Weight
                                                            ,C.Number AS MinNumber
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter AS Param
                                                            ,X.GrdScaleId
                                                            ,SUM(GR.Score) AS Score
                                                            ,COUNT(D.Descrip) AS NumberOfComponents
                                                    FROM     (
                                                             SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                    ,A.EffectiveDate
                                                                                    ,B.GrdScaleId
                                                             FROM     arGrdBkWeights A
                                                                     ,arClassSections B
                                                             WHERE    A.ReqId = B.ReqId
                                                                      AND A.EffectiveDate <= B.StartDate
                                                                      AND B.ClsSectionId = @ClsSectionId
                                                             ORDER BY A.EffectiveDate DESC
                                                             ) X
                                                            ,arGrdBkWgtDetails C
                                                            ,arGrdComponentTypes D
                                                            ,arGrdBkResults GR
                                                    WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                             AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                             AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                             AND D.SysComponentTypeId NOT IN ( 500, 503 )
                                                             AND GR.StuEnrollId = @StuEnrollId
                                                             AND GR.ClsSectionId = @ClsSectionId
                                                             AND GR.Score IS NOT NULL
                                                    GROUP BY C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,C.Weight
                                                            ,C.Number
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter
                                                            ,X.GrdScaleId
                                                    ) S
                                           GROUP BY InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight
                                                   ,NumberOfComponents
                                           ) FinalTblToComputeCurrentScore
                                    );
                IF ( @CurrentScore IS NULL )
                    BEGIN
                        -- instructor grade books
                        SET @CurrentScore = (
                                            SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                        ELSE NULL
                                                   END AS CurrentScore
                                            FROM   (
                                                   SELECT   InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                   FROM     (
                                                            SELECT   C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,ISNULL(C.Weight, 0) AS Weight
                                                                    ,C.Number AS MinNumber
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter AS Param
                                                                    ,X.GrdScaleId
                                                                    ,SUM(GR.Score) AS Score
                                                                    ,COUNT(D.Descrip) AS NumberOfComponents
                                                            FROM     (
                                                                     --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
                                                                     --FROM          arGrdBkWeights A,arClassSections B        
                                                                     --WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
                                                                     --ORDER BY      A.EffectiveDate DESC
                                                                     SELECT DISTINCT TOP 1 t1.InstrGrdBkWgtId
                                                                                          ,t1.GrdScaleId
                                                                     FROM   arClassSections t1
                                                                           ,arGrdBkWeights t2
                                                                     WHERE  t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                            AND t1.ClsSectionId = @ClsSectionId
                                                                     ) X
                                                                    ,arGrdBkWgtDetails C
                                                                    ,arGrdComponentTypes D
                                                                    ,arGrdBkResults GR
                                                            WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                     AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                     AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                     AND
                                                                -- D.SysComponentTypeID not in (500,503) and 
                                                                GR.StuEnrollId = @StuEnrollId
                                                                     AND GR.ClsSectionId = @ClsSectionId
                                                                     AND GR.Score IS NOT NULL
                                                            GROUP BY C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,C.Weight
                                                                    ,C.Number
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter
                                                                    ,X.GrdScaleId
                                                            ) S
                                                   GROUP BY InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                   ) FinalTblToComputeCurrentScore
                                            );

                    END;


                -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                IF ( LOWER(@varGradeRounding) = ''yes'' )
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @FinalScore = ROUND(@FinalScore, 0);
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;
                ELSE
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;

                UPDATE syCreditSummary
                SET    FinalScore = @FinalScore
                      ,CurrentScore = @CurrentScore
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId
                       AND ReqId = @reqid;

                --Average calculation

                -- Term Average
                SET @TermAverageCount = (
                                        SELECT COUNT(*)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND TermId = @TermId
                                               AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                      SELECT SUM(FinalScore)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount;

                -- Cumulative Average
                SET @cumAveragecount = (
                                       SELECT COUNT(*)
                                       FROM   syCreditSummary
                                       WHERE  StuEnrollId = @StuEnrollId
                                              AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                     SELECT SUM(FinalScore)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount;

                UPDATE syCreditSummary
                SET    Average = @TermAverage
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId;

                --Update Cumulative GPA
                UPDATE syCreditSummary
                SET    CumAverage = @CumAverage
                WHERE  StuEnrollId = @StuEnrollId;


                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@FinalGrade
                    ,@sysComponentTypeId
                    ,@CreditsAttempted
                    ,@ClsSectionId
                    ,@Grade
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;


        DECLARE GetCreditsSummary_Cursor CURSOR FOR
            SELECT     DISTINCT SE.StuEnrollId
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate
                               ,R.ReqId
                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                               ,RES.Score AS FinalScore
                               ,RES.GrdSysDetailId AS FinalGrade
                               ,GCT.SysComponentTypeId
                               ,R.Credits AS CreditsAttempted
                               ,CS.ClsSectionId
                               ,GSD.Grade
                               ,GSD.IsPass
                               ,GSD.IsCreditsAttempted
                               ,GSD.IsCreditsEarned
                               ,SE.PrgVerId
                               ,GSD.IsInGPA
                               ,R.FinAidCredits AS FinAidCredits
            FROM       arStuEnrollments SE
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
            INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                             AND RES.TermId = CS.TermId
            INNER JOIN arTerm T ON CS.TermId = T.TermId
            INNER JOIN arReqs R ON CS.ReqId = R.ReqId
            LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
            LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
            LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
            LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            WHERE      SE.StuEnrollId = @StuEnrollId
            ORDER BY   T.StartDate
                      ,T.TermDescrip
                      ,R.ReqId;


        SET @varGradeRounding = (
                                SELECT Value
                                FROM   syConfigAppSetValues
                                WHERE  SettingId = 45
                                );

        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor
        INTO @StuEnrollId
            ,@TermId
            ,@TermDescrip
            ,@termstartdate1
            ,@reqid
            ,@CourseCodeDescrip
            ,@FinalScore
            ,@FinalGrade
            ,@sysComponentTypeId
            ,@CreditsAttempted
            ,@ClsSectionId
            ,@Grade
            ,@IsPass
            ,@IsCreditsAttempted
            ,@IsCreditsEarned
            ,@PrgVerId
            ,@IsInGPA
            ,@FinAidCredits;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                SET @CurrentScore = (
                                    SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                ELSE NULL
                                           END AS CurrentScore
                                    FROM   (
                                           SELECT   InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight AS GradeBookWeight
                                                   ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                         ELSE 0
                                                    END AS ActualWeight
                                           FROM     (
                                                    SELECT   C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,ISNULL(C.Weight, 0) AS Weight
                                                            ,C.Number AS MinNumber
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter AS Param
                                                            ,X.GrdScaleId
                                                            ,SUM(GR.Score) AS Score
                                                            ,COUNT(D.Descrip) AS NumberOfComponents
                                                    FROM     (
                                                             SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                    ,A.EffectiveDate
                                                                                    ,B.GrdScaleId
                                                             FROM     arGrdBkWeights A
                                                                     ,arClassSections B
                                                             WHERE    A.ReqId = B.ReqId
                                                                      AND A.EffectiveDate <= B.StartDate
                                                                      AND B.ClsSectionId = @ClsSectionId
                                                             ORDER BY A.EffectiveDate DESC
                                                             ) X
                                                            ,arGrdBkWgtDetails C
                                                            ,arGrdComponentTypes D
                                                            ,arGrdBkResults GR
                                                    WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                             AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                             AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                             AND D.SysComponentTypeId NOT IN ( 500, 503 )
                                                             AND GR.StuEnrollId = @StuEnrollId
                                                             AND GR.ClsSectionId = @ClsSectionId
                                                             AND GR.Score IS NOT NULL
                                                    GROUP BY C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,C.Weight
                                                            ,C.Number
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter
                                                            ,X.GrdScaleId
                                                    ) S
                                           GROUP BY InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight
                                                   ,NumberOfComponents
                                           ) FinalTblToComputeCurrentScore
                                    );
                IF ( @CurrentScore IS NULL )
                    BEGIN
                        -- instructor grade books
                        SET @CurrentScore = (
                                            SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                        ELSE NULL
                                                   END AS CurrentScore
                                            FROM   (
                                                   SELECT   InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                   FROM     (
                                                            SELECT   C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,ISNULL(C.Weight, 0) AS Weight
                                                                    ,C.Number AS MinNumber
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter AS Param
                                                                    ,X.GrdScaleId
                                                                    ,SUM(GR.Score) AS Score
                                                                    ,COUNT(D.Descrip) AS NumberOfComponents
                                                            FROM     (
                                                                     --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
                                                                     --FROM          arGrdBkWeights A,arClassSections B        
                                                                     --WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
                                                                     --ORDER BY      A.EffectiveDate DESC
                                                                     SELECT DISTINCT TOP 1 t1.InstrGrdBkWgtId
                                                                                          ,t1.GrdScaleId
                                                                     FROM   arClassSections t1
                                                                           ,arGrdBkWeights t2
                                                                     WHERE  t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                            AND t1.ClsSectionId = @ClsSectionId
                                                                     ) X
                                                                    ,arGrdBkWgtDetails C
                                                                    ,arGrdComponentTypes D
                                                                    ,arGrdBkResults GR
                                                            WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                     AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                     AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                     AND
                                                                -- D.SysComponentTypeID not in (500,503) and 
                                                                GR.StuEnrollId = @StuEnrollId
                                                                     AND GR.ClsSectionId = @ClsSectionId
                                                                     AND GR.Score IS NOT NULL
                                                            GROUP BY C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,C.Weight
                                                                    ,C.Number
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter
                                                                    ,X.GrdScaleId
                                                            ) S
                                                   GROUP BY InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                   ) FinalTblToComputeCurrentScore
                                            );

                    END;

                -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                IF ( LOWER(@varGradeRounding) = ''yes'' )
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @FinalScore = ROUND(@FinalScore, 0);
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;
                ELSE
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;

                UPDATE syCreditSummary
                SET    FinalScore = @FinalScore
                      ,CurrentScore = @CurrentScore
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId
                       AND ReqId = @reqid;

                --Average calculation
                --			declare @CumAverage decimal(18,2),@cumAverageSum decimal(18,2),@cumAveragecount int

                -- Term Average
                SET @TermAverageCount = (
                                        SELECT COUNT(*)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND TermId = @TermId
                                               AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                      SELECT SUM(FinalScore)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount;

                -- Cumulative Average
                SET @cumAveragecount = (
                                       SELECT COUNT(*)
                                       FROM   syCreditSummary
                                       WHERE  StuEnrollId = @StuEnrollId
                                              AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                     SELECT SUM(FinalScore)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount;

                UPDATE syCreditSummary
                SET    Average = @TermAverage
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId;

                --Update Cumulative GPA
                UPDATE syCreditSummary
                SET    CumAverage = @CumAverage
                WHERE  StuEnrollId = @StuEnrollId;


                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@FinalGrade
                    ,@sysComponentTypeId
                    ,@CreditsAttempted
                    ,@ClsSectionId
                    ,@Grade
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;

        DECLARE @GradeSystemDetailId UNIQUEIDENTIFIER
               ,@IsGPA BIT;
        DECLARE GetCreditsSummary_Cursor CURSOR FOR
            SELECT     DISTINCT SE.StuEnrollId
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate
                               ,R.ReqId
                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                               ,NULL AS FinalScore
                               ,RES.GrdSysDetailId AS GradeSystemDetailId
                               ,GSD.Grade AS FinalGrade
                               ,GSD.Grade AS CurrentGrade
                               ,R.Credits
                               ,NULL AS ClsSectionId
                               ,GSD.IsPass
                               ,GSD.IsCreditsAttempted
                               ,GSD.IsCreditsEarned
                               ,GSD.GPA
                               ,GSD.IsInGPA
                               ,SE.PrgVerId
                               ,GSD.IsInGPA
                               ,R.FinAidCredits AS FinAidCredits
            FROM       arStuEnrollments SE
            INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
            INNER JOIN syCreditSummary CS ON CS.StuEnrollId = RES.StuEnrollId
            INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            INNER JOIN arReqs R ON RES.ReqId = R.ReqId
            INNER JOIN arTerm T ON RES.TermId = T.TermId
            WHERE      RES.ReqId NOT IN (
                                        SELECT DISTINCT ReqId
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = SE.StuEnrollId
                                               AND TermId = T.TermId
                                        )
                       AND SE.StuEnrollId = @StuEnrollId;

        SET @varGradeRounding = (
                                SELECT Value
                                FROM   syConfigAppSetValues
                                WHERE  SettingId = 45
                                );

        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor
        INTO @StuEnrollId
            ,@TermId
            ,@TermDescrip
            ,@termstartdate1
            ,@reqid
            ,@CourseCodeDescrip
            ,@FinalScore
            ,@GradeSystemDetailId
            ,@FinalGrade
            ,@CurrentGrade
            ,@CourseCredits
            ,@ClsSectionId
            ,@IsPass
            ,@IsCreditsAttempted
            ,@IsCreditsEarned
            ,@GPA
            ,@IsGPA
            ,@PrgVerId
            ,@IsInGPA
            ,@FinAidCredits;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                IF ( LOWER(@varGradeRounding) = ''yes'' )
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @FinalScore = ROUND(@FinalScore, 0);
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;
                ELSE
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;

                UPDATE syCreditSummary
                SET    FinalScore = @FinalScore
                      ,CurrentScore = @CurrentScore
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId
                       AND ReqId = @reqid;

                --Average calculation

                -- Term Average
                SET @TermAverageCount = (
                                        SELECT COUNT(*)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND TermId = @TermId
                                               AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                      SELECT SUM(FinalScore)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount;

                -- Cumulative Average
                SET @cumAveragecount = (
                                       SELECT COUNT(*)
                                       FROM   syCreditSummary
                                       WHERE  StuEnrollId = @StuEnrollId
                                              AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                     SELECT SUM(FinalScore)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount;

                UPDATE syCreditSummary
                SET    Average = @TermAverage
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId;

                --Update Cumulative GPA
                UPDATE syCreditSummary
                SET    CumAverage = @CumAverage
                WHERE  StuEnrollId = @StuEnrollId;


                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@GradeSystemDetailId
                    ,@FinalGrade
                    ,@CurrentGrade
                    ,@CourseCredits
                    ,@ClsSectionId
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@GPA
                    ,@IsGPA
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;

        DECLARE GetCreditsSummary_Cursor CURSOR FOR
            SELECT     DISTINCT SE.StuEnrollId
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate
                               ,R.ReqId
                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                               ,NULL AS FinalScore
                               ,RES.GrdSysDetailId AS GradeSystemDetailId
                               ,GSD.Grade AS FinalGrade
                               ,GSD.Grade AS CurrentGrade
                               ,R.Credits
                               ,NULL AS ClsSectionId
                               ,GSD.IsPass
                               ,GSD.IsCreditsAttempted
                               ,GSD.IsCreditsEarned
                               ,GSD.GPA
                               ,GSD.IsInGPA
                               ,SE.PrgVerId
                               ,GSD.IsInGPA
                               ,R.FinAidCredits AS FinAidCredits
            FROM       arStuEnrollments SE
            INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
            INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            INNER JOIN arReqs R ON RES.ReqId = R.ReqId
            INNER JOIN arTerm T ON RES.TermId = T.TermId
            WHERE      SE.StuEnrollId NOT IN (
                                             SELECT DISTINCT StuEnrollId
                                             FROM   syCreditSummary
                                             )
                       AND SE.StuEnrollId = @StuEnrollId;

        SET @varGradeRounding = (
                                SELECT Value
                                FROM   syConfigAppSetValues
                                WHERE  SettingId = 45
                                );

        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor
        INTO @StuEnrollId
            ,@TermId
            ,@TermDescrip
            ,@termstartdate1
            ,@reqid
            ,@CourseCodeDescrip
            ,@FinalScore
            ,@GradeSystemDetailId
            ,@FinalGrade
            ,@CurrentGrade
            ,@CourseCredits
            ,@ClsSectionId
            ,@IsPass
            ,@IsCreditsAttempted
            ,@IsCreditsEarned
            ,@GPA
            ,@IsGPA
            ,@PrgVerId
            ,@IsInGPA
            ,@FinAidCredits;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                IF ( LOWER(@varGradeRounding) = ''yes'' )
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @FinalScore = ROUND(@FinalScore, 0);
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;
                ELSE
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;

                UPDATE syCreditSummary
                SET    FinalScore = @FinalScore
                      ,CurrentScore = @CurrentScore
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId
                       AND ReqId = @reqid;

                --Average calculation
                SET @TermAverageCount = (
                                        SELECT COUNT(*)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND TermId = @TermId
                                               AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                      SELECT SUM(FinalScore)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount;

                -- Cumulative Average
                SET @cumAveragecount = (
                                       SELECT COUNT(*)
                                       FROM   syCreditSummary
                                       WHERE  StuEnrollId = @StuEnrollId
                                              AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                     SELECT SUM(FinalScore)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount;

                UPDATE syCreditSummary
                SET    Average = @TermAverage
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId;

                --Update Cumulative GPA
                UPDATE syCreditSummary
                SET    CumAverage = @CumAverage
                WHERE  StuEnrollId = @StuEnrollId;


                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@GradeSystemDetailId
                    ,@FinalGrade
                    ,@CurrentGrade
                    ,@CourseCredits
                    ,@ClsSectionId
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@GPA
                    ,@IsGPA
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;


        SET @TermAverageCount = (
                                SELECT COUNT(*)
                                FROM   syCreditSummary
                                WHERE  StuEnrollId = @StuEnrollId
                                       AND FinalScore IS NOT NULL
                                );
        SET @termAverageSum = (
                              SELECT SUM(FinalScore)
                              FROM   syCreditSummary
                              WHERE  StuEnrollId = @StuEnrollId
                                     AND FinalScore IS NOT NULL
                              );
        IF @TermAverageCount >= 1
            BEGIN
                SET @TermAverage = @termAverageSum / @TermAverageCount;
            END;

        -- Cumulative Average
        SET @cumAveragecount = (
                               SELECT COUNT(*)
                               FROM   syCreditSummary
                               WHERE  StuEnrollId = @StuEnrollId
                                      AND FinalScore IS NOT NULL
                               );
        ---- PRINT @cumAveragecount

        SET @cumAverageSum = (
                             SELECT SUM(FinalScore)
                             FROM   syCreditSummary
                             WHERE  StuEnrollId = @StuEnrollId
                                    AND FinalScore IS NOT NULL
                             );
        ---- PRINT @CumAverageSum

        IF @cumAveragecount >= 1
            BEGIN
                SET @CumAverage = @cumAverageSum / @cumAveragecount;
            END;
        ---- PRINT @CumAverage

        ---- PRINT @UnitTypeId;

        ---- PRINT @TrackSapAttendance;
        ---- PRINT @displayHours;

        DECLARE @UseWeightForGPA INT = 0;
        SET @UseWeightForGPA = (
                               SELECT TOP 1 PV.DoCourseWeightOverallGPA
                               FROM   dbo.arStuEnrollments E
                               JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                               WHERE  E.StuEnrollId = @StuEnrollId
                               );

        IF @UseWeightForGPA = 1
            BEGIN
                DECLARE @TotalWeight DECIMAL = (
                                               SELECT SUM(PVD.CourseWeight)
                                               FROM   dbo.syCreditSummary CS
                                               JOIN   dbo.arProgVerDef PVD ON PVD.ReqId = CS.ReqId
                                               WHERE  StuEnrollId = @StuEnrollId
                                               );

                DECLARE @ExpectedCoursesGraded INT = (
                                                     SELECT COUNT(CS.ReqId)
                                                     FROM   dbo.syCreditSummary CS
                                                     JOIN   dbo.arProgVerDef PVD ON PVD.ReqId = CS.ReqId
                                                     WHERE  StuEnrollId = @StuEnrollId
                                                     );
                DECLARE @CoursesGraded INT = (
                                             SELECT COUNT(CS.ReqId)
                                             FROM   dbo.syCreditSummary CS
                                             JOIN   dbo.arProgVerDef PVD ON PVD.ReqId = CS.ReqId
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND CS.CurrentScore IS NOT NULL
                                                    AND CS.Completed = 1
                                             );

                SET @CumAverage = CASE WHEN @TotalWeight = 100
                                            AND @ExpectedCoursesGraded = @CoursesGraded THEN (
                                                                                             SELECT SUM(PVD.CourseWeight / 100 * CS.CurrentScore)
                                                                                             FROM   dbo.syCreditSummary CS
                                                                                             JOIN   dbo.arProgVerDef PVD ON PVD.ReqId = CS.ReqId
                                                                                             WHERE  StuEnrollId = @StuEnrollId
                                                                                             )
                                       ELSE ((
                                             SELECT SUM(CS.CurrentScore)
                                             FROM   dbo.syCreditSummary CS
                                             JOIN   dbo.arProgVerDef PVD ON PVD.ReqId = CS.ReqId
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND CS.CurrentScore IS NOT NULL
                                             ) / (
                                                 SELECT COUNT(CS.CurrentScore)
                                                 FROM   dbo.syCreditSummary CS
                                                 JOIN   dbo.arProgVerDef PVD ON PVD.ReqId = CS.ReqId
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND CS.CurrentScore IS NOT NULL
                                                 )
                                            )
                                  END;
            END;

		--If Clock Hour / Numeric - use New Average Calculation
		IF (
		   SELECT TOP 1 P.ACId
		   FROM   dbo.arStuEnrollments E
		   JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
		   JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
		   WHERE  E.StuEnrollId = @StuEnrollId
		   ) = 5
		   AND @GradesFormat = ''numeric''
			BEGIN
				EXEC dbo.USP_GPACalculator @EnrollmentId = @StuEnrollId
                                          ,@StudentGPA = @CumAverage OUTPUT;
			END;

        -- Main query starts here 
        SELECT     DISTINCT TOP 1 S.SSN
                                 ,S.FirstName
                                 ,S.LastName
                                 ,SC.StatusCodeDescrip
                                 ,SE.StartDate AS StudentStartDate
                                 ,SE.ExpGradDate AS StudentExpectedGraduationDate
                                 ,PV.PrgVerId AS PrgVerId
                                 ,PV.PrgVerDescrip AS PrgVerDescrip
                                 ,PV.UnitTypeId AS UnitTypeId
                                 ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                       ELSE 0
                                  END AS PrgVersionTrackCredits
                                 ,AAUT1.UnitTypeDescrip AS UnitTypeDescripFrom_arAttUnitType
                                 ,CASE WHEN @TermStartDate IS NULL THEN (
                                                                        SELECT MAX(LDA)
                                                                        FROM   (
                                                                               SELECT MAX(AttendedDate) AS LDA
                                                                               FROM   arExternshipAttendance
                                                                               WHERE  StuEnrollId = @StuEnrollId
                                                                               UNION ALL
                                                                               SELECT MAX(MeetDate) AS LDA
                                                                               FROM   atClsSectAttendance
                                                                               WHERE  StuEnrollId = @StuEnrollId
                                                                                      AND Actual > 0
                                                                                      AND Actual <> 99.00
                                                                                      AND Actual <> 999.00
                                                                                      AND Actual <> 9999.00
                                                                               UNION ALL
                                                                               --SELECT    MAX(AttendanceDate) AS LDA
                                                                               --FROM      atAttendance
                                                                               --WHERE     EnrollId = @StuEnrollId
                                                                               --          AND Actual > 0
                                                                               --UNION ALL
                                                                               SELECT MAX(RecordDate) AS LDA
                                                                               FROM   arStudentClockAttendance
                                                                               WHERE  StuEnrollId = @StuEnrollId
                                                                                      AND (
                                                                                          ActualHours > 0
                                                                                          AND ActualHours <> 99.00
                                                                                          AND ActualHours <> 999.00
                                                                                          AND ActualHours <> 9999.00
                                                                                          )
                                                                               UNION ALL
                                                                               SELECT MAX(MeetDate) AS LDA
                                                                               FROM   atConversionAttendance
                                                                               WHERE  StuEnrollId = @StuEnrollId
                                                                                      AND (
                                                                                          Actual > 0
                                                                                          AND Actual <> 99.00
                                                                                          AND Actual <> 999.00
                                                                                          AND Actual <> 9999.00
                                                                                          )
                                                                               UNION ALL
                                                                               SELECT TOP 1 LDA
                                                                               FROM   arStuEnrollments
                                                                               WHERE  StuEnrollId = @StuEnrollId
                                                                               ) TR
                                                                        )
                                       ELSE
                                  -- Get the LDA Based on Term Start Date
                                  (
                                  SELECT MAX(LDA)
                                  FROM   (
                                         SELECT TOP 1 LDA
                                         FROM   arStuEnrollments
                                         WHERE  StuEnrollId = @StuEnrollId
                                         UNION ALL
                                         SELECT MAX(AttendedDate) AS LDA
                                         FROM   arExternshipAttendance
                                         WHERE  StuEnrollId = @StuEnrollId
                                         UNION ALL
                                         SELECT MAX(MeetDate) AS LDA
                                         FROM   atClsSectAttendance t1
                                               ,arClassSections t2
                                         WHERE  t1.ClsSectionId = t2.ClsSectionId
                                                AND StuEnrollId = @StuEnrollId
                                                AND Actual > 0
                                                AND Actual <> 99.00
                                                AND Actual <> 999.00
                                                AND Actual <> 9999.00
                                         UNION ALL
                                         SELECT MAX(RecordDate) AS LDA
                                         FROM   arStudentClockAttendance
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND (
                                                    ActualHours > 0
                                                    AND ActualHours <> 99.00
                                                    AND ActualHours <> 999.00
                                                    AND ActualHours <> 9999.00
                                                    )
                                         UNION ALL
                                         SELECT MAX(MeetDate) AS LDA
                                         FROM   atConversionAttendance
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND (
                                                    Actual > 0
                                                    AND Actual <> 99.00
                                                    AND Actual <> 999.00
                                                    AND Actual <> 9999.00
                                                    )
                                         ) TR
                                  )
                                  END AS StudentLastDateAttended
                                 ,( CASE WHEN ( AAUT1.UnitTypeId = ''2600592A-9739-4A13-BDCE-7A25FE4A7478'' ) -- NONE
                                 THEN        0.00
                                         ELSE (
                                              SELECT     SUM(ISNULL(APSD.total, 0))
                                              FROM       arProgScheduleDetails AS APSD
                                              INNER JOIN arProgSchedules AS APS ON APS.ScheduleId = APSD.ScheduleId
                                              INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = APS.PrgVerId
                                              INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                                              INNER JOIN dbo.arStudentSchedules SS ON SS.StuEnrollId = ASE.StuEnrollId
                                                                                      AND SS.ScheduleId = APSD.ScheduleId
                                              WHERE      ASE.StuEnrollId = @StuEnrollId
                                              )
                                    END
                                  ) AS WeeklySchedule
                                 ,CASE WHEN (
                                            @TrackSapAttendance = ''byclass''
                                            AND @displayHours = ''hours''
                                            AND AAUT1.UnitTypeDescrip = ''Present Absent''
                                            ) -- PA
                                 THEN      @ActualPresentDays_ConvertTo_Hours
                                       ELSE CASE WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AttendedDays / 60
                                                          ELSE SAS_ForTerm.AttendedDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.AttendedDays / 60 )
                                                          ELSE ( SAS_ForTerm.AttendedDays / 60 )
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.AttendedDays / 60 )
                                                          ELSE ( SAS_ForTerm.AttendedDays / 60 )
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AttendedDays / 60
                                                          ELSE SAS_ForTerm.AttendedDays / 60
                                                     END
                                                 ELSE CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AttendedDays
                                                           ELSE SAS_ForTerm.AttendedDays
                                                      END
                                            END
                                  END AS TotalDaysAttended
                                 ,CASE WHEN (
                                            @TrackSapAttendance = ''byclass''
                                            AND @displayHours = ''hours''
                                            AND AAUT1.UnitTypeDescrip = ''Present Absent''
                                            ) THEN @Scheduledhours
                                       ELSE CASE WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                                                          ELSE SAS_ForTerm.ScheduledDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                                                          ELSE SAS_ForTerm.ScheduledDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                                                          ELSE SAS_ForTerm.ScheduledDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                                                          ELSE SAS_ForTerm.ScheduledDays / 60
                                                     END
                                                 ELSE CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.ScheduledDays )
                                                           ELSE ( SAS_ForTerm.ScheduledDays )
                                                      END
                                            END
                                  END AS ScheduledDays
                                 ,CASE WHEN (
                                            @TrackSapAttendance = ''byclass''
                                            AND @displayHours = ''hours''
                                            AND AAUT1.UnitTypeDescrip = ''Present Absent''
                                            ) THEN @ActualAbsentDays_ConvertTo_Hours
                                       ELSE CASE WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AbsentDays / 60
                                                          ELSE SAS_ForTerm.AbsentDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AbsentDays / 60
                                                          ELSE SAS_ForTerm.AbsentDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AbsentDays / 60
                                                          ELSE SAS_ForTerm.AbsentDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AbsentDays / 60
                                                          ELSE SAS_ForTerm.AbsentDays / 60
                                                     END
                                                 ELSE CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.AbsentDays )
                                                           ELSE ( SAS_ForTerm.AbsentDays )
                                                      END
                                            END
                                  END AS DaysAbsent
                                 ,CASE WHEN (
                                            @TrackSapAttendance = ''byclass''
                                            AND LOWER(RTRIM(LTRIM(@displayHours))) = ''hours''
                                            AND AAUT1.UnitTypeDescrip = ''Present Absent''
                                            ) THEN SAS_ForTerm.MakeupDays
                                       ELSE CASE WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.MakeupDays / 60
                                                          ELSE SAS_ForTerm.MakeupDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.MakeupDays / 60
                                                          ELSE SAS_ForTerm.MakeupDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.MakeupDays / 60
                                                          ELSE SAS_ForTerm.MakeupDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.MakeupDays / 60
                                                          ELSE SAS_ForTerm.MakeupDays / 60
                                                     END
                                                 ELSE CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.MakeupDays )
                                                           ELSE ( SAS_ForTerm.MakeupDays )
                                                      END
                                            END
                                  END AS MakeupDays
                                 ,SE.StuEnrollId
                                 ,@SUMCreditsAttempted AS CreditsAttempted
                                 ,@SUMCreditsEarned AS CreditsEarned
                                 ,@SUMFACreditsEarned AS FACreditsEarned
                                 ,SE.DateDetermined AS DateDetermined
                                 ,SYS.InSchool
                                 ,P.ProgDescrip
                                 ,(
                                  SELECT SUM(ISNULL(TransAmount, 0))
                                  FROM   saTransactions t1
                                        ,saTransCodes t2
                                  WHERE  t1.TransCodeId = t2.TransCodeId
                                         AND t1.StuEnrollId = @StuEnrollId
                                         AND t2.IsInstCharge = 1
                                         AND t1.TransTypeId IN ( 0, 1 )
                                         AND t1.Voided = 0
                                  ) AS TotalCost
                                 ,(
                                  SELECT SUM(ISNULL(TransAmount, 0)) AS CurrentBalance
                                  FROM   saTransactions
                                  WHERE  StuEnrollId = @StuEnrollId
                                         AND Voided = 0
                                  ) AS CurrentBalance
                                 ,@CumAverage AS OverallAverage
                                 ,@cumWeightedGPA AS WeightedAverage_CumGPA
                                 ,@cumSimpleGPA AS SimpleAverage_CumGPA
                                 ,@ReturnValue AS StudentGroups
                                 ,CASE WHEN P.ACId = 5 THEN ''True''
                                       ELSE ''False''
                                  END AS ClockHourProgram
                                 ,CASE WHEN AAUT1.UnitTypeDescrip = ''Present Absent'' --LTRIM(RTRIM(PV.UnitTypeId)) = ''EF5535C2-142C-4223-AE3C-25A50A153CC6''    Present Absent
                                            AND @displayHours = ''hours'' THEN ''Hours''
                                       WHEN AAUT1.UnitTypeDescrip = ''Present Absent'' --LTRIM(RTRIM(PV.UnitTypeId)) = ''EF5535C2-142C-4223-AE3C-25A50A153CC6''      Present Absent
                                            AND @displayHours <> ''hours'' THEN ''Days''
                                       ELSE ''Hours''
                                  END AS UnitTypeDescrip
                                 ,SE.TransferHours AS TransferHours
                                 ,SE.ContractedGradDate AS ContractedGradDate
        FROM       arStudent S
        INNER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
        INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = PV.UnitTypeId
        INNER JOIN arPrograms P ON PV.ProgId = P.ProgId
        INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
        INNER JOIN sySysStatus SYS ON SC.SysStatusId = SYS.SysStatusId
        LEFT JOIN  (
                   SELECT   StuEnrollId
                           ,MAX(ActualRunningScheduledDays) AS ScheduledDays
                           ,MAX(AdjustedPresentDays) AS AttendedDays
                           ,MAX(AdjustedAbsentDays) AS AbsentDays
                           ,MAX(ActualRunningMakeupDays) AS MakeupDays
                           ,MAX(StudentAttendedDate) AS LDA
                   FROM     syStudentAttendanceSummary
                   GROUP BY StuEnrollId
                   ) SAS_NoTerm ON SE.StuEnrollId = SAS_NoTerm.StuEnrollId
        LEFT JOIN  (
                   SELECT   StuEnrollId
                           ,MAX(ActualRunningScheduledDays) AS ScheduledDays
                           ,MAX(AdjustedPresentDays) AS AttendedDays
                           ,MAX(AdjustedAbsentDays) AS AbsentDays
                           ,MAX(ActualRunningMakeupDays) AS MakeupDays
                           ,MAX(StudentAttendedDate) AS LDA
                   FROM     syStudentAttendanceSummary
                   GROUP BY StuEnrollId
                   ) SAS_ForTerm ON SE.StuEnrollId = SAS_ForTerm.StuEnrollId
        WHERE      ( SE.StuEnrollId = @StuEnrollId );
    END;
-- =========================================================================================================
-- END  --  Usp_PR_Sub2_Enrollment_Summary
-- =========================================================================================================

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[USP_ProcessPaymentPeriodsHours]'
GO
IF OBJECT_ID(N'[dbo].[USP_ProcessPaymentPeriodsHours]', 'P') IS NOT NULL
EXEC sp_executesql N'--================================================================================================= 
-- USP_ProcessPaymentPeriodsHours 
--================================================================================================= 
-- ============================================= 
-- Author:		Ginzo, John 
-- Create date: 11/17/2014 
-- ============================================= 
ALTER PROCEDURE [dbo].[USP_ProcessPaymentPeriodsHours]
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from 
        -- interfering with SELECT statements. 
        SET NOCOUNT ON;


        BEGIN TRAN PAYMENTPERIODTRANSACTION;

        ----------------------------------------------------------------- 
        --Table to store Student population 
        ----------------------------------------------------------------- 
        DECLARE @MasterStudentPopulation TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ClsSectionId VARCHAR(250)
               ,MaxDate DATETIME
               ,ScheduledHours DECIMAL
               ,MakeUpHours DECIMAL
               ,ActualHours DECIMAL
               ,TermId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,PrgVerCode VARCHAR(250)
               ,StartDate DATETIME
               ,EndDate DATETIME
            );

        DECLARE @MasterStudentPopulationRunningTotal TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ClsSectionId VARCHAR(250)
               ,PrgVerId UNIQUEIDENTIFIER
               ,MeetingDate DATETIME
               ,TermId UNIQUEIDENTIFIER
               ,ScheduledHoursRunningTotal DECIMAL(18, 2)
               ,ActualHoursRunningTotal DECIMAL(18, 2)
               ,IsExcused BIT
               ,ExcusedHours DECIMAL(18, 2)
               ,ScheduledDays DECIMAL(18, 2)
               ,ActualDays DECIMAL(18, 2)
            );


        ----------------------------------------------------------------- 
        -- TABLE FOR HOURS DATA 
        ----------------------------------------------------------------- 
        DECLARE @HoursScheduledActual TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,ScheduledHours DECIMAL
               ,ActualHours DECIMAL
               ,MakeUpHours DECIMAL
            );


        DECLARE @HoursExcused TABLE
            (
                PmtPeriodId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,PrgVerId UNIQUEIDENTIFIER
               ,ExcusedHours DECIMAL(18, 2)
            );

        DECLARE @TransactionDates TABLE
            (
                PmtPeriodId UNIQUEIDENTIFIER
               ,StuEnrollId UNIQUEIDENTIFIER
               ,TransactionDate DATETIME
            );
        ----------------------------------------------------------------- 
        -- Insert student population into table 
        ----------------------------------------------------------------- 
        INSERT INTO @MasterStudentPopulation
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,CS.ClsSectionId
                                       ,MD.MaxDate
                                       ,SH.ScheduledHours
                                       ,MUH.MakeUpHours
                                       ,AH.ActualHours
                                       ,TT.TermId
                                       ,PV.PrgVerId
                                       ,PV.PrgVerCode
                                       ,TT.StartDate
                                       ,TT.EndDate
                    FROM       dbo.arStuEnrollments SE
                    INNER JOIN dbo.syStudentAttendanceSummary SA ON SE.StuEnrollId = SA.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningScheduledDays) AS ScheduledHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) SH ON SA.StuEnrollId = SH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningPresentDays) AS ActualHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) AH ON SE.StuEnrollId = AH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningMakeupDays) AS MakeUpHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) MUH ON SE.StuEnrollId = MUH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(StudentAttendedDate) AS MaxDate
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) MD ON SE.StuEnrollId = MD.StuEnrollId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arClassSections CS ON SA.ClsSectionId = CS.ClsSectionId
                    INNER JOIN dbo.arTerm TT ON CS.TermId = TT.TermId
                    INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                    INNER JOIN dbo.syStatuses ST ON PV.StatusId = ST.StatusId
                    WHERE      SC.SysStatusId IN ( 9, 20 )
                               --removed due to this condition being required for by term or course charges and auto post is for payment period charging
                               --AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(SE.StuEnrollId, CS.ClsSectionId) --Added by Troy as part of fix for US8005 
                               AND SE.DisableAutoCharge <> 1 --Exclude enrollments with auto charge disabled  
                               AND PV.ProgramRegistrationType = 0
                    --AND pv.PrgVerId=''5E8BFC2C-FE62-4220-BDC6-51C7941F5AE0'' 
                    --AND se.StuEnrollId=''71A812F7-972D-4AB6-971C-3CFD02A3086B'' 

                    UNION
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,CS.ClsSectionId
                                       ,MD.MaxDate
                                       ,SH.ScheduledHours
                                       ,MUH.MakeUpHours
                                       ,AH.ActualHours
                                       ,TT.TermId
                                       ,PV.PrgVerId
                                       ,PV.PrgVerCode
                                       ,TT.StartDate
                                       ,TT.EndDate
                    FROM       dbo.arStuEnrollments SE
                    INNER JOIN dbo.syStudentAttendanceSummary SA ON SE.StuEnrollId = SA.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningScheduledDays) AS ScheduledHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) SH ON SA.StuEnrollId = SH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningPresentDays) AS ActualHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) AH ON SE.StuEnrollId = AH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(ActualRunningMakeupDays) AS MakeUpHours
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) MUH ON SE.StuEnrollId = MUH.StuEnrollId
                    INNER JOIN (
                               SELECT   StuEnrollId
                                       ,MAX(StudentAttendedDate) AS MaxDate
                               FROM     dbo.syStudentAttendanceSummary
                               GROUP BY StuEnrollId
                               ) MD ON SE.StuEnrollId = MD.StuEnrollId
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arTerm TT ON TT.ProgramVersionId = PV.PrgVerId
                    INNER JOIN dbo.arProgVerDef pvd ON pvd.PrgVerId = PV.PrgVerId
                    INNER JOIN dbo.arClassSections CS ON CS.ProgramVersionDefinitionId = pvd.ProgVerDefId
                    INNER JOIN dbo.syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
                    INNER JOIN dbo.syStatuses ST ON PV.StatusId = ST.StatusId
                    WHERE      SC.SysStatusId IN ( 9, 20 )
                               --removed due to this condition being required for by term or course charges and auto post is for payment period charging
                               --AND 1 = dbo.UDF_ShouldClassBeChargedOnThisEnrollment(SE.StuEnrollId, CS.ClsSectionId) --Added by Troy as part of fix for US8005 
                               AND SE.DisableAutoCharge <> 1 --Exclude enrollments with auto charge disabled  
                               AND PV.ProgramRegistrationType = 1;
        --AND pv.PrgVerId=''5E8BFC2C-FE62-4220-BDC6-51C7941F5AE0'' 
        --AND se.StuEnrollId=''71A812F7-972D-4AB6-971C-3CFD02A3086B'' 


        -- Get Running Totals for Current Term 
        INSERT INTO @MasterStudentPopulationRunningTotal
                    SELECT     DISTINCT SAS.StuEnrollId
                                       ,SAS.ClsSectionId
                                       ,SE.PrgVerId
                                       ,SAS.StudentAttendedDate
                                       ,(
                                        SELECT     TOP 1 TermId
                                        FROM       arResults R
                                        INNER JOIN arClassSections CS ON R.TestId = CS.ClsSectionId
                                        WHERE      R.StuEnrollId = SAS.StuEnrollId
                                                   AND CS.ClsSectionId = ClsSectionId
                                        ) AS termId
                                       ,SAS.ActualRunningScheduledDays
                                       ,SAS.AdjustedPresentDays
                                       ,SAS.IsExcused
                                       ,( CASE WHEN IsExcused = 1 THEN ( SAS.ScheduledDays - SAS.ActualDays )
                                               ELSE 0
                                          END
                                        ) AS ExcusedHours
                                       ,SAS.ScheduledDays
                                       ,SAS.ActualDays
                    FROM       syStudentAttendanceSummary SAS
                    INNER JOIN dbo.arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                    WHERE      SAS.StuEnrollId IN (
                                                  SELECT DISTINCT StuEnrollId
                                                  FROM   @MasterStudentPopulation
                                                  )
                    ORDER BY   SAS.StuEnrollId
                              ,StudentAttendedDate;


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error creating Student Population.'', 16, 1);
                RETURN;
            END;






        ----------------------------------------------------------------- 
        -- insert hours data into table 
        ----------------------------------------------------------------- 
        INSERT INTO @HoursScheduledActual
                    SELECT   StuEnrollId
                            ,PrgVerId
                            ,ScheduledHours
                            ,ActualHours
                            ,MakeUpHours
                    FROM     @MasterStudentPopulation
                    GROUP BY StuEnrollId
                            ,PrgVerId
                            ,ScheduledHours
                            ,ActualHours
                            ,MakeUpHours;


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error creating hours data.'', 16, 1);
                RETURN;
            END;




        ----------------------------------------------------------------------------------- 
        -- Cursor to handle Excused Hours 
        ----------------------------------------------------------------------------------- 
        DECLARE @pmtperiodid UNIQUEIDENTIFIER;
        DECLARE @CumulativeValue DECIMAL;
        DECLARE @StuEnrollId UNIQUEIDENTIFIER;
        DECLARE @NextCumulativeValue DECIMAL;




        DECLARE db_cursor CURSOR FOR
            SELECT     p.PmtPeriodId
                      ,p.CumulativeValue
                      ,e.StuEnrollId
                      ,(
                       SELECT   TOP 1 child.CumulativeValue
                       FROM     saPmtPeriods child
                       WHERE    child.IncrementId = p.IncrementId
                                AND child.PeriodNumber = p.PeriodNumber + 1
                       ORDER BY p.PeriodNumber ASC
                               ,p.CumulativeValue ASC
                       ) AS NextCumulativeValue
            FROM       dbo.saPmtPeriods p
            INNER JOIN dbo.saIncrements I ON p.IncrementId = I.IncrementId
            INNER JOIN dbo.saBillingMethods B ON I.BillingMethodId = B.BillingMethodId
            INNER JOIN dbo.arPrgVersions prg ON B.BillingMethodId = prg.BillingMethodId
            INNER JOIN dbo.arStuEnrollments e ON prg.PrgVerId = e.PrgVerId
            WHERE      I.IncrementType = 0
            ORDER BY   e.StuEnrollId
                      ,e.PrgVerId
                      ,I.IncrementId
                      ,p.PeriodNumber;

        DECLARE @FloorValue DECIMAL;
        SET @FloorValue = 0;

        DECLARE @currentEnrollId UNIQUEIDENTIFIER;
        SET @currentEnrollId = NEWID();

        DECLARE @curCumValue DECIMAL;
        SET @curCumValue = 0;

        OPEN db_cursor;

        FETCH NEXT FROM db_cursor
        INTO @pmtperiodid
            ,@CumulativeValue
            ,@StuEnrollId
            ,@NextCumulativeValue;


        WHILE @@FETCH_STATUS = 0
            BEGIN

                IF @StuEnrollId <> @currentEnrollId
                    BEGIN
                        SET @FloorValue = 0;
                        SET @curCumValue = @CumulativeValue;
                        SET @currentEnrollId = @StuEnrollId;
                    END;
                ELSE
                    BEGIN
                        SET @FloorValue = @curCumValue;
                    END;


                --SELECT @FloorValue,@CumulativeValue,@currentEnrollId,@StuEnrollId 

                INSERT INTO @HoursExcused
                            SELECT   @pmtperiodid AS pmtperiodid
                                    ,StuEnrollId AS StuEnrollId
                                    ,PrgVerId
                                    ,SUM(ExcusedHours) AS ExcusedHours
                            FROM     @MasterStudentPopulationRunningTotal
                            WHERE    StuEnrollId = @StuEnrollId
                                     AND ActualHoursRunningTotal > @FloorValue
                                     AND (
                                         ( ActualHoursRunningTotal <= @CumulativeValue )
                                         OR ( ActualHoursRunningTotal
                                     BETWEEN @CumulativeValue AND @NextCumulativeValue
                                            )
                                         OR ( ActualHoursRunningTotal >= @CumulativeValue )
                                         )
                            GROUP BY StuEnrollId
                                    ,PrgVerId;



                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error creating excused hours data.'', 16, 1);
                        RETURN;
                    END;



                FETCH NEXT FROM db_cursor
                INTO @pmtperiodid
                    ,@CumulativeValue
                    ,@StuEnrollId
                    ,@NextCumulativeValue;

            END;

        CLOSE db_cursor;
        DEALLOCATE db_cursor;
        ----------------------------------------------------------------------------------- 

        --for testing 
        --SELECT * FROM @MasterStudentPopulation 
        --SELECT * FROM @MasterStudentPopulationRunningTotal 
        --SELECT * FROM @HoursScheduledActual	 
        --SELECT * FROM @HoursExcused 




        DECLARE @BatchNumber INT;
        SET @BatchNumber = (
                           SELECT ISNULL(MAX(PmtPeriodBatchHeaderId), 0) + 1
                           FROM   saPmtPeriodBatchHeaders
                           );

        INSERT INTO saPmtPeriodBatchHeaders (
                                            BatchName
                                            )
                    SELECT ''Batch '' + CAST(@BatchNumber AS VARCHAR(20)) + '' (Hours) - '' + LEFT(CONVERT(VARCHAR, GETDATE(), 101), 10) + '' ''
                           + LEFT(CONVERT(VARCHAR, GETDATE(), 108), 10);


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error creating batch header.'', 16, 1);
                RETURN;
            END;

        DECLARE @InsertedHeaderId INT;
        SET @InsertedHeaderId = SCOPE_IDENTITY();





        INSERT INTO saPmtPeriodBatchItems (
                                          PmtPeriodId
                                         ,PmtPeriodBatchHeaderId
                                         ,StuEnrollId
                                         ,ChargeAmount
                                         ,CreditsHoursValue
                                         ,TransactionReference
                                         ,TransactionDescription
                                         ,ModUser
                                         ,ModDate
                                         ,TermId
                                          )
                    SELECT     DISTINCT ( p.PmtPeriodId )
                                       ,@InsertedHeaderId
                                       ,e.StuEnrollId
                                       ,p.ChargeAmount
                                       ,( CASE WHEN i.IncrementType = 0 THEN CAE.ActualHours + ( HE.ExcusedHours * i.ExcAbscenesPercent )
                                               WHEN i.IncrementType = 1 THEN CAE.ScheduledHours
                                               ELSE 0
                                          END
                                        ) AS CreditsHoursValue
                                       ,''Payment Period '' + CAST(p.PeriodNumber AS VARCHAR(5)) AS TransactionReference
                                       ,''Tuition: '' + CAST(p.CumulativeValue AS VARCHAR(20)) + '' '' + i.IncrementName AS TransactionDescription
                                       ,''AutoPost'' AS ModUser
                                       ,GETDATE() AS ModDate
                                       ,( CASE WHEN i.IncrementType = 0 THEN (
                                                                             SELECT TOP 1 TermId
                                                                             FROM   @MasterStudentPopulationRunningTotal
                                                                             WHERE  StuEnrollId = e.StuEnrollId
                                                                                    AND ActualHoursRunningTotal >= p.CumulativeValue
                                                                             )
                                               WHEN i.IncrementType = 1 THEN (
                                                                             SELECT TOP 1 TermId
                                                                             FROM   @MasterStudentPopulationRunningTotal
                                                                             WHERE  StuEnrollId = e.StuEnrollId
                                                                                    AND ScheduledHoursRunningTotal >= p.CumulativeValue
                                                                             )
                                               ELSE NULL
                                          END
                                        ) AS TermId
                    FROM       saPmtPeriods p
                    INNER JOIN dbo.saIncrements i ON p.IncrementId = i.IncrementId
                    INNER JOIN dbo.saBillingMethods B ON i.BillingMethodId = B.BillingMethodId
                    INNER JOIN dbo.arPrgVersions prg ON B.BillingMethodId = prg.BillingMethodId
                    INNER JOIN dbo.arStuEnrollments e ON prg.PrgVerId = e.PrgVerId
                    --INNER JOIN dbo.arStudent st ON e.StudentId = st.StudentId
                    INNER JOIN @HoursScheduledActual CAE ON e.StuEnrollId = CAE.StuEnrollId
                    LEFT JOIN  @HoursExcused HE ON p.PmtPeriodId = HE.PmtPeriodId
                                                   AND e.StuEnrollId = HE.StuEnrollId
                                                   AND e.PrgVerId = HE.PrgVerId
                    WHERE      B.BillingMethod = 3
                               AND e.StartDate >= i.EffectiveDate
                               AND ( CASE WHEN i.IncrementType = 0 THEN CAE.ActualHours + ( HE.ExcusedHours * i.ExcAbscenesPercent ) + CAE.MakeUpHours
                                          WHEN i.IncrementType = 1 THEN CAE.ScheduledHours
                                          ELSE 0
                                     END
                                   ) >= p.CumulativeValue
                               AND e.StuEnrollId NOT IN (
                                                        SELECT StuEnrollId
                                                        FROM   dbo.saTransactions
                                                        WHERE  StuEnrollId = e.StuEnrollId
                                                               AND PmtPeriodId = p.PmtPeriodId
                                                               OR p.PeriodNumber = PaymentPeriodNumber
                                                        )
                               AND e.StuEnrollId NOT IN (
                                                        SELECT     i.StuEnrollId
                                                        FROM       dbo.saPmtPeriodBatchItems i
                                                        INNER JOIN saPmtPeriodBatchHeaders h ON i.PmtPeriodBatchHeaderId = h.PmtPeriodBatchHeaderId
                                                        WHERE      StuEnrollId = e.StuEnrollId
                                                                   AND PmtPeriodId = p.PmtPeriodId
                                                                   AND h.IsPosted = 0
                                                        );


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error creating batch items.'', 16, 1);
                RETURN;
            END;



        ----------------------------------------------------------------- 
        -- UPDATE Batch Item count in header table 
        ----------------------------------------------------------------- 
        UPDATE saPmtPeriodBatchHeaders
        SET    BatchItemCount = (
                                SELECT     COUNT(*)
                                FROM       saPmtPeriodBatchHeaders h
                                INNER JOIN saPmtPeriodBatchItems i ON h.PmtPeriodBatchHeaderId = i.PmtPeriodBatchHeaderId
                                WHERE      h.PmtPeriodBatchHeaderId = @InsertedHeaderId
                                )
        WHERE  PmtPeriodBatchHeaderId = @InsertedHeaderId;

        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error updating batch count.'', 16, 1);
                RETURN;
            END;
        ----------------------------------------------------------------- 

        ----------------------------------------------------------------- 
        -- GET AutoPost config setting 
        ----------------------------------------------------------------- 
        DECLARE @IsAutoPost VARCHAR(50);
        SET @IsAutoPost = (
                          SELECT     TOP 1 v.Value
                          FROM       dbo.syConfigAppSettings c
                          INNER JOIN dbo.syConfigAppSetValues v ON c.SettingId = v.SettingId
                          WHERE      c.KeyName = ''PaymentPeriodAutoPost''
                          );
        ----------------------------------------------------------------- 



        ------------------------------------------------------------------- 
        ---- INSERT Transactions from Batch 
        ------------------------------------------------------------------- 
        IF @IsAutoPost = ''yes''
            BEGIN

                IF OBJECTPROPERTY(OBJECT_ID(''Trigger_Insert_Check_saTransactions''), ''IsTrigger'') = 1
                    BEGIN
                        ALTER TABLE dbo.saTransactions DISABLE TRIGGER Trigger_Insert_Check_saTransactions;
                    END;



                INSERT INTO @TransactionDates (
                                              PmtPeriodId
                                             ,StuEnrollId
                                             ,TransactionDate
                                              )
                            SELECT      ppCumulative.PmtPeriodId
                                       ,ppCumulative.StuEnrollId
                                       ,transDate.StudentAttendedDate
                            FROM        (
                                        SELECT     DISTINCT pp.PmtPeriodId
                                                           ,StuEnrollId
                                                           ,pp.CumulativeValue
                                        FROM       dbo.saPmtPeriodBatchItems ppbi
                                        INNER JOIN dbo.saPmtPeriods pp ON pp.PmtPeriodId = ppbi.PmtPeriodId
                                        WHERE      ppbi.PmtPeriodBatchHeaderId = @InsertedHeaderId
                                        ) ppCumulative
                            CROSS APPLY (
                                        SELECT   TOP 1 sas.StudentAttendedDate
                                        FROM     dbo.syStudentAttendanceSummary sas
                                        WHERE    sas.StuEnrollId = ppCumulative.StuEnrollId
                                                 AND (( sas.ActualRunningPresentDays + sas.ActualRunningMakeupDays ) >= ppCumulative.CumulativeValue )
                                        ORDER BY sas.StudentAttendedDate
                                        ) AS transDate;



                INSERT INTO dbo.saTransactions (
                                               StuEnrollId
                                              ,TermId
                                              ,CampusId
                                              ,TransDate
                                              ,TransCodeId
                                              ,TransReference
                                              ,AcademicYearId
                                              ,TransDescrip
                                              ,TransAmount
                                              ,TransTypeId
                                              ,IsPosted
                                              ,CreateDate
                                              ,BatchPaymentId
                                              ,ViewOrder
                                              ,IsAutomatic
                                              ,ModUser
                                              ,ModDate
                                              ,Voided
                                              ,FeeLevelId
                                              ,FeeId
                                              ,PaymentCodeId
                                              ,FundSourceId
                                              ,StuEnrollPayPeriodId
                                              ,DisplaySequence
                                              ,SecondDisplaySequence
                                              ,PmtPeriodId
                                              ,PaymentPeriodNumber
                                               )
                            SELECT     BI.StuEnrollId AS StuEnrollId
                                      ,BI.TermId AS TermId
                                      ,SE.CampusId
                                      ,COALESCE((
                                                SELECT TransactionDate
                                                FROM   @TransactionDates
                                                WHERE  PmtPeriodId = BI.PmtPeriodId
                                                       AND StuEnrollId = BI.StuEnrollId
                                                )
                                               ,GETDATE()
                                               ,SE.StartDate
                                               ,SE.ExpStartDate
                                               ) --transaction date
                                      ,(
                                       SELECT TOP 1 TransCodeId
                                       FROM   dbo.saTransCodes
                                       WHERE  TransCodeCode = ''AUTOPOSTTUIT''
                                       ) AS TranscodeId
                                      ,BI.TransactionReference
                                      ,NULL AS AcademicYearId
                                                 --??? HOW TO GET THIS?, 
                                      ,BI.TransactionDescription
                                      ,BI.ChargeAmount
                                      ,(
                                       SELECT TOP 1 TransTypeId
                                       FROM   dbo.saTransTypes
                                       WHERE  Description LIKE ''Charge%''
                                       ) AS TransTypeId
                                      ,1 AS IsPosted
                                      ,GETDATE() AS CreatedDate
                                      ,NULL AS BatchPaymentId
                                      ,NULL AS ViewOrder
                                      ,0 AS IsAutomatic
                                      ,''AutoPost'' AS ModUser
                                      ,GETDATE() AS ModDate
                                      ,0 AS Voided
                                      ,NULL AS FeeLevelId
                                      ,NULL AS FeeId
                                      ,NULL AS PaymentCodeId
                                      ,NULL AS FundSourceId
                                      ,NULL AS StuEnrollPayPeriodId
                                      ,NULL AS DisplaySequence
                                      ,NULL AS SecondDisplaySequence
                                      ,BI.PmtPeriodId AS PmtPeriodId
                                      ,PP.PeriodNumber
                            FROM       saPmtPeriodBatchItems BI
                            INNER JOIN saPmtPeriodBatchHeaders BH ON BI.PmtPeriodBatchHeaderId = BH.PmtPeriodBatchHeaderId
                            INNER JOIN dbo.arStuEnrollments SE ON BI.StuEnrollId = SE.StuEnrollId
                            INNER JOIN dbo.saPmtPeriods PP ON PP.PmtPeriodId = BI.PmtPeriodId
                            WHERE      BH.PmtPeriodBatchHeaderId = @InsertedHeaderId
                                       AND BI.IncludedInPost = 1
                                       AND SE.StuEnrollId NOT IN (
                                                                 SELECT StuEnrollId
                                                                 FROM   dbo.saTransactions
                                                                 WHERE  StuEnrollId = SE.StuEnrollId
                                                                        AND PmtPeriodId = BI.PmtPeriodId
                                                                        OR PaymentPeriodNumber = PP.PeriodNumber
                                                                 )
                            ORDER BY   SE.StuEnrollId
                                      ,BI.TransactionReference;


                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error inserting transactions.'', 16, 1);
                        RETURN;
                    END;
                IF OBJECTPROPERTY(OBJECT_ID(''Trigger_Insert_Check_saTransactions''), ''IsTrigger'') = 1
                    BEGIN
                        ALTER TABLE dbo.saTransactions ENABLE TRIGGER Trigger_Insert_Check_saTransactions;
                    END;


                UPDATE     saPmtPeriodBatchItems
                SET        TransactionId = T.TransactionId
                FROM       dbo.saTransactions T
                INNER JOIN dbo.saPmtPeriodBatchItems BI ON T.PmtPeriodId = BI.PmtPeriodId
                                                           AND T.StuEnrollId = BI.StuEnrollId
                INNER JOIN dbo.saPmtPeriodBatchHeaders BH ON BI.PmtPeriodBatchHeaderId = BH.PmtPeriodBatchHeaderId
                WHERE      BH.PmtPeriodBatchHeaderId = @InsertedHeaderId;

                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error updating transaction ids in batch.'', 16, 1);
                        RETURN;
                    END;
                ----------------------------------------------------------------- 

                ----------------------------------------------------------------- 
                -- Set Batch to IsPosted 
                ----------------------------------------------------------------- 
                UPDATE dbo.saPmtPeriodBatchHeaders
                SET    IsPosted = 1
                      ,PostedDate = GETDATE()
                WHERE  PmtPeriodBatchHeaderId = @InsertedHeaderId;

                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error updating batch is posted flag.'', 16, 1);
                        RETURN;
                    END;
                ----------------------------------------------------------------- 


                ----------------------------------------------------------------- 
                -- Set IsCharged flag for payment periods from this batch 
                ----------------------------------------------------------------- 
                UPDATE     dbo.saPmtPeriods
                SET        IsCharged = 1
                FROM       saPmtPeriodBatchItems I
                INNER JOIN dbo.saPmtPeriods P ON I.PmtPeriodId = P.PmtPeriodId
                WHERE      I.PmtPeriodBatchHeaderId = @InsertedHeaderId;

                IF @@ERROR <> 0
                    BEGIN
                        ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                        RAISERROR(''Error updating batch is posted flag.'', 16, 1);
                        RETURN;
                    END;
            END;


        ----------------------------------------------------------------- 
        -- Set IsBillingMethodCharged flag for Program Version from this batch 
        ----------------------------------------------------------------- 
        UPDATE     P
        SET        P.IsBillingMethodCharged = 1
        FROM       dbo.arPrgVersions P
        INNER JOIN dbo.arStuEnrollments E ON P.PrgVerId = E.PrgVerId
        INNER JOIN saPmtPeriodBatchItems I ON E.StuEnrollId = I.StuEnrollId
        INNER JOIN dbo.saPmtPeriodBatchHeaders H ON I.PmtPeriodBatchHeaderId = H.PmtPeriodBatchHeaderId
        WHERE      H.PmtPeriodBatchHeaderId = @InsertedHeaderId
                   AND I.TransactionId IS NOT NULL
                   AND I.IncludedInPost = 1;


        IF @@ERROR <> 0
            BEGIN
                ROLLBACK TRAN PAYMENTPERIODTRANSACTION;
                RAISERROR(''Error updating program version ischarged flag.'', 16, 1);
                RETURN;
            END;
        -----------------------------------------------------------------	 


        COMMIT TRAN PAYMENTPERIODTRANSACTION;
    -----------------------------------------------------------------	 



    END;
--================================================================================================= 
-- END  --  USP_ProcessPaymentPeriodsHours 
--================================================================================================= 
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[USP_TitleIV_QualitativeAndQuantitative]'
GO
IF OBJECT_ID(N'[dbo].[USP_TitleIV_QualitativeAndQuantitative]', 'P') IS NOT NULL
EXEC sp_executesql N'-- =============================================
-- Author:		<Author,,Edwin Sosa>
-- Create date: <Create Date,,>
-- Description:	<This procedure calculates the qualitative and quantitative values for Title IV by using an offset date. The attendance portion of this procedure
-- removes the makeup hours from the actual hours before storing it into the sy attendance summary table. This differs slightly from the sprocs called in the attendance job to acheive the correct results.
-- When calculating the quantitative value, the makeup hours are added back into the actuals to get a consisten result. This sproc should be refactored to use the same procedures the attendance job uses internally.>
-- =============================================
ALTER PROCEDURE [dbo].[USP_TitleIV_QualitativeAndQuantitative]
    -- Add the parameters for the stored procedure here
    @StuEnrollId VARCHAR(MAX) = NULL
   ,@QuantMinUnitTypeId INTEGER
   ,@OffsetDate DATETIME
   ,@CumAverage DECIMAL(18, 2) OUTPUT
   ,@cumWeightedGPA DECIMAL(18, 2) OUTPUT
   ,@cumSimpleGPA DECIMAL(18, 2) OUTPUT
   ,@Qualitative DECIMAL(18, 2) OUTPUT
   ,@Quantitative DECIMAL(18, 2) OUTPUT
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @attendanceSummary TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ClsSectionId UNIQUEIDENTIFIER
               ,StudentAttendedDate DATETIME
               ,ScheduledDays DECIMAL(18, 2)
               ,ActualDays DECIMAL(18, 2)
               ,ActualRunningScheduledDays DECIMAL(18, 2)
               ,ActualRunningPresentDays DECIMAL(18, 2)
               ,ActualRunningAbsentDays DECIMAL(18, 2)
               ,ActualRunningMakeupDays DECIMAL(18, 2)
               ,ActualRunningTardyDays DECIMAL(18, 2)
               ,AdjustedPresentDays DECIMAL(18, 2)
               ,AdjustedAbsentDays DECIMAL(18, 2)
               ,AttendanceTrackType VARCHAR(50)
               ,ModUser VARCHAR(50)
               ,ModDate DATETIME
               ,TardiesMakingAbsence INT
            );
        -- Insert statements for procedure here
        DECLARE @ClsSectionIntervalMinutes AS TABLE
            (
                ClsSectionId UNIQUEIDENTIFIER NOT NULL
               ,IntervalInMinutes DECIMAL(18, 2)
            );

        DECLARE @TrackSapAttendance VARCHAR(1000);
        DECLARE @displayHours AS VARCHAR(1000);
        DECLARE @GradeCourseRepetitionsMethod VARCHAR(1000);
        DECLARE @GradesFormat AS VARCHAR(1000);
        DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;

        SET @StuEnrollCampusId = COALESCE((
                                          SELECT ASE.CampusId
                                          FROM   arStuEnrollments AS ASE
                                          WHERE  ASE.StuEnrollId = @StuEnrollId
                                          )
                                         ,NULL
                                         );
        SET @TrackSapAttendance = dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', @StuEnrollCampusId);
        IF ( @TrackSapAttendance IS NOT NULL )
            BEGIN
                SET @TrackSapAttendance = LOWER(LTRIM(RTRIM(@TrackSapAttendance)));
            END;

        SET @GradeCourseRepetitionsMethod = dbo.GetAppSettingValueByKeyName(''GradeCourseRepetitionsMethod'', @StuEnrollCampusId);
        SET @GradesFormat = dbo.GetAppSettingValueByKeyName(''GradesFormat'', @StuEnrollCampusId);
        IF ( @GradesFormat IS NOT NULL )
            BEGIN
                SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
            END;



        DECLARE @CoursesNotRepeated TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,TermId UNIQUEIDENTIFIER
               ,TermDescrip VARCHAR(100)
               ,ReqId UNIQUEIDENTIFIER
               ,ReqDescrip VARCHAR(100)
               ,ClsSectionId VARCHAR(50)
               ,CreditsEarned DECIMAL(18, 2)
               ,CreditsAttempted DECIMAL(18, 2)
               ,CurrentScore DECIMAL(18, 2)
               ,CurrentGrade VARCHAR(10)
               ,FinalScore DECIMAL(18, 2)
               ,FinalGrade VARCHAR(10)
               ,Completed BIT
               ,FinalGPA DECIMAL(18, 2)
               ,Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
               ,Count_WeightedAverage_Credits DECIMAL(18, 2)
               ,Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
               ,Count_SimpleAverage_Credits DECIMAL(18, 2)
               ,ModUser VARCHAR(50)
               ,ModDate DATETIME
               ,TermGPA_Simple DECIMAL(18, 2)
               ,TermGPA_Weighted DECIMAL(18, 2)
               ,coursecredits DECIMAL(18, 2)
               ,CumulativeGPA DECIMAL(18, 2)
               ,CumulativeGPA_Simple DECIMAL(18, 2)
               ,FACreditsEarned DECIMAL(18, 2)
               ,Average DECIMAL(18, 2)
               ,CumAverage DECIMAL(18, 2)
               ,TermStartDate DATETIME
               ,rownumber INT
            );

        INSERT INTO @CoursesNotRepeated
                    SELECT StuEnrollId
                          ,TermId
                          ,TermDescrip
                          ,ReqId
                          ,ReqDescrip
                          ,ClsSectionId
                          ,CreditsEarned
                          ,CreditsAttempted
                          ,CurrentScore
                          ,CurrentGrade
                          ,FinalScore
                          ,FinalGrade
                          ,Completed
                          ,FinalGPA
                          ,Product_WeightedAverage_Credits_GPA
                          ,Count_WeightedAverage_Credits
                          ,Product_SimpleAverage_Credits_GPA
                          ,Count_SimpleAverage_Credits
                          ,ModUser
                          ,ModDate
                          ,TermGPA_Simple
                          ,TermGPA_Weighted
                          ,coursecredits
                          ,CumulativeGPA
                          ,CumulativeGPA_Simple
                          ,FACreditsEarned
                          ,Average
                          ,CumAverage
                          ,TermStartDate
                          ,NULL AS rownumber
                    FROM   (
                           SELECT     sCS.StuEnrollId
                                     ,sCS.TermId
                                     ,sCS.TermDescrip
                                     ,sCS.ReqId
                                     ,sCS.ReqDescrip
                                     ,sCS.ClsSectionId
                                     ,sCS.CreditsEarned
                                     ,sCS.CreditsAttempted
                                     ,sCS.CurrentScore
                                     ,sCS.CurrentGrade
                                     ,sCS.FinalScore
                                     ,sCS.FinalGrade
                                     ,sCS.Completed
                                     ,sCS.FinalGPA
                                     ,sCS.Product_WeightedAverage_Credits_GPA
                                     ,sCS.Count_WeightedAverage_Credits
                                     ,sCS.Product_SimpleAverage_Credits_GPA
                                     ,sCS.Count_SimpleAverage_Credits
                                     ,sCS.ModUser
                                     ,sCS.ModDate
                                     ,sCS.TermGPA_Simple
                                     ,sCS.TermGPA_Weighted
                                     ,sCS.coursecredits
                                     ,sCS.CumulativeGPA
                                     ,sCS.CumulativeGPA_Simple
                                     ,sCS.FACreditsEarned
                                     ,sCS.Average
                                     ,sCS.CumAverage
                                     ,sCS.TermStartDate
                           FROM       dbo.syCreditSummary sCS
                           INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                           INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                           WHERE      sCS.StuEnrollId = @StuEnrollId
                                      AND CS.ReqId = sCS.ReqId
                                      AND (
                                          R.DateCompleted IS NOT NULL
                                          AND R.DateCompleted <= @OffsetDate
                                          )
                           UNION ALL
                           (SELECT     sCS.StuEnrollId
                                      ,sCS.TermId
                                      ,sCS.TermDescrip
                                      ,sCS.ReqId
                                      ,sCS.ReqDescrip
                                      ,sCS.ClsSectionId
                                      ,sCS.CreditsEarned
                                      ,sCS.CreditsAttempted
                                      ,sCS.CurrentScore
                                      ,sCS.CurrentGrade
                                      ,sCS.FinalScore
                                      ,sCS.FinalGrade
                                      ,sCS.Completed
                                      ,sCS.FinalGPA
                                      ,sCS.Product_WeightedAverage_Credits_GPA
                                      ,sCS.Count_WeightedAverage_Credits
                                      ,sCS.Product_SimpleAverage_Credits_GPA
                                      ,sCS.Count_SimpleAverage_Credits
                                      ,sCS.ModUser
                                      ,sCS.ModDate
                                      ,sCS.TermGPA_Simple
                                      ,sCS.TermGPA_Weighted
                                      ,sCS.coursecredits
                                      ,sCS.CumulativeGPA
                                      ,sCS.CumulativeGPA_Simple
                                      ,sCS.FACreditsEarned
                                      ,sCS.Average
                                      ,sCS.CumAverage
                                      ,sCS.TermStartDate
                            FROM       dbo.syCreditSummary sCS
                            INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                            WHERE      sCS.StuEnrollId = @StuEnrollId
                                       AND tG.ReqId = sCS.ReqId
                                       AND (
                                           tG.CompletedDate IS NOT NULL
                                           AND tG.CompletedDate <= @OffsetDate
                                           ))
                           ) sCSEA
                    WHERE  StuEnrollId = @StuEnrollId
                           AND ReqId IN (
                                        SELECT ReqId
                                        FROM   (
                                               SELECT   ReqId
                                                       ,ReqDescrip
                                                       ,COUNT(*) AS counter
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                               GROUP BY ReqId
                                                       ,ReqDescrip
                                               HAVING   COUNT(*) = 1
                                               ) dt
                                        );



        IF LOWER(@GradeCourseRepetitionsMethod) = ''latest''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT   dt2.StuEnrollId
                                    ,dt2.TermId
                                    ,dt2.TermDescrip
                                    ,dt2.ReqId
                                    ,dt2.ReqDescrip
                                    ,dt2.ClsSectionId
                                    ,dt2.CreditsEarned
                                    ,dt2.CreditsAttempted
                                    ,dt2.CurrentScore
                                    ,dt2.CurrentGrade
                                    ,dt2.FinalScore
                                    ,dt2.FinalGrade
                                    ,dt2.Completed
                                    ,dt2.FinalGPA
                                    ,dt2.Product_WeightedAverage_Credits_GPA
                                    ,dt2.Count_WeightedAverage_Credits
                                    ,dt2.Product_SimpleAverage_Credits_GPA
                                    ,dt2.Count_SimpleAverage_Credits
                                    ,dt2.ModUser
                                    ,dt2.ModDate
                                    ,dt2.TermGPA_Simple
                                    ,dt2.TermGPA_Weighted
                                    ,dt2.coursecredits
                                    ,dt2.CumulativeGPA
                                    ,dt2.CumulativeGPA_Simple
                                    ,dt2.FACreditsEarned
                                    ,dt2.Average
                                    ,dt2.CumAverage
                                    ,dt2.TermStartDate
                                    ,dt2.RowNumber
                            FROM     (
                                     SELECT StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                                ORDER BY TermStartDate DESC
                                                              ) AS RowNumber
                                     FROM   (
                                            SELECT     sCS.StuEnrollId
                                                      ,sCS.TermId
                                                      ,sCS.TermDescrip
                                                      ,sCS.ReqId
                                                      ,sCS.ReqDescrip
                                                      ,sCS.ClsSectionId
                                                      ,sCS.CreditsEarned
                                                      ,sCS.CreditsAttempted
                                                      ,sCS.CurrentScore
                                                      ,sCS.CurrentGrade
                                                      ,sCS.FinalScore
                                                      ,sCS.FinalGrade
                                                      ,sCS.Completed
                                                      ,sCS.FinalGPA
                                                      ,sCS.Product_WeightedAverage_Credits_GPA
                                                      ,sCS.Count_WeightedAverage_Credits
                                                      ,sCS.Product_SimpleAverage_Credits_GPA
                                                      ,sCS.Count_SimpleAverage_Credits
                                                      ,sCS.ModUser
                                                      ,sCS.ModDate
                                                      ,sCS.TermGPA_Simple
                                                      ,sCS.TermGPA_Weighted
                                                      ,sCS.coursecredits
                                                      ,sCS.CumulativeGPA
                                                      ,sCS.CumulativeGPA_Simple
                                                      ,sCS.FACreditsEarned
                                                      ,sCS.Average
                                                      ,sCS.CumAverage
                                                      ,sCS.TermStartDate
                                            FROM       dbo.syCreditSummary sCS
                                            INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                            INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                            WHERE      sCS.StuEnrollId = @StuEnrollId
                                                       AND CS.ReqId = sCS.ReqId
                                                       AND (
                                                           R.DateCompleted IS NOT NULL
                                                           AND R.DateCompleted <= @OffsetDate
                                                           )
                                            UNION ALL
                                            (SELECT     sCS.StuEnrollId
                                                       ,sCS.TermId
                                                       ,sCS.TermDescrip
                                                       ,sCS.ReqId
                                                       ,sCS.ReqDescrip
                                                       ,sCS.ClsSectionId
                                                       ,sCS.CreditsEarned
                                                       ,sCS.CreditsAttempted
                                                       ,sCS.CurrentScore
                                                       ,sCS.CurrentGrade
                                                       ,sCS.FinalScore
                                                       ,sCS.FinalGrade
                                                       ,sCS.Completed
                                                       ,sCS.FinalGPA
                                                       ,sCS.Product_WeightedAverage_Credits_GPA
                                                       ,sCS.Count_WeightedAverage_Credits
                                                       ,sCS.Product_SimpleAverage_Credits_GPA
                                                       ,sCS.Count_SimpleAverage_Credits
                                                       ,sCS.ModUser
                                                       ,sCS.ModDate
                                                       ,sCS.TermGPA_Simple
                                                       ,sCS.TermGPA_Weighted
                                                       ,sCS.coursecredits
                                                       ,sCS.CumulativeGPA
                                                       ,sCS.CumulativeGPA_Simple
                                                       ,sCS.FACreditsEarned
                                                       ,sCS.Average
                                                       ,sCS.CumAverage
                                                       ,sCS.TermStartDate
                                             FROM       dbo.syCreditSummary sCS
                                             INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                             WHERE      sCS.StuEnrollId = @StuEnrollId
                                                        AND tG.ReqId = sCS.ReqId
                                                        AND (
                                                            tG.CompletedDate IS NOT NULL
                                                            AND tG.CompletedDate <= @OffsetDate
                                                            ))
                                            ) sCSEA
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND (
                                                FinalScore IS NOT NULL
                                                OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN (
                                                         SELECT ReqId
                                                         FROM   (
                                                                SELECT   ReqId
                                                                        ,ReqDescrip
                                                                        ,COUNT(*) AS Counter
                                                                FROM     syCreditSummary
                                                                WHERE    StuEnrollId = @StuEnrollId
                                                                GROUP BY ReqId
                                                                        ,ReqDescrip
                                                                HAVING   COUNT(*) > 1
                                                                ) dt
                                                         )
                                     ) dt2
                            WHERE    RowNumber = 1
                            ORDER BY TermStartDate DESC;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''best''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                            FROM   (
                                   SELECT StuEnrollId
                                         ,TermId
                                         ,TermDescrip
                                         ,ReqId
                                         ,ReqDescrip
                                         ,ClsSectionId
                                         ,CreditsEarned
                                         ,CreditsAttempted
                                         ,CurrentScore
                                         ,CurrentGrade
                                         ,FinalScore
                                         ,FinalGrade
                                         ,Completed
                                         ,FinalGPA
                                         ,Product_WeightedAverage_Credits_GPA
                                         ,Count_WeightedAverage_Credits
                                         ,Product_SimpleAverage_Credits_GPA
                                         ,Count_SimpleAverage_Credits
                                         ,ModUser
                                         ,ModDate
                                         ,TermGPA_Simple
                                         ,TermGPA_Weighted
                                         ,coursecredits
                                         ,CumulativeGPA
                                         ,CumulativeGPA_Simple
                                         ,FACreditsEarned
                                         ,Average
                                         ,CumAverage
                                         ,TermStartDate
                                         ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                              ORDER BY Completed DESC
                                                                      ,CreditsEarned DESC
                                                                      ,FinalGPA DESC
                                                            ) AS RowNumber
                                   FROM   (
                                          SELECT     sCS.StuEnrollId
                                                    ,sCS.TermId
                                                    ,sCS.TermDescrip
                                                    ,sCS.ReqId
                                                    ,sCS.ReqDescrip
                                                    ,sCS.ClsSectionId
                                                    ,sCS.CreditsEarned
                                                    ,sCS.CreditsAttempted
                                                    ,sCS.CurrentScore
                                                    ,sCS.CurrentGrade
                                                    ,sCS.FinalScore
                                                    ,sCS.FinalGrade
                                                    ,sCS.Completed
                                                    ,sCS.FinalGPA
                                                    ,sCS.Product_WeightedAverage_Credits_GPA
                                                    ,sCS.Count_WeightedAverage_Credits
                                                    ,sCS.Product_SimpleAverage_Credits_GPA
                                                    ,sCS.Count_SimpleAverage_Credits
                                                    ,sCS.ModUser
                                                    ,sCS.ModDate
                                                    ,sCS.TermGPA_Simple
                                                    ,sCS.TermGPA_Weighted
                                                    ,sCS.coursecredits
                                                    ,sCS.CumulativeGPA
                                                    ,sCS.CumulativeGPA_Simple
                                                    ,sCS.FACreditsEarned
                                                    ,sCS.Average
                                                    ,sCS.CumAverage
                                                    ,sCS.TermStartDate
                                          FROM       dbo.syCreditSummary sCS
                                          INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                          INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                          WHERE      sCS.StuEnrollId = @StuEnrollId
                                                     AND CS.ReqId = sCS.ReqId
                                                     AND (
                                                         R.DateCompleted IS NOT NULL
                                                         AND R.DateCompleted <= @OffsetDate
                                                         )
                                          UNION ALL
                                          (SELECT     sCS.StuEnrollId
                                                     ,sCS.TermId
                                                     ,sCS.TermDescrip
                                                     ,sCS.ReqId
                                                     ,sCS.ReqDescrip
                                                     ,sCS.ClsSectionId
                                                     ,sCS.CreditsEarned
                                                     ,sCS.CreditsAttempted
                                                     ,sCS.CurrentScore
                                                     ,sCS.CurrentGrade
                                                     ,sCS.FinalScore
                                                     ,sCS.FinalGrade
                                                     ,sCS.Completed
                                                     ,sCS.FinalGPA
                                                     ,sCS.Product_WeightedAverage_Credits_GPA
                                                     ,sCS.Count_WeightedAverage_Credits
                                                     ,sCS.Product_SimpleAverage_Credits_GPA
                                                     ,sCS.Count_SimpleAverage_Credits
                                                     ,sCS.ModUser
                                                     ,sCS.ModDate
                                                     ,sCS.TermGPA_Simple
                                                     ,sCS.TermGPA_Weighted
                                                     ,sCS.coursecredits
                                                     ,sCS.CumulativeGPA
                                                     ,sCS.CumulativeGPA_Simple
                                                     ,sCS.FACreditsEarned
                                                     ,sCS.Average
                                                     ,sCS.CumAverage
                                                     ,sCS.TermStartDate
                                           FROM       dbo.syCreditSummary sCS
                                           INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                           WHERE      sCS.StuEnrollId = @StuEnrollId
                                                      AND tG.ReqId = sCS.ReqId
                                                      AND (
                                                          tG.CompletedDate IS NOT NULL
                                                          AND tG.CompletedDate <= @OffsetDate
                                                          ))
                                          ) sCSEA
                                   WHERE  StuEnrollId = @StuEnrollId
                                          AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                              )
                                          AND ReqId IN (
                                                       SELECT ReqId
                                                       FROM   (
                                                              SELECT   ReqId
                                                                      ,ReqDescrip
                                                                      ,COUNT(*) AS Counter
                                                              FROM     syCreditSummary
                                                              WHERE    StuEnrollId = @StuEnrollId
                                                              GROUP BY ReqId
                                                                      ,ReqDescrip
                                                              HAVING   COUNT(*) > 1
                                                              ) dt
                                                       )
                                   ) dt2
                            WHERE  RowNumber = 1;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''average''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                            FROM   (
                                   SELECT StuEnrollId
                                         ,TermId
                                         ,TermDescrip
                                         ,ReqId
                                         ,ReqDescrip
                                         ,ClsSectionId
                                         ,CreditsEarned
                                         ,CreditsAttempted
                                         ,CurrentScore
                                         ,CurrentGrade
                                         ,FinalScore
                                         ,FinalGrade
                                         ,Completed
                                         ,FinalGPA
                                         ,Product_WeightedAverage_Credits_GPA
                                         ,Count_WeightedAverage_Credits
                                         ,Product_SimpleAverage_Credits_GPA
                                         ,Count_SimpleAverage_Credits
                                         ,ModUser
                                         ,ModDate
                                         ,TermGPA_Simple
                                         ,TermGPA_Weighted
                                         ,coursecredits
                                         ,CumulativeGPA
                                         ,CumulativeGPA_Simple
                                         ,FACreditsEarned
                                         ,Average
                                         ,CumAverage
                                         ,TermStartDate
                                         ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                              ORDER BY FinalGPA DESC
                                                            ) AS RowNumber
                                   FROM   (
                                          SELECT     sCS.StuEnrollId
                                                    ,sCS.TermId
                                                    ,sCS.TermDescrip
                                                    ,sCS.ReqId
                                                    ,sCS.ReqDescrip
                                                    ,sCS.ClsSectionId
                                                    ,sCS.CreditsEarned
                                                    ,sCS.CreditsAttempted
                                                    ,sCS.CurrentScore
                                                    ,sCS.CurrentGrade
                                                    ,sCS.FinalScore
                                                    ,sCS.FinalGrade
                                                    ,sCS.Completed
                                                    ,sCS.FinalGPA
                                                    ,sCS.Product_WeightedAverage_Credits_GPA
                                                    ,sCS.Count_WeightedAverage_Credits
                                                    ,sCS.Product_SimpleAverage_Credits_GPA
                                                    ,sCS.Count_SimpleAverage_Credits
                                                    ,sCS.ModUser
                                                    ,sCS.ModDate
                                                    ,sCS.TermGPA_Simple
                                                    ,sCS.TermGPA_Weighted
                                                    ,sCS.coursecredits
                                                    ,sCS.CumulativeGPA
                                                    ,sCS.CumulativeGPA_Simple
                                                    ,sCS.FACreditsEarned
                                                    ,sCS.Average
                                                    ,sCS.CumAverage
                                                    ,sCS.TermStartDate
                                          FROM       dbo.syCreditSummary sCS
                                          INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                          INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                          WHERE      sCS.StuEnrollId = @StuEnrollId
                                                     AND CS.ReqId = sCS.ReqId
                                                     AND (
                                                         R.DateCompleted IS NOT NULL
                                                         AND R.DateCompleted <= @OffsetDate
                                                         )
                                          UNION ALL
                                          (SELECT     sCS.StuEnrollId
                                                     ,sCS.TermId
                                                     ,sCS.TermDescrip
                                                     ,sCS.ReqId
                                                     ,sCS.ReqDescrip
                                                     ,sCS.ClsSectionId
                                                     ,sCS.CreditsEarned
                                                     ,sCS.CreditsAttempted
                                                     ,sCS.CurrentScore
                                                     ,sCS.CurrentGrade
                                                     ,sCS.FinalScore
                                                     ,sCS.FinalGrade
                                                     ,sCS.Completed
                                                     ,sCS.FinalGPA
                                                     ,sCS.Product_WeightedAverage_Credits_GPA
                                                     ,sCS.Count_WeightedAverage_Credits
                                                     ,sCS.Product_SimpleAverage_Credits_GPA
                                                     ,sCS.Count_SimpleAverage_Credits
                                                     ,sCS.ModUser
                                                     ,sCS.ModDate
                                                     ,sCS.TermGPA_Simple
                                                     ,sCS.TermGPA_Weighted
                                                     ,sCS.coursecredits
                                                     ,sCS.CumulativeGPA
                                                     ,sCS.CumulativeGPA_Simple
                                                     ,sCS.FACreditsEarned
                                                     ,sCS.Average
                                                     ,sCS.CumAverage
                                                     ,sCS.TermStartDate
                                           FROM       dbo.syCreditSummary sCS
                                           INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                           WHERE      sCS.StuEnrollId = @StuEnrollId
                                                      AND tG.ReqId = sCS.ReqId
                                                      AND (
                                                          tG.CompletedDate IS NOT NULL
                                                          AND tG.CompletedDate <= @OffsetDate
                                                          ))
                                          ) sCSEA
                                   WHERE  StuEnrollId = @StuEnrollId
                                          AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                              )
                                          AND ReqId IN (
                                                       SELECT ReqId
                                                       FROM   (
                                                              SELECT   ReqId
                                                                      ,ReqDescrip
                                                                      ,COUNT(*) AS Counter
                                                              FROM     syCreditSummary
                                                              WHERE    StuEnrollId = @StuEnrollId
                                                              GROUP BY ReqId
                                                                      ,ReqDescrip
                                                              HAVING   COUNT(*) > 1
                                                              ) dt
                                                       )
                                   ) dt2;
            END;


        DECLARE @cumSimpleCourseCredits DECIMAL(18, 2);
        DECLARE @cumSimple_GPA_Credits DECIMAL(18, 2);
        -- (OUTPUT parameter)  DECLARE @cumSimpleGPA DECIMAL(18, 2)



        DECLARE @cumCourseCredits DECIMAL(18, 2)
               ,@cumWeighted_GPA_Credits DECIMAL(18, 2);
        DECLARE @cumCourseCredits_repeated DECIMAL(18, 2)
               ,@cumWeighted_GPA_Credits_repeated DECIMAL(18, 2);

        PRINT @GradeCourseRepetitionsMethod;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''average''
            BEGIN
                SET @cumWeightedGPA = 0;
                SET @cumSimpleGPA = 0;

                SET @cumSimpleCourseCredits = (
                                              SELECT COUNT(*)
                                              FROM   @CoursesNotRepeated
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND FinalGPA IS NOT NULL
                                                     AND (
                                                         rownumber = 0
                                                         OR rownumber IS NULL
                                                         )
                                              );

                DECLARE @cumSimpleCourseCredits_repeated DECIMAL(18, 2);
                SET @cumSimpleCourseCredits_repeated = (
                                                       SELECT SUM(CourseCreditCount)
                                                       FROM   (
                                                              SELECT   ReqId
                                                                      ,COUNT(coursecredits) AS CourseCreditCount
                                                              FROM     @CoursesNotRepeated
                                                              WHERE    StuEnrollId IN ( @StuEnrollId )
                                                                       AND FinalGPA IS NOT NULL
                                                                       AND rownumber = 1
                                                              GROUP BY ReqId
                                                              ) dt
                                                       );
                SET @cumSimpleCourseCredits = @cumSimpleCourseCredits + ISNULL(@cumSimpleCourseCredits_repeated, 0);

                DECLARE @cumSimple_GPA_Credits_repeated DECIMAL(18, 2);

                SET @cumSimple_GPA_Credits = (
                                             SELECT SUM(FinalGPA)
                                             FROM   @CoursesNotRepeated
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                                    AND (
                                                        rownumber = 0
                                                        OR rownumber IS NULL
                                                        )
                                             );

                SET @cumSimple_GPA_Credits_repeated = (
                                                      SELECT SUM(AverageGPA)
                                                      FROM   (
                                                             SELECT   ReqId
                                                                     ,SUM(FinalGPA) / MAX(rownumber) AS AverageGPA
                                                             FROM     @CoursesNotRepeated
                                                             WHERE    StuEnrollId IN ( @StuEnrollId )
                                                                      AND FinalGPA IS NOT NULL
                                                                      AND rownumber >= 1
                                                             GROUP BY ReqId
                                                             ) dt
                                                      );

                SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits + ISNULL(@cumSimple_GPA_Credits_repeated, 0.00);

                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END;

                SET @cumCourseCredits = (
                                        SELECT SUM(coursecredits)
                                        FROM   @CoursesNotRepeated
                                        WHERE  StuEnrollId IN ( @StuEnrollId )
                                               AND FinalGPA IS NOT NULL
                                               AND (
                                                   rownumber = 0
                                                   OR rownumber IS NULL
                                                   )
                                        );

                SET @cumCourseCredits_repeated = (
                                                 SELECT SUM(CourseCreditCount)
                                                 FROM   (
                                                        SELECT   ReqId
                                                                ,MAX(coursecredits) AS CourseCreditCount
                                                        FROM     @CoursesNotRepeated
                                                        WHERE    StuEnrollId IN ( @StuEnrollId )
                                                                 AND FinalGPA IS NOT NULL
                                                                 AND rownumber >= 1
                                                        GROUP BY ReqId
                                                        ) dt
                                                 );
                SET @cumCourseCredits = @cumCourseCredits + ISNULL(@cumCourseCredits_repeated, 0.00);

                PRINT @cumCourseCredits;

                SET @cumWeighted_GPA_Credits = (
                                               SELECT SUM(coursecredits * FinalGPA)
                                               FROM   @CoursesNotRepeated
                                               WHERE  StuEnrollId IN ( @StuEnrollId )
                                                      AND FinalGPA IS NOT NULL
                                                      AND (
                                                          rownumber = 0
                                                          OR rownumber IS NULL
                                                          )
                                               );
                SET @cumWeighted_GPA_Credits_repeated = (
                                                        SELECT SUM(CourseCreditCount)
                                                        FROM   (
                                                               SELECT   ReqId
                                                                       ,SUM(coursecredits * FinalGPA) / MAX(rownumber) AS CourseCreditCount
                                                               FROM     @CoursesNotRepeated
                                                               WHERE    StuEnrollId IN ( @StuEnrollId )
                                                                        AND FinalGPA IS NOT NULL
                                                                        AND rownumber >= 1
                                                               GROUP BY ReqId
                                                               ) dt
                                                        );



                SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits + ISNULL(@cumWeighted_GPA_Credits_repeated, 0);
                PRINT @cumWeighted_GPA_Credits;

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END;
            END;
        ELSE
            BEGIN
                SET @cumSimpleGPA = 0;

                SET @cumSimpleCourseCredits = (
                                              SELECT COUNT(*)
                                              FROM   @CoursesNotRepeated
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND FinalGPA IS NOT NULL
                                              );
                SET @cumSimple_GPA_Credits = (
                                             SELECT SUM(FinalGPA)
                                             FROM   @CoursesNotRepeated
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                             );
                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END;
                SET @cumWeightedGPA = 0;
                SET @cumCourseCredits = (
                                        SELECT SUM(coursecredits)
                                        FROM   @CoursesNotRepeated
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND FinalGPA IS NOT NULL
                                        );
                SET @cumWeighted_GPA_Credits = (
                                               SELECT SUM(coursecredits * FinalGPA)
                                               FROM   @CoursesNotRepeated
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalGPA IS NOT NULL
                                               );

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END;

            END;





        IF @cumCourseCredits >= 1
            BEGIN
                SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
            END;


        DECLARE @TermAverageCount INT;
        DECLARE @TermAverage DECIMAL(18, 2);
        -- (output parameter)  DECLARE @CumAverage DECIMAL(18, 2)
        IF @GradesFormat <> ''letter''
           OR @QuantMinUnitTypeId = 3
            BEGIN

                DECLARE @termAverageSum DECIMAL(18, 2)
                       ,@cumAverageSum DECIMAL(18, 2)
                       ,@cumAveragecount INT;
                DECLARE @TardiesMakingAbsence INT
                       ,@UnitTypeDescrip VARCHAR(20)
                       ,@OriginalTardiesMakingAbsence INT;
                SET @TardiesMakingAbsence = (
                                            SELECT TOP 1 t1.TardiesMakingAbsence
                                            FROM   arPrgVersions t1
                                                  ,arStuEnrollments t2
                                            WHERE  t1.PrgVerId = t2.PrgVerId
                                                   AND t2.StuEnrollId = @StuEnrollId
                                            );

                SET @UnitTypeDescrip = (
                                       SELECT     LTRIM(RTRIM(AAUT.UnitTypeDescrip))
                                       FROM       arAttUnitType AS AAUT
                                       INNER JOIN arPrgVersions AS APV ON APV.UnitTypeId = AAUT.UnitTypeId
                                       INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                                       WHERE      ASE.StuEnrollId = @StuEnrollId
                                       );

                SET @OriginalTardiesMakingAbsence = (
                                                    SELECT TOP 1 tardiesmakingabsence
                                                    FROM   syStudentAttendanceSummary
                                                    WHERE  StuEnrollId = @StuEnrollId
                                                    );
                DECLARE @termstartdate1 DATETIME;

                -- Declare Variables
                BEGIN -- Declare Variables
                    DECLARE @MeetDate DATETIME
                           ,@WeekDay VARCHAR(15)
                           ,@StartDate DATETIME
                           ,@EndDate DATETIME;
                    DECLARE @PeriodDescrip VARCHAR(50)
                           ,@Actual DECIMAL(18, 2)
                           ,@Excused DECIMAL(18, 2)
                           ,@ClsSectionId UNIQUEIDENTIFIER;
                    DECLARE @Absent DECIMAL(18, 2)
                           ,@SchedHours DECIMAL(18, 2)
                           ,@TardyMinutes DECIMAL(18, 2);
                    DECLARE @tardy DECIMAL(18, 2)
                           ,@tracktardies INT
                           ,@rownumber INT
                           ,@IsTardy BIT
                           ,@ActualRunningScheduledHours DECIMAL(18, 2);
                    DECLARE @ActualRunningPresentHours DECIMAL(18, 2)
                           ,@ActualRunningAbsentHours DECIMAL(18, 2)
                           ,@ActualRunningTardyHours DECIMAL(18, 2)
                           ,@ActualRunningMakeupHours DECIMAL(18, 2);
                    DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
                           ,@intTardyBreakPoint INT
                           ,@AdjustedRunningPresentHours DECIMAL(18, 2)
                           ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
                           ,@ActualRunningScheduledDays DECIMAL(18, 2);
                    --declare @Scheduledhours decimal(18,2),@ActualPresentDays_ConvertTo_Hours decimal(18,2),@ActualAbsentDays_ConvertTo_Hours decimal(18,2),@AttendanceTrack varchar(50)
                    DECLARE @TermId VARCHAR(50)
                           ,@TermDescrip VARCHAR(50)
                           ,@PrgVerId UNIQUEIDENTIFIER;
                    DECLARE @TardyHit VARCHAR(10)
                           ,@ScheduledMinutes DECIMAL(18, 2);
                    DECLARE @Scheduledhours_noperiods DECIMAL(18, 2)
                           ,@ActualPresentDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2)
                           ,@ActualAbsentDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2);
                    DECLARE @ActualTardyDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2);
                    DECLARE @boolReset BIT
                           ,@MakeupHours DECIMAL(18, 2)
                           ,@AdjustedPresentDaysComputed DECIMAL(18, 2);
                END;

                -- Step 3  --  InsertAttendance_Class_PresentAbsent   -- UnitTypeDescrip = (''None'', ''Present Absent'') 
                --         --  TrackSapAttendance = ''byclass''
                BEGIN -- Step 3  --  InsertAttendance_Class_PresentAbsent   -- UnitTypeDescrip = (''None'', ''Present Absent'') 
                    --         --  TrackSapAttendance = ''byclass''
                    IF @UnitTypeDescrip IN ( ''none'', ''present absent'' )
                       AND @TrackSapAttendance = ''byclass''
                        BEGIN
                            PRINT ''Present absent, by class'';
                            ---- PRINT ''Step1!!!!''
                            --BEGIN
                            --    --DELETE FROM syStudentAttendanceSummary
                            --    --WHERE StuEnrollId = @StuEnrollId
                            --    --      AND StudentAttendedDate <= @OffsetDate;
                            --END;
                            INSERT INTO @ClsSectionIntervalMinutes (
                                                                   ClsSectionId
                                                                  ,IntervalInMinutes
                                                                   )
                                        SELECT     DISTINCT t1.ClsSectionId
                                                           ,DATEDIFF(MI, t6.TimeIntervalDescrip, t7.TimeIntervalDescrip)
                                        FROM       atClsSectAttendance t1
                                        INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                        INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                        INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                        INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                           AND (
                                                                               CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                               AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                               )
                                                                           AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                        INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                        INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                        INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                        INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                        INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                        INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                        WHERE      t2.StuEnrollId = @StuEnrollId
                                                   AND (
                                                       AAUT1.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                                       OR AAUT2.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                                       );

                            DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
                                SELECT   *
                                        ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                FROM     (
                                         SELECT     DISTINCT t1.StuEnrollId
                                                            ,t1.ClsSectionId
                                                            ,t1.MeetDate
                                                            ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                            ,t4.StartDate
                                                            ,t4.EndDate
                                                            ,t5.PeriodDescrip
                                                            ,t1.Actual
                                                            ,t1.Excused
                                                            ,CASE WHEN (
                                                                       t1.Actual = 0
                                                                       AND t1.Excused = 0
                                                                       ) THEN t1.Scheduled
                                                                  ELSE CASE WHEN (
                                                                                 t1.Actual <> 9999.00
                                                                                 AND t1.Actual < t1.Scheduled
                                                                                 --AND t1.Excused <> 1
                                                                                 ) THEN ( t1.Scheduled - t1.Actual )
                                                                            ELSE 0
                                                                       END
                                                             END AS Absent
                                                            ,t1.Scheduled AS ScheduledMinutes
                                                            ,CASE WHEN (
                                                                       t1.Actual > 0
                                                                       AND t1.Actual < t1.Scheduled
                                                                       ) THEN ( t1.Scheduled - t1.Actual )
                                                                  ELSE 0
                                                             END AS TardyMinutes
                                                            ,t1.Tardy AS Tardy
                                                            ,t3.TrackTardies
                                                            ,t3.TardiesMakingAbsence
                                                            ,t3.PrgVerId
                                         FROM       atClsSectAttendance t1
                                         INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                         INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                         INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                         INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                            AND (
                                                                                CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                                AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                                )
                                                                            AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                         INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                         INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                         INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                         INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                         INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                         INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                         WHERE      t2.StuEnrollId = @StuEnrollId
                                                    AND (
                                                        AAUT1.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                                        OR AAUT2.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                                        )
                                                    AND t1.Actual <> 9999
                                                    AND t1.MeetDate <= @OffsetDate
                                         ) dt
                                ORDER BY StuEnrollId
                                        ,MeetDate;
                            OPEN GetAttendance_Cursor;
                            FETCH NEXT FROM GetAttendance_Cursor
                            INTO @StuEnrollId
                                ,@ClsSectionId
                                ,@MeetDate
                                ,@WeekDay
                                ,@StartDate
                                ,@EndDate
                                ,@PeriodDescrip
                                ,@Actual
                                ,@Excused
                                ,@Absent
                                ,@ScheduledMinutes
                                ,@TardyMinutes
                                ,@tardy
                                ,@tracktardies
                                ,@TardiesMakingAbsence
                                ,@PrgVerId
                                ,@rownumber;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @boolReset = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                            SET @boolReset = 1;
                                        END;


                                    -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                                    IF @Actual <> 9999.00
                                        BEGIN
                                            -- Commented by Balaji on 2.6.2015 as Excused Absence is still considered an absence
                                            -- @Excused is not added to Present Hours
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual; -- + @Excused
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; -- + @Excused

                                            -- If there are make up hrs deduct that otherwise it will be added again in progress report
                                            IF (
                                               @Actual > 0
                                               AND @Actual > @ScheduledMinutes
                                               AND @Actual <> 9999.00
                                               )
                                                BEGIN
                                                    SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                END;

                                            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                                        END;

                                    -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                                    SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                                    -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                                    IF (
                                       @Actual > 0
                                       AND @Actual < @ScheduledMinutes
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                        END;
                                    ELSE IF (
                                            @Actual = 1
                                            AND @Actual = @ScheduledMinutes
                                            AND @tardy = 1
                                            )
                                             BEGIN
                                                 SET @ActualRunningTardyHours += 1;
                                             END;

                                    -- Track how many days student has been tardy only when 
                                    -- program version requires to track tardy
                                    IF (
                                       @tracktardies = 1
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;


                                    -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
                                    -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
                                    -- when student is tardy the second time, that second occurance will be considered as
                                    -- absence
                                    -- Variable @intTardyBreakpoint tracks how many times the student was tardy
                                    -- Variable @tardiesMakingAbsence tracks the tardy rule
                                    IF (
                                       @tracktardies = 1
                                       AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
                                            SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;

                                    -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                       @Actual > 0
                                       AND @Actual > @ScheduledMinutes
                                       AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;

                                    IF ( @tracktardies = 1 )
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                                        END;

                                    ---- PRINT @MeetDate
                                    ---- PRINT @ActualRunningAbsentHours



                                    INSERT INTO @attendanceSummary (
                                                                   StuEnrollId
                                                                  ,ClsSectionId
                                                                  ,StudentAttendedDate
                                                                  ,ScheduledDays
                                                                  ,ActualDays
                                                                  ,ActualRunningScheduledDays
                                                                  ,ActualRunningPresentDays
                                                                  ,ActualRunningAbsentDays
                                                                  ,ActualRunningMakeupDays
                                                                  ,ActualRunningTardyDays
                                                                  ,AdjustedPresentDays
                                                                  ,AdjustedAbsentDays
                                                                  ,AttendanceTrackType
                                                                  ,ModUser
                                                                  ,ModDate
                                                                  ,TardiesMakingAbsence
                                                                   )
                                    VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays
                                            ,@ActualRunningPresentHours, @ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours
                                            ,@AdjustedPresentDaysComputed, @AdjustedRunningAbsentHours, ''Post Attendance by Class Min'', ''sa'', GETDATE()
                                            ,@TardiesMakingAbsence );

                                    --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                                    SET @PrevStuEnrollId = @StuEnrollId;

                                    FETCH NEXT FROM GetAttendance_Cursor
                                    INTO @StuEnrollId
                                        ,@ClsSectionId
                                        ,@MeetDate
                                        ,@WeekDay
                                        ,@StartDate
                                        ,@EndDate
                                        ,@PeriodDescrip
                                        ,@Actual
                                        ,@Excused
                                        ,@Absent
                                        ,@ScheduledMinutes
                                        ,@TardyMinutes
                                        ,@tardy
                                        ,@tracktardies
                                        ,@TardiesMakingAbsence
                                        ,@PrgVerId
                                        ,@rownumber;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;

                            DECLARE @MyTardyTable TABLE
                                (
                                    ClsSectionId UNIQUEIDENTIFIER
                                   ,TardiesMakingAbsence INT
                                   ,AbsentHours DECIMAL(18, 2)
                                );

                            INSERT INTO @MyTardyTable
                                        SELECT     ClsSectionId
                                                  ,PV.TardiesMakingAbsence
                                                  ,CASE WHEN ( COUNT(*) >= PV.TardiesMakingAbsence ) THEN ( COUNT(*) / PV.TardiesMakingAbsence )
                                                        ELSE 0
                                                   END AS AbsentHours
                                        --Count(*) as NumberofTimesTardy 
                                        FROM       dbo.syStudentAttendanceSummary SAS
                                        INNER JOIN arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                                        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                        WHERE      SE.StuEnrollId = @StuEnrollId
                                                   AND SAS.IsTardy = 1
                                                   AND PV.TrackTardies = 1
                                        GROUP BY   ClsSectionId
                                                  ,PV.TardiesMakingAbsence;

                            --Drop table @MyTardyTable
                            DECLARE @TotalTardyAbsentDays DECIMAL(18, 2);
                            SET @TotalTardyAbsentDays = (
                                                        SELECT ISNULL(SUM(AbsentHours), 0)
                                                        FROM   @MyTardyTable
                                                        );

                            --Print @TotalTardyAbsentDays

                            UPDATE @attendanceSummary
                            SET    AdjustedPresentDays = AdjustedPresentDays - @TotalTardyAbsentDays
                                  ,AdjustedAbsentDays = AdjustedAbsentDays + @TotalTardyAbsentDays
                            WHERE  StuEnrollId = @StuEnrollId
                                   AND StudentAttendedDate = (
                                                             SELECT   TOP 1 StudentAttendedDate
                                                             FROM     @attendanceSummary
                                                             WHERE    StuEnrollId = @StuEnrollId
                                                             ORDER BY StudentAttendedDate DESC
                                                             );

                        END;
                END; -- Step 3 
                -- END -- Step 3 

                -- Step 4  --  InsertAttendance_Class_Minutes
                --         --  TrackSapAttendance = ''byclass''
                BEGIN -- Step 4  --  InsertAttendance_Class_Minutes
                    -- By Class and Attendance Unit Type - Minutes and Clock Hour

                    IF @UnitTypeDescrip IN ( ''minutes'', ''clock hours'' )
                       AND @TrackSapAttendance = ''byclass''
                        BEGIN

                            --BEGIN
                            --    --DELETE FROM syStudentAttendanceSummary
                            --    --WHERE StuEnrollId = @StuEnrollId
                            --    --      AND StudentAttendedDate <= @OffsetDate;
                            --END;
                            DECLARE GetAttendance_Cursor CURSOR FOR
                                SELECT   *
                                        ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                FROM     (
                                         SELECT     DISTINCT t1.StuEnrollId
                                                            ,t1.ClsSectionId
                                                            ,t1.MeetDate
                                                            ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                            ,t4.StartDate
                                                            ,t4.EndDate
                                                            ,t5.PeriodDescrip
                                                            ,t1.Actual
                                                            ,t1.Excused
                                                            ,CASE WHEN (
                                                                       t1.Actual = 0
                                                                       AND t1.Excused = 0
                                                                       ) THEN t1.Scheduled
                                                                  ELSE CASE WHEN (
                                                                                 t1.Actual <> 9999.00
                                                                                 AND t1.Actual < t1.Scheduled
                                                                                 ) THEN ( t1.Scheduled - t1.Actual )
                                                                            ELSE 0
                                                                       END
                                                             END AS Absent
                                                            ,t1.Scheduled AS ScheduledMinutes
                                                            ,CASE WHEN (
                                                                       t1.Actual > 0
                                                                       AND t1.Actual < t1.Scheduled
                                                                       ) THEN ( t1.Scheduled - t1.Actual )
                                                                  ELSE 0
                                                             END AS TardyMinutes
                                                            ,t1.Tardy AS Tardy
                                                            ,t3.TrackTardies
                                                            ,t3.TardiesMakingAbsence
                                                            ,t3.PrgVerId
                                         FROM       atClsSectAttendance t1
                                         INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                         INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                         INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                         INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                            AND (
                                                                                CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                                AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                                )
                                                                            AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                         INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                         INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                         INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                         INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                         INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                         INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                         WHERE      t2.StuEnrollId = @StuEnrollId
                                                    AND (
                                                        AAUT1.UnitTypeDescrip IN ( ''Minutes'', ''Clock Hours'' )
                                                        OR AAUT2.UnitTypeDescrip IN ( ''Minutes'', ''Clock Hours'' )
                                                        )
                                                    AND t1.Actual <> 9999
                                                    AND t1.MeetDate <= @OffsetDate
                                         UNION
                                         SELECT     DISTINCT t1.StuEnrollId
                                                            ,NULL AS ClsSectionId
                                                            ,t1.RecordDate AS MeetDate
                                                            ,DATENAME(dw, t1.RecordDate) AS WeekDay
                                                            ,NULL AS StartDate
                                                            ,NULL AS EndDate
                                                            ,NULL AS PeriodDescrip
                                                            ,t1.ActualHours
                                                            ,NULL AS Excused
                                                            ,CASE WHEN ( t1.ActualHours = 0 ) THEN t1.SchedHours
                                                                  ELSE 0
                                                             --ELSE 
                                                             --Case when (t1.ActualHours <> 9999.00 and t1.ActualHours < t1.SchedHours)
                                                             --		THEN (t1.SchedHours - t1.ActualHours)
                                                             --		ELSE 
                                                             --			0
                                                             --		End
                                                             END AS Absent
                                                            ,t1.SchedHours AS ScheduledMinutes
                                                            ,CASE WHEN (
                                                                       t1.ActualHours <> 9999.00
                                                                       AND t1.ActualHours > 0
                                                                       AND t1.ActualHours < t1.SchedHours
                                                                       ) THEN ( t1.SchedHours - t1.ActualHours )
                                                                  ELSE 0
                                                             END AS TardyMinutes
                                                            ,NULL AS Tardy
                                                            ,t3.TrackTardies
                                                            ,t3.TardiesMakingAbsence
                                                            ,t3.PrgVerId
                                         FROM       arStudentClockAttendance t1
                                         INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                         INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                         WHERE      t2.StuEnrollId = @StuEnrollId
                                                    AND t1.Converted = 1
                                                    AND t1.ActualHours <> 9999
                                                    AND t1.RecordDate <= @OffsetDate
                                         ) dt
                                ORDER BY StuEnrollId
                                        ,MeetDate;
                            OPEN GetAttendance_Cursor;
                            FETCH NEXT FROM GetAttendance_Cursor
                            INTO @StuEnrollId
                                ,@ClsSectionId
                                ,@MeetDate
                                ,@WeekDay
                                ,@StartDate
                                ,@EndDate
                                ,@PeriodDescrip
                                ,@Actual
                                ,@Excused
                                ,@Absent
                                ,@ScheduledMinutes
                                ,@TardyMinutes
                                ,@tardy
                                ,@tracktardies
                                ,@TardiesMakingAbsence
                                ,@PrgVerId
                                ,@rownumber;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @boolReset = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                            SET @boolReset = 1;
                                        END;


                                    -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                                    IF @Actual <> 9999.00
                                        BEGIN
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                                            -- If there are make up hrs deduct that otherwise it will be added again in progress report
                                            IF (
                                               @Actual > 0
                                               AND @Actual > @ScheduledMinutes
                                               AND @Actual <> 9999.00
                                               )
                                                BEGIN
                                                    SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                END;
                                            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                                        END;

                                    -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                                    SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                                    -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                                    IF (
                                       @Actual > 0
                                       AND @Actual < @ScheduledMinutes
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                        END;

                                    -- Track how many days student has been tardy only when 
                                    -- program version requires to track tardy
                                    IF (
                                       @tracktardies = 1
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;


                                    -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
                                    -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
                                    -- when student is tardy the second time, that second occurance will be considered as
                                    -- absence
                                    -- Variable @intTardyBreakpoint tracks how many times the student was tardy
                                    -- Variable @tardiesMakingAbsence tracks the tardy rule
                                    IF (
                                       @tracktardies = 1
                                       AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                            SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;

                                    -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                       @Actual > 0
                                       AND @Actual > @ScheduledMinutes
                                       AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;


                                    IF ( @tracktardies = 1 )
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                                        END;



                                    INSERT INTO @attendanceSummary (
                                                                   StuEnrollId
                                                                  ,ClsSectionId
                                                                  ,StudentAttendedDate
                                                                  ,ScheduledDays
                                                                  ,ActualDays
                                                                  ,ActualRunningScheduledDays
                                                                  ,ActualRunningPresentDays
                                                                  ,ActualRunningAbsentDays
                                                                  ,ActualRunningMakeupDays
                                                                  ,ActualRunningTardyDays
                                                                  ,AdjustedPresentDays
                                                                  ,AdjustedAbsentDays
                                                                  ,AttendanceTrackType
                                                                  ,ModUser
                                                                  ,ModDate
                                                                   )
                                    VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays
                                            ,@ActualRunningPresentHours, @ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours
                                            ,@AdjustedPresentDaysComputed, @AdjustedRunningAbsentHours, ''Post Attendance by Class Min'', ''sa'', GETDATE());

                                    UPDATE @attendanceSummary
                                    SET    TardiesMakingAbsence = @TardiesMakingAbsence
                                    WHERE  StuEnrollId = @StuEnrollId;
                                    SET @PrevStuEnrollId = @StuEnrollId;

                                    FETCH NEXT FROM GetAttendance_Cursor
                                    INTO @StuEnrollId
                                        ,@ClsSectionId
                                        ,@MeetDate
                                        ,@WeekDay
                                        ,@StartDate
                                        ,@EndDate
                                        ,@PeriodDescrip
                                        ,@Actual
                                        ,@Excused
                                        ,@Absent
                                        ,@ScheduledMinutes
                                        ,@TardyMinutes
                                        ,@tardy
                                        ,@tracktardies
                                        ,@TardiesMakingAbsence
                                        ,@PrgVerId
                                        ,@rownumber;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
                        END;
                END; --  Step 3  --   InsertAttendance_Class_Minutes
                --END --  Step 3  --   InsertAttendance_Class_Minutes


                --Select * from arAttUnitType


                -- By Minutes/Day
                -- remove clock hour from here
                IF @UnitTypeDescrip IN ( ''minutes'' )
                   AND @TrackSapAttendance = ''byday''
                    -- -- PRINT GETDATE();	
                    ---- PRINT @UnitTypeId;
                    ---- PRINT @TrackSapAttendance;
                    BEGIN
                        --BEGIN
                        --    --DELETE FROM syStudentAttendanceSummary
                        --    --WHERE StuEnrollId = @StuEnrollId
                        --    --      AND StudentAttendedDate <= @OffsetDate;
                        --END;
                        DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
                            SELECT     t1.StuEnrollId
                                      ,NULL AS ClsSectionId
                                      ,t1.RecordDate AS MeetDate
                                      ,t1.ActualHours
                                      ,t1.SchedHours AS ScheduledMinutes
                                      ,CASE WHEN (
                                                 (
                                                 t1.SchedHours >= 1
                                                 AND t1.SchedHours NOT IN ( 999, 9999 )
                                                 )
                                                 AND t1.ActualHours = 0
                                                 ) THEN t1.SchedHours
                                            ELSE 0
                                       END AS Absent
                                      ,t1.isTardy
                                      ,(
                                       SELECT ISNULL(SUM(SchedHours), 0)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999, 9999 )
                                                  AND t1.ActualHours NOT IN ( 999, 9999 )
                                                  )
                                       ) AS ActualRunningScheduledHours
                                      ,(
                                       SELECT SUM(ActualHours)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999, 9999 )
                                                  )
                                              AND ActualHours >= 1
                                              AND ActualHours NOT IN ( 999, 9999 )
                                       ) AS ActualRunningPresentHours
                                      ,(
                                       SELECT COUNT(ActualHours)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999, 9999 )
                                                  )
                                              AND ActualHours = 0
                                              AND ActualHours NOT IN ( 999, 9999 )
                                       ) AS ActualRunningAbsentHours
                                      ,(
                                       SELECT SUM(ActualHours)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND SchedHours = 0
                                              AND ActualHours >= 1
                                              AND ActualHours NOT IN ( 999, 9999 )
                                       ) AS ActualRunningMakeupHours
                                      ,(
                                       SELECT SUM(SchedHours - ActualHours)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND (
                                                  (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999, 9999 )
                                                  )
                                                  AND ActualHours >= 1
                                                  AND ActualHours NOT IN ( 999, 9999 )
                                                  )
                                              AND isTardy = 1
                                       ) AS ActualRunningTardyHours
                                      ,t3.TrackTardies
                                      ,t3.TardiesMakingAbsence
                                      ,t3.PrgVerId
                                      ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
                            FROM       arStudentClockAttendance t1
                            INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                            INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                            INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                            --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                            WHERE      AAUT1.UnitTypeDescrip IN ( ''Minutes'' )
                                       AND t2.StuEnrollId = @StuEnrollId
                                       AND t1.ActualHours <> 9999.00
                                       AND t1.RecordDate <= @OffsetDate
                            ORDER BY   t1.StuEnrollId
                                      ,MeetDate;
                        OPEN GetAttendance_Cursor;
                        --Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                        FETCH NEXT FROM GetAttendance_Cursor
                        INTO @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@Actual
                            ,@ScheduledMinutes
                            ,@Absent
                            ,@IsTardy
                            ,@ActualRunningScheduledHours
                            ,@ActualRunningPresentHours
                            ,@ActualRunningAbsentHours
                            ,@ActualRunningMakeupHours
                            ,@ActualRunningTardyHours
                            ,@tracktardies
                            ,@TardiesMakingAbsence
                            ,@PrgVerId
                            ,@rownumber;

                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @ActualRunningMakeupHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        WHILE @@FETCH_STATUS = 0
                            BEGIN

                                IF @PrevStuEnrollId <> @StuEnrollId
                                    BEGIN
                                        SET @ActualRunningPresentHours = 0;
                                        SET @ActualRunningAbsentHours = 0;
                                        SET @intTardyBreakPoint = 0;
                                        SET @ActualRunningTardyHours = 0;
                                        SET @AdjustedRunningPresentHours = 0;
                                        SET @AdjustedRunningAbsentHours = 0;
                                        SET @ActualRunningScheduledDays = 0;
                                    END;

                                IF (
                                   @ScheduledMinutes >= 1
                                   AND (
                                       @Actual <> 9999
                                       AND @Actual <> 999
                                       )
                                   AND (
                                       @ScheduledMinutes <> 9999
                                       AND @Actual <> 999
                                       )
                                   )
                                    BEGIN
                                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + ISNULL(@ScheduledMinutes, 0);
                                    END;
                                ELSE
                                    BEGIN
                                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0);
                                    END;

                                IF (
                                   @Actual <> 9999
                                   AND @Actual <> 999
                                   )
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual - ( @Actual - @ScheduledMinutes );
                                    END;
                                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;

                                IF (
                                   @Actual > 0
                                   AND @Actual < @ScheduledMinutes
                                   )
                                    BEGIN
                                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                                    END;
                                -- Make up hours
                                --sched=5, Actual =7, makeup= 2,ab = 0
                                --sched=0, Actual =7, makeup= 7,ab = 0
                                IF (
                                   @Actual > 0
                                   AND @ScheduledMinutes > 0
                                   AND @Actual > @ScheduledMinutes
                                   AND (
                                       @Actual <> 9999
                                       AND @Actual <> 999
                                       )
                                   )
                                    BEGIN
                                        SET @ActualRunningMakeupHours = @ActualRunningMakeupHours + ( @Actual - @ScheduledMinutes );
                                    END;


                                IF (
                                   @Actual <> 9999
                                   AND @Actual <> 999
                                   )
                                    BEGIN
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                                    END;
                                IF (
                                   @Actual = 0
                                   AND @ScheduledMinutes >= 1
                                   AND @ScheduledMinutes NOT IN ( 999, 9999 )
                                   )
                                    BEGIN
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                                    END;
                                -- Absent hours
                                --1. sched = 5, Actual = 2 then Ab = (5-3) = 2
                                IF (
                                   @Absent = 0
                                   AND @ActualRunningAbsentHours > 0
                                   AND ( @Actual < @ScheduledMinutes )
                                   )
                                    BEGIN
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                                    END;
                                IF (
                                   @Actual > 0
                                   AND @Actual < @ScheduledMinutes
                                   AND (
                                       @Actual <> 9999.00
                                       AND @Actual <> 999.00
                                       )
                                   )
                                    BEGIN
                                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                    END;

                                IF @tracktardies = 1
                                   AND (
                                       @TardyMinutes > 0
                                       OR @IsTardy = 1
                                       )
                                    BEGIN
                                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                    END;
                                IF (
                                   @tracktardies = 1
                                   AND @intTardyBreakPoint = @TardiesMakingAbsence
                                   )
                                    BEGIN
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Actual; --@TardyMinutes
                                        SET @intTardyBreakPoint = 0;
                                    END;

                                INSERT INTO @attendanceSummary (
                                                               StuEnrollId
                                                              ,ClsSectionId
                                                              ,StudentAttendedDate
                                                              ,ScheduledDays
                                                              ,ActualDays
                                                              ,ActualRunningScheduledDays
                                                              ,ActualRunningPresentDays
                                                              ,ActualRunningAbsentDays
                                                              ,ActualRunningMakeupDays
                                                              ,ActualRunningTardyDays
                                                              ,AdjustedPresentDays
                                                              ,AdjustedAbsentDays
                                                              ,AttendanceTrackType
                                                              ,ModUser
                                                              ,ModDate
                                                               )
                                VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays
                                        ,@ActualRunningPresentHours, @ActualRunningAbsentHours, @ActualRunningMakeupHours, @ActualRunningTardyHours
                                        ,@AdjustedRunningPresentHours, @AdjustedRunningAbsentHours, ''Post Attendance by Class'', ''sa'', GETDATE());

                                UPDATE @attendanceSummary
                                SET    TardiesMakingAbsence = @TardiesMakingAbsence
                                WHERE  StuEnrollId = @StuEnrollId;
                                --end
                                SET @PrevStuEnrollId = @StuEnrollId;
                                FETCH NEXT FROM GetAttendance_Cursor
                                INTO @StuEnrollId
                                    ,@ClsSectionId
                                    ,@MeetDate
                                    ,@Actual
                                    ,@ScheduledMinutes
                                    ,@Absent
                                    ,@IsTardy
                                    ,@ActualRunningScheduledHours
                                    ,@ActualRunningPresentHours
                                    ,@ActualRunningAbsentHours
                                    ,@ActualRunningMakeupHours
                                    ,@ActualRunningTardyHours
                                    ,@tracktardies
                                    ,@TardiesMakingAbsence
                                    ,@PrgVerId
                                    ,@rownumber;
                            END;
                        CLOSE GetAttendance_Cursor;
                        DEALLOCATE GetAttendance_Cursor;
                    END;

                ---- PRINT ''Does it get to Clock Hour/PA'';
                ---- PRINT @UnitTypeId;
                ---- PRINT @TrackSapAttendance;
                -- By Day and PA, Clock Hour
                -- -- Step 2  --  InsertAttendance_Day_PresentAbsent  -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'')  and TrackSapAttendance = ''byday''
                BEGIN -- Step 2  --  InsertAttendance_Day_PresentAbsent  -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'') and TrackSapAttendance = ''byday''
                    IF @UnitTypeDescrip IN ( ''present absent'', ''clock hours'' )
                       AND @TrackSapAttendance = ''byday''
                        BEGIN
                            --BEGIN
                            --    --DELETE FROM syStudentAttendanceSummary
                            --    --WHERE StuEnrollId = @StuEnrollId
                            --    --      AND StudentAttendedDate <= @OffsetDate;
                            --END;
                            ---- PRINT ''By Day inside day'';
                            DECLARE GetAttendance_Cursor CURSOR FOR
                                SELECT     t1.StuEnrollId
                                          ,t1.RecordDate
                                          ,t1.ActualHours
                                          ,t1.SchedHours
                                          ,CASE WHEN (
                                                     (
                                                     t1.SchedHours >= 1
                                                     AND t1.SchedHours NOT IN ( 999, 9999 )
                                                     )
                                                     AND t1.ActualHours = 0
                                                     ) THEN t1.SchedHours
                                                ELSE 0
                                           END AS Absent
                                          ,t1.isTardy
                                          ,t3.TrackTardies
                                          ,t3.TardiesMakingAbsence
                                FROM       arStudentClockAttendance t1
                                INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                                WHERE -- Unit Types: Present Absent and Clock Hour
                                           AAUT1.UnitTypeDescrip IN ( ''present absent'', ''clock hours'' )
                                           AND ActualHours <> 9999.00
                                           AND t2.StuEnrollId = @StuEnrollId
                                           AND t1.RecordDate <= @OffsetDate
                                ORDER BY   t1.StuEnrollId
                                          ,t1.RecordDate;
                            OPEN GetAttendance_Cursor;
                            --Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                            FETCH NEXT FROM GetAttendance_Cursor
                            INTO @StuEnrollId
                                ,@MeetDate
                                ,@Actual
                                ,@ScheduledMinutes
                                ,@Absent
                                ,@IsTardy
                                ,@tracktardies
                                ,@TardiesMakingAbsence;

                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                        END;

                                    IF (
                                       @Actual <> 9999
                                       AND @Actual <> 999
                                       )
                                        BEGIN
                                            SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + @ScheduledMinutes;
                                            SET @ActualRunningPresentHours = ISNULL(@ActualRunningPresentHours, 0) + @Actual;
                                            SET @AdjustedRunningPresentHours = ISNULL(@AdjustedRunningPresentHours, 0) + @Actual;
                                        END;
                                    SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + @Absent;
                                    IF (
                                       @Actual = 0
                                       AND @ScheduledMinutes >= 1
                                       AND @ScheduledMinutes NOT IN ( 999, 9999 )
                                       )
                                        BEGIN
                                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                                        END;

                                    -- NWH 
                                    -- If Scheduled = 3.0 hrs and Actual = 1.0 Then 2 hrs is considered absent
                                    IF (
                                       @ScheduledMinutes >= 1
                                       AND @ScheduledMinutes NOT IN ( 999, 9999 )
                                       AND @Actual > 0
                                       AND ( @Actual < @ScheduledMinutes )
                                       )
                                        BEGIN
                                            SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                                            SET @AdjustedRunningAbsentHours = ISNULL(@AdjustedRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                                        END;

                                    IF (
                                       @tracktardies = 1
                                       AND @IsTardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + 1;
                                        END;
                                    --commented by balaji on 10/22/2012 as report (rdl) doesn''t add days attended and make up days
                                    ---- If there are make up hrs deduct that otherwise it will be added again in progress report
                                    IF (
                                       @Actual > 0
                                       AND @Actual > @ScheduledMinutes
                                       AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                        END;

                                    IF @tracktardies = 1
                                       AND (
                                           @TardyMinutes > 0
                                           OR @IsTardy = 1
                                           )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;



                                    -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                       @Actual > 0
                                       AND @Actual > @ScheduledMinutes
                                       AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;

                                    IF (
                                       @tracktardies = 1
                                       AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;


                                    INSERT INTO @attendanceSummary (
                                                                   StuEnrollId
                                                                  ,ClsSectionId
                                                                  ,StudentAttendedDate
                                                                  ,ScheduledDays
                                                                  ,ActualDays
                                                                  ,ActualRunningScheduledDays
                                                                  ,ActualRunningPresentDays
                                                                  ,ActualRunningAbsentDays
                                                                  ,ActualRunningMakeupDays
                                                                  ,ActualRunningTardyDays
                                                                  ,AdjustedPresentDays
                                                                  ,AdjustedAbsentDays
                                                                  ,AttendanceTrackType
                                                                  ,ModUser
                                                                  ,ModDate
                                                                  ,TardiesMakingAbsence
                                                                   )
                                    VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, ISNULL(@ScheduledMinutes, 0), @Actual
                                            ,ISNULL(@ActualRunningScheduledDays, 0), ISNULL(@ActualRunningPresentHours, 0)
                                            ,ISNULL(@ActualRunningAbsentHours, 0), ISNULL(@MakeupHours, 0), ISNULL(@ActualRunningTardyHours, 0)
                                            ,ISNULL(@AdjustedRunningPresentHours, 0), ISNULL(@AdjustedRunningAbsentHours, 0), ''Post Attendance by Class'', ''sa''
                                            ,GETDATE(), @TardiesMakingAbsence );

                                    --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                                    --end
                                    SET @PrevStuEnrollId = @StuEnrollId;
                                    FETCH NEXT FROM GetAttendance_Cursor
                                    INTO @StuEnrollId
                                        ,@MeetDate
                                        ,@Actual
                                        ,@ScheduledMinutes
                                        ,@Absent
                                        ,@IsTardy
                                        ,@tracktardies
                                        ,@TardiesMakingAbsence;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
                        END;
                END;

                --end

                -- Based on grade rounding round the final score and current score
                --Declare @PrevTermId uniqueidentifier,@PrevReqId uniqueidentifier,@RowCount int,@FinalScore decimal(18,4),@CurrentScore decimal(18,4)
                --declare @ReqId uniqueidentifier,@CourseCodeDescrip varchar(100),@FinalGrade varchar(10),@sysComponentTypeId int
                --declare @CreditsAttempted decimal(18,4),@Grade varchar(10),@IsPass bit,@IsCreditsAttempted bit,@IsCreditsEarned bit
                --declare @IsInGPA bit,@FinAidCredits decimal(18,4),@CurrentGrade varchar(10),@CourseCredits decimal(18,4)
                DECLARE @GPA DECIMAL(18, 4);

                DECLARE @PrevReqId UNIQUEIDENTIFIER
                       ,@PrevTermId UNIQUEIDENTIFIER
                       ,@CreditsAttempted DECIMAL(18, 2)
                       ,@CreditsEarned DECIMAL(18, 2);
                DECLARE @reqid UNIQUEIDENTIFIER
                       ,@CourseCodeDescrip VARCHAR(50)
                       ,@FinalGrade VARCHAR(50)
                       ,@FinalScore DECIMAL(18, 2)
                       ,@Grade VARCHAR(50);
                DECLARE @IsPass BIT
                       ,@IsCreditsAttempted BIT
                       ,@IsCreditsEarned BIT
                       ,@CurrentScore DECIMAL(18, 2)
                       ,@CurrentGrade VARCHAR(10)
                       ,@FinalGPA DECIMAL(18, 2);
                DECLARE @sysComponentTypeId INT
                       ,@RowCount INT;
                DECLARE @IsInGPA BIT;
                DECLARE @CourseCredits DECIMAL(18, 2);
                DECLARE @FinAidCreditsEarned DECIMAL(18, 2)
                       ,@FinAidCredits DECIMAL(18, 2);

                DECLARE GetCreditsSummary_Cursor CURSOR FOR
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,T.TermId
                                       ,T.TermDescrip
                                       ,T.StartDate
                                       ,R.ReqId
                                       ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                       ,RES.Score AS FinalScore
                                       ,RES.GrdSysDetailId AS FinalGrade
                                       ,GCT.SysComponentTypeId
                                       ,R.Credits AS CreditsAttempted
                                       ,CS.ClsSectionId
                                       ,GSD.Grade
                                       ,GSD.IsPass
                                       ,GSD.IsCreditsAttempted
                                       ,GSD.IsCreditsEarned
                                       ,SE.PrgVerId
                                       ,GSD.IsInGPA
                                       ,R.FinAidCredits AS FinAidCredits
                    FROM       arStuEnrollments SE
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                    INNER JOIN arTerm T ON CS.TermId = T.TermId
                    INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                    LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                    LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                    LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                    LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    WHERE      SE.StuEnrollId = @StuEnrollId
                               AND (
                                   RES.DateCompleted IS NOT NULL
                                   AND RES.DateCompleted <= @OffsetDate
                                   )
                    ORDER BY   T.StartDate
                              ,T.TermDescrip
                              ,R.ReqId;

                DECLARE @varGradeRounding VARCHAR(3);
                DECLARE @roundfinalscore DECIMAL(18, 4);
                SET @varGradeRounding = (
                                        SELECT Value
                                        FROM   syConfigAppSetValues
                                        WHERE  SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@FinalGrade
                    ,@sysComponentTypeId
                    ,@CreditsAttempted
                    ,@ClsSectionId
                    ,@Grade
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN


                        SET @CurrentScore = (
                                            SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                        ELSE NULL
                                                   END AS CurrentScore
                                            FROM   (
                                                   SELECT   InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                   FROM     (
                                                            SELECT   C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,ISNULL(C.Weight, 0) AS Weight
                                                                    ,C.Number AS MinNumber
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter AS Param
                                                                    ,X.GrdScaleId
                                                                    ,SUM(GR.Score) AS Score
                                                                    ,COUNT(D.Descrip) AS NumberOfComponents
                                                            FROM     (
                                                                     SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                            ,A.EffectiveDate
                                                                                            ,B.GrdScaleId
                                                                     FROM     arGrdBkWeights A
                                                                             ,arClassSections B
                                                                     WHERE    A.ReqId = B.ReqId
                                                                              AND A.EffectiveDate <= B.StartDate
                                                                              AND B.ClsSectionId = @ClsSectionId
                                                                     ORDER BY A.EffectiveDate DESC
                                                                     ) X
                                                                    ,arGrdBkWgtDetails C
                                                                    ,arGrdComponentTypes D
                                                                    ,arGrdBkResults GR
                                                            WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                     AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                     AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                     AND D.SysComponentTypeId NOT IN ( 500, 503 )
                                                                     AND GR.StuEnrollId = @StuEnrollId
                                                                     AND GR.ClsSectionId = @ClsSectionId
                                                                     AND GR.Score IS NOT NULL
                                                            GROUP BY C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,C.Weight
                                                                    ,C.Number
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter
                                                                    ,X.GrdScaleId
                                                            ) S
                                                   GROUP BY InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                   ) FinalTblToComputeCurrentScore
                                            );
                        IF ( @CurrentScore IS NULL )
                            BEGIN
                                -- instructor grade books
                                SET @CurrentScore = (
                                                    SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                                ELSE NULL
                                                           END AS CurrentScore
                                                    FROM   (
                                                           SELECT   InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight AS GradeBookWeight
                                                                   ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                         ELSE 0
                                                                    END AS ActualWeight
                                                           FROM     (
                                                                    SELECT   C.InstrGrdBkWgtDetailId
                                                                            ,D.Code
                                                                            ,D.Descrip
                                                                            ,ISNULL(C.Weight, 0) AS Weight
                                                                            ,C.Number AS MinNumber
                                                                            ,C.GrdPolicyId
                                                                            ,C.Parameter AS Param
                                                                            ,X.GrdScaleId
                                                                            ,SUM(GR.Score) AS Score
                                                                            ,COUNT(D.Descrip) AS NumberOfComponents
                                                                    FROM     (
                                                                             --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
                                                                             --FROM          arGrdBkWeights A,arClassSections B        
                                                                             --WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
                                                                             --ORDER BY      A.EffectiveDate DESC
                                                                             SELECT DISTINCT TOP 1 t1.InstrGrdBkWgtId
                                                                                                  ,t1.GrdScaleId
                                                                             FROM   arClassSections t1
                                                                                   ,arGrdBkWeights t2
                                                                             WHERE  t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                                    AND t1.ClsSectionId = @ClsSectionId
                                                                             ) X
                                                                            ,arGrdBkWgtDetails C
                                                                            ,arGrdComponentTypes D
                                                                            ,arGrdBkResults GR
                                                                    WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                             AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                             AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                             AND
                                                                        -- D.SysComponentTypeID not in (500,503) and 
                                                                        GR.StuEnrollId = @StuEnrollId
                                                                             AND GR.ClsSectionId = @ClsSectionId
                                                                             AND GR.Score IS NOT NULL
                                                                    GROUP BY C.InstrGrdBkWgtDetailId
                                                                            ,D.Code
                                                                            ,D.Descrip
                                                                            ,C.Weight
                                                                            ,C.Number
                                                                            ,C.GrdPolicyId
                                                                            ,C.Parameter
                                                                            ,X.GrdScaleId
                                                                    ) S
                                                           GROUP BY InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight
                                                                   ,NumberOfComponents
                                                           ) FinalTblToComputeCurrentScore
                                                    );

                            END;


                        -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore, 0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;

                        UPDATE syCreditSummary
                        SET    FinalScore = @FinalScore
                              ,CurrentScore = @CurrentScore
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId
                               AND ReqId = @reqid;

                        --Average calculation

                        -- Term Average
                        SET @TermAverageCount = (
                                                SELECT COUNT(*)
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                       AND TermId = @TermId
                                                       AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                              SELECT SUM(FinalScore)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND TermId = @TermId
                                                     AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount;

                        -- Cumulative Average
                        SET @cumAveragecount = (
                                               SELECT COUNT(*)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                             SELECT SUM(FinalScore)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount;

                        UPDATE syCreditSummary
                        SET    Average = @TermAverage
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId;

                        --Update Cumulative GPA
                        UPDATE syCreditSummary
                        SET    CumAverage = @CumAverage
                        WHERE  StuEnrollId = @StuEnrollId;


                        FETCH NEXT FROM GetCreditsSummary_Cursor
                        INTO @StuEnrollId
                            ,@TermId
                            ,@TermDescrip
                            ,@termstartdate1
                            ,@reqid
                            ,@CourseCodeDescrip
                            ,@FinalScore
                            ,@FinalGrade
                            ,@sysComponentTypeId
                            ,@CreditsAttempted
                            ,@ClsSectionId
                            ,@Grade
                            ,@IsPass
                            ,@IsCreditsAttempted
                            ,@IsCreditsEarned
                            ,@PrgVerId
                            ,@IsInGPA
                            ,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;


                DECLARE GetCreditsSummary_Cursor CURSOR FOR
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,T.TermId
                                       ,T.TermDescrip
                                       ,T.StartDate
                                       ,R.ReqId
                                       ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                       ,RES.Score AS FinalScore
                                       ,RES.GrdSysDetailId AS FinalGrade
                                       ,GCT.SysComponentTypeId
                                       ,R.Credits AS CreditsAttempted
                                       ,CS.ClsSectionId
                                       ,GSD.Grade
                                       ,GSD.IsPass
                                       ,GSD.IsCreditsAttempted
                                       ,GSD.IsCreditsEarned
                                       ,SE.PrgVerId
                                       ,GSD.IsInGPA
                                       ,R.FinAidCredits AS FinAidCredits
                    FROM       arStuEnrollments SE
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                                     AND RES.TermId = CS.TermId
                    INNER JOIN arTerm T ON CS.TermId = T.TermId
                    INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                    LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                    LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                    LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                    LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    WHERE      SE.StuEnrollId = @StuEnrollId
                               AND (
                                   RES.CompletedDate IS NOT NULL
                                   AND RES.CompletedDate <= @OffsetDate
                                   )
                    ORDER BY   T.StartDate
                              ,T.TermDescrip
                              ,R.ReqId;


                SET @varGradeRounding = (
                                        SELECT Value
                                        FROM   syConfigAppSetValues
                                        WHERE  SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@FinalGrade
                    ,@sysComponentTypeId
                    ,@CreditsAttempted
                    ,@ClsSectionId
                    ,@Grade
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        SET @CurrentScore = (
                                            SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                        ELSE NULL
                                                   END AS CurrentScore
                                            FROM   (
                                                   SELECT   InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                   FROM     (
                                                            SELECT   C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,ISNULL(C.Weight, 0) AS Weight
                                                                    ,C.Number AS MinNumber
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter AS Param
                                                                    ,X.GrdScaleId
                                                                    ,SUM(GR.Score) AS Score
                                                                    ,COUNT(D.Descrip) AS NumberOfComponents
                                                            FROM     (
                                                                     SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                            ,A.EffectiveDate
                                                                                            ,B.GrdScaleId
                                                                     FROM     arGrdBkWeights A
                                                                             ,arClassSections B
                                                                     WHERE    A.ReqId = B.ReqId
                                                                              AND A.EffectiveDate <= B.StartDate
                                                                              AND B.ClsSectionId = @ClsSectionId
                                                                     ORDER BY A.EffectiveDate DESC
                                                                     ) X
                                                                    ,arGrdBkWgtDetails C
                                                                    ,arGrdComponentTypes D
                                                                    ,arGrdBkResults GR
                                                            WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                     AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                     AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                     AND D.SysComponentTypeId NOT IN ( 500, 503 )
                                                                     AND GR.StuEnrollId = @StuEnrollId
                                                                     AND GR.ClsSectionId = @ClsSectionId
                                                                     AND GR.Score IS NOT NULL
                                                            GROUP BY C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,C.Weight
                                                                    ,C.Number
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter
                                                                    ,X.GrdScaleId
                                                            ) S
                                                   GROUP BY InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                   ) FinalTblToComputeCurrentScore
                                            );
                        IF ( @CurrentScore IS NULL )
                            BEGIN
                                -- instructor grade books
                                SET @CurrentScore = (
                                                    SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                                ELSE NULL
                                                           END AS CurrentScore
                                                    FROM   (
                                                           SELECT   InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight AS GradeBookWeight
                                                                   ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                         ELSE 0
                                                                    END AS ActualWeight
                                                           FROM     (
                                                                    SELECT   C.InstrGrdBkWgtDetailId
                                                                            ,D.Code
                                                                            ,D.Descrip
                                                                            ,ISNULL(C.Weight, 0) AS Weight
                                                                            ,C.Number AS MinNumber
                                                                            ,C.GrdPolicyId
                                                                            ,C.Parameter AS Param
                                                                            ,X.GrdScaleId
                                                                            ,SUM(GR.Score) AS Score
                                                                            ,COUNT(D.Descrip) AS NumberOfComponents
                                                                    FROM     (
                                                                             --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
                                                                             --FROM          arGrdBkWeights A,arClassSections B        
                                                                             --WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
                                                                             --ORDER BY      A.EffectiveDate DESC
                                                                             SELECT DISTINCT TOP 1 t1.InstrGrdBkWgtId
                                                                                                  ,t1.GrdScaleId
                                                                             FROM   arClassSections t1
                                                                                   ,arGrdBkWeights t2
                                                                             WHERE  t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                                    AND t1.ClsSectionId = @ClsSectionId
                                                                             ) X
                                                                            ,arGrdBkWgtDetails C
                                                                            ,arGrdComponentTypes D
                                                                            ,arGrdBkResults GR
                                                                    WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                             AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                             AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                             AND
                                                                        -- D.SysComponentTypeID not in (500,503) and 
                                                                        GR.StuEnrollId = @StuEnrollId
                                                                             AND GR.ClsSectionId = @ClsSectionId
                                                                             AND GR.Score IS NOT NULL
                                                                    GROUP BY C.InstrGrdBkWgtDetailId
                                                                            ,D.Code
                                                                            ,D.Descrip
                                                                            ,C.Weight
                                                                            ,C.Number
                                                                            ,C.GrdPolicyId
                                                                            ,C.Parameter
                                                                            ,X.GrdScaleId
                                                                    ) S
                                                           GROUP BY InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight
                                                                   ,NumberOfComponents
                                                           ) FinalTblToComputeCurrentScore
                                                    );

                            END;

                        -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore, 0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;

                        UPDATE syCreditSummary
                        SET    FinalScore = @FinalScore
                              ,CurrentScore = @CurrentScore
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId
                               AND ReqId = @reqid;

                        --Average calculation
                        --			declare @CumAverage decimal(18,2),@cumAverageSum decimal(18,2),@cumAveragecount int

                        -- Term Average
                        SET @TermAverageCount = (
                                                SELECT COUNT(*)
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                       AND TermId = @TermId
                                                       AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                              SELECT SUM(FinalScore)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND TermId = @TermId
                                                     AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount;

                        -- Cumulative Average
                        SET @cumAveragecount = (
                                               SELECT COUNT(*)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                             SELECT SUM(FinalScore)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount;

                        UPDATE syCreditSummary
                        SET    Average = @TermAverage
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId;

                        --Update Cumulative GPA
                        UPDATE syCreditSummary
                        SET    CumAverage = @CumAverage
                        WHERE  StuEnrollId = @StuEnrollId;


                        FETCH NEXT FROM GetCreditsSummary_Cursor
                        INTO @StuEnrollId
                            ,@TermId
                            ,@TermDescrip
                            ,@termstartdate1
                            ,@reqid
                            ,@CourseCodeDescrip
                            ,@FinalScore
                            ,@FinalGrade
                            ,@sysComponentTypeId
                            ,@CreditsAttempted
                            ,@ClsSectionId
                            ,@Grade
                            ,@IsPass
                            ,@IsCreditsAttempted
                            ,@IsCreditsEarned
                            ,@PrgVerId
                            ,@IsInGPA
                            ,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

                DECLARE @GradeSystemDetailId UNIQUEIDENTIFIER
                       ,@IsGPA BIT;
                DECLARE GetCreditsSummary_Cursor CURSOR FOR
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,T.TermId
                                       ,T.TermDescrip
                                       ,T.StartDate
                                       ,R.ReqId
                                       ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                       ,NULL AS FinalScore
                                       ,RES.GrdSysDetailId AS GradeSystemDetailId
                                       ,GSD.Grade AS FinalGrade
                                       ,GSD.Grade AS CurrentGrade
                                       ,R.Credits
                                       ,NULL AS ClsSectionId
                                       ,GSD.IsPass
                                       ,GSD.IsCreditsAttempted
                                       ,GSD.IsCreditsEarned
                                       ,GSD.GPA
                                       ,GSD.IsInGPA
                                       ,SE.PrgVerId
                                       ,GSD.IsInGPA
                                       ,R.FinAidCredits AS FinAidCredits
                    FROM       arStuEnrollments SE
                    INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
                    INNER JOIN syCreditSummary CS ON CS.StuEnrollId = RES.StuEnrollId
                    INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                    INNER JOIN arTerm T ON RES.TermId = T.TermId
                    WHERE      RES.ReqId NOT IN (
                                                SELECT DISTINCT ReqId
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = SE.StuEnrollId
                                                       AND TermId = T.TermId
                                                )
                               AND SE.StuEnrollId = @StuEnrollId
                               AND (
                                   RES.CompletedDate IS NOT NULL
                                   AND RES.CompletedDate <= @OffsetDate
                                   );

                SET @varGradeRounding = (
                                        SELECT Value
                                        FROM   syConfigAppSetValues
                                        WHERE  SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@GradeSystemDetailId
                    ,@FinalGrade
                    ,@CurrentGrade
                    ,@CourseCredits
                    ,@ClsSectionId
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@GPA
                    ,@IsGPA
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore, 0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;

                        UPDATE syCreditSummary
                        SET    FinalScore = @FinalScore
                              ,CurrentScore = @CurrentScore
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId
                               AND ReqId = @reqid;

                        --Average calculation

                        -- Term Average
                        SET @TermAverageCount = (
                                                SELECT COUNT(*)
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                       AND TermId = @TermId
                                                       AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                              SELECT SUM(FinalScore)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND TermId = @TermId
                                                     AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount;

                        -- Cumulative Average
                        SET @cumAveragecount = (
                                               SELECT COUNT(*)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                               --AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                             SELECT SUM(FinalScore)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount;

                        UPDATE syCreditSummary
                        SET    Average = @TermAverage
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId;

                        --Update Cumulative GPA
                        UPDATE syCreditSummary
                        SET    CumAverage = @CumAverage
                        WHERE  StuEnrollId = @StuEnrollId;


                        FETCH NEXT FROM GetCreditsSummary_Cursor
                        INTO @StuEnrollId
                            ,@TermId
                            ,@TermDescrip
                            ,@termstartdate1
                            ,@reqid
                            ,@CourseCodeDescrip
                            ,@FinalScore
                            ,@GradeSystemDetailId
                            ,@FinalGrade
                            ,@CurrentGrade
                            ,@CourseCredits
                            ,@ClsSectionId
                            ,@IsPass
                            ,@IsCreditsAttempted
                            ,@IsCreditsEarned
                            ,@GPA
                            ,@IsGPA
                            ,@PrgVerId
                            ,@IsInGPA
                            ,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

                DECLARE GetCreditsSummary_Cursor CURSOR FOR
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,T.TermId
                                       ,T.TermDescrip
                                       ,T.StartDate
                                       ,R.ReqId
                                       ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                       ,NULL AS FinalScore
                                       ,RES.GrdSysDetailId AS GradeSystemDetailId
                                       ,GSD.Grade AS FinalGrade
                                       ,GSD.Grade AS CurrentGrade
                                       ,R.Credits
                                       ,NULL AS ClsSectionId
                                       ,GSD.IsPass
                                       ,GSD.IsCreditsAttempted
                                       ,GSD.IsCreditsEarned
                                       ,GSD.GPA
                                       ,GSD.IsInGPA
                                       ,SE.PrgVerId
                                       ,GSD.IsInGPA
                                       ,R.FinAidCredits AS FinAidCredits
                    FROM       arStuEnrollments SE
                    INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
                    INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                    INNER JOIN arTerm T ON RES.TermId = T.TermId
                    WHERE      SE.StuEnrollId NOT IN (
                                                     SELECT DISTINCT StuEnrollId
                                                     FROM   syCreditSummary
                                                     )
                               AND SE.StuEnrollId = @StuEnrollId
                               AND (
                                   RES.CompletedDate IS NOT NULL
                                   AND RES.CompletedDate <= @OffsetDate
                                   );

                SET @varGradeRounding = (
                                        SELECT Value
                                        FROM   syConfigAppSetValues
                                        WHERE  SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@GradeSystemDetailId
                    ,@FinalGrade
                    ,@CurrentGrade
                    ,@CourseCredits
                    ,@ClsSectionId
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@GPA
                    ,@IsGPA
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore, 0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;

                        UPDATE syCreditSummary
                        SET    FinalScore = @FinalScore
                              ,CurrentScore = @CurrentScore
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId
                               AND ReqId = @reqid;

                        --Average calculation
                        SET @TermAverageCount = (
                                                SELECT COUNT(*)
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                       AND TermId = @TermId
                                                       AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                              SELECT SUM(FinalScore)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND TermId = @TermId
                                                     AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount;

                        -- Cumulative Average
                        SET @cumAveragecount = (
                                               SELECT COUNT(*)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                             SELECT SUM(FinalScore)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount;

                        UPDATE syCreditSummary
                        SET    Average = @TermAverage
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId;

                        --Update Cumulative GPA
                        UPDATE syCreditSummary
                        SET    CumAverage = @CumAverage
                        WHERE  StuEnrollId = @StuEnrollId;


                        FETCH NEXT FROM GetCreditsSummary_Cursor
                        INTO @StuEnrollId
                            ,@TermId
                            ,@TermDescrip
                            ,@termstartdate1
                            ,@reqid
                            ,@CourseCodeDescrip
                            ,@FinalScore
                            ,@GradeSystemDetailId
                            ,@FinalGrade
                            ,@CurrentGrade
                            ,@CourseCredits
                            ,@ClsSectionId
                            ,@IsPass
                            ,@IsCreditsAttempted
                            ,@IsCreditsEarned
                            ,@GPA
                            ,@IsGPA
                            ,@PrgVerId
                            ,@IsInGPA
                            ,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

            ---- PRINT ''@CurrentBal_Compute'', 
            ---- PRINT @CurrentBal_Compute


            END;

        SET @TermAverageCount = (
                                SELECT COUNT(*)
                                FROM   syCreditSummary
                                WHERE  StuEnrollId = @StuEnrollId
                                       AND FinalScore IS NOT NULL
                                );
        SET @termAverageSum = (
                              SELECT SUM(FinalScore)
                              FROM   syCreditSummary
                              WHERE  StuEnrollId = @StuEnrollId
                                     AND FinalScore IS NOT NULL
                              );
        IF @TermAverageCount >= 1
            BEGIN
                SET @TermAverage = @termAverageSum / @TermAverageCount;
            END;

        -- Cumulative Average
        SET @cumAveragecount = (
                               SELECT COUNT(*)
                               FROM   (
                                      SELECT     sCS.StuEnrollId
                                                ,sCS.TermId
                                                ,sCS.TermDescrip
                                                ,sCS.ReqId
                                                ,sCS.ReqDescrip
                                                ,sCS.ClsSectionId
                                                ,sCS.CreditsEarned
                                                ,sCS.CreditsAttempted
                                                ,sCS.CurrentScore
                                                ,sCS.CurrentGrade
                                                ,sCS.FinalScore
                                                ,sCS.FinalGrade
                                                ,sCS.Completed
                                                ,sCS.FinalGPA
                                                ,sCS.Product_WeightedAverage_Credits_GPA
                                                ,sCS.Count_WeightedAverage_Credits
                                                ,sCS.Product_SimpleAverage_Credits_GPA
                                                ,sCS.Count_SimpleAverage_Credits
                                                ,sCS.ModUser
                                                ,sCS.ModDate
                                                ,sCS.TermGPA_Simple
                                                ,sCS.TermGPA_Weighted
                                                ,sCS.coursecredits
                                                ,sCS.CumulativeGPA
                                                ,sCS.CumulativeGPA_Simple
                                                ,sCS.FACreditsEarned
                                                ,sCS.Average
                                                ,sCS.CumAverage
                                                ,sCS.TermStartDate
                                      FROM       dbo.syCreditSummary sCS
                                      INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                      INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                      WHERE      sCS.StuEnrollId = @StuEnrollId
                                                 AND CS.ReqId = sCS.ReqId
                                                 AND (
                                                     R.DateCompleted IS NOT NULL
                                                     AND R.DateCompleted <= @OffsetDate
                                                     )
                                      UNION ALL
                                      (SELECT     sCS.StuEnrollId
                                                 ,sCS.TermId
                                                 ,sCS.TermDescrip
                                                 ,sCS.ReqId
                                                 ,sCS.ReqDescrip
                                                 ,sCS.ClsSectionId
                                                 ,sCS.CreditsEarned
                                                 ,sCS.CreditsAttempted
                                                 ,sCS.CurrentScore
                                                 ,sCS.CurrentGrade
                                                 ,sCS.FinalScore
                                                 ,sCS.FinalGrade
                                                 ,sCS.Completed
                                                 ,sCS.FinalGPA
                                                 ,sCS.Product_WeightedAverage_Credits_GPA
                                                 ,sCS.Count_WeightedAverage_Credits
                                                 ,sCS.Product_SimpleAverage_Credits_GPA
                                                 ,sCS.Count_SimpleAverage_Credits
                                                 ,sCS.ModUser
                                                 ,sCS.ModDate
                                                 ,sCS.TermGPA_Simple
                                                 ,sCS.TermGPA_Weighted
                                                 ,sCS.coursecredits
                                                 ,sCS.CumulativeGPA
                                                 ,sCS.CumulativeGPA_Simple
                                                 ,sCS.FACreditsEarned
                                                 ,sCS.Average
                                                 ,sCS.CumAverage
                                                 ,sCS.TermStartDate
                                       FROM       dbo.syCreditSummary sCS
                                       INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                       WHERE      sCS.StuEnrollId = @StuEnrollId
                                                  AND tG.ReqId = sCS.ReqId
                                                  AND (
                                                      tG.CompletedDate IS NOT NULL
                                                      AND tG.CompletedDate <= @OffsetDate
                                                      ))
                                      ) sCSEA
                               WHERE  StuEnrollId = @StuEnrollId
                                      AND FinalScore IS NOT NULL
                               );
        SET @cumAverageSum = (
                             SELECT SUM(FinalScore)
                             FROM   (
                                    SELECT     sCS.StuEnrollId
                                              ,sCS.TermId
                                              ,sCS.TermDescrip
                                              ,sCS.ReqId
                                              ,sCS.ReqDescrip
                                              ,sCS.ClsSectionId
                                              ,sCS.CreditsEarned
                                              ,sCS.CreditsAttempted
                                              ,sCS.CurrentScore
                                              ,sCS.CurrentGrade
                                              ,sCS.FinalScore
                                              ,sCS.FinalGrade
                                              ,sCS.Completed
                                              ,sCS.FinalGPA
                                              ,sCS.Product_WeightedAverage_Credits_GPA
                                              ,sCS.Count_WeightedAverage_Credits
                                              ,sCS.Product_SimpleAverage_Credits_GPA
                                              ,sCS.Count_SimpleAverage_Credits
                                              ,sCS.ModUser
                                              ,sCS.ModDate
                                              ,sCS.TermGPA_Simple
                                              ,sCS.TermGPA_Weighted
                                              ,sCS.coursecredits
                                              ,sCS.CumulativeGPA
                                              ,sCS.CumulativeGPA_Simple
                                              ,sCS.FACreditsEarned
                                              ,sCS.Average
                                              ,sCS.CumAverage
                                              ,sCS.TermStartDate
                                    FROM       dbo.syCreditSummary sCS
                                    INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                    INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                    WHERE      sCS.StuEnrollId = @StuEnrollId
                                               AND CS.ReqId = sCS.ReqId
                                               AND (
                                                   R.DateCompleted IS NOT NULL
                                                   AND R.DateCompleted <= @OffsetDate
                                                   )
                                    UNION ALL
                                    (SELECT     sCS.StuEnrollId
                                               ,sCS.TermId
                                               ,sCS.TermDescrip
                                               ,sCS.ReqId
                                               ,sCS.ReqDescrip
                                               ,sCS.ClsSectionId
                                               ,sCS.CreditsEarned
                                               ,sCS.CreditsAttempted
                                               ,sCS.CurrentScore
                                               ,sCS.CurrentGrade
                                               ,sCS.FinalScore
                                               ,sCS.FinalGrade
                                               ,sCS.Completed
                                               ,sCS.FinalGPA
                                               ,sCS.Product_WeightedAverage_Credits_GPA
                                               ,sCS.Count_WeightedAverage_Credits
                                               ,sCS.Product_SimpleAverage_Credits_GPA
                                               ,sCS.Count_SimpleAverage_Credits
                                               ,sCS.ModUser
                                               ,sCS.ModDate
                                               ,sCS.TermGPA_Simple
                                               ,sCS.TermGPA_Weighted
                                               ,sCS.coursecredits
                                               ,sCS.CumulativeGPA
                                               ,sCS.CumulativeGPA_Simple
                                               ,sCS.FACreditsEarned
                                               ,sCS.Average
                                               ,sCS.CumAverage
                                               ,sCS.TermStartDate
                                     FROM       dbo.syCreditSummary sCS
                                     INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                     WHERE      sCS.StuEnrollId = @StuEnrollId
                                                AND tG.ReqId = sCS.ReqId
                                                AND (
                                                    tG.CompletedDate IS NOT NULL
                                                    AND tG.CompletedDate <= @OffsetDate
                                                    ))
                                    ) sCSEA
                             WHERE  StuEnrollId = @StuEnrollId
                                    AND FinalScore IS NOT NULL
                             );

        IF @cumAveragecount >= 1
            BEGIN
                SET @CumAverage = @cumAverageSum / @cumAveragecount;
            END;

        IF @GradesFormat <> ''letter''
            BEGIN
                SET @Qualitative = @CumAverage;
            END;
        ELSE
            SET @Qualitative = @cumWeightedGPA;

        IF ( @QuantMinUnitTypeId = 3 )
            BEGIN
                IF @UnitTypeDescrip IN ( ''none'', ''present absent'' )
                   AND @TrackSapAttendance = ''byclass''
                    BEGIN
                        SET @Quantitative = (
                                            SELECT CAST(( sas.Present / sas.Scheduled ) * 100 AS DECIMAL)
                                            FROM   (
                                                   SELECT   MAX(convertedSAS.ConvertedActualRunningScheduledDays) AS Scheduled
                                                           ,MAX(convertedSAS.ConvertedActualRunningPresentDays)
                                                            + MAX(convertedSAS.ConvertedActualRunningMakeupDays) AS Present -- add makeup to present since procedure splits it before added into syattendance summary
                                                   FROM     (
                                                            SELECT    sySas.StuEnrollId
                                                                     ,sySas.StudentAttendedDate
                                                                     ,sySas.ClsSectionId
                                                                     ,sySas.ActualRunningScheduledDays * ISNULL(cim.IntervalInMinutes, 1) AS ConvertedActualRunningScheduledDays
                                                                     ,sySas.ActualRunningPresentDays * ISNULL(cim.IntervalInMinutes, 1) AS ConvertedActualRunningPresentDays
                                                                     ,sySas.ActualRunningMakeupDays * ISNULL(cim.IntervalInMinutes, 1) AS ConvertedActualRunningMakeupDays
                                                            FROM      @attendanceSummary sySas
                                                            LEFT JOIN @ClsSectionIntervalMinutes cim ON cim.ClsSectionId = sySas.ClsSectionId
                                                            ) AS convertedSAS
                                                   WHERE    convertedSAS.StuEnrollId = @StuEnrollId
                                                            AND convertedSAS.StudentAttendedDate <= @OffsetDate
                                                   GROUP BY StuEnrollId
                                                   ) AS sas
                                            );



                    END;
                ELSE
                    BEGIN
                        SET @Quantitative = (
                                            SELECT CAST(( sas.Present / sas.Scheduled ) * 100 AS DECIMAL)
                                            FROM   (
                                                   SELECT   MAX(ActualRunningScheduledDays) AS Scheduled
                                                           ,MAX(ActualRunningPresentDays) + MAX(ActualRunningMakeupDays) AS Present -- add makeup to present since procedure splits it before added into syattendance summary
                                                   FROM     @attendanceSummary
                                                   WHERE    StuEnrollId = @StuEnrollId
                                                            AND StudentAttendedDate <= @OffsetDate
                                                   GROUP BY StuEnrollId
                                                   ) sas
                                            );
                    END;

            END;
        ELSE
            BEGIN
                SET @Quantitative = (
                                    SELECT CAST(( sCSEA.Earned / sCSEA.Attempted ) * 100 AS DECIMAL)
                                    FROM   (
                                           SELECT     SUM(sCS.CreditsEarned) Earned
                                                     ,SUM(sCS.CreditsAttempted) Attempted
                                           FROM       dbo.syCreditSummary sCS
                                           INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                           INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                           WHERE      sCS.StuEnrollId = @StuEnrollId
                                                      AND CS.ReqId = sCS.ReqId
                                                      AND (
                                                          R.DateCompleted IS NOT NULL
                                                          AND R.DateCompleted <= @OffsetDate
                                                          )
                                           GROUP BY   sCS.StuEnrollId
                                           UNION ALL
                                           (SELECT     SUM(sCS.CreditsEarned)
                                                      ,SUM(sCS.CreditsAttempted)
                                            FROM       dbo.syCreditSummary sCS
                                            INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                            WHERE      sCS.StuEnrollId = @StuEnrollId
                                                       AND tG.ReqId = sCS.ReqId
                                                       AND (
                                                           tG.CompletedDate IS NOT NULL
                                                           AND tG.CompletedDate <= @OffsetDate
                                                           )
                                            GROUP BY   sCS.StuEnrollId)
                                           ) sCSEA
                                    );
            END;

        --If Clock Hour / Numeric - use New Average Calculation
        IF (
           SELECT TOP 1 P.ACId
           FROM   dbo.arStuEnrollments E
           JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
           JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
           WHERE  E.StuEnrollId = @StuEnrollId
           ) = 5
           AND @GradesFormat = ''numeric''
            BEGIN
                EXEC dbo.USP_GPACalculator @EnrollmentId = @StuEnrollId
                                          ,@EndDate = @OffsetDate
                                          ,@StudentGPA = @CumAverage OUTPUT;
            END;

    --SELECT * FROM dbo.syCreditSummary WHERE StuEnrollId  = ''F3616299-DCF2-4F59-BC8F-2F3034ED01A3''

    ---- PRINT @cumAveragecount 
    ---- PRINT @CumAverageSum
    ---- PRINT @CumAverage
    ---- PRINT @UnitTypeId;
    ---- PRINT @TrackSapAttendance;
    ---- PRINT @displayHours;
    END;

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId]'
GO
IF OBJECT_ID(N'[dbo].[Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId]', 'P') IS NOT NULL
EXEC sp_executesql N'--=================================================================================================
-- Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId
--=================================================================================================
ALTER PROCEDURE [dbo].[Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId]
    @StuEnrollId VARCHAR(MAX) = NULL
   ,@CumAverage DECIMAL(18,2) OUTPUT
   ,@cumWeightedGPA DECIMAL(18,2) OUTPUT
   ,@cumSimpleGPA DECIMAL(18,2) OUTPUT
AS
    BEGIN
        DECLARE @TrackSapAttendance VARCHAR(1000);  
        DECLARE @displayHours AS VARCHAR(1000);   
        DECLARE @GradeCourseRepetitionsMethod VARCHAR(1000);
        DECLARE @GradesFormat AS VARCHAR(1000);
        DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
		
        SET @StuEnrollCampusId = COALESCE((
                                            SELECT  ASE.CampusId
                                            FROM    arStuEnrollments AS ASE
                                            WHERE   ASE.StuEnrollId = @StuEnrollId
                                          ),NULL);
        SET @TrackSapAttendance = dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'',@StuEnrollCampusId);
        IF ( @TrackSapAttendance IS NOT NULL )
            BEGIN
                SET @TrackSapAttendance = LOWER(LTRIM(RTRIM(@TrackSapAttendance)));
            END;

        SET @GradeCourseRepetitionsMethod = dbo.GetAppSettingValueByKeyName(''GradeCourseRepetitionsMethod'',@StuEnrollCampusId); 
        SET @GradesFormat = dbo.GetAppSettingValueByKeyName(''GradesFormat'',@StuEnrollCampusId);
        IF ( @GradesFormat IS NOT NULL )
            BEGIN
                SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
            END;



        DECLARE @CoursesNotRepeated TABLE
            (
             StuEnrollId UNIQUEIDENTIFIER
            ,TermId UNIQUEIDENTIFIER
            ,TermDescrip VARCHAR(100)
            ,ReqId UNIQUEIDENTIFIER
            ,ReqDescrip VARCHAR(100)
            ,ClsSectionId VARCHAR(50)
            ,CreditsEarned DECIMAL(18,2)
            ,CreditsAttempted DECIMAL(18,2)
            ,CurrentScore DECIMAL(18,2)
            ,CurrentGrade VARCHAR(10)
            ,FinalScore DECIMAL(18,2)
            ,FinalGrade VARCHAR(10)
            ,Completed BIT
            ,FinalGPA DECIMAL(18,2)
            ,Product_WeightedAverage_Credits_GPA DECIMAL(18,2)
            ,Count_WeightedAverage_Credits DECIMAL(18,2)
            ,Product_SimpleAverage_Credits_GPA DECIMAL(18,2)
            ,Count_SimpleAverage_Credits DECIMAL(18,2)
            ,ModUser VARCHAR(50)
            ,ModDate DATETIME
            ,TermGPA_Simple DECIMAL(18,2)
            ,TermGPA_Weighted DECIMAL(18,2)
            ,coursecredits DECIMAL(18,2)
            ,CumulativeGPA DECIMAL(18,2)
            ,CumulativeGPA_Simple DECIMAL(18,2)
            ,FACreditsEarned DECIMAL(18,2)
            ,Average DECIMAL(18,2)
            ,CumAverage DECIMAL(18,2)
            ,TermStartDate DATETIME
            ,rownumber INT
            );

        INSERT  INTO @CoursesNotRepeated
                SELECT  StuEnrollId
                       ,TermId
                       ,TermDescrip
                       ,ReqId
                       ,ReqDescrip
                       ,ClsSectionId
                       ,CreditsEarned
                       ,CreditsAttempted
                       ,CurrentScore
                       ,CurrentGrade
                       ,FinalScore
                       ,FinalGrade
                       ,Completed
                       ,FinalGPA
                       ,Product_WeightedAverage_Credits_GPA
                       ,Count_WeightedAverage_Credits
                       ,Product_SimpleAverage_Credits_GPA
                       ,Count_SimpleAverage_Credits
                       ,ModUser
                       ,ModDate
                       ,TermGPA_Simple
                       ,TermGPA_Weighted
                       ,coursecredits
                       ,CumulativeGPA
                       ,CumulativeGPA_Simple
                       ,FACreditsEarned
                       ,Average
                       ,CumAverage
                       ,TermStartDate
                       ,NULL AS rownumber
                FROM    syCreditSummary
                WHERE   StuEnrollId = @StuEnrollId
                        AND ReqId IN ( SELECT   ReqId
                                       FROM     (
                                                  SELECT    ReqId
                                                           ,ReqDescrip
                                                           ,COUNT(*) AS counter
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = @StuEnrollId
                                                  GROUP BY  ReqId
                                                           ,ReqDescrip
                                                  HAVING    COUNT(*) = 1
                                                ) dt );

	

        IF LOWER(@GradeCourseRepetitionsMethod) = ''latest''
            BEGIN
                INSERT  INTO @CoursesNotRepeated
                        SELECT  dt2.StuEnrollId
                               ,dt2.TermId
                               ,dt2.TermDescrip
                               ,dt2.ReqId
                               ,dt2.ReqDescrip
                               ,dt2.ClsSectionId
                               ,dt2.CreditsEarned
                               ,dt2.CreditsAttempted
                               ,dt2.CurrentScore
                               ,dt2.CurrentGrade
                               ,dt2.FinalScore
                               ,dt2.FinalGrade
                               ,dt2.Completed
                               ,dt2.FinalGPA
                               ,dt2.Product_WeightedAverage_Credits_GPA
                               ,dt2.Count_WeightedAverage_Credits
                               ,dt2.Product_SimpleAverage_Credits_GPA
                               ,dt2.Count_SimpleAverage_Credits
                               ,dt2.ModUser
                               ,dt2.ModDate
                               ,dt2.TermGPA_Simple
                               ,dt2.TermGPA_Weighted
                               ,dt2.coursecredits
                               ,dt2.CumulativeGPA
                               ,dt2.CumulativeGPA_Simple
                               ,dt2.FACreditsEarned
                               ,dt2.Average
                               ,dt2.CumAverage
                               ,dt2.TermStartDate
                               ,dt2.RowNumber
                        FROM    (
                                  SELECT    StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY TermStartDate DESC ) AS RowNumber
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND (
                                                  FinalScore IS NOT NULL
                                                  OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN ( SELECT   ReqId
                                                           FROM     (
                                                                      SELECT    ReqId
                                                                               ,ReqDescrip
                                                                               ,COUNT(*) AS Counter
                                                                      FROM      syCreditSummary
                                                                      WHERE     StuEnrollId = @StuEnrollId
                                                                      GROUP BY  ReqId
                                                                               ,ReqDescrip
                                                                      HAVING    COUNT(*) > 1
                                                                    ) dt )
                                ) dt2
                        WHERE   RowNumber = 1
                        ORDER BY TermStartDate DESC;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''best''
            BEGIN
                INSERT  INTO @CoursesNotRepeated
                        SELECT  dt2.StuEnrollId
                               ,dt2.TermId
                               ,dt2.TermDescrip
                               ,dt2.ReqId
                               ,dt2.ReqDescrip
                               ,dt2.ClsSectionId
                               ,dt2.CreditsEarned
                               ,dt2.CreditsAttempted
                               ,dt2.CurrentScore
                               ,dt2.CurrentGrade
                               ,dt2.FinalScore
                               ,dt2.FinalGrade
                               ,dt2.Completed
                               ,dt2.FinalGPA
                               ,dt2.Product_WeightedAverage_Credits_GPA
                               ,dt2.Count_WeightedAverage_Credits
                               ,dt2.Product_SimpleAverage_Credits_GPA
                               ,dt2.Count_SimpleAverage_Credits
                               ,dt2.ModUser
                               ,dt2.ModDate
                               ,dt2.TermGPA_Simple
                               ,dt2.TermGPA_Weighted
                               ,dt2.coursecredits
                               ,dt2.CumulativeGPA
                               ,dt2.CumulativeGPA_Simple
                               ,dt2.FACreditsEarned
                               ,dt2.Average
                               ,dt2.CumAverage
                               ,dt2.TermStartDate
                               ,dt2.RowNumber
                        FROM    (
                                  SELECT    StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY Completed DESC, CreditsEarned DESC, FinalGPA DESC ) AS RowNumber
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND (
                                                  FinalScore IS NOT NULL
                                                  OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN ( SELECT   ReqId
                                                           FROM     (
                                                                      SELECT    ReqId
                                                                               ,ReqDescrip
                                                                               ,COUNT(*) AS Counter
                                                                      FROM      syCreditSummary
                                                                      WHERE     StuEnrollId = @StuEnrollId
                                                                      GROUP BY  ReqId
                                                                               ,ReqDescrip
                                                                      HAVING    COUNT(*) > 1
                                                                    ) dt )
                                ) dt2
                        WHERE   RowNumber = 1; 
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''average''
            BEGIN
                INSERT  INTO @CoursesNotRepeated
                        SELECT  dt2.StuEnrollId
                               ,dt2.TermId
                               ,dt2.TermDescrip
                               ,dt2.ReqId
                               ,dt2.ReqDescrip
                               ,dt2.ClsSectionId
                               ,dt2.CreditsEarned
                               ,dt2.CreditsAttempted
                               ,dt2.CurrentScore
                               ,dt2.CurrentGrade
                               ,dt2.FinalScore
                               ,dt2.FinalGrade
                               ,dt2.Completed
                               ,dt2.FinalGPA
                               ,dt2.Product_WeightedAverage_Credits_GPA
                               ,dt2.Count_WeightedAverage_Credits
                               ,dt2.Product_SimpleAverage_Credits_GPA
                               ,dt2.Count_SimpleAverage_Credits
                               ,dt2.ModUser
                               ,dt2.ModDate
                               ,dt2.TermGPA_Simple
                               ,dt2.TermGPA_Weighted
                               ,dt2.coursecredits
                               ,dt2.CumulativeGPA
                               ,dt2.CumulativeGPA_Simple
                               ,dt2.FACreditsEarned
                               ,dt2.Average
                               ,dt2.CumAverage
                               ,dt2.TermStartDate
                               ,dt2.RowNumber
                        FROM    (
                                  SELECT    StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY FinalGPA DESC ) AS RowNumber
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND (
                                                  FinalScore IS NOT NULL
                                                  OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN ( SELECT   ReqId
                                                           FROM     (
                                                                      SELECT    ReqId
                                                                               ,ReqDescrip
                                                                               ,COUNT(*) AS Counter
                                                                      FROM      syCreditSummary
                                                                      WHERE     StuEnrollId = @StuEnrollId
                                                                      GROUP BY  ReqId
                                                                               ,ReqDescrip
                                                                      HAVING    COUNT(*) > 1
                                                                    ) dt )
                                ) dt2; 
            END;

   
        DECLARE @cumSimpleCourseCredits DECIMAL(18,2);
        DECLARE @cumSimple_GPA_Credits DECIMAL(18,2);
	 -- (OUTPUT parameter)  DECLARE @cumSimpleGPA DECIMAL(18, 2)
        
	

        DECLARE @cumCourseCredits DECIMAL(18,2)
           ,@cumWeighted_GPA_Credits DECIMAL(18,2);
        DECLARE @cumCourseCredits_repeated DECIMAL(18,2)
           ,@cumWeighted_GPA_Credits_repeated DECIMAL(18,2);

        PRINT @GradeCourseRepetitionsMethod;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''average''
            BEGIN
                SET @cumWeightedGPA = 0;
                SET @cumSimpleGPA = 0;
					
                SET @cumSimpleCourseCredits = (
                                                SELECT  COUNT(*)
                                                FROM    @CoursesNotRepeated
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND FinalGPA IS NOT NULL
                                                        AND (
                                                              rownumber = 0
                                                              OR rownumber IS NULL
                                                            )
                                              );

                DECLARE @cumSimpleCourseCredits_repeated DECIMAL(18,2);
                SET @cumSimpleCourseCredits_repeated = (
                                                         SELECT SUM(CourseCreditCount)
                                                         FROM   (
                                                                  SELECT    ReqId
                                                                           ,COUNT(coursecredits) AS CourseCreditCount
                                                                  FROM      @CoursesNotRepeated
                                                                  WHERE     StuEnrollId IN ( @StuEnrollId )
                                                                            AND FinalGPA IS NOT NULL
                                                                            AND rownumber = 1
                                                                  GROUP BY  ReqId
                                                                ) dt
                                                       );
                SET @cumSimpleCourseCredits = @cumSimpleCourseCredits + ISNULL(@cumSimpleCourseCredits_repeated,0);
		
                DECLARE @cumSimple_GPA_Credits_repeated DECIMAL(18,2);

                SET @cumSimple_GPA_Credits = (
                                               SELECT   SUM(FinalGPA)
                                               FROM     @CoursesNotRepeated
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND FinalGPA IS NOT NULL
                                                        AND (
                                                              rownumber = 0
                                                              OR rownumber IS NULL
                                                            )
                                             );

                SET @cumSimple_GPA_Credits_repeated = (
                                                        SELECT  SUM(AverageGPA)
                                                        FROM    (
                                                                  SELECT    ReqId
                                                                           ,SUM(FinalGPA) / MAX(rownumber) AS AverageGPA
                                                                  FROM      @CoursesNotRepeated
                                                                  WHERE     StuEnrollId IN ( @StuEnrollId )
                                                                            AND FinalGPA IS NOT NULL
                                                                            AND rownumber >= 1
                                                                  GROUP BY  ReqId
                                                                ) dt
                                                      );

                SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits + ISNULL(@cumSimple_GPA_Credits_repeated,0.00);

                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END; 

                SET @cumCourseCredits = (
                                          SELECT    SUM(coursecredits)
                                          FROM      @CoursesNotRepeated
                                          WHERE     StuEnrollId IN ( @StuEnrollId )
                                                    AND FinalGPA IS NOT NULL
                                                    AND (
                                                          rownumber = 0
                                                          OR rownumber IS NULL
                                                        )
                                        );

                SET @cumCourseCredits_repeated = (
                                                   SELECT   SUM(CourseCreditCount)
                                                   FROM     (
                                                              SELECT    ReqId
                                                                       ,MAX(coursecredits) AS CourseCreditCount
                                                              FROM      @CoursesNotRepeated
                                                              WHERE     StuEnrollId IN ( @StuEnrollId )
                                                                        AND FinalGPA IS NOT NULL
                                                                        AND rownumber >= 1
                                                              GROUP BY  ReqId
                                                            ) dt
                                                 );
                SET @cumCourseCredits = @cumCourseCredits + ISNULL(@cumCourseCredits_repeated,0.00);

                PRINT @cumCourseCredits;

                SET @cumWeighted_GPA_Credits = (
                                                 SELECT SUM(coursecredits * FinalGPA)
                                                 FROM   @CoursesNotRepeated
                                                 WHERE  StuEnrollId IN ( @StuEnrollId )
                                                        AND FinalGPA IS NOT NULL
                                                        AND (
                                                              rownumber = 0
                                                              OR rownumber IS NULL
                                                            )
                                               );
                SET @cumWeighted_GPA_Credits_repeated = (
                                                          SELECT    SUM(CourseCreditCount)
                                                          FROM      (
                                                                      SELECT    ReqId
                                                                               ,SUM(coursecredits * FinalGPA) / MAX(rownumber) AS CourseCreditCount
                                                                      FROM      @CoursesNotRepeated
                                                                      WHERE     StuEnrollId IN ( @StuEnrollId )
                                                                                AND FinalGPA IS NOT NULL
                                                                                AND rownumber >= 1
                                                                      GROUP BY  ReqId
                                                                    ) dt
                                                        );
			
				
			
                SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits + ISNULL(@cumWeighted_GPA_Credits_repeated,0);
                PRINT @cumWeighted_GPA_Credits;

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END; 
            END;
        ELSE
            BEGIN 
                SET @cumSimpleGPA = 0;

                SET @cumSimpleCourseCredits = (
                                                SELECT  COUNT(*)
                                                FROM    @CoursesNotRepeated
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND FinalGPA IS NOT NULL
                                              );
                SET @cumSimple_GPA_Credits = (
                                               SELECT   SUM(FinalGPA)
                                               FROM     @CoursesNotRepeated
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND FinalGPA IS NOT NULL
                                             );		
                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END; 
                SET @cumWeightedGPA = 0;
                SET @cumCourseCredits = (
                                          SELECT    SUM(coursecredits)
                                          FROM      @CoursesNotRepeated
                                          WHERE     StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                        );
                SET @cumWeighted_GPA_Credits = (
                                                 SELECT SUM(coursecredits * FinalGPA)
                                                 FROM   @CoursesNotRepeated
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND FinalGPA IS NOT NULL
                                               );

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END; 

            END;




					
        IF @cumCourseCredits >= 1
            BEGIN
                SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
            END; 


        DECLARE @TermAverageCount INT;
        DECLARE @TermAverage DECIMAL(18,2); 
		-- (output parameter)  DECLARE @CumAverage DECIMAL(18, 2)
        IF @GradesFormat <> ''letter''
            BEGIN

                DECLARE @termAverageSum DECIMAL(18,2)
                   ,@cumAverageSum DECIMAL(18,2)
                   ,@cumAveragecount INT;
                DECLARE @TardiesMakingAbsence INT
                   ,@UnitTypeDescrip VARCHAR(20)
                   ,@OriginalTardiesMakingAbsence INT;
                SET @TardiesMakingAbsence = (
                                              SELECT TOP 1
                                                        t1.TardiesMakingAbsence
                                              FROM      arPrgVersions t1
                                                       ,arStuEnrollments t2
                                              WHERE     t1.PrgVerId = t2.PrgVerId
                                                        AND t2.StuEnrollId = @StuEnrollId
                                            );
							
                SET @UnitTypeDescrip = (
                                         SELECT LTRIM(RTRIM(AAUT.UnitTypeDescrip))
                                         FROM   arAttUnitType AS AAUT
                                         INNER JOIN arPrgVersions AS APV ON APV.UnitTypeId = AAUT.UnitTypeId
                                         INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                                         WHERE  ASE.StuEnrollId = @StuEnrollId
                                       );

                SET @OriginalTardiesMakingAbsence = (
                                                      SELECT TOP 1
                                                                tardiesmakingabsence
                                                      FROM      syStudentAttendanceSummary
                                                      WHERE     StuEnrollId = @StuEnrollId
                                                    );
                DECLARE @termstartdate1 DATETIME;

-- Declare Variables
                BEGIN -- Declare Variables
                    DECLARE @MeetDate DATETIME
                       ,@WeekDay VARCHAR(15)
                       ,@StartDate DATETIME
                       ,@EndDate DATETIME;
                    DECLARE @PeriodDescrip VARCHAR(50)
                       ,@Actual DECIMAL(18,2)
                       ,@Excused DECIMAL(18,2)
                       ,@ClsSectionId UNIQUEIDENTIFIER;
                    DECLARE @Absent DECIMAL(18,2)
                       ,@SchedHours DECIMAL(18,2)
                       ,@TardyMinutes DECIMAL(18,2);
                    DECLARE @tardy DECIMAL(18,2)
                       ,@tracktardies INT
                       ,@rownumber INT
                       ,@IsTardy BIT
                       ,@ActualRunningScheduledHours DECIMAL(18,2);
                    DECLARE @ActualRunningPresentHours DECIMAL(18,2)
                       ,@ActualRunningAbsentHours DECIMAL(18,2)
                       ,@ActualRunningTardyHours DECIMAL(18,2)
                       ,@ActualRunningMakeupHours DECIMAL(18,2);
                    DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
                       ,@intTardyBreakPoint INT
                       ,@AdjustedRunningPresentHours DECIMAL(18,2)
                       ,@AdjustedRunningAbsentHours DECIMAL(18,2)
                       ,@ActualRunningScheduledDays DECIMAL(18,2);
		--declare @Scheduledhours decimal(18,2),@ActualPresentDays_ConvertTo_Hours decimal(18,2),@ActualAbsentDays_ConvertTo_Hours decimal(18,2),@AttendanceTrack varchar(50)
                    DECLARE @TermId VARCHAR(50)
                       ,@TermDescrip VARCHAR(50)
                       ,@PrgVerId UNIQUEIDENTIFIER;
                    DECLARE @TardyHit VARCHAR(10)
                       ,@ScheduledMinutes DECIMAL(18,2);
                    DECLARE @Scheduledhours_noperiods DECIMAL(18,2)
                       ,@ActualPresentDays_ConvertTo_Hours_NoPeriods DECIMAL(18,2)
                       ,@ActualAbsentDays_ConvertTo_Hours_NoPeriods DECIMAL(18,2);
                    DECLARE @ActualTardyDays_ConvertTo_Hours_NoPeriods DECIMAL(18,2);
                    DECLARE @boolReset BIT
                       ,@MakeupHours DECIMAL(18,2)
                       ,@AdjustedPresentDaysComputed DECIMAL(18,2);		
                END;

-- Step 3  --  InsertAttendance_Class_PresentAbsent   -- UnitTypeDescrip = (''None'', ''Present Absent'') 
--         --  TrackSapAttendance = ''byclass''
                BEGIN  -- Step 3  --  InsertAttendance_Class_PresentAbsent   -- UnitTypeDescrip = (''None'', ''Present Absent'') 
--         --  TrackSapAttendance = ''byclass''
                    IF @UnitTypeDescrip IN ( ''none'',''present absent'' )
                        AND @TrackSapAttendance = ''byclass''
                        BEGIN
			---- PRINT ''Step1!!!!''
                            DELETE  FROM syStudentAttendanceSummary
                            WHERE   StuEnrollId = @StuEnrollId;

                            DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD
                            FOR
                                SELECT  *
                                       ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                FROM    (
                                          SELECT DISTINCT
                                                    t1.StuEnrollId
                                                   ,t1.ClsSectionId
                                                   ,t1.MeetDate
                                                   ,DATENAME(dw,t1.MeetDate) AS WeekDay
                                                   ,t4.StartDate
                                                   ,t4.EndDate
                                                   ,t5.PeriodDescrip
                                                   ,t1.Actual
                                                   ,t1.Excused
                                                   ,CASE WHEN (
                                                                t1.Actual = 0
                                                                AND t1.Excused = 0
                                                              ) THEN t1.Scheduled
                                                         ELSE CASE WHEN (
                                                                          t1.Actual <> 9999.00
                                                                          AND t1.Actual < t1.Scheduled
														  --AND t1.Excused <> 1
                                                                        ) THEN ( t1.Scheduled - t1.Actual )
                                                                   ELSE 0
                                                              END
                                                    END AS Absent
                                                   ,t1.Scheduled AS ScheduledMinutes
                                                   ,CASE WHEN (
                                                                t1.Actual > 0
                                                                AND t1.Actual < t1.Scheduled
                                                              ) THEN ( t1.Scheduled - t1.Actual )
                                                         ELSE 0
                                                    END AS TardyMinutes
                                                   ,t1.Tardy AS Tardy
                                                   ,t3.TrackTardies
                                                   ,t3.TardiesMakingAbsence
                                                   ,t3.PrgVerId
                                          FROM      atClsSectAttendance t1
                                          INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                          INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                          INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                          INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                             AND (
                                                                                   CONVERT(DATE,t1.MeetDate,111) >= CONVERT(DATE,t4.StartDate,111)
                                                                                   AND CONVERT(DATE,t1.MeetDate,111) <= CONVERT(DATE,t4.EndDate,111)
                                                                                 )
                                                                             AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                          INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                          INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                          INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                          INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                          INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                          INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                          WHERE     t2.StuEnrollId = @StuEnrollId
                                                    AND (
                                                          AAUT1.UnitTypeDescrip IN ( ''None'',''Present Absent'' )
                                                          OR AAUT2.UnitTypeDescrip IN ( ''None'',''Present Absent'' )
                                                        )
                                                    AND t1.Actual <> 9999
                                        ) dt
                                ORDER BY StuEnrollId
                                       ,MeetDate;
                            OPEN GetAttendance_Cursor;
                            FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,
                                @Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @boolReset = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                            SET @boolReset = 1;
                                        END;

					  
					  -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                                    IF @Actual <> 9999.00
                                        BEGIN
					-- Commented by Balaji on 2.6.2015 as Excused Absence is still considered an absence
					-- @Excused is not added to Present Hours
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;  -- + @Excused
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; -- + @Excused
					
					-- If there are make up hrs deduct that otherwise it will be added again in progress report
                                            IF (
                                                 @Actual > 0
                                                 AND @Actual > @ScheduledMinutes
                                                 AND @Actual <> 9999.00
                                               )
                                                BEGIN
                                                    SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                END;
					  
                                            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;                      
                                        END;
				   
					-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                                    SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
			  
					-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                                    IF (
                                         @Actual > 0
                                         AND @Actual < @ScheduledMinutes
                                         AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                        END;
                                    ELSE
                                        IF (
                                             @Actual = 1
                                             AND @Actual = @ScheduledMinutes
                                             AND @tardy = 1
                                           )
                                            BEGIN
                                                SET @ActualRunningTardyHours += 1;
                                            END;
							
					-- Track how many days student has been tardy only when 
					-- program version requires to track tardy
                                    IF (
                                         @tracktardies = 1
                                         AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;	    
		   
			
					-- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
					-- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
					-- when student is tardy the second time, that second occurance will be considered as
					-- absence
					-- Variable @intTardyBreakpoint tracks how many times the student was tardy
					-- Variable @tardiesMakingAbsence tracks the tardy rule
                                    IF (
                                         @tracktardies = 1
                                         AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
                                            SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;
				   
				   -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                         @Actual > 0
                                         AND @Actual > @ScheduledMinutes
                                         AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;
			  
                                    IF ( @tracktardies = 1 )
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                                        END;
				
				---- PRINT @MeetDate
				---- PRINT @ActualRunningAbsentHours
				
                                    DELETE  FROM syStudentAttendanceSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND ClsSectionId = @ClsSectionId
                                            AND StudentAttendedDate = @MeetDate;
                                    INSERT  INTO syStudentAttendanceSummary
                                            (
                                             StuEnrollId
                                            ,ClsSectionId
                                            ,StudentAttendedDate
                                            ,ScheduledDays
                                            ,ActualDays
                                            ,ActualRunningScheduledDays
                                            ,ActualRunningPresentDays
                                            ,ActualRunningAbsentDays
                                            ,ActualRunningMakeupDays
                                            ,ActualRunningTardyDays
                                            ,AdjustedPresentDays
                                            ,AdjustedAbsentDays
                                            ,AttendanceTrackType
                                            ,ModUser
                                            ,ModDate
                                            ,tardiesmakingabsence
										
                                            )
                                    VALUES  (
                                             @StuEnrollId
                                            ,@ClsSectionId
                                            ,@MeetDate
                                            ,@ScheduledMinutes
                                            ,@Actual
                                            ,@ActualRunningScheduledDays
                                            ,@ActualRunningPresentHours
                                            ,@ActualRunningAbsentHours
                                            ,ISNULL(@MakeupHours,0)
                                            ,@ActualRunningTardyHours
                                            ,@AdjustedPresentDaysComputed
                                            ,@AdjustedRunningAbsentHours
                                            ,''Post Attendance by Class Min''
                                            ,''sa''
                                            ,GETDATE()
                                            ,@TardiesMakingAbsence
										
                                            );

				--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                                    SET @PrevStuEnrollId = @StuEnrollId; 

                                    FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,
                                        @Actual,@Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
			
                            DECLARE @MyTardyTable TABLE
                                (
                                 ClsSectionId UNIQUEIDENTIFIER
                                ,TardiesMakingAbsence INT
                                ,AbsentHours DECIMAL(18,2)
                                ); 

                            INSERT  INTO @MyTardyTable
                                    SELECT  ClsSectionId
                                           ,PV.TardiesMakingAbsence
                                           ,CASE WHEN ( COUNT(*) >= PV.TardiesMakingAbsence ) THEN ( COUNT(*) / PV.TardiesMakingAbsence )
                                                 ELSE 0
                                            END AS AbsentHours 
--Count(*) as NumberofTimesTardy 
                                    FROM    syStudentAttendanceSummary SAS
                                    INNER JOIN arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                    WHERE   SE.StuEnrollId = @StuEnrollId
                                            AND SAS.IsTardy = 1
                                            AND PV.TrackTardies = 1
                                    GROUP BY ClsSectionId
                                           ,PV.TardiesMakingAbsence; 

--Drop table @MyTardyTable
                            DECLARE @TotalTardyAbsentDays DECIMAL(18,2);
                            SET @TotalTardyAbsentDays = (
                                                          SELECT    ISNULL(SUM(AbsentHours),0)
                                                          FROM      @MyTardyTable
                                                        );

--Print @TotalTardyAbsentDays

                            UPDATE  syStudentAttendanceSummary
                            SET     AdjustedPresentDays = AdjustedPresentDays - @TotalTardyAbsentDays
                                   ,AdjustedAbsentDays = AdjustedAbsentDays + @TotalTardyAbsentDays
                            WHERE   StuEnrollId = @StuEnrollId
                                    AND StudentAttendedDate = (
                                                                SELECT TOP 1
                                                                        StudentAttendedDate
                                                                FROM    syStudentAttendanceSummary
                                                                WHERE   StuEnrollId = @StuEnrollId
                                                                ORDER BY StudentAttendedDate DESC
                                                              );
			
                        END;	
                END; -- Step 3 
-- END -- Step 3 

-- Step 4  --  InsertAttendance_Class_Minutes
--         --  TrackSapAttendance = ''byclass''
                BEGIN  -- Step 4  --  InsertAttendance_Class_Minutes
		-- By Class and Attendance Unit Type - Minutes and Clock Hour
		
                    IF @UnitTypeDescrip IN ( ''minutes'',''clock hours'' )
                        AND @TrackSapAttendance = ''byclass''
                        BEGIN
                            DELETE  FROM syStudentAttendanceSummary
                            WHERE   StuEnrollId = @StuEnrollId;
				
                            DECLARE GetAttendance_Cursor CURSOR
                            FOR
                                SELECT  *
                                       ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                FROM    (
                                          SELECT DISTINCT
                                                    t1.StuEnrollId
                                                   ,t1.ClsSectionId
                                                   ,t1.MeetDate
                                                   ,DATENAME(dw,t1.MeetDate) AS WeekDay
                                                   ,t4.StartDate
                                                   ,t4.EndDate
                                                   ,t5.PeriodDescrip
                                                   ,t1.Actual
                                                   ,t1.Excused
                                                   ,CASE WHEN (
                                                                t1.Actual = 0
                                                                AND t1.Excused = 0
                                                              ) THEN t1.Scheduled
                                                         ELSE CASE WHEN (
                                                                          t1.Actual <> 9999.00
                                                                          AND t1.Actual < t1.Scheduled
                                                                        ) THEN ( t1.Scheduled - t1.Actual )
                                                                   ELSE 0
                                                              END
                                                    END AS Absent
                                                   ,t1.Scheduled AS ScheduledMinutes
                                                   ,CASE WHEN (
                                                                t1.Actual > 0
                                                                AND t1.Actual < t1.Scheduled
                                                              ) THEN ( t1.Scheduled - t1.Actual )
                                                         ELSE 0
                                                    END AS TardyMinutes
                                                   ,t1.Tardy AS Tardy
                                                   ,t3.TrackTardies
                                                   ,t3.TardiesMakingAbsence
                                                   ,t3.PrgVerId
                                          FROM      atClsSectAttendance t1
                                          INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                          INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                          INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                          INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                             AND (
                                                                                   CONVERT(DATE,t1.MeetDate,111) >= CONVERT(DATE,t4.StartDate,111)
                                                                                   AND CONVERT(DATE,t1.MeetDate,111) <= CONVERT(DATE,t4.EndDate,111)
                                                                                 )
                                                                             AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                          INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                          INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                          INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                          INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                          INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                          INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                          WHERE     t2.StuEnrollId = @StuEnrollId
                                                    AND (
                                                          AAUT1.UnitTypeDescrip IN ( ''Minutes'',''Clock Hours'' )
                                                          OR AAUT2.UnitTypeDescrip IN ( ''Minutes'',''Clock Hours'' )
                                                        )
                                                    AND t1.Actual <> 9999
                                          UNION
                                          SELECT DISTINCT
                                                    t1.StuEnrollId
                                                   ,NULL AS ClsSectionId
                                                   ,t1.RecordDate AS MeetDate
                                                   ,DATENAME(dw,t1.RecordDate) AS WeekDay
                                                   ,NULL AS StartDate
                                                   ,NULL AS EndDate
                                                   ,NULL AS PeriodDescrip
                                                   ,t1.ActualHours
                                                   ,NULL AS Excused
                                                   ,CASE WHEN ( t1.ActualHours = 0 ) THEN t1.SchedHours
                                                         ELSE 0
								 --ELSE 
									--Case when (t1.ActualHours <> 9999.00 and t1.ActualHours < t1.SchedHours)
									--		THEN (t1.SchedHours - t1.ActualHours)
									--		ELSE 
									--			0
									--		End
                                                    END AS Absent
                                                   ,t1.SchedHours AS ScheduledMinutes
                                                   ,CASE WHEN (
                                                                t1.ActualHours <> 9999.00
                                                                AND t1.ActualHours > 0
                                                                AND t1.ActualHours < t1.SchedHours
                                                              ) THEN ( t1.SchedHours - t1.ActualHours )
                                                         ELSE 0
                                                    END AS TardyMinutes
                                                   ,NULL AS Tardy
                                                   ,t3.TrackTardies
                                                   ,t3.TardiesMakingAbsence
                                                   ,t3.PrgVerId
                                          FROM      arStudentClockAttendance t1
                                          INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                          INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                          WHERE     t2.StuEnrollId = @StuEnrollId
                                                    AND t1.Converted = 1
                                                    AND t1.ActualHours <> 9999
                                        ) dt
                                ORDER BY StuEnrollId
                                       ,MeetDate;
                            OPEN GetAttendance_Cursor;
                            FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,
                                @Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @boolReset = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                            SET @boolReset = 1;
                                        END;

					  
			-- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                                    IF @Actual <> 9999.00
                                        BEGIN
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; 
					-- If there are make up hrs deduct that otherwise it will be added again in progress report
                                            IF (
                                                 @Actual > 0
                                                 AND @Actual > @ScheduledMinutes
                                                 AND @Actual <> 9999.00
                                               )
                                                BEGIN
                                                    SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                END; 
                                            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;                      
                                        END;
		   
			-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                                    SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
	  
			-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                                    IF (
                                         @Actual > 0
                                         AND @Actual < @ScheduledMinutes
                                         AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                        END;
					
			-- Track how many days student has been tardy only when 
			-- program version requires to track tardy
                                    IF (
                                         @tracktardies = 1
                                         AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;	    
		   
			
			-- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
			-- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
			-- when student is tardy the second time, that second occurance will be considered as
			-- absence
			-- Variable @intTardyBreakpoint tracks how many times the student was tardy
			-- Variable @tardiesMakingAbsence tracks the tardy rule
                                    IF (
                                         @tracktardies = 1
                                         AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                            SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;
		   
		   -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                         @Actual > 0
                                         AND @Actual > @ScheduledMinutes
                                         AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;
	  
	 
                                    IF ( @tracktardies = 1 )
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                                        END;
		
		
		
		
                                    DELETE  FROM syStudentAttendanceSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND ClsSectionId = @ClsSectionId
                                            AND StudentAttendedDate = @MeetDate;
                                    INSERT  INTO syStudentAttendanceSummary
                                            (
                                             StuEnrollId
                                            ,ClsSectionId
                                            ,StudentAttendedDate
                                            ,ScheduledDays
                                            ,ActualDays
                                            ,ActualRunningScheduledDays
                                            ,ActualRunningPresentDays
                                            ,ActualRunningAbsentDays
                                            ,ActualRunningMakeupDays
                                            ,ActualRunningTardyDays
                                            ,AdjustedPresentDays
                                            ,AdjustedAbsentDays
                                            ,AttendanceTrackType
                                            ,ModUser
                                            ,ModDate
										
                                            )
                                    VALUES  (
                                             @StuEnrollId
                                            ,@ClsSectionId
                                            ,@MeetDate
                                            ,@ScheduledMinutes
                                            ,@Actual
                                            ,@ActualRunningScheduledDays
                                            ,@ActualRunningPresentHours
                                            ,@ActualRunningAbsentHours
                                            ,ISNULL(@MakeupHours,0)
                                            ,@ActualRunningTardyHours
                                            ,@AdjustedPresentDaysComputed
                                            ,@AdjustedRunningAbsentHours
                                            ,''Post Attendance by Class Min''
                                            ,''sa''
                                            ,GETDATE()
                                            );

                                    UPDATE  syStudentAttendanceSummary
                                    SET     tardiesmakingabsence = @TardiesMakingAbsence
                                    WHERE   StuEnrollId = @StuEnrollId;
                                    SET @PrevStuEnrollId = @StuEnrollId; 

                                    FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,
                                        @Actual,@Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
                        END;
                END; --  Step 3  --   InsertAttendance_Class_Minutes
--END --  Step 3  --   InsertAttendance_Class_Minutes


--Select * from arAttUnitType

			
-- By Minutes/Day
-- remove clock hour from here
                IF @UnitTypeDescrip IN ( ''minutes'' )
                    AND @TrackSapAttendance = ''byday''
	-- -- PRINT GETDATE();	
	---- PRINT @UnitTypeId;
	---- PRINT @TrackSapAttendance;
                    BEGIN
			--Delete from syStudentAttendanceSummary where StuEnrollId=@StuEnrollId

                        DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD
                        FOR
                            SELECT  t1.StuEnrollId
                                   ,NULL AS ClsSectionId
                                   ,t1.RecordDate AS MeetDate
                                   ,t1.ActualHours
                                   ,t1.SchedHours AS ScheduledMinutes
                                   ,CASE WHEN (
                                                (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999,9999 )
                                                )
                                                AND t1.ActualHours = 0
                                              ) THEN t1.SchedHours
                                         ELSE 0
                                    END AS Absent
                                   ,t1.isTardy
                                   ,(
                                      SELECT    ISNULL(SUM(SchedHours),0)
                                      FROM      arStudentClockAttendance
                                      WHERE     StuEnrollId = t1.StuEnrollId
                                                AND RecordDate <= t1.RecordDate
                                                AND (
                                                      t1.SchedHours >= 1
                                                      AND t1.SchedHours NOT IN ( 999,9999 )
                                                      AND t1.ActualHours NOT IN ( 999,9999 )
                                                    )
                                    ) AS ActualRunningScheduledHours
                                   ,(
                                      SELECT    SUM(ActualHours)
                                      FROM      arStudentClockAttendance
                                      WHERE     StuEnrollId = t1.StuEnrollId
                                                AND RecordDate <= t1.RecordDate
                                                AND (
                                                      t1.SchedHours >= 1
                                                      AND t1.SchedHours NOT IN ( 999,9999 )
                                                    )
                                                AND ActualHours >= 1
                                                AND ActualHours NOT IN ( 999,9999 )
                                    ) AS ActualRunningPresentHours
                                   ,(
                                      SELECT    COUNT(ActualHours)
                                      FROM      arStudentClockAttendance
                                      WHERE     StuEnrollId = t1.StuEnrollId
                                                AND RecordDate <= t1.RecordDate
                                                AND (
                                                      t1.SchedHours >= 1
                                                      AND t1.SchedHours NOT IN ( 999,9999 )
                                                    )
                                                AND ActualHours = 0
                                                AND ActualHours NOT IN ( 999,9999 )
                                    ) AS ActualRunningAbsentHours
                                   ,(
                                      SELECT    SUM(ActualHours)
                                      FROM      arStudentClockAttendance
                                      WHERE     StuEnrollId = t1.StuEnrollId
                                                AND RecordDate <= t1.RecordDate
                                                AND SchedHours = 0
                                                AND ActualHours >= 1
                                                AND ActualHours NOT IN ( 999,9999 )
                                    ) AS ActualRunningMakeupHours
                                   ,(
                                      SELECT    SUM(SchedHours - ActualHours)
                                      FROM      arStudentClockAttendance
                                      WHERE     StuEnrollId = t1.StuEnrollId
                                                AND RecordDate <= t1.RecordDate
                                                AND (
                                                      (
                                                        t1.SchedHours >= 1
                                                        AND t1.SchedHours NOT IN ( 999,9999 )
                                                      )
                                                      AND ActualHours >= 1
                                                      AND ActualHours NOT IN ( 999,9999 )
                                                    )
                                                AND isTardy = 1
                                    ) AS ActualRunningTardyHours
                                   ,t3.TrackTardies
                                   ,t3.TardiesMakingAbsence
                                   ,t3.PrgVerId
                                   ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
                            FROM    arStudentClockAttendance t1
                            INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                            INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                            INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
			--inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                            WHERE   AAUT1.UnitTypeDescrip IN ( ''Minutes'' )
                                    AND t2.StuEnrollId = @StuEnrollId
                                    AND t1.ActualHours <> 9999.00
                            ORDER BY t1.StuEnrollId
                                   ,MeetDate;
                        OPEN GetAttendance_Cursor;
--Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                        FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,
                            @ActualRunningScheduledHours,@ActualRunningPresentHours,@ActualRunningAbsentHours,@ActualRunningMakeupHours,@ActualRunningTardyHours,
                            @tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;

                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @ActualRunningMakeupHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        WHILE @@FETCH_STATUS = 0
                            BEGIN

                                IF @PrevStuEnrollId <> @StuEnrollId
                                    BEGIN
                                        SET @ActualRunningPresentHours = 0;
                                        SET @ActualRunningAbsentHours = 0;
                                        SET @intTardyBreakPoint = 0;
                                        SET @ActualRunningTardyHours = 0;
                                        SET @AdjustedRunningPresentHours = 0;
                                        SET @AdjustedRunningAbsentHours = 0;
                                        SET @ActualRunningScheduledDays = 0;
                                    END;

                                IF (
                                     @ScheduledMinutes >= 1
                                     AND (
                                           @Actual <> 9999
                                           AND @Actual <> 999
                                         )
                                     AND (
                                           @ScheduledMinutes <> 9999
                                           AND @Actual <> 999
                                         )
                                   )
                                    BEGIN
                                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays,0) + ISNULL(@ScheduledMinutes,0);
                                    END;
                                ELSE
                                    BEGIN
                                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays,0); 
                                    END;
	   
                                IF (
                                     @Actual <> 9999
                                     AND @Actual <> 999
                                   )
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                    END;
                                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;

                                IF (
                                     @Actual > 0
                                     AND @Actual < @ScheduledMinutes
                                   )
                                    BEGIN
                                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                                    END;
			-- Make up hours
		--sched=5, Actual =7, makeup= 2,ab = 0
		--sched=0, Actual =7, makeup= 7,ab = 0
                                IF (
                                     @Actual > 0
                                     AND @ScheduledMinutes > 0
                                     AND @Actual > @ScheduledMinutes
                                     AND (
                                           @Actual <> 9999
                                           AND @Actual <> 999
                                         )
                                   )
                                    BEGIN
                                        SET @ActualRunningMakeupHours = @ActualRunningMakeupHours + ( @Actual - @ScheduledMinutes );
                                    END;

		
                                IF (
                                     @Actual <> 9999
                                     AND @Actual <> 999
                                   )
                                    BEGIN
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                                    END;	
                                IF (
                                     @Actual = 0
                                     AND @ScheduledMinutes >= 1
                                     AND @ScheduledMinutes NOT IN ( 999,9999 )
                                   )
                                    BEGIN
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                                    END;
		-- Absent hours
		--1. sched = 5, Actual = 2 then Ab = (5-3) = 2
                                IF (
                                     @Absent = 0
                                     AND @ActualRunningAbsentHours > 0
                                     AND ( @Actual < @ScheduledMinutes )
                                   )
                                    BEGIN
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + ( @ScheduledMinutes - @Actual );	
                                    END;
                                IF (
                                     @Actual > 0
                                     AND @Actual < @ScheduledMinutes
                                     AND (
                                           @Actual <> 9999.00
                                           AND @Actual <> 999.00
                                         )
                                   )
                                    BEGIN
                                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                    END;
	
                                IF @tracktardies = 1
                                    AND (
                                          @TardyMinutes > 0
                                          OR @IsTardy = 1
                                        )
                                    BEGIN
                                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                    END;	    
                                IF (
                                     @tracktardies = 1
                                     AND @intTardyBreakPoint = @TardiesMakingAbsence
                                   )
                                    BEGIN
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Actual; --@TardyMinutes
                                        SET @intTardyBreakPoint = 0;
                                    END;
                                DELETE  FROM syStudentAttendanceSummary
                                WHERE   StuEnrollId = @StuEnrollId
                                        AND StudentAttendedDate = @MeetDate;
                                INSERT  INTO syStudentAttendanceSummary
                                        (
                                         StuEnrollId
                                        ,ClsSectionId
                                        ,StudentAttendedDate
                                        ,ScheduledDays
                                        ,ActualDays
                                        ,ActualRunningScheduledDays
                                        ,ActualRunningPresentDays
                                        ,ActualRunningAbsentDays
                                        ,ActualRunningMakeupDays
                                        ,ActualRunningTardyDays
                                        ,AdjustedPresentDays
                                        ,AdjustedAbsentDays
                                        ,AttendanceTrackType
                                        ,ModUser
                                        ,ModDate
										)
                                VALUES  (
                                         @StuEnrollId
                                        ,@ClsSectionId
                                        ,@MeetDate
                                        ,@ScheduledMinutes
                                        ,@Actual
                                        ,@ActualRunningScheduledDays
                                        ,@ActualRunningPresentHours
                                        ,@ActualRunningAbsentHours
                                        ,@ActualRunningMakeupHours
                                        ,@ActualRunningTardyHours
                                        ,@AdjustedRunningPresentHours
                                        ,@AdjustedRunningAbsentHours
                                        ,''Post Attendance by Class''
                                        ,''sa''
                                        ,GETDATE()
                                        );
				
                                UPDATE  syStudentAttendanceSummary
                                SET     tardiesmakingabsence = @TardiesMakingAbsence
                                WHERE   StuEnrollId = @StuEnrollId;
			--end
                                SET @PrevStuEnrollId = @StuEnrollId; 
                                FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,
                                    @ActualRunningScheduledHours,@ActualRunningPresentHours,@ActualRunningAbsentHours,@ActualRunningMakeupHours,
                                    @ActualRunningTardyHours,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                            END;
                        CLOSE GetAttendance_Cursor;
                        DEALLOCATE GetAttendance_Cursor;
                    END;

	---- PRINT ''Does it get to Clock Hour/PA'';
	---- PRINT @UnitTypeId;
	---- PRINT @TrackSapAttendance;
	-- By Day and PA, Clock Hour
	-- -- Step 2  --  InsertAttendance_Day_PresentAbsent  -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'')  and TrackSapAttendance = ''byday''
                BEGIN -- Step 2  --  InsertAttendance_Day_PresentAbsent  -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'') and TrackSapAttendance = ''byday''
                    IF @UnitTypeDescrip IN ( ''present absent'',''clock hours'' )
                        AND @TrackSapAttendance = ''byday''
                        BEGIN
			---- PRINT ''By Day inside day'';
                            DECLARE GetAttendance_Cursor CURSOR
                            FOR
                                SELECT  t1.StuEnrollId
                                       ,t1.RecordDate
                                       ,t1.ActualHours
                                       ,t1.SchedHours
                                       ,CASE WHEN (
                                                    (
                                                      t1.SchedHours >= 1
                                                      AND t1.SchedHours NOT IN ( 999,9999 )
                                                    )
                                                    AND t1.ActualHours = 0
                                                  ) THEN t1.SchedHours
                                             ELSE 0
                                        END AS Absent
                                       ,t1.isTardy
                                       ,t3.TrackTardies
                                       ,t3.TardiesMakingAbsence
                                FROM    arStudentClockAttendance t1
                                INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
			--inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                                WHERE   -- Unit Types: Present Absent and Clock Hour
                                        AAUT1.UnitTypeDescrip IN ( ''present absent'',''clock hours'' )
                                        AND ActualHours <> 9999.00
                                        AND t2.StuEnrollId = @StuEnrollId
                                ORDER BY t1.StuEnrollId
                                       ,t1.RecordDate;
                            OPEN GetAttendance_Cursor;
--Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                            FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,@tracktardies,
                                @TardiesMakingAbsence;

                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                        END;
	   
                                    IF (
                                         @Actual <> 9999
                                         AND @Actual <> 999
                                       )
                                        BEGIN
                                            SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays,0) + @ScheduledMinutes;
                                            SET @ActualRunningPresentHours = ISNULL(@ActualRunningPresentHours,0) + @Actual;
                                            SET @AdjustedRunningPresentHours = ISNULL(@AdjustedRunningPresentHours,0) + @Actual;
                                        END;
                                    SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours,0) + @Absent;
                                    IF (
                                         @Actual = 0
                                         AND @ScheduledMinutes >= 1
                                         AND @ScheduledMinutes NOT IN ( 999,9999 )
                                       )
                                        BEGIN
                                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                                        END;
		
		-- NWH 
		-- If Scheduled = 3.0 hrs and Actual = 1.0 Then 2 hrs is considered absent
                                    IF (
                                         @ScheduledMinutes >= 1
                                         AND @ScheduledMinutes NOT IN ( 999,9999 )
                                         AND @Actual > 0
                                         AND ( @Actual < @ScheduledMinutes )
                                       )
                                        BEGIN
                                            SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours,0) + ( @ScheduledMinutes - @Actual );
                                            SET @AdjustedRunningAbsentHours = ISNULL(@AdjustedRunningAbsentHours,0) + ( @ScheduledMinutes - @Actual );
                                        END; 
		
                                    IF (
                                         @tracktardies = 1
                                         AND @IsTardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + 1;
                                        END;
		 --commented by balaji on 10/22/2012 as report (rdl) doesn''t add days attended and make up days
		 ---- If there are make up hrs deduct that otherwise it will be added again in progress report
                                    IF (
                                         @Actual > 0
                                         AND @Actual > @ScheduledMinutes
                                         AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                        END;
			
                                    IF @tracktardies = 1
                                        AND (
                                              @TardyMinutes > 0
                                              OR @IsTardy = 1
                                            )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;	    
		
		
		
		-- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                         @Actual > 0
                                         AND @Actual > @ScheduledMinutes
                                         AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;
		
                                    IF (
                                         @tracktardies = 1
                                         AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;
				
                                    DELETE  FROM syStudentAttendanceSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND StudentAttendedDate = @MeetDate;
                                    INSERT  INTO syStudentAttendanceSummary
                                            (
                                             StuEnrollId
                                            ,ClsSectionId
                                            ,StudentAttendedDate
                                            ,ScheduledDays
                                            ,ActualDays
                                            ,ActualRunningScheduledDays
                                            ,ActualRunningPresentDays
                                            ,ActualRunningAbsentDays
                                            ,ActualRunningMakeupDays
                                            ,ActualRunningTardyDays
                                            ,AdjustedPresentDays
                                            ,AdjustedAbsentDays
                                            ,AttendanceTrackType
                                            ,ModUser
                                            ,ModDate
                                            ,tardiesmakingabsence
										
                                            )
                                    VALUES  (
                                             @StuEnrollId
                                            ,@ClsSectionId
                                            ,@MeetDate
                                            ,ISNULL(@ScheduledMinutes,0)
                                            ,@Actual
                                            ,ISNULL(@ActualRunningScheduledDays,0)
                                            ,ISNULL(@ActualRunningPresentHours,0)
                                            ,ISNULL(@ActualRunningAbsentHours,0)
                                            ,ISNULL(@MakeupHours,0)
                                            ,ISNULL(@ActualRunningTardyHours,0)
                                            ,ISNULL(@AdjustedRunningPresentHours,0)
                                            ,ISNULL(@AdjustedRunningAbsentHours,0)
                                            ,''Post Attendance by Class''
                                            ,''sa''
                                            ,GETDATE()
                                            ,@TardiesMakingAbsence
										
                                            );

		--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
		--end
                                    SET @PrevStuEnrollId = @StuEnrollId; 
                                    FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,@tracktardies,
                                        @TardiesMakingAbsence;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
                        END;
                END;
							
--end

-- Based on grade rounding round the final score and current score
--Declare @PrevTermId uniqueidentifier,@PrevReqId uniqueidentifier,@RowCount int,@FinalScore decimal(18,4),@CurrentScore decimal(18,4)
--declare @ReqId uniqueidentifier,@CourseCodeDescrip varchar(100),@FinalGrade varchar(10),@sysComponentTypeId int
--declare @CreditsAttempted decimal(18,4),@Grade varchar(10),@IsPass bit,@IsCreditsAttempted bit,@IsCreditsEarned bit
--declare @IsInGPA bit,@FinAidCredits decimal(18,4),@CurrentGrade varchar(10),@CourseCredits decimal(18,4)
                DECLARE @GPA DECIMAL(18,4);

                DECLARE @PrevReqId UNIQUEIDENTIFIER
                   ,@PrevTermId UNIQUEIDENTIFIER
                   ,@CreditsAttempted DECIMAL(18,2)
                   ,@CreditsEarned DECIMAL(18,2);
                DECLARE @reqid UNIQUEIDENTIFIER
                   ,@CourseCodeDescrip VARCHAR(50)
                   ,@FinalGrade VARCHAR(50)
                   ,@FinalScore DECIMAL(18,2)
                   ,@Grade VARCHAR(50);
                DECLARE @IsPass BIT
                   ,@IsCreditsAttempted BIT
                   ,@IsCreditsEarned BIT
                   ,@CurrentScore DECIMAL(18,2)
                   ,@CurrentGrade VARCHAR(10)
                   ,@FinalGPA DECIMAL(18,2);
                DECLARE @sysComponentTypeId INT
                   ,@RowCount INT;
                DECLARE @IsInGPA BIT;
                DECLARE @CourseCredits DECIMAL(18,2);
                DECLARE @FinAidCreditsEarned DECIMAL(18,2)
                   ,@FinAidCredits DECIMAL(18,2);

                DECLARE GetCreditsSummary_Cursor CURSOR
                FOR
                    SELECT	DISTINCT
                            SE.StuEnrollId
                           ,T.TermId
                           ,T.TermDescrip
                           ,T.StartDate
                           ,R.ReqId
                           ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                           ,RES.Score AS FinalScore
                           ,RES.GrdSysDetailId AS FinalGrade
                           ,GCT.SysComponentTypeId
                           ,R.Credits AS CreditsAttempted
                           ,CS.ClsSectionId
                           ,GSD.Grade
                           ,GSD.IsPass
                           ,GSD.IsCreditsAttempted
                           ,GSD.IsCreditsEarned
                           ,SE.PrgVerId
                           ,GSD.IsInGPA
                           ,R.FinAidCredits AS FinAidCredits
                    FROM    arStuEnrollments SE
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                    INNER JOIN arTerm T ON CS.TermId = T.TermId
                    INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                    LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                    LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                    LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                    LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    WHERE   SE.StuEnrollId = @StuEnrollId
                    ORDER BY T.StartDate
                           ,T.TermDescrip
                           ,R.ReqId; 

                DECLARE @varGradeRounding VARCHAR(3);
                DECLARE @roundfinalscore DECIMAL(18,4);
                SET @varGradeRounding = (
                                          SELECT    Value
                                          FROM      syConfigAppSetValues
                                          WHERE     SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                    @FinalGrade,@sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,
                    @FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN
			
			
                        SET @CurrentScore = (
                                              SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                             ELSE NULL
                                                        END AS CurrentScore
                                              FROM      (
                                                          SELECT    InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight AS GradeBookWeight
                                                                   ,CASE WHEN S.NumberOfComponents > 0
                                                                         THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                         ELSE 0
                                                                    END AS ActualWeight
                                                          FROM      (
                                                                      SELECT    C.InstrGrdBkWgtDetailId
                                                                               ,D.Code
                                                                               ,D.Descrip
                                                                               ,ISNULL(C.Weight,0) AS Weight
                                                                               ,C.Number AS MinNumber
                                                                               ,C.GrdPolicyId
                                                                               ,C.Parameter AS Param
                                                                               ,X.GrdScaleId
                                                                               ,SUM(GR.Score) AS Score
                                                                               ,COUNT(D.Descrip) AS NumberOfComponents
                                                                      FROM      (
                                                                                  SELECT DISTINCT TOP 1
                                                                                            A.InstrGrdBkWgtId
                                                                                           ,A.EffectiveDate
                                                                                           ,B.GrdScaleId
                                                                                  FROM      arGrdBkWeights A
                                                                                           ,arClassSections B
                                                                                  WHERE     A.ReqId = B.ReqId
                                                                                            AND A.EffectiveDate <= B.StartDate
                                                                                            AND B.ClsSectionId = @ClsSectionId
                                                                                  ORDER BY  A.EffectiveDate DESC
                                                                                ) X
                                                                               ,arGrdBkWgtDetails C
                                                                               ,arGrdComponentTypes D
                                                                               ,arGrdBkResults GR
                                                                      WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                                AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                                AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                                AND D.SysComponentTypeId NOT IN ( 500,503 )
                                                                                AND GR.StuEnrollId = @StuEnrollId
                                                                                AND GR.ClsSectionId = @ClsSectionId
                                                                                AND GR.Score IS NOT NULL
                                                                      GROUP BY  C.InstrGrdBkWgtDetailId
                                                                               ,D.Code
                                                                               ,D.Descrip
                                                                               ,C.Weight
                                                                               ,C.Number
                                                                               ,C.GrdPolicyId
                                                                               ,C.Parameter
                                                                               ,X.GrdScaleId
                                                                    ) S
                                                          GROUP BY  InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight
                                                                   ,NumberOfComponents
                                                        ) FinalTblToComputeCurrentScore
                                            );
                        IF ( @CurrentScore IS NULL )
                            BEGIN
				-- instructor grade books
                                SET @CurrentScore = (
                                                      SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                                     ELSE NULL
                                                                END AS CurrentScore
                                                      FROM      (
                                                                  SELECT    InstrGrdBkWgtDetailId
                                                                           ,Code
                                                                           ,Descrip
                                                                           ,Weight AS GradeBookWeight
                                                                           ,CASE WHEN S.NumberOfComponents > 0
                                                                                 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                                 ELSE 0
                                                                            END AS ActualWeight
                                                                  FROM      (
                                                                              SELECT    C.InstrGrdBkWgtDetailId
                                                                                       ,D.Code
                                                                                       ,D.Descrip
                                                                                       ,ISNULL(C.Weight,0) AS Weight
                                                                                       ,C.Number AS MinNumber
                                                                                       ,C.GrdPolicyId
                                                                                       ,C.Parameter AS Param
                                                                                       ,X.GrdScaleId
                                                                                       ,SUM(GR.Score) AS Score
                                                                                       ,COUNT(D.Descrip) AS NumberOfComponents
                                                                              FROM      (
                                                                                          --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
															--FROM          arGrdBkWeights A,arClassSections B        
															--WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
															--ORDER BY      A.EffectiveDate DESC
                                                                                          SELECT DISTINCT TOP 1
                                                                                                    t1.InstrGrdBkWgtId
                                                                                                   ,t1.GrdScaleId
                                                                                          FROM      arClassSections t1
                                                                                                   ,arGrdBkWeights t2
                                                                                          WHERE     t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                                                    AND t1.ClsSectionId = @ClsSectionId
                                                                                        ) X
                                                                                       ,arGrdBkWgtDetails C
                                                                                       ,arGrdComponentTypes D
                                                                                       ,arGrdBkResults GR
                                                                              WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                                        AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                                        AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                                        AND
													-- D.SysComponentTypeID not in (500,503) and 
                                                                                        GR.StuEnrollId = @StuEnrollId
                                                                                        AND GR.ClsSectionId = @ClsSectionId
                                                                                        AND GR.Score IS NOT NULL
                                                                              GROUP BY  C.InstrGrdBkWgtDetailId
                                                                                       ,D.Code
                                                                                       ,D.Descrip
                                                                                       ,C.Weight
                                                                                       ,C.Number
                                                                                       ,C.GrdPolicyId
                                                                                       ,C.Parameter
                                                                                       ,X.GrdScaleId
                                                                            ) S
                                                                  GROUP BY  InstrGrdBkWgtDetailId
                                                                           ,Code
                                                                           ,Descrip
                                                                           ,Weight
                                                                           ,NumberOfComponents
                                                                ) FinalTblToComputeCurrentScore
                                                    );	
			
                            END;
			
		
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore,0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;	
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
				
                        UPDATE  syCreditSummary
                        SET     FinalScore = @FinalScore
                               ,CurrentScore = @CurrentScore
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId
                                AND ReqId = @reqid;

			--Average calculation
			
			-- Term Average
                        SET @TermAverageCount = (
                                                  SELECT    COUNT(*)
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = @StuEnrollId
                                                            AND TermId = @TermId
                                                            AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                                SELECT  SUM(FinalScore)
                                                FROM    syCreditSummary
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND TermId = @TermId
                                                        AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
                        SET @cumAveragecount = (
                                                 SELECT COUNT(*)
                                                 FROM   syCreditSummary
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                               SELECT   SUM(FinalScore)
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
                        UPDATE  syCreditSummary
                        SET     Average = @TermAverage
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId; 
			
			--Update Cumulative GPA
                        UPDATE  syCreditSummary
                        SET     CumAverage = @CumAverage
                        WHERE   StuEnrollId = @StuEnrollId;
			

                        FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                            @FinalGrade,@sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,
                            @IsInGPA,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;


                DECLARE GetCreditsSummary_Cursor CURSOR
                FOR
                    SELECT	DISTINCT
                            SE.StuEnrollId
                           ,T.TermId
                           ,T.TermDescrip
                           ,T.StartDate
                           ,R.ReqId
                           ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                           ,RES.Score AS FinalScore
                           ,RES.GrdSysDetailId AS FinalGrade
                           ,GCT.SysComponentTypeId
                           ,R.Credits AS CreditsAttempted
                           ,CS.ClsSectionId
                           ,GSD.Grade
                           ,GSD.IsPass
                           ,GSD.IsCreditsAttempted
                           ,GSD.IsCreditsEarned
                           ,SE.PrgVerId
                           ,GSD.IsInGPA
                           ,R.FinAidCredits AS FinAidCredits
                    FROM    arStuEnrollments SE
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                                     AND RES.TermId = CS.TermId
                    INNER JOIN arTerm T ON CS.TermId = T.TermId
                    INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                    LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                    LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                    LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                    LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    WHERE   SE.StuEnrollId = @StuEnrollId
                    ORDER BY T.StartDate
                           ,T.TermDescrip
                           ,R.ReqId;


                SET @varGradeRounding = (
                                          SELECT    Value
                                          FROM      syConfigAppSetValues
                                          WHERE     SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                    @FinalGrade,@sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,
                    @FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN
			
                        SET @CurrentScore = (
                                              SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                             ELSE NULL
                                                        END AS CurrentScore
                                              FROM      (
                                                          SELECT    InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight AS GradeBookWeight
                                                                   ,CASE WHEN S.NumberOfComponents > 0
                                                                         THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                         ELSE 0
                                                                    END AS ActualWeight
                                                          FROM      (
                                                                      SELECT    C.InstrGrdBkWgtDetailId
                                                                               ,D.Code
                                                                               ,D.Descrip
                                                                               ,ISNULL(C.Weight,0) AS Weight
                                                                               ,C.Number AS MinNumber
                                                                               ,C.GrdPolicyId
                                                                               ,C.Parameter AS Param
                                                                               ,X.GrdScaleId
                                                                               ,SUM(GR.Score) AS Score
                                                                               ,COUNT(D.Descrip) AS NumberOfComponents
                                                                      FROM      (
                                                                                  SELECT DISTINCT TOP 1
                                                                                            A.InstrGrdBkWgtId
                                                                                           ,A.EffectiveDate
                                                                                           ,B.GrdScaleId
                                                                                  FROM      arGrdBkWeights A
                                                                                           ,arClassSections B
                                                                                  WHERE     A.ReqId = B.ReqId
                                                                                            AND A.EffectiveDate <= B.StartDate
                                                                                            AND B.ClsSectionId = @ClsSectionId
                                                                                  ORDER BY  A.EffectiveDate DESC
                                                                                ) X
                                                                               ,arGrdBkWgtDetails C
                                                                               ,arGrdComponentTypes D
                                                                               ,arGrdBkResults GR
                                                                      WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                                AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                                AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                                AND D.SysComponentTypeId NOT IN ( 500,503 )
                                                                                AND GR.StuEnrollId = @StuEnrollId
                                                                                AND GR.ClsSectionId = @ClsSectionId
                                                                                AND GR.Score IS NOT NULL
                                                                      GROUP BY  C.InstrGrdBkWgtDetailId
                                                                               ,D.Code
                                                                               ,D.Descrip
                                                                               ,C.Weight
                                                                               ,C.Number
                                                                               ,C.GrdPolicyId
                                                                               ,C.Parameter
                                                                               ,X.GrdScaleId
                                                                    ) S
                                                          GROUP BY  InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight
                                                                   ,NumberOfComponents
                                                        ) FinalTblToComputeCurrentScore
                                            );
                        IF ( @CurrentScore IS NULL )
                            BEGIN
				-- instructor grade books
                                SET @CurrentScore = (
                                                      SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                                     ELSE NULL
                                                                END AS CurrentScore
                                                      FROM      (
                                                                  SELECT    InstrGrdBkWgtDetailId
                                                                           ,Code
                                                                           ,Descrip
                                                                           ,Weight AS GradeBookWeight
                                                                           ,CASE WHEN S.NumberOfComponents > 0
                                                                                 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                                 ELSE 0
                                                                            END AS ActualWeight
                                                                  FROM      (
                                                                              SELECT    C.InstrGrdBkWgtDetailId
                                                                                       ,D.Code
                                                                                       ,D.Descrip
                                                                                       ,ISNULL(C.Weight,0) AS Weight
                                                                                       ,C.Number AS MinNumber
                                                                                       ,C.GrdPolicyId
                                                                                       ,C.Parameter AS Param
                                                                                       ,X.GrdScaleId
                                                                                       ,SUM(GR.Score) AS Score
                                                                                       ,COUNT(D.Descrip) AS NumberOfComponents
                                                                              FROM      (
                                                                                          --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
															--FROM          arGrdBkWeights A,arClassSections B        
															--WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
															--ORDER BY      A.EffectiveDate DESC
                                                                                          SELECT DISTINCT TOP 1
                                                                                                    t1.InstrGrdBkWgtId
                                                                                                   ,t1.GrdScaleId
                                                                                          FROM      arClassSections t1
                                                                                                   ,arGrdBkWeights t2
                                                                                          WHERE     t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                                                    AND t1.ClsSectionId = @ClsSectionId
                                                                                        ) X
                                                                                       ,arGrdBkWgtDetails C
                                                                                       ,arGrdComponentTypes D
                                                                                       ,arGrdBkResults GR
                                                                              WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                                        AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                                        AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                                        AND
													-- D.SysComponentTypeID not in (500,503) and 
                                                                                        GR.StuEnrollId = @StuEnrollId
                                                                                        AND GR.ClsSectionId = @ClsSectionId
                                                                                        AND GR.Score IS NOT NULL
                                                                              GROUP BY  C.InstrGrdBkWgtDetailId
                                                                                       ,D.Code
                                                                                       ,D.Descrip
                                                                                       ,C.Weight
                                                                                       ,C.Number
                                                                                       ,C.GrdPolicyId
                                                                                       ,C.Parameter
                                                                                       ,X.GrdScaleId
                                                                            ) S
                                                                  GROUP BY  InstrGrdBkWgtDetailId
                                                                           ,Code
                                                                           ,Descrip
                                                                           ,Weight
                                                                           ,NumberOfComponents
                                                                ) FinalTblToComputeCurrentScore
                                                    );	
			
                            END;
			
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore,0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;	
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
				
                        UPDATE  syCreditSummary
                        SET     FinalScore = @FinalScore
                               ,CurrentScore = @CurrentScore
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId
                                AND ReqId = @reqid;

			--Average calculation
--			declare @CumAverage decimal(18,2),@cumAverageSum decimal(18,2),@cumAveragecount int
			
			-- Term Average
                        SET @TermAverageCount = (
                                                  SELECT    COUNT(*)
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = @StuEnrollId
                                                            AND TermId = @TermId
                                                            AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                                SELECT  SUM(FinalScore)
                                                FROM    syCreditSummary
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND TermId = @TermId
                                                        AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
                        SET @cumAveragecount = (
                                                 SELECT COUNT(*)
                                                 FROM   syCreditSummary
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                               SELECT   SUM(FinalScore)
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
                        UPDATE  syCreditSummary
                        SET     Average = @TermAverage
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId; 
			
			--Update Cumulative GPA
                        UPDATE  syCreditSummary
                        SET     CumAverage = @CumAverage
                        WHERE   StuEnrollId = @StuEnrollId;
			

                        FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                            @FinalGrade,@sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,
                            @IsInGPA,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

                DECLARE @GradeSystemDetailId UNIQUEIDENTIFIER
                   ,@IsGPA BIT;
                DECLARE GetCreditsSummary_Cursor CURSOR
                FOR
                    SELECT DISTINCT
                            SE.StuEnrollId
                           ,T.TermId
                           ,T.TermDescrip
                           ,T.StartDate
                           ,R.ReqId
                           ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                           ,NULL AS FinalScore
                           ,RES.GrdSysDetailId AS GradeSystemDetailId
                           ,GSD.Grade AS FinalGrade
                           ,GSD.Grade AS CurrentGrade
                           ,R.Credits
                           ,NULL AS ClsSectionId
                           ,GSD.IsPass
                           ,GSD.IsCreditsAttempted
                           ,GSD.IsCreditsEarned
                           ,GSD.GPA
                           ,GSD.IsInGPA
                           ,SE.PrgVerId
                           ,GSD.IsInGPA
                           ,R.FinAidCredits AS FinAidCredits
                    FROM    arStuEnrollments SE
                    INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
                    INNER JOIN syCreditSummary CS ON CS.StuEnrollId = RES.StuEnrollId
                    INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                    INNER JOIN arTerm T ON RES.TermId = T.TermId
                    WHERE   RES.ReqId NOT IN ( SELECT DISTINCT
                                                        ReqId
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = SE.StuEnrollId
                                                        AND TermId = T.TermId )
                            AND SE.StuEnrollId = @StuEnrollId;

                SET @varGradeRounding = (
                                          SELECT    Value
                                          FROM      syConfigAppSetValues
                                          WHERE     SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                    @GradeSystemDetailId,@FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,
                    @PrgVerId,@IsInGPA,@FinAidCredits; 
                WHILE @@FETCH_STATUS = 0
                    BEGIN
			
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore,0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;	
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
				
                        UPDATE  syCreditSummary
                        SET     FinalScore = @FinalScore
                               ,CurrentScore = @CurrentScore
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId
                                AND ReqId = @reqid;

			--Average calculation
		
			-- Term Average
                        SET @TermAverageCount = (
                                                  SELECT    COUNT(*)
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = @StuEnrollId
                                                            AND TermId = @TermId
                                                            AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                                SELECT  SUM(FinalScore)
                                                FROM    syCreditSummary
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND TermId = @TermId
                                                        AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
                        SET @cumAveragecount = (
                                                 SELECT COUNT(*)
                                                 FROM   syCreditSummary
                                                 WHERE  StuEnrollId = @StuEnrollId
											--AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                               SELECT   SUM(FinalScore)
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
                        UPDATE  syCreditSummary
                        SET     Average = @TermAverage
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId; 
			
			--Update Cumulative GPA
                        UPDATE  syCreditSummary
                        SET     CumAverage = @CumAverage
                        WHERE   StuEnrollId = @StuEnrollId;
			

                        FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                            @GradeSystemDetailId,@FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,
                            @PrgVerId,@IsInGPA,@FinAidCredits; 
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

                DECLARE GetCreditsSummary_Cursor CURSOR
                FOR
                    SELECT DISTINCT
                            SE.StuEnrollId
                           ,T.TermId
                           ,T.TermDescrip
                           ,T.StartDate
                           ,R.ReqId
                           ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                           ,NULL AS FinalScore
                           ,RES.GrdSysDetailId AS GradeSystemDetailId
                           ,GSD.Grade AS FinalGrade
                           ,GSD.Grade AS CurrentGrade
                           ,R.Credits
                           ,NULL AS ClsSectionId
                           ,GSD.IsPass
                           ,GSD.IsCreditsAttempted
                           ,GSD.IsCreditsEarned
                           ,GSD.GPA
                           ,GSD.IsInGPA
                           ,SE.PrgVerId
                           ,GSD.IsInGPA
                           ,R.FinAidCredits AS FinAidCredits
                    FROM    arStuEnrollments SE
                    INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
                    INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                    INNER JOIN arTerm T ON RES.TermId = T.TermId
                    WHERE   SE.StuEnrollId NOT IN ( SELECT DISTINCT
                                                            StuEnrollId
                                                    FROM    syCreditSummary )
                            AND SE.StuEnrollId = @StuEnrollId;

                SET @varGradeRounding = (
                                          SELECT    Value
                                          FROM      syConfigAppSetValues
                                          WHERE     SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                    @GradeSystemDetailId,@FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,
                    @PrgVerId,@IsInGPA,@FinAidCredits; 
                WHILE @@FETCH_STATUS = 0
                    BEGIN
			
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore,0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;	
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
				
                        UPDATE  syCreditSummary
                        SET     FinalScore = @FinalScore
                               ,CurrentScore = @CurrentScore
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId
                                AND ReqId = @reqid;

			--Average calculation
                        SET @TermAverageCount = (
                                                  SELECT    COUNT(*)
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = @StuEnrollId
                                                            AND TermId = @TermId
                                                            AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                                SELECT  SUM(FinalScore)
                                                FROM    syCreditSummary
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND TermId = @TermId
                                                        AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
                        SET @cumAveragecount = (
                                                 SELECT COUNT(*)
                                                 FROM   syCreditSummary
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                               SELECT   SUM(FinalScore)
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
                        UPDATE  syCreditSummary
                        SET     Average = @TermAverage
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId; 
			
			--Update Cumulative GPA
                        UPDATE  syCreditSummary
                        SET     CumAverage = @CumAverage
                        WHERE   StuEnrollId = @StuEnrollId;
			

                        FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                            @GradeSystemDetailId,@FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,
                            @PrgVerId,@IsInGPA,@FinAidCredits;  
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

---- PRINT ''@CurrentBal_Compute'', 
---- PRINT @CurrentBal_Compute
		
            END;

        SET @TermAverageCount = (
                                  SELECT    COUNT(*)
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                );
        SET @termAverageSum = (
                                SELECT  SUM(FinalScore)
                                FROM    syCreditSummary
                                WHERE   StuEnrollId = @StuEnrollId
                                        AND FinalScore IS NOT NULL
                              );
        IF @TermAverageCount >= 1
            BEGIN
                SET @TermAverage = @termAverageSum / @TermAverageCount; 
            END;
			
		-- Cumulative Average
        SET @cumAveragecount = (
                                 SELECT COUNT(*)
                                 FROM   syCreditSummary
                                 WHERE  StuEnrollId = @StuEnrollId
                                        AND FinalScore IS NOT NULL
                               );
        SET @cumAverageSum = (
                               SELECT   SUM(FinalScore)
                               FROM     syCreditSummary
                               WHERE    StuEnrollId = @StuEnrollId
                                        AND FinalScore IS NOT NULL
                             );
								
        IF @cumAveragecount >= 1
            BEGIN
                SET @CumAverage = @cumAverageSum / @cumAveragecount;
            END;    
			
        --If Clock Hour / Numeric - use New Average Calculation
        IF (
           SELECT TOP 1 P.ACId
           FROM   dbo.arStuEnrollments E
           JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
           JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
           WHERE  E.StuEnrollId = @StuEnrollId
           ) = 5
           AND @GradesFormat = ''numeric''
            BEGIN
                EXEC dbo.USP_GPACalculator @EnrollmentId = @StuEnrollId
                                          ,@StudentGPA = @CumAverage OUTPUT;
            END;
	---- PRINT @cumAveragecount 
	---- PRINT @CumAverageSum
	---- PRINT @CumAverage
	---- PRINT @UnitTypeId;
	---- PRINT @TrackSapAttendance;
	---- PRINT @displayHours;
    END;
--=================================================================================================
-- END  --  Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId
--=================================================================================================	

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[TR_UpdateLDA_SCA] on [dbo].[arStudentClockAttendance]'
GO
IF OBJECT_ID(N'[dbo].[TR_UpdateLDA_SCA]', 'TR') IS NULL
EXEC sp_executesql N'
CREATE TRIGGER [dbo].[TR_UpdateLDA_SCA]
ON [dbo].[arStudentClockAttendance]
AFTER INSERT, UPDATE
AS
DECLARE @INS INT
       ,@DEL INT
       ,@InsertedStuEnrollId UNIQUEIDENTIFIER;

DECLARE @LDA DATETIME;
SELECT @INS = COUNT(*)
FROM   INSERTED;
SELECT @DEL = COUNT(*)
FROM   DELETED;

SELECT @InsertedStuEnrollId = Inserted.StuEnrollId
FROM   Inserted;

--If update and the record date is changed or fresh insert then refresh the lda
IF (
   @INS > 0
   AND @DEL > 0
   AND UPDATE(RecordDate)
   )
   OR @INS > 0
    BEGIN

        SET @LDA = (
                   SELECT     MAX(sca.RecordDate) AS LDA
                   FROM       arStudentClockAttendance sca
                   INNER JOIN Inserted i ON i.StuEnrollId = sca.StuEnrollId
                                            AND (
                                                sca.ActualHours > 0
                                                AND sca.ActualHours <> 99.00
                                                AND sca.ActualHours <> 999.00
                                                AND sca.ActualHours <> 9999.00
                                                )
                   );



        UPDATE se
        SET    LDA = @LDA
        FROM   dbo.arStuEnrollments se
        WHERE  se.StuEnrollId = @InsertedStuEnrollId;

    END;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[TR_UpdateLDA_CSA] on [dbo].[atClsSectAttendance]'
GO
IF OBJECT_ID(N'[dbo].[TR_UpdateLDA_CSA]', 'TR') IS NULL
EXEC sp_executesql N'
CREATE TRIGGER [dbo].[TR_UpdateLDA_CSA]
ON [dbo].[atClsSectAttendance]
AFTER INSERT, UPDATE
AS
DECLARE @INS INT
       ,@DEL INT
       ,@InsertedStuEnrollId UNIQUEIDENTIFIER;

DECLARE @LDA DATETIME;
SELECT @INS = COUNT(*)
FROM   INSERTED;
SELECT @DEL = COUNT(*)
FROM   DELETED;

SELECT @InsertedStuEnrollId = Inserted.StuEnrollId
FROM   Inserted;

--If update and the record date is changed or fresh insert then refresh the lda
IF (
   @INS > 0
   AND @DEL > 0
   AND UPDATE(MeetDate)
   )
   OR @INS > 0
    BEGIN

        SET @LDA = (
                   SELECT     MAX(csa.MeetDate) AS LDA
                   FROM       atClsSectAttendance csa
                   INNER JOIN Inserted i ON i.StuEnrollId = csa.StuEnrollId
                                            AND csa.Actual > 0
                                            AND (
                                                csa.Actual <> 99.00
                                                AND csa.Actual <> 999.00
                                                AND csa.Actual <> 9999.00
                                                )
                   );


        UPDATE se
        SET    LDA = @LDA
        FROM   dbo.arStuEnrollments se
        WHERE  se.StuEnrollId = @InsertedStuEnrollId;

    END;


'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering trigger [dbo].[arPrgVersions_Audit_Update] on [dbo].[arPrgVersions]'
GO


ALTER TRIGGER [dbo].[arPrgVersions_Audit_Update] ON [dbo].[arPrgVersions]
    FOR UPDATE
AS
    SET NOCOUNT ON;

    DECLARE @AuditHistId AS UNIQUEIDENTIFIER; 
    DECLARE @EventRows AS INT; 
    DECLARE @EventDate AS DATETIME; 
    DECLARE @UserName AS VARCHAR(50);
    SET @AuditHistId = NEWID(); 
    SET @EventRows = (
                       SELECT   COUNT(*)
                       FROM     Inserted
                     ); 
    SET @EventDate = (
                       SELECT TOP 1
                                ModDate
                       FROM     Inserted
                     ); 
    SET @UserName = (
                      SELECT TOP 1
                                ModUser
                      FROM      Inserted
                    ); 
    IF @EventRows > 0
        BEGIN 
            EXEC fmAuditHistAdd @AuditHistId,'arPrgVersions','U',@EventRows,@EventDate,@UserName; 
            BEGIN 
                IF UPDATE(ProgId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'ProgId'
                                   ,CONVERT(VARCHAR(8000),Old.ProgId,121)
                                   ,CONVERT(VARCHAR(8000),New.ProgId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.ProgId <> New.ProgId
                                    OR (
                                         Old.ProgId IS NULL
                                         AND New.ProgId IS NOT NULL
                                       )
                                    OR (
                                         New.ProgId IS NULL
                                         AND Old.ProgId IS NOT NULL
                                       ); 
                IF UPDATE(PrgVerCode)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'PrgVerCode'
                                   ,CONVERT(VARCHAR(8000),Old.PrgVerCode,121)
                                   ,CONVERT(VARCHAR(8000),New.PrgVerCode,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.PrgVerCode <> New.PrgVerCode
                                    OR (
                                         Old.PrgVerCode IS NULL
                                         AND New.PrgVerCode IS NOT NULL
                                       )
                                    OR (
                                         New.PrgVerCode IS NULL
                                         AND Old.PrgVerCode IS NOT NULL
                                       ); 
                IF UPDATE(StatusId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'StatusId'
                                   ,CONVERT(VARCHAR(8000),Old.StatusId,121)
                                   ,CONVERT(VARCHAR(8000),New.StatusId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.StatusId <> New.StatusId
                                    OR (
                                         Old.StatusId IS NULL
                                         AND New.StatusId IS NOT NULL
                                       )
                                    OR (
                                         New.StatusId IS NULL
                                         AND Old.StatusId IS NOT NULL
                                       ); 
                IF UPDATE(CampGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'CampGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.CampGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.CampGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.CampGrpId <> New.CampGrpId
                                    OR (
                                         Old.CampGrpId IS NULL
                                         AND New.CampGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.CampGrpId IS NULL
                                         AND Old.CampGrpId IS NOT NULL
                                       ); 
                IF UPDATE(PrgGrpId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'PrgGrpId'
                                   ,CONVERT(VARCHAR(8000),Old.PrgGrpId,121)
                                   ,CONVERT(VARCHAR(8000),New.PrgGrpId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.PrgGrpId <> New.PrgGrpId
                                    OR (
                                         Old.PrgGrpId IS NULL
                                         AND New.PrgGrpId IS NOT NULL
                                       )
                                    OR (
                                         New.PrgGrpId IS NULL
                                         AND Old.PrgGrpId IS NOT NULL
                                       ); 
                IF UPDATE(PrgVerDescrip)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'PrgVerDescrip'
                                   ,CONVERT(VARCHAR(8000),Old.PrgVerDescrip,121)
                                   ,CONVERT(VARCHAR(8000),New.PrgVerDescrip,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.PrgVerDescrip <> New.PrgVerDescrip
                                    OR (
                                         Old.PrgVerDescrip IS NULL
                                         AND New.PrgVerDescrip IS NOT NULL
                                       )
                                    OR (
                                         New.PrgVerDescrip IS NULL
                                         AND Old.PrgVerDescrip IS NOT NULL
                                       ); 
                IF UPDATE(DegreeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'DegreeId'
                                   ,CONVERT(VARCHAR(8000),Old.DegreeId,121)
                                   ,CONVERT(VARCHAR(8000),New.DegreeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.DegreeId <> New.DegreeId
                                    OR (
                                         Old.DegreeId IS NULL
                                         AND New.DegreeId IS NOT NULL
                                       )
                                    OR (
                                         New.DegreeId IS NULL
                                         AND Old.DegreeId IS NOT NULL
                                       ); 
                IF UPDATE(SAPId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'SAPId'
                                   ,CONVERT(VARCHAR(8000),Old.SAPId,121)
                                   ,CONVERT(VARCHAR(8000),New.SAPId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.SAPId <> New.SAPId
                                    OR (
                                         Old.SAPId IS NULL
                                         AND New.SAPId IS NOT NULL
                                       )
                                    OR (
                                         New.SAPId IS NULL
                                         AND Old.SAPId IS NOT NULL
                                       ); 
                IF UPDATE(TestingModelId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'TestingModelId'
                                   ,CONVERT(VARCHAR(8000),Old.TestingModelId,121)
                                   ,CONVERT(VARCHAR(8000),New.TestingModelId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.TestingModelId <> New.TestingModelId
                                    OR (
                                         Old.TestingModelId IS NULL
                                         AND New.TestingModelId IS NOT NULL
                                       )
                                    OR (
                                         New.TestingModelId IS NULL
                                         AND Old.TestingModelId IS NOT NULL
                                       ); 
                IF UPDATE(Weeks)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'Weeks'
                                   ,CONVERT(VARCHAR(8000),Old.Weeks,121)
                                   ,CONVERT(VARCHAR(8000),New.Weeks,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.Weeks <> New.Weeks
                                    OR (
                                         Old.Weeks IS NULL
                                         AND New.Weeks IS NOT NULL
                                       )
                                    OR (
                                         New.Weeks IS NULL
                                         AND Old.Weeks IS NOT NULL
                                       ); 
                IF UPDATE(Terms)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'Terms'
                                   ,CONVERT(VARCHAR(8000),Old.Terms,121)
                                   ,CONVERT(VARCHAR(8000),New.Terms,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.Terms <> New.Terms
                                    OR (
                                         Old.Terms IS NULL
                                         AND New.Terms IS NOT NULL
                                       )
                                    OR (
                                         New.Terms IS NULL
                                         AND Old.Terms IS NOT NULL
                                       ); 
                IF UPDATE(Hours)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'Hours'
                                   ,CONVERT(VARCHAR(8000),Old.Hours,121)
                                   ,CONVERT(VARCHAR(8000),New.Hours,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.Hours <> New.Hours
                                    OR (
                                         Old.Hours IS NULL
                                         AND New.Hours IS NOT NULL
                                       )
                                    OR (
                                         New.Hours IS NULL
                                         AND Old.Hours IS NOT NULL
                                       ); 
                IF UPDATE(Credits)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'Credits'
                                   ,CONVERT(VARCHAR(8000),Old.Credits,121)
                                   ,CONVERT(VARCHAR(8000),New.Credits,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.Credits <> New.Credits
                                    OR (
                                         Old.Credits IS NULL
                                         AND New.Credits IS NOT NULL
                                       )
                                    OR (
                                         New.Credits IS NULL
                                         AND Old.Credits IS NOT NULL
                                       ); 
                IF UPDATE(LTHalfTime)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'LTHalfTime'
                                   ,CONVERT(VARCHAR(8000),Old.LTHalfTime,121)
                                   ,CONVERT(VARCHAR(8000),New.LTHalfTime,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.LTHalfTime <> New.LTHalfTime
                                    OR (
                                         Old.LTHalfTime IS NULL
                                         AND New.LTHalfTime IS NOT NULL
                                       )
                                    OR (
                                         New.LTHalfTime IS NULL
                                         AND Old.LTHalfTime IS NOT NULL
                                       ); 
                IF UPDATE(HalfTime)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'HalfTime'
                                   ,CONVERT(VARCHAR(8000),Old.HalfTime,121)
                                   ,CONVERT(VARCHAR(8000),New.HalfTime,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.HalfTime <> New.HalfTime
                                    OR (
                                         Old.HalfTime IS NULL
                                         AND New.HalfTime IS NOT NULL
                                       )
                                    OR (
                                         New.HalfTime IS NULL
                                         AND Old.HalfTime IS NOT NULL
                                       ); 
                IF UPDATE(ThreeQuartTime)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'ThreeQuartTime'
                                   ,CONVERT(VARCHAR(8000),Old.ThreeQuartTime,121)
                                   ,CONVERT(VARCHAR(8000),New.ThreeQuartTime,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.ThreeQuartTime <> New.ThreeQuartTime
                                    OR (
                                         Old.ThreeQuartTime IS NULL
                                         AND New.ThreeQuartTime IS NOT NULL
                                       )
                                    OR (
                                         New.ThreeQuartTime IS NULL
                                         AND Old.ThreeQuartTime IS NOT NULL
                                       ); 
                IF UPDATE(FullTime)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'FullTime'
                                   ,CONVERT(VARCHAR(8000),Old.FullTime,121)
                                   ,CONVERT(VARCHAR(8000),New.FullTime,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.FullTime <> New.FullTime
                                    OR (
                                         Old.FullTime IS NULL
                                         AND New.FullTime IS NOT NULL
                                       )
                                    OR (
                                         New.FullTime IS NULL
                                         AND Old.FullTime IS NOT NULL
                                       ); 
                IF UPDATE(DeptId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'DeptId'
                                   ,CONVERT(VARCHAR(8000),Old.DeptId,121)
                                   ,CONVERT(VARCHAR(8000),New.DeptId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.DeptId <> New.DeptId
                                    OR (
                                         Old.DeptId IS NULL
                                         AND New.DeptId IS NOT NULL
                                       )
                                    OR (
                                         New.DeptId IS NULL
                                         AND Old.DeptId IS NOT NULL
                                       ); 
                IF UPDATE(GrdSystemId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'GrdSystemId'
                                   ,CONVERT(VARCHAR(8000),Old.GrdSystemId,121)
                                   ,CONVERT(VARCHAR(8000),New.GrdSystemId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.GrdSystemId <> New.GrdSystemId
                                    OR (
                                         Old.GrdSystemId IS NULL
                                         AND New.GrdSystemId IS NOT NULL
                                       )
                                    OR (
                                         New.GrdSystemId IS NULL
                                         AND Old.GrdSystemId IS NOT NULL
                                       ); 
                IF UPDATE([Weighted GPA])
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'Weighted GPA'
                                   ,CONVERT(VARCHAR(8000),Old.[Weighted GPA],121)
                                   ,CONVERT(VARCHAR(8000),New.[Weighted GPA],121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.[Weighted GPA] <> New.[Weighted GPA]
                                    OR (
                                         Old.[Weighted GPA] IS NULL
                                         AND New.[Weighted GPA] IS NOT NULL
                                       )
                                    OR (
                                         New.[Weighted GPA] IS NULL
                                         AND Old.[Weighted GPA] IS NOT NULL
                                       ); 
                IF UPDATE(BillingMethodId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'BillingMethodId'
                                   ,CONVERT(VARCHAR(8000),Old.BillingMethodId,121)
                                   ,CONVERT(VARCHAR(8000),New.BillingMethodId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.BillingMethodId <> New.BillingMethodId
                                    OR (
                                         Old.BillingMethodId IS NULL
                                         AND New.BillingMethodId IS NOT NULL
                                       )
                                    OR (
                                         New.BillingMethodId IS NULL
                                         AND Old.BillingMethodId IS NOT NULL
                                       ); 
                IF UPDATE(TuitionEarningId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'TuitionEarningId'
                                   ,CONVERT(VARCHAR(8000),Old.TuitionEarningId,121)
                                   ,CONVERT(VARCHAR(8000),New.TuitionEarningId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.TuitionEarningId <> New.TuitionEarningId
                                    OR (
                                         Old.TuitionEarningId IS NULL
                                         AND New.TuitionEarningId IS NOT NULL
                                       )
                                    OR (
                                         New.TuitionEarningId IS NULL
                                         AND Old.TuitionEarningId IS NOT NULL
                                       ); 
                IF UPDATE(AttendanceLevel)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'AttendanceLevel'
                                   ,CONVERT(VARCHAR(8000),Old.AttendanceLevel,121)
                                   ,CONVERT(VARCHAR(8000),New.AttendanceLevel,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.AttendanceLevel <> New.AttendanceLevel
                                    OR (
                                         Old.AttendanceLevel IS NULL
                                         AND New.AttendanceLevel IS NOT NULL
                                       )
                                    OR (
                                         New.AttendanceLevel IS NULL
                                         AND Old.AttendanceLevel IS NOT NULL
                                       ); 
                IF UPDATE(CustomAttendance)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'CustomAttendance'
                                   ,CONVERT(VARCHAR(8000),Old.CustomAttendance,121)
                                   ,CONVERT(VARCHAR(8000),New.CustomAttendance,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.CustomAttendance <> New.CustomAttendance
                                    OR (
                                         Old.CustomAttendance IS NULL
                                         AND New.CustomAttendance IS NOT NULL
                                       )
                                    OR (
                                         New.CustomAttendance IS NULL
                                         AND Old.CustomAttendance IS NOT NULL
                                       ); 
                IF UPDATE(ProgTypId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'ProgTypId'
                                   ,CONVERT(VARCHAR(8000),Old.ProgTypId,121)
                                   ,CONVERT(VARCHAR(8000),New.ProgTypId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.ProgTypId <> New.ProgTypId
                                    OR (
                                         Old.ProgTypId IS NULL
                                         AND New.ProgTypId IS NOT NULL
                                       )
                                    OR (
                                         New.ProgTypId IS NULL
                                         AND Old.ProgTypId IS NOT NULL
                                       ); 
                IF UPDATE(IsContinuingEd)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'IsContinuingEd'
                                   ,CONVERT(VARCHAR(8000),Old.IsContinuingEd,121)
                                   ,CONVERT(VARCHAR(8000),New.IsContinuingEd,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.IsContinuingEd <> New.IsContinuingEd
                                    OR (
                                         Old.IsContinuingEd IS NULL
                                         AND New.IsContinuingEd IS NOT NULL
                                       )
                                    OR (
                                         New.IsContinuingEd IS NULL
                                         AND Old.IsContinuingEd IS NOT NULL
                                       ); 
                IF UPDATE(UnitTypeId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'UnitTypeId'
                                   ,CONVERT(VARCHAR(8000),Old.UnitTypeId,121)
                                   ,CONVERT(VARCHAR(8000),New.UnitTypeId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.UnitTypeId <> New.UnitTypeId
                                    OR (
                                         Old.UnitTypeId IS NULL
                                         AND New.UnitTypeId IS NOT NULL
                                       )
                                    OR (
                                         New.UnitTypeId IS NULL
                                         AND Old.UnitTypeId IS NOT NULL
                                       ); 
                IF UPDATE(TrackTardies)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'TrackTardies'
                                   ,CONVERT(VARCHAR(8000),Old.TrackTardies,121)
                                   ,CONVERT(VARCHAR(8000),New.TrackTardies,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.TrackTardies <> New.TrackTardies
                                    OR (
                                         Old.TrackTardies IS NULL
                                         AND New.TrackTardies IS NOT NULL
                                       )
                                    OR (
                                         New.TrackTardies IS NULL
                                         AND Old.TrackTardies IS NOT NULL
                                       ); 
                IF UPDATE(TardiesMakingAbsence)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'TardiesMakingAbsence'
                                   ,CONVERT(VARCHAR(8000),Old.TardiesMakingAbsence,121)
                                   ,CONVERT(VARCHAR(8000),New.TardiesMakingAbsence,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.TardiesMakingAbsence <> New.TardiesMakingAbsence
                                    OR (
                                         Old.TardiesMakingAbsence IS NULL
                                         AND New.TardiesMakingAbsence IS NOT NULL
                                       )
                                    OR (
                                         New.TardiesMakingAbsence IS NULL
                                         AND Old.TardiesMakingAbsence IS NOT NULL
                                       ); 
                IF UPDATE(SchedMethodId)
                    INSERT  INTO syAuditHistDetail
                            (
                             AuditHistId
                            ,RowId
                            ,ColumnName
                            ,OldValue
                            ,NewValue
                            )
                            SELECT  @AuditHistId
                                   ,New.PrgVerId
                                   ,'SchedMethodId'
                                   ,CONVERT(VARCHAR(8000),Old.SchedMethodId,121)
                                   ,CONVERT(VARCHAR(8000),New.SchedMethodId,121)
                            FROM    Inserted New
                            FULL OUTER JOIN Deleted Old ON Old.PrgVerId = New.PrgVerId
                            WHERE   Old.SchedMethodId <> New.SchedMethodId
                                    OR (
                                         Old.SchedMethodId IS NULL
                                         AND New.SchedMethodId IS NOT NULL
                                       )
                                    OR (
                                         New.SchedMethodId IS NULL
                                         AND Old.SchedMethodId IS NOT NULL
                                       ); 
            END; 
        END;



    SET NOCOUNT OFF;

GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering trigger [dbo].[arStuEnrollments_Integration_Tracking] on [dbo].[arStuEnrollments]'
GO
--BEGIN	
--CREATE TABLE dbo.IntegrationEnrollmentTracking
--(
-- EnrollmentTrackingId UNIQUEIDENTIFIER CONSTRAINT [PK_IntegrationEnrollmentTracking_EnrollmentTrackingId] PRIMARY KEY CLUSTERED 
-- CONSTRAINT [DF_IntegrationEnrollmentTracking_EnrollmentTrackingId] DEFAULT NEWID(),
-- StuEnrollId UNIQUEIDENTIFIER CONSTRAINT[FK_IntegrationEnrollmentTracking_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY REFERENCES dbo.arStuEnrollments NOT NULL,
--  EnrollmentStatus VARCHAR(100),
-- ModDate DATETIME NOT NULL ,
-- ModUser NVARCHAR(100),
-- Processed BIT CONSTRAINT [DF_IntegrationEnrollmentTracking_Processed] DEFAULT 0 NOT NULL 	

--)
--END
--DROP TABLE dbo.IntegrationEnrollmentTracking
ALTER TRIGGER [dbo].[arStuEnrollments_Integration_Tracking]
ON [dbo].[arStuEnrollments]
AFTER UPDATE
AS
DECLARE @INS INT
       ,@DEL INT;

SELECT @INS = COUNT(*)
FROM   INSERTED;
SELECT @DEL = COUNT(*)
FROM   DELETED;

IF @INS > 0
   AND @DEL > 0
    BEGIN


        IF UPDATE(StatusCodeId)
           OR UPDATE(LDA)
            BEGIN
                IF EXISTS (
                          SELECT     1
                          FROM       INSERTED i
                          INNER JOIN DELETED d ON d.StuEnrollId = i.StuEnrollId
                          WHERE      ( i.StatusCodeId <> d.StatusCodeId )
                                     OR ( i.LDA <> d.LDA )
                          )
                    BEGIN
                        IF EXISTS (
                                  SELECT     1
                                  FROM       dbo.IntegrationEnrollmentTracking IET
                                  INNER JOIN inserted i ON i.StuEnrollId = IET.StuEnrollId
                                  )
                            BEGIN


                                UPDATE     IET
                                SET        EnrollmentStatus = ss.SysStatusDescrip
                                          ,ModDate = i.ModDate
                                          ,ModUser = i.ModUser
                                          ,Processed = 0
                                FROM       dbo.IntegrationEnrollmentTracking IET
                                INNER JOIN INSERTED i ON i.StuEnrollId = IET.StuEnrollId
                                INNER JOIN dbo.arStuEnrollments se ON i.StuEnrollId = se.StuEnrollId
                                INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                INNER JOIN dbo.sySysStatus ss ON ss.SysStatusId = sc.SysStatusId
                                INNER JOIN dbo.adLeads l ON l.LeadId = se.LeadId
                                WHERE IET.StuEnrollId = i.StuEnrollId AND l.AfaStudentId IS NOT NULL
                            END;

                        ELSE
                            BEGIN

                                INSERT INTO dbo.IntegrationEnrollmentTracking (
                                                                              EnrollmentTrackingId
                                                                             ,StuEnrollId
                                                                             ,EnrollmentStatus
                                                                             ,ModDate
                                                                             ,ModUser
                                                                             ,Processed
                                                                              )
                                            SELECT     NEWID()
                                                      ,i.StuEnrollId
                                                      ,ss.SysStatusDescrip
                                                      ,i.ModDate
                                                      ,i.ModUser
                                                      ,0
                                            FROM       INSERTED i
                                            INNER JOIN dbo.arStuEnrollments se ON i.StuEnrollId = se.StuEnrollId
                                            INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                            INNER JOIN dbo.sySysStatus ss ON ss.SysStatusId = sc.SysStatusId
											INNER JOIN dbo.adLeads l ON l.LeadId = se.LeadId
											WHERE l.AfaStudentId IS NOT NULL

                            END;


                    END;

            END;



    END;
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
BEGIN
    DECLARE @Error AS INTEGER;

    SET @Error = 0;

    BEGIN TRANSACTION IdentityFix;
    BEGIN TRY


        IF NOT EXISTS
        (
            SELECT OBJECT_NAME(object_id) AS TABLENAME,
                   name AS COLUMNNAME,
                   seed_value,
                   increment_value,
                   last_value,
                   is_not_for_replication
            FROM sys.identity_columns
            WHERE OBJECT_NAME(object_id) = 'syConfigAppSettings'
        )
        BEGIN

            SELECT *
            INTO #syConfigAppSettings
            FROM syConfigAppSettings;

            ALTER TABLE syConfigAppSettings
            DROP CONSTRAINT PK_syConfigAppSettings_SettingId;

            ALTER TABLE syConfigAppSettings DROP COLUMN SettingId;

            ALTER TABLE syConfigAppSettings
            ADD SettingId INT IDENTITY(1, 1) NOT NULL;

            SET IDENTITY_INSERT dbo.syConfigAppSettings ON;

            DELETE FROM syConfigAppSettings;

            INSERT INTO syConfigAppSettings
            (
                KeyName,
                Description,
                ModUser,
                ModDate,
                CampusSpecific,
                ExtraConfirmation,
                SettingId
            )
            SELECT KeyName,
                   Description,
                   ModUser,
                   ModDate,
                   CampusSpecific,
                   ExtraConfirmation,
                   SettingId
            FROM #syConfigAppSettings;


            SET IDENTITY_INSERT dbo.syConfigAppSettings OFF;

            DROP TABLE #syConfigAppSettings;


            ALTER TABLE [dbo].[syConfigAppSettings]
            ADD CONSTRAINT [PK_syConfigAppSettings_SettingId]
                PRIMARY KEY CLUSTERED ([SettingId] ASC)
                WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF,
                      ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON
                     ) ON [PRIMARY];


					  PRINT 'identity added to syConfigAppSettings';
        END;

		IF NOT EXISTS
		(
			SELECT *
			FROM sys.default_constraints
			WHERE name = 'DF_syConfigAppSetValues_ValueId'
		)
		BEGIN
			ALTER TABLE [dbo].[syConfigAppSetValues]
			ADD CONSTRAINT [DF_syConfigAppSetValues_ValueId]
				DEFAULT (NEWSEQUENTIALID()) FOR [ValueId];
		END;

        IF (@@ERROR > 0)
        BEGIN
            SET @Error = @@ERROR;
        END;

    END TRY
    BEGIN CATCH
        DECLARE @msg NVARCHAR(MAX);
        DECLARE @severity INT;
        DECLARE @state INT;
        SELECT @msg = ERROR_MESSAGE(),
               @severity = ERROR_SEVERITY(),
               @state = ERROR_STATE();
        RAISERROR(@msg, @severity, @state);
        SET @Error = 1;
    END CATCH;

    IF (@Error = 0)
    BEGIN
        COMMIT TRANSACTION IdentityFix;

        PRINT 'IdentityFix script for syConfigAppSettings was executed';
    END;
    ELSE
    BEGIN
        ROLLBACK TRANSACTION IdentityFix;
        PRINT 'Unable to add identity to syConfigAppSettings';
    END;
END;