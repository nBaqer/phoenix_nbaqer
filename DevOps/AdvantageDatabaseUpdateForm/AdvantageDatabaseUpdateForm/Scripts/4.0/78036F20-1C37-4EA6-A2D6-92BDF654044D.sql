/*
Run this script on:

        CORDERO-LT\SQLDEV2017.TricociLive    -  This database will be modified

to synchronize it with a database with the schema represented by:

        \\cordero-lt\c$\_Git\Advantage\Databases\Advantage\Source\

You are recommended to back up your database before running this script

Script created by SQL Compare version 12.0.33.3389 from Red Gate Software Ltd at 5/9/2019 5:08:35 PM

*/
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL Serializable
GO
BEGIN TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[adLeads]'
GO
IF EXISTS (SELECT 1 FROM sys.columns WHERE name = N'LicensureWrittenAllParts' AND object_id = OBJECT_ID(N'[dbo].[adLeads]', 'U') AND default_object_id = OBJECT_ID(N'[dbo].[DF_adLeads_LicensureWrittenAllParts]', 'D'))
ALTER TABLE [dbo].[adLeads] DROP CONSTRAINT [DF_adLeads_LicensureWrittenAllParts]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping constraints from [dbo].[adLeads]'
GO
IF EXISTS (SELECT 1 FROM sys.columns WHERE name = N'LicensurePassedAllParts' AND object_id = OBJECT_ID(N'[dbo].[adLeads]', 'U') AND default_object_id = OBJECT_ID(N'[dbo].[DF_adLeads_LicensurePassedAllParts]', 'D'))
ALTER TABLE [dbo].[adLeads] DROP CONSTRAINT [DF_adLeads_LicensurePassedAllParts]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping trigger [dbo].[arStuEnrollments_Integration_Tracking] from [dbo].[arStuEnrollments]'
GO
IF OBJECT_ID(N'[dbo].[arStuEnrollments_Integration_Tracking]', 'TR') IS NOT NULL
DROP TRIGGER [dbo].[arStuEnrollments_Integration_Tracking]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_TitleIV_QualitativeAndQuantitative]'
GO
IF OBJECT_ID(N'[dbo].[USP_TitleIV_QualitativeAndQuantitative]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_TitleIV_QualitativeAndQuantitative]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_PR_Sub2_Enrollment_Summary]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub2_Enrollment_Summary]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_PR_Sub2_Enrollment_Summary]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId]'
GO
IF OBJECT_ID(N'[dbo].[Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[UDF_GetEnrollmentStatusAtGivenDate]'
GO
IF EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UDF_GetEnrollmentStatusAtGivenDate]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
DROP FUNCTION [dbo].[UDF_GetEnrollmentStatusAtGivenDate]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Dropping [dbo].[USP_GPACalculator]'
GO
IF OBJECT_ID(N'[dbo].[USP_GPACalculator]', 'P') IS NOT NULL
DROP PROCEDURE [dbo].[USP_GPACalculator]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[adLeads]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[adLeads]', N'LicensureWrittenAllParts') IS NOT NULL
ALTER TABLE [dbo].[adLeads] DROP COLUMN [LicensureWrittenAllParts]
IF COL_LENGTH(N'[dbo].[adLeads]', N'LicensureLastPartWrittenOn') IS NOT NULL
ALTER TABLE [dbo].[adLeads] DROP COLUMN [LicensureLastPartWrittenOn]
IF COL_LENGTH(N'[dbo].[adLeads]', N'LicensurePassedAllParts') IS NOT NULL
ALTER TABLE [dbo].[adLeads] DROP COLUMN [LicensurePassedAllParts]
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[arFASAPChkResults]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[arFASAPChkResults] ALTER COLUMN [HrsAttended] [decimal] (19, 2) NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[arFASAPChkResults] ALTER COLUMN [HrsEarned] [decimal] (19, 2) NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[arSAPChkResults]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[arSAPChkResults] ALTER COLUMN [HrsAttended] [decimal] (19, 2) NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
ALTER TABLE [dbo].[arSAPChkResults] ALTER COLUMN [HrsEarned] [decimal] (19, 2) NULL
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Altering [dbo].[arStuEnrollments]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
IF COL_LENGTH(N'[dbo].[arStuEnrollments]', N'LicensureLastPartWrittenOn') IS NULL
ALTER TABLE [dbo].[arStuEnrollments] ADD[LicensureLastPartWrittenOn] [datetime] NULL
IF COL_LENGTH(N'[dbo].[arStuEnrollments]', N'LicensureWrittenAllParts') IS NULL
ALTER TABLE [dbo].[arStuEnrollments] ADD[LicensureWrittenAllParts] [bit] NOT NULL CONSTRAINT [DF_arStuEnrollments_LicensureWrittenAllParts] DEFAULT ((0))
IF COL_LENGTH(N'[dbo].[arStuEnrollments]', N'LicensurePassedAllParts') IS NULL
ALTER TABLE [dbo].[arStuEnrollments] ADD[LicensurePassedAllParts] [bit] NOT NULL CONSTRAINT [DF_arStuEnrollments_LicensurePassedAllParts] DEFAULT ((0))
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudent]'
GO
IF OBJECT_ID(N'[dbo].[arStudent]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudent]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[UDF_GetEnrollmentStatusAtGivenDate]'
GO
IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[UDF_GetEnrollmentStatusAtGivenDate]') AND (type = 'IF' OR type = 'FN' OR type = 'TF'))
EXEC sp_executesql N'--------------------------------------------------------------------------------------------------------------------------------------------------------------------
--Function to return the status of an enrollment at a specified date
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE FUNCTION [dbo].[UDF_GetEnrollmentStatusAtGivenDate]
(
    @StuEnrollId UNIQUEIDENTIFIER,
    @ReportDate DATETIME
)
RETURNS VARCHAR(50)
AS
BEGIN
    DECLARE @ReturnValue VARCHAR(50);
    DECLARE @CurrentStatus VARCHAR(50);
    DECLARE @StartDate DATETIME;
    DECLARE @LastDateOfChange DATETIME;
    DECLARE @LastNewStatus VARCHAR(50);

    SET @ReturnValue = '''';

    SELECT @CurrentStatus = sc.StatusCodeDescrip,
           @StartDate = se.StartDate
    FROM dbo.arStuEnrollments se
        INNER JOIN dbo.syStatusCodes sc
            ON sc.StatusCodeId = se.StatusCodeId
    WHERE se.StuEnrollId = @StuEnrollId;

    SET @LastDateOfChange =
    (
        SELECT MAX(DateOfChange)
        FROM dbo.syStudentStatusChanges
        WHERE StuEnrollId = @StuEnrollId
    );

    SET @LastNewStatus =
    (
        SELECT TOP 1
               sc.StatusCodeDescrip
        FROM dbo.syStudentStatusChanges ssc
            INNER JOIN dbo.syStatusCodes sc
                ON sc.StatusCodeId = ssc.NewStatusId
        WHERE ssc.StuEnrollId = @StuEnrollId
        ORDER BY ssc.DateOfChange DESC,
                 ssc.ModDate DESC
    );


    --If there are no status changes for the enrollment and the given date is >= enrollment start date we can simply return the current status. 
    IF NOT EXISTS
    (
        SELECT 1
        FROM dbo.syStudentStatusChanges
        WHERE StuEnrollId = @StuEnrollId
    )
    BEGIN
        IF @ReportDate >= @StartDate
        BEGIN
            SET @ReturnValue = @CurrentStatus;
        END;
    END;
    ELSE
    BEGIN
        --If the report date > last date of change for the enrollment then we can simply return the last new status (should be same as current status)
        IF @ReportDate > @LastDateOfChange
           AND @ReportDate >= @StartDate
        BEGIN
            SET @ReturnValue = @LastNewStatus;
        END;
        ELSE
        BEGIN
            --If the report date is the same as a date of change then we can simply return the new status for that date of change.
            --Use the FORMAT function just in case the DateOfChange field in the database has a time component on it such as 2018-10-19 05:00:00.000
            IF EXISTS
            (
                SELECT 1
                FROM dbo.syStudentStatusChanges
                WHERE StuEnrollId = @StuEnrollId
                      AND CONVERT(VARCHAR, DateOfChange, 101) = CONVERT(VARCHAR, @ReportDate, 101)
            )
            BEGIN
                SET @ReturnValue =
                (
                    SELECT TOP 1
                           sc.StatusCodeDescrip
                    FROM dbo.syStudentStatusChanges ssc
                        INNER JOIN dbo.syStatusCodes sc
                            ON sc.StatusCodeId = ssc.NewStatusId
                    WHERE ssc.StuEnrollId = @StuEnrollId
                          AND CONVERT(VARCHAR, DateOfChange, 101) = CONVERT(VARCHAR, @ReportDate, 101)
                    ORDER BY ssc.DateOfChange DESC,
                             ssc.ModDate DESC
                );

            END;
            ELSE
            BEGIN
                --At this point we can get the last change record with DateOfChange that is less than the report date
                SET @ReturnValue =
                (
                    SELECT TOP 1
                           sc.StatusCodeDescrip
                    FROM dbo.syStudentStatusChanges ssc
                        INNER JOIN dbo.syStatusCodes sc
                            ON sc.StatusCodeId = ssc.NewStatusId
                    WHERE ssc.StuEnrollId = @StuEnrollId
                          AND ssc.DateOfChange < @ReportDate
                    ORDER BY ssc.DateOfChange DESC,
                             ssc.ModDate DESC
                );
            END;

        END;

    END;

    RETURN ISNULL(@ReturnValue, '''');
END;



'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudAddresses]'
GO
IF OBJECT_ID(N'[dbo].[arStudAddresses]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudAddresses]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[arStudentPhone]'
GO
IF OBJECT_ID(N'[dbo].[arStudentPhone]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[arStudentPhone]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_GPACalculator]'
GO
IF OBJECT_ID(N'[dbo].[USP_GPACalculator]', 'P') IS NULL
EXEC sp_executesql N'-- =============================================
-- Author:		FAME Inc.
-- Create date: 11/1/2018
-- Description:	Calculated Student GPA Based on a set of parameters - Numeric ( Weighted & Unweighted currently implemented)
--Referenced in :
--[Usp_PR_Sub2_Enrollment_Summary]
--[Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId]
--[USP_TitleIV_QualitativeAndQuantitative]
--CreditSummaryDB.vb
--Core Web API - EnrollmentService.GetEnrollmentProgramSummary
-- =============================================
CREATE PROCEDURE [dbo].[USP_GPACalculator]
    (
        @EnrollmentId VARCHAR(1000) = NULL
       ,@BeginDate DATETIME = NULL
       ,@EndDate DATETIME = NULL
       ,@ClassId UNIQUEIDENTIFIER = NULL
       ,@StudentGPA AS DECIMAL(16, 2) OUTPUT
    )
AS
    BEGIN
        DECLARE @GPAResult AS DECIMAL(16, 2);
		DECLARE @UseWeightForGPA BIT = 1

        IF ( @EnrollmentId IS NULL )
            RETURN @GPAResult;

        SET @UseWeightForGPA = (
                               SELECT TOP 1 PV.DoCourseWeightOverallGPA
                               FROM   dbo.arStuEnrollments E
                               JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                               WHERE  E.StuEnrollId = @EnrollmentId
                               );

        -----------------Numeric GPA Grades Format------------------------

        SET @GPAResult = (
                         SELECT ( CASE WHEN @UseWeightForGPA = 1 THEN
                                           ROUND(( SUM(b.CourseWeight * b.WeightedCourseAverage / 100) / SUM(b.CourseWeight)) * 100, 2)
                                       ELSE AVG(b.UnweightedCourseAverage)
                                  END
                                ) AS WeightedGPA
                         FROM   (
                                SELECT   SUM(OurSingleClassGradeFactor) AS CourseFactor
                                        ,SUM(a.GradeWeight) AS GradeWeight
                                        ,SUM(a.Score) AS SumOfScores
                                        ,( SUM(OurSingleClassGradeFactor) / SUM(a.GradeWeight)) * 100 AS WeightedCourseAverage
                                        ,( AVG(a.Score)) AS UnweightedCourseAverage
                                        ,ClsSectionId
                                        ,a.CourseWeight
                                FROM     (
                                         SELECT ( c.Weight * a.Score / 100 ) AS OurSingleClassGradeFactor
                                               ,c.Weight AS GradeWeight
                                               ,a.Score
                                               ,a.ClsSectionId
                                               ,d.CourseWeight
                                               ,c.Descrip AS ClassDescrip
                                         FROM   (
                                                SELECT   StuEnrollId
                                                        ,ClsSectionId
                                                        ,a.InstrGrdBkWgtDetailId
                                                        ,AVG(Score) AS Score
                                                FROM     dbo.arGrdBkResults a
                                                JOIN     dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                                JOIN     dbo.arGrdComponentTypes f ON f.GrdComponentTypeId = c.GrdComponentTypeId
                                                WHERE    a.StuEnrollId = @EnrollmentId
                                                         --AND a.IsCompGraded = 1
                                                         AND a.Score IS NOT NULL
                                                         AND a.PostDate IS NOT NULL
                                                         AND (
                                                             @EndDate IS NULL
                                                             OR ( a.PostDate <= @EndDate )
                                                             )
                                                GROUP BY StuEnrollId
                                                        ,ClsSectionId
                                                        ,a.InstrGrdBkWgtDetailId
                                                ) a -- students grades
                                         JOIN   dbo.arClassSections b ON b.ClsSectionId = a.ClsSectionId
                                         JOIN   dbo.arGrdBkWgtDetails c ON c.InstrGrdBkWgtDetailId = a.InstrGrdBkWgtDetailId
                                         JOIN   dbo.arProgVerDef d ON d.ReqId = b.ReqId
                                         --ORDER BY f.Descrip, a.Score desc
                                         ) a
                                GROUP BY ClsSectionId
                                        ,a.CourseWeight

                                --ORDER BY CourseGPA DESC
                                ) b
                         );
        --END;
        SET @StudentGPA = @GPAResult;
    END;

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_PR_Sub2_Enrollment_Summary]'
GO
IF OBJECT_ID(N'[dbo].[Usp_PR_Sub2_Enrollment_Summary]', 'P') IS NULL
EXEC sp_executesql N'-- =========================================================================================================
-- Usp_PR_Sub2_Enrollment_Summary
-- =========================================================================================================
CREATE PROCEDURE [dbo].[Usp_PR_Sub2_Enrollment_Summary]
    @StuEnrollId VARCHAR(50)
   ,@TermId VARCHAR(4000) = NULL
   ,@TermStartDate DATETIME = NULL
   ,@TermStartDateModifier VARCHAR(10) = NULL
   ,@ShowFinanceCalculations BIT = 0
   -- by default hide cost and current balance
   ,@ShowWeeklySchedule BIT = 0
-- , @TrackSapAttendance VARCHAR(10) = NULL
-- , @displayhours VARCHAR(10) = NULL
AS
    BEGIN
        DECLARE @TrackSapAttendance VARCHAR(1000);
        DECLARE @displayHours AS VARCHAR(1000);
        DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
        DECLARE @GradesFormat AS VARCHAR(50);

        IF @TermStartDate IS NULL
           OR @TermStartDate = ''''
            BEGIN
                SET @TermStartDate = CONVERT(DATETIME, GETDATE(), 120);
            END;
        SET @StuEnrollCampusId = COALESCE((
                                          SELECT ASE.CampusId
                                          FROM   arStuEnrollments AS ASE
                                          WHERE  ASE.StuEnrollId = @StuEnrollId
                                          )
                                         ,NULL
                                         );
        SET @TrackSapAttendance = dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', @StuEnrollCampusId);
        IF ( @TrackSapAttendance IS NOT NULL )
            BEGIN
                SET @TrackSapAttendance = LOWER(LTRIM(RTRIM(@TrackSapAttendance)));
            END;
        --
        SET @displayHours = dbo.GetAppSettingValueByKeyName(''DisplayAttendanceUnitForProgressReportByClass'', @StuEnrollCampusId);
        IF ( @displayHours IS NOT NULL )
            BEGIN
                SET @displayHours = LOWER(LTRIM(RTRIM(@displayHours)));
            END;
        --
        SET @GradesFormat = dbo.GetAppSettingValueByKeyName(''GradesFormat'',@StuEnrollCampusId);
        IF ( @GradesFormat IS NOT NULL )
            BEGIN
                SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
            END;

        IF ( @TermId IS NOT NULL )
            BEGIN
                SET @TermStartDate = (
                                     SELECT TOP 1 EndDate
                                     FROM   arTerm
                                     WHERE  TermId = @TermId
                                     );
                IF @TermStartDate IS NULL
                    BEGIN
                        SET @TermStartDate = (
                                             SELECT TOP 1 StartDate
                                             FROM   arTerm
                                             WHERE  TermId = @TermId
                                             );
                    END;
                SET @TermStartDateModifier = ''<='';
            END;



        DECLARE @ReturnValue VARCHAR(100);
        SELECT     @ReturnValue = COALESCE(@ReturnValue, '''') + Descrip + '',  ''
        FROM       adLeadByLeadGroups t1
        INNER JOIN adLeadGroups t2 ON t1.LeadGrpId = t2.LeadGrpId
        WHERE      t1.StuEnrollId = @StuEnrollId;
        SET @ReturnValue = SUBSTRING(@ReturnValue, 1, LEN(@ReturnValue) - 1);


        /************  Logic to calculate GPA Starts Here *****************************/

        DECLARE @CoursesNotRepeated TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,TermId UNIQUEIDENTIFIER
               ,TermDescrip VARCHAR(100)
               ,ReqId UNIQUEIDENTIFIER
               ,ReqDescrip VARCHAR(100)
               ,ClsSectionId VARCHAR(50)
               ,CreditsEarned DECIMAL(18, 2)
               ,CreditsAttempted DECIMAL(18, 2)
               ,CurrentScore DECIMAL(18, 2)
               ,CurrentGrade VARCHAR(10)
               ,FinalScore DECIMAL(18, 2)
               ,FinalGrade VARCHAR(10)
               ,Completed BIT
               ,FinalGPA DECIMAL(18, 2)
               ,Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
               ,Count_WeightedAverage_Credits DECIMAL(18, 2)
               ,Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
               ,Count_SimpleAverage_Credits DECIMAL(18, 2)
               ,ModUser VARCHAR(50)
               ,ModDate DATETIME
               ,TermGPA_Simple DECIMAL(18, 2)
               ,TermGPA_Weighted DECIMAL(18, 2)
               ,coursecredits DECIMAL(18, 2)
               ,CumulativeGPA DECIMAL(18, 2)
               ,CumulativeGPA_Simple DECIMAL(18, 2)
               ,FACreditsEarned DECIMAL(18, 2)
               ,Average DECIMAL(18, 2)
               ,CumAverage DECIMAL(18, 2)
               ,TermStartDate DATETIME
               ,rownumber INT
            );

        INSERT INTO @CoursesNotRepeated
                    SELECT StuEnrollId
                          ,TermId
                          ,TermDescrip
                          ,ReqId
                          ,ReqDescrip
                          ,ClsSectionId
                          ,CreditsEarned
                          ,CreditsAttempted
                          ,CurrentScore
                          ,CurrentGrade
                          ,FinalScore
                          ,FinalGrade
                          ,Completed
                          ,FinalGPA
                          ,Product_WeightedAverage_Credits_GPA
                          ,Count_WeightedAverage_Credits
                          ,Product_SimpleAverage_Credits_GPA
                          ,Count_SimpleAverage_Credits
                          ,ModUser
                          ,ModDate
                          ,TermGPA_Simple
                          ,TermGPA_Weighted
                          ,coursecredits
                          ,CumulativeGPA
                          ,CumulativeGPA_Simple
                          ,FACreditsEarned
                          ,Average
                          ,CumAverage
                          ,TermStartDate
                          ,NULL AS rownumber
                    FROM   syCreditSummary
                    WHERE  StuEnrollId = @StuEnrollId
                           AND ReqId IN (
                                        SELECT ReqId
                                        FROM   (
                                               SELECT   ReqId
                                                       ,ReqDescrip
                                                       ,COUNT(*) AS counter
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                               GROUP BY ReqId
                                                       ,ReqDescrip
                                               HAVING   COUNT(*) = 1
                                               ) dt
                                        );

        DECLARE @GradeCourseRepetitionsMethod VARCHAR(50);
        SET @GradeCourseRepetitionsMethod = (
                                            SELECT     Value
                                            FROM       dbo.syConfigAppSetValues t1
                                            INNER JOIN dbo.syConfigAppSettings t2 ON t1.SettingId = t2.SettingId
                                                                                     AND t2.KeyName = ''GradeCourseRepetitionsMethod''
                                            );

        IF LOWER(@GradeCourseRepetitionsMethod) = ''latest''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT   dt2.StuEnrollId
                                    ,dt2.TermId
                                    ,dt2.TermDescrip
                                    ,dt2.ReqId
                                    ,dt2.ReqDescrip
                                    ,dt2.ClsSectionId
                                    ,dt2.CreditsEarned
                                    ,dt2.CreditsAttempted
                                    ,dt2.CurrentScore
                                    ,dt2.CurrentGrade
                                    ,dt2.FinalScore
                                    ,dt2.FinalGrade
                                    ,dt2.Completed
                                    ,dt2.FinalGPA
                                    ,dt2.Product_WeightedAverage_Credits_GPA
                                    ,dt2.Count_WeightedAverage_Credits
                                    ,dt2.Product_SimpleAverage_Credits_GPA
                                    ,dt2.Count_SimpleAverage_Credits
                                    ,dt2.ModUser
                                    ,dt2.ModDate
                                    ,dt2.TermGPA_Simple
                                    ,dt2.TermGPA_Weighted
                                    ,dt2.coursecredits
                                    ,dt2.CumulativeGPA
                                    ,dt2.CumulativeGPA_Simple
                                    ,dt2.FACreditsEarned
                                    ,dt2.Average
                                    ,dt2.CumAverage
                                    ,dt2.TermStartDate
                                    ,dt2.RowNumber
                            FROM     (
                                     SELECT StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                                ORDER BY TermStartDate DESC
                                                              ) AS RowNumber
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND (
                                                FinalScore IS NOT NULL
                                                OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN (
                                                         SELECT ReqId
                                                         FROM   (
                                                                SELECT   ReqId
                                                                        ,ReqDescrip
                                                                        ,COUNT(*) AS Counter
                                                                FROM     syCreditSummary
                                                                WHERE    StuEnrollId = @StuEnrollId
                                                                GROUP BY ReqId
                                                                        ,ReqDescrip
                                                                HAVING   COUNT(*) > 1
                                                                ) dt
                                                         )
                                     ) dt2
                            WHERE    RowNumber = 1
                            ORDER BY TermStartDate DESC;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''best''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                            FROM   (
                                   SELECT StuEnrollId
                                         ,TermId
                                         ,TermDescrip
                                         ,ReqId
                                         ,ReqDescrip
                                         ,ClsSectionId
                                         ,CreditsEarned
                                         ,CreditsAttempted
                                         ,CurrentScore
                                         ,CurrentGrade
                                         ,FinalScore
                                         ,FinalGrade
                                         ,Completed
                                         ,FinalGPA
                                         ,Product_WeightedAverage_Credits_GPA
                                         ,Count_WeightedAverage_Credits
                                         ,Product_SimpleAverage_Credits_GPA
                                         ,Count_SimpleAverage_Credits
                                         ,ModUser
                                         ,ModDate
                                         ,TermGPA_Simple
                                         ,TermGPA_Weighted
                                         ,coursecredits
                                         ,CumulativeGPA
                                         ,CumulativeGPA_Simple
                                         ,FACreditsEarned
                                         ,Average
                                         ,CumAverage
                                         ,TermStartDate
                                         ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                              ORDER BY FinalGPA DESC
                                                            ) AS RowNumber
                                   FROM   syCreditSummary
                                   WHERE  StuEnrollId = @StuEnrollId
                                          AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                              )
                                          AND ReqId IN (
                                                       SELECT ReqId
                                                       FROM   (
                                                              SELECT   ReqId
                                                                      ,ReqDescrip
                                                                      ,COUNT(*) AS Counter
                                                              FROM     syCreditSummary
                                                              WHERE    StuEnrollId = @StuEnrollId
                                                              GROUP BY ReqId
                                                                      ,ReqDescrip
                                                              HAVING   COUNT(*) > 1
                                                              ) dt
                                                       )
                                   ) dt2
                            WHERE  RowNumber = 1;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''average''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                            FROM   (
                                   SELECT StuEnrollId
                                         ,TermId
                                         ,TermDescrip
                                         ,ReqId
                                         ,ReqDescrip
                                         ,ClsSectionId
                                         ,CreditsEarned
                                         ,CreditsAttempted
                                         ,CurrentScore
                                         ,CurrentGrade
                                         ,FinalScore
                                         ,FinalGrade
                                         ,Completed
                                         ,FinalGPA
                                         ,Product_WeightedAverage_Credits_GPA
                                         ,Count_WeightedAverage_Credits
                                         ,Product_SimpleAverage_Credits_GPA
                                         ,Count_SimpleAverage_Credits
                                         ,ModUser
                                         ,ModDate
                                         ,TermGPA_Simple
                                         ,TermGPA_Weighted
                                         ,coursecredits
                                         ,CumulativeGPA
                                         ,CumulativeGPA_Simple
                                         ,FACreditsEarned
                                         ,Average
                                         ,CumAverage
                                         ,TermStartDate
                                         ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                              ORDER BY FinalGPA DESC
                                                            ) AS RowNumber
                                   FROM   syCreditSummary
                                   WHERE  StuEnrollId = @StuEnrollId
                                          AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                              )
                                          AND ReqId IN (
                                                       SELECT ReqId
                                                       FROM   (
                                                              SELECT   ReqId
                                                                      ,ReqDescrip
                                                                      ,COUNT(*) AS Counter
                                                              FROM     syCreditSummary
                                                              WHERE    StuEnrollId = @StuEnrollId
                                                              GROUP BY ReqId
                                                                      ,ReqDescrip
                                                              HAVING   COUNT(*) > 1
                                                              ) dt
                                                       )
                                   ) dt2;
            END;

        /*** Calculate Cumulative Simple GPA Starts Here ****/
        DECLARE @cumSimpleCourseCredits DECIMAL(18, 2)
               ,@cumSimple_GPA_Credits DECIMAL(18, 2)
               ,@cumSimpleGPA DECIMAL(18, 2);
        SET @cumSimpleGPA = 0;
        SET @cumSimpleCourseCredits = (
                                      SELECT COUNT(coursecredits)
                                      FROM   @CoursesNotRepeated
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND FinalGPA IS NOT NULL
                                      );
        SET @cumSimple_GPA_Credits = (
                                     SELECT SUM(FinalGPA)
                                     FROM   @CoursesNotRepeated
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalGPA IS NOT NULL
                                     );

        -- 3.7 SP2 Changes
        -- Include Equivalent Courses
        DECLARE @EquivCourse_SA_CC INT
               ,@EquivCourse_SA_GPA DECIMAL(18, 2);
        SET @EquivCourse_SA_CC = (
                                 SELECT ISNULL(COUNT(Credits), 0) AS Credits
                                 FROM   arReqs
                                 WHERE  ReqId IN (
                                                 SELECT     CE.EquivReqId
                                                 FROM       arCourseEquivalent CE
                                                 INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                 INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                 WHERE      StuEnrollId = @StuEnrollId
                                                            AND R.GrdSysDetailId IS NOT NULL
                                                 )
                                 );

        SET @EquivCourse_SA_GPA = (
                                  SELECT     SUM(ISNULL(GSD.GPA, 0.00))
                                  FROM       arCourseEquivalent CE
                                  INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                  INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                  INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                  WHERE      StuEnrollId = @StuEnrollId
                                             AND R.GrdSysDetailId IS NOT NULL
                                  );

        -- PRINT ''Equiv Course Simple ''
        -- PRINT @EquivCourse_SA_CC
        -- PRINT ''Equiv Course GPA Simple''
        -- PRINT @EquivCourse_SA_GPA

        -- PRINT ''Before Course Credits Simple''
        -- PRINT @cumSimpleCourseCredits
        -- PRINT ''Before WE GPA Simple''
        -- PRINT @cumSimple_GPA_Credits

        SET @cumSimpleCourseCredits = @cumSimpleCourseCredits; -- + ISNULL(@EquivCourse_SA_CC,0.00);
        SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits; -- + ISNULL(@EquivCourse_SA_GPA,0.00);

        IF @cumSimpleCourseCredits >= 1
            BEGIN
                SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
            END;

        -- PRINT ''After Course Credits Simple''
        -- PRINT @cumSimpleCourseCredits
        -- PRINT ''After WE GPA Simple''
        -- PRINT @cumSimple_GPA_Credits
        -- PRINT @cumSimpleGPA

        /*** Calculate Cumulative Simple GPA Ends Here ****/

        /**** Calculate Cumulative GPA Weighted *********************/
        DECLARE @cumCourseCredits DECIMAL(18, 2)
               ,@cumWeighted_GPA_Credits DECIMAL(18, 2)
               ,@cumWeightedGPA DECIMAL(18, 2);
        SET @cumWeightedGPA = 0;
        SET @cumCourseCredits = (
                                SELECT SUM(coursecredits)
                                FROM   @CoursesNotRepeated
                                WHERE  StuEnrollId = @StuEnrollId
                                       AND FinalGPA IS NOT NULL
                                );
        SET @cumWeighted_GPA_Credits = (
                                       SELECT SUM(coursecredits * FinalGPA)
                                       FROM   @CoursesNotRepeated
                                       WHERE  StuEnrollId = @StuEnrollId
                                              AND FinalGPA IS NOT NULL
                                       );


        DECLARE @doesThisCourseHaveAEquivalentCourse INT;

        DECLARE @EquivCourse_WGPA_CC1 INT
               ,@EquivCourse_WGPA_GPA1 DECIMAL(18, 2);
        SET @EquivCourse_WGPA_CC1 = (
                                    SELECT SUM(ISNULL(Credits, 0)) AS Credits
                                    FROM   arReqs
                                    WHERE  ReqId IN (
                                                    SELECT     CE.EquivReqId
                                                    FROM       arCourseEquivalent CE
                                                    INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                    INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                    WHERE      StuEnrollId = @StuEnrollId
                                                               AND R.GrdSysDetailId IS NOT NULL
                                                    )
                                    );

        SET @EquivCourse_WGPA_GPA1 = (
                                     SELECT     SUM(GSD.GPA * R1.Credits)
                                     FROM       arCourseEquivalent CE
                                     INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                     INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                     INNER JOIN arGradeSystemDetails GSD ON R.GrdSysDetailId = GSD.GrdSysDetailId
                                     INNER JOIN arReqs R1 ON R1.ReqId = CE.ReqId
                                     WHERE      StuEnrollId = @StuEnrollId
                                                AND R.GrdSysDetailId IS NOT NULL
                                     );

        -- PRINT ''Equiv Course ''
        -- PRINT @EquivCourse_WGPA_CC1
        -- PRINT ''Equiv Course GPA''
        -- PRINT @EquivCourse_WGPA_GPA1

        -- PRINT ''Before Course Credits''
        -- PRINT @cumCourseCredits
        -- PRINT ''Before WE GPA''
        -- PRINT @cumWeighted_GPA_Credits


        SET @cumCourseCredits = @cumCourseCredits; -- + ISNULL(@EquivCourse_WGPA_CC1,0.00);
        SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits; -- + ISNULL(@EquivCourse_WGPA_GPA1,0.00);

        IF @cumCourseCredits >= 1
            BEGIN
                SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
            END;

        -- PRINT ''After Course Credits''
        -- PRINT @cumCourseCredits
        -- PRINT ''After WE GPA''
        -- PRINT @cumWeighted_GPA_Credits

        -- PRINT ''After Cal GPA''
        -- PRINT @cumWeightedGPA

        /**************** Cumulative GPA Ends Here ************/


        /************  Logic to calculate GPA Ends Here *****************************/



        DECLARE @SUMCreditsAttempted DECIMAL(18, 2);
        DECLARE @SUMCreditsEarned DECIMAL(18, 2);
        DECLARE @SUMFACreditsEarned DECIMAL(18, 2);
        SET @SUMCreditsAttempted = 0;
        SET @SUMCreditsEarned = 0;
        SET @SUMFACreditsEarned = 0;

        DECLARE @EquivCourse_SA_CC2 DECIMAL(18, 2);
        SET @EquivCourse_SA_CC2 = (
                                  SELECT ISNULL(SUM(Credits), 0.00) AS Credits
                                  FROM   arReqs
                                  WHERE  ReqId IN (
                                                  SELECT     CE.EquivReqId
                                                  FROM       arCourseEquivalent CE
                                                  INNER JOIN dbo.arClassSections CS ON CE.ReqId = CS.ReqId
                                                  INNER JOIN arResults R ON CS.ClsSectionId = R.TestId
                                                  WHERE      StuEnrollId = @StuEnrollId
                                                             AND R.GrdSysDetailId IS NOT NULL
                                                  )
                                  );

        SELECT @SUMCreditsAttempted = SUM(ISNULL(SCS.CreditsAttempted, 0))
              ,@SUMCreditsEarned = SUM(ISNULL(SCS.CreditsEarned, 0))
        FROM   dbo.syCreditSummary AS SCS
        WHERE  SCS.StuEnrollId = @StuEnrollId;

        --SET @SUMCreditsEarned = @SUMCreditsEarned + @EquivCourse_SA_CC2;  

        SET @SUMFACreditsEarned = (
                                  SELECT SUM(FACreditsEarned)
                                  FROM   syCreditSummary
                                  WHERE  StuEnrollId = @StuEnrollId
                                  );

        DECLARE @Scheduledhours DECIMAL(18, 2)
               ,@ActualPresentDays_ConvertTo_Hours DECIMAL(18, 2);
        DECLARE @ActualAbsentDays_ConvertTo_Hours DECIMAL(18, 2)
               ,@ActualTardyDays_ConvertTo_Hours DECIMAL(18, 2);
        SET @Scheduledhours = 0;
        SET @Scheduledhours = (
                              SELECT SUM(ScheduledHours)
                              FROM   syStudentAttendanceSummary
                              WHERE  StuEnrollId = @StuEnrollId
                              );

        SET @ActualPresentDays_ConvertTo_Hours = (
                                                 SELECT SUM(ActualPresentDays_ConvertTo_Hours)
                                                 FROM   syStudentAttendanceSummary
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                 );

        SET @ActualAbsentDays_ConvertTo_Hours = (
                                                SELECT SUM(ActualAbsentDays_ConvertTo_Hours)
                                                FROM   syStudentAttendanceSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                );


        SET @ActualTardyDays_ConvertTo_Hours = (
                                               SELECT SUM(ActualTardyDays_ConvertTo_Hours)
                                               FROM   syStudentAttendanceSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                               );


        --Average calculation
        DECLARE @termAverageSum DECIMAL(18, 2)
               ,@CumAverage DECIMAL(18, 2)
               ,@cumAverageSum DECIMAL(18, 2)
               ,@cumAveragecount INT;
        DECLARE @TermAverageCount INT
               ,@TermAverage DECIMAL(18, 2);


        DECLARE @TardiesMakingAbsence INT
               ,@UnitTypeDescrip VARCHAR(20)
               ,@OriginalTardiesMakingAbsence INT;
        SET @TardiesMakingAbsence = (
                                    SELECT TOP 1 t1.TardiesMakingAbsence
                                    FROM   arPrgVersions t1
                                          ,arStuEnrollments t2
                                    WHERE  t1.PrgVerId = t2.PrgVerId
                                           AND t2.StuEnrollId = @StuEnrollId
                                    );

        SET @UnitTypeDescrip = (
                               SELECT     LTRIM(RTRIM(AAUT.UnitTypeDescrip))
                               FROM       arAttUnitType AS AAUT
                               INNER JOIN arPrgVersions AS APV ON APV.UnitTypeId = AAUT.UnitTypeId
                               INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                               WHERE      ASE.StuEnrollId = @StuEnrollId
                               );

        SET @OriginalTardiesMakingAbsence = (
                                            SELECT TOP 1 tardiesmakingabsence
                                            FROM   syStudentAttendanceSummary
                                            WHERE  StuEnrollId = @StuEnrollId
                                            );
        DECLARE @termstartdate1 DATETIME;

        DECLARE @MeetDate DATETIME
               ,@WeekDay VARCHAR(15)
               ,@StartDate DATETIME
               ,@EndDate DATETIME;
        DECLARE @PeriodDescrip VARCHAR(50)
               ,@Actual DECIMAL(18, 2)
               ,@Excused DECIMAL(18, 2)
               ,@ClsSectionId UNIQUEIDENTIFIER;
        DECLARE @Absent DECIMAL(18, 2)
               ,@SchedHours DECIMAL(18, 2)
               ,@TardyMinutes DECIMAL(18, 2);
        DECLARE @tardy DECIMAL(18, 2)
               ,@tracktardies INT
               ,@rownumber INT
               ,@IsTardy BIT
               ,@ActualRunningScheduledHours DECIMAL(18, 2);
        DECLARE @ActualRunningPresentHours DECIMAL(18, 2)
               ,@ActualRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningTardyHours DECIMAL(18, 2)
               ,@ActualRunningMakeupHours DECIMAL(18, 2);
        DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
               ,@intTardyBreakPoint INT
               ,@AdjustedRunningPresentHours DECIMAL(18, 2)
               ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
               ,@ActualRunningScheduledDays DECIMAL(18, 2);
        --declare @Scheduledhours decimal(18,2),@ActualPresentDays_ConvertTo_Hours decimal(18,2),@ActualAbsentDays_ConvertTo_Hours decimal(18,2),@AttendanceTrack varchar(50)
        DECLARE @TermDescrip VARCHAR(50)
               ,@PrgVerId UNIQUEIDENTIFIER;
        DECLARE @TardyHit VARCHAR(10)
               ,@ScheduledMinutes DECIMAL(18, 2);
        DECLARE @Scheduledhours_noperiods DECIMAL(18, 2)
               ,@ActualPresentDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2)
               ,@ActualAbsentDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2);
        DECLARE @ActualTardyDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2);

        --Select * from arAttUnitType
        -- By Class and present absent, none
        /*
	IF @UnitTypeDescrip IN ( ''none'',''present absent'' )
		AND @TrackSapAttendance = ''byclass''
		BEGIN
			---- PRINT ''Step1!!!!''
			DELETE  FROM syStudentAttendanceSummary
			WHERE   StuEnrollId = @StuEnrollId;
			DECLARE @boolReset BIT
			   ,@MakeupHours DECIMAL(18,2)
			   ,@AdjustedPresentDaysComputed DECIMAL(18,2);
			DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD
			FOR
				SELECT  *
					   ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
				FROM    ( SELECT DISTINCT
									t1.StuEnrollId
								   ,t1.ClsSectionId
								   ,t1.MeetDate
								   ,DATENAME(dw,t1.MeetDate) AS WeekDay
								   ,t4.StartDate
								   ,t4.EndDate
								   ,t5.PeriodDescrip
								   ,t1.Actual
								   ,t1.Excused
								   ,CASE WHEN ( t1.Actual = 0
												AND t1.Excused = 0
											  ) THEN t1.Scheduled
										 ELSE CASE WHEN ( t1.Actual <> 9999.00
														  AND t1.Actual < t1.Scheduled
														  AND t1.Excused <> 1
														) THEN ( t1.Scheduled - t1.Actual )
												   ELSE 0
											  END
									END AS Absent
								   ,t1.Scheduled AS ScheduledMinutes
								   ,CASE WHEN ( t1.Actual > 0
												AND t1.Actual < t1.Scheduled
											  ) THEN ( t1.Scheduled - t1.Actual )
										 ELSE 0
									END AS TardyMinutes
								   ,t1.Tardy AS Tardy
								   ,t3.TrackTardies
								   ,t3.TardiesMakingAbsence
								   ,t3.PrgVerId
						  FROM      atClsSectAttendance t1
						  INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
						  INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
						  INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
						  INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
															 AND ( CONVERT(CHAR(10),t1.MeetDate,101) >= CONVERT(CHAR(10),t4.StartDate,101)
																   AND CONVERT(CHAR(10),t1.MeetDate,101) <= CONVERT(CHAR(10),t4.EndDate,101)
																 )
															 AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
						  INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
						  INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
						  INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
						  INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
						  INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
						  INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
						  WHERE     t2.StuEnrollId = @StuEnrollId
									AND ( AAUT1.UnitTypeDescrip IN ( ''None'',''Present Absent'' )
										  OR AAUT2.UnitTypeDescrip IN ( ''None'',''Present Absent'' )
										)
									AND t1.Actual <> 9999
						) dt
				ORDER BY StuEnrollId
					   ,MeetDate;
			OPEN GetAttendance_Cursor;
			FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,
				@PeriodDescrip,@Actual,@Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,
				@TardiesMakingAbsence,@PrgVerId,@rownumber;
			SET @ActualRunningPresentHours = 0;
			SET @ActualRunningPresentHours = 0;
			SET @ActualRunningAbsentHours = 0;
			SET @ActualRunningTardyHours = 0;
			SET @ActualRunningMakeupHours = 0;
			SET @intTardyBreakPoint = 0;
			SET @AdjustedRunningPresentHours = 0;
			SET @AdjustedRunningAbsentHours = 0;
			SET @ActualRunningScheduledDays = 0;
			SET @boolReset = 0;
			SET @MakeupHours = 0;
			WHILE @@FETCH_STATUS = 0
				BEGIN

					IF @PrevStuEnrollId <> @StuEnrollId
						BEGIN
							SET @ActualRunningPresentHours = 0;
							SET @ActualRunningAbsentHours = 0;
							SET @intTardyBreakPoint = 0;
							SET @ActualRunningTardyHours = 0;
							SET @AdjustedRunningPresentHours = 0;
							SET @AdjustedRunningAbsentHours = 0;
							SET @ActualRunningScheduledDays = 0;
							SET @MakeupHours = 0;
							SET @boolReset = 1;
						END;

					  
					-- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
					IF @Actual <> 9999.00
						BEGIN
							SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual + @Excused;
							SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual + @Excused;
							
							-- If there are make up hrs deduct that otherwise it will be added again in progress report
							IF ( @Actual > 0
								 AND @Actual > @ScheduledMinutes
								 AND @Actual <> 9999.00
							   )
								BEGIN
									SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual
																									- @ScheduledMinutes );
									SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual
																										- @ScheduledMinutes );
								END;
							  
							SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;                      
						END;
				   
					-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
					SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
					SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
			  
					-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
					IF ( @Actual > 0
						 AND @Actual < @ScheduledMinutes
						 AND @tardy = 1
					   )
						BEGIN
							SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
						END;
							
					-- Track how many days student has been tardy only when 
					-- program version requires to track tardy
					IF ( @tracktardies = 1
						 AND @tardy = 1
					   )
						BEGIN
							SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
						END;	    
		   
			
					-- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
					-- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
					-- when student is tardy the second time, that second occurance will be considered as
					-- absence
					-- Variable @intTardyBreakpoint tracks how many times the student was tardy
					-- Variable @tardiesMakingAbsence tracks the tardy rule
					IF ( @tracktardies = 1
						 AND @intTardyBreakPoint = @TardiesMakingAbsence
					   )
						BEGIN
							SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
							SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent )
								+ @ScheduledMinutes; --@TardyMinutes
							SET @intTardyBreakPoint = 0;
						END;
				   
				   -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
					IF ( @Actual > 0
						 AND @Actual > @ScheduledMinutes
						 AND @Actual <> 9999.00
					   )
						BEGIN
							SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
						END;
			  
					IF ( @tracktardies = 1 )
						BEGIN
							SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
						END;
					ELSE
						BEGIN
							SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
						END;
				
				---- PRINT @MeetDate
				---- PRINT @ActualRunningAbsentHours
				
					DELETE  FROM syStudentAttendanceSummary
					WHERE   StuEnrollId = @StuEnrollId
							AND ClsSectionId = @ClsSectionId
							AND StudentAttendedDate = @MeetDate;
					INSERT  INTO syStudentAttendanceSummary
							( StuEnrollId
							,ClsSectionId
							,StudentAttendedDate
							,ScheduledDays
							,ActualDays
							,ActualRunningScheduledDays
							,ActualRunningPresentDays
							,ActualRunningAbsentDays
							,ActualRunningMakeupDays
							,ActualRunningTardyDays
							,AdjustedPresentDays
							,AdjustedAbsentDays
							,AttendanceTrackType
							,ModUser
							,ModDate
							,tardiesmakingabsence
							)
					VALUES  ( @StuEnrollId
							,@ClsSectionId
							,@MeetDate
							,@ScheduledMinutes
							,@Actual
							,@ActualRunningScheduledDays
							,@ActualRunningPresentHours
							,@ActualRunningAbsentHours
							,ISNULL(@MakeupHours,0)
							,@ActualRunningTardyHours
							,@AdjustedPresentDaysComputed
							,@AdjustedRunningAbsentHours
							,''Post Attendance by Class Min''
							,''sa''
							,GETDATE()
							,@TardiesMakingAbsence
							);

				--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
					SET @PrevStuEnrollId = @StuEnrollId; 

					FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,
						@EndDate,@PeriodDescrip,@Actual,@Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,
						@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
				END;
			CLOSE GetAttendance_Cursor;
			DEALLOCATE GetAttendance_Cursor;
		END;			
*/
        IF @UnitTypeDescrip IN ( ''none'', ''present absent'' )
           AND @TrackSapAttendance = ''byclass''
            BEGIN
                ---- PRINT ''Step1!!!!''
                DELETE FROM syStudentAttendanceSummary
                WHERE StuEnrollId = @StuEnrollId;
                DECLARE @boolReset BIT
                       ,@MakeupHours DECIMAL(18, 2)
                       ,@AdjustedPresentDaysComputed DECIMAL(18, 2);
                DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
                    SELECT   dt.StuEnrollId
                            ,dt.ClsSectionId
                            ,dt.MeetDate
                            ,dt.WeekDay
                            ,dt.StartDate
                            ,dt.EndDate
                            ,dt.PeriodDescrip
                            ,dt.Actual
                            ,dt.Excused
                            ,dt.Absent
                            ,dt.ScheduledMinutes
                            ,dt.TardyMinutes
                            ,dt.Tardy
                            ,dt.TrackTardies
                            ,dt.TardiesMakingAbsence
                            ,dt.PrgVerId
                            ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                    FROM     (
                             SELECT     DISTINCT t1.StuEnrollId
                                                ,t1.ClsSectionId
                                                ,t1.MeetDate
                                                ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                ,t4.StartDate
                                                ,t4.EndDate
                                                ,t5.PeriodDescrip
                                                ,t1.Actual
                                                ,t1.Excused
                                                ,CASE WHEN (
                                                           t1.Actual = 0
                                                           AND t1.Excused = 0
                                                           ) THEN t1.Scheduled
                                                      ELSE CASE WHEN (
                                                                     t1.Actual <> 9999.00
                                                                     AND t1.Actual < t1.Scheduled
                                                                     --AND t1.Excused <> 1
                                                                     ) THEN ( t1.Scheduled - t1.Actual )
                                                                ELSE 0
                                                           END
                                                 END AS Absent
                                                ,t1.Scheduled AS ScheduledMinutes
                                                ,CASE WHEN (
                                                           t1.Actual > 0
                                                           AND t1.Actual < t1.Scheduled
                                                           ) THEN ( t1.Scheduled - t1.Actual )
                                                      ELSE 0
                                                 END AS TardyMinutes
                                                ,t1.Tardy AS Tardy
                                                ,t3.TrackTardies
                                                ,t3.TardiesMakingAbsence
                                                ,t3.PrgVerId
                             FROM       atClsSectAttendance t1
                             INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                             INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                             INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                             INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                AND (
                                                                    CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                    AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                    )
                                                                AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                             INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                             INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                             INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                             INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                             INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                             INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                             WHERE      t2.StuEnrollId = @StuEnrollId
                                        AND (
                                            AAUT1.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                            OR AAUT2.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                            )
                                        AND t1.Actual <> 9999
                             ) dt
                    ORDER BY StuEnrollId
                            ,MeetDate;
                OPEN GetAttendance_Cursor;
                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@ClsSectionId
                    ,@MeetDate
                    ,@WeekDay
                    ,@StartDate
                    ,@EndDate
                    ,@PeriodDescrip
                    ,@Actual
                    ,@Excused
                    ,@Absent
                    ,@ScheduledMinutes
                    ,@TardyMinutes
                    ,@tardy
                    ,@tracktardies
                    ,@TardiesMakingAbsence
                    ,@PrgVerId
                    ,@rownumber;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                SET @boolReset = 0;
                SET @MakeupHours = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                                SET @MakeupHours = 0;
                                SET @boolReset = 1;
                            END;


                        -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                        IF @Actual <> 9999.00
                            BEGIN
                                -- Commented by Balaji on 2.6.2015 as Excused Absence is still considered an absence
                                -- @Excused is not added to Present Hours
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual; -- + @Excused
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; -- + @Excused

                                -- If there are make up hrs deduct that otherwise it will be added again in progress report
                                IF (
                                   @Actual > 0
                                   AND @Actual > @ScheduledMinutes
                                   AND @Actual <> 9999.00
                                   )
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                    END;

                                SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                            END;

                        -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                        -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                        IF (
                           @Actual > 0
                           AND @Actual < @ScheduledMinutes
                           AND @tardy = 1
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                            END;
                        ELSE IF (
                                @Actual = 1
                                AND @Actual = @ScheduledMinutes
                                AND @tardy = 1
                                )
                                 BEGIN
                                     SET @ActualRunningTardyHours += 1;
                                 END;

                        -- Track how many days student has been tardy only when 
                        -- program version requires to track tardy
                        IF (
                           @tracktardies = 1
                           AND @tardy = 1
                           )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;


                        -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
                        -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
                        -- when student is tardy the second time, that second occurance will be considered as
                        -- absence
                        -- Variable @intTardyBreakpoint tracks how many times the student was tardy
                        -- Variable @tardiesMakingAbsence tracks the tardy rule
                        IF (
                           @tracktardies = 1
                           AND @TardiesMakingAbsence > 0
                           AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
                                SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;

                        -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                        IF (
                           @Actual > 0
                           AND @Actual > @ScheduledMinutes
                           AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                            END;

                        IF ( @tracktardies = 1 )
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                            END;
                        ELSE
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                            END;

                        ---- PRINT @MeetDate
                        ---- PRINT @ActualRunningAbsentHours

                        DELETE FROM syStudentAttendanceSummary
                        WHERE StuEnrollId = @StuEnrollId
                              AND ClsSectionId = @ClsSectionId
                              AND StudentAttendedDate = @MeetDate;
                        INSERT INTO syStudentAttendanceSummary (
                                                               StuEnrollId
                                                              ,ClsSectionId
                                                              ,StudentAttendedDate
                                                              ,ScheduledDays
                                                              ,ActualDays
                                                              ,ActualRunningScheduledDays
                                                              ,ActualRunningPresentDays
                                                              ,ActualRunningAbsentDays
                                                              ,ActualRunningMakeupDays
                                                              ,ActualRunningTardyDays
                                                              ,AdjustedPresentDays
                                                              ,AdjustedAbsentDays
                                                              ,AttendanceTrackType
                                                              ,ModUser
                                                              ,ModDate
                                                              ,tardiesmakingabsence
                                                               )
                        VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                                ,@ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours, @AdjustedPresentDaysComputed
                                ,@AdjustedRunningAbsentHours, ''Post Attendance by Class Min'', ''sa'', GETDATE(), @TardiesMakingAbsence );

                        --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                        SET @PrevStuEnrollId = @StuEnrollId;

                        FETCH NEXT FROM GetAttendance_Cursor
                        INTO @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@WeekDay
                            ,@StartDate
                            ,@EndDate
                            ,@PeriodDescrip
                            ,@Actual
                            ,@Excused
                            ,@Absent
                            ,@ScheduledMinutes
                            ,@TardyMinutes
                            ,@tardy
                            ,@tracktardies
                            ,@TardiesMakingAbsence
                            ,@PrgVerId
                            ,@rownumber;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;

                DECLARE @MyTardyTable TABLE
                    (
                        ClsSectionId UNIQUEIDENTIFIER
                       ,TardiesMakingAbsence INT
                       ,AbsentHours DECIMAL(18, 2)
                    );

                INSERT INTO @MyTardyTable
                            SELECT     ClsSectionId
                                      ,PV.TardiesMakingAbsence
                                      ,CASE WHEN ( COUNT(*) >= PV.TardiesMakingAbsence ) THEN ( COUNT(*) / PV.TardiesMakingAbsence )
                                            ELSE 0
                                       END AS AbsentHours
                            --Count(*) as NumberofTimesTardy 
                            FROM       syStudentAttendanceSummary SAS
                            INNER JOIN arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                            WHERE      SE.StuEnrollId = @StuEnrollId
                                       AND SAS.IsTardy = 1
                                       AND PV.TrackTardies = 1
                            GROUP BY   ClsSectionId
                                      ,PV.TardiesMakingAbsence;

                --Drop table @MyTardyTable
                DECLARE @TotalTardyAbsentDays DECIMAL(18, 2);
                SET @TotalTardyAbsentDays = (
                                            SELECT ISNULL(SUM(AbsentHours), 0)
                                            FROM   @MyTardyTable
                                            );

                --Print @TotalTardyAbsentDays

                UPDATE syStudentAttendanceSummary
                SET    AdjustedPresentDays = AdjustedPresentDays - @TotalTardyAbsentDays
                      ,AdjustedAbsentDays = AdjustedAbsentDays + @TotalTardyAbsentDays
                WHERE  StuEnrollId = @StuEnrollId
                       AND StudentAttendedDate = (
                                                 SELECT   TOP 1 StudentAttendedDate
                                                 FROM     syStudentAttendanceSummary
                                                 WHERE    StuEnrollId = @StuEnrollId
                                                 ORDER BY StudentAttendedDate DESC
                                                 );

            END;

        -- By Class and Attendance Unit Type - Minutes and Clock Hour

        IF @UnitTypeDescrip IN ( ''minutes'', ''clock hours'' )
           AND @TrackSapAttendance = ''byclass''
            BEGIN
                DELETE FROM syStudentAttendanceSummary
                WHERE StuEnrollId = @StuEnrollId;

                DECLARE GetAttendance_Cursor CURSOR FOR
                    SELECT   *
                            ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                    FROM     (
                             SELECT     DISTINCT t1.StuEnrollId
                                                ,t1.ClsSectionId
                                                ,t1.MeetDate
                                                ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                ,t4.StartDate
                                                ,t4.EndDate
                                                ,t5.PeriodDescrip
                                                ,t1.Actual
                                                ,t1.Excused
                                                ,CASE WHEN (
                                                           t1.Actual = 0
                                                           AND t1.Excused = 0
                                                           ) THEN t1.Scheduled
                                                      ELSE CASE WHEN (
                                                                     t1.Actual <> 9999.00
                                                                     AND t1.Actual < t1.Scheduled
                                                                     ) THEN ( t1.Scheduled - t1.Actual )
                                                                ELSE 0
                                                           END
                                                 END AS Absent
                                                ,t1.Scheduled AS ScheduledMinutes
                                                ,CASE WHEN (
                                                           t1.Actual > 0
                                                           AND t1.Actual < t1.Scheduled
                                                           ) THEN ( t1.Scheduled - t1.Actual )
                                                      ELSE 0
                                                 END AS TardyMinutes
                                                ,t1.Tardy AS Tardy
                                                ,t3.TrackTardies
                                                ,t3.TardiesMakingAbsence
                                                ,t3.PrgVerId
                             FROM       atClsSectAttendance t1
                             INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                             INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                             INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                             INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                AND (
                                                                    CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                    AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                    )
                                                                AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                             INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                             INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                             INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                             INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                             INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                             INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                             WHERE      t2.StuEnrollId = @StuEnrollId
                                        AND (
                                            AAUT1.UnitTypeDescrip IN ( ''Minutes'', ''Clock Hours'' )
                                            OR AAUT2.UnitTypeDescrip IN ( ''Minutes'', ''Clock Hours'' )
                                            )
                                        AND t1.Actual <> 9999
                             UNION
                             SELECT     DISTINCT t1.StuEnrollId
                                                ,NULL AS ClsSectionId
                                                ,t1.RecordDate AS MeetDate
                                                ,DATENAME(dw, t1.RecordDate) AS WeekDay
                                                ,NULL AS StartDate
                                                ,NULL AS EndDate
                                                ,NULL AS PeriodDescrip
                                                ,t1.ActualHours
                                                ,NULL AS Excused
                                                ,CASE WHEN ( t1.ActualHours = 0 ) THEN t1.SchedHours
                                                      ELSE 0
                                                 --ELSE 
                                                 --Case when (t1.ActualHours <> 9999.00 and t1.ActualHours < t1.SchedHours)
                                                 --		THEN (t1.SchedHours - t1.ActualHours)
                                                 --		ELSE 
                                                 --			0
                                                 --		End
                                                 END AS Absent
                                                ,t1.SchedHours AS ScheduledMinutes
                                                ,CASE WHEN (
                                                           t1.ActualHours <> 9999.00
                                                           AND t1.ActualHours > 0
                                                           AND t1.ActualHours < t1.SchedHours
                                                           ) THEN ( t1.SchedHours - t1.ActualHours )
                                                      ELSE 0
                                                 END AS TardyMinutes
                                                ,NULL AS Tardy
                                                ,t3.TrackTardies
                                                ,t3.TardiesMakingAbsence
                                                ,t3.PrgVerId
                             FROM       arStudentClockAttendance t1
                             INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                             INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                             WHERE      t2.StuEnrollId = @StuEnrollId
                                        AND t1.Converted = 1
                                        AND t1.ActualHours <> 9999
                             ) dt
                    ORDER BY StuEnrollId
                            ,MeetDate;
                OPEN GetAttendance_Cursor;
                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@ClsSectionId
                    ,@MeetDate
                    ,@WeekDay
                    ,@StartDate
                    ,@EndDate
                    ,@PeriodDescrip
                    ,@Actual
                    ,@Excused
                    ,@Absent
                    ,@ScheduledMinutes
                    ,@TardyMinutes
                    ,@tardy
                    ,@tracktardies
                    ,@TardiesMakingAbsence
                    ,@PrgVerId
                    ,@rownumber;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                SET @boolReset = 0;
                SET @MakeupHours = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                                SET @MakeupHours = 0;
                                SET @boolReset = 1;
                            END;


                        -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                        IF @Actual <> 9999.00
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                                -- If there are make up hrs deduct that otherwise it will be added again in progress report
                                IF (
                                   @Actual > 0
                                   AND @Actual > @ScheduledMinutes
                                   AND @Actual <> 9999.00
                                   )
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                    END;
                                SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                            END;

                        -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                        -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                        IF (
                           @Actual > 0
                           AND @Actual < @ScheduledMinutes
                           AND @tardy = 1
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                            END;

                        -- Track how many days student has been tardy only when 
                        -- program version requires to track tardy
                        IF (
                           @tracktardies = 1
                           AND @tardy = 1
                           )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;


                        -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
                        -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
                        -- when student is tardy the second time, that second occurance will be considered as
                        -- absence
                        -- Variable @intTardyBreakpoint tracks how many times the student was tardy
                        -- Variable @tardiesMakingAbsence tracks the tardy rule
                        IF (
                           @tracktardies = 1
                           AND @TardiesMakingAbsence > 0
                           AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;

                        -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                        IF (
                           @Actual > 0
                           AND @Actual > @ScheduledMinutes
                           AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                            END;


                        IF ( @tracktardies = 1 )
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                            END;
                        ELSE
                            BEGIN
                                SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                            END;




                        DELETE FROM syStudentAttendanceSummary
                        WHERE StuEnrollId = @StuEnrollId
                              AND ClsSectionId = @ClsSectionId
                              AND StudentAttendedDate = @MeetDate;
                        INSERT INTO syStudentAttendanceSummary (
                                                               StuEnrollId
                                                              ,ClsSectionId
                                                              ,StudentAttendedDate
                                                              ,ScheduledDays
                                                              ,ActualDays
                                                              ,ActualRunningScheduledDays
                                                              ,ActualRunningPresentDays
                                                              ,ActualRunningAbsentDays
                                                              ,ActualRunningMakeupDays
                                                              ,ActualRunningTardyDays
                                                              ,AdjustedPresentDays
                                                              ,AdjustedAbsentDays
                                                              ,AttendanceTrackType
                                                              ,ModUser
                                                              ,ModDate
                                                               )
                        VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                                ,@ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours, @AdjustedPresentDaysComputed
                                ,@AdjustedRunningAbsentHours, ''Post Attendance by Class Min'', ''sa'', GETDATE());

                        UPDATE syStudentAttendanceSummary
                        SET    tardiesmakingabsence = @TardiesMakingAbsence
                        WHERE  StuEnrollId = @StuEnrollId;
                        SET @PrevStuEnrollId = @StuEnrollId;

                        FETCH NEXT FROM GetAttendance_Cursor
                        INTO @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@WeekDay
                            ,@StartDate
                            ,@EndDate
                            ,@PeriodDescrip
                            ,@Actual
                            ,@Excused
                            ,@Absent
                            ,@ScheduledMinutes
                            ,@TardyMinutes
                            ,@tardy
                            ,@tracktardies
                            ,@TardiesMakingAbsence
                            ,@PrgVerId
                            ,@rownumber;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;
            END;
        --Select * from arAttUnitType


        -- By Minutes/Day
        -- remove clock hour from here
        IF @UnitTypeDescrip IN ( ''minutes'' )
           AND @TrackSapAttendance = ''byday''
            -- -- PRINT GETDATE();	
            ---- PRINT @UnitTypeId;
            ---- PRINT @TrackSapAttendance;
            BEGIN
                --Delete from syStudentAttendanceSummary where StuEnrollId=@StuEnrollId

                DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
                    SELECT     t1.StuEnrollId
                              ,NULL AS ClsSectionId
                              ,t1.RecordDate AS MeetDate
                              ,t1.ActualHours
                              ,t1.SchedHours AS ScheduledMinutes
                              ,CASE WHEN (
                                         (
                                         t1.SchedHours >= 1
                                         AND t1.SchedHours NOT IN ( 999, 9999 )
                                         )
                                         AND t1.ActualHours = 0
                                         ) THEN t1.SchedHours
                                    ELSE 0
                               END AS Absent
                              ,t1.isTardy
                              ,(
                               SELECT ISNULL(SUM(SchedHours), 0)
                               FROM   arStudentClockAttendance
                               WHERE  StuEnrollId = t1.StuEnrollId
                                      AND RecordDate <= t1.RecordDate
                                      AND (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999, 9999 )
                                          AND t1.ActualHours NOT IN ( 999, 9999 )
                                          )
                               ) AS ActualRunningScheduledHours
                              ,(
                               SELECT SUM(ActualHours)
                               FROM   arStudentClockAttendance
                               WHERE  StuEnrollId = t1.StuEnrollId
                                      AND RecordDate <= t1.RecordDate
                                      AND (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999, 9999 )
                                          )
                                      AND ActualHours >= 1
                                      AND ActualHours NOT IN ( 999, 9999 )
                               ) AS ActualRunningPresentHours
                              ,(
                               SELECT COUNT(ActualHours)
                               FROM   arStudentClockAttendance
                               WHERE  StuEnrollId = t1.StuEnrollId
                                      AND RecordDate <= t1.RecordDate
                                      AND (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999, 9999 )
                                          )
                                      AND ActualHours = 0
                                      AND ActualHours NOT IN ( 999, 9999 )
                               ) AS ActualRunningAbsentHours
                              ,(
                               SELECT SUM(ActualHours)
                               FROM   arStudentClockAttendance
                               WHERE  StuEnrollId = t1.StuEnrollId
                                      AND RecordDate <= t1.RecordDate
                                      AND SchedHours = 0
                                      AND ActualHours >= 1
                                      AND ActualHours NOT IN ( 999, 9999 )
                               ) AS ActualRunningMakeupHours
                              ,(
                               SELECT SUM(SchedHours - ActualHours)
                               FROM   arStudentClockAttendance
                               WHERE  StuEnrollId = t1.StuEnrollId
                                      AND RecordDate <= t1.RecordDate
                                      AND (
                                          (
                                          t1.SchedHours >= 1
                                          AND t1.SchedHours NOT IN ( 999, 9999 )
                                          )
                                          AND ActualHours >= 1
                                          AND ActualHours NOT IN ( 999, 9999 )
                                          )
                                      AND isTardy = 1
                               ) AS ActualRunningTardyHours
                              ,t3.TrackTardies
                              ,t3.TardiesMakingAbsence
                              ,t3.PrgVerId
                              ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
                    FROM       arStudentClockAttendance t1
                    INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                    INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                    INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                    --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                    WHERE      AAUT1.UnitTypeDescrip IN ( ''Minutes'' )
                               AND t2.StuEnrollId = @StuEnrollId
                               AND t1.ActualHours <> 9999.00
                    ORDER BY   t1.StuEnrollId
                              ,MeetDate;
                OPEN GetAttendance_Cursor;
                --Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@ClsSectionId
                    ,@MeetDate
                    ,@Actual
                    ,@ScheduledMinutes
                    ,@Absent
                    ,@IsTardy
                    ,@ActualRunningScheduledHours
                    ,@ActualRunningPresentHours
                    ,@ActualRunningAbsentHours
                    ,@ActualRunningMakeupHours
                    ,@ActualRunningTardyHours
                    ,@tracktardies
                    ,@TardiesMakingAbsence
                    ,@PrgVerId
                    ,@rownumber;

                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                            END;

                        IF (
                           @ScheduledMinutes >= 1
                           AND (
                               @Actual <> 9999
                               AND @Actual <> 999
                               )
                           AND (
                               @ScheduledMinutes <> 9999
                               AND @Actual <> 999
                               )
                           )
                            BEGIN
                                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + ISNULL(@ScheduledMinutes, 0);
                            END;
                        ELSE
                            BEGIN
                                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0);
                            END;

                        IF (
                           @Actual <> 9999
                           AND @Actual <> 999
                           )
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                            END;
                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;

                        IF (
                           @Actual > 0
                           AND @Actual < @ScheduledMinutes
                           )
                            BEGIN
                                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                            END;
                        -- Make up hours
                        --sched=5, Actual =7, makeup= 2,ab = 0
                        --sched=0, Actual =7, makeup= 7,ab = 0
                        IF (
                           @Actual > 0
                           AND @ScheduledMinutes > 0
                           AND @Actual > @ScheduledMinutes
                           AND (
                               @Actual <> 9999
                               AND @Actual <> 999
                               )
                           )
                            BEGIN
                                SET @ActualRunningMakeupHours = @ActualRunningMakeupHours + ( @Actual - @ScheduledMinutes );
                            END;


                        IF (
                           @Actual <> 9999
                           AND @Actual <> 999
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                            END;
                        IF (
                           @Actual = 0
                           AND @ScheduledMinutes >= 1
                           AND @ScheduledMinutes NOT IN ( 999, 9999 )
                           )
                            BEGIN
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                            END;
                        -- Absent hours
                        --1. sched = 5, Actual = 2 then Ab = (5-3) = 2
                        IF (
                           @Absent = 0
                           AND @ActualRunningAbsentHours > 0
                           AND ( @Actual < @ScheduledMinutes )
                           )
                            BEGIN
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                            END;
                        IF (
                           @Actual > 0
                           AND @Actual < @ScheduledMinutes
                           AND (
                               @Actual <> 9999.00
                               AND @Actual <> 999.00
                               )
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                            END;

                        IF @tracktardies = 1
                           AND (
                               @TardyMinutes > 0
                               OR @IsTardy = 1
                               )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;
                        IF (
                           @tracktardies = 1
                           AND @TardiesMakingAbsence > 0
                           AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Actual; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;
                        DELETE FROM syStudentAttendanceSummary
                        WHERE StuEnrollId = @StuEnrollId
                              AND StudentAttendedDate = @MeetDate;
                        INSERT INTO syStudentAttendanceSummary (
                                                               StuEnrollId
                                                              ,ClsSectionId
                                                              ,StudentAttendedDate
                                                              ,ScheduledDays
                                                              ,ActualDays
                                                              ,ActualRunningScheduledDays
                                                              ,ActualRunningPresentDays
                                                              ,ActualRunningAbsentDays
                                                              ,ActualRunningMakeupDays
                                                              ,ActualRunningTardyDays
                                                              ,AdjustedPresentDays
                                                              ,AdjustedAbsentDays
                                                              ,AttendanceTrackType
                                                              ,ModUser
                                                              ,ModDate
                                                               )
                        VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays, @ActualRunningPresentHours
                                ,@ActualRunningAbsentHours, @ActualRunningMakeupHours, @ActualRunningTardyHours, @AdjustedRunningPresentHours
                                ,@AdjustedRunningAbsentHours, ''Post Attendance by Class'', ''sa'', GETDATE());

                        UPDATE syStudentAttendanceSummary
                        SET    tardiesmakingabsence = @TardiesMakingAbsence
                        WHERE  StuEnrollId = @StuEnrollId;
                        --end
                        SET @PrevStuEnrollId = @StuEnrollId;
                        FETCH NEXT FROM GetAttendance_Cursor
                        INTO @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@Actual
                            ,@ScheduledMinutes
                            ,@Absent
                            ,@IsTardy
                            ,@ActualRunningScheduledHours
                            ,@ActualRunningPresentHours
                            ,@ActualRunningAbsentHours
                            ,@ActualRunningMakeupHours
                            ,@ActualRunningTardyHours
                            ,@tracktardies
                            ,@TardiesMakingAbsence
                            ,@PrgVerId
                            ,@rownumber;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;
            END;

        ---- PRINT ''Does it get to Clock Hour/PA'';
        ---- PRINT @UnitTypeId;
        ---- PRINT @TrackSapAttendance;
        -- By Day and PA, Clock Hour
        IF @UnitTypeDescrip IN ( ''present absent'', ''clock hours'' )
           AND @TrackSapAttendance = ''byday''
            BEGIN
                ---- PRINT ''By Day inside day'';
                DECLARE GetAttendance_Cursor CURSOR FOR
                    SELECT     t1.StuEnrollId
                              ,t1.RecordDate
                              ,t1.ActualHours
                              ,t1.SchedHours
                              ,CASE WHEN (
                                         (
                                         t1.SchedHours >= 1
                                         AND t1.SchedHours NOT IN ( 999, 9999 )
                                         )
                                         AND t1.ActualHours = 0
                                         ) THEN t1.SchedHours
                                    ELSE 0
                               END AS Absent
                              ,t1.isTardy
                              ,t3.TrackTardies
                              ,t3.TardiesMakingAbsence
                    FROM       arStudentClockAttendance t1
                    INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                    INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                    INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                    --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                    WHERE -- Unit Types: Present Absent and Clock Hour
                               AAUT1.UnitTypeDescrip IN ( ''present absent'', ''clock hours'' )
                               AND ActualHours <> 9999.00
                               AND t2.StuEnrollId = @StuEnrollId
                    ORDER BY   t1.StuEnrollId
                              ,t1.RecordDate;
                OPEN GetAttendance_Cursor;
                --Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                FETCH NEXT FROM GetAttendance_Cursor
                INTO @StuEnrollId
                    ,@MeetDate
                    ,@Actual
                    ,@ScheduledMinutes
                    ,@Absent
                    ,@IsTardy
                    ,@tracktardies
                    ,@TardiesMakingAbsence;

                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningPresentHours = 0;
                SET @ActualRunningAbsentHours = 0;
                SET @ActualRunningTardyHours = 0;
                SET @ActualRunningMakeupHours = 0;
                SET @intTardyBreakPoint = 0;
                SET @AdjustedRunningPresentHours = 0;
                SET @AdjustedRunningAbsentHours = 0;
                SET @ActualRunningScheduledDays = 0;
                SET @MakeupHours = 0;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        IF @PrevStuEnrollId <> @StuEnrollId
                            BEGIN
                                SET @ActualRunningPresentHours = 0;
                                SET @ActualRunningAbsentHours = 0;
                                SET @intTardyBreakPoint = 0;
                                SET @ActualRunningTardyHours = 0;
                                SET @AdjustedRunningPresentHours = 0;
                                SET @AdjustedRunningAbsentHours = 0;
                                SET @ActualRunningScheduledDays = 0;
                                SET @MakeupHours = 0;
                            END;

                        IF (
                           @Actual <> 9999
                           AND @Actual <> 999
                           )
                            BEGIN
                                SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + @ScheduledMinutes;
                                SET @ActualRunningPresentHours = ISNULL(@ActualRunningPresentHours, 0) + @Actual;
                                SET @AdjustedRunningPresentHours = ISNULL(@AdjustedRunningPresentHours, 0) + @Actual;
                            END;
                        SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + @Absent;
                        IF (
                           @Actual = 0
                           AND @ScheduledMinutes >= 1
                           AND @ScheduledMinutes NOT IN ( 999, 9999 )
                           )
                            BEGIN
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                            END;

                        -- NWH 
                        -- If Scheduled = 3.0 hrs and Actual = 1.0 Then 2 hrs is considered absent
                        IF (
                           @ScheduledMinutes >= 1
                           AND @ScheduledMinutes NOT IN ( 999, 9999 )
                           AND @Actual > 0
                           AND ( @Actual < @ScheduledMinutes )
                           )
                            BEGIN
                                SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                                SET @AdjustedRunningAbsentHours = ISNULL(@AdjustedRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                            END;

                        IF (
                           @tracktardies = 1
                           AND @IsTardy = 1
                           )
                            BEGIN
                                SET @ActualRunningTardyHours = @ActualRunningTardyHours + 1;
                            END;
                        --commented by balaji on 10/22/2012 as report (rdl) doesn''t add days attended and make up days
                        ---- If there are make up hrs deduct that otherwise it will be added again in progress report
                        IF (
                           @Actual > 0
                           AND @Actual > @ScheduledMinutes
                           AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                            END;

                        IF @tracktardies = 1
                           AND (
                               @TardyMinutes > 0
                               OR @IsTardy = 1
                               )
                            BEGIN
                                SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                            END;



                        -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                        IF (
                           @Actual > 0
                           AND @Actual > @ScheduledMinutes
                           AND @Actual <> 9999.00
                           )
                            BEGIN
                                SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                            END;

                        IF (
                           @tracktardies = 1
                           AND @TardiesMakingAbsence > 0
                           AND @intTardyBreakPoint = @TardiesMakingAbsence
                           )
                            BEGIN
                                SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                                SET @intTardyBreakPoint = 0;
                            END;

                        DELETE FROM syStudentAttendanceSummary
                        WHERE StuEnrollId = @StuEnrollId
                              AND StudentAttendedDate = @MeetDate;
                        INSERT INTO syStudentAttendanceSummary (
                                                               StuEnrollId
                                                              ,ClsSectionId
                                                              ,StudentAttendedDate
                                                              ,ScheduledDays
                                                              ,ActualDays
                                                              ,ActualRunningScheduledDays
                                                              ,ActualRunningPresentDays
                                                              ,ActualRunningAbsentDays
                                                              ,ActualRunningMakeupDays
                                                              ,ActualRunningTardyDays
                                                              ,AdjustedPresentDays
                                                              ,AdjustedAbsentDays
                                                              ,AttendanceTrackType
                                                              ,ModUser
                                                              ,ModDate
                                                              ,tardiesmakingabsence
                                                               )
                        VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, ISNULL(@ScheduledMinutes, 0), @Actual, ISNULL(@ActualRunningScheduledDays, 0)
                                ,ISNULL(@ActualRunningPresentHours, 0), ISNULL(@ActualRunningAbsentHours, 0), ISNULL(@MakeupHours, 0)
                                ,ISNULL(@ActualRunningTardyHours, 0), ISNULL(@AdjustedRunningPresentHours, 0), ISNULL(@AdjustedRunningAbsentHours, 0)
                                ,''Post Attendance by Class'', ''sa'', GETDATE(), @TardiesMakingAbsence );

                        --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                        --end
                        SET @PrevStuEnrollId = @StuEnrollId;
                        FETCH NEXT FROM GetAttendance_Cursor
                        INTO @StuEnrollId
                            ,@MeetDate
                            ,@Actual
                            ,@ScheduledMinutes
                            ,@Absent
                            ,@IsTardy
                            ,@tracktardies
                            ,@TardiesMakingAbsence;
                    END;
                CLOSE GetAttendance_Cursor;
                DEALLOCATE GetAttendance_Cursor;
            END;
        --end

        -- Based on grade rounding round the final score and current score
        --Declare @PrevTermId uniqueidentifier,@PrevReqId uniqueidentifier,@RowCount int,@FinalScore decimal(18,4),@CurrentScore decimal(18,4)
        --declare @ReqId uniqueidentifier,@CourseCodeDescrip varchar(100),@FinalGrade varchar(10),@sysComponentTypeId int
        --declare @CreditsAttempted decimal(18,4),@Grade varchar(10),@IsPass bit,@IsCreditsAttempted bit,@IsCreditsEarned bit
        --declare @IsInGPA bit,@FinAidCredits decimal(18,4),@CurrentGrade varchar(10),@CourseCredits decimal(18,4)
        DECLARE @GPA DECIMAL(18, 4);

        DECLARE @PrevReqId UNIQUEIDENTIFIER
               ,@PrevTermId UNIQUEIDENTIFIER
               ,@CreditsAttempted DECIMAL(18, 2)
               ,@CreditsEarned DECIMAL(18, 2);
        DECLARE @reqid UNIQUEIDENTIFIER
               ,@CourseCodeDescrip VARCHAR(50)
               ,@FinalGrade VARCHAR(50)
               ,@FinalScore DECIMAL(18, 2)
               ,@Grade VARCHAR(50);
        DECLARE @IsPass BIT
               ,@IsCreditsAttempted BIT
               ,@IsCreditsEarned BIT
               ,@CurrentScore DECIMAL(18, 2)
               ,@CurrentGrade VARCHAR(10)
               ,@FinalGPA DECIMAL(18, 2);
        DECLARE @sysComponentTypeId INT
               ,@RowCount INT;
        DECLARE @IsInGPA BIT;
        DECLARE @CourseCredits DECIMAL(18, 2);
        DECLARE @FinAidCreditsEarned DECIMAL(18, 2)
               ,@FinAidCredits DECIMAL(18, 2);




        DECLARE GetCreditsSummary_Cursor CURSOR FOR
            SELECT     DISTINCT SE.StuEnrollId
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate
                               ,R.ReqId
                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                               ,RES.Score AS FinalScore
                               ,RES.GrdSysDetailId AS FinalGrade
                               ,GCT.SysComponentTypeId
                               ,R.Credits AS CreditsAttempted
                               ,CS.ClsSectionId
                               ,GSD.Grade
                               ,GSD.IsPass
                               ,GSD.IsCreditsAttempted
                               ,GSD.IsCreditsEarned
                               ,SE.PrgVerId
                               ,GSD.IsInGPA
                               ,R.FinAidCredits AS FinAidCredits
            FROM       arStuEnrollments SE
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
            INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
            INNER JOIN arTerm T ON CS.TermId = T.TermId
            INNER JOIN arReqs R ON CS.ReqId = R.ReqId
            LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
            LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
            LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
            LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            WHERE      SE.StuEnrollId = @StuEnrollId
            ORDER BY   T.StartDate
                      ,T.TermDescrip
                      ,R.ReqId;

        DECLARE @varGradeRounding VARCHAR(3);
        DECLARE @roundfinalscore DECIMAL(18, 4);
        SET @varGradeRounding = (
                                SELECT Value
                                FROM   syConfigAppSetValues
                                WHERE  SettingId = 45
                                );

        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor
        INTO @StuEnrollId
            ,@TermId
            ,@TermDescrip
            ,@termstartdate1
            ,@reqid
            ,@CourseCodeDescrip
            ,@FinalScore
            ,@FinalGrade
            ,@sysComponentTypeId
            ,@CreditsAttempted
            ,@ClsSectionId
            ,@Grade
            ,@IsPass
            ,@IsCreditsAttempted
            ,@IsCreditsEarned
            ,@PrgVerId
            ,@IsInGPA
            ,@FinAidCredits;
        WHILE @@FETCH_STATUS = 0
            BEGIN


                SET @CurrentScore = (
                                    SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                ELSE NULL
                                           END AS CurrentScore
                                    FROM   (
                                           SELECT   InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight AS GradeBookWeight
                                                   ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                         ELSE 0
                                                    END AS ActualWeight
                                           FROM     (
                                                    SELECT   C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,ISNULL(C.Weight, 0) AS Weight
                                                            ,C.Number AS MinNumber
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter AS Param
                                                            ,X.GrdScaleId
                                                            ,SUM(GR.Score) AS Score
                                                            ,COUNT(D.Descrip) AS NumberOfComponents
                                                    FROM     (
                                                             SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                    ,A.EffectiveDate
                                                                                    ,B.GrdScaleId
                                                             FROM     arGrdBkWeights A
                                                                     ,arClassSections B
                                                             WHERE    A.ReqId = B.ReqId
                                                                      AND A.EffectiveDate <= B.StartDate
                                                                      AND B.ClsSectionId = @ClsSectionId
                                                             ORDER BY A.EffectiveDate DESC
                                                             ) X
                                                            ,arGrdBkWgtDetails C
                                                            ,arGrdComponentTypes D
                                                            ,arGrdBkResults GR
                                                    WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                             AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                             AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                             AND D.SysComponentTypeId NOT IN ( 500, 503 )
                                                             AND GR.StuEnrollId = @StuEnrollId
                                                             AND GR.ClsSectionId = @ClsSectionId
                                                             AND GR.Score IS NOT NULL
                                                    GROUP BY C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,C.Weight
                                                            ,C.Number
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter
                                                            ,X.GrdScaleId
                                                    ) S
                                           GROUP BY InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight
                                                   ,NumberOfComponents
                                           ) FinalTblToComputeCurrentScore
                                    );
                IF ( @CurrentScore IS NULL )
                    BEGIN
                        -- instructor grade books
                        SET @CurrentScore = (
                                            SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                        ELSE NULL
                                                   END AS CurrentScore
                                            FROM   (
                                                   SELECT   InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                   FROM     (
                                                            SELECT   C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,ISNULL(C.Weight, 0) AS Weight
                                                                    ,C.Number AS MinNumber
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter AS Param
                                                                    ,X.GrdScaleId
                                                                    ,SUM(GR.Score) AS Score
                                                                    ,COUNT(D.Descrip) AS NumberOfComponents
                                                            FROM     (
                                                                     --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
                                                                     --FROM          arGrdBkWeights A,arClassSections B        
                                                                     --WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
                                                                     --ORDER BY      A.EffectiveDate DESC
                                                                     SELECT DISTINCT TOP 1 t1.InstrGrdBkWgtId
                                                                                          ,t1.GrdScaleId
                                                                     FROM   arClassSections t1
                                                                           ,arGrdBkWeights t2
                                                                     WHERE  t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                            AND t1.ClsSectionId = @ClsSectionId
                                                                     ) X
                                                                    ,arGrdBkWgtDetails C
                                                                    ,arGrdComponentTypes D
                                                                    ,arGrdBkResults GR
                                                            WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                     AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                     AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                     AND
                                                                -- D.SysComponentTypeID not in (500,503) and 
                                                                GR.StuEnrollId = @StuEnrollId
                                                                     AND GR.ClsSectionId = @ClsSectionId
                                                                     AND GR.Score IS NOT NULL
                                                            GROUP BY C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,C.Weight
                                                                    ,C.Number
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter
                                                                    ,X.GrdScaleId
                                                            ) S
                                                   GROUP BY InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                   ) FinalTblToComputeCurrentScore
                                            );

                    END;


                -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                IF ( LOWER(@varGradeRounding) = ''yes'' )
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @FinalScore = ROUND(@FinalScore, 0);
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;
                ELSE
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;

                UPDATE syCreditSummary
                SET    FinalScore = @FinalScore
                      ,CurrentScore = @CurrentScore
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId
                       AND ReqId = @reqid;

                --Average calculation

                -- Term Average
                SET @TermAverageCount = (
                                        SELECT COUNT(*)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND TermId = @TermId
                                               AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                      SELECT SUM(FinalScore)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount;

                -- Cumulative Average
                SET @cumAveragecount = (
                                       SELECT COUNT(*)
                                       FROM   syCreditSummary
                                       WHERE  StuEnrollId = @StuEnrollId
                                              AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                     SELECT SUM(FinalScore)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount;

                UPDATE syCreditSummary
                SET    Average = @TermAverage
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId;

                --Update Cumulative GPA
                UPDATE syCreditSummary
                SET    CumAverage = @CumAverage
                WHERE  StuEnrollId = @StuEnrollId;


                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@FinalGrade
                    ,@sysComponentTypeId
                    ,@CreditsAttempted
                    ,@ClsSectionId
                    ,@Grade
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;


        DECLARE GetCreditsSummary_Cursor CURSOR FOR
            SELECT     DISTINCT SE.StuEnrollId
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate
                               ,R.ReqId
                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                               ,RES.Score AS FinalScore
                               ,RES.GrdSysDetailId AS FinalGrade
                               ,GCT.SysComponentTypeId
                               ,R.Credits AS CreditsAttempted
                               ,CS.ClsSectionId
                               ,GSD.Grade
                               ,GSD.IsPass
                               ,GSD.IsCreditsAttempted
                               ,GSD.IsCreditsEarned
                               ,SE.PrgVerId
                               ,GSD.IsInGPA
                               ,R.FinAidCredits AS FinAidCredits
            FROM       arStuEnrollments SE
            INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
            INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
            INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                             AND RES.TermId = CS.TermId
            INNER JOIN arTerm T ON CS.TermId = T.TermId
            INNER JOIN arReqs R ON CS.ReqId = R.ReqId
            LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
            LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
            LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
            LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            WHERE      SE.StuEnrollId = @StuEnrollId
            ORDER BY   T.StartDate
                      ,T.TermDescrip
                      ,R.ReqId;


        SET @varGradeRounding = (
                                SELECT Value
                                FROM   syConfigAppSetValues
                                WHERE  SettingId = 45
                                );

        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor
        INTO @StuEnrollId
            ,@TermId
            ,@TermDescrip
            ,@termstartdate1
            ,@reqid
            ,@CourseCodeDescrip
            ,@FinalScore
            ,@FinalGrade
            ,@sysComponentTypeId
            ,@CreditsAttempted
            ,@ClsSectionId
            ,@Grade
            ,@IsPass
            ,@IsCreditsAttempted
            ,@IsCreditsEarned
            ,@PrgVerId
            ,@IsInGPA
            ,@FinAidCredits;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                SET @CurrentScore = (
                                    SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                ELSE NULL
                                           END AS CurrentScore
                                    FROM   (
                                           SELECT   InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight AS GradeBookWeight
                                                   ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                         ELSE 0
                                                    END AS ActualWeight
                                           FROM     (
                                                    SELECT   C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,ISNULL(C.Weight, 0) AS Weight
                                                            ,C.Number AS MinNumber
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter AS Param
                                                            ,X.GrdScaleId
                                                            ,SUM(GR.Score) AS Score
                                                            ,COUNT(D.Descrip) AS NumberOfComponents
                                                    FROM     (
                                                             SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                    ,A.EffectiveDate
                                                                                    ,B.GrdScaleId
                                                             FROM     arGrdBkWeights A
                                                                     ,arClassSections B
                                                             WHERE    A.ReqId = B.ReqId
                                                                      AND A.EffectiveDate <= B.StartDate
                                                                      AND B.ClsSectionId = @ClsSectionId
                                                             ORDER BY A.EffectiveDate DESC
                                                             ) X
                                                            ,arGrdBkWgtDetails C
                                                            ,arGrdComponentTypes D
                                                            ,arGrdBkResults GR
                                                    WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                             AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                             AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                             AND D.SysComponentTypeId NOT IN ( 500, 503 )
                                                             AND GR.StuEnrollId = @StuEnrollId
                                                             AND GR.ClsSectionId = @ClsSectionId
                                                             AND GR.Score IS NOT NULL
                                                    GROUP BY C.InstrGrdBkWgtDetailId
                                                            ,D.Code
                                                            ,D.Descrip
                                                            ,C.Weight
                                                            ,C.Number
                                                            ,C.GrdPolicyId
                                                            ,C.Parameter
                                                            ,X.GrdScaleId
                                                    ) S
                                           GROUP BY InstrGrdBkWgtDetailId
                                                   ,Code
                                                   ,Descrip
                                                   ,Weight
                                                   ,NumberOfComponents
                                           ) FinalTblToComputeCurrentScore
                                    );
                IF ( @CurrentScore IS NULL )
                    BEGIN
                        -- instructor grade books
                        SET @CurrentScore = (
                                            SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                        ELSE NULL
                                                   END AS CurrentScore
                                            FROM   (
                                                   SELECT   InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                   FROM     (
                                                            SELECT   C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,ISNULL(C.Weight, 0) AS Weight
                                                                    ,C.Number AS MinNumber
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter AS Param
                                                                    ,X.GrdScaleId
                                                                    ,SUM(GR.Score) AS Score
                                                                    ,COUNT(D.Descrip) AS NumberOfComponents
                                                            FROM     (
                                                                     --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
                                                                     --FROM          arGrdBkWeights A,arClassSections B        
                                                                     --WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
                                                                     --ORDER BY      A.EffectiveDate DESC
                                                                     SELECT DISTINCT TOP 1 t1.InstrGrdBkWgtId
                                                                                          ,t1.GrdScaleId
                                                                     FROM   arClassSections t1
                                                                           ,arGrdBkWeights t2
                                                                     WHERE  t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                            AND t1.ClsSectionId = @ClsSectionId
                                                                     ) X
                                                                    ,arGrdBkWgtDetails C
                                                                    ,arGrdComponentTypes D
                                                                    ,arGrdBkResults GR
                                                            WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                     AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                     AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                     AND
                                                                -- D.SysComponentTypeID not in (500,503) and 
                                                                GR.StuEnrollId = @StuEnrollId
                                                                     AND GR.ClsSectionId = @ClsSectionId
                                                                     AND GR.Score IS NOT NULL
                                                            GROUP BY C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,C.Weight
                                                                    ,C.Number
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter
                                                                    ,X.GrdScaleId
                                                            ) S
                                                   GROUP BY InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                   ) FinalTblToComputeCurrentScore
                                            );

                    END;

                -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                IF ( LOWER(@varGradeRounding) = ''yes'' )
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @FinalScore = ROUND(@FinalScore, 0);
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;
                ELSE
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;

                UPDATE syCreditSummary
                SET    FinalScore = @FinalScore
                      ,CurrentScore = @CurrentScore
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId
                       AND ReqId = @reqid;

                --Average calculation
                --			declare @CumAverage decimal(18,2),@cumAverageSum decimal(18,2),@cumAveragecount int

                -- Term Average
                SET @TermAverageCount = (
                                        SELECT COUNT(*)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND TermId = @TermId
                                               AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                      SELECT SUM(FinalScore)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount;

                -- Cumulative Average
                SET @cumAveragecount = (
                                       SELECT COUNT(*)
                                       FROM   syCreditSummary
                                       WHERE  StuEnrollId = @StuEnrollId
                                              AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                     SELECT SUM(FinalScore)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount;

                UPDATE syCreditSummary
                SET    Average = @TermAverage
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId;

                --Update Cumulative GPA
                UPDATE syCreditSummary
                SET    CumAverage = @CumAverage
                WHERE  StuEnrollId = @StuEnrollId;


                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@FinalGrade
                    ,@sysComponentTypeId
                    ,@CreditsAttempted
                    ,@ClsSectionId
                    ,@Grade
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;

        DECLARE @GradeSystemDetailId UNIQUEIDENTIFIER
               ,@IsGPA BIT;
        DECLARE GetCreditsSummary_Cursor CURSOR FOR
            SELECT     DISTINCT SE.StuEnrollId
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate
                               ,R.ReqId
                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                               ,NULL AS FinalScore
                               ,RES.GrdSysDetailId AS GradeSystemDetailId
                               ,GSD.Grade AS FinalGrade
                               ,GSD.Grade AS CurrentGrade
                               ,R.Credits
                               ,NULL AS ClsSectionId
                               ,GSD.IsPass
                               ,GSD.IsCreditsAttempted
                               ,GSD.IsCreditsEarned
                               ,GSD.GPA
                               ,GSD.IsInGPA
                               ,SE.PrgVerId
                               ,GSD.IsInGPA
                               ,R.FinAidCredits AS FinAidCredits
            FROM       arStuEnrollments SE
            INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
            INNER JOIN syCreditSummary CS ON CS.StuEnrollId = RES.StuEnrollId
            INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            INNER JOIN arReqs R ON RES.ReqId = R.ReqId
            INNER JOIN arTerm T ON RES.TermId = T.TermId
            WHERE      RES.ReqId NOT IN (
                                        SELECT DISTINCT ReqId
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = SE.StuEnrollId
                                               AND TermId = T.TermId
                                        )
                       AND SE.StuEnrollId = @StuEnrollId;

        SET @varGradeRounding = (
                                SELECT Value
                                FROM   syConfigAppSetValues
                                WHERE  SettingId = 45
                                );

        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor
        INTO @StuEnrollId
            ,@TermId
            ,@TermDescrip
            ,@termstartdate1
            ,@reqid
            ,@CourseCodeDescrip
            ,@FinalScore
            ,@GradeSystemDetailId
            ,@FinalGrade
            ,@CurrentGrade
            ,@CourseCredits
            ,@ClsSectionId
            ,@IsPass
            ,@IsCreditsAttempted
            ,@IsCreditsEarned
            ,@GPA
            ,@IsGPA
            ,@PrgVerId
            ,@IsInGPA
            ,@FinAidCredits;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                IF ( LOWER(@varGradeRounding) = ''yes'' )
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @FinalScore = ROUND(@FinalScore, 0);
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;
                ELSE
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;

                UPDATE syCreditSummary
                SET    FinalScore = @FinalScore
                      ,CurrentScore = @CurrentScore
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId
                       AND ReqId = @reqid;

                --Average calculation

                -- Term Average
                SET @TermAverageCount = (
                                        SELECT COUNT(*)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND TermId = @TermId
                                               AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                      SELECT SUM(FinalScore)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount;

                -- Cumulative Average
                SET @cumAveragecount = (
                                       SELECT COUNT(*)
                                       FROM   syCreditSummary
                                       WHERE  StuEnrollId = @StuEnrollId
                                              AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                     SELECT SUM(FinalScore)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount;

                UPDATE syCreditSummary
                SET    Average = @TermAverage
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId;

                --Update Cumulative GPA
                UPDATE syCreditSummary
                SET    CumAverage = @CumAverage
                WHERE  StuEnrollId = @StuEnrollId;


                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@GradeSystemDetailId
                    ,@FinalGrade
                    ,@CurrentGrade
                    ,@CourseCredits
                    ,@ClsSectionId
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@GPA
                    ,@IsGPA
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;

        DECLARE GetCreditsSummary_Cursor CURSOR FOR
            SELECT     DISTINCT SE.StuEnrollId
                               ,T.TermId
                               ,T.TermDescrip
                               ,T.StartDate
                               ,R.ReqId
                               ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                               ,NULL AS FinalScore
                               ,RES.GrdSysDetailId AS GradeSystemDetailId
                               ,GSD.Grade AS FinalGrade
                               ,GSD.Grade AS CurrentGrade
                               ,R.Credits
                               ,NULL AS ClsSectionId
                               ,GSD.IsPass
                               ,GSD.IsCreditsAttempted
                               ,GSD.IsCreditsEarned
                               ,GSD.GPA
                               ,GSD.IsInGPA
                               ,SE.PrgVerId
                               ,GSD.IsInGPA
                               ,R.FinAidCredits AS FinAidCredits
            FROM       arStuEnrollments SE
            INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
            INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
            INNER JOIN arReqs R ON RES.ReqId = R.ReqId
            INNER JOIN arTerm T ON RES.TermId = T.TermId
            WHERE      SE.StuEnrollId NOT IN (
                                             SELECT DISTINCT StuEnrollId
                                             FROM   syCreditSummary
                                             )
                       AND SE.StuEnrollId = @StuEnrollId;

        SET @varGradeRounding = (
                                SELECT Value
                                FROM   syConfigAppSetValues
                                WHERE  SettingId = 45
                                );

        OPEN GetCreditsSummary_Cursor;
        SET @PrevStuEnrollId = NULL;
        SET @PrevTermId = NULL;
        SET @PrevReqId = NULL;
        SET @RowCount = 0;
        FETCH NEXT FROM GetCreditsSummary_Cursor
        INTO @StuEnrollId
            ,@TermId
            ,@TermDescrip
            ,@termstartdate1
            ,@reqid
            ,@CourseCodeDescrip
            ,@FinalScore
            ,@GradeSystemDetailId
            ,@FinalGrade
            ,@CurrentGrade
            ,@CourseCredits
            ,@ClsSectionId
            ,@IsPass
            ,@IsCreditsAttempted
            ,@IsCreditsEarned
            ,@GPA
            ,@IsGPA
            ,@PrgVerId
            ,@IsInGPA
            ,@FinAidCredits;
        WHILE @@FETCH_STATUS = 0
            BEGIN

                -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                IF ( LOWER(@varGradeRounding) = ''yes'' )
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @FinalScore = ROUND(@FinalScore, 0);
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;
                ELSE
                    BEGIN
                        IF @FinalScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = @FinalScore;
                            END;
                        IF @FinalScore IS NULL
                           AND @CurrentScore IS NOT NULL
                            BEGIN
                                SET @CurrentScore = ROUND(@CurrentScore, 0);
                            END;
                    END;

                UPDATE syCreditSummary
                SET    FinalScore = @FinalScore
                      ,CurrentScore = @CurrentScore
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId
                       AND ReqId = @reqid;

                --Average calculation
                SET @TermAverageCount = (
                                        SELECT COUNT(*)
                                        FROM   syCreditSummary
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND TermId = @TermId
                                               AND FinalScore IS NOT NULL
                                        );
                SET @termAverageSum = (
                                      SELECT SUM(FinalScore)
                                      FROM   syCreditSummary
                                      WHERE  StuEnrollId = @StuEnrollId
                                             AND TermId = @TermId
                                             AND FinalScore IS NOT NULL
                                      );
                SET @TermAverage = @termAverageSum / @TermAverageCount;

                -- Cumulative Average
                SET @cumAveragecount = (
                                       SELECT COUNT(*)
                                       FROM   syCreditSummary
                                       WHERE  StuEnrollId = @StuEnrollId
                                              AND FinalScore IS NOT NULL
                                       );
                SET @cumAverageSum = (
                                     SELECT SUM(FinalScore)
                                     FROM   syCreditSummary
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                     );
                SET @CumAverage = @cumAverageSum / @cumAveragecount;

                UPDATE syCreditSummary
                SET    Average = @TermAverage
                WHERE  StuEnrollId = @StuEnrollId
                       AND TermId = @TermId;

                --Update Cumulative GPA
                UPDATE syCreditSummary
                SET    CumAverage = @CumAverage
                WHERE  StuEnrollId = @StuEnrollId;


                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@GradeSystemDetailId
                    ,@FinalGrade
                    ,@CurrentGrade
                    ,@CourseCredits
                    ,@ClsSectionId
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@GPA
                    ,@IsGPA
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
            END;
        CLOSE GetCreditsSummary_Cursor;
        DEALLOCATE GetCreditsSummary_Cursor;


        SET @TermAverageCount = (
                                SELECT COUNT(*)
                                FROM   syCreditSummary
                                WHERE  StuEnrollId = @StuEnrollId
                                       AND FinalScore IS NOT NULL
                                );
        SET @termAverageSum = (
                              SELECT SUM(FinalScore)
                              FROM   syCreditSummary
                              WHERE  StuEnrollId = @StuEnrollId
                                     AND FinalScore IS NOT NULL
                              );
        IF @TermAverageCount >= 1
            BEGIN
                SET @TermAverage = @termAverageSum / @TermAverageCount;
            END;

        -- Cumulative Average
        SET @cumAveragecount = (
                               SELECT COUNT(*)
                               FROM   syCreditSummary
                               WHERE  StuEnrollId = @StuEnrollId
                                      AND FinalScore IS NOT NULL
                               );
        ---- PRINT @cumAveragecount

        SET @cumAverageSum = (
                             SELECT SUM(FinalScore)
                             FROM   syCreditSummary
                             WHERE  StuEnrollId = @StuEnrollId
                                    AND FinalScore IS NOT NULL
                             );
        ---- PRINT @CumAverageSum

        IF @cumAveragecount >= 1
            BEGIN
                SET @CumAverage = @cumAverageSum / @cumAveragecount;
            END;
        ---- PRINT @CumAverage

        ---- PRINT @UnitTypeId;

        ---- PRINT @TrackSapAttendance;
        ---- PRINT @displayHours;

        DECLARE @UseWeightForGPA INT = 0;
        SET @UseWeightForGPA = (
                               SELECT TOP 1 PV.DoCourseWeightOverallGPA
                               FROM   dbo.arStuEnrollments E
                               JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
                               WHERE  E.StuEnrollId = @StuEnrollId
                               );

        IF @UseWeightForGPA = 1
            BEGIN
                DECLARE @TotalWeight DECIMAL = (
                                               SELECT SUM(PVD.CourseWeight)
                                               FROM   dbo.syCreditSummary CS
                                               JOIN   dbo.arProgVerDef PVD ON PVD.ReqId = CS.ReqId
                                               WHERE  StuEnrollId = @StuEnrollId
                                               );

                DECLARE @ExpectedCoursesGraded INT = (
                                                     SELECT COUNT(CS.ReqId)
                                                     FROM   dbo.syCreditSummary CS
                                                     JOIN   dbo.arProgVerDef PVD ON PVD.ReqId = CS.ReqId
                                                     WHERE  StuEnrollId = @StuEnrollId
                                                     );
                DECLARE @CoursesGraded INT = (
                                             SELECT COUNT(CS.ReqId)
                                             FROM   dbo.syCreditSummary CS
                                             JOIN   dbo.arProgVerDef PVD ON PVD.ReqId = CS.ReqId
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND CS.CurrentScore IS NOT NULL
                                                    AND CS.Completed = 1
                                             );

                SET @CumAverage = CASE WHEN @TotalWeight = 100
                                            AND @ExpectedCoursesGraded = @CoursesGraded THEN (
                                                                                             SELECT SUM(PVD.CourseWeight / 100 * CS.CurrentScore)
                                                                                             FROM   dbo.syCreditSummary CS
                                                                                             JOIN   dbo.arProgVerDef PVD ON PVD.ReqId = CS.ReqId
                                                                                             WHERE  StuEnrollId = @StuEnrollId
                                                                                             )
                                       ELSE ((
                                             SELECT SUM(CS.CurrentScore)
                                             FROM   dbo.syCreditSummary CS
                                             JOIN   dbo.arProgVerDef PVD ON PVD.ReqId = CS.ReqId
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND CS.CurrentScore IS NOT NULL
                                             ) / (
                                                 SELECT COUNT(CS.CurrentScore)
                                                 FROM   dbo.syCreditSummary CS
                                                 JOIN   dbo.arProgVerDef PVD ON PVD.ReqId = CS.ReqId
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND CS.CurrentScore IS NOT NULL
                                                 )
                                            )
                                  END;
            END;

		--If Clock Hour / Numeric - use New Average Calculation
		IF (
		   SELECT TOP 1 P.ACId
		   FROM   dbo.arStuEnrollments E
		   JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
		   JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
		   WHERE  E.StuEnrollId = @StuEnrollId
		   ) = 5
		   AND @GradesFormat = ''numeric''
			BEGIN
				EXEC dbo.USP_GPACalculator @EnrollmentId = @StuEnrollId
                                          ,@StudentGPA = @CumAverage OUTPUT;
			END;

        -- Main query starts here 
        SELECT     DISTINCT TOP 1 S.SSN
                                 ,S.FirstName
                                 ,S.LastName
                                 ,SC.StatusCodeDescrip
                                 ,SE.StartDate AS StudentStartDate
                                 ,SE.ExpGradDate AS StudentExpectedGraduationDate
                                 ,PV.PrgVerId AS PrgVerId
                                 ,PV.PrgVerDescrip AS PrgVerDescrip
                                 ,PV.UnitTypeId AS UnitTypeId
                                 ,CASE WHEN ( PV.Credits > 0.0 ) THEN 1
                                       ELSE 0
                                  END AS PrgVersionTrackCredits
                                 ,AAUT1.UnitTypeDescrip AS UnitTypeDescripFrom_arAttUnitType
                                 ,CASE WHEN @TermStartDate IS NULL THEN (
                                                                        SELECT MAX(LDA)
                                                                        FROM   (
                                                                               SELECT MAX(AttendedDate) AS LDA
                                                                               FROM   arExternshipAttendance
                                                                               WHERE  StuEnrollId = @StuEnrollId
                                                                               UNION ALL
                                                                               SELECT MAX(MeetDate) AS LDA
                                                                               FROM   atClsSectAttendance
                                                                               WHERE  StuEnrollId = @StuEnrollId
                                                                                      AND Actual > 0
                                                                                      AND Actual <> 99.00
                                                                                      AND Actual <> 999.00
                                                                                      AND Actual <> 9999.00
                                                                               UNION ALL
                                                                               --SELECT    MAX(AttendanceDate) AS LDA
                                                                               --FROM      atAttendance
                                                                               --WHERE     EnrollId = @StuEnrollId
                                                                               --          AND Actual > 0
                                                                               --UNION ALL
                                                                               SELECT MAX(RecordDate) AS LDA
                                                                               FROM   arStudentClockAttendance
                                                                               WHERE  StuEnrollId = @StuEnrollId
                                                                                      AND (
                                                                                          ActualHours > 0
                                                                                          AND ActualHours <> 99.00
                                                                                          AND ActualHours <> 999.00
                                                                                          AND ActualHours <> 9999.00
                                                                                          )
                                                                               UNION ALL
                                                                               SELECT MAX(MeetDate) AS LDA
                                                                               FROM   atConversionAttendance
                                                                               WHERE  StuEnrollId = @StuEnrollId
                                                                                      AND (
                                                                                          Actual > 0
                                                                                          AND Actual <> 99.00
                                                                                          AND Actual <> 999.00
                                                                                          AND Actual <> 9999.00
                                                                                          )
                                                                               UNION ALL
                                                                               SELECT TOP 1 LDA
                                                                               FROM   arStuEnrollments
                                                                               WHERE  StuEnrollId = @StuEnrollId
                                                                               ) TR
                                                                        )
                                       ELSE
                                  -- Get the LDA Based on Term Start Date
                                  (
                                  SELECT MAX(LDA)
                                  FROM   (
                                         SELECT TOP 1 LDA
                                         FROM   arStuEnrollments
                                         WHERE  StuEnrollId = @StuEnrollId
                                         UNION ALL
                                         SELECT MAX(AttendedDate) AS LDA
                                         FROM   arExternshipAttendance
                                         WHERE  StuEnrollId = @StuEnrollId
                                         UNION ALL
                                         SELECT MAX(MeetDate) AS LDA
                                         FROM   atClsSectAttendance t1
                                               ,arClassSections t2
                                         WHERE  t1.ClsSectionId = t2.ClsSectionId
                                                AND StuEnrollId = @StuEnrollId
                                                AND Actual > 0
                                                AND Actual <> 99.00
                                                AND Actual <> 999.00
                                                AND Actual <> 9999.00
                                         UNION ALL
                                         SELECT MAX(RecordDate) AS LDA
                                         FROM   arStudentClockAttendance
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND (
                                                    ActualHours > 0
                                                    AND ActualHours <> 99.00
                                                    AND ActualHours <> 999.00
                                                    AND ActualHours <> 9999.00
                                                    )
                                         UNION ALL
                                         SELECT MAX(MeetDate) AS LDA
                                         FROM   atConversionAttendance
                                         WHERE  StuEnrollId = @StuEnrollId
                                                AND (
                                                    Actual > 0
                                                    AND Actual <> 99.00
                                                    AND Actual <> 999.00
                                                    AND Actual <> 9999.00
                                                    )
                                         ) TR
                                  )
                                  END AS StudentLastDateAttended
                                 ,( CASE WHEN ( AAUT1.UnitTypeId = ''2600592A-9739-4A13-BDCE-7A25FE4A7478'' ) -- NONE
                                 THEN        0.00
                                         ELSE (
                                              SELECT     SUM(ISNULL(APSD.total, 0))
                                              FROM       arProgScheduleDetails AS APSD
                                              INNER JOIN arProgSchedules AS APS ON APS.ScheduleId = APSD.ScheduleId
                                              INNER JOIN arPrgVersions AS APV ON APV.PrgVerId = APS.PrgVerId
                                              INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                                              INNER JOIN dbo.arStudentSchedules SS ON SS.StuEnrollId = ASE.StuEnrollId
                                                                                      AND SS.ScheduleId = APSD.ScheduleId
                                              WHERE      ASE.StuEnrollId = @StuEnrollId
                                              )
                                    END
                                  ) AS WeeklySchedule
                                 ,CASE WHEN (
                                            @TrackSapAttendance = ''byclass''
                                            AND @displayHours = ''hours''
                                            AND AAUT1.UnitTypeDescrip = ''Present Absent''
                                            ) -- PA
                                 THEN      @ActualPresentDays_ConvertTo_Hours
                                       ELSE CASE WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AttendedDays / 60
                                                          ELSE SAS_ForTerm.AttendedDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.AttendedDays / 60 )
                                                          ELSE ( SAS_ForTerm.AttendedDays / 60 )
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.AttendedDays / 60 )
                                                          ELSE ( SAS_ForTerm.AttendedDays / 60 )
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AttendedDays / 60
                                                          ELSE SAS_ForTerm.AttendedDays / 60
                                                     END
                                                 ELSE CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AttendedDays
                                                           ELSE SAS_ForTerm.AttendedDays
                                                      END
                                            END
                                  END AS TotalDaysAttended
                                 ,CASE WHEN (
                                            @TrackSapAttendance = ''byclass''
                                            AND @displayHours = ''hours''
                                            AND AAUT1.UnitTypeDescrip = ''Present Absent''
                                            ) THEN @Scheduledhours
                                       ELSE CASE WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                                                          ELSE SAS_ForTerm.ScheduledDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                                                          ELSE SAS_ForTerm.ScheduledDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                                                          ELSE SAS_ForTerm.ScheduledDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.ScheduledDays / 60
                                                          ELSE SAS_ForTerm.ScheduledDays / 60
                                                     END
                                                 ELSE CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.ScheduledDays )
                                                           ELSE ( SAS_ForTerm.ScheduledDays )
                                                      END
                                            END
                                  END AS ScheduledDays
                                 ,CASE WHEN (
                                            @TrackSapAttendance = ''byclass''
                                            AND @displayHours = ''hours''
                                            AND AAUT1.UnitTypeDescrip = ''Present Absent''
                                            ) THEN @ActualAbsentDays_ConvertTo_Hours
                                       ELSE CASE WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AbsentDays / 60
                                                          ELSE SAS_ForTerm.AbsentDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AbsentDays / 60
                                                          ELSE SAS_ForTerm.AbsentDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AbsentDays / 60
                                                          ELSE SAS_ForTerm.AbsentDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.AbsentDays / 60
                                                          ELSE SAS_ForTerm.AbsentDays / 60
                                                     END
                                                 ELSE CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.AbsentDays )
                                                           ELSE ( SAS_ForTerm.AbsentDays )
                                                      END
                                            END
                                  END AS DaysAbsent
                                 ,CASE WHEN (
                                            @TrackSapAttendance = ''byclass''
                                            AND LOWER(RTRIM(LTRIM(@displayHours))) = ''hours''
                                            AND AAUT1.UnitTypeDescrip = ''Present Absent''
                                            ) THEN SAS_ForTerm.MakeupDays
                                       ELSE CASE WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.MakeupDays / 60
                                                          ELSE SAS_ForTerm.MakeupDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''hours''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.MakeupDays / 60
                                                          ELSE SAS_ForTerm.MakeupDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Clock Hours''
                                                      ) -- Clock Hour
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.MakeupDays / 60
                                                          ELSE SAS_ForTerm.MakeupDays / 60
                                                     END
                                                 WHEN (
                                                      @TrackSapAttendance = ''byclass''
                                                      AND @displayHours = ''presentabsent''
                                                      AND AAUT1.UnitTypeDescrip = ''Minutes''
                                                      ) -- Minutes
                                 THEN                CASE WHEN @TermStartDate IS NULL THEN SAS_NoTerm.MakeupDays / 60
                                                          ELSE SAS_ForTerm.MakeupDays / 60
                                                     END
                                                 ELSE CASE WHEN @TermStartDate IS NULL THEN ( SAS_NoTerm.MakeupDays )
                                                           ELSE ( SAS_ForTerm.MakeupDays )
                                                      END
                                            END
                                  END AS MakeupDays
                                 ,SE.StuEnrollId
                                 ,@SUMCreditsAttempted AS CreditsAttempted
                                 ,@SUMCreditsEarned AS CreditsEarned
                                 ,@SUMFACreditsEarned AS FACreditsEarned
                                 ,SE.DateDetermined AS DateDetermined
                                 ,SYS.InSchool
                                 ,P.ProgDescrip
                                 ,(
                                  SELECT SUM(ISNULL(TransAmount, 0))
                                  FROM   saTransactions t1
                                        ,saTransCodes t2
                                  WHERE  t1.TransCodeId = t2.TransCodeId
                                         AND t1.StuEnrollId = @StuEnrollId
                                         AND t2.IsInstCharge = 1
                                         AND t1.TransTypeId IN ( 0, 1 )
                                         AND t1.Voided = 0
                                  ) AS TotalCost
                                 ,(
                                  SELECT SUM(ISNULL(TransAmount, 0)) AS CurrentBalance
                                  FROM   saTransactions
                                  WHERE  StuEnrollId = @StuEnrollId
                                         AND Voided = 0
                                  ) AS CurrentBalance
                                 ,@CumAverage AS OverallAverage
                                 ,@cumWeightedGPA AS WeightedAverage_CumGPA
                                 ,@cumSimpleGPA AS SimpleAverage_CumGPA
                                 ,@ReturnValue AS StudentGroups
                                 ,CASE WHEN P.ACId = 5 THEN ''True''
                                       ELSE ''False''
                                  END AS ClockHourProgram
                                 ,CASE WHEN AAUT1.UnitTypeDescrip = ''Present Absent'' --LTRIM(RTRIM(PV.UnitTypeId)) = ''EF5535C2-142C-4223-AE3C-25A50A153CC6''    Present Absent
                                            AND @displayHours = ''hours'' THEN ''Hours''
                                       WHEN AAUT1.UnitTypeDescrip = ''Present Absent'' --LTRIM(RTRIM(PV.UnitTypeId)) = ''EF5535C2-142C-4223-AE3C-25A50A153CC6''      Present Absent
                                            AND @displayHours <> ''hours'' THEN ''Days''
                                       ELSE ''Hours''
                                  END AS UnitTypeDescrip
                                 ,SE.TransferHours AS TransferHours
                                 ,SE.ContractedGradDate AS ContractedGradDate
        FROM       arStudent S
        INNER JOIN arStuEnrollments SE ON S.StudentId = SE.StudentId
        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
        INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = PV.UnitTypeId
        INNER JOIN arPrograms P ON PV.ProgId = P.ProgId
        INNER JOIN syStatusCodes SC ON SE.StatusCodeId = SC.StatusCodeId
        INNER JOIN sySysStatus SYS ON SC.SysStatusId = SYS.SysStatusId
        LEFT JOIN  (
                   SELECT   StuEnrollId
                           ,MAX(ActualRunningScheduledDays) AS ScheduledDays
                           ,MAX(AdjustedPresentDays) AS AttendedDays
                           ,MAX(AdjustedAbsentDays) AS AbsentDays
                           ,MAX(ActualRunningMakeupDays) AS MakeupDays
                           ,MAX(StudentAttendedDate) AS LDA
                   FROM     syStudentAttendanceSummary
                   GROUP BY StuEnrollId
                   ) SAS_NoTerm ON SE.StuEnrollId = SAS_NoTerm.StuEnrollId
        LEFT JOIN  (
                   SELECT   StuEnrollId
                           ,MAX(ActualRunningScheduledDays) AS ScheduledDays
                           ,MAX(AdjustedPresentDays) AS AttendedDays
                           ,MAX(AdjustedAbsentDays) AS AbsentDays
                           ,MAX(ActualRunningMakeupDays) AS MakeupDays
                           ,MAX(StudentAttendedDate) AS LDA
                   FROM     syStudentAttendanceSummary
                   GROUP BY StuEnrollId
                   ) SAS_ForTerm ON SE.StuEnrollId = SAS_ForTerm.StuEnrollId
        WHERE      ( SE.StuEnrollId = @StuEnrollId );
    END;
-- =========================================================================================================
-- END  --  Usp_PR_Sub2_Enrollment_Summary
-- =========================================================================================================

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[USP_TitleIV_QualitativeAndQuantitative]'
GO
IF OBJECT_ID(N'[dbo].[USP_TitleIV_QualitativeAndQuantitative]', 'P') IS NULL
EXEC sp_executesql N'-- =============================================
-- Author:		<Author,,Edwin Sosa>
-- Create date: <Create Date,,>
-- Description:	<This procedure calculates the qualitative and quantitative values for Title IV by using an offset date. The attendance portion of this procedure
-- removes the makeup hours from the actual hours before storing it into the sy attendance summary table. This differs slightly from the sprocs called in the attendance job to acheive the correct results.
-- When calculating the quantitative value, the makeup hours are added back into the actuals to get a consisten result. This sproc should be refactored to use the same procedures the attendance job uses internally.>
-- =============================================
CREATE PROCEDURE [dbo].[USP_TitleIV_QualitativeAndQuantitative]
    -- Add the parameters for the stored procedure here
    @StuEnrollId VARCHAR(MAX) = NULL
   ,@QuantMinUnitTypeId INTEGER
   ,@OffsetDate DATETIME
   ,@CumAverage DECIMAL(18, 2) OUTPUT
   ,@cumWeightedGPA DECIMAL(18, 2) OUTPUT
   ,@cumSimpleGPA DECIMAL(18, 2) OUTPUT
   ,@Qualitative DECIMAL(18, 2) OUTPUT
   ,@Quantitative DECIMAL(18, 2) OUTPUT
AS
    BEGIN
        -- SET NOCOUNT ON added to prevent extra result sets from
        -- interfering with SELECT statements.
        SET NOCOUNT ON;

        DECLARE @attendanceSummary TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,ClsSectionId UNIQUEIDENTIFIER
               ,StudentAttendedDate DATETIME
               ,ScheduledDays DECIMAL(18, 2)
               ,ActualDays DECIMAL(18, 2)
               ,ActualRunningScheduledDays DECIMAL(18, 2)
               ,ActualRunningPresentDays DECIMAL(18, 2)
               ,ActualRunningAbsentDays DECIMAL(18, 2)
               ,ActualRunningMakeupDays DECIMAL(18, 2)
               ,ActualRunningTardyDays DECIMAL(18, 2)
               ,AdjustedPresentDays DECIMAL(18, 2)
               ,AdjustedAbsentDays DECIMAL(18, 2)
               ,AttendanceTrackType VARCHAR(50)
               ,ModUser VARCHAR(50)
               ,ModDate DATETIME
               ,TardiesMakingAbsence INT
            );
        -- Insert statements for procedure here
        DECLARE @ClsSectionIntervalMinutes AS TABLE
            (
                ClsSectionId UNIQUEIDENTIFIER NOT NULL
               ,IntervalInMinutes DECIMAL(18, 2)
            );

        DECLARE @TrackSapAttendance VARCHAR(1000);
        DECLARE @displayHours AS VARCHAR(1000);
        DECLARE @GradeCourseRepetitionsMethod VARCHAR(1000);
        DECLARE @GradesFormat AS VARCHAR(1000);
        DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;

        SET @StuEnrollCampusId = COALESCE((
                                          SELECT ASE.CampusId
                                          FROM   arStuEnrollments AS ASE
                                          WHERE  ASE.StuEnrollId = @StuEnrollId
                                          )
                                         ,NULL
                                         );
        SET @TrackSapAttendance = dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'', @StuEnrollCampusId);
        IF ( @TrackSapAttendance IS NOT NULL )
            BEGIN
                SET @TrackSapAttendance = LOWER(LTRIM(RTRIM(@TrackSapAttendance)));
            END;

        SET @GradeCourseRepetitionsMethod = dbo.GetAppSettingValueByKeyName(''GradeCourseRepetitionsMethod'', @StuEnrollCampusId);
        SET @GradesFormat = dbo.GetAppSettingValueByKeyName(''GradesFormat'', @StuEnrollCampusId);
        IF ( @GradesFormat IS NOT NULL )
            BEGIN
                SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
            END;



        DECLARE @CoursesNotRepeated TABLE
            (
                StuEnrollId UNIQUEIDENTIFIER
               ,TermId UNIQUEIDENTIFIER
               ,TermDescrip VARCHAR(100)
               ,ReqId UNIQUEIDENTIFIER
               ,ReqDescrip VARCHAR(100)
               ,ClsSectionId VARCHAR(50)
               ,CreditsEarned DECIMAL(18, 2)
               ,CreditsAttempted DECIMAL(18, 2)
               ,CurrentScore DECIMAL(18, 2)
               ,CurrentGrade VARCHAR(10)
               ,FinalScore DECIMAL(18, 2)
               ,FinalGrade VARCHAR(10)
               ,Completed BIT
               ,FinalGPA DECIMAL(18, 2)
               ,Product_WeightedAverage_Credits_GPA DECIMAL(18, 2)
               ,Count_WeightedAverage_Credits DECIMAL(18, 2)
               ,Product_SimpleAverage_Credits_GPA DECIMAL(18, 2)
               ,Count_SimpleAverage_Credits DECIMAL(18, 2)
               ,ModUser VARCHAR(50)
               ,ModDate DATETIME
               ,TermGPA_Simple DECIMAL(18, 2)
               ,TermGPA_Weighted DECIMAL(18, 2)
               ,coursecredits DECIMAL(18, 2)
               ,CumulativeGPA DECIMAL(18, 2)
               ,CumulativeGPA_Simple DECIMAL(18, 2)
               ,FACreditsEarned DECIMAL(18, 2)
               ,Average DECIMAL(18, 2)
               ,CumAverage DECIMAL(18, 2)
               ,TermStartDate DATETIME
               ,rownumber INT
            );

        INSERT INTO @CoursesNotRepeated
                    SELECT StuEnrollId
                          ,TermId
                          ,TermDescrip
                          ,ReqId
                          ,ReqDescrip
                          ,ClsSectionId
                          ,CreditsEarned
                          ,CreditsAttempted
                          ,CurrentScore
                          ,CurrentGrade
                          ,FinalScore
                          ,FinalGrade
                          ,Completed
                          ,FinalGPA
                          ,Product_WeightedAverage_Credits_GPA
                          ,Count_WeightedAverage_Credits
                          ,Product_SimpleAverage_Credits_GPA
                          ,Count_SimpleAverage_Credits
                          ,ModUser
                          ,ModDate
                          ,TermGPA_Simple
                          ,TermGPA_Weighted
                          ,coursecredits
                          ,CumulativeGPA
                          ,CumulativeGPA_Simple
                          ,FACreditsEarned
                          ,Average
                          ,CumAverage
                          ,TermStartDate
                          ,NULL AS rownumber
                    FROM   (
                           SELECT     sCS.StuEnrollId
                                     ,sCS.TermId
                                     ,sCS.TermDescrip
                                     ,sCS.ReqId
                                     ,sCS.ReqDescrip
                                     ,sCS.ClsSectionId
                                     ,sCS.CreditsEarned
                                     ,sCS.CreditsAttempted
                                     ,sCS.CurrentScore
                                     ,sCS.CurrentGrade
                                     ,sCS.FinalScore
                                     ,sCS.FinalGrade
                                     ,sCS.Completed
                                     ,sCS.FinalGPA
                                     ,sCS.Product_WeightedAverage_Credits_GPA
                                     ,sCS.Count_WeightedAverage_Credits
                                     ,sCS.Product_SimpleAverage_Credits_GPA
                                     ,sCS.Count_SimpleAverage_Credits
                                     ,sCS.ModUser
                                     ,sCS.ModDate
                                     ,sCS.TermGPA_Simple
                                     ,sCS.TermGPA_Weighted
                                     ,sCS.coursecredits
                                     ,sCS.CumulativeGPA
                                     ,sCS.CumulativeGPA_Simple
                                     ,sCS.FACreditsEarned
                                     ,sCS.Average
                                     ,sCS.CumAverage
                                     ,sCS.TermStartDate
                           FROM       dbo.syCreditSummary sCS
                           INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                           INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                           WHERE      sCS.StuEnrollId = @StuEnrollId
                                      AND CS.ReqId = sCS.ReqId
                                      AND (
                                          R.DateCompleted IS NOT NULL
                                          AND R.DateCompleted <= @OffsetDate
                                          )
                           UNION ALL
                           (SELECT     sCS.StuEnrollId
                                      ,sCS.TermId
                                      ,sCS.TermDescrip
                                      ,sCS.ReqId
                                      ,sCS.ReqDescrip
                                      ,sCS.ClsSectionId
                                      ,sCS.CreditsEarned
                                      ,sCS.CreditsAttempted
                                      ,sCS.CurrentScore
                                      ,sCS.CurrentGrade
                                      ,sCS.FinalScore
                                      ,sCS.FinalGrade
                                      ,sCS.Completed
                                      ,sCS.FinalGPA
                                      ,sCS.Product_WeightedAverage_Credits_GPA
                                      ,sCS.Count_WeightedAverage_Credits
                                      ,sCS.Product_SimpleAverage_Credits_GPA
                                      ,sCS.Count_SimpleAverage_Credits
                                      ,sCS.ModUser
                                      ,sCS.ModDate
                                      ,sCS.TermGPA_Simple
                                      ,sCS.TermGPA_Weighted
                                      ,sCS.coursecredits
                                      ,sCS.CumulativeGPA
                                      ,sCS.CumulativeGPA_Simple
                                      ,sCS.FACreditsEarned
                                      ,sCS.Average
                                      ,sCS.CumAverage
                                      ,sCS.TermStartDate
                            FROM       dbo.syCreditSummary sCS
                            INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                            WHERE      sCS.StuEnrollId = @StuEnrollId
                                       AND tG.ReqId = sCS.ReqId
                                       AND (
                                           tG.CompletedDate IS NOT NULL
                                           AND tG.CompletedDate <= @OffsetDate
                                           ))
                           ) sCSEA
                    WHERE  StuEnrollId = @StuEnrollId
                           AND ReqId IN (
                                        SELECT ReqId
                                        FROM   (
                                               SELECT   ReqId
                                                       ,ReqDescrip
                                                       ,COUNT(*) AS counter
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                               GROUP BY ReqId
                                                       ,ReqDescrip
                                               HAVING   COUNT(*) = 1
                                               ) dt
                                        );



        IF LOWER(@GradeCourseRepetitionsMethod) = ''latest''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT   dt2.StuEnrollId
                                    ,dt2.TermId
                                    ,dt2.TermDescrip
                                    ,dt2.ReqId
                                    ,dt2.ReqDescrip
                                    ,dt2.ClsSectionId
                                    ,dt2.CreditsEarned
                                    ,dt2.CreditsAttempted
                                    ,dt2.CurrentScore
                                    ,dt2.CurrentGrade
                                    ,dt2.FinalScore
                                    ,dt2.FinalGrade
                                    ,dt2.Completed
                                    ,dt2.FinalGPA
                                    ,dt2.Product_WeightedAverage_Credits_GPA
                                    ,dt2.Count_WeightedAverage_Credits
                                    ,dt2.Product_SimpleAverage_Credits_GPA
                                    ,dt2.Count_SimpleAverage_Credits
                                    ,dt2.ModUser
                                    ,dt2.ModDate
                                    ,dt2.TermGPA_Simple
                                    ,dt2.TermGPA_Weighted
                                    ,dt2.coursecredits
                                    ,dt2.CumulativeGPA
                                    ,dt2.CumulativeGPA_Simple
                                    ,dt2.FACreditsEarned
                                    ,dt2.Average
                                    ,dt2.CumAverage
                                    ,dt2.TermStartDate
                                    ,dt2.RowNumber
                            FROM     (
                                     SELECT StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                                ORDER BY TermStartDate DESC
                                                              ) AS RowNumber
                                     FROM   (
                                            SELECT     sCS.StuEnrollId
                                                      ,sCS.TermId
                                                      ,sCS.TermDescrip
                                                      ,sCS.ReqId
                                                      ,sCS.ReqDescrip
                                                      ,sCS.ClsSectionId
                                                      ,sCS.CreditsEarned
                                                      ,sCS.CreditsAttempted
                                                      ,sCS.CurrentScore
                                                      ,sCS.CurrentGrade
                                                      ,sCS.FinalScore
                                                      ,sCS.FinalGrade
                                                      ,sCS.Completed
                                                      ,sCS.FinalGPA
                                                      ,sCS.Product_WeightedAverage_Credits_GPA
                                                      ,sCS.Count_WeightedAverage_Credits
                                                      ,sCS.Product_SimpleAverage_Credits_GPA
                                                      ,sCS.Count_SimpleAverage_Credits
                                                      ,sCS.ModUser
                                                      ,sCS.ModDate
                                                      ,sCS.TermGPA_Simple
                                                      ,sCS.TermGPA_Weighted
                                                      ,sCS.coursecredits
                                                      ,sCS.CumulativeGPA
                                                      ,sCS.CumulativeGPA_Simple
                                                      ,sCS.FACreditsEarned
                                                      ,sCS.Average
                                                      ,sCS.CumAverage
                                                      ,sCS.TermStartDate
                                            FROM       dbo.syCreditSummary sCS
                                            INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                            INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                            WHERE      sCS.StuEnrollId = @StuEnrollId
                                                       AND CS.ReqId = sCS.ReqId
                                                       AND (
                                                           R.DateCompleted IS NOT NULL
                                                           AND R.DateCompleted <= @OffsetDate
                                                           )
                                            UNION ALL
                                            (SELECT     sCS.StuEnrollId
                                                       ,sCS.TermId
                                                       ,sCS.TermDescrip
                                                       ,sCS.ReqId
                                                       ,sCS.ReqDescrip
                                                       ,sCS.ClsSectionId
                                                       ,sCS.CreditsEarned
                                                       ,sCS.CreditsAttempted
                                                       ,sCS.CurrentScore
                                                       ,sCS.CurrentGrade
                                                       ,sCS.FinalScore
                                                       ,sCS.FinalGrade
                                                       ,sCS.Completed
                                                       ,sCS.FinalGPA
                                                       ,sCS.Product_WeightedAverage_Credits_GPA
                                                       ,sCS.Count_WeightedAverage_Credits
                                                       ,sCS.Product_SimpleAverage_Credits_GPA
                                                       ,sCS.Count_SimpleAverage_Credits
                                                       ,sCS.ModUser
                                                       ,sCS.ModDate
                                                       ,sCS.TermGPA_Simple
                                                       ,sCS.TermGPA_Weighted
                                                       ,sCS.coursecredits
                                                       ,sCS.CumulativeGPA
                                                       ,sCS.CumulativeGPA_Simple
                                                       ,sCS.FACreditsEarned
                                                       ,sCS.Average
                                                       ,sCS.CumAverage
                                                       ,sCS.TermStartDate
                                             FROM       dbo.syCreditSummary sCS
                                             INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                             WHERE      sCS.StuEnrollId = @StuEnrollId
                                                        AND tG.ReqId = sCS.ReqId
                                                        AND (
                                                            tG.CompletedDate IS NOT NULL
                                                            AND tG.CompletedDate <= @OffsetDate
                                                            ))
                                            ) sCSEA
                                     WHERE  StuEnrollId = @StuEnrollId
                                            AND (
                                                FinalScore IS NOT NULL
                                                OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN (
                                                         SELECT ReqId
                                                         FROM   (
                                                                SELECT   ReqId
                                                                        ,ReqDescrip
                                                                        ,COUNT(*) AS Counter
                                                                FROM     syCreditSummary
                                                                WHERE    StuEnrollId = @StuEnrollId
                                                                GROUP BY ReqId
                                                                        ,ReqDescrip
                                                                HAVING   COUNT(*) > 1
                                                                ) dt
                                                         )
                                     ) dt2
                            WHERE    RowNumber = 1
                            ORDER BY TermStartDate DESC;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''best''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                            FROM   (
                                   SELECT StuEnrollId
                                         ,TermId
                                         ,TermDescrip
                                         ,ReqId
                                         ,ReqDescrip
                                         ,ClsSectionId
                                         ,CreditsEarned
                                         ,CreditsAttempted
                                         ,CurrentScore
                                         ,CurrentGrade
                                         ,FinalScore
                                         ,FinalGrade
                                         ,Completed
                                         ,FinalGPA
                                         ,Product_WeightedAverage_Credits_GPA
                                         ,Count_WeightedAverage_Credits
                                         ,Product_SimpleAverage_Credits_GPA
                                         ,Count_SimpleAverage_Credits
                                         ,ModUser
                                         ,ModDate
                                         ,TermGPA_Simple
                                         ,TermGPA_Weighted
                                         ,coursecredits
                                         ,CumulativeGPA
                                         ,CumulativeGPA_Simple
                                         ,FACreditsEarned
                                         ,Average
                                         ,CumAverage
                                         ,TermStartDate
                                         ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                              ORDER BY Completed DESC
                                                                      ,CreditsEarned DESC
                                                                      ,FinalGPA DESC
                                                            ) AS RowNumber
                                   FROM   (
                                          SELECT     sCS.StuEnrollId
                                                    ,sCS.TermId
                                                    ,sCS.TermDescrip
                                                    ,sCS.ReqId
                                                    ,sCS.ReqDescrip
                                                    ,sCS.ClsSectionId
                                                    ,sCS.CreditsEarned
                                                    ,sCS.CreditsAttempted
                                                    ,sCS.CurrentScore
                                                    ,sCS.CurrentGrade
                                                    ,sCS.FinalScore
                                                    ,sCS.FinalGrade
                                                    ,sCS.Completed
                                                    ,sCS.FinalGPA
                                                    ,sCS.Product_WeightedAverage_Credits_GPA
                                                    ,sCS.Count_WeightedAverage_Credits
                                                    ,sCS.Product_SimpleAverage_Credits_GPA
                                                    ,sCS.Count_SimpleAverage_Credits
                                                    ,sCS.ModUser
                                                    ,sCS.ModDate
                                                    ,sCS.TermGPA_Simple
                                                    ,sCS.TermGPA_Weighted
                                                    ,sCS.coursecredits
                                                    ,sCS.CumulativeGPA
                                                    ,sCS.CumulativeGPA_Simple
                                                    ,sCS.FACreditsEarned
                                                    ,sCS.Average
                                                    ,sCS.CumAverage
                                                    ,sCS.TermStartDate
                                          FROM       dbo.syCreditSummary sCS
                                          INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                          INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                          WHERE      sCS.StuEnrollId = @StuEnrollId
                                                     AND CS.ReqId = sCS.ReqId
                                                     AND (
                                                         R.DateCompleted IS NOT NULL
                                                         AND R.DateCompleted <= @OffsetDate
                                                         )
                                          UNION ALL
                                          (SELECT     sCS.StuEnrollId
                                                     ,sCS.TermId
                                                     ,sCS.TermDescrip
                                                     ,sCS.ReqId
                                                     ,sCS.ReqDescrip
                                                     ,sCS.ClsSectionId
                                                     ,sCS.CreditsEarned
                                                     ,sCS.CreditsAttempted
                                                     ,sCS.CurrentScore
                                                     ,sCS.CurrentGrade
                                                     ,sCS.FinalScore
                                                     ,sCS.FinalGrade
                                                     ,sCS.Completed
                                                     ,sCS.FinalGPA
                                                     ,sCS.Product_WeightedAverage_Credits_GPA
                                                     ,sCS.Count_WeightedAverage_Credits
                                                     ,sCS.Product_SimpleAverage_Credits_GPA
                                                     ,sCS.Count_SimpleAverage_Credits
                                                     ,sCS.ModUser
                                                     ,sCS.ModDate
                                                     ,sCS.TermGPA_Simple
                                                     ,sCS.TermGPA_Weighted
                                                     ,sCS.coursecredits
                                                     ,sCS.CumulativeGPA
                                                     ,sCS.CumulativeGPA_Simple
                                                     ,sCS.FACreditsEarned
                                                     ,sCS.Average
                                                     ,sCS.CumAverage
                                                     ,sCS.TermStartDate
                                           FROM       dbo.syCreditSummary sCS
                                           INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                           WHERE      sCS.StuEnrollId = @StuEnrollId
                                                      AND tG.ReqId = sCS.ReqId
                                                      AND (
                                                          tG.CompletedDate IS NOT NULL
                                                          AND tG.CompletedDate <= @OffsetDate
                                                          ))
                                          ) sCSEA
                                   WHERE  StuEnrollId = @StuEnrollId
                                          AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                              )
                                          AND ReqId IN (
                                                       SELECT ReqId
                                                       FROM   (
                                                              SELECT   ReqId
                                                                      ,ReqDescrip
                                                                      ,COUNT(*) AS Counter
                                                              FROM     syCreditSummary
                                                              WHERE    StuEnrollId = @StuEnrollId
                                                              GROUP BY ReqId
                                                                      ,ReqDescrip
                                                              HAVING   COUNT(*) > 1
                                                              ) dt
                                                       )
                                   ) dt2
                            WHERE  RowNumber = 1;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''average''
            BEGIN
                INSERT INTO @CoursesNotRepeated
                            SELECT dt2.StuEnrollId
                                  ,dt2.TermId
                                  ,dt2.TermDescrip
                                  ,dt2.ReqId
                                  ,dt2.ReqDescrip
                                  ,dt2.ClsSectionId
                                  ,dt2.CreditsEarned
                                  ,dt2.CreditsAttempted
                                  ,dt2.CurrentScore
                                  ,dt2.CurrentGrade
                                  ,dt2.FinalScore
                                  ,dt2.FinalGrade
                                  ,dt2.Completed
                                  ,dt2.FinalGPA
                                  ,dt2.Product_WeightedAverage_Credits_GPA
                                  ,dt2.Count_WeightedAverage_Credits
                                  ,dt2.Product_SimpleAverage_Credits_GPA
                                  ,dt2.Count_SimpleAverage_Credits
                                  ,dt2.ModUser
                                  ,dt2.ModDate
                                  ,dt2.TermGPA_Simple
                                  ,dt2.TermGPA_Weighted
                                  ,dt2.coursecredits
                                  ,dt2.CumulativeGPA
                                  ,dt2.CumulativeGPA_Simple
                                  ,dt2.FACreditsEarned
                                  ,dt2.Average
                                  ,dt2.CumAverage
                                  ,dt2.TermStartDate
                                  ,dt2.RowNumber
                            FROM   (
                                   SELECT StuEnrollId
                                         ,TermId
                                         ,TermDescrip
                                         ,ReqId
                                         ,ReqDescrip
                                         ,ClsSectionId
                                         ,CreditsEarned
                                         ,CreditsAttempted
                                         ,CurrentScore
                                         ,CurrentGrade
                                         ,FinalScore
                                         ,FinalGrade
                                         ,Completed
                                         ,FinalGPA
                                         ,Product_WeightedAverage_Credits_GPA
                                         ,Count_WeightedAverage_Credits
                                         ,Product_SimpleAverage_Credits_GPA
                                         ,Count_SimpleAverage_Credits
                                         ,ModUser
                                         ,ModDate
                                         ,TermGPA_Simple
                                         ,TermGPA_Weighted
                                         ,coursecredits
                                         ,CumulativeGPA
                                         ,CumulativeGPA_Simple
                                         ,FACreditsEarned
                                         ,Average
                                         ,CumAverage
                                         ,TermStartDate
                                         ,ROW_NUMBER() OVER ( PARTITION BY ReqId
                                                              ORDER BY FinalGPA DESC
                                                            ) AS RowNumber
                                   FROM   (
                                          SELECT     sCS.StuEnrollId
                                                    ,sCS.TermId
                                                    ,sCS.TermDescrip
                                                    ,sCS.ReqId
                                                    ,sCS.ReqDescrip
                                                    ,sCS.ClsSectionId
                                                    ,sCS.CreditsEarned
                                                    ,sCS.CreditsAttempted
                                                    ,sCS.CurrentScore
                                                    ,sCS.CurrentGrade
                                                    ,sCS.FinalScore
                                                    ,sCS.FinalGrade
                                                    ,sCS.Completed
                                                    ,sCS.FinalGPA
                                                    ,sCS.Product_WeightedAverage_Credits_GPA
                                                    ,sCS.Count_WeightedAverage_Credits
                                                    ,sCS.Product_SimpleAverage_Credits_GPA
                                                    ,sCS.Count_SimpleAverage_Credits
                                                    ,sCS.ModUser
                                                    ,sCS.ModDate
                                                    ,sCS.TermGPA_Simple
                                                    ,sCS.TermGPA_Weighted
                                                    ,sCS.coursecredits
                                                    ,sCS.CumulativeGPA
                                                    ,sCS.CumulativeGPA_Simple
                                                    ,sCS.FACreditsEarned
                                                    ,sCS.Average
                                                    ,sCS.CumAverage
                                                    ,sCS.TermStartDate
                                          FROM       dbo.syCreditSummary sCS
                                          INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                          INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                          WHERE      sCS.StuEnrollId = @StuEnrollId
                                                     AND CS.ReqId = sCS.ReqId
                                                     AND (
                                                         R.DateCompleted IS NOT NULL
                                                         AND R.DateCompleted <= @OffsetDate
                                                         )
                                          UNION ALL
                                          (SELECT     sCS.StuEnrollId
                                                     ,sCS.TermId
                                                     ,sCS.TermDescrip
                                                     ,sCS.ReqId
                                                     ,sCS.ReqDescrip
                                                     ,sCS.ClsSectionId
                                                     ,sCS.CreditsEarned
                                                     ,sCS.CreditsAttempted
                                                     ,sCS.CurrentScore
                                                     ,sCS.CurrentGrade
                                                     ,sCS.FinalScore
                                                     ,sCS.FinalGrade
                                                     ,sCS.Completed
                                                     ,sCS.FinalGPA
                                                     ,sCS.Product_WeightedAverage_Credits_GPA
                                                     ,sCS.Count_WeightedAverage_Credits
                                                     ,sCS.Product_SimpleAverage_Credits_GPA
                                                     ,sCS.Count_SimpleAverage_Credits
                                                     ,sCS.ModUser
                                                     ,sCS.ModDate
                                                     ,sCS.TermGPA_Simple
                                                     ,sCS.TermGPA_Weighted
                                                     ,sCS.coursecredits
                                                     ,sCS.CumulativeGPA
                                                     ,sCS.CumulativeGPA_Simple
                                                     ,sCS.FACreditsEarned
                                                     ,sCS.Average
                                                     ,sCS.CumAverage
                                                     ,sCS.TermStartDate
                                           FROM       dbo.syCreditSummary sCS
                                           INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                           WHERE      sCS.StuEnrollId = @StuEnrollId
                                                      AND tG.ReqId = sCS.ReqId
                                                      AND (
                                                          tG.CompletedDate IS NOT NULL
                                                          AND tG.CompletedDate <= @OffsetDate
                                                          ))
                                          ) sCSEA
                                   WHERE  StuEnrollId = @StuEnrollId
                                          AND (
                                              FinalScore IS NOT NULL
                                              OR FinalGrade IS NOT NULL
                                              )
                                          AND ReqId IN (
                                                       SELECT ReqId
                                                       FROM   (
                                                              SELECT   ReqId
                                                                      ,ReqDescrip
                                                                      ,COUNT(*) AS Counter
                                                              FROM     syCreditSummary
                                                              WHERE    StuEnrollId = @StuEnrollId
                                                              GROUP BY ReqId
                                                                      ,ReqDescrip
                                                              HAVING   COUNT(*) > 1
                                                              ) dt
                                                       )
                                   ) dt2;
            END;


        DECLARE @cumSimpleCourseCredits DECIMAL(18, 2);
        DECLARE @cumSimple_GPA_Credits DECIMAL(18, 2);
        -- (OUTPUT parameter)  DECLARE @cumSimpleGPA DECIMAL(18, 2)



        DECLARE @cumCourseCredits DECIMAL(18, 2)
               ,@cumWeighted_GPA_Credits DECIMAL(18, 2);
        DECLARE @cumCourseCredits_repeated DECIMAL(18, 2)
               ,@cumWeighted_GPA_Credits_repeated DECIMAL(18, 2);

        PRINT @GradeCourseRepetitionsMethod;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''average''
            BEGIN
                SET @cumWeightedGPA = 0;
                SET @cumSimpleGPA = 0;

                SET @cumSimpleCourseCredits = (
                                              SELECT COUNT(*)
                                              FROM   @CoursesNotRepeated
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND FinalGPA IS NOT NULL
                                                     AND (
                                                         rownumber = 0
                                                         OR rownumber IS NULL
                                                         )
                                              );

                DECLARE @cumSimpleCourseCredits_repeated DECIMAL(18, 2);
                SET @cumSimpleCourseCredits_repeated = (
                                                       SELECT SUM(CourseCreditCount)
                                                       FROM   (
                                                              SELECT   ReqId
                                                                      ,COUNT(coursecredits) AS CourseCreditCount
                                                              FROM     @CoursesNotRepeated
                                                              WHERE    StuEnrollId IN ( @StuEnrollId )
                                                                       AND FinalGPA IS NOT NULL
                                                                       AND rownumber = 1
                                                              GROUP BY ReqId
                                                              ) dt
                                                       );
                SET @cumSimpleCourseCredits = @cumSimpleCourseCredits + ISNULL(@cumSimpleCourseCredits_repeated, 0);

                DECLARE @cumSimple_GPA_Credits_repeated DECIMAL(18, 2);

                SET @cumSimple_GPA_Credits = (
                                             SELECT SUM(FinalGPA)
                                             FROM   @CoursesNotRepeated
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                                    AND (
                                                        rownumber = 0
                                                        OR rownumber IS NULL
                                                        )
                                             );

                SET @cumSimple_GPA_Credits_repeated = (
                                                      SELECT SUM(AverageGPA)
                                                      FROM   (
                                                             SELECT   ReqId
                                                                     ,SUM(FinalGPA) / MAX(rownumber) AS AverageGPA
                                                             FROM     @CoursesNotRepeated
                                                             WHERE    StuEnrollId IN ( @StuEnrollId )
                                                                      AND FinalGPA IS NOT NULL
                                                                      AND rownumber >= 1
                                                             GROUP BY ReqId
                                                             ) dt
                                                      );

                SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits + ISNULL(@cumSimple_GPA_Credits_repeated, 0.00);

                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END;

                SET @cumCourseCredits = (
                                        SELECT SUM(coursecredits)
                                        FROM   @CoursesNotRepeated
                                        WHERE  StuEnrollId IN ( @StuEnrollId )
                                               AND FinalGPA IS NOT NULL
                                               AND (
                                                   rownumber = 0
                                                   OR rownumber IS NULL
                                                   )
                                        );

                SET @cumCourseCredits_repeated = (
                                                 SELECT SUM(CourseCreditCount)
                                                 FROM   (
                                                        SELECT   ReqId
                                                                ,MAX(coursecredits) AS CourseCreditCount
                                                        FROM     @CoursesNotRepeated
                                                        WHERE    StuEnrollId IN ( @StuEnrollId )
                                                                 AND FinalGPA IS NOT NULL
                                                                 AND rownumber >= 1
                                                        GROUP BY ReqId
                                                        ) dt
                                                 );
                SET @cumCourseCredits = @cumCourseCredits + ISNULL(@cumCourseCredits_repeated, 0.00);

                PRINT @cumCourseCredits;

                SET @cumWeighted_GPA_Credits = (
                                               SELECT SUM(coursecredits * FinalGPA)
                                               FROM   @CoursesNotRepeated
                                               WHERE  StuEnrollId IN ( @StuEnrollId )
                                                      AND FinalGPA IS NOT NULL
                                                      AND (
                                                          rownumber = 0
                                                          OR rownumber IS NULL
                                                          )
                                               );
                SET @cumWeighted_GPA_Credits_repeated = (
                                                        SELECT SUM(CourseCreditCount)
                                                        FROM   (
                                                               SELECT   ReqId
                                                                       ,SUM(coursecredits * FinalGPA) / MAX(rownumber) AS CourseCreditCount
                                                               FROM     @CoursesNotRepeated
                                                               WHERE    StuEnrollId IN ( @StuEnrollId )
                                                                        AND FinalGPA IS NOT NULL
                                                                        AND rownumber >= 1
                                                               GROUP BY ReqId
                                                               ) dt
                                                        );



                SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits + ISNULL(@cumWeighted_GPA_Credits_repeated, 0);
                PRINT @cumWeighted_GPA_Credits;

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END;
            END;
        ELSE
            BEGIN
                SET @cumSimpleGPA = 0;

                SET @cumSimpleCourseCredits = (
                                              SELECT COUNT(*)
                                              FROM   @CoursesNotRepeated
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND FinalGPA IS NOT NULL
                                              );
                SET @cumSimple_GPA_Credits = (
                                             SELECT SUM(FinalGPA)
                                             FROM   @CoursesNotRepeated
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                             );
                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END;
                SET @cumWeightedGPA = 0;
                SET @cumCourseCredits = (
                                        SELECT SUM(coursecredits)
                                        FROM   @CoursesNotRepeated
                                        WHERE  StuEnrollId = @StuEnrollId
                                               AND FinalGPA IS NOT NULL
                                        );
                SET @cumWeighted_GPA_Credits = (
                                               SELECT SUM(coursecredits * FinalGPA)
                                               FROM   @CoursesNotRepeated
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalGPA IS NOT NULL
                                               );

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END;

            END;





        IF @cumCourseCredits >= 1
            BEGIN
                SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
            END;


        DECLARE @TermAverageCount INT;
        DECLARE @TermAverage DECIMAL(18, 2);
        -- (output parameter)  DECLARE @CumAverage DECIMAL(18, 2)
        IF @GradesFormat <> ''letter''
           OR @QuantMinUnitTypeId = 3
            BEGIN

                DECLARE @termAverageSum DECIMAL(18, 2)
                       ,@cumAverageSum DECIMAL(18, 2)
                       ,@cumAveragecount INT;
                DECLARE @TardiesMakingAbsence INT
                       ,@UnitTypeDescrip VARCHAR(20)
                       ,@OriginalTardiesMakingAbsence INT;
                SET @TardiesMakingAbsence = (
                                            SELECT TOP 1 t1.TardiesMakingAbsence
                                            FROM   arPrgVersions t1
                                                  ,arStuEnrollments t2
                                            WHERE  t1.PrgVerId = t2.PrgVerId
                                                   AND t2.StuEnrollId = @StuEnrollId
                                            );

                SET @UnitTypeDescrip = (
                                       SELECT     LTRIM(RTRIM(AAUT.UnitTypeDescrip))
                                       FROM       arAttUnitType AS AAUT
                                       INNER JOIN arPrgVersions AS APV ON APV.UnitTypeId = AAUT.UnitTypeId
                                       INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                                       WHERE      ASE.StuEnrollId = @StuEnrollId
                                       );

                SET @OriginalTardiesMakingAbsence = (
                                                    SELECT TOP 1 tardiesmakingabsence
                                                    FROM   syStudentAttendanceSummary
                                                    WHERE  StuEnrollId = @StuEnrollId
                                                    );
                DECLARE @termstartdate1 DATETIME;

                -- Declare Variables
                BEGIN -- Declare Variables
                    DECLARE @MeetDate DATETIME
                           ,@WeekDay VARCHAR(15)
                           ,@StartDate DATETIME
                           ,@EndDate DATETIME;
                    DECLARE @PeriodDescrip VARCHAR(50)
                           ,@Actual DECIMAL(18, 2)
                           ,@Excused DECIMAL(18, 2)
                           ,@ClsSectionId UNIQUEIDENTIFIER;
                    DECLARE @Absent DECIMAL(18, 2)
                           ,@SchedHours DECIMAL(18, 2)
                           ,@TardyMinutes DECIMAL(18, 2);
                    DECLARE @tardy DECIMAL(18, 2)
                           ,@tracktardies INT
                           ,@rownumber INT
                           ,@IsTardy BIT
                           ,@ActualRunningScheduledHours DECIMAL(18, 2);
                    DECLARE @ActualRunningPresentHours DECIMAL(18, 2)
                           ,@ActualRunningAbsentHours DECIMAL(18, 2)
                           ,@ActualRunningTardyHours DECIMAL(18, 2)
                           ,@ActualRunningMakeupHours DECIMAL(18, 2);
                    DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
                           ,@intTardyBreakPoint INT
                           ,@AdjustedRunningPresentHours DECIMAL(18, 2)
                           ,@AdjustedRunningAbsentHours DECIMAL(18, 2)
                           ,@ActualRunningScheduledDays DECIMAL(18, 2);
                    --declare @Scheduledhours decimal(18,2),@ActualPresentDays_ConvertTo_Hours decimal(18,2),@ActualAbsentDays_ConvertTo_Hours decimal(18,2),@AttendanceTrack varchar(50)
                    DECLARE @TermId VARCHAR(50)
                           ,@TermDescrip VARCHAR(50)
                           ,@PrgVerId UNIQUEIDENTIFIER;
                    DECLARE @TardyHit VARCHAR(10)
                           ,@ScheduledMinutes DECIMAL(18, 2);
                    DECLARE @Scheduledhours_noperiods DECIMAL(18, 2)
                           ,@ActualPresentDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2)
                           ,@ActualAbsentDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2);
                    DECLARE @ActualTardyDays_ConvertTo_Hours_NoPeriods DECIMAL(18, 2);
                    DECLARE @boolReset BIT
                           ,@MakeupHours DECIMAL(18, 2)
                           ,@AdjustedPresentDaysComputed DECIMAL(18, 2);
                END;

                -- Step 3  --  InsertAttendance_Class_PresentAbsent   -- UnitTypeDescrip = (''None'', ''Present Absent'') 
                --         --  TrackSapAttendance = ''byclass''
                BEGIN -- Step 3  --  InsertAttendance_Class_PresentAbsent   -- UnitTypeDescrip = (''None'', ''Present Absent'') 
                    --         --  TrackSapAttendance = ''byclass''
                    IF @UnitTypeDescrip IN ( ''none'', ''present absent'' )
                       AND @TrackSapAttendance = ''byclass''
                        BEGIN
                            PRINT ''Present absent, by class'';
                            ---- PRINT ''Step1!!!!''
                            --BEGIN
                            --    --DELETE FROM syStudentAttendanceSummary
                            --    --WHERE StuEnrollId = @StuEnrollId
                            --    --      AND StudentAttendedDate <= @OffsetDate;
                            --END;
                            INSERT INTO @ClsSectionIntervalMinutes (
                                                                   ClsSectionId
                                                                  ,IntervalInMinutes
                                                                   )
                                        SELECT     DISTINCT t1.ClsSectionId
                                                           ,DATEDIFF(MI, t6.TimeIntervalDescrip, t7.TimeIntervalDescrip)
                                        FROM       atClsSectAttendance t1
                                        INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                        INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                        INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                        INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                           AND (
                                                                               CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                               AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                               )
                                                                           AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                        INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                        INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                        INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                        INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                        INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                        INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                        WHERE      t2.StuEnrollId = @StuEnrollId
                                                   AND (
                                                       AAUT1.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                                       OR AAUT2.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                                       );

                            DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
                                SELECT   *
                                        ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                FROM     (
                                         SELECT     DISTINCT t1.StuEnrollId
                                                            ,t1.ClsSectionId
                                                            ,t1.MeetDate
                                                            ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                            ,t4.StartDate
                                                            ,t4.EndDate
                                                            ,t5.PeriodDescrip
                                                            ,t1.Actual
                                                            ,t1.Excused
                                                            ,CASE WHEN (
                                                                       t1.Actual = 0
                                                                       AND t1.Excused = 0
                                                                       ) THEN t1.Scheduled
                                                                  ELSE CASE WHEN (
                                                                                 t1.Actual <> 9999.00
                                                                                 AND t1.Actual < t1.Scheduled
                                                                                 --AND t1.Excused <> 1
                                                                                 ) THEN ( t1.Scheduled - t1.Actual )
                                                                            ELSE 0
                                                                       END
                                                             END AS Absent
                                                            ,t1.Scheduled AS ScheduledMinutes
                                                            ,CASE WHEN (
                                                                       t1.Actual > 0
                                                                       AND t1.Actual < t1.Scheduled
                                                                       ) THEN ( t1.Scheduled - t1.Actual )
                                                                  ELSE 0
                                                             END AS TardyMinutes
                                                            ,t1.Tardy AS Tardy
                                                            ,t3.TrackTardies
                                                            ,t3.TardiesMakingAbsence
                                                            ,t3.PrgVerId
                                         FROM       atClsSectAttendance t1
                                         INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                         INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                         INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                         INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                            AND (
                                                                                CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                                AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                                )
                                                                            AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                         INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                         INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                         INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                         INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                         INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                         INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                         WHERE      t2.StuEnrollId = @StuEnrollId
                                                    AND (
                                                        AAUT1.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                                        OR AAUT2.UnitTypeDescrip IN ( ''None'', ''Present Absent'' )
                                                        )
                                                    AND t1.Actual <> 9999
                                                    AND t1.MeetDate <= @OffsetDate
                                         ) dt
                                ORDER BY StuEnrollId
                                        ,MeetDate;
                            OPEN GetAttendance_Cursor;
                            FETCH NEXT FROM GetAttendance_Cursor
                            INTO @StuEnrollId
                                ,@ClsSectionId
                                ,@MeetDate
                                ,@WeekDay
                                ,@StartDate
                                ,@EndDate
                                ,@PeriodDescrip
                                ,@Actual
                                ,@Excused
                                ,@Absent
                                ,@ScheduledMinutes
                                ,@TardyMinutes
                                ,@tardy
                                ,@tracktardies
                                ,@TardiesMakingAbsence
                                ,@PrgVerId
                                ,@rownumber;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @boolReset = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                            SET @boolReset = 1;
                                        END;


                                    -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                                    IF @Actual <> 9999.00
                                        BEGIN
                                            -- Commented by Balaji on 2.6.2015 as Excused Absence is still considered an absence
                                            -- @Excused is not added to Present Hours
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual; -- + @Excused
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; -- + @Excused

                                            -- If there are make up hrs deduct that otherwise it will be added again in progress report
                                            IF (
                                               @Actual > 0
                                               AND @Actual > @ScheduledMinutes
                                               AND @Actual <> 9999.00
                                               )
                                                BEGIN
                                                    SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                END;

                                            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                                        END;

                                    -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                                    SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                                    -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                                    IF (
                                       @Actual > 0
                                       AND @Actual < @ScheduledMinutes
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                        END;
                                    ELSE IF (
                                            @Actual = 1
                                            AND @Actual = @ScheduledMinutes
                                            AND @tardy = 1
                                            )
                                             BEGIN
                                                 SET @ActualRunningTardyHours += 1;
                                             END;

                                    -- Track how many days student has been tardy only when 
                                    -- program version requires to track tardy
                                    IF (
                                       @tracktardies = 1
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;


                                    -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
                                    -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
                                    -- when student is tardy the second time, that second occurance will be considered as
                                    -- absence
                                    -- Variable @intTardyBreakpoint tracks how many times the student was tardy
                                    -- Variable @tardiesMakingAbsence tracks the tardy rule
                                    IF (
                                       @tracktardies = 1
                                       AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
                                            SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;

                                    -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                       @Actual > 0
                                       AND @Actual > @ScheduledMinutes
                                       AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;

                                    IF ( @tracktardies = 1 )
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                                        END;

                                    ---- PRINT @MeetDate
                                    ---- PRINT @ActualRunningAbsentHours



                                    INSERT INTO @attendanceSummary (
                                                                   StuEnrollId
                                                                  ,ClsSectionId
                                                                  ,StudentAttendedDate
                                                                  ,ScheduledDays
                                                                  ,ActualDays
                                                                  ,ActualRunningScheduledDays
                                                                  ,ActualRunningPresentDays
                                                                  ,ActualRunningAbsentDays
                                                                  ,ActualRunningMakeupDays
                                                                  ,ActualRunningTardyDays
                                                                  ,AdjustedPresentDays
                                                                  ,AdjustedAbsentDays
                                                                  ,AttendanceTrackType
                                                                  ,ModUser
                                                                  ,ModDate
                                                                  ,TardiesMakingAbsence
                                                                   )
                                    VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays
                                            ,@ActualRunningPresentHours, @ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours
                                            ,@AdjustedPresentDaysComputed, @AdjustedRunningAbsentHours, ''Post Attendance by Class Min'', ''sa'', GETDATE()
                                            ,@TardiesMakingAbsence );

                                    --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                                    SET @PrevStuEnrollId = @StuEnrollId;

                                    FETCH NEXT FROM GetAttendance_Cursor
                                    INTO @StuEnrollId
                                        ,@ClsSectionId
                                        ,@MeetDate
                                        ,@WeekDay
                                        ,@StartDate
                                        ,@EndDate
                                        ,@PeriodDescrip
                                        ,@Actual
                                        ,@Excused
                                        ,@Absent
                                        ,@ScheduledMinutes
                                        ,@TardyMinutes
                                        ,@tardy
                                        ,@tracktardies
                                        ,@TardiesMakingAbsence
                                        ,@PrgVerId
                                        ,@rownumber;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;

                            DECLARE @MyTardyTable TABLE
                                (
                                    ClsSectionId UNIQUEIDENTIFIER
                                   ,TardiesMakingAbsence INT
                                   ,AbsentHours DECIMAL(18, 2)
                                );

                            INSERT INTO @MyTardyTable
                                        SELECT     ClsSectionId
                                                  ,PV.TardiesMakingAbsence
                                                  ,CASE WHEN ( COUNT(*) >= PV.TardiesMakingAbsence ) THEN ( COUNT(*) / PV.TardiesMakingAbsence )
                                                        ELSE 0
                                                   END AS AbsentHours
                                        --Count(*) as NumberofTimesTardy 
                                        FROM       dbo.syStudentAttendanceSummary SAS
                                        INNER JOIN arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                                        INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                        WHERE      SE.StuEnrollId = @StuEnrollId
                                                   AND SAS.IsTardy = 1
                                                   AND PV.TrackTardies = 1
                                        GROUP BY   ClsSectionId
                                                  ,PV.TardiesMakingAbsence;

                            --Drop table @MyTardyTable
                            DECLARE @TotalTardyAbsentDays DECIMAL(18, 2);
                            SET @TotalTardyAbsentDays = (
                                                        SELECT ISNULL(SUM(AbsentHours), 0)
                                                        FROM   @MyTardyTable
                                                        );

                            --Print @TotalTardyAbsentDays

                            UPDATE @attendanceSummary
                            SET    AdjustedPresentDays = AdjustedPresentDays - @TotalTardyAbsentDays
                                  ,AdjustedAbsentDays = AdjustedAbsentDays + @TotalTardyAbsentDays
                            WHERE  StuEnrollId = @StuEnrollId
                                   AND StudentAttendedDate = (
                                                             SELECT   TOP 1 StudentAttendedDate
                                                             FROM     @attendanceSummary
                                                             WHERE    StuEnrollId = @StuEnrollId
                                                             ORDER BY StudentAttendedDate DESC
                                                             );

                        END;
                END; -- Step 3 
                -- END -- Step 3 

                -- Step 4  --  InsertAttendance_Class_Minutes
                --         --  TrackSapAttendance = ''byclass''
                BEGIN -- Step 4  --  InsertAttendance_Class_Minutes
                    -- By Class and Attendance Unit Type - Minutes and Clock Hour

                    IF @UnitTypeDescrip IN ( ''minutes'', ''clock hours'' )
                       AND @TrackSapAttendance = ''byclass''
                        BEGIN

                            --BEGIN
                            --    --DELETE FROM syStudentAttendanceSummary
                            --    --WHERE StuEnrollId = @StuEnrollId
                            --    --      AND StudentAttendedDate <= @OffsetDate;
                            --END;
                            DECLARE GetAttendance_Cursor CURSOR FOR
                                SELECT   *
                                        ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                FROM     (
                                         SELECT     DISTINCT t1.StuEnrollId
                                                            ,t1.ClsSectionId
                                                            ,t1.MeetDate
                                                            ,DATENAME(dw, t1.MeetDate) AS WeekDay
                                                            ,t4.StartDate
                                                            ,t4.EndDate
                                                            ,t5.PeriodDescrip
                                                            ,t1.Actual
                                                            ,t1.Excused
                                                            ,CASE WHEN (
                                                                       t1.Actual = 0
                                                                       AND t1.Excused = 0
                                                                       ) THEN t1.Scheduled
                                                                  ELSE CASE WHEN (
                                                                                 t1.Actual <> 9999.00
                                                                                 AND t1.Actual < t1.Scheduled
                                                                                 ) THEN ( t1.Scheduled - t1.Actual )
                                                                            ELSE 0
                                                                       END
                                                             END AS Absent
                                                            ,t1.Scheduled AS ScheduledMinutes
                                                            ,CASE WHEN (
                                                                       t1.Actual > 0
                                                                       AND t1.Actual < t1.Scheduled
                                                                       ) THEN ( t1.Scheduled - t1.Actual )
                                                                  ELSE 0
                                                             END AS TardyMinutes
                                                            ,t1.Tardy AS Tardy
                                                            ,t3.TrackTardies
                                                            ,t3.TardiesMakingAbsence
                                                            ,t3.PrgVerId
                                         FROM       atClsSectAttendance t1
                                         INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                         INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                         INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                         INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                            AND (
                                                                                CONVERT(DATE, t1.MeetDate, 111) >= CONVERT(DATE, t4.StartDate, 111)
                                                                                AND CONVERT(DATE, t1.MeetDate, 111) <= CONVERT(DATE, t4.EndDate, 111)
                                                                                )
                                                                            AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                         INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                         INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                         INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                         INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                         INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                         INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                         WHERE      t2.StuEnrollId = @StuEnrollId
                                                    AND (
                                                        AAUT1.UnitTypeDescrip IN ( ''Minutes'', ''Clock Hours'' )
                                                        OR AAUT2.UnitTypeDescrip IN ( ''Minutes'', ''Clock Hours'' )
                                                        )
                                                    AND t1.Actual <> 9999
                                                    AND t1.MeetDate <= @OffsetDate
                                         UNION
                                         SELECT     DISTINCT t1.StuEnrollId
                                                            ,NULL AS ClsSectionId
                                                            ,t1.RecordDate AS MeetDate
                                                            ,DATENAME(dw, t1.RecordDate) AS WeekDay
                                                            ,NULL AS StartDate
                                                            ,NULL AS EndDate
                                                            ,NULL AS PeriodDescrip
                                                            ,t1.ActualHours
                                                            ,NULL AS Excused
                                                            ,CASE WHEN ( t1.ActualHours = 0 ) THEN t1.SchedHours
                                                                  ELSE 0
                                                             --ELSE 
                                                             --Case when (t1.ActualHours <> 9999.00 and t1.ActualHours < t1.SchedHours)
                                                             --		THEN (t1.SchedHours - t1.ActualHours)
                                                             --		ELSE 
                                                             --			0
                                                             --		End
                                                             END AS Absent
                                                            ,t1.SchedHours AS ScheduledMinutes
                                                            ,CASE WHEN (
                                                                       t1.ActualHours <> 9999.00
                                                                       AND t1.ActualHours > 0
                                                                       AND t1.ActualHours < t1.SchedHours
                                                                       ) THEN ( t1.SchedHours - t1.ActualHours )
                                                                  ELSE 0
                                                             END AS TardyMinutes
                                                            ,NULL AS Tardy
                                                            ,t3.TrackTardies
                                                            ,t3.TardiesMakingAbsence
                                                            ,t3.PrgVerId
                                         FROM       arStudentClockAttendance t1
                                         INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                         INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                         WHERE      t2.StuEnrollId = @StuEnrollId
                                                    AND t1.Converted = 1
                                                    AND t1.ActualHours <> 9999
                                                    AND t1.RecordDate <= @OffsetDate
                                         ) dt
                                ORDER BY StuEnrollId
                                        ,MeetDate;
                            OPEN GetAttendance_Cursor;
                            FETCH NEXT FROM GetAttendance_Cursor
                            INTO @StuEnrollId
                                ,@ClsSectionId
                                ,@MeetDate
                                ,@WeekDay
                                ,@StartDate
                                ,@EndDate
                                ,@PeriodDescrip
                                ,@Actual
                                ,@Excused
                                ,@Absent
                                ,@ScheduledMinutes
                                ,@TardyMinutes
                                ,@tardy
                                ,@tracktardies
                                ,@TardiesMakingAbsence
                                ,@PrgVerId
                                ,@rownumber;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @boolReset = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                            SET @boolReset = 1;
                                        END;


                                    -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                                    IF @Actual <> 9999.00
                                        BEGIN
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                                            -- If there are make up hrs deduct that otherwise it will be added again in progress report
                                            IF (
                                               @Actual > 0
                                               AND @Actual > @ScheduledMinutes
                                               AND @Actual <> 9999.00
                                               )
                                                BEGIN
                                                    SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                END;
                                            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;
                                        END;

                                    -- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                                    SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;

                                    -- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                                    IF (
                                       @Actual > 0
                                       AND @Actual < @ScheduledMinutes
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                        END;

                                    -- Track how many days student has been tardy only when 
                                    -- program version requires to track tardy
                                    IF (
                                       @tracktardies = 1
                                       AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;


                                    -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
                                    -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
                                    -- when student is tardy the second time, that second occurance will be considered as
                                    -- absence
                                    -- Variable @intTardyBreakpoint tracks how many times the student was tardy
                                    -- Variable @tardiesMakingAbsence tracks the tardy rule
                                    IF (
                                       @tracktardies = 1
                                       AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                            SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;

                                    -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                       @Actual > 0
                                       AND @Actual > @ScheduledMinutes
                                       AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;


                                    IF ( @tracktardies = 1 )
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                                        END;



                                    INSERT INTO @attendanceSummary (
                                                                   StuEnrollId
                                                                  ,ClsSectionId
                                                                  ,StudentAttendedDate
                                                                  ,ScheduledDays
                                                                  ,ActualDays
                                                                  ,ActualRunningScheduledDays
                                                                  ,ActualRunningPresentDays
                                                                  ,ActualRunningAbsentDays
                                                                  ,ActualRunningMakeupDays
                                                                  ,ActualRunningTardyDays
                                                                  ,AdjustedPresentDays
                                                                  ,AdjustedAbsentDays
                                                                  ,AttendanceTrackType
                                                                  ,ModUser
                                                                  ,ModDate
                                                                   )
                                    VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays
                                            ,@ActualRunningPresentHours, @ActualRunningAbsentHours, ISNULL(@MakeupHours, 0), @ActualRunningTardyHours
                                            ,@AdjustedPresentDaysComputed, @AdjustedRunningAbsentHours, ''Post Attendance by Class Min'', ''sa'', GETDATE());

                                    UPDATE @attendanceSummary
                                    SET    TardiesMakingAbsence = @TardiesMakingAbsence
                                    WHERE  StuEnrollId = @StuEnrollId;
                                    SET @PrevStuEnrollId = @StuEnrollId;

                                    FETCH NEXT FROM GetAttendance_Cursor
                                    INTO @StuEnrollId
                                        ,@ClsSectionId
                                        ,@MeetDate
                                        ,@WeekDay
                                        ,@StartDate
                                        ,@EndDate
                                        ,@PeriodDescrip
                                        ,@Actual
                                        ,@Excused
                                        ,@Absent
                                        ,@ScheduledMinutes
                                        ,@TardyMinutes
                                        ,@tardy
                                        ,@tracktardies
                                        ,@TardiesMakingAbsence
                                        ,@PrgVerId
                                        ,@rownumber;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
                        END;
                END; --  Step 3  --   InsertAttendance_Class_Minutes
                --END --  Step 3  --   InsertAttendance_Class_Minutes


                --Select * from arAttUnitType


                -- By Minutes/Day
                -- remove clock hour from here
                IF @UnitTypeDescrip IN ( ''minutes'' )
                   AND @TrackSapAttendance = ''byday''
                    -- -- PRINT GETDATE();	
                    ---- PRINT @UnitTypeId;
                    ---- PRINT @TrackSapAttendance;
                    BEGIN
                        --BEGIN
                        --    --DELETE FROM syStudentAttendanceSummary
                        --    --WHERE StuEnrollId = @StuEnrollId
                        --    --      AND StudentAttendedDate <= @OffsetDate;
                        --END;
                        DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
                            SELECT     t1.StuEnrollId
                                      ,NULL AS ClsSectionId
                                      ,t1.RecordDate AS MeetDate
                                      ,t1.ActualHours
                                      ,t1.SchedHours AS ScheduledMinutes
                                      ,CASE WHEN (
                                                 (
                                                 t1.SchedHours >= 1
                                                 AND t1.SchedHours NOT IN ( 999, 9999 )
                                                 )
                                                 AND t1.ActualHours = 0
                                                 ) THEN t1.SchedHours
                                            ELSE 0
                                       END AS Absent
                                      ,t1.isTardy
                                      ,(
                                       SELECT ISNULL(SUM(SchedHours), 0)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999, 9999 )
                                                  AND t1.ActualHours NOT IN ( 999, 9999 )
                                                  )
                                       ) AS ActualRunningScheduledHours
                                      ,(
                                       SELECT SUM(ActualHours)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999, 9999 )
                                                  )
                                              AND ActualHours >= 1
                                              AND ActualHours NOT IN ( 999, 9999 )
                                       ) AS ActualRunningPresentHours
                                      ,(
                                       SELECT COUNT(ActualHours)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999, 9999 )
                                                  )
                                              AND ActualHours = 0
                                              AND ActualHours NOT IN ( 999, 9999 )
                                       ) AS ActualRunningAbsentHours
                                      ,(
                                       SELECT SUM(ActualHours)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND SchedHours = 0
                                              AND ActualHours >= 1
                                              AND ActualHours NOT IN ( 999, 9999 )
                                       ) AS ActualRunningMakeupHours
                                      ,(
                                       SELECT SUM(SchedHours - ActualHours)
                                       FROM   arStudentClockAttendance
                                       WHERE  StuEnrollId = t1.StuEnrollId
                                              AND RecordDate <= t1.RecordDate
                                              AND (
                                                  (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999, 9999 )
                                                  )
                                                  AND ActualHours >= 1
                                                  AND ActualHours NOT IN ( 999, 9999 )
                                                  )
                                              AND isTardy = 1
                                       ) AS ActualRunningTardyHours
                                      ,t3.TrackTardies
                                      ,t3.TardiesMakingAbsence
                                      ,t3.PrgVerId
                                      ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
                            FROM       arStudentClockAttendance t1
                            INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                            INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                            INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                            --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                            WHERE      AAUT1.UnitTypeDescrip IN ( ''Minutes'' )
                                       AND t2.StuEnrollId = @StuEnrollId
                                       AND t1.ActualHours <> 9999.00
                                       AND t1.RecordDate <= @OffsetDate
                            ORDER BY   t1.StuEnrollId
                                      ,MeetDate;
                        OPEN GetAttendance_Cursor;
                        --Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                        FETCH NEXT FROM GetAttendance_Cursor
                        INTO @StuEnrollId
                            ,@ClsSectionId
                            ,@MeetDate
                            ,@Actual
                            ,@ScheduledMinutes
                            ,@Absent
                            ,@IsTardy
                            ,@ActualRunningScheduledHours
                            ,@ActualRunningPresentHours
                            ,@ActualRunningAbsentHours
                            ,@ActualRunningMakeupHours
                            ,@ActualRunningTardyHours
                            ,@tracktardies
                            ,@TardiesMakingAbsence
                            ,@PrgVerId
                            ,@rownumber;

                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @ActualRunningMakeupHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        WHILE @@FETCH_STATUS = 0
                            BEGIN

                                IF @PrevStuEnrollId <> @StuEnrollId
                                    BEGIN
                                        SET @ActualRunningPresentHours = 0;
                                        SET @ActualRunningAbsentHours = 0;
                                        SET @intTardyBreakPoint = 0;
                                        SET @ActualRunningTardyHours = 0;
                                        SET @AdjustedRunningPresentHours = 0;
                                        SET @AdjustedRunningAbsentHours = 0;
                                        SET @ActualRunningScheduledDays = 0;
                                    END;

                                IF (
                                   @ScheduledMinutes >= 1
                                   AND (
                                       @Actual <> 9999
                                       AND @Actual <> 999
                                       )
                                   AND (
                                       @ScheduledMinutes <> 9999
                                       AND @Actual <> 999
                                       )
                                   )
                                    BEGIN
                                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + ISNULL(@ScheduledMinutes, 0);
                                    END;
                                ELSE
                                    BEGIN
                                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0);
                                    END;

                                IF (
                                   @Actual <> 9999
                                   AND @Actual <> 999
                                   )
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual - ( @Actual - @ScheduledMinutes );
                                    END;
                                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;

                                IF (
                                   @Actual > 0
                                   AND @Actual < @ScheduledMinutes
                                   )
                                    BEGIN
                                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                                    END;
                                -- Make up hours
                                --sched=5, Actual =7, makeup= 2,ab = 0
                                --sched=0, Actual =7, makeup= 7,ab = 0
                                IF (
                                   @Actual > 0
                                   AND @ScheduledMinutes > 0
                                   AND @Actual > @ScheduledMinutes
                                   AND (
                                       @Actual <> 9999
                                       AND @Actual <> 999
                                       )
                                   )
                                    BEGIN
                                        SET @ActualRunningMakeupHours = @ActualRunningMakeupHours + ( @Actual - @ScheduledMinutes );
                                    END;


                                IF (
                                   @Actual <> 9999
                                   AND @Actual <> 999
                                   )
                                    BEGIN
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                                    END;
                                IF (
                                   @Actual = 0
                                   AND @ScheduledMinutes >= 1
                                   AND @ScheduledMinutes NOT IN ( 999, 9999 )
                                   )
                                    BEGIN
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                                    END;
                                -- Absent hours
                                --1. sched = 5, Actual = 2 then Ab = (5-3) = 2
                                IF (
                                   @Absent = 0
                                   AND @ActualRunningAbsentHours > 0
                                   AND ( @Actual < @ScheduledMinutes )
                                   )
                                    BEGIN
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                                    END;
                                IF (
                                   @Actual > 0
                                   AND @Actual < @ScheduledMinutes
                                   AND (
                                       @Actual <> 9999.00
                                       AND @Actual <> 999.00
                                       )
                                   )
                                    BEGIN
                                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                    END;

                                IF @tracktardies = 1
                                   AND (
                                       @TardyMinutes > 0
                                       OR @IsTardy = 1
                                       )
                                    BEGIN
                                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                    END;
                                IF (
                                   @tracktardies = 1
                                   AND @intTardyBreakPoint = @TardiesMakingAbsence
                                   )
                                    BEGIN
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Actual; --@TardyMinutes
                                        SET @intTardyBreakPoint = 0;
                                    END;

                                INSERT INTO @attendanceSummary (
                                                               StuEnrollId
                                                              ,ClsSectionId
                                                              ,StudentAttendedDate
                                                              ,ScheduledDays
                                                              ,ActualDays
                                                              ,ActualRunningScheduledDays
                                                              ,ActualRunningPresentDays
                                                              ,ActualRunningAbsentDays
                                                              ,ActualRunningMakeupDays
                                                              ,ActualRunningTardyDays
                                                              ,AdjustedPresentDays
                                                              ,AdjustedAbsentDays
                                                              ,AttendanceTrackType
                                                              ,ModUser
                                                              ,ModDate
                                                               )
                                VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, @ScheduledMinutes, @Actual, @ActualRunningScheduledDays
                                        ,@ActualRunningPresentHours, @ActualRunningAbsentHours, @ActualRunningMakeupHours, @ActualRunningTardyHours
                                        ,@AdjustedRunningPresentHours, @AdjustedRunningAbsentHours, ''Post Attendance by Class'', ''sa'', GETDATE());

                                UPDATE @attendanceSummary
                                SET    TardiesMakingAbsence = @TardiesMakingAbsence
                                WHERE  StuEnrollId = @StuEnrollId;
                                --end
                                SET @PrevStuEnrollId = @StuEnrollId;
                                FETCH NEXT FROM GetAttendance_Cursor
                                INTO @StuEnrollId
                                    ,@ClsSectionId
                                    ,@MeetDate
                                    ,@Actual
                                    ,@ScheduledMinutes
                                    ,@Absent
                                    ,@IsTardy
                                    ,@ActualRunningScheduledHours
                                    ,@ActualRunningPresentHours
                                    ,@ActualRunningAbsentHours
                                    ,@ActualRunningMakeupHours
                                    ,@ActualRunningTardyHours
                                    ,@tracktardies
                                    ,@TardiesMakingAbsence
                                    ,@PrgVerId
                                    ,@rownumber;
                            END;
                        CLOSE GetAttendance_Cursor;
                        DEALLOCATE GetAttendance_Cursor;
                    END;

                ---- PRINT ''Does it get to Clock Hour/PA'';
                ---- PRINT @UnitTypeId;
                ---- PRINT @TrackSapAttendance;
                -- By Day and PA, Clock Hour
                -- -- Step 2  --  InsertAttendance_Day_PresentAbsent  -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'')  and TrackSapAttendance = ''byday''
                BEGIN -- Step 2  --  InsertAttendance_Day_PresentAbsent  -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'') and TrackSapAttendance = ''byday''
                    IF @UnitTypeDescrip IN ( ''present absent'', ''clock hours'' )
                       AND @TrackSapAttendance = ''byday''
                        BEGIN
                            --BEGIN
                            --    --DELETE FROM syStudentAttendanceSummary
                            --    --WHERE StuEnrollId = @StuEnrollId
                            --    --      AND StudentAttendedDate <= @OffsetDate;
                            --END;
                            ---- PRINT ''By Day inside day'';
                            DECLARE GetAttendance_Cursor CURSOR FOR
                                SELECT     t1.StuEnrollId
                                          ,t1.RecordDate
                                          ,t1.ActualHours
                                          ,t1.SchedHours
                                          ,CASE WHEN (
                                                     (
                                                     t1.SchedHours >= 1
                                                     AND t1.SchedHours NOT IN ( 999, 9999 )
                                                     )
                                                     AND t1.ActualHours = 0
                                                     ) THEN t1.SchedHours
                                                ELSE 0
                                           END AS Absent
                                          ,t1.isTardy
                                          ,t3.TrackTardies
                                          ,t3.TardiesMakingAbsence
                                FROM       arStudentClockAttendance t1
                                INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                --inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                                WHERE -- Unit Types: Present Absent and Clock Hour
                                           AAUT1.UnitTypeDescrip IN ( ''present absent'', ''clock hours'' )
                                           AND ActualHours <> 9999.00
                                           AND t2.StuEnrollId = @StuEnrollId
                                           AND t1.RecordDate <= @OffsetDate
                                ORDER BY   t1.StuEnrollId
                                          ,t1.RecordDate;
                            OPEN GetAttendance_Cursor;
                            --Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                            FETCH NEXT FROM GetAttendance_Cursor
                            INTO @StuEnrollId
                                ,@MeetDate
                                ,@Actual
                                ,@ScheduledMinutes
                                ,@Absent
                                ,@IsTardy
                                ,@tracktardies
                                ,@TardiesMakingAbsence;

                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                        END;

                                    IF (
                                       @Actual <> 9999
                                       AND @Actual <> 999
                                       )
                                        BEGIN
                                            SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays, 0) + @ScheduledMinutes;
                                            SET @ActualRunningPresentHours = ISNULL(@ActualRunningPresentHours, 0) + @Actual;
                                            SET @AdjustedRunningPresentHours = ISNULL(@AdjustedRunningPresentHours, 0) + @Actual;
                                        END;
                                    SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + @Absent;
                                    IF (
                                       @Actual = 0
                                       AND @ScheduledMinutes >= 1
                                       AND @ScheduledMinutes NOT IN ( 999, 9999 )
                                       )
                                        BEGIN
                                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                                        END;

                                    -- NWH 
                                    -- If Scheduled = 3.0 hrs and Actual = 1.0 Then 2 hrs is considered absent
                                    IF (
                                       @ScheduledMinutes >= 1
                                       AND @ScheduledMinutes NOT IN ( 999, 9999 )
                                       AND @Actual > 0
                                       AND ( @Actual < @ScheduledMinutes )
                                       )
                                        BEGIN
                                            SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                                            SET @AdjustedRunningAbsentHours = ISNULL(@AdjustedRunningAbsentHours, 0) + ( @ScheduledMinutes - @Actual );
                                        END;

                                    IF (
                                       @tracktardies = 1
                                       AND @IsTardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + 1;
                                        END;
                                    --commented by balaji on 10/22/2012 as report (rdl) doesn''t add days attended and make up days
                                    ---- If there are make up hrs deduct that otherwise it will be added again in progress report
                                    IF (
                                       @Actual > 0
                                       AND @Actual > @ScheduledMinutes
                                       AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                        END;

                                    IF @tracktardies = 1
                                       AND (
                                           @TardyMinutes > 0
                                           OR @IsTardy = 1
                                           )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;



                                    -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                       @Actual > 0
                                       AND @Actual > @ScheduledMinutes
                                       AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;

                                    IF (
                                       @tracktardies = 1
                                       AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;


                                    INSERT INTO @attendanceSummary (
                                                                   StuEnrollId
                                                                  ,ClsSectionId
                                                                  ,StudentAttendedDate
                                                                  ,ScheduledDays
                                                                  ,ActualDays
                                                                  ,ActualRunningScheduledDays
                                                                  ,ActualRunningPresentDays
                                                                  ,ActualRunningAbsentDays
                                                                  ,ActualRunningMakeupDays
                                                                  ,ActualRunningTardyDays
                                                                  ,AdjustedPresentDays
                                                                  ,AdjustedAbsentDays
                                                                  ,AttendanceTrackType
                                                                  ,ModUser
                                                                  ,ModDate
                                                                  ,TardiesMakingAbsence
                                                                   )
                                    VALUES ( @StuEnrollId, @ClsSectionId, @MeetDate, ISNULL(@ScheduledMinutes, 0), @Actual
                                            ,ISNULL(@ActualRunningScheduledDays, 0), ISNULL(@ActualRunningPresentHours, 0)
                                            ,ISNULL(@ActualRunningAbsentHours, 0), ISNULL(@MakeupHours, 0), ISNULL(@ActualRunningTardyHours, 0)
                                            ,ISNULL(@AdjustedRunningPresentHours, 0), ISNULL(@AdjustedRunningAbsentHours, 0), ''Post Attendance by Class'', ''sa''
                                            ,GETDATE(), @TardiesMakingAbsence );

                                    --update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                                    --end
                                    SET @PrevStuEnrollId = @StuEnrollId;
                                    FETCH NEXT FROM GetAttendance_Cursor
                                    INTO @StuEnrollId
                                        ,@MeetDate
                                        ,@Actual
                                        ,@ScheduledMinutes
                                        ,@Absent
                                        ,@IsTardy
                                        ,@tracktardies
                                        ,@TardiesMakingAbsence;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
                        END;
                END;

                --end

                -- Based on grade rounding round the final score and current score
                --Declare @PrevTermId uniqueidentifier,@PrevReqId uniqueidentifier,@RowCount int,@FinalScore decimal(18,4),@CurrentScore decimal(18,4)
                --declare @ReqId uniqueidentifier,@CourseCodeDescrip varchar(100),@FinalGrade varchar(10),@sysComponentTypeId int
                --declare @CreditsAttempted decimal(18,4),@Grade varchar(10),@IsPass bit,@IsCreditsAttempted bit,@IsCreditsEarned bit
                --declare @IsInGPA bit,@FinAidCredits decimal(18,4),@CurrentGrade varchar(10),@CourseCredits decimal(18,4)
                DECLARE @GPA DECIMAL(18, 4);

                DECLARE @PrevReqId UNIQUEIDENTIFIER
                       ,@PrevTermId UNIQUEIDENTIFIER
                       ,@CreditsAttempted DECIMAL(18, 2)
                       ,@CreditsEarned DECIMAL(18, 2);
                DECLARE @reqid UNIQUEIDENTIFIER
                       ,@CourseCodeDescrip VARCHAR(50)
                       ,@FinalGrade VARCHAR(50)
                       ,@FinalScore DECIMAL(18, 2)
                       ,@Grade VARCHAR(50);
                DECLARE @IsPass BIT
                       ,@IsCreditsAttempted BIT
                       ,@IsCreditsEarned BIT
                       ,@CurrentScore DECIMAL(18, 2)
                       ,@CurrentGrade VARCHAR(10)
                       ,@FinalGPA DECIMAL(18, 2);
                DECLARE @sysComponentTypeId INT
                       ,@RowCount INT;
                DECLARE @IsInGPA BIT;
                DECLARE @CourseCredits DECIMAL(18, 2);
                DECLARE @FinAidCreditsEarned DECIMAL(18, 2)
                       ,@FinAidCredits DECIMAL(18, 2);

                DECLARE GetCreditsSummary_Cursor CURSOR FOR
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,T.TermId
                                       ,T.TermDescrip
                                       ,T.StartDate
                                       ,R.ReqId
                                       ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                       ,RES.Score AS FinalScore
                                       ,RES.GrdSysDetailId AS FinalGrade
                                       ,GCT.SysComponentTypeId
                                       ,R.Credits AS CreditsAttempted
                                       ,CS.ClsSectionId
                                       ,GSD.Grade
                                       ,GSD.IsPass
                                       ,GSD.IsCreditsAttempted
                                       ,GSD.IsCreditsEarned
                                       ,SE.PrgVerId
                                       ,GSD.IsInGPA
                                       ,R.FinAidCredits AS FinAidCredits
                    FROM       arStuEnrollments SE
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                    INNER JOIN arTerm T ON CS.TermId = T.TermId
                    INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                    LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                    LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                    LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                    LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    WHERE      SE.StuEnrollId = @StuEnrollId
                               AND (
                                   RES.DateCompleted IS NOT NULL
                                   AND RES.DateCompleted <= @OffsetDate
                                   )
                    ORDER BY   T.StartDate
                              ,T.TermDescrip
                              ,R.ReqId;

                DECLARE @varGradeRounding VARCHAR(3);
                DECLARE @roundfinalscore DECIMAL(18, 4);
                SET @varGradeRounding = (
                                        SELECT Value
                                        FROM   syConfigAppSetValues
                                        WHERE  SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@FinalGrade
                    ,@sysComponentTypeId
                    ,@CreditsAttempted
                    ,@ClsSectionId
                    ,@Grade
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN


                        SET @CurrentScore = (
                                            SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                        ELSE NULL
                                                   END AS CurrentScore
                                            FROM   (
                                                   SELECT   InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                   FROM     (
                                                            SELECT   C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,ISNULL(C.Weight, 0) AS Weight
                                                                    ,C.Number AS MinNumber
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter AS Param
                                                                    ,X.GrdScaleId
                                                                    ,SUM(GR.Score) AS Score
                                                                    ,COUNT(D.Descrip) AS NumberOfComponents
                                                            FROM     (
                                                                     SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                            ,A.EffectiveDate
                                                                                            ,B.GrdScaleId
                                                                     FROM     arGrdBkWeights A
                                                                             ,arClassSections B
                                                                     WHERE    A.ReqId = B.ReqId
                                                                              AND A.EffectiveDate <= B.StartDate
                                                                              AND B.ClsSectionId = @ClsSectionId
                                                                     ORDER BY A.EffectiveDate DESC
                                                                     ) X
                                                                    ,arGrdBkWgtDetails C
                                                                    ,arGrdComponentTypes D
                                                                    ,arGrdBkResults GR
                                                            WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                     AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                     AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                     AND D.SysComponentTypeId NOT IN ( 500, 503 )
                                                                     AND GR.StuEnrollId = @StuEnrollId
                                                                     AND GR.ClsSectionId = @ClsSectionId
                                                                     AND GR.Score IS NOT NULL
                                                            GROUP BY C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,C.Weight
                                                                    ,C.Number
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter
                                                                    ,X.GrdScaleId
                                                            ) S
                                                   GROUP BY InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                   ) FinalTblToComputeCurrentScore
                                            );
                        IF ( @CurrentScore IS NULL )
                            BEGIN
                                -- instructor grade books
                                SET @CurrentScore = (
                                                    SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                                ELSE NULL
                                                           END AS CurrentScore
                                                    FROM   (
                                                           SELECT   InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight AS GradeBookWeight
                                                                   ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                         ELSE 0
                                                                    END AS ActualWeight
                                                           FROM     (
                                                                    SELECT   C.InstrGrdBkWgtDetailId
                                                                            ,D.Code
                                                                            ,D.Descrip
                                                                            ,ISNULL(C.Weight, 0) AS Weight
                                                                            ,C.Number AS MinNumber
                                                                            ,C.GrdPolicyId
                                                                            ,C.Parameter AS Param
                                                                            ,X.GrdScaleId
                                                                            ,SUM(GR.Score) AS Score
                                                                            ,COUNT(D.Descrip) AS NumberOfComponents
                                                                    FROM     (
                                                                             --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
                                                                             --FROM          arGrdBkWeights A,arClassSections B        
                                                                             --WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
                                                                             --ORDER BY      A.EffectiveDate DESC
                                                                             SELECT DISTINCT TOP 1 t1.InstrGrdBkWgtId
                                                                                                  ,t1.GrdScaleId
                                                                             FROM   arClassSections t1
                                                                                   ,arGrdBkWeights t2
                                                                             WHERE  t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                                    AND t1.ClsSectionId = @ClsSectionId
                                                                             ) X
                                                                            ,arGrdBkWgtDetails C
                                                                            ,arGrdComponentTypes D
                                                                            ,arGrdBkResults GR
                                                                    WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                             AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                             AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                             AND
                                                                        -- D.SysComponentTypeID not in (500,503) and 
                                                                        GR.StuEnrollId = @StuEnrollId
                                                                             AND GR.ClsSectionId = @ClsSectionId
                                                                             AND GR.Score IS NOT NULL
                                                                    GROUP BY C.InstrGrdBkWgtDetailId
                                                                            ,D.Code
                                                                            ,D.Descrip
                                                                            ,C.Weight
                                                                            ,C.Number
                                                                            ,C.GrdPolicyId
                                                                            ,C.Parameter
                                                                            ,X.GrdScaleId
                                                                    ) S
                                                           GROUP BY InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight
                                                                   ,NumberOfComponents
                                                           ) FinalTblToComputeCurrentScore
                                                    );

                            END;


                        -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore, 0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;

                        UPDATE syCreditSummary
                        SET    FinalScore = @FinalScore
                              ,CurrentScore = @CurrentScore
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId
                               AND ReqId = @reqid;

                        --Average calculation

                        -- Term Average
                        SET @TermAverageCount = (
                                                SELECT COUNT(*)
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                       AND TermId = @TermId
                                                       AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                              SELECT SUM(FinalScore)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND TermId = @TermId
                                                     AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount;

                        -- Cumulative Average
                        SET @cumAveragecount = (
                                               SELECT COUNT(*)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                             SELECT SUM(FinalScore)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount;

                        UPDATE syCreditSummary
                        SET    Average = @TermAverage
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId;

                        --Update Cumulative GPA
                        UPDATE syCreditSummary
                        SET    CumAverage = @CumAverage
                        WHERE  StuEnrollId = @StuEnrollId;


                        FETCH NEXT FROM GetCreditsSummary_Cursor
                        INTO @StuEnrollId
                            ,@TermId
                            ,@TermDescrip
                            ,@termstartdate1
                            ,@reqid
                            ,@CourseCodeDescrip
                            ,@FinalScore
                            ,@FinalGrade
                            ,@sysComponentTypeId
                            ,@CreditsAttempted
                            ,@ClsSectionId
                            ,@Grade
                            ,@IsPass
                            ,@IsCreditsAttempted
                            ,@IsCreditsEarned
                            ,@PrgVerId
                            ,@IsInGPA
                            ,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;


                DECLARE GetCreditsSummary_Cursor CURSOR FOR
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,T.TermId
                                       ,T.TermDescrip
                                       ,T.StartDate
                                       ,R.ReqId
                                       ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                       ,RES.Score AS FinalScore
                                       ,RES.GrdSysDetailId AS FinalGrade
                                       ,GCT.SysComponentTypeId
                                       ,R.Credits AS CreditsAttempted
                                       ,CS.ClsSectionId
                                       ,GSD.Grade
                                       ,GSD.IsPass
                                       ,GSD.IsCreditsAttempted
                                       ,GSD.IsCreditsEarned
                                       ,SE.PrgVerId
                                       ,GSD.IsInGPA
                                       ,R.FinAidCredits AS FinAidCredits
                    FROM       arStuEnrollments SE
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                                     AND RES.TermId = CS.TermId
                    INNER JOIN arTerm T ON CS.TermId = T.TermId
                    INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                    LEFT JOIN  arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                    LEFT JOIN  arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                    LEFT JOIN  arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                    LEFT JOIN  arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    WHERE      SE.StuEnrollId = @StuEnrollId
                               AND (
                                   RES.CompletedDate IS NOT NULL
                                   AND RES.CompletedDate <= @OffsetDate
                                   )
                    ORDER BY   T.StartDate
                              ,T.TermDescrip
                              ,R.ReqId;


                SET @varGradeRounding = (
                                        SELECT Value
                                        FROM   syConfigAppSetValues
                                        WHERE  SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@FinalGrade
                    ,@sysComponentTypeId
                    ,@CreditsAttempted
                    ,@ClsSectionId
                    ,@Grade
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        SET @CurrentScore = (
                                            SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                        ELSE NULL
                                                   END AS CurrentScore
                                            FROM   (
                                                   SELECT   InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight AS GradeBookWeight
                                                           ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                 ELSE 0
                                                            END AS ActualWeight
                                                   FROM     (
                                                            SELECT   C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,ISNULL(C.Weight, 0) AS Weight
                                                                    ,C.Number AS MinNumber
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter AS Param
                                                                    ,X.GrdScaleId
                                                                    ,SUM(GR.Score) AS Score
                                                                    ,COUNT(D.Descrip) AS NumberOfComponents
                                                            FROM     (
                                                                     SELECT   DISTINCT TOP 1 A.InstrGrdBkWgtId
                                                                                            ,A.EffectiveDate
                                                                                            ,B.GrdScaleId
                                                                     FROM     arGrdBkWeights A
                                                                             ,arClassSections B
                                                                     WHERE    A.ReqId = B.ReqId
                                                                              AND A.EffectiveDate <= B.StartDate
                                                                              AND B.ClsSectionId = @ClsSectionId
                                                                     ORDER BY A.EffectiveDate DESC
                                                                     ) X
                                                                    ,arGrdBkWgtDetails C
                                                                    ,arGrdComponentTypes D
                                                                    ,arGrdBkResults GR
                                                            WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                     AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                     AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                     AND D.SysComponentTypeId NOT IN ( 500, 503 )
                                                                     AND GR.StuEnrollId = @StuEnrollId
                                                                     AND GR.ClsSectionId = @ClsSectionId
                                                                     AND GR.Score IS NOT NULL
                                                            GROUP BY C.InstrGrdBkWgtDetailId
                                                                    ,D.Code
                                                                    ,D.Descrip
                                                                    ,C.Weight
                                                                    ,C.Number
                                                                    ,C.GrdPolicyId
                                                                    ,C.Parameter
                                                                    ,X.GrdScaleId
                                                            ) S
                                                   GROUP BY InstrGrdBkWgtDetailId
                                                           ,Code
                                                           ,Descrip
                                                           ,Weight
                                                           ,NumberOfComponents
                                                   ) FinalTblToComputeCurrentScore
                                            );
                        IF ( @CurrentScore IS NULL )
                            BEGIN
                                -- instructor grade books
                                SET @CurrentScore = (
                                                    SELECT CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight)) * 100
                                                                ELSE NULL
                                                           END AS CurrentScore
                                                    FROM   (
                                                           SELECT   InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight AS GradeBookWeight
                                                                   ,CASE WHEN S.NumberOfComponents > 0 THEN SUM(( Weight * ( Score / NumberOfComponents )) / 100)
                                                                         ELSE 0
                                                                    END AS ActualWeight
                                                           FROM     (
                                                                    SELECT   C.InstrGrdBkWgtDetailId
                                                                            ,D.Code
                                                                            ,D.Descrip
                                                                            ,ISNULL(C.Weight, 0) AS Weight
                                                                            ,C.Number AS MinNumber
                                                                            ,C.GrdPolicyId
                                                                            ,C.Parameter AS Param
                                                                            ,X.GrdScaleId
                                                                            ,SUM(GR.Score) AS Score
                                                                            ,COUNT(D.Descrip) AS NumberOfComponents
                                                                    FROM     (
                                                                             --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
                                                                             --FROM          arGrdBkWeights A,arClassSections B        
                                                                             --WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
                                                                             --ORDER BY      A.EffectiveDate DESC
                                                                             SELECT DISTINCT TOP 1 t1.InstrGrdBkWgtId
                                                                                                  ,t1.GrdScaleId
                                                                             FROM   arClassSections t1
                                                                                   ,arGrdBkWeights t2
                                                                             WHERE  t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                                    AND t1.ClsSectionId = @ClsSectionId
                                                                             ) X
                                                                            ,arGrdBkWgtDetails C
                                                                            ,arGrdComponentTypes D
                                                                            ,arGrdBkResults GR
                                                                    WHERE    X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                             AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                             AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                             AND
                                                                        -- D.SysComponentTypeID not in (500,503) and 
                                                                        GR.StuEnrollId = @StuEnrollId
                                                                             AND GR.ClsSectionId = @ClsSectionId
                                                                             AND GR.Score IS NOT NULL
                                                                    GROUP BY C.InstrGrdBkWgtDetailId
                                                                            ,D.Code
                                                                            ,D.Descrip
                                                                            ,C.Weight
                                                                            ,C.Number
                                                                            ,C.GrdPolicyId
                                                                            ,C.Parameter
                                                                            ,X.GrdScaleId
                                                                    ) S
                                                           GROUP BY InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight
                                                                   ,NumberOfComponents
                                                           ) FinalTblToComputeCurrentScore
                                                    );

                            END;

                        -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore, 0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;

                        UPDATE syCreditSummary
                        SET    FinalScore = @FinalScore
                              ,CurrentScore = @CurrentScore
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId
                               AND ReqId = @reqid;

                        --Average calculation
                        --			declare @CumAverage decimal(18,2),@cumAverageSum decimal(18,2),@cumAveragecount int

                        -- Term Average
                        SET @TermAverageCount = (
                                                SELECT COUNT(*)
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                       AND TermId = @TermId
                                                       AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                              SELECT SUM(FinalScore)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND TermId = @TermId
                                                     AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount;

                        -- Cumulative Average
                        SET @cumAveragecount = (
                                               SELECT COUNT(*)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                             SELECT SUM(FinalScore)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount;

                        UPDATE syCreditSummary
                        SET    Average = @TermAverage
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId;

                        --Update Cumulative GPA
                        UPDATE syCreditSummary
                        SET    CumAverage = @CumAverage
                        WHERE  StuEnrollId = @StuEnrollId;


                        FETCH NEXT FROM GetCreditsSummary_Cursor
                        INTO @StuEnrollId
                            ,@TermId
                            ,@TermDescrip
                            ,@termstartdate1
                            ,@reqid
                            ,@CourseCodeDescrip
                            ,@FinalScore
                            ,@FinalGrade
                            ,@sysComponentTypeId
                            ,@CreditsAttempted
                            ,@ClsSectionId
                            ,@Grade
                            ,@IsPass
                            ,@IsCreditsAttempted
                            ,@IsCreditsEarned
                            ,@PrgVerId
                            ,@IsInGPA
                            ,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

                DECLARE @GradeSystemDetailId UNIQUEIDENTIFIER
                       ,@IsGPA BIT;
                DECLARE GetCreditsSummary_Cursor CURSOR FOR
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,T.TermId
                                       ,T.TermDescrip
                                       ,T.StartDate
                                       ,R.ReqId
                                       ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                       ,NULL AS FinalScore
                                       ,RES.GrdSysDetailId AS GradeSystemDetailId
                                       ,GSD.Grade AS FinalGrade
                                       ,GSD.Grade AS CurrentGrade
                                       ,R.Credits
                                       ,NULL AS ClsSectionId
                                       ,GSD.IsPass
                                       ,GSD.IsCreditsAttempted
                                       ,GSD.IsCreditsEarned
                                       ,GSD.GPA
                                       ,GSD.IsInGPA
                                       ,SE.PrgVerId
                                       ,GSD.IsInGPA
                                       ,R.FinAidCredits AS FinAidCredits
                    FROM       arStuEnrollments SE
                    INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
                    INNER JOIN syCreditSummary CS ON CS.StuEnrollId = RES.StuEnrollId
                    INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                    INNER JOIN arTerm T ON RES.TermId = T.TermId
                    WHERE      RES.ReqId NOT IN (
                                                SELECT DISTINCT ReqId
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = SE.StuEnrollId
                                                       AND TermId = T.TermId
                                                )
                               AND SE.StuEnrollId = @StuEnrollId
                               AND (
                                   RES.CompletedDate IS NOT NULL
                                   AND RES.CompletedDate <= @OffsetDate
                                   );

                SET @varGradeRounding = (
                                        SELECT Value
                                        FROM   syConfigAppSetValues
                                        WHERE  SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@GradeSystemDetailId
                    ,@FinalGrade
                    ,@CurrentGrade
                    ,@CourseCredits
                    ,@ClsSectionId
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@GPA
                    ,@IsGPA
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore, 0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;

                        UPDATE syCreditSummary
                        SET    FinalScore = @FinalScore
                              ,CurrentScore = @CurrentScore
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId
                               AND ReqId = @reqid;

                        --Average calculation

                        -- Term Average
                        SET @TermAverageCount = (
                                                SELECT COUNT(*)
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                       AND TermId = @TermId
                                                       AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                              SELECT SUM(FinalScore)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND TermId = @TermId
                                                     AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount;

                        -- Cumulative Average
                        SET @cumAveragecount = (
                                               SELECT COUNT(*)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                               --AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                             SELECT SUM(FinalScore)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount;

                        UPDATE syCreditSummary
                        SET    Average = @TermAverage
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId;

                        --Update Cumulative GPA
                        UPDATE syCreditSummary
                        SET    CumAverage = @CumAverage
                        WHERE  StuEnrollId = @StuEnrollId;


                        FETCH NEXT FROM GetCreditsSummary_Cursor
                        INTO @StuEnrollId
                            ,@TermId
                            ,@TermDescrip
                            ,@termstartdate1
                            ,@reqid
                            ,@CourseCodeDescrip
                            ,@FinalScore
                            ,@GradeSystemDetailId
                            ,@FinalGrade
                            ,@CurrentGrade
                            ,@CourseCredits
                            ,@ClsSectionId
                            ,@IsPass
                            ,@IsCreditsAttempted
                            ,@IsCreditsEarned
                            ,@GPA
                            ,@IsGPA
                            ,@PrgVerId
                            ,@IsInGPA
                            ,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

                DECLARE GetCreditsSummary_Cursor CURSOR FOR
                    SELECT     DISTINCT SE.StuEnrollId
                                       ,T.TermId
                                       ,T.TermDescrip
                                       ,T.StartDate
                                       ,R.ReqId
                                       ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                                       ,NULL AS FinalScore
                                       ,RES.GrdSysDetailId AS GradeSystemDetailId
                                       ,GSD.Grade AS FinalGrade
                                       ,GSD.Grade AS CurrentGrade
                                       ,R.Credits
                                       ,NULL AS ClsSectionId
                                       ,GSD.IsPass
                                       ,GSD.IsCreditsAttempted
                                       ,GSD.IsCreditsEarned
                                       ,GSD.GPA
                                       ,GSD.IsInGPA
                                       ,SE.PrgVerId
                                       ,GSD.IsInGPA
                                       ,R.FinAidCredits AS FinAidCredits
                    FROM       arStuEnrollments SE
                    INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
                    INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                    INNER JOIN arTerm T ON RES.TermId = T.TermId
                    WHERE      SE.StuEnrollId NOT IN (
                                                     SELECT DISTINCT StuEnrollId
                                                     FROM   syCreditSummary
                                                     )
                               AND SE.StuEnrollId = @StuEnrollId
                               AND (
                                   RES.CompletedDate IS NOT NULL
                                   AND RES.CompletedDate <= @OffsetDate
                                   );

                SET @varGradeRounding = (
                                        SELECT Value
                                        FROM   syConfigAppSetValues
                                        WHERE  SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor
                INTO @StuEnrollId
                    ,@TermId
                    ,@TermDescrip
                    ,@termstartdate1
                    ,@reqid
                    ,@CourseCodeDescrip
                    ,@FinalScore
                    ,@GradeSystemDetailId
                    ,@FinalGrade
                    ,@CurrentGrade
                    ,@CourseCredits
                    ,@ClsSectionId
                    ,@IsPass
                    ,@IsCreditsAttempted
                    ,@IsCreditsEarned
                    ,@GPA
                    ,@IsGPA
                    ,@PrgVerId
                    ,@IsInGPA
                    ,@FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN

                        -- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore, 0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                   AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore, 0);
                                    END;
                            END;

                        UPDATE syCreditSummary
                        SET    FinalScore = @FinalScore
                              ,CurrentScore = @CurrentScore
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId
                               AND ReqId = @reqid;

                        --Average calculation
                        SET @TermAverageCount = (
                                                SELECT COUNT(*)
                                                FROM   syCreditSummary
                                                WHERE  StuEnrollId = @StuEnrollId
                                                       AND TermId = @TermId
                                                       AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                              SELECT SUM(FinalScore)
                                              FROM   syCreditSummary
                                              WHERE  StuEnrollId = @StuEnrollId
                                                     AND TermId = @TermId
                                                     AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount;

                        -- Cumulative Average
                        SET @cumAveragecount = (
                                               SELECT COUNT(*)
                                               FROM   syCreditSummary
                                               WHERE  StuEnrollId = @StuEnrollId
                                                      AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                             SELECT SUM(FinalScore)
                                             FROM   syCreditSummary
                                             WHERE  StuEnrollId = @StuEnrollId
                                                    AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount;

                        UPDATE syCreditSummary
                        SET    Average = @TermAverage
                        WHERE  StuEnrollId = @StuEnrollId
                               AND TermId = @TermId;

                        --Update Cumulative GPA
                        UPDATE syCreditSummary
                        SET    CumAverage = @CumAverage
                        WHERE  StuEnrollId = @StuEnrollId;


                        FETCH NEXT FROM GetCreditsSummary_Cursor
                        INTO @StuEnrollId
                            ,@TermId
                            ,@TermDescrip
                            ,@termstartdate1
                            ,@reqid
                            ,@CourseCodeDescrip
                            ,@FinalScore
                            ,@GradeSystemDetailId
                            ,@FinalGrade
                            ,@CurrentGrade
                            ,@CourseCredits
                            ,@ClsSectionId
                            ,@IsPass
                            ,@IsCreditsAttempted
                            ,@IsCreditsEarned
                            ,@GPA
                            ,@IsGPA
                            ,@PrgVerId
                            ,@IsInGPA
                            ,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

            ---- PRINT ''@CurrentBal_Compute'', 
            ---- PRINT @CurrentBal_Compute


            END;

        SET @TermAverageCount = (
                                SELECT COUNT(*)
                                FROM   syCreditSummary
                                WHERE  StuEnrollId = @StuEnrollId
                                       AND FinalScore IS NOT NULL
                                );
        SET @termAverageSum = (
                              SELECT SUM(FinalScore)
                              FROM   syCreditSummary
                              WHERE  StuEnrollId = @StuEnrollId
                                     AND FinalScore IS NOT NULL
                              );
        IF @TermAverageCount >= 1
            BEGIN
                SET @TermAverage = @termAverageSum / @TermAverageCount;
            END;

        -- Cumulative Average
        SET @cumAveragecount = (
                               SELECT COUNT(*)
                               FROM   (
                                      SELECT     sCS.StuEnrollId
                                                ,sCS.TermId
                                                ,sCS.TermDescrip
                                                ,sCS.ReqId
                                                ,sCS.ReqDescrip
                                                ,sCS.ClsSectionId
                                                ,sCS.CreditsEarned
                                                ,sCS.CreditsAttempted
                                                ,sCS.CurrentScore
                                                ,sCS.CurrentGrade
                                                ,sCS.FinalScore
                                                ,sCS.FinalGrade
                                                ,sCS.Completed
                                                ,sCS.FinalGPA
                                                ,sCS.Product_WeightedAverage_Credits_GPA
                                                ,sCS.Count_WeightedAverage_Credits
                                                ,sCS.Product_SimpleAverage_Credits_GPA
                                                ,sCS.Count_SimpleAverage_Credits
                                                ,sCS.ModUser
                                                ,sCS.ModDate
                                                ,sCS.TermGPA_Simple
                                                ,sCS.TermGPA_Weighted
                                                ,sCS.coursecredits
                                                ,sCS.CumulativeGPA
                                                ,sCS.CumulativeGPA_Simple
                                                ,sCS.FACreditsEarned
                                                ,sCS.Average
                                                ,sCS.CumAverage
                                                ,sCS.TermStartDate
                                      FROM       dbo.syCreditSummary sCS
                                      INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                      INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                      WHERE      sCS.StuEnrollId = @StuEnrollId
                                                 AND CS.ReqId = sCS.ReqId
                                                 AND (
                                                     R.DateCompleted IS NOT NULL
                                                     AND R.DateCompleted <= @OffsetDate
                                                     )
                                      UNION ALL
                                      (SELECT     sCS.StuEnrollId
                                                 ,sCS.TermId
                                                 ,sCS.TermDescrip
                                                 ,sCS.ReqId
                                                 ,sCS.ReqDescrip
                                                 ,sCS.ClsSectionId
                                                 ,sCS.CreditsEarned
                                                 ,sCS.CreditsAttempted
                                                 ,sCS.CurrentScore
                                                 ,sCS.CurrentGrade
                                                 ,sCS.FinalScore
                                                 ,sCS.FinalGrade
                                                 ,sCS.Completed
                                                 ,sCS.FinalGPA
                                                 ,sCS.Product_WeightedAverage_Credits_GPA
                                                 ,sCS.Count_WeightedAverage_Credits
                                                 ,sCS.Product_SimpleAverage_Credits_GPA
                                                 ,sCS.Count_SimpleAverage_Credits
                                                 ,sCS.ModUser
                                                 ,sCS.ModDate
                                                 ,sCS.TermGPA_Simple
                                                 ,sCS.TermGPA_Weighted
                                                 ,sCS.coursecredits
                                                 ,sCS.CumulativeGPA
                                                 ,sCS.CumulativeGPA_Simple
                                                 ,sCS.FACreditsEarned
                                                 ,sCS.Average
                                                 ,sCS.CumAverage
                                                 ,sCS.TermStartDate
                                       FROM       dbo.syCreditSummary sCS
                                       INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                       WHERE      sCS.StuEnrollId = @StuEnrollId
                                                  AND tG.ReqId = sCS.ReqId
                                                  AND (
                                                      tG.CompletedDate IS NOT NULL
                                                      AND tG.CompletedDate <= @OffsetDate
                                                      ))
                                      ) sCSEA
                               WHERE  StuEnrollId = @StuEnrollId
                                      AND FinalScore IS NOT NULL
                               );
        SET @cumAverageSum = (
                             SELECT SUM(FinalScore)
                             FROM   (
                                    SELECT     sCS.StuEnrollId
                                              ,sCS.TermId
                                              ,sCS.TermDescrip
                                              ,sCS.ReqId
                                              ,sCS.ReqDescrip
                                              ,sCS.ClsSectionId
                                              ,sCS.CreditsEarned
                                              ,sCS.CreditsAttempted
                                              ,sCS.CurrentScore
                                              ,sCS.CurrentGrade
                                              ,sCS.FinalScore
                                              ,sCS.FinalGrade
                                              ,sCS.Completed
                                              ,sCS.FinalGPA
                                              ,sCS.Product_WeightedAverage_Credits_GPA
                                              ,sCS.Count_WeightedAverage_Credits
                                              ,sCS.Product_SimpleAverage_Credits_GPA
                                              ,sCS.Count_SimpleAverage_Credits
                                              ,sCS.ModUser
                                              ,sCS.ModDate
                                              ,sCS.TermGPA_Simple
                                              ,sCS.TermGPA_Weighted
                                              ,sCS.coursecredits
                                              ,sCS.CumulativeGPA
                                              ,sCS.CumulativeGPA_Simple
                                              ,sCS.FACreditsEarned
                                              ,sCS.Average
                                              ,sCS.CumAverage
                                              ,sCS.TermStartDate
                                    FROM       dbo.syCreditSummary sCS
                                    INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                    INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                    WHERE      sCS.StuEnrollId = @StuEnrollId
                                               AND CS.ReqId = sCS.ReqId
                                               AND (
                                                   R.DateCompleted IS NOT NULL
                                                   AND R.DateCompleted <= @OffsetDate
                                                   )
                                    UNION ALL
                                    (SELECT     sCS.StuEnrollId
                                               ,sCS.TermId
                                               ,sCS.TermDescrip
                                               ,sCS.ReqId
                                               ,sCS.ReqDescrip
                                               ,sCS.ClsSectionId
                                               ,sCS.CreditsEarned
                                               ,sCS.CreditsAttempted
                                               ,sCS.CurrentScore
                                               ,sCS.CurrentGrade
                                               ,sCS.FinalScore
                                               ,sCS.FinalGrade
                                               ,sCS.Completed
                                               ,sCS.FinalGPA
                                               ,sCS.Product_WeightedAverage_Credits_GPA
                                               ,sCS.Count_WeightedAverage_Credits
                                               ,sCS.Product_SimpleAverage_Credits_GPA
                                               ,sCS.Count_SimpleAverage_Credits
                                               ,sCS.ModUser
                                               ,sCS.ModDate
                                               ,sCS.TermGPA_Simple
                                               ,sCS.TermGPA_Weighted
                                               ,sCS.coursecredits
                                               ,sCS.CumulativeGPA
                                               ,sCS.CumulativeGPA_Simple
                                               ,sCS.FACreditsEarned
                                               ,sCS.Average
                                               ,sCS.CumAverage
                                               ,sCS.TermStartDate
                                     FROM       dbo.syCreditSummary sCS
                                     INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                     WHERE      sCS.StuEnrollId = @StuEnrollId
                                                AND tG.ReqId = sCS.ReqId
                                                AND (
                                                    tG.CompletedDate IS NOT NULL
                                                    AND tG.CompletedDate <= @OffsetDate
                                                    ))
                                    ) sCSEA
                             WHERE  StuEnrollId = @StuEnrollId
                                    AND FinalScore IS NOT NULL
                             );

        IF @cumAveragecount >= 1
            BEGIN
                SET @CumAverage = @cumAverageSum / @cumAveragecount;
            END;

        IF @GradesFormat <> ''letter''
            BEGIN
                SET @Qualitative = @CumAverage;
            END;
        ELSE
            SET @Qualitative = @cumWeightedGPA;

        IF ( @QuantMinUnitTypeId = 3 )
            BEGIN
                IF @UnitTypeDescrip IN ( ''none'', ''present absent'' )
                   AND @TrackSapAttendance = ''byclass''
                    BEGIN
                        SET @Quantitative = (
                                            SELECT CAST(( sas.Present / sas.Scheduled ) * 100 AS DECIMAL)
                                            FROM   (
                                                   SELECT   MAX(convertedSAS.ConvertedActualRunningScheduledDays) AS Scheduled
                                                           ,MAX(convertedSAS.ConvertedActualRunningPresentDays)
                                                            + MAX(convertedSAS.ConvertedActualRunningMakeupDays) AS Present -- add makeup to present since procedure splits it before added into syattendance summary
                                                   FROM     (
                                                            SELECT    sySas.StuEnrollId
                                                                     ,sySas.StudentAttendedDate
                                                                     ,sySas.ClsSectionId
                                                                     ,sySas.ActualRunningScheduledDays * ISNULL(cim.IntervalInMinutes, 1) AS ConvertedActualRunningScheduledDays
                                                                     ,sySas.ActualRunningPresentDays * ISNULL(cim.IntervalInMinutes, 1) AS ConvertedActualRunningPresentDays
                                                                     ,sySas.ActualRunningMakeupDays * ISNULL(cim.IntervalInMinutes, 1) AS ConvertedActualRunningMakeupDays
                                                            FROM      @attendanceSummary sySas
                                                            LEFT JOIN @ClsSectionIntervalMinutes cim ON cim.ClsSectionId = sySas.ClsSectionId
                                                            ) AS convertedSAS
                                                   WHERE    convertedSAS.StuEnrollId = @StuEnrollId
                                                            AND convertedSAS.StudentAttendedDate <= @OffsetDate
                                                   GROUP BY StuEnrollId
                                                   ) AS sas
                                            );



                    END;
                ELSE
                    BEGIN
                        SET @Quantitative = (
                                            SELECT CAST(( sas.Present / sas.Scheduled ) * 100 AS DECIMAL)
                                            FROM   (
                                                   SELECT   MAX(ActualRunningScheduledDays) AS Scheduled
                                                           ,MAX(ActualRunningPresentDays) + MAX(ActualRunningMakeupDays) AS Present -- add makeup to present since procedure splits it before added into syattendance summary
                                                   FROM     @attendanceSummary
                                                   WHERE    StuEnrollId = @StuEnrollId
                                                            AND StudentAttendedDate <= @OffsetDate
                                                   GROUP BY StuEnrollId
                                                   ) sas
                                            );
                    END;

            END;
        ELSE
            BEGIN
                SET @Quantitative = (
                                    SELECT CAST(( sCSEA.Earned / sCSEA.Attempted ) * 100 AS DECIMAL)
                                    FROM   (
                                           SELECT     SUM(sCS.CreditsEarned) Earned
                                                     ,SUM(sCS.CreditsAttempted) Attempted
                                           FROM       dbo.syCreditSummary sCS
                                           INNER JOIN dbo.arClassSections CS ON sCS.ReqId = CS.ReqId
                                           INNER JOIN arResults R ON R.StuEnrollId = sCS.StuEnrollId
                                           WHERE      sCS.StuEnrollId = @StuEnrollId
                                                      AND CS.ReqId = sCS.ReqId
                                                      AND (
                                                          R.DateCompleted IS NOT NULL
                                                          AND R.DateCompleted <= @OffsetDate
                                                          )
                                           GROUP BY   sCS.StuEnrollId
                                           UNION ALL
                                           (SELECT     SUM(sCS.CreditsEarned)
                                                      ,SUM(sCS.CreditsAttempted)
                                            FROM       dbo.syCreditSummary sCS
                                            INNER JOIN dbo.arTransferGrades tG ON tG.StuEnrollId = sCS.StuEnrollId
                                            WHERE      sCS.StuEnrollId = @StuEnrollId
                                                       AND tG.ReqId = sCS.ReqId
                                                       AND (
                                                           tG.CompletedDate IS NOT NULL
                                                           AND tG.CompletedDate <= @OffsetDate
                                                           )
                                            GROUP BY   sCS.StuEnrollId)
                                           ) sCSEA
                                    );
            END;

        --If Clock Hour / Numeric - use New Average Calculation
        IF (
           SELECT TOP 1 P.ACId
           FROM   dbo.arStuEnrollments E
           JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
           JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
           WHERE  E.StuEnrollId = @StuEnrollId
           ) = 5
           AND @GradesFormat = ''numeric''
            BEGIN
                EXEC dbo.USP_GPACalculator @EnrollmentId = @StuEnrollId
                                          ,@EndDate = @OffsetDate
                                          ,@StudentGPA = @CumAverage OUTPUT;
            END;

    --SELECT * FROM dbo.syCreditSummary WHERE StuEnrollId  = ''F3616299-DCF2-4F59-BC8F-2F3034ED01A3''

    ---- PRINT @cumAveragecount 
    ---- PRINT @CumAverageSum
    ---- PRINT @CumAverage
    ---- PRINT @UnitTypeId;
    ---- PRINT @TrackSapAttendance;
    ---- PRINT @displayHours;
    END;

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating [dbo].[Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId]'
GO
IF OBJECT_ID(N'[dbo].[Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId]', 'P') IS NULL
EXEC sp_executesql N'--=================================================================================================
-- Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId
--=================================================================================================
CREATE PROCEDURE [dbo].[Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId]
    @StuEnrollId VARCHAR(MAX) = NULL
   ,@CumAverage DECIMAL(18,2) OUTPUT
   ,@cumWeightedGPA DECIMAL(18,2) OUTPUT
   ,@cumSimpleGPA DECIMAL(18,2) OUTPUT
AS
    BEGIN
        DECLARE @TrackSapAttendance VARCHAR(1000);  
        DECLARE @displayHours AS VARCHAR(1000);   
        DECLARE @GradeCourseRepetitionsMethod VARCHAR(1000);
        DECLARE @GradesFormat AS VARCHAR(1000);
        DECLARE @StuEnrollCampusId UNIQUEIDENTIFIER;
		
        SET @StuEnrollCampusId = COALESCE((
                                            SELECT  ASE.CampusId
                                            FROM    arStuEnrollments AS ASE
                                            WHERE   ASE.StuEnrollId = @StuEnrollId
                                          ),NULL);
        SET @TrackSapAttendance = dbo.GetAppSettingValueByKeyName(''TrackSapAttendance'',@StuEnrollCampusId);
        IF ( @TrackSapAttendance IS NOT NULL )
            BEGIN
                SET @TrackSapAttendance = LOWER(LTRIM(RTRIM(@TrackSapAttendance)));
            END;

        SET @GradeCourseRepetitionsMethod = dbo.GetAppSettingValueByKeyName(''GradeCourseRepetitionsMethod'',@StuEnrollCampusId); 
        SET @GradesFormat = dbo.GetAppSettingValueByKeyName(''GradesFormat'',@StuEnrollCampusId);
        IF ( @GradesFormat IS NOT NULL )
            BEGIN
                SET @GradesFormat = LOWER(LTRIM(RTRIM(@GradesFormat)));
            END;



        DECLARE @CoursesNotRepeated TABLE
            (
             StuEnrollId UNIQUEIDENTIFIER
            ,TermId UNIQUEIDENTIFIER
            ,TermDescrip VARCHAR(100)
            ,ReqId UNIQUEIDENTIFIER
            ,ReqDescrip VARCHAR(100)
            ,ClsSectionId VARCHAR(50)
            ,CreditsEarned DECIMAL(18,2)
            ,CreditsAttempted DECIMAL(18,2)
            ,CurrentScore DECIMAL(18,2)
            ,CurrentGrade VARCHAR(10)
            ,FinalScore DECIMAL(18,2)
            ,FinalGrade VARCHAR(10)
            ,Completed BIT
            ,FinalGPA DECIMAL(18,2)
            ,Product_WeightedAverage_Credits_GPA DECIMAL(18,2)
            ,Count_WeightedAverage_Credits DECIMAL(18,2)
            ,Product_SimpleAverage_Credits_GPA DECIMAL(18,2)
            ,Count_SimpleAverage_Credits DECIMAL(18,2)
            ,ModUser VARCHAR(50)
            ,ModDate DATETIME
            ,TermGPA_Simple DECIMAL(18,2)
            ,TermGPA_Weighted DECIMAL(18,2)
            ,coursecredits DECIMAL(18,2)
            ,CumulativeGPA DECIMAL(18,2)
            ,CumulativeGPA_Simple DECIMAL(18,2)
            ,FACreditsEarned DECIMAL(18,2)
            ,Average DECIMAL(18,2)
            ,CumAverage DECIMAL(18,2)
            ,TermStartDate DATETIME
            ,rownumber INT
            );

        INSERT  INTO @CoursesNotRepeated
                SELECT  StuEnrollId
                       ,TermId
                       ,TermDescrip
                       ,ReqId
                       ,ReqDescrip
                       ,ClsSectionId
                       ,CreditsEarned
                       ,CreditsAttempted
                       ,CurrentScore
                       ,CurrentGrade
                       ,FinalScore
                       ,FinalGrade
                       ,Completed
                       ,FinalGPA
                       ,Product_WeightedAverage_Credits_GPA
                       ,Count_WeightedAverage_Credits
                       ,Product_SimpleAverage_Credits_GPA
                       ,Count_SimpleAverage_Credits
                       ,ModUser
                       ,ModDate
                       ,TermGPA_Simple
                       ,TermGPA_Weighted
                       ,coursecredits
                       ,CumulativeGPA
                       ,CumulativeGPA_Simple
                       ,FACreditsEarned
                       ,Average
                       ,CumAverage
                       ,TermStartDate
                       ,NULL AS rownumber
                FROM    syCreditSummary
                WHERE   StuEnrollId = @StuEnrollId
                        AND ReqId IN ( SELECT   ReqId
                                       FROM     (
                                                  SELECT    ReqId
                                                           ,ReqDescrip
                                                           ,COUNT(*) AS counter
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = @StuEnrollId
                                                  GROUP BY  ReqId
                                                           ,ReqDescrip
                                                  HAVING    COUNT(*) = 1
                                                ) dt );

	

        IF LOWER(@GradeCourseRepetitionsMethod) = ''latest''
            BEGIN
                INSERT  INTO @CoursesNotRepeated
                        SELECT  dt2.StuEnrollId
                               ,dt2.TermId
                               ,dt2.TermDescrip
                               ,dt2.ReqId
                               ,dt2.ReqDescrip
                               ,dt2.ClsSectionId
                               ,dt2.CreditsEarned
                               ,dt2.CreditsAttempted
                               ,dt2.CurrentScore
                               ,dt2.CurrentGrade
                               ,dt2.FinalScore
                               ,dt2.FinalGrade
                               ,dt2.Completed
                               ,dt2.FinalGPA
                               ,dt2.Product_WeightedAverage_Credits_GPA
                               ,dt2.Count_WeightedAverage_Credits
                               ,dt2.Product_SimpleAverage_Credits_GPA
                               ,dt2.Count_SimpleAverage_Credits
                               ,dt2.ModUser
                               ,dt2.ModDate
                               ,dt2.TermGPA_Simple
                               ,dt2.TermGPA_Weighted
                               ,dt2.coursecredits
                               ,dt2.CumulativeGPA
                               ,dt2.CumulativeGPA_Simple
                               ,dt2.FACreditsEarned
                               ,dt2.Average
                               ,dt2.CumAverage
                               ,dt2.TermStartDate
                               ,dt2.RowNumber
                        FROM    (
                                  SELECT    StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY TermStartDate DESC ) AS RowNumber
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND (
                                                  FinalScore IS NOT NULL
                                                  OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN ( SELECT   ReqId
                                                           FROM     (
                                                                      SELECT    ReqId
                                                                               ,ReqDescrip
                                                                               ,COUNT(*) AS Counter
                                                                      FROM      syCreditSummary
                                                                      WHERE     StuEnrollId = @StuEnrollId
                                                                      GROUP BY  ReqId
                                                                               ,ReqDescrip
                                                                      HAVING    COUNT(*) > 1
                                                                    ) dt )
                                ) dt2
                        WHERE   RowNumber = 1
                        ORDER BY TermStartDate DESC;
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''best''
            BEGIN
                INSERT  INTO @CoursesNotRepeated
                        SELECT  dt2.StuEnrollId
                               ,dt2.TermId
                               ,dt2.TermDescrip
                               ,dt2.ReqId
                               ,dt2.ReqDescrip
                               ,dt2.ClsSectionId
                               ,dt2.CreditsEarned
                               ,dt2.CreditsAttempted
                               ,dt2.CurrentScore
                               ,dt2.CurrentGrade
                               ,dt2.FinalScore
                               ,dt2.FinalGrade
                               ,dt2.Completed
                               ,dt2.FinalGPA
                               ,dt2.Product_WeightedAverage_Credits_GPA
                               ,dt2.Count_WeightedAverage_Credits
                               ,dt2.Product_SimpleAverage_Credits_GPA
                               ,dt2.Count_SimpleAverage_Credits
                               ,dt2.ModUser
                               ,dt2.ModDate
                               ,dt2.TermGPA_Simple
                               ,dt2.TermGPA_Weighted
                               ,dt2.coursecredits
                               ,dt2.CumulativeGPA
                               ,dt2.CumulativeGPA_Simple
                               ,dt2.FACreditsEarned
                               ,dt2.Average
                               ,dt2.CumAverage
                               ,dt2.TermStartDate
                               ,dt2.RowNumber
                        FROM    (
                                  SELECT    StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY Completed DESC, CreditsEarned DESC, FinalGPA DESC ) AS RowNumber
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND (
                                                  FinalScore IS NOT NULL
                                                  OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN ( SELECT   ReqId
                                                           FROM     (
                                                                      SELECT    ReqId
                                                                               ,ReqDescrip
                                                                               ,COUNT(*) AS Counter
                                                                      FROM      syCreditSummary
                                                                      WHERE     StuEnrollId = @StuEnrollId
                                                                      GROUP BY  ReqId
                                                                               ,ReqDescrip
                                                                      HAVING    COUNT(*) > 1
                                                                    ) dt )
                                ) dt2
                        WHERE   RowNumber = 1; 
            END;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''average''
            BEGIN
                INSERT  INTO @CoursesNotRepeated
                        SELECT  dt2.StuEnrollId
                               ,dt2.TermId
                               ,dt2.TermDescrip
                               ,dt2.ReqId
                               ,dt2.ReqDescrip
                               ,dt2.ClsSectionId
                               ,dt2.CreditsEarned
                               ,dt2.CreditsAttempted
                               ,dt2.CurrentScore
                               ,dt2.CurrentGrade
                               ,dt2.FinalScore
                               ,dt2.FinalGrade
                               ,dt2.Completed
                               ,dt2.FinalGPA
                               ,dt2.Product_WeightedAverage_Credits_GPA
                               ,dt2.Count_WeightedAverage_Credits
                               ,dt2.Product_SimpleAverage_Credits_GPA
                               ,dt2.Count_SimpleAverage_Credits
                               ,dt2.ModUser
                               ,dt2.ModDate
                               ,dt2.TermGPA_Simple
                               ,dt2.TermGPA_Weighted
                               ,dt2.coursecredits
                               ,dt2.CumulativeGPA
                               ,dt2.CumulativeGPA_Simple
                               ,dt2.FACreditsEarned
                               ,dt2.Average
                               ,dt2.CumAverage
                               ,dt2.TermStartDate
                               ,dt2.RowNumber
                        FROM    (
                                  SELECT    StuEnrollId
                                           ,TermId
                                           ,TermDescrip
                                           ,ReqId
                                           ,ReqDescrip
                                           ,ClsSectionId
                                           ,CreditsEarned
                                           ,CreditsAttempted
                                           ,CurrentScore
                                           ,CurrentGrade
                                           ,FinalScore
                                           ,FinalGrade
                                           ,Completed
                                           ,FinalGPA
                                           ,Product_WeightedAverage_Credits_GPA
                                           ,Count_WeightedAverage_Credits
                                           ,Product_SimpleAverage_Credits_GPA
                                           ,Count_SimpleAverage_Credits
                                           ,ModUser
                                           ,ModDate
                                           ,TermGPA_Simple
                                           ,TermGPA_Weighted
                                           ,coursecredits
                                           ,CumulativeGPA
                                           ,CumulativeGPA_Simple
                                           ,FACreditsEarned
                                           ,Average
                                           ,CumAverage
                                           ,TermStartDate
                                           ,ROW_NUMBER() OVER ( PARTITION BY ReqId ORDER BY FinalGPA DESC ) AS RowNumber
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND (
                                                  FinalScore IS NOT NULL
                                                  OR FinalGrade IS NOT NULL
                                                )
                                            AND ReqId IN ( SELECT   ReqId
                                                           FROM     (
                                                                      SELECT    ReqId
                                                                               ,ReqDescrip
                                                                               ,COUNT(*) AS Counter
                                                                      FROM      syCreditSummary
                                                                      WHERE     StuEnrollId = @StuEnrollId
                                                                      GROUP BY  ReqId
                                                                               ,ReqDescrip
                                                                      HAVING    COUNT(*) > 1
                                                                    ) dt )
                                ) dt2; 
            END;

   
        DECLARE @cumSimpleCourseCredits DECIMAL(18,2);
        DECLARE @cumSimple_GPA_Credits DECIMAL(18,2);
	 -- (OUTPUT parameter)  DECLARE @cumSimpleGPA DECIMAL(18, 2)
        
	

        DECLARE @cumCourseCredits DECIMAL(18,2)
           ,@cumWeighted_GPA_Credits DECIMAL(18,2);
        DECLARE @cumCourseCredits_repeated DECIMAL(18,2)
           ,@cumWeighted_GPA_Credits_repeated DECIMAL(18,2);

        PRINT @GradeCourseRepetitionsMethod;

        IF LOWER(@GradeCourseRepetitionsMethod) = ''average''
            BEGIN
                SET @cumWeightedGPA = 0;
                SET @cumSimpleGPA = 0;
					
                SET @cumSimpleCourseCredits = (
                                                SELECT  COUNT(*)
                                                FROM    @CoursesNotRepeated
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND FinalGPA IS NOT NULL
                                                        AND (
                                                              rownumber = 0
                                                              OR rownumber IS NULL
                                                            )
                                              );

                DECLARE @cumSimpleCourseCredits_repeated DECIMAL(18,2);
                SET @cumSimpleCourseCredits_repeated = (
                                                         SELECT SUM(CourseCreditCount)
                                                         FROM   (
                                                                  SELECT    ReqId
                                                                           ,COUNT(coursecredits) AS CourseCreditCount
                                                                  FROM      @CoursesNotRepeated
                                                                  WHERE     StuEnrollId IN ( @StuEnrollId )
                                                                            AND FinalGPA IS NOT NULL
                                                                            AND rownumber = 1
                                                                  GROUP BY  ReqId
                                                                ) dt
                                                       );
                SET @cumSimpleCourseCredits = @cumSimpleCourseCredits + ISNULL(@cumSimpleCourseCredits_repeated,0);
		
                DECLARE @cumSimple_GPA_Credits_repeated DECIMAL(18,2);

                SET @cumSimple_GPA_Credits = (
                                               SELECT   SUM(FinalGPA)
                                               FROM     @CoursesNotRepeated
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND FinalGPA IS NOT NULL
                                                        AND (
                                                              rownumber = 0
                                                              OR rownumber IS NULL
                                                            )
                                             );

                SET @cumSimple_GPA_Credits_repeated = (
                                                        SELECT  SUM(AverageGPA)
                                                        FROM    (
                                                                  SELECT    ReqId
                                                                           ,SUM(FinalGPA) / MAX(rownumber) AS AverageGPA
                                                                  FROM      @CoursesNotRepeated
                                                                  WHERE     StuEnrollId IN ( @StuEnrollId )
                                                                            AND FinalGPA IS NOT NULL
                                                                            AND rownumber >= 1
                                                                  GROUP BY  ReqId
                                                                ) dt
                                                      );

                SET @cumSimple_GPA_Credits = @cumSimple_GPA_Credits + ISNULL(@cumSimple_GPA_Credits_repeated,0.00);

                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END; 

                SET @cumCourseCredits = (
                                          SELECT    SUM(coursecredits)
                                          FROM      @CoursesNotRepeated
                                          WHERE     StuEnrollId IN ( @StuEnrollId )
                                                    AND FinalGPA IS NOT NULL
                                                    AND (
                                                          rownumber = 0
                                                          OR rownumber IS NULL
                                                        )
                                        );

                SET @cumCourseCredits_repeated = (
                                                   SELECT   SUM(CourseCreditCount)
                                                   FROM     (
                                                              SELECT    ReqId
                                                                       ,MAX(coursecredits) AS CourseCreditCount
                                                              FROM      @CoursesNotRepeated
                                                              WHERE     StuEnrollId IN ( @StuEnrollId )
                                                                        AND FinalGPA IS NOT NULL
                                                                        AND rownumber >= 1
                                                              GROUP BY  ReqId
                                                            ) dt
                                                 );
                SET @cumCourseCredits = @cumCourseCredits + ISNULL(@cumCourseCredits_repeated,0.00);

                PRINT @cumCourseCredits;

                SET @cumWeighted_GPA_Credits = (
                                                 SELECT SUM(coursecredits * FinalGPA)
                                                 FROM   @CoursesNotRepeated
                                                 WHERE  StuEnrollId IN ( @StuEnrollId )
                                                        AND FinalGPA IS NOT NULL
                                                        AND (
                                                              rownumber = 0
                                                              OR rownumber IS NULL
                                                            )
                                               );
                SET @cumWeighted_GPA_Credits_repeated = (
                                                          SELECT    SUM(CourseCreditCount)
                                                          FROM      (
                                                                      SELECT    ReqId
                                                                               ,SUM(coursecredits * FinalGPA) / MAX(rownumber) AS CourseCreditCount
                                                                      FROM      @CoursesNotRepeated
                                                                      WHERE     StuEnrollId IN ( @StuEnrollId )
                                                                                AND FinalGPA IS NOT NULL
                                                                                AND rownumber >= 1
                                                                      GROUP BY  ReqId
                                                                    ) dt
                                                        );
			
				
			
                SET @cumWeighted_GPA_Credits = @cumWeighted_GPA_Credits + ISNULL(@cumWeighted_GPA_Credits_repeated,0);
                PRINT @cumWeighted_GPA_Credits;

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END; 
            END;
        ELSE
            BEGIN 
                SET @cumSimpleGPA = 0;

                SET @cumSimpleCourseCredits = (
                                                SELECT  COUNT(*)
                                                FROM    @CoursesNotRepeated
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND FinalGPA IS NOT NULL
                                              );
                SET @cumSimple_GPA_Credits = (
                                               SELECT   SUM(FinalGPA)
                                               FROM     @CoursesNotRepeated
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND FinalGPA IS NOT NULL
                                             );		
                IF @cumSimpleCourseCredits >= 1
                    BEGIN
                        SET @cumSimpleGPA = @cumSimple_GPA_Credits / @cumSimpleCourseCredits;
                    END; 
                SET @cumWeightedGPA = 0;
                SET @cumCourseCredits = (
                                          SELECT    SUM(coursecredits)
                                          FROM      @CoursesNotRepeated
                                          WHERE     StuEnrollId = @StuEnrollId
                                                    AND FinalGPA IS NOT NULL
                                        );
                SET @cumWeighted_GPA_Credits = (
                                                 SELECT SUM(coursecredits * FinalGPA)
                                                 FROM   @CoursesNotRepeated
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND FinalGPA IS NOT NULL
                                               );

                IF @cumCourseCredits >= 1
                    BEGIN
                        SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
                    END; 

            END;




					
        IF @cumCourseCredits >= 1
            BEGIN
                SET @cumWeightedGPA = @cumWeighted_GPA_Credits / @cumCourseCredits;
            END; 


        DECLARE @TermAverageCount INT;
        DECLARE @TermAverage DECIMAL(18,2); 
		-- (output parameter)  DECLARE @CumAverage DECIMAL(18, 2)
        IF @GradesFormat <> ''letter''
            BEGIN

                DECLARE @termAverageSum DECIMAL(18,2)
                   ,@cumAverageSum DECIMAL(18,2)
                   ,@cumAveragecount INT;
                DECLARE @TardiesMakingAbsence INT
                   ,@UnitTypeDescrip VARCHAR(20)
                   ,@OriginalTardiesMakingAbsence INT;
                SET @TardiesMakingAbsence = (
                                              SELECT TOP 1
                                                        t1.TardiesMakingAbsence
                                              FROM      arPrgVersions t1
                                                       ,arStuEnrollments t2
                                              WHERE     t1.PrgVerId = t2.PrgVerId
                                                        AND t2.StuEnrollId = @StuEnrollId
                                            );
							
                SET @UnitTypeDescrip = (
                                         SELECT LTRIM(RTRIM(AAUT.UnitTypeDescrip))
                                         FROM   arAttUnitType AS AAUT
                                         INNER JOIN arPrgVersions AS APV ON APV.UnitTypeId = AAUT.UnitTypeId
                                         INNER JOIN arStuEnrollments AS ASE ON ASE.PrgVerId = APV.PrgVerId
                                         WHERE  ASE.StuEnrollId = @StuEnrollId
                                       );

                SET @OriginalTardiesMakingAbsence = (
                                                      SELECT TOP 1
                                                                tardiesmakingabsence
                                                      FROM      syStudentAttendanceSummary
                                                      WHERE     StuEnrollId = @StuEnrollId
                                                    );
                DECLARE @termstartdate1 DATETIME;

-- Declare Variables
                BEGIN -- Declare Variables
                    DECLARE @MeetDate DATETIME
                       ,@WeekDay VARCHAR(15)
                       ,@StartDate DATETIME
                       ,@EndDate DATETIME;
                    DECLARE @PeriodDescrip VARCHAR(50)
                       ,@Actual DECIMAL(18,2)
                       ,@Excused DECIMAL(18,2)
                       ,@ClsSectionId UNIQUEIDENTIFIER;
                    DECLARE @Absent DECIMAL(18,2)
                       ,@SchedHours DECIMAL(18,2)
                       ,@TardyMinutes DECIMAL(18,2);
                    DECLARE @tardy DECIMAL(18,2)
                       ,@tracktardies INT
                       ,@rownumber INT
                       ,@IsTardy BIT
                       ,@ActualRunningScheduledHours DECIMAL(18,2);
                    DECLARE @ActualRunningPresentHours DECIMAL(18,2)
                       ,@ActualRunningAbsentHours DECIMAL(18,2)
                       ,@ActualRunningTardyHours DECIMAL(18,2)
                       ,@ActualRunningMakeupHours DECIMAL(18,2);
                    DECLARE @PrevStuEnrollId UNIQUEIDENTIFIER
                       ,@intTardyBreakPoint INT
                       ,@AdjustedRunningPresentHours DECIMAL(18,2)
                       ,@AdjustedRunningAbsentHours DECIMAL(18,2)
                       ,@ActualRunningScheduledDays DECIMAL(18,2);
		--declare @Scheduledhours decimal(18,2),@ActualPresentDays_ConvertTo_Hours decimal(18,2),@ActualAbsentDays_ConvertTo_Hours decimal(18,2),@AttendanceTrack varchar(50)
                    DECLARE @TermId VARCHAR(50)
                       ,@TermDescrip VARCHAR(50)
                       ,@PrgVerId UNIQUEIDENTIFIER;
                    DECLARE @TardyHit VARCHAR(10)
                       ,@ScheduledMinutes DECIMAL(18,2);
                    DECLARE @Scheduledhours_noperiods DECIMAL(18,2)
                       ,@ActualPresentDays_ConvertTo_Hours_NoPeriods DECIMAL(18,2)
                       ,@ActualAbsentDays_ConvertTo_Hours_NoPeriods DECIMAL(18,2);
                    DECLARE @ActualTardyDays_ConvertTo_Hours_NoPeriods DECIMAL(18,2);
                    DECLARE @boolReset BIT
                       ,@MakeupHours DECIMAL(18,2)
                       ,@AdjustedPresentDaysComputed DECIMAL(18,2);		
                END;

-- Step 3  --  InsertAttendance_Class_PresentAbsent   -- UnitTypeDescrip = (''None'', ''Present Absent'') 
--         --  TrackSapAttendance = ''byclass''
                BEGIN  -- Step 3  --  InsertAttendance_Class_PresentAbsent   -- UnitTypeDescrip = (''None'', ''Present Absent'') 
--         --  TrackSapAttendance = ''byclass''
                    IF @UnitTypeDescrip IN ( ''none'',''present absent'' )
                        AND @TrackSapAttendance = ''byclass''
                        BEGIN
			---- PRINT ''Step1!!!!''
                            DELETE  FROM syStudentAttendanceSummary
                            WHERE   StuEnrollId = @StuEnrollId;

                            DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD
                            FOR
                                SELECT  *
                                       ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                FROM    (
                                          SELECT DISTINCT
                                                    t1.StuEnrollId
                                                   ,t1.ClsSectionId
                                                   ,t1.MeetDate
                                                   ,DATENAME(dw,t1.MeetDate) AS WeekDay
                                                   ,t4.StartDate
                                                   ,t4.EndDate
                                                   ,t5.PeriodDescrip
                                                   ,t1.Actual
                                                   ,t1.Excused
                                                   ,CASE WHEN (
                                                                t1.Actual = 0
                                                                AND t1.Excused = 0
                                                              ) THEN t1.Scheduled
                                                         ELSE CASE WHEN (
                                                                          t1.Actual <> 9999.00
                                                                          AND t1.Actual < t1.Scheduled
														  --AND t1.Excused <> 1
                                                                        ) THEN ( t1.Scheduled - t1.Actual )
                                                                   ELSE 0
                                                              END
                                                    END AS Absent
                                                   ,t1.Scheduled AS ScheduledMinutes
                                                   ,CASE WHEN (
                                                                t1.Actual > 0
                                                                AND t1.Actual < t1.Scheduled
                                                              ) THEN ( t1.Scheduled - t1.Actual )
                                                         ELSE 0
                                                    END AS TardyMinutes
                                                   ,t1.Tardy AS Tardy
                                                   ,t3.TrackTardies
                                                   ,t3.TardiesMakingAbsence
                                                   ,t3.PrgVerId
                                          FROM      atClsSectAttendance t1
                                          INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                          INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                          INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                          INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                             AND (
                                                                                   CONVERT(DATE,t1.MeetDate,111) >= CONVERT(DATE,t4.StartDate,111)
                                                                                   AND CONVERT(DATE,t1.MeetDate,111) <= CONVERT(DATE,t4.EndDate,111)
                                                                                 )
                                                                             AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                          INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                          INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                          INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                          INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                          INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                          INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                          WHERE     t2.StuEnrollId = @StuEnrollId
                                                    AND (
                                                          AAUT1.UnitTypeDescrip IN ( ''None'',''Present Absent'' )
                                                          OR AAUT2.UnitTypeDescrip IN ( ''None'',''Present Absent'' )
                                                        )
                                                    AND t1.Actual <> 9999
                                        ) dt
                                ORDER BY StuEnrollId
                                       ,MeetDate;
                            OPEN GetAttendance_Cursor;
                            FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,
                                @Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @boolReset = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                            SET @boolReset = 1;
                                        END;

					  
					  -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                                    IF @Actual <> 9999.00
                                        BEGIN
					-- Commented by Balaji on 2.6.2015 as Excused Absence is still considered an absence
					-- @Excused is not added to Present Hours
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;  -- + @Excused
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; -- + @Excused
					
					-- If there are make up hrs deduct that otherwise it will be added again in progress report
                                            IF (
                                                 @Actual > 0
                                                 AND @Actual > @ScheduledMinutes
                                                 AND @Actual <> 9999.00
                                               )
                                                BEGIN
                                                    SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                END;
					  
                                            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;                      
                                        END;
				   
					-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                                    SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
			  
					-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                                    IF (
                                         @Actual > 0
                                         AND @Actual < @ScheduledMinutes
                                         AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                        END;
                                    ELSE
                                        IF (
                                             @Actual = 1
                                             AND @Actual = @ScheduledMinutes
                                             AND @tardy = 1
                                           )
                                            BEGIN
                                                SET @ActualRunningTardyHours += 1;
                                            END;
							
					-- Track how many days student has been tardy only when 
					-- program version requires to track tardy
                                    IF (
                                         @tracktardies = 1
                                         AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;	    
		   
			
					-- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
					-- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
					-- when student is tardy the second time, that second occurance will be considered as
					-- absence
					-- Variable @intTardyBreakpoint tracks how many times the student was tardy
					-- Variable @tardiesMakingAbsence tracks the tardy rule
                                    IF (
                                         @tracktardies = 1
                                         AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused;
                                            SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;
				   
				   -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                         @Actual > 0
                                         AND @Actual > @ScheduledMinutes
                                         AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;
			  
                                    IF ( @tracktardies = 1 )
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                                        END;
				
				---- PRINT @MeetDate
				---- PRINT @ActualRunningAbsentHours
				
                                    DELETE  FROM syStudentAttendanceSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND ClsSectionId = @ClsSectionId
                                            AND StudentAttendedDate = @MeetDate;
                                    INSERT  INTO syStudentAttendanceSummary
                                            (
                                             StuEnrollId
                                            ,ClsSectionId
                                            ,StudentAttendedDate
                                            ,ScheduledDays
                                            ,ActualDays
                                            ,ActualRunningScheduledDays
                                            ,ActualRunningPresentDays
                                            ,ActualRunningAbsentDays
                                            ,ActualRunningMakeupDays
                                            ,ActualRunningTardyDays
                                            ,AdjustedPresentDays
                                            ,AdjustedAbsentDays
                                            ,AttendanceTrackType
                                            ,ModUser
                                            ,ModDate
                                            ,tardiesmakingabsence
										
                                            )
                                    VALUES  (
                                             @StuEnrollId
                                            ,@ClsSectionId
                                            ,@MeetDate
                                            ,@ScheduledMinutes
                                            ,@Actual
                                            ,@ActualRunningScheduledDays
                                            ,@ActualRunningPresentHours
                                            ,@ActualRunningAbsentHours
                                            ,ISNULL(@MakeupHours,0)
                                            ,@ActualRunningTardyHours
                                            ,@AdjustedPresentDaysComputed
                                            ,@AdjustedRunningAbsentHours
                                            ,''Post Attendance by Class Min''
                                            ,''sa''
                                            ,GETDATE()
                                            ,@TardiesMakingAbsence
										
                                            );

				--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
                                    SET @PrevStuEnrollId = @StuEnrollId; 

                                    FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,
                                        @Actual,@Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
			
                            DECLARE @MyTardyTable TABLE
                                (
                                 ClsSectionId UNIQUEIDENTIFIER
                                ,TardiesMakingAbsence INT
                                ,AbsentHours DECIMAL(18,2)
                                ); 

                            INSERT  INTO @MyTardyTable
                                    SELECT  ClsSectionId
                                           ,PV.TardiesMakingAbsence
                                           ,CASE WHEN ( COUNT(*) >= PV.TardiesMakingAbsence ) THEN ( COUNT(*) / PV.TardiesMakingAbsence )
                                                 ELSE 0
                                            END AS AbsentHours 
--Count(*) as NumberofTimesTardy 
                                    FROM    syStudentAttendanceSummary SAS
                                    INNER JOIN arStuEnrollments SE ON SAS.StuEnrollId = SE.StuEnrollId
                                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                                    WHERE   SE.StuEnrollId = @StuEnrollId
                                            AND SAS.IsTardy = 1
                                            AND PV.TrackTardies = 1
                                    GROUP BY ClsSectionId
                                           ,PV.TardiesMakingAbsence; 

--Drop table @MyTardyTable
                            DECLARE @TotalTardyAbsentDays DECIMAL(18,2);
                            SET @TotalTardyAbsentDays = (
                                                          SELECT    ISNULL(SUM(AbsentHours),0)
                                                          FROM      @MyTardyTable
                                                        );

--Print @TotalTardyAbsentDays

                            UPDATE  syStudentAttendanceSummary
                            SET     AdjustedPresentDays = AdjustedPresentDays - @TotalTardyAbsentDays
                                   ,AdjustedAbsentDays = AdjustedAbsentDays + @TotalTardyAbsentDays
                            WHERE   StuEnrollId = @StuEnrollId
                                    AND StudentAttendedDate = (
                                                                SELECT TOP 1
                                                                        StudentAttendedDate
                                                                FROM    syStudentAttendanceSummary
                                                                WHERE   StuEnrollId = @StuEnrollId
                                                                ORDER BY StudentAttendedDate DESC
                                                              );
			
                        END;	
                END; -- Step 3 
-- END -- Step 3 

-- Step 4  --  InsertAttendance_Class_Minutes
--         --  TrackSapAttendance = ''byclass''
                BEGIN  -- Step 4  --  InsertAttendance_Class_Minutes
		-- By Class and Attendance Unit Type - Minutes and Clock Hour
		
                    IF @UnitTypeDescrip IN ( ''minutes'',''clock hours'' )
                        AND @TrackSapAttendance = ''byclass''
                        BEGIN
                            DELETE  FROM syStudentAttendanceSummary
                            WHERE   StuEnrollId = @StuEnrollId;
				
                            DECLARE GetAttendance_Cursor CURSOR
                            FOR
                                SELECT  *
                                       ,ROW_NUMBER() OVER ( ORDER BY MeetDate ) AS RowNumber
                                FROM    (
                                          SELECT DISTINCT
                                                    t1.StuEnrollId
                                                   ,t1.ClsSectionId
                                                   ,t1.MeetDate
                                                   ,DATENAME(dw,t1.MeetDate) AS WeekDay
                                                   ,t4.StartDate
                                                   ,t4.EndDate
                                                   ,t5.PeriodDescrip
                                                   ,t1.Actual
                                                   ,t1.Excused
                                                   ,CASE WHEN (
                                                                t1.Actual = 0
                                                                AND t1.Excused = 0
                                                              ) THEN t1.Scheduled
                                                         ELSE CASE WHEN (
                                                                          t1.Actual <> 9999.00
                                                                          AND t1.Actual < t1.Scheduled
                                                                        ) THEN ( t1.Scheduled - t1.Actual )
                                                                   ELSE 0
                                                              END
                                                    END AS Absent
                                                   ,t1.Scheduled AS ScheduledMinutes
                                                   ,CASE WHEN (
                                                                t1.Actual > 0
                                                                AND t1.Actual < t1.Scheduled
                                                              ) THEN ( t1.Scheduled - t1.Actual )
                                                         ELSE 0
                                                    END AS TardyMinutes
                                                   ,t1.Tardy AS Tardy
                                                   ,t3.TrackTardies
                                                   ,t3.TardiesMakingAbsence
                                                   ,t3.PrgVerId
                                          FROM      atClsSectAttendance t1
                                          INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                          INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                          INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
                                          INNER JOIN arClsSectMeetings t4 ON t1.ClsSectionId = t4.ClsSectionId
                                                                             AND (
                                                                                   CONVERT(DATE,t1.MeetDate,111) >= CONVERT(DATE,t4.StartDate,111)
                                                                                   AND CONVERT(DATE,t1.MeetDate,111) <= CONVERT(DATE,t4.EndDate,111)
                                                                                 )
                                                                             AND t1.ClsSectMeetingId = t4.ClsSectMeetingId --DE8479 line added
                                          INNER JOIN syPeriods t5 ON t4.PeriodId = t5.PeriodId
                                          INNER JOIN cmTimeInterval t6 ON t5.StartTimeId = t6.TimeIntervalId
                                          INNER JOIN cmTimeInterval t7 ON t5.EndTimeId = t7.TimeIntervalId
                                          INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
                                          INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
                                          INNER JOIN arAttUnitType AS AAUT2 ON AAUT2.UnitTypeId = t10.UnitTypeId
                                          WHERE     t2.StuEnrollId = @StuEnrollId
                                                    AND (
                                                          AAUT1.UnitTypeDescrip IN ( ''Minutes'',''Clock Hours'' )
                                                          OR AAUT2.UnitTypeDescrip IN ( ''Minutes'',''Clock Hours'' )
                                                        )
                                                    AND t1.Actual <> 9999
                                          UNION
                                          SELECT DISTINCT
                                                    t1.StuEnrollId
                                                   ,NULL AS ClsSectionId
                                                   ,t1.RecordDate AS MeetDate
                                                   ,DATENAME(dw,t1.RecordDate) AS WeekDay
                                                   ,NULL AS StartDate
                                                   ,NULL AS EndDate
                                                   ,NULL AS PeriodDescrip
                                                   ,t1.ActualHours
                                                   ,NULL AS Excused
                                                   ,CASE WHEN ( t1.ActualHours = 0 ) THEN t1.SchedHours
                                                         ELSE 0
								 --ELSE 
									--Case when (t1.ActualHours <> 9999.00 and t1.ActualHours < t1.SchedHours)
									--		THEN (t1.SchedHours - t1.ActualHours)
									--		ELSE 
									--			0
									--		End
                                                    END AS Absent
                                                   ,t1.SchedHours AS ScheduledMinutes
                                                   ,CASE WHEN (
                                                                t1.ActualHours <> 9999.00
                                                                AND t1.ActualHours > 0
                                                                AND t1.ActualHours < t1.SchedHours
                                                              ) THEN ( t1.SchedHours - t1.ActualHours )
                                                         ELSE 0
                                                    END AS TardyMinutes
                                                   ,NULL AS Tardy
                                                   ,t3.TrackTardies
                                                   ,t3.TardiesMakingAbsence
                                                   ,t3.PrgVerId
                                          FROM      arStudentClockAttendance t1
                                          INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                          INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                          WHERE     t2.StuEnrollId = @StuEnrollId
                                                    AND t1.Converted = 1
                                                    AND t1.ActualHours <> 9999
                                        ) dt
                                ORDER BY StuEnrollId
                                       ,MeetDate;
                            OPEN GetAttendance_Cursor;
                            FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,@Actual,
                                @Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @boolReset = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                            SET @boolReset = 1;
                                        END;

					  
			-- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
                                    IF @Actual <> 9999.00
                                        BEGIN
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual; 
					-- If there are make up hrs deduct that otherwise it will be added again in progress report
                                            IF (
                                                 @Actual > 0
                                                 AND @Actual > @ScheduledMinutes
                                                 AND @Actual <> 9999.00
                                               )
                                                BEGIN
                                                    SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                                END; 
                                            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes;                      
                                        END;
		   
			-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
                                    SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;
                                    SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;	
	  
			-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
                                    IF (
                                         @Actual > 0
                                         AND @Actual < @ScheduledMinutes
                                         AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                        END;
					
			-- Track how many days student has been tardy only when 
			-- program version requires to track tardy
                                    IF (
                                         @tracktardies = 1
                                         AND @tardy = 1
                                       )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;	    
		   
			
			-- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
			-- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
			-- when student is tardy the second time, that second occurance will be considered as
			-- absence
			-- Variable @intTardyBreakpoint tracks how many times the student was tardy
			-- Variable @tardiesMakingAbsence tracks the tardy rule
                                    IF (
                                         @tracktardies = 1
                                         AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                            SET @AdjustedRunningAbsentHours = ( @AdjustedRunningAbsentHours - @Absent ) + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;
		   
		   -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                         @Actual > 0
                                         AND @Actual > @ScheduledMinutes
                                         AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;
	  
	 
                                    IF ( @tracktardies = 1 )
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours; --+IsNULL(@ActualRunningTardyHours,0)
                                        END;
                                    ELSE
                                        BEGIN
                                            SET @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours;
                                        END;
		
		
		
		
                                    DELETE  FROM syStudentAttendanceSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND ClsSectionId = @ClsSectionId
                                            AND StudentAttendedDate = @MeetDate;
                                    INSERT  INTO syStudentAttendanceSummary
                                            (
                                             StuEnrollId
                                            ,ClsSectionId
                                            ,StudentAttendedDate
                                            ,ScheduledDays
                                            ,ActualDays
                                            ,ActualRunningScheduledDays
                                            ,ActualRunningPresentDays
                                            ,ActualRunningAbsentDays
                                            ,ActualRunningMakeupDays
                                            ,ActualRunningTardyDays
                                            ,AdjustedPresentDays
                                            ,AdjustedAbsentDays
                                            ,AttendanceTrackType
                                            ,ModUser
                                            ,ModDate
										
                                            )
                                    VALUES  (
                                             @StuEnrollId
                                            ,@ClsSectionId
                                            ,@MeetDate
                                            ,@ScheduledMinutes
                                            ,@Actual
                                            ,@ActualRunningScheduledDays
                                            ,@ActualRunningPresentHours
                                            ,@ActualRunningAbsentHours
                                            ,ISNULL(@MakeupHours,0)
                                            ,@ActualRunningTardyHours
                                            ,@AdjustedPresentDaysComputed
                                            ,@AdjustedRunningAbsentHours
                                            ,''Post Attendance by Class Min''
                                            ,''sa''
                                            ,GETDATE()
                                            );

                                    UPDATE  syStudentAttendanceSummary
                                    SET     tardiesmakingabsence = @TardiesMakingAbsence
                                    WHERE   StuEnrollId = @StuEnrollId;
                                    SET @PrevStuEnrollId = @StuEnrollId; 

                                    FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,@PeriodDescrip,
                                        @Actual,@Excused,@Absent,@ScheduledMinutes,@TardyMinutes,@tardy,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
                        END;
                END; --  Step 3  --   InsertAttendance_Class_Minutes
--END --  Step 3  --   InsertAttendance_Class_Minutes


--Select * from arAttUnitType

			
-- By Minutes/Day
-- remove clock hour from here
                IF @UnitTypeDescrip IN ( ''minutes'' )
                    AND @TrackSapAttendance = ''byday''
	-- -- PRINT GETDATE();	
	---- PRINT @UnitTypeId;
	---- PRINT @TrackSapAttendance;
                    BEGIN
			--Delete from syStudentAttendanceSummary where StuEnrollId=@StuEnrollId

                        DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD
                        FOR
                            SELECT  t1.StuEnrollId
                                   ,NULL AS ClsSectionId
                                   ,t1.RecordDate AS MeetDate
                                   ,t1.ActualHours
                                   ,t1.SchedHours AS ScheduledMinutes
                                   ,CASE WHEN (
                                                (
                                                  t1.SchedHours >= 1
                                                  AND t1.SchedHours NOT IN ( 999,9999 )
                                                )
                                                AND t1.ActualHours = 0
                                              ) THEN t1.SchedHours
                                         ELSE 0
                                    END AS Absent
                                   ,t1.isTardy
                                   ,(
                                      SELECT    ISNULL(SUM(SchedHours),0)
                                      FROM      arStudentClockAttendance
                                      WHERE     StuEnrollId = t1.StuEnrollId
                                                AND RecordDate <= t1.RecordDate
                                                AND (
                                                      t1.SchedHours >= 1
                                                      AND t1.SchedHours NOT IN ( 999,9999 )
                                                      AND t1.ActualHours NOT IN ( 999,9999 )
                                                    )
                                    ) AS ActualRunningScheduledHours
                                   ,(
                                      SELECT    SUM(ActualHours)
                                      FROM      arStudentClockAttendance
                                      WHERE     StuEnrollId = t1.StuEnrollId
                                                AND RecordDate <= t1.RecordDate
                                                AND (
                                                      t1.SchedHours >= 1
                                                      AND t1.SchedHours NOT IN ( 999,9999 )
                                                    )
                                                AND ActualHours >= 1
                                                AND ActualHours NOT IN ( 999,9999 )
                                    ) AS ActualRunningPresentHours
                                   ,(
                                      SELECT    COUNT(ActualHours)
                                      FROM      arStudentClockAttendance
                                      WHERE     StuEnrollId = t1.StuEnrollId
                                                AND RecordDate <= t1.RecordDate
                                                AND (
                                                      t1.SchedHours >= 1
                                                      AND t1.SchedHours NOT IN ( 999,9999 )
                                                    )
                                                AND ActualHours = 0
                                                AND ActualHours NOT IN ( 999,9999 )
                                    ) AS ActualRunningAbsentHours
                                   ,(
                                      SELECT    SUM(ActualHours)
                                      FROM      arStudentClockAttendance
                                      WHERE     StuEnrollId = t1.StuEnrollId
                                                AND RecordDate <= t1.RecordDate
                                                AND SchedHours = 0
                                                AND ActualHours >= 1
                                                AND ActualHours NOT IN ( 999,9999 )
                                    ) AS ActualRunningMakeupHours
                                   ,(
                                      SELECT    SUM(SchedHours - ActualHours)
                                      FROM      arStudentClockAttendance
                                      WHERE     StuEnrollId = t1.StuEnrollId
                                                AND RecordDate <= t1.RecordDate
                                                AND (
                                                      (
                                                        t1.SchedHours >= 1
                                                        AND t1.SchedHours NOT IN ( 999,9999 )
                                                      )
                                                      AND ActualHours >= 1
                                                      AND ActualHours NOT IN ( 999,9999 )
                                                    )
                                                AND isTardy = 1
                                    ) AS ActualRunningTardyHours
                                   ,t3.TrackTardies
                                   ,t3.TardiesMakingAbsence
                                   ,t3.PrgVerId
                                   ,ROW_NUMBER() OVER ( ORDER BY t1.RecordDate ) AS RowNumber
                            FROM    arStudentClockAttendance t1
                            INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                            INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                            INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
			--inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                            WHERE   AAUT1.UnitTypeDescrip IN ( ''Minutes'' )
                                    AND t2.StuEnrollId = @StuEnrollId
                                    AND t1.ActualHours <> 9999.00
                            ORDER BY t1.StuEnrollId
                                   ,MeetDate;
                        OPEN GetAttendance_Cursor;
--Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                        FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,
                            @ActualRunningScheduledHours,@ActualRunningPresentHours,@ActualRunningAbsentHours,@ActualRunningMakeupHours,@ActualRunningTardyHours,
                            @tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;

                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningPresentHours = 0;
                        SET @ActualRunningAbsentHours = 0;
                        SET @ActualRunningTardyHours = 0;
                        SET @ActualRunningMakeupHours = 0;
                        SET @intTardyBreakPoint = 0;
                        SET @AdjustedRunningPresentHours = 0;
                        SET @AdjustedRunningAbsentHours = 0;
                        SET @ActualRunningScheduledDays = 0;
                        WHILE @@FETCH_STATUS = 0
                            BEGIN

                                IF @PrevStuEnrollId <> @StuEnrollId
                                    BEGIN
                                        SET @ActualRunningPresentHours = 0;
                                        SET @ActualRunningAbsentHours = 0;
                                        SET @intTardyBreakPoint = 0;
                                        SET @ActualRunningTardyHours = 0;
                                        SET @AdjustedRunningPresentHours = 0;
                                        SET @AdjustedRunningAbsentHours = 0;
                                        SET @ActualRunningScheduledDays = 0;
                                    END;

                                IF (
                                     @ScheduledMinutes >= 1
                                     AND (
                                           @Actual <> 9999
                                           AND @Actual <> 999
                                         )
                                     AND (
                                           @ScheduledMinutes <> 9999
                                           AND @Actual <> 999
                                         )
                                   )
                                    BEGIN
                                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays,0) + ISNULL(@ScheduledMinutes,0);
                                    END;
                                ELSE
                                    BEGIN
                                        SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays,0); 
                                    END;
	   
                                IF (
                                     @Actual <> 9999
                                     AND @Actual <> 999
                                   )
                                    BEGIN
                                        SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual;
                                    END;
                                SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent;

                                IF (
                                     @Actual > 0
                                     AND @Actual < @ScheduledMinutes
                                   )
                                    BEGIN
                                        SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + ( @ScheduledMinutes - @Actual );
                                    END;
			-- Make up hours
		--sched=5, Actual =7, makeup= 2,ab = 0
		--sched=0, Actual =7, makeup= 7,ab = 0
                                IF (
                                     @Actual > 0
                                     AND @ScheduledMinutes > 0
                                     AND @Actual > @ScheduledMinutes
                                     AND (
                                           @Actual <> 9999
                                           AND @Actual <> 999
                                         )
                                   )
                                    BEGIN
                                        SET @ActualRunningMakeupHours = @ActualRunningMakeupHours + ( @Actual - @ScheduledMinutes );
                                    END;

		
                                IF (
                                     @Actual <> 9999
                                     AND @Actual <> 999
                                   )
                                    BEGIN
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual;
                                    END;	
                                IF (
                                     @Actual = 0
                                     AND @ScheduledMinutes >= 1
                                     AND @ScheduledMinutes NOT IN ( 999,9999 )
                                   )
                                    BEGIN
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                                    END;
		-- Absent hours
		--1. sched = 5, Actual = 2 then Ab = (5-3) = 2
                                IF (
                                     @Absent = 0
                                     AND @ActualRunningAbsentHours > 0
                                     AND ( @Actual < @ScheduledMinutes )
                                   )
                                    BEGIN
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + ( @ScheduledMinutes - @Actual );	
                                    END;
                                IF (
                                     @Actual > 0
                                     AND @Actual < @ScheduledMinutes
                                     AND (
                                           @Actual <> 9999.00
                                           AND @Actual <> 999.00
                                         )
                                   )
                                    BEGIN
                                        SET @ActualRunningTardyHours = @ActualRunningTardyHours + ( @ScheduledMinutes - @Actual );
                                    END;
	
                                IF @tracktardies = 1
                                    AND (
                                          @TardyMinutes > 0
                                          OR @IsTardy = 1
                                        )
                                    BEGIN
                                        SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                    END;	    
                                IF (
                                     @tracktardies = 1
                                     AND @intTardyBreakPoint = @TardiesMakingAbsence
                                   )
                                    BEGIN
                                        SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                        SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Actual; --@TardyMinutes
                                        SET @intTardyBreakPoint = 0;
                                    END;
                                DELETE  FROM syStudentAttendanceSummary
                                WHERE   StuEnrollId = @StuEnrollId
                                        AND StudentAttendedDate = @MeetDate;
                                INSERT  INTO syStudentAttendanceSummary
                                        (
                                         StuEnrollId
                                        ,ClsSectionId
                                        ,StudentAttendedDate
                                        ,ScheduledDays
                                        ,ActualDays
                                        ,ActualRunningScheduledDays
                                        ,ActualRunningPresentDays
                                        ,ActualRunningAbsentDays
                                        ,ActualRunningMakeupDays
                                        ,ActualRunningTardyDays
                                        ,AdjustedPresentDays
                                        ,AdjustedAbsentDays
                                        ,AttendanceTrackType
                                        ,ModUser
                                        ,ModDate
										)
                                VALUES  (
                                         @StuEnrollId
                                        ,@ClsSectionId
                                        ,@MeetDate
                                        ,@ScheduledMinutes
                                        ,@Actual
                                        ,@ActualRunningScheduledDays
                                        ,@ActualRunningPresentHours
                                        ,@ActualRunningAbsentHours
                                        ,@ActualRunningMakeupHours
                                        ,@ActualRunningTardyHours
                                        ,@AdjustedRunningPresentHours
                                        ,@AdjustedRunningAbsentHours
                                        ,''Post Attendance by Class''
                                        ,''sa''
                                        ,GETDATE()
                                        );
				
                                UPDATE  syStudentAttendanceSummary
                                SET     tardiesmakingabsence = @TardiesMakingAbsence
                                WHERE   StuEnrollId = @StuEnrollId;
			--end
                                SET @PrevStuEnrollId = @StuEnrollId; 
                                FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@ClsSectionId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,
                                    @ActualRunningScheduledHours,@ActualRunningPresentHours,@ActualRunningAbsentHours,@ActualRunningMakeupHours,
                                    @ActualRunningTardyHours,@tracktardies,@TardiesMakingAbsence,@PrgVerId,@rownumber;
                            END;
                        CLOSE GetAttendance_Cursor;
                        DEALLOCATE GetAttendance_Cursor;
                    END;

	---- PRINT ''Does it get to Clock Hour/PA'';
	---- PRINT @UnitTypeId;
	---- PRINT @TrackSapAttendance;
	-- By Day and PA, Clock Hour
	-- -- Step 2  --  InsertAttendance_Day_PresentAbsent  -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'')  and TrackSapAttendance = ''byday''
                BEGIN -- Step 2  --  InsertAttendance_Day_PresentAbsent  -- UnitTypeDescrip = (''Present Absent'', ''Clock Hours'') and TrackSapAttendance = ''byday''
                    IF @UnitTypeDescrip IN ( ''present absent'',''clock hours'' )
                        AND @TrackSapAttendance = ''byday''
                        BEGIN
			---- PRINT ''By Day inside day'';
                            DECLARE GetAttendance_Cursor CURSOR
                            FOR
                                SELECT  t1.StuEnrollId
                                       ,t1.RecordDate
                                       ,t1.ActualHours
                                       ,t1.SchedHours
                                       ,CASE WHEN (
                                                    (
                                                      t1.SchedHours >= 1
                                                      AND t1.SchedHours NOT IN ( 999,9999 )
                                                    )
                                                    AND t1.ActualHours = 0
                                                  ) THEN t1.SchedHours
                                             ELSE 0
                                        END AS Absent
                                       ,t1.isTardy
                                       ,t3.TrackTardies
                                       ,t3.TardiesMakingAbsence
                                FROM    arStudentClockAttendance t1
                                INNER JOIN arStuEnrollments t2 ON t1.StuEnrollId = t2.StuEnrollId
                                INNER JOIN arPrgVersions t3 ON t2.PrgVerId = t3.PrgVerId
                                INNER JOIN arAttUnitType AS AAUT1 ON AAUT1.UnitTypeId = t3.UnitTypeId
			--inner join Inserted t4 on t1.StuEnrollId = t4.StuEnrollId and t1.RecordDate=t4.RecordDate
                                WHERE   -- Unit Types: Present Absent and Clock Hour
                                        AAUT1.UnitTypeDescrip IN ( ''present absent'',''clock hours'' )
                                        AND ActualHours <> 9999.00
                                        AND t2.StuEnrollId = @StuEnrollId
                                ORDER BY t1.StuEnrollId
                                       ,t1.RecordDate;
                            OPEN GetAttendance_Cursor;
--Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
                            FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,@tracktardies,
                                @TardiesMakingAbsence;

                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningPresentHours = 0;
                            SET @ActualRunningAbsentHours = 0;
                            SET @ActualRunningTardyHours = 0;
                            SET @ActualRunningMakeupHours = 0;
                            SET @intTardyBreakPoint = 0;
                            SET @AdjustedRunningPresentHours = 0;
                            SET @AdjustedRunningAbsentHours = 0;
                            SET @ActualRunningScheduledDays = 0;
                            SET @MakeupHours = 0;
                            WHILE @@FETCH_STATUS = 0
                                BEGIN

                                    IF @PrevStuEnrollId <> @StuEnrollId
                                        BEGIN
                                            SET @ActualRunningPresentHours = 0;
                                            SET @ActualRunningAbsentHours = 0;
                                            SET @intTardyBreakPoint = 0;
                                            SET @ActualRunningTardyHours = 0;
                                            SET @AdjustedRunningPresentHours = 0;
                                            SET @AdjustedRunningAbsentHours = 0;
                                            SET @ActualRunningScheduledDays = 0;
                                            SET @MakeupHours = 0;
                                        END;
	   
                                    IF (
                                         @Actual <> 9999
                                         AND @Actual <> 999
                                       )
                                        BEGIN
                                            SET @ActualRunningScheduledDays = ISNULL(@ActualRunningScheduledDays,0) + @ScheduledMinutes;
                                            SET @ActualRunningPresentHours = ISNULL(@ActualRunningPresentHours,0) + @Actual;
                                            SET @AdjustedRunningPresentHours = ISNULL(@AdjustedRunningPresentHours,0) + @Actual;
                                        END;
                                    SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours,0) + @Absent;
                                    IF (
                                         @Actual = 0
                                         AND @ScheduledMinutes >= 1
                                         AND @ScheduledMinutes NOT IN ( 999,9999 )
                                       )
                                        BEGIN
                                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent;
                                        END;
		
		-- NWH 
		-- If Scheduled = 3.0 hrs and Actual = 1.0 Then 2 hrs is considered absent
                                    IF (
                                         @ScheduledMinutes >= 1
                                         AND @ScheduledMinutes NOT IN ( 999,9999 )
                                         AND @Actual > 0
                                         AND ( @Actual < @ScheduledMinutes )
                                       )
                                        BEGIN
                                            SET @ActualRunningAbsentHours = ISNULL(@ActualRunningAbsentHours,0) + ( @ScheduledMinutes - @Actual );
                                            SET @AdjustedRunningAbsentHours = ISNULL(@AdjustedRunningAbsentHours,0) + ( @ScheduledMinutes - @Actual );
                                        END; 
		
                                    IF (
                                         @tracktardies = 1
                                         AND @IsTardy = 1
                                       )
                                        BEGIN
                                            SET @ActualRunningTardyHours = @ActualRunningTardyHours + 1;
                                        END;
		 --commented by balaji on 10/22/2012 as report (rdl) doesn''t add days attended and make up days
		 ---- If there are make up hrs deduct that otherwise it will be added again in progress report
                                    IF (
                                         @Actual > 0
                                         AND @Actual > @ScheduledMinutes
                                         AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @ActualRunningPresentHours = @ActualRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - ( @Actual - @ScheduledMinutes );
                                        END;
			
                                    IF @tracktardies = 1
                                        AND (
                                              @TardyMinutes > 0
                                              OR @IsTardy = 1
                                            )
                                        BEGIN
                                            SET @intTardyBreakPoint = @intTardyBreakPoint + 1;
                                        END;	    
		
		
		
		-- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
                                    IF (
                                         @Actual > 0
                                         AND @Actual > @ScheduledMinutes
                                         AND @Actual <> 9999.00
                                       )
                                        BEGIN
                                            SET @MakeupHours = @MakeupHours + ( @Actual - @ScheduledMinutes );
                                        END;
		
                                    IF (
                                         @tracktardies = 1
                                         AND @intTardyBreakPoint = @TardiesMakingAbsence
                                       )
                                        BEGIN
                                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual;
                                            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes; --@TardyMinutes
                                            SET @intTardyBreakPoint = 0;
                                        END;
				
                                    DELETE  FROM syStudentAttendanceSummary
                                    WHERE   StuEnrollId = @StuEnrollId
                                            AND StudentAttendedDate = @MeetDate;
                                    INSERT  INTO syStudentAttendanceSummary
                                            (
                                             StuEnrollId
                                            ,ClsSectionId
                                            ,StudentAttendedDate
                                            ,ScheduledDays
                                            ,ActualDays
                                            ,ActualRunningScheduledDays
                                            ,ActualRunningPresentDays
                                            ,ActualRunningAbsentDays
                                            ,ActualRunningMakeupDays
                                            ,ActualRunningTardyDays
                                            ,AdjustedPresentDays
                                            ,AdjustedAbsentDays
                                            ,AttendanceTrackType
                                            ,ModUser
                                            ,ModDate
                                            ,tardiesmakingabsence
										
                                            )
                                    VALUES  (
                                             @StuEnrollId
                                            ,@ClsSectionId
                                            ,@MeetDate
                                            ,ISNULL(@ScheduledMinutes,0)
                                            ,@Actual
                                            ,ISNULL(@ActualRunningScheduledDays,0)
                                            ,ISNULL(@ActualRunningPresentHours,0)
                                            ,ISNULL(@ActualRunningAbsentHours,0)
                                            ,ISNULL(@MakeupHours,0)
                                            ,ISNULL(@ActualRunningTardyHours,0)
                                            ,ISNULL(@AdjustedRunningPresentHours,0)
                                            ,ISNULL(@AdjustedRunningAbsentHours,0)
                                            ,''Post Attendance by Class''
                                            ,''sa''
                                            ,GETDATE()
                                            ,@TardiesMakingAbsence
										
                                            );

		--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
		--end
                                    SET @PrevStuEnrollId = @StuEnrollId; 
                                    FETCH NEXT FROM GetAttendance_Cursor INTO @StuEnrollId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,@IsTardy,@tracktardies,
                                        @TardiesMakingAbsence;
                                END;
                            CLOSE GetAttendance_Cursor;
                            DEALLOCATE GetAttendance_Cursor;
                        END;
                END;
							
--end

-- Based on grade rounding round the final score and current score
--Declare @PrevTermId uniqueidentifier,@PrevReqId uniqueidentifier,@RowCount int,@FinalScore decimal(18,4),@CurrentScore decimal(18,4)
--declare @ReqId uniqueidentifier,@CourseCodeDescrip varchar(100),@FinalGrade varchar(10),@sysComponentTypeId int
--declare @CreditsAttempted decimal(18,4),@Grade varchar(10),@IsPass bit,@IsCreditsAttempted bit,@IsCreditsEarned bit
--declare @IsInGPA bit,@FinAidCredits decimal(18,4),@CurrentGrade varchar(10),@CourseCredits decimal(18,4)
                DECLARE @GPA DECIMAL(18,4);

                DECLARE @PrevReqId UNIQUEIDENTIFIER
                   ,@PrevTermId UNIQUEIDENTIFIER
                   ,@CreditsAttempted DECIMAL(18,2)
                   ,@CreditsEarned DECIMAL(18,2);
                DECLARE @reqid UNIQUEIDENTIFIER
                   ,@CourseCodeDescrip VARCHAR(50)
                   ,@FinalGrade VARCHAR(50)
                   ,@FinalScore DECIMAL(18,2)
                   ,@Grade VARCHAR(50);
                DECLARE @IsPass BIT
                   ,@IsCreditsAttempted BIT
                   ,@IsCreditsEarned BIT
                   ,@CurrentScore DECIMAL(18,2)
                   ,@CurrentGrade VARCHAR(10)
                   ,@FinalGPA DECIMAL(18,2);
                DECLARE @sysComponentTypeId INT
                   ,@RowCount INT;
                DECLARE @IsInGPA BIT;
                DECLARE @CourseCredits DECIMAL(18,2);
                DECLARE @FinAidCreditsEarned DECIMAL(18,2)
                   ,@FinAidCredits DECIMAL(18,2);

                DECLARE GetCreditsSummary_Cursor CURSOR
                FOR
                    SELECT	DISTINCT
                            SE.StuEnrollId
                           ,T.TermId
                           ,T.TermDescrip
                           ,T.StartDate
                           ,R.ReqId
                           ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                           ,RES.Score AS FinalScore
                           ,RES.GrdSysDetailId AS FinalGrade
                           ,GCT.SysComponentTypeId
                           ,R.Credits AS CreditsAttempted
                           ,CS.ClsSectionId
                           ,GSD.Grade
                           ,GSD.IsPass
                           ,GSD.IsCreditsAttempted
                           ,GSD.IsCreditsEarned
                           ,SE.PrgVerId
                           ,GSD.IsInGPA
                           ,R.FinAidCredits AS FinAidCredits
                    FROM    arStuEnrollments SE
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arResults RES ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arClassSections CS ON RES.TestId = CS.ClsSectionId
                    INNER JOIN arTerm T ON CS.TermId = T.TermId
                    INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                    LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                    LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                    LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                    LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    WHERE   SE.StuEnrollId = @StuEnrollId
                    ORDER BY T.StartDate
                           ,T.TermDescrip
                           ,R.ReqId; 

                DECLARE @varGradeRounding VARCHAR(3);
                DECLARE @roundfinalscore DECIMAL(18,4);
                SET @varGradeRounding = (
                                          SELECT    Value
                                          FROM      syConfigAppSetValues
                                          WHERE     SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                    @FinalGrade,@sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,
                    @FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN
			
			
                        SET @CurrentScore = (
                                              SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                             ELSE NULL
                                                        END AS CurrentScore
                                              FROM      (
                                                          SELECT    InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight AS GradeBookWeight
                                                                   ,CASE WHEN S.NumberOfComponents > 0
                                                                         THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                         ELSE 0
                                                                    END AS ActualWeight
                                                          FROM      (
                                                                      SELECT    C.InstrGrdBkWgtDetailId
                                                                               ,D.Code
                                                                               ,D.Descrip
                                                                               ,ISNULL(C.Weight,0) AS Weight
                                                                               ,C.Number AS MinNumber
                                                                               ,C.GrdPolicyId
                                                                               ,C.Parameter AS Param
                                                                               ,X.GrdScaleId
                                                                               ,SUM(GR.Score) AS Score
                                                                               ,COUNT(D.Descrip) AS NumberOfComponents
                                                                      FROM      (
                                                                                  SELECT DISTINCT TOP 1
                                                                                            A.InstrGrdBkWgtId
                                                                                           ,A.EffectiveDate
                                                                                           ,B.GrdScaleId
                                                                                  FROM      arGrdBkWeights A
                                                                                           ,arClassSections B
                                                                                  WHERE     A.ReqId = B.ReqId
                                                                                            AND A.EffectiveDate <= B.StartDate
                                                                                            AND B.ClsSectionId = @ClsSectionId
                                                                                  ORDER BY  A.EffectiveDate DESC
                                                                                ) X
                                                                               ,arGrdBkWgtDetails C
                                                                               ,arGrdComponentTypes D
                                                                               ,arGrdBkResults GR
                                                                      WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                                AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                                AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                                AND D.SysComponentTypeId NOT IN ( 500,503 )
                                                                                AND GR.StuEnrollId = @StuEnrollId
                                                                                AND GR.ClsSectionId = @ClsSectionId
                                                                                AND GR.Score IS NOT NULL
                                                                      GROUP BY  C.InstrGrdBkWgtDetailId
                                                                               ,D.Code
                                                                               ,D.Descrip
                                                                               ,C.Weight
                                                                               ,C.Number
                                                                               ,C.GrdPolicyId
                                                                               ,C.Parameter
                                                                               ,X.GrdScaleId
                                                                    ) S
                                                          GROUP BY  InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight
                                                                   ,NumberOfComponents
                                                        ) FinalTblToComputeCurrentScore
                                            );
                        IF ( @CurrentScore IS NULL )
                            BEGIN
				-- instructor grade books
                                SET @CurrentScore = (
                                                      SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                                     ELSE NULL
                                                                END AS CurrentScore
                                                      FROM      (
                                                                  SELECT    InstrGrdBkWgtDetailId
                                                                           ,Code
                                                                           ,Descrip
                                                                           ,Weight AS GradeBookWeight
                                                                           ,CASE WHEN S.NumberOfComponents > 0
                                                                                 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                                 ELSE 0
                                                                            END AS ActualWeight
                                                                  FROM      (
                                                                              SELECT    C.InstrGrdBkWgtDetailId
                                                                                       ,D.Code
                                                                                       ,D.Descrip
                                                                                       ,ISNULL(C.Weight,0) AS Weight
                                                                                       ,C.Number AS MinNumber
                                                                                       ,C.GrdPolicyId
                                                                                       ,C.Parameter AS Param
                                                                                       ,X.GrdScaleId
                                                                                       ,SUM(GR.Score) AS Score
                                                                                       ,COUNT(D.Descrip) AS NumberOfComponents
                                                                              FROM      (
                                                                                          --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
															--FROM          arGrdBkWeights A,arClassSections B        
															--WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
															--ORDER BY      A.EffectiveDate DESC
                                                                                          SELECT DISTINCT TOP 1
                                                                                                    t1.InstrGrdBkWgtId
                                                                                                   ,t1.GrdScaleId
                                                                                          FROM      arClassSections t1
                                                                                                   ,arGrdBkWeights t2
                                                                                          WHERE     t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                                                    AND t1.ClsSectionId = @ClsSectionId
                                                                                        ) X
                                                                                       ,arGrdBkWgtDetails C
                                                                                       ,arGrdComponentTypes D
                                                                                       ,arGrdBkResults GR
                                                                              WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                                        AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                                        AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                                        AND
													-- D.SysComponentTypeID not in (500,503) and 
                                                                                        GR.StuEnrollId = @StuEnrollId
                                                                                        AND GR.ClsSectionId = @ClsSectionId
                                                                                        AND GR.Score IS NOT NULL
                                                                              GROUP BY  C.InstrGrdBkWgtDetailId
                                                                                       ,D.Code
                                                                                       ,D.Descrip
                                                                                       ,C.Weight
                                                                                       ,C.Number
                                                                                       ,C.GrdPolicyId
                                                                                       ,C.Parameter
                                                                                       ,X.GrdScaleId
                                                                            ) S
                                                                  GROUP BY  InstrGrdBkWgtDetailId
                                                                           ,Code
                                                                           ,Descrip
                                                                           ,Weight
                                                                           ,NumberOfComponents
                                                                ) FinalTblToComputeCurrentScore
                                                    );	
			
                            END;
			
		
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore,0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;	
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
				
                        UPDATE  syCreditSummary
                        SET     FinalScore = @FinalScore
                               ,CurrentScore = @CurrentScore
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId
                                AND ReqId = @reqid;

			--Average calculation
			
			-- Term Average
                        SET @TermAverageCount = (
                                                  SELECT    COUNT(*)
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = @StuEnrollId
                                                            AND TermId = @TermId
                                                            AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                                SELECT  SUM(FinalScore)
                                                FROM    syCreditSummary
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND TermId = @TermId
                                                        AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
                        SET @cumAveragecount = (
                                                 SELECT COUNT(*)
                                                 FROM   syCreditSummary
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                               SELECT   SUM(FinalScore)
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
                        UPDATE  syCreditSummary
                        SET     Average = @TermAverage
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId; 
			
			--Update Cumulative GPA
                        UPDATE  syCreditSummary
                        SET     CumAverage = @CumAverage
                        WHERE   StuEnrollId = @StuEnrollId;
			

                        FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                            @FinalGrade,@sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,
                            @IsInGPA,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;


                DECLARE GetCreditsSummary_Cursor CURSOR
                FOR
                    SELECT	DISTINCT
                            SE.StuEnrollId
                           ,T.TermId
                           ,T.TermDescrip
                           ,T.StartDate
                           ,R.ReqId
                           ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                           ,RES.Score AS FinalScore
                           ,RES.GrdSysDetailId AS FinalGrade
                           ,GCT.SysComponentTypeId
                           ,R.Credits AS CreditsAttempted
                           ,CS.ClsSectionId
                           ,GSD.Grade
                           ,GSD.IsPass
                           ,GSD.IsCreditsAttempted
                           ,GSD.IsCreditsEarned
                           ,SE.PrgVerId
                           ,GSD.IsInGPA
                           ,R.FinAidCredits AS FinAidCredits
                    FROM    arStuEnrollments SE
                    INNER JOIN arPrgVersions PV ON SE.PrgVerId = PV.PrgVerId
                    INNER JOIN arTransferGrades RES ON RES.StuEnrollId = SE.StuEnrollId
                    INNER JOIN arClassSections CS ON RES.ReqId = CS.ReqId
                                                     AND RES.TermId = CS.TermId
                    INNER JOIN arTerm T ON CS.TermId = T.TermId
                    INNER JOIN arReqs R ON CS.ReqId = R.ReqId
                    LEFT JOIN arGrdBkResults GBR ON CS.ClsSectionId = GBR.ClsSectionId
                    LEFT JOIN arGrdBkWgtDetails GBWD ON GBR.InstrGrdBkWgtDetailId = GBWD.InstrGrdBkWgtDetailId
                    LEFT JOIN arGrdComponentTypes GCT ON GCT.GrdComponentTypeId = GBWD.GrdComponentTypeId
                    LEFT JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    WHERE   SE.StuEnrollId = @StuEnrollId
                    ORDER BY T.StartDate
                           ,T.TermDescrip
                           ,R.ReqId;


                SET @varGradeRounding = (
                                          SELECT    Value
                                          FROM      syConfigAppSetValues
                                          WHERE     SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                    @FinalGrade,@sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,@IsInGPA,
                    @FinAidCredits;
                WHILE @@FETCH_STATUS = 0
                    BEGIN
			
                        SET @CurrentScore = (
                                              SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                             ELSE NULL
                                                        END AS CurrentScore
                                              FROM      (
                                                          SELECT    InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight AS GradeBookWeight
                                                                   ,CASE WHEN S.NumberOfComponents > 0
                                                                         THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                         ELSE 0
                                                                    END AS ActualWeight
                                                          FROM      (
                                                                      SELECT    C.InstrGrdBkWgtDetailId
                                                                               ,D.Code
                                                                               ,D.Descrip
                                                                               ,ISNULL(C.Weight,0) AS Weight
                                                                               ,C.Number AS MinNumber
                                                                               ,C.GrdPolicyId
                                                                               ,C.Parameter AS Param
                                                                               ,X.GrdScaleId
                                                                               ,SUM(GR.Score) AS Score
                                                                               ,COUNT(D.Descrip) AS NumberOfComponents
                                                                      FROM      (
                                                                                  SELECT DISTINCT TOP 1
                                                                                            A.InstrGrdBkWgtId
                                                                                           ,A.EffectiveDate
                                                                                           ,B.GrdScaleId
                                                                                  FROM      arGrdBkWeights A
                                                                                           ,arClassSections B
                                                                                  WHERE     A.ReqId = B.ReqId
                                                                                            AND A.EffectiveDate <= B.StartDate
                                                                                            AND B.ClsSectionId = @ClsSectionId
                                                                                  ORDER BY  A.EffectiveDate DESC
                                                                                ) X
                                                                               ,arGrdBkWgtDetails C
                                                                               ,arGrdComponentTypes D
                                                                               ,arGrdBkResults GR
                                                                      WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                                AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                                AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                                AND D.SysComponentTypeId NOT IN ( 500,503 )
                                                                                AND GR.StuEnrollId = @StuEnrollId
                                                                                AND GR.ClsSectionId = @ClsSectionId
                                                                                AND GR.Score IS NOT NULL
                                                                      GROUP BY  C.InstrGrdBkWgtDetailId
                                                                               ,D.Code
                                                                               ,D.Descrip
                                                                               ,C.Weight
                                                                               ,C.Number
                                                                               ,C.GrdPolicyId
                                                                               ,C.Parameter
                                                                               ,X.GrdScaleId
                                                                    ) S
                                                          GROUP BY  InstrGrdBkWgtDetailId
                                                                   ,Code
                                                                   ,Descrip
                                                                   ,Weight
                                                                   ,NumberOfComponents
                                                        ) FinalTblToComputeCurrentScore
                                            );
                        IF ( @CurrentScore IS NULL )
                            BEGIN
				-- instructor grade books
                                SET @CurrentScore = (
                                                      SELECT    CASE WHEN SUM(GradeBookWeight) > 0 THEN ( SUM(ActualWeight) / SUM(GradeBookWeight) ) * 100
                                                                     ELSE NULL
                                                                END AS CurrentScore
                                                      FROM      (
                                                                  SELECT    InstrGrdBkWgtDetailId
                                                                           ,Code
                                                                           ,Descrip
                                                                           ,Weight AS GradeBookWeight
                                                                           ,CASE WHEN S.NumberOfComponents > 0
                                                                                 THEN SUM(( Weight * ( Score / NumberOfComponents ) ) / 100)
                                                                                 ELSE 0
                                                                            END AS ActualWeight
                                                                  FROM      (
                                                                              SELECT    C.InstrGrdBkWgtDetailId
                                                                                       ,D.Code
                                                                                       ,D.Descrip
                                                                                       ,ISNULL(C.Weight,0) AS Weight
                                                                                       ,C.Number AS MinNumber
                                                                                       ,C.GrdPolicyId
                                                                                       ,C.Parameter AS Param
                                                                                       ,X.GrdScaleId
                                                                                       ,SUM(GR.Score) AS Score
                                                                                       ,COUNT(D.Descrip) AS NumberOfComponents
                                                                              FROM      (
                                                                                          --SELECT Distinct Top 1 A.InstrGrdBkWgtId,A.EffectiveDate,B.GrdScaleId        
															--FROM          arGrdBkWeights A,arClassSections B        
															--WHERE         A.ReqId=B.ReqId AND A.EffectiveDate<=B.StartDate AND B.ClsSectionId=@ClsSectionId
															--ORDER BY      A.EffectiveDate DESC
                                                                                          SELECT DISTINCT TOP 1
                                                                                                    t1.InstrGrdBkWgtId
                                                                                                   ,t1.GrdScaleId
                                                                                          FROM      arClassSections t1
                                                                                                   ,arGrdBkWeights t2
                                                                                          WHERE     t1.InstrGrdBkWgtId = t2.InstrGrdBkWgtId
                                                                                                    AND t1.ClsSectionId = @ClsSectionId
                                                                                        ) X
                                                                                       ,arGrdBkWgtDetails C
                                                                                       ,arGrdComponentTypes D
                                                                                       ,arGrdBkResults GR
                                                                              WHERE     X.InstrGrdBkWgtId = C.InstrGrdBkWgtId
                                                                                        AND C.GrdComponentTypeId = D.GrdComponentTypeId
                                                                                        AND C.InstrGrdBkWgtDetailId = GR.InstrGrdBkWgtDetailId
                                                                                        AND
													-- D.SysComponentTypeID not in (500,503) and 
                                                                                        GR.StuEnrollId = @StuEnrollId
                                                                                        AND GR.ClsSectionId = @ClsSectionId
                                                                                        AND GR.Score IS NOT NULL
                                                                              GROUP BY  C.InstrGrdBkWgtDetailId
                                                                                       ,D.Code
                                                                                       ,D.Descrip
                                                                                       ,C.Weight
                                                                                       ,C.Number
                                                                                       ,C.GrdPolicyId
                                                                                       ,C.Parameter
                                                                                       ,X.GrdScaleId
                                                                            ) S
                                                                  GROUP BY  InstrGrdBkWgtDetailId
                                                                           ,Code
                                                                           ,Descrip
                                                                           ,Weight
                                                                           ,NumberOfComponents
                                                                ) FinalTblToComputeCurrentScore
                                                    );	
			
                            END;
			
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore,0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;	
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
				
                        UPDATE  syCreditSummary
                        SET     FinalScore = @FinalScore
                               ,CurrentScore = @CurrentScore
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId
                                AND ReqId = @reqid;

			--Average calculation
--			declare @CumAverage decimal(18,2),@cumAverageSum decimal(18,2),@cumAveragecount int
			
			-- Term Average
                        SET @TermAverageCount = (
                                                  SELECT    COUNT(*)
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = @StuEnrollId
                                                            AND TermId = @TermId
                                                            AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                                SELECT  SUM(FinalScore)
                                                FROM    syCreditSummary
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND TermId = @TermId
                                                        AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
                        SET @cumAveragecount = (
                                                 SELECT COUNT(*)
                                                 FROM   syCreditSummary
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                               SELECT   SUM(FinalScore)
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
                        UPDATE  syCreditSummary
                        SET     Average = @TermAverage
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId; 
			
			--Update Cumulative GPA
                        UPDATE  syCreditSummary
                        SET     CumAverage = @CumAverage
                        WHERE   StuEnrollId = @StuEnrollId;
			

                        FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                            @FinalGrade,@sysComponentTypeId,@CreditsAttempted,@ClsSectionId,@Grade,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@PrgVerId,
                            @IsInGPA,@FinAidCredits;
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

                DECLARE @GradeSystemDetailId UNIQUEIDENTIFIER
                   ,@IsGPA BIT;
                DECLARE GetCreditsSummary_Cursor CURSOR
                FOR
                    SELECT DISTINCT
                            SE.StuEnrollId
                           ,T.TermId
                           ,T.TermDescrip
                           ,T.StartDate
                           ,R.ReqId
                           ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                           ,NULL AS FinalScore
                           ,RES.GrdSysDetailId AS GradeSystemDetailId
                           ,GSD.Grade AS FinalGrade
                           ,GSD.Grade AS CurrentGrade
                           ,R.Credits
                           ,NULL AS ClsSectionId
                           ,GSD.IsPass
                           ,GSD.IsCreditsAttempted
                           ,GSD.IsCreditsEarned
                           ,GSD.GPA
                           ,GSD.IsInGPA
                           ,SE.PrgVerId
                           ,GSD.IsInGPA
                           ,R.FinAidCredits AS FinAidCredits
                    FROM    arStuEnrollments SE
                    INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
                    INNER JOIN syCreditSummary CS ON CS.StuEnrollId = RES.StuEnrollId
                    INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                    INNER JOIN arTerm T ON RES.TermId = T.TermId
                    WHERE   RES.ReqId NOT IN ( SELECT DISTINCT
                                                        ReqId
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = SE.StuEnrollId
                                                        AND TermId = T.TermId )
                            AND SE.StuEnrollId = @StuEnrollId;

                SET @varGradeRounding = (
                                          SELECT    Value
                                          FROM      syConfigAppSetValues
                                          WHERE     SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                    @GradeSystemDetailId,@FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,
                    @PrgVerId,@IsInGPA,@FinAidCredits; 
                WHILE @@FETCH_STATUS = 0
                    BEGIN
			
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore,0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;	
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
				
                        UPDATE  syCreditSummary
                        SET     FinalScore = @FinalScore
                               ,CurrentScore = @CurrentScore
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId
                                AND ReqId = @reqid;

			--Average calculation
		
			-- Term Average
                        SET @TermAverageCount = (
                                                  SELECT    COUNT(*)
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = @StuEnrollId
                                                            AND TermId = @TermId
                                                            AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                                SELECT  SUM(FinalScore)
                                                FROM    syCreditSummary
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND TermId = @TermId
                                                        AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
                        SET @cumAveragecount = (
                                                 SELECT COUNT(*)
                                                 FROM   syCreditSummary
                                                 WHERE  StuEnrollId = @StuEnrollId
											--AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                               SELECT   SUM(FinalScore)
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
                        UPDATE  syCreditSummary
                        SET     Average = @TermAverage
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId; 
			
			--Update Cumulative GPA
                        UPDATE  syCreditSummary
                        SET     CumAverage = @CumAverage
                        WHERE   StuEnrollId = @StuEnrollId;
			

                        FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                            @GradeSystemDetailId,@FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,
                            @PrgVerId,@IsInGPA,@FinAidCredits; 
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

                DECLARE GetCreditsSummary_Cursor CURSOR
                FOR
                    SELECT DISTINCT
                            SE.StuEnrollId
                           ,T.TermId
                           ,T.TermDescrip
                           ,T.StartDate
                           ,R.ReqId
                           ,''('' + R.Code + '' ) '' + R.Descrip AS CourseCodeDescrip
                           ,NULL AS FinalScore
                           ,RES.GrdSysDetailId AS GradeSystemDetailId
                           ,GSD.Grade AS FinalGrade
                           ,GSD.Grade AS CurrentGrade
                           ,R.Credits
                           ,NULL AS ClsSectionId
                           ,GSD.IsPass
                           ,GSD.IsCreditsAttempted
                           ,GSD.IsCreditsEarned
                           ,GSD.GPA
                           ,GSD.IsInGPA
                           ,SE.PrgVerId
                           ,GSD.IsInGPA
                           ,R.FinAidCredits AS FinAidCredits
                    FROM    arStuEnrollments SE
                    INNER JOIN arTransferGrades RES ON SE.StuEnrollId = RES.StuEnrollId
                    INNER JOIN arGradeSystemDetails GSD ON RES.GrdSysDetailId = GSD.GrdSysDetailId
                    INNER JOIN arReqs R ON RES.ReqId = R.ReqId
                    INNER JOIN arTerm T ON RES.TermId = T.TermId
                    WHERE   SE.StuEnrollId NOT IN ( SELECT DISTINCT
                                                            StuEnrollId
                                                    FROM    syCreditSummary )
                            AND SE.StuEnrollId = @StuEnrollId;

                SET @varGradeRounding = (
                                          SELECT    Value
                                          FROM      syConfigAppSetValues
                                          WHERE     SettingId = 45
                                        );

                OPEN GetCreditsSummary_Cursor;
                SET @PrevStuEnrollId = NULL;
                SET @PrevTermId = NULL;
                SET @PrevReqId = NULL;
                SET @RowCount = 0;
                FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                    @GradeSystemDetailId,@FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,
                    @PrgVerId,@IsInGPA,@FinAidCredits; 
                WHILE @@FETCH_STATUS = 0
                    BEGIN
			
			-- If rounding is set to yes, then round the scores to next available score or ignore rounding
                        IF ( LOWER(@varGradeRounding) = ''yes'' )
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @FinalScore = ROUND(@FinalScore,0);
                                        SET @CurrentScore = @FinalScore;
                                    END;
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
                        ELSE
                            BEGIN
                                IF @FinalScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = @FinalScore;
                                    END;	
                                IF @FinalScore IS NULL
                                    AND @CurrentScore IS NOT NULL
                                    BEGIN
                                        SET @CurrentScore = ROUND(@CurrentScore,0);
                                    END;
                            END;
				
                        UPDATE  syCreditSummary
                        SET     FinalScore = @FinalScore
                               ,CurrentScore = @CurrentScore
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId
                                AND ReqId = @reqid;

			--Average calculation
                        SET @TermAverageCount = (
                                                  SELECT    COUNT(*)
                                                  FROM      syCreditSummary
                                                  WHERE     StuEnrollId = @StuEnrollId
                                                            AND TermId = @TermId
                                                            AND FinalScore IS NOT NULL
                                                );
                        SET @termAverageSum = (
                                                SELECT  SUM(FinalScore)
                                                FROM    syCreditSummary
                                                WHERE   StuEnrollId = @StuEnrollId
                                                        AND TermId = @TermId
                                                        AND FinalScore IS NOT NULL
                                              );
                        SET @TermAverage = @termAverageSum / @TermAverageCount; 
			
			-- Cumulative Average
                        SET @cumAveragecount = (
                                                 SELECT COUNT(*)
                                                 FROM   syCreditSummary
                                                 WHERE  StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                               );
                        SET @cumAverageSum = (
                                               SELECT   SUM(FinalScore)
                                               FROM     syCreditSummary
                                               WHERE    StuEnrollId = @StuEnrollId
                                                        AND FinalScore IS NOT NULL
                                             );
                        SET @CumAverage = @cumAverageSum / @cumAveragecount; 
			
                        UPDATE  syCreditSummary
                        SET     Average = @TermAverage
                        WHERE   StuEnrollId = @StuEnrollId
                                AND TermId = @TermId; 
			
			--Update Cumulative GPA
                        UPDATE  syCreditSummary
                        SET     CumAverage = @CumAverage
                        WHERE   StuEnrollId = @StuEnrollId;
			

                        FETCH NEXT FROM GetCreditsSummary_Cursor INTO @StuEnrollId,@TermId,@TermDescrip,@termstartdate1,@reqid,@CourseCodeDescrip,@FinalScore,
                            @GradeSystemDetailId,@FinalGrade,@CurrentGrade,@CourseCredits,@ClsSectionId,@IsPass,@IsCreditsAttempted,@IsCreditsEarned,@GPA,@IsGPA,
                            @PrgVerId,@IsInGPA,@FinAidCredits;  
                    END;
                CLOSE GetCreditsSummary_Cursor;
                DEALLOCATE GetCreditsSummary_Cursor;

---- PRINT ''@CurrentBal_Compute'', 
---- PRINT @CurrentBal_Compute
		
            END;

        SET @TermAverageCount = (
                                  SELECT    COUNT(*)
                                  FROM      syCreditSummary
                                  WHERE     StuEnrollId = @StuEnrollId
                                            AND FinalScore IS NOT NULL
                                );
        SET @termAverageSum = (
                                SELECT  SUM(FinalScore)
                                FROM    syCreditSummary
                                WHERE   StuEnrollId = @StuEnrollId
                                        AND FinalScore IS NOT NULL
                              );
        IF @TermAverageCount >= 1
            BEGIN
                SET @TermAverage = @termAverageSum / @TermAverageCount; 
            END;
			
		-- Cumulative Average
        SET @cumAveragecount = (
                                 SELECT COUNT(*)
                                 FROM   syCreditSummary
                                 WHERE  StuEnrollId = @StuEnrollId
                                        AND FinalScore IS NOT NULL
                               );
        SET @cumAverageSum = (
                               SELECT   SUM(FinalScore)
                               FROM     syCreditSummary
                               WHERE    StuEnrollId = @StuEnrollId
                                        AND FinalScore IS NOT NULL
                             );
								
        IF @cumAveragecount >= 1
            BEGIN
                SET @CumAverage = @cumAverageSum / @cumAveragecount;
            END;    
			
        --If Clock Hour / Numeric - use New Average Calculation
        IF (
           SELECT TOP 1 P.ACId
           FROM   dbo.arStuEnrollments E
           JOIN   dbo.arPrgVersions PV ON PV.PrgVerId = E.PrgVerId
           JOIN   dbo.arPrograms P ON P.ProgId = PV.ProgId
           WHERE  E.StuEnrollId = @StuEnrollId
           ) = 5
           AND @GradesFormat = ''numeric''
            BEGIN
                EXEC dbo.USP_GPACalculator @EnrollmentId = @StuEnrollId
                                          ,@StudentGPA = @CumAverage OUTPUT;
            END;
	---- PRINT @cumAveragecount 
	---- PRINT @CumAverageSum
	---- PRINT @CumAverage
	---- PRINT @UnitTypeId;
	---- PRINT @TrackSapAttendance;
	---- PRINT @displayHours;
    END;
--=================================================================================================
-- END  --  Usp_TR_Sub4_GetCumGPAandAverageforAGivenStuEnrollId
--=================================================================================================	

'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Creating trigger [dbo].[arStuEnrollments_Integration_Tracking] on [dbo].[arStuEnrollments]'
GO
IF OBJECT_ID(N'[dbo].[arStuEnrollments_Integration_Tracking]', 'TR') IS NULL
EXEC sp_executesql N'--BEGIN	
--CREATE TABLE dbo.IntegrationEnrollmentTracking
--(
-- EnrollmentTrackingId UNIQUEIDENTIFIER CONSTRAINT [PK_IntegrationEnrollmentTracking_EnrollmentTrackingId] PRIMARY KEY CLUSTERED 
-- CONSTRAINT [DF_IntegrationEnrollmentTracking_EnrollmentTrackingId] DEFAULT NEWID(),
-- StuEnrollId UNIQUEIDENTIFIER CONSTRAINT[FK_IntegrationEnrollmentTracking_arStuEnrollments_StuEnrollId_StuEnrollId] FOREIGN KEY REFERENCES dbo.arStuEnrollments NOT NULL,
--  EnrollmentStatus VARCHAR(100),
-- ModDate DATETIME NOT NULL ,
-- ModUser NVARCHAR(100),
-- Processed BIT CONSTRAINT [DF_IntegrationEnrollmentTracking_Processed] DEFAULT 0 NOT NULL 	

--)
--END
--DROP TABLE dbo.IntegrationEnrollmentTracking
CREATE TRIGGER [dbo].[arStuEnrollments_Integration_Tracking]
ON [dbo].[arStuEnrollments]
AFTER UPDATE
AS
DECLARE @INS INT
       ,@DEL INT;

SELECT @INS = COUNT(*)
FROM   INSERTED;
SELECT @DEL = COUNT(*)
FROM   DELETED;

IF @INS > 0
   AND @DEL > 0
    BEGIN


        IF UPDATE(StatusCodeId)
           OR UPDATE(LDA)
            BEGIN
                IF EXISTS (
                          SELECT     1
                          FROM       INSERTED i
                          INNER JOIN DELETED d ON d.StuEnrollId = i.StuEnrollId
                          WHERE      ( i.StatusCodeId <> d.StatusCodeId )
                                     OR ( i.LDA <> d.LDA )
                          )
                    BEGIN
                        IF EXISTS (
                                  SELECT     1
                                  FROM       dbo.IntegrationEnrollmentTracking IET
                                  INNER JOIN inserted i ON i.StuEnrollId = IET.StuEnrollId
                                  )
                            BEGIN


                                UPDATE     IET
                                SET        EnrollmentStatus = ss.SysStatusDescrip
                                          ,ModDate = i.ModDate
                                          ,ModUser = i.ModUser
                                          ,Processed = 0
                                FROM       dbo.IntegrationEnrollmentTracking IET
                                INNER JOIN INSERTED i ON i.StuEnrollId = IET.StuEnrollId
                                INNER JOIN dbo.arStuEnrollments se ON i.StuEnrollId = se.StuEnrollId
                                INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                INNER JOIN dbo.sySysStatus ss ON ss.SysStatusId = sc.SysStatusId
                                INNER JOIN dbo.adLeads l ON l.LeadId = se.LeadId
                                WHERE IET.StuEnrollId = i.StuEnrollId AND l.AfaStudentId IS NOT NULL
                            END;

                        ELSE
                            BEGIN

                                INSERT INTO dbo.IntegrationEnrollmentTracking (
                                                                              EnrollmentTrackingId
                                                                             ,StuEnrollId
                                                                             ,EnrollmentStatus
                                                                             ,ModDate
                                                                             ,ModUser
                                                                             ,Processed
                                                                              )
                                            SELECT     NEWID()
                                                      ,i.StuEnrollId
                                                      ,ss.SysStatusDescrip
                                                      ,i.ModDate
                                                      ,i.ModUser
                                                      ,0
                                            FROM       INSERTED i
                                            INNER JOIN dbo.arStuEnrollments se ON i.StuEnrollId = se.StuEnrollId
                                            INNER JOIN dbo.syStatusCodes sc ON sc.StatusCodeId = se.StatusCodeId
                                            INNER JOIN dbo.sySysStatus ss ON ss.SysStatusId = sc.SysStatusId
											INNER JOIN dbo.adLeads l ON l.LeadId = se.LeadId
											WHERE l.AfaStudentId IS NOT NULL

                            END;


                    END;

            END;



    END;
'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[adLeadsView]'
GO
IF OBJECT_ID(N'[dbo].[adLeadsView]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[adLeadsView]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[GlTransView1]'
GO
IF OBJECT_ID(N'[dbo].[GlTransView1]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[GlTransView1]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[GlTransView2]'
GO
IF OBJECT_ID(N'[dbo].[GlTransView2]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[GlTransView2]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[NewStudentSearch]'
GO
IF OBJECT_ID(N'[dbo].[NewStudentSearch]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[NewStudentSearch]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[plStudentEducation]'
GO
IF OBJECT_ID(N'[dbo].[plStudentEducation]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[plStudentEducation]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[plStudentEmployment]'
GO
IF OBJECT_ID(N'[dbo].[plStudentEmployment]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[plStudentEmployment]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[plStudentExtraCurriculars]'
GO
IF OBJECT_ID(N'[dbo].[plStudentExtraCurriculars]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[plStudentExtraCurriculars]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[plStudentSkills]'
GO
IF OBJECT_ID(N'[dbo].[plStudentSkills]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[plStudentSkills]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[SettingStdScoreLimit]'
GO
IF OBJECT_ID(N'[dbo].[SettingStdScoreLimit]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[SettingStdScoreLimit]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[StudentEnrollmentSearch]'
GO
IF OBJECT_ID(N'[dbo].[StudentEnrollmentSearch]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[StudentEnrollmentSearch]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[syStudentContactAddresses]'
GO
IF OBJECT_ID(N'[dbo].[syStudentContactAddresses]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[syStudentContactAddresses]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[syStudentContactPhones]'
GO
IF OBJECT_ID(N'[dbo].[syStudentContactPhones]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[syStudentContactPhones]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[syStudentContacts]'
GO
IF OBJECT_ID(N'[dbo].[syStudentContacts]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[syStudentContacts]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[syStudentNotes]'
GO
IF OBJECT_ID(N'[dbo].[syStudentNotes]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[syStudentNotes]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[VIEW_GetHistory]'
GO
IF OBJECT_ID(N'[dbo].[VIEW_GetHistory]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[VIEW_GetHistory]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[VIEW_LeadUserTask]'
GO
IF OBJECT_ID(N'[dbo].[VIEW_LeadUserTask]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[VIEW_LeadUserTask]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[View_Messages]'
GO
IF OBJECT_ID(N'[dbo].[View_Messages]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[View_Messages]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[VIEW_SySDFModuleValue_Lead]'
GO
IF OBJECT_ID(N'[dbo].[VIEW_SySDFModuleValue_Lead]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[VIEW_SySDFModuleValue_Lead]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[View_TaskNotes]'
GO
IF OBJECT_ID(N'[dbo].[View_TaskNotes]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[View_TaskNotes]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
PRINT N'Refreshing [dbo].[VIEW1]'
GO
IF OBJECT_ID(N'[dbo].[VIEW1]', 'V') IS NOT NULL
EXEC sp_refreshview N'[dbo].[VIEW1]'
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
COMMIT TRANSACTION
GO
IF @@ERROR <> 0 SET NOEXEC ON
GO
DECLARE @Success AS BIT
SET @Success = 1
SET NOEXEC OFF
IF (@Success = 1) PRINT 'The database update succeeded'
ELSE BEGIN
	IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
	PRINT 'The database update failed'
END
BEGIN
    DECLARE @Error AS INTEGER;

    SET @Error = 0;

    BEGIN TRANSACTION IdentityFix;
    BEGIN TRY


        IF NOT EXISTS
        (
            SELECT OBJECT_NAME(object_id) AS TABLENAME,
                   name AS COLUMNNAME,
                   seed_value,
                   increment_value,
                   last_value,
                   is_not_for_replication
            FROM sys.identity_columns
            WHERE OBJECT_NAME(object_id) = 'syConfigAppSettings'
        )
        BEGIN

            SELECT *
            INTO #syConfigAppSettings
            FROM syConfigAppSettings;

            ALTER TABLE syConfigAppSettings
            DROP CONSTRAINT PK_syConfigAppSettings_SettingId;

            ALTER TABLE syConfigAppSettings DROP COLUMN SettingId;

            ALTER TABLE syConfigAppSettings
            ADD SettingId INT IDENTITY(1, 1) NOT NULL;

            SET IDENTITY_INSERT dbo.syConfigAppSettings ON;

            DELETE FROM syConfigAppSettings;

            INSERT INTO syConfigAppSettings
            (
                KeyName,
                Description,
                ModUser,
                ModDate,
                CampusSpecific,
                ExtraConfirmation,
                SettingId
            )
            SELECT KeyName,
                   Description,
                   ModUser,
                   ModDate,
                   CampusSpecific,
                   ExtraConfirmation,
                   SettingId
            FROM #syConfigAppSettings;


            SET IDENTITY_INSERT dbo.syConfigAppSettings OFF;

            DROP TABLE #syConfigAppSettings;


            ALTER TABLE [dbo].[syConfigAppSettings]
            ADD CONSTRAINT [PK_syConfigAppSettings_SettingId]
                PRIMARY KEY CLUSTERED ([SettingId] ASC)
                WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF,
                      ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON
                     ) ON [PRIMARY];


					  PRINT 'identity added to syConfigAppSettings';
        END;

		IF NOT EXISTS
		(
			SELECT *
			FROM sys.default_constraints
			WHERE name = 'DF_syConfigAppSetValues_ValueId'
		)
		BEGIN
			ALTER TABLE [dbo].[syConfigAppSetValues]
			ADD CONSTRAINT [DF_syConfigAppSetValues_ValueId]
				DEFAULT (NEWSEQUENTIALID()) FOR [ValueId];
		END;

        IF (@@ERROR > 0)
        BEGIN
            SET @Error = @@ERROR;
        END;

    END TRY
    BEGIN CATCH
        DECLARE @msg NVARCHAR(MAX);
        DECLARE @severity INT;
        DECLARE @state INT;
        SELECT @msg = ERROR_MESSAGE(),
               @severity = ERROR_SEVERITY(),
               @state = ERROR_STATE();
        RAISERROR(@msg, @severity, @state);
        SET @Error = 1;
    END CATCH;

    IF (@Error = 0)
    BEGIN
        COMMIT TRANSACTION IdentityFix;

        PRINT 'IdentityFix script for syConfigAppSettings was executed';
    END;
    ELSE
    BEGIN
        ROLLBACK TRANSACTION IdentityFix;
        PRINT 'Unable to add identity to syConfigAppSettings';
    END;
END;