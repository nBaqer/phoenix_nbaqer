﻿--=================================================================================================
-- START AD-12633 Enable export to excel for Attendance Summary report.
--=================================================================================================
DECLARE @ErrorAllowExportTypeExcelForAttSummReport AS INT;
SET @ErrorAllowExportTypeExcelForAttSummReport = 0;
BEGIN TRANSACTION AllowExportExcelForAttSummRep;
BEGIN TRY
    UPDATE syReports
    SET AllowedExportTypes = 4
    WHERE ResourceId = 877;
END TRY
BEGIN CATCH
    SET @ErrorAllowExportTypeExcelForAttSummReport = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @ErrorAllowExportTypeExcelForAttSummReport > 0
BEGIN
    ROLLBACK TRANSACTION AllowExportExcelForAttSummRep;
    PRINT 'Failed transcation AllowExportTypeExcelForAttSummReport.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION AllowExportExcelForAttSummRep;
    PRINT 'successful transaction AllowExportTypeExcelForAttSummReport.';
END;
GO
--=================================================================================================
-- END AD-12633 Enable export to excel for Attendance Summary report.
--=================================================================================================
--=================================================================================================
-- START AD-13246 : Beta 4.0: Back Fill Script for Date Completed
--=================================================================================================
DECLARE @ErrorUpdate AS INT;
SET @ErrorUpdate = 0;
BEGIN TRANSACTION DateCompleteUpdate;
BEGIN TRY
    IF EXISTS
    (
        SELECT 1
        FROM dbo.arResults
        WHERE GrdSysDetailId IS NOT NULL
              AND
              (
                  DateCompleted IS NULL
                  OR CAST(DateCompleted AS DATE) = '1/1/1900'
              )
    )
    BEGIN
        ALTER TABLE dbo.arResults DISABLE TRIGGER ALL;
		
        UPDATE r
        SET r.DateCompleted = CASE
                                  WHEN r.ModDate IS NULL THEN
                                      e.ContractedGradDate
                                  ELSE
                                      CASE
                                          WHEN r.ModDate
                                               BETWEEN e.StartDate AND e.ExpGradDate THEN
                                              r.ModDate
                                          ELSE
                                              e.ExpGradDate
                                      END
                              END
        FROM dbo.arResults r
            INNER JOIN dbo.arStuEnrollments e
                ON e.StuEnrollId = r.StuEnrollId
        WHERE GrdSysDetailId IS NOT NULL
              AND
              (
                  DateCompleted IS NULL
                  OR CAST(DateCompleted AS DATE) = '1/1/1900'
              );

        ALTER TABLE dbo.arResults ENABLE TRIGGER ALL;
    END;

END TRY
BEGIN CATCH
    SET @ErrorUpdate = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @ErrorUpdate > 0
BEGIN
    ROLLBACK TRANSACTION DateCompleteUpdate;
    PRINT 'Failed transcation DateCompleteUpdate.';

END;
ELSE
BEGIN
    COMMIT TRANSACTION DateCompleteUpdate;
    PRINT 'successful transaction DateCompleteUpdate.';
END;
GO
--=================================================================================================
-- END AD-13246 : Beta 4.0: Back Fill Script for Date Completed
--=================================================================================================
--=================================================================================================
-- START AD-13507 Backfill Script for Historical Component Dates
--=================================================================================================
DECLARE @Error AS INT;
SET @Error = 0;
BEGIN TRANSACTION GrdBkResultsDateComp;
BEGIN TRY
    UPDATE dbo.arGrdBkResults
    SET DateCompleted = CASE
                            WHEN arGrdBkResults.ModDate < EndDate
                                 AND arGrdBkResults.ModDate < ExpGradDate THEN
                                dbo.arGrdBkResults.ModDate
                            WHEN arGrdBkResults.ModDate > EndDate
                                 AND EndDate < ExpGradDate THEN
                                EndDate
                            WHEN arGrdBkResults.ModDate > EndDate
                                 AND EndDate > ExpGradDate THEN
                                ExpGradDate
                            ELSE
                                ExpGradDate
                        END
    FROM dbo.arGrdBkResults
        JOIN dbo.arClassSections
            ON arClassSections.ClsSectionId = arGrdBkResults.ClsSectionId
        JOIN dbo.arStuEnrollments
            ON arStuEnrollments.StuEnrollId = arGrdBkResults.StuEnrollId
    WHERE DateCompleted IS NULL
          AND Score IS NOT NULL;
END TRY
BEGIN CATCH
    SET @Error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE(),
           @severity = ERROR_SEVERITY(),
           @state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @Error > 0
BEGIN
    ROLLBACK TRANSACTION GrdBkResultsDateComp;
    PRINT 'Failed transaction GrdBkResultsDateComp.';
END;
ELSE
BEGIN
    COMMIT TRANSACTION GrdBkResultsDateComp;
    PRINT 'successful transaction GrdBkResultsDateComp.';
END;
GO
--=================================================================================================
-- END AD-13507 Backfill Script for Historical Component Dates
--=================================================================================================

