-- ===============================================================================================
-- Consolidated Script Version 3.11
-- Data Changes Zone
-- Please do not deploy your schema changes in this area
-- Please use SQL Prompt format before insert here please
-- Please run after Schema changes....
-- ===============================================================================================

---------------------------------------------------------------------------------------------------------------------------------------------
--Dev Coding: AD-3308 - Adhoc Reporting: Update links for student phone numbers and addresses
---------------------------------------------------------------------------------------------------------------------------------------------
BEGIN

    DECLARE @Error AS INTEGER;
    DECLARE @LeadEntityId AS INTEGER;
    DECLARE @StudentEntityId AS INTEGER;

    SET @Error = 0;

    SET @LeadEntityId = (
                        SELECT ResourceID
                        FROM   dbo.syResources
                        WHERE  Resource = 'Lead'
                               AND ResourceTypeID = 8
                        );
    SET @StudentEntityId = (
                           SELECT ResourceID
                           FROM   dbo.syResources
                           WHERE  Resource = 'Student'
                                  AND ResourceTypeID = 8
                           );

    BEGIN TRANSACTION AdhocReports;
    BEGIN TRY
        ------------------------------------------------------------------------------------------------------
        --LEAD PHONES
        ------------------------------------------------------------------------------------------------------
        IF @Error = 0
            BEGIN
                --Create a new category for Lead Phones
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFldCategories
                              WHERE  EntityId = @LeadEntityId
                                     AND Descrip = 'Lead Phones'
                              )
                    BEGIN
                        INSERT INTO dbo.syFldCategories (
                                                        EntityId
                                                       ,Descrip
                                                       ,CategoryId
                                                        )
                        VALUES ( @LeadEntityId -- EntityId - smallint
                                ,'Lead Phones' -- Descrip - varchar(50)
                                ,(
                                 SELECT MAX(CategoryId)
                                 FROM   dbo.syFldCategories
                                 ) + 1         -- CategoryId - int
                               );
                    END;

                --Remove the Phone_Best field from the metadata for the adLeadPhone table
                IF EXISTS (
                          SELECT *
                          FROM   dbo.syTables t
                          INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                          INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                          WHERE  t.TblName = 'adLeadPhone'
                                 AND f.FldName = 'Phone_Best'
                          )
                    BEGIN
                        DELETE FROM dbo.syTblFlds
                        WHERE EXISTS (
                                     SELECT *
                                     FROM   dbo.syTables t
                                     INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                                     INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                                     WHERE  t.TblName = 'adLeadPhone'
                                            AND f.FldName = 'Phone_Best'
                                            AND dbo.syTblFlds.TblFldsId = tf.TblFldsId
                                     );
                    END;


                --Add the LeadPhoneId if it is missing
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFields
                              WHERE  FldName = 'LeadPhoneId'
                              )
                    BEGIN
                        INSERT INTO dbo.syFields (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                 )
                        VALUES ((
                                SELECT MAX(FldId)
                                FROM   dbo.syFields
                                ) + 1         -- FldId - int
                               ,'LeadPhoneId' -- FldName - varchar(200)
                               ,72            -- FldTypeId - int
                               ,16            -- FldLen - int
                               ,NULL          -- DDLId - int
                               ,0             -- DerivedFld - bit
                               ,0             -- SchlReq - bit
                               ,0             -- LogChanges - bit
                               ,NULL          -- Mask - varchar(50)
                               );
                    END;

                --Add the Position field if it is missing
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFields
                              WHERE  FldName = 'Position'
                              )
                    BEGIN
                        INSERT INTO dbo.syFields (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                 )
                        VALUES ((
                                SELECT MAX(FldId)
                                FROM   dbo.syFields
                                ) + 1      -- FldId - int
                               ,'Position' -- FldName - varchar(200)
                               ,3          -- FldTypeId - int
                               ,50         -- FldLen - int
                               ,NULL       -- DDLId - int
                               ,0          -- DerivedFld - bit
                               ,0          -- SchlReq - bit
                               ,0          -- LogChanges - bit
                               ,NULL       -- Mask - varchar(50)
                               );
                    END;

                --Add the caption for the Position field
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFldCaptions fc
                              INNER JOIN dbo.syFields f ON f.FldId = fc.FldId
                              WHERE  f.FldName = 'Position'
                              )
                    INSERT INTO dbo.syFldCaptions (
                                                  FldCapId
                                                 ,FldId
                                                 ,LangId
                                                 ,Caption
                                                 ,FldDescrip
                                                  )
                    VALUES ((
                            SELECT MAX(FldCapId)
                            FROM   dbo.syFldCaptions
                            ) + 1      -- FldCapId - int
                           ,(
                            SELECT FldId
                            FROM   dbo.syFields
                            WHERE  FldName = 'Position'
                            )          -- FldId - int
                           ,1          -- LangId - tinyint
                           ,'Position' -- Caption - varchar(100)
                           ,NULL       -- FldDescrip - varchar(150)
                           );

                --Add the IsBest field if it is missing
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFields
                              WHERE  FldName = 'IsBest'
                              )
                    BEGIN
                        INSERT INTO dbo.syFields (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                 )
                        VALUES ((
                                SELECT MAX(FldId)
                                FROM   dbo.syFields
                                ) + 1    -- FldId - int
                               ,'IsBest' -- FldName - varchar(200)
                               ,11       -- FldTypeId - int
                               ,1        -- FldLen - int
                               ,NULL     -- DDLId - int
                               ,0        -- DerivedFld - bit
                               ,0        -- SchlReq - bit
                               ,0        -- LogChanges - bit
                               ,NULL     -- Mask - varchar(50)
                               );
                    END;

                --Add the caption for the IsBest field
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFldCaptions fc
                              INNER JOIN dbo.syFields f ON f.FldId = fc.FldId
                              WHERE  f.FldName = 'IsBest'
                              )
                    INSERT INTO dbo.syFldCaptions (
                                                  FldCapId
                                                 ,FldId
                                                 ,LangId
                                                 ,Caption
                                                 ,FldDescrip
                                                  )
                    VALUES ((
                            SELECT MAX(FldCapId)
                            FROM   dbo.syFldCaptions
                            ) + 1        -- FldCapId - int
                           ,(
                            SELECT FldId
                            FROM   dbo.syFields
                            WHERE  FldName = 'IsBest'
                            )            -- FldId - int
                           ,1            -- LangId - tinyint
                           ,'Best Phone' -- Caption - varchar(100)
                           ,NULL         -- FldDescrip - varchar(150)
                           );

                --Add the IsShowOnLeadPage field if it is missing
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFields
                              WHERE  FldName = 'IsShowOnLeadPage'
                              )
                    BEGIN
                        INSERT INTO dbo.syFields (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                 )
                        VALUES ((
                                SELECT MAX(FldId)
                                FROM   dbo.syFields
                                ) + 1              -- FldId - int
                               ,'IsShowOnLeadPage' -- FldName - varchar(200)
                               ,11                 -- FldTypeId - int
                               ,1                  -- FldLen - int
                               ,NULL               -- DDLId - int
                               ,0                  -- DerivedFld - bit
                               ,0                  -- SchlReq - bit
                               ,0                  -- LogChanges - bit
                               ,NULL               -- Mask - varchar(50)
                               );
                    END;

                --Add the caption for the IsShowOnLeadPage field
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFldCaptions fc
                              INNER JOIN dbo.syFields f ON f.FldId = fc.FldId
                              WHERE  f.FldName = 'IsShowOnLeadPage'
                              )
                    INSERT INTO dbo.syFldCaptions (
                                                  FldCapId
                                                 ,FldId
                                                 ,LangId
                                                 ,Caption
                                                 ,FldDescrip
                                                  )
                    VALUES ((
                            SELECT MAX(FldCapId)
                            FROM   dbo.syFldCaptions
                            ) + 1               -- FldCapId - int
                           ,(
                            SELECT FldId
                            FROM   dbo.syFields
                            WHERE  FldName = 'IsShowOnLeadPage'
                            )                   -- FldId - int
                           ,1                   -- LangId - tinyint
                           ,'Show on Lead Page' -- Caption - varchar(100)
                           ,NULL                -- FldDescrip - varchar(150)
                           );

                --Fix the caption for the Phone field. It is currently saying Phone - Best.
                --It should just say Phone
                UPDATE fc
                SET    fc.Caption = 'Phone'
                FROM   dbo.syFldCaptions fc
                INNER JOIN dbo.syFields f ON f.FldId = fc.FldId
                WHERE  f.FldName = 'Phone';


                --Add the fields for Lead Phones
                ---------------------------------
                --LeadPhoneId
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'adLeadPhone'
                                     AND f.FldName = 'LeadPhoneId'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'LeadPhoneId'
                                )     -- FldId - int
                               ,NULL  -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;

                --LeadId
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'adLeadPhone'
                                     AND f.FldName = 'LeadId'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'LeadId'
                                )     -- FldId - int
                               ,NULL  -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;

                --PhoneTypeId
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'adLeadPhone'
                                     AND f.FldName = 'PhoneTypeId'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1              -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )                  -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'PhoneTypeId'
                                )                  -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Phones'
                                )                  -- CategoryId - int
                               ,'PhoneTypeDescrip' -- FKColDescrip - varchar(50)
                               );
                    END;

                --Phone
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'adLeadPhone'
                                     AND f.FldName = 'Phone'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'Phone'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;

                --ModDate
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'adLeadPhone'
                                     AND f.FldName = 'ModDate'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'ModDate'
                                )     -- FldId - int
                               ,NULL  -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;

                --ModUser
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'adLeadPhone'
                                     AND f.FldName = 'ModUser'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'ModUser'
                                )     -- FldId - int
                               ,NULL  -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;

                --Position
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'adLeadPhone'
                                     AND f.FldName = 'Position'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'Position'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;


                --Extension
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'adLeadPhone'
                                     AND f.FldName = 'Extension'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'Extension'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;

                --IsForeignPhone
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'adLeadPhone'
                                     AND f.FldName = 'IsForeignPhone'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsForeignPhone'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;

                --IsBest
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'adLeadPhone'
                                     AND f.FldName = 'IsBest'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsBest'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;

                --IsShowOnLeadPage
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'adLeadPhone'
                                     AND f.FldName = 'IsShowOnLeadPage'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsShowOnLeadPage'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;


                --StatusId
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'adLeadPhone'
                                     AND f.FldName = 'StatusId'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1    -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadPhone'
                                )        -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'StatusId'
                                )        -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Phones'
                                )        -- CategoryId - int
                               ,'Status' -- FKColDescrip - varchar(50)
                               );
                    END;


                --Update the CategoryId for the IsForeignPhone field on the adLeadPhone table
                UPDATE tf
                SET    tf.CategoryId = (
                                       SELECT CategoryId
                                       FROM   dbo.syFldCategories
                                       WHERE  EntityId = @LeadEntityId
                                              AND Descrip = 'Lead Phones'
                                       )
                FROM   dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE  t.TblName = 'adLeadPhone'
                       AND f.FldName = 'IsForeignPhone';

                --Update the CategoryId and FKColDescrip for the PhoneTypeId on the adLeadPhone table
                UPDATE tf
                SET    tf.CategoryId = (
                                       SELECT CategoryId
                                       FROM   dbo.syFldCategories
                                       WHERE  EntityId = @LeadEntityId
                                              AND Descrip = 'Lead Phones'
                                       )
                      ,tf.FKColDescrip = 'PhoneTypeDescrip'
                FROM   dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE  t.TblName = 'adLeadPhone'
                       AND f.FldName = 'PhoneTypeId';

                --Remove old category from the table
                UPDATE tf
                SET    tf.CategoryId = NULL
                FROM   dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE  t.TblName = 'adLeadPhone'
                       AND tf.CategoryId IS NOT NULL
                       AND tf.CategoryId <> (
                                            SELECT CategoryId
                                            FROM   dbo.syFldCategories
                                            WHERE  EntityId = @LeadEntityId
                                                   AND Descrip = 'Lead Phones'
                                            );


                IF ( @@ERROR > 0 )
                    BEGIN
                        SET @Error = @@ERROR;
                    END;
            END;


        -------------------------------------------------------------------------------------------------------
        --LEAD ADDRESSES
        -------------------------------------------------------------------------------------------------------
        IF @Error = 0
            BEGIN
                --Create a new category for LeadAddresses
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFldCategories
                              WHERE  EntityId = @LeadEntityId
                                     AND Descrip = 'Lead Addresses'
                              )
                    BEGIN
                        INSERT INTO dbo.syFldCategories (
                                                        EntityId
                                                       ,Descrip
                                                       ,CategoryId
                                                        )
                        VALUES ( @LeadEntityId    -- EntityId - smallint
                                ,'Lead Addresses' -- Descrip - varchar(50)
                                ,(
                                 SELECT MAX(CategoryId)
                                 FROM   dbo.syFldCategories
                                 ) + 1            -- CategoryId - int
                               );
                    END;

                --Add the IsMailingAddress field if it is missing
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFields
                              WHERE  FldName = 'IsMailingAddress'
                              )
                    BEGIN
                        INSERT INTO dbo.syFields (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                 )
                        VALUES ((
                                SELECT MAX(FldId)
                                FROM   dbo.syFields
                                ) + 1              -- FldId - int
                               ,'IsMailingAddress' -- FldName - varchar(200)
                               ,11                 -- FldTypeId - int
                               ,1                  -- FldLen - int
                               ,NULL               -- DDLId - int
                               ,0                  -- DerivedFld - bit
                               ,0                  -- SchlReq - bit
                               ,0                  -- LogChanges - bit
                               ,NULL               -- Mask - varchar(50)
                               );
                    END;

                --Add the caption for the IsMailingAddress field
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFldCaptions fc
                              INNER JOIN dbo.syFields f ON f.FldId = fc.FldId
                              WHERE  f.FldName = 'IsMailingAddress'
                              )
                    INSERT INTO dbo.syFldCaptions (
                                                  FldCapId
                                                 ,FldId
                                                 ,LangId
                                                 ,Caption
                                                 ,FldDescrip
                                                  )
                    VALUES ((
                            SELECT MAX(FldCapId)
                            FROM   dbo.syFldCaptions
                            ) + 1                 -- FldCapId - int
                           ,(
                            SELECT FldId
                            FROM   dbo.syFields
                            WHERE  FldName = 'IsMailingAddress'
                            )                     -- FldId - int
                           ,1                     -- LangId - tinyint
                           ,'Is Mailing Address?' -- Caption - varchar(100)
                           ,NULL                  -- FldDescrip - varchar(150)
                           );


                --Add new fields to Lead Addresses category
                -------------------------------------------
                --Add the IsMailingAddress field
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'adLeadAddresses'
                                     AND f.FldName = 'IsMailingAddress'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadAddresses'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsMailingAddress'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Addresses'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;

                --IsShowOnLeadPage
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'adLeadAddresses'
                                     AND f.FldName = 'IsShowOnLeadPage'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'adLeadAddresses'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsShowOnLeadPage'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @LeadEntityId
                                       AND Descrip = 'Lead Addresses'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;

                --All of the fields defined for the adLeadAddresses table metadata needs to be shown for the Lead Addresses category
                UPDATE tf
                SET    tf.CategoryId = (
                                       SELECT CategoryId
                                       FROM   dbo.syFldCategories
                                       WHERE  EntityId = @LeadEntityId
                                              AND Descrip = 'Lead Addresses'
                                       )
                FROM   dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE  t.TblName = 'adLeadAddresses';

                --Update the FKColDescrip for the lookup fields
                UPDATE tf
                SET    tf.FKColDescrip = 'StateDescrip'
                FROM   dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE  t.TblName = 'adLeadAddresses'
                       AND f.FldName = 'StateId';

                UPDATE tf
                SET    tf.FKColDescrip = 'CountryDescrip'
                FROM   dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE  t.TblName = 'adLeadAddresses'
                       AND f.FldName = 'CountryId';

                UPDATE tf
                SET    tf.FKColDescrip = 'CountyDescrip'
                FROM   dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE  t.TblName = 'adLeadAddresses'
                       AND f.FldName = 'CountyId';

                UPDATE tf
                SET    tf.FKColDescrip = 'AddressDescrip'
                FROM   dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE  t.TblName = 'adLeadAddresses'
                       AND f.FldName = 'AddressTypeId';

                --Remove any old categories from the table
                UPDATE tf
                SET    tf.CategoryId = NULL
                FROM   dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE  t.TblName = 'adLeadAddresses'
                       AND tf.CategoryId IS NOT NULL
                       AND tf.CategoryId <> (
                                            SELECT CategoryId
                                            FROM   dbo.syFldCategories
                                            WHERE  EntityId = @LeadEntityId
                                                   AND Descrip = 'Lead Addresses'
                                            );

                IF ( @@ERROR > 0 )
                    BEGIN
                        SET @Error = @@ERROR;
                    END;
            END;


        -------------------------------------------------------------------------------------------------------
        --STUDENT PHONES
        -------------------------------------------------------------------------------------------------------
        IF @Error = 0
            BEGIN
                --Create a new category for Student Phones
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFldCategories
                              WHERE  EntityId = @StudentEntityId
                                     AND Descrip = 'Student Phones'
                              )
                    BEGIN
                        INSERT INTO dbo.syFldCategories (
                                                        EntityId
                                                       ,Descrip
                                                       ,CategoryId
                                                        )
                        VALUES ( @StudentEntityId -- EntityId - smallint
                                ,'Student Phones' -- Descrip - varchar(50)
                                ,(
                                 SELECT MAX(CategoryId)
                                 FROM   dbo.syFldCategories
                                 ) + 1            -- CategoryId - int
                               );
                    END;

                --Create the metadata for the new fields for arStudentPhone
                -----------------------------------------------------------
                --Position
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'arStudentPhone'
                                     AND f.FldName = 'Position'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudentPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'Position'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;


                --Extension
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'arStudentPhone'
                                     AND f.FldName = 'Extension'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudentPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'Extension'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;


                --IsForeignPhone
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'arStudentPhone'
                                     AND f.FldName = 'IsForeignPhone'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudentPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsForeignPhone'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;

                --IsBest
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'arStudentPhone'
                                     AND f.FldName = 'IsBest'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudentPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsBest'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;


                --IsShowOnLeadPage
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'arStudentPhone'
                                     AND f.FldName = 'IsShowOnLeadPage'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudentPhone'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsShowOnLeadPage'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Phones'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;


                --Update the CategoryId for the PhoneTypeId field on the arStudentTable table
                UPDATE tf
                SET    tf.CategoryId = (
                                       SELECT CategoryId
                                       FROM   dbo.syFldCategories
                                       WHERE  EntityId = @StudentEntityId
                                              AND Descrip = 'Student Phones'
                                       )
                FROM   dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE  t.TblName = 'arStudentPhone'
                       AND f.FldName = 'PhoneTypeId';


                --Update the CategoryId for the Phone field on the arStudentTable table
                UPDATE tf
                SET    tf.CategoryId = (
                                       SELECT CategoryId
                                       FROM   dbo.syFldCategories
                                       WHERE  EntityId = @StudentEntityId
                                              AND Descrip = 'Student Phones'
                                       )
                FROM   dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE  t.TblName = 'arStudentPhone'
                       AND f.FldName = 'Phone';

                --Update the CategoryId for the StatusId field on the arStudentTable table
                UPDATE tf
                SET    tf.CategoryId = (
                                       SELECT CategoryId
                                       FROM   dbo.syFldCategories
                                       WHERE  EntityId = @StudentEntityId
                                              AND Descrip = 'Student Phones'
                                       )
                FROM   dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE  t.TblName = 'arStudentPhone'
                       AND f.FldName = 'StatusId';

                --Remove any old categories from the table
                UPDATE tf
                SET    tf.CategoryId = NULL
                FROM   dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE  t.TblName = 'arStudentPhone'
                       AND tf.CategoryId IS NOT NULL
                       AND tf.CategoryId <> (
                                            SELECT CategoryId
                                            FROM   dbo.syFldCategories
                                            WHERE  EntityId = @StudentEntityId
                                                   AND Descrip = 'Student Phones'
                                            );


                IF ( @@ERROR > 0 )
                    BEGIN
                        SET @Error = @@ERROR;
                    END;
            END;


        -------------------------------------------------------------------------------------------------------
        --STUDENT ADDRESSES
        -------------------------------------------------------------------------------------------------------
        IF @Error = 0
            BEGIN
                --Create a new category for Student Addresses
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFldCategories
                              WHERE  EntityId = @StudentEntityId
                                     AND Descrip = 'Student Addresses'
                              )
                    BEGIN
                        INSERT INTO dbo.syFldCategories (
                                                        EntityId
                                                       ,Descrip
                                                       ,CategoryId
                                                        )
                        VALUES ( @StudentEntityId    -- EntityId - smallint
                                ,'Student Addresses' -- Descrip - varchar(50)
                                ,(
                                 SELECT MAX(CategoryId)
                                 FROM   dbo.syFldCategories
                                 ) + 1               -- CategoryId - int
                               );
                    END;

                --Create the ForeignCountyStr field if it is missing
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFields
                              WHERE  FldName = 'ForeignCountyStr'
                              )
                    BEGIN
                        INSERT INTO dbo.syFields (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                 )
                        VALUES ((
                                SELECT MAX(FldId)
                                FROM   syFields
                                ) + 1              -- FldId - int
                               ,'ForeignCountyStr' -- FldName - varchar(200)
                               ,200                -- FldTypeId - int
                               ,100                -- FldLen - int
                               ,NULL               -- DDLId - int
                               ,0                  -- DerivedFld - bit
                               ,0                  -- SchlReq - bit
                               ,0                  -- LogChanges - bit
                               ,NULL               -- Mask - varchar(50)
                               );
                    END;


                --Create the ForeignCountryStr field if it is missing
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFields
                              WHERE  FldName = 'ForeignCountryStr'
                              )
                    BEGIN
                        INSERT INTO dbo.syFields (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                 )
                        VALUES ((
                                SELECT MAX(FldId)
                                FROM   syFields
                                ) + 1               -- FldId - int
                               ,'ForeignCountryStr' -- FldName - varchar(200)
                               ,200                 -- FldTypeId - int
                               ,100                 -- FldLen - int
                               ,NULL                -- DDLId - int
                               ,0                   -- DerivedFld - bit
                               ,0                   -- SchlReq - bit
                               ,0                   -- LogChanges - bit
                               ,NULL                -- Mask - varchar(50)
                               );
                    END;

                --Create the AddressApto field if it is missing
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syFields
                              WHERE  FldName = 'AddressApto'
                              )
                    BEGIN
                        INSERT INTO dbo.syFields (
                                                 FldId
                                                ,FldName
                                                ,FldTypeId
                                                ,FldLen
                                                ,DDLId
                                                ,DerivedFld
                                                ,SchlReq
                                                ,LogChanges
                                                ,Mask
                                                 )
                        VALUES ((
                                SELECT MAX(FldId)
                                FROM   syFields
                                ) + 1         -- FldId - int
                               ,'AddressApto' -- FldName - varchar(200)
                               ,200           -- FldTypeId - int
                               ,20            -- FldLen - int
                               ,NULL          -- DDLId - int
                               ,0             -- DerivedFld - bit
                               ,0             -- SchlReq - bit
                               ,0             -- LogChanges - bit
                               ,NULL          -- Mask - varchar(50)
                               );
                    END;

                --Create the metadata for the new fields for arStudAddresses
                -----------------------------------------------------------
                --IsMailingAddress
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'arStudAddresses'
                                     AND f.FldName = 'IsMailingAddress'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudAddresses'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsMailingAddress'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Addresses'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;


                --IsShowOnLeadPage
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'arStudAddresses'
                                     AND f.FldName = 'IsShowOnLeadPage'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudAddresses'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsShowOnLeadPage'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Addresses'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;


                --IsInternational
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'arStudAddresses'
                                     AND f.FldName = 'IsInternational'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudAddresses'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'IsInternational'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Addresses'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;


                --CountyId
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'arStudAddresses'
                                     AND f.FldName = 'CountyId'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1           -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudAddresses'
                                )               -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'CountyId'
                                )               -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Addresses'
                                )               -- CategoryId - int
                               ,'CountyDescrip' -- FKColDescrip - varchar(50)
                               );
                    END;


                --ForeignCountyStr
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'arStudAddresses'
                                     AND f.FldName = 'ForeignCountyStr'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudAddresses'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'ForeignCountyStr'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Addresses'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;

                --ForeignCountryStr
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'arStudAddresses'
                                     AND f.FldName = 'ForeignCountryStr'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudAddresses'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'ForeignCountryStr'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Addresses'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;

                --AddressApto
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syTables t
                              INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                              INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                              WHERE  t.TblName = 'arStudAddresses'
                                     AND f.FldName = 'AddressApto'
                              )
                    BEGIN
                        INSERT INTO dbo.syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                        VALUES ((
                                SELECT MAX(TblFldsId)
                                FROM   dbo.syTblFlds
                                ) + 1 -- TblFldsId - int
                               ,(
                                SELECT TblId
                                FROM   dbo.syTables
                                WHERE  TblName = 'arStudAddresses'
                                )     -- TblId - int
                               ,(
                                SELECT FldId
                                FROM   dbo.syFields
                                WHERE  FldName = 'AddressApto'
                                )     -- FldId - int
                               ,(
                                SELECT CategoryId
                                FROM   dbo.syFldCategories
                                WHERE  EntityId = @StudentEntityId
                                       AND Descrip = 'Student Addresses'
                                )     -- CategoryId - int
                               ,NULL  -- FKColDescrip - varchar(50)
                               );
                    END;


                --Update the CategoryId for the new fields on the arStudAddresses table
                UPDATE tf
                SET    tf.CategoryId = (
                                       SELECT CategoryId
                                       FROM   dbo.syFldCategories
                                       WHERE  EntityId = @StudentEntityId
                                              AND Descrip = 'Student Addresses'
                                       )
                FROM   dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE  t.TblName = 'arStudAddresses'
                       AND f.FldName IN ( 'StatusId', 'Address1', 'Address2', 'City', 'StateId', 'Zip', 'CountryId', 'AddressTypeId', '' );


                --Create a record for the arStudAddresses table and CountyId field in syRptAdhocRelations
                --If this is not done the value for the CouuntyId field will be GUID instead of the CountyDescrip
                IF NOT EXISTS (
                              SELECT *
                              FROM   dbo.syRptAdhocRelations
                              WHERE  FkTable = 'arStudAddresses'
                                     AND FkColumn = 'CountyId'
                              )
                    BEGIN
                        INSERT INTO dbo.syRptAdhocRelations (
                                                            FkTable
                                                           ,FkColumn
                                                           ,PkTable
                                                           ,PkColumn
                                                            )
                        VALUES ( N'arStudAddresses' -- FkTable - nvarchar(100)
                                ,N'CountyId'        -- FkColumn - nvarchar(100)
                                ,N'adCounties'      -- PkTable - nvarchar(100)
                                ,N'CountyId'        -- PkColumn - nvarchar(100)
                               );

                    END;


                --Remove any old categories from the table
                UPDATE tf
                SET    tf.CategoryId = NULL
                FROM   dbo.syTables t
                INNER JOIN dbo.syTblFlds tf ON tf.TblId = t.TblId
                INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
                WHERE  t.TblName = 'arStudAddresses'
                       AND tf.CategoryId IS NOT NULL
                       AND tf.CategoryId <> (
                                            SELECT CategoryId
                                            FROM   dbo.syFldCategories
                                            WHERE  EntityId = @StudentEntityId
                                                   AND Descrip = 'Student Addresses'
                                            );

                IF ( @@ERROR > 0 )
                    BEGIN
                        SET @Error = @@ERROR;
                    END;
            END;


        -------------------------------------------------------------------------------------------------------
        --REMOVE THE OLD FIELDS FROM THE GENERAL INFORMATION CATEGORY FOR LEADS
        -------------------------------------------------------------------------------------------------------
        UPDATE tf
        SET    tf.CategoryId = NULL
        FROM   dbo.syFldCategories fc
        INNER JOIN dbo.syTblFlds tf ON tf.CategoryId = fc.CategoryId
        INNER JOIN dbo.syTables t ON t.TblId = tf.TblId
        INNER JOIN dbo.syFields f ON f.FldId = tf.FldId
        WHERE  fc.EntityId = 395
               AND fc.Descrip = 'General Information'
               AND t.TblName = 'adLeadsView'
               AND f.FldName NOT IN ( 'Email_Best', '[Email]' );




    END TRY
    BEGIN CATCH
        DECLARE @msg NVARCHAR(MAX);
        DECLARE @severity INT;
        DECLARE @state INT;
        SELECT @msg = ERROR_MESSAGE()
              ,@severity = ERROR_SEVERITY()
              ,@state = ERROR_STATE();
        RAISERROR(@msg, @severity, @state);
        SET @Error = 1;
    END CATCH;

    IF ( @Error = 0 )
        BEGIN
            COMMIT TRANSACTION AdhocReports;
        END;
    ELSE
        BEGIN
            ROLLBACK TRANSACTION AdhocReports;
        END;
END;
GO
--=================================================================================================
-- AD-4468 Create Title IV SAP (FA SAP) statuses
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION titleIvSap;
BEGIN TRY
    DECLARE @CreatedById UNIQUEIDENTIFIER;
    DECLARE @CreatedDate DATETIME;
    DECLARE @StatusId UNIQUEIDENTIFIER;

    SET @CreatedById = (
                       SELECT   TOP 1 UserId
                       FROM     dbo.syUsers
                       WHERE    IsAdvantageSuperUser = 1
                                AND (
                                    FullName = 'support'
                                    OR FullName = 'sa'
                                    )
                       ORDER BY FullName ASC
                       );
    SET @CreatedDate = GETDATE();
    SET @StatusId = (
                    SELECT TOP 1 StatusId
                    FROM   dbo.syStatuses
                    WHERE  StatusCode = 'A'
                    );

    IF NOT EXISTS (
                  SELECT *
                  FROM   syTitleIVSapStatus
                  WHERE  Code = 'Passed'
                  )
        BEGIN
            INSERT INTO syTitleIVSapStatus
            VALUES ( 'Passed', 'Title IV Passed', @CreatedById, @CreatedDate, NULL, NULL, @StatusId );
        END;

    IF NOT EXISTS (
                  SELECT *
                  FROM   syTitleIVSapStatus
                  WHERE  Code = 'Warning'
                  )
        BEGIN
            INSERT INTO syTitleIVSapStatus
            VALUES ( 'Warning', 'Title IV Warning', @CreatedById, @CreatedDate, NULL, NULL, @StatusId );
        END;

    IF NOT EXISTS (
                  SELECT *
                  FROM   syTitleIVSapStatus
                  WHERE  Code = 'Ineligible'
                  )
        BEGIN
            INSERT INTO syTitleIVSapStatus
            VALUES ( 'Ineligible', 'Title IV Ineligible', @CreatedById, @CreatedDate, NULL, NULL, @StatusId );
        END;

    IF NOT EXISTS (
                  SELECT *
                  FROM   syTitleIVSapStatus
                  WHERE  Code = 'Probation'
                  )
        BEGIN
            INSERT INTO syTitleIVSapStatus
            VALUES ( 'Probation', 'Title IV Probation', @CreatedById, @CreatedDate, NULL, NULL, @StatusId );
        END;
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION titleIvSap;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION titleIvSap;
        PRINT 'Failed to Insert Create Title IV SAP (FA SAP) statuses';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION titleIvSap;
        PRINT 'Inserted Title IV SAP (FA SAP) statuses';
    END;
GO

--=================================================================================================
-- END -- AD-4468 Create Title IV SAP (FA SAP) statuses
--=================================================================================================

--=================================================================================================
-- START AD-586 Add Vendor field to Adhoc Reports
--=================================================================================================
DECLARE @Error AS INTEGER;
SET @Error = 0;

BEGIN TRANSACTION vendorAdhocReport;
BEGIN TRY
    IF @Error = 0
        BEGIN
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syFields
                          WHERE  FldName = 'Vendor'
                          )
                BEGIN
                    DECLARE @fldId INT;
                    SET @fldId = (
                                 SELECT MAX(FldId)
                                 FROM   syFields
                                 ) + 1;
                    INSERT INTO syFields (
                                         FldId
                                        ,FldName
                                        ,FldTypeId
                                        ,FldLen
                                        ,DDLId
                                        ,DerivedFld
                                        ,SchlReq
                                        ,LogChanges
                                        ,Mask
                                         )
                    VALUES ( @fldId   -- FldId - int
                            ,'Vendor' -- FldName - varchar(200)
                            ,200      -- FldTypeId - int
                            ,150      -- FldLen - int
                            ,NULL     -- DDLId - int
                            ,0        -- DerivedFld - bit
                            ,0        -- SchlReq - bit
                            ,0        -- LogChanges - bit
                            ,NULL     -- Mask - varchar(50)
                           );
                    IF ( @@ERROR > 0 )
                        BEGIN
                            SET @Error = @@ERROR;
                        END;
                    IF NOT EXISTS (
                                  SELECT 1
                                  FROM   syTblFlds
                                  WHERE  FldId = @fldId
                                  )
                        BEGIN
                            DECLARE @tblFldId INT;
                            DECLARE @tblId INT;
                            DECLARE @categoryId INT;
                            IF EXISTS (
                                      SELECT 1
                                      FROM   syFldCategories
                                      WHERE  Descrip = 'Advertisement'
                                             AND EntityId = 395
                                      )
                                BEGIN
                                    UPDATE syFldCategories
                                    SET    Descrip = 'Source'
                                    WHERE  Descrip = 'Advertisement'
                                           AND EntityId = 395;
                                END;
                            SET @categoryId = (
                                              SELECT CategoryId
                                              FROM   syFldCategories
                                              WHERE  Descrip = 'Source'
                                                     AND EntityId = 395
                                              );
                            SET @tblId = (
                                         SELECT TblId
                                         FROM   syTables
                                         WHERE  TblName = 'adleads'
                                         );
                            SET @tblFldId = (
                                            SELECT MAX(TblFldsId)
                                            FROM   syTblFlds
                                            ) + 1;
                            INSERT INTO syTblFlds (
                                                  TblFldsId
                                                 ,TblId
                                                 ,FldId
                                                 ,CategoryId
                                                 ,FKColDescrip
                                                  )
                            VALUES ( @tblFldId   -- TblFldsId - int
                                    ,@tblId      -- TblId - int
                                    ,@fldId      -- FldId - int
                                    ,@categoryId -- CategoryId - int
                                    ,NULL        -- FKColDescrip - varchar(50)
                                   );
                        END;
                    IF ( @@ERROR > 0 )
                        BEGIN
                            SET @Error = @@ERROR;
                        END;
                    IF NOT EXISTS (
                                  SELECT 1
                                  FROM   syFldCaptions
                                  WHERE  FldId = @fldId
                                  )
                        BEGIN
                            DECLARE @fldCaptionId INT;
                            SET @fldCaptionId = (
                                                SELECT MAX(FldCapId)
                                                FROM   syFldCaptions
                                                ) + 1;
                            INSERT INTO syFldCaptions (
                                                      FldCapId
                                                     ,FldId
                                                     ,LangId
                                                     ,Caption
                                                     ,FldDescrip
                                                      )
                            VALUES ( @fldCaptionId                                 -- FldCapId - int
                                    ,@fldId                                        -- FldId - int
                                    ,1                                             -- LangId - tinyint
                                    ,'Vendor'                                      -- Caption - varchar(100)
                                    ,'Vendor FullName from syusers of type Vendor' -- FldDescrip - varchar(150)
                                   );
                        END;
                    IF ( @@ERROR > 0 )
                        BEGIN
                            SET @Error = @@ERROR;
                        END;
                    IF NOT EXISTS (
                                  SELECT 1
                                  FROM   syFieldCalculation
                                  WHERE  FldId = @fldId
                                  )
                        BEGIN
                            INSERT INTO syFieldCalculation (
                                                           FldId
                                                          ,CalculationSql
                                                           )
                            VALUES ( @fldId                                                                             -- FldId - int
                                    ,'(SELECT FullName FROM syUsers WHERE syUsers.UserId = adLeads.VendorId) as Vendor' -- CalculationSql - varchar(8000)
                                   );
                        END;
                    IF ( @@ERROR > 0 )
                        BEGIN
                            SET @Error = @@ERROR;
                        END;
                END;
        END;
END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @Error = 1;
END CATCH;

IF ( @Error = 0 )
    BEGIN
        COMMIT TRANSACTION vendorAdhocReport;
    END;
ELSE
    BEGIN
        ROLLBACK TRANSACTION vendorAdhocReport;
    END;
GO
--=================================================================================================
-- END AD-586 Add Vendor field to Adhoc Reports
--=================================================================================================


--=================================================================================================
-- AD-3541 Display Updated Terms and Conditions on Host Page for advantage User
-- This will make sure all permissions have at least SY-Read
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION defaultPermissions;
BEGIN TRY
    IF EXISTS (
              SELECT Permission
              FROM   dbo.sySysRoles
              WHERE  Permission LIKE '%{"name": "SY","level": "none"}%'
              )
        BEGIN
            UPDATE dbo.sySysRoles
            SET    Permission = REPLACE(Permission, '{"name": "SY","level": "none"}', '{"name": "SY","level": "read"}')
            WHERE  Permission LIKE '%{"name": "SY","level": "none"}%';
        END;
END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @error = 1;
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION defaultPermissions;
        PRINT 'Failed to modify permissions to SY-read';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION defaultPermissions;
        PRINT 'Modified all permissions to have SY-read';
    END;
GO

--=================================================================================================
-- END -- AD-3541 Display Updated Terms and Conditions on Host Page for advantage User
--=================================================================================================

--=================================================================================================
-- START -- AD-596 Manage Users - Maintenance Page
--=================================================================================================
DECLARE @Error AS INTEGER;
SET @Error = 0;

BEGIN TRANSACTION Rename;
BEGIN TRY
    DECLARE @updateDate DATETIME;
    SET @updateDate = GETDATE();
    IF @Error = 0
        BEGIN
            IF EXISTS (
                      SELECT 1
                      FROM   syMenuItems
                      WHERE  MenuName = 'Manage Roles'
                      )
                BEGIN
                    UPDATE syMenuItems
                    SET    MenuName = 'Roles'
                          ,ModUser = 'support'
                          ,ModDate = @updateDate
                    WHERE  MenuName = 'Manage Roles';
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   syMenuItems
                      WHERE  MenuName = 'Manage Security'
                      )
                BEGIN
                    UPDATE syMenuItems
                    SET    MenuName = 'Security'
                          ,ModUser = 'support'
                          ,ModDate = @updateDate
                    WHERE  MenuName = 'Manage Security';
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF EXISTS (
                      SELECT 1
                      FROM   syMenuItems
                      WHERE  MenuName = 'Manage Users'
                      )
                BEGIN
                    UPDATE syMenuItems
                    SET    MenuName = 'Users'
                          ,ModUser = 'support'
                          ,ModDate = @updateDate
                    WHERE  MenuName = 'Manage Users';
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
        END;
END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @Error = 1;
END CATCH;

IF ( @Error = 0 )
    BEGIN
        COMMIT TRANSACTION Rename;
    END;
ELSE
    BEGIN
        ROLLBACK TRANSACTION Rename;
    END;
GO

--=================================================================================================
-- END -- AD-596 Manage Users - Maintenance Page
--=================================================================================================
--=================================================================================================
-- START -- AD-3706 Create NACCAS Report Menu in Advantage
--=================================================================================================
DECLARE @Error AS INTEGER;
SET @Error = 0;

BEGIN TRANSACTION addNewResource;
BEGIN TRY
    DECLARE @modDate DATETIME;
    SET @modDate = GETDATE();
    IF @Error = 0
        BEGIN
            -- insert into sy resources
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syResources
                          WHERE  ResourceID = 844
                                 AND Resource = 'NACCAS Reports'
                          )
                BEGIN
                    INSERT INTO syResources (
                                            ResourceID
                                           ,Resource
                                           ,ResourceTypeID
                                           ,ResourceURL
                                           ,SummListId
                                           ,ChildTypeId
                                           ,ModDate
                                           ,ModUser
                                           ,AllowSchlReqFlds
                                           ,MRUTypeId
                                           ,UsedIn
                                           ,TblFldsId
                                           ,DisplayName
                                            )
                    VALUES ( 844              -- ResourceID - smallint
                            ,'NACCAS Reports' -- Resource - varchar(200)
                            ,2                -- ResourceTypeID - tinyint
                            ,NULL             -- ResourceURL - varchar(100)
                            ,NULL             -- SummListId - smallint
                            ,5                -- ChildTypeId - tinyint
                            ,@modDate         -- ModDate - datetime
                            ,'support'        -- ModUser - varchar(50)
                            ,NULL             -- AllowSchlReqFlds - bit
                            ,NULL             -- MRUTypeId - smallint
                            ,975              -- UsedIn - int
                            ,NULL             -- TblFldsId - int
                            ,NULL             -- DisplayName - varchar(200)
                           );

                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syResources
                          WHERE  ResourceID = 845
                                 AND Resource = 'Preliminary Annual Report'
                          )
                BEGIN
                    INSERT INTO syResources (
                                            ResourceID
                                           ,Resource
                                           ,ResourceTypeID
                                           ,ResourceURL
                                           ,SummListId
                                           ,ChildTypeId
                                           ,ModDate
                                           ,ModUser
                                           ,AllowSchlReqFlds
                                           ,MRUTypeId
                                           ,UsedIn
                                           ,TblFldsId
                                           ,DisplayName
                                            )
                    VALUES ( 845                         -- ResourceID - smallint
                            ,'Preliminary Annual Report' -- Resource - varchar(200)
                            ,2                           -- ResourceTypeID - tinyint
                            ,NULL                        -- ResourceURL - varchar(100)
                            ,NULL                        -- SummListId - smallint
                            ,5                           -- ChildTypeId - tinyint
                            ,@modDate                    -- ModDate - datetime
                            ,'support'                   -- ModUser - varchar(50)
                            ,0                           -- AllowSchlReqFlds - bit
                            ,1                           -- MRUTypeId - smallint
                            ,975                         -- UsedIn - int
                            ,NULL                        -- TblFldsId - int
                            ,NULL                        -- DisplayName - varchar(200)
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syResources
                          WHERE  ResourceID = 846
                                 AND Resource = 'Fall Annual Report'
                          )
                BEGIN
                    INSERT INTO syResources (
                                            ResourceID
                                           ,Resource
                                           ,ResourceTypeID
                                           ,ResourceURL
                                           ,SummListId
                                           ,ChildTypeId
                                           ,ModDate
                                           ,ModUser
                                           ,AllowSchlReqFlds
                                           ,MRUTypeId
                                           ,UsedIn
                                           ,TblFldsId
                                           ,DisplayName
                                            )
                    VALUES ( 846                  -- ResourceID - smallint
                            ,'Fall Annual Report' -- Resource - varchar(200)
                            ,2                    -- ResourceTypeID - tinyint
                            ,NULL                 -- ResourceURL - varchar(100)
                            ,NULL                 -- SummListId - smallint
                            ,5                    -- ChildTypeId - tinyint
                            ,@modDate             -- ModDate - datetime
                            ,'support'            -- ModUser - varchar(50)
                            ,0                    -- AllowSchlReqFlds - bit
                            ,1                    -- MRUTypeId - smallint
                            ,975                  -- UsedIn - int
                            ,NULL                 -- TblFldsId - int
                            ,NULL                 -- DisplayName - varchar(200)
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syResources
                          WHERE  ResourceID = 847
                                 AND Resource = 'Supplemental Annual Report'
                          )
                BEGIN
                    INSERT INTO syResources (
                                            ResourceID
                                           ,Resource
                                           ,ResourceTypeID
                                           ,ResourceURL
                                           ,SummListId
                                           ,ChildTypeId
                                           ,ModDate
                                           ,ModUser
                                           ,AllowSchlReqFlds
                                           ,MRUTypeId
                                           ,UsedIn
                                           ,TblFldsId
                                           ,DisplayName
                                            )
                    VALUES ( 847                          -- ResourceID - smallint
                            ,'Supplemental Annual Report' -- Resource - varchar(200)
                            ,2                            -- ResourceTypeID - tinyint
                            ,NULL                         -- ResourceURL - varchar(100)
                            ,NULL                         -- SummListId - smallint
                            ,5                            -- ChildTypeId - tinyint
                            ,@modDate                     -- ModDate - datetime
                            ,'support'                    -- ModUser - varchar(50)
                            ,0                            -- AllowSchlReqFlds - bit
                            ,1                            -- MRUTypeId - smallint
                            ,975                          -- UsedIn - int
                            ,NULL                         -- TblFldsId - int
                            ,NULL                         -- DisplayName - varchar(200)
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syResources
                          WHERE  ResourceID = 848
                                 AND Resource = 'Disclosure Report'
                          )
                BEGIN
                    INSERT INTO syResources (
                                            ResourceID
                                           ,Resource
                                           ,ResourceTypeID
                                           ,ResourceURL
                                           ,SummListId
                                           ,ChildTypeId
                                           ,ModDate
                                           ,ModUser
                                           ,AllowSchlReqFlds
                                           ,MRUTypeId
                                           ,UsedIn
                                           ,TblFldsId
                                           ,DisplayName
                                            )
                    VALUES ( 848                 -- ResourceID - smallint
                            ,'Disclosure Report' -- Resource - varchar(200)
                            ,2                   -- ResourceTypeID - tinyint
                            ,NULL                -- ResourceURL - varchar(100)
                            ,NULL                -- SummListId - smallint
                            ,5                   -- ChildTypeId - tinyint
                            ,@modDate            -- ModDate - datetime
                            ,'support'           -- ModUser - varchar(50)
                            ,0                   -- AllowSchlReqFlds - bit
                            ,1                   -- MRUTypeId - smallint
                            ,975                 -- UsedIn - int
                            ,NULL                -- TblFldsId - int
                            ,NULL                -- DisplayName - varchar(200)
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syResources
                          WHERE  ResourceID = 849
                                 AND Resource = 'Instructions'
                          )
                BEGIN
                    INSERT INTO syResources (
                                            ResourceID
                                           ,Resource
                                           ,ResourceTypeID
                                           ,ResourceURL
                                           ,SummListId
                                           ,ChildTypeId
                                           ,ModDate
                                           ,ModUser
                                           ,AllowSchlReqFlds
                                           ,MRUTypeId
                                           ,UsedIn
                                           ,TblFldsId
                                           ,DisplayName
                                            )
                    VALUES ( 849                     -- ResourceID - smallint
                            ,'Instructions'          -- Resource - varchar(200)
                            ,5                       -- ResourceTypeID - tinyint
                            ,'~/sy/ParamReport.aspx' -- ResourceURL - varchar(100)
                            ,NULL                    -- SummListId - smallint
                            ,5                       -- ChildTypeId - tinyint
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,0                       -- AllowSchlReqFlds - bit
                            ,1                       -- MRUTypeId - smallint
                            ,975                     -- UsedIn - int
                            ,NULL                    -- TblFldsId - int
                            ,NULL                    -- DisplayName - varchar(200)
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syResources
                          WHERE  ResourceID = 850
                                 AND Resource = 'Enrollment Count'
                          )
                BEGIN
                    INSERT INTO syResources (
                                            ResourceID
                                           ,Resource
                                           ,ResourceTypeID
                                           ,ResourceURL
                                           ,SummListId
                                           ,ChildTypeId
                                           ,ModDate
                                           ,ModUser
                                           ,AllowSchlReqFlds
                                           ,MRUTypeId
                                           ,UsedIn
                                           ,TblFldsId
                                           ,DisplayName
                                            )
                    VALUES ( 850                     -- ResourceID - smallint
                            ,'Enrollment Count'      -- Resource - varchar(200)
                            ,5                       -- ResourceTypeID - tinyint
                            ,'~/sy/ParamReport.aspx' -- ResourceURL - varchar(100)
                            ,NULL                    -- SummListId - smallint
                            ,5                       -- ChildTypeId - tinyint
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,0                       -- AllowSchlReqFlds - bit
                            ,1                       -- MRUTypeId - smallint
                            ,975                     -- UsedIn - int
                            ,NULL                    -- TblFldsId - int
                            ,NULL                    -- DisplayName - varchar(200)
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syResources
                          WHERE  ResourceID = 851
                                 AND Resource = 'Cohort Grid'
                          )
                BEGIN
                    INSERT INTO syResources (
                                            ResourceID
                                           ,Resource
                                           ,ResourceTypeID
                                           ,ResourceURL
                                           ,SummListId
                                           ,ChildTypeId
                                           ,ModDate
                                           ,ModUser
                                           ,AllowSchlReqFlds
                                           ,MRUTypeId
                                           ,UsedIn
                                           ,TblFldsId
                                           ,DisplayName
                                            )
                    VALUES ( 851                     -- ResourceID - smallint
                            ,'Cohort Grid'           -- Resource - varchar(200)
                            ,5                       -- ResourceTypeID - tinyint
                            ,'~/sy/ParamReport.aspx' -- ResourceURL - varchar(100)
                            ,NULL                    -- SummListId - smallint
                            ,5                       -- ChildTypeId - tinyint
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,0                       -- AllowSchlReqFlds - bit
                            ,1                       -- MRUTypeId - smallint
                            ,975                     -- UsedIn - int
                            ,NULL                    -- TblFldsId - int
                            ,NULL                    -- DisplayName - varchar(200)
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syResources
                          WHERE  ResourceID = 852
                                 AND Resource = 'Summary'
                          )
                BEGIN
                    INSERT INTO syResources (
                                            ResourceID
                                           ,Resource
                                           ,ResourceTypeID
                                           ,ResourceURL
                                           ,SummListId
                                           ,ChildTypeId
                                           ,ModDate
                                           ,ModUser
                                           ,AllowSchlReqFlds
                                           ,MRUTypeId
                                           ,UsedIn
                                           ,TblFldsId
                                           ,DisplayName
                                            )
                    VALUES ( 852                     -- ResourceID - smallint
                            ,'Summary'               -- Resource - varchar(200)
                            ,5                       -- ResourceTypeID - tinyint
                            ,'~/sy/ParamReport.aspx' -- ResourceURL - varchar(100)
                            ,NULL                    -- SummListId - smallint
                            ,5                       -- ChildTypeId - tinyint
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,0                       -- AllowSchlReqFlds - bit
                            ,1                       -- MRUTypeId - smallint
                            ,975                     -- UsedIn - int
                            ,NULL                    -- TblFldsId - int
                            ,NULL                    -- DisplayName - varchar(200)
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syResources
                          WHERE  ResourceID = 853
                                 AND Resource = 'Instructions'
                          )
                BEGIN
                    INSERT INTO syResources (
                                            ResourceID
                                           ,Resource
                                           ,ResourceTypeID
                                           ,ResourceURL
                                           ,SummListId
                                           ,ChildTypeId
                                           ,ModDate
                                           ,ModUser
                                           ,AllowSchlReqFlds
                                           ,MRUTypeId
                                           ,UsedIn
                                           ,TblFldsId
                                           ,DisplayName
                                            )
                    VALUES ( 853                     -- ResourceID - smallint
                            ,'Instructions'          -- Resource - varchar(200)
                            ,5                       -- ResourceTypeID - tinyint
                            ,'~/sy/ParamReport.aspx' -- ResourceURL - varchar(100)
                            ,NULL                    -- SummListId - smallint
                            ,5                       -- ChildTypeId - tinyint
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,0                       -- AllowSchlReqFlds - bit
                            ,1                       -- MRUTypeId - smallint
                            ,975                     -- UsedIn - int
                            ,NULL                    -- TblFldsId - int
                            ,NULL                    -- DisplayName - varchar(200)
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syResources
                          WHERE  ResourceID = 854
                                 AND Resource = 'Enrollment Count'
                          )
                BEGIN
                    INSERT INTO syResources (
                                            ResourceID
                                           ,Resource
                                           ,ResourceTypeID
                                           ,ResourceURL
                                           ,SummListId
                                           ,ChildTypeId
                                           ,ModDate
                                           ,ModUser
                                           ,AllowSchlReqFlds
                                           ,MRUTypeId
                                           ,UsedIn
                                           ,TblFldsId
                                           ,DisplayName
                                            )
                    VALUES ( 854                     -- ResourceID - smallint
                            ,'Enrollment Count'      -- Resource - varchar(200)
                            ,5                       -- ResourceTypeID - tinyint
                            ,'~/sy/ParamReport.aspx' -- ResourceURL - varchar(100)
                            ,NULL                    -- SummListId - smallint
                            ,5                       -- ChildTypeId - tinyint
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,0                       -- AllowSchlReqFlds - bit
                            ,1                       -- MRUTypeId - smallint
                            ,975                     -- UsedIn - int
                            ,NULL                    -- TblFldsId - int
                            ,NULL                    -- DisplayName - varchar(200)
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syResources
                          WHERE  ResourceID = 855
                                 AND Resource = 'Cohort Grid-Exempted Student List Export'
                          )
                BEGIN
                    INSERT INTO syResources (
                                            ResourceID
                                           ,Resource
                                           ,ResourceTypeID
                                           ,ResourceURL
                                           ,SummListId
                                           ,ChildTypeId
                                           ,ModDate
                                           ,ModUser
                                           ,AllowSchlReqFlds
                                           ,MRUTypeId
                                           ,UsedIn
                                           ,TblFldsId
                                           ,DisplayName
                                            )
                    VALUES ( 855                                        -- ResourceID - smallint
                            ,'Cohort Grid-Exempted Student List Export' -- Resource - varchar(200)
                            ,5                                          -- ResourceTypeID - tinyint
                            ,'~/sy/ParamReport.aspx'                    -- ResourceURL - varchar(100)
                            ,NULL                                       -- SummListId - smallint
                            ,5                                          -- ChildTypeId - tinyint
                            ,@modDate                                   -- ModDate - datetime
                            ,'support'                                  -- ModUser - varchar(50)
                            ,0                                          -- AllowSchlReqFlds - bit
                            ,1                                          -- MRUTypeId - smallint
                            ,975                                        -- UsedIn - int
                            ,NULL                                       -- TblFldsId - int
                            ,NULL                                       -- DisplayName - varchar(200)
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syResources
                          WHERE  ResourceID = 856
                                 AND Resource = 'Summary'
                          )
                BEGIN
                    INSERT INTO syResources (
                                            ResourceID
                                           ,Resource
                                           ,ResourceTypeID
                                           ,ResourceURL
                                           ,SummListId
                                           ,ChildTypeId
                                           ,ModDate
                                           ,ModUser
                                           ,AllowSchlReqFlds
                                           ,MRUTypeId
                                           ,UsedIn
                                           ,TblFldsId
                                           ,DisplayName
                                            )
                    VALUES ( 856                     -- ResourceID - smallint
                            ,'Summary'               -- Resource - varchar(200)
                            ,5                       -- ResourceTypeID - tinyint
                            ,'~/sy/ParamReport.aspx' -- ResourceURL - varchar(100)
                            ,NULL                    -- SummListId - smallint
                            ,5                       -- ChildTypeId - tinyint
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,0                       -- AllowSchlReqFlds - bit
                            ,1                       -- MRUTypeId - smallint
                            ,975                     -- UsedIn - int
                            ,NULL                    -- TblFldsId - int
                            ,NULL                    -- DisplayName - varchar(200)
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syResources
                          WHERE  ResourceID = 857
                                 AND Resource = 'Instructions'
                          )
                BEGIN
                    INSERT INTO syResources (
                                            ResourceID
                                           ,Resource
                                           ,ResourceTypeID
                                           ,ResourceURL
                                           ,SummListId
                                           ,ChildTypeId
                                           ,ModDate
                                           ,ModUser
                                           ,AllowSchlReqFlds
                                           ,MRUTypeId
                                           ,UsedIn
                                           ,TblFldsId
                                           ,DisplayName
                                            )
                    VALUES ( 857                     -- ResourceID - smallint
                            ,'Instructions'          -- Resource - varchar(200)
                            ,5                       -- ResourceTypeID - tinyint
                            ,'~/sy/ParamReport.aspx' -- ResourceURL - varchar(100)
                            ,NULL                    -- SummListId - smallint
                            ,5                       -- ChildTypeId - tinyint
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,0                       -- AllowSchlReqFlds - bit
                            ,1                       -- MRUTypeId - smallint
                            ,975                     -- UsedIn - int
                            ,NULL                    -- TblFldsId - int
                            ,NULL                    -- DisplayName - varchar(200)
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syResources
                          WHERE  ResourceID = 858
                                 AND Resource = 'Cohort Grid'
                          )
                BEGIN
                    INSERT INTO syResources (
                                            ResourceID
                                           ,Resource
                                           ,ResourceTypeID
                                           ,ResourceURL
                                           ,SummListId
                                           ,ChildTypeId
                                           ,ModDate
                                           ,ModUser
                                           ,AllowSchlReqFlds
                                           ,MRUTypeId
                                           ,UsedIn
                                           ,TblFldsId
                                           ,DisplayName
                                            )
                    VALUES ( 858                     -- ResourceID - smallint
                            ,'Cohort Grid'           -- Resource - varchar(200)
                            ,5                       -- ResourceTypeID - tinyint
                            ,'~/sy/ParamReport.aspx' -- ResourceURL - varchar(100)
                            ,NULL                    -- SummListId - smallint
                            ,5                       -- ChildTypeId - tinyint
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,0                       -- AllowSchlReqFlds - bit
                            ,1                       -- MRUTypeId - smallint
                            ,975                     -- UsedIn - int
                            ,NULL                    -- TblFldsId - int
                            ,NULL                    -- DisplayName - varchar(200)
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syResources
                          WHERE  ResourceID = 859
                                 AND Resource = 'Disclosure'
                          )
                BEGIN
                    INSERT INTO syResources (
                                            ResourceID
                                           ,Resource
                                           ,ResourceTypeID
                                           ,ResourceURL
                                           ,SummListId
                                           ,ChildTypeId
                                           ,ModDate
                                           ,ModUser
                                           ,AllowSchlReqFlds
                                           ,MRUTypeId
                                           ,UsedIn
                                           ,TblFldsId
                                           ,DisplayName
                                            )
                    VALUES ( 859                     -- ResourceID - smallint
                            ,'Disclosure'            -- Resource - varchar(200)
                            ,5                       -- ResourceTypeID - tinyint
                            ,'~/sy/ParamReport.aspx' -- ResourceURL - varchar(100)
                            ,NULL                    -- SummListId - smallint
                            ,5                       -- ChildTypeId - tinyint
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,0                       -- AllowSchlReqFlds - bit
                            ,1                       -- MRUTypeId - smallint
                            ,975                     -- UsedIn - int
                            ,NULL                    -- TblFldsId - int
                            ,NULL                    -- DisplayName - varchar(200)
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            -- insert into synavigation nodes for manage security page            
            DECLARE @hierarchyId UNIQUEIDENTIFIER;
            DECLARE @parentId UNIQUEIDENTIFIER;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syNavigationNodes
                          WHERE  ResourceId = 844
                          ) --NACCAS Reports
                BEGIN
                    SET @hierarchyId = NEWID();
                    SET @parentId = (
                                    SELECT HierarchyId
                                    FROM   syNavigationNodes
                                    WHERE  ResourceId = 689
                                           AND ParentId = (
                                                          SELECT HierarchyId
                                                          FROM   syNavigationNodes
                                                          WHERE  ResourceId = 26
                                                          )
                                    );
                    INSERT INTO syNavigationNodes (
                                                  HierarchyId
                                                 ,HierarchyIndex
                                                 ,ResourceId
                                                 ,ParentId
                                                 ,ModUser
                                                 ,ModDate
                                                 ,IsPopupWindow
                                                 ,IsShipped
                                                  )
                    VALUES ( @hierarchyId -- HierarchyId - uniqueidentifier
                            ,1            -- HierarchyIndex - smallint
                            ,844          -- ResourceId - smallint
                            ,@parentId    -- ParentId - uniqueidentifier
                            ,'support'    -- ModUser - varchar(50)
                            ,@modDate     -- ModDate - datetime
                            ,0            -- IsPopupWindow - bit
                            ,1            -- IsShipped - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syNavigationNodes
                          WHERE  ResourceId = 845
                          ) --Preliminary Annual Report
                BEGIN
                    SET @hierarchyId = NEWID();
                    SET @parentId = (
                                    SELECT HierarchyId
                                    FROM   syNavigationNodes
                                    WHERE  ResourceId = 689
                                           AND ParentId = (
                                                          SELECT HierarchyId
                                                          FROM   syNavigationNodes
                                                          WHERE  ResourceId = 26
                                                          )
                                    );
                    INSERT INTO syNavigationNodes (
                                                  HierarchyId
                                                 ,HierarchyIndex
                                                 ,ResourceId
                                                 ,ParentId
                                                 ,ModUser
                                                 ,ModDate
                                                 ,IsPopupWindow
                                                 ,IsShipped
                                                  )
                    VALUES ( @hierarchyId -- HierarchyId - uniqueidentifier
                            ,1            -- HierarchyIndex - smallint
                            ,845          -- ResourceId - smallint
                            ,@parentId    -- ParentId - uniqueidentifier
                            ,'support'    -- ModUser - varchar(50)
                            ,@modDate     -- ModDate - datetime
                            ,0            -- IsPopupWindow - bit
                            ,1            -- IsShipped - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syNavigationNodes
                          WHERE  ResourceId = 846
                          ) --Fall Annual Report
                BEGIN
                    SET @hierarchyId = NEWID();
                    SET @parentId = (
                                    SELECT HierarchyId
                                    FROM   syNavigationNodes
                                    WHERE  ResourceId = 689
                                           AND ParentId = (
                                                          SELECT HierarchyId
                                                          FROM   syNavigationNodes
                                                          WHERE  ResourceId = 26
                                                          )
                                    );
                    INSERT INTO syNavigationNodes (
                                                  HierarchyId
                                                 ,HierarchyIndex
                                                 ,ResourceId
                                                 ,ParentId
                                                 ,ModUser
                                                 ,ModDate
                                                 ,IsPopupWindow
                                                 ,IsShipped
                                                  )
                    VALUES ( @hierarchyId -- HierarchyId - uniqueidentifier
                            ,1            -- HierarchyIndex - smallint
                            ,846          -- ResourceId - smallint
                            ,@parentId    -- ParentId - uniqueidentifier
                            ,'support'    -- ModUser - varchar(50)
                            ,@modDate     -- ModDate - datetime
                            ,0            -- IsPopupWindow - bit
                            ,1            -- IsShipped - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syNavigationNodes
                          WHERE  ResourceId = 847
                          ) --Supplemental Annual Report
                BEGIN
                    SET @hierarchyId = NEWID();
                    SET @parentId = (
                                    SELECT HierarchyId
                                    FROM   syNavigationNodes
                                    WHERE  ResourceId = 689
                                           AND ParentId = (
                                                          SELECT HierarchyId
                                                          FROM   syNavigationNodes
                                                          WHERE  ResourceId = 26
                                                          )
                                    );
                    INSERT INTO syNavigationNodes (
                                                  HierarchyId
                                                 ,HierarchyIndex
                                                 ,ResourceId
                                                 ,ParentId
                                                 ,ModUser
                                                 ,ModDate
                                                 ,IsPopupWindow
                                                 ,IsShipped
                                                  )
                    VALUES ( @hierarchyId -- HierarchyId - uniqueidentifier
                            ,1            -- HierarchyIndex - smallint
                            ,847          -- ResourceId - smallint
                            ,@parentId    -- ParentId - uniqueidentifier
                            ,'support'    -- ModUser - varchar(50)
                            ,@modDate     -- ModDate - datetime
                            ,0            -- IsPopupWindow - bit
                            ,1            -- IsShipped - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syNavigationNodes
                          WHERE  ResourceId = 848
                          ) --Disclosure Report
                BEGIN
                    SET @hierarchyId = NEWID();
                    SET @parentId = (
                                    SELECT HierarchyId
                                    FROM   syNavigationNodes
                                    WHERE  ResourceId = 689
                                           AND ParentId = (
                                                          SELECT HierarchyId
                                                          FROM   syNavigationNodes
                                                          WHERE  ResourceId = 26
                                                          )
                                    );
                    INSERT INTO syNavigationNodes (
                                                  HierarchyId
                                                 ,HierarchyIndex
                                                 ,ResourceId
                                                 ,ParentId
                                                 ,ModUser
                                                 ,ModDate
                                                 ,IsPopupWindow
                                                 ,IsShipped
                                                  )
                    VALUES ( @hierarchyId -- HierarchyId - uniqueidentifier
                            ,1            -- HierarchyIndex - smallint
                            ,848          -- ResourceId - smallint
                            ,@parentId    -- ParentId - uniqueidentifier
                            ,'support'    -- ModUser - varchar(50)
                            ,@modDate     -- ModDate - datetime
                            ,0            -- IsPopupWindow - bit
                            ,1            -- IsShipped - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syNavigationNodes
                          WHERE  ResourceId = 849
                          ) --Instructions report for Preliminary Annual Report - NACCAS
                BEGIN
                    SET @hierarchyId = NEWID();
                    SET @parentId = (
                                    SELECT HierarchyId
                                    FROM   syNavigationNodes
                                    WHERE  ResourceId = 845
                                    );
                    INSERT INTO syNavigationNodes (
                                                  HierarchyId
                                                 ,HierarchyIndex
                                                 ,ResourceId
                                                 ,ParentId
                                                 ,ModUser
                                                 ,ModDate
                                                 ,IsPopupWindow
                                                 ,IsShipped
                                                  )
                    VALUES ( @hierarchyId -- HierarchyId - uniqueidentifier
                            ,3            -- HierarchyIndex - smallint
                            ,849          -- ResourceId - smallint
                            ,@parentId    -- ParentId - uniqueidentifier
                            ,'support'    -- ModUser - varchar(50)
                            ,@modDate     -- ModDate - datetime
                            ,0            -- IsPopupWindow - bit
                            ,1            -- IsShipped - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syNavigationNodes
                          WHERE  ResourceId = 850
                          ) --Enrollment Count report for Preliminary Annual Report - NACCAS
                BEGIN
                    SET @hierarchyId = NEWID();
                    SET @parentId = (
                                    SELECT HierarchyId
                                    FROM   syNavigationNodes
                                    WHERE  ResourceId = 845
                                    );
                    INSERT INTO syNavigationNodes (
                                                  HierarchyId
                                                 ,HierarchyIndex
                                                 ,ResourceId
                                                 ,ParentId
                                                 ,ModUser
                                                 ,ModDate
                                                 ,IsPopupWindow
                                                 ,IsShipped
                                                  )
                    VALUES ( @hierarchyId -- HierarchyId - uniqueidentifier
                            ,3            -- HierarchyIndex - smallint
                            ,850          -- ResourceId - smallint
                            ,@parentId    -- ParentId - uniqueidentifier
                            ,'support'    -- ModUser - varchar(50)
                            ,@modDate     -- ModDate - datetime
                            ,0            -- IsPopupWindow - bit
                            ,1            -- IsShipped - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syNavigationNodes
                          WHERE  ResourceId = 851
                          ) --Cohort Grid report for Preliminary Annual Report - NACCAS
                BEGIN
                    SET @hierarchyId = NEWID();
                    SET @parentId = (
                                    SELECT HierarchyId
                                    FROM   syNavigationNodes
                                    WHERE  ResourceId = 845
                                    );
                    INSERT INTO syNavigationNodes (
                                                  HierarchyId
                                                 ,HierarchyIndex
                                                 ,ResourceId
                                                 ,ParentId
                                                 ,ModUser
                                                 ,ModDate
                                                 ,IsPopupWindow
                                                 ,IsShipped
                                                  )
                    VALUES ( @hierarchyId -- HierarchyId - uniqueidentifier
                            ,3            -- HierarchyIndex - smallint
                            ,851          -- ResourceId - smallint
                            ,@parentId    -- ParentId - uniqueidentifier
                            ,'support'    -- ModUser - varchar(50)
                            ,@modDate     -- ModDate - datetime
                            ,0            -- IsPopupWindow - bit
                            ,1            -- IsShipped - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syNavigationNodes
                          WHERE  ResourceId = 852
                          ) --Summary report for Preliminary Annual Report - NACCAS
                BEGIN
                    SET @hierarchyId = NEWID();
                    SET @parentId = (
                                    SELECT HierarchyId
                                    FROM   syNavigationNodes
                                    WHERE  ResourceId = 845
                                    );
                    INSERT INTO syNavigationNodes (
                                                  HierarchyId
                                                 ,HierarchyIndex
                                                 ,ResourceId
                                                 ,ParentId
                                                 ,ModUser
                                                 ,ModDate
                                                 ,IsPopupWindow
                                                 ,IsShipped
                                                  )
                    VALUES ( @hierarchyId -- HierarchyId - uniqueidentifier
                            ,3            -- HierarchyIndex - smallint
                            ,852          -- ResourceId - smallint
                            ,@parentId    -- ParentId - uniqueidentifier
                            ,'support'    -- ModUser - varchar(50)
                            ,@modDate     -- ModDate - datetime
                            ,0            -- IsPopupWindow - bit
                            ,1            -- IsShipped - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syNavigationNodes
                          WHERE  ResourceId = 853
                          ) --Instructions report for Fall Annual Report - NACCAS
                BEGIN
                    SET @hierarchyId = NEWID();
                    SET @parentId = (
                                    SELECT HierarchyId
                                    FROM   syNavigationNodes
                                    WHERE  ResourceId = 846
                                    );
                    INSERT INTO syNavigationNodes (
                                                  HierarchyId
                                                 ,HierarchyIndex
                                                 ,ResourceId
                                                 ,ParentId
                                                 ,ModUser
                                                 ,ModDate
                                                 ,IsPopupWindow
                                                 ,IsShipped
                                                  )
                    VALUES ( @hierarchyId -- HierarchyId - uniqueidentifier
                            ,3            -- HierarchyIndex - smallint
                            ,853          -- ResourceId - smallint
                            ,@parentId    -- ParentId - uniqueidentifier
                            ,'support'    -- ModUser - varchar(50)
                            ,@modDate     -- ModDate - datetime
                            ,0            -- IsPopupWindow - bit
                            ,1            -- IsShipped - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syNavigationNodes
                          WHERE  ResourceId = 854
                          ) --Enrollment Count report for Fall Annual Report - NACCAS
                BEGIN
                    SET @hierarchyId = NEWID();
                    SET @parentId = (
                                    SELECT HierarchyId
                                    FROM   syNavigationNodes
                                    WHERE  ResourceId = 846
                                    );
                    INSERT INTO syNavigationNodes (
                                                  HierarchyId
                                                 ,HierarchyIndex
                                                 ,ResourceId
                                                 ,ParentId
                                                 ,ModUser
                                                 ,ModDate
                                                 ,IsPopupWindow
                                                 ,IsShipped
                                                  )
                    VALUES ( @hierarchyId -- HierarchyId - uniqueidentifier
                            ,3            -- HierarchyIndex - smallint
                            ,854          -- ResourceId - smallint
                            ,@parentId    -- ParentId - uniqueidentifier
                            ,'support'    -- ModUser - varchar(50)
                            ,@modDate     -- ModDate - datetime
                            ,0            -- IsPopupWindow - bit
                            ,1            -- IsShipped - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syNavigationNodes
                          WHERE  ResourceId = 855
                          ) --Cohort Grid-Exempted Student List Export report for Fall Annual Report - NACCAS
                BEGIN
                    SET @hierarchyId = NEWID();
                    SET @parentId = (
                                    SELECT HierarchyId
                                    FROM   syNavigationNodes
                                    WHERE  ResourceId = 846
                                    );
                    INSERT INTO syNavigationNodes (
                                                  HierarchyId
                                                 ,HierarchyIndex
                                                 ,ResourceId
                                                 ,ParentId
                                                 ,ModUser
                                                 ,ModDate
                                                 ,IsPopupWindow
                                                 ,IsShipped
                                                  )
                    VALUES ( @hierarchyId -- HierarchyId - uniqueidentifier
                            ,3            -- HierarchyIndex - smallint
                            ,855          -- ResourceId - smallint
                            ,@parentId    -- ParentId - uniqueidentifier
                            ,'support'    -- ModUser - varchar(50)
                            ,@modDate     -- ModDate - datetime
                            ,0            -- IsPopupWindow - bit
                            ,1            -- IsShipped - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syNavigationNodes
                          WHERE  ResourceId = 856
                          ) --Summary report for Fall Annual Report - NACCAS
                BEGIN
                    SET @hierarchyId = NEWID();
                    SET @parentId = (
                                    SELECT HierarchyId
                                    FROM   syNavigationNodes
                                    WHERE  ResourceId = 846
                                    );
                    INSERT INTO syNavigationNodes (
                                                  HierarchyId
                                                 ,HierarchyIndex
                                                 ,ResourceId
                                                 ,ParentId
                                                 ,ModUser
                                                 ,ModDate
                                                 ,IsPopupWindow
                                                 ,IsShipped
                                                  )
                    VALUES ( @hierarchyId -- HierarchyId - uniqueidentifier
                            ,3            -- HierarchyIndex - smallint
                            ,856          -- ResourceId - smallint
                            ,@parentId    -- ParentId - uniqueidentifier
                            ,'support'    -- ModUser - varchar(50)
                            ,@modDate     -- ModDate - datetime
                            ,0            -- IsPopupWindow - bit
                            ,1            -- IsShipped - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syNavigationNodes
                          WHERE  ResourceId = 857
                          ) --Instructions report for Supplemental Annual Report - NACCAS
                BEGIN
                    SET @hierarchyId = NEWID();
                    SET @parentId = (
                                    SELECT HierarchyId
                                    FROM   syNavigationNodes
                                    WHERE  ResourceId = 847
                                    );
                    INSERT INTO syNavigationNodes (
                                                  HierarchyId
                                                 ,HierarchyIndex
                                                 ,ResourceId
                                                 ,ParentId
                                                 ,ModUser
                                                 ,ModDate
                                                 ,IsPopupWindow
                                                 ,IsShipped
                                                  )
                    VALUES ( @hierarchyId -- HierarchyId - uniqueidentifier
                            ,3            -- HierarchyIndex - smallint
                            ,857          -- ResourceId - smallint
                            ,@parentId    -- ParentId - uniqueidentifier
                            ,'support'    -- ModUser - varchar(50)
                            ,@modDate     -- ModDate - datetime
                            ,0            -- IsPopupWindow - bit
                            ,1            -- IsShipped - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syNavigationNodes
                          WHERE  ResourceId = 858
                          ) --Cohort Grid report for Supplemental Annual Report - NACCAS
                BEGIN
                    SET @hierarchyId = NEWID();
                    SET @parentId = (
                                    SELECT HierarchyId
                                    FROM   syNavigationNodes
                                    WHERE  ResourceId = 847
                                    );
                    INSERT INTO syNavigationNodes (
                                                  HierarchyId
                                                 ,HierarchyIndex
                                                 ,ResourceId
                                                 ,ParentId
                                                 ,ModUser
                                                 ,ModDate
                                                 ,IsPopupWindow
                                                 ,IsShipped
                                                  )
                    VALUES ( @hierarchyId -- HierarchyId - uniqueidentifier
                            ,3            -- HierarchyIndex - smallint
                            ,858          -- ResourceId - smallint
                            ,@parentId    -- ParentId - uniqueidentifier
                            ,'support'    -- ModUser - varchar(50)
                            ,@modDate     -- ModDate - datetime
                            ,0            -- IsPopupWindow - bit
                            ,1            -- IsShipped - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syNavigationNodes
                          WHERE  ResourceId = 859
                          ) --Disclosure
                BEGIN
                    SET @hierarchyId = NEWID();
                    SET @parentId = (
                                    SELECT HierarchyId
                                    FROM   syNavigationNodes
                                    WHERE  ResourceId = 848
                                    );
                    INSERT INTO syNavigationNodes (
                                                  HierarchyId
                                                 ,HierarchyIndex
                                                 ,ResourceId
                                                 ,ParentId
                                                 ,ModUser
                                                 ,ModDate
                                                 ,IsPopupWindow
                                                 ,IsShipped
                                                  )
                    VALUES ( @hierarchyId -- HierarchyId - uniqueidentifier
                            ,3            -- HierarchyIndex - smallint
                            ,859          -- ResourceId - smallint
                            ,@parentId    -- ParentId - uniqueidentifier
                            ,'support'    -- ModUser - varchar(50)
                            ,@modDate     -- ModDate - datetime
                            ,0            -- IsPopupWindow - bit
                            ,1            -- IsShipped - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            -- insert into syMenuItems
            DECLARE @parentMenuId INT;
            DECLARE @ReportMenuParentId INT;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syMenuItems
                          WHERE  ResourceId = 844
                          )
                BEGIN
                    SET @hierarchyId = (
                                       SELECT HierarchyId
                                       FROM   syNavigationNodes
                                       WHERE  ResourceId = 844
                                       );
                    SET @ReportMenuParentId = (
                                              SELECT MenuItemId
                                              FROM   syMenuItems
                                              WHERE  MenuName = 'Reports'
                                                     AND MenuItemTypeId = 1
                                              );
                    INSERT INTO syMenuItems (
                                            MenuName
                                           ,DisplayName
                                           ,Url
                                           ,MenuItemTypeId
                                           ,ParentId
                                           ,DisplayOrder
                                           ,IsPopup
                                           ,ModDate
                                           ,ModUser
                                           ,IsActive
                                           ,ResourceId
                                           ,HierarchyId
                                           ,ModuleCode
                                           ,MRUType
                                           ,HideStatusBar
                                            )
                    VALUES ( 'NACCAS Reports'    -- MenuName - varchar(250)
                            ,'NACCAS Reports'    -- DisplayName - varchar(250)
                            ,NULL                -- Url - nvarchar(250)
                            ,2                   -- MenuItemTypeId - smallint
                            ,@ReportMenuParentId -- ParentId - int
                            ,400                 -- DisplayOrder - int
                            ,0                   -- IsPopup - bit
                            ,@modDate            -- ModDate - datetime
                            ,'support'           -- ModUser - varchar(50)
                            ,1                   -- IsActive - bit
                            ,844                 -- ResourceId - smallint
                            ,@hierarchyId        -- HierarchyId - uniqueidentifier
                            ,NULL                -- ModuleCode - varchar(5)
                            ,NULL                -- MRUType - int
                            ,NULL                -- HideStatusBar - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            SET @parentMenuId = (
                                SELECT MenuItemId
                                FROM   syMenuItems
                                WHERE  ResourceId = 844
                                );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syMenuItems
                          WHERE  ResourceId = 845
                          )
                BEGIN
                    SET @hierarchyId = (
                                       SELECT HierarchyId
                                       FROM   syNavigationNodes
                                       WHERE  ResourceId = 845
                                       );
                    INSERT INTO syMenuItems (
                                            MenuName
                                           ,DisplayName
                                           ,Url
                                           ,MenuItemTypeId
                                           ,ParentId
                                           ,DisplayOrder
                                           ,IsPopup
                                           ,ModDate
                                           ,ModUser
                                           ,IsActive
                                           ,ResourceId
                                           ,HierarchyId
                                           ,ModuleCode
                                           ,MRUType
                                           ,HideStatusBar
                                            )
                    VALUES ( 'Preliminary Annual Report' -- MenuName - varchar(250)
                            ,'Preliminary Annual Report' -- DisplayName - varchar(250)
                            ,NULL                        -- Url - nvarchar(250)
                            ,3                           -- MenuItemTypeId - smallint
                            ,@parentMenuId               -- ParentId - int
                            ,500                         -- DisplayOrder - int
                            ,0                           -- IsPopup - bit
                            ,@modDate                    -- ModDate - datetime
                            ,'support'                   -- ModUser - varchar(50)
                            ,1                           -- IsActive - bit
                            ,845                         -- ResourceId - smallint
                            ,@hierarchyId                -- HierarchyId - uniqueidentifier
                            ,NULL                        -- ModuleCode - varchar(5)
                            ,NULL                        -- MRUType - int
                            ,NULL                        -- HideStatusBar - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syMenuItems
                          WHERE  ResourceId = 846
                          )
                BEGIN
                    SET @hierarchyId = (
                                       SELECT HierarchyId
                                       FROM   syNavigationNodes
                                       WHERE  ResourceId = 846
                                       );
                    INSERT INTO syMenuItems (
                                            MenuName
                                           ,DisplayName
                                           ,Url
                                           ,MenuItemTypeId
                                           ,ParentId
                                           ,DisplayOrder
                                           ,IsPopup
                                           ,ModDate
                                           ,ModUser
                                           ,IsActive
                                           ,ResourceId
                                           ,HierarchyId
                                           ,ModuleCode
                                           ,MRUType
                                           ,HideStatusBar
                                            )
                    VALUES ( 'Fall Annual Report' -- MenuName - varchar(250)
                            ,'Fall Annual Report' -- DisplayName - varchar(250)
                            ,NULL                 -- Url - nvarchar(250)
                            ,3                    -- MenuItemTypeId - smallint
                            ,@parentMenuId        -- ParentId - int
                            ,500                  -- DisplayOrder - int
                            ,0                    -- IsPopup - bit
                            ,@modDate             -- ModDate - datetime
                            ,'support'            -- ModUser - varchar(50)
                            ,1                    -- IsActive - bit
                            ,846                  -- ResourceId - smallint
                            ,@hierarchyId         -- HierarchyId - uniqueidentifier
                            ,NULL                 -- ModuleCode - varchar(5)
                            ,NULL                 -- MRUType - int
                            ,NULL                 -- HideStatusBar - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syMenuItems
                          WHERE  ResourceId = 847
                          )
                BEGIN
                    SET @hierarchyId = (
                                       SELECT HierarchyId
                                       FROM   syNavigationNodes
                                       WHERE  ResourceId = 847
                                       );
                    INSERT INTO syMenuItems (
                                            MenuName
                                           ,DisplayName
                                           ,Url
                                           ,MenuItemTypeId
                                           ,ParentId
                                           ,DisplayOrder
                                           ,IsPopup
                                           ,ModDate
                                           ,ModUser
                                           ,IsActive
                                           ,ResourceId
                                           ,HierarchyId
                                           ,ModuleCode
                                           ,MRUType
                                           ,HideStatusBar
                                            )
                    VALUES ( 'Supplemental Annual Report' -- MenuName - varchar(250)
                            ,'Supplemental Annual Report' -- DisplayName - varchar(250)
                            ,NULL                         -- Url - nvarchar(250)
                            ,3                            -- MenuItemTypeId - smallint
                            ,@parentMenuId                -- ParentId - int
                            ,500                          -- DisplayOrder - int
                            ,0                            -- IsPopup - bit
                            ,@modDate                     -- ModDate - datetime
                            ,'support'                    -- ModUser - varchar(50)
                            ,1                            -- IsActive - bit
                            ,847                          -- ResourceId - smallint
                            ,@hierarchyId                 -- HierarchyId - uniqueidentifier
                            ,NULL                         -- ModuleCode - varchar(5)
                            ,NULL                         -- MRUType - int
                            ,NULL                         -- HideStatusBar - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syMenuItems
                          WHERE  ResourceId = 848
                          )
                BEGIN
                    SET @hierarchyId = (
                                       SELECT HierarchyId
                                       FROM   syNavigationNodes
                                       WHERE  ResourceId = 848
                                       );
                    INSERT INTO syMenuItems (
                                            MenuName
                                           ,DisplayName
                                           ,Url
                                           ,MenuItemTypeId
                                           ,ParentId
                                           ,DisplayOrder
                                           ,IsPopup
                                           ,ModDate
                                           ,ModUser
                                           ,IsActive
                                           ,ResourceId
                                           ,HierarchyId
                                           ,ModuleCode
                                           ,MRUType
                                           ,HideStatusBar
                                            )
                    VALUES ( 'Disclosure Report' -- MenuName - varchar(250)
                            ,'Disclosure Report' -- DisplayName - varchar(250)
                            ,NULL                -- Url - nvarchar(250)
                            ,3                   -- MenuItemTypeId - smallint
                            ,@parentMenuId       -- ParentId - int
                            ,500                 -- DisplayOrder - int
                            ,0                   -- IsPopup - bit
                            ,@modDate            -- ModDate - datetime
                            ,'support'           -- ModUser - varchar(50)
                            ,1                   -- IsActive - bit
                            ,848                 -- ResourceId - smallint
                            ,@hierarchyId        -- HierarchyId - uniqueidentifier
                            ,NULL                -- ModuleCode - varchar(5)
                            ,NULL                -- MRUType - int
                            ,NULL                -- HideStatusBar - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            SET @parentMenuId = (
                                SELECT MenuItemId
                                FROM   syMenuItems
                                WHERE  ResourceId = 845
                                );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syMenuItems
                          WHERE  ResourceId = 849
                          )
                BEGIN
                    SET @hierarchyId = (
                                       SELECT HierarchyId
                                       FROM   syNavigationNodes
                                       WHERE  ResourceId = 849
                                       );
                    INSERT INTO syMenuItems (
                                            MenuName
                                           ,DisplayName
                                           ,Url
                                           ,MenuItemTypeId
                                           ,ParentId
                                           ,DisplayOrder
                                           ,IsPopup
                                           ,ModDate
                                           ,ModUser
                                           ,IsActive
                                           ,ResourceId
                                           ,HierarchyId
                                           ,ModuleCode
                                           ,MRUType
                                           ,HideStatusBar
                                            )
                    VALUES ( 'Instructions'          -- MenuName - varchar(250)
                            ,'Instructions'          -- DisplayName - varchar(250)
                            ,N'/SY/ParamReport.aspx' -- Url - nvarchar(250)
                            ,4                       -- MenuItemTypeId - smallint
                            ,@parentMenuId           -- ParentId - int
                            ,100                     -- DisplayOrder - int
                            ,0                       -- IsPopup - bit
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,1                       -- IsActive - bit
                            ,849                     -- ResourceId - smallint
                            ,@hierarchyId            -- HierarchyId - uniqueidentifier
                            ,NULL                    -- ModuleCode - varchar(5)
                            ,NULL                    -- MRUType - int
                            ,NULL                    -- HideStatusBar - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syMenuItems
                          WHERE  ResourceId = 850
                          )
                BEGIN
                    SET @hierarchyId = (
                                       SELECT HierarchyId
                                       FROM   syNavigationNodes
                                       WHERE  ResourceId = 850
                                       );
                    INSERT INTO syMenuItems (
                                            MenuName
                                           ,DisplayName
                                           ,Url
                                           ,MenuItemTypeId
                                           ,ParentId
                                           ,DisplayOrder
                                           ,IsPopup
                                           ,ModDate
                                           ,ModUser
                                           ,IsActive
                                           ,ResourceId
                                           ,HierarchyId
                                           ,ModuleCode
                                           ,MRUType
                                           ,HideStatusBar
                                            )
                    VALUES ( 'Enrollment Count'      -- MenuName - varchar(250)
                            ,'Enrollment Count'      -- DisplayName - varchar(250)
                            ,N'/SY/ParamReport.aspx' -- Url - nvarchar(250)
                            ,4                       -- MenuItemTypeId - smallint
                            ,@parentMenuId           -- ParentId - int
                            ,100                     -- DisplayOrder - int
                            ,0                       -- IsPopup - bit
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,1                       -- IsActive - bit
                            ,850                     -- ResourceId - smallint
                            ,@hierarchyId            -- HierarchyId - uniqueidentifier
                            ,NULL                    -- ModuleCode - varchar(5)
                            ,NULL                    -- MRUType - int
                            ,NULL                    -- HideStatusBar - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syMenuItems
                          WHERE  ResourceId = 851
                          )
                BEGIN
                    SET @hierarchyId = (
                                       SELECT HierarchyId
                                       FROM   syNavigationNodes
                                       WHERE  ResourceId = 851
                                       );
                    INSERT INTO syMenuItems (
                                            MenuName
                                           ,DisplayName
                                           ,Url
                                           ,MenuItemTypeId
                                           ,ParentId
                                           ,DisplayOrder
                                           ,IsPopup
                                           ,ModDate
                                           ,ModUser
                                           ,IsActive
                                           ,ResourceId
                                           ,HierarchyId
                                           ,ModuleCode
                                           ,MRUType
                                           ,HideStatusBar
                                            )
                    VALUES ( 'Cohort Grid'           -- MenuName - varchar(250)
                            ,'Cohort Grid'           -- DisplayName - varchar(250)
                            ,N'/SY/ParamReport.aspx' -- Url - nvarchar(250)
                            ,4                       -- MenuItemTypeId - smallint
                            ,@parentMenuId           -- ParentId - int
                            ,100                     -- DisplayOrder - int
                            ,0                       -- IsPopup - bit
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,1                       -- IsActive - bit
                            ,851                     -- ResourceId - smallint
                            ,@hierarchyId            -- HierarchyId - uniqueidentifier
                            ,NULL                    -- ModuleCode - varchar(5)
                            ,NULL                    -- MRUType - int
                            ,NULL                    -- HideStatusBar - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syMenuItems
                          WHERE  ResourceId = 852
                          )
                BEGIN
                    SET @hierarchyId = (
                                       SELECT HierarchyId
                                       FROM   syNavigationNodes
                                       WHERE  ResourceId = 852
                                       );
                    INSERT INTO syMenuItems (
                                            MenuName
                                           ,DisplayName
                                           ,Url
                                           ,MenuItemTypeId
                                           ,ParentId
                                           ,DisplayOrder
                                           ,IsPopup
                                           ,ModDate
                                           ,ModUser
                                           ,IsActive
                                           ,ResourceId
                                           ,HierarchyId
                                           ,ModuleCode
                                           ,MRUType
                                           ,HideStatusBar
                                            )
                    VALUES ( 'Summary'               -- MenuName - varchar(250)
                            ,'Summary'               -- DisplayName - varchar(250)
                            ,N'/SY/ParamReport.aspx' -- Url - nvarchar(250)
                            ,4                       -- MenuItemTypeId - smallint
                            ,@parentMenuId           -- ParentId - int
                            ,100                     -- DisplayOrder - int
                            ,0                       -- IsPopup - bit
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,1                       -- IsActive - bit
                            ,852                     -- ResourceId - smallint
                            ,@hierarchyId            -- HierarchyId - uniqueidentifier
                            ,NULL                    -- ModuleCode - varchar(5)
                            ,NULL                    -- MRUType - int
                            ,NULL                    -- HideStatusBar - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            SET @parentMenuId = (
                                SELECT MenuItemId
                                FROM   syMenuItems
                                WHERE  ResourceId = 846
                                );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syMenuItems
                          WHERE  ResourceId = 853
                          )
                BEGIN
                    SET @hierarchyId = (
                                       SELECT HierarchyId
                                       FROM   syNavigationNodes
                                       WHERE  ResourceId = 853
                                       );
                    INSERT INTO syMenuItems (
                                            MenuName
                                           ,DisplayName
                                           ,Url
                                           ,MenuItemTypeId
                                           ,ParentId
                                           ,DisplayOrder
                                           ,IsPopup
                                           ,ModDate
                                           ,ModUser
                                           ,IsActive
                                           ,ResourceId
                                           ,HierarchyId
                                           ,ModuleCode
                                           ,MRUType
                                           ,HideStatusBar
                                            )
                    VALUES ( 'Instructions'          -- MenuName - varchar(250)
                            ,'Instructions'          -- DisplayName - varchar(250)
                            ,N'/SY/ParamReport.aspx' -- Url - nvarchar(250)
                            ,4                       -- MenuItemTypeId - smallint
                            ,@parentMenuId           -- ParentId - int
                            ,100                     -- DisplayOrder - int
                            ,0                       -- IsPopup - bit
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,1                       -- IsActive - bit
                            ,853                     -- ResourceId - smallint
                            ,@hierarchyId            -- HierarchyId - uniqueidentifier
                            ,NULL                    -- ModuleCode - varchar(5)
                            ,NULL                    -- MRUType - int
                            ,NULL                    -- HideStatusBar - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syMenuItems
                          WHERE  ResourceId = 854
                          )
                BEGIN
                    SET @hierarchyId = (
                                       SELECT HierarchyId
                                       FROM   syNavigationNodes
                                       WHERE  ResourceId = 854
                                       );
                    INSERT INTO syMenuItems (
                                            MenuName
                                           ,DisplayName
                                           ,Url
                                           ,MenuItemTypeId
                                           ,ParentId
                                           ,DisplayOrder
                                           ,IsPopup
                                           ,ModDate
                                           ,ModUser
                                           ,IsActive
                                           ,ResourceId
                                           ,HierarchyId
                                           ,ModuleCode
                                           ,MRUType
                                           ,HideStatusBar
                                            )
                    VALUES ( 'Enrollment Count'      -- MenuName - varchar(250)
                            ,'Enrollment Count'      -- DisplayName - varchar(250)
                            ,N'/SY/ParamReport.aspx' -- Url - nvarchar(250)
                            ,4                       -- MenuItemTypeId - smallint
                            ,@parentMenuId           -- ParentId - int
                            ,100                     -- DisplayOrder - int
                            ,0                       -- IsPopup - bit
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,1                       -- IsActive - bit
                            ,854                     -- ResourceId - smallint
                            ,@hierarchyId            -- HierarchyId - uniqueidentifier
                            ,NULL                    -- ModuleCode - varchar(5)
                            ,NULL                    -- MRUType - int
                            ,NULL                    -- HideStatusBar - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syMenuItems
                          WHERE  ResourceId = 855
                          )
                BEGIN
                    SET @hierarchyId = (
                                       SELECT HierarchyId
                                       FROM   syNavigationNodes
                                       WHERE  ResourceId = 855
                                       );
                    INSERT INTO syMenuItems (
                                            MenuName
                                           ,DisplayName
                                           ,Url
                                           ,MenuItemTypeId
                                           ,ParentId
                                           ,DisplayOrder
                                           ,IsPopup
                                           ,ModDate
                                           ,ModUser
                                           ,IsActive
                                           ,ResourceId
                                           ,HierarchyId
                                           ,ModuleCode
                                           ,MRUType
                                           ,HideStatusBar
                                            )
                    VALUES ( 'Cohort Grid-Exempted Student List Export' -- MenuName - varchar(250)
                            ,'Cohort Grid-Exempted Student List Export' -- DisplayName - varchar(250)
                            ,N'/SY/ParamReport.aspx'                    -- Url - nvarchar(250)
                            ,4                                          -- MenuItemTypeId - smallint
                            ,@parentMenuId                              -- ParentId - int
                            ,100                                        -- DisplayOrder - int
                            ,0                                          -- IsPopup - bit
                            ,@modDate                                   -- ModDate - datetime
                            ,'support'                                  -- ModUser - varchar(50)
                            ,1                                          -- IsActive - bit
                            ,855                                        -- ResourceId - smallint
                            ,@hierarchyId                               -- HierarchyId - uniqueidentifier
                            ,NULL                                       -- ModuleCode - varchar(5)
                            ,NULL                                       -- MRUType - int
                            ,NULL                                       -- HideStatusBar - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syMenuItems
                          WHERE  ResourceId = 856
                          )
                BEGIN
                    SET @hierarchyId = (
                                       SELECT HierarchyId
                                       FROM   syNavigationNodes
                                       WHERE  ResourceId = 856
                                       );
                    INSERT INTO syMenuItems (
                                            MenuName
                                           ,DisplayName
                                           ,Url
                                           ,MenuItemTypeId
                                           ,ParentId
                                           ,DisplayOrder
                                           ,IsPopup
                                           ,ModDate
                                           ,ModUser
                                           ,IsActive
                                           ,ResourceId
                                           ,HierarchyId
                                           ,ModuleCode
                                           ,MRUType
                                           ,HideStatusBar
                                            )
                    VALUES ( 'Summary'               -- MenuName - varchar(250)
                            ,'Summary'               -- DisplayName - varchar(250)
                            ,N'/SY/ParamReport.aspx' -- Url - nvarchar(250)
                            ,4                       -- MenuItemTypeId - smallint
                            ,@parentMenuId           -- ParentId - int
                            ,100                     -- DisplayOrder - int
                            ,0                       -- IsPopup - bit
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,1                       -- IsActive - bit
                            ,856                     -- ResourceId - smallint
                            ,@hierarchyId            -- HierarchyId - uniqueidentifier
                            ,NULL                    -- ModuleCode - varchar(5)
                            ,NULL                    -- MRUType - int
                            ,NULL                    -- HideStatusBar - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            SET @parentMenuId = (
                                SELECT MenuItemId
                                FROM   syMenuItems
                                WHERE  ResourceId = 847
                                );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syMenuItems
                          WHERE  ResourceId = 857
                          )
                BEGIN
                    SET @hierarchyId = (
                                       SELECT HierarchyId
                                       FROM   syNavigationNodes
                                       WHERE  ResourceId = 857
                                       );
                    INSERT INTO syMenuItems (
                                            MenuName
                                           ,DisplayName
                                           ,Url
                                           ,MenuItemTypeId
                                           ,ParentId
                                           ,DisplayOrder
                                           ,IsPopup
                                           ,ModDate
                                           ,ModUser
                                           ,IsActive
                                           ,ResourceId
                                           ,HierarchyId
                                           ,ModuleCode
                                           ,MRUType
                                           ,HideStatusBar
                                            )
                    VALUES ( 'Instructions'          -- MenuName - varchar(250)
                            ,'Instructions'          -- DisplayName - varchar(250)
                            ,N'/SY/ParamReport.aspx' -- Url - nvarchar(250)
                            ,4                       -- MenuItemTypeId - smallint
                            ,@parentMenuId           -- ParentId - int
                            ,100                     -- DisplayOrder - int
                            ,0                       -- IsPopup - bit
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,1                       -- IsActive - bit
                            ,857                     -- ResourceId - smallint
                            ,@hierarchyId            -- HierarchyId - uniqueidentifier
                            ,NULL                    -- ModuleCode - varchar(5)
                            ,NULL                    -- MRUType - int
                            ,NULL                    -- HideStatusBar - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syMenuItems
                          WHERE  ResourceId = 858
                          )
                BEGIN
                    SET @hierarchyId = (
                                       SELECT HierarchyId
                                       FROM   syNavigationNodes
                                       WHERE  ResourceId = 858
                                       );
                    INSERT INTO syMenuItems (
                                            MenuName
                                           ,DisplayName
                                           ,Url
                                           ,MenuItemTypeId
                                           ,ParentId
                                           ,DisplayOrder
                                           ,IsPopup
                                           ,ModDate
                                           ,ModUser
                                           ,IsActive
                                           ,ResourceId
                                           ,HierarchyId
                                           ,ModuleCode
                                           ,MRUType
                                           ,HideStatusBar
                                            )
                    VALUES ( 'Cohort Grid'           -- MenuName - varchar(250)
                            ,'Cohort Grid'           -- DisplayName - varchar(250)
                            ,N'/SY/ParamReport.aspx' -- Url - nvarchar(250)
                            ,4                       -- MenuItemTypeId - smallint
                            ,@parentMenuId           -- ParentId - int
                            ,100                     -- DisplayOrder - int
                            ,0                       -- IsPopup - bit
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,1                       -- IsActive - bit
                            ,858                     -- ResourceId - smallint
                            ,@hierarchyId            -- HierarchyId - uniqueidentifier
                            ,NULL                    -- ModuleCode - varchar(5)
                            ,NULL                    -- MRUType - int
                            ,NULL                    -- HideStatusBar - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
            SET @parentMenuId = (
                                SELECT MenuItemId
                                FROM   syMenuItems
                                WHERE  ResourceId = 848
                                );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   syMenuItems
                          WHERE  ResourceId = 859
                          )
                BEGIN
                    SET @hierarchyId = (
                                       SELECT HierarchyId
                                       FROM   syNavigationNodes
                                       WHERE  ResourceId = 859
                                       );
                    INSERT INTO syMenuItems (
                                            MenuName
                                           ,DisplayName
                                           ,Url
                                           ,MenuItemTypeId
                                           ,ParentId
                                           ,DisplayOrder
                                           ,IsPopup
                                           ,ModDate
                                           ,ModUser
                                           ,IsActive
                                           ,ResourceId
                                           ,HierarchyId
                                           ,ModuleCode
                                           ,MRUType
                                           ,HideStatusBar
                                            )
                    VALUES ( 'Disclosure'            -- MenuName - varchar(250)
                            ,'Disclosure'            -- DisplayName - varchar(250)
                            ,N'/SY/ParamReport.aspx' -- Url - nvarchar(250)
                            ,4                       -- MenuItemTypeId - smallint
                            ,@parentMenuId           -- ParentId - int
                            ,100                     -- DisplayOrder - int
                            ,0                       -- IsPopup - bit
                            ,@modDate                -- ModDate - datetime
                            ,'support'               -- ModUser - varchar(50)
                            ,1                       -- IsActive - bit
                            ,859                     -- ResourceId - smallint
                            ,@hierarchyId            -- HierarchyId - uniqueidentifier
                            ,NULL                    -- ModuleCode - varchar(5)
                            ,NULL                    -- MRUType - int
                            ,NULL                    -- HideStatusBar - bit
                           );
                END;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
        END;
END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @Error = 1;
END CATCH;

IF ( @Error = 0 )
    BEGIN
        COMMIT TRANSACTION addNewResource;
    END;
ELSE
    BEGIN
        ROLLBACK TRANSACTION addNewResource;
    END;
GO
--=================================================================================================
-- END -- AD-3706 Create NACCAS Report Menu in Advantage
--=================================================================================================
--=================================================================================================
-- START AD-2369 - Ability to change the Campus when working on Student termination page 
--=================================================================================================
DECLARE @Error AS INTEGER;
SET @Error = 0;

BEGIN TRANSACTION terminateStudentRlsRes;
BEGIN TRY

    DECLARE @ActiveStatusId UNIQUEIDENTIFIER;
    SET @ActiveStatusId = (
                          SELECT   TOP ( 1 ) StatusId
                          FROM     dbo.syStatuses
                          WHERE    StatusCode = 'A'
                          ORDER BY StatusCode
                          );

    IF NOT EXISTS (
                  SELECT TOP ( 1 ) RoleId
                  FROM   dbo.syRoles
                  WHERE  SysRoleId = 18
                         AND StatusId = @ActiveStatusId
                  )
        BEGIN
            INSERT INTO dbo.syRoles (
                                    RoleId
                                   ,Role
                                   ,Code
                                   ,StatusId
                                   ,SysRoleId
                                   ,ModDate
                                   ,ModUser
                                    )
            VALUES ( NEWID(), 'Student Termination', 'Student Term', @ActiveStatusId, 18, GETDATE(), 'support' );
        END;


    DECLARE @TerminateStudentResourceId INT;
    SET @TerminateStudentResourceId = (
                                      SELECT   TOP ( 1 ) ResourceID
                                      FROM     dbo.syResources
                                      WHERE    Resource = 'Terminate Student'
                                               AND ResourceTypeID = 3
                                               AND SummListId = 0
                                               AND ChildTypeId = 0
                                               AND MRUTypeId = 0
                                               AND UsedIn = 0
                                               AND TblFldsId = 0
                                               AND AllowSchlReqFlds IS NULL
                                      ORDER BY ModDate ASC
                                      );


    DECLARE @TerminateStudentRoleID UNIQUEIDENTIFIER;
    SET @TerminateStudentRoleID = (
                                  SELECT   TOP ( 1 ) schoolRoles.RoleId
                                  FROM     dbo.syRoles AS schoolRoles
                                  INNER JOIN dbo.sySysRoles AS systemRoles ON systemRoles.SysRoleId = schoolRoles.SysRoleId
                                  WHERE    systemRoles.SysRoleId = 18
                                           AND systemRoles.StatusId = @ActiveStatusId
                                           AND schoolRoles.StatusId = @ActiveStatusId
                                  ORDER BY schoolRoles.ModDate ASC
                                  );

    IF NOT EXISTS (
                  SELECT TOP ( 1 ) *
                  FROM   dbo.syRlsResLvls
                  WHERE  RoleId = @TerminateStudentRoleID
                         AND ResourceID = @TerminateStudentResourceId
                         AND AccessLevel = 15
                  )
        BEGIN
            INSERT INTO dbo.syRlsResLvls (
                                         RRLId
                                        ,RoleId
                                        ,ResourceID
                                        ,AccessLevel
                                        ,ModDate
                                        ,ModUser
                                        ,ParentId
                                         )
            VALUES ( NEWID()                     -- RRLId - uniqueidentifier
                    ,@TerminateStudentRoleID     -- RoleId - uniqueidentifier
                    ,@TerminateStudentResourceId -- ResourceID - smallint
                    ,15                          -- AccessLevel - smallint
                    ,GETDATE()                   -- ModDate - datetime
                    ,'Support'                   -- ModUser - varchar(50)
                    ,NULL                        -- ParentId - int
                   );
        END;

    IF ( @@ERROR > 0 )
        BEGIN
            SET @Error = @@ERROR;
        END;
END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @Error = 1;
END CATCH;

IF ( @Error = 0 )
    BEGIN
        COMMIT TRANSACTION terminateStudentRlsRes;
    END;
ELSE
    BEGIN
        ROLLBACK TRANSACTION terminateStudentRlsRes;
    END;
GO
--=================================================================================================
-- END AD-2369 - Ability to change the Campus when working on Student termination page 
--=================================================================================================
--=================================================================================================
-- START -- AD-5058 TECH: Use Currently Attending default status upon enrollment
--=================================================================================================
IF NOT EXISTS (
              SELECT TOP 1 *
              FROM   dbo.syStatusCodes
              WHERE  SysStatusId = 9
                     AND IsDefaultLeadStatus = 1
              )
    BEGIN
        UPDATE syStatusCodes
        SET    IsDefaultLeadStatus = 1
        WHERE  StatusCodeId IN (
                               SELECT TOP 1 StatusCodeId
                               FROM   syStatusCodes
                               WHERE  SysStatusId = 9
                                      AND StatusCodeDescrip IN ( 'active', 'Currently  Attending', 'currently attending', 'Currently Attending/Active' )
                                      AND StatusCode NOT IN ( 'active2' )
                               );
    END;
GO
--=================================================================================================
-- END -- AD-5058 TECH: Use Currently Attending default status upon enrollment
--=================================================================================================
--=================================================================================================
-- Start - AD-318 The calsulation period types
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION calculationPeriodTypes;
BEGIN TRY
    DECLARE @PeriodName VARCHAR(100) = 'Payment Period'
           ,@PeriodOfEnrollment VARCHAR(100) = 'Period of Enrollment';
    BEGIN
        IF NOT EXISTS (
                      SELECT 1
                      FROM   syR2T4CalculationPeriodTypes
                      WHERE  Code IN ( @PeriodName, @PeriodOfEnrollment )
                      )
            BEGIN
                DECLARE @SupportUserId UNIQUEIDENTIFIER = (
                                                          SELECT TOP 1 UserId
                                                          FROM   dbo.syUsers
                                                          WHERE  (
                                                                 IsAdvantageSuperUser = 1
                                                                 AND FullName = 'support'
                                                                 )
                                                                 OR (
                                                                    IsAdvantageSuperUser = 1
                                                                    AND FullName = 'sa'
                                                                    )
                                                          );
                INSERT INTO syR2T4CalculationPeriodTypes (
                                                         CalculationPeriodTypeId
                                                        ,Code
                                                        ,Description
                                                        ,ModDate
                                                        ,ModifiedByUserId
                                                         )
                VALUES ( NEWID(), @PeriodName, @PeriodName, GETDATE(), @SupportUserId );
                INSERT INTO syR2T4CalculationPeriodTypes (
                                                         CalculationPeriodTypeId
                                                        ,Code
                                                        ,Description
                                                        ,ModDate
                                                        ,ModifiedByUserId
                                                         )
                VALUES ( NEWID(), @PeriodOfEnrollment, @PeriodOfEnrollment, GETDATE(), @SupportUserId );
            END;
    END;
END TRY
BEGIN CATCH
    ROLLBACK TRANSACTION calculationPeriodTypes;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION calculationPeriodTypes;
        PRINT 'Failed to insert the data to SyR2t4calculationPeriodTypes table ';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION calculationPeriodTypes;
        PRINT 'Calculation period types data inserted';
    END;
GO

--=================================================================================================
-- END - AD-318 The calsulation period types
--=================================================================================================

--=================================================================================================
-- Start - AD-4552 Add Student Group filter to Student Listing Report
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION studentListingStudentGroups;
BEGIN TRY
    DECLARE @leadGroupTblFieldId INT;
    SELECT @leadGroupTblFieldId = (
                                  SELECT   TOP ( 1 ) tblfields.TblFldsId
                                  FROM     dbo.syFldCaptions fieldcap
                                  INNER JOIN dbo.syTblFlds tblfields ON tblfields.FldId = fieldcap.FldId
                                  INNER JOIN dbo.syTables tbls ON tbls.TblId = tblfields.TblId
                                  WHERE    tbls.TblName = 'adLeadByLeadGroups'
                                           AND fieldcap.Caption = 'lead group'
                                  ORDER BY tblfields.TblFldsId
                                  );

    IF ( @leadGroupTblFieldId IS NOT NULL )
        BEGIN
            DECLARE @studentListingResId INT;
            SELECT @studentListingResId = (
                                          SELECT   TOP ( 1 ) ResourceID
                                          FROM     dbo.syResources
                                          WHERE    Resource = 'Student Listing'
                                          ORDER BY ResourceID
                                          );
            DECLARE @existingRptParam INT;

            SELECT @existingRptParam = (
                                       SELECT   TOP ( 1 ) RptParamId
                                       FROM     dbo.syRptParams
                                       WHERE    RptCaption = 'student group'
                                                AND ResourceId = @studentListingResId
                                       ORDER BY RptParamId
                                       );

            IF ( @existingRptParam IS NULL )
                BEGIN
                    PRINT 'Inserting student group filter to student listing report.';
                    INSERT INTO dbo.syRptParams (
                                                RptParamId
                                               ,ResourceId
                                               ,TblFldsId
                                               ,Required
                                               ,SortSec
                                               ,FilterListSec
                                               ,FilterOtherSec
                                               ,RptCaption
                                                )
                    VALUES ((
                            SELECT   TOP ( 1 ) RptParamId + 1
                            FROM     dbo.syRptParams
                            ORDER BY RptParamId DESC
                            )                    --RptParamId - smallint
                           ,@studentListingResId -- ResourceId - smallint
                           ,@leadGroupTblFieldId -- TblFldsId - int
                           ,0                    -- Required - bit
                           ,0                    -- SortSec - bit
                           ,1                    -- FilterListSec - bit
                           ,0                    -- FilterOtherSec - bit
                           ,'Student Group'      -- RptCaption - varchar(50)
                           );
                END;
        END;
END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @error = 1;
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION studentListingStudentGroups;
        PRINT 'Failed to insert student group filter to student listing report.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION studentListingStudentGroups;
        PRINT 'Student group filter added to student listing report.';
    END;
GO

--=================================================================================================
-- END - AD-4552 Add Student Group filter to Student Listing Report
--=================================================================================================

--=================================================================================================
-- Start - AD-5693 Update Terms & Conditions Configuration Value
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION updateTermsAndConditionsValue;
BEGIN TRY
    DECLARE @KeyName VARCHAR(50) = 'TermsOfUseVersionNumber';
    DECLARE @TCVersion VARCHAR(50) = 'v20180401';

    DECLARE @settingId AS INT;
    SET @settingId = (
                     SELECT syConfigAppSettings.SettingId
                     FROM   dbo.syConfigAppSettings
                     WHERE  KeyName = @KeyName
                     );

    IF ( @settingId IS NOT NULL )
        BEGIN
            UPDATE dbo.syConfigAppSetValues
            SET    Value = @TCVersion
                  ,ModUser = 'Support'
                  ,ModDate = GETDATE()
            WHERE  SettingId = @settingId
                   AND Value <> @TCVersion;
        END;


END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @error = 1;
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION updateTermsAndConditionsValue;
        PRINT 'Failed to Update Terms and Conditions Version.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION updateTermsAndConditionsValue;
        PRINT 'Updated Terms and Conditions Version.';
    END;
GO
--=================================================================================================
-- End - AD-5693 Update Terms & Conditions Configuration Value
--=================================================================================================

--=================================================================================================
-- Start - AD-6008 Format Consolidated Script
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION importStatusOps;
BEGIN TRY
    DECLARE @campusGroupAll UNIQUEIDENTIFIER = (
                                               SELECT TOP 1 CampGrpId
                                               FROM   dbo.syCampGrps
                                               WHERE  CampGrpDescrip = 'All'
                                               );
    DECLARE @activeStatus UNIQUEIDENTIFIER = (
                                             SELECT TOP 1 StatusId
                                             FROM   dbo.syStatuses
                                             WHERE  StatusCode = 'A'
                                             );
    DECLARE @importedSysId INT;
    DECLARE @importedStatusCode UNIQUEIDENTIFIER;
    DECLARE @newStatusCode UNIQUEIDENTIFIER = ( COALESCE((
                                                         SELECT   TOP 1 StatusCodeId
                                                         FROM     dbo.syStatusCodes
                                                         WHERE    SysStatusId = 1
                                                                  AND StatusCodeDescrip LIKE UPPER('%NEW LEAD%')
                                                                  AND StatusId = @activeStatus
                                                                  AND CampGrpId = @campusGroupAll
                                                         ORDER BY ModDate ASC
                                                         ) ,(
                                                            SELECT   TOP 1 StatusCodeId
                                                            FROM     dbo.syStatusCodes
                                                            WHERE    SysStatusId = 1
                                                                     AND StatusId = @activeStatus
                                                                     AND CampGrpId = @campusGroupAll
                                                            ORDER BY ModDate ASC
                                                            ))
                                              );

    IF @newStatusCode IS NULL
        BEGIN
            PRINT 'Could not find appropriate new status code, creating one instead.';

            INSERT INTO dbo.syStatusCodes (
                                          StatusCodeId
                                         ,StatusCode
                                         ,StatusCodeDescrip
                                         ,StatusId
                                         ,CampGrpId
                                         ,SysStatusId
                                         ,ModDate
                                         ,ModUser
                                         ,AcadProbation
                                         ,DiscProbation
                                         ,IsDefaultLeadStatus
                                          )
            VALUES ( NEWID()         -- StatusCodeId - uniqueidentifier
                    ,'NEWLEAD'       -- StatusCode - varchar(20)
                    ,'New Lead'      -- StatusCodeDescrip - varchar(80)
                    ,@activeStatus   -- StatusId - uniqueidentifier
                    ,@campusGroupAll -- CampGrpId - uniqueidentifier
                    ,1               -- SysStatusId - int
                    ,GETDATE()       -- ModDate - datetime
                    ,'Support'       -- ModUser - varchar(50)
                    ,0               -- AcadProbation - bit
                    ,0               -- DiscProbation - bit
                    ,0               -- IsDefaultLeadStatus - bit
                   );

            SELECT @newStatusCode = (
                                    SELECT TOP 1 StatusCodeId
                                    FROM   dbo.syStatusCodes
                                    WHERE  SysStatusId = 1
                                           AND StatusCodeDescrip LIKE UPPER('%NEW LEAD%')
                                           AND StatusId = @activeStatus
                                           AND CampGrpId = @campusGroupAll
                                    );
        END;

    IF NOT EXISTS (
                  SELECT 1
                  FROM   dbo.sySysStatus
                  WHERE  SysStatusDescrip = 'Imported'
                         AND StatusId = @activeStatus
                  )
        BEGIN
            PRINT 'Imported system status does not exist';
            INSERT INTO dbo.sySysStatus (
                                        SysStatusId
                                       ,SysStatusDescrip
                                       ,StatusId
                                       ,StatusLevelId
                                       ,ModUser
                                       ,ModDate
                                       ,InSchool
                                       ,GEProgramStatus
                                       ,PostAcademics
                                        )
            VALUES ((
                    SELECT MAX(SysStatusId) + 1
                    FROM   dbo.sySysStatus
                    )             -- SysStatusId - int
                   ,'Imported'    -- SysStatusDescrip - varchar(80)
                   ,@activeStatus -- StatusId - uniqueidentifier
                   ,1             -- StatusLevelId - int
                   ,'Support'     -- ModUser - varchar(50)
                   ,GETDATE()     -- ModDate - datetime
                   ,0             -- InSchool - int
                   ,NULL          -- GEProgramStatus - varchar(1)
                   ,0             -- PostAcademics - bit
                   );

            PRINT 'Inserted Imported system status.';
            SELECT @importedSysId = (
                                    SELECT TOP 1 SysStatusId
                                    FROM   dbo.sySysStatus
                                    WHERE  SysStatusDescrip = 'Imported'
                                    );

            INSERT INTO dbo.syStatusCodes (
                                          StatusCodeId
                                         ,StatusCode
                                         ,StatusCodeDescrip
                                         ,StatusId
                                         ,CampGrpId
                                         ,SysStatusId
                                         ,ModDate
                                         ,ModUser
                                         ,AcadProbation
                                         ,DiscProbation
                                         ,IsDefaultLeadStatus
                                          )
            VALUES ( NEWID()
                                                 -- StatusCodeId - uniqueidentifier
                    ,'IMPORTED'                  -- StatusCode - varchar(20)
                    ,'Lead Imported from Vendor' -- StatusCodeDescrip - varchar(80)
                    ,@activeStatus               -- StatusId - uniqueidentifier
                    ,@campusGroupAll             -- CampGrpId - uniqueidentifier
                    ,@importedSysId              -- SysStatusId - int
                    ,GETDATE()                   -- ModDate - datetime
                    ,'Support'                   -- ModUser - varchar(50)
                    ,0                           -- AcadProbation - bit
                    ,0                           -- DiscProbation - bit
                    ,0                           -- IsDefaultLeadStatus - bit
                   );
            PRINT 'Inserted Imported status code for all campus group.';

            SELECT @importedStatusCode = (
                                         SELECT TOP 1 StatusCodeId
                                         FROM   dbo.syStatusCodes
                                         WHERE  SysStatusId = @importedSysId
                                                AND StatusId = @activeStatus
                                                AND CampGrpId = @campusGroupAll
                                         );
            INSERT INTO dbo.syLeadStatusChanges (
                                                LeadStatusChangeId
                                               ,OrigStatusId
                                               ,NewStatusId
                                               ,CampGrpId
                                               ,ModDate
                                               ,ModUser
                                               ,IsReversal
                                                )
            VALUES ( NEWID()             -- LeadStatusChangeId - uniqueidentifier
                    ,@importedStatusCode -- OrigStatusId - uniqueidentifier
                    ,@newStatusCode      -- NewStatusId - uniqueidentifier
                    ,@campusGroupAll     -- CampGrpId - uniqueidentifier
                    ,GETDATE()           -- ModDate - datetime
                    ,'Support'           -- ModUser - varchar(50)
                    ,0                   -- IsReversal - bit
                   );

            PRINT 'Inserted status change sequence for imported to new lead.';

        END;

    ELSE
        BEGIN
            PRINT 'Imported system status exists';

            SELECT @importedSysId = (
                                    SELECT TOP 1 SysStatusId
                                    FROM   dbo.sySysStatus
                                    WHERE  SysStatusDescrip = 'Imported'
                                    );

            IF NOT EXISTS (
                          SELECT 1
                          FROM   dbo.syStatusCodes
                          WHERE  SysStatusId = @importedSysId
                                 AND StatusId = @activeStatus
                                 AND CampGrpId = @campusGroupAll
                          )
                BEGIN
                    INSERT INTO dbo.syStatusCodes (
                                                  StatusCodeId
                                                 ,StatusCode
                                                 ,StatusCodeDescrip
                                                 ,StatusId
                                                 ,CampGrpId
                                                 ,SysStatusId
                                                 ,ModDate
                                                 ,ModUser
                                                 ,AcadProbation
                                                 ,DiscProbation
                                                 ,IsDefaultLeadStatus
                                                  )
                    VALUES ( NEWID()
                                                         -- StatusCodeId - uniqueidentifier
                            ,'IMPORTED'                  -- StatusCode - varchar(20)
                            ,'Lead Imported from Vendor' -- StatusCodeDescrip - varchar(80)
                            ,@activeStatus               -- StatusId - uniqueidentifier
                            ,@campusGroupAll             -- CampGrpId - uniqueidentifier
                            ,@importedSysId              -- SysStatusId - int
                            ,GETDATE()                   -- ModDate - datetime
                            ,'Support'                   -- ModUser - varchar(50)
                            ,0                           -- AcadProbation - bit
                            ,0                           -- DiscProbation - bit
                            ,0                           -- IsDefaultLeadStatus - bit
                           );

                    PRINT 'Inserted Imported status code for all campus group.';

                END;

            SELECT @importedStatusCode = (
                                         SELECT TOP 1 StatusCodeId
                                         FROM   dbo.syStatusCodes
                                         WHERE  SysStatusId = @importedSysId
                                                AND StatusId = @activeStatus
                                                AND CampGrpId = @campusGroupAll
                                         );
            IF NOT EXISTS (
                          SELECT 1
                          FROM   dbo.syLeadStatusChanges
                          WHERE  OrigStatusId = @importedStatusCode
                                 AND NewStatusId = @newStatusCode
                                 AND CampGrpId = @campusGroupAll
                          )
                BEGIN
                    INSERT INTO dbo.syLeadStatusChanges (
                                                        LeadStatusChangeId
                                                       ,OrigStatusId
                                                       ,NewStatusId
                                                       ,CampGrpId
                                                       ,ModDate
                                                       ,ModUser
                                                       ,IsReversal
                                                        )
                    VALUES ( NEWID()             -- LeadStatusChangeId - uniqueidentifier
                            ,@importedStatusCode -- OrigStatusId - uniqueidentifier
                            ,@newStatusCode      -- NewStatusId - uniqueidentifier
                            ,@campusGroupAll     -- CampGrpId - uniqueidentifier
                            ,GETDATE()           -- ModDate - datetime
                            ,'Support'           -- ModUser - varchar(50)
                            ,0                   -- IsReversal - bit
                           );
                    PRINT 'Inserted status change sequence for imported to new lead.';

                END;
        END;
END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @error = 1;
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION importStatusOps;
        PRINT 'Failed to complete import status operations.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION importStatusOps;
        PRINT 'Import status operations completed.';
    END;
GO
--=================================================================================================
-- End - AD-6008 Format Consolidated Script
--=================================================================================================
--=================================================================================================
-- START AD-6026 Manage Config Setting to Turn NACCAS Menu on/off
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION ShowNACCASReportsManageConfig;
BEGIN TRY
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syConfigAppSettings
                  WHERE  KeyName = 'ShowNACCASReports'
                  )
        BEGIN
            DECLARE @settingId INT;
            DECLARE @modDate DATETIME;
            SET @modDate = GETDATE();
            SET @settingId = (
                             SELECT MAX(SettingId)
                             FROM   syConfigAppSettings
                             ) + 1;

			SET IDENTITY_INSERT [dbo].[syConfigAppSettings] ON;
            INSERT INTO syConfigAppSettings (
                                            SettingId
                                           ,KeyName
                                           ,Description
                                           ,ModUser
                                           ,ModDate
                                           ,CampusSpecific
                                           ,ExtraConfirmation
                                            )
            VALUES ( @settingId                                                                     -- SettingId - int
                    ,'ShowNACCASReports'                                                            -- KeyName - varchar(200)
                    ,'Yes/No campus specific value to show the NACCAS reports in the reports Page.' -- Description - varchar(1000)
                    ,'Support'                                                                      -- ModUser - varchar(50)
                    ,@modDate                                                                       -- ModDate - datetime
                    ,1                                                                              -- CampusSpecific - bit
                    ,0                                                                              -- ExtraConfirmation - bit
                );
			SET IDENTITY_INSERT [dbo].[syConfigAppSettings] OFF;

            INSERT INTO syConfigAppSet_Lookup (
                                              LookUpId
                                             ,SettingId
                                             ,ValueOptions
                                             ,ModUser
                                             ,ModDate
                                              )
            VALUES ( NEWID()    -- LookUpId - uniqueidentifier
                    ,@settingId -- SettingId - int
                    ,'No'       -- ValueOptions - varchar(50)
                    ,'Support'  -- ModUser - varchar(50)
                    ,@modDate   -- ModDate - datetime
                   );
            INSERT INTO syConfigAppSet_Lookup (
                                              LookUpId
                                             ,SettingId
                                             ,ValueOptions
                                             ,ModUser
                                             ,ModDate
                                              )
            VALUES ( NEWID()    -- LookUpId - uniqueidentifier
                    ,@settingId -- SettingId - int
                    ,'Yes'      -- ValueOptions - varchar(50)
                    ,'Support'  -- ModUser - varchar(50)
                    ,@modDate   -- ModDate - datetime
                   );
            INSERT INTO syConfigAppSetValues (
                                             ValueId
                                            ,SettingId
                                            ,CampusId
                                            ,Value
                                            ,ModUser
                                            ,ModDate
                                            ,Active
                                             )
            VALUES ( NEWID()    -- ValueId - uniqueidentifier
                    ,@settingId -- SettingId - int
                    ,NULL       -- CampusId - uniqueidentifier
                    ,'No'       -- Value - varchar(1000)
                    ,'Support'  -- ModUser - varchar(50)
                    ,@modDate   -- ModDate - datetime
                    ,1          -- Active - bit
                   );
        END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION ShowNACCASReportsManageConfig;
        PRINT 'Failed transcation ShowNACCASReportsManageConfig.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION ShowNACCASReportsManageConfig;
        PRINT 'successful transaction ShowNACCASReportsManageConfig.';
    END;
GO
--=================================================================================================
-- END AD-6026 Manage Config Setting to Turn NACCAS Menu on/off
--=================================================================================================
--=================================================================================================
-- START AD-6705 Add Manage Configuration Setting to turn on Course Weighting feature by Campus
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION AllowCourseWeightingManageConfig;
BEGIN TRY
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syConfigAppSettings
                  WHERE  KeyName = 'AllowCourseWeighting'
                  )
        BEGIN
            DECLARE @settingId INT;
            DECLARE @modDate DATETIME;
            SET @modDate = GETDATE();
            SET @settingId = (
                             SELECT MAX(SettingId)
                             FROM   syConfigAppSettings
                             ) + 1;
							 SET IDENTITY_INSERT [dbo].[syConfigAppSettings] ON;
            INSERT INTO syConfigAppSettings (
                                            SettingId
                                           ,KeyName
                                           ,Description
                                           ,ModUser
                                           ,ModDate
                                           ,CampusSpecific
                                           ,ExtraConfirmation
                                            )
            VALUES ( @settingId                                                                    -- SettingId - int
                    ,'AllowCourseWeighting'                                                        -- KeyName - varchar(200)
                    ,'Yes/No campus specific value to allow course weighting for numeric schools.' -- Description - varchar(1000)
                    ,'Support'                                                                     -- ModUser - varchar(50)
                    ,@modDate                                                                      -- ModDate - datetime
                    ,1                                                                             -- CampusSpecific - bit
                    ,0                                                                             -- ExtraConfirmation - bit
                );
				SET IDENTITY_INSERT [dbo].[syConfigAppSettings] OFF;
            INSERT INTO syConfigAppSet_Lookup (
                                              LookUpId
                                             ,SettingId
                                             ,ValueOptions
                                             ,ModUser
                                             ,ModDate
                                              )
            VALUES ( NEWID()    -- LookUpId - uniqueidentifier
                    ,@settingId -- SettingId - int
                    ,'No'       -- ValueOptions - varchar(50)
                    ,'Support'  -- ModUser - varchar(50)
                    ,@modDate   -- ModDate - datetime
                   );
            INSERT INTO syConfigAppSet_Lookup (
                                              LookUpId
                                             ,SettingId
                                             ,ValueOptions
                                             ,ModUser
                                             ,ModDate
                                              )
            VALUES ( NEWID()    -- LookUpId - uniqueidentifier
                    ,@settingId -- SettingId - int
                    ,'Yes'      -- ValueOptions - varchar(50)
                    ,'Support'  -- ModUser - varchar(50)
                    ,@modDate   -- ModDate - datetime
                   );
            INSERT INTO syConfigAppSetValues (
                                             ValueId
                                            ,SettingId
                                            ,CampusId
                                            ,Value
                                            ,ModUser
                                            ,ModDate
                                            ,Active
                                             )
            VALUES ( NEWID()    -- ValueId - uniqueidentifier
                    ,@settingId -- SettingId - int
                    ,NULL       -- CampusId - uniqueidentifier
                    ,'No'       -- Value - varchar(1000)
                    ,'Support'  -- ModUser - varchar(50)
                    ,@modDate   -- ModDate - datetime
                    ,1          -- Active - bit
                   );
        END;
END TRY
BEGIN CATCH
    SET @error = 1;
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION AllowCourseWeightingManageConfig;
        PRINT 'Failed transcation AllowCourseWeightingManageConfig.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION AllowCourseWeightingManageConfig;
        PRINT 'Successful transaction AllowCourseWeightingManageConfig.';
    END;
GO
--=================================================================================================
-- END AD-6705 Add Manage Configuration Setting to turn on Course Weighting feature by Campus
--=================================================================================================
--=================================================================================================
-- START AD-7326: User Types not Displayed when Creating/viewing users in manage user page (Chat311 Master DB)
--=================================================================================================
DECLARE @Error AS INTEGER;
SET @Error = 0;

BEGIN TRANSACTION UpdateUrl;
BEGIN TRY
    DECLARE @serviceSetting INT;
    DECLARE @siteSetting INT;
    SET @serviceSetting = (
                          SELECT SettingId
                          FROM   syConfigAppSettings
                          WHERE  KeyName = 'AdvantageServiceUri'
                          );
    SET @siteSetting = (
                       SELECT SettingId
                       FROM   syConfigAppSettings
                       WHERE  KeyName = 'AdvantageSiteUri'
                       );

    DECLARE @siteUrl VARCHAR(200);
    DECLARE @serviceUrl VARCHAR(200);
    SET @siteUrl = (
                   SELECT Value
                   FROM   syConfigAppSetValues
                   WHERE  SettingId = @siteSetting
                   );
    SET @serviceUrl = (
                      SELECT Value
                      FROM   syConfigAppSetValues
                      WHERE  SettingId = @serviceSetting
                      );
    DECLARE @part VARCHAR(6);
    DECLARE @partService VARCHAR(6);
    SET @part = SUBSTRING(@siteUrl, 5, 1);
    SET @siteUrl = SUBSTRING(@siteUrl, 5, ( LEN(@siteUrl) - 4 ));
    SET @partService = SUBSTRING(@serviceUrl, 5, 1);
    SET @serviceUrl = SUBSTRING(@serviceUrl, 5, ( LEN(@serviceUrl) - 4 ));
    IF @part = ':'
        BEGIN
            SET @siteUrl = 'https' + @siteUrl;
        END;
    ELSE
        BEGIN
            SET @siteUrl = 'http' + @siteUrl;
        END;
    IF @partService = ':'
        BEGIN
            SET @serviceUrl = 'https' + @serviceUrl;
        END;
    ELSE
        BEGIN
            SET @serviceUrl = 'http' + @serviceUrl;
        END;
    DECLARE @modDate DATETIME;
    SET @modDate = GETDATE();
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syConfigAppSetValues
                  WHERE  SettingId = @siteSetting
                         AND Value = @siteUrl
                  )
        BEGIN
            UPDATE syConfigAppSetValues
            SET    Value = @siteUrl
                  ,ModUser = 'Support'
                  ,ModDate = @modDate
            WHERE  SettingId = @siteSetting;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
        END;
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syConfigAppSetValues
                  WHERE  SettingId = @serviceSetting
                         AND Value = @serviceUrl
                  )
        BEGIN
            UPDATE syConfigAppSetValues
            SET    Value = @serviceUrl
                  ,ModUser = 'Support'
                  ,ModDate = @modDate
            WHERE  SettingId = @serviceSetting;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
        END;
END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @Error = 1;
END CATCH;

IF ( @Error = 0 )
    BEGIN
        COMMIT TRANSACTION addNewResource;
    END;
ELSE
    BEGIN
        ROLLBACK TRANSACTION addNewResource;
    END;
GO
--=================================================================================================
-- END AD-7326: User Types not Displayed when Creating/viewing users in manage user page (Chat311 Master DB)
--=================================================================================================
--=================================================================================================
-- START AD-6119 Tech: Install Google Analytics in Advantage 
--=================================================================================================

BEGIN TRANSACTION UpdateGoogleAnalytics;
DECLARE @GoogleAnalyticsUpdateError AS INTEGER;
BEGIN TRY
    IF NOT EXISTS (
                  SELECT TOP 1 SettingId
                  FROM   dbo.syConfigAppSettings
                  WHERE  KeyName = 'GoogleAnalyticsCode'
                  )
        BEGIN

            DECLARE @GoogleAnalyticsCodeSettingId INT;
			SET IDENTITY_INSERT [dbo].[syConfigAppSettings] OFF;
            INSERT INTO dbo.syConfigAppSettings (
                                                KeyName
                                               ,Description
                                               ,ModUser
                                               ,ModDate
                                               ,CampusSpecific
                                               ,ExtraConfirmation
                                                )
            VALUES ( 'GoogleAnalyticsCode', 'Code for Google Analytics', 'sa', GETDATE(), 0, 0 );

            SET @GoogleAnalyticsCodeSettingId = (
                                                SELECT TOP 1 SettingId
                                                FROM   dbo.syConfigAppSettings
                                                WHERE  KeyName = 'GoogleAnalyticsCode'
                                                );

            IF ( @GoogleAnalyticsCodeSettingId > 0 )
                BEGIN
                    INSERT INTO dbo.syConfigAppSetValues (
                                                         SettingId
                                                        ,CampusId
                                                        ,Value
                                                        ,ModUser
                                                        ,ModDate
                                                        ,Active
                                                         )
                    VALUES ( @GoogleAnalyticsCodeSettingId, NULL, '', 'sa', GETDATE(), 1 );
                END;
        END;

END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @GoogleAnalyticsUpdateError = 1;
END CATCH;
IF ( @GoogleAnalyticsUpdateError = 0 )
    BEGIN
        COMMIT TRANSACTION UpdateGoogleAnalytics;
    END;
ELSE
    BEGIN
        ROLLBACK TRANSACTION UpdateGoogleAnalytics;
    END;
GO

--=================================================================================================
-- END AD-6119 Tech: Install Google Analytics in Advantage 
--=================================================================================================
--=================================================================================================
-- Start AD-6018 Update all the A1 Databases to 3.11 to test the DB Tool will work for all the client databases.
--=================================================================================================
DECLARE @EnrollmentReqId UNIQUEIDENTIFIER;
DECLARE @FinAidReqId UNIQUEIDENTIFIER;
DECLARE @GraduationtReqId UNIQUEIDENTIFIER;
DECLARE @CampGroupReqId UNIQUEIDENTIFIER;
DECLARE @CampGroupAllId UNIQUEIDENTIFIER;

SET @CampGroupAllId = (
                      SELECT CampGrpId
                      FROM   dbo.syCampGrps
                      WHERE  IsAllCampusGrp = 1
                      );

IF NOT EXISTS (
              SELECT TOP 1 TypeOfRequirementId
              FROM   dbo.SyTypeofRequirement
              WHERE  TypeOfReq = 'Enrollment'
              )
    BEGIN
        INSERT INTO dbo.SyTypeofRequirement (
                                            RequirementID
                                           ,TypeOfReq
                                           ,CampGrpID
                                           ,TypeOfRequirementId
                                            )
        VALUES ( NEWID(), 'Enrollment', @CampGroupAllId, 1 );
    END;

IF NOT EXISTS (
              SELECT TOP 1 TypeOfRequirementId
              FROM   dbo.SyTypeofRequirement
              WHERE  TypeOfReq = 'Financial Aid'
              )
    BEGIN
        INSERT INTO dbo.SyTypeofRequirement (
                                            RequirementID
                                           ,TypeOfReq
                                           ,CampGrpID
                                           ,TypeOfRequirementId
                                            )
        VALUES ( NEWID(), 'Financial Aid', @CampGroupAllId, 2 );
    END;

IF NOT EXISTS (
              SELECT TOP 1 TypeOfRequirementId
              FROM   dbo.SyTypeofRequirement
              WHERE  TypeOfReq = 'Graduation'
              )
    BEGIN
        INSERT INTO dbo.SyTypeofRequirement (
                                            RequirementID
                                           ,TypeOfReq
                                           ,CampGrpID
                                           ,TypeOfRequirementId
                                            )
        VALUES ( NEWID(), 'Graduation', @CampGroupAllId, 3 );
    END;
GO
--=================================================================================================
-- END AD-6018 Update all the A1 Databases to 3.11 to test the DB Tool will work for all the client databases.
--=================================================================================================
--=================================================================================================
-- START AD-7433 : Enhance Lead page to manage Imported Leads from API
--=================================================================================================
DECLARE @Error AS INTEGER;
SET @Error = 0;

BEGIN TRANSACTION MakeMandatory;
BEGIN TRY
    DECLARE @tblFldId INT;
    DECLARE @fldId INT;
    DECLARE @tblId INT;
    SET @tblId = (
                 SELECT TblId
                 FROM   syTables
                 WHERE  TblName = 'adleads'
                 );
    SET @fldId = (
                 SELECT FldId
                 FROM   syFields
                 WHERE  FldName = 'LeadStatus'
                 );
    SET @tblFldId = (
                    SELECT TblFldsId
                    FROM   syTblFlds
                    WHERE  TblId = @tblId
                           AND FldId = @fldId
                    );
    IF NOT EXISTS (
                  SELECT 1
                  FROM   syResTblFlds
                  WHERE  TblFldsId = @tblFldId
                         AND ResourceId = 170
                         AND Required = 1
                  )
        BEGIN
            UPDATE syResTblFlds
            SET    Required = 1
            WHERE  ResourceId = 170
                   AND TblFldsId = @tblFldId;
            IF ( @@ERROR > 0 )
                BEGIN
                    SET @Error = @@ERROR;
                END;
        END;
END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @Error = 1;
END CATCH;

IF ( @Error = 0 )
    BEGIN
        COMMIT TRANSACTION MakeMandatory;
    END;
ELSE
    BEGIN
        ROLLBACK TRANSACTION MakeMandatory;
    END;
GO
--=================================================================================================
-- END AD-7433 : Enhance Lead page to manage Imported Leads from API
--=================================================================================================
-- ===============================================================================================
-- END  --   Consolidated Script Version 3.11
-- ===============================================================================================