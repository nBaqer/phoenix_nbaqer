USE TenantAuthDB;
GO
IF EXISTS (
          SELECT *
          FROM   sys.objects
          WHERE  type = 'P'
                 AND name = 'USP_MigrateUsers'
          )
    DROP PROCEDURE USP_MigrateUsers;
GO

CREATE PROCEDURE USP_MigrateUsers
    @DatabaseName VARCHAR(50)
   ,@TenantId INT
AS
    BEGIN

        DECLARE @SecurityQuestion VARCHAR(100)
               ,@SecurityAnswer VARCHAR(50);
        DECLARE @ApplicationId UNIQUEIDENTIFIER
               ,@LowerEmail VARCHAR(100);
        DECLARE @UserId UNIQUEIDENTIFIER
               ,@Email VARCHAR(100)
               ,@AccountActive BIT;
        SET @ApplicationId = 'C75D06DD-25F6-45B5-93A3-B71806320191';
        DECLARE @SQL VARCHAR(500)
               ,@SearchString VARCHAR(50)
               ,@saSearchString VARCHAR(5);


        --Set @SecurityQuestion = 'What is your favoutite color?'  
        --Set @SecurityAnswer = 'Blue'  

        SET @SearchString = 'support';
        SET @saSearchString = 'sa';
        SET @SQL = 'insert into tblUsers(DatabaseName,UserId,Email,AccountActive) Select Distinct ''' + @DatabaseName + ''',UserId,Email,AccountActive from '
                   + @DatabaseName + '.dbo.syusers where Lower(username) <> ''' + @SearchString + '''' + ' and Lower(username) <> ''' + @saSearchString + ''''
                   + ' order by email';
        PRINT @SQL;
        EXECUTE ( @SQL );

        DECLARE MigrateUsers_Cursor CURSOR LOCAL FORWARD_ONLY FAST_FORWARD READ_ONLY FOR
            SELECT DISTINCT UserId
                           ,Email
                           ,AccountActive
            FROM   tblUsers;

        OPEN MigrateUsers_Cursor;
        FETCH NEXT FROM MigrateUsers_Cursor
        INTO @UserId
            ,@Email
            ,@AccountActive;
        WHILE @@FETCH_STATUS = 0
            BEGIN
                --insert user's email address  
                --DBName.OWNER.SOURCETABLENAME : Syntax is mandatory in insert statement  
                BEGIN TRY
                    SET @LowerEmail = LOWER(@Email);

                    IF NOT EXISTS (
                                  SELECT *
                                  FROM   TenantAuthDB.dbo.aspnet_Users
                                  WHERE  UserId = @UserId
                                  )
                        BEGIN
                            INSERT INTO TenantAuthDB.dbo.aspnet_Users (
                                                                      ApplicationId
                                                                     ,UserId
                                                                     ,UserName
                                                                     ,LoweredUserName
                                                                     ,MobileAlias
                                                                     ,IsAnonymous
                                                                     ,LastActivityDate
                                                                      )
                            VALUES ( @ApplicationId, @UserId, @LowerEmail, @LowerEmail, NULL, 0, GETDATE());

                            -- Default password being used is : advantage3$  
                            -- Default security anwer: security answer  
                            -- User will be forced to change password at first login  
                            INSERT INTO dbo.aspnet_Membership (
                                                              ApplicationId
                                                             ,UserId
                                                             ,Password
                                                             ,PasswordFormat
                                                             ,PasswordSalt
                                                             ,MobilePIN
                                                             ,Email
                                                             ,LoweredEmail
                                                             ,PasswordQuestion
                                                             ,PasswordAnswer
                                                             ,IsApproved
                                                             ,IsLockedOut
                                                             ,CreateDate
                                                             ,LastLoginDate
                                                             ,LastPasswordChangedDate
                                                             ,LastLockoutDate
                                                             ,FailedPasswordAttemptCount
                                                             ,FailedPasswordAttemptWindowStart
                                                             ,FailedPasswordAnswerAttemptCount
                                                             ,FailedPasswordAnswerAttemptWindowStart
                                                             ,Comment
                                                              )
                            VALUES ( @ApplicationId, @UserId, 'CcnbzYVEUDPVOH1ne2m/X2ttvQ4=', 1, 'ghMdIEJSDwFgiWkA/a0JTw==', NULL, @LowerEmail, @LowerEmail
                                    ,'What is your favourite color', 'BtJLiybtDny54+q4nmhXtzlXovE=', @AccountActive, 0, GETDATE(), GETDATE(), GETDATE()
                                    ,GETDATE(), 0, GETDATE(), 0, GETDATE(), NULL );
                        END;

                    IF NOT EXISTS (
                                  SELECT *
                                  FROM   TenantAuthDB.dbo.TenantUsers
                                  WHERE  TenantId = @TenantId
                                         AND UserId = @UserId
                                  )
                        BEGIN
                            INSERT INTO TenantAuthDB.dbo.TenantUsers (
                                                                     TenantId
                                                                    ,UserId
                                                                    ,IsDefaultTenant
                                                                    ,IsSupportUser
                                                                     )
                            VALUES ( @TenantId, @UserId, 0, 0 );
                        END;

                END TRY
                BEGIN CATCH
                    INSERT INTO UserMigrationErrors (
                                                    DatabaseName
                                                   ,UserId
                                                   ,Email
                                                   ,ErrorNumber
                                                   ,ErrorSeverity
                                                   ,ErrorState
                                                   ,ErrorProcedure
                                                   ,ErrorLine
                                                   ,ErrorMessage
                                                   ,ModUser
                                                   ,ModDate
                                                    )
                                SELECT @DatabaseName
                                      ,@UserId
                                      ,@Email
                                      ,ERROR_NUMBER() AS ErrorNumber
                                      ,ERROR_SEVERITY() AS ErrorSeverity
                                      ,ERROR_STATE() AS ErrorState
                                      ,ERROR_PROCEDURE() AS ErrorProcedure
                                      ,ERROR_LINE() AS ErrorLine
                                      ,ERROR_MESSAGE() AS ErrorMessage
                                      ,'support'
                                      ,GETDATE();
                END CATCH;
                FETCH NEXT FROM MigrateUsers_Cursor
                INTO @UserId
                    ,@Email
                    ,@AccountActive;
            END;
        --Truncate table tblUsers -- Clear contents   
        CLOSE MigrateUsers_Cursor;
        DEALLOCATE MigrateUsers_Cursor;

    END;
GO
--=================================================================================================
-- Start - AD-6024 Generate a new support password
-- Password changed to: f@m3.Supp0rt!
-- Password answer: blue
--=================================================================================================
DECLARE @error AS INT;
SET @error = 0;
BEGIN TRANSACTION updateSupportPassword;
BEGIN TRY
    DECLARE @salt VARCHAR(100) = 't47aZP3KnfHnAqqo1KFmZwy9aCMfbBnlNXDvq5qsMZc=';
    DECLARE @password VARCHAR(50) = '9o3HGcesFJzG0CQq8F6IomRSdaQ=';
    DECLARE @passwordAnswer VARCHAR(75) = 'h79s5afTZKBgZwqND8ITKkokf3Q=';
    DECLARE @support VARCHAR(50) = 'support@fameinc.com';
    DECLARE @userId AS UNIQUEIDENTIFIER;

    SET @userId = COALESCE((
                           SELECT UserId
                           FROM   dbo.aspnet_Membership
                           WHERE  Email = @support
                           ) -- Attempt to find support account using 'support@fameinc.com' email from membership table
                          ,(
                           SELECT UserId
                           FROM   dbo.aspnet_Users
                           WHERE  UserName = @support
                           ) -- Attempt to find support account using 'support@fameinc.com' email from users table
                          ,(
                           SELECT UserId
                           FROM   dbo.aspnet_Membership
                           WHERE  UserId = '864335EE-C9C6-47C9-BBCB-DEEE766F27A1' -- Attempt to find support account using Premise support User Id
                           )
                          ,(
                           SELECT UserId
                           FROM   dbo.aspnet_Membership
                           WHERE  UserId = 'B81DEAC3-DCEA-46BE-A26D-4DB74E8CBF07' -- Attempt to find support account using SAAS support User Id
                           )
                          );
    IF ( @userId IS NOT NULL )
        BEGIN
            IF EXISTS (
                      SELECT 1
                      FROM   dbo.aspnet_Membership
                      WHERE  UserId = @userId
                             AND Password = @password
                             AND PasswordSalt = @salt
                             AND PasswordAnswer = @passwordAnswer
                      )
                BEGIN
                    PRINT 'Support password has already been updated.';
                    ROLLBACK TRANSACTION updateSupportPassword;
                    RETURN;
                END;
            ELSE
                BEGIN
                    UPDATE dbo.aspnet_Membership
                    SET    Password = @password
                          ,PasswordSalt = @salt
                          ,PasswordAnswer = @passwordAnswer
                    WHERE  UserId = @userId;
                END;
        END;
    ELSE
        BEGIN
            PRINT 'Could not find a support account to update password.';
            ROLLBACK TRANSACTION updateSupportPassword;
            RETURN;
        END;


END TRY
BEGIN CATCH
    DECLARE @msg NVARCHAR(MAX);
    DECLARE @severity INT;
    DECLARE @state INT;
    SELECT @msg = ERROR_MESSAGE()
          ,@severity = ERROR_SEVERITY()
          ,@state = ERROR_STATE();
    RAISERROR(@msg, @severity, @state);
    SET @error = 1;
END CATCH;
IF @error > 0
    BEGIN
        ROLLBACK TRANSACTION updateSupportPassword;
        PRINT 'Failed to update support password.';

    END;
ELSE
    BEGIN
        COMMIT TRANSACTION updateSupportPassword;
        PRINT 'Updated support password.';
    END;
GO
--=================================================================================================
-- End - AD-6024 Generate a new support password
--=================================================================================================