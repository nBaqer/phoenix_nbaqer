USE [msdb]



/* 

INSTRUCTIONS:  
-- This script create "Attendance job" which run daily to keep updated daily syStudentAttendanceSummary table.
-- Please before run this script replace all “DATABASENAME” for client database name (8 occurrences in whole script).

*/
--DECLARE @DatabaseName AS NVARCHAR(500);
DECLARE @JobName AS NVARCHAR(500);

--SET @DatabaseName = 'A1_Starter'
SET @JobName = @DatabaseName + @JobStringName

IF EXISTS ( SELECT  1
            FROM    msdb.dbo.sysjobs_view
            WHERE   name = @JobName )
    BEGIN
        EXEC msdb.dbo.sp_delete_job @job_name = @JobName,@delete_unused_schedule = 1
    END

BEGIN TRANSACTION
DECLARE @ReturnCode INT
SELECT @ReturnCode = 0
/****** Object:  JobCategory [[Uncategorized (Local)]]    Script Date: 4/6/2017 8:49:30 AM ******/
IF NOT EXISTS (SELECT name FROM msdb.dbo.syscategories WHERE name=N'[Uncategorized (Local)]' AND category_class=1)
BEGIN
EXEC @ReturnCode = msdb.dbo.sp_add_category @class=N'JOB', @type=N'LOCAL', @name=N'[Uncategorized (Local)]'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback

END

DECLARE @jobId BINARY(16)
EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name = @JobName,  
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'No description available.', 
		@category_name=N'[Uncategorized (Local)]', 
		@owner_login_name=N'sa', @job_id = @jobId OUTPUT
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [Create_Table]    Script Date: 4/6/2017 8:49:30 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Create_Table', 
		@step_id=1, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'if not exists (select * from sysobjects where type=''u'' and name=''syStudentAttendanceSummary'')
begin
	Create table syStudentAttendanceSummary
	(
	 StuEnrollId uniqueidentifier,
	 ClsSectionId uniqueidentifier,
	 StudentAttendedDate datetime,
	 ScheduledDays decimal(18,2),
	 ActualDays decimal(18,2),
	 ActualRunningScheduledDays decimal(18,2),
	 ActualRunningPresentDays decimal(18,2),
	 ActualRunningAbsentDays decimal(18,2),
	 ActualRunningMakeupDays decimal(18,2),
	 ActualRunningTardyDays decimal(18,2),
	 AdjustedPresentDays decimal(18,2),
	 AdjustedAbsentDays decimal(18,2),
	 AttendanceTrackType varchar(50), 
	 ModUser varchar(50),
	 ModDate datetime)
end
go
Truncate Table syStudentAttendanceSummary
go
', 
		@database_name=@DatabaseName, 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [InsertAttendance_Day_PresentAbsent]    Script Date: 4/6/2017 8:49:30 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'InsertAttendance_Day_PresentAbsent', 
		@step_id=2, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'Declare @StuEnrollId uniqueidentifier,@MeetDate datetime,@WeekDay varchar(15),@StartDate datetime,@EndDate datetime
	Declare @PeriodDescrip varchar(50), @Actual decimal(18,2),@Excused decimal(18,2),@ClsSectionId uniqueidentifier
	Declare @Absent decimal(18,2), @ScheduledMinutes decimal(18,2), @TardyMinutes decimal(18,2),@MakeupHours decimal(18,2)
	declare @tardy decimal(18,2),@tracktardies int,@tardiesMakingAbsence int,@PrgVerId uniqueidentifier,@rownumber int,@IsTardy bit,@ActualRunningScheduledHours decimal(18,2)
	declare @ActualRunningPresentHours decimal(18,2),@ActualRunningAbsentHours decimal(18,2),@ActualRunningTardyHours decimal(18,2),@ActualRunningMakeupHours decimal(18,2)
	declare @PrevStuEnrollId uniqueidentifier,@intTardyBreakPoint int,@AdjustedRunningPresentHours decimal(18,2),@AdjustedRunningAbsentHours decimal(18,2),@ActualRunningScheduledDays decimal(18,2)
	DECLARE GetAttendance_Cursor CURSOR FAST_FORWARD FORWARD_ONLY FOR
		Select t1.StuEnrollId,t1.RecordDate,t1.ActualHours,t1.SchedHours,
				Case when ((t1.SchedHours >= 1 and t1.SchedHours not in (999,9999)) and t1.ActualHours=0)  Then t1.SchedHours else 0 end as Absent,
				t1.IsTardy,
  				t3.TrackTardies,
				t3.tardiesMakingAbsence
		from
			arStudentClockAttendance t1 inner join arStuEnrollments t2 on t1.StuEnrollId=t2.StuEnrollId 
			inner join arPrgVersions t3 on t2.PrgVerId=t3.PrgVerId 
		where
			t3.UnitTypeId in (''ef5535c2-142c-4223-ae3c-25a50a153cc6'',''B937C92E-FD7A-455E-A731-527A9918C734'') and t1.ActualHours <> 9999.00
		order by t1.StuEnrollId, t1.RecordDate
OPEN GetAttendance_Cursor
Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
FETCH NEXT FROM GetAttendance_Cursor INTO  
@StuEnrollId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,
@IsTardy,
@TrackTardies,@tardiesMakingAbsence

set @ActualRunningPresentHours=0
set @ActualRunningPresentHours=0
set @ActualRunningAbsentHours=0
set @ActualRunningTardyHours=0
set @ActualRunningMakeupHours=0
set @intTardyBreakPoint=0
set @AdjustedRunningPresentHours=0
set @AdjustedRunningAbsentHours=0
set @ActualRunningScheduledDays=0
set @MakeupHours=0
WHILE @@FETCH_STATUS = 0
BEGIN

		if @PrevStuEnrollId <> @StuEnrollId 
		begin
				set @ActualRunningPresentHours = 0
				set @ActualRunningAbsentHours = 0
				set @intTardyBreakPoint = 0
				set @ActualRunningTardyHours = 0
				set @AdjustedRunningPresentHours = 0
				set @AdjustedRunningAbsentHours = 0
				set @ActualRunningScheduledDays = 0
				set @MakeupHours=0
		end
	   
		if (@Actual <> 9999 and @Actual <> 999)
		begin
			set @ActualRunningScheduledDays = IsNULL(@ActualRunningScheduledDays,0) +  @ScheduledMinutes
			set @ActualRunningPresentHours = isNULL(@ActualRunningPresentHours,0) + @Actual
			set @AdjustedRunningPresentHours = IsNULL(@AdjustedRunningPresentHours,0) + @Actual
		end
		set @ActualRunningAbsentHours = IsNULL(@ActualRunningAbsentHours,0) + @Absent
    	if (@Actual=0 and @ScheduledMinutes >= 1 and @ScheduledMinutes not in (999,9999)) 
		begin
			set @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent
		end
		-- Applies only to AMC 
		-- If Scheduled = 3.0 hrs and Actual = 1.0 Then 2 hrs is considered absent
		if (@ScheduledMinutes >= 1 and @ScheduledMinutes not in (999,9999) and @Actual>0 and (@Actual<@ScheduledMinutes)) 
		begin
			set @ActualRunningAbsentHours = IsNULL(@ActualRunningAbsentHours,0) + (@ScheduledMinutes - @Actual)
			set @AdjustedRunningAbsentHours = IsNULL(@AdjustedRunningAbsentHours,0) + (@ScheduledMinutes - @Actual)
		end 
		if (@tracktardies=1 and @IsTardy=1)
		begin
			set @ActualRunningTardyHours = @ActualRunningTardyHours + 1
		end
		 -- commented by balaji on 10.22.2012 as rdl not adding attended days and makeup days
		 -- If there are make up hrs deduct that otherwise it will be added again in progress report
         IF (@Actual > 0 AND @Actual > @ScheduledMinutes and @Actual<>9999.00) 
					BEGIN
						SET @ActualRunningPresentHours = @ActualRunningPresentHours - (@Actual - @ScheduledMinutes)
						SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - (@Actual - @ScheduledMinutes)
					END
		if @tracktardies=1 and (@TardyMinutes > 0 or @IsTardy=1)
		begin
			set @intTardyBreakPoint = @intTardyBreakPoint + 1
		end	    
		
		-- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
            IF (@Actual > 0 AND @Actual > @ScheduledMinutes and @Actual<>9999.00) 
                BEGIN
                    SET @MakeupHours = @MakeupHours + (@Actual - @ScheduledMinutes)
                END
		
		if (@tracktardies=1 and @intTardyBreakPoint=@tardiesMakingAbsence)
		begin
			set @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual
			set @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes --@TardyMinutes
			set @intTardyBreakPoint=0
		end
		
		Delete from syStudentAttendanceSummary where StuEnrollId=@StuEnrollId and StudentAttendedDate=@MeetDate
		insert into syStudentAttendanceSummary
					(StuEnrollId,ClsSectionId,StudentAttendedDate,ScheduledDays,ActualDays,
					ActualRunningScheduledDays,
					ActualRunningPresentDays,
					ActualRunningAbsentDays,
					ActualRunningMakeupDays,
					ActualRunningTardyDays,
					AdjustedPresentDays,
					AdjustedAbsentDays,AttendanceTrackType,
					ModUser,ModDate,TardiesMakingAbsence)
				values
					(@StuEnrollId,@ClsSectionId,@MeetDate,IsNULL(@ScheduledMinutes,0),@Actual,IsNULL(@ActualRunningScheduledDays,0),IsNULL(@ActualRunningPresentHours,0),
					IsNULL(@ActualRunningAbsentHours,0),IsNULL(@MakeupHours,0),IsNULL(@ActualRunningTardyHours,0),isNULL(@AdjustedRunningPresentHours,0),
					isNULL(@AdjustedRunningAbsentHours,0),''Post Attendance by Class'',''sa'',getdate(),@TardiesMakingAbsence)

	--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId

	 set @PrevStuEnrollId=@StuEnrollId 
	FETCH NEXT FROM GetAttendance_Cursor INTO  
@StuEnrollId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,
@IsTardy,
--@ActualRunningScheduledHours,@ActualRunningPresentHours,
--@ActualRunningAbsentHours,@ActualRunningMakeupHours,@ActualRunningTardyHours,
@TrackTardies,@tardiesMakingAbsence
END
CLOSE GetAttendance_Cursor
DEALLOCATE GetAttendance_Cursor
go
', 
		@database_name=@DatabaseName, 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [InsertAttendance_Class_PresentAbsent]    Script Date: 4/6/2017 8:49:30 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'InsertAttendance_Class_PresentAbsent', 
		@step_id=3, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'Declare @StuEnrollId uniqueidentifier,@MeetDate datetime,@WeekDay varchar(15),@StartDate datetime,@EndDate datetime
	Declare @PeriodDescrip varchar(50), @Actual decimal(18,2),@Excused decimal(18,2),@ClsSectionId uniqueidentifier
	Declare @Absent decimal(18,2), @ScheduledMinutes decimal(18,2), @TardyMinutes decimal(18,2)
	declare @tardy decimal(18,2),@tracktardies int,@tardiesMakingAbsence int,@PrgVerId uniqueidentifier,@rownumber int
	declare @ActualRunningPresentHours decimal(18,2),@ActualRunningAbsentHours decimal(18,2),@ActualRunningTardyHours decimal(18,2),@ActualRunningMakeupHours decimal(18,2)
	declare @PrevStuEnrollId uniqueidentifier,@intTardyBreakPoint int,@AdjustedRunningPresentHours decimal(18,2),@AdjustedRunningAbsentHours decimal(18,2),@ActualRunningScheduledDays decimal(18,2)
	declare @AdjustedPresentDaysComputed decimal(18,2), @MakeupHours decimal(18,2)
	
	Declare @boolReset bit
	DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
	Select *,Row_Number() OVER (Order by MeetDate) as RowNumber 
	from
	( 
	Select Distinct
				t1.StuEnrollId,t1.ClsSectionId,t1.MeetDate,datename(dw,t1.MeetDate) as WeekDay,t4.StartDate,t4.EndDate,t5.PeriodDescrip,t1.Actual,t1.Excused,
				CASE WHEN (t1.Actual = 0 AND t1.Excused = 0)
                     THEN t1.Scheduled
                     ELSE 
						Case when (t1.Actual <> 9999.00 and t1.Actual < t1.Scheduled)
						--Commented by Balaji on 2.6.2015 as Excused absence is still considered Absent, but considered present only while calculating payment period
						--Case when (t1.Actual <> 9999.00 and t1.Actual < t1.Scheduled and t1.Excused <> 1) 
								THEN (t1.Scheduled - t1.Actual)
								ELSE 
									0
								End
                 END AS Absent,
				t1.Scheduled as ScheduledMinutes,
				Case when (t1.Actual>0 and t1.Actual<t1.Scheduled) Then (t1.Scheduled-t1.Actual) else 0 end as TardyMinutes,
				t1.Tardy AS Tardy,
				t3.TrackTardies,
				t3.tardiesMakingAbsence,
				t3.PrgVerId
		from
			atClsSectAttendance t1 inner join arStuEnrollments t2 on t1.StuEnrollId=t2.StuEnrollId 
			inner join arPrgVersions t3 on t2.PrgVerId=t3.PrgVerId 
			inner join arClsSectMeetings t4 on t1.ClsSectionId=t4.ClsSectionId and 
			(convert(DATE,t1.MeetDate,111)>=convert(DATE,t4.StartDate,111) and 
			 convert(DATE,t1.MeetDate,111)<=convert(DATE,t4.EndDate,111))  AND t1.ClsSectMeetingId=t4.ClsSectMeetingId --DE8479 line added
			inner join syPeriods t5 on t4.periodid=t5.periodid
			inner join cmTimeInterval t6 on t5.StartTimeId=t6.TimeIntervalId
			inner join cmTimeInterval t7 on t5.EndTimeId=t7.TimeIntervalId
			INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
		    INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
		where
			--t2.StuEnrollId in (''2DE27471-A05F-4F8A-ACC4-82A6395A6A76'') and --t1.ClsSectionId=''7F213AF5-FD99-4A8E-8E2A-13CC8FE0F821'' and
			(t10.UnitTypeId IN (''2600592A-9739-4A13-BDCE-7A25FE4A7478'',''EF5535C2-142C-4223-AE3C-25A50A153CC6'' )   
			OR t3.UnitTypeId IN (''2600592A-9739-4A13-BDCE-7A25FE4A7478'',''EF5535C2-142C-4223-AE3C-25A50A153CC6'')) -- Minutes
			and t1.Actual<>9999
		) dt
		--exec Usp_ProgressReport_StudentData_GetList ''BDB21FA4-CBED-4780-AD7C-A55C50725503'',null,0,0,null,''byclass'',null,null
	order by StuEnrollId, MeetDate
OPEN GetAttendance_Cursor
FETCH NEXT FROM GetAttendance_Cursor INTO  
@StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,
@PeriodDescrip,@Actual,@Excused,
@Absent, @ScheduledMinutes, @TardyMinutes,
@tardy,@tracktardies,@tardiesMakingAbsence,@PrgVerId,@rownumber
set @ActualRunningPresentHours=0
set @ActualRunningPresentHours=0
set @ActualRunningAbsentHours=0
set @ActualRunningTardyHours=0
set @ActualRunningMakeupHours=0
set @intTardyBreakPoint=0
set @AdjustedRunningPresentHours=0
set @AdjustedRunningAbsentHours=0
set @ActualRunningScheduledDays=0
set @boolReset=0
Set @MakeupHours=0
WHILE @@FETCH_STATUS = 0
BEGIN

		if @PrevStuEnrollId <> @StuEnrollId 
		begin
				set @ActualRunningPresentHours = 0
				set @ActualRunningAbsentHours = 0
				set @intTardyBreakPoint = 0
				set @ActualRunningTardyHours = 0
				set @AdjustedRunningPresentHours = 0
				set @AdjustedRunningAbsentHours = 0
				set @ActualRunningScheduledDays = 0
				set @MakeupHours=0
				set @boolReset=1
		end

	                  
            -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
            IF @Actual <> 9999.00 
                BEGIN
					-- Commented by Balaji on 2.6.2015 as Excused Absence is still considered an absence
					-- @Excused is not added to Present Hours
                    SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual  -- + @Excused
                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual -- + @Excused
                    
                    -- If there are make up hrs deduct that otherwise it will be added again in progress report
                    IF (@Actual > 0 AND @Actual > @ScheduledMinutes and @Actual<>9999.00) 
					BEGIN
						SET @ActualRunningPresentHours = @ActualRunningPresentHours - (@Actual - @ScheduledMinutes)
						SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - (@Actual - @ScheduledMinutes)
					END
                      
                    SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes                      
                END
           
			-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
            SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent
            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent	
      
			-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
            IF ( @Actual > 0 AND @Actual < @ScheduledMinutes and @Tardy=1) 
                BEGIN
                    SET @ActualRunningTardyHours = @ActualRunningTardyHours + (@ScheduledMinutes - @Actual)
                END
			ELSE IF (@Actual=1 AND @Actual=@ScheduledMinutes and @Tardy=1)
				begin
					 SET @ActualRunningTardyHours += 1
				END
					
			-- Track how many days student has been tardy only when 
			-- program version requires to track tardy
            IF (@tracktardies = 1 and @Tardy=1)
                BEGIN
                    SET @intTardyBreakPoint = @intTardyBreakPoint + 1
                END	    
           
            
            -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
            -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
            -- when student is tardy the second time, that second occurance will be considered as
            -- absence
            -- Variable @intTardyBreakpoint tracks how many times the student was tardy
            -- Variable @tardiesMakingAbsence tracks the tardy rule
			Declare @CountTardyForClass int
			Set @CountTardyForClass = (select Count(*) from atClsSectAttendance where StuEnrollId=@StuEnrollId and ClsSectionId=@ClsSectionId and Tardy=1)
            --IF (@tracktardies = 1 AND @intTardyBreakPoint = @tardiesMakingAbsence) 
			/*
				IF (@tracktardies = 1 AND @intTardyBreakPoint = @tardiesMakingAbsence) 
                BEGIN
				Commented by Balaji on 2.9.2015 as this tardy calculation doesn''t take class section in to account
                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual - @Excused
                    SET @AdjustedRunningAbsentHours = (@AdjustedRunningAbsentHours - @Absent) + @ScheduledMinutes --@TardyMinutes
                    SET @intTardyBreakPoint = 0
				
                END
			*/
          
           -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
            IF (@Actual > 0 AND @Actual > @ScheduledMinutes and @Actual<>9999.00) 
                BEGIN
                    SET @MakeupHours = @MakeupHours + (@Actual - @ScheduledMinutes)
                END
	  
			  if (@tracktardies=1)
				  begin
					set @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours --+IsNULL(@ActualRunningTardyHours,0)
				  end
			  else
				begin
					set @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours
				end

		delete from syStudentAttendanceSummary where StuEnrollId=@StuEnrollId and ClsSectionId=@ClsSectionId and StudentAttendedDate=@MeetDate
		insert into syStudentAttendanceSummary
					(StuEnrollId,ClsSectionId,StudentAttendedDate,ScheduledDays,ActualDays,
					ActualRunningScheduledDays,
					ActualRunningPresentDays,
					ActualRunningAbsentDays,
					ActualRunningMakeupDays,
					ActualRunningTardyDays,
					AdjustedPresentDays,
					AdjustedAbsentDays,AttendanceTrackType,
					ModUser,ModDate,TardiesMakingAbsence,IsTardy)
				values
					(@StuEnrollId,@ClsSectionId,@MeetDate,@ScheduledMinutes,@Actual,@ActualRunningScheduledDays,@ActualRunningPresentHours,
					@ActualRunningAbsentHours,IsNULL(@MakeupHours,0),@ActualRunningTardyHours,@AdjustedPresentDaysComputed,@AdjustedRunningAbsentHours,
					''Post Attendance by Class Min'',''sa'',getdate(),@TardiesMakingAbsence,@tardy)

		update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
		set @PrevStuEnrollId=@StuEnrollId 

	FETCH NEXT FROM GetAttendance_Cursor INTO  
	@StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,
	@PeriodDescrip,@Actual,@Excused,
	@Absent, @ScheduledMinutes, @TardyMinutes,
	@tardy,@tracktardies,@tardiesMakingAbsence,@PrgVerId,@rownumber
END
CLOSE GetAttendance_Cursor
DEALLOCATE GetAttendance_Cursor

Declare @MyTardyTable table(
	ClsSectionId uniqueidentifier,
	TardiesMakingAbsence int,
	AbsentHours decimal(18,2)
) 

Insert into @MyTardyTable
Select ClsSectionId,PV.TardiesMakingAbsence,
			Case When (Count(*)>=PV.TardiesMakingAbsence) THEN (Count(*)/PV.TardiesMakingAbsence) Else 0 End as AbsentHours 
--Count(*) as NumberofTimesTardy 
from syStudentAttendanceSummary SAS inner join arStuEnrollments SE on SAS.StuEnrollId = SE.StuEnrollId 
inner join arPrgVersions PV on SE.PrgVerId = PV.PrgVerId
where SE.StuEnrollId=@StuEnrollId and SAS.IsTardy=1 and PV.TrackTardies=1
Group by ClsSectionId,PV.TardiesMakingAbsence 

--Drop table @MyTardyTable
Declare @TotalTardyAbsentDays decimal(18,2)
Set @TotalTardyAbsentDays =(Select ISNULL(SUM(AbsentHours),0) from @MyTardyTable)

--Print @TotalTardyAbsentDays

update syStudentAttendanceSummary set 
			AdjustedPresentDays=AdjustedPresentDays-@TotalTardyAbsentDays,
			AdjustedAbsentDays=AdjustedAbsentDays+@TotalTardyAbsentDays
where StuEnrollId=@StuEnrollId and StudentAttendedDate=(select Top 1 StudentAttendedDate from syStudentAttendanceSummary
														where StuEnrollId=@StuEnrollId order by StudentAttendedDate desc)
go
', 
		@database_name=@DatabaseName,
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [InsertAttendance_Class_Minutes]    Script Date: 4/6/2017 8:49:30 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'InsertAttendance_Class_Minutes', 
		@step_id=4, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'Declare @StuEnrollId uniqueidentifier,@MeetDate datetime,@WeekDay varchar(15),@StartDate datetime,@EndDate datetime
	Declare @PeriodDescrip varchar(50), @Actual decimal(18,2),@Excused decimal(18,2),@ClsSectionId uniqueidentifier
	Declare @Absent decimal(18,2), @ScheduledMinutes decimal(18,2), @TardyMinutes decimal(18,2)
	declare @tardy decimal(18,2),@tracktardies int,@tardiesMakingAbsence int,@PrgVerId uniqueidentifier,@rownumber int
	declare @ActualRunningPresentHours decimal(18,2),@ActualRunningAbsentHours decimal(18,2),@ActualRunningTardyHours decimal(18,2),@ActualRunningMakeupHours decimal(18,2)
	declare @PrevStuEnrollId uniqueidentifier,@intTardyBreakPoint int,@AdjustedRunningPresentHours decimal(18,2),@AdjustedRunningAbsentHours decimal(18,2),@ActualRunningScheduledDays decimal(18,2)
	declare @AdjustedPresentDaysComputed decimal(18,2), @MakeupHours decimal(18,2)
	Declare @boolReset bit
	DECLARE GetAttendance_Cursor CURSOR FORWARD_ONLY FAST_FORWARD FOR
	Select *,Row_Number() OVER (Order by MeetDate) as RowNumber 
	from
	( 
				Select Distinct
							t1.StuEnrollId,t1.ClsSectionId,t1.MeetDate,datename(dw,t1.MeetDate) as WeekDay,t4.StartDate,t4.EndDate,t5.PeriodDescrip,t1.Actual,t1.Excused,
							CASE WHEN (t1.Actual = 0 AND t1.Excused = 0)
							 THEN t1.Scheduled
							 ELSE 
								Case when (t1.Actual <> 9999.00 and t1.Actual < t1.Scheduled)
										THEN (t1.Scheduled - t1.Actual)
										ELSE 
											0
										End
							END AS Absent,
							t1.Scheduled as ScheduledMinutes,
							Case when (t1.Actual>0 and t1.Actual<t1.Scheduled) Then (t1.Scheduled-t1.Actual) else 0 end as TardyMinutes,
							t1.Tardy AS Tardy,
							t3.TrackTardies,
							t3.tardiesMakingAbsence,
							t3.PrgVerId 
						from
							atClsSectAttendance t1 inner join arStuEnrollments t2 on t1.StuEnrollId=t2.StuEnrollId 
							inner join arPrgVersions t3 on t2.PrgVerId=t3.PrgVerId 
							inner join arClsSectMeetings t4 on t1.ClsSectionId=t4.ClsSectionId and 
							(convert(DATE,t1.MeetDate,111)>=convert(DATE,t4.StartDate,111) and 
							 convert(DATE,t1.MeetDate,111)<=convert(DATE,t4.EndDate,111))  AND t1.ClsSectMeetingId=t4.ClsSectMeetingId --DE8479 line added
							inner join syPeriods t5 on t4.periodid=t5.periodid
							inner join cmTimeInterval t6 on t5.StartTimeId=t6.TimeIntervalId
							inner join cmTimeInterval t7 on t5.EndTimeId=t7.TimeIntervalId
							INNER JOIN dbo.arClassSections t9 ON t9.ClsSectionId = t1.ClsSectionId
							INNER JOIN dbo.arReqs t10 ON t10.ReqId = t9.ReqId
						where
							--t2.StuEnrollId =@StuEnrollId and
							(t10.UnitTypeId IN (''A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'',''B937C92E-FD7A-455E-A731-527A9918C734'')   OR t3.UnitTypeId IN (''A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'',''B937C92E-FD7A-455E-A731-527A9918C734'')) -- Minutes
							and t1.Actual<>9999
		) dt
	order by StuEnrollId, MeetDate
OPEN GetAttendance_Cursor
FETCH NEXT FROM GetAttendance_Cursor INTO  
@StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,
@PeriodDescrip,@Actual,@Excused,
@Absent, @ScheduledMinutes, @TardyMinutes,
@tardy,@tracktardies,@tardiesMakingAbsence,@PrgVerId,@rownumber
set @ActualRunningPresentHours=0
set @ActualRunningPresentHours=0
set @ActualRunningAbsentHours=0
set @ActualRunningTardyHours=0
set @ActualRunningMakeupHours=0
set @intTardyBreakPoint=0
set @AdjustedRunningPresentHours=0
set @AdjustedRunningAbsentHours=0
set @ActualRunningScheduledDays=0
set @boolReset=0
Set @MakeupHours=0
WHILE @@FETCH_STATUS = 0
BEGIN

		if @PrevStuEnrollId <> @StuEnrollId 
		begin
				set @ActualRunningPresentHours = 0
				set @ActualRunningAbsentHours = 0
				set @intTardyBreakPoint = 0
				set @ActualRunningTardyHours = 0
				set @AdjustedRunningPresentHours = 0
				set @AdjustedRunningAbsentHours = 0
				set @ActualRunningScheduledDays = 0
				set @MakeupHours=0
				set @boolReset=1
		end

	                  
            -- Scheduled and Actual: Calculate Students Actual and Adjusted Running Present Hours/Min/Day
            IF @Actual <> 9999.00 
                BEGIN
                    SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual
                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual 
                    
                     -- If there are make up hrs deduct that otherwise it will be added again in progress report
                    IF (@Actual > 0 AND @Actual > @ScheduledMinutes and @Actual<>9999.00) 
					BEGIN
						SET @ActualRunningPresentHours = @ActualRunningPresentHours - (@Actual - @ScheduledMinutes)
						SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - (@Actual - @ScheduledMinutes)
					END
                     
                    SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes                      
                END
           
			-- Absent: Calculate Students Actual and Adjusted Running Absent Hours/Min/Day
            SET @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent
            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent	
      
			-- Tardy: Calculate Students Actual Running Tardy Hours/Min/Day			
            IF ( @Actual > 0 AND @Actual < @ScheduledMinutes and @Tardy=1) 
                BEGIN
                    SET @ActualRunningTardyHours = @ActualRunningTardyHours + (@ScheduledMinutes - @Actual)
                END
					
			-- Track how many days student has been tardy only when 
			-- program version requires to track tardy
            IF (@tracktardies = 1 and @Tardy=1)
                BEGIN
                    SET @intTardyBreakPoint = @intTardyBreakPoint + 1
                END	    
           
            
            -- Tardy: If student is tardy multiple times and when the number of days student is tardy matches
            -- the tardy rule set (for ex: 2 Tardies make 1 Absence). This rule indicates that
            -- when student is tardy the second time, that second occurance will be considered as
            -- absence
            -- Variable @intTardyBreakpoint tracks how many times the student was tardy
            -- Variable @tardiesMakingAbsence tracks the tardy rule
            IF (@tracktardies = 1 AND @intTardyBreakPoint = @tardiesMakingAbsence) 
                BEGIN
                    SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual
                    SET @AdjustedRunningAbsentHours = (@AdjustedRunningAbsentHours - @Absent) + @ScheduledMinutes --@TardyMinutes
                    SET @intTardyBreakPoint = 0
                END
           
           -- Makeup: Calculate Makeup Hours when Student Attended more than scheduled hours          	       
            IF (@Actual > 0 AND @Actual > @ScheduledMinutes and @Actual<>9999.00) 
                BEGIN
                    SET @MakeupHours = @MakeupHours + (@Actual - @ScheduledMinutes)
                END
	  
	  if (@tracktardies=1)
		  begin
			set @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours --+IsNULL(@ActualRunningTardyHours,0)
		  end
	  else
		begin
			set @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours
		end
		
		
		
		
		delete from syStudentAttendanceSummary where StuEnrollId=@StuEnrollId and ClsSectionId=@ClsSectionId and StudentAttendedDate=@MeetDate
		insert into syStudentAttendanceSummary
					(StuEnrollId,ClsSectionId,StudentAttendedDate,ScheduledDays,ActualDays,
					ActualRunningScheduledDays,
					ActualRunningPresentDays,
					ActualRunningAbsentDays,
					ActualRunningMakeupDays,
					ActualRunningTardyDays,
					AdjustedPresentDays,
					AdjustedAbsentDays,AttendanceTrackType,
					ModUser,ModDate,TardiesMakingAbsence)
				values
					(@StuEnrollId,@ClsSectionId,@MeetDate,@ScheduledMinutes,@Actual,@ActualRunningScheduledDays,@ActualRunningPresentHours,
					@ActualRunningAbsentHours,IsNULL(@MakeupHours,0),@ActualRunningTardyHours,@AdjustedPresentDaysComputed,@AdjustedRunningAbsentHours,''Post Attendance by Class Min'',''sa'',getdate(),@TardiesMakingAbsence)

		--update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
	  set @PrevStuEnrollId=@StuEnrollId 

	FETCH NEXT FROM GetAttendance_Cursor INTO  
	@StuEnrollId,@ClsSectionId,@MeetDate,@WeekDay,@StartDate,@EndDate,
	@PeriodDescrip,@Actual,@Excused,
	@Absent, @ScheduledMinutes, @TardyMinutes,
	@tardy,@tracktardies,@tardiesMakingAbsence,@PrgVerId,@rownumber
END
CLOSE GetAttendance_Cursor
DEALLOCATE GetAttendance_Cursor
go
', 
		@database_name=@DatabaseName, 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [InsertAttendance_Day_Minutes]    Script Date: 4/6/2017 8:49:30 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'InsertAttendance_Day_Minutes', 
		@step_id=5, 
		@cmdexec_success_code=0, 
		@on_success_action=3, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'Declare @StuEnrollId uniqueidentifier,@MeetDate datetime,@WeekDay varchar(15),@StartDate datetime,@EndDate datetime
	Declare @PeriodDescrip varchar(50), @Actual decimal(18,2),@Excused decimal(18,2),@ClsSectionId uniqueidentifier
	Declare @Absent decimal(18,2), @ScheduledMinutes decimal(18,2), @TardyMinutes decimal(18,2)
	declare @tardy decimal(18,2),@tracktardies int,@tardiesMakingAbsence int,@PrgVerId uniqueidentifier,@rownumber int,@IsTardy bit,@ActualRunningScheduledHours decimal(18,2)
	declare @ActualRunningPresentHours decimal(18,2),@ActualRunningAbsentHours decimal(18,2),@ActualRunningTardyHours decimal(18,2),@ActualRunningMakeupHours decimal(18,2)
	declare @PrevStuEnrollId uniqueidentifier,@intTardyBreakPoint int,@AdjustedRunningPresentHours decimal(18,2),@AdjustedRunningAbsentHours decimal(18,2),@ActualRunningScheduledDays decimal(18,2)
	DECLARE GetAttendance_Cursor CURSOR FOR
			Select t1.StuEnrollId,NULL as ClsSectionId,t1.RecordDate as MeetDate,
			t1.ActualHours,	t1.SchedHours as ScheduledMinutes,
			Case when ((t1.SchedHours >= 1 and t1.SchedHours not in (999,9999)) and t1.ActualHours=0)  Then t1.SchedHours else 0 end as Absent,
			t1.IsTardy,
				(	
						Select Sum(SchedHours) from arStudentClockAttendance 
						where StuEnrollId=t1.StuEnrollId and RecordDate <= t1.RecordDate
						and (t1.SchedHours >= 1 and t1.SchedHours not in (999,9999) and t1.ActualHours not in (999,9999))
				) as ActualRunningScheduledHours,
				(	
						Select Sum(ActualHours) from arStudentClockAttendance 
						where StuEnrollId=t1.StuEnrollId and RecordDate <= t1.RecordDate
						and (t1.SchedHours >= 1 and t1.SchedHours not in (999,9999)) and ActualHours>=1 and 
						ActualHours not in (999,9999)
				) as ActualRunningPresentHours,
				(	
						Select Count(ActualHours) from arStudentClockAttendance 
						where StuEnrollId=t1.StuEnrollId and RecordDate <= t1.RecordDate
						and (t1.SchedHours >= 1 and t1.SchedHours not in (999,9999)) and ActualHours=0
						and ActualHours not in (999,9999)
				) as ActualRunningAbsentHours,
				(
						Select Sum(ActualHours) from arStudentClockAttendance 
						where StuEnrollId=t1.StuEnrollId and RecordDate <= t1.RecordDate
						and SchedHours=0 and ActualHours>=1 and ActualHours not in (999,9999)
				) as ActualRunningMakeupHours,
				(	
						Select Sum(SchedHours - ActualHours) from arStudentClockAttendance 
						where StuEnrollId=t1.StuEnrollId and RecordDate <= t1.RecordDate
						and ((t1.SchedHours >= 1 and t1.SchedHours not in (999,9999)) and ActualHours >= 1
						and ActualHours not in (999,9999)) and IsTardy=1
				) as ActualRunningTardyHours,
			t3.TrackTardies,
			t3.tardiesMakingAbsence,
			t3.PrgVerId,
			Row_Number() OVER (Order by t1.RecordDate) as RowNumber 
		from
			arStudentClockAttendance t1 inner join arStuEnrollments t2 on t1.StuEnrollId=t2.StuEnrollId 
			inner join arPrgVersions t3 on t2.PrgVerId=t3.PrgVerId 
		where 
			t3.UnitTypeId in (''A1389C74-0BB9-4BBF-A47F-68428BE7FA4D'')
			and t1.ActualHours <> 9999.00
			--and t1.StuEnrollId=''4556D66D-D435-488D-8015-39037A8BBA14''
			order by t1.StuEnrollId,t1.RecordDate
OPEN GetAttendance_Cursor
Declare @ActualHours decimal(18,2),@SchedHours decimal(18,2)
FETCH NEXT FROM GetAttendance_Cursor INTO  
@StuEnrollId,@ClsSectionId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,
@IsTardy,@ActualRunningScheduledHours,@ActualRunningPresentHours,
@ActualRunningAbsentHours,@ActualRunningMakeupHours,@ActualRunningTardyHours,
@TrackTardies,@tardiesMakingAbsence,@PrgVerId,@RowNumber

set @ActualRunningPresentHours=0
set @ActualRunningPresentHours=0
set @ActualRunningAbsentHours=0
set @ActualRunningTardyHours=0
set @ActualRunningMakeupHours=0
set @intTardyBreakPoint=0
set @AdjustedRunningPresentHours=0
set @AdjustedRunningAbsentHours=0
set @ActualRunningScheduledDays=0
WHILE @@FETCH_STATUS = 0
BEGIN

		if @PrevStuEnrollId <> @StuEnrollId 
		begin
				set @ActualRunningPresentHours = 0
				set @ActualRunningAbsentHours = 0
				set @intTardyBreakPoint = 0
				set @ActualRunningTardyHours = 0
				set @AdjustedRunningPresentHours = 0
				set @AdjustedRunningAbsentHours = 0
				set @ActualRunningScheduledDays = 0
		end

	    if (@ScheduledMinutes>=1 and (@Actual <> 9999 and @Actual <> 999) and (@ScheduledMinutes <> 9999 and @Actual <> 999))
			begin
				set @ActualRunningScheduledDays = IsNULL(@ActualRunningScheduledDays,0) +  IsNULL(@ScheduledMinutes,0)
			end
		else
			begin
				set @ActualRunningScheduledDays = IsNULL(@ActualRunningScheduledDays,0) 
			end
		
		if (@Actual <> 9999 and @Actual <> 999)
		begin
			set @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual
		end
		set @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent
		-- Absent hours
		--1. sched = 5, Actual = 2 then Ab = (5-3) = 2
		if (@Actual>0 and @Actual<@ScheduledMinutes)
		begin
			set @ActualRunningAbsentHours = @ActualRunningAbsentHours + (@ScheduledMinutes-@Actual)
		end
		-- Make up hours
		--sched=5, Actual =7, makeup= 2,ab = 0
		--sched=0, Actual =7, makeup= 7,ab = 0
		if (@Actual>0 and @ScheduledMinutes>0 and @Actual>@ScheduledMinutes and (@Actual <> 9999 and @Actual <> 999))
		begin
			set @ActualRunningMakeupHours = @ActualRunningMakeupHours + (@Actual-@ScheduledMinutes)
		end
--		if (@Actual>0 and @ScheduledMinutes=0 and (@Actual <> 9999.00 and @Actual <> 999.00))
--		begin
--			set @ActualRunningMakeupHours = @ActualRunningMakeupHours + (@Actual-@ScheduledMinutes)
--		end
		if (@Actual <> 9999 and @Actual <> 999)
		begin
			set @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual
		end	
		if (@Actual=0 and @ScheduledMinutes >= 1 and @ScheduledMinutes not in (999,9999)) 
		begin
			set @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent
		end
		-- Absent hours
		--1. sched = 5, Actual = 2 then Ab = (5-3) = 2
	   if (@Absent=0 and @ActualRunningAbsentHours>0 and (@Actual<@ScheduledMinutes))
		begin
			set @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + (@ScheduledMinutes-@Actual)	
		end
 	    if (@Actual>0 and @Actual<@ScheduledMinutes and (@Actual <> 9999.00 and @Actual <> 999.00))
		begin
			set @ActualRunningTardyHours = @ActualRunningTardyHours + (@ScheduledMinutes-@Actual)
		end
	
		if @tracktardies=1 and (@TardyMinutes > 0 or @IsTardy=1)
		begin
			set @intTardyBreakPoint = @intTardyBreakPoint + 1
		end	    
		if (@tracktardies=1 and @intTardyBreakPoint=@tardiesMakingAbsence)
		begin
			set @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual
			set @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @ScheduledMinutes --@TardyMinutes
			set @intTardyBreakPoint=0
		end

				insert into syStudentAttendanceSummary
					(StuEnrollId,ClsSectionId,StudentAttendedDate,ScheduledDays,ActualDays,
					ActualRunningScheduledDays,
					ActualRunningPresentDays,
					ActualRunningAbsentDays,
					ActualRunningMakeupDays,
					ActualRunningTardyDays,
					AdjustedPresentDays,
					AdjustedAbsentDays,AttendanceTrackType,
					ModUser,ModDate)
				values
					(@StuEnrollId,@ClsSectionId,@MeetDate,@ScheduledMinutes,@Actual,@ActualRunningScheduledDays,@ActualRunningPresentHours,
					@ActualRunningAbsentHours,@ActualRunningMakeupHours,@ActualRunningTardyHours,@AdjustedRunningPresentHours,@AdjustedRunningAbsentHours,''Post Attendance by Class'',''sa'',getdate())

update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence where StuEnrollId=@StuEnrollId
			--end
	  set @PrevStuEnrollId=@StuEnrollId 

	FETCH NEXT FROM GetAttendance_Cursor INTO  
	@StuEnrollId,@ClsSectionId,@MeetDate,@Actual,@ScheduledMinutes,@Absent,
	@IsTardy,@ActualRunningScheduledHours,@ActualRunningPresentHours,
	@ActualRunningAbsentHours,@ActualRunningMakeupHours,@ActualRunningTardyHours,
	@TrackTardies,@tardiesMakingAbsence,@PrgVerId,@RowNumber
END
CLOSE GetAttendance_Cursor
DEALLOCATE GetAttendance_Cursor
go
', 
		@database_name=@DatabaseName, 
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
/****** Object:  Step [InsertAttendance_Class_ClockHour]    Script Date: 4/6/2017 8:49:30 AM ******/
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'InsertAttendance_Class_ClockHour', 
		@step_id=6, 
		@cmdexec_success_code=0, 
		@on_success_action=1, 
		@on_success_step_id=0, 
		@on_fail_action=2, 
		@on_fail_step_id=0, 
		@retry_attempts=0, 
		@retry_interval=0, 
		@os_run_priority=0, @subsystem=N'TSQL', 
		@command=N'Declare @StuEnrollId uniqueidentifier,@MeetDate datetime,@WeekDay varchar(15),@StartDate datetime,@EndDate datetime
	Declare @PeriodDescrip varchar(50), @Actual decimal(18,2),@Excused decimal(18,2),@ClsSectionId uniqueidentifier
	Declare @Absent decimal(18,2), @ScheduledMinutes decimal(18,2), @TardyMinutes decimal(18,2)
	declare @tardy decimal(18,2),@tracktardies int,@tardiesMakingAbsence int,@PrgVerId uniqueidentifier,@rownumber int
	declare @ActualRunningPresentHours decimal(18,2),@ActualRunningAbsentHours decimal(18,2)
	DECLARE @ActualRunningTardyHours decimal(18,2),@ActualRunningMakeupHours decimal(18,2)
	declare @PrevStuEnrollId uniqueidentifier,@intTardyBreakPoint int,@AdjustedRunningPresentHours decimal(18,2),
	@AdjustedRunningAbsentHours decimal(18,2),@ActualRunningScheduledDays decimal(18,2),@ActualRunningExcusedDays int
	declare @AdjustedPresentDaysComputed decimal(18,2),@PeriodDescrip1 VARCHAR(500),@MakeupHours DECIMAL(18,2)
	DECLARE GetAttendance_Cursor CURSOR FOR
	Select *,Row_Number() OVER (Order by MeetDate) as RowNumber 
	from
	( 
	
	Select DISTINCT
				t1.StuEnrollId,t1.ClsSectionId,
				(SELECT Descrip FROM arReqs A1,dbo.arClassSections A2 WHERE A1.ReqId=A2.ReqId AND A2.ClsSectionId=t1.ClsSectionId)  + ''  '' + 
				t5.PeriodDescrip AS PeriodDescrip1,
				t1.MeetDate,datename(dw,t1.MeetDate) as WeekDay,t4.StartDate,t4.EndDate,t5.PeriodDescrip,
				CASE WHEN (t1.Actual>=0 AND t1.Actual<>9999) THEN t1.Actual/cast(60 as float) ELSE t1.Actual END AS Actual,
				t1.Excused,
				Case when (t1.Actual>=0 AND t1.Actual<>9999 AND (t1.Actual/cast(60 as float))<t1.scheduled/CAST(60 AS FLOAT))
				Then ((t1.Scheduled/CAST(60 AS FLOAT))-(t1.Actual/cast(60 as float)))	
				else 0 end as Absent,
				CASE WHEN t1.Scheduled>0 THEN (t1.scheduled/CAST(60 AS FLOAT)) ELSE 0 END 
				as ScheduledMinutes,
				NULL AS TardyMinutes,
				CASE WHEN t1.Tardy=1 THEN 1 ELSE 0 END AS Tardy,
				t3.TrackTardies,
				t3.tardiesMakingAbsence,
				t3.PrgVerId
			from
			atClsSectAttendance t1 inner join arStuEnrollments t2 on t1.StuEnrollId=t2.StuEnrollId 
			inner join arPrgVersions t3 on t2.PrgVerId=t3.PrgVerId 
			inner join arClsSectMeetings t4 on t1.ClsSectionId=t4.ClsSectionId and 
			(convert(DATE,t1.MeetDate,111)>=convert(DATE,t4.StartDate,111) and 
			DateDiff(day,t1.MeetDate,t4.EndDate) >=0
)  AND t1.ClsSectMeetingId=t4.ClsSectMeetingId --DE8522 line added
			inner join syPeriods t5 on t4.periodid=t5.periodid 
			inner join cmTimeInterval t6 on t5.StartTimeId=t6.TimeIntervalId
			inner join cmTimeInterval t7 on t5.EndTimeId=t7.TimeIntervalId
			INNER JOIN syPeriodsWorkDays PWD ON t5.PeriodId=PWD.PeriodId 
			INNER JOIN plWorkDays WD ON WD.WorkDaysId=PWD.WorkDayId
			--inner join Inserted t20 on t20.ClsSectAttId = t1.ClsSectAttId --Inserted Table
 				INNER JOIN dbo.arClassSections t8 ON t8.ClsSectionId = t1.ClsSectionId
                         		   INNER JOIN dbo.arReqs t9 ON t9.ReqId = t8.ReqId
		where
			--t2.StuEnrollId=''525B5DB6-6EF8-47AE-A5C3-8DAFC71FB471'' and
			( t9.UnitTypeId IN (''B937C92E-FD7A-455E-A731-527A9918C734'' )  OR t3.UnitTypeId IN (''B937C92E-FD7A-455E-A731-527A9918C734'' )) -- Clock Hours
			--AND t1.ClsSectionId=''3F4CD85B-CFED-47BE-BC79-4B926DDFD2B3''
			AND t1.Actual <> 9999.00 
			AND Substring(datename(dw,t1.MeetDate),1,3) = Substring(WD.WorkDaysDescrip,1,3)
			
		-- Some times Makeup days don''t fall inside the schedule
		-- ex: there may be a schedule for T-Fri and school may mark attendance for saturday
		-- the following query will bring whatever was left out in above query
		UNION
		Select DISTINCT
		--t1.ClsSectAttId,
				t1.StuEnrollId,t1.ClsSectionId,
				(SELECT Descrip FROM arReqs A1,dbo.arClassSections A2 WHERE A1.ReqId=A2.ReqId AND A2.ClsSectionId=t1.ClsSectionId)  + ''  '' + 
				t5.PeriodDescrip AS PeriodDescrip1,
				t1.MeetDate,datename(dw,t1.MeetDate) as WeekDay,t4.StartDate,t4.EndDate,t5.PeriodDescrip,
				CASE WHEN (t1.Actual>=0 AND t1.Actual<>9999) THEN t1.Actual/cast(60 as float) ELSE t1.Actual END AS Actual,
				t1.Excused,
				Case when (t1.Actual>=0 AND t1.Actual<>9999 AND (t1.Actual/cast(60 as float))<t1.scheduled/CAST(60 AS FLOAT))
				Then ((t1.Scheduled/CAST(60 AS FLOAT))-(t1.Actual/cast(60 as float)))	
				else 0 end as Absent,
				CASE WHEN t1.Scheduled>0 THEN (t1.scheduled/CAST(60 AS FLOAT)) ELSE 0 END 
				as ScheduledMinutes,
				NULL AS TardyMinutes,
				CASE WHEN t1.Tardy=1 THEN 1 ELSE 0 END AS Tardy,
				t3.TrackTardies,
				t3.tardiesMakingAbsence,
				t3.PrgVerId
		from
			atClsSectAttendance t1 inner join arStuEnrollments t2 on t1.StuEnrollId=t2.StuEnrollId 
			inner join arPrgVersions t3 on t2.PrgVerId=t3.PrgVerId 
			inner join arClsSectMeetings t4 on t1.ClsSectionId=t4.ClsSectionId and 
			(convert(DATE,t1.MeetDate,111)>=convert(DATE,t4.StartDate,111) and 
			 --convert(DATE,t1.MeetDate,111)<=convert(DATE,t4.EndDate,111)
                                               DateDiff(day,t1.MeetDate,t4.EndDate) >=1
) AND t1.ClsSectMeetingId=t4.ClsSectMeetingId --DE8522 line added
			inner join syPeriods t5 on t4.periodid=t5.periodid 
			inner join cmTimeInterval t6 on t5.StartTimeId=t6.TimeIntervalId
			inner join cmTimeInterval t7 on t5.EndTimeId=t7.TimeIntervalId
			INNER JOIN syPeriodsWorkDays PWD ON t5.PeriodId=PWD.PeriodId 
			INNER JOIN plWorkDays WD ON WD.WorkDaysId=PWD.WorkDayId
			--inner join Inserted t20 on t20.ClsSectAttId = t1.ClsSectAttId --Inserted Table
 			INNER JOIN dbo.arClassSections t8 ON t8.ClsSectionId = t1.ClsSectionId
                           		 INNER JOIN dbo.arReqs t9 ON t9.ReqId = t8.ReqId
		where
			--t2.StuEnrollId=''525B5DB6-6EF8-47AE-A5C3-8DAFC71FB471'' and
			( t9.UnitTypeId IN (''B937C92E-FD7A-455E-A731-527A9918C734'' )  OR t3.UnitTypeId IN (''B937C92E-FD7A-455E-A731-527A9918C734'' )) -- Clock Hours
			AND t1.Actual <> 9999.00 
			AND t1.MeetDate NOT IN 
			(
				SELECT t1.MeetDate
				from
					atClsSectAttendance t1 inner join arStuEnrollments t2 on t1.StuEnrollId=t2.StuEnrollId 
					inner join arPrgVersions t3 on t2.PrgVerId=t3.PrgVerId 
					inner join arClsSectMeetings t4 on t1.ClsSectionId=t4.ClsSectionId and 
					(convert(DATE,t1.MeetDate,111)>=convert(DATE,t4.StartDate,111) and 
					 convert(DATE,t1.MeetDate,111)<=convert(DATE,t4.EndDate,111))
					inner join syPeriods t5 on t4.periodid=t5.periodid 
					inner join cmTimeInterval t6 on t5.StartTimeId=t6.TimeIntervalId
					inner join cmTimeInterval t7 on t5.EndTimeId=t7.TimeIntervalId
					INNER JOIN syPeriodsWorkDays PWD ON t5.PeriodId=PWD.PeriodId 
					INNER JOIN plWorkDays WD ON WD.WorkDaysId=PWD.WorkDayId
				where
					--t2.StuEnrollId=''525B5DB6-6EF8-47AE-A5C3-8DAFC71FB471''   and
					( t9.UnitTypeId IN (''B937C92E-FD7A-455E-A731-527A9918C734'' )  OR t3.UnitTypeId IN (''B937C92E-FD7A-455E-A731-527A9918C734'' )) -- Clock Hours
					AND t1.Actual <> 9999.00  and t1.ClsSectionId=t8.ClsSectionId
					AND Substring(datename(dw,t1.MeetDate),1,3) = Substring(WD.WorkDaysDescrip,1,3) 
			)
		) dt
	order by StuEnrollId, MeetDate
	
	
OPEN GetAttendance_Cursor
FETCH NEXT FROM GetAttendance_Cursor INTO  
@StuEnrollId,@ClsSectionId,@PeriodDescrip1,@MeetDate,@WeekDay,@StartDate,@EndDate,
@PeriodDescrip,@Actual,@Excused,
@Absent, @ScheduledMinutes, @TardyMinutes,
@tardy,@tracktardies,@tardiesMakingAbsence,@PrgVerId,@rownumber
set @ActualRunningPresentHours=0
set @ActualRunningPresentHours=0
set @ActualRunningAbsentHours=0
set @ActualRunningTardyHours=0
set @ActualRunningMakeupHours=0
set @intTardyBreakPoint=0
set @AdjustedRunningPresentHours=0
set @AdjustedRunningAbsentHours=0
set @ActualRunningScheduledDays=0
SET @MakeupHours=0
Set @ActualRunningExcusedDays=0
WHILE @@FETCH_STATUS = 0
BEGIN

		if @PrevStuEnrollId <> @StuEnrollId 
		begin
				set @ActualRunningPresentHours = 0
				set @ActualRunningAbsentHours = 0
				set @intTardyBreakPoint = 0
				set @ActualRunningTardyHours = 0
				set @AdjustedRunningPresentHours = 0
				set @AdjustedRunningAbsentHours = 0
				set @ActualRunningScheduledDays = 0
				SET @MakeupHours = 0
				SET @ActualRunningExcusedDays = 0
		end

	    -- Calculate : Actual Running Scheduled Days
            SET @ActualRunningScheduledDays = @ActualRunningScheduledDays + @ScheduledMinutes
            
            -- Calculate: Actual and Adjusted Running Present Hours
            IF @Actual <> 9999 
                BEGIN
                    IF (@Actual > 0 AND @Actual > @ScheduledMinutes) -- Makeup Hours
                        BEGIN
                            SET @ActualRunningPresentHours = @ActualRunningPresentHours + @Actual - ( @Actual - @ScheduledMinutes ) -- Subtract Makeup Hours from Actual
                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours + @Actual - ( @Actual - @ScheduledMinutes ) -- Subtract Makeup Hours from Actual
                        END 
                    ELSE 
                        BEGIN
                            SET @ActualRunningPresentHours = @ActualRunningPresentHours
                                + @Actual 
                            SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours
                                + @Actual
                        END
                END 
            
            -- Calculate: Adjusted Running Absent Hours
            Set @ActualRunningAbsentHours = @ActualRunningAbsentHours + @Absent
            SET @AdjustedRunningAbsentHours = @AdjustedRunningAbsentHours + @Absent	
            Set @TardyMinutes = (@ScheduledMinutes - @Actual) 
			If (@Excused=1)
			begin
				Set @ActualRunningExcusedDays = @ActualRunningExcusedDays + 1
			end
             
            IF (@Actual > 0 AND @Actual < @ScheduledMinutes and @Tardy=1) 
                BEGIN
                    SET @ActualRunningTardyHours = @ActualRunningTardyHours + (@ScheduledMinutes - @Actual)
                END
	
            IF ( @Actual > 0 AND @Actual > @ScheduledMinutes) 
                BEGIN
                    SET @MakeupHours = @MakeupHours + (@Actual- @ScheduledMinutes)
                END
            IF (@tracktardies = 1 AND @TardyMinutes > 0 and @Tardy=1) 
                BEGIN
                    SET @intTardyBreakPoint = @intTardyBreakPoint + 1
                END	    
            --IF (@tracktardies = 1 AND @intTardyBreakPoint = @tardiesMakingAbsence) 
            --    BEGIN
		          --  SET @AdjustedRunningPresentHours = @AdjustedRunningPresentHours - @Actual
            --        SET @AdjustedRunningAbsentHours = (@AdjustedRunningAbsentHours - @Absent) + @ScheduledMinutes --@TardyMinutes
            --        SET @ActualRunningTardyHours = @ActualRunningTardyHours - (@ScheduledMinutes - @Actual)
            --        SET @intTardyBreakPoint = 0
            --    END
	  
	  
	 -- if (@tracktardies=1)
		--  begin
		--	set @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours --+IsNULL(@ActualRunningTardyHours,0)
		--  end
	 -- else
		begin
			set @AdjustedPresentDaysComputed = @AdjustedRunningPresentHours
		end
		
		
				
		delete from syStudentAttendanceSummary where 
		StuEnrollId=@StuEnrollId and ClsSectionId=@ClsSectionId and StudentAttendedDate=@MeetDate
		
		insert into syStudentAttendanceSummary
					(StuEnrollId,ClsSectionId,StudentAttendedDate,ScheduledDays,ActualDays,
					ActualRunningScheduledDays,
					ActualRunningPresentDays,
					ActualRunningAbsentDays,
					ActualRunningMakeupDays,
					ActualRunningTardyDays,
					AdjustedPresentDays,
					AdjustedAbsentDays,AttendanceTrackType,
					ModUser,ModDate,IsExcused,ActualRunningExcusedDays,IsTardy)
				values
					(@StuEnrollId,@ClsSectionId,@MeetDate,@ScheduledMinutes,@Actual,
					@ActualRunningScheduledDays,
					@ActualRunningPresentHours,
					@ActualRunningAbsentHours,
					ISNULL(@MakeupHours,0),
					@ActualRunningTardyHours,
					@AdjustedPresentDaysComputed,@AdjustedRunningAbsentHours,
					''Post Attendance by Class Clock'',''sa'',getdate(),@Excused,@ActualRunningExcusedDays,@tardy)

		update syStudentAttendanceSummary set TardiesMakingAbsence=@TardiesMakingAbsence 
		where StuEnrollId=@StuEnrollId
		set @PrevStuEnrollId=@StuEnrollId 

	FETCH NEXT FROM GetAttendance_Cursor INTO  
	@StuEnrollId,@ClsSectionId,@PeriodDescrip1,@MeetDate,@WeekDay,@StartDate,@EndDate,
	@PeriodDescrip,@Actual,@Excused,
	@Absent, @ScheduledMinutes, @TardyMinutes,
	@tardy,@tracktardies,@tardiesMakingAbsence,@PrgVerId,@rownumber
END
CLOSE GetAttendance_Cursor
DEALLOCATE GetAttendance_Cursor

Declare @MyTardyTable table(
	ClsSectionId uniqueidentifier,
	TardiesMakingAbsence int,
	AbsentHours decimal(18,2)
) 

Insert into @MyTardyTable
Select ClsSectionId,PV.TardiesMakingAbsence,
Case When (Count(*)>=PV.TardiesMakingAbsence) THEN ((Count(*)/PV.TardiesMakingAbsence) * SAS.ScheduledDays)  Else 0 End as AbsentHours 
--Count(*) as NumberofTimesTardy 
from syStudentAttendanceSummary SAS inner join arStuEnrollments SE on SAS.StuEnrollId = SE.StuEnrollId 
inner join arPrgVersions PV on SE.PrgVerId = PV.PrgVerId
where SE.StuEnrollId=@StuEnrollId and SAS.IsTardy=1 and PV.TrackTardies=1
Group by ClsSectionId,PV.TardiesMakingAbsence, SAS.ScheduledDays

--Drop table @MyTardyTable
Declare @TotalTardyAbsentDays decimal(18,2)
Set @TotalTardyAbsentDays =(Select ISNULL(SUM(AbsentHours),0)  from @MyTardyTable)

--Print @TotalTardyAbsentDays

update syStudentAttendanceSummary set 
			AdjustedPresentDays=AdjustedPresentDays-@TotalTardyAbsentDays,
			AdjustedAbsentDays=AdjustedAbsentDays+@TotalTardyAbsentDays
where StuEnrollId=@StuEnrollId and StudentAttendedDate=(select Top 1 StudentAttendedDate from syStudentAttendanceSummary
														where StuEnrollId=@StuEnrollId order by StudentAttendedDate desc)

GO
', 
		@database_name=@DatabaseName,
		@flags=0
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @jobId, @start_step_id = 1
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name=N'MAUAttendanceJob', 
		@enabled=1, 
		@freq_type=4, 
		@freq_interval=1, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=0, 
		@active_start_date=20150106, 
		@active_end_date=99991231, 
		@active_start_time=20000, 
		@active_end_time=235959, 
		@schedule_uid=N'8bc1b4f3-a1bc-4345-88bb-f3383eea46f6'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
EXEC @ReturnCode = msdb.dbo.sp_add_jobserver @job_id = @jobId, @server_name = N'(local)'
IF (@@ERROR <> 0 OR @ReturnCode <> 0) GOTO QuitWithRollback
COMMIT TRANSACTION
GOTO EndSave
QuitWithRollback:
    IF (@@TRANCOUNT > 0) ROLLBACK TRANSACTION
EndSave:




