﻿-- Insert a record in Tenant if does not exists
IF NOT EXISTS (SELECT * FROM dbo.Tenant WHERE TenantName = @DataBaseName)
BEGIN

INSERT INTO dbo.Tenant
        (
         TenantName
        ,CreatedDate
        ,ModifiedDate
        ,ModifiedBy
        ,ServerName
        ,DatabaseName
        ,UserName
        ,Password
        ,EnvironmentID
        )
VALUES  (
         @DataBaseName  -- TenantName - varchar(300)
        ,GETDATE()  -- CreatedDate - datetime
        ,GETDATE()  -- ModifiedDate - datetime
        ,'SUPPORT'  -- ModifiedBy - varchar(50)
        ,@ServerName  -- ServerName - varchar(50)
        ,@DatabaseName  -- DatabaseName - varchar(50)
        ,@UserName  -- UserName - varchar(50)
        ,@Password  -- Password - varchar(50)
        ,@EnvironmentId  -- EnvironmentID - int
        )
END